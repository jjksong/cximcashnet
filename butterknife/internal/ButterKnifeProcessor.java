package butterknife.internal;

import butterknife.Bind;
import butterknife.BindBool;
import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindDrawable;
import butterknife.BindInt;
import butterknife.BindString;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import butterknife.OnItemSelected;
import butterknife.OnLongClick;
import butterknife.OnPageChange;
import butterknife.OnTextChanged;
import butterknife.OnTouch;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

public final class ButterKnifeProcessor
  extends AbstractProcessor
{
  public static final String ANDROID_PREFIX = "android.";
  private static final String COLOR_STATE_LIST_TYPE = "android.content.res.ColorStateList";
  private static final String DRAWABLE_TYPE = "android.graphics.drawable.Drawable";
  private static final String ITERABLE_TYPE = "java.lang.Iterable<?>";
  public static final String JAVA_PREFIX = "java.";
  private static final List<Class<? extends Annotation>> LISTENERS = Arrays.asList(new Class[] { OnCheckedChanged.class, OnClick.class, OnEditorAction.class, OnFocusChange.class, OnItemClick.class, OnItemLongClick.class, OnItemSelected.class, OnLongClick.class, OnPageChange.class, OnTextChanged.class, OnTouch.class });
  private static final String LIST_TYPE = List.class.getCanonicalName();
  private static final String NULLABLE_ANNOTATION_NAME = "Nullable";
  public static final String SUFFIX = "$$ViewBinder";
  static final String VIEW_TYPE = "android.view.View";
  private Elements elementUtils;
  private Filer filer;
  private Types typeUtils;
  
  private String doubleErasure(TypeMirror paramTypeMirror)
  {
    String str = this.typeUtils.erasure(paramTypeMirror).toString();
    int i = str.indexOf('<');
    paramTypeMirror = str;
    if (i != -1) {
      paramTypeMirror = str.substring(0, i);
    }
    return paramTypeMirror;
  }
  
  private void error(Element paramElement, String paramString, Object... paramVarArgs)
  {
    String str = paramString;
    if (paramVarArgs.length > 0) {
      str = String.format(paramString, paramVarArgs);
    }
    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, str, paramElement);
  }
  
  private void findAndParseListener(RoundEnvironment paramRoundEnvironment, Class<? extends Annotation> paramClass, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    Iterator localIterator = paramRoundEnvironment.getElementsAnnotatedWith(paramClass).iterator();
    while (localIterator.hasNext())
    {
      Element localElement = (Element)localIterator.next();
      try
      {
        parseListenerAnnotation(paramClass, localElement, paramMap, paramSet);
      }
      catch (Exception localException)
      {
        paramRoundEnvironment = new StringWriter();
        localException.printStackTrace(new PrintWriter(paramRoundEnvironment));
        error(localElement, "Unable to generate view binder for @%s.\n\n%s", new Object[] { paramClass.getSimpleName(), paramRoundEnvironment.toString() });
      }
    }
  }
  
  private Map<TypeElement, BindingClass> findAndParseTargets(RoundEnvironment paramRoundEnvironment)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    Iterator localIterator = paramRoundEnvironment.getElementsAnnotatedWith(Bind.class).iterator();
    while (localIterator.hasNext())
    {
      Element localElement1 = (Element)localIterator.next();
      try
      {
        parseBind(localElement1, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException1)
      {
        logParsingError(localElement1, Bind.class, localException1);
      }
    }
    Object localObject1 = LISTENERS.iterator();
    while (((Iterator)localObject1).hasNext()) {
      findAndParseListener(paramRoundEnvironment, (Class)((Iterator)localObject1).next(), localLinkedHashMap, localLinkedHashSet);
    }
    localIterator = paramRoundEnvironment.getElementsAnnotatedWith(BindBool.class).iterator();
    while (localIterator.hasNext())
    {
      localObject1 = (Element)localIterator.next();
      try
      {
        parseResourceBool((Element)localObject1, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException3)
      {
        logParsingError((Element)localObject1, BindBool.class, localException3);
      }
    }
    localIterator = paramRoundEnvironment.getElementsAnnotatedWith(BindColor.class).iterator();
    while (localIterator.hasNext())
    {
      localObject1 = (Element)localIterator.next();
      try
      {
        parseResourceColor((Element)localObject1, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException4)
      {
        logParsingError((Element)localObject1, BindColor.class, localException4);
      }
    }
    localIterator = paramRoundEnvironment.getElementsAnnotatedWith(BindDimen.class).iterator();
    while (localIterator.hasNext())
    {
      localObject1 = (Element)localIterator.next();
      try
      {
        parseResourceDimen((Element)localObject1, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException5)
      {
        logParsingError((Element)localObject1, BindDimen.class, localException5);
      }
    }
    localObject1 = paramRoundEnvironment.getElementsAnnotatedWith(BindDrawable.class).iterator();
    Element localElement2;
    while (((Iterator)localObject1).hasNext())
    {
      localElement2 = (Element)((Iterator)localObject1).next();
      try
      {
        parseResourceDrawable(localElement2, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException7)
      {
        logParsingError(localElement2, BindDrawable.class, localException7);
      }
    }
    Object localObject4 = paramRoundEnvironment.getElementsAnnotatedWith(BindInt.class).iterator();
    while (((Iterator)localObject4).hasNext())
    {
      localElement2 = (Element)((Iterator)localObject4).next();
      try
      {
        parseResourceInt(localElement2, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException2)
      {
        logParsingError(localElement2, BindInt.class, localException2);
      }
    }
    Object localObject2 = paramRoundEnvironment.getElementsAnnotatedWith(BindString.class).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      paramRoundEnvironment = (Element)((Iterator)localObject2).next();
      try
      {
        parseResourceString(paramRoundEnvironment, localLinkedHashMap, localLinkedHashSet);
      }
      catch (Exception localException6)
      {
        logParsingError(paramRoundEnvironment, BindString.class, localException6);
      }
    }
    paramRoundEnvironment = localLinkedHashMap.entrySet().iterator();
    while (paramRoundEnvironment.hasNext())
    {
      Object localObject3 = (Map.Entry)paramRoundEnvironment.next();
      localObject2 = findParentFqcn((TypeElement)((Map.Entry)localObject3).getKey(), localLinkedHashSet);
      if (localObject2 != null)
      {
        localObject3 = (BindingClass)((Map.Entry)localObject3).getValue();
        localObject4 = new StringBuilder();
        ((StringBuilder)localObject4).append((String)localObject2);
        ((StringBuilder)localObject4).append("$$ViewBinder");
        ((BindingClass)localObject3).setParentViewBinder(((StringBuilder)localObject4).toString());
      }
    }
    return localLinkedHashMap;
  }
  
  private static Integer findDuplicate(int[] paramArrayOfInt)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    int j = paramArrayOfInt.length;
    for (int i = 0; i < j; i++)
    {
      int k = paramArrayOfInt[i];
      if (!localLinkedHashSet.add(Integer.valueOf(k))) {
        return Integer.valueOf(k);
      }
    }
    return null;
  }
  
  private String findParentFqcn(TypeElement paramTypeElement, Set<String> paramSet)
  {
    TypeElement localTypeElement;
    do
    {
      paramTypeElement = paramTypeElement.getSuperclass();
      if (paramTypeElement.getKind() == TypeKind.NONE) {
        return null;
      }
      localTypeElement = (TypeElement)((DeclaredType)paramTypeElement).asElement();
      paramTypeElement = localTypeElement;
    } while (!paramSet.contains(localTypeElement.toString()));
    paramSet = getPackageName(localTypeElement);
    paramTypeElement = new StringBuilder();
    paramTypeElement.append(paramSet);
    paramTypeElement.append(".");
    paramTypeElement.append(getClassName(localTypeElement, paramSet));
    return paramTypeElement.toString();
  }
  
  private static String getClassName(TypeElement paramTypeElement, String paramString)
  {
    int i = paramString.length();
    return paramTypeElement.getQualifiedName().toString().substring(i + 1).replace('.', '$');
  }
  
  private BindingClass getOrCreateTargetClass(Map<TypeElement, BindingClass> paramMap, TypeElement paramTypeElement)
  {
    Object localObject2 = (BindingClass)paramMap.get(paramTypeElement);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = paramTypeElement.getQualifiedName().toString();
      localObject2 = getPackageName(paramTypeElement);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(getClassName(paramTypeElement, (String)localObject2));
      localStringBuilder.append("$$ViewBinder");
      localObject1 = new BindingClass((String)localObject2, localStringBuilder.toString(), (String)localObject1);
      paramMap.put(paramTypeElement, localObject1);
    }
    return (BindingClass)localObject1;
  }
  
  private String getPackageName(TypeElement paramTypeElement)
  {
    return this.elementUtils.getPackageOf(paramTypeElement).getQualifiedName().toString();
  }
  
  private static boolean hasAnnotationWithName(Element paramElement, String paramString)
  {
    paramElement = paramElement.getAnnotationMirrors().iterator();
    while (paramElement.hasNext()) {
      if (paramString.equals(((AnnotationMirror)paramElement.next()).getAnnotationType().asElement().getSimpleName().toString())) {
        return true;
      }
    }
    return false;
  }
  
  private boolean isBindingInWrongPackage(Class<? extends Annotation> paramClass, Element paramElement)
  {
    String str = ((TypeElement)paramElement.getEnclosingElement()).getQualifiedName().toString();
    if (str.startsWith("android."))
    {
      error(paramElement, "@%s-annotated class incorrectly in Android framework package. (%s)", new Object[] { paramClass.getSimpleName(), str });
      return true;
    }
    if (str.startsWith("java."))
    {
      error(paramElement, "@%s-annotated class incorrectly in Java framework package. (%s)", new Object[] { paramClass.getSimpleName(), str });
      return true;
    }
    return false;
  }
  
  private boolean isInaccessibleViaGeneratedCode(Class<? extends Annotation> paramClass, String paramString, Element paramElement)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Set localSet = paramElement.getModifiers();
    boolean bool;
    if ((!localSet.contains(Modifier.PRIVATE)) && (!localSet.contains(Modifier.STATIC)))
    {
      bool = false;
    }
    else
    {
      error(paramElement, "@%s %s must not be private or static. (%s.%s)", new Object[] { paramClass.getSimpleName(), paramString, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      bool = true;
    }
    if (localTypeElement.getKind() != ElementKind.CLASS)
    {
      error(localTypeElement, "@%s %s may only be contained in classes. (%s.%s)", new Object[] { paramClass.getSimpleName(), paramString, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      bool = true;
    }
    if (localTypeElement.getModifiers().contains(Modifier.PRIVATE))
    {
      error(localTypeElement, "@%s %s may not be contained in private classes. (%s.%s)", new Object[] { paramClass.getSimpleName(), paramString, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      bool = true;
    }
    return bool;
  }
  
  private boolean isInterface(TypeMirror paramTypeMirror)
  {
    boolean bool2 = paramTypeMirror instanceof DeclaredType;
    boolean bool1 = false;
    if (!bool2) {
      return false;
    }
    if (((DeclaredType)paramTypeMirror).asElement().getKind() == ElementKind.INTERFACE) {
      bool1 = true;
    }
    return bool1;
  }
  
  private static boolean isRequiredBinding(Element paramElement)
  {
    return hasAnnotationWithName(paramElement, "Nullable") ^ true;
  }
  
  private boolean isSubtypeOfType(TypeMirror paramTypeMirror, String paramString)
  {
    if (paramString.equals(paramTypeMirror.toString())) {
      return true;
    }
    if (paramTypeMirror.getKind() != TypeKind.DECLARED) {
      return false;
    }
    paramTypeMirror = (DeclaredType)paramTypeMirror;
    List localList = paramTypeMirror.getTypeArguments();
    if (localList.size() > 0)
    {
      StringBuilder localStringBuilder = new StringBuilder(paramTypeMirror.asElement().toString());
      localStringBuilder.append('<');
      for (int i = 0; i < localList.size(); i++)
      {
        if (i > 0) {
          localStringBuilder.append(',');
        }
        localStringBuilder.append('?');
      }
      localStringBuilder.append('>');
      if (localStringBuilder.toString().equals(paramString)) {
        return true;
      }
    }
    paramTypeMirror = paramTypeMirror.asElement();
    if (!(paramTypeMirror instanceof TypeElement)) {
      return false;
    }
    paramTypeMirror = (TypeElement)paramTypeMirror;
    if (isSubtypeOfType(paramTypeMirror.getSuperclass(), paramString)) {
      return true;
    }
    paramTypeMirror = paramTypeMirror.getInterfaces().iterator();
    while (paramTypeMirror.hasNext()) {
      if (isSubtypeOfType((TypeMirror)paramTypeMirror.next(), paramString)) {
        return true;
      }
    }
    return false;
  }
  
  private void logParsingError(Element paramElement, Class<? extends Annotation> paramClass, Exception paramException)
  {
    StringWriter localStringWriter = new StringWriter();
    paramException.printStackTrace(new PrintWriter(localStringWriter));
    error(paramElement, "Unable to parse @%s binding.\n\n%s", new Object[] { paramClass.getSimpleName(), localStringWriter });
  }
  
  private void parseBind(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    if ((!isInaccessibleViaGeneratedCode(Bind.class, "fields", paramElement)) && (!isBindingInWrongPackage(Bind.class, paramElement)))
    {
      TypeMirror localTypeMirror = paramElement.asType();
      if (localTypeMirror.getKind() == TypeKind.ARRAY) {
        parseBindMany(paramElement, paramMap, paramSet);
      } else if (LIST_TYPE.equals(doubleErasure(localTypeMirror))) {
        parseBindMany(paramElement, paramMap, paramSet);
      } else if (isSubtypeOfType(localTypeMirror, "java.lang.Iterable<?>")) {
        error(paramElement, "@%s must be a List or array. (%s.%s)", new Object[] { Bind.class.getSimpleName(), ((TypeElement)paramElement.getEnclosingElement()).getQualifiedName(), paramElement.getSimpleName() });
      } else {
        parseBindOne(paramElement, paramMap, paramSet);
      }
      return;
    }
  }
  
  private void parseBindMany(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Object localObject2 = paramElement.asType();
    Object localObject1 = doubleErasure((TypeMirror)localObject2);
    int i;
    if (((TypeMirror)localObject2).getKind() == TypeKind.ARRAY)
    {
      localObject1 = ((ArrayType)localObject2).getComponentType();
      localObject2 = FieldCollectionViewBinding.Kind.ARRAY;
      i = 0;
    }
    else
    {
      if (!LIST_TYPE.equals(localObject1)) {
        break label475;
      }
      localObject1 = ((DeclaredType)localObject2).getTypeArguments();
      if (((List)localObject1).size() != 1)
      {
        error(paramElement, "@%s List must have a generic component. (%s.%s)", new Object[] { Bind.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
        localObject1 = null;
        i = 1;
      }
      else
      {
        localObject1 = (TypeMirror)((List)localObject1).get(0);
        i = 0;
      }
      localObject2 = FieldCollectionViewBinding.Kind.LIST;
    }
    Object localObject3 = localObject1;
    if (localObject1 != null)
    {
      localObject3 = localObject1;
      if (((TypeMirror)localObject1).getKind() == TypeKind.TYPEVAR) {
        localObject3 = ((TypeVariable)localObject1).getUpperBound();
      }
    }
    int j = i;
    if (localObject3 != null)
    {
      j = i;
      if (!isSubtypeOfType((TypeMirror)localObject3, "android.view.View"))
      {
        j = i;
        if (!isInterface((TypeMirror)localObject3))
        {
          error(paramElement, "@%s List or array type must extend from View or be an interface. (%s.%s)", new Object[] { Bind.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
          j = 1;
        }
      }
    }
    if (j != 0) {
      return;
    }
    localObject1 = paramElement.getSimpleName().toString();
    int[] arrayOfInt = ((Bind)paramElement.getAnnotation(Bind.class)).value();
    if (arrayOfInt.length == 0)
    {
      error(paramElement, "@%s must specify at least one ID. (%s.%s)", new Object[] { Bind.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      return;
    }
    Integer localInteger = findDuplicate(arrayOfInt);
    if (localInteger != null) {
      error(paramElement, "@%s annotation contains duplicate ID %d. (%s.%s)", new Object[] { Bind.class.getSimpleName(), localInteger, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
    }
    localObject3 = ((TypeMirror)localObject3).toString();
    boolean bool = isRequiredBinding(paramElement);
    getOrCreateTargetClass(paramMap, localTypeElement).addFieldCollection(arrayOfInt, new FieldCollectionViewBinding((String)localObject1, (String)localObject3, (FieldCollectionViewBinding.Kind)localObject2, bool));
    paramSet.add(localTypeElement.toString());
    return;
    label475:
    throw new AssertionError();
  }
  
  private void parseBindOne(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Object localObject2 = paramElement.asType();
    Object localObject1 = localObject2;
    if (((TypeMirror)localObject2).getKind() == TypeKind.TYPEVAR) {
      localObject1 = ((TypeVariable)localObject2).getUpperBound();
    }
    if ((!isSubtypeOfType((TypeMirror)localObject1, "android.view.View")) && (!isInterface((TypeMirror)localObject1)))
    {
      error(paramElement, "@%s fields must extend from View or be an interface. (%s.%s)", new Object[] { Bind.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      i = 1;
    }
    else
    {
      i = 0;
    }
    localObject2 = ((Bind)paramElement.getAnnotation(Bind.class)).value();
    if (localObject2.length != 1)
    {
      error(paramElement, "@%s for a view must only specify one ID. Found: %s. (%s.%s)", new Object[] { Bind.class.getSimpleName(), Arrays.toString((int[])localObject2), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
      i = 1;
    }
    if (i != 0) {
      return;
    }
    int i = localObject2[0];
    localObject2 = (BindingClass)paramMap.get(localTypeElement);
    if (localObject2 != null)
    {
      Object localObject3 = ((BindingClass)localObject2).getViewBinding(i);
      paramMap = (Map<TypeElement, BindingClass>)localObject2;
      if (localObject3 != null)
      {
        localObject3 = ((ViewBindings)localObject3).getFieldBindings().iterator();
        paramMap = (Map<TypeElement, BindingClass>)localObject2;
        if (((Iterator)localObject3).hasNext())
        {
          paramMap = (FieldViewBinding)((Iterator)localObject3).next();
          error(paramElement, "Attempt to use @%s for an already bound ID %d on '%s'. (%s.%s)", new Object[] { Bind.class.getSimpleName(), Integer.valueOf(i), paramMap.getName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
        }
      }
    }
    else
    {
      paramMap = getOrCreateTargetClass(paramMap, localTypeElement);
    }
    paramMap.addField(i, new FieldViewBinding(paramElement.getSimpleName().toString(), ((TypeMirror)localObject1).toString(), isRequiredBinding(paramElement)));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseListenerAnnotation(Class<? extends Annotation> paramClass, Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
    throws Exception
  {
    if (((paramElement instanceof ExecutableElement)) && (paramElement.getKind() == ElementKind.METHOD))
    {
      ExecutableElement localExecutableElement = (ExecutableElement)paramElement;
      TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
      Object localObject1 = paramElement.getAnnotation(paramClass);
      Object localObject2 = paramClass.getDeclaredMethod("value", new Class[0]);
      if (((Method)localObject2).getReturnType() == int[].class)
      {
        int[] arrayOfInt = (int[])((Method)localObject2).invoke(localObject1, new Object[0]);
        String str = localExecutableElement.getSimpleName().toString();
        boolean bool = isRequiredBinding(paramElement);
        int i = isInaccessibleViaGeneratedCode(paramClass, "methods", paramElement) | isBindingInWrongPackage(paramClass, paramElement);
        localObject2 = findDuplicate(arrayOfInt);
        if (localObject2 != null)
        {
          error(paramElement, "@%s annotation for method contains duplicate ID %d. (%s.%s)", new Object[] { paramClass.getSimpleName(), localObject2, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
          i = 1;
        }
        ListenerClass localListenerClass = (ListenerClass)paramClass.getAnnotation(ListenerClass.class);
        if (localListenerClass != null)
        {
          int i1 = arrayOfInt.length;
          int n = 0;
          int m;
          while (n < i1)
          {
            int i2 = arrayOfInt[n];
            int k = i;
            if (i2 == -1) {
              if (arrayOfInt.length == 1)
              {
                if (!bool)
                {
                  error(paramElement, "ID-free binding must not be annotated with @Nullable. (%s.%s)", new Object[] { localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
                  i = 1;
                }
                localObject2 = localListenerClass.targetType();
                if ((!isSubtypeOfType(localTypeElement.asType(), (String)localObject2)) && (!isInterface(localTypeElement.asType())))
                {
                  error(paramElement, "@%s annotation without an ID may only be used with an object of type \"%s\" or an interface. (%s.%s)", new Object[] { paramClass.getSimpleName(), localObject2, localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
                  i = 1;
                }
                k = i;
              }
              else
              {
                error(paramElement, "@%s annotation contains invalid ID %d. (%s.%s)", new Object[] { paramClass.getSimpleName(), Integer.valueOf(i2), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
                m = 1;
              }
            }
            n++;
            i = m;
          }
          localObject2 = localListenerClass.method();
          if (localObject2.length <= 1)
          {
            if (localObject2.length == 1)
            {
              if (localListenerClass.callbacks() == ListenerClass.NONE.class) {
                localObject1 = localObject2[0];
              } else {
                throw new IllegalStateException(String.format("Both method() and callback() defined on @%s.", new Object[] { paramClass.getSimpleName() }));
              }
            }
            else
            {
              localObject2 = (Enum)paramClass.getDeclaredMethod("callback", new Class[0]).invoke(localObject1, new Object[0]);
              localObject1 = (ListenerMethod)((Enum)localObject2).getDeclaringClass().getField(((Enum)localObject2).name()).getAnnotation(ListenerMethod.class);
              if (localObject1 == null) {
                break label1385;
              }
            }
            List localList = localExecutableElement.getParameters();
            if (localList.size() > ((ListenerMethod)localObject1).parameters().length)
            {
              error(paramElement, "@%s methods can have at most %s parameter(s). (%s.%s)", new Object[] { paramClass.getSimpleName(), Integer.valueOf(((ListenerMethod)localObject1).parameters().length), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
              i = 1;
            }
            Object localObject3 = localExecutableElement.getReturnType();
            localObject2 = localObject3;
            if ((localObject3 instanceof TypeVariable)) {
              localObject2 = ((TypeVariable)localObject3).getUpperBound();
            }
            if (!((TypeMirror)localObject2).toString().equals(((ListenerMethod)localObject1).returnType()))
            {
              error(paramElement, "@%s methods must have a '%s' return type. (%s.%s)", new Object[] { paramClass.getSimpleName(), ((ListenerMethod)localObject1).returnType(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
              i = 1;
            }
            if (i != 0) {
              return;
            }
            localObject2 = Parameter.NONE;
            if (!localList.isEmpty())
            {
              localObject3 = new Parameter[localList.size()];
              BitSet localBitSet = new BitSet(localList.size());
              String[] arrayOfString = ((ListenerMethod)localObject1).parameters();
              for (i = 0;; j++)
              {
                localObject2 = localObject3;
                if (i >= localList.size()) {
                  break;
                }
                TypeMirror localTypeMirror = ((VariableElement)localList.get(i)).asType();
                localObject2 = localTypeMirror;
                if ((localTypeMirror instanceof TypeVariable)) {
                  localObject2 = ((TypeVariable)localTypeMirror).getUpperBound();
                }
                m = 0;
                while (m < arrayOfString.length) {
                  if ((localBitSet.get(m)) || ((!isSubtypeOfType((TypeMirror)localObject2, arrayOfString[m])) && (!isInterface((TypeMirror)localObject2))))
                  {
                    m++;
                  }
                  else
                  {
                    localObject3[i] = new Parameter(m, ((TypeMirror)localObject2).toString());
                    localBitSet.set(m);
                  }
                }
                if (localObject3[i] == null)
                {
                  paramMap = new StringBuilder();
                  paramMap.append("Unable to match @");
                  paramMap.append(paramClass.getSimpleName());
                  paramMap.append(" method arguments. (");
                  paramMap.append(localTypeElement.getQualifiedName());
                  paramMap.append('.');
                  paramMap.append(paramElement.getSimpleName());
                  paramMap.append(')');
                  for (j = 0; j < localObject3.length; j = m)
                  {
                    paramClass = localObject3[j];
                    paramMap.append("\n\n  Parameter #");
                    m = j + 1;
                    paramMap.append(m);
                    paramMap.append(": ");
                    paramMap.append(((VariableElement)localList.get(j)).asType().toString());
                    paramMap.append("\n    ");
                    if (paramClass == null)
                    {
                      paramMap.append("did not match any listener parameters");
                    }
                    else
                    {
                      paramMap.append("matched listener parameter #");
                      paramMap.append(paramClass.getListenerPosition() + 1);
                      paramMap.append(": ");
                      paramMap.append(paramClass.getType());
                    }
                  }
                  paramMap.append("\n\nMethods may have up to ");
                  paramMap.append(((ListenerMethod)localObject1).parameters().length);
                  paramMap.append(" parameter(s):\n");
                  for (paramElement : ((ListenerMethod)localObject1).parameters())
                  {
                    paramMap.append("\n  ");
                    paramMap.append(paramElement);
                  }
                  paramMap.append("\n\nThese may be listed in any order but will be searched for from top to bottom.");
                  error(localExecutableElement, paramMap.toString(), new Object[0]);
                  return;
                }
              }
            }
            paramClass = new MethodViewBinding(str, Arrays.asList((Object[])localObject2), bool);
            paramMap = getOrCreateTargetClass(paramMap, localTypeElement);
            m = arrayOfInt.length;
            for (int j = 0; j < m; j++)
            {
              n = arrayOfInt[j];
              if (!paramMap.addMethod(n, localListenerClass, (ListenerMethod)localObject1, paramClass))
              {
                error(paramElement, "Multiple listener methods with return value specified for ID %d. (%s.%s)", new Object[] { Integer.valueOf(n), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
                return;
              }
            }
            paramSet.add(localTypeElement.toString());
            return;
            label1385:
            throw new IllegalStateException(String.format("No @%s defined on @%s's %s.%s.", new Object[] { ListenerMethod.class.getSimpleName(), paramClass.getSimpleName(), ((Enum)localObject2).getDeclaringClass().getSimpleName(), ((Enum)localObject2).name() }));
          }
          throw new IllegalStateException(String.format("Multiple listener methods specified on @%s.", new Object[] { paramClass.getSimpleName() }));
        }
        throw new IllegalStateException(String.format("No @%s defined on @%s.", new Object[] { ListenerClass.class.getSimpleName(), paramClass.getSimpleName() }));
      }
      throw new IllegalStateException(String.format("@%s annotation value() type not int[].", new Object[] { paramClass }));
    }
    throw new IllegalStateException(String.format("@%s annotation must be on a method.", new Object[] { paramClass.getSimpleName() }));
  }
  
  private void parseResourceBool(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Object localObject = paramElement.asType().getKind();
    TypeKind localTypeKind = TypeKind.BOOLEAN;
    int i = 1;
    if (localObject != localTypeKind) {
      error(paramElement, "@%s field type must be 'boolean'. (%s.%s)", new Object[] { BindBool.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
    } else {
      i = 0;
    }
    if ((isInaccessibleViaGeneratedCode(BindBool.class, "fields", paramElement) | i | isBindingInWrongPackage(BindBool.class, paramElement))) {
      return;
    }
    localObject = paramElement.getSimpleName().toString();
    i = ((BindBool)paramElement.getAnnotation(BindBool.class)).value();
    getOrCreateTargetClass(paramMap, localTypeElement).addResource(new FieldResourceBinding(i, (String)localObject, "getBoolean"));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseResourceColor(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Object localObject = paramElement.asType();
    boolean bool = "android.content.res.ColorStateList".equals(((TypeMirror)localObject).toString());
    int i = 1;
    int j = 0;
    if (!bool) {
      if (((TypeMirror)localObject).getKind() != TypeKind.INT)
      {
        error(paramElement, "@%s field type must be 'int' or 'ColorStateList'. (%s.%s)", new Object[] { BindColor.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
        i = 0;
        j = 1;
      }
      else
      {
        i = 0;
      }
    }
    if ((isInaccessibleViaGeneratedCode(BindColor.class, "fields", paramElement) | j | isBindingInWrongPackage(BindColor.class, paramElement))) {
      return;
    }
    localObject = paramElement.getSimpleName().toString();
    j = ((BindColor)paramElement.getAnnotation(BindColor.class)).value();
    paramMap = getOrCreateTargetClass(paramMap, localTypeElement);
    if (i != 0) {
      paramElement = "getColorStateList";
    } else {
      paramElement = "getColor";
    }
    paramMap.addResource(new FieldResourceBinding(j, (String)localObject, paramElement));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseResourceDimen(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    Object localObject = paramElement.asType();
    TypeKind localTypeKind1 = ((TypeMirror)localObject).getKind();
    TypeKind localTypeKind2 = TypeKind.INT;
    int i = 1;
    int j = 0;
    if (localTypeKind1 != localTypeKind2) {
      if (((TypeMirror)localObject).getKind() != TypeKind.FLOAT)
      {
        error(paramElement, "@%s field type must be 'int' or 'float'. (%s.%s)", new Object[] { BindDimen.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
        i = 0;
        j = 1;
      }
      else
      {
        i = 0;
      }
    }
    if ((isInaccessibleViaGeneratedCode(BindDimen.class, "fields", paramElement) | j | isBindingInWrongPackage(BindDimen.class, paramElement))) {
      return;
    }
    localObject = paramElement.getSimpleName().toString();
    j = ((BindDimen)paramElement.getAnnotation(BindDimen.class)).value();
    paramMap = getOrCreateTargetClass(paramMap, localTypeElement);
    if (i != 0) {
      paramElement = "getDimensionPixelSize";
    } else {
      paramElement = "getDimension";
    }
    paramMap.addResource(new FieldResourceBinding(j, (String)localObject, paramElement));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseResourceDrawable(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    boolean bool = "android.graphics.drawable.Drawable".equals(paramElement.asType().toString());
    int i = 1;
    if (!bool) {
      error(paramElement, "@%s field type must be 'Drawable'. (%s.%s)", new Object[] { BindDrawable.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
    } else {
      i = 0;
    }
    if ((isInaccessibleViaGeneratedCode(BindDrawable.class, "fields", paramElement) | i | isBindingInWrongPackage(BindDrawable.class, paramElement))) {
      return;
    }
    String str = paramElement.getSimpleName().toString();
    i = ((BindDrawable)paramElement.getAnnotation(BindDrawable.class)).value();
    getOrCreateTargetClass(paramMap, localTypeElement).addResource(new FieldResourceBinding(i, str, "getDrawable"));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseResourceInt(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    TypeKind localTypeKind = paramElement.asType().getKind();
    Object localObject = TypeKind.INT;
    int i = 1;
    if (localTypeKind != localObject) {
      error(paramElement, "@%s field type must be 'int'. (%s.%s)", new Object[] { BindInt.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
    } else {
      i = 0;
    }
    if ((isInaccessibleViaGeneratedCode(BindInt.class, "fields", paramElement) | i | isBindingInWrongPackage(BindInt.class, paramElement))) {
      return;
    }
    localObject = paramElement.getSimpleName().toString();
    i = ((BindInt)paramElement.getAnnotation(BindInt.class)).value();
    getOrCreateTargetClass(paramMap, localTypeElement).addResource(new FieldResourceBinding(i, (String)localObject, "getInteger"));
    paramSet.add(localTypeElement.toString());
  }
  
  private void parseResourceString(Element paramElement, Map<TypeElement, BindingClass> paramMap, Set<String> paramSet)
  {
    TypeElement localTypeElement = (TypeElement)paramElement.getEnclosingElement();
    boolean bool = "java.lang.String".equals(paramElement.asType().toString());
    int i = 1;
    if (!bool) {
      error(paramElement, "@%s field type must be 'String'. (%s.%s)", new Object[] { BindString.class.getSimpleName(), localTypeElement.getQualifiedName(), paramElement.getSimpleName() });
    } else {
      i = 0;
    }
    if ((isInaccessibleViaGeneratedCode(BindString.class, "fields", paramElement) | i | isBindingInWrongPackage(BindString.class, paramElement))) {
      return;
    }
    String str = paramElement.getSimpleName().toString();
    i = ((BindString)paramElement.getAnnotation(BindString.class)).value();
    getOrCreateTargetClass(paramMap, localTypeElement).addResource(new FieldResourceBinding(i, str, "getString"));
    paramSet.add(localTypeElement.toString());
  }
  
  public Set<String> getSupportedAnnotationTypes()
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    localLinkedHashSet.add(Bind.class.getCanonicalName());
    Iterator localIterator = LISTENERS.iterator();
    while (localIterator.hasNext()) {
      localLinkedHashSet.add(((Class)localIterator.next()).getCanonicalName());
    }
    localLinkedHashSet.add(BindBool.class.getCanonicalName());
    localLinkedHashSet.add(BindColor.class.getCanonicalName());
    localLinkedHashSet.add(BindDimen.class.getCanonicalName());
    localLinkedHashSet.add(BindDrawable.class.getCanonicalName());
    localLinkedHashSet.add(BindInt.class.getCanonicalName());
    localLinkedHashSet.add(BindString.class.getCanonicalName());
    return localLinkedHashSet;
  }
  
  public SourceVersion getSupportedSourceVersion()
  {
    return SourceVersion.latestSupported();
  }
  
  public void init(ProcessingEnvironment paramProcessingEnvironment)
  {
    try
    {
      super.init(paramProcessingEnvironment);
      this.elementUtils = paramProcessingEnvironment.getElementUtils();
      this.typeUtils = paramProcessingEnvironment.getTypeUtils();
      this.filer = paramProcessingEnvironment.getFiler();
      return;
    }
    finally
    {
      paramProcessingEnvironment = finally;
      throw paramProcessingEnvironment;
    }
  }
  
  public boolean process(Set<? extends TypeElement> paramSet, RoundEnvironment paramRoundEnvironment)
  {
    paramSet = findAndParseTargets(paramRoundEnvironment).entrySet().iterator();
    while (paramSet.hasNext())
    {
      Object localObject = (Map.Entry)paramSet.next();
      paramRoundEnvironment = (TypeElement)((Map.Entry)localObject).getKey();
      BindingClass localBindingClass = (BindingClass)((Map.Entry)localObject).getValue();
      try
      {
        localObject = this.filer.createSourceFile(localBindingClass.getFqcn(), new Element[] { paramRoundEnvironment }).openWriter();
        ((Writer)localObject).write(localBindingClass.brewJava());
        ((Writer)localObject).flush();
        ((Writer)localObject).close();
      }
      catch (IOException localIOException)
      {
        error(paramRoundEnvironment, "Unable to write view binder for type %s: %s", new Object[] { paramRoundEnvironment, localIOException.getMessage() });
      }
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/ButterKnifeProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */