package butterknife.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class MethodViewBinding
  implements ViewBinding
{
  private final String name;
  private final List<Parameter> parameters;
  private final boolean required;
  
  MethodViewBinding(String paramString, List<Parameter> paramList, boolean paramBoolean)
  {
    this.name = paramString;
    this.parameters = Collections.unmodifiableList(new ArrayList(paramList));
    this.required = paramBoolean;
  }
  
  public String getDescription()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("method '");
    localStringBuilder.append(this.name);
    localStringBuilder.append("'");
    return localStringBuilder.toString();
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public List<Parameter> getParameters()
  {
    return this.parameters;
  }
  
  public boolean isRequired()
  {
    return this.required;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/MethodViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */