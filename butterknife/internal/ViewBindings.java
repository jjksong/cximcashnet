package butterknife.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class ViewBindings
{
  private final Set<FieldViewBinding> fieldBindings = new LinkedHashSet();
  private final int id;
  private final LinkedHashMap<ListenerClass, Map<ListenerMethod, Set<MethodViewBinding>>> methodBindings = new LinkedHashMap();
  
  ViewBindings(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void addFieldBinding(FieldViewBinding paramFieldViewBinding)
  {
    this.fieldBindings.add(paramFieldViewBinding);
  }
  
  public void addMethodBinding(ListenerClass paramListenerClass, ListenerMethod paramListenerMethod, MethodViewBinding paramMethodViewBinding)
  {
    Object localObject1 = (Map)this.methodBindings.get(paramListenerClass);
    if (localObject1 == null)
    {
      localObject1 = new LinkedHashMap();
      this.methodBindings.put(paramListenerClass, localObject1);
      paramListenerClass = null;
    }
    else
    {
      paramListenerClass = (Set)((Map)localObject1).get(paramListenerMethod);
    }
    Object localObject2 = paramListenerClass;
    if (paramListenerClass == null)
    {
      localObject2 = new LinkedHashSet();
      ((Map)localObject1).put(paramListenerMethod, localObject2);
    }
    ((Set)localObject2).add(paramMethodViewBinding);
  }
  
  public Collection<FieldViewBinding> getFieldBindings()
  {
    return this.fieldBindings;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public Map<ListenerClass, Map<ListenerMethod, Set<MethodViewBinding>>> getMethodBindings()
  {
    return this.methodBindings;
  }
  
  public List<ViewBinding> getRequiredBindings()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = this.fieldBindings.iterator();
    while (localIterator1.hasNext())
    {
      localObject = (FieldViewBinding)localIterator1.next();
      if (((FieldViewBinding)localObject).isRequired()) {
        localArrayList.add(localObject);
      }
    }
    Object localObject = this.methodBindings.values().iterator();
    if (((Iterator)localObject).hasNext())
    {
      Iterator localIterator2 = ((Map)((Iterator)localObject).next()).values().iterator();
      while (localIterator2.hasNext())
      {
        localIterator1 = ((Set)localIterator2.next()).iterator();
        while (localIterator1.hasNext())
        {
          MethodViewBinding localMethodViewBinding = (MethodViewBinding)localIterator1.next();
          if (localMethodViewBinding.isRequired()) {
            localArrayList.add(localMethodViewBinding);
          }
        }
      }
    }
    return localArrayList;
  }
  
  public boolean hasMethodBinding(ListenerClass paramListenerClass, ListenerMethod paramListenerMethod)
  {
    paramListenerClass = (Map)this.methodBindings.get(paramListenerClass);
    boolean bool;
    if ((paramListenerClass != null) && (paramListenerClass.containsKey(paramListenerMethod))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/ViewBindings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */