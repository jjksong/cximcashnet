package butterknife.internal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class BindingClass
{
  private final String className;
  private final String classPackage;
  private final Map<FieldCollectionViewBinding, int[]> collectionBindings = new LinkedHashMap();
  private String parentViewBinder;
  private final List<FieldResourceBinding> resourceBindings = new ArrayList();
  private final String targetClass;
  private final Map<Integer, ViewBindings> viewIdMap = new LinkedHashMap();
  
  BindingClass(String paramString1, String paramString2, String paramString3)
  {
    this.classPackage = paramString1;
    this.className = paramString2;
    this.targetClass = paramString3;
  }
  
  private void emitBindMethod(StringBuilder paramStringBuilder)
  {
    paramStringBuilder.append("  @Override ");
    paramStringBuilder.append("public void bind(final Finder finder, final T target, Object source) {\n");
    if (this.parentViewBinder != null) {
      paramStringBuilder.append("    super.bind(finder, target, source);\n\n");
    }
    Iterator localIterator;
    Object localObject;
    if ((!this.viewIdMap.isEmpty()) || (!this.collectionBindings.isEmpty()))
    {
      paramStringBuilder.append("    View view;\n");
      localIterator = this.viewIdMap.values().iterator();
      while (localIterator.hasNext()) {
        emitViewBindings(paramStringBuilder, (ViewBindings)localIterator.next());
      }
      localIterator = this.collectionBindings.entrySet().iterator();
      while (localIterator.hasNext())
      {
        localObject = (Map.Entry)localIterator.next();
        emitCollectionBinding(paramStringBuilder, (FieldCollectionViewBinding)((Map.Entry)localObject).getKey(), (int[])((Map.Entry)localObject).getValue());
      }
    }
    if (!this.resourceBindings.isEmpty())
    {
      paramStringBuilder.append("    Resources res = finder.getContext(source).getResources();\n");
      localIterator = this.resourceBindings.iterator();
      while (localIterator.hasNext())
      {
        localObject = (FieldResourceBinding)localIterator.next();
        paramStringBuilder.append("    target.");
        paramStringBuilder.append(((FieldResourceBinding)localObject).getName());
        paramStringBuilder.append(" = res.");
        paramStringBuilder.append(((FieldResourceBinding)localObject).getMethod());
        paramStringBuilder.append('(');
        paramStringBuilder.append(((FieldResourceBinding)localObject).getId());
        paramStringBuilder.append(");\n");
      }
    }
    paramStringBuilder.append("  }\n");
  }
  
  private void emitCollectionBinding(StringBuilder paramStringBuilder, FieldCollectionViewBinding paramFieldCollectionViewBinding, int[] paramArrayOfInt)
  {
    paramStringBuilder.append("    target.");
    paramStringBuilder.append(paramFieldCollectionViewBinding.getName());
    paramStringBuilder.append(" = ");
    switch (paramFieldCollectionViewBinding.getKind())
    {
    default: 
      paramStringBuilder = new StringBuilder();
      paramStringBuilder.append("Unknown kind: ");
      paramStringBuilder.append(paramFieldCollectionViewBinding.getKind());
      throw new IllegalStateException(paramStringBuilder.toString());
    case ???: 
      paramStringBuilder.append("Finder.listOf(");
      break;
    case ???: 
      paramStringBuilder.append("Finder.arrayOf(");
    }
    for (int i = 0; i < paramArrayOfInt.length; i++)
    {
      if (i > 0) {
        paramStringBuilder.append(',');
      }
      paramStringBuilder.append("\n        finder.<");
      paramStringBuilder.append(paramFieldCollectionViewBinding.getType());
      paramStringBuilder.append(">");
      String str;
      if (paramFieldCollectionViewBinding.isRequired()) {
        str = "findRequiredView";
      } else {
        str = "findOptionalView";
      }
      paramStringBuilder.append(str);
      paramStringBuilder.append("(source, ");
      paramStringBuilder.append(paramArrayOfInt[i]);
      paramStringBuilder.append(", \"");
      emitHumanDescription(paramStringBuilder, Collections.singleton(paramFieldCollectionViewBinding));
      paramStringBuilder.append("\")");
    }
    paramStringBuilder.append("\n    );\n");
  }
  
  private void emitFieldBindings(StringBuilder paramStringBuilder, ViewBindings paramViewBindings)
  {
    Collection localCollection = paramViewBindings.getFieldBindings();
    if (localCollection.isEmpty()) {
      return;
    }
    Iterator localIterator = localCollection.iterator();
    while (localIterator.hasNext())
    {
      FieldViewBinding localFieldViewBinding = (FieldViewBinding)localIterator.next();
      paramStringBuilder.append("    target.");
      paramStringBuilder.append(localFieldViewBinding.getName());
      paramStringBuilder.append(" = ");
      if (localFieldViewBinding.requiresCast())
      {
        paramStringBuilder.append("finder.castView(view");
        paramStringBuilder.append(", ");
        paramStringBuilder.append(paramViewBindings.getId());
        paramStringBuilder.append(", \"");
        emitHumanDescription(paramStringBuilder, localCollection);
        paramStringBuilder.append("\");\n");
      }
      else
      {
        paramStringBuilder.append("view;\n");
      }
    }
  }
  
  static void emitHumanDescription(StringBuilder paramStringBuilder, Collection<? extends ViewBinding> paramCollection)
  {
    Iterator localIterator = paramCollection.iterator();
    int i;
    int j;
    switch (paramCollection.size())
    {
    default: 
      i = 0;
      j = paramCollection.size();
      break;
    case 2: 
      paramStringBuilder.append(((ViewBinding)localIterator.next()).getDescription());
      paramStringBuilder.append(" and ");
      paramStringBuilder.append(((ViewBinding)localIterator.next()).getDescription());
      break;
    case 1: 
      paramStringBuilder.append(((ViewBinding)localIterator.next()).getDescription());
      break;
    }
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(", ");
      }
      if (i == j - 1) {
        paramStringBuilder.append("and ");
      }
      paramStringBuilder.append(((ViewBinding)localIterator.next()).getDescription());
      i++;
    }
  }
  
  private void emitMethodBindings(StringBuilder paramStringBuilder, ViewBindings paramViewBindings)
  {
    Object localObject2 = paramViewBindings.getMethodBindings();
    if (((Map)localObject2).isEmpty()) {
      return;
    }
    Object localObject1 = "";
    boolean bool2 = paramViewBindings.getRequiredBindings().isEmpty();
    paramViewBindings = (ViewBindings)localObject1;
    if (bool2)
    {
      paramStringBuilder.append("    if (view != null) {\n");
      paramViewBindings = "  ";
    }
    localObject1 = ((Map)localObject2).entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      Object localObject3 = (ListenerClass)((Map.Entry)localObject2).getKey();
      localObject2 = (Map)((Map.Entry)localObject2).getValue();
      boolean bool1 = "android.view.View".equals(((ListenerClass)localObject3).targetType()) ^ true;
      paramStringBuilder.append(paramViewBindings);
      paramStringBuilder.append("    ");
      int i;
      if (bool1)
      {
        paramStringBuilder.append("((");
        paramStringBuilder.append(((ListenerClass)localObject3).targetType());
        if (((ListenerClass)localObject3).genericArguments() > 0)
        {
          paramStringBuilder.append('<');
          for (i = 0; i < ((ListenerClass)localObject3).genericArguments(); i++)
          {
            if (i > 0) {
              paramStringBuilder.append(", ");
            }
            paramStringBuilder.append('?');
          }
          paramStringBuilder.append('>');
        }
        paramStringBuilder.append(") ");
      }
      paramStringBuilder.append("view");
      if (bool1) {
        paramStringBuilder.append(')');
      }
      paramStringBuilder.append('.');
      paramStringBuilder.append(((ListenerClass)localObject3).setter());
      paramStringBuilder.append("(\n");
      paramStringBuilder.append(paramViewBindings);
      paramStringBuilder.append("      new ");
      paramStringBuilder.append(((ListenerClass)localObject3).type());
      paramStringBuilder.append("() {\n");
      localObject3 = getListenerMethods((ListenerClass)localObject3).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        ListenerMethod localListenerMethod = (ListenerMethod)((Iterator)localObject3).next();
        paramStringBuilder.append(paramViewBindings);
        paramStringBuilder.append("        @Override public ");
        paramStringBuilder.append(localListenerMethod.returnType());
        paramStringBuilder.append(' ');
        paramStringBuilder.append(localListenerMethod.name());
        paramStringBuilder.append("(\n");
        Object localObject4 = localListenerMethod.parameters();
        int k = localObject4.length;
        for (i = 0; i < k; i++)
        {
          paramStringBuilder.append(paramViewBindings);
          paramStringBuilder.append("          ");
          paramStringBuilder.append(localObject4[i]);
          paramStringBuilder.append(" p");
          paramStringBuilder.append(i);
          if (i < k - 1) {
            paramStringBuilder.append(',');
          }
          paramStringBuilder.append('\n');
        }
        paramStringBuilder.append(paramViewBindings);
        paramStringBuilder.append("        ) {\n");
        paramStringBuilder.append(paramViewBindings);
        paramStringBuilder.append("          ");
        int j = "void".equals(localListenerMethod.returnType()) ^ true;
        if (j != 0) {
          paramStringBuilder.append("return ");
        }
        if (((Map)localObject2).containsKey(localListenerMethod))
        {
          Iterator localIterator = ((Set)((Map)localObject2).get(localListenerMethod)).iterator();
          while (localIterator.hasNext())
          {
            MethodViewBinding localMethodViewBinding = (MethodViewBinding)localIterator.next();
            paramStringBuilder.append("target.");
            paramStringBuilder.append(localMethodViewBinding.getName());
            paramStringBuilder.append('(');
            localObject4 = localMethodViewBinding.getParameters();
            String[] arrayOfString = localListenerMethod.parameters();
            k = ((List)localObject4).size();
            for (j = 0; j < k; j++)
            {
              Parameter localParameter = (Parameter)((List)localObject4).get(j);
              int m = localParameter.getListenerPosition();
              if (localParameter.requiresCast(arrayOfString[m]))
              {
                paramStringBuilder.append("finder.<");
                paramStringBuilder.append(localParameter.getType());
                paramStringBuilder.append(">castParam(p");
                paramStringBuilder.append(m);
                paramStringBuilder.append(", \"");
                paramStringBuilder.append(localListenerMethod.name());
                paramStringBuilder.append("\", ");
                paramStringBuilder.append(m);
                paramStringBuilder.append(", \"");
                paramStringBuilder.append(localMethodViewBinding.getName());
                paramStringBuilder.append("\", ");
                paramStringBuilder.append(j);
                paramStringBuilder.append(")");
              }
              else
              {
                paramStringBuilder.append('p');
                paramStringBuilder.append(m);
              }
              if (j < k - 1) {
                paramStringBuilder.append(", ");
              }
            }
            paramStringBuilder.append(");");
            if (localIterator.hasNext())
            {
              paramStringBuilder.append("\n");
              paramStringBuilder.append("          ");
            }
          }
        }
        if (j != 0)
        {
          paramStringBuilder.append(localListenerMethod.defaultReturn());
          paramStringBuilder.append(';');
        }
        paramStringBuilder.append('\n');
        paramStringBuilder.append(paramViewBindings);
        paramStringBuilder.append("        }\n");
      }
      paramStringBuilder.append(paramViewBindings);
      paramStringBuilder.append("      });\n");
    }
    if (bool2) {
      paramStringBuilder.append("    }\n");
    }
  }
  
  private void emitUnbindMethod(StringBuilder paramStringBuilder)
  {
    paramStringBuilder.append("  @Override public void unbind(T target) {\n");
    if (this.parentViewBinder != null) {
      paramStringBuilder.append("    super.unbind(target);\n\n");
    }
    Iterator localIterator2 = this.viewIdMap.values().iterator();
    Object localObject;
    while (localIterator2.hasNext())
    {
      localIterator1 = ((ViewBindings)localIterator2.next()).getFieldBindings().iterator();
      while (localIterator1.hasNext())
      {
        localObject = (FieldViewBinding)localIterator1.next();
        paramStringBuilder.append("    target.");
        paramStringBuilder.append(((FieldViewBinding)localObject).getName());
        paramStringBuilder.append(" = null;\n");
      }
    }
    Iterator localIterator1 = this.collectionBindings.keySet().iterator();
    while (localIterator1.hasNext())
    {
      localObject = (FieldCollectionViewBinding)localIterator1.next();
      paramStringBuilder.append("    target.");
      paramStringBuilder.append(((FieldCollectionViewBinding)localObject).getName());
      paramStringBuilder.append(" = null;\n");
    }
    paramStringBuilder.append("  }\n");
  }
  
  private void emitViewBindings(StringBuilder paramStringBuilder, ViewBindings paramViewBindings)
  {
    paramStringBuilder.append("    view = ");
    List localList = paramViewBindings.getRequiredBindings();
    if (localList.isEmpty())
    {
      paramStringBuilder.append("finder.findOptionalView(source, ");
      paramStringBuilder.append(paramViewBindings.getId());
      paramStringBuilder.append(", null);\n");
    }
    else if (paramViewBindings.getId() == -1)
    {
      paramStringBuilder.append("target;\n");
    }
    else
    {
      paramStringBuilder.append("finder.findRequiredView(source, ");
      paramStringBuilder.append(paramViewBindings.getId());
      paramStringBuilder.append(", \"");
      emitHumanDescription(paramStringBuilder, localList);
      paramStringBuilder.append("\");\n");
    }
    emitFieldBindings(paramStringBuilder, paramViewBindings);
    emitMethodBindings(paramStringBuilder, paramViewBindings);
  }
  
  static List<ListenerMethod> getListenerMethods(ListenerClass paramListenerClass)
  {
    if (paramListenerClass.method().length == 1) {
      return Arrays.asList(paramListenerClass.method());
    }
    try
    {
      Object localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      paramListenerClass = paramListenerClass.callbacks();
      Enum[] arrayOfEnum = (Enum[])paramListenerClass.getEnumConstants();
      int j = arrayOfEnum.length;
      int i = 0;
      while (i < j)
      {
        Enum localEnum = arrayOfEnum[i];
        ListenerMethod localListenerMethod = (ListenerMethod)paramListenerClass.getField(localEnum.name()).getAnnotation(ListenerMethod.class);
        if (localListenerMethod != null)
        {
          ((List)localObject).add(localListenerMethod);
          i++;
        }
        else
        {
          localObject = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject).<init>(String.format("@%s's %s.%s missing @%s annotation.", new Object[] { paramListenerClass.getEnclosingClass().getSimpleName(), paramListenerClass.getSimpleName(), localEnum.name(), ListenerMethod.class.getSimpleName() }));
          throw ((Throwable)localObject);
        }
      }
      return (List<ListenerMethod>)localObject;
    }
    catch (NoSuchFieldException paramListenerClass)
    {
      throw new AssertionError(paramListenerClass);
    }
  }
  
  private ViewBindings getOrCreateViewBindings(int paramInt)
  {
    ViewBindings localViewBindings2 = (ViewBindings)this.viewIdMap.get(Integer.valueOf(paramInt));
    ViewBindings localViewBindings1 = localViewBindings2;
    if (localViewBindings2 == null)
    {
      localViewBindings1 = new ViewBindings(paramInt);
      this.viewIdMap.put(Integer.valueOf(paramInt), localViewBindings1);
    }
    return localViewBindings1;
  }
  
  void addField(int paramInt, FieldViewBinding paramFieldViewBinding)
  {
    getOrCreateViewBindings(paramInt).addFieldBinding(paramFieldViewBinding);
  }
  
  void addFieldCollection(int[] paramArrayOfInt, FieldCollectionViewBinding paramFieldCollectionViewBinding)
  {
    this.collectionBindings.put(paramFieldCollectionViewBinding, paramArrayOfInt);
  }
  
  boolean addMethod(int paramInt, ListenerClass paramListenerClass, ListenerMethod paramListenerMethod, MethodViewBinding paramMethodViewBinding)
  {
    ViewBindings localViewBindings = getOrCreateViewBindings(paramInt);
    if ((localViewBindings.hasMethodBinding(paramListenerClass, paramListenerMethod)) && (!"void".equals(paramListenerMethod.returnType()))) {
      return false;
    }
    localViewBindings.addMethodBinding(paramListenerClass, paramListenerMethod, paramMethodViewBinding);
    return true;
  }
  
  void addResource(FieldResourceBinding paramFieldResourceBinding)
  {
    this.resourceBindings.add(paramFieldResourceBinding);
  }
  
  String brewJava()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("// Generated code from Butter Knife. Do not modify!\n");
    localStringBuilder.append("package ");
    localStringBuilder.append(this.classPackage);
    localStringBuilder.append(";\n\n");
    if (!this.resourceBindings.isEmpty()) {
      localStringBuilder.append("import android.content.res.Resources;\n");
    }
    if ((!this.viewIdMap.isEmpty()) || (!this.collectionBindings.isEmpty())) {
      localStringBuilder.append("import android.view.View;\n");
    }
    localStringBuilder.append("import butterknife.ButterKnife.Finder;\n");
    if (this.parentViewBinder == null) {
      localStringBuilder.append("import butterknife.ButterKnife.ViewBinder;\n");
    }
    localStringBuilder.append('\n');
    localStringBuilder.append("public class ");
    localStringBuilder.append(this.className);
    localStringBuilder.append("<T extends ");
    localStringBuilder.append(this.targetClass);
    localStringBuilder.append(">");
    if (this.parentViewBinder != null)
    {
      localStringBuilder.append(" extends ");
      localStringBuilder.append(this.parentViewBinder);
      localStringBuilder.append("<T>");
    }
    else
    {
      localStringBuilder.append(" implements ViewBinder<T>");
    }
    localStringBuilder.append(" {\n");
    emitBindMethod(localStringBuilder);
    localStringBuilder.append('\n');
    emitUnbindMethod(localStringBuilder);
    localStringBuilder.append("}\n");
    return localStringBuilder.toString();
  }
  
  String getFqcn()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.classPackage);
    localStringBuilder.append(".");
    localStringBuilder.append(this.className);
    return localStringBuilder.toString();
  }
  
  ViewBindings getViewBinding(int paramInt)
  {
    return (ViewBindings)this.viewIdMap.get(Integer.valueOf(paramInt));
  }
  
  void setParentViewBinder(String paramString)
  {
    this.parentViewBinder = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/BindingClass.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */