package butterknife.internal;

final class FieldCollectionViewBinding
  implements ViewBinding
{
  private final Kind kind;
  private final String name;
  private final boolean required;
  private final String type;
  
  FieldCollectionViewBinding(String paramString1, String paramString2, Kind paramKind, boolean paramBoolean)
  {
    this.name = paramString1;
    this.type = paramString2;
    this.kind = paramKind;
    this.required = paramBoolean;
  }
  
  public String getDescription()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("field '");
    localStringBuilder.append(this.name);
    localStringBuilder.append("'");
    return localStringBuilder.toString();
  }
  
  public Kind getKind()
  {
    return this.kind;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isRequired()
  {
    return this.required;
  }
  
  static enum Kind
  {
    ARRAY,  LIST;
    
    private Kind() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/FieldCollectionViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */