package butterknife.internal;

final class FieldViewBinding
  implements ViewBinding
{
  private final String name;
  private final boolean required;
  private final String type;
  
  FieldViewBinding(String paramString1, String paramString2, boolean paramBoolean)
  {
    this.name = paramString1;
    this.type = paramString2;
    this.required = paramBoolean;
  }
  
  public String getDescription()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("field '");
    localStringBuilder.append(this.name);
    localStringBuilder.append("'");
    return localStringBuilder.toString();
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isRequired()
  {
    return this.required;
  }
  
  public boolean requiresCast()
  {
    return "android.view.View".equals(this.type) ^ true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/internal/FieldViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */