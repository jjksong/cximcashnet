package butterknife;

import java.util.AbstractList;
import java.util.RandomAccess;

final class ImmutableList<T>
  extends AbstractList<T>
  implements RandomAccess
{
  private final T[] views;
  
  ImmutableList(T[] paramArrayOfT)
  {
    this.views = paramArrayOfT;
  }
  
  public boolean contains(Object paramObject)
  {
    Object[] arrayOfObject = this.views;
    int j = arrayOfObject.length;
    for (int i = 0; i < j; i++) {
      if (arrayOfObject[i] == paramObject) {
        return true;
      }
    }
    return false;
  }
  
  public T get(int paramInt)
  {
    return (T)this.views[paramInt];
  }
  
  public int size()
  {
    return this.views.length;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/ImmutableList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */