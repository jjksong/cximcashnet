package butterknife;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target({java.lang.annotation.ElementType.FIELD})
public @interface BindString
{
  int value();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/butterknife/BindString.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */