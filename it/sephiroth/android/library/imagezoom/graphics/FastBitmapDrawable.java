package it.sephiroth.android.library.imagezoom.graphics;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import java.io.InputStream;

public class FastBitmapDrawable
  extends Drawable
  implements IBitmapDrawable
{
  protected Bitmap mBitmap;
  protected int mIntrinsicHeight;
  protected int mIntrinsicWidth;
  protected Paint mPaint;
  
  public FastBitmapDrawable(Resources paramResources, InputStream paramInputStream)
  {
    this(BitmapFactory.decodeStream(paramInputStream));
  }
  
  public FastBitmapDrawable(Bitmap paramBitmap)
  {
    this.mBitmap = paramBitmap;
    paramBitmap = this.mBitmap;
    if (paramBitmap != null)
    {
      this.mIntrinsicWidth = paramBitmap.getWidth();
      this.mIntrinsicHeight = this.mBitmap.getHeight();
    }
    else
    {
      this.mIntrinsicWidth = 0;
      this.mIntrinsicHeight = 0;
    }
    this.mPaint = new Paint();
    this.mPaint.setDither(true);
    this.mPaint.setFilterBitmap(true);
  }
  
  public void draw(Canvas paramCanvas)
  {
    Bitmap localBitmap = this.mBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled())) {
      paramCanvas.drawBitmap(this.mBitmap, 0.0F, 0.0F, this.mPaint);
    }
  }
  
  public Bitmap getBitmap()
  {
    return this.mBitmap;
  }
  
  public int getIntrinsicHeight()
  {
    return this.mIntrinsicHeight;
  }
  
  public int getIntrinsicWidth()
  {
    return this.mIntrinsicWidth;
  }
  
  public int getMinimumHeight()
  {
    return this.mIntrinsicHeight;
  }
  
  public int getMinimumWidth()
  {
    return this.mIntrinsicWidth;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public Paint getPaint()
  {
    return this.mPaint;
  }
  
  public void setAlpha(int paramInt)
  {
    this.mPaint.setAlpha(paramInt);
  }
  
  public void setAntiAlias(boolean paramBoolean)
  {
    this.mPaint.setAntiAlias(paramBoolean);
    invalidateSelf();
  }
  
  public void setBitmap(Bitmap paramBitmap)
  {
    this.mBitmap = paramBitmap;
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.mPaint.setColorFilter(paramColorFilter);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/imagezoom/graphics/FastBitmapDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */