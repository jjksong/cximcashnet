package it.sephiroth.android.library.imagezoom.graphics;

import android.graphics.Bitmap;

public abstract interface IBitmapDrawable
{
  public abstract Bitmap getBitmap();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/imagezoom/graphics/IBitmapDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */