package it.sephiroth.android.library.imagezoom.utils;

public abstract interface IDisposable
{
  public abstract void dispose();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/imagezoom/utils/IDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */