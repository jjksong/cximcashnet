package it.sephiroth.android.library.imagezoom;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.ViewConfiguration;

public class ImageViewTouch
  extends ImageViewTouchBase
{
  static final float SCROLL_DELTA_THRESHOLD = 1.0F;
  protected int mDoubleTapDirection;
  protected boolean mDoubleTapEnabled = true;
  private OnImageViewTouchDoubleTapListener mDoubleTapListener;
  protected GestureDetector mGestureDetector;
  protected GestureDetector.OnGestureListener mGestureListener;
  protected ScaleGestureDetector mScaleDetector;
  protected boolean mScaleEnabled = true;
  protected float mScaleFactor;
  protected ScaleGestureDetector.OnScaleGestureListener mScaleListener;
  protected boolean mScrollEnabled = true;
  private OnImageViewTouchSingleTapListener mSingleTapListener;
  protected int mTouchSlop;
  
  public ImageViewTouch(Context paramContext)
  {
    super(paramContext);
  }
  
  public ImageViewTouch(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ImageViewTouch(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void _setImageDrawable(Drawable paramDrawable, Matrix paramMatrix, float paramFloat1, float paramFloat2)
  {
    super._setImageDrawable(paramDrawable, paramMatrix, paramFloat1, paramFloat2);
    this.mScaleFactor = (getMaxScale() / 3.0F);
  }
  
  public boolean canScroll(int paramInt)
  {
    RectF localRectF = getBitmapRect();
    updateRect(localRectF, this.mScrollRect);
    Rect localRect = new Rect();
    getGlobalVisibleRect(localRect);
    boolean bool2 = false;
    boolean bool1 = false;
    if (localRectF == null) {
      return false;
    }
    if ((localRectF.right >= localRect.right) && (paramInt < 0))
    {
      if (Math.abs(localRectF.right - localRect.right) > 1.0F) {
        bool1 = true;
      }
      return bool1;
    }
    bool1 = bool2;
    if (Math.abs(localRectF.left - this.mScrollRect.left) > 1.0D) {
      bool1 = true;
    }
    return bool1;
  }
  
  public boolean getDoubleTapEnabled()
  {
    return this.mDoubleTapEnabled;
  }
  
  protected GestureDetector.OnGestureListener getGestureListener()
  {
    return new GestureListener();
  }
  
  protected ScaleGestureDetector.OnScaleGestureListener getScaleListener()
  {
    return new ScaleListener();
  }
  
  protected void init(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super.init(paramContext, paramAttributeSet, paramInt);
    this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    this.mGestureListener = getGestureListener();
    this.mScaleListener = getScaleListener();
    this.mScaleDetector = new ScaleGestureDetector(getContext(), this.mScaleListener);
    this.mGestureDetector = new GestureDetector(getContext(), this.mGestureListener, null, true);
    this.mDoubleTapDirection = 1;
  }
  
  protected float onDoubleTapPost(float paramFloat1, float paramFloat2)
  {
    if (this.mDoubleTapDirection == 1)
    {
      float f = this.mScaleFactor;
      if (2.0F * f + paramFloat1 <= paramFloat2) {
        return paramFloat1 + f;
      }
      this.mDoubleTapDirection = -1;
      return paramFloat2;
    }
    this.mDoubleTapDirection = 1;
    return 1.0F;
  }
  
  public boolean onDown(MotionEvent paramMotionEvent)
  {
    return true;
  }
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    float f2 = paramMotionEvent2.getX();
    float f3 = paramMotionEvent1.getX();
    float f1 = paramMotionEvent2.getY();
    float f4 = paramMotionEvent1.getY();
    if ((Math.abs(paramFloat1) <= 800.0F) && (Math.abs(paramFloat2) <= 800.0F)) {
      return false;
    }
    this.mUserScaled = true;
    scrollBy((f2 - f3) / 2.0F, (f1 - f4) / 2.0F, 300.0D);
    invalidate();
    return true;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    if (getScale() == 1.0F) {
      return false;
    }
    this.mUserScaled = true;
    scrollBy(-paramFloat1, -paramFloat2);
    invalidate();
    return true;
  }
  
  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    return true;
  }
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    this.mScaleDetector.onTouchEvent(paramMotionEvent);
    if (!this.mScaleDetector.isInProgress()) {
      this.mGestureDetector.onTouchEvent(paramMotionEvent);
    }
    if ((paramMotionEvent.getAction() & 0xFF) != 1) {
      return true;
    }
    return onUp(paramMotionEvent);
  }
  
  public boolean onUp(MotionEvent paramMotionEvent)
  {
    if (getScale() < getMinScale()) {
      zoomTo(getMinScale(), 50.0F);
    }
    return true;
  }
  
  protected void onZoomAnimationCompleted(float paramFloat)
  {
    if (paramFloat < getMinScale()) {
      zoomTo(getMinScale(), 50.0F);
    }
  }
  
  public void setDoubleTapEnabled(boolean paramBoolean)
  {
    this.mDoubleTapEnabled = paramBoolean;
  }
  
  public void setDoubleTapListener(OnImageViewTouchDoubleTapListener paramOnImageViewTouchDoubleTapListener)
  {
    this.mDoubleTapListener = paramOnImageViewTouchDoubleTapListener;
  }
  
  public void setScaleEnabled(boolean paramBoolean)
  {
    this.mScaleEnabled = paramBoolean;
  }
  
  public void setScrollEnabled(boolean paramBoolean)
  {
    this.mScrollEnabled = paramBoolean;
  }
  
  public void setSingleTapListener(OnImageViewTouchSingleTapListener paramOnImageViewTouchSingleTapListener)
  {
    this.mSingleTapListener = paramOnImageViewTouchSingleTapListener;
  }
  
  public class GestureListener
    extends GestureDetector.SimpleOnGestureListener
  {
    public GestureListener() {}
    
    public boolean onDoubleTap(MotionEvent paramMotionEvent)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("onDoubleTap. double tap enabled? ");
      ((StringBuilder)localObject).append(ImageViewTouch.this.mDoubleTapEnabled);
      Log.i("ImageViewTouchBase", ((StringBuilder)localObject).toString());
      if (ImageViewTouch.this.mDoubleTapEnabled)
      {
        localObject = ImageViewTouch.this;
        ((ImageViewTouch)localObject).mUserScaled = true;
        float f = ((ImageViewTouch)localObject).getScale();
        localObject = ImageViewTouch.this;
        f = ((ImageViewTouch)localObject).onDoubleTapPost(f, ((ImageViewTouch)localObject).getMaxScale());
        f = Math.min(ImageViewTouch.this.getMaxScale(), Math.max(f, ImageViewTouch.this.getMinScale()));
        ImageViewTouch.this.zoomTo(f, paramMotionEvent.getX(), paramMotionEvent.getY(), 200.0F);
        ImageViewTouch.this.invalidate();
      }
      if (ImageViewTouch.this.mDoubleTapListener != null) {
        ImageViewTouch.this.mDoubleTapListener.onDoubleTap();
      }
      return super.onDoubleTap(paramMotionEvent);
    }
    
    public boolean onDown(MotionEvent paramMotionEvent)
    {
      return ImageViewTouch.this.onDown(paramMotionEvent);
    }
    
    public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
      if (!ImageViewTouch.this.mScrollEnabled) {
        return false;
      }
      if ((paramMotionEvent1.getPointerCount() <= 1) && (paramMotionEvent2.getPointerCount() <= 1))
      {
        if (ImageViewTouch.this.mScaleDetector.isInProgress()) {
          return false;
        }
        if (ImageViewTouch.this.getScale() == 1.0F) {
          return false;
        }
        return ImageViewTouch.this.onFling(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
      }
      return false;
    }
    
    public void onLongPress(MotionEvent paramMotionEvent)
    {
      if ((ImageViewTouch.this.isLongClickable()) && (!ImageViewTouch.this.mScaleDetector.isInProgress()))
      {
        ImageViewTouch.this.setPressed(true);
        ImageViewTouch.this.performLongClick();
      }
    }
    
    public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
      if (!ImageViewTouch.this.mScrollEnabled) {
        return false;
      }
      if ((paramMotionEvent1 != null) && (paramMotionEvent2 != null))
      {
        if ((paramMotionEvent1.getPointerCount() <= 1) && (paramMotionEvent2.getPointerCount() <= 1))
        {
          if (ImageViewTouch.this.mScaleDetector.isInProgress()) {
            return false;
          }
          return ImageViewTouch.this.onScroll(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
        }
        return false;
      }
      return false;
    }
    
    public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
    {
      if (ImageViewTouch.this.mSingleTapListener != null) {
        ImageViewTouch.this.mSingleTapListener.onSingleTapConfirmed();
      }
      return ImageViewTouch.this.onSingleTapConfirmed(paramMotionEvent);
    }
    
    public boolean onSingleTapUp(MotionEvent paramMotionEvent)
    {
      return ImageViewTouch.this.onSingleTapUp(paramMotionEvent);
    }
  }
  
  public static abstract interface OnImageViewTouchDoubleTapListener
  {
    public abstract void onDoubleTap();
  }
  
  public static abstract interface OnImageViewTouchSingleTapListener
  {
    public abstract void onSingleTapConfirmed();
  }
  
  public class ScaleListener
    extends ScaleGestureDetector.SimpleOnScaleGestureListener
  {
    protected boolean mScaled = false;
    
    public ScaleListener() {}
    
    public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
    {
      float f1 = paramScaleGestureDetector.getCurrentSpan();
      float f2 = paramScaleGestureDetector.getPreviousSpan();
      float f4 = ImageViewTouch.this.getScale();
      float f3 = paramScaleGestureDetector.getScaleFactor();
      if (ImageViewTouch.this.mScaleEnabled)
      {
        if ((this.mScaled) && (f1 - f2 != 0.0F))
        {
          ImageViewTouch localImageViewTouch = ImageViewTouch.this;
          localImageViewTouch.mUserScaled = true;
          f1 = Math.min(localImageViewTouch.getMaxScale(), Math.max(f4 * f3, ImageViewTouch.this.getMinScale() - 0.1F));
          ImageViewTouch.this.zoomTo(f1, paramScaleGestureDetector.getFocusX(), paramScaleGestureDetector.getFocusY());
          paramScaleGestureDetector = ImageViewTouch.this;
          paramScaleGestureDetector.mDoubleTapDirection = 1;
          paramScaleGestureDetector.invalidate();
          return true;
        }
        if (!this.mScaled) {
          this.mScaled = true;
        }
      }
      return true;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/imagezoom/ImageViewTouch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */