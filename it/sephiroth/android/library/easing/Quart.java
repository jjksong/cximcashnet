package it.sephiroth.android.library.easing;

public class Quart
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble1 /= paramDouble4;
    return paramDouble3 * paramDouble1 * paramDouble1 * paramDouble1 * paramDouble1 + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble1 /= paramDouble4 / 2.0D;
    if (paramDouble1 < 1.0D) {
      return paramDouble3 / 2.0D * paramDouble1 * paramDouble1 * paramDouble1 * paramDouble1 + paramDouble2;
    }
    paramDouble3 = -paramDouble3 / 2.0D;
    paramDouble1 -= 2.0D;
    return paramDouble3 * (paramDouble1 * paramDouble1 * paramDouble1 * paramDouble1 - 2.0D) + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble3 = -paramDouble3;
    paramDouble1 = paramDouble1 / paramDouble4 - 1.0D;
    return paramDouble3 * (paramDouble1 * paramDouble1 * paramDouble1 * paramDouble1 - 1.0D) + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Quart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */