package it.sephiroth.android.library.easing;

public class Sine
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return -paramDouble3 * Math.cos(paramDouble1 / paramDouble4 * 1.5707963267948966D) + paramDouble3 + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return -paramDouble3 / 2.0D * (Math.cos(paramDouble1 * 3.141592653589793D / paramDouble4) - 1.0D) + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 * Math.sin(paramDouble1 / paramDouble4 * 1.5707963267948966D) + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Sine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */