package it.sephiroth.android.library.easing;

public class Linear
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 * paramDouble1 / paramDouble4 + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 * paramDouble1 / paramDouble4 + paramDouble2;
  }
  
  public double easeNone(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 * paramDouble1 / paramDouble4 + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 * paramDouble1 / paramDouble4 + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Linear.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */