package it.sephiroth.android.library.easing;

public class Bounce
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return paramDouble3 - easeOut(paramDouble4 - paramDouble1, 0.0D, paramDouble3, paramDouble4) + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    if (paramDouble1 < paramDouble4 / 2.0D) {
      return easeIn(paramDouble1 * 2.0D, 0.0D, paramDouble3, paramDouble4) * 0.5D + paramDouble2;
    }
    return easeOut(2.0D * paramDouble1 - paramDouble4, 0.0D, paramDouble3, paramDouble4) * 0.5D + paramDouble3 * 0.5D + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble1 /= paramDouble4;
    if (paramDouble1 < 0.36363636363636365D) {
      return paramDouble3 * (7.5625D * paramDouble1 * paramDouble1) + paramDouble2;
    }
    if (paramDouble1 < 0.7272727272727273D)
    {
      paramDouble1 -= 0.5454545454545454D;
      return paramDouble3 * (7.5625D * paramDouble1 * paramDouble1 + 0.75D) + paramDouble2;
    }
    if (paramDouble1 < 0.9090909090909091D)
    {
      paramDouble1 -= 0.8181818181818182D;
      return paramDouble3 * (7.5625D * paramDouble1 * paramDouble1 + 0.9375D) + paramDouble2;
    }
    paramDouble1 -= 0.9545454545454546D;
    return paramDouble3 * (7.5625D * paramDouble1 * paramDouble1 + 0.984375D) + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Bounce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */