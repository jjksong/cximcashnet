package it.sephiroth.android.library.easing;

public class Elastic
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeIn(paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramDouble2 + paramDouble3, paramDouble4);
  }
  
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
  {
    if (paramDouble1 == 0.0D) {
      return paramDouble2;
    }
    double d = paramDouble1 / paramDouble4;
    if (d == 1.0D) {
      return paramDouble2 + paramDouble3;
    }
    if (paramDouble6 <= 0.0D) {
      paramDouble1 = 0.3D * paramDouble4;
    } else {
      paramDouble1 = paramDouble6;
    }
    if ((paramDouble5 > 0.0D) && (paramDouble5 >= Math.abs(paramDouble3)))
    {
      paramDouble3 = paramDouble1 / 6.283185307179586D * Math.asin(paramDouble3 / paramDouble5);
    }
    else
    {
      paramDouble6 = paramDouble1 / 4.0D;
      paramDouble5 = paramDouble3;
      paramDouble3 = paramDouble6;
    }
    paramDouble6 = d - 1.0D;
    return -(paramDouble5 * Math.pow(2.0D, 10.0D * paramDouble6) * Math.sin((paramDouble6 * paramDouble4 - paramDouble3) * 6.283185307179586D / paramDouble1)) + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeInOut(paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramDouble2 + paramDouble3, paramDouble4);
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
  {
    if (paramDouble1 == 0.0D) {
      return paramDouble2;
    }
    double d = paramDouble1 / (paramDouble4 / 2.0D);
    if (d == 2.0D) {
      return paramDouble2 + paramDouble3;
    }
    if (paramDouble6 <= 0.0D) {
      paramDouble1 = 0.44999999999999996D * paramDouble4;
    } else {
      paramDouble1 = paramDouble6;
    }
    if ((paramDouble5 > 0.0D) && (paramDouble5 >= Math.abs(paramDouble3)))
    {
      paramDouble6 = paramDouble1 / 6.283185307179586D * Math.asin(paramDouble3 / paramDouble5);
    }
    else
    {
      paramDouble6 = paramDouble1 / 4.0D;
      paramDouble5 = paramDouble3;
    }
    if (d < 1.0D)
    {
      paramDouble3 = d - 1.0D;
      return paramDouble5 * Math.pow(2.0D, paramDouble3 * 10.0D) * Math.sin((paramDouble3 * paramDouble4 - paramDouble6) * 6.283185307179586D / paramDouble1) * -0.5D + paramDouble2;
    }
    d -= 1.0D;
    return paramDouble5 * Math.pow(2.0D, -10.0D * d) * Math.sin((d * paramDouble4 - paramDouble6) * 6.283185307179586D / paramDouble1) * 0.5D + paramDouble3 + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeOut(paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramDouble2 + paramDouble3, paramDouble4);
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
  {
    if (paramDouble1 == 0.0D) {
      return paramDouble2;
    }
    double d = paramDouble1 / paramDouble4;
    if (d == 1.0D) {
      return paramDouble2 + paramDouble3;
    }
    if (paramDouble6 <= 0.0D) {
      paramDouble1 = 0.3D * paramDouble4;
    } else {
      paramDouble1 = paramDouble6;
    }
    if ((paramDouble5 > 0.0D) && (paramDouble5 >= Math.abs(paramDouble3)))
    {
      paramDouble6 = paramDouble1 / 6.283185307179586D * Math.asin(paramDouble3 / paramDouble5);
    }
    else
    {
      paramDouble6 = paramDouble1 / 4.0D;
      paramDouble5 = paramDouble3;
    }
    return paramDouble5 * Math.pow(2.0D, -10.0D * d) * Math.sin((d * paramDouble4 - paramDouble6) * 6.283185307179586D / paramDouble1) + paramDouble3 + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Elastic.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */