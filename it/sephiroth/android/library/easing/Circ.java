package it.sephiroth.android.library.easing;

public class Circ
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble3 = -paramDouble3;
    paramDouble1 /= paramDouble4;
    return paramDouble3 * (Math.sqrt(1.0D - paramDouble1 * paramDouble1) - 1.0D) + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble4 = paramDouble1 / (paramDouble4 / 2.0D);
    if (paramDouble4 < 1.0D) {
      return -paramDouble3 / 2.0D * (Math.sqrt(1.0D - paramDouble4 * paramDouble4) - 1.0D) + paramDouble2;
    }
    paramDouble1 = paramDouble3 / 2.0D;
    paramDouble3 = paramDouble4 - 2.0D;
    return paramDouble1 * (Math.sqrt(1.0D - paramDouble3 * paramDouble3) + 1.0D) + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble1 = paramDouble1 / paramDouble4 - 1.0D;
    return paramDouble3 * Math.sqrt(1.0D - paramDouble1 * paramDouble1) + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Circ.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */