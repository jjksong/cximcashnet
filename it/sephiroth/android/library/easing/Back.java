package it.sephiroth.android.library.easing;

public class Back
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeIn(paramDouble1, paramDouble2, paramDouble3, paramDouble4, 0.0D);
  }
  
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5)
  {
    double d = paramDouble5;
    if (paramDouble5 == 0.0D) {
      d = 1.70158D;
    }
    paramDouble1 /= paramDouble4;
    return paramDouble3 * paramDouble1 * paramDouble1 * ((1.0D + d) * paramDouble1 - d) + paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeInOut(paramDouble1, paramDouble2, paramDouble3, paramDouble4, 0.9D);
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5)
  {
    if (paramDouble5 == 0.0D) {
      paramDouble5 = 1.70158D;
    }
    paramDouble1 /= paramDouble4 / 2.0D;
    if (paramDouble1 < 1.0D)
    {
      paramDouble3 /= 2.0D;
      paramDouble4 = paramDouble5 * 1.525D;
      return paramDouble3 * (paramDouble1 * paramDouble1 * ((1.0D + paramDouble4) * paramDouble1 - paramDouble4)) + paramDouble2;
    }
    paramDouble3 /= 2.0D;
    paramDouble1 -= 2.0D;
    paramDouble4 = paramDouble5 * 1.525D;
    return paramDouble3 * (paramDouble1 * paramDouble1 * ((1.0D + paramDouble4) * paramDouble1 + paramDouble4) + 2.0D) + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return easeOut(paramDouble1, paramDouble2, paramDouble3, paramDouble4, 0.0D);
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5)
  {
    double d = paramDouble5;
    if (paramDouble5 == 0.0D) {
      d = 1.70158D;
    }
    paramDouble1 = paramDouble1 / paramDouble4 - 1.0D;
    return paramDouble3 * (paramDouble1 * paramDouble1 * ((d + 1.0D) * paramDouble1 + d) + 1.0D) + paramDouble2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Back.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */