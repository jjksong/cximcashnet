package it.sephiroth.android.library.easing;

public abstract interface Easing
{
  public abstract double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);
  
  public abstract double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);
  
  public abstract double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Easing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */