package it.sephiroth.android.library.easing;

public class Expo
  implements Easing
{
  public double easeIn(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    if (paramDouble1 != 0.0D) {
      paramDouble2 += paramDouble3 * Math.pow(2.0D, (paramDouble1 / paramDouble4 - 1.0D) * 10.0D);
    }
    return paramDouble2;
  }
  
  public double easeInOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    if (paramDouble1 == 0.0D) {
      return paramDouble2;
    }
    if (paramDouble1 == paramDouble4) {
      return paramDouble2 + paramDouble3;
    }
    paramDouble1 /= paramDouble4 / 2.0D;
    if (paramDouble1 < 1.0D) {
      return paramDouble3 / 2.0D * Math.pow(2.0D, (paramDouble1 - 1.0D) * 10.0D) + paramDouble2;
    }
    return paramDouble3 / 2.0D * (-Math.pow(2.0D, (paramDouble1 - 1.0D) * -10.0D) + 2.0D) + paramDouble2;
  }
  
  public double easeOut(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    if (paramDouble1 == paramDouble4) {
      paramDouble1 = paramDouble2 + paramDouble3;
    } else {
      paramDouble1 = paramDouble2 + paramDouble3 * (-Math.pow(2.0D, paramDouble1 * -10.0D / paramDouble4) + 1.0D);
    }
    return paramDouble1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/it/sephiroth/android/library/easing/Expo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */