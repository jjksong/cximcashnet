package cz.msebera.android.httpclient;

import java.io.IOException;

public class MessageConstraintException
  extends IOException
{
  private static final long serialVersionUID = 6077207720446368695L;
  
  public MessageConstraintException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/MessageConstraintException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */