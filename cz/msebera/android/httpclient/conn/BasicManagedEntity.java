package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.entity.HttpEntityWrapper;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;

@Deprecated
@NotThreadSafe
public class BasicManagedEntity
  extends HttpEntityWrapper
  implements ConnectionReleaseTrigger, EofSensorWatcher
{
  protected final boolean attemptReuse;
  protected ManagedClientConnection managedConn;
  
  public BasicManagedEntity(HttpEntity paramHttpEntity, ManagedClientConnection paramManagedClientConnection, boolean paramBoolean)
  {
    super(paramHttpEntity);
    Args.notNull(paramManagedClientConnection, "Connection");
    this.managedConn = paramManagedClientConnection;
    this.attemptReuse = paramBoolean;
  }
  
  private void ensureConsumed()
    throws IOException
  {
    ManagedClientConnection localManagedClientConnection = this.managedConn;
    if (localManagedClientConnection == null) {
      return;
    }
    try
    {
      if (this.attemptReuse)
      {
        EntityUtils.consume(this.wrappedEntity);
        this.managedConn.markReusable();
      }
      else
      {
        localManagedClientConnection.unmarkReusable();
      }
      return;
    }
    finally
    {
      releaseManagedConnection();
    }
  }
  
  /* Error */
  public void abortConnection()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +25 -> 31
    //   9: aload_1
    //   10: invokeinterface 60 1 0
    //   15: aload_0
    //   16: aconst_null
    //   17: putfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   20: goto +11 -> 31
    //   23: astore_1
    //   24: aload_0
    //   25: aconst_null
    //   26: putfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   29: aload_1
    //   30: athrow
    //   31: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	32	0	this	BasicManagedEntity
    //   4	6	1	localManagedClientConnection	ManagedClientConnection
    //   23	7	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	15	23	finally
  }
  
  @Deprecated
  public void consumeContent()
    throws IOException
  {
    ensureConsumed();
  }
  
  public boolean eofDetected(InputStream paramInputStream)
    throws IOException
  {
    try
    {
      if (this.managedConn != null) {
        if (this.attemptReuse)
        {
          paramInputStream.close();
          this.managedConn.markReusable();
        }
        else
        {
          this.managedConn.unmarkReusable();
        }
      }
      return false;
    }
    finally
    {
      releaseManagedConnection();
    }
  }
  
  public InputStream getContent()
    throws IOException
  {
    return new EofSensorInputStream(this.wrappedEntity.getContent(), this);
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public void releaseConnection()
    throws IOException
  {
    ensureConsumed();
  }
  
  /* Error */
  protected void releaseManagedConnection()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +25 -> 31
    //   9: aload_1
    //   10: invokeinterface 87 1 0
    //   15: aload_0
    //   16: aconst_null
    //   17: putfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   20: goto +11 -> 31
    //   23: astore_1
    //   24: aload_0
    //   25: aconst_null
    //   26: putfield 29	cz/msebera/android/httpclient/conn/BasicManagedEntity:managedConn	Lcz/msebera/android/httpclient/conn/ManagedClientConnection;
    //   29: aload_1
    //   30: athrow
    //   31: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	32	0	this	BasicManagedEntity
    //   4	6	1	localManagedClientConnection	ManagedClientConnection
    //   23	7	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	15	23	finally
  }
  
  public boolean streamAbort(InputStream paramInputStream)
    throws IOException
  {
    paramInputStream = this.managedConn;
    if (paramInputStream != null) {
      paramInputStream.abortConnection();
    }
    return false;
  }
  
  public boolean streamClosed(InputStream paramInputStream)
    throws IOException
  {
    try
    {
      if (this.managedConn != null) {
        if (this.attemptReuse)
        {
          boolean bool = this.managedConn.isOpen();
          try
          {
            paramInputStream.close();
            this.managedConn.markReusable();
          }
          catch (SocketException paramInputStream)
          {
            if (bool) {
              break label48;
            }
          }
          break label59;
          label48:
          throw paramInputStream;
        }
        else
        {
          this.managedConn.unmarkReusable();
        }
      }
      label59:
      return false;
    }
    finally
    {
      releaseManagedConnection();
    }
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    super.writeTo(paramOutputStream);
    ensureConsumed();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/BasicManagedEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */