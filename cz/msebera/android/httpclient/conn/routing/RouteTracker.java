package cz.msebera.android.httpclient.conn.routing;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.LangUtils;
import java.net.InetAddress;

@NotThreadSafe
public final class RouteTracker
  implements RouteInfo, Cloneable
{
  private boolean connected;
  private RouteInfo.LayerType layered;
  private final InetAddress localAddress;
  private HttpHost[] proxyChain;
  private boolean secure;
  private final HttpHost targetHost;
  private RouteInfo.TunnelType tunnelled;
  
  public RouteTracker(HttpHost paramHttpHost, InetAddress paramInetAddress)
  {
    Args.notNull(paramHttpHost, "Target host");
    this.targetHost = paramHttpHost;
    this.localAddress = paramInetAddress;
    this.tunnelled = RouteInfo.TunnelType.PLAIN;
    this.layered = RouteInfo.LayerType.PLAIN;
  }
  
  public RouteTracker(HttpRoute paramHttpRoute)
  {
    this(paramHttpRoute.getTargetHost(), paramHttpRoute.getLocalAddress());
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public final void connectProxy(HttpHost paramHttpHost, boolean paramBoolean)
  {
    Args.notNull(paramHttpHost, "Proxy host");
    Asserts.check(this.connected ^ true, "Already connected");
    this.connected = true;
    this.proxyChain = new HttpHost[] { paramHttpHost };
    this.secure = paramBoolean;
  }
  
  public final void connectTarget(boolean paramBoolean)
  {
    Asserts.check(this.connected ^ true, "Already connected");
    this.connected = true;
    this.secure = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (paramObject == this) {
      return true;
    }
    if (!(paramObject instanceof RouteTracker)) {
      return false;
    }
    paramObject = (RouteTracker)paramObject;
    if ((this.connected != ((RouteTracker)paramObject).connected) || (this.secure != ((RouteTracker)paramObject).secure) || (this.tunnelled != ((RouteTracker)paramObject).tunnelled) || (this.layered != ((RouteTracker)paramObject).layered) || (!LangUtils.equals(this.targetHost, ((RouteTracker)paramObject).targetHost)) || (!LangUtils.equals(this.localAddress, ((RouteTracker)paramObject).localAddress)) || (!LangUtils.equals(this.proxyChain, ((RouteTracker)paramObject).proxyChain))) {
      bool = false;
    }
    return bool;
  }
  
  public final int getHopCount()
  {
    boolean bool = this.connected;
    int i = 1;
    if (bool)
    {
      HttpHost[] arrayOfHttpHost = this.proxyChain;
      if (arrayOfHttpHost != null) {
        i = 1 + arrayOfHttpHost.length;
      }
    }
    else
    {
      i = 0;
    }
    return i;
  }
  
  public final HttpHost getHopTarget(int paramInt)
  {
    Args.notNegative(paramInt, "Hop index");
    int i = getHopCount();
    boolean bool;
    if (paramInt < i) {
      bool = true;
    } else {
      bool = false;
    }
    Args.check(bool, "Hop index exceeds tracked route length");
    HttpHost localHttpHost;
    if (paramInt < i - 1) {
      localHttpHost = this.proxyChain[paramInt];
    } else {
      localHttpHost = this.targetHost;
    }
    return localHttpHost;
  }
  
  public final RouteInfo.LayerType getLayerType()
  {
    return this.layered;
  }
  
  public final InetAddress getLocalAddress()
  {
    return this.localAddress;
  }
  
  public final HttpHost getProxyHost()
  {
    Object localObject = this.proxyChain;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = localObject[0];
    }
    return (HttpHost)localObject;
  }
  
  public final HttpHost getTargetHost()
  {
    return this.targetHost;
  }
  
  public final RouteInfo.TunnelType getTunnelType()
  {
    return this.tunnelled;
  }
  
  public final int hashCode()
  {
    int i = LangUtils.hashCode(LangUtils.hashCode(17, this.targetHost), this.localAddress);
    HttpHost[] arrayOfHttpHost = this.proxyChain;
    int k = i;
    if (arrayOfHttpHost != null)
    {
      int m = arrayOfHttpHost.length;
      for (int j = 0;; j++)
      {
        k = i;
        if (j >= m) {
          break;
        }
        i = LangUtils.hashCode(i, arrayOfHttpHost[j]);
      }
    }
    return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(k, this.connected), this.secure), this.tunnelled), this.layered);
  }
  
  public final boolean isConnected()
  {
    return this.connected;
  }
  
  public final boolean isLayered()
  {
    boolean bool;
    if (this.layered == RouteInfo.LayerType.LAYERED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final boolean isSecure()
  {
    return this.secure;
  }
  
  public final boolean isTunnelled()
  {
    boolean bool;
    if (this.tunnelled == RouteInfo.TunnelType.TUNNELLED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final void layerProtocol(boolean paramBoolean)
  {
    Asserts.check(this.connected, "No layered protocol unless connected");
    this.layered = RouteInfo.LayerType.LAYERED;
    this.secure = paramBoolean;
  }
  
  public void reset()
  {
    this.connected = false;
    this.proxyChain = null;
    this.tunnelled = RouteInfo.TunnelType.PLAIN;
    this.layered = RouteInfo.LayerType.PLAIN;
    this.secure = false;
  }
  
  public final HttpRoute toRoute()
  {
    HttpRoute localHttpRoute;
    if (!this.connected) {
      localHttpRoute = null;
    } else {
      localHttpRoute = new HttpRoute(this.targetHost, this.localAddress, this.proxyChain, this.secure, this.tunnelled, this.layered);
    }
    return localHttpRoute;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(getHopCount() * 30 + 50);
    localStringBuilder.append("RouteTracker[");
    Object localObject = this.localAddress;
    if (localObject != null)
    {
      localStringBuilder.append(localObject);
      localStringBuilder.append("->");
    }
    localStringBuilder.append('{');
    if (this.connected) {
      localStringBuilder.append('c');
    }
    if (this.tunnelled == RouteInfo.TunnelType.TUNNELLED) {
      localStringBuilder.append('t');
    }
    if (this.layered == RouteInfo.LayerType.LAYERED) {
      localStringBuilder.append('l');
    }
    if (this.secure) {
      localStringBuilder.append('s');
    }
    localStringBuilder.append("}->");
    localObject = this.proxyChain;
    if (localObject != null)
    {
      int j = localObject.length;
      for (int i = 0; i < j; i++)
      {
        localStringBuilder.append(localObject[i]);
        localStringBuilder.append("->");
      }
    }
    localStringBuilder.append(this.targetHost);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
  
  public final void tunnelProxy(HttpHost paramHttpHost, boolean paramBoolean)
  {
    Args.notNull(paramHttpHost, "Proxy host");
    Asserts.check(this.connected, "No tunnel unless connected");
    Asserts.notNull(this.proxyChain, "No tunnel without proxy");
    HttpHost[] arrayOfHttpHost1 = this.proxyChain;
    HttpHost[] arrayOfHttpHost2 = new HttpHost[arrayOfHttpHost1.length + 1];
    System.arraycopy(arrayOfHttpHost1, 0, arrayOfHttpHost2, 0, arrayOfHttpHost1.length);
    arrayOfHttpHost2[(arrayOfHttpHost2.length - 1)] = paramHttpHost;
    this.proxyChain = arrayOfHttpHost2;
    this.secure = paramBoolean;
  }
  
  public final void tunnelTarget(boolean paramBoolean)
  {
    Asserts.check(this.connected, "No tunnel unless connected");
    Asserts.notNull(this.proxyChain, "No tunnel without proxy");
    this.tunnelled = RouteInfo.TunnelType.TUNNELLED;
    this.secure = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/routing/RouteTracker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */