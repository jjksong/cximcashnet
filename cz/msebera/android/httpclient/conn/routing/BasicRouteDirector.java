package cz.msebera.android.httpclient.conn.routing;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.net.InetAddress;

@Immutable
public class BasicRouteDirector
  implements HttpRouteDirector
{
  protected int directStep(RouteInfo paramRouteInfo1, RouteInfo paramRouteInfo2)
  {
    if (paramRouteInfo2.getHopCount() > 1) {
      return -1;
    }
    if (!paramRouteInfo1.getTargetHost().equals(paramRouteInfo2.getTargetHost())) {
      return -1;
    }
    if (paramRouteInfo1.isSecure() != paramRouteInfo2.isSecure()) {
      return -1;
    }
    if ((paramRouteInfo1.getLocalAddress() != null) && (!paramRouteInfo1.getLocalAddress().equals(paramRouteInfo2.getLocalAddress()))) {
      return -1;
    }
    return 0;
  }
  
  protected int firstStep(RouteInfo paramRouteInfo)
  {
    int j = paramRouteInfo.getHopCount();
    int i = 1;
    if (j > 1) {
      i = 2;
    }
    return i;
  }
  
  public int nextStep(RouteInfo paramRouteInfo1, RouteInfo paramRouteInfo2)
  {
    Args.notNull(paramRouteInfo1, "Planned route");
    int i;
    if ((paramRouteInfo2 != null) && (paramRouteInfo2.getHopCount() >= 1))
    {
      if (paramRouteInfo1.getHopCount() > 1) {
        i = proxiedStep(paramRouteInfo1, paramRouteInfo2);
      } else {
        i = directStep(paramRouteInfo1, paramRouteInfo2);
      }
    }
    else {
      i = firstStep(paramRouteInfo1);
    }
    return i;
  }
  
  protected int proxiedStep(RouteInfo paramRouteInfo1, RouteInfo paramRouteInfo2)
  {
    if (paramRouteInfo2.getHopCount() <= 1) {
      return -1;
    }
    if (!paramRouteInfo1.getTargetHost().equals(paramRouteInfo2.getTargetHost())) {
      return -1;
    }
    int j = paramRouteInfo1.getHopCount();
    int k = paramRouteInfo2.getHopCount();
    if (j < k) {
      return -1;
    }
    for (int i = 0; i < k - 1; i++) {
      if (!paramRouteInfo1.getHopTarget(i).equals(paramRouteInfo2.getHopTarget(i))) {
        return -1;
      }
    }
    if (j > k) {
      return 4;
    }
    if (((paramRouteInfo2.isTunnelled()) && (!paramRouteInfo1.isTunnelled())) || ((paramRouteInfo2.isLayered()) && (!paramRouteInfo1.isLayered()))) {
      return -1;
    }
    if ((paramRouteInfo1.isTunnelled()) && (!paramRouteInfo2.isTunnelled())) {
      return 3;
    }
    if ((paramRouteInfo1.isLayered()) && (!paramRouteInfo2.isLayered())) {
      return 5;
    }
    if (paramRouteInfo1.isSecure() != paramRouteInfo2.isSecure()) {
      return -1;
    }
    return 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/routing/BasicRouteDirector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */