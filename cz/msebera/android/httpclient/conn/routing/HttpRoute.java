package cz.msebera.android.httpclient.conn.routing;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Immutable
public final class HttpRoute
  implements RouteInfo, Cloneable
{
  private final RouteInfo.LayerType layered;
  private final InetAddress localAddress;
  private final List<HttpHost> proxyChain;
  private final boolean secure;
  private final HttpHost targetHost;
  private final RouteInfo.TunnelType tunnelled;
  
  public HttpRoute(HttpHost paramHttpHost)
  {
    this(paramHttpHost, null, Collections.emptyList(), false, RouteInfo.TunnelType.PLAIN, RouteInfo.LayerType.PLAIN);
  }
  
  public HttpRoute(HttpHost paramHttpHost1, HttpHost paramHttpHost2)
  {
    this(paramHttpHost1, null, paramHttpHost2, false);
  }
  
  public HttpRoute(HttpHost paramHttpHost1, InetAddress paramInetAddress, HttpHost paramHttpHost2, boolean paramBoolean)
  {
    this(paramHttpHost1, paramInetAddress, localList, paramBoolean, paramHttpHost2, localLayerType);
  }
  
  public HttpRoute(HttpHost paramHttpHost1, InetAddress paramInetAddress, HttpHost paramHttpHost2, boolean paramBoolean, RouteInfo.TunnelType paramTunnelType, RouteInfo.LayerType paramLayerType)
  {
    this(paramHttpHost1, paramInetAddress, paramHttpHost2, paramBoolean, paramTunnelType, paramLayerType);
  }
  
  private HttpRoute(HttpHost paramHttpHost, InetAddress paramInetAddress, List<HttpHost> paramList, boolean paramBoolean, RouteInfo.TunnelType paramTunnelType, RouteInfo.LayerType paramLayerType)
  {
    Args.notNull(paramHttpHost, "Target host");
    this.targetHost = paramHttpHost;
    this.localAddress = paramInetAddress;
    if ((paramList != null) && (!paramList.isEmpty())) {
      this.proxyChain = new ArrayList(paramList);
    } else {
      this.proxyChain = null;
    }
    if (paramTunnelType == RouteInfo.TunnelType.TUNNELLED)
    {
      boolean bool;
      if (this.proxyChain != null) {
        bool = true;
      } else {
        bool = false;
      }
      Args.check(bool, "Proxy required if tunnelled");
    }
    this.secure = paramBoolean;
    if (paramTunnelType == null) {
      paramTunnelType = RouteInfo.TunnelType.PLAIN;
    }
    this.tunnelled = paramTunnelType;
    if (paramLayerType == null) {
      paramLayerType = RouteInfo.LayerType.PLAIN;
    }
    this.layered = paramLayerType;
  }
  
  public HttpRoute(HttpHost paramHttpHost, InetAddress paramInetAddress, boolean paramBoolean)
  {
    this(paramHttpHost, paramInetAddress, Collections.emptyList(), paramBoolean, RouteInfo.TunnelType.PLAIN, RouteInfo.LayerType.PLAIN);
  }
  
  public HttpRoute(HttpHost paramHttpHost, InetAddress paramInetAddress, HttpHost[] paramArrayOfHttpHost, boolean paramBoolean, RouteInfo.TunnelType paramTunnelType, RouteInfo.LayerType paramLayerType)
  {
    this(paramHttpHost, paramInetAddress, paramArrayOfHttpHost, paramBoolean, paramTunnelType, paramLayerType);
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof HttpRoute))
    {
      paramObject = (HttpRoute)paramObject;
      if ((this.secure != ((HttpRoute)paramObject).secure) || (this.tunnelled != ((HttpRoute)paramObject).tunnelled) || (this.layered != ((HttpRoute)paramObject).layered) || (!LangUtils.equals(this.targetHost, ((HttpRoute)paramObject).targetHost)) || (!LangUtils.equals(this.localAddress, ((HttpRoute)paramObject).localAddress)) || (!LangUtils.equals(this.proxyChain, ((HttpRoute)paramObject).proxyChain))) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public final int getHopCount()
  {
    List localList = this.proxyChain;
    int i = 1;
    if (localList != null) {
      i = 1 + localList.size();
    }
    return i;
  }
  
  public final HttpHost getHopTarget(int paramInt)
  {
    Args.notNegative(paramInt, "Hop index");
    int i = getHopCount();
    boolean bool;
    if (paramInt < i) {
      bool = true;
    } else {
      bool = false;
    }
    Args.check(bool, "Hop index exceeds tracked route length");
    if (paramInt < i - 1) {
      return (HttpHost)this.proxyChain.get(paramInt);
    }
    return this.targetHost;
  }
  
  public final RouteInfo.LayerType getLayerType()
  {
    return this.layered;
  }
  
  public final InetAddress getLocalAddress()
  {
    return this.localAddress;
  }
  
  public final InetSocketAddress getLocalSocketAddress()
  {
    Object localObject = this.localAddress;
    if (localObject != null) {
      localObject = new InetSocketAddress((InetAddress)localObject, 0);
    } else {
      localObject = null;
    }
    return (InetSocketAddress)localObject;
  }
  
  public final HttpHost getProxyHost()
  {
    Object localObject = this.proxyChain;
    if ((localObject != null) && (!((List)localObject).isEmpty())) {
      localObject = (HttpHost)this.proxyChain.get(0);
    } else {
      localObject = null;
    }
    return (HttpHost)localObject;
  }
  
  public final HttpHost getTargetHost()
  {
    return this.targetHost;
  }
  
  public final RouteInfo.TunnelType getTunnelType()
  {
    return this.tunnelled;
  }
  
  public final int hashCode()
  {
    int i = LangUtils.hashCode(LangUtils.hashCode(17, this.targetHost), this.localAddress);
    Object localObject = this.proxyChain;
    int j = i;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      for (;;)
      {
        j = i;
        if (!((Iterator)localObject).hasNext()) {
          break;
        }
        i = LangUtils.hashCode(i, (HttpHost)((Iterator)localObject).next());
      }
    }
    return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(j, this.secure), this.tunnelled), this.layered);
  }
  
  public final boolean isLayered()
  {
    boolean bool;
    if (this.layered == RouteInfo.LayerType.LAYERED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final boolean isSecure()
  {
    return this.secure;
  }
  
  public final boolean isTunnelled()
  {
    boolean bool;
    if (this.tunnelled == RouteInfo.TunnelType.TUNNELLED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(getHopCount() * 30 + 50);
    Object localObject = this.localAddress;
    if (localObject != null)
    {
      localStringBuilder.append(localObject);
      localStringBuilder.append("->");
    }
    localStringBuilder.append('{');
    if (this.tunnelled == RouteInfo.TunnelType.TUNNELLED) {
      localStringBuilder.append('t');
    }
    if (this.layered == RouteInfo.LayerType.LAYERED) {
      localStringBuilder.append('l');
    }
    if (this.secure) {
      localStringBuilder.append('s');
    }
    localStringBuilder.append("}->");
    localObject = this.proxyChain;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        localStringBuilder.append((HttpHost)((Iterator)localObject).next());
        localStringBuilder.append("->");
      }
    }
    localStringBuilder.append(this.targetHost);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/routing/HttpRoute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */