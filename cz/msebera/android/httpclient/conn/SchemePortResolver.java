package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.HttpHost;

public abstract interface SchemePortResolver
{
  public abstract int resolve(HttpHost paramHttpHost)
    throws UnsupportedSchemeException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/SchemePortResolver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */