package cz.msebera.android.httpclient.conn.socket;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

@Immutable
public class PlainConnectionSocketFactory
  implements ConnectionSocketFactory
{
  public static final PlainConnectionSocketFactory INSTANCE = new PlainConnectionSocketFactory();
  
  public static PlainConnectionSocketFactory getSocketFactory()
  {
    return INSTANCE;
  }
  
  public Socket connectSocket(int paramInt, Socket paramSocket, HttpHost paramHttpHost, InetSocketAddress paramInetSocketAddress1, InetSocketAddress paramInetSocketAddress2, HttpContext paramHttpContext)
    throws IOException
  {
    if (paramSocket == null) {
      paramSocket = createSocket(paramHttpContext);
    }
    if (paramInetSocketAddress2 != null) {
      paramSocket.bind(paramInetSocketAddress2);
    }
    try
    {
      paramSocket.connect(paramInetSocketAddress1, paramInt);
      return paramSocket;
    }
    catch (IOException paramHttpHost) {}
    try
    {
      paramSocket.close();
      throw paramHttpHost;
    }
    catch (IOException paramSocket)
    {
      for (;;) {}
    }
  }
  
  public Socket createSocket(HttpContext paramHttpContext)
    throws IOException
  {
    return new Socket();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/socket/PlainConnectionSocketFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */