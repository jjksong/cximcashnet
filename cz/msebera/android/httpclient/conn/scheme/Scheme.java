package cz.msebera.android.httpclient.conn.scheme;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.util.Locale;

@Deprecated
@Immutable
public final class Scheme
{
  private final int defaultPort;
  private final boolean layered;
  private final String name;
  private final SchemeSocketFactory socketFactory;
  private String stringRep;
  
  public Scheme(String paramString, int paramInt, SchemeSocketFactory paramSchemeSocketFactory)
  {
    Args.notNull(paramString, "Scheme name");
    boolean bool;
    if ((paramInt > 0) && (paramInt <= 65535)) {
      bool = true;
    } else {
      bool = false;
    }
    Args.check(bool, "Port is invalid");
    Args.notNull(paramSchemeSocketFactory, "Socket factory");
    this.name = paramString.toLowerCase(Locale.ENGLISH);
    this.defaultPort = paramInt;
    if ((paramSchemeSocketFactory instanceof SchemeLayeredSocketFactory))
    {
      this.layered = true;
      this.socketFactory = paramSchemeSocketFactory;
    }
    else if ((paramSchemeSocketFactory instanceof LayeredSchemeSocketFactory))
    {
      this.layered = true;
      this.socketFactory = new SchemeLayeredSocketFactoryAdaptor2((LayeredSchemeSocketFactory)paramSchemeSocketFactory);
    }
    else
    {
      this.layered = false;
      this.socketFactory = paramSchemeSocketFactory;
    }
  }
  
  @Deprecated
  public Scheme(String paramString, SocketFactory paramSocketFactory, int paramInt)
  {
    Args.notNull(paramString, "Scheme name");
    Args.notNull(paramSocketFactory, "Socket factory");
    boolean bool;
    if ((paramInt > 0) && (paramInt <= 65535)) {
      bool = true;
    } else {
      bool = false;
    }
    Args.check(bool, "Port is invalid");
    this.name = paramString.toLowerCase(Locale.ENGLISH);
    if ((paramSocketFactory instanceof LayeredSocketFactory))
    {
      this.socketFactory = new SchemeLayeredSocketFactoryAdaptor((LayeredSocketFactory)paramSocketFactory);
      this.layered = true;
    }
    else
    {
      this.socketFactory = new SchemeSocketFactoryAdaptor(paramSocketFactory);
      this.layered = false;
    }
    this.defaultPort = paramInt;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof Scheme))
    {
      paramObject = (Scheme)paramObject;
      if ((!this.name.equals(((Scheme)paramObject).name)) || (this.defaultPort != ((Scheme)paramObject).defaultPort) || (this.layered != ((Scheme)paramObject).layered)) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public final int getDefaultPort()
  {
    return this.defaultPort;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final SchemeSocketFactory getSchemeSocketFactory()
  {
    return this.socketFactory;
  }
  
  @Deprecated
  public final SocketFactory getSocketFactory()
  {
    SchemeSocketFactory localSchemeSocketFactory = this.socketFactory;
    if ((localSchemeSocketFactory instanceof SchemeSocketFactoryAdaptor)) {
      return ((SchemeSocketFactoryAdaptor)localSchemeSocketFactory).getFactory();
    }
    if (this.layered) {
      return new LayeredSocketFactoryAdaptor((LayeredSchemeSocketFactory)localSchemeSocketFactory);
    }
    return new SocketFactoryAdaptor(localSchemeSocketFactory);
  }
  
  public int hashCode()
  {
    return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, this.defaultPort), this.name), this.layered);
  }
  
  public final boolean isLayered()
  {
    return this.layered;
  }
  
  public final int resolvePort(int paramInt)
  {
    int i = paramInt;
    if (paramInt <= 0) {
      i = this.defaultPort;
    }
    return i;
  }
  
  public final String toString()
  {
    if (this.stringRep == null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(this.name);
      localStringBuilder.append(':');
      localStringBuilder.append(Integer.toString(this.defaultPort));
      this.stringRep = localStringBuilder.toString();
    }
    return this.stringRep;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/scheme/Scheme.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */