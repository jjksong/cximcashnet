package cz.msebera.android.httpclient.conn.scheme;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
@ThreadSafe
public final class SchemeRegistry
{
  private final ConcurrentHashMap<String, Scheme> registeredSchemes = new ConcurrentHashMap();
  
  public final Scheme get(String paramString)
  {
    Args.notNull(paramString, "Scheme name");
    return (Scheme)this.registeredSchemes.get(paramString);
  }
  
  public final Scheme getScheme(HttpHost paramHttpHost)
  {
    Args.notNull(paramHttpHost, "Host");
    return getScheme(paramHttpHost.getSchemeName());
  }
  
  public final Scheme getScheme(String paramString)
  {
    Object localObject = get(paramString);
    if (localObject != null) {
      return (Scheme)localObject;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Scheme '");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("' not registered.");
    throw new IllegalStateException(((StringBuilder)localObject).toString());
  }
  
  public final List<String> getSchemeNames()
  {
    return new ArrayList(this.registeredSchemes.keySet());
  }
  
  public final Scheme register(Scheme paramScheme)
  {
    Args.notNull(paramScheme, "Scheme");
    return (Scheme)this.registeredSchemes.put(paramScheme.getName(), paramScheme);
  }
  
  public void setItems(Map<String, Scheme> paramMap)
  {
    if (paramMap == null) {
      return;
    }
    this.registeredSchemes.clear();
    this.registeredSchemes.putAll(paramMap);
  }
  
  public final Scheme unregister(String paramString)
  {
    Args.notNull(paramString, "Scheme name");
    return (Scheme)this.registeredSchemes.remove(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/scheme/SchemeRegistry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */