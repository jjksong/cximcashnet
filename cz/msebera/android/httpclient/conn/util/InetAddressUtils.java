package cz.msebera.android.httpclient.conn.util;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Immutable
public class InetAddressUtils
{
  private static final char COLON_CHAR = ':';
  private static final String IPV4_BASIC_PATTERN_STRING = "(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])";
  private static final Pattern IPV4_MAPPED_IPV6_PATTERN;
  private static final Pattern IPV4_PATTERN = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
  private static final Pattern IPV6_HEX_COMPRESSED_PATTERN = Pattern.compile("^(([0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,5})?)::(([0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,5})?)$");
  private static final Pattern IPV6_STD_PATTERN;
  private static final int MAX_COLON_COUNT = 7;
  
  static
  {
    IPV4_MAPPED_IPV6_PATTERN = Pattern.compile("^::[fF]{4}:(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
    IPV6_STD_PATTERN = Pattern.compile("^[0-9a-fA-F]{1,4}(:[0-9a-fA-F]{1,4}){7}$");
  }
  
  public static boolean isIPv4Address(String paramString)
  {
    return IPV4_PATTERN.matcher(paramString).matches();
  }
  
  public static boolean isIPv4MappedIPv64Address(String paramString)
  {
    return IPV4_MAPPED_IPV6_PATTERN.matcher(paramString).matches();
  }
  
  public static boolean isIPv6Address(String paramString)
  {
    boolean bool;
    if ((!isIPv6StdAddress(paramString)) && (!isIPv6HexCompressedAddress(paramString))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public static boolean isIPv6HexCompressedAddress(String paramString)
  {
    boolean bool2 = false;
    int k = 0;
    int i;
    for (int j = 0; k < paramString.length(); j = i)
    {
      i = j;
      if (paramString.charAt(k) == ':') {
        i = j + 1;
      }
      k++;
    }
    boolean bool1 = bool2;
    if (j <= 7)
    {
      bool1 = bool2;
      if (IPV6_HEX_COMPRESSED_PATTERN.matcher(paramString).matches()) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public static boolean isIPv6StdAddress(String paramString)
  {
    return IPV6_STD_PATTERN.matcher(paramString).matches();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/util/InetAddressUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */