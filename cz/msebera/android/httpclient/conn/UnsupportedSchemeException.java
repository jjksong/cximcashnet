package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.io.IOException;

@Immutable
public class UnsupportedSchemeException
  extends IOException
{
  private static final long serialVersionUID = 3597127619218687636L;
  
  public UnsupportedSchemeException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/UnsupportedSchemeException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */