package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.scheme.SocketFactory;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Deprecated
@Immutable
public final class MultihomePlainSocketFactory
  implements SocketFactory
{
  private static final MultihomePlainSocketFactory DEFAULT_FACTORY = new MultihomePlainSocketFactory();
  
  public static MultihomePlainSocketFactory getSocketFactory()
  {
    return DEFAULT_FACTORY;
  }
  
  public Socket connectSocket(Socket paramSocket, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramString, "Target host");
    Args.notNull(paramHttpParams, "HTTP parameters");
    Socket localSocket = paramSocket;
    if (paramSocket == null) {
      localSocket = createSocket();
    }
    if ((paramInetAddress != null) || (paramInt2 > 0))
    {
      if (paramInt2 <= 0) {
        paramInt2 = 0;
      }
      localSocket.bind(new InetSocketAddress(paramInetAddress, paramInt2));
    }
    paramInt2 = HttpConnectionParams.getConnectionTimeout(paramHttpParams);
    paramSocket = InetAddress.getAllByName(paramString);
    paramString = new ArrayList(paramSocket.length);
    paramString.addAll(Arrays.asList(paramSocket));
    Collections.shuffle(paramString);
    paramSocket = null;
    paramString = paramString.iterator();
    while (paramString.hasNext())
    {
      paramInetAddress = (InetAddress)paramString.next();
      try
      {
        paramHttpParams = new java/net/InetSocketAddress;
        paramHttpParams.<init>(paramInetAddress, paramInt1);
        localSocket.connect(paramHttpParams, paramInt2);
      }
      catch (IOException paramSocket)
      {
        localSocket = new Socket();
      }
      catch (SocketTimeoutException paramSocket)
      {
        paramSocket = new StringBuilder();
        paramSocket.append("Connect to ");
        paramSocket.append(paramInetAddress);
        paramSocket.append(" timed out");
        throw new ConnectTimeoutException(paramSocket.toString());
      }
    }
    if (paramSocket == null) {
      return localSocket;
    }
    throw paramSocket;
  }
  
  public Socket createSocket()
  {
    return new Socket();
  }
  
  public final boolean isSecure(Socket paramSocket)
    throws IllegalArgumentException
  {
    Args.notNull(paramSocket, "Socket");
    Asserts.check(paramSocket.isClosed() ^ true, "Socket is closed");
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/MultihomePlainSocketFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */