package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class ConnectionPoolTimeoutException
  extends ConnectTimeoutException
{
  private static final long serialVersionUID = -7898874842020245128L;
  
  public ConnectionPoolTimeoutException() {}
  
  public ConnectionPoolTimeoutException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ConnectionPoolTimeoutException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */