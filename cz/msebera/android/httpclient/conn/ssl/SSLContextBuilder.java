package cz.msebera.android.httpclient.conn.ssl;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

@NotThreadSafe
public class SSLContextBuilder
{
  static final String SSL = "SSL";
  static final String TLS = "TLS";
  private Set<KeyManager> keymanagers = new HashSet();
  private String protocol;
  private SecureRandom secureRandom;
  private Set<TrustManager> trustmanagers = new HashSet();
  
  public SSLContext build()
    throws NoSuchAlgorithmException, KeyManagementException
  {
    Object localObject1 = this.protocol;
    if (localObject1 == null) {
      localObject1 = "TLS";
    }
    SSLContext localSSLContext = SSLContext.getInstance((String)localObject1);
    boolean bool = this.keymanagers.isEmpty();
    Object localObject2 = null;
    if (!bool)
    {
      localObject1 = this.keymanagers;
      localObject1 = (KeyManager[])((Set)localObject1).toArray(new KeyManager[((Set)localObject1).size()]);
    }
    else
    {
      localObject1 = null;
    }
    if (!this.trustmanagers.isEmpty())
    {
      localObject2 = this.trustmanagers;
      localObject2 = (TrustManager[])((Set)localObject2).toArray(new TrustManager[((Set)localObject2).size()]);
    }
    localSSLContext.init((KeyManager[])localObject1, (TrustManager[])localObject2, this.secureRandom);
    return localSSLContext;
  }
  
  public SSLContextBuilder loadKeyMaterial(KeyStore paramKeyStore, char[] paramArrayOfChar)
    throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException
  {
    loadKeyMaterial(paramKeyStore, paramArrayOfChar, null);
    return this;
  }
  
  public SSLContextBuilder loadKeyMaterial(KeyStore paramKeyStore, char[] paramArrayOfChar, PrivateKeyStrategy paramPrivateKeyStrategy)
    throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException
  {
    KeyManagerFactory localKeyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    localKeyManagerFactory.init(paramKeyStore, paramArrayOfChar);
    paramKeyStore = localKeyManagerFactory.getKeyManagers();
    if (paramKeyStore != null)
    {
      int j = 0;
      if (paramPrivateKeyStrategy != null) {
        for (i = 0; i < paramKeyStore.length; i++)
        {
          paramArrayOfChar = paramKeyStore[i];
          if ((paramArrayOfChar instanceof X509KeyManager)) {
            paramKeyStore[i] = new KeyManagerDelegate((X509KeyManager)paramArrayOfChar, paramPrivateKeyStrategy);
          }
        }
      }
      int k = paramKeyStore.length;
      for (int i = j; i < k; i++)
      {
        paramArrayOfChar = paramKeyStore[i];
        this.keymanagers.add(paramArrayOfChar);
      }
    }
    return this;
  }
  
  public SSLContextBuilder loadTrustMaterial(KeyStore paramKeyStore)
    throws NoSuchAlgorithmException, KeyStoreException
  {
    return loadTrustMaterial(paramKeyStore, null);
  }
  
  public SSLContextBuilder loadTrustMaterial(KeyStore paramKeyStore, TrustStrategy paramTrustStrategy)
    throws NoSuchAlgorithmException, KeyStoreException
  {
    TrustManagerFactory localTrustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    localTrustManagerFactory.init(paramKeyStore);
    paramKeyStore = localTrustManagerFactory.getTrustManagers();
    if (paramKeyStore != null)
    {
      int j = 0;
      if (paramTrustStrategy != null) {
        for (i = 0; i < paramKeyStore.length; i++)
        {
          localTrustManagerFactory = paramKeyStore[i];
          if ((localTrustManagerFactory instanceof X509TrustManager)) {
            paramKeyStore[i] = new TrustManagerDelegate((X509TrustManager)localTrustManagerFactory, paramTrustStrategy);
          }
        }
      }
      int k = paramKeyStore.length;
      for (int i = j; i < k; i++)
      {
        paramTrustStrategy = paramKeyStore[i];
        this.trustmanagers.add(paramTrustStrategy);
      }
    }
    return this;
  }
  
  public SSLContextBuilder setSecureRandom(SecureRandom paramSecureRandom)
  {
    this.secureRandom = paramSecureRandom;
    return this;
  }
  
  public SSLContextBuilder useProtocol(String paramString)
  {
    this.protocol = paramString;
    return this;
  }
  
  public SSLContextBuilder useSSL()
  {
    this.protocol = "SSL";
    return this;
  }
  
  public SSLContextBuilder useTLS()
  {
    this.protocol = "TLS";
    return this;
  }
  
  static class KeyManagerDelegate
    implements X509KeyManager
  {
    private final PrivateKeyStrategy aliasStrategy;
    private final X509KeyManager keyManager;
    
    KeyManagerDelegate(X509KeyManager paramX509KeyManager, PrivateKeyStrategy paramPrivateKeyStrategy)
    {
      this.keyManager = paramX509KeyManager;
      this.aliasStrategy = paramPrivateKeyStrategy;
    }
    
    public String chooseClientAlias(String[] paramArrayOfString, Principal[] paramArrayOfPrincipal, Socket paramSocket)
    {
      HashMap localHashMap = new HashMap();
      int k = paramArrayOfString.length;
      for (int i = 0; i < k; i++)
      {
        String str2 = paramArrayOfString[i];
        String[] arrayOfString = this.keyManager.getClientAliases(str2, paramArrayOfPrincipal);
        if (arrayOfString != null)
        {
          int m = arrayOfString.length;
          for (int j = 0; j < m; j++)
          {
            String str1 = arrayOfString[j];
            localHashMap.put(str1, new PrivateKeyDetails(str2, this.keyManager.getCertificateChain(str1)));
          }
        }
      }
      return this.aliasStrategy.chooseAlias(localHashMap, paramSocket);
    }
    
    public String chooseServerAlias(String paramString, Principal[] paramArrayOfPrincipal, Socket paramSocket)
    {
      HashMap localHashMap = new HashMap();
      paramArrayOfPrincipal = this.keyManager.getServerAliases(paramString, paramArrayOfPrincipal);
      if (paramArrayOfPrincipal != null)
      {
        int j = paramArrayOfPrincipal.length;
        for (int i = 0; i < j; i++)
        {
          Principal localPrincipal = paramArrayOfPrincipal[i];
          localHashMap.put(localPrincipal, new PrivateKeyDetails(paramString, this.keyManager.getCertificateChain(localPrincipal)));
        }
      }
      return this.aliasStrategy.chooseAlias(localHashMap, paramSocket);
    }
    
    public X509Certificate[] getCertificateChain(String paramString)
    {
      return this.keyManager.getCertificateChain(paramString);
    }
    
    public String[] getClientAliases(String paramString, Principal[] paramArrayOfPrincipal)
    {
      return this.keyManager.getClientAliases(paramString, paramArrayOfPrincipal);
    }
    
    public PrivateKey getPrivateKey(String paramString)
    {
      return this.keyManager.getPrivateKey(paramString);
    }
    
    public String[] getServerAliases(String paramString, Principal[] paramArrayOfPrincipal)
    {
      return this.keyManager.getServerAliases(paramString, paramArrayOfPrincipal);
    }
  }
  
  static class TrustManagerDelegate
    implements X509TrustManager
  {
    private final X509TrustManager trustManager;
    private final TrustStrategy trustStrategy;
    
    TrustManagerDelegate(X509TrustManager paramX509TrustManager, TrustStrategy paramTrustStrategy)
    {
      this.trustManager = paramX509TrustManager;
      this.trustStrategy = paramTrustStrategy;
    }
    
    public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
      throws CertificateException
    {
      this.trustManager.checkClientTrusted(paramArrayOfX509Certificate, paramString);
    }
    
    public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
      throws CertificateException
    {
      if (!this.trustStrategy.isTrusted(paramArrayOfX509Certificate, paramString)) {
        this.trustManager.checkServerTrusted(paramArrayOfX509Certificate, paramString);
      }
    }
    
    public X509Certificate[] getAcceptedIssuers()
    {
      return this.trustManager.getAcceptedIssuers();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ssl/SSLContextBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */