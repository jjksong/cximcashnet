package cz.msebera.android.httpclient.conn.ssl;

public class SSLInitializationException
  extends IllegalStateException
{
  private static final long serialVersionUID = -8243587425648536702L;
  
  public SSLInitializationException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ssl/SSLInitializationException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */