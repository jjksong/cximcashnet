package cz.msebera.android.httpclient.conn.ssl;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;

@Immutable
public class SSLContexts
{
  public static SSLContext createDefault()
    throws SSLInitializationException
  {
    try
    {
      SSLContext localSSLContext = SSLContext.getInstance("TLS");
      localSSLContext.init(null, null, null);
      return localSSLContext;
    }
    catch (KeyManagementException localKeyManagementException)
    {
      throw new SSLInitializationException(localKeyManagementException.getMessage(), localKeyManagementException);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new SSLInitializationException(localNoSuchAlgorithmException.getMessage(), localNoSuchAlgorithmException);
    }
  }
  
  public static SSLContext createSystemDefault()
    throws SSLInitializationException
  {
    try
    {
      SSLContext localSSLContext = SSLContext.getInstance("Default");
      return localSSLContext;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}
    return createDefault();
  }
  
  public static SSLContextBuilder custom()
  {
    return new SSLContextBuilder();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ssl/SSLContexts.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */