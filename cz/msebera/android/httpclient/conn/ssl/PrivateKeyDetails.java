package cz.msebera.android.httpclient.conn.ssl;

import cz.msebera.android.httpclient.util.Args;
import java.security.cert.X509Certificate;
import java.util.Arrays;

public final class PrivateKeyDetails
{
  private final X509Certificate[] certChain;
  private final String type;
  
  public PrivateKeyDetails(String paramString, X509Certificate[] paramArrayOfX509Certificate)
  {
    this.type = ((String)Args.notNull(paramString, "Private key type"));
    this.certChain = paramArrayOfX509Certificate;
  }
  
  public X509Certificate[] getCertChain()
  {
    return this.certChain;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.type);
    localStringBuilder.append(':');
    localStringBuilder.append(Arrays.toString(this.certChain));
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ssl/PrivateKeyDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */