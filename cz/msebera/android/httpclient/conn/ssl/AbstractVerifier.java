package cz.msebera.android.httpclient.conn.ssl;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.util.InetAddressUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.security.auth.x500.X500Principal;

@Immutable
public abstract class AbstractVerifier
  implements X509HostnameVerifier
{
  private static final String[] BAD_COUNTRY_2LDS = { "ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info", "lg", "ne", "net", "or", "org" };
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  static
  {
    Arrays.sort(BAD_COUNTRY_2LDS);
  }
  
  @Deprecated
  public static boolean acceptableCountryWildcard(String paramString)
  {
    paramString = paramString.split("\\.");
    int i = paramString.length;
    boolean bool = true;
    if ((i == 3) && (paramString[2].length() == 2))
    {
      if (Arrays.binarySearch(BAD_COUNTRY_2LDS, paramString[1]) >= 0) {
        bool = false;
      }
      return bool;
    }
    return true;
  }
  
  public static int countDots(String paramString)
  {
    int j = 0;
    int k;
    for (int i = 0; j < paramString.length(); i = k)
    {
      k = i;
      if (paramString.charAt(j) == '.') {
        k = i + 1;
      }
      j++;
    }
    return i;
  }
  
  static String[] extractCNs(String paramString)
    throws SSLException
  {
    if (paramString == null) {
      return null;
    }
    LinkedList localLinkedList = new LinkedList();
    paramString = new StringTokenizer(paramString, ",");
    while (paramString.hasMoreTokens())
    {
      String str = paramString.nextToken();
      int i = str.indexOf("CN=");
      if (i >= 0) {
        localLinkedList.add(str.substring(i + 3));
      }
    }
    if (!localLinkedList.isEmpty())
    {
      paramString = new String[localLinkedList.size()];
      localLinkedList.toArray(paramString);
      return paramString;
    }
    return null;
  }
  
  public static String[] getCNs(X509Certificate paramX509Certificate)
  {
    paramX509Certificate = paramX509Certificate.getSubjectX500Principal().toString();
    try
    {
      paramX509Certificate = extractCNs(paramX509Certificate);
      return paramX509Certificate;
    }
    catch (SSLException paramX509Certificate) {}
    return null;
  }
  
  public static String[] getDNSSubjectAlts(X509Certificate paramX509Certificate)
  {
    return getSubjectAlts(paramX509Certificate, null);
  }
  
  private static String[] getSubjectAlts(X509Certificate paramX509Certificate, String paramString)
  {
    int i;
    if (isIPAddress(paramString)) {
      i = 7;
    } else {
      i = 2;
    }
    paramString = new LinkedList();
    try
    {
      paramX509Certificate = paramX509Certificate.getSubjectAlternativeNames();
    }
    catch (CertificateParsingException paramX509Certificate)
    {
      paramX509Certificate = null;
    }
    if (paramX509Certificate != null)
    {
      paramX509Certificate = paramX509Certificate.iterator();
      while (paramX509Certificate.hasNext())
      {
        List localList = (List)paramX509Certificate.next();
        if (((Integer)localList.get(0)).intValue() == i) {
          paramString.add((String)localList.get(1));
        }
      }
    }
    if (!paramString.isEmpty())
    {
      paramX509Certificate = new String[paramString.size()];
      paramString.toArray(paramX509Certificate);
      return paramX509Certificate;
    }
    return null;
  }
  
  private static boolean isIPAddress(String paramString)
  {
    boolean bool;
    if ((paramString != null) && ((InetAddressUtils.isIPv4Address(paramString)) || (InetAddressUtils.isIPv6Address(paramString)))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private String normaliseIPv6Address(String paramString)
  {
    if ((paramString != null) && (InetAddressUtils.isIPv6Address(paramString))) {
      try
      {
        localObject = InetAddress.getByName(paramString).getHostAddress();
        return (String)localObject;
      }
      catch (UnknownHostException localUnknownHostException)
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        Object localObject = new StringBuilder();
        ((StringBuilder)localObject).append("Unexpected error converting ");
        ((StringBuilder)localObject).append(paramString);
        localHttpClientAndroidLog.error(((StringBuilder)localObject).toString(), localUnknownHostException);
        return paramString;
      }
    }
    return paramString;
  }
  
  boolean validCountryWildcard(String paramString)
  {
    paramString = paramString.split("\\.");
    int i = paramString.length;
    boolean bool = true;
    if ((i == 3) && (paramString[2].length() == 2))
    {
      if (Arrays.binarySearch(BAD_COUNTRY_2LDS, paramString[1]) >= 0) {
        bool = false;
      }
      return bool;
    }
    return true;
  }
  
  public final void verify(String paramString, X509Certificate paramX509Certificate)
    throws SSLException
  {
    verify(paramString, getCNs(paramX509Certificate), getSubjectAlts(paramX509Certificate, paramString));
  }
  
  public final void verify(String paramString, SSLSocket paramSSLSocket)
    throws IOException
  {
    if (paramString != null)
    {
      SSLSession localSSLSession2 = paramSSLSocket.getSession();
      SSLSession localSSLSession1 = localSSLSession2;
      if (localSSLSession2 == null)
      {
        paramSSLSocket.getInputStream().available();
        localSSLSession2 = paramSSLSocket.getSession();
        localSSLSession1 = localSSLSession2;
        if (localSSLSession2 == null)
        {
          paramSSLSocket.startHandshake();
          localSSLSession1 = paramSSLSocket.getSession();
        }
      }
      verify(paramString, (X509Certificate)localSSLSession1.getPeerCertificates()[0]);
      return;
    }
    throw new NullPointerException("host to verify is null");
  }
  
  public final void verify(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean)
    throws SSLException
  {
    Object localObject1 = new LinkedList();
    if ((paramArrayOfString1 != null) && (paramArrayOfString1.length > 0) && (paramArrayOfString1[0] != null)) {
      ((LinkedList)localObject1).add(paramArrayOfString1[0]);
    }
    int i;
    if (paramArrayOfString2 != null)
    {
      int j = paramArrayOfString2.length;
      for (i = 0; i < j; i++)
      {
        paramArrayOfString1 = paramArrayOfString2[i];
        if (paramArrayOfString1 != null) {
          ((LinkedList)localObject1).add(paramArrayOfString1);
        }
      }
    }
    if (!((LinkedList)localObject1).isEmpty())
    {
      paramArrayOfString1 = new StringBuilder();
      paramArrayOfString2 = normaliseIPv6Address(paramString.trim().toLowerCase(Locale.ENGLISH));
      localObject1 = ((LinkedList)localObject1).iterator();
      boolean bool2 = false;
      boolean bool1;
      do
      {
        bool1 = bool2;
        if (!((Iterator)localObject1).hasNext()) {
          break;
        }
        String str1 = ((String)((Iterator)localObject1).next()).toLowerCase(Locale.ENGLISH);
        paramArrayOfString1.append(" <");
        paramArrayOfString1.append(str1);
        paramArrayOfString1.append('>');
        if (((Iterator)localObject1).hasNext()) {
          paramArrayOfString1.append(" OR");
        }
        Object localObject2 = str1.split("\\.");
        if ((localObject2.length >= 3) && (localObject2[0].endsWith("*")) && (validCountryWildcard(str1)) && (!isIPAddress(paramString))) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          String str2 = localObject2[0];
          if (str2.length() > 1)
          {
            localObject2 = str2.substring(0, str2.length() - 1);
            str2 = str1.substring(str2.length());
            String str3 = paramArrayOfString2.substring(((String)localObject2).length());
            if ((paramArrayOfString2.startsWith((String)localObject2)) && (str3.endsWith(str2))) {
              bool2 = true;
            } else {
              bool2 = false;
            }
          }
          else
          {
            bool2 = paramArrayOfString2.endsWith(str1.substring(1));
          }
          bool1 = bool2;
          if (bool2)
          {
            bool1 = bool2;
            if (paramBoolean) {
              if (countDots(paramArrayOfString2) == countDots(str1)) {
                bool1 = true;
              } else {
                bool1 = false;
              }
            }
          }
        }
        else
        {
          bool1 = paramArrayOfString2.equals(normaliseIPv6Address(str1));
        }
        bool2 = bool1;
      } while (!bool1);
      if (bool1) {
        return;
      }
      paramArrayOfString2 = new StringBuilder();
      paramArrayOfString2.append("hostname in certificate didn't match: <");
      paramArrayOfString2.append(paramString);
      paramArrayOfString2.append("> !=");
      paramArrayOfString2.append(paramArrayOfString1);
      throw new SSLException(paramArrayOfString2.toString());
    }
    paramArrayOfString1 = new StringBuilder();
    paramArrayOfString1.append("Certificate for <");
    paramArrayOfString1.append(paramString);
    paramArrayOfString1.append("> doesn't contain CN or DNS subjectAlt");
    throw new SSLException(paramArrayOfString1.toString());
  }
  
  public final boolean verify(String paramString, SSLSession paramSSLSession)
  {
    try
    {
      verify(paramString, (X509Certificate)paramSSLSession.getPeerCertificates()[0]);
      return true;
    }
    catch (SSLException paramString) {}
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ssl/AbstractVerifier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */