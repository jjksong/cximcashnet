package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
public class EofSensorInputStream
  extends InputStream
  implements ConnectionReleaseTrigger
{
  private final EofSensorWatcher eofWatcher;
  private boolean selfClosed;
  protected InputStream wrappedStream;
  
  public EofSensorInputStream(InputStream paramInputStream, EofSensorWatcher paramEofSensorWatcher)
  {
    Args.notNull(paramInputStream, "Wrapped stream");
    this.wrappedStream = paramInputStream;
    this.selfClosed = false;
    this.eofWatcher = paramEofSensorWatcher;
  }
  
  public void abortConnection()
    throws IOException
  {
    this.selfClosed = true;
    checkAbort();
  }
  
  public int available()
    throws IOException
  {
    int i;
    if (isReadAllowed()) {
      try
      {
        i = this.wrappedStream.available();
      }
      catch (IOException localIOException)
      {
        checkAbort();
        throw localIOException;
      }
    } else {
      i = 0;
    }
    return i;
  }
  
  protected void checkAbort()
    throws IOException
  {
    InputStream localInputStream = this.wrappedStream;
    if (localInputStream != null)
    {
      boolean bool = true;
      try
      {
        if (this.eofWatcher != null) {
          bool = this.eofWatcher.streamAbort(localInputStream);
        }
        if (bool) {
          this.wrappedStream.close();
        }
      }
      finally
      {
        this.wrappedStream = null;
      }
    }
  }
  
  protected void checkClose()
    throws IOException
  {
    InputStream localInputStream = this.wrappedStream;
    if (localInputStream != null)
    {
      boolean bool = true;
      try
      {
        if (this.eofWatcher != null) {
          bool = this.eofWatcher.streamClosed(localInputStream);
        }
        if (bool) {
          this.wrappedStream.close();
        }
      }
      finally
      {
        this.wrappedStream = null;
      }
    }
  }
  
  protected void checkEOF(int paramInt)
    throws IOException
  {
    InputStream localInputStream = this.wrappedStream;
    if ((localInputStream != null) && (paramInt < 0))
    {
      boolean bool = true;
      try
      {
        if (this.eofWatcher != null) {
          bool = this.eofWatcher.eofDetected(localInputStream);
        }
        if (bool) {
          this.wrappedStream.close();
        }
      }
      finally
      {
        this.wrappedStream = null;
      }
    }
  }
  
  public void close()
    throws IOException
  {
    this.selfClosed = true;
    checkClose();
  }
  
  InputStream getWrappedStream()
  {
    return this.wrappedStream;
  }
  
  protected boolean isReadAllowed()
    throws IOException
  {
    if (!this.selfClosed)
    {
      boolean bool;
      if (this.wrappedStream != null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    throw new IOException("Attempted read on closed stream.");
  }
  
  boolean isSelfClosed()
  {
    return this.selfClosed;
  }
  
  public int read()
    throws IOException
  {
    int i;
    if (isReadAllowed()) {
      try
      {
        i = this.wrappedStream.read();
        checkEOF(i);
      }
      catch (IOException localIOException)
      {
        checkAbort();
        throw localIOException;
      }
    } else {
      i = -1;
    }
    return i;
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (isReadAllowed()) {
      try
      {
        paramInt1 = this.wrappedStream.read(paramArrayOfByte, paramInt1, paramInt2);
        checkEOF(paramInt1);
      }
      catch (IOException paramArrayOfByte)
      {
        checkAbort();
        throw paramArrayOfByte;
      }
    } else {
      paramInt1 = -1;
    }
    return paramInt1;
  }
  
  public void releaseConnection()
    throws IOException
  {
    close();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/EofSensorInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */