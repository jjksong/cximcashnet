package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetAddress;

@Immutable
public class ConnectTimeoutException
  extends InterruptedIOException
{
  private static final long serialVersionUID = -4816682903149535989L;
  private final HttpHost host;
  
  public ConnectTimeoutException()
  {
    this.host = null;
  }
  
  public ConnectTimeoutException(IOException paramIOException, HttpHost paramHttpHost, InetAddress... paramVarArgs)
  {
    super(localStringBuilder.toString());
    this.host = paramHttpHost;
    initCause(paramIOException);
  }
  
  public ConnectTimeoutException(String paramString)
  {
    super(paramString);
    this.host = null;
  }
  
  public HttpHost getHost()
  {
    return this.host;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/conn/ConnectTimeoutException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */