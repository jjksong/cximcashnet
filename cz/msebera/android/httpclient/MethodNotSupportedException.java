package cz.msebera.android.httpclient;

public class MethodNotSupportedException
  extends HttpException
{
  private static final long serialVersionUID = 3365359036840171201L;
  
  public MethodNotSupportedException(String paramString)
  {
    super(paramString);
  }
  
  public MethodNotSupportedException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/MethodNotSupportedException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */