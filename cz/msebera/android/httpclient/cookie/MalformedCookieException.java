package cz.msebera.android.httpclient.cookie;

import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class MalformedCookieException
  extends ProtocolException
{
  private static final long serialVersionUID = -6695462944287282185L;
  
  public MalformedCookieException() {}
  
  public MalformedCookieException(String paramString)
  {
    super(paramString);
  }
  
  public MalformedCookieException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/MalformedCookieException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */