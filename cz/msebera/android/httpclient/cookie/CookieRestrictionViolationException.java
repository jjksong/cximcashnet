package cz.msebera.android.httpclient.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class CookieRestrictionViolationException
  extends MalformedCookieException
{
  private static final long serialVersionUID = 7371235577078589013L;
  
  public CookieRestrictionViolationException() {}
  
  public CookieRestrictionViolationException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/CookieRestrictionViolationException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */