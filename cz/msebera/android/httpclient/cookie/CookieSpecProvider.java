package cz.msebera.android.httpclient.cookie;

import cz.msebera.android.httpclient.protocol.HttpContext;

public abstract interface CookieSpecProvider
{
  public abstract CookieSpec create(HttpContext paramHttpContext);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/CookieSpecProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */