package cz.msebera.android.httpclient.cookie;

public abstract interface SetCookie2
  extends SetCookie
{
  public abstract void setCommentURL(String paramString);
  
  public abstract void setDiscard(boolean paramBoolean);
  
  public abstract void setPorts(int[] paramArrayOfInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/SetCookie2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */