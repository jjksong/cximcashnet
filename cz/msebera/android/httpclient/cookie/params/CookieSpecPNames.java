package cz.msebera.android.httpclient.cookie.params;

@Deprecated
public abstract interface CookieSpecPNames
{
  public static final String DATE_PATTERNS = "http.protocol.cookie-datepatterns";
  public static final String SINGLE_COOKIE_HEADER = "http.protocol.single-cookie-header";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/params/CookieSpecPNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */