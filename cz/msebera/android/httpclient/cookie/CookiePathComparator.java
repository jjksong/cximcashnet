package cz.msebera.android.httpclient.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.io.Serializable;
import java.util.Comparator;

@Immutable
public class CookiePathComparator
  implements Serializable, Comparator<Cookie>
{
  private static final long serialVersionUID = 7523645369616405818L;
  
  private String normalizePath(Cookie paramCookie)
  {
    Object localObject = paramCookie.getPath();
    paramCookie = (Cookie)localObject;
    if (localObject == null) {
      paramCookie = "/";
    }
    localObject = paramCookie;
    if (!paramCookie.endsWith("/"))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramCookie);
      ((StringBuilder)localObject).append('/');
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  public int compare(Cookie paramCookie1, Cookie paramCookie2)
  {
    paramCookie1 = normalizePath(paramCookie1);
    paramCookie2 = normalizePath(paramCookie2);
    if (paramCookie1.equals(paramCookie2)) {
      return 0;
    }
    if (paramCookie1.startsWith(paramCookie2)) {
      return -1;
    }
    if (paramCookie2.startsWith(paramCookie1)) {
      return 1;
    }
    return 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/cookie/CookiePathComparator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */