package cz.msebera.android.httpclient.config;

public abstract interface Lookup<I>
{
  public abstract I lookup(String paramString);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/config/Lookup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */