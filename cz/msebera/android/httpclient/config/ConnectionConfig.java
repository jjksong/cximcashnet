package cz.msebera.android.httpclient.config;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;

@Immutable
public class ConnectionConfig
  implements Cloneable
{
  public static final ConnectionConfig DEFAULT = new Builder().build();
  private final int bufferSize;
  private final Charset charset;
  private final int fragmentSizeHint;
  private final CodingErrorAction malformedInputAction;
  private final MessageConstraints messageConstraints;
  private final CodingErrorAction unmappableInputAction;
  
  ConnectionConfig(int paramInt1, int paramInt2, Charset paramCharset, CodingErrorAction paramCodingErrorAction1, CodingErrorAction paramCodingErrorAction2, MessageConstraints paramMessageConstraints)
  {
    this.bufferSize = paramInt1;
    this.fragmentSizeHint = paramInt2;
    this.charset = paramCharset;
    this.malformedInputAction = paramCodingErrorAction1;
    this.unmappableInputAction = paramCodingErrorAction2;
    this.messageConstraints = paramMessageConstraints;
  }
  
  public static Builder copy(ConnectionConfig paramConnectionConfig)
  {
    Args.notNull(paramConnectionConfig, "Connection config");
    return new Builder().setCharset(paramConnectionConfig.getCharset()).setMalformedInputAction(paramConnectionConfig.getMalformedInputAction()).setUnmappableInputAction(paramConnectionConfig.getUnmappableInputAction()).setMessageConstraints(paramConnectionConfig.getMessageConstraints());
  }
  
  public static Builder custom()
  {
    return new Builder();
  }
  
  protected ConnectionConfig clone()
    throws CloneNotSupportedException
  {
    return (ConnectionConfig)super.clone();
  }
  
  public int getBufferSize()
  {
    return this.bufferSize;
  }
  
  public Charset getCharset()
  {
    return this.charset;
  }
  
  public int getFragmentSizeHint()
  {
    return this.fragmentSizeHint;
  }
  
  public CodingErrorAction getMalformedInputAction()
  {
    return this.malformedInputAction;
  }
  
  public MessageConstraints getMessageConstraints()
  {
    return this.messageConstraints;
  }
  
  public CodingErrorAction getUnmappableInputAction()
  {
    return this.unmappableInputAction;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[bufferSize=");
    localStringBuilder.append(this.bufferSize);
    localStringBuilder.append(", fragmentSizeHint=");
    localStringBuilder.append(this.fragmentSizeHint);
    localStringBuilder.append(", charset=");
    localStringBuilder.append(this.charset);
    localStringBuilder.append(", malformedInputAction=");
    localStringBuilder.append(this.malformedInputAction);
    localStringBuilder.append(", unmappableInputAction=");
    localStringBuilder.append(this.unmappableInputAction);
    localStringBuilder.append(", messageConstraints=");
    localStringBuilder.append(this.messageConstraints);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public static class Builder
  {
    private int bufferSize;
    private Charset charset;
    private int fragmentSizeHint = -1;
    private CodingErrorAction malformedInputAction;
    private MessageConstraints messageConstraints;
    private CodingErrorAction unmappableInputAction;
    
    public ConnectionConfig build()
    {
      Charset localCharset = this.charset;
      if ((localCharset == null) && ((this.malformedInputAction != null) || (this.unmappableInputAction != null))) {
        localCharset = Consts.ASCII;
      }
      int i = this.bufferSize;
      if (i <= 0) {
        i = 8192;
      }
      int j = this.fragmentSizeHint;
      if (j < 0) {
        j = i;
      }
      return new ConnectionConfig(i, j, localCharset, this.malformedInputAction, this.unmappableInputAction, this.messageConstraints);
    }
    
    public Builder setBufferSize(int paramInt)
    {
      this.bufferSize = paramInt;
      return this;
    }
    
    public Builder setCharset(Charset paramCharset)
    {
      this.charset = paramCharset;
      return this;
    }
    
    public Builder setFragmentSizeHint(int paramInt)
    {
      this.fragmentSizeHint = paramInt;
      return this;
    }
    
    public Builder setMalformedInputAction(CodingErrorAction paramCodingErrorAction)
    {
      this.malformedInputAction = paramCodingErrorAction;
      if ((paramCodingErrorAction != null) && (this.charset == null)) {
        this.charset = Consts.ASCII;
      }
      return this;
    }
    
    public Builder setMessageConstraints(MessageConstraints paramMessageConstraints)
    {
      this.messageConstraints = paramMessageConstraints;
      return this;
    }
    
    public Builder setUnmappableInputAction(CodingErrorAction paramCodingErrorAction)
    {
      this.unmappableInputAction = paramCodingErrorAction;
      if ((paramCodingErrorAction != null) && (this.charset == null)) {
        this.charset = Consts.ASCII;
      }
      return this;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/config/ConnectionConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */