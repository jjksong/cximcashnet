package cz.msebera.android.httpclient.config;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ThreadSafe
public final class Registry<I>
  implements Lookup<I>
{
  private final Map<String, I> map;
  
  Registry(Map<String, I> paramMap)
  {
    this.map = new ConcurrentHashMap(paramMap);
  }
  
  public I lookup(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return (I)this.map.get(paramString.toLowerCase(Locale.ENGLISH));
  }
  
  public String toString()
  {
    return this.map.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/config/Registry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */