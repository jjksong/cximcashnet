package cz.msebera.android.httpclient.concurrent;

import cz.msebera.android.httpclient.util.Args;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class BasicFuture<T>
  implements Future<T>, Cancellable
{
  private final FutureCallback<T> callback;
  private volatile boolean cancelled;
  private volatile boolean completed;
  private volatile Exception ex;
  private volatile T result;
  
  public BasicFuture(FutureCallback<T> paramFutureCallback)
  {
    this.callback = paramFutureCallback;
  }
  
  private T getResult()
    throws ExecutionException
  {
    if (this.ex == null) {
      return (T)this.result;
    }
    throw new ExecutionException(this.ex);
  }
  
  public boolean cancel()
  {
    return cancel(true);
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    try
    {
      if (this.completed) {
        return false;
      }
      this.completed = true;
      this.cancelled = true;
      notifyAll();
      FutureCallback localFutureCallback = this.callback;
      if (localFutureCallback != null) {
        localFutureCallback.cancelled();
      }
      return true;
    }
    finally {}
  }
  
  public boolean completed(T paramT)
  {
    try
    {
      if (this.completed) {
        return false;
      }
      this.completed = true;
      this.result = paramT;
      notifyAll();
      FutureCallback localFutureCallback = this.callback;
      if (localFutureCallback != null) {
        localFutureCallback.completed(paramT);
      }
      return true;
    }
    finally {}
  }
  
  public boolean failed(Exception paramException)
  {
    try
    {
      if (this.completed) {
        return false;
      }
      this.completed = true;
      this.ex = paramException;
      notifyAll();
      FutureCallback localFutureCallback = this.callback;
      if (localFutureCallback != null) {
        localFutureCallback.failed(paramException);
      }
      return true;
    }
    finally {}
  }
  
  public T get()
    throws InterruptedException, ExecutionException
  {
    try
    {
      while (!this.completed) {
        wait();
      }
      Object localObject1 = getResult();
      return (T)localObject1;
    }
    finally {}
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    try
    {
      Args.notNull(paramTimeUnit, "Time unit");
      long l2 = paramTimeUnit.toMillis(paramLong);
      if (l2 <= 0L) {
        paramLong = 0L;
      } else {
        paramLong = System.currentTimeMillis();
      }
      if (this.completed)
      {
        paramTimeUnit = getResult();
        return paramTimeUnit;
      }
      if (l2 > 0L)
      {
        long l1 = l2;
        do
        {
          wait(l1);
          if (this.completed)
          {
            paramTimeUnit = getResult();
            return paramTimeUnit;
          }
          l1 = l2 - (System.currentTimeMillis() - paramLong);
        } while (l1 > 0L);
        paramTimeUnit = new java/util/concurrent/TimeoutException;
        paramTimeUnit.<init>();
        throw paramTimeUnit;
      }
      paramTimeUnit = new java/util/concurrent/TimeoutException;
      paramTimeUnit.<init>();
      throw paramTimeUnit;
    }
    finally {}
  }
  
  public boolean isCancelled()
  {
    return this.cancelled;
  }
  
  public boolean isDone()
  {
    return this.completed;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/concurrent/BasicFuture.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */