package cz.msebera.android.httpclient.concurrent;

public abstract interface Cancellable
{
  public abstract boolean cancel();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/concurrent/Cancellable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */