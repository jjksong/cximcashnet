package cz.msebera.android.httpclient.pool;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.concurrent.FutureCallback;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

@ThreadSafe
abstract class PoolEntryFuture<T>
  implements Future<T>
{
  private final FutureCallback<T> callback;
  private volatile boolean cancelled;
  private volatile boolean completed;
  private final Condition condition;
  private final Lock lock;
  private T result;
  
  PoolEntryFuture(Lock paramLock, FutureCallback<T> paramFutureCallback)
  {
    this.lock = paramLock;
    this.condition = paramLock.newCondition();
    this.callback = paramFutureCallback;
  }
  
  public boolean await(Date paramDate)
    throws InterruptedException
  {
    this.lock.lock();
    try
    {
      if (!this.cancelled)
      {
        boolean bool1;
        if (paramDate != null)
        {
          bool1 = this.condition.awaitUntil(paramDate);
        }
        else
        {
          this.condition.await();
          bool1 = true;
        }
        boolean bool2 = this.cancelled;
        if (!bool2) {
          return bool1;
        }
        paramDate = new java/lang/InterruptedException;
        paramDate.<init>("Operation interrupted");
        throw paramDate;
      }
      paramDate = new java/lang/InterruptedException;
      paramDate.<init>("Operation interrupted");
      throw paramDate;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  /* Error */
  public boolean cancel(boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 47 1 0
    //   9: aload_0
    //   10: getfield 69	cz/msebera/android/httpclient/pool/PoolEntryFuture:completed	Z
    //   13: istore_1
    //   14: iload_1
    //   15: ifeq +16 -> 31
    //   18: iconst_0
    //   19: istore_1
    //   20: aload_0
    //   21: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   24: invokeinterface 59 1 0
    //   29: iload_1
    //   30: ireturn
    //   31: iconst_1
    //   32: istore_1
    //   33: aload_0
    //   34: iconst_1
    //   35: putfield 69	cz/msebera/android/httpclient/pool/PoolEntryFuture:completed	Z
    //   38: aload_0
    //   39: iconst_1
    //   40: putfield 49	cz/msebera/android/httpclient/pool/PoolEntryFuture:cancelled	Z
    //   43: aload_0
    //   44: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   47: ifnull +12 -> 59
    //   50: aload_0
    //   51: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   54: invokeinterface 73 1 0
    //   59: aload_0
    //   60: getfield 36	cz/msebera/android/httpclient/pool/PoolEntryFuture:condition	Ljava/util/concurrent/locks/Condition;
    //   63: invokeinterface 76 1 0
    //   68: goto -48 -> 20
    //   71: astore_2
    //   72: aload_0
    //   73: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   76: invokeinterface 59 1 0
    //   81: aload_2
    //   82: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	this	PoolEntryFuture
    //   0	83	1	paramBoolean	boolean
    //   71	11	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	14	71	finally
    //   33	59	71	finally
    //   59	68	71	finally
  }
  
  public T get()
    throws InterruptedException, ExecutionException
  {
    try
    {
      Object localObject = get(0L, TimeUnit.MILLISECONDS);
      return (T)localObject;
    }
    catch (TimeoutException localTimeoutException)
    {
      throw new ExecutionException(localTimeoutException);
    }
  }
  
  /* Error */
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    // Byte code:
    //   0: aload_3
    //   1: ldc 99
    //   3: invokestatic 105	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_0
    //   8: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   11: invokeinterface 47 1 0
    //   16: aload_0
    //   17: getfield 69	cz/msebera/android/httpclient/pool/PoolEntryFuture:completed	Z
    //   20: ifeq +19 -> 39
    //   23: aload_0
    //   24: getfield 107	cz/msebera/android/httpclient/pool/PoolEntryFuture:result	Ljava/lang/Object;
    //   27: astore_3
    //   28: aload_0
    //   29: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   32: invokeinterface 59 1 0
    //   37: aload_3
    //   38: areturn
    //   39: aload_0
    //   40: aload_0
    //   41: lload_1
    //   42: aload_3
    //   43: invokevirtual 110	cz/msebera/android/httpclient/pool/PoolEntryFuture:getPoolEntry	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    //   46: putfield 107	cz/msebera/android/httpclient/pool/PoolEntryFuture:result	Ljava/lang/Object;
    //   49: aload_0
    //   50: iconst_1
    //   51: putfield 69	cz/msebera/android/httpclient/pool/PoolEntryFuture:completed	Z
    //   54: aload_0
    //   55: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   58: ifnull +16 -> 74
    //   61: aload_0
    //   62: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   65: aload_0
    //   66: getfield 107	cz/msebera/android/httpclient/pool/PoolEntryFuture:result	Ljava/lang/Object;
    //   69: invokeinterface 113 2 0
    //   74: aload_0
    //   75: getfield 107	cz/msebera/android/httpclient/pool/PoolEntryFuture:result	Ljava/lang/Object;
    //   78: astore_3
    //   79: goto -51 -> 28
    //   82: astore_3
    //   83: goto +45 -> 128
    //   86: astore 4
    //   88: aload_0
    //   89: iconst_1
    //   90: putfield 69	cz/msebera/android/httpclient/pool/PoolEntryFuture:completed	Z
    //   93: aload_0
    //   94: aconst_null
    //   95: putfield 107	cz/msebera/android/httpclient/pool/PoolEntryFuture:result	Ljava/lang/Object;
    //   98: aload_0
    //   99: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   102: ifnull +14 -> 116
    //   105: aload_0
    //   106: getfield 38	cz/msebera/android/httpclient/pool/PoolEntryFuture:callback	Lcz/msebera/android/httpclient/concurrent/FutureCallback;
    //   109: aload 4
    //   111: invokeinterface 117 2 0
    //   116: new 80	java/util/concurrent/ExecutionException
    //   119: astore_3
    //   120: aload_3
    //   121: aload 4
    //   123: invokespecial 94	java/util/concurrent/ExecutionException:<init>	(Ljava/lang/Throwable;)V
    //   126: aload_3
    //   127: athrow
    //   128: aload_0
    //   129: getfield 28	cz/msebera/android/httpclient/pool/PoolEntryFuture:lock	Ljava/util/concurrent/locks/Lock;
    //   132: invokeinterface 59 1 0
    //   137: aload_3
    //   138: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	139	0	this	PoolEntryFuture
    //   0	139	1	paramLong	long
    //   0	139	3	paramTimeUnit	TimeUnit
    //   86	36	4	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   16	28	82	finally
    //   39	74	82	finally
    //   74	79	82	finally
    //   88	116	82	finally
    //   116	128	82	finally
    //   16	28	86	java/io/IOException
    //   39	74	86	java/io/IOException
    //   74	79	86	java/io/IOException
  }
  
  protected abstract T getPoolEntry(long paramLong, TimeUnit paramTimeUnit)
    throws IOException, InterruptedException, TimeoutException;
  
  public boolean isCancelled()
  {
    return this.cancelled;
  }
  
  public boolean isDone()
  {
    return this.completed;
  }
  
  public void wakeup()
  {
    this.lock.lock();
    try
    {
      this.condition.signalAll();
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/pool/PoolEntryFuture.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */