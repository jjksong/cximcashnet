package cz.msebera.android.httpclient.pool;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class PoolStats
{
  private final int available;
  private final int leased;
  private final int max;
  private final int pending;
  
  public PoolStats(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.leased = paramInt1;
    this.pending = paramInt2;
    this.available = paramInt3;
    this.max = paramInt4;
  }
  
  public int getAvailable()
  {
    return this.available;
  }
  
  public int getLeased()
  {
    return this.leased;
  }
  
  public int getMax()
  {
    return this.max;
  }
  
  public int getPending()
  {
    return this.pending;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[leased: ");
    localStringBuilder.append(this.leased);
    localStringBuilder.append("; pending: ");
    localStringBuilder.append(this.pending);
    localStringBuilder.append("; available: ");
    localStringBuilder.append(this.available);
    localStringBuilder.append("; max: ");
    localStringBuilder.append(this.max);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/pool/PoolStats.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */