package cz.msebera.android.httpclient.pool;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.concurrent.TimeUnit;

@ThreadSafe
public abstract class PoolEntry<T, C>
{
  private final C conn;
  private final long created;
  @GuardedBy("this")
  private long expiry;
  private final String id;
  private final T route;
  private volatile Object state;
  @GuardedBy("this")
  private long updated;
  private final long validUnit;
  
  public PoolEntry(String paramString, T paramT, C paramC)
  {
    this(paramString, paramT, paramC, 0L, TimeUnit.MILLISECONDS);
  }
  
  public PoolEntry(String paramString, T paramT, C paramC, long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramT, "Route");
    Args.notNull(paramC, "Connection");
    Args.notNull(paramTimeUnit, "Time unit");
    this.id = paramString;
    this.route = paramT;
    this.conn = paramC;
    this.created = System.currentTimeMillis();
    if (paramLong > 0L) {
      this.validUnit = (this.created + paramTimeUnit.toMillis(paramLong));
    } else {
      this.validUnit = Long.MAX_VALUE;
    }
    this.expiry = this.validUnit;
  }
  
  public abstract void close();
  
  public C getConnection()
  {
    return (C)this.conn;
  }
  
  public long getCreated()
  {
    return this.created;
  }
  
  public long getExpiry()
  {
    try
    {
      long l = this.expiry;
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public T getRoute()
  {
    return (T)this.route;
  }
  
  public Object getState()
  {
    return this.state;
  }
  
  public long getUpdated()
  {
    try
    {
      long l = this.updated;
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public long getValidUnit()
  {
    return this.validUnit;
  }
  
  public abstract boolean isClosed();
  
  public boolean isExpired(long paramLong)
  {
    try
    {
      long l = this.expiry;
      boolean bool;
      if (paramLong >= l) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void setState(Object paramObject)
  {
    this.state = paramObject;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[id:");
    localStringBuilder.append(this.id);
    localStringBuilder.append("][route:");
    localStringBuilder.append(this.route);
    localStringBuilder.append("][state:");
    localStringBuilder.append(this.state);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public void updateExpiry(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      Args.notNull(paramTimeUnit, "Time unit");
      this.updated = System.currentTimeMillis();
      if (paramLong > 0L) {
        paramLong = this.updated + paramTimeUnit.toMillis(paramLong);
      } else {
        paramLong = Long.MAX_VALUE;
      }
      this.expiry = Math.min(paramLong, this.validUnit);
      return;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/pool/PoolEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */