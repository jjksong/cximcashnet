package cz.msebera.android.httpclient.pool;

public abstract interface PoolEntryCallback<T, C>
{
  public abstract void process(PoolEntry<T, C> paramPoolEntry);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/pool/PoolEntryCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */