package cz.msebera.android.httpclient.pool;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.concurrent.FutureCallback;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@ThreadSafe
public abstract class AbstractConnPool<T, C, E extends PoolEntry<T, C>>
  implements ConnPool<T, E>, ConnPoolControl<T>
{
  private final LinkedList<E> available;
  private final ConnFactory<T, C> connFactory;
  private volatile int defaultMaxPerRoute;
  private volatile boolean isShutDown;
  private final Set<E> leased;
  private final Lock lock;
  private final Map<T, Integer> maxPerRoute;
  private volatile int maxTotal;
  private final LinkedList<PoolEntryFuture<E>> pending;
  private final Map<T, RouteSpecificPool<T, C, E>> routeToPool;
  
  public AbstractConnPool(ConnFactory<T, C> paramConnFactory, int paramInt1, int paramInt2)
  {
    this.connFactory = ((ConnFactory)Args.notNull(paramConnFactory, "Connection factory"));
    this.defaultMaxPerRoute = Args.notNegative(paramInt1, "Max per route value");
    this.maxTotal = Args.notNegative(paramInt2, "Max total value");
    this.lock = new ReentrantLock();
    this.routeToPool = new HashMap();
    this.leased = new HashSet();
    this.available = new LinkedList();
    this.pending = new LinkedList();
    this.maxPerRoute = new HashMap();
  }
  
  private int getMax(T paramT)
  {
    paramT = (Integer)this.maxPerRoute.get(paramT);
    if (paramT != null) {
      return paramT.intValue();
    }
    return this.defaultMaxPerRoute;
  }
  
  private RouteSpecificPool<T, C, E> getPool(final T paramT)
  {
    RouteSpecificPool localRouteSpecificPool = (RouteSpecificPool)this.routeToPool.get(paramT);
    Object localObject = localRouteSpecificPool;
    if (localRouteSpecificPool == null)
    {
      localObject = new RouteSpecificPool(paramT)
      {
        protected E createEntry(C paramAnonymousC)
        {
          return AbstractConnPool.this.createEntry(paramT, paramAnonymousC);
        }
      };
      this.routeToPool.put(paramT, localObject);
    }
    return (RouteSpecificPool<T, C, E>)localObject;
  }
  
  private E getPoolEntryBlocking(T paramT, Object paramObject, long paramLong, TimeUnit paramTimeUnit, PoolEntryFuture<E> paramPoolEntryFuture)
    throws IOException, InterruptedException, TimeoutException
  {
    PoolEntry localPoolEntry1 = null;
    if (paramLong > 0L) {
      paramTimeUnit = new Date(System.currentTimeMillis() + paramTimeUnit.toMillis(paramLong));
    } else {
      paramTimeUnit = null;
    }
    this.lock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool = getPool(paramT);
      if (localPoolEntry1 == null)
      {
        boolean bool;
        if (!this.isShutDown) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Connection pool shut down");
        for (;;)
        {
          localPoolEntry1 = localRouteSpecificPool.getFree(paramObject);
          if ((localPoolEntry1 == null) || ((!localPoolEntry1.isClosed()) && (!localPoolEntry1.isExpired(System.currentTimeMillis()))))
          {
            if (localPoolEntry1 != null)
            {
              this.available.remove(localPoolEntry1);
              this.leased.add(localPoolEntry1);
              return localPoolEntry1;
            }
            int j = getMax(paramT);
            int k = Math.max(0, localRouteSpecificPool.getAllocatedCount() + 1 - j);
            int i;
            if (k > 0) {
              for (i = 0; i < k; i++)
              {
                PoolEntry localPoolEntry2 = localRouteSpecificPool.getLastUsed();
                if (localPoolEntry2 == null) {
                  break;
                }
                localPoolEntry2.close();
                this.available.remove(localPoolEntry2);
                localRouteSpecificPool.remove(localPoolEntry2);
              }
            }
            if (localRouteSpecificPool.getAllocatedCount() < j)
            {
              i = this.leased.size();
              i = Math.max(this.maxTotal - i, 0);
              if (i > 0)
              {
                if ((this.available.size() > i - 1) && (!this.available.isEmpty()))
                {
                  paramObject = (PoolEntry)this.available.removeLast();
                  ((PoolEntry)paramObject).close();
                  getPool(((PoolEntry)paramObject).getRoute()).remove((PoolEntry)paramObject);
                }
                paramT = localRouteSpecificPool.add(this.connFactory.create(paramT));
                this.leased.add(paramT);
                return paramT;
              }
            }
          }
          try
          {
            localRouteSpecificPool.queue(paramPoolEntryFuture);
            this.pending.add(paramPoolEntryFuture);
            bool = paramPoolEntryFuture.await(paramTimeUnit);
            localRouteSpecificPool.unqueue(paramPoolEntryFuture);
            this.pending.remove(paramPoolEntryFuture);
            if ((!bool) && (paramTimeUnit != null)) {
              if (paramTimeUnit.getTime() <= System.currentTimeMillis()) {
                break label480;
              }
            }
          }
          finally
          {
            localRouteSpecificPool.unqueue(paramPoolEntryFuture);
            this.pending.remove(paramPoolEntryFuture);
          }
          localRouteSpecificPool.free(localPoolEntry1, false);
        }
      }
      label480:
      paramT = new java/util/concurrent/TimeoutException;
      paramT.<init>("Timeout waiting for connection");
      throw paramT;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  private void purgePoolMap()
  {
    Iterator localIterator = this.routeToPool.entrySet().iterator();
    while (localIterator.hasNext())
    {
      RouteSpecificPool localRouteSpecificPool = (RouteSpecificPool)((Map.Entry)localIterator.next()).getValue();
      if (localRouteSpecificPool.getPendingCount() + localRouteSpecificPool.getAllocatedCount() == 0) {
        localIterator.remove();
      }
    }
  }
  
  public void closeExpired()
  {
    enumAvailable(new PoolEntryCallback()
    {
      public void process(PoolEntry<T, C> paramAnonymousPoolEntry)
      {
        if (paramAnonymousPoolEntry.isExpired(this.val$now)) {
          paramAnonymousPoolEntry.close();
        }
      }
    });
  }
  
  public void closeIdle(long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramTimeUnit, "Time unit");
    long l = paramTimeUnit.toMillis(paramLong);
    paramLong = l;
    if (l < 0L) {
      paramLong = 0L;
    }
    enumAvailable(new PoolEntryCallback()
    {
      public void process(PoolEntry<T, C> paramAnonymousPoolEntry)
      {
        if (paramAnonymousPoolEntry.getUpdated() <= this.val$deadline) {
          paramAnonymousPoolEntry.close();
        }
      }
    });
  }
  
  protected abstract E createEntry(T paramT, C paramC);
  
  protected void enumAvailable(PoolEntryCallback<T, C> paramPoolEntryCallback)
  {
    this.lock.lock();
    try
    {
      Iterator localIterator = this.available.iterator();
      while (localIterator.hasNext())
      {
        PoolEntry localPoolEntry = (PoolEntry)localIterator.next();
        paramPoolEntryCallback.process(localPoolEntry);
        if (localPoolEntry.isClosed())
        {
          getPool(localPoolEntry.getRoute()).remove(localPoolEntry);
          localIterator.remove();
        }
      }
      purgePoolMap();
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  protected void enumLeased(PoolEntryCallback<T, C> paramPoolEntryCallback)
  {
    this.lock.lock();
    try
    {
      Iterator localIterator = this.leased.iterator();
      while (localIterator.hasNext()) {
        paramPoolEntryCallback.process((PoolEntry)localIterator.next());
      }
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public int getDefaultMaxPerRoute()
  {
    this.lock.lock();
    try
    {
      int i = this.defaultMaxPerRoute;
      return i;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public int getMaxPerRoute(T paramT)
  {
    Args.notNull(paramT, "Route");
    this.lock.lock();
    try
    {
      int i = getMax(paramT);
      return i;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public int getMaxTotal()
  {
    this.lock.lock();
    try
    {
      int i = this.maxTotal;
      return i;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public PoolStats getStats(T paramT)
  {
    Args.notNull(paramT, "Route");
    this.lock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool = getPool(paramT);
      paramT = new PoolStats(localRouteSpecificPool.getLeasedCount(), localRouteSpecificPool.getPendingCount(), localRouteSpecificPool.getAvailableCount(), getMax(paramT));
      return paramT;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public PoolStats getTotalStats()
  {
    this.lock.lock();
    try
    {
      PoolStats localPoolStats = new PoolStats(this.leased.size(), this.pending.size(), this.available.size(), this.maxTotal);
      return localPoolStats;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public boolean isShutdown()
  {
    return this.isShutDown;
  }
  
  public Future<E> lease(T paramT, Object paramObject)
  {
    return lease(paramT, paramObject, null);
  }
  
  public Future<E> lease(final T paramT, final Object paramObject, FutureCallback<E> paramFutureCallback)
  {
    Args.notNull(paramT, "Route");
    Asserts.check(this.isShutDown ^ true, "Connection pool shut down");
    new PoolEntryFuture(this.lock, paramFutureCallback)
    {
      public E getPoolEntry(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
        throws InterruptedException, TimeoutException, IOException
      {
        paramAnonymousTimeUnit = AbstractConnPool.this.getPoolEntryBlocking(paramT, paramObject, paramAnonymousLong, paramAnonymousTimeUnit, this);
        AbstractConnPool.this.onLease(paramAnonymousTimeUnit);
        return paramAnonymousTimeUnit;
      }
    };
  }
  
  protected void onLease(E paramE) {}
  
  protected void onRelease(E paramE) {}
  
  public void release(E paramE, boolean paramBoolean)
  {
    this.lock.lock();
    try
    {
      if (this.leased.remove(paramE))
      {
        RouteSpecificPool localRouteSpecificPool = getPool(paramE.getRoute());
        localRouteSpecificPool.free(paramE, paramBoolean);
        if ((paramBoolean) && (!this.isShutDown))
        {
          this.available.addFirst(paramE);
          onRelease(paramE);
        }
        else
        {
          paramE.close();
        }
        paramE = localRouteSpecificPool.nextPending();
        if (paramE != null) {
          this.pending.remove(paramE);
        } else {
          paramE = (PoolEntryFuture)this.pending.poll();
        }
        if (paramE != null) {
          paramE.wakeup();
        }
      }
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    Args.notNegative(paramInt, "Max per route value");
    this.lock.lock();
    try
    {
      this.defaultMaxPerRoute = paramInt;
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public void setMaxPerRoute(T paramT, int paramInt)
  {
    Args.notNull(paramT, "Route");
    Args.notNegative(paramInt, "Max per route value");
    this.lock.lock();
    try
    {
      this.maxPerRoute.put(paramT, Integer.valueOf(paramInt));
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public void setMaxTotal(int paramInt)
  {
    Args.notNegative(paramInt, "Max value");
    this.lock.lock();
    try
    {
      this.maxTotal = paramInt;
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public void shutdown()
    throws IOException
  {
    if (this.isShutDown) {
      return;
    }
    this.isShutDown = true;
    this.lock.lock();
    try
    {
      Iterator localIterator = this.available.iterator();
      while (localIterator.hasNext()) {
        ((PoolEntry)localIterator.next()).close();
      }
      localIterator = this.leased.iterator();
      while (localIterator.hasNext()) {
        ((PoolEntry)localIterator.next()).close();
      }
      localIterator = this.routeToPool.values().iterator();
      while (localIterator.hasNext()) {
        ((RouteSpecificPool)localIterator.next()).shutdown();
      }
      this.routeToPool.clear();
      this.leased.clear();
      this.available.clear();
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[leased: ");
    localStringBuilder.append(this.leased);
    localStringBuilder.append("][available: ");
    localStringBuilder.append(this.available);
    localStringBuilder.append("][pending: ");
    localStringBuilder.append(this.pending);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/pool/AbstractConnPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */