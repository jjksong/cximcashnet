package cz.msebera.android.httpclient.util;

public class Asserts
{
  public static void check(boolean paramBoolean, String paramString)
  {
    if (paramBoolean) {
      return;
    }
    throw new IllegalStateException(paramString);
  }
  
  public static void check(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean) {
      return;
    }
    throw new IllegalStateException(String.format(paramString, paramVarArgs));
  }
  
  public static void notBlank(CharSequence paramCharSequence, String paramString)
  {
    if (!TextUtils.isBlank(paramCharSequence)) {
      return;
    }
    paramCharSequence = new StringBuilder();
    paramCharSequence.append(paramString);
    paramCharSequence.append(" is blank");
    throw new IllegalStateException(paramCharSequence.toString());
  }
  
  public static void notEmpty(CharSequence paramCharSequence, String paramString)
  {
    if (!TextUtils.isEmpty(paramCharSequence)) {
      return;
    }
    paramCharSequence = new StringBuilder();
    paramCharSequence.append(paramString);
    paramCharSequence.append(" is empty");
    throw new IllegalStateException(paramCharSequence.toString());
  }
  
  public static void notNull(Object paramObject, String paramString)
  {
    if (paramObject != null) {
      return;
    }
    paramObject = new StringBuilder();
    ((StringBuilder)paramObject).append(paramString);
    ((StringBuilder)paramObject).append(" is null");
    throw new IllegalStateException(((StringBuilder)paramObject).toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/Asserts.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */