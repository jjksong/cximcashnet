package cz.msebera.android.httpclient.util;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.io.Serializable;

@NotThreadSafe
public final class ByteArrayBuffer
  implements Serializable
{
  private static final long serialVersionUID = 4359112959524048036L;
  private byte[] buffer;
  private int len;
  
  public ByteArrayBuffer(int paramInt)
  {
    Args.notNegative(paramInt, "Buffer capacity");
    this.buffer = new byte[paramInt];
  }
  
  private void expand(int paramInt)
  {
    byte[] arrayOfByte = new byte[Math.max(this.buffer.length << 1, paramInt)];
    System.arraycopy(this.buffer, 0, arrayOfByte, 0, this.len);
    this.buffer = arrayOfByte;
  }
  
  public void append(int paramInt)
  {
    int i = this.len + 1;
    if (i > this.buffer.length) {
      expand(i);
    }
    this.buffer[this.len] = ((byte)paramInt);
    this.len = i;
  }
  
  public void append(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
  {
    if (paramCharArrayBuffer == null) {
      return;
    }
    append(paramCharArrayBuffer.buffer(), paramInt1, paramInt2);
  }
  
  public void append(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte == null) {
      return;
    }
    if ((paramInt1 >= 0) && (paramInt1 <= paramArrayOfByte.length) && (paramInt2 >= 0))
    {
      int i = paramInt1 + paramInt2;
      if ((i >= 0) && (i <= paramArrayOfByte.length))
      {
        if (paramInt2 == 0) {
          return;
        }
        i = this.len + paramInt2;
        if (i > this.buffer.length) {
          expand(i);
        }
        System.arraycopy(paramArrayOfByte, paramInt1, this.buffer, this.len, paramInt2);
        this.len = i;
        return;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("off: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(" len: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(" b.length: ");
    localStringBuilder.append(paramArrayOfByte.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public void append(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    if (paramArrayOfChar == null) {
      return;
    }
    if ((paramInt1 >= 0) && (paramInt1 <= paramArrayOfChar.length) && (paramInt2 >= 0))
    {
      int i = paramInt1 + paramInt2;
      if ((i >= 0) && (i <= paramArrayOfChar.length))
      {
        if (paramInt2 == 0) {
          return;
        }
        int j = this.len;
        int k = paramInt2 + j;
        paramInt2 = j;
        i = paramInt1;
        if (k > this.buffer.length)
        {
          expand(k);
          i = paramInt1;
        }
        for (paramInt2 = j; paramInt2 < k; paramInt2++)
        {
          this.buffer[paramInt2] = ((byte)paramArrayOfChar[i]);
          i++;
        }
        this.len = k;
        return;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("off: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(" len: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(" b.length: ");
    localStringBuilder.append(paramArrayOfChar.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public byte[] buffer()
  {
    return this.buffer;
  }
  
  public int byteAt(int paramInt)
  {
    return this.buffer[paramInt];
  }
  
  public int capacity()
  {
    return this.buffer.length;
  }
  
  public void clear()
  {
    this.len = 0;
  }
  
  public void ensureCapacity(int paramInt)
  {
    if (paramInt <= 0) {
      return;
    }
    int i = this.buffer.length;
    int j = this.len;
    if (paramInt > i - j) {
      expand(j + paramInt);
    }
  }
  
  public int indexOf(byte paramByte)
  {
    return indexOf(paramByte, 0, this.len);
  }
  
  public int indexOf(byte paramByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    if (paramInt1 < 0) {
      i = 0;
    }
    int j = this.len;
    paramInt1 = paramInt2;
    if (paramInt2 > j) {
      paramInt1 = j;
    }
    paramInt2 = i;
    if (i > paramInt1) {
      return -1;
    }
    while (paramInt2 < paramInt1)
    {
      if (this.buffer[paramInt2] == paramByte) {
        return paramInt2;
      }
      paramInt2++;
    }
    return -1;
  }
  
  public boolean isEmpty()
  {
    boolean bool;
    if (this.len == 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isFull()
  {
    boolean bool;
    if (this.len == this.buffer.length) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int length()
  {
    return this.len;
  }
  
  public void setLength(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt <= this.buffer.length))
    {
      this.len = paramInt;
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("len: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" < 0 or > buffer len: ");
    localStringBuilder.append(this.buffer.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public byte[] toByteArray()
  {
    int i = this.len;
    byte[] arrayOfByte = new byte[i];
    if (i > 0) {
      System.arraycopy(this.buffer, 0, arrayOfByte, 0, i);
    }
    return arrayOfByte;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/ByteArrayBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */