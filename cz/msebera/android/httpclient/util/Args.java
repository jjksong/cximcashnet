package cz.msebera.android.httpclient.util;

import java.util.Collection;

public class Args
{
  public static void check(boolean paramBoolean, String paramString)
  {
    if (paramBoolean) {
      return;
    }
    throw new IllegalArgumentException(paramString);
  }
  
  public static void check(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean) {
      return;
    }
    throw new IllegalArgumentException(String.format(paramString, paramVarArgs));
  }
  
  public static <T extends CharSequence> T notBlank(T paramT, String paramString)
  {
    if (paramT != null)
    {
      if (!TextUtils.isBlank(paramT)) {
        return paramT;
      }
      paramT = new StringBuilder();
      paramT.append(paramString);
      paramT.append(" may not be blank");
      throw new IllegalArgumentException(paramT.toString());
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
  
  public static <T extends CharSequence> T notEmpty(T paramT, String paramString)
  {
    if (paramT != null)
    {
      if (!TextUtils.isEmpty(paramT)) {
        return paramT;
      }
      paramT = new StringBuilder();
      paramT.append(paramString);
      paramT.append(" may not be empty");
      throw new IllegalArgumentException(paramT.toString());
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
  
  public static <E, T extends Collection<E>> T notEmpty(T paramT, String paramString)
  {
    if (paramT != null)
    {
      if (!paramT.isEmpty()) {
        return paramT;
      }
      paramT = new StringBuilder();
      paramT.append(paramString);
      paramT.append(" may not be empty");
      throw new IllegalArgumentException(paramT.toString());
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
  
  public static int notNegative(int paramInt, String paramString)
  {
    if (paramInt >= 0) {
      return paramInt;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public static long notNegative(long paramLong, String paramString)
  {
    if (paramLong >= 0L) {
      return paramLong;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public static <T> T notNull(T paramT, String paramString)
  {
    if (paramT != null) {
      return paramT;
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
  
  public static int positive(int paramInt, String paramString)
  {
    if (paramInt > 0) {
      return paramInt;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative or zero");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public static long positive(long paramLong, String paramString)
  {
    if (paramLong > 0L) {
      return paramLong;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative or zero");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/Args.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */