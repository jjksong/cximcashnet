package cz.msebera.android.httpclient.util;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.ParseException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public final class EntityUtils
{
  public static void consume(HttpEntity paramHttpEntity)
    throws IOException
  {
    if (paramHttpEntity == null) {
      return;
    }
    if (paramHttpEntity.isStreaming())
    {
      paramHttpEntity = paramHttpEntity.getContent();
      if (paramHttpEntity != null) {
        paramHttpEntity.close();
      }
    }
  }
  
  public static void consumeQuietly(HttpEntity paramHttpEntity)
  {
    try
    {
      consume(paramHttpEntity);
      return;
    }
    catch (IOException paramHttpEntity)
    {
      for (;;) {}
    }
  }
  
  @Deprecated
  public static String getContentCharSet(HttpEntity paramHttpEntity)
    throws ParseException
  {
    Args.notNull(paramHttpEntity, "Entity");
    if (paramHttpEntity.getContentType() != null)
    {
      paramHttpEntity = paramHttpEntity.getContentType().getElements();
      if (paramHttpEntity.length > 0)
      {
        paramHttpEntity = paramHttpEntity[0].getParameterByName("charset");
        if (paramHttpEntity != null) {
          return paramHttpEntity.getValue();
        }
      }
    }
    paramHttpEntity = null;
    return paramHttpEntity;
  }
  
  @Deprecated
  public static String getContentMimeType(HttpEntity paramHttpEntity)
    throws ParseException
  {
    Args.notNull(paramHttpEntity, "Entity");
    if (paramHttpEntity.getContentType() != null)
    {
      paramHttpEntity = paramHttpEntity.getContentType().getElements();
      if (paramHttpEntity.length > 0) {
        return paramHttpEntity[0].getName();
      }
    }
    paramHttpEntity = null;
    return paramHttpEntity;
  }
  
  public static byte[] toByteArray(HttpEntity paramHttpEntity)
    throws IOException
  {
    Args.notNull(paramHttpEntity, "Entity");
    InputStream localInputStream = paramHttpEntity.getContent();
    if (localInputStream == null) {
      return null;
    }
    try
    {
      boolean bool;
      if (paramHttpEntity.getContentLength() <= 2147483647L) {
        bool = true;
      } else {
        bool = false;
      }
      Args.check(bool, "HTTP entity too large to be buffered in memory");
      int j = (int)paramHttpEntity.getContentLength();
      int i = j;
      if (j < 0) {
        i = 4096;
      }
      ByteArrayBuffer localByteArrayBuffer = new cz/msebera/android/httpclient/util/ByteArrayBuffer;
      localByteArrayBuffer.<init>(i);
      paramHttpEntity = new byte['က'];
      for (;;)
      {
        i = localInputStream.read(paramHttpEntity);
        if (i == -1) {
          break;
        }
        localByteArrayBuffer.append(paramHttpEntity, 0, i);
      }
      paramHttpEntity = localByteArrayBuffer.toByteArray();
      return paramHttpEntity;
    }
    finally
    {
      localInputStream.close();
    }
  }
  
  public static String toString(HttpEntity paramHttpEntity)
    throws IOException, ParseException
  {
    return toString(paramHttpEntity, (Charset)null);
  }
  
  public static String toString(HttpEntity paramHttpEntity, String paramString)
    throws IOException, ParseException
  {
    if (paramString != null) {
      paramString = Charset.forName(paramString);
    } else {
      paramString = null;
    }
    return toString(paramHttpEntity, paramString);
  }
  
  /* Error */
  public static String toString(HttpEntity paramHttpEntity, Charset paramCharset)
    throws IOException, ParseException
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 39
    //   3: invokestatic 45	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_0
    //   8: invokeinterface 23 1 0
    //   13: astore 8
    //   15: aload 8
    //   17: ifnonnull +5 -> 22
    //   20: aconst_null
    //   21: areturn
    //   22: aload_0
    //   23: invokeinterface 80 1 0
    //   28: ldc2_w 81
    //   31: lcmp
    //   32: ifgt +9 -> 41
    //   35: iconst_1
    //   36: istore 6
    //   38: goto +6 -> 44
    //   41: iconst_0
    //   42: istore 6
    //   44: iload 6
    //   46: ldc 84
    //   48: invokestatic 88	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   51: aload_0
    //   52: invokeinterface 80 1 0
    //   57: lstore 4
    //   59: lload 4
    //   61: l2i
    //   62: istore_3
    //   63: iload_3
    //   64: istore_2
    //   65: iload_3
    //   66: ifge +7 -> 73
    //   69: sipush 4096
    //   72: istore_2
    //   73: aload_0
    //   74: invokestatic 123	cz/msebera/android/httpclient/entity/ContentType:get	(Lcz/msebera/android/httpclient/HttpEntity;)Lcz/msebera/android/httpclient/entity/ContentType;
    //   77: astore_0
    //   78: aload_0
    //   79: ifnull +12 -> 91
    //   82: aload_0
    //   83: invokevirtual 127	cz/msebera/android/httpclient/entity/ContentType:getCharset	()Ljava/nio/charset/Charset;
    //   86: astore 7
    //   88: goto +6 -> 94
    //   91: aconst_null
    //   92: astore 7
    //   94: aload 7
    //   96: astore_0
    //   97: aload 7
    //   99: ifnonnull +5 -> 104
    //   102: aload_1
    //   103: astore_0
    //   104: aload_0
    //   105: astore_1
    //   106: aload_0
    //   107: ifnonnull +7 -> 114
    //   110: getstatic 133	cz/msebera/android/httpclient/protocol/HTTP:DEF_CONTENT_CHARSET	Ljava/nio/charset/Charset;
    //   113: astore_1
    //   114: new 135	java/io/InputStreamReader
    //   117: astore_0
    //   118: aload_0
    //   119: aload 8
    //   121: aload_1
    //   122: invokespecial 138	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   125: new 140	cz/msebera/android/httpclient/util/CharArrayBuffer
    //   128: astore_1
    //   129: aload_1
    //   130: iload_2
    //   131: invokespecial 141	cz/msebera/android/httpclient/util/CharArrayBuffer:<init>	(I)V
    //   134: sipush 1024
    //   137: newarray <illegal type>
    //   139: astore 7
    //   141: aload_0
    //   142: aload 7
    //   144: invokevirtual 146	java/io/Reader:read	([C)I
    //   147: istore_2
    //   148: iload_2
    //   149: iconst_m1
    //   150: if_icmpeq +14 -> 164
    //   153: aload_1
    //   154: aload 7
    //   156: iconst_0
    //   157: iload_2
    //   158: invokevirtual 149	cz/msebera/android/httpclient/util/CharArrayBuffer:append	([CII)V
    //   161: goto -20 -> 141
    //   164: aload_1
    //   165: invokevirtual 151	cz/msebera/android/httpclient/util/CharArrayBuffer:toString	()Ljava/lang/String;
    //   168: astore_0
    //   169: aload 8
    //   171: invokevirtual 28	java/io/InputStream:close	()V
    //   174: aload_0
    //   175: areturn
    //   176: astore_0
    //   177: new 153	java/io/UnsupportedEncodingException
    //   180: astore_1
    //   181: aload_1
    //   182: aload_0
    //   183: invokevirtual 156	java/nio/charset/UnsupportedCharsetException:getMessage	()Ljava/lang/String;
    //   186: invokespecial 159	java/io/UnsupportedEncodingException:<init>	(Ljava/lang/String;)V
    //   189: aload_1
    //   190: athrow
    //   191: astore_0
    //   192: aload 8
    //   194: invokevirtual 28	java/io/InputStream:close	()V
    //   197: aload_0
    //   198: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	199	0	paramHttpEntity	HttpEntity
    //   0	199	1	paramCharset	Charset
    //   64	94	2	i	int
    //   62	4	3	j	int
    //   57	3	4	l	long
    //   36	9	6	bool	boolean
    //   86	69	7	localObject	Object
    //   13	180	8	localInputStream	InputStream
    // Exception table:
    //   from	to	target	type
    //   73	78	176	java/nio/charset/UnsupportedCharsetException
    //   82	88	176	java/nio/charset/UnsupportedCharsetException
    //   22	35	191	finally
    //   44	59	191	finally
    //   73	78	191	finally
    //   82	88	191	finally
    //   110	114	191	finally
    //   114	141	191	finally
    //   141	148	191	finally
    //   153	161	191	finally
    //   164	169	191	finally
    //   177	191	191	finally
  }
  
  public static void updateEntity(HttpResponse paramHttpResponse, HttpEntity paramHttpEntity)
    throws IOException
  {
    Args.notNull(paramHttpResponse, "Response");
    consume(paramHttpResponse.getEntity());
    paramHttpResponse.setEntity(paramHttpEntity);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/EntityUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */