package cz.msebera.android.httpclient.util;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.protocol.HTTP;
import java.io.Serializable;

@NotThreadSafe
public final class CharArrayBuffer
  implements Serializable
{
  private static final long serialVersionUID = -6208952725094867135L;
  private char[] buffer;
  private int len;
  
  public CharArrayBuffer(int paramInt)
  {
    Args.notNegative(paramInt, "Buffer capacity");
    this.buffer = new char[paramInt];
  }
  
  private void expand(int paramInt)
  {
    char[] arrayOfChar = new char[Math.max(this.buffer.length << 1, paramInt)];
    System.arraycopy(this.buffer, 0, arrayOfChar, 0, this.len);
    this.buffer = arrayOfChar;
  }
  
  public void append(char paramChar)
  {
    int i = this.len + 1;
    if (i > this.buffer.length) {
      expand(i);
    }
    this.buffer[this.len] = paramChar;
    this.len = i;
  }
  
  public void append(ByteArrayBuffer paramByteArrayBuffer, int paramInt1, int paramInt2)
  {
    if (paramByteArrayBuffer == null) {
      return;
    }
    append(paramByteArrayBuffer.buffer(), paramInt1, paramInt2);
  }
  
  public void append(CharArrayBuffer paramCharArrayBuffer)
  {
    if (paramCharArrayBuffer == null) {
      return;
    }
    append(paramCharArrayBuffer.buffer, 0, paramCharArrayBuffer.len);
  }
  
  public void append(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
  {
    if (paramCharArrayBuffer == null) {
      return;
    }
    append(paramCharArrayBuffer.buffer, paramInt1, paramInt2);
  }
  
  public void append(Object paramObject)
  {
    append(String.valueOf(paramObject));
  }
  
  public void append(String paramString)
  {
    if (paramString == null) {
      paramString = "null";
    }
    int i = paramString.length();
    int j = this.len + i;
    if (j > this.buffer.length) {
      expand(j);
    }
    paramString.getChars(0, i, this.buffer, this.len);
    this.len = j;
  }
  
  public void append(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte == null) {
      return;
    }
    if ((paramInt1 >= 0) && (paramInt1 <= paramArrayOfByte.length) && (paramInt2 >= 0))
    {
      int i = paramInt1 + paramInt2;
      if ((i >= 0) && (i <= paramArrayOfByte.length))
      {
        if (paramInt2 == 0) {
          return;
        }
        int j = this.len;
        int k = paramInt2 + j;
        paramInt2 = j;
        i = paramInt1;
        if (k > this.buffer.length)
        {
          expand(k);
          i = paramInt1;
        }
        for (paramInt2 = j; paramInt2 < k; paramInt2++)
        {
          this.buffer[paramInt2] = ((char)(paramArrayOfByte[i] & 0xFF));
          i++;
        }
        this.len = k;
        return;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("off: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(" len: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(" b.length: ");
    localStringBuilder.append(paramArrayOfByte.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public void append(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    if (paramArrayOfChar == null) {
      return;
    }
    if ((paramInt1 >= 0) && (paramInt1 <= paramArrayOfChar.length) && (paramInt2 >= 0))
    {
      int i = paramInt1 + paramInt2;
      if ((i >= 0) && (i <= paramArrayOfChar.length))
      {
        if (paramInt2 == 0) {
          return;
        }
        i = this.len + paramInt2;
        if (i > this.buffer.length) {
          expand(i);
        }
        System.arraycopy(paramArrayOfChar, paramInt1, this.buffer, this.len, paramInt2);
        this.len = i;
        return;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("off: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(" len: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(" b.length: ");
    localStringBuilder.append(paramArrayOfChar.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public char[] buffer()
  {
    return this.buffer;
  }
  
  public int capacity()
  {
    return this.buffer.length;
  }
  
  public char charAt(int paramInt)
  {
    return this.buffer[paramInt];
  }
  
  public void clear()
  {
    this.len = 0;
  }
  
  public void ensureCapacity(int paramInt)
  {
    if (paramInt <= 0) {
      return;
    }
    int j = this.buffer.length;
    int i = this.len;
    if (paramInt > j - i) {
      expand(i + paramInt);
    }
  }
  
  public int indexOf(int paramInt)
  {
    return indexOf(paramInt, 0, this.len);
  }
  
  public int indexOf(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt2;
    if (paramInt2 < 0) {
      i = 0;
    }
    int j = this.len;
    paramInt2 = paramInt3;
    if (paramInt3 > j) {
      paramInt2 = j;
    }
    paramInt3 = i;
    if (i > paramInt2) {
      return -1;
    }
    while (paramInt3 < paramInt2)
    {
      if (this.buffer[paramInt3] == paramInt1) {
        return paramInt3;
      }
      paramInt3++;
    }
    return -1;
  }
  
  public boolean isEmpty()
  {
    boolean bool;
    if (this.len == 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isFull()
  {
    boolean bool;
    if (this.len == this.buffer.length) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int length()
  {
    return this.len;
  }
  
  public void setLength(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt <= this.buffer.length))
    {
      this.len = paramInt;
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("len: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" < 0 or > buffer len: ");
    localStringBuilder.append(this.buffer.length);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public String substring(int paramInt1, int paramInt2)
  {
    return new String(this.buffer, paramInt1, paramInt2 - paramInt1);
  }
  
  public String substringTrimmed(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      if (paramInt2 <= this.len)
      {
        if (paramInt1 <= paramInt2)
        {
          int i;
          for (;;)
          {
            i = paramInt2;
            if (paramInt1 >= paramInt2) {
              break;
            }
            i = paramInt2;
            if (!HTTP.isWhitespace(this.buffer[paramInt1])) {
              break;
            }
            paramInt1++;
          }
          while ((i > paramInt1) && (HTTP.isWhitespace(this.buffer[(i - 1)]))) {
            i--;
          }
          return new String(this.buffer, paramInt1, i - paramInt1);
        }
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("beginIndex: ");
        localStringBuilder.append(paramInt1);
        localStringBuilder.append(" > endIndex: ");
        localStringBuilder.append(paramInt2);
        throw new IndexOutOfBoundsException(localStringBuilder.toString());
      }
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("endIndex: ");
      localStringBuilder.append(paramInt2);
      localStringBuilder.append(" > length: ");
      localStringBuilder.append(this.len);
      throw new IndexOutOfBoundsException(localStringBuilder.toString());
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Negative beginIndex: ");
    localStringBuilder.append(paramInt1);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
  
  public char[] toCharArray()
  {
    int i = this.len;
    char[] arrayOfChar = new char[i];
    if (i > 0) {
      System.arraycopy(this.buffer, 0, arrayOfChar, 0, i);
    }
    return arrayOfChar;
  }
  
  public String toString()
  {
    return new String(this.buffer, 0, this.len);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/CharArrayBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */