package cz.msebera.android.httpclient.util;

public final class TextUtils
{
  public static boolean isBlank(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return true;
    }
    for (int i = 0; i < paramCharSequence.length(); i++) {
      if (!Character.isWhitespace(paramCharSequence.charAt(i))) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean isEmpty(CharSequence paramCharSequence)
  {
    boolean bool = true;
    if (paramCharSequence == null) {
      return true;
    }
    if (paramCharSequence.length() != 0) {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/TextUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */