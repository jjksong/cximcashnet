package cz.msebera.android.httpclient.util;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public final class NetUtils
{
  public static void formatAddress(StringBuilder paramStringBuilder, SocketAddress paramSocketAddress)
  {
    Args.notNull(paramStringBuilder, "Buffer");
    Args.notNull(paramSocketAddress, "Socket address");
    if ((paramSocketAddress instanceof InetSocketAddress))
    {
      InetSocketAddress localInetSocketAddress = (InetSocketAddress)paramSocketAddress;
      InetAddress localInetAddress = localInetSocketAddress.getAddress();
      paramSocketAddress = localInetAddress;
      if (localInetAddress != null) {
        paramSocketAddress = localInetAddress.getHostAddress();
      }
      paramStringBuilder.append(paramSocketAddress);
      paramStringBuilder.append(':');
      paramStringBuilder.append(localInetSocketAddress.getPort());
    }
    else
    {
      paramStringBuilder.append(paramSocketAddress);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/NetUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */