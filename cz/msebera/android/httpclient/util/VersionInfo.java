package cz.msebera.android.httpclient.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map<**>;
import java.util.Properties;

public class VersionInfo
{
  public static final String PROPERTY_MODULE = "info.module";
  public static final String PROPERTY_RELEASE = "info.release";
  public static final String PROPERTY_TIMESTAMP = "info.timestamp";
  public static final String UNAVAILABLE = "UNAVAILABLE";
  public static final String VERSION_PROPERTY_FILE = "version.properties";
  private final String infoClassloader;
  private final String infoModule;
  private final String infoPackage;
  private final String infoRelease;
  private final String infoTimestamp;
  
  protected VersionInfo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Args.notNull(paramString1, "Package identifier");
    this.infoPackage = paramString1;
    if (paramString2 == null) {
      paramString2 = "UNAVAILABLE";
    }
    this.infoModule = paramString2;
    if (paramString3 == null) {
      paramString3 = "UNAVAILABLE";
    }
    this.infoRelease = paramString3;
    if (paramString4 == null) {
      paramString4 = "UNAVAILABLE";
    }
    this.infoTimestamp = paramString4;
    if (paramString5 == null) {
      paramString5 = "UNAVAILABLE";
    }
    this.infoClassloader = paramString5;
  }
  
  protected static VersionInfo fromMap(String paramString, Map<?, ?> paramMap, ClassLoader paramClassLoader)
  {
    Args.notNull(paramString, "Package identifier");
    Object localObject2;
    Object localObject1;
    if (paramMap != null)
    {
      localObject2 = (String)paramMap.get("info.module");
      localObject1 = localObject2;
      if (localObject2 != null)
      {
        localObject1 = localObject2;
        if (((String)localObject2).length() < 1) {
          localObject1 = null;
        }
      }
      String str = (String)paramMap.get("info.release");
      localObject2 = str;
      if (str != null) {
        if (str.length() >= 1)
        {
          localObject2 = str;
          if (!str.equals("${pom.version}")) {}
        }
        else
        {
          localObject2 = null;
        }
      }
      str = (String)paramMap.get("info.timestamp");
      if ((str != null) && ((str.length() < 1) || (str.equals("${mvn.timestamp}"))))
      {
        str = null;
        paramMap = (Map<?, ?>)localObject1;
        localObject1 = localObject2;
        localObject2 = str;
      }
      else
      {
        paramMap = (Map<?, ?>)localObject1;
        localObject1 = localObject2;
        localObject2 = str;
      }
    }
    else
    {
      paramMap = null;
      localObject1 = paramMap;
      localObject2 = localObject1;
    }
    if (paramClassLoader != null) {
      paramClassLoader = paramClassLoader.toString();
    } else {
      paramClassLoader = null;
    }
    return new VersionInfo(paramString, paramMap, (String)localObject1, (String)localObject2, paramClassLoader);
  }
  
  public static String getUserAgent(String paramString1, String paramString2, Class<?> paramClass)
  {
    paramString2 = loadVersionInfo(paramString2, paramClass.getClassLoader());
    if (paramString2 != null) {
      paramString2 = paramString2.getRelease();
    } else {
      paramString2 = "UNAVAILABLE";
    }
    String str = System.getProperty("java.version");
    paramClass = new StringBuilder();
    paramClass.append(paramString1);
    paramClass.append("/");
    paramClass.append(paramString2);
    paramClass.append(" (Java 1.5 minimum; Java/");
    paramClass.append(str);
    paramClass.append(")");
    return paramClass.toString();
  }
  
  public static VersionInfo loadVersionInfo(String paramString, ClassLoader paramClassLoader)
  {
    Args.notNull(paramString, "Package identifier");
    ClassLoader localClassLoader;
    if (paramClassLoader != null) {
      localClassLoader = paramClassLoader;
    } else {
      localClassLoader = Thread.currentThread().getContextClassLoader();
    }
    VersionInfo localVersionInfo = null;
    try
    {
      paramClassLoader = new java/lang/StringBuilder;
      paramClassLoader.<init>();
      paramClassLoader.append(paramString.replace('.', '/'));
      paramClassLoader.append("/");
      paramClassLoader.append("version.properties");
      InputStream localInputStream = localClassLoader.getResourceAsStream(paramClassLoader.toString());
      if (localInputStream != null) {}
      try
      {
        paramClassLoader = new java/util/Properties;
        paramClassLoader.<init>();
        paramClassLoader.load(localInputStream);
      }
      finally
      {
        try
        {
          localInputStream.close();
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
        paramClassLoader = finally;
        localInputStream.close();
        throw paramClassLoader;
        paramClassLoader = null;
      }
      if (paramClassLoader == null) {
        break label124;
      }
    }
    catch (IOException paramClassLoader)
    {
      paramClassLoader = null;
    }
    localVersionInfo = fromMap(paramString, paramClassLoader, localClassLoader);
    label124:
    return localVersionInfo;
  }
  
  public static VersionInfo[] loadVersionInfo(String[] paramArrayOfString, ClassLoader paramClassLoader)
  {
    Args.notNull(paramArrayOfString, "Package identifier array");
    ArrayList localArrayList = new ArrayList(paramArrayOfString.length);
    int j = paramArrayOfString.length;
    for (int i = 0; i < j; i++)
    {
      VersionInfo localVersionInfo = loadVersionInfo(paramArrayOfString[i], paramClassLoader);
      if (localVersionInfo != null) {
        localArrayList.add(localVersionInfo);
      }
    }
    return (VersionInfo[])localArrayList.toArray(new VersionInfo[localArrayList.size()]);
  }
  
  public final String getClassloader()
  {
    return this.infoClassloader;
  }
  
  public final String getModule()
  {
    return this.infoModule;
  }
  
  public final String getPackage()
  {
    return this.infoPackage;
  }
  
  public final String getRelease()
  {
    return this.infoRelease;
  }
  
  public final String getTimestamp()
  {
    return this.infoTimestamp;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(this.infoPackage.length() + 20 + this.infoModule.length() + this.infoRelease.length() + this.infoTimestamp.length() + this.infoClassloader.length());
    localStringBuilder.append("VersionInfo(");
    localStringBuilder.append(this.infoPackage);
    localStringBuilder.append(':');
    localStringBuilder.append(this.infoModule);
    if (!"UNAVAILABLE".equals(this.infoRelease))
    {
      localStringBuilder.append(':');
      localStringBuilder.append(this.infoRelease);
    }
    if (!"UNAVAILABLE".equals(this.infoTimestamp))
    {
      localStringBuilder.append(':');
      localStringBuilder.append(this.infoTimestamp);
    }
    localStringBuilder.append(')');
    if (!"UNAVAILABLE".equals(this.infoClassloader))
    {
      localStringBuilder.append('@');
      localStringBuilder.append(this.infoClassloader);
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/util/VersionInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */