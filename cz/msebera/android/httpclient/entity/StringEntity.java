package cz.msebera.android.httpclient.entity;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.Args;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

@NotThreadSafe
public class StringEntity
  extends AbstractHttpEntity
  implements Cloneable
{
  protected final byte[] content;
  
  public StringEntity(String paramString)
    throws UnsupportedEncodingException
  {
    this(paramString, ContentType.DEFAULT_TEXT);
  }
  
  public StringEntity(String paramString, ContentType paramContentType)
    throws UnsupportedCharsetException
  {
    Args.notNull(paramString, "Source string");
    Charset localCharset1;
    if (paramContentType != null) {
      localCharset1 = paramContentType.getCharset();
    } else {
      localCharset1 = null;
    }
    Charset localCharset2 = localCharset1;
    if (localCharset1 == null) {
      localCharset2 = HTTP.DEF_CONTENT_CHARSET;
    }
    try
    {
      this.content = paramString.getBytes(localCharset2.name());
      if (paramContentType != null) {
        setContentType(paramContentType.toString());
      }
      return;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new UnsupportedCharsetException(localCharset2.name());
    }
  }
  
  public StringEntity(String paramString1, String paramString2)
    throws UnsupportedCharsetException
  {
    this(paramString1, ContentType.create(ContentType.TEXT_PLAIN.getMimeType(), paramString2));
  }
  
  @Deprecated
  public StringEntity(String paramString1, String paramString2, String paramString3)
    throws UnsupportedEncodingException
  {
    Args.notNull(paramString1, "Source string");
    if (paramString2 == null) {
      paramString2 = "text/plain";
    }
    if (paramString3 == null) {
      paramString3 = "ISO-8859-1";
    }
    this.content = paramString1.getBytes(paramString3);
    paramString1 = new StringBuilder();
    paramString1.append(paramString2);
    paramString1.append("; charset=");
    paramString1.append(paramString3);
    setContentType(paramString1.toString());
  }
  
  public StringEntity(String paramString, Charset paramCharset)
  {
    this(paramString, ContentType.create(ContentType.TEXT_PLAIN.getMimeType(), paramCharset));
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public InputStream getContent()
    throws IOException
  {
    return new ByteArrayInputStream(this.content);
  }
  
  public long getContentLength()
  {
    return this.content.length;
  }
  
  public boolean isRepeatable()
  {
    return true;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    paramOutputStream.write(this.content);
    paramOutputStream.flush();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/StringEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */