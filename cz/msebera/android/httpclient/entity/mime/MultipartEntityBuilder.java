package cz.msebera.android.httpclient.entity.mime;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.entity.mime.content.ContentBody;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.entity.mime.content.InputStreamBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.util.Args;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MultipartEntityBuilder
{
  private static final String DEFAULT_SUBTYPE = "form-data";
  private static final char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  private List<FormBodyPart> bodyParts = null;
  private String boundary = null;
  private Charset charset = null;
  private HttpMultipartMode mode = HttpMultipartMode.STRICT;
  private String subType = "form-data";
  
  public static MultipartEntityBuilder create()
  {
    return new MultipartEntityBuilder();
  }
  
  private String generateBoundary()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Random localRandom = new Random();
    int j = localRandom.nextInt(11);
    for (int i = 0; i < j + 30; i++)
    {
      char[] arrayOfChar = MULTIPART_CHARS;
      localStringBuilder.append(arrayOfChar[localRandom.nextInt(arrayOfChar.length)]);
    }
    return localStringBuilder.toString();
  }
  
  private String generateContentType(String paramString, Charset paramCharset)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("multipart/form-data; boundary=");
    localStringBuilder.append(paramString);
    if (paramCharset != null)
    {
      localStringBuilder.append("; charset=");
      localStringBuilder.append(paramCharset.name());
    }
    return localStringBuilder.toString();
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString, File paramFile)
  {
    ContentType localContentType = ContentType.DEFAULT_BINARY;
    String str;
    if (paramFile != null) {
      str = paramFile.getName();
    } else {
      str = null;
    }
    return addBinaryBody(paramString, paramFile, localContentType, str);
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString1, File paramFile, ContentType paramContentType, String paramString2)
  {
    return addPart(paramString1, new FileBody(paramFile, paramContentType, paramString2));
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString, InputStream paramInputStream)
  {
    return addBinaryBody(paramString, paramInputStream, ContentType.DEFAULT_BINARY, null);
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString1, InputStream paramInputStream, ContentType paramContentType, String paramString2)
  {
    return addPart(paramString1, new InputStreamBody(paramInputStream, paramContentType, paramString2));
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString, byte[] paramArrayOfByte)
  {
    return addBinaryBody(paramString, paramArrayOfByte, ContentType.DEFAULT_BINARY, null);
  }
  
  public MultipartEntityBuilder addBinaryBody(String paramString1, byte[] paramArrayOfByte, ContentType paramContentType, String paramString2)
  {
    return addPart(paramString1, new ByteArrayBody(paramArrayOfByte, paramContentType, paramString2));
  }
  
  MultipartEntityBuilder addPart(FormBodyPart paramFormBodyPart)
  {
    if (paramFormBodyPart == null) {
      return this;
    }
    if (this.bodyParts == null) {
      this.bodyParts = new ArrayList();
    }
    this.bodyParts.add(paramFormBodyPart);
    return this;
  }
  
  public MultipartEntityBuilder addPart(String paramString, ContentBody paramContentBody)
  {
    Args.notNull(paramString, "Name");
    Args.notNull(paramContentBody, "Content body");
    return addPart(new FormBodyPart(paramString, paramContentBody));
  }
  
  public MultipartEntityBuilder addTextBody(String paramString1, String paramString2)
  {
    return addTextBody(paramString1, paramString2, ContentType.DEFAULT_TEXT);
  }
  
  public MultipartEntityBuilder addTextBody(String paramString1, String paramString2, ContentType paramContentType)
  {
    return addPart(paramString1, new StringBody(paramString2, paramContentType));
  }
  
  public HttpEntity build()
  {
    return buildEntity();
  }
  
  MultipartFormEntity buildEntity()
  {
    Object localObject1 = this.subType;
    if (localObject1 == null) {
      localObject1 = "form-data";
    }
    Charset localCharset = this.charset;
    String str = this.boundary;
    if (str == null) {
      str = generateBoundary();
    }
    Object localObject2 = this.bodyParts;
    if (localObject2 != null) {
      localObject2 = new ArrayList((Collection)localObject2);
    } else {
      localObject2 = Collections.emptyList();
    }
    HttpMultipartMode localHttpMultipartMode = this.mode;
    if (localHttpMultipartMode == null) {
      localHttpMultipartMode = HttpMultipartMode.STRICT;
    }
    switch (localHttpMultipartMode)
    {
    default: 
      localObject1 = new HttpStrictMultipart((String)localObject1, localCharset, str, (List)localObject2);
      break;
    case ???: 
      localObject1 = new HttpRFC6532Multipart((String)localObject1, localCharset, str, (List)localObject2);
      break;
    case ???: 
      localObject1 = new HttpBrowserCompatibleMultipart((String)localObject1, localCharset, str, (List)localObject2);
    }
    return new MultipartFormEntity((AbstractMultipartForm)localObject1, generateContentType(str, localCharset), ((AbstractMultipartForm)localObject1).getTotalLength());
  }
  
  public MultipartEntityBuilder setBoundary(String paramString)
  {
    this.boundary = paramString;
    return this;
  }
  
  public MultipartEntityBuilder setCharset(Charset paramCharset)
  {
    this.charset = paramCharset;
    return this;
  }
  
  public MultipartEntityBuilder setLaxMode()
  {
    this.mode = HttpMultipartMode.BROWSER_COMPATIBLE;
    return this;
  }
  
  public MultipartEntityBuilder setMode(HttpMultipartMode paramHttpMultipartMode)
  {
    this.mode = paramHttpMultipartMode;
    return this;
  }
  
  public MultipartEntityBuilder setStrictMode()
  {
    this.mode = HttpMultipartMode.STRICT;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/mime/MultipartEntityBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */