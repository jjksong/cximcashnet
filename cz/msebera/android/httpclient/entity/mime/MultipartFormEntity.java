package cz.msebera.android.httpclient.entity.mime;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class MultipartFormEntity
  implements HttpEntity
{
  private final long contentLength;
  private final Header contentType;
  private final AbstractMultipartForm multipart;
  
  MultipartFormEntity(AbstractMultipartForm paramAbstractMultipartForm, String paramString, long paramLong)
  {
    this.multipart = paramAbstractMultipartForm;
    this.contentType = new BasicHeader("Content-Type", paramString);
    this.contentLength = paramLong;
  }
  
  public void consumeContent()
    throws IOException, UnsupportedOperationException
  {
    if (!isStreaming()) {
      return;
    }
    throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
  }
  
  public InputStream getContent()
    throws IOException
  {
    throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
  }
  
  public Header getContentEncoding()
  {
    return null;
  }
  
  public long getContentLength()
  {
    return this.contentLength;
  }
  
  public Header getContentType()
  {
    return this.contentType;
  }
  
  AbstractMultipartForm getMultipart()
  {
    return this.multipart;
  }
  
  public boolean isChunked()
  {
    return isRepeatable() ^ true;
  }
  
  public boolean isRepeatable()
  {
    boolean bool;
    if (this.contentLength != -1L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isStreaming()
  {
    return isRepeatable() ^ true;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    this.multipart.writeTo(paramOutputStream);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/mime/MultipartFormEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */