package cz.msebera.android.httpclient.entity.mime.content;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.util.Args;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

public class StringBody
  extends AbstractContentBody
{
  private final byte[] content;
  
  @Deprecated
  public StringBody(String paramString)
    throws UnsupportedEncodingException
  {
    this(paramString, "text/plain", Consts.ASCII);
  }
  
  public StringBody(String paramString, ContentType paramContentType)
  {
    super(paramContentType);
    Args.notNull(paramString, "Text");
    paramContentType = paramContentType.getCharset();
    if (paramContentType == null) {
      paramContentType = Consts.ASCII;
    }
    paramContentType = paramContentType.name();
    try
    {
      this.content = paramString.getBytes(paramContentType);
      return;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new UnsupportedCharsetException(paramContentType);
    }
  }
  
  @Deprecated
  public StringBody(String paramString1, String paramString2, Charset paramCharset)
    throws UnsupportedEncodingException
  {
    this(paramString1, ContentType.create(paramString2, paramCharset));
  }
  
  @Deprecated
  public StringBody(String paramString, Charset paramCharset)
    throws UnsupportedEncodingException
  {
    this(paramString, "text/plain", paramCharset);
  }
  
  @Deprecated
  public static StringBody create(String paramString)
    throws IllegalArgumentException
  {
    return create(paramString, null, null);
  }
  
  @Deprecated
  public static StringBody create(String paramString1, String paramString2, Charset paramCharset)
    throws IllegalArgumentException
  {
    try
    {
      paramString1 = new StringBody(paramString1, paramString2, paramCharset);
      return paramString1;
    }
    catch (UnsupportedEncodingException paramString2)
    {
      paramString1 = new StringBuilder();
      paramString1.append("Charset ");
      paramString1.append(paramCharset);
      paramString1.append(" is not supported");
      throw new IllegalArgumentException(paramString1.toString(), paramString2);
    }
  }
  
  @Deprecated
  public static StringBody create(String paramString, Charset paramCharset)
    throws IllegalArgumentException
  {
    return create(paramString, null, paramCharset);
  }
  
  public long getContentLength()
  {
    return this.content.length;
  }
  
  public String getFilename()
  {
    return null;
  }
  
  public Reader getReader()
  {
    Charset localCharset = getContentType().getCharset();
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(this.content);
    if (localCharset == null) {
      localCharset = Consts.ASCII;
    }
    return new InputStreamReader(localByteArrayInputStream, localCharset);
  }
  
  public String getTransferEncoding()
  {
    return "8bit";
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(this.content);
    byte[] arrayOfByte = new byte['က'];
    for (;;)
    {
      int i = localByteArrayInputStream.read(arrayOfByte);
      if (i == -1) {
        break;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
    paramOutputStream.flush();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/mime/content/StringBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */