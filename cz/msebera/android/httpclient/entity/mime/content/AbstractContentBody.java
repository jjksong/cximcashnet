package cz.msebera.android.httpclient.entity.mime.content;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.util.Args;
import java.nio.charset.Charset;

public abstract class AbstractContentBody
  implements ContentBody
{
  private final ContentType contentType;
  
  public AbstractContentBody(ContentType paramContentType)
  {
    Args.notNull(paramContentType, "Content type");
    this.contentType = paramContentType;
  }
  
  @Deprecated
  public AbstractContentBody(String paramString)
  {
    this(ContentType.parse(paramString));
  }
  
  public String getCharset()
  {
    Object localObject = this.contentType.getCharset();
    if (localObject != null) {
      localObject = ((Charset)localObject).name();
    } else {
      localObject = null;
    }
    return (String)localObject;
  }
  
  public ContentType getContentType()
  {
    return this.contentType;
  }
  
  public String getMediaType()
  {
    String str = this.contentType.getMimeType();
    int i = str.indexOf('/');
    if (i != -1) {
      return str.substring(0, i);
    }
    return str;
  }
  
  public String getMimeType()
  {
    return this.contentType.getMimeType();
  }
  
  public String getSubType()
  {
    String str = this.contentType.getMimeType();
    int i = str.indexOf('/');
    if (i != -1) {
      return str.substring(i + 1);
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/mime/content/AbstractContentBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */