package cz.msebera.android.httpclient.entity;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.message.BasicHeaderValueFormatter;
import cz.msebera.android.httpclient.message.BasicHeaderValueParser;
import cz.msebera.android.httpclient.message.ParserCursor;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import cz.msebera.android.httpclient.util.TextUtils;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Locale;

@Immutable
public final class ContentType
  implements Serializable
{
  public static final ContentType APPLICATION_ATOM_XML = create("application/atom+xml", Consts.ISO_8859_1);
  public static final ContentType APPLICATION_FORM_URLENCODED = create("application/x-www-form-urlencoded", Consts.ISO_8859_1);
  public static final ContentType APPLICATION_JSON = create("application/json", Consts.UTF_8);
  public static final ContentType APPLICATION_OCTET_STREAM;
  public static final ContentType APPLICATION_SVG_XML;
  public static final ContentType APPLICATION_XHTML_XML;
  public static final ContentType APPLICATION_XML;
  public static final ContentType DEFAULT_BINARY = APPLICATION_OCTET_STREAM;
  public static final ContentType DEFAULT_TEXT;
  public static final ContentType MULTIPART_FORM_DATA;
  public static final ContentType TEXT_HTML;
  public static final ContentType TEXT_PLAIN;
  public static final ContentType TEXT_XML;
  public static final ContentType WILDCARD;
  private static final long serialVersionUID = -7768694718232371896L;
  private final Charset charset;
  private final String mimeType;
  private final NameValuePair[] params;
  
  static
  {
    Charset localCharset = (Charset)null;
    APPLICATION_OCTET_STREAM = create("application/octet-stream", localCharset);
    APPLICATION_SVG_XML = create("application/svg+xml", Consts.ISO_8859_1);
    APPLICATION_XHTML_XML = create("application/xhtml+xml", Consts.ISO_8859_1);
    APPLICATION_XML = create("application/xml", Consts.ISO_8859_1);
    MULTIPART_FORM_DATA = create("multipart/form-data", Consts.ISO_8859_1);
    TEXT_HTML = create("text/html", Consts.ISO_8859_1);
    TEXT_PLAIN = create("text/plain", Consts.ISO_8859_1);
    TEXT_XML = create("text/xml", Consts.ISO_8859_1);
    WILDCARD = create("*/*", localCharset);
    DEFAULT_TEXT = TEXT_PLAIN;
  }
  
  ContentType(String paramString, Charset paramCharset)
  {
    this.mimeType = paramString;
    this.charset = paramCharset;
    this.params = null;
  }
  
  ContentType(String paramString, NameValuePair[] paramArrayOfNameValuePair)
    throws UnsupportedCharsetException
  {
    this.mimeType = paramString;
    this.params = paramArrayOfNameValuePair;
    paramString = getParameter("charset");
    if (!TextUtils.isBlank(paramString)) {
      paramString = Charset.forName(paramString);
    } else {
      paramString = null;
    }
    this.charset = paramString;
  }
  
  private static ContentType create(HeaderElement paramHeaderElement)
  {
    String str = paramHeaderElement.getName();
    paramHeaderElement = paramHeaderElement.getParameters();
    if ((paramHeaderElement == null) || (paramHeaderElement.length <= 0)) {
      paramHeaderElement = null;
    }
    return new ContentType(str, paramHeaderElement);
  }
  
  public static ContentType create(String paramString)
  {
    return new ContentType(paramString, (Charset)null);
  }
  
  public static ContentType create(String paramString1, String paramString2)
    throws UnsupportedCharsetException
  {
    if (!TextUtils.isBlank(paramString2)) {
      paramString2 = Charset.forName(paramString2);
    } else {
      paramString2 = null;
    }
    return create(paramString1, paramString2);
  }
  
  public static ContentType create(String paramString, Charset paramCharset)
  {
    paramString = ((String)Args.notBlank(paramString, "MIME type")).toLowerCase(Locale.ENGLISH);
    Args.check(valid(paramString), "MIME type may not contain reserved characters");
    return new ContentType(paramString, paramCharset);
  }
  
  public static ContentType get(HttpEntity paramHttpEntity)
    throws ParseException, UnsupportedCharsetException
  {
    if (paramHttpEntity == null) {
      return null;
    }
    paramHttpEntity = paramHttpEntity.getContentType();
    if (paramHttpEntity != null)
    {
      paramHttpEntity = paramHttpEntity.getElements();
      if (paramHttpEntity.length > 0) {
        return create(paramHttpEntity[0]);
      }
    }
    return null;
  }
  
  public static ContentType getOrDefault(HttpEntity paramHttpEntity)
    throws ParseException, UnsupportedCharsetException
  {
    paramHttpEntity = get(paramHttpEntity);
    if (paramHttpEntity == null) {
      paramHttpEntity = DEFAULT_TEXT;
    }
    return paramHttpEntity;
  }
  
  public static ContentType parse(String paramString)
    throws ParseException, UnsupportedCharsetException
  {
    Args.notNull(paramString, "Content type");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    Object localObject = new ParserCursor(0, paramString.length());
    localObject = BasicHeaderValueParser.INSTANCE.parseElements(localCharArrayBuffer, (ParserCursor)localObject);
    if (localObject.length > 0) {
      return create(localObject[0]);
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Invalid content type: ");
    ((StringBuilder)localObject).append(paramString);
    throw new ParseException(((StringBuilder)localObject).toString());
  }
  
  private static boolean valid(String paramString)
  {
    int i = 0;
    while (i < paramString.length())
    {
      int j = paramString.charAt(i);
      if ((j != 34) && (j != 44) && (j != 59)) {
        i++;
      } else {
        return false;
      }
    }
    return true;
  }
  
  public Charset getCharset()
  {
    return this.charset;
  }
  
  public String getMimeType()
  {
    return this.mimeType;
  }
  
  public String getParameter(String paramString)
  {
    Args.notEmpty(paramString, "Parameter name");
    NameValuePair[] arrayOfNameValuePair = this.params;
    if (arrayOfNameValuePair == null) {
      return null;
    }
    int j = arrayOfNameValuePair.length;
    for (int i = 0; i < j; i++)
    {
      NameValuePair localNameValuePair = arrayOfNameValuePair[i];
      if (localNameValuePair.getName().equalsIgnoreCase(paramString)) {
        return localNameValuePair.getValue();
      }
    }
    return null;
  }
  
  public String toString()
  {
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(64);
    localCharArrayBuffer.append(this.mimeType);
    if (this.params != null)
    {
      localCharArrayBuffer.append("; ");
      BasicHeaderValueFormatter.INSTANCE.formatParameters(localCharArrayBuffer, this.params, false);
    }
    else if (this.charset != null)
    {
      localCharArrayBuffer.append("; charset=");
      localCharArrayBuffer.append(this.charset.name());
    }
    return localCharArrayBuffer.toString();
  }
  
  public ContentType withCharset(String paramString)
  {
    return create(getMimeType(), paramString);
  }
  
  public ContentType withCharset(Charset paramCharset)
  {
    return create(getMimeType(), paramCharset);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/ContentType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */