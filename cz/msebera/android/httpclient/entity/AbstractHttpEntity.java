package cz.msebera.android.httpclient.entity;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.message.BasicHeader;
import java.io.IOException;

@NotThreadSafe
public abstract class AbstractHttpEntity
  implements HttpEntity
{
  protected static final int OUTPUT_BUFFER_SIZE = 4096;
  protected boolean chunked;
  protected Header contentEncoding;
  protected Header contentType;
  
  @Deprecated
  public void consumeContent()
    throws IOException
  {}
  
  public Header getContentEncoding()
  {
    return this.contentEncoding;
  }
  
  public Header getContentType()
  {
    return this.contentType;
  }
  
  public boolean isChunked()
  {
    return this.chunked;
  }
  
  public void setChunked(boolean paramBoolean)
  {
    this.chunked = paramBoolean;
  }
  
  public void setContentEncoding(Header paramHeader)
  {
    this.contentEncoding = paramHeader;
  }
  
  public void setContentEncoding(String paramString)
  {
    if (paramString != null) {
      paramString = new BasicHeader("Content-Encoding", paramString);
    } else {
      paramString = null;
    }
    setContentEncoding(paramString);
  }
  
  public void setContentType(Header paramHeader)
  {
    this.contentType = paramHeader;
  }
  
  public void setContentType(String paramString)
  {
    if (paramString != null) {
      paramString = new BasicHeader("Content-Type", paramString);
    } else {
      paramString = null;
    }
    setContentType(paramString);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append('[');
    if (this.contentType != null)
    {
      localStringBuilder.append("Content-Type: ");
      localStringBuilder.append(this.contentType.getValue());
      localStringBuilder.append(',');
    }
    if (this.contentEncoding != null)
    {
      localStringBuilder.append("Content-Encoding: ");
      localStringBuilder.append(this.contentEncoding.getValue());
      localStringBuilder.append(',');
    }
    long l = getContentLength();
    if (l >= 0L)
    {
      localStringBuilder.append("Content-Length: ");
      localStringBuilder.append(l);
      localStringBuilder.append(',');
    }
    localStringBuilder.append("Chunked: ");
    localStringBuilder.append(this.chunked);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/AbstractHttpEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */