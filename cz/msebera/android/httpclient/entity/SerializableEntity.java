package cz.msebera.android.httpclient.entity;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

@NotThreadSafe
public class SerializableEntity
  extends AbstractHttpEntity
{
  private Serializable objRef;
  private byte[] objSer;
  
  public SerializableEntity(Serializable paramSerializable)
  {
    Args.notNull(paramSerializable, "Source object");
    this.objRef = paramSerializable;
  }
  
  public SerializableEntity(Serializable paramSerializable, boolean paramBoolean)
    throws IOException
  {
    Args.notNull(paramSerializable, "Source object");
    if (paramBoolean) {
      createBytes(paramSerializable);
    } else {
      this.objRef = paramSerializable;
    }
  }
  
  private void createBytes(Serializable paramSerializable)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(localByteArrayOutputStream);
    localObjectOutputStream.writeObject(paramSerializable);
    localObjectOutputStream.flush();
    this.objSer = localByteArrayOutputStream.toByteArray();
  }
  
  public InputStream getContent()
    throws IOException, IllegalStateException
  {
    if (this.objSer == null) {
      createBytes(this.objRef);
    }
    return new ByteArrayInputStream(this.objSer);
  }
  
  public long getContentLength()
  {
    byte[] arrayOfByte = this.objSer;
    if (arrayOfByte == null) {
      return -1L;
    }
    return arrayOfByte.length;
  }
  
  public boolean isRepeatable()
  {
    return true;
  }
  
  public boolean isStreaming()
  {
    boolean bool;
    if (this.objSer == null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    byte[] arrayOfByte = this.objSer;
    if (arrayOfByte == null)
    {
      paramOutputStream = new ObjectOutputStream(paramOutputStream);
      paramOutputStream.writeObject(this.objRef);
      paramOutputStream.flush();
    }
    else
    {
      paramOutputStream.write(arrayOfByte);
      paramOutputStream.flush();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/SerializableEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */