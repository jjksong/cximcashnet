package cz.msebera.android.httpclient.entity;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@NotThreadSafe
public class BufferedHttpEntity
  extends HttpEntityWrapper
{
  private final byte[] buffer;
  
  public BufferedHttpEntity(HttpEntity paramHttpEntity)
    throws IOException
  {
    super(paramHttpEntity);
    if ((paramHttpEntity.isRepeatable()) && (paramHttpEntity.getContentLength() >= 0L)) {
      this.buffer = null;
    } else {
      this.buffer = EntityUtils.toByteArray(paramHttpEntity);
    }
  }
  
  public InputStream getContent()
    throws IOException
  {
    byte[] arrayOfByte = this.buffer;
    if (arrayOfByte != null) {
      return new ByteArrayInputStream(arrayOfByte);
    }
    return super.getContent();
  }
  
  public long getContentLength()
  {
    byte[] arrayOfByte = this.buffer;
    if (arrayOfByte != null) {
      return arrayOfByte.length;
    }
    return super.getContentLength();
  }
  
  public boolean isChunked()
  {
    boolean bool;
    if ((this.buffer == null) && (super.isChunked())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isRepeatable()
  {
    return true;
  }
  
  public boolean isStreaming()
  {
    boolean bool;
    if ((this.buffer == null) && (super.isStreaming())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    byte[] arrayOfByte = this.buffer;
    if (arrayOfByte != null) {
      paramOutputStream.write(arrayOfByte);
    } else {
      super.writeTo(paramOutputStream);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/entity/BufferedHttpEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */