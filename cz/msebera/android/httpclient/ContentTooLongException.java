package cz.msebera.android.httpclient;

import java.io.IOException;

public class ContentTooLongException
  extends IOException
{
  private static final long serialVersionUID = -924287689552495383L;
  
  public ContentTooLongException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/ContentTooLongException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */