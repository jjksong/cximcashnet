package cz.msebera.android.httpclient.params;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
@ThreadSafe
public class BasicHttpParams
  extends AbstractHttpParams
  implements Serializable, Cloneable
{
  private static final long serialVersionUID = -7086398485908701455L;
  private final Map<String, Object> parameters = new ConcurrentHashMap();
  
  public void clear()
  {
    this.parameters.clear();
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    BasicHttpParams localBasicHttpParams = (BasicHttpParams)super.clone();
    copyParams(localBasicHttpParams);
    return localBasicHttpParams;
  }
  
  public HttpParams copy()
  {
    try
    {
      HttpParams localHttpParams = (HttpParams)clone();
      return localHttpParams;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new UnsupportedOperationException("Cloning not supported");
    }
  }
  
  public void copyParams(HttpParams paramHttpParams)
  {
    Iterator localIterator = this.parameters.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      paramHttpParams.setParameter((String)localEntry.getKey(), localEntry.getValue());
    }
  }
  
  public Set<String> getNames()
  {
    return new HashSet(this.parameters.keySet());
  }
  
  public Object getParameter(String paramString)
  {
    return this.parameters.get(paramString);
  }
  
  public boolean isParameterSet(String paramString)
  {
    boolean bool;
    if (getParameter(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isParameterSetLocally(String paramString)
  {
    boolean bool;
    if (this.parameters.get(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean removeParameter(String paramString)
  {
    if (this.parameters.containsKey(paramString))
    {
      this.parameters.remove(paramString);
      return true;
    }
    return false;
  }
  
  public HttpParams setParameter(String paramString, Object paramObject)
  {
    if (paramString == null) {
      return this;
    }
    if (paramObject != null) {
      this.parameters.put(paramString, paramObject);
    } else {
      this.parameters.remove(paramString);
    }
    return this;
  }
  
  public void setParameters(String[] paramArrayOfString, Object paramObject)
  {
    int j = paramArrayOfString.length;
    for (int i = 0; i < j; i++) {
      setParameter(paramArrayOfString[i], paramObject);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/params/BasicHttpParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */