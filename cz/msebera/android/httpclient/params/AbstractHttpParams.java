package cz.msebera.android.httpclient.params;

import java.util.Set;

@Deprecated
public abstract class AbstractHttpParams
  implements HttpParams, HttpParamsNames
{
  public boolean getBooleanParameter(String paramString, boolean paramBoolean)
  {
    paramString = getParameter(paramString);
    if (paramString == null) {
      return paramBoolean;
    }
    return ((Boolean)paramString).booleanValue();
  }
  
  public double getDoubleParameter(String paramString, double paramDouble)
  {
    paramString = getParameter(paramString);
    if (paramString == null) {
      return paramDouble;
    }
    return ((Double)paramString).doubleValue();
  }
  
  public int getIntParameter(String paramString, int paramInt)
  {
    paramString = getParameter(paramString);
    if (paramString == null) {
      return paramInt;
    }
    return ((Integer)paramString).intValue();
  }
  
  public long getLongParameter(String paramString, long paramLong)
  {
    paramString = getParameter(paramString);
    if (paramString == null) {
      return paramLong;
    }
    return ((Long)paramString).longValue();
  }
  
  public Set<String> getNames()
  {
    throw new UnsupportedOperationException();
  }
  
  public boolean isParameterFalse(String paramString)
  {
    return getBooleanParameter(paramString, false) ^ true;
  }
  
  public boolean isParameterTrue(String paramString)
  {
    return getBooleanParameter(paramString, false);
  }
  
  public HttpParams setBooleanParameter(String paramString, boolean paramBoolean)
  {
    Boolean localBoolean;
    if (paramBoolean) {
      localBoolean = Boolean.TRUE;
    } else {
      localBoolean = Boolean.FALSE;
    }
    setParameter(paramString, localBoolean);
    return this;
  }
  
  public HttpParams setDoubleParameter(String paramString, double paramDouble)
  {
    setParameter(paramString, Double.valueOf(paramDouble));
    return this;
  }
  
  public HttpParams setIntParameter(String paramString, int paramInt)
  {
    setParameter(paramString, Integer.valueOf(paramInt));
    return this;
  }
  
  public HttpParams setLongParameter(String paramString, long paramLong)
  {
    setParameter(paramString, Long.valueOf(paramLong));
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/params/AbstractHttpParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */