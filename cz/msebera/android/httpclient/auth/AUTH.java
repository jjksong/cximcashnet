package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public final class AUTH
{
  public static final String PROXY_AUTH = "Proxy-Authenticate";
  public static final String PROXY_AUTH_RESP = "Proxy-Authorization";
  public static final String WWW_AUTH = "WWW-Authenticate";
  public static final String WWW_AUTH_RESP = "Authorization";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/AUTH.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */