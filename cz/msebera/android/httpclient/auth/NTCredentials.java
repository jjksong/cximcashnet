package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Serializable;
import java.security.Principal;
import java.util.Locale;

@Immutable
public class NTCredentials
  implements Credentials, Serializable
{
  private static final long serialVersionUID = -7385699315228907265L;
  private final String password;
  private final NTUserPrincipal principal;
  private final String workstation;
  
  public NTCredentials(String paramString)
  {
    Args.notNull(paramString, "Username:password string");
    int i = paramString.indexOf(':');
    if (i >= 0)
    {
      String str = paramString.substring(0, i);
      this.password = paramString.substring(i + 1);
      paramString = str;
    }
    else
    {
      this.password = null;
    }
    i = paramString.indexOf('/');
    if (i >= 0) {
      this.principal = new NTUserPrincipal(paramString.substring(0, i).toUpperCase(Locale.ENGLISH), paramString.substring(i + 1));
    } else {
      this.principal = new NTUserPrincipal(null, paramString.substring(i + 1));
    }
    this.workstation = null;
  }
  
  public NTCredentials(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Args.notNull(paramString1, "User name");
    this.principal = new NTUserPrincipal(paramString4, paramString1);
    this.password = paramString2;
    if (paramString3 != null) {
      this.workstation = paramString3.toUpperCase(Locale.ENGLISH);
    } else {
      this.workstation = null;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof NTCredentials))
    {
      paramObject = (NTCredentials)paramObject;
      if ((LangUtils.equals(this.principal, ((NTCredentials)paramObject).principal)) && (LangUtils.equals(this.workstation, ((NTCredentials)paramObject).workstation))) {
        return true;
      }
    }
    return false;
  }
  
  public String getDomain()
  {
    return this.principal.getDomain();
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public String getUserName()
  {
    return this.principal.getUsername();
  }
  
  public Principal getUserPrincipal()
  {
    return this.principal;
  }
  
  public String getWorkstation()
  {
    return this.workstation;
  }
  
  public int hashCode()
  {
    return LangUtils.hashCode(LangUtils.hashCode(17, this.principal), this.workstation);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[principal: ");
    localStringBuilder.append(this.principal);
    localStringBuilder.append("][workstation: ");
    localStringBuilder.append(this.workstation);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/NTCredentials.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */