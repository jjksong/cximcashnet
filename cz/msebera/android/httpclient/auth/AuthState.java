package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.Queue;

@NotThreadSafe
public class AuthState
{
  private Queue<AuthOption> authOptions;
  private AuthScheme authScheme;
  private AuthScope authScope;
  private Credentials credentials;
  private AuthProtocolState state = AuthProtocolState.UNCHALLENGED;
  
  public Queue<AuthOption> getAuthOptions()
  {
    return this.authOptions;
  }
  
  public AuthScheme getAuthScheme()
  {
    return this.authScheme;
  }
  
  @Deprecated
  public AuthScope getAuthScope()
  {
    return this.authScope;
  }
  
  public Credentials getCredentials()
  {
    return this.credentials;
  }
  
  public AuthProtocolState getState()
  {
    return this.state;
  }
  
  public boolean hasAuthOptions()
  {
    Queue localQueue = this.authOptions;
    boolean bool;
    if ((localQueue != null) && (!localQueue.isEmpty())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  @Deprecated
  public void invalidate()
  {
    reset();
  }
  
  @Deprecated
  public boolean isValid()
  {
    boolean bool;
    if (this.authScheme != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void reset()
  {
    this.state = AuthProtocolState.UNCHALLENGED;
    this.authOptions = null;
    this.authScheme = null;
    this.authScope = null;
    this.credentials = null;
  }
  
  @Deprecated
  public void setAuthScheme(AuthScheme paramAuthScheme)
  {
    if (paramAuthScheme == null)
    {
      reset();
      return;
    }
    this.authScheme = paramAuthScheme;
  }
  
  @Deprecated
  public void setAuthScope(AuthScope paramAuthScope)
  {
    this.authScope = paramAuthScope;
  }
  
  @Deprecated
  public void setCredentials(Credentials paramCredentials)
  {
    this.credentials = paramCredentials;
  }
  
  public void setState(AuthProtocolState paramAuthProtocolState)
  {
    if (paramAuthProtocolState == null) {
      paramAuthProtocolState = AuthProtocolState.UNCHALLENGED;
    }
    this.state = paramAuthProtocolState;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("state:");
    localStringBuilder.append(this.state);
    localStringBuilder.append(";");
    if (this.authScheme != null)
    {
      localStringBuilder.append("auth scheme:");
      localStringBuilder.append(this.authScheme.getSchemeName());
      localStringBuilder.append(";");
    }
    if (this.credentials != null) {
      localStringBuilder.append("credentials present");
    }
    return localStringBuilder.toString();
  }
  
  public void update(AuthScheme paramAuthScheme, Credentials paramCredentials)
  {
    Args.notNull(paramAuthScheme, "Auth scheme");
    Args.notNull(paramCredentials, "Credentials");
    this.authScheme = paramAuthScheme;
    this.credentials = paramCredentials;
    this.authOptions = null;
  }
  
  public void update(Queue<AuthOption> paramQueue)
  {
    Args.notEmpty(paramQueue, "Queue of auth options");
    this.authOptions = paramQueue;
    this.authScheme = null;
    this.credentials = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/AuthState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */