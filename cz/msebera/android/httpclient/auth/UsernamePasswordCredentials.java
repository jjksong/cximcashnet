package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Serializable;
import java.security.Principal;

@Immutable
public class UsernamePasswordCredentials
  implements Credentials, Serializable
{
  private static final long serialVersionUID = 243343858802739403L;
  private final String password;
  private final BasicUserPrincipal principal;
  
  public UsernamePasswordCredentials(String paramString)
  {
    Args.notNull(paramString, "Username:password string");
    int i = paramString.indexOf(':');
    if (i >= 0)
    {
      this.principal = new BasicUserPrincipal(paramString.substring(0, i));
      this.password = paramString.substring(i + 1);
    }
    else
    {
      this.principal = new BasicUserPrincipal(paramString);
      this.password = null;
    }
  }
  
  public UsernamePasswordCredentials(String paramString1, String paramString2)
  {
    Args.notNull(paramString1, "Username");
    this.principal = new BasicUserPrincipal(paramString1);
    this.password = paramString2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof UsernamePasswordCredentials))
    {
      paramObject = (UsernamePasswordCredentials)paramObject;
      if (LangUtils.equals(this.principal, ((UsernamePasswordCredentials)paramObject).principal)) {
        return true;
      }
    }
    return false;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public String getUserName()
  {
    return this.principal.getName();
  }
  
  public Principal getUserPrincipal()
  {
    return this.principal;
  }
  
  public int hashCode()
  {
    return this.principal.hashCode();
  }
  
  public String toString()
  {
    return this.principal.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/UsernamePasswordCredentials.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */