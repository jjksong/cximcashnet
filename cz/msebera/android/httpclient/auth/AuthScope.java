package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.util.Locale;

@Immutable
public class AuthScope
{
  public static final AuthScope ANY = new AuthScope(ANY_HOST, -1, ANY_REALM, ANY_SCHEME);
  public static final String ANY_HOST;
  public static final int ANY_PORT = -1;
  public static final String ANY_REALM;
  public static final String ANY_SCHEME;
  private final String host;
  private final int port;
  private final String realm;
  private final String scheme;
  
  public AuthScope(HttpHost paramHttpHost)
  {
    this(paramHttpHost, ANY_REALM, ANY_SCHEME);
  }
  
  public AuthScope(HttpHost paramHttpHost, String paramString1, String paramString2)
  {
    this(paramHttpHost.getHostName(), paramHttpHost.getPort(), paramString1, paramString2);
  }
  
  public AuthScope(AuthScope paramAuthScope)
  {
    Args.notNull(paramAuthScope, "Scope");
    this.host = paramAuthScope.getHost();
    this.port = paramAuthScope.getPort();
    this.realm = paramAuthScope.getRealm();
    this.scheme = paramAuthScope.getScheme();
  }
  
  public AuthScope(String paramString, int paramInt)
  {
    this(paramString, paramInt, ANY_REALM, ANY_SCHEME);
  }
  
  public AuthScope(String paramString1, int paramInt, String paramString2)
  {
    this(paramString1, paramInt, paramString2, ANY_SCHEME);
  }
  
  public AuthScope(String paramString1, int paramInt, String paramString2, String paramString3)
  {
    if (paramString1 == null) {
      paramString1 = ANY_HOST;
    } else {
      paramString1 = paramString1.toLowerCase(Locale.ENGLISH);
    }
    this.host = paramString1;
    int i = paramInt;
    if (paramInt < 0) {
      i = -1;
    }
    this.port = i;
    paramString1 = paramString2;
    if (paramString2 == null) {
      paramString1 = ANY_REALM;
    }
    this.realm = paramString1;
    if (paramString3 == null) {
      paramString1 = ANY_SCHEME;
    } else {
      paramString1 = paramString3.toUpperCase(Locale.ENGLISH);
    }
    this.scheme = paramString1;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (!(paramObject instanceof AuthScope)) {
      return super.equals(paramObject);
    }
    paramObject = (AuthScope)paramObject;
    boolean bool1 = bool2;
    if (LangUtils.equals(this.host, ((AuthScope)paramObject).host))
    {
      bool1 = bool2;
      if (this.port == ((AuthScope)paramObject).port)
      {
        bool1 = bool2;
        if (LangUtils.equals(this.realm, ((AuthScope)paramObject).realm))
        {
          bool1 = bool2;
          if (LangUtils.equals(this.scheme, ((AuthScope)paramObject).scheme)) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  public String getHost()
  {
    return this.host;
  }
  
  public int getPort()
  {
    return this.port;
  }
  
  public String getRealm()
  {
    return this.realm;
  }
  
  public String getScheme()
  {
    return this.scheme;
  }
  
  public int hashCode()
  {
    return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, this.host), this.port), this.realm), this.scheme);
  }
  
  public int match(AuthScope paramAuthScope)
  {
    int i;
    String str2;
    String str1;
    if (LangUtils.equals(this.scheme, paramAuthScope.scheme))
    {
      i = 1;
    }
    else
    {
      str2 = this.scheme;
      str1 = ANY_SCHEME;
      if ((str2 != str1) && (paramAuthScope.scheme != str1)) {
        return -1;
      }
      i = 0;
    }
    int j;
    if (LangUtils.equals(this.realm, paramAuthScope.realm))
    {
      j = i + 2;
    }
    else
    {
      str2 = this.realm;
      str1 = ANY_REALM;
      j = i;
      if (str2 != str1)
      {
        j = i;
        if (paramAuthScope.realm != str1) {
          return -1;
        }
      }
    }
    int m = this.port;
    int k = paramAuthScope.port;
    if (m == k)
    {
      i = j + 4;
    }
    else
    {
      i = j;
      if (m != -1)
      {
        i = j;
        if (k != -1) {
          return -1;
        }
      }
    }
    if (LangUtils.equals(this.host, paramAuthScope.host))
    {
      j = i + 8;
    }
    else
    {
      str1 = this.host;
      str2 = ANY_HOST;
      j = i;
      if (str1 != str2)
      {
        j = i;
        if (paramAuthScope.host != str2) {
          return -1;
        }
      }
    }
    return j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    String str = this.scheme;
    if (str != null)
    {
      localStringBuilder.append(str.toUpperCase(Locale.ENGLISH));
      localStringBuilder.append(' ');
    }
    if (this.realm != null)
    {
      localStringBuilder.append('\'');
      localStringBuilder.append(this.realm);
      localStringBuilder.append('\'');
    }
    else
    {
      localStringBuilder.append("<any realm>");
    }
    if (this.host != null)
    {
      localStringBuilder.append('@');
      localStringBuilder.append(this.host);
      if (this.port >= 0)
      {
        localStringBuilder.append(':');
        localStringBuilder.append(this.port);
      }
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/AuthScope.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */