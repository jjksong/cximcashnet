package cz.msebera.android.httpclient.auth.params;

@Deprecated
public abstract interface AuthPNames
{
  public static final String CREDENTIAL_CHARSET = "http.auth.credential-charset";
  public static final String PROXY_AUTH_PREF = "http.auth.proxy-scheme-pref";
  public static final String TARGET_AUTH_PREF = "http.auth.target-scheme-pref";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/params/AuthPNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */