package cz.msebera.android.httpclient.auth;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Serializable;
import java.security.Principal;
import java.util.Locale;

@Immutable
public class NTUserPrincipal
  implements Principal, Serializable
{
  private static final long serialVersionUID = -6870169797924406894L;
  private final String domain;
  private final String ntname;
  private final String username;
  
  public NTUserPrincipal(String paramString1, String paramString2)
  {
    Args.notNull(paramString2, "User name");
    this.username = paramString2;
    if (paramString1 != null) {
      this.domain = paramString1.toUpperCase(Locale.ENGLISH);
    } else {
      this.domain = null;
    }
    paramString1 = this.domain;
    if ((paramString1 != null) && (paramString1.length() > 0))
    {
      paramString1 = new StringBuilder();
      paramString1.append(this.domain);
      paramString1.append('\\');
      paramString1.append(this.username);
      this.ntname = paramString1.toString();
    }
    else
    {
      this.ntname = this.username;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof NTUserPrincipal))
    {
      paramObject = (NTUserPrincipal)paramObject;
      if ((LangUtils.equals(this.username, ((NTUserPrincipal)paramObject).username)) && (LangUtils.equals(this.domain, ((NTUserPrincipal)paramObject).domain))) {
        return true;
      }
    }
    return false;
  }
  
  public String getDomain()
  {
    return this.domain;
  }
  
  public String getName()
  {
    return this.ntname;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public int hashCode()
  {
    return LangUtils.hashCode(LangUtils.hashCode(17, this.username), this.domain);
  }
  
  public String toString()
  {
    return this.ntname;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/auth/NTUserPrincipal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */