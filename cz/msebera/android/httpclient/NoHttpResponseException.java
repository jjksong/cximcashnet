package cz.msebera.android.httpclient;

import java.io.IOException;

public class NoHttpResponseException
  extends IOException
{
  private static final long serialVersionUID = -7658940387386078766L;
  
  public NoHttpResponseException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/NoHttpResponseException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */