package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class RequestUserAgent
  implements HttpRequestInterceptor
{
  private final String userAgent;
  
  public RequestUserAgent()
  {
    this(null);
  }
  
  public RequestUserAgent(String paramString)
  {
    this.userAgent = paramString;
  }
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    if (!paramHttpRequest.containsHeader("User-Agent"))
    {
      paramHttpContext = null;
      Object localObject = paramHttpRequest.getParams();
      if (localObject != null) {
        paramHttpContext = (String)((HttpParams)localObject).getParameter("http.useragent");
      }
      localObject = paramHttpContext;
      if (paramHttpContext == null) {
        localObject = this.userAgent;
      }
      if (localObject != null) {
        paramHttpRequest.addHeader("User-Agent", (String)localObject);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/RequestUserAgent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */