package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpInetConnection;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.InetAddress;

@Immutable
public class RequestTargetHost
  implements HttpRequestInterceptor
{
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    paramHttpContext = HttpCoreContext.adapt(paramHttpContext);
    ProtocolVersion localProtocolVersion = paramHttpRequest.getRequestLine().getProtocolVersion();
    if ((paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("CONNECT")) && (localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0))) {
      return;
    }
    if (!paramHttpRequest.containsHeader("Host"))
    {
      HttpHost localHttpHost = paramHttpContext.getTargetHost();
      Object localObject = localHttpHost;
      if (localHttpHost == null)
      {
        localObject = paramHttpContext.getConnection();
        paramHttpContext = localHttpHost;
        if ((localObject instanceof HttpInetConnection))
        {
          paramHttpContext = (HttpInetConnection)localObject;
          localObject = paramHttpContext.getRemoteAddress();
          int i = paramHttpContext.getRemotePort();
          paramHttpContext = localHttpHost;
          if (localObject != null) {
            paramHttpContext = new HttpHost(((InetAddress)localObject).getHostName(), i);
          }
        }
        localObject = paramHttpContext;
        if (paramHttpContext == null)
        {
          if (localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0)) {
            return;
          }
          throw new ProtocolException("Target host missing");
        }
      }
      paramHttpRequest.addHeader("Host", ((HttpHost)localObject).toHostString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/RequestTargetHost.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */