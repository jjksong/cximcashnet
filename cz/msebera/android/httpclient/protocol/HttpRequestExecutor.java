package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class HttpRequestExecutor
{
  public static final int DEFAULT_WAIT_FOR_CONTINUE = 3000;
  private final int waitForContinue;
  
  public HttpRequestExecutor()
  {
    this(3000);
  }
  
  public HttpRequestExecutor(int paramInt)
  {
    this.waitForContinue = Args.positive(paramInt, "Wait for continue time");
  }
  
  private static void closeConnection(HttpClientConnection paramHttpClientConnection)
  {
    try
    {
      paramHttpClientConnection.close();
      return;
    }
    catch (IOException paramHttpClientConnection)
    {
      for (;;) {}
    }
  }
  
  protected boolean canResponseHaveBody(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    boolean bool1 = "HEAD".equalsIgnoreCase(paramHttpRequest.getRequestLine().getMethod());
    boolean bool2 = false;
    if (bool1) {
      return false;
    }
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    bool1 = bool2;
    if (i >= 200)
    {
      bool1 = bool2;
      if (i != 204)
      {
        bool1 = bool2;
        if (i != 304)
        {
          bool1 = bool2;
          if (i != 205) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  protected HttpResponse doReceiveResponse(HttpRequest paramHttpRequest, HttpClientConnection paramHttpClientConnection, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpClientConnection, "Client connection");
    Args.notNull(paramHttpContext, "HTTP context");
    paramHttpContext = null;
    for (int i = 0;; i = paramHttpContext.getStatusLine().getStatusCode())
    {
      if ((paramHttpContext != null) && (i >= 200)) {
        return paramHttpContext;
      }
      paramHttpContext = paramHttpClientConnection.receiveResponseHeader();
      if (canResponseHaveBody(paramHttpRequest, paramHttpContext)) {
        paramHttpClientConnection.receiveResponseEntity(paramHttpContext);
      }
    }
  }
  
  protected HttpResponse doSendRequest(HttpRequest paramHttpRequest, HttpClientConnection paramHttpClientConnection, HttpContext paramHttpContext)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpClientConnection, "Client connection");
    Args.notNull(paramHttpContext, "HTTP context");
    paramHttpContext.setAttribute("http.connection", paramHttpClientConnection);
    paramHttpContext.setAttribute("http.request_sent", Boolean.FALSE);
    paramHttpClientConnection.sendRequestHeader(paramHttpRequest);
    boolean bool = paramHttpRequest instanceof HttpEntityEnclosingRequest;
    Object localObject2 = null;
    Object localObject3 = null;
    if (bool)
    {
      int j = 1;
      localObject2 = paramHttpRequest.getRequestLine().getProtocolVersion();
      HttpEntityEnclosingRequest localHttpEntityEnclosingRequest = (HttpEntityEnclosingRequest)paramHttpRequest;
      int i = j;
      Object localObject1 = localObject3;
      if (localHttpEntityEnclosingRequest.expectContinue())
      {
        i = j;
        localObject1 = localObject3;
        if (!((ProtocolVersion)localObject2).lessEquals(HttpVersion.HTTP_1_0))
        {
          paramHttpClientConnection.flush();
          i = j;
          localObject1 = localObject3;
          if (paramHttpClientConnection.isResponseAvailable(this.waitForContinue))
          {
            localObject1 = paramHttpClientConnection.receiveResponseHeader();
            if (canResponseHaveBody(paramHttpRequest, (HttpResponse)localObject1)) {
              paramHttpClientConnection.receiveResponseEntity((HttpResponse)localObject1);
            }
            i = ((HttpResponse)localObject1).getStatusLine().getStatusCode();
            if (i < 200)
            {
              if (i == 100)
              {
                i = j;
                localObject1 = localObject3;
              }
              else
              {
                paramHttpRequest = new StringBuilder();
                paramHttpRequest.append("Unexpected response: ");
                paramHttpRequest.append(((HttpResponse)localObject1).getStatusLine());
                throw new ProtocolException(paramHttpRequest.toString());
              }
            }
            else {
              i = 0;
            }
          }
        }
      }
      localObject2 = localObject1;
      if (i != 0)
      {
        paramHttpClientConnection.sendRequestEntity(localHttpEntityEnclosingRequest);
        localObject2 = localObject1;
      }
    }
    paramHttpClientConnection.flush();
    paramHttpContext.setAttribute("http.request_sent", Boolean.TRUE);
    return (HttpResponse)localObject2;
  }
  
  public HttpResponse execute(HttpRequest paramHttpRequest, HttpClientConnection paramHttpClientConnection, HttpContext paramHttpContext)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpClientConnection, "Client connection");
    Args.notNull(paramHttpContext, "HTTP context");
    try
    {
      HttpResponse localHttpResponse2 = doSendRequest(paramHttpRequest, paramHttpClientConnection, paramHttpContext);
      HttpResponse localHttpResponse1 = localHttpResponse2;
      if (localHttpResponse2 == null) {
        localHttpResponse1 = doReceiveResponse(paramHttpRequest, paramHttpClientConnection, paramHttpContext);
      }
      return localHttpResponse1;
    }
    catch (RuntimeException paramHttpRequest)
    {
      closeConnection(paramHttpClientConnection);
      throw paramHttpRequest;
    }
    catch (HttpException paramHttpRequest)
    {
      closeConnection(paramHttpClientConnection);
      throw paramHttpRequest;
    }
    catch (IOException paramHttpRequest)
    {
      closeConnection(paramHttpClientConnection);
      throw paramHttpRequest;
    }
  }
  
  public void postProcess(HttpResponse paramHttpResponse, HttpProcessor paramHttpProcessor, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    Args.notNull(paramHttpProcessor, "HTTP processor");
    Args.notNull(paramHttpContext, "HTTP context");
    paramHttpContext.setAttribute("http.response", paramHttpResponse);
    paramHttpProcessor.process(paramHttpResponse, paramHttpContext);
  }
  
  public void preProcess(HttpRequest paramHttpRequest, HttpProcessor paramHttpProcessor, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpProcessor, "HTTP processor");
    Args.notNull(paramHttpContext, "HTTP context");
    paramHttpContext.setAttribute("http.request", paramHttpRequest);
    paramHttpProcessor.process(paramHttpRequest, paramHttpContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/HttpRequestExecutor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */