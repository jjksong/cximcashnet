package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.util.Args;

@ThreadSafe
public class UriHttpRequestHandlerMapper
  implements HttpRequestHandlerMapper
{
  private final UriPatternMatcher<HttpRequestHandler> matcher;
  
  public UriHttpRequestHandlerMapper()
  {
    this(new UriPatternMatcher());
  }
  
  protected UriHttpRequestHandlerMapper(UriPatternMatcher<HttpRequestHandler> paramUriPatternMatcher)
  {
    this.matcher = ((UriPatternMatcher)Args.notNull(paramUriPatternMatcher, "Pattern matcher"));
  }
  
  protected String getRequestPath(HttpRequest paramHttpRequest)
  {
    String str = paramHttpRequest.getRequestLine().getUri();
    int i = str.indexOf("?");
    if (i != -1)
    {
      paramHttpRequest = str.substring(0, i);
    }
    else
    {
      i = str.indexOf("#");
      paramHttpRequest = str;
      if (i != -1) {
        paramHttpRequest = str.substring(0, i);
      }
    }
    return paramHttpRequest;
  }
  
  public HttpRequestHandler lookup(HttpRequest paramHttpRequest)
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    return (HttpRequestHandler)this.matcher.lookup(getRequestPath(paramHttpRequest));
  }
  
  public void register(String paramString, HttpRequestHandler paramHttpRequestHandler)
  {
    Args.notNull(paramString, "Pattern");
    Args.notNull(paramHttpRequestHandler, "Handler");
    this.matcher.register(paramString, paramHttpRequestHandler);
  }
  
  public void unregister(String paramString)
  {
    this.matcher.unregister(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/UriHttpRequestHandlerMapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */