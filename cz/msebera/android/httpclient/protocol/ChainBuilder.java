package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

@NotThreadSafe
final class ChainBuilder<E>
{
  private final LinkedList<E> list = new LinkedList();
  private final Map<Class<?>, E> uniqueClasses = new HashMap();
  
  private void ensureUnique(E paramE)
  {
    Object localObject = this.uniqueClasses.remove(paramE.getClass());
    if (localObject != null) {
      this.list.remove(localObject);
    }
    this.uniqueClasses.put(paramE.getClass(), paramE);
  }
  
  public ChainBuilder<E> addAllFirst(Collection<E> paramCollection)
  {
    if (paramCollection == null) {
      return this;
    }
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      addFirst(paramCollection.next());
    }
    return this;
  }
  
  public ChainBuilder<E> addAllFirst(E... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++) {
      addFirst(paramVarArgs[i]);
    }
    return this;
  }
  
  public ChainBuilder<E> addAllLast(Collection<E> paramCollection)
  {
    if (paramCollection == null) {
      return this;
    }
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      addLast(paramCollection.next());
    }
    return this;
  }
  
  public ChainBuilder<E> addAllLast(E... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++) {
      addLast(paramVarArgs[i]);
    }
    return this;
  }
  
  public ChainBuilder<E> addFirst(E paramE)
  {
    if (paramE == null) {
      return this;
    }
    ensureUnique(paramE);
    this.list.addFirst(paramE);
    return this;
  }
  
  public ChainBuilder<E> addLast(E paramE)
  {
    if (paramE == null) {
      return this;
    }
    ensureUnique(paramE);
    this.list.addLast(paramE);
    return this;
  }
  
  public LinkedList<E> build()
  {
    return new LinkedList(this.list);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/ChainBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */