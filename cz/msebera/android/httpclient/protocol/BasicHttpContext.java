package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ThreadSafe
public class BasicHttpContext
  implements HttpContext
{
  private final Map<String, Object> map = new ConcurrentHashMap();
  private final HttpContext parentContext;
  
  public BasicHttpContext()
  {
    this(null);
  }
  
  public BasicHttpContext(HttpContext paramHttpContext)
  {
    this.parentContext = paramHttpContext;
  }
  
  public void clear()
  {
    this.map.clear();
  }
  
  public Object getAttribute(String paramString)
  {
    Args.notNull(paramString, "Id");
    Object localObject2 = this.map.get(paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      HttpContext localHttpContext = this.parentContext;
      localObject1 = localObject2;
      if (localHttpContext != null) {
        localObject1 = localHttpContext.getAttribute(paramString);
      }
    }
    return localObject1;
  }
  
  public Object removeAttribute(String paramString)
  {
    Args.notNull(paramString, "Id");
    return this.map.remove(paramString);
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    Args.notNull(paramString, "Id");
    if (paramObject != null) {
      this.map.put(paramString, paramObject);
    } else {
      this.map.remove(paramString);
    }
  }
  
  public String toString()
  {
    return this.map.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/BasicHttpContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */