package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class ResponseContent
  implements HttpResponseInterceptor
{
  private final boolean overwrite;
  
  public ResponseContent()
  {
    this(false);
  }
  
  public ResponseContent(boolean paramBoolean)
  {
    this.overwrite = paramBoolean;
  }
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    if (this.overwrite)
    {
      paramHttpResponse.removeHeaders("Transfer-Encoding");
      paramHttpResponse.removeHeaders("Content-Length");
    }
    else
    {
      if (paramHttpResponse.containsHeader("Transfer-Encoding")) {
        break label265;
      }
      if (paramHttpResponse.containsHeader("Content-Length")) {
        break label255;
      }
    }
    ProtocolVersion localProtocolVersion = paramHttpResponse.getStatusLine().getProtocolVersion();
    paramHttpContext = paramHttpResponse.getEntity();
    if (paramHttpContext != null)
    {
      long l = paramHttpContext.getContentLength();
      if ((paramHttpContext.isChunked()) && (!localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0))) {
        paramHttpResponse.addHeader("Transfer-Encoding", "chunked");
      } else if (l >= 0L) {
        paramHttpResponse.addHeader("Content-Length", Long.toString(paramHttpContext.getContentLength()));
      }
      if ((paramHttpContext.getContentType() != null) && (!paramHttpResponse.containsHeader("Content-Type"))) {
        paramHttpResponse.addHeader(paramHttpContext.getContentType());
      }
      if ((paramHttpContext.getContentEncoding() != null) && (!paramHttpResponse.containsHeader("Content-Encoding"))) {
        paramHttpResponse.addHeader(paramHttpContext.getContentEncoding());
      }
    }
    else
    {
      int i = paramHttpResponse.getStatusLine().getStatusCode();
      if ((i != 204) && (i != 304) && (i != 205)) {
        paramHttpResponse.addHeader("Content-Length", "0");
      }
    }
    return;
    label255:
    throw new ProtocolException("Content-Length header already present");
    label265:
    throw new ProtocolException("Transfer-encoding header already present");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/ResponseContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */