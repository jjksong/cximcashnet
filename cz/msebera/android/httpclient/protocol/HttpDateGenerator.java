package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@ThreadSafe
public class HttpDateGenerator
{
  public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
  public static final String PATTERN_RFC1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";
  @GuardedBy("this")
  private long dateAsLong = 0L;
  @GuardedBy("this")
  private String dateAsText = null;
  @GuardedBy("this")
  private final DateFormat dateformat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
  
  public HttpDateGenerator()
  {
    this.dateformat.setTimeZone(GMT);
  }
  
  public String getCurrentDate()
  {
    try
    {
      long l = System.currentTimeMillis();
      if (l - this.dateAsLong > 1000L)
      {
        localObject1 = this.dateformat;
        Date localDate = new java/util/Date;
        localDate.<init>(l);
        this.dateAsText = ((DateFormat)localObject1).format(localDate);
        this.dateAsLong = l;
      }
      Object localObject1 = this.dateAsText;
      return (String)localObject1;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/HttpDateGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */