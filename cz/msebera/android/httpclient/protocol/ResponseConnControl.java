package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class ResponseConnControl
  implements HttpResponseInterceptor
{
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    paramHttpContext = HttpCoreContext.adapt(paramHttpContext);
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    if ((i != 400) && (i != 408) && (i != 411) && (i != 413) && (i != 414) && (i != 503) && (i != 501))
    {
      Object localObject = paramHttpResponse.getFirstHeader("Connection");
      if ((localObject != null) && ("Close".equalsIgnoreCase(((Header)localObject).getValue()))) {
        return;
      }
      localObject = paramHttpResponse.getEntity();
      if (localObject != null)
      {
        ProtocolVersion localProtocolVersion = paramHttpResponse.getStatusLine().getProtocolVersion();
        if ((((HttpEntity)localObject).getContentLength() < 0L) && ((!((HttpEntity)localObject).isChunked()) || (localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0))))
        {
          paramHttpResponse.setHeader("Connection", "Close");
          return;
        }
      }
      paramHttpContext = paramHttpContext.getRequest();
      if (paramHttpContext != null)
      {
        localObject = paramHttpContext.getFirstHeader("Connection");
        if (localObject != null) {
          paramHttpResponse.setHeader("Connection", ((Header)localObject).getValue());
        } else if (paramHttpContext.getProtocolVersion().lessEquals(HttpVersion.HTTP_1_0)) {
          paramHttpResponse.setHeader("Connection", "Close");
        }
      }
      return;
    }
    paramHttpResponse.setHeader("Connection", "Close");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/ResponseConnControl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */