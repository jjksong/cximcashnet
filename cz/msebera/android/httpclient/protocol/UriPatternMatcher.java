package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@ThreadSafe
public class UriPatternMatcher<T>
{
  @GuardedBy("this")
  private final Map<String, T> map = new HashMap();
  
  @Deprecated
  public Map<String, T> getObjects()
  {
    try
    {
      Map localMap = this.map;
      return localMap;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public T lookup(String paramString)
  {
    try
    {
      Args.notNull(paramString, "Request path");
      Object localObject1 = this.map.get(paramString);
      Object localObject3 = localObject1;
      if (localObject1 == null)
      {
        Object localObject2 = null;
        Iterator localIterator = this.map.keySet().iterator();
        for (;;)
        {
          localObject3 = localObject1;
          if (!localIterator.hasNext()) {
            break;
          }
          localObject3 = (String)localIterator.next();
          if ((matchUriRequestPattern((String)localObject3, paramString)) && ((localObject2 == null) || (((String)localObject2).length() < ((String)localObject3).length()) || ((((String)localObject2).length() == ((String)localObject3).length()) && (((String)localObject3).endsWith("*")))))
          {
            localObject1 = this.map.get(localObject3);
            localObject2 = localObject3;
          }
        }
      }
      return (T)localObject3;
    }
    finally {}
  }
  
  protected boolean matchUriRequestPattern(String paramString1, String paramString2)
  {
    boolean bool1 = paramString1.equals("*");
    boolean bool2 = true;
    if (bool1) {
      return true;
    }
    if (paramString1.endsWith("*"))
    {
      bool1 = bool2;
      if (paramString2.startsWith(paramString1.substring(0, paramString1.length() - 1))) {}
    }
    else if ((paramString1.startsWith("*")) && (paramString2.endsWith(paramString1.substring(1, paramString1.length()))))
    {
      bool1 = bool2;
    }
    else
    {
      bool1 = false;
    }
    return bool1;
  }
  
  public void register(String paramString, T paramT)
  {
    try
    {
      Args.notNull(paramString, "URI request pattern");
      this.map.put(paramString, paramT);
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  @Deprecated
  public void setHandlers(Map<String, T> paramMap)
  {
    try
    {
      Args.notNull(paramMap, "Map of handlers");
      this.map.clear();
      this.map.putAll(paramMap);
      return;
    }
    finally
    {
      paramMap = finally;
      throw paramMap;
    }
  }
  
  @Deprecated
  public void setObjects(Map<String, T> paramMap)
  {
    try
    {
      Args.notNull(paramMap, "Map of handlers");
      this.map.clear();
      this.map.putAll(paramMap);
      return;
    }
    finally
    {
      paramMap = finally;
      throw paramMap;
    }
  }
  
  public String toString()
  {
    return this.map.toString();
  }
  
  public void unregister(String paramString)
  {
    if (paramString == null) {
      return;
    }
    try
    {
      this.map.remove(paramString);
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/UriPatternMatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */