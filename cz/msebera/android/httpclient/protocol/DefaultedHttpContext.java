package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.util.Args;

@Deprecated
public final class DefaultedHttpContext
  implements HttpContext
{
  private final HttpContext defaults;
  private final HttpContext local;
  
  public DefaultedHttpContext(HttpContext paramHttpContext1, HttpContext paramHttpContext2)
  {
    this.local = ((HttpContext)Args.notNull(paramHttpContext1, "HTTP context"));
    this.defaults = paramHttpContext2;
  }
  
  public Object getAttribute(String paramString)
  {
    Object localObject = this.local.getAttribute(paramString);
    if (localObject == null) {
      return this.defaults.getAttribute(paramString);
    }
    return localObject;
  }
  
  public HttpContext getDefaults()
  {
    return this.defaults;
  }
  
  public Object removeAttribute(String paramString)
  {
    return this.local.removeAttribute(paramString);
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    this.local.setAttribute(paramString, paramObject);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[local: ");
    localStringBuilder.append(this.local);
    localStringBuilder.append("defaults: ");
    localStringBuilder.append(this.defaults);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/DefaultedHttpContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */