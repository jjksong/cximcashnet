package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpConnection;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;

@NotThreadSafe
public class HttpCoreContext
  implements HttpContext
{
  public static final String HTTP_CONNECTION = "http.connection";
  public static final String HTTP_REQUEST = "http.request";
  public static final String HTTP_REQ_SENT = "http.request_sent";
  public static final String HTTP_RESPONSE = "http.response";
  public static final String HTTP_TARGET_HOST = "http.target_host";
  private final HttpContext context;
  
  public HttpCoreContext()
  {
    this.context = new BasicHttpContext();
  }
  
  public HttpCoreContext(HttpContext paramHttpContext)
  {
    this.context = paramHttpContext;
  }
  
  public static HttpCoreContext adapt(HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpContext, "HTTP context");
    if ((paramHttpContext instanceof HttpCoreContext)) {
      return (HttpCoreContext)paramHttpContext;
    }
    return new HttpCoreContext(paramHttpContext);
  }
  
  public static HttpCoreContext create()
  {
    return new HttpCoreContext(new BasicHttpContext());
  }
  
  public Object getAttribute(String paramString)
  {
    return this.context.getAttribute(paramString);
  }
  
  public <T> T getAttribute(String paramString, Class<T> paramClass)
  {
    Args.notNull(paramClass, "Attribute class");
    paramString = getAttribute(paramString);
    if (paramString == null) {
      return null;
    }
    return (T)paramClass.cast(paramString);
  }
  
  public HttpConnection getConnection()
  {
    return (HttpConnection)getAttribute("http.connection", HttpConnection.class);
  }
  
  public <T extends HttpConnection> T getConnection(Class<T> paramClass)
  {
    return (HttpConnection)getAttribute("http.connection", paramClass);
  }
  
  public HttpRequest getRequest()
  {
    return (HttpRequest)getAttribute("http.request", HttpRequest.class);
  }
  
  public HttpResponse getResponse()
  {
    return (HttpResponse)getAttribute("http.response", HttpResponse.class);
  }
  
  public HttpHost getTargetHost()
  {
    return (HttpHost)getAttribute("http.target_host", HttpHost.class);
  }
  
  public boolean isRequestSent()
  {
    Boolean localBoolean = (Boolean)getAttribute("http.request_sent", Boolean.class);
    boolean bool;
    if ((localBoolean != null) && (localBoolean.booleanValue())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public Object removeAttribute(String paramString)
  {
    return this.context.removeAttribute(paramString);
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    this.context.setAttribute(paramString, paramObject);
  }
  
  public void setTargetHost(HttpHost paramHttpHost)
  {
    setAttribute("http.target_host", paramHttpHost);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/HttpCoreContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */