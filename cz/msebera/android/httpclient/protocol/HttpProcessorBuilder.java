package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import java.util.LinkedList;
import java.util.List;

public class HttpProcessorBuilder
{
  private ChainBuilder<HttpRequestInterceptor> requestChainBuilder;
  private ChainBuilder<HttpResponseInterceptor> responseChainBuilder;
  
  public static HttpProcessorBuilder create()
  {
    return new HttpProcessorBuilder();
  }
  
  private ChainBuilder<HttpRequestInterceptor> getRequestChainBuilder()
  {
    if (this.requestChainBuilder == null) {
      this.requestChainBuilder = new ChainBuilder();
    }
    return this.requestChainBuilder;
  }
  
  private ChainBuilder<HttpResponseInterceptor> getResponseChainBuilder()
  {
    if (this.responseChainBuilder == null) {
      this.responseChainBuilder = new ChainBuilder();
    }
    return this.responseChainBuilder;
  }
  
  public HttpProcessorBuilder add(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    return addLast(paramHttpRequestInterceptor);
  }
  
  public HttpProcessorBuilder add(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    return addLast(paramHttpResponseInterceptor);
  }
  
  public HttpProcessorBuilder addAll(HttpRequestInterceptor... paramVarArgs)
  {
    return addAllLast(paramVarArgs);
  }
  
  public HttpProcessorBuilder addAll(HttpResponseInterceptor... paramVarArgs)
  {
    return addAllLast(paramVarArgs);
  }
  
  public HttpProcessorBuilder addAllFirst(HttpRequestInterceptor... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    getRequestChainBuilder().addAllFirst(paramVarArgs);
    return this;
  }
  
  public HttpProcessorBuilder addAllFirst(HttpResponseInterceptor... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    getResponseChainBuilder().addAllFirst(paramVarArgs);
    return this;
  }
  
  public HttpProcessorBuilder addAllLast(HttpRequestInterceptor... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    getRequestChainBuilder().addAllLast(paramVarArgs);
    return this;
  }
  
  public HttpProcessorBuilder addAllLast(HttpResponseInterceptor... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return this;
    }
    getResponseChainBuilder().addAllLast(paramVarArgs);
    return this;
  }
  
  public HttpProcessorBuilder addFirst(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    if (paramHttpRequestInterceptor == null) {
      return this;
    }
    getRequestChainBuilder().addFirst(paramHttpRequestInterceptor);
    return this;
  }
  
  public HttpProcessorBuilder addFirst(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    if (paramHttpResponseInterceptor == null) {
      return this;
    }
    getResponseChainBuilder().addFirst(paramHttpResponseInterceptor);
    return this;
  }
  
  public HttpProcessorBuilder addLast(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    if (paramHttpRequestInterceptor == null) {
      return this;
    }
    getRequestChainBuilder().addLast(paramHttpRequestInterceptor);
    return this;
  }
  
  public HttpProcessorBuilder addLast(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    if (paramHttpResponseInterceptor == null) {
      return this;
    }
    getResponseChainBuilder().addLast(paramHttpResponseInterceptor);
    return this;
  }
  
  public HttpProcessor build()
  {
    Object localObject = this.requestChainBuilder;
    LinkedList localLinkedList = null;
    if (localObject != null) {
      localObject = ((ChainBuilder)localObject).build();
    } else {
      localObject = null;
    }
    ChainBuilder localChainBuilder = this.responseChainBuilder;
    if (localChainBuilder != null) {
      localLinkedList = localChainBuilder.build();
    }
    return new ImmutableHttpProcessor((List)localObject, localLinkedList);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/HttpProcessorBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */