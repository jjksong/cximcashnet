package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class RequestContent
  implements HttpRequestInterceptor
{
  private final boolean overwrite;
  
  public RequestContent()
  {
    this(false);
  }
  
  public RequestContent(boolean paramBoolean)
  {
    this.overwrite = paramBoolean;
  }
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest))
    {
      if (this.overwrite)
      {
        paramHttpRequest.removeHeaders("Transfer-Encoding");
        paramHttpRequest.removeHeaders("Content-Length");
      }
      else
      {
        if (paramHttpRequest.containsHeader("Transfer-Encoding")) {
          break label272;
        }
        if (paramHttpRequest.containsHeader("Content-Length")) {
          break label262;
        }
      }
      paramHttpContext = paramHttpRequest.getRequestLine().getProtocolVersion();
      HttpEntity localHttpEntity = ((HttpEntityEnclosingRequest)paramHttpRequest).getEntity();
      if (localHttpEntity == null)
      {
        paramHttpRequest.addHeader("Content-Length", "0");
        return;
      }
      if ((!localHttpEntity.isChunked()) && (localHttpEntity.getContentLength() >= 0L))
      {
        paramHttpRequest.addHeader("Content-Length", Long.toString(localHttpEntity.getContentLength()));
      }
      else
      {
        if (paramHttpContext.lessEquals(HttpVersion.HTTP_1_0)) {
          break label229;
        }
        paramHttpRequest.addHeader("Transfer-Encoding", "chunked");
      }
      if ((localHttpEntity.getContentType() != null) && (!paramHttpRequest.containsHeader("Content-Type"))) {
        paramHttpRequest.addHeader(localHttpEntity.getContentType());
      }
      if ((localHttpEntity.getContentEncoding() != null) && (!paramHttpRequest.containsHeader("Content-Encoding")))
      {
        paramHttpRequest.addHeader(localHttpEntity.getContentEncoding());
        return;
        label229:
        paramHttpRequest = new StringBuilder();
        paramHttpRequest.append("Chunked transfer encoding not allowed for ");
        paramHttpRequest.append(paramHttpContext);
        throw new ProtocolException(paramHttpRequest.toString());
        label262:
        throw new ProtocolException("Content-Length header already present");
        label272:
        throw new ProtocolException("Transfer-encoding header already present");
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/RequestContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */