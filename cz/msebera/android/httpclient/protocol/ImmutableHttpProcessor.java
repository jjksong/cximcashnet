package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.io.IOException;
import java.util.List;

@ThreadSafe
public final class ImmutableHttpProcessor
  implements HttpProcessor
{
  private final HttpRequestInterceptor[] requestInterceptors;
  private final HttpResponseInterceptor[] responseInterceptors;
  
  @Deprecated
  public ImmutableHttpProcessor(HttpRequestInterceptorList paramHttpRequestInterceptorList, HttpResponseInterceptorList paramHttpResponseInterceptorList)
  {
    int j = 0;
    int k;
    int i;
    if (paramHttpRequestInterceptorList != null)
    {
      k = paramHttpRequestInterceptorList.getRequestInterceptorCount();
      this.requestInterceptors = new HttpRequestInterceptor[k];
      for (i = 0; i < k; i++) {
        this.requestInterceptors[i] = paramHttpRequestInterceptorList.getRequestInterceptor(i);
      }
    }
    this.requestInterceptors = new HttpRequestInterceptor[0];
    if (paramHttpResponseInterceptorList != null)
    {
      k = paramHttpResponseInterceptorList.getResponseInterceptorCount();
      this.responseInterceptors = new HttpResponseInterceptor[k];
      for (i = j; i < k; i++) {
        this.responseInterceptors[i] = paramHttpResponseInterceptorList.getResponseInterceptor(i);
      }
    }
    this.responseInterceptors = new HttpResponseInterceptor[0];
  }
  
  public ImmutableHttpProcessor(List<HttpRequestInterceptor> paramList, List<HttpResponseInterceptor> paramList1)
  {
    if (paramList != null) {
      this.requestInterceptors = ((HttpRequestInterceptor[])paramList.toArray(new HttpRequestInterceptor[paramList.size()]));
    } else {
      this.requestInterceptors = new HttpRequestInterceptor[0];
    }
    if (paramList1 != null) {
      this.responseInterceptors = ((HttpResponseInterceptor[])paramList1.toArray(new HttpResponseInterceptor[paramList1.size()]));
    } else {
      this.responseInterceptors = new HttpResponseInterceptor[0];
    }
  }
  
  public ImmutableHttpProcessor(HttpRequestInterceptor... paramVarArgs)
  {
    this(paramVarArgs, null);
  }
  
  public ImmutableHttpProcessor(HttpRequestInterceptor[] paramArrayOfHttpRequestInterceptor, HttpResponseInterceptor[] paramArrayOfHttpResponseInterceptor)
  {
    int i;
    if (paramArrayOfHttpRequestInterceptor != null)
    {
      i = paramArrayOfHttpRequestInterceptor.length;
      this.requestInterceptors = new HttpRequestInterceptor[i];
      System.arraycopy(paramArrayOfHttpRequestInterceptor, 0, this.requestInterceptors, 0, i);
    }
    else
    {
      this.requestInterceptors = new HttpRequestInterceptor[0];
    }
    if (paramArrayOfHttpResponseInterceptor != null)
    {
      i = paramArrayOfHttpResponseInterceptor.length;
      this.responseInterceptors = new HttpResponseInterceptor[i];
      System.arraycopy(paramArrayOfHttpResponseInterceptor, 0, this.responseInterceptors, 0, i);
    }
    else
    {
      this.responseInterceptors = new HttpResponseInterceptor[0];
    }
  }
  
  public ImmutableHttpProcessor(HttpResponseInterceptor... paramVarArgs)
  {
    this(null, paramVarArgs);
  }
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, HttpException
  {
    HttpRequestInterceptor[] arrayOfHttpRequestInterceptor = this.requestInterceptors;
    int j = arrayOfHttpRequestInterceptor.length;
    for (int i = 0; i < j; i++) {
      arrayOfHttpRequestInterceptor[i].process(paramHttpRequest, paramHttpContext);
    }
  }
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws IOException, HttpException
  {
    HttpResponseInterceptor[] arrayOfHttpResponseInterceptor = this.responseInterceptors;
    int j = arrayOfHttpResponseInterceptor.length;
    for (int i = 0; i < j; i++) {
      arrayOfHttpResponseInterceptor[i].process(paramHttpResponse, paramHttpContext);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/protocol/ImmutableHttpProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */