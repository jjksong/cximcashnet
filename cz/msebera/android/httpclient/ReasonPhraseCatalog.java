package cz.msebera.android.httpclient;

import java.util.Locale;

public abstract interface ReasonPhraseCatalog
{
  public abstract String getReason(int paramInt, Locale paramLocale);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/ReasonPhraseCatalog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */