package cz.msebera.android.httpclient;

public class UnsupportedHttpVersionException
  extends ProtocolException
{
  private static final long serialVersionUID = -1348448090193107031L;
  
  public UnsupportedHttpVersionException() {}
  
  public UnsupportedHttpVersionException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/UnsupportedHttpVersionException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */