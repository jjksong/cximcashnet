package cz.msebera.android.httpclient.extras;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Process;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.SecureRandomSpi;
import java.security.Security;

@TargetApi(4)
public final class PRNGFixes
{
  private static final byte[] BUILD_FINGERPRINT_AND_DEVICE_SERIAL = ;
  private static final int VERSION_CODE_JELLY_BEAN = 16;
  private static final int VERSION_CODE_JELLY_BEAN_MR2 = 18;
  
  public static void apply()
  {
    applyOpenSSLFix();
    installLinuxPRNGSecureRandom();
  }
  
  private static void applyOpenSSLFix()
    throws SecurityException
  {
    if ((Build.VERSION.SDK_INT >= 16) && (Build.VERSION.SDK_INT <= 18)) {
      try
      {
        Class.forName("org.apache.harmony.xnet.provider.jsse.NativeCrypto").getMethod("RAND_seed", new Class[] { byte[].class }).invoke(null, new Object[] { generateSeed() });
        int i = ((Integer)Class.forName("org.apache.harmony.xnet.provider.jsse.NativeCrypto").getMethod("RAND_load_file", new Class[] { String.class, Long.TYPE }).invoke(null, new Object[] { "/dev/urandom", Integer.valueOf(1024) })).intValue();
        if (i == 1024) {
          return;
        }
        IOException localIOException = new java/io/IOException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("Unexpected number of bytes read from Linux PRNG: ");
        localStringBuilder.append(i);
        localIOException.<init>(localStringBuilder.toString());
        throw localIOException;
      }
      catch (Exception localException)
      {
        throw new SecurityException("Failed to seed OpenSSL PRNG", localException);
      }
    }
  }
  
  private static byte[] generateSeed()
  {
    try
    {
      Object localObject = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject).<init>();
      DataOutputStream localDataOutputStream = new java/io/DataOutputStream;
      localDataOutputStream.<init>((OutputStream)localObject);
      localDataOutputStream.writeLong(System.currentTimeMillis());
      localDataOutputStream.writeLong(System.nanoTime());
      localDataOutputStream.writeInt(Process.myPid());
      localDataOutputStream.writeInt(Process.myUid());
      localDataOutputStream.write(BUILD_FINGERPRINT_AND_DEVICE_SERIAL);
      localDataOutputStream.close();
      localObject = ((ByteArrayOutputStream)localObject).toByteArray();
      return (byte[])localObject;
    }
    catch (IOException localIOException)
    {
      throw new SecurityException("Failed to generate seed", localIOException);
    }
  }
  
  private static byte[] getBuildFingerprintAndDeviceSerial()
  {
    Object localObject = new StringBuilder();
    String str = Build.FINGERPRINT;
    if (str != null) {
      ((StringBuilder)localObject).append(str);
    }
    str = getDeviceSerialNumber();
    if (str != null) {
      ((StringBuilder)localObject).append(str);
    }
    try
    {
      localObject = ((StringBuilder)localObject).toString().getBytes("UTF-8");
      return (byte[])localObject;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new RuntimeException("UTF-8 encoding not supported");
    }
  }
  
  private static String getDeviceSerialNumber()
  {
    try
    {
      String str = (String)Build.class.getField("SERIAL").get(null);
      return str;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static void installLinuxPRNGSecureRandom()
    throws SecurityException
  {
    if (Build.VERSION.SDK_INT > 18) {
      return;
    }
    Object localObject = Security.getProviders("SecureRandom.SHA1PRNG");
    if ((localObject == null) || (localObject.length < 1) || (!LinuxPRNGSecureRandomProvider.class.equals(localObject[0].getClass()))) {
      Security.insertProviderAt(new LinuxPRNGSecureRandomProvider(), 1);
    }
    localObject = new SecureRandom();
    if (LinuxPRNGSecureRandomProvider.class.equals(((SecureRandom)localObject).getProvider().getClass())) {
      try
      {
        localObject = SecureRandom.getInstance("SHA1PRNG");
        if (LinuxPRNGSecureRandomProvider.class.equals(((SecureRandom)localObject).getProvider().getClass())) {
          return;
        }
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("SecureRandom.getInstance(\"SHA1PRNG\") backed by wrong Provider: ");
        localStringBuilder.append(((SecureRandom)localObject).getProvider().getClass());
        throw new SecurityException(localStringBuilder.toString());
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        throw new SecurityException("SHA1PRNG not available", localNoSuchAlgorithmException);
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("new SecureRandom() backed by wrong Provider: ");
    localStringBuilder.append(localNoSuchAlgorithmException.getProvider().getClass());
    throw new SecurityException(localStringBuilder.toString());
  }
  
  public static class LinuxPRNGSecureRandom
    extends SecureRandomSpi
  {
    private static final File URANDOM_FILE = new File("/dev/urandom");
    private static final Object sLock = new Object();
    private static DataInputStream sUrandomIn;
    private static OutputStream sUrandomOut;
    private boolean mSeeded;
    
    private DataInputStream getUrandomInputStream()
    {
      synchronized (sLock)
      {
        Object localObject2 = sUrandomIn;
        if (localObject2 == null) {
          try
          {
            localObject4 = new java/io/DataInputStream;
            localObject2 = new java/io/FileInputStream;
            ((FileInputStream)localObject2).<init>(URANDOM_FILE);
            ((DataInputStream)localObject4).<init>((InputStream)localObject2);
            sUrandomIn = (DataInputStream)localObject4;
          }
          catch (IOException localIOException)
          {
            Object localObject4 = new java/lang/SecurityException;
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Failed to open ");
            localStringBuilder.append(URANDOM_FILE);
            localStringBuilder.append(" for reading");
            ((SecurityException)localObject4).<init>(localStringBuilder.toString(), localIOException);
            throw ((Throwable)localObject4);
          }
        }
        DataInputStream localDataInputStream = sUrandomIn;
        return localDataInputStream;
      }
    }
    
    private OutputStream getUrandomOutputStream()
      throws IOException
    {
      synchronized (sLock)
      {
        if (sUrandomOut == null)
        {
          localObject2 = new java/io/FileOutputStream;
          ((FileOutputStream)localObject2).<init>(URANDOM_FILE);
          sUrandomOut = (OutputStream)localObject2;
        }
        Object localObject2 = sUrandomOut;
        return (OutputStream)localObject2;
      }
    }
    
    protected byte[] engineGenerateSeed(int paramInt)
    {
      byte[] arrayOfByte = new byte[paramInt];
      engineNextBytes(arrayOfByte);
      return arrayOfByte;
    }
    
    /* Error */
    protected void engineNextBytes(byte[] paramArrayOfByte)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 92	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:mSeeded	Z
      //   4: ifne +10 -> 14
      //   7: aload_0
      //   8: invokestatic 96	cz/msebera/android/httpclient/extras/PRNGFixes:access$000	()[B
      //   11: invokevirtual 99	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:engineSetSeed	([B)V
      //   14: getstatic 35	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:sLock	Ljava/lang/Object;
      //   17: astore_2
      //   18: aload_2
      //   19: monitorenter
      //   20: aload_0
      //   21: invokespecial 101	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:getUrandomInputStream	()Ljava/io/DataInputStream;
      //   24: astore_3
      //   25: aload_2
      //   26: monitorexit
      //   27: aload_3
      //   28: monitorenter
      //   29: aload_3
      //   30: aload_1
      //   31: invokevirtual 104	java/io/DataInputStream:readFully	([B)V
      //   34: aload_3
      //   35: monitorexit
      //   36: return
      //   37: astore_1
      //   38: aload_3
      //   39: monitorexit
      //   40: aload_1
      //   41: athrow
      //   42: astore_1
      //   43: aload_2
      //   44: monitorexit
      //   45: aload_1
      //   46: athrow
      //   47: astore_1
      //   48: new 57	java/lang/StringBuilder
      //   51: dup
      //   52: invokespecial 58	java/lang/StringBuilder:<init>	()V
      //   55: astore_2
      //   56: aload_2
      //   57: ldc 106
      //   59: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload_2
      //   64: getstatic 29	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:URANDOM_FILE	Ljava/io/File;
      //   67: invokevirtual 67	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   70: pop
      //   71: new 55	java/lang/SecurityException
      //   74: dup
      //   75: aload_2
      //   76: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   79: aload_1
      //   80: invokespecial 76	java/lang/SecurityException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   83: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	84	0	this	LinuxPRNGSecureRandom
      //   0	84	1	paramArrayOfByte	byte[]
      //   24	15	3	localDataInputStream	DataInputStream
      // Exception table:
      //   from	to	target	type
      //   29	36	37	finally
      //   38	40	37	finally
      //   20	27	42	finally
      //   43	45	42	finally
      //   14	20	47	java/io/IOException
      //   27	29	47	java/io/IOException
      //   40	42	47	java/io/IOException
      //   45	47	47	java/io/IOException
    }
    
    /* Error */
    protected void engineSetSeed(byte[] paramArrayOfByte)
    {
      // Byte code:
      //   0: getstatic 35	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:sLock	Ljava/lang/Object;
      //   3: astore_2
      //   4: aload_2
      //   5: monitorenter
      //   6: aload_0
      //   7: invokespecial 108	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:getUrandomOutputStream	()Ljava/io/OutputStream;
      //   10: astore_3
      //   11: aload_2
      //   12: monitorexit
      //   13: aload_3
      //   14: aload_1
      //   15: invokevirtual 113	java/io/OutputStream:write	([B)V
      //   18: aload_3
      //   19: invokevirtual 116	java/io/OutputStream:flush	()V
      //   22: aload_0
      //   23: iconst_1
      //   24: putfield 92	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:mSeeded	Z
      //   27: goto +54 -> 81
      //   30: astore_1
      //   31: aload_2
      //   32: monitorexit
      //   33: aload_1
      //   34: athrow
      //   35: astore_1
      //   36: goto +46 -> 82
      //   39: astore_1
      //   40: ldc 6
      //   42: invokevirtual 121	java/lang/Class:getSimpleName	()Ljava/lang/String;
      //   45: astore_1
      //   46: new 57	java/lang/StringBuilder
      //   49: astore_2
      //   50: aload_2
      //   51: invokespecial 58	java/lang/StringBuilder:<init>	()V
      //   54: aload_2
      //   55: ldc 123
      //   57: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   60: pop
      //   61: aload_2
      //   62: getstatic 29	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:URANDOM_FILE	Ljava/io/File;
      //   65: invokevirtual 67	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   68: pop
      //   69: aload_1
      //   70: aload_2
      //   71: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   74: invokestatic 129	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   77: pop
      //   78: goto -56 -> 22
      //   81: return
      //   82: aload_0
      //   83: iconst_1
      //   84: putfield 92	cz/msebera/android/httpclient/extras/PRNGFixes$LinuxPRNGSecureRandom:mSeeded	Z
      //   87: aload_1
      //   88: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	89	0	this	LinuxPRNGSecureRandom
      //   0	89	1	paramArrayOfByte	byte[]
      //   10	9	3	localOutputStream	OutputStream
      // Exception table:
      //   from	to	target	type
      //   6	13	30	finally
      //   31	33	30	finally
      //   0	6	35	finally
      //   13	22	35	finally
      //   33	35	35	finally
      //   40	78	35	finally
      //   0	6	39	java/io/IOException
      //   13	22	39	java/io/IOException
      //   33	35	39	java/io/IOException
    }
  }
  
  private static class LinuxPRNGSecureRandomProvider
    extends Provider
  {
    public LinuxPRNGSecureRandomProvider()
    {
      super(1.0D, "A Linux-specific random number provider that uses /dev/urandom");
      put("SecureRandom.SHA1PRNG", PRNGFixes.LinuxPRNGSecureRandom.class.getName());
      put("SecureRandom.SHA1PRNG ImplementedIn", "Software");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/extras/PRNGFixes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */