package cz.msebera.android.httpclient.extras;

import android.util.Log;

public class HttpClientAndroidLog
{
  private boolean debugEnabled;
  private boolean errorEnabled;
  private boolean infoEnabled;
  private String logTag;
  private boolean traceEnabled;
  private boolean warnEnabled;
  
  public HttpClientAndroidLog(Object paramObject)
  {
    this.logTag = paramObject.toString();
    this.debugEnabled = false;
    this.errorEnabled = false;
    this.traceEnabled = false;
    this.warnEnabled = false;
    this.infoEnabled = false;
  }
  
  public void debug(Object paramObject)
  {
    if (isDebugEnabled()) {
      Log.d(this.logTag, paramObject.toString());
    }
  }
  
  public void debug(Object paramObject, Throwable paramThrowable)
  {
    if (isDebugEnabled()) {
      Log.d(this.logTag, paramObject.toString(), paramThrowable);
    }
  }
  
  public void enableDebug(boolean paramBoolean)
  {
    this.debugEnabled = paramBoolean;
  }
  
  public void enableError(boolean paramBoolean)
  {
    this.errorEnabled = paramBoolean;
  }
  
  public void enableInfo(boolean paramBoolean)
  {
    this.infoEnabled = paramBoolean;
  }
  
  public void enableTrace(boolean paramBoolean)
  {
    this.traceEnabled = paramBoolean;
  }
  
  public void enableWarn(boolean paramBoolean)
  {
    this.warnEnabled = paramBoolean;
  }
  
  public void error(Object paramObject)
  {
    if (isErrorEnabled()) {
      Log.e(this.logTag, paramObject.toString());
    }
  }
  
  public void error(Object paramObject, Throwable paramThrowable)
  {
    if (isErrorEnabled()) {
      Log.e(this.logTag, paramObject.toString(), paramThrowable);
    }
  }
  
  public void info(Object paramObject)
  {
    if (isInfoEnabled()) {
      Log.i(this.logTag, paramObject.toString());
    }
  }
  
  public void info(Object paramObject, Throwable paramThrowable)
  {
    if (isInfoEnabled()) {
      Log.i(this.logTag, paramObject.toString(), paramThrowable);
    }
  }
  
  public boolean isDebugEnabled()
  {
    return this.debugEnabled;
  }
  
  public boolean isErrorEnabled()
  {
    return this.errorEnabled;
  }
  
  public boolean isInfoEnabled()
  {
    return this.infoEnabled;
  }
  
  public boolean isTraceEnabled()
  {
    return this.traceEnabled;
  }
  
  public boolean isWarnEnabled()
  {
    return this.warnEnabled;
  }
  
  public void trace(Object paramObject)
  {
    if (isTraceEnabled()) {
      Log.i(this.logTag, paramObject.toString());
    }
  }
  
  public void trace(Object paramObject, Throwable paramThrowable)
  {
    if (isTraceEnabled()) {
      Log.i(this.logTag, paramObject.toString(), paramThrowable);
    }
  }
  
  public void warn(Object paramObject)
  {
    if (isWarnEnabled()) {
      Log.w(this.logTag, paramObject.toString());
    }
  }
  
  public void warn(Object paramObject, Throwable paramThrowable)
  {
    if (isWarnEnabled()) {
      Log.w(this.logTag, paramObject.toString(), paramThrowable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/extras/HttpClientAndroidLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */