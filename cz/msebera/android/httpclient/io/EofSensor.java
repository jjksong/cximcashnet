package cz.msebera.android.httpclient.io;

@Deprecated
public abstract interface EofSensor
{
  public abstract boolean isEof();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/io/EofSensor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */