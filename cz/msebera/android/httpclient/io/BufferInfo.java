package cz.msebera.android.httpclient.io;

public abstract interface BufferInfo
{
  public abstract int available();
  
  public abstract int capacity();
  
  public abstract int length();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/io/BufferInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */