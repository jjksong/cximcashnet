package cz.msebera.android.httpclient;

public class ParseException
  extends RuntimeException
{
  private static final long serialVersionUID = -7288819855864183578L;
  
  public ParseException() {}
  
  public ParseException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/ParseException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */