package cz.msebera.android.httpclient;

public class TruncatedChunkException
  extends MalformedChunkCodingException
{
  private static final long serialVersionUID = -23506263930279460L;
  
  public TruncatedChunkException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/TruncatedChunkException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */