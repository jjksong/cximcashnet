package cz.msebera.android.httpclient;

import java.io.IOException;

public class ConnectionClosedException
  extends IOException
{
  private static final long serialVersionUID = 617550366255636674L;
  
  public ConnectionClosedException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/ConnectionClosedException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */