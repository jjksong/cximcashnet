package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;

@ThreadSafe
public class RedirectExec
  implements ClientExecChain
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final RedirectStrategy redirectStrategy;
  private final ClientExecChain requestExecutor;
  private final HttpRoutePlanner routePlanner;
  
  public RedirectExec(ClientExecChain paramClientExecChain, HttpRoutePlanner paramHttpRoutePlanner, RedirectStrategy paramRedirectStrategy)
  {
    Args.notNull(paramClientExecChain, "HTTP client request executor");
    Args.notNull(paramHttpRoutePlanner, "HTTP route planner");
    Args.notNull(paramRedirectStrategy, "HTTP redirect strategy");
    this.requestExecutor = paramClientExecChain;
    this.routePlanner = paramHttpRoutePlanner;
    this.redirectStrategy = paramRedirectStrategy;
  }
  
  /* Error */
  public cz.msebera.android.httpclient.client.methods.CloseableHttpResponse execute(cz.msebera.android.httpclient.conn.routing.HttpRoute paramHttpRoute, cz.msebera.android.httpclient.client.methods.HttpRequestWrapper paramHttpRequestWrapper, cz.msebera.android.httpclient.client.protocol.HttpClientContext paramHttpClientContext, cz.msebera.android.httpclient.client.methods.HttpExecutionAware paramHttpExecutionAware)
    throws java.io.IOException, cz.msebera.android.httpclient.HttpException
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 60
    //   3: invokestatic 39	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_2
    //   8: ldc 62
    //   10: invokestatic 39	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   13: pop
    //   14: aload_3
    //   15: ldc 64
    //   17: invokestatic 39	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   20: pop
    //   21: aload_3
    //   22: invokevirtual 70	cz/msebera/android/httpclient/client/protocol/HttpClientContext:getRedirectLocations	()Ljava/util/List;
    //   25: astore 7
    //   27: aload 7
    //   29: ifnull +10 -> 39
    //   32: aload 7
    //   34: invokeinterface 75 1 0
    //   39: aload_3
    //   40: invokevirtual 79	cz/msebera/android/httpclient/client/protocol/HttpClientContext:getRequestConfig	()Lcz/msebera/android/httpclient/client/config/RequestConfig;
    //   43: astore 9
    //   45: aload 9
    //   47: invokevirtual 85	cz/msebera/android/httpclient/client/config/RequestConfig:getMaxRedirects	()I
    //   50: ifle +13 -> 63
    //   53: aload 9
    //   55: invokevirtual 85	cz/msebera/android/httpclient/client/config/RequestConfig:getMaxRedirects	()I
    //   58: istore 5
    //   60: goto +7 -> 67
    //   63: bipush 50
    //   65: istore 5
    //   67: aload_2
    //   68: astore 7
    //   70: iconst_0
    //   71: istore 6
    //   73: aload_0
    //   74: getfield 45	cz/msebera/android/httpclient/impl/execchain/RedirectExec:requestExecutor	Lcz/msebera/android/httpclient/impl/execchain/ClientExecChain;
    //   77: aload_1
    //   78: aload 7
    //   80: aload_3
    //   81: aload 4
    //   83: invokeinterface 87 5 0
    //   88: astore 8
    //   90: aload 9
    //   92: invokevirtual 91	cz/msebera/android/httpclient/client/config/RequestConfig:isRedirectsEnabled	()Z
    //   95: ifeq +378 -> 473
    //   98: aload_0
    //   99: getfield 49	cz/msebera/android/httpclient/impl/execchain/RedirectExec:redirectStrategy	Lcz/msebera/android/httpclient/client/RedirectStrategy;
    //   102: aload 7
    //   104: aload 8
    //   106: aload_3
    //   107: invokeinterface 97 4 0
    //   112: ifeq +361 -> 473
    //   115: iload 6
    //   117: iload 5
    //   119: if_icmpge +311 -> 430
    //   122: iinc 6 1
    //   125: aload_0
    //   126: getfield 49	cz/msebera/android/httpclient/impl/execchain/RedirectExec:redirectStrategy	Lcz/msebera/android/httpclient/client/RedirectStrategy;
    //   129: aload 7
    //   131: aload 8
    //   133: aload_3
    //   134: invokeinterface 101 4 0
    //   139: astore 7
    //   141: aload 7
    //   143: invokeinterface 107 1 0
    //   148: invokeinterface 112 1 0
    //   153: ifne +19 -> 172
    //   156: aload 7
    //   158: aload_2
    //   159: invokevirtual 118	cz/msebera/android/httpclient/client/methods/HttpRequestWrapper:getOriginal	()Lcz/msebera/android/httpclient/HttpRequest;
    //   162: invokeinterface 122 1 0
    //   167: invokeinterface 126 2 0
    //   172: aload 7
    //   174: invokestatic 130	cz/msebera/android/httpclient/client/methods/HttpRequestWrapper:wrap	(Lcz/msebera/android/httpclient/HttpRequest;)Lcz/msebera/android/httpclient/client/methods/HttpRequestWrapper;
    //   177: astore 7
    //   179: aload 7
    //   181: instanceof 132
    //   184: ifeq +11 -> 195
    //   187: aload 7
    //   189: checkcast 132	cz/msebera/android/httpclient/HttpEntityEnclosingRequest
    //   192: invokestatic 138	cz/msebera/android/httpclient/impl/execchain/RequestEntityProxy:enhance	(Lcz/msebera/android/httpclient/HttpEntityEnclosingRequest;)V
    //   195: aload 7
    //   197: invokevirtual 142	cz/msebera/android/httpclient/client/methods/HttpRequestWrapper:getURI	()Ljava/net/URI;
    //   200: astore 10
    //   202: aload 10
    //   204: invokestatic 148	cz/msebera/android/httpclient/client/utils/URIUtils:extractHost	(Ljava/net/URI;)Lcz/msebera/android/httpclient/HttpHost;
    //   207: astore 11
    //   209: aload 11
    //   211: ifnull +183 -> 394
    //   214: aload_1
    //   215: invokevirtual 154	cz/msebera/android/httpclient/conn/routing/HttpRoute:getTargetHost	()Lcz/msebera/android/httpclient/HttpHost;
    //   218: aload 11
    //   220: invokevirtual 160	cz/msebera/android/httpclient/HttpHost:equals	(Ljava/lang/Object;)Z
    //   223: ifne +69 -> 292
    //   226: aload_3
    //   227: invokevirtual 164	cz/msebera/android/httpclient/client/protocol/HttpClientContext:getTargetAuthState	()Lcz/msebera/android/httpclient/auth/AuthState;
    //   230: astore_1
    //   231: aload_1
    //   232: ifnull +16 -> 248
    //   235: aload_0
    //   236: getfield 31	cz/msebera/android/httpclient/impl/execchain/RedirectExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   239: ldc -90
    //   241: invokevirtual 169	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   244: aload_1
    //   245: invokevirtual 174	cz/msebera/android/httpclient/auth/AuthState:reset	()V
    //   248: aload_3
    //   249: invokevirtual 177	cz/msebera/android/httpclient/client/protocol/HttpClientContext:getProxyAuthState	()Lcz/msebera/android/httpclient/auth/AuthState;
    //   252: astore 12
    //   254: aload 12
    //   256: ifnull +36 -> 292
    //   259: aload 12
    //   261: invokevirtual 181	cz/msebera/android/httpclient/auth/AuthState:getAuthScheme	()Lcz/msebera/android/httpclient/auth/AuthScheme;
    //   264: astore_1
    //   265: aload_1
    //   266: ifnull +26 -> 292
    //   269: aload_1
    //   270: invokeinterface 186 1 0
    //   275: ifeq +17 -> 292
    //   278: aload_0
    //   279: getfield 31	cz/msebera/android/httpclient/impl/execchain/RedirectExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   282: ldc -68
    //   284: invokevirtual 169	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   287: aload 12
    //   289: invokevirtual 174	cz/msebera/android/httpclient/auth/AuthState:reset	()V
    //   292: aload_0
    //   293: getfield 47	cz/msebera/android/httpclient/impl/execchain/RedirectExec:routePlanner	Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;
    //   296: aload 11
    //   298: aload 7
    //   300: aload_3
    //   301: invokeinterface 194 4 0
    //   306: astore_1
    //   307: aload_0
    //   308: getfield 31	cz/msebera/android/httpclient/impl/execchain/RedirectExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   311: invokevirtual 197	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   314: ifeq +60 -> 374
    //   317: aload_0
    //   318: getfield 31	cz/msebera/android/httpclient/impl/execchain/RedirectExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   321: astore 12
    //   323: new 199	java/lang/StringBuilder
    //   326: astore 11
    //   328: aload 11
    //   330: invokespecial 200	java/lang/StringBuilder:<init>	()V
    //   333: aload 11
    //   335: ldc -54
    //   337: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload 11
    //   343: aload 10
    //   345: invokevirtual 209	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   348: pop
    //   349: aload 11
    //   351: ldc -45
    //   353: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   356: pop
    //   357: aload 11
    //   359: aload_1
    //   360: invokevirtual 209	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   363: pop
    //   364: aload 12
    //   366: aload 11
    //   368: invokevirtual 215	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   371: invokevirtual 169	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   374: aload 8
    //   376: invokeinterface 221 1 0
    //   381: invokestatic 227	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   384: aload 8
    //   386: invokeinterface 230 1 0
    //   391: goto -318 -> 73
    //   394: new 232	cz/msebera/android/httpclient/ProtocolException
    //   397: astore_1
    //   398: new 199	java/lang/StringBuilder
    //   401: astore_2
    //   402: aload_2
    //   403: invokespecial 200	java/lang/StringBuilder:<init>	()V
    //   406: aload_2
    //   407: ldc -22
    //   409: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: pop
    //   413: aload_2
    //   414: aload 10
    //   416: invokevirtual 209	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   419: pop
    //   420: aload_1
    //   421: aload_2
    //   422: invokevirtual 215	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   425: invokespecial 237	cz/msebera/android/httpclient/ProtocolException:<init>	(Ljava/lang/String;)V
    //   428: aload_1
    //   429: athrow
    //   430: new 239	cz/msebera/android/httpclient/client/RedirectException
    //   433: astore_1
    //   434: new 199	java/lang/StringBuilder
    //   437: astore_2
    //   438: aload_2
    //   439: invokespecial 200	java/lang/StringBuilder:<init>	()V
    //   442: aload_2
    //   443: ldc -15
    //   445: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: pop
    //   449: aload_2
    //   450: iload 5
    //   452: invokevirtual 244	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   455: pop
    //   456: aload_2
    //   457: ldc -10
    //   459: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   462: pop
    //   463: aload_1
    //   464: aload_2
    //   465: invokevirtual 215	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   468: invokespecial 247	cz/msebera/android/httpclient/client/RedirectException:<init>	(Ljava/lang/String;)V
    //   471: aload_1
    //   472: athrow
    //   473: aload 8
    //   475: areturn
    //   476: astore_2
    //   477: aload 8
    //   479: invokeinterface 221 1 0
    //   484: invokestatic 227	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   487: aload 8
    //   489: invokeinterface 230 1 0
    //   494: goto +21 -> 515
    //   497: astore_1
    //   498: goto +19 -> 517
    //   501: astore_1
    //   502: aload_0
    //   503: getfield 31	cz/msebera/android/httpclient/impl/execchain/RedirectExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   506: ldc -7
    //   508: aload_1
    //   509: invokevirtual 252	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   512: goto -25 -> 487
    //   515: aload_2
    //   516: athrow
    //   517: aload 8
    //   519: invokeinterface 230 1 0
    //   524: aload_1
    //   525: athrow
    //   526: astore_1
    //   527: aload 8
    //   529: invokeinterface 230 1 0
    //   534: aload_1
    //   535: athrow
    //   536: astore_1
    //   537: aload 8
    //   539: invokeinterface 230 1 0
    //   544: aload_1
    //   545: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	546	0	this	RedirectExec
    //   0	546	1	paramHttpRoute	cz.msebera.android.httpclient.conn.routing.HttpRoute
    //   0	546	2	paramHttpRequestWrapper	cz.msebera.android.httpclient.client.methods.HttpRequestWrapper
    //   0	546	3	paramHttpClientContext	cz.msebera.android.httpclient.client.protocol.HttpClientContext
    //   0	546	4	paramHttpExecutionAware	cz.msebera.android.httpclient.client.methods.HttpExecutionAware
    //   58	393	5	i	int
    //   71	52	6	j	int
    //   25	274	7	localObject1	Object
    //   88	450	8	localCloseableHttpResponse	cz.msebera.android.httpclient.client.methods.CloseableHttpResponse
    //   43	48	9	localRequestConfig	cz.msebera.android.httpclient.client.config.RequestConfig
    //   200	215	10	localURI	java.net.URI
    //   207	160	11	localObject2	Object
    //   252	113	12	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   90	115	476	cz/msebera/android/httpclient/HttpException
    //   125	172	476	cz/msebera/android/httpclient/HttpException
    //   172	195	476	cz/msebera/android/httpclient/HttpException
    //   195	209	476	cz/msebera/android/httpclient/HttpException
    //   214	231	476	cz/msebera/android/httpclient/HttpException
    //   235	248	476	cz/msebera/android/httpclient/HttpException
    //   248	254	476	cz/msebera/android/httpclient/HttpException
    //   259	265	476	cz/msebera/android/httpclient/HttpException
    //   269	292	476	cz/msebera/android/httpclient/HttpException
    //   292	374	476	cz/msebera/android/httpclient/HttpException
    //   374	391	476	cz/msebera/android/httpclient/HttpException
    //   394	430	476	cz/msebera/android/httpclient/HttpException
    //   430	473	476	cz/msebera/android/httpclient/HttpException
    //   477	487	497	finally
    //   502	512	497	finally
    //   477	487	501	java/io/IOException
    //   90	115	526	java/io/IOException
    //   125	172	526	java/io/IOException
    //   172	195	526	java/io/IOException
    //   195	209	526	java/io/IOException
    //   214	231	526	java/io/IOException
    //   235	248	526	java/io/IOException
    //   248	254	526	java/io/IOException
    //   259	265	526	java/io/IOException
    //   269	292	526	java/io/IOException
    //   292	374	526	java/io/IOException
    //   374	391	526	java/io/IOException
    //   394	430	526	java/io/IOException
    //   430	473	526	java/io/IOException
    //   90	115	536	java/lang/RuntimeException
    //   125	172	536	java/lang/RuntimeException
    //   172	195	536	java/lang/RuntimeException
    //   195	209	536	java/lang/RuntimeException
    //   214	231	536	java/lang/RuntimeException
    //   235	248	536	java/lang/RuntimeException
    //   248	254	536	java/lang/RuntimeException
    //   259	265	536	java/lang/RuntimeException
    //   269	292	536	java/lang/RuntimeException
    //   292	374	536	java/lang/RuntimeException
    //   374	391	536	java/lang/RuntimeException
    //   394	430	536	java/lang/RuntimeException
    //   430	473	536	java/lang/RuntimeException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/RedirectExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */