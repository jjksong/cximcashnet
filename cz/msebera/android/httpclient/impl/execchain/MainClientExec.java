package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthProtocolState;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.NonRepeatableRequestException;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.client.protocol.RequestClientConnControl;
import cz.msebera.android.httpclient.concurrent.Cancellable;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.ConnectionRequest;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.routing.BasicRouteDirector;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRouteDirector;
import cz.msebera.android.httpclient.conn.routing.RouteInfo;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.auth.HttpAuthenticator;
import cz.msebera.android.httpclient.impl.conn.ConnectionShutdownException;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;
import cz.msebera.android.httpclient.protocol.RequestTargetHost;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Immutable
public class MainClientExec
  implements ClientExecChain
{
  private final HttpAuthenticator authenticator;
  private final HttpClientConnectionManager connManager;
  private final ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final AuthenticationStrategy proxyAuthStrategy;
  private final HttpProcessor proxyHttpProcessor;
  private final HttpRequestExecutor requestExecutor;
  private final ConnectionReuseStrategy reuseStrategy;
  private final HttpRouteDirector routeDirector;
  private final AuthenticationStrategy targetAuthStrategy;
  private final UserTokenHandler userTokenHandler;
  
  public MainClientExec(HttpRequestExecutor paramHttpRequestExecutor, HttpClientConnectionManager paramHttpClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, AuthenticationStrategy paramAuthenticationStrategy1, AuthenticationStrategy paramAuthenticationStrategy2, UserTokenHandler paramUserTokenHandler)
  {
    Args.notNull(paramHttpRequestExecutor, "HTTP request executor");
    Args.notNull(paramHttpClientConnectionManager, "Client connection manager");
    Args.notNull(paramConnectionReuseStrategy, "Connection reuse strategy");
    Args.notNull(paramConnectionKeepAliveStrategy, "Connection keep alive strategy");
    Args.notNull(paramAuthenticationStrategy1, "Target authentication strategy");
    Args.notNull(paramAuthenticationStrategy2, "Proxy authentication strategy");
    Args.notNull(paramUserTokenHandler, "User token handler");
    this.authenticator = new HttpAuthenticator();
    this.proxyHttpProcessor = new ImmutableHttpProcessor(new HttpRequestInterceptor[] { new RequestTargetHost(), new RequestClientConnControl() });
    this.routeDirector = new BasicRouteDirector();
    this.requestExecutor = paramHttpRequestExecutor;
    this.connManager = paramHttpClientConnectionManager;
    this.reuseStrategy = paramConnectionReuseStrategy;
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
    this.targetAuthStrategy = paramAuthenticationStrategy1;
    this.proxyAuthStrategy = paramAuthenticationStrategy2;
    this.userTokenHandler = paramUserTokenHandler;
  }
  
  private boolean createTunnelToProxy(HttpRoute paramHttpRoute, int paramInt, HttpClientContext paramHttpClientContext)
    throws HttpException
  {
    throw new HttpException("Proxy chains are not supported.");
  }
  
  private boolean createTunnelToTarget(AuthState paramAuthState, HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpRequest paramHttpRequest, HttpClientContext paramHttpClientContext)
    throws HttpException, IOException
  {
    RequestConfig localRequestConfig = paramHttpClientContext.getRequestConfig();
    int j = localRequestConfig.getConnectTimeout();
    Object localObject = paramHttpRoute.getTargetHost();
    HttpHost localHttpHost = paramHttpRoute.getProxyHost();
    localObject = new BasicHttpRequest("CONNECT", ((HttpHost)localObject).toHostString(), paramHttpRequest.getProtocolVersion());
    this.requestExecutor.preProcess((HttpRequest)localObject, this.proxyHttpProcessor, paramHttpClientContext);
    paramHttpRequest = null;
    for (;;)
    {
      int i = 0;
      if (paramHttpRequest != null) {
        break label319;
      }
      if (!paramHttpClientConnection.isOpen())
      {
        paramHttpRequest = this.connManager;
        if (j > 0) {
          i = j;
        }
        paramHttpRequest.connect(paramHttpClientConnection, paramHttpRoute, i, paramHttpClientContext);
      }
      ((HttpRequest)localObject).removeHeaders("Proxy-Authorization");
      this.authenticator.generateAuthResponse((HttpRequest)localObject, paramAuthState, paramHttpClientContext);
      paramHttpRequest = this.requestExecutor.execute((HttpRequest)localObject, paramHttpClientConnection, paramHttpClientContext);
      if (paramHttpRequest.getStatusLine().getStatusCode() < 200) {
        break;
      }
      if (localRequestConfig.isAuthenticationEnabled()) {
        if (this.authenticator.isAuthenticationRequested(localHttpHost, paramHttpRequest, this.proxyAuthStrategy, paramAuthState, paramHttpClientContext))
        {
          if (this.authenticator.handleAuthChallenge(localHttpHost, paramHttpRequest, this.proxyAuthStrategy, paramAuthState, paramHttpClientContext))
          {
            if (this.reuseStrategy.keepAlive(paramHttpRequest, paramHttpClientContext))
            {
              this.log.debug("Connection kept alive");
              EntityUtils.consume(paramHttpRequest.getEntity());
            }
            else
            {
              paramHttpClientConnection.close();
            }
            paramHttpRequest = null;
          }
          else {}
        }
        else {}
      }
    }
    paramAuthState = new StringBuilder();
    paramAuthState.append("Unexpected response to CONNECT request: ");
    paramAuthState.append(paramHttpRequest.getStatusLine());
    throw new HttpException(paramAuthState.toString());
    label319:
    if (paramHttpRequest.getStatusLine().getStatusCode() > 299)
    {
      paramAuthState = paramHttpRequest.getEntity();
      if (paramAuthState != null) {
        paramHttpRequest.setEntity(new BufferedHttpEntity(paramAuthState));
      }
      paramHttpClientConnection.close();
      paramAuthState = new StringBuilder();
      paramAuthState.append("CONNECT refused by proxy: ");
      paramAuthState.append(paramHttpRequest.getStatusLine());
      throw new TunnelRefusedException(paramAuthState.toString(), paramHttpRequest);
    }
    return false;
  }
  
  private boolean needAuthentication(AuthState paramAuthState1, AuthState paramAuthState2, HttpRoute paramHttpRoute, HttpResponse paramHttpResponse, HttpClientContext paramHttpClientContext)
  {
    if (paramHttpClientContext.getRequestConfig().isAuthenticationEnabled())
    {
      Object localObject2 = paramHttpClientContext.getTargetHost();
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = paramHttpRoute.getTargetHost();
      }
      localObject2 = localObject1;
      if (((HttpHost)localObject1).getPort() < 0) {
        localObject2 = new HttpHost(((HttpHost)localObject1).getHostName(), paramHttpRoute.getTargetHost().getPort(), ((HttpHost)localObject1).getSchemeName());
      }
      boolean bool2 = this.authenticator.isAuthenticationRequested((HttpHost)localObject2, paramHttpResponse, this.targetAuthStrategy, paramAuthState1, paramHttpClientContext);
      HttpHost localHttpHost = paramHttpRoute.getProxyHost();
      localObject1 = localHttpHost;
      if (localHttpHost == null) {
        localObject1 = paramHttpRoute.getTargetHost();
      }
      boolean bool1 = this.authenticator.isAuthenticationRequested((HttpHost)localObject1, paramHttpResponse, this.proxyAuthStrategy, paramAuthState2, paramHttpClientContext);
      if (bool2) {
        return this.authenticator.handleAuthChallenge((HttpHost)localObject2, paramHttpResponse, this.targetAuthStrategy, paramAuthState1, paramHttpClientContext);
      }
      if (bool1) {
        return this.authenticator.handleAuthChallenge((HttpHost)localObject1, paramHttpResponse, this.proxyAuthStrategy, paramAuthState2, paramHttpClientContext);
      }
    }
    return false;
  }
  
  void establishRoute(AuthState paramAuthState, HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpRequest paramHttpRequest, HttpClientContext paramHttpClientContext)
    throws HttpException, IOException
  {
    int i = paramHttpClientContext.getRequestConfig().getConnectTimeout();
    RouteTracker localRouteTracker = new RouteTracker(paramHttpRoute);
    Object localObject;
    int k;
    do
    {
      localObject = localRouteTracker.toRoute();
      k = this.routeDirector.nextStep(paramHttpRoute, (RouteInfo)localObject);
      int j = 0;
      boolean bool;
      switch (k)
      {
      default: 
        paramAuthState = new StringBuilder();
        paramAuthState.append("Unknown step indicator ");
        paramAuthState.append(k);
        paramAuthState.append(" from RouteDirector.");
        throw new IllegalStateException(paramAuthState.toString());
      case 5: 
        this.connManager.upgrade(paramHttpClientConnection, paramHttpRoute, paramHttpClientContext);
        localRouteTracker.layerProtocol(paramHttpRoute.isSecure());
        break;
      case 4: 
        j = ((HttpRoute)localObject).getHopCount() - 1;
        bool = createTunnelToProxy(paramHttpRoute, j, paramHttpClientContext);
        this.log.debug("Tunnel to proxy created.");
        localRouteTracker.tunnelProxy(paramHttpRoute.getHopTarget(j), bool);
        break;
      case 3: 
        bool = createTunnelToTarget(paramAuthState, paramHttpClientConnection, paramHttpRoute, paramHttpRequest, paramHttpClientContext);
        this.log.debug("Tunnel to target created.");
        localRouteTracker.tunnelTarget(bool);
        break;
      case 2: 
        localObject = this.connManager;
        if (i > 0) {
          j = i;
        } else {
          j = 0;
        }
        ((HttpClientConnectionManager)localObject).connect(paramHttpClientConnection, paramHttpRoute, j, paramHttpClientContext);
        localRouteTracker.connectProxy(paramHttpRoute.getProxyHost(), false);
        break;
      case 1: 
        localObject = this.connManager;
        if (i > 0) {
          j = i;
        }
        ((HttpClientConnectionManager)localObject).connect(paramHttpClientConnection, paramHttpRoute, j, paramHttpClientContext);
        localRouteTracker.connectTarget(paramHttpRoute.isSecure());
        break;
      case 0: 
        this.connManager.routeComplete(paramHttpClientConnection, paramHttpRoute, paramHttpClientContext);
      }
    } while (k > 0);
    return;
    paramAuthState = new StringBuilder();
    paramAuthState.append("Unable to establish route: planned = ");
    paramAuthState.append(paramHttpRoute);
    paramAuthState.append("; current = ");
    paramAuthState.append(localObject);
    throw new HttpException(paramAuthState.toString());
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Object localObject1 = paramHttpRoute;
    Object localObject2 = paramHttpExecutionAware;
    Args.notNull(localObject1, "HTTP route");
    Args.notNull(paramHttpRequestWrapper, "HTTP request");
    Args.notNull(paramHttpClientContext, "HTTP context");
    AuthState localAuthState1 = paramHttpClientContext.getTargetAuthState();
    if (localAuthState1 == null)
    {
      localAuthState1 = new AuthState();
      paramHttpClientContext.setAttribute("http.auth.target-scope", localAuthState1);
    }
    AuthState localAuthState2 = paramHttpClientContext.getProxyAuthState();
    if (localAuthState2 == null)
    {
      localAuthState2 = new AuthState();
      paramHttpClientContext.setAttribute("http.auth.proxy-scope", localAuthState2);
    }
    if ((paramHttpRequestWrapper instanceof HttpEntityEnclosingRequest)) {
      RequestEntityProxy.enhance((HttpEntityEnclosingRequest)paramHttpRequestWrapper);
    }
    Object localObject6 = paramHttpClientContext.getUserToken();
    localObject1 = this.connManager.requestConnection((HttpRoute)localObject1, localObject6);
    if (localObject2 != null) {
      if (!paramHttpExecutionAware.isAborted())
      {
        ((HttpExecutionAware)localObject2).setCancellable((Cancellable)localObject1);
      }
      else
      {
        ((ConnectionRequest)localObject1).cancel();
        throw new RequestAbortedException("Request aborted");
      }
    }
    RequestConfig localRequestConfig = paramHttpClientContext.getRequestConfig();
    try
    {
      int i = localRequestConfig.getConnectionRequestTimeout();
      long l;
      if (i > 0) {
        l = i;
      } else {
        l = 0L;
      }
      Object localObject7 = ((ConnectionRequest)localObject1).get(l, TimeUnit.MILLISECONDS);
      paramHttpClientContext.setAttribute("http.connection", localObject7);
      if ((localRequestConfig.isStaleConnectionCheckEnabled()) && (((HttpClientConnection)localObject7).isOpen()))
      {
        this.log.debug("Stale connection check");
        if (((HttpClientConnection)localObject7).isStale())
        {
          this.log.debug("Stale connection detected");
          ((HttpClientConnection)localObject7).close();
        }
      }
      localObject1 = new ConnectionHolder(this.log, this.connManager, (HttpClientConnection)localObject7);
      Object localObject5;
      Object localObject3;
      Object localObject4;
      if (localObject2 != null)
      {
        localObject5 = localObject1;
        localObject3 = localObject1;
        localObject4 = localObject1;
        try
        {
          ((HttpExecutionAware)localObject2).setCancellable((Cancellable)localObject1);
        }
        catch (RuntimeException paramHttpRoute)
        {
          localObject1 = localObject5;
          break label1394;
        }
        catch (IOException paramHttpRoute)
        {
          paramHttpRequestWrapper = (HttpRequestWrapper)localObject3;
          break label1405;
        }
        catch (HttpException paramHttpRoute)
        {
          localObject1 = localObject4;
          break label1412;
        }
        catch (ConnectionShutdownException paramHttpRequestWrapper)
        {
          break label1419;
        }
      }
      i = 1;
      localObject2 = localObject6;
      localObject6 = localObject7;
      for (;;)
      {
        localObject7 = paramHttpExecutionAware;
        if (i > 1)
        {
          localObject5 = localObject1;
          localObject3 = localObject1;
          localObject4 = localObject1;
          if (!RequestEntityProxy.isRepeatable(paramHttpRequestWrapper))
          {
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            paramHttpRoute = new cz/msebera/android/httpclient/client/NonRepeatableRequestException;
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            paramHttpRoute.<init>("Cannot retry request with a non-repeatable request entity.");
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            throw paramHttpRoute;
          }
        }
        if (localObject7 != null)
        {
          localObject5 = localObject1;
          localObject3 = localObject1;
          localObject4 = localObject1;
          if (paramHttpExecutionAware.isAborted())
          {
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            paramHttpRoute = new cz/msebera/android/httpclient/impl/execchain/RequestAbortedException;
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            paramHttpRoute.<init>("Request aborted");
            localObject5 = localObject1;
            localObject3 = localObject1;
            localObject4 = localObject1;
            throw paramHttpRoute;
          }
        }
        try
        {
          if (!((HttpClientConnection)localObject6).isOpen())
          {
            localObject3 = this.log;
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            ((StringBuilder)localObject4).append("Opening connection ");
            ((StringBuilder)localObject4).append(paramHttpRoute);
            ((HttpClientAndroidLog)localObject3).debug(((StringBuilder)localObject4).toString());
          }
          try
          {
            try
            {
              establishRoute(localAuthState2, (HttpClientConnection)localObject6, paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext);
            }
            catch (TunnelRefusedException paramHttpRoute)
            {
              if (this.log.isDebugEnabled()) {
                this.log.debug(paramHttpRoute.getMessage());
              }
              localObject4 = paramHttpRoute.getResponse();
              break label1286;
            }
            localObject3 = localObject1;
            int j = localRequestConfig.getSocketTimeout();
            if (j >= 0) {
              ((HttpClientConnection)localObject6).setSocketTimeout(j);
            }
            if ((localObject7 != null) && (paramHttpExecutionAware.isAborted()))
            {
              paramHttpRoute = new cz/msebera/android/httpclient/impl/execchain/RequestAbortedException;
              paramHttpRoute.<init>("Request aborted");
              throw paramHttpRoute;
            }
            if (this.log.isDebugEnabled())
            {
              localObject4 = this.log;
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              ((StringBuilder)localObject5).append("Executing request ");
              ((StringBuilder)localObject5).append(paramHttpRequestWrapper.getRequestLine());
              ((HttpClientAndroidLog)localObject4).debug(((StringBuilder)localObject5).toString());
            }
            if (!paramHttpRequestWrapper.containsHeader("Authorization"))
            {
              if (this.log.isDebugEnabled())
              {
                localObject4 = this.log;
                localObject5 = new java/lang/StringBuilder;
                ((StringBuilder)localObject5).<init>();
                ((StringBuilder)localObject5).append("Target auth state: ");
                ((StringBuilder)localObject5).append(localAuthState1.getState());
                ((HttpClientAndroidLog)localObject4).debug(((StringBuilder)localObject5).toString());
              }
              this.authenticator.generateAuthResponse(paramHttpRequestWrapper, localAuthState1, paramHttpClientContext);
            }
            if ((!paramHttpRequestWrapper.containsHeader("Proxy-Authorization")) && (!paramHttpRoute.isTunnelled()))
            {
              if (this.log.isDebugEnabled())
              {
                localObject4 = this.log;
                localObject5 = new java/lang/StringBuilder;
                ((StringBuilder)localObject5).<init>();
                ((StringBuilder)localObject5).append("Proxy auth state: ");
                ((StringBuilder)localObject5).append(localAuthState2.getState());
                ((HttpClientAndroidLog)localObject4).debug(((StringBuilder)localObject5).toString());
              }
              this.authenticator.generateAuthResponse(paramHttpRequestWrapper, localAuthState2, paramHttpClientContext);
            }
            localObject7 = this.requestExecutor.execute(paramHttpRequestWrapper, (HttpClientConnection)localObject6, paramHttpClientContext);
            if (this.reuseStrategy.keepAlive((HttpResponse)localObject7, paramHttpClientContext))
            {
              l = this.keepAliveStrategy.getKeepAliveDuration((HttpResponse)localObject7, paramHttpClientContext);
              if (this.log.isDebugEnabled())
              {
                if (l > 0L)
                {
                  localObject4 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject4).<init>();
                  ((StringBuilder)localObject4).append("for ");
                  ((StringBuilder)localObject4).append(l);
                  ((StringBuilder)localObject4).append(" ");
                  ((StringBuilder)localObject4).append(TimeUnit.MILLISECONDS);
                  localObject4 = ((StringBuilder)localObject4).toString();
                }
                else
                {
                  localObject4 = "indefinitely";
                }
                localObject5 = this.log;
                StringBuilder localStringBuilder = new java/lang/StringBuilder;
                localStringBuilder.<init>();
                localStringBuilder.append("Connection can be kept alive ");
                localStringBuilder.append((String)localObject4);
                ((HttpClientAndroidLog)localObject5).debug(localStringBuilder.toString());
              }
              ((ConnectionHolder)localObject3).setValidFor(l, TimeUnit.MILLISECONDS);
              ((ConnectionHolder)localObject3).markReusable();
            }
            else
            {
              ((ConnectionHolder)localObject3).markNonReusable();
            }
            localObject5 = localObject7;
            localObject4 = localObject5;
            if (needAuthentication(localAuthState1, localAuthState2, paramHttpRoute, (HttpResponse)localObject7, paramHttpClientContext))
            {
              localObject4 = ((HttpResponse)localObject5).getEntity();
              if (((ConnectionHolder)localObject3).isReusable())
              {
                EntityUtils.consume((HttpEntity)localObject4);
              }
              else
              {
                ((HttpClientConnection)localObject6).close();
                if ((localAuthState2.getState() == AuthProtocolState.SUCCESS) && (localAuthState2.getAuthScheme() != null) && (localAuthState2.getAuthScheme().isConnectionBased()))
                {
                  this.log.debug("Resetting proxy auth state");
                  localAuthState2.reset();
                }
                if ((localAuthState1.getState() == AuthProtocolState.SUCCESS) && (localAuthState1.getAuthScheme() != null) && (localAuthState1.getAuthScheme().isConnectionBased()))
                {
                  this.log.debug("Resetting target auth state");
                  localAuthState1.reset();
                }
              }
              localObject4 = paramHttpRequestWrapper.getOriginal();
              if (!((HttpRequest)localObject4).containsHeader("Authorization")) {
                paramHttpRequestWrapper.removeHeaders("Authorization");
              }
              if (!((HttpRequest)localObject4).containsHeader("Proxy-Authorization")) {
                paramHttpRequestWrapper.removeHeaders("Proxy-Authorization");
              }
              i++;
              localObject1 = localObject3;
              continue;
            }
            label1286:
            paramHttpRoute = (HttpRoute)localObject1;
            if (localObject2 == null)
            {
              localObject2 = this.userTokenHandler.getUserToken(paramHttpClientContext);
              paramHttpClientContext.setAttribute("http.user-token", localObject2);
            }
            if (localObject2 != null) {
              paramHttpRoute.setState(localObject2);
            }
            paramHttpRequestWrapper = ((HttpResponse)localObject4).getEntity();
            if ((paramHttpRequestWrapper != null) && (paramHttpRequestWrapper.isStreaming())) {
              return new HttpResponseProxy((HttpResponse)localObject4, paramHttpRoute);
            }
            paramHttpRoute.releaseConnection();
            paramHttpRoute = new HttpResponseProxy((HttpResponse)localObject4, null);
            return paramHttpRoute;
          }
          catch (RuntimeException paramHttpRoute) {}catch (IOException paramHttpRoute) {}catch (HttpException paramHttpRoute) {}
          label1394:
          label1405:
          ((ConnectionHolder)localObject1).abortConnection();
        }
        catch (RuntimeException paramHttpRoute)
        {
          ((ConnectionHolder)localObject1).abortConnection();
          throw paramHttpRoute;
        }
        catch (IOException paramHttpRoute)
        {
          paramHttpRequestWrapper = (HttpRequestWrapper)localObject1;
          paramHttpRequestWrapper.abortConnection();
          throw paramHttpRoute;
        }
        catch (HttpException paramHttpRoute) {}
      }
      label1412:
      throw paramHttpRoute;
    }
    catch (ExecutionException paramHttpRoute)
    {
      paramHttpRequestWrapper = paramHttpRoute.getCause();
      if (paramHttpRequestWrapper == null) {
        break label1453;
      }
      paramHttpRoute = paramHttpRequestWrapper;
      throw new RequestAbortedException("Request execution failed", paramHttpRoute);
    }
    catch (InterruptedException paramHttpRoute)
    {
      label1419:
      Thread.currentThread().interrupt();
      throw new RequestAbortedException("Request aborted", paramHttpRoute);
    }
    paramHttpRoute = new InterruptedIOException("Connection has been shut down");
    paramHttpRoute.initCause(paramHttpRequestWrapper);
    throw paramHttpRoute;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/MainClientExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */