package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.concurrent.Cancellable;
import cz.msebera.android.httpclient.conn.ConnectionReleaseTrigger;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@ThreadSafe
class ConnectionHolder
  implements ConnectionReleaseTrigger, Cancellable, Closeable
{
  public HttpClientAndroidLog log;
  private final HttpClientConnection managedConn;
  private final HttpClientConnectionManager manager;
  private volatile boolean released;
  private volatile boolean reusable;
  private volatile Object state;
  private volatile TimeUnit tunit;
  private volatile long validDuration;
  
  public ConnectionHolder(HttpClientAndroidLog paramHttpClientAndroidLog, HttpClientConnectionManager paramHttpClientConnectionManager, HttpClientConnection paramHttpClientConnection)
  {
    this.log = paramHttpClientAndroidLog;
    this.manager = paramHttpClientConnectionManager;
    this.managedConn = paramHttpClientConnection;
  }
  
  /* Error */
  public void abortConnection()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 43	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:released	Z
    //   11: ifeq +6 -> 17
    //   14: aload_1
    //   15: monitorexit
    //   16: return
    //   17: aload_0
    //   18: iconst_1
    //   19: putfield 43	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:released	Z
    //   22: aload_0
    //   23: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   26: invokeinterface 48 1 0
    //   31: aload_0
    //   32: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   35: ldc 50
    //   37: invokevirtual 56	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   40: aload_0
    //   41: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   44: aload_0
    //   45: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   48: aconst_null
    //   49: lconst_0
    //   50: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   53: invokeinterface 67 6 0
    //   58: goto +48 -> 106
    //   61: astore_2
    //   62: goto +47 -> 109
    //   65: astore_2
    //   66: aload_0
    //   67: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   70: invokevirtual 71	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   73: ifeq +15 -> 88
    //   76: aload_0
    //   77: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   80: aload_2
    //   81: invokevirtual 75	java/io/IOException:getMessage	()Ljava/lang/String;
    //   84: aload_2
    //   85: invokevirtual 78	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   88: aload_0
    //   89: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   92: aload_0
    //   93: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   96: aconst_null
    //   97: lconst_0
    //   98: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   101: invokeinterface 67 6 0
    //   106: aload_1
    //   107: monitorexit
    //   108: return
    //   109: aload_0
    //   110: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   113: aload_0
    //   114: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   117: aconst_null
    //   118: lconst_0
    //   119: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   122: invokeinterface 67 6 0
    //   127: aload_2
    //   128: athrow
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	134	0	this	ConnectionHolder
    //   4	127	1	localHttpClientConnection	HttpClientConnection
    //   61	1	2	localObject1	Object
    //   65	63	2	localIOException	IOException
    //   129	4	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   22	40	61	finally
    //   66	88	61	finally
    //   22	40	65	java/io/IOException
    //   7	16	129	finally
    //   17	22	129	finally
    //   40	58	129	finally
    //   88	106	129	finally
    //   106	108	129	finally
    //   109	129	129	finally
    //   130	132	129	finally
  }
  
  public boolean cancel()
  {
    boolean bool = this.released;
    this.log.debug("Cancelling request execution");
    abortConnection();
    return bool ^ true;
  }
  
  public void close()
    throws IOException
  {
    abortConnection();
  }
  
  public boolean isReleased()
  {
    return this.released;
  }
  
  public boolean isReusable()
  {
    return this.reusable;
  }
  
  public void markNonReusable()
  {
    this.reusable = false;
  }
  
  public void markReusable()
  {
    this.reusable = true;
  }
  
  /* Error */
  public void releaseConnection()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 43	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:released	Z
    //   11: ifeq +6 -> 17
    //   14: aload_1
    //   15: monitorexit
    //   16: return
    //   17: aload_0
    //   18: iconst_1
    //   19: putfield 43	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:released	Z
    //   22: aload_0
    //   23: getfield 89	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:reusable	Z
    //   26: ifeq +31 -> 57
    //   29: aload_0
    //   30: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   33: aload_0
    //   34: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   37: aload_0
    //   38: getfield 93	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:state	Ljava/lang/Object;
    //   41: aload_0
    //   42: getfield 95	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:validDuration	J
    //   45: aload_0
    //   46: getfield 97	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:tunit	Ljava/util/concurrent/TimeUnit;
    //   49: invokeinterface 67 6 0
    //   54: goto +87 -> 141
    //   57: aload_0
    //   58: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   61: invokeinterface 99 1 0
    //   66: aload_0
    //   67: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   70: ldc 50
    //   72: invokevirtual 56	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   75: aload_0
    //   76: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   79: aload_0
    //   80: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   83: aconst_null
    //   84: lconst_0
    //   85: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   88: invokeinterface 67 6 0
    //   93: goto +48 -> 141
    //   96: astore_2
    //   97: goto +47 -> 144
    //   100: astore_2
    //   101: aload_0
    //   102: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   105: invokevirtual 71	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   108: ifeq +15 -> 123
    //   111: aload_0
    //   112: getfield 33	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   115: aload_2
    //   116: invokevirtual 75	java/io/IOException:getMessage	()Ljava/lang/String;
    //   119: aload_2
    //   120: invokevirtual 78	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   123: aload_0
    //   124: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   127: aload_0
    //   128: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   131: aconst_null
    //   132: lconst_0
    //   133: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   136: invokeinterface 67 6 0
    //   141: aload_1
    //   142: monitorexit
    //   143: return
    //   144: aload_0
    //   145: getfield 35	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:manager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   148: aload_0
    //   149: getfield 37	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:managedConn	Lcz/msebera/android/httpclient/HttpClientConnection;
    //   152: aconst_null
    //   153: lconst_0
    //   154: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   157: invokeinterface 67 6 0
    //   162: aload_2
    //   163: athrow
    //   164: astore_2
    //   165: aload_1
    //   166: monitorexit
    //   167: aload_2
    //   168: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	169	0	this	ConnectionHolder
    //   4	162	1	localHttpClientConnection	HttpClientConnection
    //   96	1	2	localObject1	Object
    //   100	63	2	localIOException	IOException
    //   164	4	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   57	75	96	finally
    //   101	123	96	finally
    //   57	75	100	java/io/IOException
    //   7	16	164	finally
    //   17	54	164	finally
    //   75	93	164	finally
    //   123	141	164	finally
    //   141	143	164	finally
    //   144	164	164	finally
    //   165	167	164	finally
  }
  
  public void setState(Object paramObject)
  {
    this.state = paramObject;
  }
  
  public void setValidFor(long paramLong, TimeUnit paramTimeUnit)
  {
    synchronized (this.managedConn)
    {
      this.validDuration = paramLong;
      this.tunit = paramTimeUnit;
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/ConnectionHolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */