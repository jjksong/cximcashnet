package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.client.BasicCredentialsProvider;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Immutable
public class ProtocolExec
  implements ClientExecChain
{
  private final HttpProcessor httpProcessor;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ClientExecChain requestExecutor;
  
  public ProtocolExec(ClientExecChain paramClientExecChain, HttpProcessor paramHttpProcessor)
  {
    Args.notNull(paramClientExecChain, "HTTP client request executor");
    Args.notNull(paramHttpProcessor, "HTTP protocol processor");
    this.requestExecutor = paramClientExecChain;
    this.httpProcessor = paramHttpProcessor;
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    Args.notNull(paramHttpRequestWrapper, "HTTP request");
    Args.notNull(paramHttpClientContext, "HTTP context");
    Object localObject1 = paramHttpRequestWrapper.getOriginal();
    boolean bool = localObject1 instanceof HttpUriRequest;
    Object localObject5 = null;
    URI localURI;
    Object localObject2;
    if (bool)
    {
      localURI = ((HttpUriRequest)localObject1).getURI();
    }
    else
    {
      localObject1 = ((HttpRequest)localObject1).getRequestLine().getUri();
      try
      {
        localURI = URI.create((String)localObject1);
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        if (this.log.isDebugEnabled())
        {
          localObject3 = this.log;
          localObject4 = new StringBuilder();
          ((StringBuilder)localObject4).append("Unable to parse '");
          ((StringBuilder)localObject4).append((String)localObject1);
          ((StringBuilder)localObject4).append("' as a valid URI; ");
          ((StringBuilder)localObject4).append("request URI and Host header may be inconsistent");
          ((HttpClientAndroidLog)localObject3).debug(((StringBuilder)localObject4).toString(), localIllegalArgumentException);
        }
        localObject2 = null;
      }
    }
    paramHttpRequestWrapper.setURI((URI)localObject2);
    rewriteRequestURI(paramHttpRequestWrapper, paramHttpRoute);
    Object localObject4 = (HttpHost)paramHttpRequestWrapper.getParams().getParameter("http.virtual-host");
    Object localObject3 = localObject4;
    if (localObject4 != null)
    {
      localObject3 = localObject4;
      if (((HttpHost)localObject4).getPort() == -1)
      {
        int i = paramHttpRoute.getTargetHost().getPort();
        localObject1 = localObject4;
        if (i != -1) {
          localObject1 = new HttpHost(((HttpHost)localObject4).getHostName(), i, ((HttpHost)localObject4).getSchemeName());
        }
        localObject3 = localObject1;
        if (this.log.isDebugEnabled())
        {
          localObject4 = this.log;
          localObject3 = new StringBuilder();
          ((StringBuilder)localObject3).append("Using virtual host");
          ((StringBuilder)localObject3).append(localObject1);
          ((HttpClientAndroidLog)localObject4).debug(((StringBuilder)localObject3).toString());
          localObject3 = localObject1;
        }
      }
    }
    if (localObject3 != null)
    {
      localObject1 = localObject3;
    }
    else
    {
      localObject1 = localObject5;
      if (localObject2 != null)
      {
        localObject1 = localObject5;
        if (((URI)localObject2).isAbsolute())
        {
          localObject1 = localObject5;
          if (((URI)localObject2).getHost() != null) {
            localObject1 = new HttpHost(((URI)localObject2).getHost(), ((URI)localObject2).getPort(), ((URI)localObject2).getScheme());
          }
        }
      }
    }
    localObject3 = localObject1;
    if (localObject1 == null) {
      localObject3 = paramHttpRoute.getTargetHost();
    }
    if (localObject2 != null)
    {
      localObject4 = ((URI)localObject2).getUserInfo();
      if (localObject4 != null)
      {
        localObject2 = paramHttpClientContext.getCredentialsProvider();
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject1 = new BasicCredentialsProvider();
          paramHttpClientContext.setCredentialsProvider((CredentialsProvider)localObject1);
        }
        ((CredentialsProvider)localObject1).setCredentials(new AuthScope((HttpHost)localObject3), new UsernamePasswordCredentials((String)localObject4));
      }
    }
    paramHttpClientContext.setAttribute("http.target_host", localObject3);
    paramHttpClientContext.setAttribute("http.route", paramHttpRoute);
    paramHttpClientContext.setAttribute("http.request", paramHttpRequestWrapper);
    this.httpProcessor.process(paramHttpRequestWrapper, paramHttpClientContext);
    paramHttpRoute = this.requestExecutor.execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    try
    {
      paramHttpClientContext.setAttribute("http.response", paramHttpRoute);
      this.httpProcessor.process(paramHttpRoute, paramHttpClientContext);
      return paramHttpRoute;
    }
    catch (HttpException paramHttpRequestWrapper)
    {
      paramHttpRoute.close();
      throw paramHttpRequestWrapper;
    }
    catch (IOException paramHttpRequestWrapper)
    {
      paramHttpRoute.close();
      throw paramHttpRequestWrapper;
    }
    catch (RuntimeException paramHttpRequestWrapper)
    {
      paramHttpRoute.close();
      throw paramHttpRequestWrapper;
    }
  }
  
  void rewriteRequestURI(HttpRequestWrapper paramHttpRequestWrapper, HttpRoute paramHttpRoute)
    throws ProtocolException
  {
    try
    {
      URI localURI = paramHttpRequestWrapper.getURI();
      if (localURI != null)
      {
        if ((paramHttpRoute.getProxyHost() != null) && (!paramHttpRoute.isTunnelled()))
        {
          if (!localURI.isAbsolute()) {
            paramHttpRoute = URIUtils.rewriteURI(localURI, paramHttpRoute.getTargetHost(), true);
          } else {
            paramHttpRoute = URIUtils.rewriteURI(localURI);
          }
        }
        else if (localURI.isAbsolute()) {
          paramHttpRoute = URIUtils.rewriteURI(localURI, null, true);
        } else {
          paramHttpRoute = URIUtils.rewriteURI(localURI);
        }
        paramHttpRequestWrapper.setURI(paramHttpRoute);
      }
      return;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      paramHttpRoute = new StringBuilder();
      paramHttpRoute.append("Invalid URI: ");
      paramHttpRoute.append(paramHttpRequestWrapper.getRequestLine().getUri());
      throw new ProtocolException(paramHttpRoute.toString(), localURISyntaxException);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/ProtocolExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */