package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.NoHttpResponseException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.NonRepeatableRequestException;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class RetryExec
  implements ClientExecChain
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ClientExecChain requestExecutor;
  private final HttpRequestRetryHandler retryHandler;
  
  public RetryExec(ClientExecChain paramClientExecChain, HttpRequestRetryHandler paramHttpRequestRetryHandler)
  {
    Args.notNull(paramClientExecChain, "HTTP request executor");
    Args.notNull(paramHttpRequestRetryHandler, "HTTP request retry handler");
    this.requestExecutor = paramClientExecChain;
    this.retryHandler = paramHttpRequestRetryHandler;
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    Args.notNull(paramHttpRequestWrapper, "HTTP request");
    Args.notNull(paramHttpClientContext, "HTTP context");
    Header[] arrayOfHeader = paramHttpRequestWrapper.getAllHeaders();
    int i = 1;
    try
    {
      CloseableHttpResponse localCloseableHttpResponse = this.requestExecutor.execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
      return localCloseableHttpResponse;
    }
    catch (IOException localIOException)
    {
      StringBuilder localStringBuilder1;
      for (;;)
      {
        if ((paramHttpExecutionAware != null) && (paramHttpExecutionAware.isAborted()))
        {
          this.log.debug("Request has been aborted");
          throw localIOException;
        }
        if (!this.retryHandler.retryRequest(localIOException, i, paramHttpClientContext)) {
          break label303;
        }
        HttpClientAndroidLog localHttpClientAndroidLog;
        if (this.log.isInfoEnabled())
        {
          localHttpClientAndroidLog = this.log;
          StringBuilder localStringBuilder2 = new StringBuilder();
          localStringBuilder2.append("I/O exception (");
          localStringBuilder2.append(localIOException.getClass().getName());
          localStringBuilder2.append(") caught when processing request to ");
          localStringBuilder2.append(paramHttpRoute);
          localStringBuilder2.append(": ");
          localStringBuilder2.append(localIOException.getMessage());
          localHttpClientAndroidLog.info(localStringBuilder2.toString());
        }
        if (this.log.isDebugEnabled()) {
          this.log.debug(localIOException.getMessage(), localIOException);
        }
        if (!RequestEntityProxy.isRepeatable(paramHttpRequestWrapper)) {
          break;
        }
        paramHttpRequestWrapper.setHeaders(arrayOfHeader);
        if (this.log.isInfoEnabled())
        {
          localHttpClientAndroidLog = this.log;
          localStringBuilder1 = new StringBuilder();
          localStringBuilder1.append("Retrying request to ");
          localStringBuilder1.append(paramHttpRoute);
          localHttpClientAndroidLog.info(localStringBuilder1.toString());
        }
        i++;
      }
      this.log.debug("Cannot retry non-repeatable request");
      throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity", localStringBuilder1);
      label303:
      if ((localStringBuilder1 instanceof NoHttpResponseException))
      {
        paramHttpRequestWrapper = new StringBuilder();
        paramHttpRequestWrapper.append(paramHttpRoute.getTargetHost().toHostString());
        paramHttpRequestWrapper.append(" failed to respond");
        paramHttpRoute = new NoHttpResponseException(paramHttpRequestWrapper.toString());
        paramHttpRoute.setStackTrace(localStringBuilder1.getStackTrace());
        throw paramHttpRoute;
      }
      throw localStringBuilder1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/RetryExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */