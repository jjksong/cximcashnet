package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.EofSensorInputStream;
import cz.msebera.android.httpclient.conn.EofSensorWatcher;
import cz.msebera.android.httpclient.entity.HttpEntityWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;

@NotThreadSafe
class ResponseEntityProxy
  extends HttpEntityWrapper
  implements EofSensorWatcher
{
  private final ConnectionHolder connHolder;
  
  ResponseEntityProxy(HttpEntity paramHttpEntity, ConnectionHolder paramConnectionHolder)
  {
    super(paramHttpEntity);
    this.connHolder = paramConnectionHolder;
  }
  
  private void cleanup()
  {
    ConnectionHolder localConnectionHolder = this.connHolder;
    if (localConnectionHolder != null) {
      localConnectionHolder.abortConnection();
    }
  }
  
  public static void enchance(HttpResponse paramHttpResponse, ConnectionHolder paramConnectionHolder)
  {
    HttpEntity localHttpEntity = paramHttpResponse.getEntity();
    if ((localHttpEntity != null) && (localHttpEntity.isStreaming()) && (paramConnectionHolder != null)) {
      paramHttpResponse.setEntity(new ResponseEntityProxy(localHttpEntity, paramConnectionHolder));
    }
  }
  
  @Deprecated
  public void consumeContent()
    throws IOException
  {
    releaseConnection();
  }
  
  public boolean eofDetected(InputStream paramInputStream)
    throws IOException
  {
    try
    {
      paramInputStream.close();
      releaseConnection();
      return false;
    }
    finally
    {
      cleanup();
    }
  }
  
  public InputStream getContent()
    throws IOException
  {
    return new EofSensorInputStream(this.wrappedEntity.getContent(), this);
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  /* Error */
  public void releaseConnection()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 16	cz/msebera/android/httpclient/impl/execchain/ResponseEntityProxy:connHolder	Lcz/msebera/android/httpclient/impl/execchain/ConnectionHolder;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +31 -> 37
    //   9: aload_1
    //   10: invokevirtual 78	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:isReusable	()Z
    //   13: ifeq +10 -> 23
    //   16: aload_0
    //   17: getfield 16	cz/msebera/android/httpclient/impl/execchain/ResponseEntityProxy:connHolder	Lcz/msebera/android/httpclient/impl/execchain/ConnectionHolder;
    //   20: invokevirtual 79	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:releaseConnection	()V
    //   23: aload_0
    //   24: invokespecial 61	cz/msebera/android/httpclient/impl/execchain/ResponseEntityProxy:cleanup	()V
    //   27: goto +10 -> 37
    //   30: astore_1
    //   31: aload_0
    //   32: invokespecial 61	cz/msebera/android/httpclient/impl/execchain/ResponseEntityProxy:cleanup	()V
    //   35: aload_1
    //   36: athrow
    //   37: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	38	0	this	ResponseEntityProxy
    //   4	6	1	localConnectionHolder	ConnectionHolder
    //   30	6	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	23	30	finally
  }
  
  public boolean streamAbort(InputStream paramInputStream)
    throws IOException
  {
    cleanup();
    return false;
  }
  
  public boolean streamClosed(InputStream paramInputStream)
    throws IOException
  {
    try
    {
      if (this.connHolder != null)
      {
        boolean bool = this.connHolder.isReleased();
        if (!bool)
        {
          i = 1;
          break label26;
        }
      }
      int i = 0;
      try
      {
        label26:
        paramInputStream.close();
        releaseConnection();
      }
      catch (SocketException paramInputStream)
      {
        if (i != 0) {
          break label48;
        }
      }
      return false;
    }
    finally
    {
      label48:
      cleanup();
    }
    throw paramInputStream;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("ResponseEntityProxy{");
    localStringBuilder.append(this.wrappedEntity);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    try
    {
      this.wrappedEntity.writeTo(paramOutputStream);
      releaseConnection();
      return;
    }
    finally
    {
      cleanup();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/ResponseEntityProxy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */