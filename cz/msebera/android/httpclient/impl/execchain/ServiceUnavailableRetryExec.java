package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.ServiceUnavailableRetryStrategy;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InterruptedIOException;

@Immutable
public class ServiceUnavailableRetryExec
  implements ClientExecChain
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ClientExecChain requestExecutor;
  private final ServiceUnavailableRetryStrategy retryStrategy;
  
  public ServiceUnavailableRetryExec(ClientExecChain paramClientExecChain, ServiceUnavailableRetryStrategy paramServiceUnavailableRetryStrategy)
  {
    Args.notNull(paramClientExecChain, "HTTP request executor");
    Args.notNull(paramServiceUnavailableRetryStrategy, "Retry strategy");
    this.requestExecutor = paramClientExecChain;
    this.retryStrategy = paramServiceUnavailableRetryStrategy;
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Header[] arrayOfHeader = paramHttpRequestWrapper.getAllHeaders();
    int i = 1;
    for (;;)
    {
      CloseableHttpResponse localCloseableHttpResponse = this.requestExecutor.execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
      try
      {
        if (this.retryStrategy.retryRequest(localCloseableHttpResponse, i, paramHttpClientContext))
        {
          localCloseableHttpResponse.close();
          long l = this.retryStrategy.getRetryInterval();
          if (l > 0L) {
            try
            {
              HttpClientAndroidLog localHttpClientAndroidLog = this.log;
              StringBuilder localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>();
              localStringBuilder.append("Wait for ");
              localStringBuilder.append(l);
              localHttpClientAndroidLog.trace(localStringBuilder.toString());
              Thread.sleep(l);
            }
            catch (InterruptedException paramHttpRoute)
            {
              Thread.currentThread().interrupt();
              paramHttpRoute = new java/io/InterruptedIOException;
              paramHttpRoute.<init>();
              throw paramHttpRoute;
            }
          }
          paramHttpRequestWrapper.setHeaders(arrayOfHeader);
          i++;
          continue;
        }
        return localCloseableHttpResponse;
      }
      catch (RuntimeException paramHttpRoute)
      {
        localCloseableHttpResponse.close();
        throw paramHttpRoute;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/ServiceUnavailableRetryExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */