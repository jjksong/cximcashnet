package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.RequestClientConnControl;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;
import cz.msebera.android.httpclient.protocol.RequestContent;
import cz.msebera.android.httpclient.protocol.RequestTargetHost;
import cz.msebera.android.httpclient.protocol.RequestUserAgent;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.VersionInfo;
import java.net.URI;
import java.net.URISyntaxException;

@Immutable
public class MinimalClientExec
  implements ClientExecChain
{
  private final HttpClientConnectionManager connManager;
  private final HttpProcessor httpProcessor;
  private final ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final HttpRequestExecutor requestExecutor;
  private final ConnectionReuseStrategy reuseStrategy;
  
  public MinimalClientExec(HttpRequestExecutor paramHttpRequestExecutor, HttpClientConnectionManager paramHttpClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy)
  {
    Args.notNull(paramHttpRequestExecutor, "HTTP request executor");
    Args.notNull(paramHttpClientConnectionManager, "Client connection manager");
    Args.notNull(paramConnectionReuseStrategy, "Connection reuse strategy");
    Args.notNull(paramConnectionKeepAliveStrategy, "Connection keep alive strategy");
    this.httpProcessor = new ImmutableHttpProcessor(new HttpRequestInterceptor[] { new RequestContent(), new RequestTargetHost(), new RequestClientConnControl(), new RequestUserAgent(VersionInfo.getUserAgent("Apache-HttpClient", "cz.msebera.android.httpclient.client", getClass())) });
    this.requestExecutor = paramHttpRequestExecutor;
    this.connManager = paramHttpClientConnectionManager;
    this.reuseStrategy = paramConnectionReuseStrategy;
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
  }
  
  static void rewriteRequestURI(HttpRequestWrapper paramHttpRequestWrapper, HttpRoute paramHttpRoute)
    throws ProtocolException
  {
    try
    {
      paramHttpRoute = paramHttpRequestWrapper.getURI();
      if (paramHttpRoute != null)
      {
        if (paramHttpRoute.isAbsolute()) {
          paramHttpRoute = URIUtils.rewriteURI(paramHttpRoute, null, true);
        } else {
          paramHttpRoute = URIUtils.rewriteURI(paramHttpRoute);
        }
        paramHttpRequestWrapper.setURI(paramHttpRoute);
      }
      return;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      paramHttpRoute = new StringBuilder();
      paramHttpRoute.append("Invalid URI: ");
      paramHttpRoute.append(paramHttpRequestWrapper.getRequestLine().getUri());
      throw new ProtocolException(paramHttpRoute.toString(), localURISyntaxException);
    }
  }
  
  /* Error */
  public cz.msebera.android.httpclient.client.methods.CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, cz.msebera.android.httpclient.client.protocol.HttpClientContext paramHttpClientContext, cz.msebera.android.httpclient.client.methods.HttpExecutionAware paramHttpExecutionAware)
    throws java.io.IOException, cz.msebera.android.httpclient.HttpException
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -92
    //   3: invokestatic 43	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_2
    //   8: ldc -90
    //   10: invokestatic 43	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   13: pop
    //   14: aload_3
    //   15: ldc -88
    //   17: invokestatic 43	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   20: pop
    //   21: aload_2
    //   22: aload_1
    //   23: invokestatic 170	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:rewriteRequestURI	(Lcz/msebera/android/httpclient/client/methods/HttpRequestWrapper;Lcz/msebera/android/httpclient/conn/routing/HttpRoute;)V
    //   26: aload_0
    //   27: getfield 86	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:connManager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   30: aload_1
    //   31: aconst_null
    //   32: invokeinterface 176 3 0
    //   37: astore 9
    //   39: aload 4
    //   41: ifnull +43 -> 84
    //   44: aload 4
    //   46: invokeinterface 181 1 0
    //   51: ifne +15 -> 66
    //   54: aload 4
    //   56: aload 9
    //   58: invokeinterface 185 2 0
    //   63: goto +21 -> 84
    //   66: aload 9
    //   68: invokeinterface 190 1 0
    //   73: pop
    //   74: new 192	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException
    //   77: dup
    //   78: ldc -62
    //   80: invokespecial 195	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException:<init>	(Ljava/lang/String;)V
    //   83: athrow
    //   84: aload_3
    //   85: invokevirtual 201	cz/msebera/android/httpclient/client/protocol/HttpClientContext:getRequestConfig	()Lcz/msebera/android/httpclient/client/config/RequestConfig;
    //   88: astore 8
    //   90: aload 8
    //   92: invokevirtual 207	cz/msebera/android/httpclient/client/config/RequestConfig:getConnectionRequestTimeout	()I
    //   95: istore 5
    //   97: iload 5
    //   99: ifle +11 -> 110
    //   102: iload 5
    //   104: i2l
    //   105: lstore 6
    //   107: goto +6 -> 113
    //   110: lconst_0
    //   111: lstore 6
    //   113: aload 9
    //   115: lload 6
    //   117: getstatic 213	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   120: invokeinterface 217 4 0
    //   125: astore 10
    //   127: new 219	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder
    //   130: dup
    //   131: aload_0
    //   132: getfield 35	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   135: aload_0
    //   136: getfield 86	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:connManager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   139: aload 10
    //   141: invokespecial 222	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:<init>	(Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;Lcz/msebera/android/httpclient/HttpClientConnection;)V
    //   144: astore 9
    //   146: aload 4
    //   148: ifnull +42 -> 190
    //   151: aload 4
    //   153: invokeinterface 181 1 0
    //   158: ifne +15 -> 173
    //   161: aload 4
    //   163: aload 9
    //   165: invokeinterface 185 2 0
    //   170: goto +20 -> 190
    //   173: aload 9
    //   175: invokevirtual 225	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:close	()V
    //   178: new 192	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException
    //   181: astore_1
    //   182: aload_1
    //   183: ldc -62
    //   185: invokespecial 195	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException:<init>	(Ljava/lang/String;)V
    //   188: aload_1
    //   189: athrow
    //   190: aload 10
    //   192: invokeinterface 230 1 0
    //   197: ifne +53 -> 250
    //   200: aload 8
    //   202: invokevirtual 233	cz/msebera/android/httpclient/client/config/RequestConfig:getConnectTimeout	()I
    //   205: istore 5
    //   207: aload_0
    //   208: getfield 86	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:connManager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   211: astore 4
    //   213: iload 5
    //   215: ifle +6 -> 221
    //   218: goto +6 -> 224
    //   221: iconst_0
    //   222: istore 5
    //   224: aload 4
    //   226: aload 10
    //   228: aload_1
    //   229: iload 5
    //   231: aload_3
    //   232: invokeinterface 237 5 0
    //   237: aload_0
    //   238: getfield 86	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:connManager	Lcz/msebera/android/httpclient/conn/HttpClientConnectionManager;
    //   241: aload 10
    //   243: aload_1
    //   244: aload_3
    //   245: invokeinterface 241 4 0
    //   250: aload 8
    //   252: invokevirtual 244	cz/msebera/android/httpclient/client/config/RequestConfig:getSocketTimeout	()I
    //   255: istore 5
    //   257: iload 5
    //   259: iflt +12 -> 271
    //   262: aload 10
    //   264: iload 5
    //   266: invokeinterface 248 2 0
    //   271: aload_2
    //   272: invokevirtual 252	cz/msebera/android/httpclient/client/methods/HttpRequestWrapper:getOriginal	()Lcz/msebera/android/httpclient/HttpRequest;
    //   275: astore 4
    //   277: aload 4
    //   279: instanceof 254
    //   282: ifeq +51 -> 333
    //   285: aload 4
    //   287: checkcast 254	cz/msebera/android/httpclient/client/methods/HttpUriRequest
    //   290: invokeinterface 255 1 0
    //   295: astore 8
    //   297: aload 8
    //   299: invokevirtual 109	java/net/URI:isAbsolute	()Z
    //   302: ifeq +31 -> 333
    //   305: new 257	cz/msebera/android/httpclient/HttpHost
    //   308: astore 4
    //   310: aload 4
    //   312: aload 8
    //   314: invokevirtual 260	java/net/URI:getHost	()Ljava/lang/String;
    //   317: aload 8
    //   319: invokevirtual 263	java/net/URI:getPort	()I
    //   322: aload 8
    //   324: invokevirtual 266	java/net/URI:getScheme	()Ljava/lang/String;
    //   327: invokespecial 269	cz/msebera/android/httpclient/HttpHost:<init>	(Ljava/lang/String;ILjava/lang/String;)V
    //   330: goto +6 -> 336
    //   333: aconst_null
    //   334: astore 4
    //   336: aload 4
    //   338: astore 8
    //   340: aload 4
    //   342: ifnonnull +9 -> 351
    //   345: aload_1
    //   346: invokevirtual 275	cz/msebera/android/httpclient/conn/routing/HttpRoute:getTargetHost	()Lcz/msebera/android/httpclient/HttpHost;
    //   349: astore 8
    //   351: aload_3
    //   352: ldc_w 277
    //   355: aload 8
    //   357: invokevirtual 281	cz/msebera/android/httpclient/client/protocol/HttpClientContext:setAttribute	(Ljava/lang/String;Ljava/lang/Object;)V
    //   360: aload_3
    //   361: ldc_w 283
    //   364: aload_2
    //   365: invokevirtual 281	cz/msebera/android/httpclient/client/protocol/HttpClientContext:setAttribute	(Ljava/lang/String;Ljava/lang/Object;)V
    //   368: aload_3
    //   369: ldc_w 285
    //   372: aload 10
    //   374: invokevirtual 281	cz/msebera/android/httpclient/client/protocol/HttpClientContext:setAttribute	(Ljava/lang/String;Ljava/lang/Object;)V
    //   377: aload_3
    //   378: ldc_w 287
    //   381: aload_1
    //   382: invokevirtual 281	cz/msebera/android/httpclient/client/protocol/HttpClientContext:setAttribute	(Ljava/lang/String;Ljava/lang/Object;)V
    //   385: aload_0
    //   386: getfield 82	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:httpProcessor	Lcz/msebera/android/httpclient/protocol/HttpProcessor;
    //   389: aload_2
    //   390: aload_3
    //   391: invokeinterface 293 3 0
    //   396: aload_0
    //   397: getfield 84	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:requestExecutor	Lcz/msebera/android/httpclient/protocol/HttpRequestExecutor;
    //   400: aload_2
    //   401: aload 10
    //   403: aload_3
    //   404: invokevirtual 298	cz/msebera/android/httpclient/protocol/HttpRequestExecutor:execute	(Lcz/msebera/android/httpclient/HttpRequest;Lcz/msebera/android/httpclient/HttpClientConnection;Lcz/msebera/android/httpclient/protocol/HttpContext;)Lcz/msebera/android/httpclient/HttpResponse;
    //   407: astore_1
    //   408: aload_0
    //   409: getfield 82	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:httpProcessor	Lcz/msebera/android/httpclient/protocol/HttpProcessor;
    //   412: aload_1
    //   413: aload_3
    //   414: invokeinterface 301 3 0
    //   419: aload_0
    //   420: getfield 88	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:reuseStrategy	Lcz/msebera/android/httpclient/ConnectionReuseStrategy;
    //   423: aload_1
    //   424: aload_3
    //   425: invokeinterface 307 3 0
    //   430: ifeq +30 -> 460
    //   433: aload 9
    //   435: aload_0
    //   436: getfield 90	cz/msebera/android/httpclient/impl/execchain/MinimalClientExec:keepAliveStrategy	Lcz/msebera/android/httpclient/conn/ConnectionKeepAliveStrategy;
    //   439: aload_1
    //   440: aload_3
    //   441: invokeinterface 313 3 0
    //   446: getstatic 213	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   449: invokevirtual 317	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:setValidFor	(JLjava/util/concurrent/TimeUnit;)V
    //   452: aload 9
    //   454: invokevirtual 320	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:markReusable	()V
    //   457: goto +8 -> 465
    //   460: aload 9
    //   462: invokevirtual 323	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:markNonReusable	()V
    //   465: aload_1
    //   466: invokeinterface 329 1 0
    //   471: astore_2
    //   472: aload_2
    //   473: ifnull +26 -> 499
    //   476: aload_2
    //   477: invokeinterface 334 1 0
    //   482: ifne +6 -> 488
    //   485: goto +14 -> 499
    //   488: new 336	cz/msebera/android/httpclient/impl/execchain/HttpResponseProxy
    //   491: dup
    //   492: aload_1
    //   493: aload 9
    //   495: invokespecial 339	cz/msebera/android/httpclient/impl/execchain/HttpResponseProxy:<init>	(Lcz/msebera/android/httpclient/HttpResponse;Lcz/msebera/android/httpclient/impl/execchain/ConnectionHolder;)V
    //   498: areturn
    //   499: aload 9
    //   501: invokevirtual 342	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:releaseConnection	()V
    //   504: new 336	cz/msebera/android/httpclient/impl/execchain/HttpResponseProxy
    //   507: dup
    //   508: aload_1
    //   509: aconst_null
    //   510: invokespecial 339	cz/msebera/android/httpclient/impl/execchain/HttpResponseProxy:<init>	(Lcz/msebera/android/httpclient/HttpResponse;Lcz/msebera/android/httpclient/impl/execchain/ConnectionHolder;)V
    //   513: astore_1
    //   514: aload_1
    //   515: areturn
    //   516: astore_1
    //   517: aload 9
    //   519: invokevirtual 345	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:abortConnection	()V
    //   522: aload_1
    //   523: athrow
    //   524: astore_1
    //   525: aload 9
    //   527: invokevirtual 345	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:abortConnection	()V
    //   530: aload_1
    //   531: athrow
    //   532: astore_1
    //   533: aload 9
    //   535: invokevirtual 345	cz/msebera/android/httpclient/impl/execchain/ConnectionHolder:abortConnection	()V
    //   538: aload_1
    //   539: athrow
    //   540: astore_1
    //   541: new 347	java/io/InterruptedIOException
    //   544: dup
    //   545: ldc_w 349
    //   548: invokespecial 350	java/io/InterruptedIOException:<init>	(Ljava/lang/String;)V
    //   551: astore_2
    //   552: aload_2
    //   553: aload_1
    //   554: invokevirtual 354	java/io/InterruptedIOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   557: pop
    //   558: aload_2
    //   559: athrow
    //   560: astore_1
    //   561: aload_1
    //   562: invokevirtual 358	java/util/concurrent/ExecutionException:getCause	()Ljava/lang/Throwable;
    //   565: astore_2
    //   566: aload_2
    //   567: ifnonnull +6 -> 573
    //   570: goto +5 -> 575
    //   573: aload_2
    //   574: astore_1
    //   575: new 192	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException
    //   578: dup
    //   579: ldc_w 360
    //   582: aload_1
    //   583: invokespecial 361	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   586: athrow
    //   587: astore_1
    //   588: invokestatic 367	java/lang/Thread:currentThread	()Ljava/lang/Thread;
    //   591: invokevirtual 370	java/lang/Thread:interrupt	()V
    //   594: new 192	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException
    //   597: dup
    //   598: ldc -62
    //   600: aload_1
    //   601: invokespecial 361	cz/msebera/android/httpclient/impl/execchain/RequestAbortedException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   604: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	605	0	this	MinimalClientExec
    //   0	605	1	paramHttpRoute	HttpRoute
    //   0	605	2	paramHttpRequestWrapper	HttpRequestWrapper
    //   0	605	3	paramHttpClientContext	cz.msebera.android.httpclient.client.protocol.HttpClientContext
    //   0	605	4	paramHttpExecutionAware	cz.msebera.android.httpclient.client.methods.HttpExecutionAware
    //   95	170	5	i	int
    //   105	11	6	l	long
    //   88	268	8	localObject1	Object
    //   37	497	9	localObject2	Object
    //   125	277	10	localHttpClientConnection	cz.msebera.android.httpclient.HttpClientConnection
    // Exception table:
    //   from	to	target	type
    //   151	170	516	java/lang/RuntimeException
    //   173	190	516	java/lang/RuntimeException
    //   190	213	516	java/lang/RuntimeException
    //   224	250	516	java/lang/RuntimeException
    //   250	257	516	java/lang/RuntimeException
    //   262	271	516	java/lang/RuntimeException
    //   271	330	516	java/lang/RuntimeException
    //   345	351	516	java/lang/RuntimeException
    //   351	457	516	java/lang/RuntimeException
    //   460	465	516	java/lang/RuntimeException
    //   465	472	516	java/lang/RuntimeException
    //   476	485	516	java/lang/RuntimeException
    //   488	499	516	java/lang/RuntimeException
    //   499	514	516	java/lang/RuntimeException
    //   151	170	524	java/io/IOException
    //   173	190	524	java/io/IOException
    //   190	213	524	java/io/IOException
    //   224	250	524	java/io/IOException
    //   250	257	524	java/io/IOException
    //   262	271	524	java/io/IOException
    //   271	330	524	java/io/IOException
    //   345	351	524	java/io/IOException
    //   351	457	524	java/io/IOException
    //   460	465	524	java/io/IOException
    //   465	472	524	java/io/IOException
    //   476	485	524	java/io/IOException
    //   488	499	524	java/io/IOException
    //   499	514	524	java/io/IOException
    //   151	170	532	cz/msebera/android/httpclient/HttpException
    //   173	190	532	cz/msebera/android/httpclient/HttpException
    //   190	213	532	cz/msebera/android/httpclient/HttpException
    //   224	250	532	cz/msebera/android/httpclient/HttpException
    //   250	257	532	cz/msebera/android/httpclient/HttpException
    //   262	271	532	cz/msebera/android/httpclient/HttpException
    //   271	330	532	cz/msebera/android/httpclient/HttpException
    //   345	351	532	cz/msebera/android/httpclient/HttpException
    //   351	457	532	cz/msebera/android/httpclient/HttpException
    //   460	465	532	cz/msebera/android/httpclient/HttpException
    //   465	472	532	cz/msebera/android/httpclient/HttpException
    //   476	485	532	cz/msebera/android/httpclient/HttpException
    //   488	499	532	cz/msebera/android/httpclient/HttpException
    //   499	514	532	cz/msebera/android/httpclient/HttpException
    //   151	170	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   173	190	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   190	213	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   224	250	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   250	257	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   262	271	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   271	330	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   345	351	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   351	457	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   460	465	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   465	472	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   476	485	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   488	499	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   499	514	540	cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException
    //   90	97	560	java/util/concurrent/ExecutionException
    //   113	127	560	java/util/concurrent/ExecutionException
    //   90	97	587	java/lang/InterruptedException
    //   113	127	587	java/lang/InterruptedException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/MinimalClientExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */