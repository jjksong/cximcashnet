package cz.msebera.android.httpclient.impl.execchain;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.BackoffManager;
import cz.msebera.android.httpclient.client.ConnectionBackoffStrategy;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;

@Immutable
public class BackoffStrategyExec
  implements ClientExecChain
{
  private final BackoffManager backoffManager;
  private final ConnectionBackoffStrategy connectionBackoffStrategy;
  private final ClientExecChain requestExecutor;
  
  public BackoffStrategyExec(ClientExecChain paramClientExecChain, ConnectionBackoffStrategy paramConnectionBackoffStrategy, BackoffManager paramBackoffManager)
  {
    Args.notNull(paramClientExecChain, "HTTP client request executor");
    Args.notNull(paramConnectionBackoffStrategy, "Connection backoff strategy");
    Args.notNull(paramBackoffManager, "Backoff manager");
    this.requestExecutor = paramClientExecChain;
    this.connectionBackoffStrategy = paramConnectionBackoffStrategy;
    this.backoffManager = paramBackoffManager;
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    Args.notNull(paramHttpRequestWrapper, "HTTP request");
    Args.notNull(paramHttpClientContext, "HTTP context");
    try
    {
      paramHttpRequestWrapper = this.requestExecutor.execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
      if (this.connectionBackoffStrategy.shouldBackoff(paramHttpRequestWrapper)) {
        this.backoffManager.backOff(paramHttpRoute);
      } else {
        this.backoffManager.probe(paramHttpRoute);
      }
      return paramHttpRequestWrapper;
    }
    catch (Exception paramHttpRequestWrapper)
    {
      if (this.connectionBackoffStrategy.shouldBackoff(paramHttpRequestWrapper)) {
        this.backoffManager.backOff(paramHttpRoute);
      }
      if (!(paramHttpRequestWrapper instanceof RuntimeException))
      {
        if (!(paramHttpRequestWrapper instanceof HttpException))
        {
          if ((paramHttpRequestWrapper instanceof IOException)) {
            throw ((IOException)paramHttpRequestWrapper);
          }
          throw new UndeclaredThrowableException(paramHttpRequestWrapper);
        }
        throw ((HttpException)paramHttpRequestWrapper);
      }
      throw ((RuntimeException)paramHttpRequestWrapper);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/execchain/BackoffStrategyExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */