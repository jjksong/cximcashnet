package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.impl.io.DefaultHttpRequestWriterFactory;
import cz.msebera.android.httpclient.impl.io.DefaultHttpResponseParserFactory;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.HttpMessageWriter;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

@NotThreadSafe
public class DefaultBHttpClientConnection
  extends BHttpConnectionBase
  implements HttpClientConnection
{
  private final HttpMessageWriter<HttpRequest> requestWriter;
  private final HttpMessageParser<HttpResponse> responseParser;
  
  public DefaultBHttpClientConnection(int paramInt)
  {
    this(paramInt, paramInt, null, null, null, null, null, null, null);
  }
  
  public DefaultBHttpClientConnection(int paramInt1, int paramInt2, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints, ContentLengthStrategy paramContentLengthStrategy1, ContentLengthStrategy paramContentLengthStrategy2, HttpMessageWriterFactory<HttpRequest> paramHttpMessageWriterFactory, HttpMessageParserFactory<HttpResponse> paramHttpMessageParserFactory)
  {
    super(paramInt1, paramInt2, paramCharsetDecoder, paramCharsetEncoder, paramMessageConstraints, paramContentLengthStrategy1, paramContentLengthStrategy2);
    if (paramHttpMessageWriterFactory == null) {
      paramHttpMessageWriterFactory = DefaultHttpRequestWriterFactory.INSTANCE;
    }
    this.requestWriter = paramHttpMessageWriterFactory.create(getSessionOutputBuffer());
    if (paramHttpMessageParserFactory == null) {
      paramHttpMessageParserFactory = DefaultHttpResponseParserFactory.INSTANCE;
    }
    this.responseParser = paramHttpMessageParserFactory.create(getSessionInputBuffer(), paramMessageConstraints);
  }
  
  public DefaultBHttpClientConnection(int paramInt, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints)
  {
    this(paramInt, paramInt, paramCharsetDecoder, paramCharsetEncoder, paramMessageConstraints, null, null, null, null);
  }
  
  public void bind(Socket paramSocket)
    throws IOException
  {
    super.bind(paramSocket);
  }
  
  public void flush()
    throws IOException
  {
    ensureOpen();
    doFlush();
  }
  
  public boolean isResponseAvailable(int paramInt)
    throws IOException
  {
    ensureOpen();
    try
    {
      boolean bool = awaitInput(paramInt);
      return bool;
    }
    catch (SocketTimeoutException localSocketTimeoutException) {}
    return false;
  }
  
  protected void onRequestSubmitted(HttpRequest paramHttpRequest) {}
  
  protected void onResponseReceived(HttpResponse paramHttpResponse) {}
  
  public void receiveResponseEntity(HttpResponse paramHttpResponse)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    ensureOpen();
    paramHttpResponse.setEntity(prepareInput(paramHttpResponse));
  }
  
  public HttpResponse receiveResponseHeader()
    throws HttpException, IOException
  {
    ensureOpen();
    HttpResponse localHttpResponse = (HttpResponse)this.responseParser.parse();
    onResponseReceived(localHttpResponse);
    if (localHttpResponse.getStatusLine().getStatusCode() >= 200) {
      incrementResponseCount();
    }
    return localHttpResponse;
  }
  
  public void sendRequestEntity(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpEntityEnclosingRequest, "HTTP request");
    ensureOpen();
    HttpEntity localHttpEntity = paramHttpEntityEnclosingRequest.getEntity();
    if (localHttpEntity == null) {
      return;
    }
    paramHttpEntityEnclosingRequest = prepareOutput(paramHttpEntityEnclosingRequest);
    localHttpEntity.writeTo(paramHttpEntityEnclosingRequest);
    paramHttpEntityEnclosingRequest.close();
  }
  
  public void sendRequestHeader(HttpRequest paramHttpRequest)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    ensureOpen();
    this.requestWriter.write(paramHttpRequest);
    onRequestSubmitted(paramHttpRequest);
    incrementRequestCount();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/DefaultBHttpClientConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */