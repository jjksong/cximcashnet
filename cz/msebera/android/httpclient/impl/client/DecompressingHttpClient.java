package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.protocol.RequestAcceptEncoding;
import cz.msebera.android.httpclient.client.protocol.ResponseContentEncoding;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;

@Deprecated
public class DecompressingHttpClient
  implements HttpClient
{
  private final HttpRequestInterceptor acceptEncodingInterceptor;
  private final HttpClient backend;
  private final HttpResponseInterceptor contentEncodingInterceptor;
  
  public DecompressingHttpClient()
  {
    this(new DefaultHttpClient());
  }
  
  public DecompressingHttpClient(HttpClient paramHttpClient)
  {
    this(paramHttpClient, new RequestAcceptEncoding(), new ResponseContentEncoding());
  }
  
  DecompressingHttpClient(HttpClient paramHttpClient, HttpRequestInterceptor paramHttpRequestInterceptor, HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    this.backend = paramHttpClient;
    this.acceptEncodingInterceptor = paramHttpRequestInterceptor;
    this.contentEncodingInterceptor = paramHttpResponseInterceptor;
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException, ClientProtocolException
  {
    return execute(paramHttpHost, paramHttpRequest, (HttpContext)null);
  }
  
  /* Error */
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    // Byte code:
    //   0: aload_3
    //   1: ifnull +6 -> 7
    //   4: goto +11 -> 15
    //   7: new 57	cz/msebera/android/httpclient/protocol/BasicHttpContext
    //   10: dup
    //   11: invokespecial 58	cz/msebera/android/httpclient/protocol/BasicHttpContext:<init>	()V
    //   14: astore_3
    //   15: aload_2
    //   16: instanceof 60
    //   19: ifeq +23 -> 42
    //   22: new 62	cz/msebera/android/httpclient/impl/client/EntityEnclosingRequestWrapper
    //   25: astore 4
    //   27: aload 4
    //   29: aload_2
    //   30: checkcast 60	cz/msebera/android/httpclient/HttpEntityEnclosingRequest
    //   33: invokespecial 65	cz/msebera/android/httpclient/impl/client/EntityEnclosingRequestWrapper:<init>	(Lcz/msebera/android/httpclient/HttpEntityEnclosingRequest;)V
    //   36: aload 4
    //   38: astore_2
    //   39: goto +12 -> 51
    //   42: new 67	cz/msebera/android/httpclient/impl/client/RequestWrapper
    //   45: dup
    //   46: aload_2
    //   47: invokespecial 70	cz/msebera/android/httpclient/impl/client/RequestWrapper:<init>	(Lcz/msebera/android/httpclient/HttpRequest;)V
    //   50: astore_2
    //   51: aload_0
    //   52: getfield 37	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:acceptEncodingInterceptor	Lcz/msebera/android/httpclient/HttpRequestInterceptor;
    //   55: aload_2
    //   56: aload_3
    //   57: invokeinterface 76 3 0
    //   62: aload_0
    //   63: getfield 35	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:backend	Lcz/msebera/android/httpclient/client/HttpClient;
    //   66: aload_1
    //   67: aload_2
    //   68: aload_3
    //   69: invokeinterface 77 4 0
    //   74: astore_1
    //   75: aload_0
    //   76: getfield 39	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:contentEncodingInterceptor	Lcz/msebera/android/httpclient/HttpResponseInterceptor;
    //   79: aload_1
    //   80: aload_3
    //   81: invokeinterface 82 3 0
    //   86: getstatic 88	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   89: aload_3
    //   90: ldc 90
    //   92: invokeinterface 94 2 0
    //   97: invokevirtual 98	java/lang/Boolean:equals	(Ljava/lang/Object;)Z
    //   100: ifeq +27 -> 127
    //   103: aload_1
    //   104: ldc 100
    //   106: invokeinterface 106 2 0
    //   111: aload_1
    //   112: ldc 108
    //   114: invokeinterface 106 2 0
    //   119: aload_1
    //   120: ldc 110
    //   122: invokeinterface 106 2 0
    //   127: aload_1
    //   128: areturn
    //   129: astore_2
    //   130: aload_1
    //   131: invokeinterface 114 1 0
    //   136: invokestatic 120	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   139: aload_2
    //   140: athrow
    //   141: astore_2
    //   142: aload_1
    //   143: invokeinterface 114 1 0
    //   148: invokestatic 120	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   151: aload_2
    //   152: athrow
    //   153: astore_2
    //   154: aload_1
    //   155: invokeinterface 114 1 0
    //   160: invokestatic 120	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   163: aload_2
    //   164: athrow
    //   165: astore_1
    //   166: new 45	cz/msebera/android/httpclient/client/ClientProtocolException
    //   169: dup
    //   170: aload_1
    //   171: invokespecial 123	cz/msebera/android/httpclient/client/ClientProtocolException:<init>	(Ljava/lang/Throwable;)V
    //   174: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	175	0	this	DecompressingHttpClient
    //   0	175	1	paramHttpHost	HttpHost
    //   0	175	2	paramHttpRequest	HttpRequest
    //   0	175	3	paramHttpContext	HttpContext
    //   25	12	4	localEntityEnclosingRequestWrapper	EntityEnclosingRequestWrapper
    // Exception table:
    //   from	to	target	type
    //   75	127	129	java/lang/RuntimeException
    //   75	127	141	java/io/IOException
    //   75	127	153	cz/msebera/android/httpclient/HttpException
    //   7	15	165	cz/msebera/android/httpclient/HttpException
    //   15	36	165	cz/msebera/android/httpclient/HttpException
    //   42	51	165	cz/msebera/android/httpclient/HttpException
    //   51	75	165	cz/msebera/android/httpclient/HttpException
    //   130	141	165	cz/msebera/android/httpclient/HttpException
    //   142	153	165	cz/msebera/android/httpclient/HttpException
    //   154	165	165	cz/msebera/android/httpclient/HttpException
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws IOException, ClientProtocolException
  {
    return execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, (HttpContext)null);
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramHttpContext);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(paramHttpHost, paramHttpRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    paramHttpHost = execute(paramHttpHost, paramHttpRequest, paramHttpContext);
    try
    {
      paramHttpRequest = paramResponseHandler.handleResponse(paramHttpHost);
      return paramHttpRequest;
    }
    finally
    {
      paramHttpHost = paramHttpHost.getEntity();
      if (paramHttpHost != null) {
        EntityUtils.consume(paramHttpHost);
      }
    }
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramResponseHandler);
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return (T)execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramResponseHandler, paramHttpContext);
  }
  
  public ClientConnectionManager getConnectionManager()
  {
    return this.backend.getConnectionManager();
  }
  
  public HttpClient getHttpClient()
  {
    return this.backend;
  }
  
  HttpHost getHttpHost(HttpUriRequest paramHttpUriRequest)
  {
    return URIUtils.extractHost(paramHttpUriRequest.getURI());
  }
  
  public HttpParams getParams()
  {
    return this.backend.getParams();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DecompressingHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */