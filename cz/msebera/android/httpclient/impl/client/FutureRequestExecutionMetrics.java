package cz.msebera.android.httpclient.impl.client;

import java.util.concurrent.atomic.AtomicLong;

public final class FutureRequestExecutionMetrics
{
  private final AtomicLong activeConnections = new AtomicLong();
  private final DurationCounter failedConnections = new DurationCounter();
  private final DurationCounter requests = new DurationCounter();
  private final AtomicLong scheduledConnections = new AtomicLong();
  private final DurationCounter successfulConnections = new DurationCounter();
  private final DurationCounter tasks = new DurationCounter();
  
  public long getActiveConnectionCount()
  {
    return this.activeConnections.get();
  }
  
  AtomicLong getActiveConnections()
  {
    return this.activeConnections;
  }
  
  public long getFailedConnectionAverageDuration()
  {
    return this.failedConnections.averageDuration();
  }
  
  public long getFailedConnectionCount()
  {
    return this.failedConnections.count();
  }
  
  DurationCounter getFailedConnections()
  {
    return this.failedConnections;
  }
  
  public long getRequestAverageDuration()
  {
    return this.requests.averageDuration();
  }
  
  public long getRequestCount()
  {
    return this.requests.count();
  }
  
  DurationCounter getRequests()
  {
    return this.requests;
  }
  
  public long getScheduledConnectionCount()
  {
    return this.scheduledConnections.get();
  }
  
  AtomicLong getScheduledConnections()
  {
    return this.scheduledConnections;
  }
  
  public long getSuccessfulConnectionAverageDuration()
  {
    return this.successfulConnections.averageDuration();
  }
  
  public long getSuccessfulConnectionCount()
  {
    return this.successfulConnections.count();
  }
  
  DurationCounter getSuccessfulConnections()
  {
    return this.successfulConnections;
  }
  
  public long getTaskAverageDuration()
  {
    return this.tasks.averageDuration();
  }
  
  public long getTaskCount()
  {
    return this.tasks.count();
  }
  
  DurationCounter getTasks()
  {
    return this.tasks;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[activeConnections=");
    localStringBuilder.append(this.activeConnections);
    localStringBuilder.append(", scheduledConnections=");
    localStringBuilder.append(this.scheduledConnections);
    localStringBuilder.append(", successfulConnections=");
    localStringBuilder.append(this.successfulConnections);
    localStringBuilder.append(", failedConnections=");
    localStringBuilder.append(this.failedConnections);
    localStringBuilder.append(", requests=");
    localStringBuilder.append(this.requests);
    localStringBuilder.append(", tasks=");
    localStringBuilder.append(this.tasks);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  static class DurationCounter
  {
    private final AtomicLong count = new AtomicLong(0L);
    private final AtomicLong cumulativeDuration = new AtomicLong(0L);
    
    public long averageDuration()
    {
      long l2 = this.count.get();
      long l1 = 0L;
      if (l2 > 0L) {
        l1 = this.cumulativeDuration.get() / l2;
      }
      return l1;
    }
    
    public long count()
    {
      return this.count.get();
    }
    
    public void increment(long paramLong)
    {
      this.count.incrementAndGet();
      this.cumulativeDuration.addAndGet(System.currentTimeMillis() - paramLong);
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[count=");
      localStringBuilder.append(count());
      localStringBuilder.append(", averageDuration=");
      localStringBuilder.append(averageDuration());
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/FutureRequestExecutionMetrics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */