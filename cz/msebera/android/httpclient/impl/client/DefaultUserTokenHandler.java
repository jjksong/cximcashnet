package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpConnection;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.security.Principal;
import javax.net.ssl.SSLSession;

@Immutable
public class DefaultUserTokenHandler
  implements UserTokenHandler
{
  public static final DefaultUserTokenHandler INSTANCE = new DefaultUserTokenHandler();
  
  private static Principal getAuthPrincipal(AuthState paramAuthState)
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    if ((localAuthScheme != null) && (localAuthScheme.isComplete()) && (localAuthScheme.isConnectionBased()))
    {
      paramAuthState = paramAuthState.getCredentials();
      if (paramAuthState != null) {
        return paramAuthState.getUserPrincipal();
      }
    }
    return null;
  }
  
  public Object getUserToken(HttpContext paramHttpContext)
  {
    Object localObject2 = HttpClientContext.adapt(paramHttpContext);
    paramHttpContext = ((HttpClientContext)localObject2).getTargetAuthState();
    if (paramHttpContext != null)
    {
      localObject1 = getAuthPrincipal(paramHttpContext);
      paramHttpContext = (HttpContext)localObject1;
      if (localObject1 == null) {
        paramHttpContext = getAuthPrincipal(((HttpClientContext)localObject2).getProxyAuthState());
      }
    }
    else
    {
      paramHttpContext = null;
    }
    Object localObject1 = paramHttpContext;
    if (paramHttpContext == null)
    {
      localObject2 = ((HttpClientContext)localObject2).getConnection();
      localObject1 = paramHttpContext;
      if (((HttpConnection)localObject2).isOpen())
      {
        localObject1 = paramHttpContext;
        if ((localObject2 instanceof ManagedHttpClientConnection))
        {
          localObject2 = ((ManagedHttpClientConnection)localObject2).getSSLSession();
          localObject1 = paramHttpContext;
          if (localObject2 != null) {
            localObject1 = ((SSLSession)localObject2).getLocalPrincipal();
          }
        }
      }
    }
    return localObject1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultUserTokenHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */