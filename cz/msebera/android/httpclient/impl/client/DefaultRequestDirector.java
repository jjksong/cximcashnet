package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NoHttpResponseException;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthProtocolState;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.NonRepeatableRequestException;
import cz.msebera.android.httpclient.client.RedirectException;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.RequestDirector;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.client.methods.AbortableHttpRequest;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.params.HttpClientParams;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.BasicManagedEntity;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.BasicRouteDirector;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRouteDirector;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.auth.BasicScheme;
import cz.msebera.android.httpclient.impl.conn.ConnectionShutdownException;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

@Deprecated
@NotThreadSafe
public class DefaultRequestDirector
  implements RequestDirector
{
  private final HttpAuthenticator authenticator;
  protected final ClientConnectionManager connManager;
  private int execCount;
  protected final HttpProcessor httpProcessor;
  protected final ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log;
  protected ManagedClientConnection managedConn;
  private final int maxRedirects;
  protected final HttpParams params;
  @Deprecated
  protected final AuthenticationHandler proxyAuthHandler;
  protected final AuthState proxyAuthState;
  protected final AuthenticationStrategy proxyAuthStrategy;
  private int redirectCount;
  @Deprecated
  protected final RedirectHandler redirectHandler;
  protected final RedirectStrategy redirectStrategy;
  protected final HttpRequestExecutor requestExec;
  protected final HttpRequestRetryHandler retryHandler;
  protected final ConnectionReuseStrategy reuseStrategy;
  protected final HttpRoutePlanner routePlanner;
  @Deprecated
  protected final AuthenticationHandler targetAuthHandler;
  protected final AuthState targetAuthState;
  protected final AuthenticationStrategy targetAuthStrategy;
  protected final UserTokenHandler userTokenHandler;
  private HttpHost virtualHost;
  
  @Deprecated
  public DefaultRequestDirector(HttpClientAndroidLog paramHttpClientAndroidLog, HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    this(new HttpClientAndroidLog(DefaultRequestDirector.class), paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, new AuthenticationStrategyAdaptor(paramAuthenticationHandler1), new AuthenticationStrategyAdaptor(paramAuthenticationHandler2), paramUserTokenHandler, paramHttpParams);
  }
  
  public DefaultRequestDirector(HttpClientAndroidLog paramHttpClientAndroidLog, HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationStrategy paramAuthenticationStrategy1, AuthenticationStrategy paramAuthenticationStrategy2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    Args.notNull(paramHttpClientAndroidLog, "Log");
    Args.notNull(paramHttpRequestExecutor, "Request executor");
    Args.notNull(paramClientConnectionManager, "Client connection manager");
    Args.notNull(paramConnectionReuseStrategy, "Connection reuse strategy");
    Args.notNull(paramConnectionKeepAliveStrategy, "Connection keep alive strategy");
    Args.notNull(paramHttpRoutePlanner, "Route planner");
    Args.notNull(paramHttpProcessor, "HTTP protocol processor");
    Args.notNull(paramHttpRequestRetryHandler, "HTTP request retry handler");
    Args.notNull(paramRedirectStrategy, "Redirect strategy");
    Args.notNull(paramAuthenticationStrategy1, "Target authentication strategy");
    Args.notNull(paramAuthenticationStrategy2, "Proxy authentication strategy");
    Args.notNull(paramUserTokenHandler, "User token handler");
    Args.notNull(paramHttpParams, "HTTP parameters");
    this.log = paramHttpClientAndroidLog;
    this.authenticator = new HttpAuthenticator(paramHttpClientAndroidLog);
    this.requestExec = paramHttpRequestExecutor;
    this.connManager = paramClientConnectionManager;
    this.reuseStrategy = paramConnectionReuseStrategy;
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
    this.routePlanner = paramHttpRoutePlanner;
    this.httpProcessor = paramHttpProcessor;
    this.retryHandler = paramHttpRequestRetryHandler;
    this.redirectStrategy = paramRedirectStrategy;
    this.targetAuthStrategy = paramAuthenticationStrategy1;
    this.proxyAuthStrategy = paramAuthenticationStrategy2;
    this.userTokenHandler = paramUserTokenHandler;
    this.params = paramHttpParams;
    if ((paramRedirectStrategy instanceof DefaultRedirectStrategyAdaptor)) {
      this.redirectHandler = ((DefaultRedirectStrategyAdaptor)paramRedirectStrategy).getHandler();
    } else {
      this.redirectHandler = null;
    }
    if ((paramAuthenticationStrategy1 instanceof AuthenticationStrategyAdaptor)) {
      this.targetAuthHandler = ((AuthenticationStrategyAdaptor)paramAuthenticationStrategy1).getHandler();
    } else {
      this.targetAuthHandler = null;
    }
    if ((paramAuthenticationStrategy2 instanceof AuthenticationStrategyAdaptor)) {
      this.proxyAuthHandler = ((AuthenticationStrategyAdaptor)paramAuthenticationStrategy2).getHandler();
    } else {
      this.proxyAuthHandler = null;
    }
    this.managedConn = null;
    this.execCount = 0;
    this.redirectCount = 0;
    this.targetAuthState = new AuthState();
    this.proxyAuthState = new AuthState();
    this.maxRedirects = this.params.getIntParameter("http.protocol.max-redirects", 100);
  }
  
  @Deprecated
  public DefaultRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectHandler paramRedirectHandler, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    this(new HttpClientAndroidLog(DefaultRequestDirector.class), paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, new DefaultRedirectStrategyAdaptor(paramRedirectHandler), new AuthenticationStrategyAdaptor(paramAuthenticationHandler1), new AuthenticationStrategyAdaptor(paramAuthenticationHandler2), paramUserTokenHandler, paramHttpParams);
  }
  
  private void abortConnection()
  {
    ManagedClientConnection localManagedClientConnection = this.managedConn;
    if (localManagedClientConnection != null)
    {
      this.managedConn = null;
      try
      {
        localManagedClientConnection.abortConnection();
      }
      catch (IOException localIOException1)
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug(localIOException1.getMessage(), localIOException1);
        }
      }
      try
      {
        localManagedClientConnection.releaseConnection();
      }
      catch (IOException localIOException2)
      {
        this.log.debug("Error releasing connection", localIOException2);
      }
    }
  }
  
  private void tryConnect(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    paramRoutedRequest = paramRoutedRequest.getRequest();
    int i = 0;
    for (;;)
    {
      paramHttpContext.setAttribute("http.request", paramRoutedRequest);
      int j = i + 1;
      try
      {
        if (!this.managedConn.isOpen()) {
          this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
        } else {
          this.managedConn.setSocketTimeout(HttpConnectionParams.getSoTimeout(this.params));
        }
        establishRoute(localHttpRoute, paramHttpContext);
        return;
      }
      catch (IOException localIOException1) {}
      try
      {
        this.managedConn.close();
        HttpClientAndroidLog localHttpClientAndroidLog;
        if (this.retryHandler.retryRequest(localIOException1, j, paramHttpContext))
        {
          i = j;
          if (!this.log.isInfoEnabled()) {
            continue;
          }
          Object localObject = this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("I/O exception (");
          localStringBuilder.append(localIOException1.getClass().getName());
          localStringBuilder.append(") caught when connecting to ");
          localStringBuilder.append(localHttpRoute);
          localStringBuilder.append(": ");
          localStringBuilder.append(localIOException1.getMessage());
          ((HttpClientAndroidLog)localObject).info(localStringBuilder.toString());
          if (this.log.isDebugEnabled()) {
            this.log.debug(localIOException1.getMessage(), localIOException1);
          }
          localHttpClientAndroidLog = this.log;
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("Retrying connect to ");
          ((StringBuilder)localObject).append(localHttpRoute);
          localHttpClientAndroidLog.info(((StringBuilder)localObject).toString());
          i = j;
          continue;
        }
        throw localHttpClientAndroidLog;
      }
      catch (IOException localIOException2)
      {
        for (;;) {}
      }
    }
  }
  
  private HttpResponse tryExecute(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    RequestWrapper localRequestWrapper = paramRoutedRequest.getRequest();
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    Object localObject2 = null;
    Object localObject1 = null;
    for (;;)
    {
      this.execCount += 1;
      localRequestWrapper.incrementExecCount();
      if (!localRequestWrapper.isRepeatable())
      {
        this.log.debug("Cannot retry non-repeatable request");
        if (localObject1 != null) {
          throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", (Throwable)localObject1);
        }
        throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.");
      }
      try
      {
        if (!this.managedConn.isOpen()) {
          if (!localHttpRoute.isTunnelled())
          {
            this.log.debug("Reopening the direct connection.");
            this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
          }
          else
          {
            this.log.debug("Proxied connection. Need to start over.");
            paramRoutedRequest = (RoutedRequest)localObject2;
            break label213;
          }
        }
        if (this.log.isDebugEnabled())
        {
          paramRoutedRequest = this.log;
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("Attempt ");
          ((StringBuilder)localObject1).append(this.execCount);
          ((StringBuilder)localObject1).append(" to execute request");
          paramRoutedRequest.debug(((StringBuilder)localObject1).toString());
        }
        paramRoutedRequest = this.requestExec.execute(localRequestWrapper, this.managedConn, paramHttpContext);
        label213:
        return paramRoutedRequest;
      }
      catch (IOException paramRoutedRequest)
      {
        this.log.debug("Closing the connection.");
      }
      try
      {
        this.managedConn.close();
        if (this.retryHandler.retryRequest(paramRoutedRequest, localRequestWrapper.getExecCount(), paramHttpContext))
        {
          if (this.log.isInfoEnabled())
          {
            localObject1 = this.log;
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append("I/O exception (");
            ((StringBuilder)localObject3).append(paramRoutedRequest.getClass().getName());
            ((StringBuilder)localObject3).append(") caught when processing request to ");
            ((StringBuilder)localObject3).append(localHttpRoute);
            ((StringBuilder)localObject3).append(": ");
            ((StringBuilder)localObject3).append(paramRoutedRequest.getMessage());
            ((HttpClientAndroidLog)localObject1).info(((StringBuilder)localObject3).toString());
          }
          if (this.log.isDebugEnabled()) {
            this.log.debug(paramRoutedRequest.getMessage(), paramRoutedRequest);
          }
          localObject1 = paramRoutedRequest;
          if (!this.log.isInfoEnabled()) {
            continue;
          }
          Object localObject3 = this.log;
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("Retrying request to ");
          ((StringBuilder)localObject1).append(localHttpRoute);
          ((HttpClientAndroidLog)localObject3).info(((StringBuilder)localObject1).toString());
          localObject1 = paramRoutedRequest;
          continue;
        }
        if ((paramRoutedRequest instanceof NoHttpResponseException))
        {
          paramHttpContext = new StringBuilder();
          paramHttpContext.append(localHttpRoute.getTargetHost().toHostString());
          paramHttpContext.append(" failed to respond");
          paramHttpContext = new NoHttpResponseException(paramHttpContext.toString());
          paramHttpContext.setStackTrace(paramRoutedRequest.getStackTrace());
          throw paramHttpContext;
        }
        throw paramRoutedRequest;
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  private RequestWrapper wrapRequest(HttpRequest paramHttpRequest)
    throws ProtocolException
  {
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      return new EntityEnclosingRequestWrapper((HttpEntityEnclosingRequest)paramHttpRequest);
    }
    return new RequestWrapper(paramHttpRequest);
  }
  
  protected HttpRequest createConnectRequest(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
  {
    paramHttpContext = paramHttpRoute.getTargetHost();
    paramHttpRoute = paramHttpContext.getHostName();
    int j = paramHttpContext.getPort();
    int i = j;
    if (j < 0) {
      i = this.connManager.getSchemeRegistry().getScheme(paramHttpContext.getSchemeName()).getDefaultPort();
    }
    paramHttpContext = new StringBuilder(paramHttpRoute.length() + 6);
    paramHttpContext.append(paramHttpRoute);
    paramHttpContext.append(':');
    paramHttpContext.append(Integer.toString(i));
    return new BasicHttpRequest("CONNECT", paramHttpContext.toString(), HttpProtocolParams.getVersion(this.params));
  }
  
  protected boolean createTunnelToProxy(HttpRoute paramHttpRoute, int paramInt, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    throw new HttpException("Proxy chains are not supported.");
  }
  
  protected boolean createTunnelToTarget(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpHost localHttpHost2 = paramHttpRoute.getProxyHost();
    HttpHost localHttpHost1 = paramHttpRoute.getTargetHost();
    Object localObject;
    for (;;)
    {
      if (!this.managedConn.isOpen()) {
        this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
      }
      localObject = createConnectRequest(paramHttpRoute, paramHttpContext);
      ((HttpRequest)localObject).setParams(this.params);
      paramHttpContext.setAttribute("http.target_host", localHttpHost1);
      paramHttpContext.setAttribute("http.route", paramHttpRoute);
      paramHttpContext.setAttribute("http.proxy_host", localHttpHost2);
      paramHttpContext.setAttribute("http.connection", this.managedConn);
      paramHttpContext.setAttribute("http.request", localObject);
      this.requestExec.preProcess((HttpRequest)localObject, this.httpProcessor, paramHttpContext);
      localObject = this.requestExec.execute((HttpRequest)localObject, this.managedConn, paramHttpContext);
      ((HttpResponse)localObject).setParams(this.params);
      this.requestExec.postProcess((HttpResponse)localObject, this.httpProcessor, paramHttpContext);
      if (((HttpResponse)localObject).getStatusLine().getStatusCode() < 200) {
        break label381;
      }
      if (HttpClientParams.isAuthenticating(this.params))
      {
        if ((!this.authenticator.isAuthenticationRequested(localHttpHost2, (HttpResponse)localObject, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext)) || (!this.authenticator.authenticate(localHttpHost2, (HttpResponse)localObject, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext))) {
          break;
        }
        if (this.reuseStrategy.keepAlive((HttpResponse)localObject, paramHttpContext))
        {
          this.log.debug("Connection kept alive");
          EntityUtils.consume(((HttpResponse)localObject).getEntity());
        }
        else
        {
          this.managedConn.close();
        }
      }
    }
    if (((HttpResponse)localObject).getStatusLine().getStatusCode() > 299)
    {
      paramHttpRoute = ((HttpResponse)localObject).getEntity();
      if (paramHttpRoute != null) {
        ((HttpResponse)localObject).setEntity(new BufferedHttpEntity(paramHttpRoute));
      }
      this.managedConn.close();
      paramHttpRoute = new StringBuilder();
      paramHttpRoute.append("CONNECT refused by proxy: ");
      paramHttpRoute.append(((HttpResponse)localObject).getStatusLine());
      throw new TunnelRefusedException(paramHttpRoute.toString(), (HttpResponse)localObject);
    }
    this.managedConn.markReusable();
    return false;
    label381:
    paramHttpRoute = new StringBuilder();
    paramHttpRoute.append("Unexpected response to CONNECT request: ");
    paramHttpRoute.append(((HttpResponse)localObject).getStatusLine());
    throw new HttpException(paramHttpRoute.toString());
  }
  
  protected HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    HttpRoutePlanner localHttpRoutePlanner = this.routePlanner;
    if (paramHttpHost == null) {
      paramHttpHost = (HttpHost)paramHttpRequest.getParams().getParameter("http.default-host");
    }
    return localHttpRoutePlanner.determineRoute(paramHttpHost, paramHttpRequest, paramHttpContext);
  }
  
  protected void establishRoute(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    BasicRouteDirector localBasicRouteDirector = new BasicRouteDirector();
    HttpRoute localHttpRoute;
    int j;
    do
    {
      localHttpRoute = this.managedConn.getRoute();
      j = localBasicRouteDirector.nextStep(paramHttpRoute, localHttpRoute);
      boolean bool;
      switch (j)
      {
      default: 
        paramHttpRoute = new StringBuilder();
        paramHttpRoute.append("Unknown step indicator ");
        paramHttpRoute.append(j);
        paramHttpRoute.append(" from RouteDirector.");
        throw new IllegalStateException(paramHttpRoute.toString());
      case 5: 
        this.managedConn.layerProtocol(paramHttpContext, this.params);
        break;
      case 4: 
        int i = localHttpRoute.getHopCount() - 1;
        bool = createTunnelToProxy(paramHttpRoute, i, paramHttpContext);
        this.log.debug("Tunnel to proxy created.");
        this.managedConn.tunnelProxy(paramHttpRoute.getHopTarget(i), bool, this.params);
        break;
      case 3: 
        bool = createTunnelToTarget(paramHttpRoute, paramHttpContext);
        this.log.debug("Tunnel to target created.");
        this.managedConn.tunnelTarget(bool, this.params);
        break;
      case 1: 
      case 2: 
        this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
      }
    } while (j > 0);
    return;
    paramHttpContext = new StringBuilder();
    paramHttpContext.append("Unable to establish route: planned = ");
    paramHttpContext.append(paramHttpRoute);
    paramHttpContext.append("; current = ");
    paramHttpContext.append(localHttpRoute);
    throw new HttpException(paramHttpContext.toString());
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    paramHttpContext.setAttribute("http.auth.target-scope", this.targetAuthState);
    paramHttpContext.setAttribute("http.auth.proxy-scope", this.proxyAuthState);
    Object localObject2 = wrapRequest(paramHttpRequest);
    ((RequestWrapper)localObject2).setParams(this.params);
    Object localObject3 = determineRoute(paramHttpHost, (HttpRequest)localObject2, paramHttpContext);
    this.virtualHost = ((HttpHost)((RequestWrapper)localObject2).getParams().getParameter("http.virtual-host"));
    Object localObject1 = this.virtualHost;
    if ((localObject1 != null) && (((HttpHost)localObject1).getPort() == -1))
    {
      if (paramHttpHost != null) {
        localObject1 = paramHttpHost;
      } else {
        localObject1 = ((HttpRoute)localObject3).getTargetHost();
      }
      i = ((HttpHost)localObject1).getPort();
      if (i != -1) {
        this.virtualHost = new HttpHost(this.virtualHost.getHostName(), i, this.virtualHost.getSchemeName());
      }
    }
    localObject3 = new RoutedRequest((RequestWrapper)localObject2, (HttpRoute)localObject3);
    localObject2 = null;
    int i = 0;
    boolean bool1 = false;
    for (;;)
    {
      if (i == 0) {
        try
        {
          localObject2 = ((RoutedRequest)localObject3).getRequest();
          Object localObject4 = ((RoutedRequest)localObject3).getRoute();
          Object localObject6 = paramHttpContext.getAttribute("http.user-token");
          long l;
          if (this.managedConn == null)
          {
            localObject1 = this.connManager.requestConnection((HttpRoute)localObject4, localObject6);
            if ((paramHttpRequest instanceof AbortableHttpRequest)) {
              ((AbortableHttpRequest)paramHttpRequest).setConnectionRequest((ClientConnectionRequest)localObject1);
            }
            l = HttpClientParams.getConnectionManagerTimeout(this.params);
            try
            {
              this.managedConn = ((ClientConnectionRequest)localObject1).getConnection(l, TimeUnit.MILLISECONDS);
              if ((HttpConnectionParams.isStaleCheckingEnabled(this.params)) && (this.managedConn.isOpen()))
              {
                this.log.debug("Stale connection check");
                if (this.managedConn.isStale())
                {
                  this.log.debug("Stale connection detected");
                  this.managedConn.close();
                }
              }
            }
            catch (InterruptedException paramHttpHost)
            {
              Thread.currentThread().interrupt();
              paramHttpHost = new java/io/InterruptedIOException;
              paramHttpHost.<init>();
              throw paramHttpHost;
            }
          }
          if ((paramHttpRequest instanceof AbortableHttpRequest)) {
            ((AbortableHttpRequest)paramHttpRequest).setReleaseTrigger(this.managedConn);
          }
          try
          {
            tryConnect((RoutedRequest)localObject3, paramHttpContext);
            localObject1 = ((RequestWrapper)localObject2).getURI().getUserInfo();
            if (localObject1 != null)
            {
              localObject7 = this.targetAuthState;
              localObject5 = new cz/msebera/android/httpclient/impl/auth/BasicScheme;
              ((BasicScheme)localObject5).<init>();
              UsernamePasswordCredentials localUsernamePasswordCredentials = new cz/msebera/android/httpclient/auth/UsernamePasswordCredentials;
              localUsernamePasswordCredentials.<init>((String)localObject1);
              ((AuthState)localObject7).update((AuthScheme)localObject5, localUsernamePasswordCredentials);
            }
            if (this.virtualHost != null)
            {
              paramHttpHost = this.virtualHost;
            }
            else
            {
              localObject1 = ((RequestWrapper)localObject2).getURI();
              if (((URI)localObject1).isAbsolute()) {
                paramHttpHost = URIUtils.extractHost((URI)localObject1);
              }
            }
            localObject1 = paramHttpHost;
            if (paramHttpHost == null) {
              localObject1 = ((HttpRoute)localObject4).getTargetHost();
            }
            ((RequestWrapper)localObject2).resetHeaders();
            rewriteRequestURI((RequestWrapper)localObject2, (HttpRoute)localObject4);
            paramHttpContext.setAttribute("http.target_host", localObject1);
            paramHttpContext.setAttribute("http.route", localObject4);
            paramHttpContext.setAttribute("http.connection", this.managedConn);
            this.requestExec.preProcess((HttpRequest)localObject2, this.httpProcessor, paramHttpContext);
            Object localObject7 = tryExecute((RoutedRequest)localObject3, paramHttpContext);
            if (localObject7 == null)
            {
              localObject2 = localObject7;
              paramHttpHost = (HttpHost)localObject1;
              continue;
            }
            ((HttpResponse)localObject7).setParams(this.params);
            this.requestExec.postProcess((HttpResponse)localObject7, this.httpProcessor, paramHttpContext);
            boolean bool2 = this.reuseStrategy.keepAlive((HttpResponse)localObject7, paramHttpContext);
            if (bool2)
            {
              l = this.keepAliveStrategy.getKeepAliveDuration((HttpResponse)localObject7, paramHttpContext);
              if (this.log.isDebugEnabled())
              {
                if (l > 0L)
                {
                  paramHttpHost = new java/lang/StringBuilder;
                  paramHttpHost.<init>();
                  paramHttpHost.append("for ");
                  paramHttpHost.append(l);
                  paramHttpHost.append(" ");
                  paramHttpHost.append(TimeUnit.MILLISECONDS);
                  paramHttpHost = paramHttpHost.toString();
                }
                else
                {
                  paramHttpHost = "indefinitely";
                }
                localObject2 = this.log;
                localObject4 = new java/lang/StringBuilder;
                ((StringBuilder)localObject4).<init>();
                ((StringBuilder)localObject4).append("Connection can be kept alive ");
                ((StringBuilder)localObject4).append(paramHttpHost);
                ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject4).toString());
              }
              this.managedConn.setIdleDuration(l, TimeUnit.MILLISECONDS);
            }
            localObject4 = handleResponse((RoutedRequest)localObject3, (HttpResponse)localObject7, paramHttpContext);
            int j;
            if (localObject4 == null)
            {
              j = 1;
              localObject4 = localObject3;
            }
            else
            {
              if (bool2)
              {
                EntityUtils.consume(((HttpResponse)localObject7).getEntity());
                this.managedConn.markReusable();
              }
              else
              {
                this.managedConn.close();
                if ((this.proxyAuthState.getState().compareTo(AuthProtocolState.CHALLENGED) > 0) && (this.proxyAuthState.getAuthScheme() != null) && (this.proxyAuthState.getAuthScheme().isConnectionBased()))
                {
                  this.log.debug("Resetting proxy auth state");
                  this.proxyAuthState.reset();
                }
                if ((this.targetAuthState.getState().compareTo(AuthProtocolState.CHALLENGED) > 0) && (this.targetAuthState.getAuthScheme() != null) && (this.targetAuthState.getAuthScheme().isConnectionBased()))
                {
                  this.log.debug("Resetting target auth state");
                  this.targetAuthState.reset();
                }
              }
              if (!((RoutedRequest)localObject4).getRoute().equals(((RoutedRequest)localObject3).getRoute())) {
                releaseConnection();
              }
              j = i;
            }
            localObject2 = localObject7;
            i = j;
            localObject3 = localObject4;
            paramHttpHost = (HttpHost)localObject1;
            bool1 = bool2;
            if (this.managedConn == null) {
              continue;
            }
            Object localObject5 = localObject6;
            if (localObject6 == null)
            {
              localObject5 = this.userTokenHandler.getUserToken(paramHttpContext);
              paramHttpContext.setAttribute("http.user-token", localObject5);
            }
            localObject2 = localObject7;
            i = j;
            localObject3 = localObject4;
            paramHttpHost = (HttpHost)localObject1;
            bool1 = bool2;
            if (localObject5 == null) {
              continue;
            }
            this.managedConn.setState(localObject5);
            localObject2 = localObject7;
            i = j;
            localObject3 = localObject4;
            paramHttpHost = (HttpHost)localObject1;
            bool1 = bool2;
          }
          catch (TunnelRefusedException paramHttpHost)
          {
            if (this.log.isDebugEnabled()) {
              this.log.debug(paramHttpHost.getMessage());
            }
            localObject2 = paramHttpHost.getResponse();
          }
          if (localObject2 == null) {
            break label1177;
          }
        }
        catch (RuntimeException paramHttpHost)
        {
          break label1198;
        }
        catch (IOException paramHttpHost)
        {
          break label1204;
        }
        catch (HttpException paramHttpHost)
        {
          break label1210;
        }
        catch (ConnectionShutdownException paramHttpRequest) {}
      }
    }
    if ((((HttpResponse)localObject2).getEntity() != null) && (((HttpResponse)localObject2).getEntity().isStreaming()))
    {
      paramHttpRequest = ((HttpResponse)localObject2).getEntity();
      paramHttpHost = new cz/msebera/android/httpclient/conn/BasicManagedEntity;
      paramHttpHost.<init>(paramHttpRequest, this.managedConn, bool1);
      ((HttpResponse)localObject2).setEntity(paramHttpHost);
    }
    else
    {
      label1177:
      if (bool1) {
        this.managedConn.markReusable();
      }
      releaseConnection();
    }
    return (HttpResponse)localObject2;
    label1198:
    abortConnection();
    throw paramHttpHost;
    label1204:
    abortConnection();
    throw paramHttpHost;
    label1210:
    abortConnection();
    throw paramHttpHost;
    paramHttpHost = new InterruptedIOException("Connection has been shut down");
    paramHttpHost.initCause(paramHttpRequest);
    throw paramHttpHost;
  }
  
  protected RoutedRequest handleResponse(RoutedRequest paramRoutedRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    RequestWrapper localRequestWrapper = paramRoutedRequest.getRequest();
    HttpParams localHttpParams = localRequestWrapper.getParams();
    Object localObject2;
    Object localObject1;
    if (HttpClientParams.isAuthenticating(localHttpParams))
    {
      localObject2 = (HttpHost)paramHttpContext.getAttribute("http.target_host");
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = localHttpRoute.getTargetHost();
      }
      if (((HttpHost)localObject1).getPort() < 0)
      {
        localObject2 = this.connManager.getSchemeRegistry().getScheme((HttpHost)localObject1);
        localObject1 = new HttpHost(((HttpHost)localObject1).getHostName(), ((Scheme)localObject2).getDefaultPort(), ((HttpHost)localObject1).getSchemeName());
      }
      boolean bool2 = this.authenticator.isAuthenticationRequested((HttpHost)localObject1, paramHttpResponse, this.targetAuthStrategy, this.targetAuthState, paramHttpContext);
      localObject2 = localHttpRoute.getProxyHost();
      if (localObject2 == null) {
        localObject2 = localHttpRoute.getTargetHost();
      }
      boolean bool1 = this.authenticator.isAuthenticationRequested((HttpHost)localObject2, paramHttpResponse, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext);
      if ((bool2) && (this.authenticator.authenticate((HttpHost)localObject1, paramHttpResponse, this.targetAuthStrategy, this.targetAuthState, paramHttpContext))) {
        return paramRoutedRequest;
      }
      if ((bool1) && (this.authenticator.authenticate((HttpHost)localObject2, paramHttpResponse, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext))) {
        return paramRoutedRequest;
      }
    }
    if ((HttpClientParams.isRedirecting(localHttpParams)) && (this.redirectStrategy.isRedirected(localRequestWrapper, paramHttpResponse, paramHttpContext)))
    {
      int i = this.redirectCount;
      if (i < this.maxRedirects)
      {
        this.redirectCount = (i + 1);
        this.virtualHost = null;
        localObject1 = this.redirectStrategy.getRedirect(localRequestWrapper, paramHttpResponse, paramHttpContext);
        ((HttpUriRequest)localObject1).setHeaders(localRequestWrapper.getOriginal().getAllHeaders());
        paramRoutedRequest = ((HttpUriRequest)localObject1).getURI();
        paramHttpResponse = URIUtils.extractHost(paramRoutedRequest);
        if (paramHttpResponse != null)
        {
          if (!localHttpRoute.getTargetHost().equals(paramHttpResponse))
          {
            this.log.debug("Resetting target auth state");
            this.targetAuthState.reset();
            localObject2 = this.proxyAuthState.getAuthScheme();
            if ((localObject2 != null) && (((AuthScheme)localObject2).isConnectionBased()))
            {
              this.log.debug("Resetting proxy auth state");
              this.proxyAuthState.reset();
            }
          }
          localObject1 = wrapRequest((HttpRequest)localObject1);
          ((RequestWrapper)localObject1).setParams(localHttpParams);
          paramHttpResponse = determineRoute(paramHttpResponse, (HttpRequest)localObject1, paramHttpContext);
          localObject2 = new RoutedRequest((RequestWrapper)localObject1, paramHttpResponse);
          if (this.log.isDebugEnabled())
          {
            localObject1 = this.log;
            paramHttpContext = new StringBuilder();
            paramHttpContext.append("Redirecting to '");
            paramHttpContext.append(paramRoutedRequest);
            paramHttpContext.append("' via ");
            paramHttpContext.append(paramHttpResponse);
            ((HttpClientAndroidLog)localObject1).debug(paramHttpContext.toString());
          }
          return (RoutedRequest)localObject2;
        }
        paramHttpResponse = new StringBuilder();
        paramHttpResponse.append("Redirect URI does not specify a valid host name: ");
        paramHttpResponse.append(paramRoutedRequest);
        throw new ProtocolException(paramHttpResponse.toString());
      }
      paramRoutedRequest = new StringBuilder();
      paramRoutedRequest.append("Maximum redirects (");
      paramRoutedRequest.append(this.maxRedirects);
      paramRoutedRequest.append(") exceeded");
      throw new RedirectException(paramRoutedRequest.toString());
    }
    return null;
  }
  
  protected void releaseConnection()
  {
    try
    {
      this.managedConn.releaseConnection();
    }
    catch (IOException localIOException)
    {
      this.log.debug("IOException releasing connection", localIOException);
    }
    this.managedConn = null;
  }
  
  protected void rewriteRequestURI(RequestWrapper paramRequestWrapper, HttpRoute paramHttpRoute)
    throws ProtocolException
  {
    try
    {
      localObject = paramRequestWrapper.getURI();
      if ((paramHttpRoute.getProxyHost() != null) && (!paramHttpRoute.isTunnelled()))
      {
        if (!((URI)localObject).isAbsolute()) {
          paramHttpRoute = URIUtils.rewriteURI((URI)localObject, paramHttpRoute.getTargetHost(), true);
        } else {
          paramHttpRoute = URIUtils.rewriteURI((URI)localObject);
        }
      }
      else if (((URI)localObject).isAbsolute()) {
        paramHttpRoute = URIUtils.rewriteURI((URI)localObject, null, true);
      } else {
        paramHttpRoute = URIUtils.rewriteURI((URI)localObject);
      }
      paramRequestWrapper.setURI(paramHttpRoute);
      return;
    }
    catch (URISyntaxException paramHttpRoute)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Invalid URI: ");
      ((StringBuilder)localObject).append(paramRequestWrapper.getRequestLine().getUri());
      throw new ProtocolException(((StringBuilder)localObject).toString(), paramHttpRoute);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultRequestDirector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */