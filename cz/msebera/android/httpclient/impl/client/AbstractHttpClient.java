package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.BackoffManager;
import cz.msebera.android.httpclient.client.ConnectionBackoffStrategy;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.RequestDirector;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionManagerFactory;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.cookie.CookieSpecRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.auth.BasicSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.DigestSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.NTLMSchemeFactory;
import cz.msebera.android.httpclient.impl.conn.BasicClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.DefaultHttpRoutePlanner;
import cz.msebera.android.httpclient.impl.conn.SchemeRegistryFactory;
import cz.msebera.android.httpclient.impl.cookie.BestMatchSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.BrowserCompatSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.IgnoreSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.NetscapeDraftSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2109SpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2965SpecFactory;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.BasicHttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;

@Deprecated
@ThreadSafe
public abstract class AbstractHttpClient
  extends CloseableHttpClient
{
  @GuardedBy("this")
  private BackoffManager backoffManager;
  @GuardedBy("this")
  private ClientConnectionManager connManager;
  @GuardedBy("this")
  private ConnectionBackoffStrategy connectionBackoffStrategy;
  @GuardedBy("this")
  private CookieStore cookieStore;
  @GuardedBy("this")
  private CredentialsProvider credsProvider;
  @GuardedBy("this")
  private HttpParams defaultParams;
  @GuardedBy("this")
  private ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  private BasicHttpProcessor mutableProcessor;
  @GuardedBy("this")
  private ImmutableHttpProcessor protocolProcessor;
  @GuardedBy("this")
  private AuthenticationStrategy proxyAuthStrategy;
  @GuardedBy("this")
  private RedirectStrategy redirectStrategy;
  @GuardedBy("this")
  private HttpRequestExecutor requestExec;
  @GuardedBy("this")
  private HttpRequestRetryHandler retryHandler;
  @GuardedBy("this")
  private ConnectionReuseStrategy reuseStrategy;
  @GuardedBy("this")
  private HttpRoutePlanner routePlanner;
  @GuardedBy("this")
  private AuthSchemeRegistry supportedAuthSchemes;
  @GuardedBy("this")
  private CookieSpecRegistry supportedCookieSpecs;
  @GuardedBy("this")
  private AuthenticationStrategy targetAuthStrategy;
  @GuardedBy("this")
  private UserTokenHandler userTokenHandler;
  
  protected AbstractHttpClient(ClientConnectionManager paramClientConnectionManager, HttpParams paramHttpParams)
  {
    this.defaultParams = paramHttpParams;
    this.connManager = paramClientConnectionManager;
  }
  
  private HttpProcessor getProtocolProcessor()
  {
    try
    {
      if (this.protocolProcessor == null)
      {
        Object localObject3 = getHttpProcessor();
        int k = ((BasicHttpProcessor)localObject3).getRequestInterceptorCount();
        HttpRequestInterceptor[] arrayOfHttpRequestInterceptor = new HttpRequestInterceptor[k];
        int j = 0;
        for (int i = 0; i < k; i++) {
          arrayOfHttpRequestInterceptor[i] = ((BasicHttpProcessor)localObject3).getRequestInterceptor(i);
        }
        k = ((BasicHttpProcessor)localObject3).getResponseInterceptorCount();
        localObject1 = new HttpResponseInterceptor[k];
        for (i = j; i < k; i++) {
          localObject1[i] = ((BasicHttpProcessor)localObject3).getResponseInterceptor(i);
        }
        localObject3 = new cz/msebera/android/httpclient/protocol/ImmutableHttpProcessor;
        ((ImmutableHttpProcessor)localObject3).<init>(arrayOfHttpRequestInterceptor, (HttpResponseInterceptor[])localObject1);
        this.protocolProcessor = ((ImmutableHttpProcessor)localObject3);
      }
      Object localObject1 = this.protocolProcessor;
      return (HttpProcessor)localObject1;
    }
    finally {}
  }
  
  public void addRequestInterceptor(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpRequestInterceptor);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpRequestInterceptor = finally;
      throw paramHttpRequestInterceptor;
    }
  }
  
  public void addRequestInterceptor(HttpRequestInterceptor paramHttpRequestInterceptor, int paramInt)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpRequestInterceptor, paramInt);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpRequestInterceptor = finally;
      throw paramHttpRequestInterceptor;
    }
  }
  
  public void addResponseInterceptor(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpResponseInterceptor);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpResponseInterceptor = finally;
      throw paramHttpResponseInterceptor;
    }
  }
  
  public void addResponseInterceptor(HttpResponseInterceptor paramHttpResponseInterceptor, int paramInt)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpResponseInterceptor, paramInt);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpResponseInterceptor = finally;
      throw paramHttpResponseInterceptor;
    }
  }
  
  public void clearRequestInterceptors()
  {
    try
    {
      getHttpProcessor().clearRequestInterceptors();
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void clearResponseInterceptors()
  {
    try
    {
      getHttpProcessor().clearResponseInterceptors();
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void close()
  {
    getConnectionManager().shutdown();
  }
  
  protected AuthSchemeRegistry createAuthSchemeRegistry()
  {
    AuthSchemeRegistry localAuthSchemeRegistry = new AuthSchemeRegistry();
    localAuthSchemeRegistry.register("Basic", new BasicSchemeFactory());
    localAuthSchemeRegistry.register("Digest", new DigestSchemeFactory());
    localAuthSchemeRegistry.register("NTLM", new NTLMSchemeFactory());
    return localAuthSchemeRegistry;
  }
  
  protected ClientConnectionManager createClientConnectionManager()
  {
    SchemeRegistry localSchemeRegistry = SchemeRegistryFactory.createDefault();
    HttpParams localHttpParams = getParams();
    String str = (String)localHttpParams.getParameter("http.connection-manager.factory-class-name");
    Object localObject;
    if (str != null) {
      try
      {
        ClientConnectionManagerFactory localClientConnectionManagerFactory = (ClientConnectionManagerFactory)Class.forName(str).newInstance();
      }
      catch (InstantiationException localInstantiationException)
      {
        throw new InstantiationError(localInstantiationException.getMessage());
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new IllegalAccessError(localIllegalAccessException.getMessage());
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("Invalid class name: ");
        ((StringBuilder)localObject).append(str);
        throw new IllegalStateException(((StringBuilder)localObject).toString());
      }
    } else {
      localObject = null;
    }
    if (localObject != null) {
      localObject = ((ClientConnectionManagerFactory)localObject).newInstance(localHttpParams, localSchemeRegistry);
    } else {
      localObject = new BasicClientConnectionManager(localSchemeRegistry);
    }
    return (ClientConnectionManager)localObject;
  }
  
  @Deprecated
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectHandler paramRedirectHandler, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectHandler, paramAuthenticationHandler1, paramAuthenticationHandler2, paramUserTokenHandler, paramHttpParams);
  }
  
  @Deprecated
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(this.log, paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, paramAuthenticationHandler1, paramAuthenticationHandler2, paramUserTokenHandler, paramHttpParams);
  }
  
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationStrategy paramAuthenticationStrategy1, AuthenticationStrategy paramAuthenticationStrategy2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(this.log, paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, paramAuthenticationStrategy1, paramAuthenticationStrategy2, paramUserTokenHandler, paramHttpParams);
  }
  
  protected ConnectionKeepAliveStrategy createConnectionKeepAliveStrategy()
  {
    return new DefaultConnectionKeepAliveStrategy();
  }
  
  protected ConnectionReuseStrategy createConnectionReuseStrategy()
  {
    return new DefaultConnectionReuseStrategy();
  }
  
  protected CookieSpecRegistry createCookieSpecRegistry()
  {
    CookieSpecRegistry localCookieSpecRegistry = new CookieSpecRegistry();
    localCookieSpecRegistry.register("best-match", new BestMatchSpecFactory());
    localCookieSpecRegistry.register("compatibility", new BrowserCompatSpecFactory());
    localCookieSpecRegistry.register("netscape", new NetscapeDraftSpecFactory());
    localCookieSpecRegistry.register("rfc2109", new RFC2109SpecFactory());
    localCookieSpecRegistry.register("rfc2965", new RFC2965SpecFactory());
    localCookieSpecRegistry.register("ignoreCookies", new IgnoreSpecFactory());
    return localCookieSpecRegistry;
  }
  
  protected CookieStore createCookieStore()
  {
    return new BasicCookieStore();
  }
  
  protected CredentialsProvider createCredentialsProvider()
  {
    return new BasicCredentialsProvider();
  }
  
  protected HttpContext createHttpContext()
  {
    BasicHttpContext localBasicHttpContext = new BasicHttpContext();
    localBasicHttpContext.setAttribute("http.scheme-registry", getConnectionManager().getSchemeRegistry());
    localBasicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
    localBasicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
    localBasicHttpContext.setAttribute("http.cookie-store", getCookieStore());
    localBasicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
    return localBasicHttpContext;
  }
  
  protected abstract HttpParams createHttpParams();
  
  protected abstract BasicHttpProcessor createHttpProcessor();
  
  protected HttpRequestRetryHandler createHttpRequestRetryHandler()
  {
    return new DefaultHttpRequestRetryHandler();
  }
  
  protected HttpRoutePlanner createHttpRoutePlanner()
  {
    return new DefaultHttpRoutePlanner(getConnectionManager().getSchemeRegistry());
  }
  
  @Deprecated
  protected AuthenticationHandler createProxyAuthenticationHandler()
  {
    return new DefaultProxyAuthenticationHandler();
  }
  
  protected AuthenticationStrategy createProxyAuthenticationStrategy()
  {
    return new ProxyAuthenticationStrategy();
  }
  
  @Deprecated
  protected RedirectHandler createRedirectHandler()
  {
    return new DefaultRedirectHandler();
  }
  
  protected HttpRequestExecutor createRequestExecutor()
  {
    return new HttpRequestExecutor();
  }
  
  @Deprecated
  protected AuthenticationHandler createTargetAuthenticationHandler()
  {
    return new DefaultTargetAuthenticationHandler();
  }
  
  protected AuthenticationStrategy createTargetAuthenticationStrategy()
  {
    return new TargetAuthenticationStrategy();
  }
  
  protected UserTokenHandler createUserTokenHandler()
  {
    return new DefaultUserTokenHandler();
  }
  
  protected HttpParams determineParams(HttpRequest paramHttpRequest)
  {
    return new ClientParamsStack(null, getParams(), paramHttpRequest.getParams(), null);
  }
  
  /* Error */
  protected final cz.msebera.android.httpclient.client.methods.CloseableHttpResponse doExecute(cz.msebera.android.httpclient.HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws java.io.IOException, cz.msebera.android.httpclient.client.ClientProtocolException
  {
    // Byte code:
    //   0: aload_2
    //   1: ldc_w 414
    //   4: invokestatic 420	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   7: pop
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: invokevirtual 422	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:createHttpContext	()Lcz/msebera/android/httpclient/protocol/HttpContext;
    //   14: astore 4
    //   16: aload_3
    //   17: ifnonnull +9 -> 26
    //   20: aload 4
    //   22: astore_3
    //   23: goto +14 -> 37
    //   26: new 424	cz/msebera/android/httpclient/protocol/DefaultedHttpContext
    //   29: dup
    //   30: aload_3
    //   31: aload 4
    //   33: invokespecial 427	cz/msebera/android/httpclient/protocol/DefaultedHttpContext:<init>	(Lcz/msebera/android/httpclient/protocol/HttpContext;Lcz/msebera/android/httpclient/protocol/HttpContext;)V
    //   36: astore_3
    //   37: aload_0
    //   38: aload_2
    //   39: invokevirtual 429	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:determineParams	(Lcz/msebera/android/httpclient/HttpRequest;)Lcz/msebera/android/httpclient/params/HttpParams;
    //   42: astore 4
    //   44: aload_3
    //   45: ldc_w 431
    //   48: aload 4
    //   50: invokestatic 437	cz/msebera/android/httpclient/client/params/HttpClientParamConfig:getRequestConfig	(Lcz/msebera/android/httpclient/params/HttpParams;)Lcz/msebera/android/httpclient/client/config/RequestConfig;
    //   53: invokeinterface 325 3 0
    //   58: aload_0
    //   59: aload_0
    //   60: invokevirtual 440	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRequestExecutor	()Lcz/msebera/android/httpclient/protocol/HttpRequestExecutor;
    //   63: aload_0
    //   64: invokevirtual 131	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   67: aload_0
    //   68: invokevirtual 443	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionReuseStrategy	()Lcz/msebera/android/httpclient/ConnectionReuseStrategy;
    //   71: aload_0
    //   72: invokevirtual 446	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionKeepAliveStrategy	()Lcz/msebera/android/httpclient/conn/ConnectionKeepAliveStrategy;
    //   75: aload_0
    //   76: invokevirtual 449	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRoutePlanner	()Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;
    //   79: aload_0
    //   80: invokespecial 451	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getProtocolProcessor	()Lcz/msebera/android/httpclient/protocol/HttpProcessor;
    //   83: aload_0
    //   84: invokevirtual 454	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getHttpRequestRetryHandler	()Lcz/msebera/android/httpclient/client/HttpRequestRetryHandler;
    //   87: aload_0
    //   88: invokevirtual 458	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRedirectStrategy	()Lcz/msebera/android/httpclient/client/RedirectStrategy;
    //   91: aload_0
    //   92: invokevirtual 461	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getTargetAuthenticationStrategy	()Lcz/msebera/android/httpclient/client/AuthenticationStrategy;
    //   95: aload_0
    //   96: invokevirtual 464	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getProxyAuthenticationStrategy	()Lcz/msebera/android/httpclient/client/AuthenticationStrategy;
    //   99: aload_0
    //   100: invokevirtual 467	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getUserTokenHandler	()Lcz/msebera/android/httpclient/client/UserTokenHandler;
    //   103: aload 4
    //   105: invokevirtual 469	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:createClientRequestDirector	(Lcz/msebera/android/httpclient/protocol/HttpRequestExecutor;Lcz/msebera/android/httpclient/conn/ClientConnectionManager;Lcz/msebera/android/httpclient/ConnectionReuseStrategy;Lcz/msebera/android/httpclient/conn/ConnectionKeepAliveStrategy;Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;Lcz/msebera/android/httpclient/protocol/HttpProcessor;Lcz/msebera/android/httpclient/client/HttpRequestRetryHandler;Lcz/msebera/android/httpclient/client/RedirectStrategy;Lcz/msebera/android/httpclient/client/AuthenticationStrategy;Lcz/msebera/android/httpclient/client/AuthenticationStrategy;Lcz/msebera/android/httpclient/client/UserTokenHandler;Lcz/msebera/android/httpclient/params/HttpParams;)Lcz/msebera/android/httpclient/client/RequestDirector;
    //   108: astore 7
    //   110: aload_0
    //   111: invokevirtual 449	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRoutePlanner	()Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;
    //   114: astore 8
    //   116: aload_0
    //   117: invokevirtual 473	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionBackoffStrategy	()Lcz/msebera/android/httpclient/client/ConnectionBackoffStrategy;
    //   120: astore 6
    //   122: aload_0
    //   123: invokevirtual 477	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getBackoffManager	()Lcz/msebera/android/httpclient/client/BackoffManager;
    //   126: astore 5
    //   128: aload_0
    //   129: monitorexit
    //   130: aload 6
    //   132: ifnull +176 -> 308
    //   135: aload 5
    //   137: ifnull +171 -> 308
    //   140: aload_1
    //   141: ifnull +9 -> 150
    //   144: aload_1
    //   145: astore 4
    //   147: goto +21 -> 168
    //   150: aload_0
    //   151: aload_2
    //   152: invokevirtual 429	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:determineParams	(Lcz/msebera/android/httpclient/HttpRequest;)Lcz/msebera/android/httpclient/params/HttpParams;
    //   155: ldc_w 479
    //   158: invokeinterface 185 2 0
    //   163: checkcast 481	cz/msebera/android/httpclient/HttpHost
    //   166: astore 4
    //   168: aload 8
    //   170: aload 4
    //   172: aload_2
    //   173: aload_3
    //   174: invokeinterface 487 4 0
    //   179: astore 4
    //   181: aload 7
    //   183: aload_1
    //   184: aload_2
    //   185: aload_3
    //   186: invokeinterface 493 4 0
    //   191: invokestatic 499	cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy:newProxy	(Lcz/msebera/android/httpclient/HttpResponse;)Lcz/msebera/android/httpclient/client/methods/CloseableHttpResponse;
    //   194: astore_1
    //   195: aload 6
    //   197: aload_1
    //   198: invokeinterface 505 2 0
    //   203: ifeq +15 -> 218
    //   206: aload 5
    //   208: aload 4
    //   210: invokeinterface 511 2 0
    //   215: goto +12 -> 227
    //   218: aload 5
    //   220: aload 4
    //   222: invokeinterface 514 2 0
    //   227: aload_1
    //   228: areturn
    //   229: astore_1
    //   230: aload 6
    //   232: aload_1
    //   233: invokeinterface 517 2 0
    //   238: ifeq +12 -> 250
    //   241: aload 5
    //   243: aload 4
    //   245: invokeinterface 511 2 0
    //   250: aload_1
    //   251: instanceof 408
    //   254: ifne +26 -> 280
    //   257: aload_1
    //   258: instanceof 404
    //   261: ifeq +8 -> 269
    //   264: aload_1
    //   265: checkcast 404	java/io/IOException
    //   268: athrow
    //   269: new 519	java/lang/reflect/UndeclaredThrowableException
    //   272: astore_2
    //   273: aload_2
    //   274: aload_1
    //   275: invokespecial 522	java/lang/reflect/UndeclaredThrowableException:<init>	(Ljava/lang/Throwable;)V
    //   278: aload_2
    //   279: athrow
    //   280: aload_1
    //   281: checkcast 408	cz/msebera/android/httpclient/HttpException
    //   284: athrow
    //   285: astore_1
    //   286: aload 6
    //   288: aload_1
    //   289: invokeinterface 517 2 0
    //   294: ifeq +12 -> 306
    //   297: aload 5
    //   299: aload 4
    //   301: invokeinterface 511 2 0
    //   306: aload_1
    //   307: athrow
    //   308: aload 7
    //   310: aload_1
    //   311: aload_2
    //   312: aload_3
    //   313: invokeinterface 493 4 0
    //   318: invokestatic 499	cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy:newProxy	(Lcz/msebera/android/httpclient/HttpResponse;)Lcz/msebera/android/httpclient/client/methods/CloseableHttpResponse;
    //   321: astore_1
    //   322: aload_1
    //   323: areturn
    //   324: astore_1
    //   325: new 406	cz/msebera/android/httpclient/client/ClientProtocolException
    //   328: dup
    //   329: aload_1
    //   330: invokespecial 523	cz/msebera/android/httpclient/client/ClientProtocolException:<init>	(Ljava/lang/Throwable;)V
    //   333: athrow
    //   334: astore_1
    //   335: aload_0
    //   336: monitorexit
    //   337: aload_1
    //   338: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	339	0	this	AbstractHttpClient
    //   0	339	1	paramHttpHost	cz.msebera.android.httpclient.HttpHost
    //   0	339	2	paramHttpRequest	HttpRequest
    //   0	339	3	paramHttpContext	HttpContext
    //   14	286	4	localObject	Object
    //   126	172	5	localBackoffManager	BackoffManager
    //   120	167	6	localConnectionBackoffStrategy	ConnectionBackoffStrategy
    //   108	201	7	localRequestDirector	RequestDirector
    //   114	55	8	localHttpRoutePlanner	HttpRoutePlanner
    // Exception table:
    //   from	to	target	type
    //   181	195	229	java/lang/Exception
    //   181	195	285	java/lang/RuntimeException
    //   150	168	324	cz/msebera/android/httpclient/HttpException
    //   168	181	324	cz/msebera/android/httpclient/HttpException
    //   181	195	324	cz/msebera/android/httpclient/HttpException
    //   195	215	324	cz/msebera/android/httpclient/HttpException
    //   218	227	324	cz/msebera/android/httpclient/HttpException
    //   230	250	324	cz/msebera/android/httpclient/HttpException
    //   250	269	324	cz/msebera/android/httpclient/HttpException
    //   269	280	324	cz/msebera/android/httpclient/HttpException
    //   280	285	324	cz/msebera/android/httpclient/HttpException
    //   286	306	324	cz/msebera/android/httpclient/HttpException
    //   306	308	324	cz/msebera/android/httpclient/HttpException
    //   308	322	324	cz/msebera/android/httpclient/HttpException
    //   10	16	334	finally
    //   26	37	334	finally
    //   37	130	334	finally
    //   335	337	334	finally
  }
  
  public final AuthSchemeRegistry getAuthSchemes()
  {
    try
    {
      if (this.supportedAuthSchemes == null) {
        this.supportedAuthSchemes = createAuthSchemeRegistry();
      }
      AuthSchemeRegistry localAuthSchemeRegistry = this.supportedAuthSchemes;
      return localAuthSchemeRegistry;
    }
    finally {}
  }
  
  public final BackoffManager getBackoffManager()
  {
    try
    {
      BackoffManager localBackoffManager = this.backoffManager;
      return localBackoffManager;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final ConnectionBackoffStrategy getConnectionBackoffStrategy()
  {
    try
    {
      ConnectionBackoffStrategy localConnectionBackoffStrategy = this.connectionBackoffStrategy;
      return localConnectionBackoffStrategy;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final ConnectionKeepAliveStrategy getConnectionKeepAliveStrategy()
  {
    try
    {
      if (this.keepAliveStrategy == null) {
        this.keepAliveStrategy = createConnectionKeepAliveStrategy();
      }
      ConnectionKeepAliveStrategy localConnectionKeepAliveStrategy = this.keepAliveStrategy;
      return localConnectionKeepAliveStrategy;
    }
    finally {}
  }
  
  public final ClientConnectionManager getConnectionManager()
  {
    try
    {
      if (this.connManager == null) {
        this.connManager = createClientConnectionManager();
      }
      ClientConnectionManager localClientConnectionManager = this.connManager;
      return localClientConnectionManager;
    }
    finally {}
  }
  
  public final ConnectionReuseStrategy getConnectionReuseStrategy()
  {
    try
    {
      if (this.reuseStrategy == null) {
        this.reuseStrategy = createConnectionReuseStrategy();
      }
      ConnectionReuseStrategy localConnectionReuseStrategy = this.reuseStrategy;
      return localConnectionReuseStrategy;
    }
    finally {}
  }
  
  public final CookieSpecRegistry getCookieSpecs()
  {
    try
    {
      if (this.supportedCookieSpecs == null) {
        this.supportedCookieSpecs = createCookieSpecRegistry();
      }
      CookieSpecRegistry localCookieSpecRegistry = this.supportedCookieSpecs;
      return localCookieSpecRegistry;
    }
    finally {}
  }
  
  public final CookieStore getCookieStore()
  {
    try
    {
      if (this.cookieStore == null) {
        this.cookieStore = createCookieStore();
      }
      CookieStore localCookieStore = this.cookieStore;
      return localCookieStore;
    }
    finally {}
  }
  
  public final CredentialsProvider getCredentialsProvider()
  {
    try
    {
      if (this.credsProvider == null) {
        this.credsProvider = createCredentialsProvider();
      }
      CredentialsProvider localCredentialsProvider = this.credsProvider;
      return localCredentialsProvider;
    }
    finally {}
  }
  
  protected final BasicHttpProcessor getHttpProcessor()
  {
    try
    {
      if (this.mutableProcessor == null) {
        this.mutableProcessor = createHttpProcessor();
      }
      BasicHttpProcessor localBasicHttpProcessor = this.mutableProcessor;
      return localBasicHttpProcessor;
    }
    finally {}
  }
  
  public final HttpRequestRetryHandler getHttpRequestRetryHandler()
  {
    try
    {
      if (this.retryHandler == null) {
        this.retryHandler = createHttpRequestRetryHandler();
      }
      HttpRequestRetryHandler localHttpRequestRetryHandler = this.retryHandler;
      return localHttpRequestRetryHandler;
    }
    finally {}
  }
  
  public final HttpParams getParams()
  {
    try
    {
      if (this.defaultParams == null) {
        this.defaultParams = createHttpParams();
      }
      HttpParams localHttpParams = this.defaultParams;
      return localHttpParams;
    }
    finally {}
  }
  
  @Deprecated
  public final AuthenticationHandler getProxyAuthenticationHandler()
  {
    try
    {
      AuthenticationHandler localAuthenticationHandler = createProxyAuthenticationHandler();
      return localAuthenticationHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final AuthenticationStrategy getProxyAuthenticationStrategy()
  {
    try
    {
      if (this.proxyAuthStrategy == null) {
        this.proxyAuthStrategy = createProxyAuthenticationStrategy();
      }
      AuthenticationStrategy localAuthenticationStrategy = this.proxyAuthStrategy;
      return localAuthenticationStrategy;
    }
    finally {}
  }
  
  @Deprecated
  public final RedirectHandler getRedirectHandler()
  {
    try
    {
      RedirectHandler localRedirectHandler = createRedirectHandler();
      return localRedirectHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final RedirectStrategy getRedirectStrategy()
  {
    try
    {
      if (this.redirectStrategy == null)
      {
        localObject1 = new cz/msebera/android/httpclient/impl/client/DefaultRedirectStrategy;
        ((DefaultRedirectStrategy)localObject1).<init>();
        this.redirectStrategy = ((RedirectStrategy)localObject1);
      }
      Object localObject1 = this.redirectStrategy;
      return (RedirectStrategy)localObject1;
    }
    finally {}
  }
  
  public final HttpRequestExecutor getRequestExecutor()
  {
    try
    {
      if (this.requestExec == null) {
        this.requestExec = createRequestExecutor();
      }
      HttpRequestExecutor localHttpRequestExecutor = this.requestExec;
      return localHttpRequestExecutor;
    }
    finally {}
  }
  
  public HttpRequestInterceptor getRequestInterceptor(int paramInt)
  {
    try
    {
      HttpRequestInterceptor localHttpRequestInterceptor = getHttpProcessor().getRequestInterceptor(paramInt);
      return localHttpRequestInterceptor;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public int getRequestInterceptorCount()
  {
    try
    {
      int i = getHttpProcessor().getRequestInterceptorCount();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public HttpResponseInterceptor getResponseInterceptor(int paramInt)
  {
    try
    {
      HttpResponseInterceptor localHttpResponseInterceptor = getHttpProcessor().getResponseInterceptor(paramInt);
      return localHttpResponseInterceptor;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public int getResponseInterceptorCount()
  {
    try
    {
      int i = getHttpProcessor().getResponseInterceptorCount();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final HttpRoutePlanner getRoutePlanner()
  {
    try
    {
      if (this.routePlanner == null) {
        this.routePlanner = createHttpRoutePlanner();
      }
      HttpRoutePlanner localHttpRoutePlanner = this.routePlanner;
      return localHttpRoutePlanner;
    }
    finally {}
  }
  
  @Deprecated
  public final AuthenticationHandler getTargetAuthenticationHandler()
  {
    try
    {
      AuthenticationHandler localAuthenticationHandler = createTargetAuthenticationHandler();
      return localAuthenticationHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final AuthenticationStrategy getTargetAuthenticationStrategy()
  {
    try
    {
      if (this.targetAuthStrategy == null) {
        this.targetAuthStrategy = createTargetAuthenticationStrategy();
      }
      AuthenticationStrategy localAuthenticationStrategy = this.targetAuthStrategy;
      return localAuthenticationStrategy;
    }
    finally {}
  }
  
  public final UserTokenHandler getUserTokenHandler()
  {
    try
    {
      if (this.userTokenHandler == null) {
        this.userTokenHandler = createUserTokenHandler();
      }
      UserTokenHandler localUserTokenHandler = this.userTokenHandler;
      return localUserTokenHandler;
    }
    finally {}
  }
  
  public void removeRequestInterceptorByClass(Class<? extends HttpRequestInterceptor> paramClass)
  {
    try
    {
      getHttpProcessor().removeRequestInterceptorByClass(paramClass);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  public void removeResponseInterceptorByClass(Class<? extends HttpResponseInterceptor> paramClass)
  {
    try
    {
      getHttpProcessor().removeResponseInterceptorByClass(paramClass);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  public void setAuthSchemes(AuthSchemeRegistry paramAuthSchemeRegistry)
  {
    try
    {
      this.supportedAuthSchemes = paramAuthSchemeRegistry;
      return;
    }
    finally
    {
      paramAuthSchemeRegistry = finally;
      throw paramAuthSchemeRegistry;
    }
  }
  
  public void setBackoffManager(BackoffManager paramBackoffManager)
  {
    try
    {
      this.backoffManager = paramBackoffManager;
      return;
    }
    finally
    {
      paramBackoffManager = finally;
      throw paramBackoffManager;
    }
  }
  
  public void setConnectionBackoffStrategy(ConnectionBackoffStrategy paramConnectionBackoffStrategy)
  {
    try
    {
      this.connectionBackoffStrategy = paramConnectionBackoffStrategy;
      return;
    }
    finally
    {
      paramConnectionBackoffStrategy = finally;
      throw paramConnectionBackoffStrategy;
    }
  }
  
  public void setCookieSpecs(CookieSpecRegistry paramCookieSpecRegistry)
  {
    try
    {
      this.supportedCookieSpecs = paramCookieSpecRegistry;
      return;
    }
    finally
    {
      paramCookieSpecRegistry = finally;
      throw paramCookieSpecRegistry;
    }
  }
  
  public void setCookieStore(CookieStore paramCookieStore)
  {
    try
    {
      this.cookieStore = paramCookieStore;
      return;
    }
    finally
    {
      paramCookieStore = finally;
      throw paramCookieStore;
    }
  }
  
  public void setCredentialsProvider(CredentialsProvider paramCredentialsProvider)
  {
    try
    {
      this.credsProvider = paramCredentialsProvider;
      return;
    }
    finally
    {
      paramCredentialsProvider = finally;
      throw paramCredentialsProvider;
    }
  }
  
  public void setHttpRequestRetryHandler(HttpRequestRetryHandler paramHttpRequestRetryHandler)
  {
    try
    {
      this.retryHandler = paramHttpRequestRetryHandler;
      return;
    }
    finally
    {
      paramHttpRequestRetryHandler = finally;
      throw paramHttpRequestRetryHandler;
    }
  }
  
  public void setKeepAliveStrategy(ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy)
  {
    try
    {
      this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
      return;
    }
    finally
    {
      paramConnectionKeepAliveStrategy = finally;
      throw paramConnectionKeepAliveStrategy;
    }
  }
  
  public void setParams(HttpParams paramHttpParams)
  {
    try
    {
      this.defaultParams = paramHttpParams;
      return;
    }
    finally
    {
      paramHttpParams = finally;
      throw paramHttpParams;
    }
  }
  
  @Deprecated
  public void setProxyAuthenticationHandler(AuthenticationHandler paramAuthenticationHandler)
  {
    try
    {
      AuthenticationStrategyAdaptor localAuthenticationStrategyAdaptor = new cz/msebera/android/httpclient/impl/client/AuthenticationStrategyAdaptor;
      localAuthenticationStrategyAdaptor.<init>(paramAuthenticationHandler);
      this.proxyAuthStrategy = localAuthenticationStrategyAdaptor;
      return;
    }
    finally
    {
      paramAuthenticationHandler = finally;
      throw paramAuthenticationHandler;
    }
  }
  
  public void setProxyAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    try
    {
      this.proxyAuthStrategy = paramAuthenticationStrategy;
      return;
    }
    finally
    {
      paramAuthenticationStrategy = finally;
      throw paramAuthenticationStrategy;
    }
  }
  
  @Deprecated
  public void setRedirectHandler(RedirectHandler paramRedirectHandler)
  {
    try
    {
      DefaultRedirectStrategyAdaptor localDefaultRedirectStrategyAdaptor = new cz/msebera/android/httpclient/impl/client/DefaultRedirectStrategyAdaptor;
      localDefaultRedirectStrategyAdaptor.<init>(paramRedirectHandler);
      this.redirectStrategy = localDefaultRedirectStrategyAdaptor;
      return;
    }
    finally
    {
      paramRedirectHandler = finally;
      throw paramRedirectHandler;
    }
  }
  
  public void setRedirectStrategy(RedirectStrategy paramRedirectStrategy)
  {
    try
    {
      this.redirectStrategy = paramRedirectStrategy;
      return;
    }
    finally
    {
      paramRedirectStrategy = finally;
      throw paramRedirectStrategy;
    }
  }
  
  public void setReuseStrategy(ConnectionReuseStrategy paramConnectionReuseStrategy)
  {
    try
    {
      this.reuseStrategy = paramConnectionReuseStrategy;
      return;
    }
    finally
    {
      paramConnectionReuseStrategy = finally;
      throw paramConnectionReuseStrategy;
    }
  }
  
  public void setRoutePlanner(HttpRoutePlanner paramHttpRoutePlanner)
  {
    try
    {
      this.routePlanner = paramHttpRoutePlanner;
      return;
    }
    finally
    {
      paramHttpRoutePlanner = finally;
      throw paramHttpRoutePlanner;
    }
  }
  
  @Deprecated
  public void setTargetAuthenticationHandler(AuthenticationHandler paramAuthenticationHandler)
  {
    try
    {
      AuthenticationStrategyAdaptor localAuthenticationStrategyAdaptor = new cz/msebera/android/httpclient/impl/client/AuthenticationStrategyAdaptor;
      localAuthenticationStrategyAdaptor.<init>(paramAuthenticationHandler);
      this.targetAuthStrategy = localAuthenticationStrategyAdaptor;
      return;
    }
    finally
    {
      paramAuthenticationHandler = finally;
      throw paramAuthenticationHandler;
    }
  }
  
  public void setTargetAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    try
    {
      this.targetAuthStrategy = paramAuthenticationStrategy;
      return;
    }
    finally
    {
      paramAuthenticationStrategy = finally;
      throw paramAuthenticationStrategy;
    }
  }
  
  public void setUserTokenHandler(UserTokenHandler paramUserTokenHandler)
  {
    try
    {
      this.userTokenHandler = paramUserTokenHandler;
      return;
    }
    finally
    {
      paramUserTokenHandler = finally;
      throw paramUserTokenHandler;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/AbstractHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */