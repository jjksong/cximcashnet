package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.Configurable;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.execchain.MinimalClientExec;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@ThreadSafe
class MinimalHttpClient
  extends CloseableHttpClient
{
  private final HttpClientConnectionManager connManager;
  private final HttpParams params;
  private final MinimalClientExec requestExecutor;
  
  public MinimalHttpClient(HttpClientConnectionManager paramHttpClientConnectionManager)
  {
    this.connManager = ((HttpClientConnectionManager)Args.notNull(paramHttpClientConnectionManager, "HTTP connection manager"));
    this.requestExecutor = new MinimalClientExec(new HttpRequestExecutor(), paramHttpClientConnectionManager, DefaultConnectionReuseStrategy.INSTANCE, DefaultConnectionKeepAliveStrategy.INSTANCE);
    this.params = new BasicHttpParams();
  }
  
  public void close()
  {
    this.connManager.shutdown();
  }
  
  protected CloseableHttpResponse doExecute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpRequest, "HTTP request");
    boolean bool = paramHttpRequest instanceof HttpExecutionAware;
    Object localObject = null;
    HttpExecutionAware localHttpExecutionAware;
    if (bool) {
      localHttpExecutionAware = (HttpExecutionAware)paramHttpRequest;
    } else {
      localHttpExecutionAware = null;
    }
    try
    {
      HttpRequestWrapper localHttpRequestWrapper = HttpRequestWrapper.wrap(paramHttpRequest);
      if (paramHttpContext == null) {
        paramHttpContext = new BasicHttpContext();
      }
      HttpClientContext localHttpClientContext = HttpClientContext.adapt(paramHttpContext);
      paramHttpContext = new cz/msebera/android/httpclient/conn/routing/HttpRoute;
      paramHttpContext.<init>(paramHttpHost);
      paramHttpHost = (HttpHost)localObject;
      if ((paramHttpRequest instanceof Configurable)) {
        paramHttpHost = ((Configurable)paramHttpRequest).getConfig();
      }
      if (paramHttpHost != null) {
        localHttpClientContext.setRequestConfig(paramHttpHost);
      }
      paramHttpHost = this.requestExecutor.execute(paramHttpContext, localHttpRequestWrapper, localHttpClientContext, localHttpExecutionAware);
      return paramHttpHost;
    }
    catch (HttpException paramHttpHost)
    {
      throw new ClientProtocolException(paramHttpHost);
    }
  }
  
  public ClientConnectionManager getConnectionManager()
  {
    new ClientConnectionManager()
    {
      public void closeExpiredConnections()
      {
        MinimalHttpClient.this.connManager.closeExpiredConnections();
      }
      
      public void closeIdleConnections(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        MinimalHttpClient.this.connManager.closeIdleConnections(paramAnonymousLong, paramAnonymousTimeUnit);
      }
      
      public SchemeRegistry getSchemeRegistry()
      {
        throw new UnsupportedOperationException();
      }
      
      public void releaseConnection(ManagedClientConnection paramAnonymousManagedClientConnection, long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        throw new UnsupportedOperationException();
      }
      
      public ClientConnectionRequest requestConnection(HttpRoute paramAnonymousHttpRoute, Object paramAnonymousObject)
      {
        throw new UnsupportedOperationException();
      }
      
      public void shutdown()
      {
        MinimalHttpClient.this.connManager.shutdown();
      }
    };
  }
  
  public HttpParams getParams()
  {
    return this.params;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/MinimalHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */