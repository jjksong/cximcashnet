package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.net.URI;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@NotThreadSafe
public class RedirectLocations
  extends AbstractList<Object>
{
  private final List<URI> all = new ArrayList();
  private final Set<URI> unique = new HashSet();
  
  public void add(int paramInt, Object paramObject)
  {
    List localList = this.all;
    paramObject = (URI)paramObject;
    localList.add(paramInt, paramObject);
    this.unique.add(paramObject);
  }
  
  public void add(URI paramURI)
  {
    this.unique.add(paramURI);
    this.all.add(paramURI);
  }
  
  public boolean contains(Object paramObject)
  {
    return this.unique.contains(paramObject);
  }
  
  public boolean contains(URI paramURI)
  {
    return this.unique.contains(paramURI);
  }
  
  public URI get(int paramInt)
  {
    return (URI)this.all.get(paramInt);
  }
  
  public List<URI> getAll()
  {
    return new ArrayList(this.all);
  }
  
  public URI remove(int paramInt)
  {
    URI localURI = (URI)this.all.remove(paramInt);
    this.unique.remove(localURI);
    if (this.all.size() != this.unique.size()) {
      this.unique.addAll(this.all);
    }
    return localURI;
  }
  
  public boolean remove(URI paramURI)
  {
    boolean bool = this.unique.remove(paramURI);
    if (bool)
    {
      Iterator localIterator = this.all.iterator();
      while (localIterator.hasNext()) {
        if (((URI)localIterator.next()).equals(paramURI)) {
          localIterator.remove();
        }
      }
    }
    return bool;
  }
  
  public Object set(int paramInt, Object paramObject)
  {
    Object localObject = this.all;
    paramObject = (URI)paramObject;
    localObject = (URI)((List)localObject).set(paramInt, paramObject);
    this.unique.remove(localObject);
    this.unique.add(paramObject);
    if (this.all.size() != this.unique.size()) {
      this.unique.addAll(this.all);
    }
    return localObject;
  }
  
  public int size()
  {
    return this.all.size();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/RedirectLocations.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */