package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.client.AuthCache;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.UnsupportedSchemeException;
import cz.msebera.android.httpclient.impl.conn.DefaultSchemePortResolver;
import cz.msebera.android.httpclient.util.Args;
import java.util.HashMap;

@NotThreadSafe
public class BasicAuthCache
  implements AuthCache
{
  private final HashMap<HttpHost, AuthScheme> map = new HashMap();
  private final SchemePortResolver schemePortResolver;
  
  public BasicAuthCache()
  {
    this(null);
  }
  
  public BasicAuthCache(SchemePortResolver paramSchemePortResolver)
  {
    if (paramSchemePortResolver == null) {
      paramSchemePortResolver = DefaultSchemePortResolver.INSTANCE;
    }
    this.schemePortResolver = paramSchemePortResolver;
  }
  
  public void clear()
  {
    this.map.clear();
  }
  
  public AuthScheme get(HttpHost paramHttpHost)
  {
    Args.notNull(paramHttpHost, "HTTP host");
    return (AuthScheme)this.map.get(getKey(paramHttpHost));
  }
  
  protected HttpHost getKey(HttpHost paramHttpHost)
  {
    if (paramHttpHost.getPort() <= 0) {
      try
      {
        int i = this.schemePortResolver.resolve(paramHttpHost);
        return new HttpHost(paramHttpHost.getHostName(), i, paramHttpHost.getSchemeName());
      }
      catch (UnsupportedSchemeException localUnsupportedSchemeException)
      {
        return paramHttpHost;
      }
    }
    return paramHttpHost;
  }
  
  public void put(HttpHost paramHttpHost, AuthScheme paramAuthScheme)
  {
    Args.notNull(paramHttpHost, "HTTP host");
    this.map.put(getKey(paramHttpHost), paramAuthScheme);
  }
  
  public void remove(HttpHost paramHttpHost)
  {
    Args.notNull(paramHttpHost, "HTTP host");
    this.map.remove(getKey(paramHttpHost));
  }
  
  public String toString()
  {
    return this.map.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/BasicAuthCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */