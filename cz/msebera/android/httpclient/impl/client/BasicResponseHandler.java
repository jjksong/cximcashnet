package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.HttpResponseException;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;

@Immutable
public class BasicResponseHandler
  implements ResponseHandler<String>
{
  public String handleResponse(HttpResponse paramHttpResponse)
    throws HttpResponseException, IOException
  {
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    paramHttpResponse = paramHttpResponse.getEntity();
    if (localStatusLine.getStatusCode() < 300)
    {
      if (paramHttpResponse == null) {
        paramHttpResponse = null;
      } else {
        paramHttpResponse = EntityUtils.toString(paramHttpResponse);
      }
      return paramHttpResponse;
    }
    EntityUtils.consume(paramHttpResponse);
    throw new HttpResponseException(localStatusLine.getStatusCode(), localStatusLine.getReasonPhrase());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/BasicResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */