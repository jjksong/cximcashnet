package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthSchemeProvider;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.BackoffManager;
import cz.msebera.android.httpclient.client.ConnectionBackoffStrategy;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.ServiceUnavailableRetryStrategy;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.protocol.RequestAcceptEncoding;
import cz.msebera.android.httpclient.client.protocol.RequestAddCookies;
import cz.msebera.android.httpclient.client.protocol.RequestAuthCache;
import cz.msebera.android.httpclient.client.protocol.RequestClientConnControl;
import cz.msebera.android.httpclient.client.protocol.RequestDefaultHeaders;
import cz.msebera.android.httpclient.client.protocol.RequestExpectContinue;
import cz.msebera.android.httpclient.client.protocol.ResponseContentEncoding;
import cz.msebera.android.httpclient.client.protocol.ResponseProcessCookies;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.socket.LayeredConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.PlainConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLContexts;
import cz.msebera.android.httpclient.conn.ssl.X509HostnameVerifier;
import cz.msebera.android.httpclient.cookie.CookieSpecProvider;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.NoConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.auth.BasicSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.DigestSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.NTLMSchemeFactory;
import cz.msebera.android.httpclient.impl.conn.DefaultProxyRoutePlanner;
import cz.msebera.android.httpclient.impl.conn.DefaultRoutePlanner;
import cz.msebera.android.httpclient.impl.conn.DefaultSchemePortResolver;
import cz.msebera.android.httpclient.impl.conn.PoolingHttpClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.SystemDefaultRoutePlanner;
import cz.msebera.android.httpclient.impl.cookie.BestMatchSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.BrowserCompatSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.IgnoreSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.NetscapeDraftSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2109SpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2965SpecFactory;
import cz.msebera.android.httpclient.impl.execchain.BackoffStrategyExec;
import cz.msebera.android.httpclient.impl.execchain.ClientExecChain;
import cz.msebera.android.httpclient.impl.execchain.MainClientExec;
import cz.msebera.android.httpclient.impl.execchain.ProtocolExec;
import cz.msebera.android.httpclient.impl.execchain.RedirectExec;
import cz.msebera.android.httpclient.impl.execchain.RetryExec;
import cz.msebera.android.httpclient.impl.execchain.ServiceUnavailableRetryExec;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpProcessorBuilder;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.RequestContent;
import cz.msebera.android.httpclient.protocol.RequestTargetHost;
import cz.msebera.android.httpclient.protocol.RequestUserAgent;
import cz.msebera.android.httpclient.util.TextUtils;
import cz.msebera.android.httpclient.util.VersionInfo;
import java.io.Closeable;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

@NotThreadSafe
public class HttpClientBuilder
{
  static final String DEFAULT_USER_AGENT;
  private boolean authCachingDisabled;
  private Lookup<AuthSchemeProvider> authSchemeRegistry;
  private boolean automaticRetriesDisabled;
  private BackoffManager backoffManager;
  private List<Closeable> closeables;
  private HttpClientConnectionManager connManager;
  private ConnectionBackoffStrategy connectionBackoffStrategy;
  private boolean connectionStateDisabled;
  private boolean contentCompressionDisabled;
  private boolean cookieManagementDisabled;
  private Lookup<CookieSpecProvider> cookieSpecRegistry;
  private CookieStore cookieStore;
  private CredentialsProvider credentialsProvider;
  private ConnectionConfig defaultConnectionConfig;
  private Collection<? extends Header> defaultHeaders;
  private RequestConfig defaultRequestConfig;
  private SocketConfig defaultSocketConfig;
  private X509HostnameVerifier hostnameVerifier;
  private HttpProcessor httpprocessor;
  private ConnectionKeepAliveStrategy keepAliveStrategy;
  private int maxConnPerRoute = 0;
  private int maxConnTotal = 0;
  private HttpHost proxy;
  private AuthenticationStrategy proxyAuthStrategy;
  private boolean redirectHandlingDisabled;
  private RedirectStrategy redirectStrategy;
  private HttpRequestExecutor requestExec;
  private LinkedList<HttpRequestInterceptor> requestFirst;
  private LinkedList<HttpRequestInterceptor> requestLast;
  private LinkedList<HttpResponseInterceptor> responseFirst;
  private LinkedList<HttpResponseInterceptor> responseLast;
  private HttpRequestRetryHandler retryHandler;
  private ConnectionReuseStrategy reuseStrategy;
  private HttpRoutePlanner routePlanner;
  private SchemePortResolver schemePortResolver;
  private ServiceUnavailableRetryStrategy serviceUnavailStrategy;
  private LayeredConnectionSocketFactory sslSocketFactory;
  private SSLContext sslcontext;
  private boolean systemProperties;
  private AuthenticationStrategy targetAuthStrategy;
  private String userAgent;
  private UserTokenHandler userTokenHandler;
  
  static
  {
    Object localObject = VersionInfo.loadVersionInfo("cz.msebera.android.httpclient.client", HttpClientBuilder.class.getClassLoader());
    if (localObject != null) {
      localObject = ((VersionInfo)localObject).getRelease();
    } else {
      localObject = "UNAVAILABLE";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Apache-HttpClient/");
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" (java 1.5)");
    DEFAULT_USER_AGENT = localStringBuilder.toString();
  }
  
  public static HttpClientBuilder create()
  {
    return new HttpClientBuilder();
  }
  
  private static String[] split(String paramString)
  {
    if (TextUtils.isBlank(paramString)) {
      return null;
    }
    return paramString.split(" *, *");
  }
  
  protected void addCloseable(Closeable paramCloseable)
  {
    if (paramCloseable == null) {
      return;
    }
    if (this.closeables == null) {
      this.closeables = new ArrayList();
    }
    this.closeables.add(paramCloseable);
  }
  
  public final HttpClientBuilder addInterceptorFirst(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    if (paramHttpRequestInterceptor == null) {
      return this;
    }
    if (this.requestFirst == null) {
      this.requestFirst = new LinkedList();
    }
    this.requestFirst.addFirst(paramHttpRequestInterceptor);
    return this;
  }
  
  public final HttpClientBuilder addInterceptorFirst(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    if (paramHttpResponseInterceptor == null) {
      return this;
    }
    if (this.responseFirst == null) {
      this.responseFirst = new LinkedList();
    }
    this.responseFirst.addFirst(paramHttpResponseInterceptor);
    return this;
  }
  
  public final HttpClientBuilder addInterceptorLast(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    if (paramHttpRequestInterceptor == null) {
      return this;
    }
    if (this.requestLast == null) {
      this.requestLast = new LinkedList();
    }
    this.requestLast.addLast(paramHttpRequestInterceptor);
    return this;
  }
  
  public final HttpClientBuilder addInterceptorLast(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    if (paramHttpResponseInterceptor == null) {
      return this;
    }
    if (this.responseLast == null) {
      this.responseLast = new LinkedList();
    }
    this.responseLast.addLast(paramHttpResponseInterceptor);
    return this;
  }
  
  public CloseableHttpClient build()
  {
    Object localObject3 = this.requestExec;
    if (localObject3 == null) {
      localObject3 = new HttpRequestExecutor();
    }
    Object localObject4 = this.connManager;
    ArrayList localArrayList = null;
    if (localObject4 == null)
    {
      localObject2 = this.sslSocketFactory;
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        if (this.systemProperties) {
          localObject1 = split(System.getProperty("https.protocols"));
        } else {
          localObject1 = null;
        }
        if (this.systemProperties) {
          localObject2 = split(System.getProperty("https.cipherSuites"));
        } else {
          localObject2 = null;
        }
        localObject5 = this.hostnameVerifier;
        localObject4 = localObject5;
        if (localObject5 == null) {
          localObject4 = SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
        }
        localObject5 = this.sslcontext;
        if (localObject5 != null) {
          localObject1 = new SSLConnectionSocketFactory((SSLContext)localObject5, (String[])localObject1, (String[])localObject2, (X509HostnameVerifier)localObject4);
        } else if (this.systemProperties) {
          localObject1 = new SSLConnectionSocketFactory((SSLSocketFactory)SSLSocketFactory.getDefault(), (String[])localObject1, (String[])localObject2, (X509HostnameVerifier)localObject4);
        } else {
          localObject1 = new SSLConnectionSocketFactory(SSLContexts.createDefault(), (X509HostnameVerifier)localObject4);
        }
      }
      localObject4 = new PoolingHttpClientConnectionManager(RegistryBuilder.create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", localObject1).build());
      localObject1 = this.defaultSocketConfig;
      if (localObject1 != null) {
        ((PoolingHttpClientConnectionManager)localObject4).setDefaultSocketConfig((SocketConfig)localObject1);
      }
      localObject1 = this.defaultConnectionConfig;
      if (localObject1 != null) {
        ((PoolingHttpClientConnectionManager)localObject4).setDefaultConnectionConfig((ConnectionConfig)localObject1);
      }
      if ((this.systemProperties) && ("true".equalsIgnoreCase(System.getProperty("http.keepAlive", "true"))))
      {
        i = Integer.parseInt(System.getProperty("http.maxConnections", "5"));
        ((PoolingHttpClientConnectionManager)localObject4).setDefaultMaxPerRoute(i);
        ((PoolingHttpClientConnectionManager)localObject4).setMaxTotal(i * 2);
      }
      int i = this.maxConnTotal;
      if (i > 0) {
        ((PoolingHttpClientConnectionManager)localObject4).setMaxTotal(i);
      }
      i = this.maxConnPerRoute;
      if (i > 0) {
        ((PoolingHttpClientConnectionManager)localObject4).setDefaultMaxPerRoute(i);
      }
    }
    Object localObject1 = this.reuseStrategy;
    if (localObject1 == null) {
      if (this.systemProperties)
      {
        if ("true".equalsIgnoreCase(System.getProperty("http.keepAlive", "true"))) {
          localObject1 = DefaultConnectionReuseStrategy.INSTANCE;
        } else {
          localObject1 = NoConnectionReuseStrategy.INSTANCE;
        }
      }
      else {
        localObject1 = DefaultConnectionReuseStrategy.INSTANCE;
      }
    }
    Object localObject5 = this.keepAliveStrategy;
    if (localObject5 == null) {
      localObject5 = DefaultConnectionKeepAliveStrategy.INSTANCE;
    }
    Object localObject6 = this.targetAuthStrategy;
    if (localObject6 == null) {
      localObject6 = TargetAuthenticationStrategy.INSTANCE;
    }
    Object localObject7 = this.proxyAuthStrategy;
    if (localObject7 == null) {
      localObject7 = ProxyAuthenticationStrategy.INSTANCE;
    }
    Object localObject2 = this.userTokenHandler;
    if (localObject2 == null) {
      if (!this.connectionStateDisabled) {
        localObject2 = DefaultUserTokenHandler.INSTANCE;
      } else {
        localObject2 = NoopUserTokenHandler.INSTANCE;
      }
    }
    localObject3 = decorateMainExec(new MainClientExec((HttpRequestExecutor)localObject3, (HttpClientConnectionManager)localObject4, (ConnectionReuseStrategy)localObject1, (ConnectionKeepAliveStrategy)localObject5, (AuthenticationStrategy)localObject6, (AuthenticationStrategy)localObject7, (UserTokenHandler)localObject2));
    localObject2 = this.httpprocessor;
    localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject2 = this.userAgent;
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        if (this.systemProperties) {
          localObject2 = System.getProperty("http.agent");
        }
        localObject1 = localObject2;
        if (localObject2 == null) {
          localObject1 = DEFAULT_USER_AGENT;
        }
      }
      localObject2 = HttpProcessorBuilder.create();
      localObject5 = this.requestFirst;
      if (localObject5 != null)
      {
        localObject5 = ((LinkedList)localObject5).iterator();
        while (((Iterator)localObject5).hasNext()) {
          ((HttpProcessorBuilder)localObject2).addFirst((HttpRequestInterceptor)((Iterator)localObject5).next());
        }
      }
      localObject5 = this.responseFirst;
      if (localObject5 != null)
      {
        localObject5 = ((LinkedList)localObject5).iterator();
        while (((Iterator)localObject5).hasNext()) {
          ((HttpProcessorBuilder)localObject2).addFirst((HttpResponseInterceptor)((Iterator)localObject5).next());
        }
      }
      ((HttpProcessorBuilder)localObject2).addAll(new HttpRequestInterceptor[] { new RequestDefaultHeaders(this.defaultHeaders), new RequestContent(), new RequestTargetHost(), new RequestClientConnControl(), new RequestUserAgent((String)localObject1), new RequestExpectContinue() });
      if (!this.cookieManagementDisabled) {
        ((HttpProcessorBuilder)localObject2).add(new RequestAddCookies());
      }
      if (!this.contentCompressionDisabled) {
        ((HttpProcessorBuilder)localObject2).add(new RequestAcceptEncoding());
      }
      if (!this.authCachingDisabled) {
        ((HttpProcessorBuilder)localObject2).add(new RequestAuthCache());
      }
      if (!this.cookieManagementDisabled) {
        ((HttpProcessorBuilder)localObject2).add(new ResponseProcessCookies());
      }
      if (!this.contentCompressionDisabled) {
        ((HttpProcessorBuilder)localObject2).add(new ResponseContentEncoding());
      }
      localObject1 = this.requestLast;
      if (localObject1 != null)
      {
        localObject1 = ((LinkedList)localObject1).iterator();
        while (((Iterator)localObject1).hasNext()) {
          ((HttpProcessorBuilder)localObject2).addLast((HttpRequestInterceptor)((Iterator)localObject1).next());
        }
      }
      localObject1 = this.responseLast;
      if (localObject1 != null)
      {
        localObject1 = ((LinkedList)localObject1).iterator();
        while (((Iterator)localObject1).hasNext()) {
          ((HttpProcessorBuilder)localObject2).addLast((HttpResponseInterceptor)((Iterator)localObject1).next());
        }
      }
      localObject1 = ((HttpProcessorBuilder)localObject2).build();
    }
    localObject3 = decorateProtocolExec(new ProtocolExec((ClientExecChain)localObject3, (HttpProcessor)localObject1));
    localObject2 = localObject3;
    if (!this.automaticRetriesDisabled)
    {
      localObject2 = this.retryHandler;
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = DefaultHttpRequestRetryHandler.INSTANCE;
      }
      localObject2 = new RetryExec((ClientExecChain)localObject3, (HttpRequestRetryHandler)localObject1);
    }
    localObject1 = this.routePlanner;
    if (localObject1 == null)
    {
      localObject3 = this.schemePortResolver;
      localObject1 = localObject3;
      if (localObject3 == null) {
        localObject1 = DefaultSchemePortResolver.INSTANCE;
      }
      localObject3 = this.proxy;
      if (localObject3 != null) {
        localObject1 = new DefaultProxyRoutePlanner((HttpHost)localObject3, (SchemePortResolver)localObject1);
      } else if (this.systemProperties) {
        localObject1 = new SystemDefaultRoutePlanner((SchemePortResolver)localObject1, ProxySelector.getDefault());
      } else {
        localObject1 = new DefaultRoutePlanner((SchemePortResolver)localObject1);
      }
    }
    localObject3 = localObject2;
    if (!this.redirectHandlingDisabled)
    {
      localObject5 = this.redirectStrategy;
      localObject3 = localObject5;
      if (localObject5 == null) {
        localObject3 = DefaultRedirectStrategy.INSTANCE;
      }
      localObject3 = new RedirectExec((ClientExecChain)localObject2, (HttpRoutePlanner)localObject1, (RedirectStrategy)localObject3);
    }
    localObject5 = this.serviceUnavailStrategy;
    localObject2 = localObject3;
    if (localObject5 != null) {
      localObject2 = new ServiceUnavailableRetryExec((ClientExecChain)localObject3, (ServiceUnavailableRetryStrategy)localObject5);
    }
    localObject3 = this.backoffManager;
    localObject5 = this.connectionBackoffStrategy;
    if ((localObject3 != null) && (localObject5 != null)) {
      localObject3 = new BackoffStrategyExec((ClientExecChain)localObject2, (ConnectionBackoffStrategy)localObject5, (BackoffManager)localObject3);
    } else {
      localObject3 = localObject2;
    }
    localObject5 = this.authSchemeRegistry;
    if (localObject5 == null) {
      localObject5 = RegistryBuilder.create().register("Basic", new BasicSchemeFactory()).register("Digest", new DigestSchemeFactory()).register("NTLM", new NTLMSchemeFactory()).build();
    }
    localObject6 = this.cookieSpecRegistry;
    if (localObject6 == null) {
      localObject6 = RegistryBuilder.create().register("best-match", new BestMatchSpecFactory()).register("standard", new RFC2965SpecFactory()).register("compatibility", new BrowserCompatSpecFactory()).register("netscape", new NetscapeDraftSpecFactory()).register("ignoreCookies", new IgnoreSpecFactory()).register("rfc2109", new RFC2109SpecFactory()).register("rfc2965", new RFC2965SpecFactory()).build();
    }
    localObject7 = this.cookieStore;
    if (localObject7 == null) {
      localObject7 = new BasicCookieStore();
    }
    localObject2 = this.credentialsProvider;
    if (localObject2 == null) {
      if (this.systemProperties) {
        localObject2 = new SystemDefaultCredentialsProvider();
      } else {
        localObject2 = new BasicCredentialsProvider();
      }
    }
    RequestConfig localRequestConfig = this.defaultRequestConfig;
    if (localRequestConfig == null) {
      localRequestConfig = RequestConfig.DEFAULT;
    }
    List localList = this.closeables;
    if (localList != null) {
      localArrayList = new ArrayList(localList);
    }
    return new InternalHttpClient((ClientExecChain)localObject3, (HttpClientConnectionManager)localObject4, (HttpRoutePlanner)localObject1, (Lookup)localObject6, (Lookup)localObject5, (CookieStore)localObject7, (CredentialsProvider)localObject2, localRequestConfig, localArrayList);
  }
  
  protected ClientExecChain decorateMainExec(ClientExecChain paramClientExecChain)
  {
    return paramClientExecChain;
  }
  
  protected ClientExecChain decorateProtocolExec(ClientExecChain paramClientExecChain)
  {
    return paramClientExecChain;
  }
  
  public final HttpClientBuilder disableAuthCaching()
  {
    this.authCachingDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder disableAutomaticRetries()
  {
    this.automaticRetriesDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder disableConnectionState()
  {
    this.connectionStateDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder disableContentCompression()
  {
    this.contentCompressionDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder disableCookieManagement()
  {
    this.cookieManagementDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder disableRedirectHandling()
  {
    this.redirectHandlingDisabled = true;
    return this;
  }
  
  public final HttpClientBuilder setBackoffManager(BackoffManager paramBackoffManager)
  {
    this.backoffManager = paramBackoffManager;
    return this;
  }
  
  public final HttpClientBuilder setConnectionBackoffStrategy(ConnectionBackoffStrategy paramConnectionBackoffStrategy)
  {
    this.connectionBackoffStrategy = paramConnectionBackoffStrategy;
    return this;
  }
  
  public final HttpClientBuilder setConnectionManager(HttpClientConnectionManager paramHttpClientConnectionManager)
  {
    this.connManager = paramHttpClientConnectionManager;
    return this;
  }
  
  public final HttpClientBuilder setConnectionReuseStrategy(ConnectionReuseStrategy paramConnectionReuseStrategy)
  {
    this.reuseStrategy = paramConnectionReuseStrategy;
    return this;
  }
  
  public final HttpClientBuilder setDefaultAuthSchemeRegistry(Lookup<AuthSchemeProvider> paramLookup)
  {
    this.authSchemeRegistry = paramLookup;
    return this;
  }
  
  public final HttpClientBuilder setDefaultConnectionConfig(ConnectionConfig paramConnectionConfig)
  {
    this.defaultConnectionConfig = paramConnectionConfig;
    return this;
  }
  
  public final HttpClientBuilder setDefaultCookieSpecRegistry(Lookup<CookieSpecProvider> paramLookup)
  {
    this.cookieSpecRegistry = paramLookup;
    return this;
  }
  
  public final HttpClientBuilder setDefaultCookieStore(CookieStore paramCookieStore)
  {
    this.cookieStore = paramCookieStore;
    return this;
  }
  
  public final HttpClientBuilder setDefaultCredentialsProvider(CredentialsProvider paramCredentialsProvider)
  {
    this.credentialsProvider = paramCredentialsProvider;
    return this;
  }
  
  public final HttpClientBuilder setDefaultHeaders(Collection<? extends Header> paramCollection)
  {
    this.defaultHeaders = paramCollection;
    return this;
  }
  
  public final HttpClientBuilder setDefaultRequestConfig(RequestConfig paramRequestConfig)
  {
    this.defaultRequestConfig = paramRequestConfig;
    return this;
  }
  
  public final HttpClientBuilder setDefaultSocketConfig(SocketConfig paramSocketConfig)
  {
    this.defaultSocketConfig = paramSocketConfig;
    return this;
  }
  
  public final HttpClientBuilder setHostnameVerifier(X509HostnameVerifier paramX509HostnameVerifier)
  {
    this.hostnameVerifier = paramX509HostnameVerifier;
    return this;
  }
  
  public final HttpClientBuilder setHttpProcessor(HttpProcessor paramHttpProcessor)
  {
    this.httpprocessor = paramHttpProcessor;
    return this;
  }
  
  public final HttpClientBuilder setKeepAliveStrategy(ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy)
  {
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
    return this;
  }
  
  public final HttpClientBuilder setMaxConnPerRoute(int paramInt)
  {
    this.maxConnPerRoute = paramInt;
    return this;
  }
  
  public final HttpClientBuilder setMaxConnTotal(int paramInt)
  {
    this.maxConnTotal = paramInt;
    return this;
  }
  
  public final HttpClientBuilder setProxy(HttpHost paramHttpHost)
  {
    this.proxy = paramHttpHost;
    return this;
  }
  
  public final HttpClientBuilder setProxyAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    this.proxyAuthStrategy = paramAuthenticationStrategy;
    return this;
  }
  
  public final HttpClientBuilder setRedirectStrategy(RedirectStrategy paramRedirectStrategy)
  {
    this.redirectStrategy = paramRedirectStrategy;
    return this;
  }
  
  public final HttpClientBuilder setRequestExecutor(HttpRequestExecutor paramHttpRequestExecutor)
  {
    this.requestExec = paramHttpRequestExecutor;
    return this;
  }
  
  public final HttpClientBuilder setRetryHandler(HttpRequestRetryHandler paramHttpRequestRetryHandler)
  {
    this.retryHandler = paramHttpRequestRetryHandler;
    return this;
  }
  
  public final HttpClientBuilder setRoutePlanner(HttpRoutePlanner paramHttpRoutePlanner)
  {
    this.routePlanner = paramHttpRoutePlanner;
    return this;
  }
  
  public final HttpClientBuilder setSSLSocketFactory(LayeredConnectionSocketFactory paramLayeredConnectionSocketFactory)
  {
    this.sslSocketFactory = paramLayeredConnectionSocketFactory;
    return this;
  }
  
  public final HttpClientBuilder setSchemePortResolver(SchemePortResolver paramSchemePortResolver)
  {
    this.schemePortResolver = paramSchemePortResolver;
    return this;
  }
  
  public final HttpClientBuilder setServiceUnavailableRetryStrategy(ServiceUnavailableRetryStrategy paramServiceUnavailableRetryStrategy)
  {
    this.serviceUnavailStrategy = paramServiceUnavailableRetryStrategy;
    return this;
  }
  
  public final HttpClientBuilder setSslcontext(SSLContext paramSSLContext)
  {
    this.sslcontext = paramSSLContext;
    return this;
  }
  
  public final HttpClientBuilder setTargetAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    this.targetAuthStrategy = paramAuthenticationStrategy;
    return this;
  }
  
  public final HttpClientBuilder setUserAgent(String paramString)
  {
    this.userAgent = paramString;
    return this;
  }
  
  public final HttpClientBuilder setUserTokenHandler(UserTokenHandler paramUserTokenHandler)
  {
    this.userTokenHandler = paramUserTokenHandler;
    return this;
  }
  
  public final HttpClientBuilder useSystemProperties()
  {
    this.systemProperties = true;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/HttpClientBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */