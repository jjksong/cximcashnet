package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.ConnectionBackoffStrategy;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

public class DefaultBackoffStrategy
  implements ConnectionBackoffStrategy
{
  public boolean shouldBackoff(HttpResponse paramHttpResponse)
  {
    boolean bool;
    if (paramHttpResponse.getStatusLine().getStatusCode() == 503) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean shouldBackoff(Throwable paramThrowable)
  {
    boolean bool;
    if ((!(paramThrowable instanceof SocketTimeoutException)) && (!(paramThrowable instanceof ConnectException))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultBackoffStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */