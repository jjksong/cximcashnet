package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Immutable
class ResponseCachingPolicy
{
  private static final String[] AUTH_CACHEABLE_PARAMS = { "s-maxage", "must-revalidate", "public" };
  private static final Set<Integer> cacheableStatuses = new HashSet(Arrays.asList(new Integer[] { Integer.valueOf(200), Integer.valueOf(203), Integer.valueOf(300), Integer.valueOf(301), Integer.valueOf(410) }));
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final long maxObjectSizeBytes;
  private final boolean neverCache1_0ResponsesWithQueryString;
  private final boolean sharedCache;
  private final Set<Integer> uncacheableStatuses;
  
  public ResponseCachingPolicy(long paramLong, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.maxObjectSizeBytes = paramLong;
    this.sharedCache = paramBoolean1;
    this.neverCache1_0ResponsesWithQueryString = paramBoolean2;
    if (paramBoolean3) {
      this.uncacheableStatuses = new HashSet(Arrays.asList(new Integer[] { Integer.valueOf(206) }));
    } else {
      this.uncacheableStatuses = new HashSet(Arrays.asList(new Integer[] { Integer.valueOf(206), Integer.valueOf(303) }));
    }
  }
  
  private boolean expiresHeaderLessOrEqualToDateHeaderAndNoCacheControl(HttpResponse paramHttpResponse)
  {
    Object localObject = paramHttpResponse.getFirstHeader("Cache-Control");
    boolean bool = false;
    if (localObject != null) {
      return false;
    }
    localObject = paramHttpResponse.getFirstHeader("Expires");
    paramHttpResponse = paramHttpResponse.getFirstHeader("Date");
    if ((localObject != null) && (paramHttpResponse != null))
    {
      localObject = DateUtils.parseDate(((Header)localObject).getValue());
      paramHttpResponse = DateUtils.parseDate(paramHttpResponse.getValue());
      if ((localObject != null) && (paramHttpResponse != null))
      {
        if ((((Date)localObject).equals(paramHttpResponse)) || (((Date)localObject).before(paramHttpResponse))) {
          bool = true;
        }
        return bool;
      }
      return false;
    }
    return false;
  }
  
  private boolean from1_0Origin(HttpResponse paramHttpResponse)
  {
    Object localObject = paramHttpResponse.getFirstHeader("Via");
    if (localObject != null)
    {
      localObject = ((Header)localObject).getElements();
      if (localObject.length > 0)
      {
        paramHttpResponse = localObject[0].toString().split("\\s")[0];
        if (paramHttpResponse.contains("/")) {
          return paramHttpResponse.equals("HTTP/1.0");
        }
        return paramHttpResponse.equals("1.0");
      }
    }
    return HttpVersion.HTTP_1_0.equals(paramHttpResponse.getProtocolVersion());
  }
  
  private boolean requestProtocolGreaterThanAccepted(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if (paramHttpRequest.getProtocolVersion().compareToVersion(HttpVersion.HTTP_1_1) > 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean unknownStatusCode(int paramInt)
  {
    if ((paramInt >= 100) && (paramInt <= 101)) {
      return false;
    }
    if ((paramInt >= 200) && (paramInt <= 206)) {
      return false;
    }
    if ((paramInt >= 300) && (paramInt <= 307)) {
      return false;
    }
    if ((paramInt >= 400) && (paramInt <= 417)) {
      return false;
    }
    return (paramInt < 500) || (paramInt > 505);
  }
  
  protected boolean hasCacheControlParameterFrom(HttpMessage paramHttpMessage, String[] paramArrayOfString)
  {
    Header[] arrayOfHeader = paramHttpMessage.getHeaders("Cache-Control");
    int m = arrayOfHeader.length;
    for (int i = 0; i < m; i++) {
      for (paramHttpMessage : arrayOfHeader[i].getElements())
      {
        int i1 = paramArrayOfString.length;
        for (int k = 0; k < i1; k++) {
          if (paramArrayOfString[k].equalsIgnoreCase(paramHttpMessage.getName())) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  protected boolean isExplicitlyCacheable(HttpResponse paramHttpResponse)
  {
    if (paramHttpResponse.getFirstHeader("Expires") != null) {
      return true;
    }
    return hasCacheControlParameterFrom(paramHttpResponse, new String[] { "max-age", "s-maxage", "must-revalidate", "proxy-revalidate", "public" });
  }
  
  protected boolean isExplicitlyNonCacheable(HttpResponse paramHttpResponse)
  {
    Header[] arrayOfHeader = paramHttpResponse.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    for (int i = 0; i < k; i++)
    {
      paramHttpResponse = arrayOfHeader[i].getElements();
      int m = paramHttpResponse.length;
      int j = 0;
      while (j < m)
      {
        Object localObject = paramHttpResponse[j];
        if ((!"no-store".equals(((HeaderElement)localObject).getName())) && (!"no-cache".equals(((HeaderElement)localObject).getName())) && ((!this.sharedCache) || (!"private".equals(((HeaderElement)localObject).getName())))) {
          j++;
        } else {
          return true;
        }
      }
    }
    return false;
  }
  
  public boolean isResponseCacheable(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    if (requestProtocolGreaterThanAccepted(paramHttpRequest))
    {
      this.log.debug("Response was not cacheable.");
      return false;
    }
    if (hasCacheControlParameterFrom(paramHttpRequest, new String[] { "no-store" })) {
      return false;
    }
    if (paramHttpRequest.getRequestLine().getUri().contains("?"))
    {
      if ((this.neverCache1_0ResponsesWithQueryString) && (from1_0Origin(paramHttpResponse)))
      {
        this.log.debug("Response was not cacheable as it had a query string.");
        return false;
      }
      if (!isExplicitlyCacheable(paramHttpResponse))
      {
        this.log.debug("Response was not cacheable as it is missing explicit caching headers.");
        return false;
      }
    }
    if (expiresHeaderLessOrEqualToDateHeaderAndNoCacheControl(paramHttpResponse)) {
      return false;
    }
    if (this.sharedCache)
    {
      Header[] arrayOfHeader = paramHttpRequest.getHeaders("Authorization");
      if ((arrayOfHeader != null) && (arrayOfHeader.length > 0) && (!hasCacheControlParameterFrom(paramHttpResponse, AUTH_CACHEABLE_PARAMS))) {
        return false;
      }
    }
    return isResponseCacheable(paramHttpRequest.getRequestLine().getMethod(), paramHttpResponse);
  }
  
  public boolean isResponseCacheable(String paramString, HttpResponse paramHttpResponse)
  {
    boolean bool2 = "GET".equals(paramString);
    boolean bool1 = false;
    if (!bool2)
    {
      this.log.debug("Response was not cacheable.");
      return false;
    }
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    if (cacheableStatuses.contains(Integer.valueOf(i)))
    {
      i = 1;
    }
    else
    {
      if (this.uncacheableStatuses.contains(Integer.valueOf(i))) {
        return false;
      }
      if (unknownStatusCode(i)) {
        return false;
      }
      i = 0;
    }
    paramString = paramHttpResponse.getFirstHeader("Content-Length");
    if ((paramString != null) && (Integer.parseInt(paramString.getValue()) > this.maxObjectSizeBytes)) {
      return false;
    }
    if (paramHttpResponse.getHeaders("Age").length > 1) {
      return false;
    }
    if (paramHttpResponse.getHeaders("Expires").length > 1) {
      return false;
    }
    paramString = paramHttpResponse.getHeaders("Date");
    if (paramString.length != 1) {
      return false;
    }
    if (DateUtils.parseDate(paramString[0].getValue()) == null) {
      return false;
    }
    Header[] arrayOfHeader = paramHttpResponse.getHeaders("Vary");
    int m = arrayOfHeader.length;
    for (int j = 0; j < m; j++)
    {
      paramString = arrayOfHeader[j].getElements();
      int n = paramString.length;
      for (int k = 0; k < n; k++) {
        if ("*".equals(paramString[k].getName())) {
          return false;
        }
      }
    }
    if (isExplicitlyNonCacheable(paramHttpResponse)) {
      return false;
    }
    if ((i != 0) || (isExplicitlyCacheable(paramHttpResponse))) {
      bool1 = true;
    }
    return bool1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ResponseCachingPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */