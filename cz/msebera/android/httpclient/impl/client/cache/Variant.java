package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;

class Variant
{
  private final String cacheKey;
  private final HttpCacheEntry entry;
  private final String variantKey;
  
  public Variant(String paramString1, String paramString2, HttpCacheEntry paramHttpCacheEntry)
  {
    this.variantKey = paramString1;
    this.cacheKey = paramString2;
    this.entry = paramHttpCacheEntry;
  }
  
  public String getCacheKey()
  {
    return this.cacheKey;
  }
  
  public HttpCacheEntry getEntry()
  {
    return this.entry;
  }
  
  public String getVariantKey()
  {
    return this.variantKey;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/Variant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */