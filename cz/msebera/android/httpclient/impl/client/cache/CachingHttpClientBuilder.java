package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.client.cache.HttpCacheInvalidator;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.impl.execchain.ClientExecChain;
import java.io.Closeable;
import java.io.File;

public class CachingHttpClientBuilder
  extends HttpClientBuilder
{
  private CacheConfig cacheConfig;
  private File cacheDir;
  private HttpCacheInvalidator httpCacheInvalidator;
  private ResourceFactory resourceFactory;
  private SchedulingStrategy schedulingStrategy;
  private HttpCacheStorage storage;
  
  public static CachingHttpClientBuilder create()
  {
    return new CachingHttpClientBuilder();
  }
  
  private AsynchronousValidator createAsynchronousRevalidator(CacheConfig paramCacheConfig)
  {
    if (paramCacheConfig.getAsynchronousWorkersMax() > 0)
    {
      paramCacheConfig = new AsynchronousValidator(createSchedulingStrategy(paramCacheConfig));
      addCloseable(paramCacheConfig);
      return paramCacheConfig;
    }
    return null;
  }
  
  private SchedulingStrategy createSchedulingStrategy(CacheConfig paramCacheConfig)
  {
    SchedulingStrategy localSchedulingStrategy = this.schedulingStrategy;
    if (localSchedulingStrategy != null) {
      paramCacheConfig = localSchedulingStrategy;
    } else {
      paramCacheConfig = new ImmediateSchedulingStrategy(paramCacheConfig);
    }
    return paramCacheConfig;
  }
  
  protected ClientExecChain decorateMainExec(ClientExecChain paramClientExecChain)
  {
    CacheConfig localCacheConfig = this.cacheConfig;
    if (localCacheConfig == null) {
      localCacheConfig = CacheConfig.DEFAULT;
    }
    Object localObject1 = this.resourceFactory;
    if (localObject1 == null)
    {
      localObject1 = this.cacheDir;
      if (localObject1 == null) {
        localObject1 = new HeapResourceFactory();
      } else {
        localObject1 = new FileResourceFactory((File)localObject1);
      }
    }
    Object localObject2 = this.storage;
    if (localObject2 == null) {
      if (this.cacheDir == null)
      {
        localObject2 = new BasicHttpCacheStorage(localCacheConfig);
      }
      else
      {
        localObject2 = new ManagedHttpCacheStorage(localCacheConfig);
        addCloseable((Closeable)localObject2);
      }
    }
    AsynchronousValidator localAsynchronousValidator = createAsynchronousRevalidator(localCacheConfig);
    CacheKeyGenerator localCacheKeyGenerator = new CacheKeyGenerator();
    Object localObject3 = this.httpCacheInvalidator;
    if (localObject3 == null) {
      localObject3 = new CacheInvalidator(localCacheKeyGenerator, (HttpCacheStorage)localObject2);
    }
    return new CachingExec(paramClientExecChain, new BasicHttpCache((ResourceFactory)localObject1, (HttpCacheStorage)localObject2, localCacheConfig, localCacheKeyGenerator, (HttpCacheInvalidator)localObject3), localCacheConfig, localAsynchronousValidator);
  }
  
  public final CachingHttpClientBuilder setCacheConfig(CacheConfig paramCacheConfig)
  {
    this.cacheConfig = paramCacheConfig;
    return this;
  }
  
  public final CachingHttpClientBuilder setCacheDir(File paramFile)
  {
    this.cacheDir = paramFile;
    return this;
  }
  
  public final CachingHttpClientBuilder setHttpCacheInvalidator(HttpCacheInvalidator paramHttpCacheInvalidator)
  {
    this.httpCacheInvalidator = paramHttpCacheInvalidator;
    return this;
  }
  
  public final CachingHttpClientBuilder setHttpCacheStorage(HttpCacheStorage paramHttpCacheStorage)
  {
    this.storage = paramHttpCacheStorage;
    return this;
  }
  
  public final CachingHttpClientBuilder setResourceFactory(ResourceFactory paramResourceFactory)
  {
    this.resourceFactory = paramResourceFactory;
    return this;
  }
  
  public final CachingHttpClientBuilder setSchedulingStrategy(SchedulingStrategy paramSchedulingStrategy)
  {
    this.schedulingStrategy = paramSchedulingStrategy;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CachingHttpClientBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */