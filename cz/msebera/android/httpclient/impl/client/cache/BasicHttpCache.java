package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheInvalidator;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.cache.HttpCacheUpdateCallback;
import cz.msebera.android.httpclient.client.cache.HttpCacheUpdateException;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.message.BasicHttpResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class BasicHttpCache
  implements HttpCache
{
  private static final Set<String> safeRequestMethods = new HashSet(Arrays.asList(new String[] { "HEAD", "GET", "OPTIONS", "TRACE" }));
  private final CacheEntryUpdater cacheEntryUpdater;
  private final HttpCacheInvalidator cacheInvalidator;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final long maxObjectSizeBytes;
  private final ResourceFactory resourceFactory;
  private final CachedHttpResponseGenerator responseGenerator;
  private final HttpCacheStorage storage;
  private final CacheKeyGenerator uriExtractor;
  
  public BasicHttpCache()
  {
    this(CacheConfig.DEFAULT);
  }
  
  public BasicHttpCache(ResourceFactory paramResourceFactory, HttpCacheStorage paramHttpCacheStorage, CacheConfig paramCacheConfig)
  {
    this(paramResourceFactory, paramHttpCacheStorage, paramCacheConfig, new CacheKeyGenerator());
  }
  
  public BasicHttpCache(ResourceFactory paramResourceFactory, HttpCacheStorage paramHttpCacheStorage, CacheConfig paramCacheConfig, CacheKeyGenerator paramCacheKeyGenerator)
  {
    this(paramResourceFactory, paramHttpCacheStorage, paramCacheConfig, paramCacheKeyGenerator, new CacheInvalidator(paramCacheKeyGenerator, paramHttpCacheStorage));
  }
  
  public BasicHttpCache(ResourceFactory paramResourceFactory, HttpCacheStorage paramHttpCacheStorage, CacheConfig paramCacheConfig, CacheKeyGenerator paramCacheKeyGenerator, HttpCacheInvalidator paramHttpCacheInvalidator)
  {
    this.resourceFactory = paramResourceFactory;
    this.uriExtractor = paramCacheKeyGenerator;
    this.cacheEntryUpdater = new CacheEntryUpdater(paramResourceFactory);
    this.maxObjectSizeBytes = paramCacheConfig.getMaxObjectSize();
    this.responseGenerator = new CachedHttpResponseGenerator();
    this.storage = paramHttpCacheStorage;
    this.cacheInvalidator = paramHttpCacheInvalidator;
  }
  
  public BasicHttpCache(CacheConfig paramCacheConfig)
  {
    this(new HeapResourceFactory(), new BasicHttpCacheStorage(paramCacheConfig), paramCacheConfig);
  }
  
  private void addVariantWithEtag(String paramString1, String paramString2, Map<String, Variant> paramMap)
    throws IOException
  {
    HttpCacheEntry localHttpCacheEntry = this.storage.getEntry(paramString2);
    if (localHttpCacheEntry == null) {
      return;
    }
    Header localHeader = localHttpCacheEntry.getFirstHeader("ETag");
    if (localHeader == null) {
      return;
    }
    paramMap.put(localHeader.getValue(), new Variant(paramString1, paramString2, localHttpCacheEntry));
  }
  
  public HttpResponse cacheAndReturnResponse(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException
  {
    return cacheAndReturnResponse(paramHttpHost, paramHttpRequest, Proxies.enhanceResponse(paramHttpResponse), paramDate1, paramDate2);
  }
  
  public CloseableHttpResponse cacheAndReturnResponse(HttpHost paramHttpHost, HttpRequest paramHttpRequest, CloseableHttpResponse paramCloseableHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException
  {
    Object localObject = getResponseReader(paramHttpRequest, paramCloseableHttpResponse);
    int j = 1;
    int i = j;
    try
    {
      ((SizeLimitedResponseReader)localObject).readResponse();
      i = j;
      if (((SizeLimitedResponseReader)localObject).isLimitReached())
      {
        i = 0;
        return ((SizeLimitedResponseReader)localObject).getReconstructedResponse();
      }
      i = j;
      localObject = ((SizeLimitedResponseReader)localObject).getResource();
      i = j;
      if (isIncompleteResponse(paramCloseableHttpResponse, (Resource)localObject))
      {
        i = j;
        paramHttpHost = generateIncompleteResponseError(paramCloseableHttpResponse, (Resource)localObject);
        paramCloseableHttpResponse.close();
        return paramHttpHost;
      }
      i = j;
      HttpCacheEntry localHttpCacheEntry = new cz/msebera/android/httpclient/client/cache/HttpCacheEntry;
      i = j;
      localHttpCacheEntry.<init>(paramDate1, paramDate2, paramCloseableHttpResponse.getStatusLine(), paramCloseableHttpResponse.getAllHeaders(), (Resource)localObject);
      i = j;
      storeInCache(paramHttpHost, paramHttpRequest, localHttpCacheEntry);
      i = j;
      paramHttpHost = this.responseGenerator.generateResponse(localHttpCacheEntry);
      paramCloseableHttpResponse.close();
      return paramHttpHost;
    }
    finally
    {
      if (i != 0) {
        paramCloseableHttpResponse.close();
      }
    }
  }
  
  HttpCacheEntry doGetUpdatedParentEntry(String paramString1, HttpCacheEntry paramHttpCacheEntry1, HttpCacheEntry paramHttpCacheEntry2, String paramString2, String paramString3)
    throws IOException
  {
    HttpCacheEntry localHttpCacheEntry = paramHttpCacheEntry1;
    if (paramHttpCacheEntry1 == null) {
      localHttpCacheEntry = paramHttpCacheEntry2;
    }
    if (localHttpCacheEntry.getResource() != null) {
      paramString1 = this.resourceFactory.copy(paramString1, localHttpCacheEntry.getResource());
    } else {
      paramString1 = null;
    }
    paramHttpCacheEntry1 = new HashMap(localHttpCacheEntry.getVariantMap());
    paramHttpCacheEntry1.put(paramString2, paramString3);
    return new HttpCacheEntry(localHttpCacheEntry.getRequestDate(), localHttpCacheEntry.getResponseDate(), localHttpCacheEntry.getStatusLine(), localHttpCacheEntry.getAllHeaders(), paramString1, paramHttpCacheEntry1);
  }
  
  public void flushCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException
  {
    if (!safeRequestMethods.contains(paramHttpRequest.getRequestLine().getMethod()))
    {
      paramHttpHost = this.uriExtractor.getURI(paramHttpHost, paramHttpRequest);
      this.storage.removeEntry(paramHttpHost);
    }
  }
  
  public void flushInvalidatedCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException
  {
    this.cacheInvalidator.flushInvalidatedCacheEntries(paramHttpHost, paramHttpRequest);
  }
  
  public void flushInvalidatedCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    if (!safeRequestMethods.contains(paramHttpRequest.getRequestLine().getMethod())) {
      this.cacheInvalidator.flushInvalidatedCacheEntries(paramHttpHost, paramHttpRequest, paramHttpResponse);
    }
  }
  
  CloseableHttpResponse generateIncompleteResponseError(HttpResponse paramHttpResponse, Resource paramResource)
  {
    int i = Integer.parseInt(paramHttpResponse.getFirstHeader("Content-Length").getValue());
    paramHttpResponse = new BasicHttpResponse(HttpVersion.HTTP_1_1, 502, "Bad Gateway");
    paramHttpResponse.setHeader("Content-Type", "text/plain;charset=UTF-8");
    paramResource = String.format("Received incomplete response with Content-Length %d but actual body length %d", new Object[] { Integer.valueOf(i), Long.valueOf(paramResource.length()) }).getBytes();
    paramHttpResponse.setHeader("Content-Length", Integer.toString(paramResource.length));
    paramHttpResponse.setEntity(new ByteArrayEntity(paramResource));
    return Proxies.enhanceResponse(paramHttpResponse);
  }
  
  public HttpCacheEntry getCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException
  {
    paramHttpHost = this.storage.getEntry(this.uriExtractor.getURI(paramHttpHost, paramHttpRequest));
    if (paramHttpHost == null) {
      return null;
    }
    if (!paramHttpHost.hasVariants()) {
      return paramHttpHost;
    }
    paramHttpHost = (String)paramHttpHost.getVariantMap().get(this.uriExtractor.getVariantKey(paramHttpRequest, paramHttpHost));
    if (paramHttpHost == null) {
      return null;
    }
    return this.storage.getEntry(paramHttpHost);
  }
  
  SizeLimitedResponseReader getResponseReader(HttpRequest paramHttpRequest, CloseableHttpResponse paramCloseableHttpResponse)
  {
    return new SizeLimitedResponseReader(this.resourceFactory, this.maxObjectSizeBytes, paramHttpRequest, paramCloseableHttpResponse);
  }
  
  public Map<String, Variant> getVariantCacheEntriesWithEtags(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException
  {
    HashMap localHashMap = new HashMap();
    paramHttpHost = this.storage.getEntry(this.uriExtractor.getURI(paramHttpHost, paramHttpRequest));
    if ((paramHttpHost != null) && (paramHttpHost.hasVariants()))
    {
      paramHttpHost = paramHttpHost.getVariantMap().entrySet().iterator();
      while (paramHttpHost.hasNext())
      {
        paramHttpRequest = (Map.Entry)paramHttpHost.next();
        addVariantWithEtag((String)paramHttpRequest.getKey(), (String)paramHttpRequest.getValue(), localHashMap);
      }
      return localHashMap;
    }
    return localHashMap;
  }
  
  boolean isIncompleteResponse(HttpResponse paramHttpResponse, Resource paramResource)
  {
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    boolean bool = false;
    if ((i != 200) && (i != 206)) {
      return false;
    }
    paramHttpResponse = paramHttpResponse.getFirstHeader("Content-Length");
    if (paramHttpResponse == null) {
      return false;
    }
    try
    {
      i = Integer.parseInt(paramHttpResponse.getValue());
      if (paramResource.length() < i) {
        bool = true;
      }
      return bool;
    }
    catch (NumberFormatException paramHttpResponse) {}
    return false;
  }
  
  public void reuseVariantEntryFor(HttpHost paramHttpHost, final HttpRequest paramHttpRequest, Variant paramVariant)
    throws IOException
  {
    paramHttpHost = this.uriExtractor.getURI(paramHttpHost, paramHttpRequest);
    final HttpCacheEntry localHttpCacheEntry = paramVariant.getEntry();
    paramHttpRequest = new HttpCacheUpdateCallback()
    {
      public HttpCacheEntry update(HttpCacheEntry paramAnonymousHttpCacheEntry)
        throws IOException
      {
        return BasicHttpCache.this.doGetUpdatedParentEntry(paramHttpRequest.getRequestLine().getUri(), paramAnonymousHttpCacheEntry, localHttpCacheEntry, this.val$variantKey, this.val$variantCacheKey);
      }
    };
    try
    {
      this.storage.updateEntry(paramHttpHost, paramHttpRequest);
    }
    catch (HttpCacheUpdateException localHttpCacheUpdateException)
    {
      paramHttpRequest = this.log;
      paramVariant = new StringBuilder();
      paramVariant.append("Could not update key [");
      paramVariant.append(paramHttpHost);
      paramVariant.append("]");
      paramHttpRequest.warn(paramVariant.toString(), localHttpCacheUpdateException);
    }
  }
  
  void storeInCache(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    if (paramHttpCacheEntry.hasVariants()) {
      storeVariantEntry(paramHttpHost, paramHttpRequest, paramHttpCacheEntry);
    } else {
      storeNonVariantEntry(paramHttpHost, paramHttpRequest, paramHttpCacheEntry);
    }
  }
  
  void storeNonVariantEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    paramHttpHost = this.uriExtractor.getURI(paramHttpHost, paramHttpRequest);
    this.storage.putEntry(paramHttpHost, paramHttpCacheEntry);
  }
  
  void storeVariantEntry(final HttpHost paramHttpHost, final HttpRequest paramHttpRequest, final HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    String str = this.uriExtractor.getURI(paramHttpHost, paramHttpRequest);
    paramHttpHost = this.uriExtractor.getVariantURI(paramHttpHost, paramHttpRequest, paramHttpCacheEntry);
    this.storage.putEntry(paramHttpHost, paramHttpCacheEntry);
    paramHttpHost = new HttpCacheUpdateCallback()
    {
      public HttpCacheEntry update(HttpCacheEntry paramAnonymousHttpCacheEntry)
        throws IOException
      {
        return BasicHttpCache.this.doGetUpdatedParentEntry(paramHttpRequest.getRequestLine().getUri(), paramAnonymousHttpCacheEntry, paramHttpCacheEntry, BasicHttpCache.this.uriExtractor.getVariantKey(paramHttpRequest, paramHttpCacheEntry), paramHttpHost);
      }
    };
    try
    {
      this.storage.updateEntry(str, paramHttpHost);
    }
    catch (HttpCacheUpdateException paramHttpHost)
    {
      paramHttpCacheEntry = this.log;
      paramHttpRequest = new StringBuilder();
      paramHttpRequest.append("Could not update key [");
      paramHttpRequest.append(str);
      paramHttpRequest.append("]");
      paramHttpCacheEntry.warn(paramHttpRequest.toString(), paramHttpHost);
    }
  }
  
  public HttpCacheEntry updateCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException
  {
    paramHttpCacheEntry = this.cacheEntryUpdater.updateCacheEntry(paramHttpRequest.getRequestLine().getUri(), paramHttpCacheEntry, paramDate1, paramDate2, paramHttpResponse);
    storeInCache(paramHttpHost, paramHttpRequest, paramHttpCacheEntry);
    return paramHttpCacheEntry;
  }
  
  public HttpCacheEntry updateVariantCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2, String paramString)
    throws IOException
  {
    paramHttpHost = this.cacheEntryUpdater.updateCacheEntry(paramHttpRequest.getRequestLine().getUri(), paramHttpCacheEntry, paramDate1, paramDate2, paramHttpResponse);
    this.storage.putEntry(paramString, paramHttpHost);
    return paramHttpHost;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/BasicHttpCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */