package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.annotation.Immutable;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Immutable
class IOUtils
{
  static void closeSilently(Closeable paramCloseable)
  {
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException paramCloseable)
    {
      for (;;) {}
    }
  }
  
  static void consume(HttpEntity paramHttpEntity)
    throws IOException
  {
    if (paramHttpEntity == null) {
      return;
    }
    if (paramHttpEntity.isStreaming())
    {
      paramHttpEntity = paramHttpEntity.getContent();
      if (paramHttpEntity != null) {
        paramHttpEntity.close();
      }
    }
  }
  
  static void copy(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte['ࠀ'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1) {
        break;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  static void copyAndClose(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    try
    {
      copy(paramInputStream, paramOutputStream);
      paramInputStream.close();
      paramOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      closeSilently(paramInputStream);
      closeSilently(paramOutputStream);
      throw localIOException;
    }
  }
  
  /* Error */
  static void copyFile(java.io.File paramFile1, java.io.File paramFile2)
    throws IOException
  {
    // Byte code:
    //   0: new 57	java/io/RandomAccessFile
    //   3: dup
    //   4: aload_0
    //   5: ldc 59
    //   7: invokespecial 62	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   10: astore_0
    //   11: new 57	java/io/RandomAccessFile
    //   14: dup
    //   15: aload_1
    //   16: ldc 64
    //   18: invokespecial 62	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   21: astore_1
    //   22: aload_0
    //   23: invokevirtual 68	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   26: astore_3
    //   27: aload_1
    //   28: invokevirtual 68	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   31: astore_2
    //   32: aload_3
    //   33: lconst_0
    //   34: aload_0
    //   35: invokevirtual 72	java/io/RandomAccessFile:length	()J
    //   38: aload_2
    //   39: invokevirtual 78	java/nio/channels/FileChannel:transferTo	(JJLjava/nio/channels/WritableByteChannel;)J
    //   42: pop2
    //   43: aload_3
    //   44: invokevirtual 79	java/nio/channels/FileChannel:close	()V
    //   47: aload_2
    //   48: invokevirtual 79	java/nio/channels/FileChannel:close	()V
    //   51: aload_0
    //   52: invokevirtual 80	java/io/RandomAccessFile:close	()V
    //   55: aload_1
    //   56: invokevirtual 80	java/io/RandomAccessFile:close	()V
    //   59: return
    //   60: astore 4
    //   62: aload_3
    //   63: invokestatic 53	cz/msebera/android/httpclient/impl/client/cache/IOUtils:closeSilently	(Ljava/io/Closeable;)V
    //   66: aload_2
    //   67: invokestatic 53	cz/msebera/android/httpclient/impl/client/cache/IOUtils:closeSilently	(Ljava/io/Closeable;)V
    //   70: aload 4
    //   72: athrow
    //   73: astore_2
    //   74: aload_0
    //   75: invokestatic 53	cz/msebera/android/httpclient/impl/client/cache/IOUtils:closeSilently	(Ljava/io/Closeable;)V
    //   78: aload_1
    //   79: invokestatic 53	cz/msebera/android/httpclient/impl/client/cache/IOUtils:closeSilently	(Ljava/io/Closeable;)V
    //   82: aload_2
    //   83: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	paramFile1	java.io.File
    //   0	84	1	paramFile2	java.io.File
    //   31	36	2	localFileChannel1	java.nio.channels.FileChannel
    //   73	10	2	localIOException1	IOException
    //   26	37	3	localFileChannel2	java.nio.channels.FileChannel
    //   60	11	4	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   32	51	60	java/io/IOException
    //   22	32	73	java/io/IOException
    //   51	59	73	java/io/IOException
    //   62	73	73	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/IOUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */