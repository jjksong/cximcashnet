package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;

@Immutable
class CacheableRequestPolicy
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  public boolean isServableFromCache(HttpRequest paramHttpRequest)
  {
    Object localObject2 = paramHttpRequest.getRequestLine().getMethod();
    Object localObject1 = paramHttpRequest.getRequestLine().getProtocolVersion();
    if (HttpVersion.HTTP_1_1.compareToVersion((ProtocolVersion)localObject1) != 0)
    {
      this.log.trace("non-HTTP/1.1 request was not serveable from cache");
      return false;
    }
    if (!((String)localObject2).equals("GET"))
    {
      this.log.trace("non-GET request was not serveable from cache");
      return false;
    }
    if (paramHttpRequest.getHeaders("Pragma").length > 0)
    {
      this.log.trace("request with Pragma header was not serveable from cache");
      return false;
    }
    localObject1 = paramHttpRequest.getHeaders("Cache-Control");
    int k = localObject1.length;
    for (int i = 0; i < k; i++) {
      for (paramHttpRequest : localObject1[i].getElements())
      {
        if ("no-store".equalsIgnoreCase(paramHttpRequest.getName()))
        {
          this.log.trace("Request with no-store was not serveable from cache");
          return false;
        }
        if ("no-cache".equalsIgnoreCase(paramHttpRequest.getName()))
        {
          this.log.trace("Request with no-cache was not serveable from cache");
          return false;
        }
      }
    }
    this.log.trace("Request was serveable from cache");
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheableRequestPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */