package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.util.Date;

@Immutable
class CachedResponseSuitabilityChecker
{
  private final float heuristicCoefficient;
  private final long heuristicDefaultLifetime;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final boolean sharedCache;
  private final boolean useHeuristicCaching;
  private final CacheValidityPolicy validityStrategy;
  
  CachedResponseSuitabilityChecker(CacheConfig paramCacheConfig)
  {
    this(new CacheValidityPolicy(), paramCacheConfig);
  }
  
  CachedResponseSuitabilityChecker(CacheValidityPolicy paramCacheValidityPolicy, CacheConfig paramCacheConfig)
  {
    this.validityStrategy = paramCacheValidityPolicy;
    this.sharedCache = paramCacheConfig.isSharedCache();
    this.useHeuristicCaching = paramCacheConfig.isHeuristicCachingEnabled();
    this.heuristicCoefficient = paramCacheConfig.getHeuristicCoefficient();
    this.heuristicDefaultLifetime = paramCacheConfig.getHeuristicDefaultLifetime();
  }
  
  private boolean etagValidatorMatches(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("ETag");
    if (paramHttpCacheEntry != null) {
      paramHttpCacheEntry = paramHttpCacheEntry.getValue();
    } else {
      paramHttpCacheEntry = null;
    }
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("If-None-Match");
    if (arrayOfHeader != null)
    {
      int k = arrayOfHeader.length;
      for (int i = 0; i < k; i++)
      {
        HeaderElement[] arrayOfHeaderElement = arrayOfHeader[i].getElements();
        int m = arrayOfHeaderElement.length;
        for (int j = 0; j < m; j++)
        {
          paramHttpRequest = arrayOfHeaderElement[j].toString();
          if ((("*".equals(paramHttpRequest)) && (paramHttpCacheEntry != null)) || (paramHttpRequest.equals(paramHttpCacheEntry))) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private long getMaxStale(HttpRequest paramHttpRequest)
  {
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    long l1 = -1L;
    int i = 0;
    while (i < k)
    {
      paramHttpRequest = arrayOfHeader[i].getElements();
      int m = paramHttpRequest.length;
      int j = 0;
      for (long l2 = l1; j < m; l2 = l1)
      {
        Object localObject = paramHttpRequest[j];
        l1 = l2;
        if ("max-stale".equals(((HeaderElement)localObject).getName())) {
          if (((((HeaderElement)localObject).getValue() == null) || ("".equals(((HeaderElement)localObject).getValue().trim()))) && (l2 == -1L)) {
            l1 = Long.MAX_VALUE;
          } else {
            try
            {
              l1 = Long.parseLong(((HeaderElement)localObject).getValue());
              long l3 = l1;
              if (l1 < 0L) {
                l3 = 0L;
              }
              if (l2 != -1L)
              {
                l1 = l2;
                if (l3 >= l2) {}
              }
              else
              {
                l1 = l3;
              }
            }
            catch (NumberFormatException localNumberFormatException)
            {
              l1 = 0L;
            }
          }
        }
        j++;
      }
      i++;
      l1 = l2;
    }
    return l1;
  }
  
  private boolean hasSupportedEtagValidator(HttpRequest paramHttpRequest)
  {
    return paramHttpRequest.containsHeader("If-None-Match");
  }
  
  private boolean hasSupportedLastModifiedValidator(HttpRequest paramHttpRequest)
  {
    return hasValidDateField(paramHttpRequest, "If-Modified-Since");
  }
  
  private boolean hasUnsupportedConditionalHeaders(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if ((paramHttpRequest.getFirstHeader("If-Range") == null) && (paramHttpRequest.getFirstHeader("If-Match") == null) && (!hasValidDateField(paramHttpRequest, "If-Unmodified-Since"))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean hasValidDateField(HttpRequest paramHttpRequest, String paramString)
  {
    paramHttpRequest = paramHttpRequest.getHeaders(paramString);
    int i = paramHttpRequest.length;
    boolean bool = false;
    if (i > 0)
    {
      if (DateUtils.parseDate(paramHttpRequest[0].getValue()) != null) {
        bool = true;
      }
      return bool;
    }
    return false;
  }
  
  private boolean isFreshEnough(HttpCacheEntry paramHttpCacheEntry, HttpRequest paramHttpRequest, Date paramDate)
  {
    boolean bool2 = this.validityStrategy.isResponseFresh(paramHttpCacheEntry, paramDate);
    boolean bool1 = true;
    if (bool2) {
      return true;
    }
    if ((this.useHeuristicCaching) && (this.validityStrategy.isResponseHeuristicallyFresh(paramHttpCacheEntry, paramDate, this.heuristicCoefficient, this.heuristicDefaultLifetime))) {
      return true;
    }
    if (originInsistsOnFreshness(paramHttpCacheEntry)) {
      return false;
    }
    long l = getMaxStale(paramHttpRequest);
    if (l == -1L) {
      return false;
    }
    if (l <= this.validityStrategy.getStalenessSecs(paramHttpCacheEntry, paramDate)) {
      bool1 = false;
    }
    return bool1;
  }
  
  private boolean lastModifiedValidatorMatches(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Last-Modified");
    if (paramHttpCacheEntry != null) {
      paramHttpCacheEntry = DateUtils.parseDate(paramHttpCacheEntry.getValue());
    } else {
      paramHttpCacheEntry = null;
    }
    if (paramHttpCacheEntry == null) {
      return false;
    }
    paramHttpRequest = paramHttpRequest.getHeaders("If-Modified-Since");
    int j = paramHttpRequest.length;
    for (int i = 0; i < j; i++)
    {
      Date localDate = DateUtils.parseDate(paramHttpRequest[i].getValue());
      if ((localDate != null) && ((localDate.after(paramDate)) || (paramHttpCacheEntry.after(localDate)))) {
        return false;
      }
    }
    return true;
  }
  
  private boolean originInsistsOnFreshness(HttpCacheEntry paramHttpCacheEntry)
  {
    boolean bool1 = this.validityStrategy.mustRevalidate(paramHttpCacheEntry);
    boolean bool2 = true;
    if (bool1) {
      return true;
    }
    if (!this.sharedCache) {
      return false;
    }
    bool1 = bool2;
    if (!this.validityStrategy.proxyRevalidate(paramHttpCacheEntry)) {
      if (this.validityStrategy.hasCacheControlDirective(paramHttpCacheEntry, "s-maxage")) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  public boolean allConditionalsMatch(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    boolean bool2 = hasSupportedEtagValidator(paramHttpRequest);
    boolean bool1 = hasSupportedLastModifiedValidator(paramHttpRequest);
    int i;
    if ((bool2) && (etagValidatorMatches(paramHttpRequest, paramHttpCacheEntry))) {
      i = 1;
    } else {
      i = 0;
    }
    int j;
    if ((bool1) && (lastModifiedValidatorMatches(paramHttpRequest, paramHttpCacheEntry, paramDate))) {
      j = 1;
    } else {
      j = 0;
    }
    if ((bool2) && (bool1) && ((i == 0) || (j == 0))) {
      return false;
    }
    if ((bool2) && (i == 0)) {
      return false;
    }
    return (!bool1) || (j != 0);
  }
  
  public boolean canCachedResponseBeUsed(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    if (!isFreshEnough(paramHttpCacheEntry, paramHttpRequest, paramDate))
    {
      this.log.trace("Cache entry was not fresh enough");
      return false;
    }
    if (!this.validityStrategy.contentLengthHeaderMatchesActualLength(paramHttpCacheEntry))
    {
      this.log.debug("Cache entry Content-Length and header information do not match");
      return false;
    }
    if (hasUnsupportedConditionalHeaders(paramHttpRequest))
    {
      this.log.debug("Request contained conditional headers we don't handle");
      return false;
    }
    if ((!isConditional(paramHttpRequest)) && (paramHttpCacheEntry.getStatusCode() == 304)) {
      return false;
    }
    if ((isConditional(paramHttpRequest)) && (!allConditionalsMatch(paramHttpRequest, paramHttpCacheEntry, paramDate))) {
      return false;
    }
    paramHttpHost = paramHttpRequest.getHeaders("Cache-Control");
    int k = paramHttpHost.length;
    for (int i = 0; i < k; i++) {
      for (Object localObject : paramHttpHost[i].getElements())
      {
        if ("no-cache".equals(((HeaderElement)localObject).getName()))
        {
          this.log.trace("Response contained NO CACHE directive, cache was not suitable");
          return false;
        }
        if ("no-store".equals(((HeaderElement)localObject).getName()))
        {
          this.log.trace("Response contained NO STORE directive, cache was not suitable");
          return false;
        }
        int n;
        if ("max-age".equals(((HeaderElement)localObject).getName())) {
          try
          {
            n = Integer.parseInt(((HeaderElement)localObject).getValue());
            if (this.validityStrategy.getCurrentAgeSecs(paramHttpCacheEntry, paramDate) > n)
            {
              this.log.trace("Response from cache was NOT suitable due to max age");
              return false;
            }
          }
          catch (NumberFormatException paramHttpCacheEntry)
          {
            paramHttpRequest = this.log;
            paramHttpHost = new StringBuilder();
            paramHttpHost.append("Response from cache was malformed");
            paramHttpHost.append(paramHttpCacheEntry.getMessage());
            paramHttpRequest.debug(paramHttpHost.toString());
            return false;
          }
        }
        if ("max-stale".equals(((HeaderElement)localObject).getName())) {
          try
          {
            n = Integer.parseInt(((HeaderElement)localObject).getValue());
            if (this.validityStrategy.getFreshnessLifetimeSecs(paramHttpCacheEntry) > n)
            {
              this.log.trace("Response from cache was not suitable due to Max stale freshness");
              return false;
            }
          }
          catch (NumberFormatException paramHttpCacheEntry)
          {
            paramHttpHost = this.log;
            paramHttpRequest = new StringBuilder();
            paramHttpRequest.append("Response from cache was malformed: ");
            paramHttpRequest.append(paramHttpCacheEntry.getMessage());
            paramHttpHost.debug(paramHttpRequest.toString());
            return false;
          }
        }
        if ("min-fresh".equals(((HeaderElement)localObject).getName())) {
          try
          {
            long l1 = Long.parseLong(((HeaderElement)localObject).getValue());
            if (l1 < 0L) {
              return false;
            }
            long l2 = this.validityStrategy.getCurrentAgeSecs(paramHttpCacheEntry, paramDate);
            if (this.validityStrategy.getFreshnessLifetimeSecs(paramHttpCacheEntry) - l2 < l1)
            {
              this.log.trace("Response from cache was not suitable due to min fresh freshness requirement");
              return false;
            }
          }
          catch (NumberFormatException paramHttpCacheEntry)
          {
            paramHttpRequest = this.log;
            paramHttpHost = new StringBuilder();
            paramHttpHost.append("Response from cache was malformed: ");
            paramHttpHost.append(paramHttpCacheEntry.getMessage());
            paramHttpRequest.debug(paramHttpHost.toString());
            return false;
          }
        }
      }
    }
    this.log.trace("Response from cache was suitable");
    return true;
  }
  
  public boolean isConditional(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if ((!hasSupportedEtagValidator(paramHttpRequest)) && (!hasSupportedLastModifiedValidator(paramHttpRequest))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CachedResponseSuitabilityChecker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */