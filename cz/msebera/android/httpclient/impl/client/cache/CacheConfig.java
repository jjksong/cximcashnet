package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.util.Args;

public class CacheConfig
  implements Cloneable
{
  public static final CacheConfig DEFAULT = new Builder().build();
  public static final boolean DEFAULT_303_CACHING_ENABLED = false;
  public static final int DEFAULT_ASYNCHRONOUS_WORKERS_CORE = 1;
  public static final int DEFAULT_ASYNCHRONOUS_WORKERS_MAX = 1;
  public static final int DEFAULT_ASYNCHRONOUS_WORKER_IDLE_LIFETIME_SECS = 60;
  public static final boolean DEFAULT_HEURISTIC_CACHING_ENABLED = false;
  public static final float DEFAULT_HEURISTIC_COEFFICIENT = 0.1F;
  public static final long DEFAULT_HEURISTIC_LIFETIME = 0L;
  public static final int DEFAULT_MAX_CACHE_ENTRIES = 1000;
  public static final int DEFAULT_MAX_OBJECT_SIZE_BYTES = 8192;
  public static final int DEFAULT_MAX_UPDATE_RETRIES = 1;
  public static final int DEFAULT_REVALIDATION_QUEUE_SIZE = 100;
  public static final boolean DEFAULT_WEAK_ETAG_ON_PUTDELETE_ALLOWED = false;
  private boolean allow303Caching;
  private int asynchronousWorkerIdleLifetimeSecs;
  private int asynchronousWorkersCore;
  private int asynchronousWorkersMax;
  private boolean heuristicCachingEnabled;
  private float heuristicCoefficient;
  private long heuristicDefaultLifetime;
  private boolean isSharedCache;
  private int maxCacheEntries;
  private long maxObjectSize;
  private int maxUpdateRetries;
  private boolean neverCacheHTTP10ResponsesWithQuery;
  private int revalidationQueueSize;
  private boolean weakETagOnPutDeleteAllowed;
  
  @Deprecated
  public CacheConfig()
  {
    this.maxObjectSize = 8192L;
    this.maxCacheEntries = 1000;
    this.maxUpdateRetries = 1;
    this.allow303Caching = false;
    this.weakETagOnPutDeleteAllowed = false;
    this.heuristicCachingEnabled = false;
    this.heuristicCoefficient = 0.1F;
    this.heuristicDefaultLifetime = 0L;
    this.isSharedCache = true;
    this.asynchronousWorkersMax = 1;
    this.asynchronousWorkersCore = 1;
    this.asynchronousWorkerIdleLifetimeSecs = 60;
    this.revalidationQueueSize = 100;
  }
  
  CacheConfig(long paramLong1, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, float paramFloat, long paramLong2, boolean paramBoolean4, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean5)
  {
    this.maxObjectSize = paramLong1;
    this.maxCacheEntries = paramInt1;
    this.maxUpdateRetries = paramInt2;
    this.allow303Caching = paramBoolean1;
    this.weakETagOnPutDeleteAllowed = paramBoolean2;
    this.heuristicCachingEnabled = paramBoolean3;
    this.heuristicCoefficient = paramFloat;
    this.heuristicDefaultLifetime = paramLong2;
    this.isSharedCache = paramBoolean4;
    this.asynchronousWorkersMax = paramInt3;
    this.asynchronousWorkersCore = paramInt4;
    this.asynchronousWorkerIdleLifetimeSecs = paramInt5;
    this.revalidationQueueSize = paramInt6;
  }
  
  public static Builder copy(CacheConfig paramCacheConfig)
  {
    Args.notNull(paramCacheConfig, "Cache config");
    return new Builder().setMaxObjectSize(paramCacheConfig.getMaxObjectSize()).setMaxCacheEntries(paramCacheConfig.getMaxCacheEntries()).setMaxUpdateRetries(paramCacheConfig.getMaxUpdateRetries()).setHeuristicCachingEnabled(paramCacheConfig.isHeuristicCachingEnabled()).setHeuristicCoefficient(paramCacheConfig.getHeuristicCoefficient()).setHeuristicDefaultLifetime(paramCacheConfig.getHeuristicDefaultLifetime()).setSharedCache(paramCacheConfig.isSharedCache()).setAsynchronousWorkersMax(paramCacheConfig.getAsynchronousWorkersMax()).setAsynchronousWorkersCore(paramCacheConfig.getAsynchronousWorkersCore()).setAsynchronousWorkerIdleLifetimeSecs(paramCacheConfig.getAsynchronousWorkerIdleLifetimeSecs()).setRevalidationQueueSize(paramCacheConfig.getRevalidationQueueSize()).setNeverCacheHTTP10ResponsesWithQueryString(paramCacheConfig.isNeverCacheHTTP10ResponsesWithQuery());
  }
  
  public static Builder custom()
  {
    return new Builder();
  }
  
  protected CacheConfig clone()
    throws CloneNotSupportedException
  {
    return (CacheConfig)super.clone();
  }
  
  public int getAsynchronousWorkerIdleLifetimeSecs()
  {
    return this.asynchronousWorkerIdleLifetimeSecs;
  }
  
  public int getAsynchronousWorkersCore()
  {
    return this.asynchronousWorkersCore;
  }
  
  public int getAsynchronousWorkersMax()
  {
    return this.asynchronousWorkersMax;
  }
  
  public float getHeuristicCoefficient()
  {
    return this.heuristicCoefficient;
  }
  
  public long getHeuristicDefaultLifetime()
  {
    return this.heuristicDefaultLifetime;
  }
  
  public int getMaxCacheEntries()
  {
    return this.maxCacheEntries;
  }
  
  public long getMaxObjectSize()
  {
    return this.maxObjectSize;
  }
  
  @Deprecated
  public int getMaxObjectSizeBytes()
  {
    long l = this.maxObjectSize;
    int i;
    if (l > 2147483647L) {
      i = Integer.MAX_VALUE;
    } else {
      i = (int)l;
    }
    return i;
  }
  
  public int getMaxUpdateRetries()
  {
    return this.maxUpdateRetries;
  }
  
  public int getRevalidationQueueSize()
  {
    return this.revalidationQueueSize;
  }
  
  public boolean is303CachingEnabled()
  {
    return this.allow303Caching;
  }
  
  public boolean isHeuristicCachingEnabled()
  {
    return this.heuristicCachingEnabled;
  }
  
  public boolean isNeverCacheHTTP10ResponsesWithQuery()
  {
    return this.neverCacheHTTP10ResponsesWithQuery;
  }
  
  public boolean isSharedCache()
  {
    return this.isSharedCache;
  }
  
  public boolean isWeakETagOnPutDeleteAllowed()
  {
    return this.weakETagOnPutDeleteAllowed;
  }
  
  @Deprecated
  public void setAsynchronousWorkerIdleLifetimeSecs(int paramInt)
  {
    this.asynchronousWorkerIdleLifetimeSecs = paramInt;
  }
  
  @Deprecated
  public void setAsynchronousWorkersCore(int paramInt)
  {
    this.asynchronousWorkersCore = paramInt;
  }
  
  @Deprecated
  public void setAsynchronousWorkersMax(int paramInt)
  {
    this.asynchronousWorkersMax = paramInt;
  }
  
  @Deprecated
  public void setHeuristicCachingEnabled(boolean paramBoolean)
  {
    this.heuristicCachingEnabled = paramBoolean;
  }
  
  @Deprecated
  public void setHeuristicCoefficient(float paramFloat)
  {
    this.heuristicCoefficient = paramFloat;
  }
  
  @Deprecated
  public void setHeuristicDefaultLifetime(long paramLong)
  {
    this.heuristicDefaultLifetime = paramLong;
  }
  
  @Deprecated
  public void setMaxCacheEntries(int paramInt)
  {
    this.maxCacheEntries = paramInt;
  }
  
  @Deprecated
  public void setMaxObjectSize(long paramLong)
  {
    this.maxObjectSize = paramLong;
  }
  
  @Deprecated
  public void setMaxObjectSizeBytes(int paramInt)
  {
    if (paramInt > Integer.MAX_VALUE) {
      this.maxObjectSize = 2147483647L;
    } else {
      this.maxObjectSize = paramInt;
    }
  }
  
  @Deprecated
  public void setMaxUpdateRetries(int paramInt)
  {
    this.maxUpdateRetries = paramInt;
  }
  
  @Deprecated
  public void setRevalidationQueueSize(int paramInt)
  {
    this.revalidationQueueSize = paramInt;
  }
  
  @Deprecated
  public void setSharedCache(boolean paramBoolean)
  {
    this.isSharedCache = paramBoolean;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[maxObjectSize=");
    localStringBuilder.append(this.maxObjectSize);
    localStringBuilder.append(", maxCacheEntries=");
    localStringBuilder.append(this.maxCacheEntries);
    localStringBuilder.append(", maxUpdateRetries=");
    localStringBuilder.append(this.maxUpdateRetries);
    localStringBuilder.append(", 303CachingEnabled=");
    localStringBuilder.append(this.allow303Caching);
    localStringBuilder.append(", weakETagOnPutDeleteAllowed=");
    localStringBuilder.append(this.weakETagOnPutDeleteAllowed);
    localStringBuilder.append(", heuristicCachingEnabled=");
    localStringBuilder.append(this.heuristicCachingEnabled);
    localStringBuilder.append(", heuristicCoefficient=");
    localStringBuilder.append(this.heuristicCoefficient);
    localStringBuilder.append(", heuristicDefaultLifetime=");
    localStringBuilder.append(this.heuristicDefaultLifetime);
    localStringBuilder.append(", isSharedCache=");
    localStringBuilder.append(this.isSharedCache);
    localStringBuilder.append(", asynchronousWorkersMax=");
    localStringBuilder.append(this.asynchronousWorkersMax);
    localStringBuilder.append(", asynchronousWorkersCore=");
    localStringBuilder.append(this.asynchronousWorkersCore);
    localStringBuilder.append(", asynchronousWorkerIdleLifetimeSecs=");
    localStringBuilder.append(this.asynchronousWorkerIdleLifetimeSecs);
    localStringBuilder.append(", revalidationQueueSize=");
    localStringBuilder.append(this.revalidationQueueSize);
    localStringBuilder.append(", neverCacheHTTP10ResponsesWithQuery=");
    localStringBuilder.append(this.neverCacheHTTP10ResponsesWithQuery);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public static class Builder
  {
    private boolean allow303Caching = false;
    private int asynchronousWorkerIdleLifetimeSecs = 60;
    private int asynchronousWorkersCore = 1;
    private int asynchronousWorkersMax = 1;
    private boolean heuristicCachingEnabled = false;
    private float heuristicCoefficient = 0.1F;
    private long heuristicDefaultLifetime = 0L;
    private boolean isSharedCache = true;
    private int maxCacheEntries = 1000;
    private long maxObjectSize = 8192L;
    private int maxUpdateRetries = 1;
    private boolean neverCacheHTTP10ResponsesWithQuery;
    private int revalidationQueueSize = 100;
    private boolean weakETagOnPutDeleteAllowed = false;
    
    public CacheConfig build()
    {
      return new CacheConfig(this.maxObjectSize, this.maxCacheEntries, this.maxUpdateRetries, this.allow303Caching, this.weakETagOnPutDeleteAllowed, this.heuristicCachingEnabled, this.heuristicCoefficient, this.heuristicDefaultLifetime, this.isSharedCache, this.asynchronousWorkersMax, this.asynchronousWorkersCore, this.asynchronousWorkerIdleLifetimeSecs, this.revalidationQueueSize, this.neverCacheHTTP10ResponsesWithQuery);
    }
    
    public Builder setAllow303Caching(boolean paramBoolean)
    {
      this.allow303Caching = paramBoolean;
      return this;
    }
    
    public Builder setAsynchronousWorkerIdleLifetimeSecs(int paramInt)
    {
      this.asynchronousWorkerIdleLifetimeSecs = paramInt;
      return this;
    }
    
    public Builder setAsynchronousWorkersCore(int paramInt)
    {
      this.asynchronousWorkersCore = paramInt;
      return this;
    }
    
    public Builder setAsynchronousWorkersMax(int paramInt)
    {
      this.asynchronousWorkersMax = paramInt;
      return this;
    }
    
    public Builder setHeuristicCachingEnabled(boolean paramBoolean)
    {
      this.heuristicCachingEnabled = paramBoolean;
      return this;
    }
    
    public Builder setHeuristicCoefficient(float paramFloat)
    {
      this.heuristicCoefficient = paramFloat;
      return this;
    }
    
    public Builder setHeuristicDefaultLifetime(long paramLong)
    {
      this.heuristicDefaultLifetime = paramLong;
      return this;
    }
    
    public Builder setMaxCacheEntries(int paramInt)
    {
      this.maxCacheEntries = paramInt;
      return this;
    }
    
    public Builder setMaxObjectSize(long paramLong)
    {
      this.maxObjectSize = paramLong;
      return this;
    }
    
    public Builder setMaxUpdateRetries(int paramInt)
    {
      this.maxUpdateRetries = paramInt;
      return this;
    }
    
    public Builder setNeverCacheHTTP10ResponsesWithQueryString(boolean paramBoolean)
    {
      this.neverCacheHTTP10ResponsesWithQuery = paramBoolean;
      return this;
    }
    
    public Builder setRevalidationQueueSize(int paramInt)
    {
      this.revalidationQueueSize = paramInt;
      return this;
    }
    
    public Builder setSharedCache(boolean paramBoolean)
    {
      this.isSharedCache = paramBoolean;
      return this;
    }
    
    public Builder setWeakETagOnPutDeleteAllowed(boolean paramBoolean)
    {
      this.weakETagOnPutDeleteAllowed = paramBoolean;
      return this;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */