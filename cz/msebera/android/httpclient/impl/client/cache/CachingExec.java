package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.cache.CacheResponseStatus;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.execchain.ClientExecChain;
import cz.msebera.android.httpclient.message.BasicHttpResponse;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.VersionInfo;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@ThreadSafe
public class CachingExec
  implements ClientExecChain
{
  private static final boolean SUPPORTS_RANGE_AND_CONTENT_RANGE_HEADERS = false;
  private final AsynchronousValidator asynchRevalidator;
  private final ClientExecChain backend;
  private final CacheConfig cacheConfig;
  private final AtomicLong cacheHits = new AtomicLong();
  private final AtomicLong cacheMisses = new AtomicLong();
  private final AtomicLong cacheUpdates = new AtomicLong();
  private final CacheableRequestPolicy cacheableRequestPolicy;
  private final ConditionalRequestBuilder conditionalRequestBuilder;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final RequestProtocolCompliance requestCompliance;
  private final HttpCache responseCache;
  private final ResponseCachingPolicy responseCachingPolicy;
  private final ResponseProtocolCompliance responseCompliance;
  private final CachedHttpResponseGenerator responseGenerator;
  private final CachedResponseSuitabilityChecker suitabilityChecker;
  private final CacheValidityPolicy validityPolicy;
  private final Map<ProtocolVersion, String> viaHeaders = new HashMap(4);
  
  public CachingExec(ClientExecChain paramClientExecChain)
  {
    this(paramClientExecChain, new BasicHttpCache(), CacheConfig.DEFAULT);
  }
  
  public CachingExec(ClientExecChain paramClientExecChain, ResourceFactory paramResourceFactory, HttpCacheStorage paramHttpCacheStorage, CacheConfig paramCacheConfig)
  {
    this(paramClientExecChain, new BasicHttpCache(paramResourceFactory, paramHttpCacheStorage, paramCacheConfig), paramCacheConfig);
  }
  
  public CachingExec(ClientExecChain paramClientExecChain, HttpCache paramHttpCache, CacheConfig paramCacheConfig)
  {
    this(paramClientExecChain, paramHttpCache, paramCacheConfig, null);
  }
  
  public CachingExec(ClientExecChain paramClientExecChain, HttpCache paramHttpCache, CacheConfig paramCacheConfig, AsynchronousValidator paramAsynchronousValidator)
  {
    Args.notNull(paramClientExecChain, "HTTP backend");
    Args.notNull(paramHttpCache, "HttpCache");
    if (paramCacheConfig == null) {
      paramCacheConfig = CacheConfig.DEFAULT;
    }
    this.cacheConfig = paramCacheConfig;
    this.backend = paramClientExecChain;
    this.responseCache = paramHttpCache;
    this.validityPolicy = new CacheValidityPolicy();
    this.responseGenerator = new CachedHttpResponseGenerator(this.validityPolicy);
    this.cacheableRequestPolicy = new CacheableRequestPolicy();
    this.suitabilityChecker = new CachedResponseSuitabilityChecker(this.validityPolicy, this.cacheConfig);
    this.conditionalRequestBuilder = new ConditionalRequestBuilder();
    this.responseCompliance = new ResponseProtocolCompliance();
    this.requestCompliance = new RequestProtocolCompliance(this.cacheConfig.isWeakETagOnPutDeleteAllowed());
    this.responseCachingPolicy = new ResponseCachingPolicy(this.cacheConfig.getMaxObjectSize(), this.cacheConfig.isSharedCache(), this.cacheConfig.isNeverCacheHTTP10ResponsesWithQuery(), this.cacheConfig.is303CachingEnabled());
    this.asynchRevalidator = paramAsynchronousValidator;
  }
  
  CachingExec(ClientExecChain paramClientExecChain, HttpCache paramHttpCache, CacheValidityPolicy paramCacheValidityPolicy, ResponseCachingPolicy paramResponseCachingPolicy, CachedHttpResponseGenerator paramCachedHttpResponseGenerator, CacheableRequestPolicy paramCacheableRequestPolicy, CachedResponseSuitabilityChecker paramCachedResponseSuitabilityChecker, ConditionalRequestBuilder paramConditionalRequestBuilder, ResponseProtocolCompliance paramResponseProtocolCompliance, RequestProtocolCompliance paramRequestProtocolCompliance, CacheConfig paramCacheConfig, AsynchronousValidator paramAsynchronousValidator)
  {
    if (paramCacheConfig == null) {
      paramCacheConfig = CacheConfig.DEFAULT;
    }
    this.cacheConfig = paramCacheConfig;
    this.backend = paramClientExecChain;
    this.responseCache = paramHttpCache;
    this.validityPolicy = paramCacheValidityPolicy;
    this.responseCachingPolicy = paramResponseCachingPolicy;
    this.responseGenerator = paramCachedHttpResponseGenerator;
    this.cacheableRequestPolicy = paramCacheableRequestPolicy;
    this.suitabilityChecker = paramCachedResponseSuitabilityChecker;
    this.conditionalRequestBuilder = paramConditionalRequestBuilder;
    this.responseCompliance = paramResponseProtocolCompliance;
    this.requestCompliance = paramRequestProtocolCompliance;
    this.asynchRevalidator = paramAsynchronousValidator;
  }
  
  private boolean alreadyHaveNewerCacheEntry(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper, HttpResponse paramHttpResponse)
  {
    try
    {
      paramHttpHost = this.responseCache.getCacheEntry(paramHttpHost, paramHttpRequestWrapper);
    }
    catch (IOException paramHttpHost)
    {
      paramHttpHost = null;
    }
    if (paramHttpHost == null) {
      return false;
    }
    paramHttpRequestWrapper = paramHttpHost.getFirstHeader("Date");
    if (paramHttpRequestWrapper == null) {
      return false;
    }
    paramHttpHost = paramHttpResponse.getFirstHeader("Date");
    if (paramHttpHost == null) {
      return false;
    }
    paramHttpRequestWrapper = DateUtils.parseDate(paramHttpRequestWrapper.getValue());
    paramHttpHost = DateUtils.parseDate(paramHttpHost.getValue());
    if ((paramHttpRequestWrapper != null) && (paramHttpHost != null)) {
      return paramHttpHost.before(paramHttpRequestWrapper);
    }
    return false;
  }
  
  private boolean explicitFreshnessRequest(HttpRequestWrapper paramHttpRequestWrapper, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    Header[] arrayOfHeader = paramHttpRequestWrapper.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    for (int i = 0; i < k; i++)
    {
      HeaderElement[] arrayOfHeaderElement = arrayOfHeader[i].getElements();
      int m = arrayOfHeaderElement.length;
      int j = 0;
      while (j < m)
      {
        paramHttpRequestWrapper = arrayOfHeaderElement[j];
        if ("max-stale".equals(paramHttpRequestWrapper.getName())) {
          try
          {
            int n = Integer.parseInt(paramHttpRequestWrapper.getValue());
            long l1 = this.validityPolicy.getCurrentAgeSecs(paramHttpCacheEntry, paramDate);
            long l2 = this.validityPolicy.getFreshnessLifetimeSecs(paramHttpCacheEntry);
            if (l1 - l2 <= n) {
              break label152;
            }
            return true;
          }
          catch (NumberFormatException paramHttpRequestWrapper)
          {
            return true;
          }
        } else {
          if (("min-fresh".equals(paramHttpRequestWrapper.getName())) || ("max-age".equals(paramHttpRequestWrapper.getName()))) {
            break label158;
          }
        }
        label152:
        j++;
        continue;
        label158:
        return true;
      }
    }
    return false;
  }
  
  private void flushEntriesInvalidatedByRequest(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper)
  {
    try
    {
      this.responseCache.flushInvalidatedCacheEntriesFor(paramHttpHost, paramHttpRequestWrapper);
    }
    catch (IOException paramHttpHost)
    {
      this.log.warn("Unable to flush invalidated entries from cache", paramHttpHost);
    }
  }
  
  private CloseableHttpResponse generateCachedResponse(HttpRequestWrapper paramHttpRequestWrapper, HttpContext paramHttpContext, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    if ((!paramHttpRequestWrapper.containsHeader("If-None-Match")) && (!paramHttpRequestWrapper.containsHeader("If-Modified-Since"))) {
      paramHttpRequestWrapper = this.responseGenerator.generateResponse(paramHttpCacheEntry);
    } else {
      paramHttpRequestWrapper = this.responseGenerator.generateNotModifiedResponse(paramHttpCacheEntry);
    }
    setResponseStatus(paramHttpContext, CacheResponseStatus.CACHE_HIT);
    if (this.validityPolicy.getStalenessSecs(paramHttpCacheEntry, paramDate) > 0L) {
      paramHttpRequestWrapper.addHeader("Warning", "110 localhost \"Response is stale\"");
    }
    return paramHttpRequestWrapper;
  }
  
  private CloseableHttpResponse generateGatewayTimeout(HttpContext paramHttpContext)
  {
    setResponseStatus(paramHttpContext, CacheResponseStatus.CACHE_MODULE_RESPONSE);
    return Proxies.enhanceResponse(new BasicHttpResponse(HttpVersion.HTTP_1_1, 504, "Gateway Timeout"));
  }
  
  private String generateViaHeader(HttpMessage paramHttpMessage)
  {
    ProtocolVersion localProtocolVersion = paramHttpMessage.getProtocolVersion();
    paramHttpMessage = (String)this.viaHeaders.get(localProtocolVersion);
    if (paramHttpMessage != null) {
      return paramHttpMessage;
    }
    paramHttpMessage = VersionInfo.loadVersionInfo("cz.msebera.android.httpclient.client", getClass().getClassLoader());
    if (paramHttpMessage != null) {
      paramHttpMessage = paramHttpMessage.getRelease();
    } else {
      paramHttpMessage = "UNAVAILABLE";
    }
    if ("http".equalsIgnoreCase(localProtocolVersion.getProtocol())) {
      paramHttpMessage = String.format("%d.%d localhost (Apache-HttpClient/%s (cache))", new Object[] { Integer.valueOf(localProtocolVersion.getMajor()), Integer.valueOf(localProtocolVersion.getMinor()), paramHttpMessage });
    } else {
      paramHttpMessage = String.format("%s/%d.%d localhost (Apache-HttpClient/%s (cache))", new Object[] { localProtocolVersion.getProtocol(), Integer.valueOf(localProtocolVersion.getMajor()), Integer.valueOf(localProtocolVersion.getMinor()), paramHttpMessage });
    }
    this.viaHeaders.put(localProtocolVersion, paramHttpMessage);
    return paramHttpMessage;
  }
  
  private Map<String, Variant> getExistingCacheVariants(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper)
  {
    try
    {
      paramHttpHost = this.responseCache.getVariantCacheEntriesWithEtags(paramHttpHost, paramHttpRequestWrapper);
    }
    catch (IOException paramHttpHost)
    {
      this.log.warn("Unable to retrieve variant entries from cache", paramHttpHost);
      paramHttpHost = null;
    }
    return paramHttpHost;
  }
  
  private HttpResponse getFatallyNoncompliantResponse(HttpRequestWrapper paramHttpRequestWrapper, HttpContext paramHttpContext)
  {
    Iterator localIterator = this.requestCompliance.requestIsFatallyNonCompliant(paramHttpRequestWrapper).iterator();
    for (paramHttpRequestWrapper = null; localIterator.hasNext(); paramHttpRequestWrapper = this.requestCompliance.getErrorForRequest(paramHttpRequestWrapper))
    {
      paramHttpRequestWrapper = (RequestProtocolError)localIterator.next();
      setResponseStatus(paramHttpContext, CacheResponseStatus.CACHE_MODULE_RESPONSE);
    }
    return paramHttpRequestWrapper;
  }
  
  /* Error */
  private HttpCacheEntry getUpdatedVariantEntry(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper, Date paramDate1, Date paramDate2, CloseableHttpResponse paramCloseableHttpResponse, Variant paramVariant, HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 110	cz/msebera/android/httpclient/impl/client/cache/CachingExec:responseCache	Lcz/msebera/android/httpclient/impl/client/cache/HttpCache;
    //   4: aload_1
    //   5: aload_2
    //   6: aload 7
    //   8: aload 5
    //   10: aload_3
    //   11: aload 4
    //   13: aload 6
    //   15: invokevirtual 451	cz/msebera/android/httpclient/impl/client/cache/Variant:getCacheKey	()Ljava/lang/String;
    //   18: invokeinterface 455 8 0
    //   23: astore_1
    //   24: aload_1
    //   25: astore 7
    //   27: aload 5
    //   29: invokeinterface 458 1 0
    //   34: goto +22 -> 56
    //   37: astore_1
    //   38: goto +21 -> 59
    //   41: astore_1
    //   42: aload_0
    //   43: getfield 94	cz/msebera/android/httpclient/impl/client/cache/CachingExec:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   46: ldc_w 460
    //   49: aload_1
    //   50: invokevirtual 277	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:warn	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   53: goto -26 -> 27
    //   56: aload 7
    //   58: areturn
    //   59: aload 5
    //   61: invokeinterface 458 1 0
    //   66: aload_1
    //   67: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	68	0	this	CachingExec
    //   0	68	1	paramHttpHost	HttpHost
    //   0	68	2	paramHttpRequestWrapper	HttpRequestWrapper
    //   0	68	3	paramDate1	Date
    //   0	68	4	paramDate2	Date
    //   0	68	5	paramCloseableHttpResponse	CloseableHttpResponse
    //   0	68	6	paramVariant	Variant
    //   0	68	7	paramHttpCacheEntry	HttpCacheEntry
    // Exception table:
    //   from	to	target	type
    //   0	24	37	finally
    //   42	53	37	finally
    //   0	24	41	java/io/IOException
  }
  
  private CloseableHttpResponse handleCacheHit(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry)
    throws IOException, HttpException
  {
    HttpHost localHttpHost = paramHttpClientContext.getTargetHost();
    recordCacheHit(localHttpHost, paramHttpRequestWrapper);
    Date localDate = getCurrentDate();
    if (this.suitabilityChecker.canCachedResponseBeUsed(localHttpHost, paramHttpRequestWrapper, paramHttpCacheEntry, localDate))
    {
      this.log.debug("Cache hit");
      paramHttpExecutionAware = generateCachedResponse(paramHttpRequestWrapper, paramHttpClientContext, paramHttpCacheEntry, localDate);
    }
    else
    {
      if (mayCallBackend(paramHttpRequestWrapper)) {
        break label133;
      }
      this.log.debug("Cache entry not suitable but only-if-cached requested");
      paramHttpExecutionAware = generateGatewayTimeout(paramHttpClientContext);
    }
    paramHttpClientContext.setAttribute("http.route", paramHttpRoute);
    paramHttpClientContext.setAttribute("http.target_host", localHttpHost);
    paramHttpClientContext.setAttribute("http.request", paramHttpRequestWrapper);
    paramHttpClientContext.setAttribute("http.response", paramHttpExecutionAware);
    paramHttpClientContext.setAttribute("http.request_sent", Boolean.TRUE);
    return paramHttpExecutionAware;
    label133:
    if ((paramHttpCacheEntry.getStatusCode() == 304) && (!this.suitabilityChecker.isConditional(paramHttpRequestWrapper)))
    {
      this.log.debug("Cache entry not usable; calling backend");
      return callBackend(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    }
    this.log.debug("Revalidating cache entry");
    return revalidateCacheEntry(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, paramHttpCacheEntry, localDate);
  }
  
  private CloseableHttpResponse handleCacheMiss(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Object localObject = paramHttpClientContext.getTargetHost();
    recordCacheMiss((HttpHost)localObject, paramHttpRequestWrapper);
    if (!mayCallBackend(paramHttpRequestWrapper)) {
      return Proxies.enhanceResponse(new BasicHttpResponse(HttpVersion.HTTP_1_1, 504, "Gateway Timeout"));
    }
    localObject = getExistingCacheVariants((HttpHost)localObject, paramHttpRequestWrapper);
    if ((localObject != null) && (((Map)localObject).size() > 0)) {
      return negotiateResponseFromVariants(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, (Map)localObject);
    }
    return callBackend(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
  }
  
  private CloseableHttpResponse handleRevalidationFailure(HttpRequestWrapper paramHttpRequestWrapper, HttpContext paramHttpContext, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    if (staleResponseNotAllowed(paramHttpRequestWrapper, paramHttpCacheEntry, paramDate)) {
      return generateGatewayTimeout(paramHttpContext);
    }
    return unvalidatedCacheHit(paramHttpContext, paramHttpCacheEntry);
  }
  
  private boolean mayCallBackend(HttpRequestWrapper paramHttpRequestWrapper)
  {
    paramHttpRequestWrapper = paramHttpRequestWrapper.getHeaders("Cache-Control");
    int k = paramHttpRequestWrapper.length;
    for (int i = 0; i < k; i++)
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpRequestWrapper[i].getElements();
      int m = arrayOfHeaderElement.length;
      for (int j = 0; j < m; j++) {
        if ("only-if-cached".equals(arrayOfHeaderElement[j].getName()))
        {
          this.log.trace("Request marked only-if-cached");
          return false;
        }
      }
    }
    return true;
  }
  
  private void recordCacheHit(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper)
  {
    this.cacheHits.getAndIncrement();
    if (this.log.isTraceEnabled())
    {
      RequestLine localRequestLine = paramHttpRequestWrapper.getRequestLine();
      paramHttpRequestWrapper = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Cache hit [host: ");
      localStringBuilder.append(paramHttpHost);
      localStringBuilder.append("; uri: ");
      localStringBuilder.append(localRequestLine.getUri());
      localStringBuilder.append("]");
      paramHttpRequestWrapper.trace(localStringBuilder.toString());
    }
  }
  
  private void recordCacheMiss(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper)
  {
    this.cacheMisses.getAndIncrement();
    if (this.log.isTraceEnabled())
    {
      RequestLine localRequestLine = paramHttpRequestWrapper.getRequestLine();
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      paramHttpRequestWrapper = new StringBuilder();
      paramHttpRequestWrapper.append("Cache miss [host: ");
      paramHttpRequestWrapper.append(paramHttpHost);
      paramHttpRequestWrapper.append("; uri: ");
      paramHttpRequestWrapper.append(localRequestLine.getUri());
      paramHttpRequestWrapper.append("]");
      localHttpClientAndroidLog.trace(paramHttpRequestWrapper.toString());
    }
  }
  
  private void recordCacheUpdate(HttpContext paramHttpContext)
  {
    this.cacheUpdates.getAndIncrement();
    setResponseStatus(paramHttpContext, CacheResponseStatus.VALIDATED);
  }
  
  private CloseableHttpResponse retryRequestUnconditionally(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry)
    throws IOException, HttpException
  {
    return callBackend(paramHttpRoute, this.conditionalRequestBuilder.buildUnconditionalRequest(paramHttpRequestWrapper, paramHttpCacheEntry), paramHttpClientContext, paramHttpExecutionAware);
  }
  
  private CloseableHttpResponse revalidateCacheEntry(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
    throws HttpException
  {
    try
    {
      if ((this.asynchRevalidator != null) && (!staleResponseNotAllowed(paramHttpRequestWrapper, paramHttpCacheEntry, paramDate)) && (this.validityPolicy.mayReturnStaleWhileRevalidating(paramHttpCacheEntry, paramDate)))
      {
        this.log.trace("Serving stale with asynchronous revalidation");
        CloseableHttpResponse localCloseableHttpResponse = generateCachedResponse(paramHttpRequestWrapper, paramHttpClientContext, paramHttpCacheEntry, paramDate);
        this.asynchRevalidator.revalidateCacheEntry(this, paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, paramHttpCacheEntry);
        return localCloseableHttpResponse;
      }
      paramHttpRoute = revalidateCacheEntry(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, paramHttpCacheEntry);
      return paramHttpRoute;
    }
    catch (IOException paramHttpRoute) {}
    return handleRevalidationFailure(paramHttpRequestWrapper, paramHttpClientContext, paramHttpCacheEntry, paramDate);
  }
  
  private boolean revalidationResponseIsTooOld(HttpResponse paramHttpResponse, HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Date");
    Header localHeader = paramHttpResponse.getFirstHeader("Date");
    if ((paramHttpCacheEntry != null) && (localHeader != null))
    {
      paramHttpResponse = DateUtils.parseDate(paramHttpCacheEntry.getValue());
      paramHttpCacheEntry = DateUtils.parseDate(localHeader.getValue());
      if ((paramHttpResponse != null) && (paramHttpCacheEntry != null))
      {
        if (paramHttpCacheEntry.before(paramHttpResponse)) {
          return true;
        }
      }
      else {
        return false;
      }
    }
    return false;
  }
  
  private HttpCacheEntry satisfyFromCache(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper)
  {
    try
    {
      paramHttpHost = this.responseCache.getCacheEntry(paramHttpHost, paramHttpRequestWrapper);
    }
    catch (IOException paramHttpHost)
    {
      this.log.warn("Unable to retrieve entries from cache", paramHttpHost);
      paramHttpHost = null;
    }
    return paramHttpHost;
  }
  
  private void setResponseStatus(HttpContext paramHttpContext, CacheResponseStatus paramCacheResponseStatus)
  {
    if (paramHttpContext != null) {
      paramHttpContext.setAttribute("http.cache.response.status", paramCacheResponseStatus);
    }
  }
  
  private boolean shouldSendNotModifiedResponse(HttpRequestWrapper paramHttpRequestWrapper, HttpCacheEntry paramHttpCacheEntry)
  {
    boolean bool;
    if ((this.suitabilityChecker.isConditional(paramHttpRequestWrapper)) && (this.suitabilityChecker.allConditionalsMatch(paramHttpRequestWrapper, paramHttpCacheEntry, new Date()))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean staleIfErrorAppliesTo(int paramInt)
  {
    boolean bool;
    if ((paramInt != 500) && (paramInt != 502) && (paramInt != 503) && (paramInt != 504)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean staleResponseNotAllowed(HttpRequestWrapper paramHttpRequestWrapper, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    boolean bool;
    if ((!this.validityPolicy.mustRevalidate(paramHttpCacheEntry)) && ((!this.cacheConfig.isSharedCache()) || (!this.validityPolicy.proxyRevalidate(paramHttpCacheEntry))) && (!explicitFreshnessRequest(paramHttpRequestWrapper, paramHttpCacheEntry, paramDate))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private void storeRequestIfModifiedSinceFor304Response(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    if (paramHttpResponse.getStatusLine().getStatusCode() == 304)
    {
      paramHttpRequest = paramHttpRequest.getFirstHeader("If-Modified-Since");
      if (paramHttpRequest != null) {
        paramHttpResponse.addHeader("Last-Modified", paramHttpRequest.getValue());
      }
    }
  }
  
  private void tryToUpdateVariantMap(HttpHost paramHttpHost, HttpRequestWrapper paramHttpRequestWrapper, Variant paramVariant)
  {
    try
    {
      this.responseCache.reuseVariantEntryFor(paramHttpHost, paramHttpRequestWrapper, paramVariant);
    }
    catch (IOException paramHttpHost)
    {
      this.log.warn("Could not update cache entry to reuse variant", paramHttpHost);
    }
  }
  
  private CloseableHttpResponse unvalidatedCacheHit(HttpContext paramHttpContext, HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = this.responseGenerator.generateResponse(paramHttpCacheEntry);
    setResponseStatus(paramHttpContext, CacheResponseStatus.CACHE_HIT);
    paramHttpCacheEntry.addHeader("Warning", "111 localhost \"Revalidation failed\"");
    return paramHttpCacheEntry;
  }
  
  CloseableHttpResponse callBackend(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Date localDate = getCurrentDate();
    this.log.trace("Calling the backend");
    CloseableHttpResponse localCloseableHttpResponse = this.backend.execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    try
    {
      localCloseableHttpResponse.addHeader("Via", generateViaHeader(localCloseableHttpResponse));
      paramHttpRoute = handleBackendResponse(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, localDate, getCurrentDate(), localCloseableHttpResponse);
      return paramHttpRoute;
    }
    catch (RuntimeException paramHttpRoute)
    {
      localCloseableHttpResponse.close();
      throw paramHttpRoute;
    }
    catch (IOException paramHttpRoute)
    {
      localCloseableHttpResponse.close();
      throw paramHttpRoute;
    }
  }
  
  boolean clientRequestsOurOptions(HttpRequest paramHttpRequest)
  {
    RequestLine localRequestLine = paramHttpRequest.getRequestLine();
    if (!"OPTIONS".equals(localRequestLine.getMethod())) {
      return false;
    }
    if (!"*".equals(localRequestLine.getUri())) {
      return false;
    }
    return "0".equals(paramHttpRequest.getFirstHeader("Max-Forwards").getValue());
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper)
    throws IOException, HttpException
  {
    return execute(paramHttpRoute, paramHttpRequestWrapper, HttpClientContext.create(), null);
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext)
    throws IOException, HttpException
  {
    return execute(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, null);
  }
  
  public CloseableHttpResponse execute(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware)
    throws IOException, HttpException
  {
    Object localObject = paramHttpClientContext.getTargetHost();
    String str = generateViaHeader(paramHttpRequestWrapper.getOriginal());
    setResponseStatus(paramHttpClientContext, CacheResponseStatus.CACHE_MISS);
    if (clientRequestsOurOptions(paramHttpRequestWrapper))
    {
      setResponseStatus(paramHttpClientContext, CacheResponseStatus.CACHE_MODULE_RESPONSE);
      return Proxies.enhanceResponse(new OptionsHttp11Response());
    }
    HttpResponse localHttpResponse = getFatallyNoncompliantResponse(paramHttpRequestWrapper, paramHttpClientContext);
    if (localHttpResponse != null) {
      return Proxies.enhanceResponse(localHttpResponse);
    }
    this.requestCompliance.makeRequestCompliant(paramHttpRequestWrapper);
    paramHttpRequestWrapper.addHeader("Via", str);
    flushEntriesInvalidatedByRequest(paramHttpClientContext.getTargetHost(), paramHttpRequestWrapper);
    if (!this.cacheableRequestPolicy.isServableFromCache(paramHttpRequestWrapper))
    {
      this.log.debug("Request is not servable from cache");
      return callBackend(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    }
    localObject = satisfyFromCache((HttpHost)localObject, paramHttpRequestWrapper);
    if (localObject == null)
    {
      this.log.debug("Cache miss");
      return handleCacheMiss(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    }
    return handleCacheHit(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, (HttpCacheEntry)localObject);
  }
  
  public long getCacheHits()
  {
    return this.cacheHits.get();
  }
  
  public long getCacheMisses()
  {
    return this.cacheMisses.get();
  }
  
  public long getCacheUpdates()
  {
    return this.cacheUpdates.get();
  }
  
  Date getCurrentDate()
  {
    return new Date();
  }
  
  CloseableHttpResponse handleBackendResponse(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, Date paramDate1, Date paramDate2, CloseableHttpResponse paramCloseableHttpResponse)
    throws IOException
  {
    this.log.trace("Handling Backend response");
    this.responseCompliance.ensureProtocolCompliance(paramHttpRequestWrapper, paramCloseableHttpResponse);
    paramHttpRoute = paramHttpClientContext.getTargetHost();
    boolean bool = this.responseCachingPolicy.isResponseCacheable(paramHttpRequestWrapper, paramCloseableHttpResponse);
    this.responseCache.flushInvalidatedCacheEntriesFor(paramHttpRoute, paramHttpRequestWrapper, paramCloseableHttpResponse);
    if ((bool) && (!alreadyHaveNewerCacheEntry(paramHttpRoute, paramHttpRequestWrapper, paramCloseableHttpResponse)))
    {
      storeRequestIfModifiedSinceFor304Response(paramHttpRequestWrapper, paramCloseableHttpResponse);
      return this.responseCache.cacheAndReturnResponse(paramHttpRoute, paramHttpRequestWrapper, paramCloseableHttpResponse, paramDate1, paramDate2);
    }
    if (!bool) {
      try
      {
        this.responseCache.flushCacheEntriesFor(paramHttpRoute, paramHttpRequestWrapper);
      }
      catch (IOException paramHttpRoute)
      {
        this.log.warn("Unable to flush invalid cache entries", paramHttpRoute);
      }
    }
    return paramCloseableHttpResponse;
  }
  
  CloseableHttpResponse negotiateResponseFromVariants(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, Map<String, Variant> paramMap)
    throws IOException, HttpException
  {
    HttpRequestWrapper localHttpRequestWrapper = this.conditionalRequestBuilder.buildConditionalRequestFromVariants(paramHttpRequestWrapper, paramMap);
    Date localDate1 = getCurrentDate();
    CloseableHttpResponse localCloseableHttpResponse = this.backend.execute(paramHttpRoute, localHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    try
    {
      Date localDate2 = getCurrentDate();
      localCloseableHttpResponse.addHeader("Via", generateViaHeader(localCloseableHttpResponse));
      if (localCloseableHttpResponse.getStatusLine().getStatusCode() != 304) {
        return handleBackendResponse(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, localDate1, localDate2, localCloseableHttpResponse);
      }
      Object localObject = localCloseableHttpResponse.getFirstHeader("ETag");
      if (localObject == null)
      {
        this.log.warn("304 response did not contain ETag");
        IOUtils.consume(localCloseableHttpResponse.getEntity());
        localCloseableHttpResponse.close();
        return callBackend(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
      }
      paramMap = (Variant)paramMap.get(((Header)localObject).getValue());
      if (paramMap == null)
      {
        this.log.debug("304 response did not contain ETag matching one sent in If-None-Match");
        IOUtils.consume(localCloseableHttpResponse.getEntity());
        localCloseableHttpResponse.close();
        return callBackend(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
      }
      localObject = paramMap.getEntry();
      if (revalidationResponseIsTooOld(localCloseableHttpResponse, (HttpCacheEntry)localObject))
      {
        IOUtils.consume(localCloseableHttpResponse.getEntity());
        localCloseableHttpResponse.close();
        return retryRequestUnconditionally(paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, (HttpCacheEntry)localObject);
      }
      recordCacheUpdate(paramHttpClientContext);
      paramHttpExecutionAware = getUpdatedVariantEntry(paramHttpClientContext.getTargetHost(), localHttpRequestWrapper, localDate1, localDate2, localCloseableHttpResponse, paramMap, (HttpCacheEntry)localObject);
      localCloseableHttpResponse.close();
      paramHttpRoute = this.responseGenerator.generateResponse(paramHttpExecutionAware);
      tryToUpdateVariantMap(paramHttpClientContext.getTargetHost(), paramHttpRequestWrapper, paramMap);
      if (shouldSendNotModifiedResponse(paramHttpRequestWrapper, paramHttpExecutionAware))
      {
        paramHttpRoute = this.responseGenerator.generateNotModifiedResponse(paramHttpExecutionAware);
        return paramHttpRoute;
      }
      return paramHttpRoute;
    }
    catch (RuntimeException paramHttpRoute)
    {
      localCloseableHttpResponse.close();
      throw paramHttpRoute;
    }
    catch (IOException paramHttpRoute)
    {
      localCloseableHttpResponse.close();
      throw paramHttpRoute;
    }
  }
  
  CloseableHttpResponse revalidateCacheEntry(HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry)
    throws IOException, HttpException
  {
    HttpRequestWrapper localHttpRequestWrapper = this.conditionalRequestBuilder.buildConditionalRequest(paramHttpRequestWrapper, paramHttpCacheEntry);
    Object localObject1 = localHttpRequestWrapper.getURI();
    if (localObject1 != null) {
      try
      {
        localHttpRequestWrapper.setURI(InternalURIUtils.rewriteURIForRoute((URI)localObject1, paramHttpRoute));
      }
      catch (URISyntaxException paramHttpRequestWrapper)
      {
        paramHttpRoute = new StringBuilder();
        paramHttpRoute.append("Invalid URI: ");
        paramHttpRoute.append(localObject1);
        throw new ProtocolException(paramHttpRoute.toString(), paramHttpRequestWrapper);
      }
    }
    localObject1 = getCurrentDate();
    CloseableHttpResponse localCloseableHttpResponse = this.backend.execute(paramHttpRoute, localHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware);
    Date localDate = getCurrentDate();
    Object localObject2;
    if (revalidationResponseIsTooOld(localCloseableHttpResponse, paramHttpCacheEntry))
    {
      localCloseableHttpResponse.close();
      localObject1 = this.conditionalRequestBuilder.buildUnconditionalRequest(paramHttpRequestWrapper, paramHttpCacheEntry);
      localObject2 = getCurrentDate();
      localObject1 = this.backend.execute(paramHttpRoute, (HttpRequestWrapper)localObject1, paramHttpClientContext, paramHttpExecutionAware);
      localDate = getCurrentDate();
    }
    else
    {
      localObject2 = localObject1;
      localObject1 = localCloseableHttpResponse;
    }
    ((CloseableHttpResponse)localObject1).addHeader("Via", generateViaHeader((HttpMessage)localObject1));
    int i = ((CloseableHttpResponse)localObject1).getStatusLine().getStatusCode();
    if ((i == 304) || (i == 200)) {
      recordCacheUpdate(paramHttpClientContext);
    }
    if (i == 304)
    {
      paramHttpRoute = this.responseCache.updateCacheEntry(paramHttpClientContext.getTargetHost(), paramHttpRequestWrapper, paramHttpCacheEntry, (HttpResponse)localObject1, (Date)localObject2, localDate);
      if ((this.suitabilityChecker.isConditional(paramHttpRequestWrapper)) && (this.suitabilityChecker.allConditionalsMatch(paramHttpRequestWrapper, paramHttpRoute, new Date()))) {
        return this.responseGenerator.generateNotModifiedResponse(paramHttpRoute);
      }
      return this.responseGenerator.generateResponse(paramHttpRoute);
    }
    if ((staleIfErrorAppliesTo(i)) && (!staleResponseNotAllowed(paramHttpRequestWrapper, paramHttpCacheEntry, getCurrentDate())) && (this.validityPolicy.mayReturnStaleIfError(paramHttpRequestWrapper, paramHttpCacheEntry, localDate))) {
      try
      {
        paramHttpRoute = this.responseGenerator.generateResponse(paramHttpCacheEntry);
        paramHttpRoute.addHeader("Warning", "110 localhost \"Response is stale\"");
        return paramHttpRoute;
      }
      finally
      {
        ((CloseableHttpResponse)localObject1).close();
      }
    }
    return handleBackendResponse(paramHttpRoute, localHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, (Date)localObject2, localDate, (CloseableHttpResponse)localObject1);
  }
  
  public boolean supportsRangeAndContentRangeHeaders()
  {
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CachingExec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */