package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

@Immutable
class CacheEntity
  implements HttpEntity, Serializable
{
  private static final long serialVersionUID = -3467082284120936233L;
  private final HttpCacheEntry cacheEntry;
  
  public CacheEntity(HttpCacheEntry paramHttpCacheEntry)
  {
    this.cacheEntry = paramHttpCacheEntry;
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public void consumeContent()
    throws IOException
  {}
  
  public InputStream getContent()
    throws IOException
  {
    return this.cacheEntry.getResource().getInputStream();
  }
  
  public Header getContentEncoding()
  {
    return this.cacheEntry.getFirstHeader("Content-Encoding");
  }
  
  public long getContentLength()
  {
    return this.cacheEntry.getResource().length();
  }
  
  public Header getContentType()
  {
    return this.cacheEntry.getFirstHeader("Content-Type");
  }
  
  public boolean isChunked()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return true;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    InputStream localInputStream = this.cacheEntry.getResource().getInputStream();
    try
    {
      IOUtils.copy(localInputStream, paramOutputStream);
      return;
    }
    finally
    {
      localInputStream.close();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */