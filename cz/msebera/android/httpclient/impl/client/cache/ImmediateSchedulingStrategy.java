package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@ThreadSafe
public class ImmediateSchedulingStrategy
  implements SchedulingStrategy
{
  private final ExecutorService executor;
  
  public ImmediateSchedulingStrategy(CacheConfig paramCacheConfig)
  {
    this(new ThreadPoolExecutor(paramCacheConfig.getAsynchronousWorkersCore(), paramCacheConfig.getAsynchronousWorkersMax(), paramCacheConfig.getAsynchronousWorkerIdleLifetimeSecs(), TimeUnit.SECONDS, new ArrayBlockingQueue(paramCacheConfig.getRevalidationQueueSize())));
  }
  
  ImmediateSchedulingStrategy(ExecutorService paramExecutorService)
  {
    this.executor = paramExecutorService;
  }
  
  void awaitTermination(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException
  {
    this.executor.awaitTermination(paramLong, paramTimeUnit);
  }
  
  public void close()
  {
    this.executor.shutdown();
  }
  
  public void schedule(AsynchronousValidationRequest paramAsynchronousValidationRequest)
  {
    if (paramAsynchronousValidationRequest != null)
    {
      this.executor.execute(paramAsynchronousValidationRequest);
      return;
    }
    throw new IllegalArgumentException("AsynchronousValidationRequest may not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ImmediateSchedulingStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */