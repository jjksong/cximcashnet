package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.entity.AbstractHttpEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.message.BasicHttpResponse;
import cz.msebera.android.httpclient.message.BasicStatusLine;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Immutable
class RequestProtocolCompliance
{
  private static final List<String> disallowedWithNoCache = Arrays.asList(new String[] { "min-fresh", "max-stale", "max-age" });
  private final boolean weakETagOnPutDeleteAllowed;
  
  public RequestProtocolCompliance()
  {
    this.weakETagOnPutDeleteAllowed = false;
  }
  
  public RequestProtocolCompliance(boolean paramBoolean)
  {
    this.weakETagOnPutDeleteAllowed = paramBoolean;
  }
  
  private void add100ContinueHeaderIfMissing(HttpRequest paramHttpRequest)
  {
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("Expect");
    int m = arrayOfHeader.length;
    int j = 0;
    int i = 0;
    while (j < m)
    {
      HeaderElement[] arrayOfHeaderElement = arrayOfHeader[j].getElements();
      int n = arrayOfHeaderElement.length;
      for (int k = 0; k < n; k++) {
        if ("100-continue".equalsIgnoreCase(arrayOfHeaderElement[k].getName())) {
          i = 1;
        }
      }
      j++;
    }
    if (i == 0) {
      paramHttpRequest.addHeader("Expect", "100-continue");
    }
  }
  
  private void addContentTypeHeaderIfMissing(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
  {
    if (paramHttpEntityEnclosingRequest.getEntity().getContentType() == null) {
      ((AbstractHttpEntity)paramHttpEntityEnclosingRequest.getEntity()).setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
    }
  }
  
  private String buildHeaderFromElements(List<HeaderElement> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder("");
    Iterator localIterator = paramList.iterator();
    int i = 1;
    while (localIterator.hasNext())
    {
      paramList = (HeaderElement)localIterator.next();
      if (i == 0) {
        localStringBuilder.append(",");
      } else {
        i = 0;
      }
      localStringBuilder.append(paramList.toString());
    }
    return localStringBuilder.toString();
  }
  
  private void decrementOPTIONSMaxForwardsIfGreaterThen0(HttpRequest paramHttpRequest)
  {
    if (!"OPTIONS".equals(paramHttpRequest.getRequestLine().getMethod())) {
      return;
    }
    Header localHeader = paramHttpRequest.getFirstHeader("Max-Forwards");
    if (localHeader == null) {
      return;
    }
    paramHttpRequest.removeHeaders("Max-Forwards");
    paramHttpRequest.setHeader("Max-Forwards", Integer.toString(Integer.parseInt(localHeader.getValue()) - 1));
  }
  
  private void remove100ContinueHeaderIfExists(HttpRequest paramHttpRequest)
  {
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("Expect");
    Object localObject = new ArrayList();
    int m = arrayOfHeader.length;
    int j = 0;
    int i = 0;
    while (j < m)
    {
      Header localHeader = arrayOfHeader[j];
      for (HeaderElement localHeaderElement : localHeader.getElements()) {
        if (!"100-continue".equalsIgnoreCase(localHeaderElement.getName())) {
          ((List)localObject).add(localHeaderElement);
        } else {
          i = 1;
        }
      }
      if (i != 0)
      {
        paramHttpRequest.removeHeader(localHeader);
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          paramHttpRequest.addHeader(new BasicHeader("Expect", ((HeaderElement)((Iterator)localObject).next()).getName()));
        }
        return;
      }
      localObject = new ArrayList();
      j++;
    }
  }
  
  private RequestProtocolError requestContainsNoCacheDirectiveWithFieldName(HttpRequest paramHttpRequest)
  {
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    for (int i = 0; i < k; i++) {
      for (paramHttpRequest : arrayOfHeader[i].getElements()) {
        if (("no-cache".equalsIgnoreCase(paramHttpRequest.getName())) && (paramHttpRequest.getValue() != null)) {
          return RequestProtocolError.NO_CACHE_DIRECTIVE_WITH_FIELD_NAME;
        }
      }
    }
    return null;
  }
  
  private RequestProtocolError requestHasWeakETagAndRange(HttpRequest paramHttpRequest)
  {
    if (!"GET".equals(paramHttpRequest.getRequestLine().getMethod())) {
      return null;
    }
    if (paramHttpRequest.getFirstHeader("Range") == null) {
      return null;
    }
    paramHttpRequest = paramHttpRequest.getFirstHeader("If-Range");
    if (paramHttpRequest == null) {
      return null;
    }
    if (paramHttpRequest.getValue().startsWith("W/")) {
      return RequestProtocolError.WEAK_ETAG_AND_RANGE_ERROR;
    }
    return null;
  }
  
  private RequestProtocolError requestHasWeekETagForPUTOrDELETEIfMatch(HttpRequest paramHttpRequest)
  {
    Object localObject = paramHttpRequest.getRequestLine().getMethod();
    if ((!"PUT".equals(localObject)) && (!"DELETE".equals(localObject))) {
      return null;
    }
    localObject = paramHttpRequest.getFirstHeader("If-Match");
    if (localObject != null)
    {
      if (((Header)localObject).getValue().startsWith("W/")) {
        return RequestProtocolError.WEAK_ETAG_ON_PUTDELETE_METHOD_ERROR;
      }
    }
    else
    {
      paramHttpRequest = paramHttpRequest.getFirstHeader("If-None-Match");
      if (paramHttpRequest == null) {
        return null;
      }
      if (paramHttpRequest.getValue().startsWith("W/")) {
        return RequestProtocolError.WEAK_ETAG_ON_PUTDELETE_METHOD_ERROR;
      }
    }
    return null;
  }
  
  private boolean requestMustNotHaveEntity(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if (("TRACE".equals(paramHttpRequest.getRequestLine().getMethod())) && ((paramHttpRequest instanceof HttpEntityEnclosingRequest))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void stripOtherFreshnessDirectivesWithNoCache(HttpRequest paramHttpRequest)
  {
    ArrayList localArrayList = new ArrayList();
    Header[] arrayOfHeader = paramHttpRequest.getHeaders("Cache-Control");
    int m = arrayOfHeader.length;
    int j = 0;
    int i = 0;
    while (j < m)
    {
      for (HeaderElement localHeaderElement : arrayOfHeader[j].getElements())
      {
        if (!disallowedWithNoCache.contains(localHeaderElement.getName())) {
          localArrayList.add(localHeaderElement);
        }
        if ("no-cache".equals(localHeaderElement.getName())) {
          i = 1;
        }
      }
      j++;
    }
    if (i == 0) {
      return;
    }
    paramHttpRequest.removeHeaders("Cache-Control");
    paramHttpRequest.setHeader("Cache-Control", buildHeaderFromElements(localArrayList));
  }
  
  private void verifyOPTIONSRequestWithBodyHasContentType(HttpRequest paramHttpRequest)
  {
    if (!"OPTIONS".equals(paramHttpRequest.getRequestLine().getMethod())) {
      return;
    }
    if (!(paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      return;
    }
    addContentTypeHeaderIfMissing((HttpEntityEnclosingRequest)paramHttpRequest);
  }
  
  private void verifyRequestWithExpectContinueFlagHas100continueHeader(HttpRequest paramHttpRequest)
  {
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest))
    {
      HttpEntityEnclosingRequest localHttpEntityEnclosingRequest = (HttpEntityEnclosingRequest)paramHttpRequest;
      if ((localHttpEntityEnclosingRequest.expectContinue()) && (localHttpEntityEnclosingRequest.getEntity() != null)) {
        add100ContinueHeaderIfMissing(paramHttpRequest);
      } else {
        remove100ContinueHeaderIfExists(paramHttpRequest);
      }
    }
    else
    {
      remove100ContinueHeaderIfExists(paramHttpRequest);
    }
  }
  
  public HttpResponse getErrorForRequest(RequestProtocolError paramRequestProtocolError)
  {
    switch (paramRequestProtocolError)
    {
    default: 
      throw new IllegalStateException("The request was compliant, therefore no error can be generated for it.");
    case ???: 
      return new BasicHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, 400, "No-Cache directive MUST NOT include a field name"));
    case ???: 
      return new BasicHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, 400, "Weak eTag not compatible with PUT or DELETE requests"));
    case ???: 
      return new BasicHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, 400, "Weak eTag not compatible with byte range"));
    }
    return new BasicHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, 411, ""));
  }
  
  public void makeRequestCompliant(HttpRequestWrapper paramHttpRequestWrapper)
    throws ClientProtocolException
  {
    if (requestMustNotHaveEntity(paramHttpRequestWrapper)) {
      ((HttpEntityEnclosingRequest)paramHttpRequestWrapper).setEntity(null);
    }
    verifyRequestWithExpectContinueFlagHas100continueHeader(paramHttpRequestWrapper);
    verifyOPTIONSRequestWithBodyHasContentType(paramHttpRequestWrapper);
    decrementOPTIONSMaxForwardsIfGreaterThen0(paramHttpRequestWrapper);
    stripOtherFreshnessDirectivesWithNoCache(paramHttpRequestWrapper);
    if ((requestVersionIsTooLow(paramHttpRequestWrapper)) || (requestMinorVersionIsTooHighMajorVersionsMatch(paramHttpRequestWrapper))) {
      paramHttpRequestWrapper.setProtocolVersion(HttpVersion.HTTP_1_1);
    }
  }
  
  public List<RequestProtocolError> requestIsFatallyNonCompliant(HttpRequest paramHttpRequest)
  {
    ArrayList localArrayList = new ArrayList();
    RequestProtocolError localRequestProtocolError = requestHasWeakETagAndRange(paramHttpRequest);
    if (localRequestProtocolError != null) {
      localArrayList.add(localRequestProtocolError);
    }
    if (!this.weakETagOnPutDeleteAllowed)
    {
      localRequestProtocolError = requestHasWeekETagForPUTOrDELETEIfMatch(paramHttpRequest);
      if (localRequestProtocolError != null) {
        localArrayList.add(localRequestProtocolError);
      }
    }
    paramHttpRequest = requestContainsNoCacheDirectiveWithFieldName(paramHttpRequest);
    if (paramHttpRequest != null) {
      localArrayList.add(paramHttpRequest);
    }
    return localArrayList;
  }
  
  protected boolean requestMinorVersionIsTooHighMajorVersionsMatch(HttpRequest paramHttpRequest)
  {
    paramHttpRequest = paramHttpRequest.getProtocolVersion();
    if (paramHttpRequest.getMajor() != HttpVersion.HTTP_1_1.getMajor()) {
      return false;
    }
    return paramHttpRequest.getMinor() > HttpVersion.HTTP_1_1.getMinor();
  }
  
  protected boolean requestVersionIsTooLow(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if (paramHttpRequest.getProtocolVersion().compareToVersion(HttpVersion.HTTP_1_1) < 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/RequestProtocolCompliance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */