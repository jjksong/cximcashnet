package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.cache.InputLimit;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.message.BasicHttpResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

@NotThreadSafe
class SizeLimitedResponseReader
{
  private boolean consumed;
  private InputStream instream;
  private InputLimit limit;
  private final long maxResponseSizeBytes;
  private final HttpRequest request;
  private Resource resource;
  private final ResourceFactory resourceFactory;
  private final CloseableHttpResponse response;
  
  public SizeLimitedResponseReader(ResourceFactory paramResourceFactory, long paramLong, HttpRequest paramHttpRequest, CloseableHttpResponse paramCloseableHttpResponse)
  {
    this.resourceFactory = paramResourceFactory;
    this.maxResponseSizeBytes = paramLong;
    this.request = paramHttpRequest;
    this.response = paramCloseableHttpResponse;
  }
  
  private void doConsume()
    throws IOException
  {
    ensureNotConsumed();
    this.consumed = true;
    this.limit = new InputLimit(this.maxResponseSizeBytes);
    HttpEntity localHttpEntity = this.response.getEntity();
    if (localHttpEntity == null) {
      return;
    }
    String str = this.request.getRequestLine().getUri();
    this.instream = localHttpEntity.getContent();
    try
    {
      this.resource = this.resourceFactory.generate(str, this.instream, this.limit);
      return;
    }
    finally
    {
      if (!this.limit.isReached()) {
        this.instream.close();
      }
    }
  }
  
  private void ensureConsumed()
  {
    if (this.consumed) {
      return;
    }
    throw new IllegalStateException("Response has not been consumed");
  }
  
  private void ensureNotConsumed()
  {
    if (!this.consumed) {
      return;
    }
    throw new IllegalStateException("Response has already been consumed");
  }
  
  CloseableHttpResponse getReconstructedResponse()
    throws IOException
  {
    ensureConsumed();
    Object localObject1 = new BasicHttpResponse(this.response.getStatusLine());
    ((HttpResponse)localObject1).setHeaders(this.response.getAllHeaders());
    Object localObject2 = new CombinedEntity(this.resource, this.instream);
    HttpEntity localHttpEntity = this.response.getEntity();
    if (localHttpEntity != null)
    {
      ((CombinedEntity)localObject2).setContentType(localHttpEntity.getContentType());
      ((CombinedEntity)localObject2).setContentEncoding(localHttpEntity.getContentEncoding());
      ((CombinedEntity)localObject2).setChunked(localHttpEntity.isChunked());
    }
    ((HttpResponse)localObject1).setEntity((HttpEntity)localObject2);
    localObject2 = ResponseProxyHandler.class.getClassLoader();
    localObject1 = new ResponseProxyHandler((HttpResponse)localObject1)
    {
      public void close()
        throws IOException
      {
        SizeLimitedResponseReader.this.response.close();
      }
    };
    return (CloseableHttpResponse)Proxy.newProxyInstance((ClassLoader)localObject2, new Class[] { CloseableHttpResponse.class }, (InvocationHandler)localObject1);
  }
  
  Resource getResource()
  {
    ensureConsumed();
    return this.resource;
  }
  
  boolean isLimitReached()
  {
    ensureConsumed();
    return this.limit.isReached();
  }
  
  protected void readResponse()
    throws IOException
  {
    if (!this.consumed) {
      doConsume();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/SizeLimitedResponseReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */