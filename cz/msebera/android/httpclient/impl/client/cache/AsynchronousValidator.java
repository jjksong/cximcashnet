package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;

class AsynchronousValidator
  implements Closeable
{
  private final CacheKeyGenerator cacheKeyGenerator;
  private final FailureCache failureCache;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final Set<String> queued;
  private final SchedulingStrategy schedulingStrategy;
  
  public AsynchronousValidator(CacheConfig paramCacheConfig)
  {
    this(new ImmediateSchedulingStrategy(paramCacheConfig));
  }
  
  AsynchronousValidator(SchedulingStrategy paramSchedulingStrategy)
  {
    this.schedulingStrategy = paramSchedulingStrategy;
    this.queued = new HashSet();
    this.cacheKeyGenerator = new CacheKeyGenerator();
    this.failureCache = new DefaultFailureCache();
  }
  
  public void close()
    throws IOException
  {
    this.schedulingStrategy.close();
  }
  
  Set<String> getScheduledIdentifiers()
  {
    return Collections.unmodifiableSet(this.queued);
  }
  
  void jobFailed(String paramString)
  {
    this.failureCache.increaseErrorCount(paramString);
  }
  
  void jobSuccessful(String paramString)
  {
    this.failureCache.resetErrorCount(paramString);
  }
  
  void markComplete(String paramString)
  {
    try
    {
      this.queued.remove(paramString);
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  public void revalidateCacheEntry(CachingExec paramCachingExec, HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry)
  {
    try
    {
      String str = this.cacheKeyGenerator.getVariantURI(paramHttpClientContext.getTargetHost(), paramHttpRequestWrapper, paramHttpCacheEntry);
      if (!this.queued.contains(str))
      {
        int i = this.failureCache.getErrorCount(str);
        AsynchronousValidationRequest localAsynchronousValidationRequest = new cz/msebera/android/httpclient/impl/client/cache/AsynchronousValidationRequest;
        localAsynchronousValidationRequest.<init>(this, paramCachingExec, paramHttpRoute, paramHttpRequestWrapper, paramHttpClientContext, paramHttpExecutionAware, paramHttpCacheEntry, str, i);
        try
        {
          this.schedulingStrategy.schedule(localAsynchronousValidationRequest);
          this.queued.add(str);
        }
        catch (RejectedExecutionException paramHttpRoute)
        {
          paramCachingExec = this.log;
          paramHttpRequestWrapper = new java/lang/StringBuilder;
          paramHttpRequestWrapper.<init>();
          paramHttpRequestWrapper.append("Revalidation for [");
          paramHttpRequestWrapper.append(str);
          paramHttpRequestWrapper.append("] not scheduled: ");
          paramHttpRequestWrapper.append(paramHttpRoute);
          paramCachingExec.debug(paramHttpRequestWrapper.toString());
        }
      }
      return;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/AsynchronousValidator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */