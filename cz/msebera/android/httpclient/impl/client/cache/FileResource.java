package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.cache.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@ThreadSafe
public class FileResource
  implements Resource
{
  private static final long serialVersionUID = 4132244415919043397L;
  private volatile boolean disposed;
  private final File file;
  
  public FileResource(File paramFile)
  {
    this.file = paramFile;
    this.disposed = false;
  }
  
  public void dispose()
  {
    try
    {
      boolean bool = this.disposed;
      if (bool) {
        return;
      }
      this.disposed = true;
      this.file.delete();
      return;
    }
    finally {}
  }
  
  File getFile()
  {
    try
    {
      File localFile = this.file;
      return localFile;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(this.file);
      return localFileInputStream;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public long length()
  {
    try
    {
      long l = this.file.length();
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/FileResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */