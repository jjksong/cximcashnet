package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

abstract interface HttpCache
{
  public abstract HttpResponse cacheAndReturnResponse(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException;
  
  public abstract CloseableHttpResponse cacheAndReturnResponse(HttpHost paramHttpHost, HttpRequest paramHttpRequest, CloseableHttpResponse paramCloseableHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException;
  
  public abstract void flushCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException;
  
  public abstract void flushInvalidatedCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException;
  
  public abstract void flushInvalidatedCacheEntriesFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse);
  
  public abstract HttpCacheEntry getCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException;
  
  public abstract Map<String, Variant> getVariantCacheEntriesWithEtags(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException;
  
  public abstract void reuseVariantEntryFor(HttpHost paramHttpHost, HttpRequest paramHttpRequest, Variant paramVariant)
    throws IOException;
  
  public abstract HttpCacheEntry updateCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2)
    throws IOException;
  
  public abstract HttpCacheEntry updateVariantCacheEntry(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse, Date paramDate1, Date paramDate2, String paramString)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/HttpCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */