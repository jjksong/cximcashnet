package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.message.AbstractHttpMessage;
import cz.msebera.android.httpclient.message.BasicStatusLine;
import cz.msebera.android.httpclient.message.HeaderGroup;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
import java.util.Locale;

@Immutable
final class OptionsHttp11Response
  extends AbstractHttpMessage
  implements HttpResponse
{
  private final StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 501, "");
  private final ProtocolVersion version = HttpVersion.HTTP_1_1;
  
  public void addHeader(Header paramHeader) {}
  
  public void addHeader(String paramString1, String paramString2) {}
  
  public boolean containsHeader(String paramString)
  {
    return this.headergroup.containsHeader(paramString);
  }
  
  public Header[] getAllHeaders()
  {
    return this.headergroup.getAllHeaders();
  }
  
  public HttpEntity getEntity()
  {
    return null;
  }
  
  public Header getFirstHeader(String paramString)
  {
    return this.headergroup.getFirstHeader(paramString);
  }
  
  public Header[] getHeaders(String paramString)
  {
    return this.headergroup.getHeaders(paramString);
  }
  
  public Header getLastHeader(String paramString)
  {
    return this.headergroup.getLastHeader(paramString);
  }
  
  public Locale getLocale()
  {
    return null;
  }
  
  public HttpParams getParams()
  {
    if (this.params == null) {
      this.params = new BasicHttpParams();
    }
    return this.params;
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    return this.version;
  }
  
  public StatusLine getStatusLine()
  {
    return this.statusLine;
  }
  
  public HeaderIterator headerIterator()
  {
    return this.headergroup.iterator();
  }
  
  public HeaderIterator headerIterator(String paramString)
  {
    return this.headergroup.iterator(paramString);
  }
  
  public void removeHeader(Header paramHeader) {}
  
  public void removeHeaders(String paramString) {}
  
  public void setEntity(HttpEntity paramHttpEntity) {}
  
  public void setHeader(Header paramHeader) {}
  
  public void setHeader(String paramString1, String paramString2) {}
  
  public void setHeaders(Header[] paramArrayOfHeader) {}
  
  public void setLocale(Locale paramLocale) {}
  
  public void setParams(HttpParams paramHttpParams) {}
  
  public void setReasonPhrase(String paramString)
    throws IllegalStateException
  {}
  
  public void setStatusCode(int paramInt)
    throws IllegalStateException
  {}
  
  public void setStatusLine(ProtocolVersion paramProtocolVersion, int paramInt) {}
  
  public void setStatusLine(ProtocolVersion paramProtocolVersion, int paramInt, String paramString) {}
  
  public void setStatusLine(StatusLine paramStatusLine) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/OptionsHttp11Response.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */