package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import java.util.Date;

@Immutable
class CacheValidityPolicy
{
  public static final long MAX_AGE = 2147483648L;
  
  private boolean mayReturnStaleIfError(Header[] paramArrayOfHeader, long paramLong)
  {
    int k = paramArrayOfHeader.length;
    int i = 0;
    for (boolean bool1 = false; i < k; bool1 = bool2)
    {
      HeaderElement[] arrayOfHeaderElement = paramArrayOfHeader[i].getElements();
      int m = arrayOfHeaderElement.length;
      for (int j = 0;; j++)
      {
        boolean bool2 = bool1;
        if (j >= m) {
          break;
        }
        HeaderElement localHeaderElement = arrayOfHeaderElement[j];
        if ("stale-if-error".equals(localHeaderElement.getName())) {}
        try
        {
          int n = Integer.parseInt(localHeaderElement.getValue());
          if (paramLong <= n) {
            bool2 = true;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          for (;;) {}
        }
      }
      i++;
    }
    return bool1;
  }
  
  protected boolean contentLengthHeaderMatchesActualLength(HttpCacheEntry paramHttpCacheEntry)
  {
    boolean bool;
    if ((hasContentLengthHeader(paramHttpCacheEntry)) && (getContentLengthValue(paramHttpCacheEntry) != paramHttpCacheEntry.getResource().length())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  protected long getAgeValue(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Age");
    int j = paramHttpCacheEntry.length;
    int i = 0;
    for (long l2 = 0L; i < j; l2 = l3)
    {
      Object localObject = paramHttpCacheEntry[i];
      l3 = 2147483648L;
      try
      {
        l1 = Long.parseLong(((Header)localObject).getValue());
        if (l1 < 0L) {
          l1 = l3;
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;)
        {
          long l1 = l3;
        }
      }
      l3 = l2;
      if (l1 > l2) {
        l3 = l1;
      }
      i++;
    }
    return l2;
  }
  
  protected long getApparentAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    Date localDate = paramHttpCacheEntry.getDate();
    if (localDate == null) {
      return 2147483648L;
    }
    long l = paramHttpCacheEntry.getResponseDate().getTime() - localDate.getTime();
    if (l < 0L) {
      return 0L;
    }
    return l / 1000L;
  }
  
  protected long getContentLengthValue(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Content-Length");
    if (paramHttpCacheEntry == null) {
      return -1L;
    }
    try
    {
      long l = Long.parseLong(paramHttpCacheEntry.getValue());
      return l;
    }
    catch (NumberFormatException paramHttpCacheEntry) {}
    return -1L;
  }
  
  protected long getCorrectedInitialAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    return getCorrectedReceivedAgeSecs(paramHttpCacheEntry) + getResponseDelaySecs(paramHttpCacheEntry);
  }
  
  protected long getCorrectedReceivedAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    long l1 = getApparentAgeSecs(paramHttpCacheEntry);
    long l2 = getAgeValue(paramHttpCacheEntry);
    if (l1 <= l2) {
      l1 = l2;
    }
    return l1;
  }
  
  public long getCurrentAgeSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    return getCorrectedInitialAgeSecs(paramHttpCacheEntry) + getResidentTimeSecs(paramHttpCacheEntry, paramDate);
  }
  
  @Deprecated
  protected Date getDateValue(HttpCacheEntry paramHttpCacheEntry)
  {
    return paramHttpCacheEntry.getDate();
  }
  
  protected Date getExpirationDate(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Expires");
    if (paramHttpCacheEntry == null) {
      return null;
    }
    return DateUtils.parseDate(paramHttpCacheEntry.getValue());
  }
  
  public long getFreshnessLifetimeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    long l = getMaxAge(paramHttpCacheEntry);
    if (l > -1L) {
      return l;
    }
    Date localDate = paramHttpCacheEntry.getDate();
    if (localDate == null) {
      return 0L;
    }
    paramHttpCacheEntry = getExpirationDate(paramHttpCacheEntry);
    if (paramHttpCacheEntry == null) {
      return 0L;
    }
    return (paramHttpCacheEntry.getTime() - localDate.getTime()) / 1000L;
  }
  
  public long getHeuristicFreshnessLifetimeSecs(HttpCacheEntry paramHttpCacheEntry, float paramFloat, long paramLong)
  {
    Date localDate = paramHttpCacheEntry.getDate();
    paramHttpCacheEntry = getLastModifiedValue(paramHttpCacheEntry);
    if ((localDate != null) && (paramHttpCacheEntry != null))
    {
      paramLong = localDate.getTime() - paramHttpCacheEntry.getTime();
      if (paramLong < 0L) {
        return 0L;
      }
      return (paramFloat * (float)(paramLong / 1000L));
    }
    return paramLong;
  }
  
  protected Date getLastModifiedValue(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Last-Modified");
    if (paramHttpCacheEntry == null) {
      return null;
    }
    return DateUtils.parseDate(paramHttpCacheEntry.getValue());
  }
  
  protected long getMaxAge(HttpCacheEntry paramHttpCacheEntry)
  {
    Header[] arrayOfHeader = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    long l1 = -1L;
    for (int i = 0; i < k; i++)
    {
      paramHttpCacheEntry = arrayOfHeader[i].getElements();
      int m = paramHttpCacheEntry.length;
      int j = 0;
      while (j < m)
      {
        Object localObject = paramHttpCacheEntry[j];
        long l2;
        if (!"max-age".equals(((HeaderElement)localObject).getName()))
        {
          l2 = l1;
          if (!"s-maxage".equals(((HeaderElement)localObject).getName())) {}
        }
        else
        {
          try
          {
            long l3 = Long.parseLong(((HeaderElement)localObject).getValue());
            if (l1 != -1L)
            {
              l2 = l1;
              if (l3 >= l1) {}
            }
            else
            {
              l2 = l3;
            }
          }
          catch (NumberFormatException localNumberFormatException)
          {
            l2 = 0L;
          }
        }
        j++;
        l1 = l2;
      }
    }
    return l1;
  }
  
  protected long getResidentTimeSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    return (paramDate.getTime() - paramHttpCacheEntry.getResponseDate().getTime()) / 1000L;
  }
  
  protected long getResponseDelaySecs(HttpCacheEntry paramHttpCacheEntry)
  {
    return (paramHttpCacheEntry.getResponseDate().getTime() - paramHttpCacheEntry.getRequestDate().getTime()) / 1000L;
  }
  
  public long getStalenessSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    long l2 = getCurrentAgeSecs(paramHttpCacheEntry, paramDate);
    long l1 = getFreshnessLifetimeSecs(paramHttpCacheEntry);
    if (l2 <= l1) {
      return 0L;
    }
    return l2 - l1;
  }
  
  public boolean hasCacheControlDirective(HttpCacheEntry paramHttpCacheEntry, String paramString)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = paramHttpCacheEntry.length;
    for (int i = 0; i < k; i++)
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpCacheEntry[i].getElements();
      int m = arrayOfHeaderElement.length;
      for (int j = 0; j < m; j++) {
        if (paramString.equalsIgnoreCase(arrayOfHeaderElement[j].getName())) {
          return true;
        }
      }
    }
    return false;
  }
  
  protected boolean hasContentLengthHeader(HttpCacheEntry paramHttpCacheEntry)
  {
    boolean bool;
    if (paramHttpCacheEntry.getFirstHeader("Content-Length") != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isResponseFresh(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    boolean bool;
    if (getCurrentAgeSecs(paramHttpCacheEntry, paramDate) < getFreshnessLifetimeSecs(paramHttpCacheEntry)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isResponseHeuristicallyFresh(HttpCacheEntry paramHttpCacheEntry, Date paramDate, float paramFloat, long paramLong)
  {
    boolean bool;
    if (getCurrentAgeSecs(paramHttpCacheEntry, paramDate) < getHeuristicFreshnessLifetimeSecs(paramHttpCacheEntry, paramFloat, paramLong)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isRevalidatable(HttpCacheEntry paramHttpCacheEntry)
  {
    boolean bool;
    if ((paramHttpCacheEntry.getFirstHeader("ETag") == null) && (paramHttpCacheEntry.getFirstHeader("Last-Modified") == null)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean mayReturnStaleIfError(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    long l = getStalenessSecs(paramHttpCacheEntry, paramDate);
    boolean bool;
    if ((!mayReturnStaleIfError(paramHttpRequest.getHeaders("Cache-Control"), l)) && (!mayReturnStaleIfError(paramHttpCacheEntry.getHeaders("Cache-Control"), l))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean mayReturnStaleWhileRevalidating(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    Header[] arrayOfHeader = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    for (int i = 0; i < k; i++) {
      for (HeaderElement localHeaderElement : arrayOfHeader[i].getElements())
      {
        if ("stale-while-revalidate".equalsIgnoreCase(localHeaderElement.getName())) {}
        try
        {
          int n = Integer.parseInt(localHeaderElement.getValue());
          long l = getStalenessSecs(paramHttpCacheEntry, paramDate);
          if (l <= n) {
            return true;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          for (;;) {}
        }
      }
    }
    return false;
  }
  
  public boolean mustRevalidate(HttpCacheEntry paramHttpCacheEntry)
  {
    return hasCacheControlDirective(paramHttpCacheEntry, "must-revalidate");
  }
  
  public boolean proxyRevalidate(HttpCacheEntry paramHttpCacheEntry)
  {
    return hasCacheControlDirective(paramHttpCacheEntry, "proxy-revalidate");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheValidityPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */