package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

final class CacheMap
  extends LinkedHashMap<String, HttpCacheEntry>
{
  private static final long serialVersionUID = -7750025207539768511L;
  private final int maxEntries;
  
  CacheMap(int paramInt)
  {
    super(20, 0.75F, true);
    this.maxEntries = paramInt;
  }
  
  protected boolean removeEldestEntry(Map.Entry<String, HttpCacheEntry> paramEntry)
  {
    boolean bool;
    if (size() > this.maxEntries) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */