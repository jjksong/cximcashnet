package cz.msebera.android.httpclient.impl.client.cache;

import java.io.Closeable;

public abstract interface SchedulingStrategy
  extends Closeable
{
  public abstract void schedule(AsynchronousValidationRequest paramAsynchronousValidationRequest);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/SchedulingStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */