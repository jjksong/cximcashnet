package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.InputLimit;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Immutable
public class HeapResourceFactory
  implements ResourceFactory
{
  public Resource copy(String paramString, Resource paramResource)
    throws IOException
  {
    if ((paramResource instanceof HeapResource))
    {
      paramString = ((HeapResource)paramResource).getByteArray();
    }
    else
    {
      paramString = new ByteArrayOutputStream();
      IOUtils.copyAndClose(paramResource.getInputStream(), paramString);
      paramString = paramString.toByteArray();
    }
    return createResource(paramString);
  }
  
  Resource createResource(byte[] paramArrayOfByte)
  {
    return new HeapResource(paramArrayOfByte);
  }
  
  public Resource generate(String paramString, InputStream paramInputStream, InputLimit paramInputLimit)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramString = new byte['ࠀ'];
    long l1 = 0L;
    long l2;
    do
    {
      do
      {
        int i = paramInputStream.read(paramString);
        if (i == -1) {
          break;
        }
        localByteArrayOutputStream.write(paramString, 0, i);
        l2 = l1 + i;
        l1 = l2;
      } while (paramInputLimit == null);
      l1 = l2;
    } while (l2 <= paramInputLimit.getValue());
    paramInputLimit.reached();
    return createResource(localByteArrayOutputStream.toByteArray());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/HeapResourceFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */