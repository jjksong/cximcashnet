package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@NotThreadSafe
class ResponseProxyHandler
  implements InvocationHandler
{
  private static final Method CLOSE_METHOD;
  private final HttpResponse original;
  
  static
  {
    try
    {
      CLOSE_METHOD = Closeable.class.getMethod("close", new Class[0]);
      return;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      throw new Error(localNoSuchMethodException);
    }
  }
  
  ResponseProxyHandler(HttpResponse paramHttpResponse)
  {
    this.original = paramHttpResponse;
  }
  
  public void close()
    throws IOException
  {
    IOUtils.consume(this.original.getEntity());
  }
  
  public Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
    throws Throwable
  {
    if (paramMethod.equals(CLOSE_METHOD))
    {
      close();
      return null;
    }
    try
    {
      paramObject = paramMethod.invoke(this.original, paramArrayOfObject);
      return paramObject;
    }
    catch (InvocationTargetException paramMethod)
    {
      paramObject = paramMethod.getCause();
      if (paramObject != null) {
        throw ((Throwable)paramObject);
      }
      throw paramMethod;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ResponseProxyHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */