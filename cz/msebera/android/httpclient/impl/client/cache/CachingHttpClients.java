package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import java.io.File;

@Immutable
public class CachingHttpClients
{
  public static CloseableHttpClient createFileBound(File paramFile)
  {
    return CachingHttpClientBuilder.create().setCacheDir(paramFile).build();
  }
  
  public static CloseableHttpClient createMemoryBound()
  {
    return CachingHttpClientBuilder.create().build();
  }
  
  public static CachingHttpClientBuilder custom()
  {
    return CachingHttpClientBuilder.create();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CachingHttpClients.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */