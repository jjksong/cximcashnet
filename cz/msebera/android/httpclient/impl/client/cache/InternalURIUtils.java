package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.routing.RouteInfo;
import java.net.URI;
import java.net.URISyntaxException;

@Immutable
class InternalURIUtils
{
  public static URI rewriteURIForRoute(URI paramURI, RouteInfo paramRouteInfo)
    throws URISyntaxException
  {
    if (paramURI == null) {
      return null;
    }
    if ((paramRouteInfo.getProxyHost() != null) && (!paramRouteInfo.isTunnelled()))
    {
      if (!paramURI.isAbsolute()) {
        return URIUtils.rewriteURI(paramURI, paramRouteInfo.getTargetHost(), true);
      }
      return URIUtils.rewriteURI(paramURI);
    }
    if (paramURI.isAbsolute()) {
      return URIUtils.rewriteURI(paramURI, null, true);
    }
    return URIUtils.rewriteURI(paramURI);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/InternalURIUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */