package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class WarningValue
{
  private static final String ASCTIME_DATE = "(Mon|Tue|Wed|Thu|Fri|Sat|Sun) ((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( |\\d)\\d) (\\d{2}:\\d{2}:\\d{2}) \\d{4}";
  private static final String DATE1 = "\\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4}";
  private static final String DATE2 = "\\d{2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\\d{2}";
  private static final String DATE3 = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( |\\d)\\d";
  private static final String DOMAINLABEL = "\\p{Alnum}([\\p{Alnum}-]*\\p{Alnum})?";
  private static final String HOST = "((\\p{Alnum}([\\p{Alnum}-]*\\p{Alnum})?\\.)*\\p{Alpha}([\\p{Alnum}-]*\\p{Alnum})?\\.?)|(\\d+\\.\\d+\\.\\d+\\.\\d+)";
  private static final String HOSTNAME = "(\\p{Alnum}([\\p{Alnum}-]*\\p{Alnum})?\\.)*\\p{Alpha}([\\p{Alnum}-]*\\p{Alnum})?\\.?";
  private static final String HOSTPORT = "(((\\p{Alnum}([\\p{Alnum}-]*\\p{Alnum})?\\.)*\\p{Alpha}([\\p{Alnum}-]*\\p{Alnum})?\\.?)|(\\d+\\.\\d+\\.\\d+\\.\\d+))(\\:\\d*)?";
  private static final Pattern HOSTPORT_PATTERN = Pattern.compile("(((\\p{Alnum}([\\p{Alnum}-]*\\p{Alnum})?\\.)*\\p{Alpha}([\\p{Alnum}-]*\\p{Alnum})?\\.?)|(\\d+\\.\\d+\\.\\d+\\.\\d+))(\\:\\d*)?");
  private static final String HTTP_DATE = "((Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), (\\d{2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\\d{2}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Mon|Tue|Wed|Thu|Fri|Sat|Sun) ((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( |\\d)\\d) (\\d{2}:\\d{2}:\\d{2}) \\d{4})";
  private static final String IPV4ADDRESS = "\\d+\\.\\d+\\.\\d+\\.\\d+";
  private static final String MONTH = "Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec";
  private static final String PORT = "\\d*";
  private static final String RFC1123_DATE = "(Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4}) (\\d{2}:\\d{2}:\\d{2}) GMT";
  private static final String RFC850_DATE = "(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), (\\d{2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\\d{2}) (\\d{2}:\\d{2}:\\d{2}) GMT";
  private static final String TIME = "\\d{2}:\\d{2}:\\d{2}";
  private static final String TOPLABEL = "\\p{Alpha}([\\p{Alnum}-]*\\p{Alnum})?";
  private static final String WARN_DATE = "\"(((Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), (\\d{2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\\d{2}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Mon|Tue|Wed|Thu|Fri|Sat|Sun) ((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( |\\d)\\d) (\\d{2}:\\d{2}:\\d{2}) \\d{4}))\"";
  private static final Pattern WARN_DATE_PATTERN = Pattern.compile("\"(((Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), (\\d{2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\\d{2}) (\\d{2}:\\d{2}:\\d{2}) GMT)|((Mon|Tue|Wed|Thu|Fri|Sat|Sun) ((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( |\\d)\\d) (\\d{2}:\\d{2}:\\d{2}) \\d{4}))\"");
  private static final String WEEKDAY = "Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday";
  private static final String WKDAY = "Mon|Tue|Wed|Thu|Fri|Sat|Sun";
  private int init_offs;
  private int offs;
  private final String src;
  private String warnAgent;
  private int warnCode;
  private Date warnDate;
  private String warnText;
  
  WarningValue(String paramString)
  {
    this(paramString, 0);
  }
  
  WarningValue(String paramString, int paramInt)
  {
    this.init_offs = paramInt;
    this.offs = paramInt;
    this.src = paramString;
    consumeWarnValue();
  }
  
  public static WarningValue[] getWarningValues(Header paramHeader)
  {
    ArrayList localArrayList = new ArrayList();
    paramHeader = paramHeader.getValue();
    int i = 0;
    for (;;)
    {
      if (i < paramHeader.length()) {
        try
        {
          WarningValue localWarningValue = new cz/msebera/android/httpclient/impl/client/cache/WarningValue;
          localWarningValue.<init>(paramHeader, i);
          localArrayList.add(localWarningValue);
          int j = localWarningValue.offs;
          i = j;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          i = paramHeader.indexOf(',', i);
          if (i != -1) {
            i++;
          }
        }
      }
    }
    return (WarningValue[])localArrayList.toArray(new WarningValue[0]);
  }
  
  private boolean isChar(char paramChar)
  {
    boolean bool;
    if ((paramChar >= 0) && (paramChar <= '')) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean isControl(char paramChar)
  {
    boolean bool;
    if ((paramChar != '') && ((paramChar < 0) || (paramChar > '\037'))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isSeparator(char paramChar)
  {
    boolean bool;
    if ((paramChar != '(') && (paramChar != ')') && (paramChar != '<') && (paramChar != '>') && (paramChar != '@') && (paramChar != ',') && (paramChar != ';') && (paramChar != ':') && (paramChar != '\\') && (paramChar != '"') && (paramChar != '/') && (paramChar != '[') && (paramChar != ']') && (paramChar != '?') && (paramChar != '=') && (paramChar != '{') && (paramChar != '}') && (paramChar != ' ') && (paramChar != '\t')) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isTokenChar(char paramChar)
  {
    boolean bool;
    if ((isChar(paramChar)) && (!isControl(paramChar)) && (!isSeparator(paramChar))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void parseError()
  {
    String str = this.src.substring(this.init_offs);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Bad warn code \"");
    localStringBuilder.append(str);
    localStringBuilder.append("\"");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  protected void consumeCharacter(char paramChar)
  {
    if ((this.offs + 1 > this.src.length()) || (paramChar != this.src.charAt(this.offs))) {
      parseError();
    }
    this.offs += 1;
  }
  
  protected void consumeHostPort()
  {
    Matcher localMatcher = HOSTPORT_PATTERN.matcher(this.src.substring(this.offs));
    if (!localMatcher.find()) {
      parseError();
    }
    if (localMatcher.start() != 0) {
      parseError();
    }
    this.offs += localMatcher.end();
  }
  
  protected void consumeLinearWhitespace()
  {
    while (this.offs < this.src.length())
    {
      int i = this.src.charAt(this.offs);
      if (i != 9) {
        if (i != 13)
        {
          if (i == 32) {}
        }
        else if ((this.offs + 2 < this.src.length()) && (this.src.charAt(this.offs + 1) == '\n') && ((this.src.charAt(this.offs + 2) == ' ') || (this.src.charAt(this.offs + 2) == '\t'))) {
          this.offs += 2;
        } else {
          return;
        }
      }
      this.offs += 1;
    }
  }
  
  protected void consumeQuotedString()
  {
    if (this.src.charAt(this.offs) != '"') {
      parseError();
    }
    this.offs += 1;
    int i = 0;
    while ((this.offs < this.src.length()) && (i == 0))
    {
      char c = this.src.charAt(this.offs);
      if ((this.offs + 1 < this.src.length()) && (c == '\\') && (isChar(this.src.charAt(this.offs + 1))))
      {
        this.offs += 2;
      }
      else if (c == '"')
      {
        this.offs += 1;
        i = 1;
      }
      else if ((c != '"') && (!isControl(c)))
      {
        this.offs += 1;
      }
      else
      {
        parseError();
      }
    }
    if (i == 0) {
      parseError();
    }
  }
  
  protected void consumeToken()
  {
    if (!isTokenChar(this.src.charAt(this.offs))) {
      parseError();
    }
    while ((this.offs < this.src.length()) && (isTokenChar(this.src.charAt(this.offs)))) {
      this.offs += 1;
    }
  }
  
  protected void consumeWarnAgent()
  {
    int i = this.offs;
    try
    {
      consumeHostPort();
      this.warnAgent = this.src.substring(i, this.offs);
      consumeCharacter(' ');
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      this.offs = i;
      consumeToken();
      this.warnAgent = this.src.substring(i, this.offs);
      consumeCharacter(' ');
    }
  }
  
  protected void consumeWarnCode()
  {
    if ((this.offs + 4 > this.src.length()) || (!Character.isDigit(this.src.charAt(this.offs))) || (!Character.isDigit(this.src.charAt(this.offs + 1))) || (!Character.isDigit(this.src.charAt(this.offs + 2))) || (this.src.charAt(this.offs + 3) != ' ')) {
      parseError();
    }
    String str = this.src;
    int i = this.offs;
    this.warnCode = Integer.parseInt(str.substring(i, i + 3));
    this.offs += 4;
  }
  
  protected void consumeWarnDate()
  {
    int i = this.offs;
    Matcher localMatcher = WARN_DATE_PATTERN.matcher(this.src.substring(i));
    if (!localMatcher.lookingAt()) {
      parseError();
    }
    this.offs += localMatcher.end();
    this.warnDate = DateUtils.parseDate(this.src.substring(i + 1, this.offs - 1));
  }
  
  protected void consumeWarnText()
  {
    int i = this.offs;
    consumeQuotedString();
    this.warnText = this.src.substring(i, this.offs);
  }
  
  protected void consumeWarnValue()
  {
    consumeLinearWhitespace();
    consumeWarnCode();
    consumeWarnAgent();
    consumeWarnText();
    if ((this.offs + 1 < this.src.length()) && (this.src.charAt(this.offs) == ' ') && (this.src.charAt(this.offs + 1) == '"'))
    {
      consumeCharacter(' ');
      consumeWarnDate();
    }
    consumeLinearWhitespace();
    if (this.offs != this.src.length()) {
      consumeCharacter(',');
    }
  }
  
  public String getWarnAgent()
  {
    return this.warnAgent;
  }
  
  public int getWarnCode()
  {
    return this.warnCode;
  }
  
  public Date getWarnDate()
  {
    return this.warnDate;
  }
  
  public String getWarnText()
  {
    return this.warnText;
  }
  
  public String toString()
  {
    if (this.warnDate != null) {
      return String.format("%d %s %s \"%s\"", new Object[] { Integer.valueOf(this.warnCode), this.warnAgent, this.warnText, DateUtils.formatDate(this.warnDate) });
    }
    return String.format("%d %s %s", new Object[] { Integer.valueOf(this.warnCode), this.warnAgent, this.warnText });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/WarningValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */