package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class FailureCacheValue
{
  private final long creationTimeInNanos = System.nanoTime();
  private final int errorCount;
  private final String key;
  
  public FailureCacheValue(String paramString, int paramInt)
  {
    this.key = paramString;
    this.errorCount = paramInt;
  }
  
  public long getCreationTimeInNanos()
  {
    return this.creationTimeInNanos;
  }
  
  public int getErrorCount()
  {
    return this.errorCount;
  }
  
  public String getKey()
  {
    return this.key;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[entry creationTimeInNanos=");
    localStringBuilder.append(this.creationTimeInNanos);
    localStringBuilder.append("; ");
    localStringBuilder.append("key=");
    localStringBuilder.append(this.key);
    localStringBuilder.append("; errorCount=");
    localStringBuilder.append(this.errorCount);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/FailureCacheValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */