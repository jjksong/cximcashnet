package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.InputLimit;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Immutable
public class FileResourceFactory
  implements ResourceFactory
{
  private final File cacheDir;
  private final BasicIdGenerator idgen;
  
  public FileResourceFactory(File paramFile)
  {
    this.cacheDir = paramFile;
    this.idgen = new BasicIdGenerator();
  }
  
  private File generateUniqueCacheFile(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    this.idgen.generate(localStringBuilder);
    localStringBuilder.append('.');
    int j = Math.min(paramString.length(), 100);
    for (int i = 0; i < j; i++)
    {
      char c = paramString.charAt(i);
      if ((!Character.isLetterOrDigit(c)) && (c != '.')) {
        localStringBuilder.append('-');
      } else {
        localStringBuilder.append(c);
      }
    }
    return new File(this.cacheDir, localStringBuilder.toString());
  }
  
  public Resource copy(String paramString, Resource paramResource)
    throws IOException
  {
    File localFile = generateUniqueCacheFile(paramString);
    if ((paramResource instanceof FileResource))
    {
      IOUtils.copyFile(((FileResource)paramResource).getFile(), localFile);
    }
    else
    {
      paramString = new FileOutputStream(localFile);
      IOUtils.copyAndClose(paramResource.getInputStream(), paramString);
    }
    return new FileResource(localFile);
  }
  
  public Resource generate(String paramString, InputStream paramInputStream, InputLimit paramInputLimit)
    throws IOException
  {
    File localFile = generateUniqueCacheFile(paramString);
    paramString = new FileOutputStream(localFile);
    try
    {
      byte[] arrayOfByte = new byte['ࠀ'];
      long l1 = 0L;
      long l2;
      do
      {
        do
        {
          int i = paramInputStream.read(arrayOfByte);
          if (i == -1) {
            break;
          }
          paramString.write(arrayOfByte, 0, i);
          l2 = l1 + i;
          l1 = l2;
        } while (paramInputLimit == null);
        l1 = l2;
      } while (l2 <= paramInputLimit.getValue());
      paramInputLimit.reached();
      return new FileResource(localFile);
    }
    finally
    {
      paramString.close();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/FileResourceFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */