package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.message.BasicHttpResponse;
import java.util.Date;

@Immutable
class CachedHttpResponseGenerator
{
  private final CacheValidityPolicy validityStrategy;
  
  CachedHttpResponseGenerator()
  {
    this(new CacheValidityPolicy());
  }
  
  CachedHttpResponseGenerator(CacheValidityPolicy paramCacheValidityPolicy)
  {
    this.validityStrategy = paramCacheValidityPolicy;
  }
  
  private void addMissingContentLengthHeader(HttpResponse paramHttpResponse, HttpEntity paramHttpEntity)
  {
    if (transferEncodingIsPresent(paramHttpResponse)) {
      return;
    }
    if (paramHttpResponse.getFirstHeader("Content-Length") == null) {
      paramHttpResponse.setHeader(new BasicHeader("Content-Length", Long.toString(paramHttpEntity.getContentLength())));
    }
  }
  
  private boolean transferEncodingIsPresent(HttpResponse paramHttpResponse)
  {
    boolean bool;
    if (paramHttpResponse.getFirstHeader("Transfer-Encoding") != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  CloseableHttpResponse generateNotModifiedResponse(HttpCacheEntry paramHttpCacheEntry)
  {
    BasicHttpResponse localBasicHttpResponse = new BasicHttpResponse(HttpVersion.HTTP_1_1, 304, "Not Modified");
    Header localHeader = paramHttpCacheEntry.getFirstHeader("Date");
    Object localObject = localHeader;
    if (localHeader == null) {
      localObject = new BasicHeader("Date", DateUtils.formatDate(new Date()));
    }
    localBasicHttpResponse.addHeader((Header)localObject);
    localObject = paramHttpCacheEntry.getFirstHeader("ETag");
    if (localObject != null) {
      localBasicHttpResponse.addHeader((Header)localObject);
    }
    localObject = paramHttpCacheEntry.getFirstHeader("Content-Location");
    if (localObject != null) {
      localBasicHttpResponse.addHeader((Header)localObject);
    }
    localObject = paramHttpCacheEntry.getFirstHeader("Expires");
    if (localObject != null) {
      localBasicHttpResponse.addHeader((Header)localObject);
    }
    localObject = paramHttpCacheEntry.getFirstHeader("Cache-Control");
    if (localObject != null) {
      localBasicHttpResponse.addHeader((Header)localObject);
    }
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Vary");
    if (paramHttpCacheEntry != null) {
      localBasicHttpResponse.addHeader(paramHttpCacheEntry);
    }
    return Proxies.enhanceResponse(localBasicHttpResponse);
  }
  
  CloseableHttpResponse generateResponse(HttpCacheEntry paramHttpCacheEntry)
  {
    Date localDate = new Date();
    BasicHttpResponse localBasicHttpResponse = new BasicHttpResponse(HttpVersion.HTTP_1_1, paramHttpCacheEntry.getStatusCode(), paramHttpCacheEntry.getReasonPhrase());
    localBasicHttpResponse.setHeaders(paramHttpCacheEntry.getAllHeaders());
    if (paramHttpCacheEntry.getResource() != null)
    {
      CacheEntity localCacheEntity = new CacheEntity(paramHttpCacheEntry);
      addMissingContentLengthHeader(localBasicHttpResponse, localCacheEntity);
      localBasicHttpResponse.setEntity(localCacheEntity);
    }
    long l = this.validityStrategy.getCurrentAgeSecs(paramHttpCacheEntry, localDate);
    if (l > 0L) {
      if (l >= 2147483647L)
      {
        localBasicHttpResponse.setHeader("Age", "2147483648");
      }
      else
      {
        paramHttpCacheEntry = new StringBuilder();
        paramHttpCacheEntry.append("");
        paramHttpCacheEntry.append((int)l);
        localBasicHttpResponse.setHeader("Age", paramHttpCacheEntry.toString());
      }
    }
    return Proxies.enhanceResponse(localBasicHttpResponse);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CachedHttpResponseGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */