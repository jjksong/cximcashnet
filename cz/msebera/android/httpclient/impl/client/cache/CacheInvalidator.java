package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheInvalidator;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

@Immutable
class CacheInvalidator
  implements HttpCacheInvalidator
{
  private final CacheKeyGenerator cacheKeyGenerator;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final HttpCacheStorage storage;
  
  public CacheInvalidator(CacheKeyGenerator paramCacheKeyGenerator, HttpCacheStorage paramHttpCacheStorage)
  {
    this.cacheKeyGenerator = paramCacheKeyGenerator;
    this.storage = paramHttpCacheStorage;
  }
  
  private void flushEntry(String paramString)
  {
    try
    {
      this.storage.removeEntry(paramString);
    }
    catch (IOException paramString)
    {
      this.log.warn("unable to flush cache entry", paramString);
    }
  }
  
  private void flushLocationCacheEntry(URL paramURL1, HttpResponse paramHttpResponse, URL paramURL2)
  {
    HttpCacheEntry localHttpCacheEntry = getEntry(this.cacheKeyGenerator.canonicalizeUri(paramURL2.toString()));
    if (localHttpCacheEntry == null) {
      return;
    }
    if (responseDateOlderThanEntryDate(paramHttpResponse, localHttpCacheEntry)) {
      return;
    }
    if (!responseAndEntryEtagsDiffer(paramHttpResponse, localHttpCacheEntry)) {
      return;
    }
    flushUriIfSameHost(paramURL1, paramURL2);
  }
  
  private URL getAbsoluteURL(String paramString)
  {
    try
    {
      URL localURL = new java/net/URL;
      localURL.<init>(paramString);
      paramString = localURL;
    }
    catch (MalformedURLException paramString)
    {
      paramString = null;
    }
    return paramString;
  }
  
  private URL getContentLocationURL(URL paramURL, HttpResponse paramHttpResponse)
  {
    paramHttpResponse = paramHttpResponse.getFirstHeader("Content-Location");
    if (paramHttpResponse == null) {
      return null;
    }
    paramHttpResponse = paramHttpResponse.getValue();
    URL localURL = getAbsoluteURL(paramHttpResponse);
    if (localURL != null) {
      return localURL;
    }
    return getRelativeURL(paramURL, paramHttpResponse);
  }
  
  private HttpCacheEntry getEntry(String paramString)
  {
    try
    {
      paramString = this.storage.getEntry(paramString);
      return paramString;
    }
    catch (IOException paramString)
    {
      this.log.warn("could not retrieve entry from storage", paramString);
    }
    return null;
  }
  
  private URL getLocationURL(URL paramURL, HttpResponse paramHttpResponse)
  {
    paramHttpResponse = paramHttpResponse.getFirstHeader("Location");
    if (paramHttpResponse == null) {
      return null;
    }
    String str = paramHttpResponse.getValue();
    paramHttpResponse = getAbsoluteURL(str);
    if (paramHttpResponse != null) {
      return paramHttpResponse;
    }
    return getRelativeURL(paramURL, str);
  }
  
  private URL getRelativeURL(URL paramURL, String paramString)
  {
    try
    {
      URL localURL = new java/net/URL;
      localURL.<init>(paramURL, paramString);
      paramURL = localURL;
    }
    catch (MalformedURLException paramURL)
    {
      paramURL = null;
    }
    return paramURL;
  }
  
  private boolean notGetOrHeadRequest(String paramString)
  {
    boolean bool;
    if ((!"GET".equals(paramString)) && (!"HEAD".equals(paramString))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean responseAndEntryEtagsDiffer(HttpResponse paramHttpResponse, HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("ETag");
    paramHttpResponse = paramHttpResponse.getFirstHeader("ETag");
    if ((paramHttpCacheEntry != null) && (paramHttpResponse != null)) {
      return paramHttpCacheEntry.getValue().equals(paramHttpResponse.getValue()) ^ true;
    }
    return false;
  }
  
  private boolean responseDateOlderThanEntryDate(HttpResponse paramHttpResponse, HttpCacheEntry paramHttpCacheEntry)
  {
    Header localHeader = paramHttpCacheEntry.getFirstHeader("Date");
    paramHttpCacheEntry = paramHttpResponse.getFirstHeader("Date");
    if ((localHeader != null) && (paramHttpCacheEntry != null))
    {
      paramHttpResponse = DateUtils.parseDate(localHeader.getValue());
      paramHttpCacheEntry = DateUtils.parseDate(paramHttpCacheEntry.getValue());
      if ((paramHttpResponse != null) && (paramHttpCacheEntry != null)) {
        return paramHttpCacheEntry.before(paramHttpResponse);
      }
      return false;
    }
    return false;
  }
  
  protected boolean flushAbsoluteUriFromSameHost(URL paramURL, String paramString)
  {
    paramString = getAbsoluteURL(paramString);
    if (paramString == null) {
      return false;
    }
    flushUriIfSameHost(paramURL, paramString);
    return true;
  }
  
  public void flushInvalidatedCacheEntries(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
  {
    if (requestShouldNotBeCached(paramHttpRequest))
    {
      this.log.debug("Request should not be cached");
      paramHttpHost = this.cacheKeyGenerator.getURI(paramHttpHost, paramHttpRequest);
      HttpCacheEntry localHttpCacheEntry = getEntry(paramHttpHost);
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("parent entry: ");
      ((StringBuilder)localObject).append(localHttpCacheEntry);
      localHttpClientAndroidLog.debug(((StringBuilder)localObject).toString());
      if (localHttpCacheEntry != null)
      {
        localObject = localHttpCacheEntry.getVariantMap().values().iterator();
        while (((Iterator)localObject).hasNext()) {
          flushEntry((String)((Iterator)localObject).next());
        }
        flushEntry(paramHttpHost);
      }
      paramHttpHost = getAbsoluteURL(paramHttpHost);
      if (paramHttpHost == null)
      {
        this.log.error("Couldn't transform request into valid URL");
        return;
      }
      localObject = paramHttpRequest.getFirstHeader("Content-Location");
      if (localObject != null)
      {
        localObject = ((Header)localObject).getValue();
        if (!flushAbsoluteUriFromSameHost(paramHttpHost, (String)localObject)) {
          flushRelativeUriFromSameHost(paramHttpHost, (String)localObject);
        }
      }
      paramHttpRequest = paramHttpRequest.getFirstHeader("Location");
      if (paramHttpRequest != null) {
        flushAbsoluteUriFromSameHost(paramHttpHost, paramHttpRequest.getValue());
      }
    }
  }
  
  public void flushInvalidatedCacheEntries(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    if ((i >= 200) && (i <= 299))
    {
      paramHttpHost = getAbsoluteURL(this.cacheKeyGenerator.getURI(paramHttpHost, paramHttpRequest));
      if (paramHttpHost == null) {
        return;
      }
      paramHttpRequest = getContentLocationURL(paramHttpHost, paramHttpResponse);
      if (paramHttpRequest != null) {
        flushLocationCacheEntry(paramHttpHost, paramHttpResponse, paramHttpRequest);
      }
      paramHttpRequest = getLocationURL(paramHttpHost, paramHttpResponse);
      if (paramHttpRequest != null) {
        flushLocationCacheEntry(paramHttpHost, paramHttpResponse, paramHttpRequest);
      }
      return;
    }
  }
  
  protected void flushRelativeUriFromSameHost(URL paramURL, String paramString)
  {
    paramString = getRelativeURL(paramURL, paramString);
    if (paramString == null) {
      return;
    }
    flushUriIfSameHost(paramURL, paramString);
  }
  
  protected void flushUriIfSameHost(URL paramURL1, URL paramURL2)
  {
    paramURL2 = getAbsoluteURL(this.cacheKeyGenerator.canonicalizeUri(paramURL2.toString()));
    if (paramURL2 == null) {
      return;
    }
    if (paramURL2.getAuthority().equalsIgnoreCase(paramURL1.getAuthority())) {
      flushEntry(paramURL2.toString());
    }
  }
  
  protected boolean requestShouldNotBeCached(HttpRequest paramHttpRequest)
  {
    return notGetOrHeadRequest(paramHttpRequest.getRequestLine().getMethod());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheInvalidator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */