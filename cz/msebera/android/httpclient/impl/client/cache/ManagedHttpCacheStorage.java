package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.cache.HttpCacheUpdateCallback;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.util.Args;
import java.io.Closeable;
import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@ThreadSafe
public class ManagedHttpCacheStorage
  implements HttpCacheStorage, Closeable
{
  private final AtomicBoolean active;
  private final CacheMap entries;
  private final ReferenceQueue<HttpCacheEntry> morque;
  private final Set<ResourceReference> resources;
  
  public ManagedHttpCacheStorage(CacheConfig paramCacheConfig)
  {
    this.entries = new CacheMap(paramCacheConfig.getMaxCacheEntries());
    this.morque = new ReferenceQueue();
    this.resources = new HashSet();
    this.active = new AtomicBoolean(true);
  }
  
  private void ensureValidState()
    throws IllegalStateException
  {
    if (this.active.get()) {
      return;
    }
    throw new IllegalStateException("Cache has been shut down");
  }
  
  private void keepResourceReference(HttpCacheEntry paramHttpCacheEntry)
  {
    if (paramHttpCacheEntry.getResource() != null)
    {
      paramHttpCacheEntry = new ResourceReference(paramHttpCacheEntry, this.morque);
      this.resources.add(paramHttpCacheEntry);
    }
  }
  
  public void cleanResources()
  {
    if (this.active.get()) {
      for (;;)
      {
        ResourceReference localResourceReference = (ResourceReference)this.morque.poll();
        if (localResourceReference != null) {
          try
          {
            this.resources.remove(localResourceReference);
            localResourceReference.getResource().dispose();
          }
          finally {}
        }
      }
    }
  }
  
  public void close()
  {
    shutdown();
  }
  
  public HttpCacheEntry getEntry(String paramString)
    throws IOException
  {
    Args.notNull(paramString, "URL");
    ensureValidState();
    try
    {
      paramString = (HttpCacheEntry)this.entries.get(paramString);
      return paramString;
    }
    finally {}
  }
  
  public void putEntry(String paramString, HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    Args.notNull(paramString, "URL");
    Args.notNull(paramHttpCacheEntry, "Cache entry");
    ensureValidState();
    try
    {
      this.entries.put(paramString, paramHttpCacheEntry);
      keepResourceReference(paramHttpCacheEntry);
      return;
    }
    finally {}
  }
  
  public void removeEntry(String paramString)
    throws IOException
  {
    Args.notNull(paramString, "URL");
    ensureValidState();
    try
    {
      this.entries.remove(paramString);
      return;
    }
    finally {}
  }
  
  public void shutdown()
  {
    if (this.active.compareAndSet(true, false)) {
      try
      {
        this.entries.clear();
        Iterator localIterator = this.resources.iterator();
        while (localIterator.hasNext()) {
          ((ResourceReference)localIterator.next()).getResource().dispose();
        }
        this.resources.clear();
        while (this.morque.poll() != null) {}
      }
      finally {}
    }
  }
  
  public void updateEntry(String paramString, HttpCacheUpdateCallback paramHttpCacheUpdateCallback)
    throws IOException
  {
    Args.notNull(paramString, "URL");
    Args.notNull(paramHttpCacheUpdateCallback, "Callback");
    ensureValidState();
    try
    {
      HttpCacheEntry localHttpCacheEntry = (HttpCacheEntry)this.entries.get(paramString);
      paramHttpCacheUpdateCallback = paramHttpCacheUpdateCallback.update(localHttpCacheEntry);
      this.entries.put(paramString, paramHttpCacheUpdateCallback);
      if (localHttpCacheEntry != paramHttpCacheUpdateCallback) {
        keepResourceReference(paramHttpCacheUpdateCallback);
      }
      return;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ManagedHttpCacheStorage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */