package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.IOException;

class AsynchronousValidationRequest
  implements Runnable
{
  private final HttpCacheEntry cacheEntry;
  private final CachingExec cachingExec;
  private final int consecutiveFailedAttempts;
  private final HttpClientContext context;
  private final HttpExecutionAware execAware;
  private final String identifier;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final AsynchronousValidator parent;
  private final HttpRequestWrapper request;
  private final HttpRoute route;
  
  AsynchronousValidationRequest(AsynchronousValidator paramAsynchronousValidator, CachingExec paramCachingExec, HttpRoute paramHttpRoute, HttpRequestWrapper paramHttpRequestWrapper, HttpClientContext paramHttpClientContext, HttpExecutionAware paramHttpExecutionAware, HttpCacheEntry paramHttpCacheEntry, String paramString, int paramInt)
  {
    this.parent = paramAsynchronousValidator;
    this.cachingExec = paramCachingExec;
    this.route = paramHttpRoute;
    this.request = paramHttpRequestWrapper;
    this.context = paramHttpClientContext;
    this.execAware = paramHttpExecutionAware;
    this.cacheEntry = paramHttpCacheEntry;
    this.identifier = paramString;
    this.consecutiveFailedAttempts = paramInt;
  }
  
  private boolean isNotServerError(int paramInt)
  {
    boolean bool;
    if (paramInt < 500) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean isNotStale(HttpResponse paramHttpResponse)
  {
    Header[] arrayOfHeader = paramHttpResponse.getHeaders("Warning");
    if (arrayOfHeader != null)
    {
      int j = arrayOfHeader.length;
      int i = 0;
      while (i < j)
      {
        paramHttpResponse = arrayOfHeader[i].getValue();
        if ((!paramHttpResponse.startsWith("110")) && (!paramHttpResponse.startsWith("111"))) {
          i++;
        } else {
          return false;
        }
      }
    }
    return true;
  }
  
  public int getConsecutiveFailedAttempts()
  {
    return this.consecutiveFailedAttempts;
  }
  
  String getIdentifier()
  {
    return this.identifier;
  }
  
  protected boolean revalidateCacheEntry()
  {
    try
    {
      Object localObject2 = this.cachingExec.revalidateCacheEntry(this.route, this.request, this.context, this.execAware, this.cacheEntry);
      try
      {
        if (isNotServerError(((CloseableHttpResponse)localObject2).getStatusLine().getStatusCode()))
        {
          bool = isNotStale((HttpResponse)localObject2);
          if (bool)
          {
            bool = true;
            break label63;
          }
        }
        boolean bool = false;
        label63:
        return bool;
      }
      finally
      {
        ((CloseableHttpResponse)localObject2).close();
      }
      StringBuilder localStringBuilder;
      return false;
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject2 = this.log;
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("RuntimeException thrown during asynchronous revalidation: ");
      localStringBuilder.append(localRuntimeException);
      ((HttpClientAndroidLog)localObject2).error(localStringBuilder.toString());
      return false;
    }
    catch (HttpException localHttpException)
    {
      this.log.error("HTTP protocol exception during asynchronous revalidation", localHttpException);
      return false;
    }
    catch (IOException localIOException)
    {
      this.log.debug("Asynchronous revalidation failed due to I/O error", localIOException);
    }
  }
  
  public void run()
  {
    try
    {
      if (revalidateCacheEntry()) {
        this.parent.jobSuccessful(this.identifier);
      } else {
        this.parent.jobFailed(this.identifier);
      }
      return;
    }
    finally
    {
      this.parent.markComplete(this.identifier);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/AsynchronousValidationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */