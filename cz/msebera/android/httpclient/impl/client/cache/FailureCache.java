package cz.msebera.android.httpclient.impl.client.cache;

public abstract interface FailureCache
{
  public abstract int getErrorCount(String paramString);
  
  public abstract void increaseErrorCount(String paramString);
  
  public abstract void resetErrorCount(String paramString);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/FailureCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */