package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@ThreadSafe
public class DefaultFailureCache
  implements FailureCache
{
  static final int DEFAULT_MAX_SIZE = 1000;
  static final int MAX_UPDATE_TRIES = 10;
  private final int maxSize;
  private final ConcurrentMap<String, FailureCacheValue> storage;
  
  public DefaultFailureCache()
  {
    this(1000);
  }
  
  public DefaultFailureCache(int paramInt)
  {
    this.maxSize = paramInt;
    this.storage = new ConcurrentHashMap();
  }
  
  private FailureCacheValue findValueWithOldestTimestamp()
  {
    Iterator localIterator = this.storage.entrySet().iterator();
    long l1 = Long.MAX_VALUE;
    FailureCacheValue localFailureCacheValue = null;
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      long l2 = ((FailureCacheValue)localEntry.getValue()).getCreationTimeInNanos();
      if (l2 < l1)
      {
        localFailureCacheValue = (FailureCacheValue)localEntry.getValue();
        l1 = l2;
      }
    }
    return localFailureCacheValue;
  }
  
  private void removeOldestEntryIfMapSizeExceeded()
  {
    if (this.storage.size() > this.maxSize)
    {
      FailureCacheValue localFailureCacheValue = findValueWithOldestTimestamp();
      if (localFailureCacheValue != null) {
        this.storage.remove(localFailureCacheValue.getKey(), localFailureCacheValue);
      }
    }
  }
  
  private void updateValue(String paramString)
  {
    for (int i = 0; i < 10; i++)
    {
      FailureCacheValue localFailureCacheValue1 = (FailureCacheValue)this.storage.get(paramString);
      if (localFailureCacheValue1 == null)
      {
        localFailureCacheValue1 = new FailureCacheValue(paramString, 1);
        if (this.storage.putIfAbsent(paramString, localFailureCacheValue1) != null) {}
      }
      else
      {
        int j = localFailureCacheValue1.getErrorCount();
        if (j == Integer.MAX_VALUE) {
          return;
        }
        FailureCacheValue localFailureCacheValue2 = new FailureCacheValue(paramString, j + 1);
        if (this.storage.replace(paramString, localFailureCacheValue1, localFailureCacheValue2)) {
          return;
        }
      }
    }
  }
  
  public int getErrorCount(String paramString)
  {
    if (paramString != null)
    {
      paramString = (FailureCacheValue)this.storage.get(paramString);
      int i;
      if (paramString != null) {
        i = paramString.getErrorCount();
      } else {
        i = 0;
      }
      return i;
    }
    throw new IllegalArgumentException("identifier may not be null");
  }
  
  public void increaseErrorCount(String paramString)
  {
    if (paramString != null)
    {
      updateValue(paramString);
      removeOldestEntryIfMapSizeExceeded();
      return;
    }
    throw new IllegalArgumentException("identifier may not be null");
  }
  
  public void resetErrorCount(String paramString)
  {
    if (paramString != null)
    {
      this.storage.remove(paramString);
      return;
    }
    throw new IllegalArgumentException("identifier may not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/DefaultFailureCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */