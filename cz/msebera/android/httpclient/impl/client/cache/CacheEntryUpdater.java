package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.ResourceFactory;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

@Immutable
class CacheEntryUpdater
{
  private final ResourceFactory resourceFactory;
  
  CacheEntryUpdater()
  {
    this(new HeapResourceFactory());
  }
  
  CacheEntryUpdater(ResourceFactory paramResourceFactory)
  {
    this.resourceFactory = paramResourceFactory;
  }
  
  private boolean entryAndResponseHaveDateHeader(HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse)
  {
    return (paramHttpCacheEntry.getFirstHeader("Date") != null) && (paramHttpResponse.getFirstHeader("Date") != null);
  }
  
  private boolean entryDateHeaderNewerThenResponse(HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse)
  {
    paramHttpCacheEntry = DateUtils.parseDate(paramHttpCacheEntry.getFirstHeader("Date").getValue());
    paramHttpResponse = DateUtils.parseDate(paramHttpResponse.getFirstHeader("Date").getValue());
    if ((paramHttpCacheEntry != null) && (paramHttpResponse != null)) {
      return paramHttpCacheEntry.after(paramHttpResponse);
    }
    return false;
  }
  
  private void removeCacheEntry1xxWarnings(List<Header> paramList, HttpCacheEntry paramHttpCacheEntry)
  {
    ListIterator localListIterator = paramList.listIterator();
    while (localListIterator.hasNext()) {
      if ("Warning".equals(((Header)localListIterator.next()).getName()))
      {
        paramList = paramHttpCacheEntry.getHeaders("Warning");
        int j = paramList.length;
        for (int i = 0; i < j; i++) {
          if (paramList[i].getValue().startsWith("1")) {
            localListIterator.remove();
          }
        }
      }
    }
  }
  
  private void removeCacheHeadersThatMatchResponse(List<Header> paramList, HttpResponse paramHttpResponse)
  {
    for (Header localHeader : paramHttpResponse.getAllHeaders())
    {
      paramHttpResponse = paramList.listIterator();
      while (paramHttpResponse.hasNext()) {
        if (((Header)paramHttpResponse.next()).getName().equals(localHeader.getName())) {
          paramHttpResponse.remove();
        }
      }
    }
  }
  
  protected Header[] mergeHeaders(HttpCacheEntry paramHttpCacheEntry, HttpResponse paramHttpResponse)
  {
    if ((entryAndResponseHaveDateHeader(paramHttpCacheEntry, paramHttpResponse)) && (entryDateHeaderNewerThenResponse(paramHttpCacheEntry, paramHttpResponse))) {
      return paramHttpCacheEntry.getAllHeaders();
    }
    ArrayList localArrayList = new ArrayList(Arrays.asList(paramHttpCacheEntry.getAllHeaders()));
    removeCacheHeadersThatMatchResponse(localArrayList, paramHttpResponse);
    removeCacheEntry1xxWarnings(localArrayList, paramHttpCacheEntry);
    localArrayList.addAll(Arrays.asList(paramHttpResponse.getAllHeaders()));
    return (Header[])localArrayList.toArray(new Header[localArrayList.size()]);
  }
  
  public HttpCacheEntry updateCacheEntry(String paramString, HttpCacheEntry paramHttpCacheEntry, Date paramDate1, Date paramDate2, HttpResponse paramHttpResponse)
    throws IOException
  {
    boolean bool;
    if (paramHttpResponse.getStatusLine().getStatusCode() == 304) {
      bool = true;
    } else {
      bool = false;
    }
    Args.check(bool, "Response must have 304 status code");
    paramHttpResponse = mergeHeaders(paramHttpCacheEntry, paramHttpResponse);
    if (paramHttpCacheEntry.getResource() != null) {
      paramString = this.resourceFactory.copy(paramString, paramHttpCacheEntry.getResource());
    } else {
      paramString = null;
    }
    return new HttpCacheEntry(paramDate1, paramDate2, paramHttpCacheEntry.getStatusLine(), paramHttpResponse, paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheEntryUpdater.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */