package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Immutable
class ConditionalRequestBuilder
{
  public HttpRequestWrapper buildConditionalRequest(HttpRequestWrapper paramHttpRequestWrapper, HttpCacheEntry paramHttpCacheEntry)
    throws ProtocolException
  {
    HttpRequestWrapper localHttpRequestWrapper = HttpRequestWrapper.wrap(paramHttpRequestWrapper.getOriginal());
    localHttpRequestWrapper.setHeaders(paramHttpRequestWrapper.getAllHeaders());
    paramHttpRequestWrapper = paramHttpCacheEntry.getFirstHeader("ETag");
    if (paramHttpRequestWrapper != null) {
      localHttpRequestWrapper.setHeader("If-None-Match", paramHttpRequestWrapper.getValue());
    }
    paramHttpRequestWrapper = paramHttpCacheEntry.getFirstHeader("Last-Modified");
    if (paramHttpRequestWrapper != null) {
      localHttpRequestWrapper.setHeader("If-Modified-Since", paramHttpRequestWrapper.getValue());
    }
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Cache-Control");
    int n = paramHttpCacheEntry.length;
    int j = 0;
    int k;
    for (int i = 0; j < n; i = k)
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpCacheEntry[j].getElements();
      int i1 = arrayOfHeaderElement.length;
      for (int m = 0;; m++)
      {
        k = i;
        if (m >= i1) {
          break label164;
        }
        paramHttpRequestWrapper = arrayOfHeaderElement[m];
        if (("must-revalidate".equalsIgnoreCase(paramHttpRequestWrapper.getName())) || ("proxy-revalidate".equalsIgnoreCase(paramHttpRequestWrapper.getName()))) {
          break;
        }
      }
      k = 1;
      label164:
      j++;
    }
    if (i != 0) {
      localHttpRequestWrapper.addHeader("Cache-Control", "max-age=0");
    }
    return localHttpRequestWrapper;
  }
  
  public HttpRequestWrapper buildConditionalRequestFromVariants(HttpRequestWrapper paramHttpRequestWrapper, Map<String, Variant> paramMap)
  {
    HttpRequestWrapper localHttpRequestWrapper = HttpRequestWrapper.wrap(paramHttpRequestWrapper.getOriginal());
    localHttpRequestWrapper.setHeaders(paramHttpRequestWrapper.getAllHeaders());
    paramHttpRequestWrapper = new StringBuilder();
    Iterator localIterator = paramMap.keySet().iterator();
    int i = 1;
    while (localIterator.hasNext())
    {
      paramMap = (String)localIterator.next();
      if (i == 0) {
        paramHttpRequestWrapper.append(",");
      }
      i = 0;
      paramHttpRequestWrapper.append(paramMap);
    }
    localHttpRequestWrapper.setHeader("If-None-Match", paramHttpRequestWrapper.toString());
    return localHttpRequestWrapper;
  }
  
  public HttpRequestWrapper buildUnconditionalRequest(HttpRequestWrapper paramHttpRequestWrapper, HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = HttpRequestWrapper.wrap(paramHttpRequestWrapper.getOriginal());
    paramHttpCacheEntry.setHeaders(paramHttpRequestWrapper.getAllHeaders());
    paramHttpCacheEntry.addHeader("Cache-Control", "no-cache");
    paramHttpCacheEntry.addHeader("Pragma", "no-cache");
    paramHttpCacheEntry.removeHeaders("If-Range");
    paramHttpCacheEntry.removeHeaders("If-Match");
    paramHttpCacheEntry.removeHeaders("If-None-Match");
    paramHttpCacheEntry.removeHeaders("If-Unmodified-Since");
    paramHttpCacheEntry.removeHeaders("If-Modified-Since");
    return paramHttpCacheEntry;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ConditionalRequestBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */