package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheStorage;
import cz.msebera.android.httpclient.client.cache.HttpCacheUpdateCallback;
import java.io.IOException;

@ThreadSafe
public class BasicHttpCacheStorage
  implements HttpCacheStorage
{
  private final CacheMap entries;
  
  public BasicHttpCacheStorage(CacheConfig paramCacheConfig)
  {
    this.entries = new CacheMap(paramCacheConfig.getMaxCacheEntries());
  }
  
  public HttpCacheEntry getEntry(String paramString)
    throws IOException
  {
    try
    {
      paramString = (HttpCacheEntry)this.entries.get(paramString);
      return paramString;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  public void putEntry(String paramString, HttpCacheEntry paramHttpCacheEntry)
    throws IOException
  {
    try
    {
      this.entries.put(paramString, paramHttpCacheEntry);
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  public void removeEntry(String paramString)
    throws IOException
  {
    try
    {
      this.entries.remove(paramString);
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  public void updateEntry(String paramString, HttpCacheUpdateCallback paramHttpCacheUpdateCallback)
    throws IOException
  {
    try
    {
      HttpCacheEntry localHttpCacheEntry = (HttpCacheEntry)this.entries.get(paramString);
      this.entries.put(paramString, paramHttpCacheUpdateCallback.update(localHttpCacheEntry));
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/BasicHttpCacheStorage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */