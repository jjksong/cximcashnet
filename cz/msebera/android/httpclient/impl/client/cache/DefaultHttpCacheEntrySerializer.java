package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntrySerializer;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

@Immutable
public class DefaultHttpCacheEntrySerializer
  implements HttpCacheEntrySerializer
{
  /* Error */
  public HttpCacheEntry readFrom(java.io.InputStream paramInputStream)
    throws IOException
  {
    // Byte code:
    //   0: new 20	java/io/ObjectInputStream
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 23	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   8: astore_1
    //   9: aload_1
    //   10: invokevirtual 27	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   13: checkcast 29	cz/msebera/android/httpclient/client/cache/HttpCacheEntry
    //   16: astore_2
    //   17: aload_1
    //   18: invokevirtual 32	java/io/ObjectInputStream:close	()V
    //   21: aload_2
    //   22: areturn
    //   23: astore_2
    //   24: goto +46 -> 70
    //   27: astore_2
    //   28: new 34	cz/msebera/android/httpclient/client/cache/HttpCacheEntrySerializationException
    //   31: astore 4
    //   33: new 36	java/lang/StringBuilder
    //   36: astore_3
    //   37: aload_3
    //   38: invokespecial 37	java/lang/StringBuilder:<init>	()V
    //   41: aload_3
    //   42: ldc 39
    //   44: invokevirtual 43	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_3
    //   49: aload_2
    //   50: invokevirtual 47	java/lang/ClassNotFoundException:getMessage	()Ljava/lang/String;
    //   53: invokevirtual 43	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload 4
    //   59: aload_3
    //   60: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   63: aload_2
    //   64: invokespecial 53	cz/msebera/android/httpclient/client/cache/HttpCacheEntrySerializationException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   67: aload 4
    //   69: athrow
    //   70: aload_1
    //   71: invokevirtual 32	java/io/ObjectInputStream:close	()V
    //   74: aload_2
    //   75: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	76	0	this	DefaultHttpCacheEntrySerializer
    //   0	76	1	paramInputStream	java.io.InputStream
    //   16	6	2	localHttpCacheEntry	HttpCacheEntry
    //   23	1	2	localObject	Object
    //   27	48	2	localClassNotFoundException	ClassNotFoundException
    //   36	24	3	localStringBuilder	StringBuilder
    //   31	37	4	localHttpCacheEntrySerializationException	cz.msebera.android.httpclient.client.cache.HttpCacheEntrySerializationException
    // Exception table:
    //   from	to	target	type
    //   9	17	23	finally
    //   28	70	23	finally
    //   9	17	27	java/lang/ClassNotFoundException
  }
  
  public void writeTo(HttpCacheEntry paramHttpCacheEntry, OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream = new ObjectOutputStream(paramOutputStream);
    try
    {
      paramOutputStream.writeObject(paramHttpCacheEntry);
      return;
    }
    finally
    {
      paramOutputStream.close();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/DefaultHttpCacheEntrySerializer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */