package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@ThreadSafe
public class ExponentialBackOffSchedulingStrategy
  implements SchedulingStrategy
{
  public static final long DEFAULT_BACK_OFF_RATE = 10L;
  public static final long DEFAULT_INITIAL_EXPIRY_IN_MILLIS = TimeUnit.SECONDS.toMillis(6L);
  public static final long DEFAULT_MAX_EXPIRY_IN_MILLIS = TimeUnit.SECONDS.toMillis(86400L);
  private final long backOffRate;
  private final ScheduledExecutorService executor;
  private final long initialExpiryInMillis;
  private final long maxExpiryInMillis;
  
  public ExponentialBackOffSchedulingStrategy(CacheConfig paramCacheConfig)
  {
    this(paramCacheConfig, 10L, DEFAULT_INITIAL_EXPIRY_IN_MILLIS, DEFAULT_MAX_EXPIRY_IN_MILLIS);
  }
  
  public ExponentialBackOffSchedulingStrategy(CacheConfig paramCacheConfig, long paramLong1, long paramLong2, long paramLong3)
  {
    this(createThreadPoolFromCacheConfig(paramCacheConfig), paramLong1, paramLong2, paramLong3);
  }
  
  ExponentialBackOffSchedulingStrategy(ScheduledExecutorService paramScheduledExecutorService, long paramLong1, long paramLong2, long paramLong3)
  {
    this.executor = ((ScheduledExecutorService)checkNotNull("executor", paramScheduledExecutorService));
    this.backOffRate = checkNotNegative("backOffRate", paramLong1);
    this.initialExpiryInMillis = checkNotNegative("initialExpiryInMillis", paramLong2);
    this.maxExpiryInMillis = checkNotNegative("maxExpiryInMillis", paramLong3);
  }
  
  protected static long checkNotNegative(String paramString, long paramLong)
  {
    if (paramLong >= 0L) {
      return paramLong;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  protected static <T> T checkNotNull(String paramString, T paramT)
  {
    if (paramT != null) {
      return paramT;
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
  
  private static ScheduledThreadPoolExecutor createThreadPoolFromCacheConfig(CacheConfig paramCacheConfig)
  {
    paramCacheConfig = new ScheduledThreadPoolExecutor(paramCacheConfig.getAsynchronousWorkersMax());
    paramCacheConfig.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
    return paramCacheConfig;
  }
  
  protected long calculateDelayInMillis(int paramInt)
  {
    if (paramInt > 0)
    {
      double d2 = this.initialExpiryInMillis;
      double d1 = Math.pow(this.backOffRate, paramInt - 1);
      Double.isNaN(d2);
      return Math.min((d2 * d1), this.maxExpiryInMillis);
    }
    return 0L;
  }
  
  public void close()
  {
    this.executor.shutdown();
  }
  
  public long getBackOffRate()
  {
    return this.backOffRate;
  }
  
  public long getInitialExpiryInMillis()
  {
    return this.initialExpiryInMillis;
  }
  
  public long getMaxExpiryInMillis()
  {
    return this.maxExpiryInMillis;
  }
  
  public void schedule(AsynchronousValidationRequest paramAsynchronousValidationRequest)
  {
    checkNotNull("revalidationRequest", paramAsynchronousValidationRequest);
    long l = calculateDelayInMillis(paramAsynchronousValidationRequest.getConsecutiveFailedAttempts());
    this.executor.schedule(paramAsynchronousValidationRequest, l, TimeUnit.MILLISECONDS);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ExponentialBackOffSchedulingStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */