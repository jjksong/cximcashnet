package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.Resource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Immutable
public class HeapResource
  implements Resource
{
  private static final long serialVersionUID = -2078599905620463394L;
  private final byte[] b;
  
  public HeapResource(byte[] paramArrayOfByte)
  {
    this.b = paramArrayOfByte;
  }
  
  public void dispose() {}
  
  byte[] getByteArray()
  {
    return this.b;
  }
  
  public InputStream getInputStream()
  {
    return new ByteArrayInputStream(this.b);
  }
  
  public long length()
  {
    return this.b.length;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/HeapResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */