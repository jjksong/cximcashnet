package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.util.Args;
import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

@Immutable
class ResourceReference
  extends PhantomReference<HttpCacheEntry>
{
  private final Resource resource;
  
  public ResourceReference(HttpCacheEntry paramHttpCacheEntry, ReferenceQueue<HttpCacheEntry> paramReferenceQueue)
  {
    super(paramHttpCacheEntry, paramReferenceQueue);
    Args.notNull(paramHttpCacheEntry.getResource(), "Resource");
    this.resource = paramHttpCacheEntry.getResource();
  }
  
  public boolean equals(Object paramObject)
  {
    return this.resource.equals(paramObject);
  }
  
  public Resource getResource()
  {
    return this.resource;
  }
  
  public int hashCode()
  {
    return this.resource.hashCode();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ResourceReference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */