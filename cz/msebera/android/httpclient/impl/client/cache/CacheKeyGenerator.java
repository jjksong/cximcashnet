package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Immutable
class CacheKeyGenerator
{
  private static final URI BASE_URI = URI.create("http://example.com/");
  
  private int canonicalizePort(int paramInt, String paramString)
  {
    if ((paramInt == -1) && ("http".equalsIgnoreCase(paramString))) {
      return 80;
    }
    if ((paramInt == -1) && ("https".equalsIgnoreCase(paramString))) {
      return 443;
    }
    return paramInt;
  }
  
  private boolean isRelativeRequest(HttpRequest paramHttpRequest)
  {
    paramHttpRequest = paramHttpRequest.getRequestLine().getUri();
    boolean bool;
    if ((!"*".equals(paramHttpRequest)) && (!paramHttpRequest.startsWith("/"))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public String canonicalizeUri(String paramString)
  {
    try
    {
      Object localObject2 = URIUtils.resolve(BASE_URI, paramString);
      Object localObject1 = new java/net/URL;
      ((URL)localObject1).<init>(((URI)localObject2).toASCIIString());
      String str1 = ((URL)localObject1).getProtocol();
      String str2 = ((URL)localObject1).getHost();
      int i = canonicalizePort(((URL)localObject1).getPort(), str1);
      localObject2 = ((URL)localObject1).getPath();
      String str3 = ((URL)localObject1).getQuery();
      localObject1 = localObject2;
      if (str3 != null)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("?");
        ((StringBuilder)localObject1).append(str3);
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      localObject2 = new java/net/URL;
      ((URL)localObject2).<init>(str1, str2, i, (String)localObject1);
      localObject1 = ((URL)localObject2).toString();
      return (String)localObject1;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      return paramString;
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    return paramString;
  }
  
  protected String getFullHeaderValue(Header[] paramArrayOfHeader)
  {
    if (paramArrayOfHeader == null) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder("");
    int k = paramArrayOfHeader.length;
    int i = 0;
    for (int j = 1; i < k; j = 0)
    {
      Header localHeader = paramArrayOfHeader[i];
      if (j == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(localHeader.getValue().trim());
      i++;
    }
    return localStringBuilder.toString();
  }
  
  public String getURI(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
  {
    if (isRelativeRequest(paramHttpRequest)) {
      return canonicalizeUri(String.format("%s%s", new Object[] { paramHttpHost.toString(), paramHttpRequest.getRequestLine().getUri() }));
    }
    return canonicalizeUri(paramHttpRequest.getRequestLine().getUri());
  }
  
  public String getVariantKey(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry)
  {
    Object localObject1 = new ArrayList();
    Object localObject2 = paramHttpCacheEntry.getHeaders("Vary");
    int k = localObject2.length;
    for (int i = 0; i < k; i++)
    {
      paramHttpCacheEntry = localObject2[i].getElements();
      int m = paramHttpCacheEntry.length;
      for (int j = 0; j < m; j++) {
        ((List)localObject1).add(paramHttpCacheEntry[j].getName());
      }
    }
    Collections.sort((List)localObject1);
    try
    {
      paramHttpCacheEntry = new java/lang/StringBuilder;
      paramHttpCacheEntry.<init>("{");
      localObject2 = ((List)localObject1).iterator();
      for (i = 1; ((Iterator)localObject2).hasNext(); i = 0)
      {
        localObject1 = (String)((Iterator)localObject2).next();
        if (i == 0) {
          paramHttpCacheEntry.append("&");
        }
        paramHttpCacheEntry.append(URLEncoder.encode((String)localObject1, Consts.UTF_8.name()));
        paramHttpCacheEntry.append("=");
        paramHttpCacheEntry.append(URLEncoder.encode(getFullHeaderValue(paramHttpRequest.getHeaders((String)localObject1)), Consts.UTF_8.name()));
      }
      paramHttpCacheEntry.append("}");
      return paramHttpCacheEntry.toString();
    }
    catch (UnsupportedEncodingException paramHttpRequest)
    {
      throw new RuntimeException("couldn't encode to UTF-8", paramHttpRequest);
    }
  }
  
  public String getVariantURI(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry)
  {
    if (!paramHttpCacheEntry.hasVariants()) {
      return getURI(paramHttpHost, paramHttpRequest);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getVariantKey(paramHttpRequest, paramHttpCacheEntry));
    localStringBuilder.append(getURI(paramHttpHost, paramHttpRequest));
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheKeyGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */