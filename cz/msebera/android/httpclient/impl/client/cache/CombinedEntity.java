package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.entity.AbstractHttpEntity;
import cz.msebera.android.httpclient.util.Args;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;

@NotThreadSafe
class CombinedEntity
  extends AbstractHttpEntity
{
  private final InputStream combinedStream;
  private final Resource resource;
  
  CombinedEntity(Resource paramResource, InputStream paramInputStream)
    throws IOException
  {
    this.resource = paramResource;
    this.combinedStream = new SequenceInputStream(new ResourceStream(paramResource.getInputStream()), paramInputStream);
  }
  
  private void dispose()
  {
    this.resource.dispose();
  }
  
  public InputStream getContent()
    throws IOException, IllegalStateException
  {
    return this.combinedStream;
  }
  
  public long getContentLength()
  {
    return -1L;
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public boolean isStreaming()
  {
    return true;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    InputStream localInputStream = getContent();
    try
    {
      byte[] arrayOfByte = new byte['ࠀ'];
      for (;;)
      {
        int i = localInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        paramOutputStream.write(arrayOfByte, 0, i);
      }
      return;
    }
    finally
    {
      localInputStream.close();
    }
  }
  
  class ResourceStream
    extends FilterInputStream
  {
    protected ResourceStream(InputStream paramInputStream)
    {
      super();
    }
    
    public void close()
      throws IOException
    {
      try
      {
        super.close();
        return;
      }
      finally
      {
        CombinedEntity.this.dispose();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/CombinedEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */