package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.util.Args;
import java.lang.reflect.Proxy;

@NotThreadSafe
class Proxies
{
  public static CloseableHttpResponse enhanceResponse(HttpResponse paramHttpResponse)
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    if ((paramHttpResponse instanceof CloseableHttpResponse)) {
      return (CloseableHttpResponse)paramHttpResponse;
    }
    ClassLoader localClassLoader = ResponseProxyHandler.class.getClassLoader();
    paramHttpResponse = new ResponseProxyHandler(paramHttpResponse);
    return (CloseableHttpResponse)Proxy.newProxyInstance(localClassLoader, new Class[] { CloseableHttpResponse.class }, paramHttpResponse);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/Proxies.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */