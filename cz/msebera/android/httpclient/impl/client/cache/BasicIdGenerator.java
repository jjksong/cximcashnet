package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Formatter;
import java.util.Locale;

@ThreadSafe
class BasicIdGenerator
{
  @GuardedBy("this")
  private long count;
  private final String hostname;
  private final SecureRandom rnd;
  
  public BasicIdGenerator()
  {
    String str2;
    try
    {
      String str1 = InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException localUnknownHostException)
    {
      str2 = "localhost";
    }
    this.hostname = str2;
    try
    {
      this.rnd = SecureRandom.getInstance("SHA1PRNG");
      return;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new Error(localNoSuchAlgorithmException);
    }
  }
  
  public String generate()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    generate(localStringBuilder);
    return localStringBuilder.toString();
  }
  
  public void generate(StringBuilder paramStringBuilder)
  {
    try
    {
      this.count += 1L;
      int i = this.rnd.nextInt();
      paramStringBuilder.append(System.currentTimeMillis());
      paramStringBuilder.append('.');
      Formatter localFormatter = new java/util/Formatter;
      localFormatter.<init>(paramStringBuilder, Locale.US);
      localFormatter.format("%1$016x-%2$08x", new Object[] { Long.valueOf(this.count), Integer.valueOf(i) });
      localFormatter.close();
      paramStringBuilder.append('.');
      paramStringBuilder.append(this.hostname);
      return;
    }
    finally
    {
      paramStringBuilder = finally;
      throw paramStringBuilder;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/BasicIdGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */