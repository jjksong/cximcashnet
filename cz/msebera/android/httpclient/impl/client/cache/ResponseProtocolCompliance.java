package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.message.BasicHeader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Immutable
class ResponseProtocolCompliance
{
  private static final String UNEXPECTED_100_CONTINUE = "The incoming request did not contain a 100-continue header, but the response was a Status 100, continue.";
  private static final String UNEXPECTED_PARTIAL_CONTENT = "partial content was returned for a request that did not ask for it";
  
  private boolean backendResponseMustNotHaveBody(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    boolean bool;
    if ((!"HEAD".equals(paramHttpRequest.getRequestLine().getMethod())) && (paramHttpResponse.getStatusLine().getStatusCode() != 204) && (paramHttpResponse.getStatusLine().getStatusCode() != 205) && (paramHttpResponse.getStatusLine().getStatusCode() != 304)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private void consumeBody(HttpResponse paramHttpResponse)
    throws IOException
  {
    paramHttpResponse = paramHttpResponse.getEntity();
    if (paramHttpResponse != null) {
      IOUtils.consume(paramHttpResponse);
    }
  }
  
  private void ensure200ForOPTIONSRequestWithNoBodyHasContentLengthZero(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
  {
    if (!paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("OPTIONS")) {
      return;
    }
    if (paramHttpResponse.getStatusLine().getStatusCode() != 200) {
      return;
    }
    if (paramHttpResponse.getFirstHeader("Content-Length") == null) {
      paramHttpResponse.addHeader("Content-Length", "0");
    }
  }
  
  private void ensure206ContainsDateHeader(HttpResponse paramHttpResponse)
  {
    if (paramHttpResponse.getFirstHeader("Date") == null) {
      paramHttpResponse.addHeader("Date", DateUtils.formatDate(new Date()));
    }
  }
  
  private void ensure304DoesNotContainExtraEntityHeaders(HttpResponse paramHttpResponse)
  {
    String[] arrayOfString = new String[8];
    int i = 0;
    arrayOfString[0] = "Allow";
    arrayOfString[1] = "Content-Encoding";
    arrayOfString[2] = "Content-Language";
    arrayOfString[3] = "Content-Length";
    arrayOfString[4] = "Content-MD5";
    arrayOfString[5] = "Content-Range";
    arrayOfString[6] = "Content-Type";
    arrayOfString[7] = "Last-Modified";
    if (paramHttpResponse.getStatusLine().getStatusCode() == 304)
    {
      int j = arrayOfString.length;
      while (i < j)
      {
        paramHttpResponse.removeHeaders(arrayOfString[i]);
        i++;
      }
    }
  }
  
  private void ensurePartialContentIsNotSentToAClientThatDidNotRequestIt(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse)
    throws IOException
  {
    if ((paramHttpRequest.getFirstHeader("Range") == null) && (paramHttpResponse.getStatusLine().getStatusCode() == 206))
    {
      consumeBody(paramHttpResponse);
      throw new ClientProtocolException("partial content was returned for a request that did not ask for it");
    }
  }
  
  private void identityIsNotUsedInContentEncoding(HttpResponse paramHttpResponse)
  {
    Header[] arrayOfHeader = paramHttpResponse.getHeaders("Content-Encoding");
    if ((arrayOfHeader != null) && (arrayOfHeader.length != 0))
    {
      Object localObject1 = new ArrayList();
      int n = arrayOfHeader.length;
      int j = 0;
      int i = 0;
      while (j < n)
      {
        Object localObject3 = arrayOfHeader[j];
        Object localObject2 = new StringBuilder();
        localObject3 = ((Header)localObject3).getElements();
        int i1 = localObject3.length;
        int k = 0;
        int m = 1;
        while (k < i1)
        {
          Object localObject4 = localObject3[k];
          if ("identity".equalsIgnoreCase(((HeaderElement)localObject4).getName()))
          {
            i = 1;
          }
          else
          {
            if (m == 0) {
              ((StringBuilder)localObject2).append(",");
            }
            ((StringBuilder)localObject2).append(localObject4.toString());
            m = 0;
          }
          k++;
        }
        localObject2 = ((StringBuilder)localObject2).toString();
        if (!"".equals(localObject2)) {
          ((List)localObject1).add(new BasicHeader("Content-Encoding", (String)localObject2));
        }
        j++;
      }
      if (i == 0) {
        return;
      }
      paramHttpResponse.removeHeaders("Content-Encoding");
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext()) {
        paramHttpResponse.addHeader((Header)((Iterator)localObject1).next());
      }
      return;
    }
  }
  
  private void removeResponseTransferEncoding(HttpResponse paramHttpResponse)
  {
    paramHttpResponse.removeHeaders("TE");
    paramHttpResponse.removeHeaders("Transfer-Encoding");
  }
  
  private void requestDidNotExpect100ContinueButResponseIsOne(HttpRequestWrapper paramHttpRequestWrapper, HttpResponse paramHttpResponse)
    throws IOException
  {
    if (paramHttpResponse.getStatusLine().getStatusCode() != 100) {
      return;
    }
    paramHttpRequestWrapper = paramHttpRequestWrapper.getOriginal();
    if (((paramHttpRequestWrapper instanceof HttpEntityEnclosingRequest)) && (((HttpEntityEnclosingRequest)paramHttpRequestWrapper).expectContinue())) {
      return;
    }
    consumeBody(paramHttpResponse);
    throw new ClientProtocolException("The incoming request did not contain a 100-continue header, but the response was a Status 100, continue.");
  }
  
  private void transferEncodingIsNotReturnedTo1_0Client(HttpRequestWrapper paramHttpRequestWrapper, HttpResponse paramHttpResponse)
  {
    if (paramHttpRequestWrapper.getOriginal().getProtocolVersion().compareToVersion(HttpVersion.HTTP_1_1) >= 0) {
      return;
    }
    removeResponseTransferEncoding(paramHttpResponse);
  }
  
  private void warningsWithNonMatchingWarnDatesAreRemoved(HttpResponse paramHttpResponse)
  {
    Date localDate1 = DateUtils.parseDate(paramHttpResponse.getFirstHeader("Date").getValue());
    if (localDate1 == null) {
      return;
    }
    Header[] arrayOfHeader = paramHttpResponse.getHeaders("Warning");
    if ((arrayOfHeader != null) && (arrayOfHeader.length != 0))
    {
      ArrayList localArrayList = new ArrayList();
      int m = arrayOfHeader.length;
      int j = 0;
      int i = 0;
      while (j < m)
      {
        for (Object localObject2 : WarningValue.getWarningValues(arrayOfHeader[j]))
        {
          Date localDate2 = ((WarningValue)localObject2).getWarnDate();
          if ((localDate2 != null) && (!localDate2.equals(localDate1))) {
            i = 1;
          } else {
            localArrayList.add(new BasicHeader("Warning", ((WarningValue)localObject2).toString()));
          }
        }
        j++;
      }
      if (i != 0)
      {
        paramHttpResponse.removeHeaders("Warning");
        ??? = localArrayList.iterator();
        while (((Iterator)???).hasNext()) {
          paramHttpResponse.addHeader((Header)((Iterator)???).next());
        }
      }
      return;
    }
  }
  
  public void ensureProtocolCompliance(HttpRequestWrapper paramHttpRequestWrapper, HttpResponse paramHttpResponse)
    throws IOException
  {
    if (backendResponseMustNotHaveBody(paramHttpRequestWrapper, paramHttpResponse))
    {
      consumeBody(paramHttpResponse);
      paramHttpResponse.setEntity(null);
    }
    requestDidNotExpect100ContinueButResponseIsOne(paramHttpRequestWrapper, paramHttpResponse);
    transferEncodingIsNotReturnedTo1_0Client(paramHttpRequestWrapper, paramHttpResponse);
    ensurePartialContentIsNotSentToAClientThatDidNotRequestIt(paramHttpRequestWrapper, paramHttpResponse);
    ensure200ForOPTIONSRequestWithNoBodyHasContentLengthZero(paramHttpRequestWrapper, paramHttpResponse);
    ensure206ContainsDateHeader(paramHttpResponse);
    ensure304DoesNotContainExtraEntityHeaders(paramHttpResponse);
    identityIsNotUsedInContentEncoding(paramHttpResponse);
    warningsWithNonMatchingWarnDatesAreRemoved(paramHttpResponse);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/cache/ResponseProtocolCompliance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */