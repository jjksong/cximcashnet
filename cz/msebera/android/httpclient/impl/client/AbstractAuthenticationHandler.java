package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Deprecated
@Immutable
public abstract class AbstractAuthenticationHandler
  implements AuthenticationHandler
{
  private static final List<String> DEFAULT_SCHEME_PRIORITY = Collections.unmodifiableList(Arrays.asList(new String[] { "negotiate", "NTLM", "Digest", "Basic" }));
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  protected List<String> getAuthPreferences()
  {
    return DEFAULT_SCHEME_PRIORITY;
  }
  
  protected List<String> getAuthPreferences(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    return getAuthPreferences();
  }
  
  protected Map<String, Header> parseChallenges(Header[] paramArrayOfHeader)
    throws MalformedChallengeException
  {
    HashMap localHashMap = new HashMap(paramArrayOfHeader.length);
    int m = paramArrayOfHeader.length;
    int j = 0;
    while (j < m)
    {
      Header localHeader = paramArrayOfHeader[j];
      Object localObject;
      CharArrayBuffer localCharArrayBuffer;
      if ((localHeader instanceof FormattedHeader))
      {
        localObject = (FormattedHeader)localHeader;
        localCharArrayBuffer = ((FormattedHeader)localObject).getBuffer();
        i = ((FormattedHeader)localObject).getValuePos();
      }
      else
      {
        localObject = localHeader.getValue();
        if (localObject == null) {
          break label189;
        }
        localCharArrayBuffer = new CharArrayBuffer(((String)localObject).length());
        localCharArrayBuffer.append((String)localObject);
      }
      for (int i = 0; (i < localCharArrayBuffer.length()) && (HTTP.isWhitespace(localCharArrayBuffer.charAt(i))); i++) {}
      for (int k = i; (k < localCharArrayBuffer.length()) && (!HTTP.isWhitespace(localCharArrayBuffer.charAt(k))); k++) {}
      localHashMap.put(localCharArrayBuffer.substring(i, k).toLowerCase(Locale.ENGLISH), localHeader);
      j++;
      continue;
      label189:
      throw new MalformedChallengeException("Header value is null");
    }
    return localHashMap;
  }
  
  public AuthScheme selectScheme(Map<String, Header> paramMap, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    AuthSchemeRegistry localAuthSchemeRegistry = (AuthSchemeRegistry)paramHttpContext.getAttribute("http.authscheme-registry");
    Asserts.notNull(localAuthSchemeRegistry, "AuthScheme registry");
    Object localObject1 = getAuthPreferences(paramHttpResponse, paramHttpContext);
    paramHttpContext = (HttpContext)localObject1;
    if (localObject1 == null) {
      paramHttpContext = DEFAULT_SCHEME_PRIORITY;
    }
    if (this.log.isDebugEnabled())
    {
      localObject1 = this.log;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Authentication schemes in the order of preference: ");
      ((StringBuilder)localObject2).append(paramHttpContext);
      ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
    }
    localObject1 = null;
    Object localObject2 = paramHttpContext.iterator();
    for (;;)
    {
      paramHttpContext = (HttpContext)localObject1;
      if (!((Iterator)localObject2).hasNext()) {
        break;
      }
      String str = (String)((Iterator)localObject2).next();
      Object localObject3;
      if ((Header)paramMap.get(str.toLowerCase(Locale.ENGLISH)) != null)
      {
        if (this.log.isDebugEnabled())
        {
          paramHttpContext = this.log;
          localObject3 = new StringBuilder();
          ((StringBuilder)localObject3).append(str);
          ((StringBuilder)localObject3).append(" authentication scheme selected");
          paramHttpContext.debug(((StringBuilder)localObject3).toString());
        }
        try
        {
          paramHttpContext = localAuthSchemeRegistry.getAuthScheme(str, paramHttpResponse.getParams());
        }
        catch (IllegalStateException paramHttpContext) {}
        if (this.log.isWarnEnabled())
        {
          localObject3 = this.log;
          paramHttpContext = new StringBuilder();
          paramHttpContext.append("Authentication scheme ");
          paramHttpContext.append(str);
          paramHttpContext.append(" not supported");
          ((HttpClientAndroidLog)localObject3).warn(paramHttpContext.toString());
        }
      }
      else if (this.log.isDebugEnabled())
      {
        localObject3 = this.log;
        paramHttpContext = new StringBuilder();
        paramHttpContext.append("Challenge for ");
        paramHttpContext.append(str);
        paramHttpContext.append(" authentication scheme not available");
        ((HttpClientAndroidLog)localObject3).debug(paramHttpContext.toString());
      }
    }
    if (paramHttpContext != null) {
      return paramHttpContext;
    }
    paramHttpResponse = new StringBuilder();
    paramHttpResponse.append("Unable to respond to any of these challenges: ");
    paramHttpResponse.append(paramMap);
    throw new AuthenticationException(paramHttpResponse.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/AbstractAuthenticationHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */