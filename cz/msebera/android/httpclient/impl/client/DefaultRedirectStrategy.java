package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.CircularRedirectException;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpHead;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.methods.RequestBuilder;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

@Immutable
public class DefaultRedirectStrategy
  implements RedirectStrategy
{
  public static final DefaultRedirectStrategy INSTANCE = new DefaultRedirectStrategy();
  @Deprecated
  public static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  private static final String[] REDIRECT_METHODS = { "GET", "HEAD" };
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  protected URI createLocationURI(String paramString)
    throws ProtocolException
  {
    try
    {
      localObject1 = new cz/msebera/android/httpclient/client/utils/URIBuilder;
      Object localObject2 = new java/net/URI;
      ((URI)localObject2).<init>(paramString);
      ((URIBuilder)localObject1).<init>(((URI)localObject2).normalize());
      localObject2 = ((URIBuilder)localObject1).getHost();
      if (localObject2 != null) {
        ((URIBuilder)localObject1).setHost(((String)localObject2).toLowerCase(Locale.ENGLISH));
      }
      if (TextUtils.isEmpty(((URIBuilder)localObject1).getPath())) {
        ((URIBuilder)localObject1).setPath("/");
      }
      localObject1 = ((URIBuilder)localObject1).build();
      return (URI)localObject1;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Invalid redirect URI: ");
      ((StringBuilder)localObject1).append(paramString);
      throw new ProtocolException(((StringBuilder)localObject1).toString(), localURISyntaxException);
    }
  }
  
  public URI getLocationURI(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpResponse, "HTTP response");
    Args.notNull(paramHttpContext, "HTTP context");
    HttpClientContext localHttpClientContext = HttpClientContext.adapt(paramHttpContext);
    Object localObject1 = paramHttpResponse.getFirstHeader("location");
    if (localObject1 != null)
    {
      paramHttpResponse = ((Header)localObject1).getValue();
      if (this.log.isDebugEnabled())
      {
        localObject2 = this.log;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("Redirect requested to location '");
        ((StringBuilder)localObject1).append(paramHttpResponse);
        ((StringBuilder)localObject1).append("'");
        ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
      }
      Object localObject2 = localHttpClientContext.getRequestConfig();
      localObject1 = createLocationURI(paramHttpResponse);
      paramHttpResponse = (HttpResponse)localObject1;
      try
      {
        if (!((URI)localObject1).isAbsolute()) {
          if (((RequestConfig)localObject2).isRelativeRedirectsAllowed())
          {
            HttpHost localHttpHost = localHttpClientContext.getTargetHost();
            Asserts.notNull(localHttpHost, "Target host");
            paramHttpResponse = new java/net/URI;
            paramHttpResponse.<init>(paramHttpRequest.getRequestLine().getUri());
            paramHttpResponse = URIUtils.resolve(URIUtils.rewriteURI(paramHttpResponse, localHttpHost, false), (URI)localObject1);
          }
          else
          {
            paramHttpRequest = new cz/msebera/android/httpclient/ProtocolException;
            paramHttpResponse = new java/lang/StringBuilder;
            paramHttpResponse.<init>();
            paramHttpResponse.append("Relative redirect location '");
            paramHttpResponse.append(localObject1);
            paramHttpResponse.append("' not allowed");
            paramHttpRequest.<init>(paramHttpResponse.toString());
            throw paramHttpRequest;
          }
        }
        localObject1 = (RedirectLocations)localHttpClientContext.getAttribute("http.protocol.redirect-locations");
        paramHttpRequest = (HttpRequest)localObject1;
        if (localObject1 == null)
        {
          paramHttpRequest = new RedirectLocations();
          paramHttpContext.setAttribute("http.protocol.redirect-locations", paramHttpRequest);
        }
        if ((!((RequestConfig)localObject2).isCircularRedirectsAllowed()) && (paramHttpRequest.contains(paramHttpResponse)))
        {
          paramHttpRequest = new StringBuilder();
          paramHttpRequest.append("Circular redirect to '");
          paramHttpRequest.append(paramHttpResponse);
          paramHttpRequest.append("'");
          throw new CircularRedirectException(paramHttpRequest.toString());
        }
        paramHttpRequest.add(paramHttpResponse);
        return paramHttpResponse;
      }
      catch (URISyntaxException paramHttpRequest)
      {
        throw new ProtocolException(paramHttpRequest.getMessage(), paramHttpRequest);
      }
    }
    paramHttpRequest = new StringBuilder();
    paramHttpRequest.append("Received redirect response ");
    paramHttpRequest.append(paramHttpResponse.getStatusLine());
    paramHttpRequest.append(" but no location header");
    throw new ProtocolException(paramHttpRequest.toString());
  }
  
  public HttpUriRequest getRedirect(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    paramHttpContext = getLocationURI(paramHttpRequest, paramHttpResponse, paramHttpContext);
    String str = paramHttpRequest.getRequestLine().getMethod();
    if (str.equalsIgnoreCase("HEAD")) {
      return new HttpHead(paramHttpContext);
    }
    if (str.equalsIgnoreCase("GET")) {
      return new HttpGet(paramHttpContext);
    }
    if (paramHttpResponse.getStatusLine().getStatusCode() == 307) {
      return RequestBuilder.copy(paramHttpRequest).setUri(paramHttpContext).build();
    }
    return new HttpGet(paramHttpContext);
  }
  
  protected boolean isRedirectable(String paramString)
  {
    String[] arrayOfString = REDIRECT_METHODS;
    int j = arrayOfString.length;
    for (int i = 0; i < j; i++) {
      if (arrayOfString[i].equalsIgnoreCase(paramString)) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isRedirected(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpResponse, "HTTP response");
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    paramHttpRequest = paramHttpRequest.getRequestLine().getMethod();
    paramHttpResponse = paramHttpResponse.getFirstHeader("location");
    if (i != 307)
    {
      boolean bool = true;
      switch (i)
      {
      default: 
        return false;
      case 303: 
        return true;
      case 302: 
        if ((!isRedirectable(paramHttpRequest)) || (paramHttpResponse == null)) {
          bool = false;
        }
        return bool;
      }
    }
    return isRedirectable(paramHttpRequest);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultRedirectStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */