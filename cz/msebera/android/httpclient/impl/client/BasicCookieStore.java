package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieIdentityComparator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

@ThreadSafe
public class BasicCookieStore
  implements CookieStore, Serializable
{
  private static final long serialVersionUID = -7581093305228232025L;
  @GuardedBy("this")
  private final TreeSet<Cookie> cookies = new TreeSet(new CookieIdentityComparator());
  
  public void addCookie(Cookie paramCookie)
  {
    if (paramCookie != null) {}
    try
    {
      this.cookies.remove(paramCookie);
      Date localDate = new java/util/Date;
      localDate.<init>();
      if (!paramCookie.isExpired(localDate)) {
        this.cookies.add(paramCookie);
      }
    }
    finally {}
  }
  
  public void addCookies(Cookie[] paramArrayOfCookie)
  {
    if (paramArrayOfCookie != null) {
      try
      {
        int j = paramArrayOfCookie.length;
        for (int i = 0; i < j; i++) {
          addCookie(paramArrayOfCookie[i]);
        }
      }
      finally {}
    }
  }
  
  public void clear()
  {
    try
    {
      this.cookies.clear();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean clearExpired(Date paramDate)
  {
    boolean bool = false;
    if (paramDate == null) {
      return false;
    }
    try
    {
      Iterator localIterator = this.cookies.iterator();
      while (localIterator.hasNext()) {
        if (((Cookie)localIterator.next()).isExpired(paramDate))
        {
          localIterator.remove();
          bool = true;
        }
      }
      return bool;
    }
    finally {}
  }
  
  public List<Cookie> getCookies()
  {
    try
    {
      ArrayList localArrayList = new ArrayList(this.cookies);
      return localArrayList;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public String toString()
  {
    try
    {
      String str = this.cookies.toString();
      return str;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/BasicCookieStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */