package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.concurrent.FutureCallback;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@ThreadSafe
public class FutureRequestExecutionService
  implements Closeable
{
  private final AtomicBoolean closed = new AtomicBoolean(false);
  private final ExecutorService executorService;
  private final HttpClient httpclient;
  private final FutureRequestExecutionMetrics metrics = new FutureRequestExecutionMetrics();
  
  public FutureRequestExecutionService(HttpClient paramHttpClient, ExecutorService paramExecutorService)
  {
    this.httpclient = paramHttpClient;
    this.executorService = paramExecutorService;
  }
  
  public void close()
    throws IOException
  {
    this.closed.set(true);
    this.executorService.shutdownNow();
    HttpClient localHttpClient = this.httpclient;
    if ((localHttpClient instanceof Closeable)) {
      ((Closeable)localHttpClient).close();
    }
  }
  
  public <T> HttpRequestFutureTask<T> execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext, ResponseHandler<T> paramResponseHandler)
  {
    return execute(paramHttpUriRequest, paramHttpContext, paramResponseHandler, null);
  }
  
  public <T> HttpRequestFutureTask<T> execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext, ResponseHandler<T> paramResponseHandler, FutureCallback<T> paramFutureCallback)
  {
    if (!this.closed.get())
    {
      this.metrics.getScheduledConnections().incrementAndGet();
      paramHttpUriRequest = new HttpRequestFutureTask(paramHttpUriRequest, new HttpRequestTaskCallable(this.httpclient, paramHttpUriRequest, paramHttpContext, paramResponseHandler, paramFutureCallback, this.metrics));
      this.executorService.execute(paramHttpUriRequest);
      return paramHttpUriRequest;
    }
    throw new IllegalStateException("Close has been called on this httpclient instance.");
  }
  
  public FutureRequestExecutionMetrics metrics()
  {
    return this.metrics;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/FutureRequestExecutionService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */