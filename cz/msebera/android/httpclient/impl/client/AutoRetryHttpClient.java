package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.ServiceUnavailableRetryStrategy;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;

@Deprecated
@ThreadSafe
public class AutoRetryHttpClient
  implements HttpClient
{
  private final HttpClient backend;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ServiceUnavailableRetryStrategy retryStrategy;
  
  public AutoRetryHttpClient()
  {
    this(new DefaultHttpClient(), new DefaultServiceUnavailableRetryStrategy());
  }
  
  public AutoRetryHttpClient(HttpClient paramHttpClient)
  {
    this(paramHttpClient, new DefaultServiceUnavailableRetryStrategy());
  }
  
  public AutoRetryHttpClient(HttpClient paramHttpClient, ServiceUnavailableRetryStrategy paramServiceUnavailableRetryStrategy)
  {
    Args.notNull(paramHttpClient, "HttpClient");
    Args.notNull(paramServiceUnavailableRetryStrategy, "ServiceUnavailableRetryStrategy");
    this.backend = paramHttpClient;
    this.retryStrategy = paramServiceUnavailableRetryStrategy;
  }
  
  public AutoRetryHttpClient(ServiceUnavailableRetryStrategy paramServiceUnavailableRetryStrategy)
  {
    this(new DefaultHttpClient(), paramServiceUnavailableRetryStrategy);
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException
  {
    return execute(paramHttpHost, paramHttpRequest, null);
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException
  {
    int i = 1;
    for (;;)
    {
      HttpResponse localHttpResponse = this.backend.execute(paramHttpHost, paramHttpRequest, paramHttpContext);
      try
      {
        if (this.retryStrategy.retryRequest(localHttpResponse, i, paramHttpContext))
        {
          EntityUtils.consume(localHttpResponse.getEntity());
          long l = this.retryStrategy.getRetryInterval();
          try
          {
            HttpClientAndroidLog localHttpClientAndroidLog = this.log;
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Wait for ");
            localStringBuilder.append(l);
            localHttpClientAndroidLog.trace(localStringBuilder.toString());
            Thread.sleep(l);
            i++;
          }
          catch (InterruptedException paramHttpHost)
          {
            Thread.currentThread().interrupt();
            paramHttpHost = new java/io/InterruptedIOException;
            paramHttpHost.<init>();
            throw paramHttpHost;
          }
        }
        return localHttpResponse;
      }
      catch (RuntimeException paramHttpHost)
      {
        try
        {
          EntityUtils.consume(localHttpResponse.getEntity());
        }
        catch (IOException paramHttpRequest)
        {
          this.log.warn("I/O error consuming response content", paramHttpRequest);
        }
        throw paramHttpHost;
      }
    }
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws IOException
  {
    return execute(paramHttpUriRequest, null);
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
    throws IOException
  {
    URI localURI = paramHttpUriRequest.getURI();
    return execute(new HttpHost(localURI.getHost(), localURI.getPort(), localURI.getScheme()), paramHttpUriRequest, paramHttpContext);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException
  {
    return (T)execute(paramHttpHost, paramHttpRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException
  {
    return (T)paramResponseHandler.handleResponse(execute(paramHttpHost, paramHttpRequest, paramHttpContext));
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException
  {
    return (T)execute(paramHttpUriRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException
  {
    return (T)paramResponseHandler.handleResponse(execute(paramHttpUriRequest, paramHttpContext));
  }
  
  public ClientConnectionManager getConnectionManager()
  {
    return this.backend.getConnectionManager();
  }
  
  public HttpParams getParams()
  {
    return this.backend.getParams();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/AutoRetryHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */