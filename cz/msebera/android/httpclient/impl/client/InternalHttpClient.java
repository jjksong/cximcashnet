package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.auth.AuthSchemeProvider;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.Configurable;
import cz.msebera.android.httpclient.client.methods.HttpExecutionAware;
import cz.msebera.android.httpclient.client.methods.HttpRequestWrapper;
import cz.msebera.android.httpclient.client.params.HttpClientParamConfig;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.cookie.CookieSpecProvider;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.execchain.ClientExecChain;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpParamsNames;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@ThreadSafe
class InternalHttpClient
  extends CloseableHttpClient
{
  private final Lookup<AuthSchemeProvider> authSchemeRegistry;
  private final List<Closeable> closeables;
  private final HttpClientConnectionManager connManager;
  private final Lookup<CookieSpecProvider> cookieSpecRegistry;
  private final CookieStore cookieStore;
  private final CredentialsProvider credentialsProvider;
  private final RequestConfig defaultConfig;
  private final ClientExecChain execChain;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final HttpRoutePlanner routePlanner;
  
  public InternalHttpClient(ClientExecChain paramClientExecChain, HttpClientConnectionManager paramHttpClientConnectionManager, HttpRoutePlanner paramHttpRoutePlanner, Lookup<CookieSpecProvider> paramLookup, Lookup<AuthSchemeProvider> paramLookup1, CookieStore paramCookieStore, CredentialsProvider paramCredentialsProvider, RequestConfig paramRequestConfig, List<Closeable> paramList)
  {
    Args.notNull(paramClientExecChain, "HTTP client exec chain");
    Args.notNull(paramHttpClientConnectionManager, "HTTP connection manager");
    Args.notNull(paramHttpRoutePlanner, "HTTP route planner");
    this.execChain = paramClientExecChain;
    this.connManager = paramHttpClientConnectionManager;
    this.routePlanner = paramHttpRoutePlanner;
    this.cookieSpecRegistry = paramLookup;
    this.authSchemeRegistry = paramLookup1;
    this.cookieStore = paramCookieStore;
    this.credentialsProvider = paramCredentialsProvider;
    this.defaultConfig = paramRequestConfig;
    this.closeables = paramList;
  }
  
  private HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    HttpHost localHttpHost = paramHttpHost;
    if (paramHttpHost == null) {
      localHttpHost = (HttpHost)paramHttpRequest.getParams().getParameter("http.default-host");
    }
    return this.routePlanner.determineRoute(localHttpHost, paramHttpRequest, paramHttpContext);
  }
  
  private void setupContext(HttpClientContext paramHttpClientContext)
  {
    if (paramHttpClientContext.getAttribute("http.auth.target-scope") == null) {
      paramHttpClientContext.setAttribute("http.auth.target-scope", new AuthState());
    }
    if (paramHttpClientContext.getAttribute("http.auth.proxy-scope") == null) {
      paramHttpClientContext.setAttribute("http.auth.proxy-scope", new AuthState());
    }
    if (paramHttpClientContext.getAttribute("http.authscheme-registry") == null) {
      paramHttpClientContext.setAttribute("http.authscheme-registry", this.authSchemeRegistry);
    }
    if (paramHttpClientContext.getAttribute("http.cookiespec-registry") == null) {
      paramHttpClientContext.setAttribute("http.cookiespec-registry", this.cookieSpecRegistry);
    }
    if (paramHttpClientContext.getAttribute("http.cookie-store") == null) {
      paramHttpClientContext.setAttribute("http.cookie-store", this.cookieStore);
    }
    if (paramHttpClientContext.getAttribute("http.auth.credentials-provider") == null) {
      paramHttpClientContext.setAttribute("http.auth.credentials-provider", this.credentialsProvider);
    }
    if (paramHttpClientContext.getAttribute("http.request-config") == null) {
      paramHttpClientContext.setAttribute("http.request-config", this.defaultConfig);
    }
  }
  
  public void close()
  {
    this.connManager.shutdown();
    Object localObject = this.closeables;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        Closeable localCloseable = (Closeable)((Iterator)localObject).next();
        try
        {
          localCloseable.close();
        }
        catch (IOException localIOException)
        {
          this.log.error(localIOException.getMessage(), localIOException);
        }
      }
    }
  }
  
  protected CloseableHttpResponse doExecute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    boolean bool = paramHttpRequest instanceof HttpExecutionAware;
    Object localObject = null;
    HttpExecutionAware localHttpExecutionAware;
    if (bool) {
      localHttpExecutionAware = (HttpExecutionAware)paramHttpRequest;
    } else {
      localHttpExecutionAware = null;
    }
    try
    {
      HttpRequestWrapper localHttpRequestWrapper = HttpRequestWrapper.wrap(paramHttpRequest);
      if (paramHttpContext == null) {
        paramHttpContext = new BasicHttpContext();
      }
      HttpClientContext localHttpClientContext = HttpClientContext.adapt(paramHttpContext);
      paramHttpContext = (HttpContext)localObject;
      if ((paramHttpRequest instanceof Configurable)) {
        paramHttpContext = ((Configurable)paramHttpRequest).getConfig();
      }
      localObject = paramHttpContext;
      if (paramHttpContext == null)
      {
        paramHttpRequest = paramHttpRequest.getParams();
        if ((paramHttpRequest instanceof HttpParamsNames))
        {
          localObject = paramHttpContext;
          if (!((HttpParamsNames)paramHttpRequest).getNames().isEmpty()) {
            localObject = HttpClientParamConfig.getRequestConfig(paramHttpRequest);
          }
        }
        else
        {
          localObject = HttpClientParamConfig.getRequestConfig(paramHttpRequest);
        }
      }
      if (localObject != null) {
        localHttpClientContext.setRequestConfig((RequestConfig)localObject);
      }
      setupContext(localHttpClientContext);
      paramHttpHost = determineRoute(paramHttpHost, localHttpRequestWrapper, localHttpClientContext);
      paramHttpHost = this.execChain.execute(paramHttpHost, localHttpRequestWrapper, localHttpClientContext, localHttpExecutionAware);
      return paramHttpHost;
    }
    catch (HttpException paramHttpHost)
    {
      throw new ClientProtocolException(paramHttpHost);
    }
  }
  
  public ClientConnectionManager getConnectionManager()
  {
    new ClientConnectionManager()
    {
      public void closeExpiredConnections()
      {
        InternalHttpClient.this.connManager.closeExpiredConnections();
      }
      
      public void closeIdleConnections(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        InternalHttpClient.this.connManager.closeIdleConnections(paramAnonymousLong, paramAnonymousTimeUnit);
      }
      
      public SchemeRegistry getSchemeRegistry()
      {
        throw new UnsupportedOperationException();
      }
      
      public void releaseConnection(ManagedClientConnection paramAnonymousManagedClientConnection, long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        throw new UnsupportedOperationException();
      }
      
      public ClientConnectionRequest requestConnection(HttpRoute paramAnonymousHttpRoute, Object paramAnonymousObject)
      {
        throw new UnsupportedOperationException();
      }
      
      public void shutdown()
      {
        InternalHttpClient.this.connManager.shutdown();
      }
    };
  }
  
  public HttpParams getParams()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/InternalHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */