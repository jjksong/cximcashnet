package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.net.ssl.SSLException;

@Immutable
public class DefaultHttpRequestRetryHandler
  implements HttpRequestRetryHandler
{
  public static final DefaultHttpRequestRetryHandler INSTANCE = new DefaultHttpRequestRetryHandler();
  private final Set<Class<? extends IOException>> nonRetriableClasses;
  private final boolean requestSentRetryEnabled;
  private final int retryCount;
  
  public DefaultHttpRequestRetryHandler()
  {
    this(3, false);
  }
  
  public DefaultHttpRequestRetryHandler(int paramInt, boolean paramBoolean)
  {
    this(paramInt, paramBoolean, Arrays.asList(new Class[] { InterruptedIOException.class, UnknownHostException.class, ConnectException.class, SSLException.class }));
  }
  
  protected DefaultHttpRequestRetryHandler(int paramInt, boolean paramBoolean, Collection<Class<? extends IOException>> paramCollection)
  {
    this.retryCount = paramInt;
    this.requestSentRetryEnabled = paramBoolean;
    this.nonRetriableClasses = new HashSet();
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      paramCollection = (Class)localIterator.next();
      this.nonRetriableClasses.add(paramCollection);
    }
  }
  
  public int getRetryCount()
  {
    return this.retryCount;
  }
  
  protected boolean handleAsIdempotent(HttpRequest paramHttpRequest)
  {
    boolean bool;
    if (!(paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isRequestSentRetryEnabled()
  {
    return this.requestSentRetryEnabled;
  }
  
  @Deprecated
  protected boolean requestIsAborted(HttpRequest paramHttpRequest)
  {
    HttpRequest localHttpRequest = paramHttpRequest;
    if ((paramHttpRequest instanceof RequestWrapper)) {
      localHttpRequest = ((RequestWrapper)paramHttpRequest).getOriginal();
    }
    boolean bool;
    if (((localHttpRequest instanceof HttpUriRequest)) && (((HttpUriRequest)localHttpRequest).isAborted())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean retryRequest(IOException paramIOException, int paramInt, HttpContext paramHttpContext)
  {
    Args.notNull(paramIOException, "Exception parameter");
    Args.notNull(paramHttpContext, "HTTP context");
    if (paramInt > this.retryCount) {
      return false;
    }
    if (this.nonRetriableClasses.contains(paramIOException.getClass())) {
      return false;
    }
    Iterator localIterator = this.nonRetriableClasses.iterator();
    while (localIterator.hasNext()) {
      if (((Class)localIterator.next()).isInstance(paramIOException)) {
        return false;
      }
    }
    paramIOException = HttpClientContext.adapt(paramHttpContext);
    paramHttpContext = paramIOException.getRequest();
    if (requestIsAborted(paramHttpContext)) {
      return false;
    }
    if (handleAsIdempotent(paramHttpContext)) {
      return true;
    }
    return (!paramIOException.isRequestSent()) || (this.requestSentRetryEnabled);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultHttpRequestRetryHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */