package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.params.AbstractHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;

@Deprecated
@NotThreadSafe
public class ClientParamsStack
  extends AbstractHttpParams
{
  protected final HttpParams applicationParams;
  protected final HttpParams clientParams;
  protected final HttpParams overrideParams;
  protected final HttpParams requestParams;
  
  public ClientParamsStack(ClientParamsStack paramClientParamsStack)
  {
    this(paramClientParamsStack.getApplicationParams(), paramClientParamsStack.getClientParams(), paramClientParamsStack.getRequestParams(), paramClientParamsStack.getOverrideParams());
  }
  
  public ClientParamsStack(ClientParamsStack paramClientParamsStack, HttpParams paramHttpParams1, HttpParams paramHttpParams2, HttpParams paramHttpParams3, HttpParams paramHttpParams4)
  {
    this(paramHttpParams1, paramHttpParams2, paramHttpParams3, paramHttpParams4);
  }
  
  public ClientParamsStack(HttpParams paramHttpParams1, HttpParams paramHttpParams2, HttpParams paramHttpParams3, HttpParams paramHttpParams4)
  {
    this.applicationParams = paramHttpParams1;
    this.clientParams = paramHttpParams2;
    this.requestParams = paramHttpParams3;
    this.overrideParams = paramHttpParams4;
  }
  
  public HttpParams copy()
  {
    return this;
  }
  
  public final HttpParams getApplicationParams()
  {
    return this.applicationParams;
  }
  
  public final HttpParams getClientParams()
  {
    return this.clientParams;
  }
  
  public final HttpParams getOverrideParams()
  {
    return this.overrideParams;
  }
  
  public Object getParameter(String paramString)
  {
    Args.notNull(paramString, "Parameter name");
    Object localObject1 = this.overrideParams;
    if (localObject1 != null) {
      localObject1 = ((HttpParams)localObject1).getParameter(paramString);
    } else {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    HttpParams localHttpParams;
    if (localObject1 == null)
    {
      localHttpParams = this.requestParams;
      localObject2 = localObject1;
      if (localHttpParams != null) {
        localObject2 = localHttpParams.getParameter(paramString);
      }
    }
    localObject1 = localObject2;
    if (localObject2 == null)
    {
      localHttpParams = this.clientParams;
      localObject1 = localObject2;
      if (localHttpParams != null) {
        localObject1 = localHttpParams.getParameter(paramString);
      }
    }
    localObject2 = localObject1;
    if (localObject1 == null)
    {
      localHttpParams = this.applicationParams;
      localObject2 = localObject1;
      if (localHttpParams != null) {
        localObject2 = localHttpParams.getParameter(paramString);
      }
    }
    return localObject2;
  }
  
  public final HttpParams getRequestParams()
  {
    return this.requestParams;
  }
  
  public boolean removeParameter(String paramString)
  {
    throw new UnsupportedOperationException("Removing parameters in a stack is not supported.");
  }
  
  public HttpParams setParameter(String paramString, Object paramObject)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException("Setting parameters in a stack is not supported.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/ClientParamsStack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */