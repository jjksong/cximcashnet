package cz.msebera.android.httpclient.impl.client;

abstract interface Clock
{
  public abstract long getCurrentTime();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/Clock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */