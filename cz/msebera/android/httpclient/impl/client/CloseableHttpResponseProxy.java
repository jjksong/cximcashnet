package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@NotThreadSafe
class CloseableHttpResponseProxy
  implements InvocationHandler
{
  private static final Constructor<?> CONSTRUCTOR;
  private final HttpResponse original;
  
  static
  {
    try
    {
      CONSTRUCTOR = Proxy.getProxyClass(CloseableHttpResponseProxy.class.getClassLoader(), new Class[] { CloseableHttpResponse.class }).getConstructor(new Class[] { InvocationHandler.class });
      return;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      throw new IllegalStateException(localNoSuchMethodException);
    }
  }
  
  CloseableHttpResponseProxy(HttpResponse paramHttpResponse)
  {
    this.original = paramHttpResponse;
  }
  
  public static CloseableHttpResponse newProxy(HttpResponse paramHttpResponse)
  {
    try
    {
      Constructor localConstructor = CONSTRUCTOR;
      CloseableHttpResponseProxy localCloseableHttpResponseProxy = new cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy;
      localCloseableHttpResponseProxy.<init>(paramHttpResponse);
      paramHttpResponse = (CloseableHttpResponse)localConstructor.newInstance(new Object[] { localCloseableHttpResponseProxy });
      return paramHttpResponse;
    }
    catch (IllegalAccessException paramHttpResponse)
    {
      throw new IllegalStateException(paramHttpResponse);
    }
    catch (InvocationTargetException paramHttpResponse)
    {
      throw new IllegalStateException(paramHttpResponse);
    }
    catch (InstantiationException paramHttpResponse)
    {
      throw new IllegalStateException(paramHttpResponse);
    }
  }
  
  public void close()
    throws IOException
  {
    EntityUtils.consume(this.original.getEntity());
  }
  
  public Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
    throws Throwable
  {
    if (paramMethod.getName().equals("close"))
    {
      close();
      return null;
    }
    try
    {
      paramObject = paramMethod.invoke(this.original, paramArrayOfObject);
      return paramObject;
    }
    catch (InvocationTargetException paramMethod)
    {
      paramObject = paramMethod.getCause();
      if (paramObject != null) {
        throw ((Throwable)paramObject);
      }
      throw paramMethod;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */