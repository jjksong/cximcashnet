package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.concurrent.FutureCallback;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

class HttpRequestTaskCallable<V>
  implements Callable<V>
{
  private final FutureCallback<V> callback;
  private final AtomicBoolean cancelled = new AtomicBoolean(false);
  private final HttpContext context;
  private long ended = -1L;
  private final HttpClient httpclient;
  private final FutureRequestExecutionMetrics metrics;
  private final HttpUriRequest request;
  private final ResponseHandler<V> responseHandler;
  private final long scheduled = System.currentTimeMillis();
  private long started = -1L;
  
  HttpRequestTaskCallable(HttpClient paramHttpClient, HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext, ResponseHandler<V> paramResponseHandler, FutureCallback<V> paramFutureCallback, FutureRequestExecutionMetrics paramFutureRequestExecutionMetrics)
  {
    this.httpclient = paramHttpClient;
    this.responseHandler = paramResponseHandler;
    this.request = paramHttpUriRequest;
    this.context = paramHttpContext;
    this.callback = paramFutureCallback;
    this.metrics = paramFutureRequestExecutionMetrics;
  }
  
  public V call()
    throws Exception
  {
    if (!this.cancelled.get()) {
      try
      {
        this.metrics.getActiveConnections().incrementAndGet();
        this.started = System.currentTimeMillis();
        try
        {
          this.metrics.getScheduledConnections().decrementAndGet();
          Object localObject1 = this.httpclient.execute(this.request, this.responseHandler, this.context);
          this.ended = System.currentTimeMillis();
          this.metrics.getSuccessfulConnections().increment(this.started);
          if (this.callback != null) {
            this.callback.completed(localObject1);
          }
          return (V)localObject1;
        }
        catch (Exception localException)
        {
          this.metrics.getFailedConnections().increment(this.started);
          this.ended = System.currentTimeMillis();
          if (this.callback != null) {
            this.callback.failed(localException);
          }
          throw localException;
        }
        localStringBuilder = new StringBuilder();
      }
      finally
      {
        this.metrics.getRequests().increment(this.started);
        this.metrics.getTasks().increment(this.started);
        this.metrics.getActiveConnections().decrementAndGet();
      }
    }
    StringBuilder localStringBuilder;
    localStringBuilder.append("call has been cancelled for request ");
    localStringBuilder.append(this.request.getURI());
    throw new IllegalStateException(localStringBuilder.toString());
  }
  
  public void cancel()
  {
    this.cancelled.set(true);
    FutureCallback localFutureCallback = this.callback;
    if (localFutureCallback != null) {
      localFutureCallback.cancelled();
    }
  }
  
  public long getEnded()
  {
    return this.ended;
  }
  
  public long getScheduled()
  {
    return this.scheduled;
  }
  
  public long getStarted()
  {
    return this.started;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/HttpRequestTaskCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */