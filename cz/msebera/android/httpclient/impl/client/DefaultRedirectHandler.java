package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.CircularRedirectException;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.net.URI;
import java.net.URISyntaxException;

@Deprecated
@Immutable
public class DefaultRedirectHandler
  implements RedirectHandler
{
  private static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  public URI getLocationURI(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    Object localObject1 = paramHttpResponse.getFirstHeader("location");
    if (localObject1 != null)
    {
      Object localObject2 = ((Header)localObject1).getValue();
      Object localObject3;
      if (this.log.isDebugEnabled())
      {
        localObject1 = this.log;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("Redirect requested to location '");
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append("'");
        ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject3).toString());
      }
      try
      {
        localObject1 = new URI((String)localObject2);
        localObject2 = paramHttpResponse.getParams();
        paramHttpResponse = (HttpResponse)localObject1;
        if (!((URI)localObject1).isAbsolute()) {
          if (!((HttpParams)localObject2).isParameterTrue("http.protocol.reject-relative-redirect"))
          {
            HttpHost localHttpHost = (HttpHost)paramHttpContext.getAttribute("http.target_host");
            Asserts.notNull(localHttpHost, "Target host");
            localObject3 = (HttpRequest)paramHttpContext.getAttribute("http.request");
            try
            {
              paramHttpResponse = new java/net/URI;
              paramHttpResponse.<init>(((HttpRequest)localObject3).getRequestLine().getUri());
              paramHttpResponse = URIUtils.resolve(URIUtils.rewriteURI(paramHttpResponse, localHttpHost, true), (URI)localObject1);
            }
            catch (URISyntaxException paramHttpResponse)
            {
              throw new ProtocolException(paramHttpResponse.getMessage(), paramHttpResponse);
            }
          }
          else
          {
            paramHttpResponse = new StringBuilder();
            paramHttpResponse.append("Relative redirect location '");
            paramHttpResponse.append(localObject1);
            paramHttpResponse.append("' not allowed");
            throw new ProtocolException(paramHttpResponse.toString());
          }
        }
        if (((HttpParams)localObject2).isParameterFalse("http.protocol.allow-circular-redirects"))
        {
          localObject2 = (RedirectLocations)paramHttpContext.getAttribute("http.protocol.redirect-locations");
          localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = new RedirectLocations();
            paramHttpContext.setAttribute("http.protocol.redirect-locations", localObject1);
          }
          if (paramHttpResponse.getFragment() != null) {
            try
            {
              paramHttpContext = new cz/msebera/android/httpclient/HttpHost;
              paramHttpContext.<init>(paramHttpResponse.getHost(), paramHttpResponse.getPort(), paramHttpResponse.getScheme());
              paramHttpContext = URIUtils.rewriteURI(paramHttpResponse, paramHttpContext, true);
            }
            catch (URISyntaxException paramHttpResponse)
            {
              throw new ProtocolException(paramHttpResponse.getMessage(), paramHttpResponse);
            }
          } else {
            paramHttpContext = paramHttpResponse;
          }
          if (!((RedirectLocations)localObject1).contains(paramHttpContext))
          {
            ((RedirectLocations)localObject1).add(paramHttpContext);
          }
          else
          {
            paramHttpResponse = new StringBuilder();
            paramHttpResponse.append("Circular redirect to '");
            paramHttpResponse.append(paramHttpContext);
            paramHttpResponse.append("'");
            throw new CircularRedirectException(paramHttpResponse.toString());
          }
        }
        return paramHttpResponse;
      }
      catch (URISyntaxException paramHttpContext)
      {
        paramHttpResponse = new StringBuilder();
        paramHttpResponse.append("Invalid redirect URI: ");
        paramHttpResponse.append((String)localObject2);
        throw new ProtocolException(paramHttpResponse.toString(), paramHttpContext);
      }
    }
    paramHttpContext = new StringBuilder();
    paramHttpContext.append("Received redirect response ");
    paramHttpContext.append(paramHttpResponse.getStatusLine());
    paramHttpContext.append(" but no location header");
    throw new ProtocolException(paramHttpContext.toString());
  }
  
  public boolean isRedirectRequested(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    boolean bool2 = true;
    if (i != 307) {
      switch (i)
      {
      default: 
        return false;
      case 303: 
        return true;
      }
    }
    paramHttpResponse = ((HttpRequest)paramHttpContext.getAttribute("http.request")).getRequestLine().getMethod();
    boolean bool1 = bool2;
    if (!paramHttpResponse.equalsIgnoreCase("GET")) {
      if (paramHttpResponse.equalsIgnoreCase("HEAD")) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
    }
    return bool1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/DefaultRedirectHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */