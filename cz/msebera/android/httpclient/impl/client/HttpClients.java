package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.PoolingHttpClientConnectionManager;

@Immutable
public class HttpClients
{
  public static CloseableHttpClient createDefault()
  {
    return HttpClientBuilder.create().build();
  }
  
  public static CloseableHttpClient createMinimal()
  {
    return new MinimalHttpClient(new PoolingHttpClientConnectionManager());
  }
  
  public static CloseableHttpClient createMinimal(HttpClientConnectionManager paramHttpClientConnectionManager)
  {
    return new MinimalHttpClient(paramHttpClientConnectionManager);
  }
  
  public static CloseableHttpClient createSystem()
  {
    return HttpClientBuilder.create().useSystemProperties().build();
  }
  
  public static HttpClientBuilder custom()
  {
    return HttpClientBuilder.create();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/HttpClients.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */