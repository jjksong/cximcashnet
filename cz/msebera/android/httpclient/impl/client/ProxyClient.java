package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.params.HttpClientParamConfig;
import cz.msebera.android.httpclient.client.protocol.RequestClientConnControl;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.conn.HttpConnectionFactory;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteInfo.LayerType;
import cz.msebera.android.httpclient.conn.routing.RouteInfo.TunnelType;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.auth.BasicSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.DigestSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.HttpAuthenticator;
import cz.msebera.android.httpclient.impl.auth.NTLMSchemeFactory;
import cz.msebera.android.httpclient.impl.conn.ManagedHttpClientConnectionFactory;
import cz.msebera.android.httpclient.impl.execchain.TunnelRefusedException;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParamConfig;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;
import cz.msebera.android.httpclient.protocol.RequestTargetHost;
import cz.msebera.android.httpclient.protocol.RequestUserAgent;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.net.Socket;

public class ProxyClient
{
  private final AuthSchemeRegistry authSchemeRegistry;
  private final HttpAuthenticator authenticator;
  private final HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> connFactory;
  private final ConnectionConfig connectionConfig;
  private final HttpProcessor httpProcessor;
  private final AuthState proxyAuthState;
  private final ProxyAuthenticationStrategy proxyAuthStrategy;
  private final RequestConfig requestConfig;
  private final HttpRequestExecutor requestExec;
  private final ConnectionReuseStrategy reuseStrategy;
  
  public ProxyClient()
  {
    this(null, null, null);
  }
  
  public ProxyClient(RequestConfig paramRequestConfig)
  {
    this(null, null, paramRequestConfig);
  }
  
  public ProxyClient(HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory, ConnectionConfig paramConnectionConfig, RequestConfig paramRequestConfig)
  {
    if (paramHttpConnectionFactory == null) {
      paramHttpConnectionFactory = ManagedHttpClientConnectionFactory.INSTANCE;
    }
    this.connFactory = paramHttpConnectionFactory;
    if (paramConnectionConfig == null) {
      paramConnectionConfig = ConnectionConfig.DEFAULT;
    }
    this.connectionConfig = paramConnectionConfig;
    if (paramRequestConfig == null) {
      paramRequestConfig = RequestConfig.DEFAULT;
    }
    this.requestConfig = paramRequestConfig;
    this.httpProcessor = new ImmutableHttpProcessor(new HttpRequestInterceptor[] { new RequestTargetHost(), new RequestClientConnControl(), new RequestUserAgent() });
    this.requestExec = new HttpRequestExecutor();
    this.proxyAuthStrategy = new ProxyAuthenticationStrategy();
    this.authenticator = new HttpAuthenticator();
    this.proxyAuthState = new AuthState();
    this.authSchemeRegistry = new AuthSchemeRegistry();
    this.authSchemeRegistry.register("Basic", new BasicSchemeFactory());
    this.authSchemeRegistry.register("Digest", new DigestSchemeFactory());
    this.authSchemeRegistry.register("NTLM", new NTLMSchemeFactory());
    this.reuseStrategy = new DefaultConnectionReuseStrategy();
  }
  
  @Deprecated
  public ProxyClient(HttpParams paramHttpParams)
  {
    this(null, HttpParamConfig.getConnectionConfig(paramHttpParams), HttpClientParamConfig.getRequestConfig(paramHttpParams));
  }
  
  @Deprecated
  public AuthSchemeRegistry getAuthSchemeRegistry()
  {
    return this.authSchemeRegistry;
  }
  
  @Deprecated
  public HttpParams getParams()
  {
    return new BasicHttpParams();
  }
  
  public Socket tunnel(HttpHost paramHttpHost1, HttpHost paramHttpHost2, Credentials paramCredentials)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpHost1, "Proxy host");
    Args.notNull(paramHttpHost2, "Target host");
    Args.notNull(paramCredentials, "Credentials");
    if (paramHttpHost2.getPort() <= 0) {
      localObject = new HttpHost(paramHttpHost2.getHostName(), 80, paramHttpHost2.getSchemeName());
    } else {
      localObject = paramHttpHost2;
    }
    HttpRoute localHttpRoute = new HttpRoute((HttpHost)localObject, this.requestConfig.getLocalAddress(), paramHttpHost1, false, RouteInfo.TunnelType.TUNNELLED, RouteInfo.LayerType.PLAIN);
    ManagedHttpClientConnection localManagedHttpClientConnection = (ManagedHttpClientConnection)this.connFactory.create(localHttpRoute, this.connectionConfig);
    BasicHttpContext localBasicHttpContext = new BasicHttpContext();
    Object localObject = new BasicHttpRequest("CONNECT", ((HttpHost)localObject).toHostString(), HttpVersion.HTTP_1_1);
    BasicCredentialsProvider localBasicCredentialsProvider = new BasicCredentialsProvider();
    localBasicCredentialsProvider.setCredentials(new AuthScope(paramHttpHost1), paramCredentials);
    localBasicHttpContext.setAttribute("http.target_host", paramHttpHost2);
    localBasicHttpContext.setAttribute("http.connection", localManagedHttpClientConnection);
    localBasicHttpContext.setAttribute("http.request", localObject);
    localBasicHttpContext.setAttribute("http.route", localHttpRoute);
    localBasicHttpContext.setAttribute("http.auth.proxy-scope", this.proxyAuthState);
    localBasicHttpContext.setAttribute("http.auth.credentials-provider", localBasicCredentialsProvider);
    localBasicHttpContext.setAttribute("http.authscheme-registry", this.authSchemeRegistry);
    localBasicHttpContext.setAttribute("http.request-config", this.requestConfig);
    this.requestExec.preProcess((HttpRequest)localObject, this.httpProcessor, localBasicHttpContext);
    for (;;)
    {
      if (!localManagedHttpClientConnection.isOpen()) {
        localManagedHttpClientConnection.bind(new Socket(paramHttpHost1.getHostName(), paramHttpHost1.getPort()));
      }
      this.authenticator.generateAuthResponse((HttpRequest)localObject, this.proxyAuthState, localBasicHttpContext);
      paramHttpHost2 = this.requestExec.execute((HttpRequest)localObject, localManagedHttpClientConnection, localBasicHttpContext);
      if (paramHttpHost2.getStatusLine().getStatusCode() < 200) {
        break label528;
      }
      if ((!this.authenticator.isAuthenticationRequested(paramHttpHost1, paramHttpHost2, this.proxyAuthStrategy, this.proxyAuthState, localBasicHttpContext)) || (!this.authenticator.handleAuthChallenge(paramHttpHost1, paramHttpHost2, this.proxyAuthStrategy, this.proxyAuthState, localBasicHttpContext))) {
        break;
      }
      if (this.reuseStrategy.keepAlive(paramHttpHost2, localBasicHttpContext)) {
        EntityUtils.consume(paramHttpHost2.getEntity());
      } else {
        localManagedHttpClientConnection.close();
      }
      ((HttpRequest)localObject).removeHeaders("Proxy-Authorization");
    }
    if (paramHttpHost2.getStatusLine().getStatusCode() > 299)
    {
      paramHttpHost1 = paramHttpHost2.getEntity();
      if (paramHttpHost1 != null) {
        paramHttpHost2.setEntity(new BufferedHttpEntity(paramHttpHost1));
      }
      localManagedHttpClientConnection.close();
      paramHttpHost1 = new StringBuilder();
      paramHttpHost1.append("CONNECT refused by proxy: ");
      paramHttpHost1.append(paramHttpHost2.getStatusLine());
      throw new TunnelRefusedException(paramHttpHost1.toString(), paramHttpHost2);
    }
    return localManagedHttpClientConnection.getSocket();
    label528:
    paramHttpHost1 = new StringBuilder();
    paramHttpHost1.append("Unexpected response to CONNECT request: ");
    paramHttpHost1.append(paramHttpHost2.getStatusLine());
    throw new HttpException(paramHttpHost1.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/ProxyClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */