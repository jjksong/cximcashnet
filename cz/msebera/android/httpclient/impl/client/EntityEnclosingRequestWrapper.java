package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.entity.HttpEntityWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Deprecated
@NotThreadSafe
public class EntityEnclosingRequestWrapper
  extends RequestWrapper
  implements HttpEntityEnclosingRequest
{
  private boolean consumed;
  private HttpEntity entity;
  
  public EntityEnclosingRequestWrapper(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    throws ProtocolException
  {
    super(paramHttpEntityEnclosingRequest);
    setEntity(paramHttpEntityEnclosingRequest.getEntity());
  }
  
  public boolean expectContinue()
  {
    Header localHeader = getFirstHeader("Expect");
    boolean bool;
    if ((localHeader != null) && ("100-continue".equalsIgnoreCase(localHeader.getValue()))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public HttpEntity getEntity()
  {
    return this.entity;
  }
  
  public boolean isRepeatable()
  {
    HttpEntity localHttpEntity = this.entity;
    boolean bool;
    if ((localHttpEntity != null) && (!localHttpEntity.isRepeatable()) && (this.consumed)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public void setEntity(HttpEntity paramHttpEntity)
  {
    if (paramHttpEntity != null) {
      paramHttpEntity = new EntityWrapper(paramHttpEntity);
    } else {
      paramHttpEntity = null;
    }
    this.entity = paramHttpEntity;
    this.consumed = false;
  }
  
  class EntityWrapper
    extends HttpEntityWrapper
  {
    EntityWrapper(HttpEntity paramHttpEntity)
    {
      super();
    }
    
    public void consumeContent()
      throws IOException
    {
      EntityEnclosingRequestWrapper.access$002(EntityEnclosingRequestWrapper.this, true);
      super.consumeContent();
    }
    
    public InputStream getContent()
      throws IOException
    {
      EntityEnclosingRequestWrapper.access$002(EntityEnclosingRequestWrapper.this, true);
      return super.getContent();
    }
    
    public void writeTo(OutputStream paramOutputStream)
      throws IOException
    {
      EntityEnclosingRequestWrapper.access$002(EntityEnclosingRequestWrapper.this, true);
      super.writeTo(paramOutputStream);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/EntityEnclosingRequestWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */