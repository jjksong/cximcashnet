package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.message.AbstractHttpMessage;
import cz.msebera.android.httpclient.message.BasicRequestLine;
import cz.msebera.android.httpclient.message.HeaderGroup;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.util.Args;
import java.net.URI;
import java.net.URISyntaxException;

@Deprecated
@NotThreadSafe
public class RequestWrapper
  extends AbstractHttpMessage
  implements HttpUriRequest
{
  private int execCount;
  private String method;
  private final HttpRequest original;
  private URI uri;
  private ProtocolVersion version;
  
  public RequestWrapper(HttpRequest paramHttpRequest)
    throws ProtocolException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    this.original = paramHttpRequest;
    setParams(paramHttpRequest.getParams());
    setHeaders(paramHttpRequest.getAllHeaders());
    RequestLine localRequestLine;
    if ((paramHttpRequest instanceof HttpUriRequest))
    {
      paramHttpRequest = (HttpUriRequest)paramHttpRequest;
      this.uri = paramHttpRequest.getURI();
      this.method = paramHttpRequest.getMethod();
      this.version = null;
    }
    else
    {
      localRequestLine = paramHttpRequest.getRequestLine();
    }
    try
    {
      localObject = new java/net/URI;
      ((URI)localObject).<init>(localRequestLine.getUri());
      this.uri = ((URI)localObject);
      this.method = localRequestLine.getMethod();
      this.version = paramHttpRequest.getProtocolVersion();
      this.execCount = 0;
      return;
    }
    catch (URISyntaxException paramHttpRequest)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Invalid request URI: ");
      ((StringBuilder)localObject).append(localRequestLine.getUri());
      throw new ProtocolException(((StringBuilder)localObject).toString(), paramHttpRequest);
    }
  }
  
  public void abort()
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException();
  }
  
  public int getExecCount()
  {
    return this.execCount;
  }
  
  public String getMethod()
  {
    return this.method;
  }
  
  public HttpRequest getOriginal()
  {
    return this.original;
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    if (this.version == null) {
      this.version = HttpProtocolParams.getVersion(getParams());
    }
    return this.version;
  }
  
  public RequestLine getRequestLine()
  {
    String str = getMethod();
    ProtocolVersion localProtocolVersion = getProtocolVersion();
    Object localObject1 = this.uri;
    if (localObject1 != null) {
      localObject1 = ((URI)localObject1).toASCIIString();
    } else {
      localObject1 = null;
    }
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      if (((String)localObject1).length() != 0) {}
    }
    else
    {
      localObject2 = "/";
    }
    return new BasicRequestLine(str, (String)localObject2, localProtocolVersion);
  }
  
  public URI getURI()
  {
    return this.uri;
  }
  
  public void incrementExecCount()
  {
    this.execCount += 1;
  }
  
  public boolean isAborted()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return true;
  }
  
  public void resetHeaders()
  {
    this.headergroup.clear();
    setHeaders(this.original.getAllHeaders());
  }
  
  public void setMethod(String paramString)
  {
    Args.notNull(paramString, "Method name");
    this.method = paramString;
  }
  
  public void setProtocolVersion(ProtocolVersion paramProtocolVersion)
  {
    this.version = paramProtocolVersion;
  }
  
  public void setURI(URI paramURI)
  {
    this.uri = paramURI;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/RequestWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */