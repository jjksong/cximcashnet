package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.NTCredentials;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.util.Args;
import java.net.Authenticator;
import java.net.Authenticator.RequestorType;
import java.net.PasswordAuthentication;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ThreadSafe
public class SystemDefaultCredentialsProvider
  implements CredentialsProvider
{
  private static final Map<String, String> SCHEME_MAP = new ConcurrentHashMap();
  private final BasicCredentialsProvider internal = new BasicCredentialsProvider();
  
  static
  {
    SCHEME_MAP.put("Basic".toUpperCase(Locale.ENGLISH), "Basic");
    SCHEME_MAP.put("Digest".toUpperCase(Locale.ENGLISH), "Digest");
    SCHEME_MAP.put("NTLM".toUpperCase(Locale.ENGLISH), "NTLM");
    SCHEME_MAP.put("negotiate".toUpperCase(Locale.ENGLISH), "SPNEGO");
    SCHEME_MAP.put("Kerberos".toUpperCase(Locale.ENGLISH), "Kerberos");
  }
  
  private static PasswordAuthentication getSystemCreds(AuthScope paramAuthScope, Authenticator.RequestorType paramRequestorType)
  {
    String str2 = paramAuthScope.getHost();
    int i = paramAuthScope.getPort();
    String str1;
    if (i == 443) {
      str1 = "https";
    } else {
      str1 = "http";
    }
    return Authenticator.requestPasswordAuthentication(str2, null, i, str1, null, translateScheme(paramAuthScope.getScheme()), null, paramRequestorType);
  }
  
  private static String translateScheme(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    String str = (String)SCHEME_MAP.get(paramString);
    if (str != null) {
      paramString = str;
    }
    return paramString;
  }
  
  public void clear()
  {
    this.internal.clear();
  }
  
  public Credentials getCredentials(AuthScope paramAuthScope)
  {
    Args.notNull(paramAuthScope, "Auth scope");
    Object localObject1 = this.internal.getCredentials(paramAuthScope);
    if (localObject1 != null) {
      return (Credentials)localObject1;
    }
    if (paramAuthScope.getHost() != null)
    {
      Object localObject2 = getSystemCreds(paramAuthScope, Authenticator.RequestorType.SERVER);
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = getSystemCreds(paramAuthScope, Authenticator.RequestorType.PROXY);
      }
      if (localObject1 != null)
      {
        localObject2 = System.getProperty("http.auth.ntlm.domain");
        if (localObject2 != null) {
          return new NTCredentials(((PasswordAuthentication)localObject1).getUserName(), new String(((PasswordAuthentication)localObject1).getPassword()), null, (String)localObject2);
        }
        if ("NTLM".equalsIgnoreCase(paramAuthScope.getScheme())) {
          return new NTCredentials(((PasswordAuthentication)localObject1).getUserName(), new String(((PasswordAuthentication)localObject1).getPassword()), null, null);
        }
        return new UsernamePasswordCredentials(((PasswordAuthentication)localObject1).getUserName(), new String(((PasswordAuthentication)localObject1).getPassword()));
      }
    }
    return null;
  }
  
  public void setCredentials(AuthScope paramAuthScope, Credentials paramCredentials)
  {
    this.internal.setCredentials(paramAuthScope, paramCredentials);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/SystemDefaultCredentialsProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */