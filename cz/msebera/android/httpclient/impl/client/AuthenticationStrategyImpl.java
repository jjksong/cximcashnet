package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthOption;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthSchemeProvider;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.client.AuthCache;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;

@Immutable
abstract class AuthenticationStrategyImpl
  implements AuthenticationStrategy
{
  private static final List<String> DEFAULT_SCHEME_PRIORITY = Collections.unmodifiableList(Arrays.asList(new String[] { "negotiate", "Kerberos", "NTLM", "Digest", "Basic" }));
  private final int challengeCode;
  private final String headerName;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  AuthenticationStrategyImpl(int paramInt, String paramString)
  {
    this.challengeCode = paramInt;
    this.headerName = paramString;
  }
  
  public void authFailed(HttpHost paramHttpHost, AuthScheme paramAuthScheme, HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpHost, "Host");
    Args.notNull(paramHttpContext, "HTTP context");
    AuthCache localAuthCache = HttpClientContext.adapt(paramHttpContext).getAuthCache();
    if (localAuthCache != null)
    {
      if (this.log.isDebugEnabled())
      {
        paramAuthScheme = this.log;
        paramHttpContext = new StringBuilder();
        paramHttpContext.append("Clearing cached auth scheme for ");
        paramHttpContext.append(paramHttpHost);
        paramAuthScheme.debug(paramHttpContext.toString());
      }
      localAuthCache.remove(paramHttpHost);
    }
  }
  
  public void authSucceeded(HttpHost paramHttpHost, AuthScheme paramAuthScheme, HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpHost, "Host");
    Args.notNull(paramAuthScheme, "Auth scheme");
    Args.notNull(paramHttpContext, "HTTP context");
    Object localObject2 = HttpClientContext.adapt(paramHttpContext);
    if (isCachable(paramAuthScheme))
    {
      Object localObject1 = ((HttpClientContext)localObject2).getAuthCache();
      paramHttpContext = (HttpContext)localObject1;
      if (localObject1 == null)
      {
        paramHttpContext = new BasicAuthCache();
        ((HttpClientContext)localObject2).setAuthCache(paramHttpContext);
      }
      if (this.log.isDebugEnabled())
      {
        localObject2 = this.log;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("Caching '");
        ((StringBuilder)localObject1).append(paramAuthScheme.getSchemeName());
        ((StringBuilder)localObject1).append("' auth scheme for ");
        ((StringBuilder)localObject1).append(paramHttpHost);
        ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
      }
      paramHttpContext.put(paramHttpHost, paramAuthScheme);
    }
  }
  
  public Map<String, Header> getChallenges(HttpHost paramHttpHost, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws MalformedChallengeException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    paramHttpContext = paramHttpResponse.getHeaders(this.headerName);
    paramHttpResponse = new HashMap(paramHttpContext.length);
    int m = paramHttpContext.length;
    int j = 0;
    while (j < m)
    {
      Object localObject1 = paramHttpContext[j];
      Object localObject2;
      if ((localObject1 instanceof FormattedHeader))
      {
        localObject2 = (FormattedHeader)localObject1;
        paramHttpHost = ((FormattedHeader)localObject2).getBuffer();
        i = ((FormattedHeader)localObject2).getValuePos();
      }
      else
      {
        localObject2 = ((Header)localObject1).getValue();
        if (localObject2 == null) {
          break label206;
        }
        paramHttpHost = new CharArrayBuffer(((String)localObject2).length());
        paramHttpHost.append((String)localObject2);
      }
      for (int i = 0; (i < paramHttpHost.length()) && (HTTP.isWhitespace(paramHttpHost.charAt(i))); i++) {}
      for (int k = i; (k < paramHttpHost.length()) && (!HTTP.isWhitespace(paramHttpHost.charAt(k))); k++) {}
      paramHttpResponse.put(paramHttpHost.substring(i, k).toLowerCase(Locale.ENGLISH), localObject1);
      j++;
      continue;
      label206:
      throw new MalformedChallengeException("Header value is null");
    }
    return paramHttpResponse;
  }
  
  abstract Collection<String> getPreferredAuthSchemes(RequestConfig paramRequestConfig);
  
  public boolean isAuthenticationRequested(HttpHost paramHttpHost, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    boolean bool;
    if (paramHttpResponse.getStatusLine().getStatusCode() == this.challengeCode) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected boolean isCachable(AuthScheme paramAuthScheme)
  {
    boolean bool = false;
    if ((paramAuthScheme != null) && (paramAuthScheme.isComplete()))
    {
      paramAuthScheme = paramAuthScheme.getSchemeName();
      if ((paramAuthScheme.equalsIgnoreCase("Basic")) || (paramAuthScheme.equalsIgnoreCase("Digest"))) {
        bool = true;
      }
      return bool;
    }
    return false;
  }
  
  public Queue<AuthOption> select(Map<String, Header> paramMap, HttpHost paramHttpHost, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws MalformedChallengeException
  {
    Args.notNull(paramMap, "Map of auth challenges");
    Args.notNull(paramHttpHost, "Host");
    Args.notNull(paramHttpResponse, "HTTP response");
    Args.notNull(paramHttpContext, "HTTP context");
    paramHttpResponse = HttpClientContext.adapt(paramHttpContext);
    LinkedList localLinkedList = new LinkedList();
    Lookup localLookup = paramHttpResponse.getAuthSchemeRegistry();
    if (localLookup == null)
    {
      this.log.debug("Auth scheme registry not set in the context");
      return localLinkedList;
    }
    CredentialsProvider localCredentialsProvider = paramHttpResponse.getCredentialsProvider();
    if (localCredentialsProvider == null)
    {
      this.log.debug("Credentials provider not set in the context");
      return localLinkedList;
    }
    Object localObject1 = getPreferredAuthSchemes(paramHttpResponse.getRequestConfig());
    paramHttpResponse = (HttpResponse)localObject1;
    if (localObject1 == null) {
      paramHttpResponse = DEFAULT_SCHEME_PRIORITY;
    }
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject2 = this.log;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Authentication schemes in the order of preference: ");
      ((StringBuilder)localObject1).append(paramHttpResponse);
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
    }
    paramHttpResponse = paramHttpResponse.iterator();
    while (paramHttpResponse.hasNext())
    {
      localObject1 = (String)paramHttpResponse.next();
      localObject2 = (Header)paramMap.get(((String)localObject1).toLowerCase(Locale.ENGLISH));
      Object localObject3;
      if (localObject2 != null)
      {
        localObject3 = (AuthSchemeProvider)localLookup.lookup((String)localObject1);
        if (localObject3 == null)
        {
          if (this.log.isWarnEnabled())
          {
            localObject2 = this.log;
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append("Authentication scheme ");
            ((StringBuilder)localObject3).append((String)localObject1);
            ((StringBuilder)localObject3).append(" not supported");
            ((HttpClientAndroidLog)localObject2).warn(((StringBuilder)localObject3).toString());
          }
        }
        else
        {
          localObject1 = ((AuthSchemeProvider)localObject3).create(paramHttpContext);
          ((AuthScheme)localObject1).processChallenge((Header)localObject2);
          localObject2 = localCredentialsProvider.getCredentials(new AuthScope(paramHttpHost.getHostName(), paramHttpHost.getPort(), ((AuthScheme)localObject1).getRealm(), ((AuthScheme)localObject1).getSchemeName()));
          if (localObject2 != null) {
            localLinkedList.add(new AuthOption((AuthScheme)localObject1, (Credentials)localObject2));
          }
        }
      }
      else if (this.log.isDebugEnabled())
      {
        localObject2 = this.log;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("Challenge for ");
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append(" authentication scheme not available");
        ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject3).toString());
      }
    }
    return localLinkedList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/client/AuthenticationStrategyImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */