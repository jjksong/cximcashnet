package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpServerConnection;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.impl.io.DefaultHttpRequestParserFactory;
import cz.msebera.android.httpclient.impl.io.DefaultHttpResponseWriterFactory;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.HttpMessageWriter;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

@NotThreadSafe
public class DefaultBHttpServerConnection
  extends BHttpConnectionBase
  implements HttpServerConnection
{
  private final HttpMessageParser<HttpRequest> requestParser;
  private final HttpMessageWriter<HttpResponse> responseWriter;
  
  public DefaultBHttpServerConnection(int paramInt)
  {
    this(paramInt, paramInt, null, null, null, null, null, null, null);
  }
  
  public DefaultBHttpServerConnection(int paramInt1, int paramInt2, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints, ContentLengthStrategy paramContentLengthStrategy1, ContentLengthStrategy paramContentLengthStrategy2, HttpMessageParserFactory<HttpRequest> paramHttpMessageParserFactory, HttpMessageWriterFactory<HttpResponse> paramHttpMessageWriterFactory)
  {
    super(paramInt1, paramInt2, paramCharsetDecoder, paramCharsetEncoder, paramMessageConstraints, paramContentLengthStrategy1, paramContentLengthStrategy2);
    if (paramHttpMessageParserFactory != null) {
      paramCharsetDecoder = paramHttpMessageParserFactory;
    } else {
      paramCharsetDecoder = DefaultHttpRequestParserFactory.INSTANCE;
    }
    this.requestParser = paramCharsetDecoder.create(getSessionInputBuffer(), paramMessageConstraints);
    if (paramHttpMessageWriterFactory == null) {
      paramHttpMessageWriterFactory = DefaultHttpResponseWriterFactory.INSTANCE;
    }
    this.responseWriter = paramHttpMessageWriterFactory.create(getSessionOutputBuffer());
  }
  
  public DefaultBHttpServerConnection(int paramInt, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints)
  {
    this(paramInt, paramInt, paramCharsetDecoder, paramCharsetEncoder, paramMessageConstraints, null, null, null, null);
  }
  
  public void bind(Socket paramSocket)
    throws IOException
  {
    super.bind(paramSocket);
  }
  
  public void flush()
    throws IOException
  {
    ensureOpen();
    doFlush();
  }
  
  protected void onRequestReceived(HttpRequest paramHttpRequest) {}
  
  protected void onResponseSubmitted(HttpResponse paramHttpResponse) {}
  
  public void receiveRequestEntity(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpEntityEnclosingRequest, "HTTP request");
    ensureOpen();
    paramHttpEntityEnclosingRequest.setEntity(prepareInput(paramHttpEntityEnclosingRequest));
  }
  
  public HttpRequest receiveRequestHeader()
    throws HttpException, IOException
  {
    ensureOpen();
    HttpRequest localHttpRequest = (HttpRequest)this.requestParser.parse();
    onRequestReceived(localHttpRequest);
    incrementRequestCount();
    return localHttpRequest;
  }
  
  public void sendResponseEntity(HttpResponse paramHttpResponse)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    ensureOpen();
    HttpEntity localHttpEntity = paramHttpResponse.getEntity();
    if (localHttpEntity == null) {
      return;
    }
    paramHttpResponse = prepareOutput(paramHttpResponse);
    localHttpEntity.writeTo(paramHttpResponse);
    paramHttpResponse.close();
  }
  
  public void sendResponseHeader(HttpResponse paramHttpResponse)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    ensureOpen();
    this.responseWriter.write(paramHttpResponse);
    onResponseSubmitted(paramHttpResponse);
    if (paramHttpResponse.getStatusLine().getStatusCode() >= 200) {
      incrementResponseCount();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/DefaultBHttpServerConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */