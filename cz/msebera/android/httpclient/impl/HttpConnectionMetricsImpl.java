package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.HttpConnectionMetrics;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;
import java.util.HashMap;
import java.util.Map;

@NotThreadSafe
public class HttpConnectionMetricsImpl
  implements HttpConnectionMetrics
{
  public static final String RECEIVED_BYTES_COUNT = "http.received-bytes-count";
  public static final String REQUEST_COUNT = "http.request-count";
  public static final String RESPONSE_COUNT = "http.response-count";
  public static final String SENT_BYTES_COUNT = "http.sent-bytes-count";
  private final HttpTransportMetrics inTransportMetric;
  private Map<String, Object> metricsCache;
  private final HttpTransportMetrics outTransportMetric;
  private long requestCount = 0L;
  private long responseCount = 0L;
  
  public HttpConnectionMetricsImpl(HttpTransportMetrics paramHttpTransportMetrics1, HttpTransportMetrics paramHttpTransportMetrics2)
  {
    this.inTransportMetric = paramHttpTransportMetrics1;
    this.outTransportMetric = paramHttpTransportMetrics2;
  }
  
  public Object getMetric(String paramString)
  {
    Object localObject1 = this.metricsCache;
    Object localObject2;
    if (localObject1 != null) {
      localObject2 = ((Map)localObject1).get(paramString);
    } else {
      localObject2 = null;
    }
    localObject1 = localObject2;
    if (localObject2 == null) {
      if ("http.request-count".equals(paramString))
      {
        localObject1 = Long.valueOf(this.requestCount);
      }
      else if ("http.response-count".equals(paramString))
      {
        localObject1 = Long.valueOf(this.responseCount);
      }
      else
      {
        if ("http.received-bytes-count".equals(paramString))
        {
          paramString = this.inTransportMetric;
          if (paramString != null) {
            return Long.valueOf(paramString.getBytesTransferred());
          }
          return null;
        }
        localObject1 = localObject2;
        if ("http.sent-bytes-count".equals(paramString))
        {
          paramString = this.outTransportMetric;
          if (paramString != null) {
            return Long.valueOf(paramString.getBytesTransferred());
          }
          return null;
        }
      }
    }
    return localObject1;
  }
  
  public long getReceivedBytesCount()
  {
    HttpTransportMetrics localHttpTransportMetrics = this.inTransportMetric;
    if (localHttpTransportMetrics != null) {
      return localHttpTransportMetrics.getBytesTransferred();
    }
    return -1L;
  }
  
  public long getRequestCount()
  {
    return this.requestCount;
  }
  
  public long getResponseCount()
  {
    return this.responseCount;
  }
  
  public long getSentBytesCount()
  {
    HttpTransportMetrics localHttpTransportMetrics = this.outTransportMetric;
    if (localHttpTransportMetrics != null) {
      return localHttpTransportMetrics.getBytesTransferred();
    }
    return -1L;
  }
  
  public void incrementRequestCount()
  {
    this.requestCount += 1L;
  }
  
  public void incrementResponseCount()
  {
    this.responseCount += 1L;
  }
  
  public void reset()
  {
    HttpTransportMetrics localHttpTransportMetrics = this.outTransportMetric;
    if (localHttpTransportMetrics != null) {
      localHttpTransportMetrics.reset();
    }
    localHttpTransportMetrics = this.inTransportMetric;
    if (localHttpTransportMetrics != null) {
      localHttpTransportMetrics.reset();
    }
    this.requestCount = 0L;
    this.responseCount = 0L;
    this.metricsCache = null;
  }
  
  public void setMetric(String paramString, Object paramObject)
  {
    if (this.metricsCache == null) {
      this.metricsCache = new HashMap();
    }
    this.metricsCache.put(paramString, paramObject);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/HttpConnectionMetricsImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */