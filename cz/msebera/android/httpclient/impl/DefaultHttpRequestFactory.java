package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestFactory;
import cz.msebera.android.httpclient.MethodNotSupportedException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.message.BasicHttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class DefaultHttpRequestFactory
  implements HttpRequestFactory
{
  public static final DefaultHttpRequestFactory INSTANCE = new DefaultHttpRequestFactory();
  private static final String[] RFC2616_COMMON_METHODS = { "GET" };
  private static final String[] RFC2616_ENTITY_ENC_METHODS = { "POST", "PUT" };
  private static final String[] RFC2616_SPECIAL_METHODS = { "HEAD", "OPTIONS", "DELETE", "TRACE", "CONNECT" };
  
  private static boolean isOneOf(String[] paramArrayOfString, String paramString)
  {
    int j = paramArrayOfString.length;
    for (int i = 0; i < j; i++) {
      if (paramArrayOfString[i].equalsIgnoreCase(paramString)) {
        return true;
      }
    }
    return false;
  }
  
  public HttpRequest newHttpRequest(RequestLine paramRequestLine)
    throws MethodNotSupportedException
  {
    Args.notNull(paramRequestLine, "Request line");
    String str = paramRequestLine.getMethod();
    if (isOneOf(RFC2616_COMMON_METHODS, str)) {
      return new BasicHttpRequest(paramRequestLine);
    }
    if (isOneOf(RFC2616_ENTITY_ENC_METHODS, str)) {
      return new BasicHttpEntityEnclosingRequest(paramRequestLine);
    }
    if (isOneOf(RFC2616_SPECIAL_METHODS, str)) {
      return new BasicHttpRequest(paramRequestLine);
    }
    paramRequestLine = new StringBuilder();
    paramRequestLine.append(str);
    paramRequestLine.append(" method not supported");
    throw new MethodNotSupportedException(paramRequestLine.toString());
  }
  
  public HttpRequest newHttpRequest(String paramString1, String paramString2)
    throws MethodNotSupportedException
  {
    if (isOneOf(RFC2616_COMMON_METHODS, paramString1)) {
      return new BasicHttpRequest(paramString1, paramString2);
    }
    if (isOneOf(RFC2616_ENTITY_ENC_METHODS, paramString1)) {
      return new BasicHttpEntityEnclosingRequest(paramString1, paramString2);
    }
    if (isOneOf(RFC2616_SPECIAL_METHODS, paramString1)) {
      return new BasicHttpRequest(paramString1, paramString2);
    }
    paramString2 = new StringBuilder();
    paramString2.append(paramString1);
    paramString2.append(" method not supported");
    throw new MethodNotSupportedException(paramString2.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/DefaultHttpRequestFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */