package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpConnection;
import cz.msebera.android.httpclient.HttpConnectionMetrics;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpInetConnection;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.entity.BasicHttpEntity;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.impl.entity.LaxContentLengthStrategy;
import cz.msebera.android.httpclient.impl.entity.StrictContentLengthStrategy;
import cz.msebera.android.httpclient.impl.io.ChunkedInputStream;
import cz.msebera.android.httpclient.impl.io.ChunkedOutputStream;
import cz.msebera.android.httpclient.impl.io.ContentLengthInputStream;
import cz.msebera.android.httpclient.impl.io.ContentLengthOutputStream;
import cz.msebera.android.httpclient.impl.io.HttpTransportMetricsImpl;
import cz.msebera.android.httpclient.impl.io.IdentityInputStream;
import cz.msebera.android.httpclient.impl.io.IdentityOutputStream;
import cz.msebera.android.httpclient.impl.io.SessionInputBufferImpl;
import cz.msebera.android.httpclient.impl.io.SessionOutputBufferImpl;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.NetUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.concurrent.atomic.AtomicReference;

@NotThreadSafe
public class BHttpConnectionBase
  implements HttpConnection, HttpInetConnection
{
  private final HttpConnectionMetricsImpl connMetrics;
  private final SessionInputBufferImpl inbuffer;
  private final ContentLengthStrategy incomingContentStrategy;
  private final SessionOutputBufferImpl outbuffer;
  private final ContentLengthStrategy outgoingContentStrategy;
  private final AtomicReference<Socket> socketHolder;
  
  protected BHttpConnectionBase(int paramInt1, int paramInt2, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints, ContentLengthStrategy paramContentLengthStrategy1, ContentLengthStrategy paramContentLengthStrategy2)
  {
    Args.positive(paramInt1, "Buffer size");
    HttpTransportMetricsImpl localHttpTransportMetricsImpl2 = new HttpTransportMetricsImpl();
    HttpTransportMetricsImpl localHttpTransportMetricsImpl1 = new HttpTransportMetricsImpl();
    if (paramMessageConstraints == null) {
      paramMessageConstraints = MessageConstraints.DEFAULT;
    }
    this.inbuffer = new SessionInputBufferImpl(localHttpTransportMetricsImpl2, paramInt1, -1, paramMessageConstraints, paramCharsetDecoder);
    this.outbuffer = new SessionOutputBufferImpl(localHttpTransportMetricsImpl1, paramInt1, paramInt2, paramCharsetEncoder);
    this.connMetrics = new HttpConnectionMetricsImpl(localHttpTransportMetricsImpl2, localHttpTransportMetricsImpl1);
    if (paramContentLengthStrategy1 != null) {
      paramCharsetDecoder = paramContentLengthStrategy1;
    } else {
      paramCharsetDecoder = LaxContentLengthStrategy.INSTANCE;
    }
    this.incomingContentStrategy = paramCharsetDecoder;
    if (paramContentLengthStrategy2 != null) {
      paramCharsetDecoder = paramContentLengthStrategy2;
    } else {
      paramCharsetDecoder = StrictContentLengthStrategy.INSTANCE;
    }
    this.outgoingContentStrategy = paramCharsetDecoder;
    this.socketHolder = new AtomicReference();
  }
  
  private int fillInputBuffer(int paramInt)
    throws IOException
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    int i = localSocket.getSoTimeout();
    try
    {
      localSocket.setSoTimeout(paramInt);
      paramInt = this.inbuffer.fillBuffer();
      return paramInt;
    }
    finally
    {
      localSocket.setSoTimeout(i);
    }
  }
  
  protected boolean awaitInput(int paramInt)
    throws IOException
  {
    if (this.inbuffer.hasBufferedData()) {
      return true;
    }
    fillInputBuffer(paramInt);
    return this.inbuffer.hasBufferedData();
  }
  
  protected void bind(Socket paramSocket)
    throws IOException
  {
    Args.notNull(paramSocket, "Socket");
    this.socketHolder.set(paramSocket);
    this.inbuffer.bind(null);
    this.outbuffer.bind(null);
  }
  
  /* Error */
  public void close()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 84	cz/msebera/android/httpclient/impl/BHttpConnectionBase:socketHolder	Ljava/util/concurrent/atomic/AtomicReference;
    //   4: aconst_null
    //   5: invokevirtual 140	java/util/concurrent/atomic/AtomicReference:getAndSet	(Ljava/lang/Object;)Ljava/lang/Object;
    //   8: checkcast 95	java/net/Socket
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull +39 -> 52
    //   16: aload_0
    //   17: getfield 50	cz/msebera/android/httpclient/impl/BHttpConnectionBase:inbuffer	Lcz/msebera/android/httpclient/impl/io/SessionInputBufferImpl;
    //   20: invokevirtual 143	cz/msebera/android/httpclient/impl/io/SessionInputBufferImpl:clear	()V
    //   23: aload_0
    //   24: getfield 57	cz/msebera/android/httpclient/impl/BHttpConnectionBase:outbuffer	Lcz/msebera/android/httpclient/impl/io/SessionOutputBufferImpl;
    //   27: invokevirtual 146	cz/msebera/android/httpclient/impl/io/SessionOutputBufferImpl:flush	()V
    //   30: aload_1
    //   31: invokevirtual 149	java/net/Socket:shutdownOutput	()V
    //   34: aload_1
    //   35: invokevirtual 152	java/net/Socket:shutdownInput	()V
    //   38: aload_1
    //   39: invokevirtual 154	java/net/Socket:close	()V
    //   42: goto +10 -> 52
    //   45: astore_2
    //   46: aload_1
    //   47: invokevirtual 154	java/net/Socket:close	()V
    //   50: aload_2
    //   51: athrow
    //   52: return
    //   53: astore_2
    //   54: goto -20 -> 34
    //   57: astore_2
    //   58: goto -20 -> 38
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	61	0	this	BHttpConnectionBase
    //   11	36	1	localSocket	Socket
    //   45	6	2	localObject	Object
    //   53	1	2	localIOException	IOException
    //   57	1	2	localUnsupportedOperationException	UnsupportedOperationException
    // Exception table:
    //   from	to	target	type
    //   16	30	45	finally
    //   30	34	45	finally
    //   34	38	45	finally
    //   30	34	53	java/io/IOException
    //   30	34	57	java/lang/UnsupportedOperationException
    //   34	38	57	java/io/IOException
    //   34	38	57	java/lang/UnsupportedOperationException
  }
  
  protected InputStream createInputStream(long paramLong, SessionInputBuffer paramSessionInputBuffer)
  {
    if (paramLong == -2L) {
      return new ChunkedInputStream(paramSessionInputBuffer);
    }
    if (paramLong == -1L) {
      return new IdentityInputStream(paramSessionInputBuffer);
    }
    return new ContentLengthInputStream(paramSessionInputBuffer, paramLong);
  }
  
  protected OutputStream createOutputStream(long paramLong, SessionOutputBuffer paramSessionOutputBuffer)
  {
    if (paramLong == -2L) {
      return new ChunkedOutputStream(2048, paramSessionOutputBuffer);
    }
    if (paramLong == -1L) {
      return new IdentityOutputStream(paramSessionOutputBuffer);
    }
    return new ContentLengthOutputStream(paramSessionOutputBuffer, paramLong);
  }
  
  protected void doFlush()
    throws IOException
  {
    this.outbuffer.flush();
  }
  
  protected void ensureOpen()
    throws IOException
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    boolean bool;
    if (localSocket != null) {
      bool = true;
    } else {
      bool = false;
    }
    Asserts.check(bool, "Connection is not open");
    if (!this.inbuffer.isBound()) {
      this.inbuffer.bind(getSocketInputStream(localSocket));
    }
    if (!this.outbuffer.isBound()) {
      this.outbuffer.bind(getSocketOutputStream(localSocket));
    }
  }
  
  public InetAddress getLocalAddress()
  {
    Object localObject = (Socket)this.socketHolder.get();
    if (localObject != null) {
      localObject = ((Socket)localObject).getLocalAddress();
    } else {
      localObject = null;
    }
    return (InetAddress)localObject;
  }
  
  public int getLocalPort()
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    int i;
    if (localSocket != null) {
      i = localSocket.getLocalPort();
    } else {
      i = -1;
    }
    return i;
  }
  
  public HttpConnectionMetrics getMetrics()
  {
    return this.connMetrics;
  }
  
  public InetAddress getRemoteAddress()
  {
    Object localObject = (Socket)this.socketHolder.get();
    if (localObject != null) {
      localObject = ((Socket)localObject).getInetAddress();
    } else {
      localObject = null;
    }
    return (InetAddress)localObject;
  }
  
  public int getRemotePort()
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    int i;
    if (localSocket != null) {
      i = localSocket.getPort();
    } else {
      i = -1;
    }
    return i;
  }
  
  protected SessionInputBuffer getSessionInputBuffer()
  {
    return this.inbuffer;
  }
  
  protected SessionOutputBuffer getSessionOutputBuffer()
  {
    return this.outbuffer;
  }
  
  protected Socket getSocket()
  {
    return (Socket)this.socketHolder.get();
  }
  
  protected InputStream getSocketInputStream(Socket paramSocket)
    throws IOException
  {
    return paramSocket.getInputStream();
  }
  
  protected OutputStream getSocketOutputStream(Socket paramSocket)
    throws IOException
  {
    return paramSocket.getOutputStream();
  }
  
  public int getSocketTimeout()
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    if (localSocket != null) {
      try
      {
        int i = localSocket.getSoTimeout();
        return i;
      }
      catch (SocketException localSocketException)
      {
        return -1;
      }
    }
    return -1;
  }
  
  protected void incrementRequestCount()
  {
    this.connMetrics.incrementRequestCount();
  }
  
  protected void incrementResponseCount()
  {
    this.connMetrics.incrementResponseCount();
  }
  
  public boolean isOpen()
  {
    boolean bool;
    if (this.socketHolder.get() != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isStale()
  {
    if (!isOpen()) {
      return true;
    }
    boolean bool = false;
    try
    {
      int i = fillInputBuffer(1);
      if (i < 0) {
        bool = true;
      }
      return bool;
    }
    catch (IOException localIOException)
    {
      return true;
    }
    catch (SocketTimeoutException localSocketTimeoutException) {}
    return false;
  }
  
  protected HttpEntity prepareInput(HttpMessage paramHttpMessage)
    throws HttpException
  {
    BasicHttpEntity localBasicHttpEntity = new BasicHttpEntity();
    long l = this.incomingContentStrategy.determineLength(paramHttpMessage);
    Object localObject = createInputStream(l, this.inbuffer);
    if (l == -2L)
    {
      localBasicHttpEntity.setChunked(true);
      localBasicHttpEntity.setContentLength(-1L);
      localBasicHttpEntity.setContent((InputStream)localObject);
    }
    else if (l == -1L)
    {
      localBasicHttpEntity.setChunked(false);
      localBasicHttpEntity.setContentLength(-1L);
      localBasicHttpEntity.setContent((InputStream)localObject);
    }
    else
    {
      localBasicHttpEntity.setChunked(false);
      localBasicHttpEntity.setContentLength(l);
      localBasicHttpEntity.setContent((InputStream)localObject);
    }
    localObject = paramHttpMessage.getFirstHeader("Content-Type");
    if (localObject != null) {
      localBasicHttpEntity.setContentType((Header)localObject);
    }
    paramHttpMessage = paramHttpMessage.getFirstHeader("Content-Encoding");
    if (paramHttpMessage != null) {
      localBasicHttpEntity.setContentEncoding(paramHttpMessage);
    }
    return localBasicHttpEntity;
  }
  
  protected OutputStream prepareOutput(HttpMessage paramHttpMessage)
    throws HttpException
  {
    return createOutputStream(this.outgoingContentStrategy.determineLength(paramHttpMessage), this.outbuffer);
  }
  
  public void setSocketTimeout(int paramInt)
  {
    Socket localSocket = (Socket)this.socketHolder.get();
    if (localSocket != null) {}
    try
    {
      localSocket.setSoTimeout(paramInt);
      return;
    }
    catch (SocketException localSocketException)
    {
      for (;;) {}
    }
  }
  
  public void shutdown()
    throws IOException
  {
    Socket localSocket = (Socket)this.socketHolder.getAndSet(null);
    if (localSocket != null) {
      localSocket.close();
    }
  }
  
  public String toString()
  {
    Object localObject = (Socket)this.socketHolder.get();
    if (localObject != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      SocketAddress localSocketAddress = ((Socket)localObject).getRemoteSocketAddress();
      localObject = ((Socket)localObject).getLocalSocketAddress();
      if ((localSocketAddress != null) && (localObject != null))
      {
        NetUtils.formatAddress(localStringBuilder, (SocketAddress)localObject);
        localStringBuilder.append("<->");
        NetUtils.formatAddress(localStringBuilder, localSocketAddress);
      }
      return localStringBuilder.toString();
    }
    return "[Not bound]";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/BHttpConnectionBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */