package cz.msebera.android.httpclient.impl.entity;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class StrictContentLengthStrategy
  implements ContentLengthStrategy
{
  public static final StrictContentLengthStrategy INSTANCE = new StrictContentLengthStrategy();
  private final int implicitLen;
  
  public StrictContentLengthStrategy()
  {
    this(-1);
  }
  
  public StrictContentLengthStrategy(int paramInt)
  {
    this.implicitLen = paramInt;
  }
  
  public long determineLength(HttpMessage paramHttpMessage)
    throws HttpException
  {
    Args.notNull(paramHttpMessage, "HTTP message");
    Object localObject = paramHttpMessage.getFirstHeader("Transfer-Encoding");
    if (localObject != null)
    {
      localObject = ((Header)localObject).getValue();
      if ("chunked".equalsIgnoreCase((String)localObject))
      {
        if (!paramHttpMessage.getProtocolVersion().lessEquals(HttpVersion.HTTP_1_0)) {
          return -2L;
        }
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("Chunked transfer encoding not allowed for ");
        ((StringBuilder)localObject).append(paramHttpMessage.getProtocolVersion());
        throw new ProtocolException(((StringBuilder)localObject).toString());
      }
      if ("identity".equalsIgnoreCase((String)localObject)) {
        return -1L;
      }
      paramHttpMessage = new StringBuilder();
      paramHttpMessage.append("Unsupported transfer encoding: ");
      paramHttpMessage.append((String)localObject);
      throw new ProtocolException(paramHttpMessage.toString());
    }
    paramHttpMessage = paramHttpMessage.getFirstHeader("Content-Length");
    if (paramHttpMessage != null)
    {
      paramHttpMessage = paramHttpMessage.getValue();
      try
      {
        long l = Long.parseLong(paramHttpMessage);
        if (l >= 0L) {
          return l;
        }
        localObject = new cz/msebera/android/httpclient/ProtocolException;
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>();
        localStringBuilder2.append("Negative content length: ");
        localStringBuilder2.append(paramHttpMessage);
        ((ProtocolException)localObject).<init>(localStringBuilder2.toString());
        throw ((Throwable)localObject);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("Invalid content length: ");
        localStringBuilder1.append(paramHttpMessage);
        throw new ProtocolException(localStringBuilder1.toString());
      }
    }
    return this.implicitLen;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/entity/StrictContentLengthStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */