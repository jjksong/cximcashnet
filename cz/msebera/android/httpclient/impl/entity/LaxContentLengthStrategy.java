package cz.msebera.android.httpclient.impl.entity;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class LaxContentLengthStrategy
  implements ContentLengthStrategy
{
  public static final LaxContentLengthStrategy INSTANCE = new LaxContentLengthStrategy();
  private final int implicitLen;
  
  public LaxContentLengthStrategy()
  {
    this(-1);
  }
  
  public LaxContentLengthStrategy(int paramInt)
  {
    this.implicitLen = paramInt;
  }
  
  public long determineLength(HttpMessage paramHttpMessage)
    throws HttpException
  {
    Args.notNull(paramHttpMessage, "HTTP message");
    Header localHeader = paramHttpMessage.getFirstHeader("Transfer-Encoding");
    int i;
    if (localHeader != null) {
      try
      {
        paramHttpMessage = localHeader.getElements();
        i = paramHttpMessage.length;
        if ("identity".equalsIgnoreCase(localHeader.getValue())) {
          return -1L;
        }
        if ((i > 0) && ("chunked".equalsIgnoreCase(paramHttpMessage[(i - 1)].getName()))) {
          return -2L;
        }
        return -1L;
      }
      catch (ParseException paramHttpMessage)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Invalid Transfer-Encoding header value: ");
        localStringBuilder.append(localHeader);
        throw new ProtocolException(localStringBuilder.toString(), paramHttpMessage);
      }
    }
    if (paramHttpMessage.getFirstHeader("Content-Length") != null)
    {
      paramHttpMessage = paramHttpMessage.getHeaders("Content-Length");
      i = paramHttpMessage.length - 1;
      while (i >= 0)
      {
        localHeader = paramHttpMessage[i];
        try
        {
          l = Long.parseLong(localHeader.getValue());
        }
        catch (NumberFormatException localNumberFormatException)
        {
          i--;
        }
      }
      long l = -1L;
      if (l >= 0L) {
        return l;
      }
      return -1L;
    }
    return this.implicitLen;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/entity/LaxContentLengthStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */