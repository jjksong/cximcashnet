package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.MalformedChunkCodingException;
import cz.msebera.android.httpclient.TruncatedChunkException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.BufferInfo;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
public class ChunkedInputStream
  extends InputStream
{
  private static final int BUFFER_SIZE = 2048;
  private static final int CHUNK_CRLF = 3;
  private static final int CHUNK_DATA = 2;
  private static final int CHUNK_LEN = 1;
  private final CharArrayBuffer buffer;
  private int chunkSize;
  private boolean closed = false;
  private boolean eof = false;
  private Header[] footers = new Header[0];
  private final SessionInputBuffer in;
  private int pos;
  private int state;
  
  public ChunkedInputStream(SessionInputBuffer paramSessionInputBuffer)
  {
    this.in = ((SessionInputBuffer)Args.notNull(paramSessionInputBuffer, "Session input buffer"));
    this.pos = 0;
    this.buffer = new CharArrayBuffer(16);
    this.state = 1;
  }
  
  private int getChunkSize()
    throws IOException
  {
    int i = this.state;
    if (i != 1) {
      if (i == 3)
      {
        this.buffer.clear();
        if (this.in.readLine(this.buffer) == -1) {
          return 0;
        }
        if (this.buffer.isEmpty()) {
          this.state = 1;
        } else {
          throw new MalformedChunkCodingException("Unexpected content at the end of chunk");
        }
      }
      else
      {
        throw new IllegalStateException("Inconsistent codec state");
      }
    }
    this.buffer.clear();
    if (this.in.readLine(this.buffer) == -1) {
      return 0;
    }
    int j = this.buffer.indexOf(59);
    i = j;
    if (j < 0) {
      i = this.buffer.length();
    }
    try
    {
      i = Integer.parseInt(this.buffer.substringTrimmed(0, i), 16);
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new MalformedChunkCodingException("Bad chunk header");
    }
  }
  
  private void nextChunk()
    throws IOException
  {
    this.chunkSize = getChunkSize();
    int i = this.chunkSize;
    if (i >= 0)
    {
      this.state = 2;
      this.pos = 0;
      if (i == 0)
      {
        this.eof = true;
        parseTrailerHeaders();
      }
      return;
    }
    throw new MalformedChunkCodingException("Negative chunk size");
  }
  
  private void parseTrailerHeaders()
    throws IOException
  {
    try
    {
      this.footers = AbstractMessageParser.parseHeaders(this.in, -1, -1, null);
      return;
    }
    catch (HttpException localHttpException)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Invalid footer: ");
      ((StringBuilder)localObject).append(localHttpException.getMessage());
      localObject = new MalformedChunkCodingException(((StringBuilder)localObject).toString());
      ((IOException)localObject).initCause(localHttpException);
      throw ((Throwable)localObject);
    }
  }
  
  public int available()
    throws IOException
  {
    SessionInputBuffer localSessionInputBuffer = this.in;
    if ((localSessionInputBuffer instanceof BufferInfo)) {
      return Math.min(((BufferInfo)localSessionInputBuffer).length(), this.chunkSize - this.pos);
    }
    return 0;
  }
  
  public void close()
    throws IOException
  {
    if (!this.closed) {
      try
      {
        if (!this.eof)
        {
          byte[] arrayOfByte = new byte['ࠀ'];
          for (;;)
          {
            int i = read(arrayOfByte);
            if (i < 0) {
              break;
            }
          }
        }
      }
      finally
      {
        this.eof = true;
        this.closed = true;
      }
    }
  }
  
  public Header[] getFooters()
  {
    return (Header[])this.footers.clone();
  }
  
  public int read()
    throws IOException
  {
    if (!this.closed)
    {
      if (this.eof) {
        return -1;
      }
      if (this.state != 2)
      {
        nextChunk();
        if (this.eof) {
          return -1;
        }
      }
      int i = this.in.read();
      if (i != -1)
      {
        this.pos += 1;
        if (this.pos >= this.chunkSize) {
          this.state = 3;
        }
      }
      return i;
    }
    throw new IOException("Attempted read from closed stream.");
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (!this.closed)
    {
      if (this.eof) {
        return -1;
      }
      if (this.state != 2)
      {
        nextChunk();
        if (this.eof) {
          return -1;
        }
      }
      paramInt1 = this.in.read(paramArrayOfByte, paramInt1, Math.min(paramInt2, this.chunkSize - this.pos));
      if (paramInt1 != -1)
      {
        this.pos += paramInt1;
        if (this.pos >= this.chunkSize) {
          this.state = 3;
        }
        return paramInt1;
      }
      this.eof = true;
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("Truncated chunk ( expected size: ");
      paramArrayOfByte.append(this.chunkSize);
      paramArrayOfByte.append("; actual size: ");
      paramArrayOfByte.append(this.pos);
      paramArrayOfByte.append(")");
      throw new TruncatedChunkException(paramArrayOfByte.toString());
    }
    throw new IOException("Attempted read from closed stream.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/ChunkedInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */