package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.EofSensor;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.Socket;

@Deprecated
@NotThreadSafe
public class SocketInputBuffer
  extends AbstractSessionInputBuffer
  implements EofSensor
{
  private boolean eof;
  private final Socket socket;
  
  public SocketInputBuffer(Socket paramSocket, int paramInt, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramSocket, "Socket");
    this.socket = paramSocket;
    this.eof = false;
    int i = paramInt;
    if (paramInt < 0) {
      i = paramSocket.getReceiveBufferSize();
    }
    paramInt = i;
    if (i < 1024) {
      paramInt = 1024;
    }
    init(paramSocket.getInputStream(), paramInt, paramHttpParams);
  }
  
  protected int fillBuffer()
    throws IOException
  {
    int i = super.fillBuffer();
    boolean bool;
    if (i == -1) {
      bool = true;
    } else {
      bool = false;
    }
    this.eof = bool;
    return i;
  }
  
  /* Error */
  public boolean isDataAvailable(int paramInt)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 56	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:hasBufferedData	()Z
    //   4: istore_3
    //   5: iload_3
    //   6: ifne +53 -> 59
    //   9: aload_0
    //   10: getfield 29	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:socket	Ljava/net/Socket;
    //   13: invokevirtual 59	java/net/Socket:getSoTimeout	()I
    //   16: istore_2
    //   17: aload_0
    //   18: getfield 29	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:socket	Ljava/net/Socket;
    //   21: iload_1
    //   22: invokevirtual 63	java/net/Socket:setSoTimeout	(I)V
    //   25: aload_0
    //   26: invokevirtual 64	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:fillBuffer	()I
    //   29: pop
    //   30: aload_0
    //   31: invokevirtual 56	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:hasBufferedData	()Z
    //   34: istore_3
    //   35: aload_0
    //   36: getfield 29	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:socket	Ljava/net/Socket;
    //   39: iload_2
    //   40: invokevirtual 63	java/net/Socket:setSoTimeout	(I)V
    //   43: goto +16 -> 59
    //   46: astore 4
    //   48: aload_0
    //   49: getfield 29	cz/msebera/android/httpclient/impl/io/SocketInputBuffer:socket	Ljava/net/Socket;
    //   52: iload_2
    //   53: invokevirtual 63	java/net/Socket:setSoTimeout	(I)V
    //   56: aload 4
    //   58: athrow
    //   59: iload_3
    //   60: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	61	0	this	SocketInputBuffer
    //   0	61	1	paramInt	int
    //   16	37	2	i	int
    //   4	56	3	bool	boolean
    //   46	11	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   17	35	46	finally
  }
  
  public boolean isEof()
  {
    return this.eof;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/SocketInputBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */