package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestFactory;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.impl.DefaultHttpRequestFactory;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.message.BasicLineParser;
import cz.msebera.android.httpclient.message.LineParser;

@Immutable
public class DefaultHttpRequestParserFactory
  implements HttpMessageParserFactory<HttpRequest>
{
  public static final DefaultHttpRequestParserFactory INSTANCE = new DefaultHttpRequestParserFactory();
  private final LineParser lineParser;
  private final HttpRequestFactory requestFactory;
  
  public DefaultHttpRequestParserFactory()
  {
    this(null, null);
  }
  
  public DefaultHttpRequestParserFactory(LineParser paramLineParser, HttpRequestFactory paramHttpRequestFactory)
  {
    if (paramLineParser == null) {
      paramLineParser = BasicLineParser.INSTANCE;
    }
    this.lineParser = paramLineParser;
    if (paramHttpRequestFactory == null) {
      paramHttpRequestFactory = DefaultHttpRequestFactory.INSTANCE;
    }
    this.requestFactory = paramHttpRequestFactory;
  }
  
  public HttpMessageParser<HttpRequest> create(SessionInputBuffer paramSessionInputBuffer, MessageConstraints paramMessageConstraints)
  {
    return new DefaultHttpRequestParser(paramSessionInputBuffer, this.lineParser, this.requestFactory, paramMessageConstraints);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/DefaultHttpRequestParserFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */