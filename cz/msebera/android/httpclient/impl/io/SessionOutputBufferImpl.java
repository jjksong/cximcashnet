package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.BufferInfo;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;

@NotThreadSafe
public class SessionOutputBufferImpl
  implements SessionOutputBuffer, BufferInfo
{
  private static final byte[] CRLF = { 13, 10 };
  private ByteBuffer bbuf;
  private final ByteArrayBuffer buffer;
  private final CharsetEncoder encoder;
  private final int fragementSizeHint;
  private final HttpTransportMetricsImpl metrics;
  private OutputStream outstream;
  
  public SessionOutputBufferImpl(HttpTransportMetricsImpl paramHttpTransportMetricsImpl, int paramInt)
  {
    this(paramHttpTransportMetricsImpl, paramInt, paramInt, null);
  }
  
  public SessionOutputBufferImpl(HttpTransportMetricsImpl paramHttpTransportMetricsImpl, int paramInt1, int paramInt2, CharsetEncoder paramCharsetEncoder)
  {
    Args.positive(paramInt1, "Buffer size");
    Args.notNull(paramHttpTransportMetricsImpl, "HTTP transport metrcis");
    this.metrics = paramHttpTransportMetricsImpl;
    this.buffer = new ByteArrayBuffer(paramInt1);
    if (paramInt2 < 0) {
      paramInt2 = 0;
    }
    this.fragementSizeHint = paramInt2;
    this.encoder = paramCharsetEncoder;
  }
  
  private void flushBuffer()
    throws IOException
  {
    int i = this.buffer.length();
    if (i > 0)
    {
      streamWrite(this.buffer.buffer(), 0, i);
      this.buffer.clear();
      this.metrics.incrementBytesTransferred(i);
    }
  }
  
  private void flushStream()
    throws IOException
  {
    OutputStream localOutputStream = this.outstream;
    if (localOutputStream != null) {
      localOutputStream.flush();
    }
  }
  
  private void handleEncodingResult(CoderResult paramCoderResult)
    throws IOException
  {
    if (paramCoderResult.isError()) {
      paramCoderResult.throwException();
    }
    this.bbuf.flip();
    while (this.bbuf.hasRemaining()) {
      write(this.bbuf.get());
    }
    this.bbuf.compact();
  }
  
  private void streamWrite(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    Asserts.notNull(this.outstream, "Output stream");
    this.outstream.write(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  private void writeEncoded(CharBuffer paramCharBuffer)
    throws IOException
  {
    if (!paramCharBuffer.hasRemaining()) {
      return;
    }
    if (this.bbuf == null) {
      this.bbuf = ByteBuffer.allocate(1024);
    }
    this.encoder.reset();
    while (paramCharBuffer.hasRemaining()) {
      handleEncodingResult(this.encoder.encode(paramCharBuffer, this.bbuf, true));
    }
    handleEncodingResult(this.encoder.flush(this.bbuf));
    this.bbuf.clear();
  }
  
  public int available()
  {
    return capacity() - length();
  }
  
  public void bind(OutputStream paramOutputStream)
  {
    this.outstream = paramOutputStream;
  }
  
  public int capacity()
  {
    return this.buffer.capacity();
  }
  
  public void flush()
    throws IOException
  {
    flushBuffer();
    flushStream();
  }
  
  public HttpTransportMetrics getMetrics()
  {
    return this.metrics;
  }
  
  public boolean isBound()
  {
    boolean bool;
    if (this.outstream != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int length()
  {
    return this.buffer.length();
  }
  
  public void write(int paramInt)
    throws IOException
  {
    if (this.fragementSizeHint > 0)
    {
      if (this.buffer.isFull()) {
        flushBuffer();
      }
      this.buffer.append(paramInt);
    }
    else
    {
      flushBuffer();
      this.outstream.write(paramInt);
    }
  }
  
  public void write(byte[] paramArrayOfByte)
    throws IOException
  {
    if (paramArrayOfByte == null) {
      return;
    }
    write(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramArrayOfByte == null) {
      return;
    }
    if ((paramInt2 <= this.fragementSizeHint) && (paramInt2 <= this.buffer.capacity()))
    {
      if (paramInt2 > this.buffer.capacity() - this.buffer.length()) {
        flushBuffer();
      }
      this.buffer.append(paramArrayOfByte, paramInt1, paramInt2);
    }
    else
    {
      flushBuffer();
      streamWrite(paramArrayOfByte, paramInt1, paramInt2);
      this.metrics.incrementBytesTransferred(paramInt2);
    }
  }
  
  public void writeLine(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    if (paramCharArrayBuffer == null) {
      return;
    }
    CharsetEncoder localCharsetEncoder = this.encoder;
    int i = 0;
    if (localCharsetEncoder == null)
    {
      int j = paramCharArrayBuffer.length();
      while (j > 0)
      {
        int k = Math.min(this.buffer.capacity() - this.buffer.length(), j);
        if (k > 0) {
          this.buffer.append(paramCharArrayBuffer, i, k);
        }
        if (this.buffer.isFull()) {
          flushBuffer();
        }
        i += k;
        j -= k;
      }
    }
    writeEncoded(CharBuffer.wrap(paramCharArrayBuffer.buffer(), 0, paramCharArrayBuffer.length()));
    write(CRLF);
  }
  
  public void writeLine(String paramString)
    throws IOException
  {
    if (paramString == null) {
      return;
    }
    if (paramString.length() > 0)
    {
      if (this.encoder == null) {
        for (int i = 0; i < paramString.length(); i++) {
          write(paramString.charAt(i));
        }
      }
      writeEncoded(CharBuffer.wrap(paramString));
    }
    write(CRLF);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/SessionOutputBufferImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */