package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;

@NotThreadSafe
public class HttpTransportMetricsImpl
  implements HttpTransportMetrics
{
  private long bytesTransferred = 0L;
  
  public long getBytesTransferred()
  {
    return this.bytesTransferred;
  }
  
  public void incrementBytesTransferred(long paramLong)
  {
    this.bytesTransferred += paramLong;
  }
  
  public void reset()
  {
    this.bytesTransferred = 0L;
  }
  
  public void setBytesTransferred(long paramLong)
  {
    this.bytesTransferred = paramLong;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/HttpTransportMetricsImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */