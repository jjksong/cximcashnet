package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.io.HttpMessageWriter;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.message.BasicLineFormatter;
import cz.msebera.android.httpclient.message.LineFormatter;

@Immutable
public class DefaultHttpResponseWriterFactory
  implements HttpMessageWriterFactory<HttpResponse>
{
  public static final DefaultHttpResponseWriterFactory INSTANCE = new DefaultHttpResponseWriterFactory();
  private final LineFormatter lineFormatter;
  
  public DefaultHttpResponseWriterFactory()
  {
    this(null);
  }
  
  public DefaultHttpResponseWriterFactory(LineFormatter paramLineFormatter)
  {
    if (paramLineFormatter == null) {
      paramLineFormatter = BasicLineFormatter.INSTANCE;
    }
    this.lineFormatter = paramLineFormatter;
  }
  
  public HttpMessageWriter<HttpResponse> create(SessionOutputBuffer paramSessionOutputBuffer)
  {
    return new DefaultHttpResponseWriter(paramSessionOutputBuffer, this.lineFormatter);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/DefaultHttpResponseWriterFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */