package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import java.io.IOException;
import java.io.OutputStream;

@NotThreadSafe
public class ChunkedOutputStream
  extends OutputStream
{
  private final byte[] cache;
  private int cachePosition = 0;
  private boolean closed = false;
  private final SessionOutputBuffer out;
  private boolean wroteLastChunk = false;
  
  public ChunkedOutputStream(int paramInt, SessionOutputBuffer paramSessionOutputBuffer)
  {
    this.cache = new byte[paramInt];
    this.out = paramSessionOutputBuffer;
  }
  
  @Deprecated
  public ChunkedOutputStream(SessionOutputBuffer paramSessionOutputBuffer)
    throws IOException
  {
    this(2048, paramSessionOutputBuffer);
  }
  
  @Deprecated
  public ChunkedOutputStream(SessionOutputBuffer paramSessionOutputBuffer, int paramInt)
    throws IOException
  {
    this(paramInt, paramSessionOutputBuffer);
  }
  
  public void close()
    throws IOException
  {
    if (!this.closed)
    {
      this.closed = true;
      finish();
      this.out.flush();
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.wroteLastChunk)
    {
      flushCache();
      writeClosingChunk();
      this.wroteLastChunk = true;
    }
  }
  
  public void flush()
    throws IOException
  {
    flushCache();
    this.out.flush();
  }
  
  protected void flushCache()
    throws IOException
  {
    int i = this.cachePosition;
    if (i > 0)
    {
      this.out.writeLine(Integer.toHexString(i));
      this.out.write(this.cache, 0, this.cachePosition);
      this.out.writeLine("");
      this.cachePosition = 0;
    }
  }
  
  protected void flushCacheWithAppend(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.out.writeLine(Integer.toHexString(this.cachePosition + paramInt2));
    this.out.write(this.cache, 0, this.cachePosition);
    this.out.write(paramArrayOfByte, paramInt1, paramInt2);
    this.out.writeLine("");
    this.cachePosition = 0;
  }
  
  public void write(int paramInt)
    throws IOException
  {
    if (!this.closed)
    {
      byte[] arrayOfByte = this.cache;
      int i = this.cachePosition;
      arrayOfByte[i] = ((byte)paramInt);
      this.cachePosition = (i + 1);
      if (this.cachePosition == arrayOfByte.length) {
        flushCache();
      }
      return;
    }
    throw new IOException("Attempted write to closed stream.");
  }
  
  public void write(byte[] paramArrayOfByte)
    throws IOException
  {
    write(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (!this.closed)
    {
      byte[] arrayOfByte = this.cache;
      int j = arrayOfByte.length;
      int i = this.cachePosition;
      if (paramInt2 >= j - i)
      {
        flushCacheWithAppend(paramArrayOfByte, paramInt1, paramInt2);
      }
      else
      {
        System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, i, paramInt2);
        this.cachePosition += paramInt2;
      }
      return;
    }
    throw new IOException("Attempted write to closed stream.");
  }
  
  protected void writeClosingChunk()
    throws IOException
  {
    this.out.writeLine("0");
    this.out.writeLine("");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/ChunkedOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */