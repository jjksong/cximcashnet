package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpMessage;
import cz.msebera.android.httpclient.MessageConstraintException;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.message.BasicLineParser;
import cz.msebera.android.httpclient.message.LineParser;
import cz.msebera.android.httpclient.params.HttpParamConfig;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NotThreadSafe
public abstract class AbstractMessageParser<T extends HttpMessage>
  implements HttpMessageParser<T>
{
  private static final int HEADERS = 1;
  private static final int HEAD_LINE = 0;
  private final List<CharArrayBuffer> headerLines;
  protected final LineParser lineParser;
  private T message;
  private final MessageConstraints messageConstraints;
  private final SessionInputBuffer sessionBuffer;
  private int state;
  
  public AbstractMessageParser(SessionInputBuffer paramSessionInputBuffer, LineParser paramLineParser, MessageConstraints paramMessageConstraints)
  {
    this.sessionBuffer = ((SessionInputBuffer)Args.notNull(paramSessionInputBuffer, "Session input buffer"));
    if (paramLineParser == null) {
      paramLineParser = BasicLineParser.INSTANCE;
    }
    this.lineParser = paramLineParser;
    if (paramMessageConstraints == null) {
      paramMessageConstraints = MessageConstraints.DEFAULT;
    }
    this.messageConstraints = paramMessageConstraints;
    this.headerLines = new ArrayList();
    this.state = 0;
  }
  
  @Deprecated
  public AbstractMessageParser(SessionInputBuffer paramSessionInputBuffer, LineParser paramLineParser, HttpParams paramHttpParams)
  {
    Args.notNull(paramSessionInputBuffer, "Session input buffer");
    Args.notNull(paramHttpParams, "HTTP parameters");
    this.sessionBuffer = paramSessionInputBuffer;
    this.messageConstraints = HttpParamConfig.getMessageConstraints(paramHttpParams);
    if (paramLineParser == null) {
      paramLineParser = BasicLineParser.INSTANCE;
    }
    this.lineParser = paramLineParser;
    this.headerLines = new ArrayList();
    this.state = 0;
  }
  
  public static Header[] parseHeaders(SessionInputBuffer paramSessionInputBuffer, int paramInt1, int paramInt2, LineParser paramLineParser)
    throws HttpException, IOException
  {
    ArrayList localArrayList = new ArrayList();
    if (paramLineParser == null) {
      paramLineParser = BasicLineParser.INSTANCE;
    }
    return parseHeaders(paramSessionInputBuffer, paramInt1, paramInt2, paramLineParser, localArrayList);
  }
  
  public static Header[] parseHeaders(SessionInputBuffer paramSessionInputBuffer, int paramInt1, int paramInt2, LineParser paramLineParser, List<CharArrayBuffer> paramList)
    throws HttpException, IOException
  {
    Args.notNull(paramSessionInputBuffer, "Session input buffer");
    Args.notNull(paramLineParser, "Line parser");
    Args.notNull(paramList, "Header line list");
    Object localObject1 = null;
    Object localObject3 = localObject1;
    int j;
    for (;;)
    {
      if (localObject1 == null) {
        localObject1 = new CharArrayBuffer(64);
      } else {
        ((CharArrayBuffer)localObject1).clear();
      }
      int k = paramSessionInputBuffer.readLine((CharArrayBuffer)localObject1);
      j = 0;
      int i = 0;
      if ((k == -1) || (((CharArrayBuffer)localObject1).length() < 1)) {
        break label289;
      }
      Object localObject2;
      Object localObject4;
      if (((((CharArrayBuffer)localObject1).charAt(0) == ' ') || (((CharArrayBuffer)localObject1).charAt(0) == '\t')) && (localObject3 != null))
      {
        while (i < ((CharArrayBuffer)localObject1).length())
        {
          j = ((CharArrayBuffer)localObject1).charAt(i);
          if ((j != 32) && (j != 9)) {
            break;
          }
          i++;
        }
        if ((paramInt2 > 0) && (((CharArrayBuffer)localObject3).length() + 1 + ((CharArrayBuffer)localObject1).length() - i > paramInt2)) {
          throw new MessageConstraintException("Maximum line length limit exceeded");
        }
        ((CharArrayBuffer)localObject3).append(' ');
        ((CharArrayBuffer)localObject3).append((CharArrayBuffer)localObject1, i, ((CharArrayBuffer)localObject1).length() - i);
        localObject2 = localObject1;
        localObject4 = localObject3;
      }
      else
      {
        paramList.add(localObject1);
        localObject2 = null;
        localObject4 = localObject1;
      }
      localObject1 = localObject2;
      localObject3 = localObject4;
      if (paramInt1 > 0)
      {
        if (paramList.size() >= paramInt1) {
          break;
        }
        localObject1 = localObject2;
        localObject3 = localObject4;
      }
    }
    throw new MessageConstraintException("Maximum header count exceeded");
    label289:
    localObject1 = new Header[paramList.size()];
    paramInt1 = j;
    while (paramInt1 < paramList.size())
    {
      paramSessionInputBuffer = (CharArrayBuffer)paramList.get(paramInt1);
      try
      {
        localObject1[paramInt1] = paramLineParser.parseHeader(paramSessionInputBuffer);
        paramInt1++;
      }
      catch (ParseException paramSessionInputBuffer)
      {
        throw new ProtocolException(paramSessionInputBuffer.getMessage());
      }
    }
    return (Header[])localObject1;
  }
  
  public T parse()
    throws IOException, HttpException
  {
    switch (this.state)
    {
    default: 
      throw new IllegalStateException("Inconsistent parser state");
    }
    try
    {
      this.message = parseHead(this.sessionBuffer);
      this.state = 1;
      Object localObject = parseHeaders(this.sessionBuffer, this.messageConstraints.getMaxHeaderCount(), this.messageConstraints.getMaxLineLength(), this.lineParser, this.headerLines);
      this.message.setHeaders((Header[])localObject);
      localObject = this.message;
      this.message = null;
      this.headerLines.clear();
      this.state = 0;
      return (T)localObject;
    }
    catch (ParseException localParseException)
    {
      throw new ProtocolException(localParseException.getMessage(), localParseException);
    }
  }
  
  protected abstract T parseHead(SessionInputBuffer paramSessionInputBuffer)
    throws IOException, HttpException, ParseException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/AbstractMessageParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */