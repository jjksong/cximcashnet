package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.ConnectionClosedException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.BufferInfo;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
public class ContentLengthInputStream
  extends InputStream
{
  private static final int BUFFER_SIZE = 2048;
  private boolean closed = false;
  private final long contentLength;
  private SessionInputBuffer in = null;
  private long pos = 0L;
  
  public ContentLengthInputStream(SessionInputBuffer paramSessionInputBuffer, long paramLong)
  {
    this.in = ((SessionInputBuffer)Args.notNull(paramSessionInputBuffer, "Session input buffer"));
    this.contentLength = Args.notNegative(paramLong, "Content length");
  }
  
  public int available()
    throws IOException
  {
    SessionInputBuffer localSessionInputBuffer = this.in;
    if ((localSessionInputBuffer instanceof BufferInfo)) {
      return Math.min(((BufferInfo)localSessionInputBuffer).length(), (int)(this.contentLength - this.pos));
    }
    return 0;
  }
  
  public void close()
    throws IOException
  {
    if (!this.closed) {
      try
      {
        if (this.pos < this.contentLength)
        {
          byte[] arrayOfByte = new byte['ࠀ'];
          for (;;)
          {
            int i = read(arrayOfByte);
            if (i < 0) {
              break;
            }
          }
        }
      }
      finally
      {
        this.closed = true;
      }
    }
  }
  
  public int read()
    throws IOException
  {
    if (!this.closed)
    {
      if (this.pos >= this.contentLength) {
        return -1;
      }
      int i = this.in.read();
      if (i == -1)
      {
        if (this.pos < this.contentLength)
        {
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Premature end of Content-Length delimited message body (expected: ");
          localStringBuilder.append(this.contentLength);
          localStringBuilder.append("; received: ");
          localStringBuilder.append(this.pos);
          throw new ConnectionClosedException(localStringBuilder.toString());
        }
      }
      else {
        this.pos += 1L;
      }
      return i;
    }
    throw new IOException("Attempted read from closed stream.");
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (!this.closed)
    {
      long l1 = this.pos;
      long l2 = this.contentLength;
      if (l1 >= l2) {
        return -1;
      }
      int i = paramInt2;
      if (paramInt2 + l1 > l2) {
        i = (int)(l2 - l1);
      }
      paramInt1 = this.in.read(paramArrayOfByte, paramInt1, i);
      if ((paramInt1 == -1) && (this.pos < this.contentLength))
      {
        paramArrayOfByte = new StringBuilder();
        paramArrayOfByte.append("Premature end of Content-Length delimited message body (expected: ");
        paramArrayOfByte.append(this.contentLength);
        paramArrayOfByte.append("; received: ");
        paramArrayOfByte.append(this.pos);
        throw new ConnectionClosedException(paramArrayOfByte.toString());
      }
      if (paramInt1 > 0) {
        this.pos += paramInt1;
      }
      return paramInt1;
    }
    throw new IOException("Attempted read from closed stream.");
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    if (paramLong <= 0L) {
      return 0L;
    }
    byte[] arrayOfByte = new byte['ࠀ'];
    paramLong = Math.min(paramLong, this.contentLength - this.pos);
    long l1 = 0L;
    while (paramLong > 0L)
    {
      int i = read(arrayOfByte, 0, (int)Math.min(2048L, paramLong));
      if (i == -1) {
        break;
      }
      long l2 = i;
      l1 += l2;
      paramLong -= l2;
    }
    return l1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/ContentLengthInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */