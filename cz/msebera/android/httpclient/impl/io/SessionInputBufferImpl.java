package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.MessageConstraintException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.io.BufferInfo;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

@NotThreadSafe
public class SessionInputBufferImpl
  implements SessionInputBuffer, BufferInfo
{
  private final byte[] buffer;
  private int bufferlen;
  private int bufferpos;
  private CharBuffer cbuf;
  private final MessageConstraints constraints;
  private final CharsetDecoder decoder;
  private InputStream instream;
  private final ByteArrayBuffer linebuffer;
  private final HttpTransportMetricsImpl metrics;
  private final int minChunkLimit;
  
  public SessionInputBufferImpl(HttpTransportMetricsImpl paramHttpTransportMetricsImpl, int paramInt)
  {
    this(paramHttpTransportMetricsImpl, paramInt, paramInt, null, null);
  }
  
  public SessionInputBufferImpl(HttpTransportMetricsImpl paramHttpTransportMetricsImpl, int paramInt1, int paramInt2, MessageConstraints paramMessageConstraints, CharsetDecoder paramCharsetDecoder)
  {
    Args.notNull(paramHttpTransportMetricsImpl, "HTTP transport metrcis");
    Args.positive(paramInt1, "Buffer size");
    this.metrics = paramHttpTransportMetricsImpl;
    this.buffer = new byte[paramInt1];
    this.bufferpos = 0;
    this.bufferlen = 0;
    if (paramInt2 < 0) {
      paramInt2 = 512;
    }
    this.minChunkLimit = paramInt2;
    if (paramMessageConstraints == null) {
      paramMessageConstraints = MessageConstraints.DEFAULT;
    }
    this.constraints = paramMessageConstraints;
    this.linebuffer = new ByteArrayBuffer(paramInt1);
    this.decoder = paramCharsetDecoder;
  }
  
  private int appendDecoded(CharArrayBuffer paramCharArrayBuffer, ByteBuffer paramByteBuffer)
    throws IOException
  {
    boolean bool = paramByteBuffer.hasRemaining();
    int i = 0;
    if (!bool) {
      return 0;
    }
    if (this.cbuf == null) {
      this.cbuf = CharBuffer.allocate(1024);
    }
    this.decoder.reset();
    while (paramByteBuffer.hasRemaining()) {
      i += handleDecodingResult(this.decoder.decode(paramByteBuffer, this.cbuf, true), paramCharArrayBuffer, paramByteBuffer);
    }
    int j = handleDecodingResult(this.decoder.flush(this.cbuf), paramCharArrayBuffer, paramByteBuffer);
    this.cbuf.clear();
    return i + j;
  }
  
  private int handleDecodingResult(CoderResult paramCoderResult, CharArrayBuffer paramCharArrayBuffer, ByteBuffer paramByteBuffer)
    throws IOException
  {
    if (paramCoderResult.isError()) {
      paramCoderResult.throwException();
    }
    this.cbuf.flip();
    int i = this.cbuf.remaining();
    while (this.cbuf.hasRemaining()) {
      paramCharArrayBuffer.append(this.cbuf.get());
    }
    this.cbuf.compact();
    return i;
  }
  
  private int lineFromLineBuffer(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    int k = this.linebuffer.length();
    int i = k;
    if (k > 0)
    {
      int j = k;
      if (this.linebuffer.byteAt(k - 1) == 10) {
        j = k - 1;
      }
      i = j;
      if (j > 0)
      {
        i = j;
        if (this.linebuffer.byteAt(j - 1) == 13) {
          i = j - 1;
        }
      }
    }
    if (this.decoder == null) {
      paramCharArrayBuffer.append(this.linebuffer, 0, i);
    } else {
      i = appendDecoded(paramCharArrayBuffer, ByteBuffer.wrap(this.linebuffer.buffer(), 0, i));
    }
    this.linebuffer.clear();
    return i;
  }
  
  private int lineFromReadBuffer(CharArrayBuffer paramCharArrayBuffer, int paramInt)
    throws IOException
  {
    int j = this.bufferpos;
    this.bufferpos = (paramInt + 1);
    int i = paramInt;
    if (paramInt > j)
    {
      i = paramInt;
      if (this.buffer[(paramInt - 1)] == 13) {
        i = paramInt - 1;
      }
    }
    paramInt = i - j;
    if (this.decoder == null) {
      paramCharArrayBuffer.append(this.buffer, j, paramInt);
    } else {
      paramInt = appendDecoded(paramCharArrayBuffer, ByteBuffer.wrap(this.buffer, j, paramInt));
    }
    return paramInt;
  }
  
  private int locateLF()
  {
    for (int i = this.bufferpos; i < this.bufferlen; i++) {
      if (this.buffer[i] == 10) {
        return i;
      }
    }
    return -1;
  }
  
  private int streamRead(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    Asserts.notNull(this.instream, "Input stream");
    return this.instream.read(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public int available()
  {
    return capacity() - length();
  }
  
  public void bind(InputStream paramInputStream)
  {
    this.instream = paramInputStream;
  }
  
  public int capacity()
  {
    return this.buffer.length;
  }
  
  public void clear()
  {
    this.bufferpos = 0;
    this.bufferlen = 0;
  }
  
  public int fillBuffer()
    throws IOException
  {
    int j = this.bufferpos;
    if (j > 0)
    {
      i = this.bufferlen - j;
      if (i > 0)
      {
        arrayOfByte = this.buffer;
        System.arraycopy(arrayOfByte, j, arrayOfByte, 0, i);
      }
      this.bufferpos = 0;
      this.bufferlen = i;
    }
    int i = this.bufferlen;
    byte[] arrayOfByte = this.buffer;
    j = streamRead(arrayOfByte, i, arrayOfByte.length - i);
    if (j == -1) {
      return -1;
    }
    this.bufferlen = (i + j);
    this.metrics.incrementBytesTransferred(j);
    return j;
  }
  
  public HttpTransportMetrics getMetrics()
  {
    return this.metrics;
  }
  
  public boolean hasBufferedData()
  {
    boolean bool;
    if (this.bufferpos < this.bufferlen) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isBound()
  {
    boolean bool;
    if (this.instream != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isDataAvailable(int paramInt)
    throws IOException
  {
    return hasBufferedData();
  }
  
  public int length()
  {
    return this.bufferlen - this.bufferpos;
  }
  
  public int read()
    throws IOException
  {
    while (!hasBufferedData()) {
      if (fillBuffer() == -1) {
        return -1;
      }
    }
    byte[] arrayOfByte = this.buffer;
    int i = this.bufferpos;
    this.bufferpos = (i + 1);
    return arrayOfByte[i] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    if (paramArrayOfByte == null) {
      return 0;
    }
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramArrayOfByte == null) {
      return 0;
    }
    if (hasBufferedData())
    {
      paramInt2 = Math.min(paramInt2, this.bufferlen - this.bufferpos);
      System.arraycopy(this.buffer, this.bufferpos, paramArrayOfByte, paramInt1, paramInt2);
      this.bufferpos += paramInt2;
      return paramInt2;
    }
    if (paramInt2 > this.minChunkLimit)
    {
      paramInt1 = streamRead(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt1 > 0) {
        this.metrics.incrementBytesTransferred(paramInt1);
      }
      return paramInt1;
    }
    while (!hasBufferedData()) {
      if (fillBuffer() == -1) {
        return -1;
      }
    }
    paramInt2 = Math.min(paramInt2, this.bufferlen - this.bufferpos);
    System.arraycopy(this.buffer, this.bufferpos, paramArrayOfByte, paramInt1, paramInt2);
    this.bufferpos += paramInt2;
    return paramInt2;
  }
  
  public int readLine(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    int k = 1;
    int j = 0;
    while (k != 0)
    {
      int i = locateLF();
      int m;
      if (i != -1)
      {
        if (this.linebuffer.isEmpty()) {
          return lineFromReadBuffer(paramCharArrayBuffer, i);
        }
        i++;
        k = this.bufferpos;
        this.linebuffer.append(this.buffer, k, i - k);
        this.bufferpos = i;
        i = 0;
        m = j;
      }
      else
      {
        if (hasBufferedData())
        {
          j = this.bufferlen;
          i = this.bufferpos;
          this.linebuffer.append(this.buffer, i, j - i);
          this.bufferpos = this.bufferlen;
        }
        j = fillBuffer();
        i = k;
        m = j;
        if (j == -1)
        {
          i = 0;
          m = j;
        }
      }
      int n = this.constraints.getMaxLineLength();
      k = i;
      j = m;
      if (n > 0) {
        if (this.linebuffer.length() < n)
        {
          k = i;
          j = m;
        }
        else
        {
          throw new MessageConstraintException("Maximum line length limit exceeded");
        }
      }
    }
    if ((j == -1) && (this.linebuffer.isEmpty())) {
      return -1;
    }
    return lineFromLineBuffer(paramCharArrayBuffer);
  }
  
  public String readLine()
    throws IOException
  {
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(64);
    if (readLine(localCharArrayBuffer) != -1) {
      return localCharArrayBuffer.toString();
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/SessionInputBufferImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */