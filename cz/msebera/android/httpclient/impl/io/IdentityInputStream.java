package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.BufferInfo;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
public class IdentityInputStream
  extends InputStream
{
  private boolean closed = false;
  private final SessionInputBuffer in;
  
  public IdentityInputStream(SessionInputBuffer paramSessionInputBuffer)
  {
    this.in = ((SessionInputBuffer)Args.notNull(paramSessionInputBuffer, "Session input buffer"));
  }
  
  public int available()
    throws IOException
  {
    SessionInputBuffer localSessionInputBuffer = this.in;
    if ((localSessionInputBuffer instanceof BufferInfo)) {
      return ((BufferInfo)localSessionInputBuffer).length();
    }
    return 0;
  }
  
  public void close()
    throws IOException
  {
    this.closed = true;
  }
  
  public int read()
    throws IOException
  {
    if (this.closed) {
      return -1;
    }
    return this.in.read();
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (this.closed) {
      return -1;
    }
    return this.in.read(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/IdentityInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */