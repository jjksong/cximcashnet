package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.io.HttpMessageWriter;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.message.BasicLineFormatter;
import cz.msebera.android.httpclient.message.LineFormatter;

@Immutable
public class DefaultHttpRequestWriterFactory
  implements HttpMessageWriterFactory<HttpRequest>
{
  public static final DefaultHttpRequestWriterFactory INSTANCE = new DefaultHttpRequestWriterFactory();
  private final LineFormatter lineFormatter;
  
  public DefaultHttpRequestWriterFactory()
  {
    this(null);
  }
  
  public DefaultHttpRequestWriterFactory(LineFormatter paramLineFormatter)
  {
    if (paramLineFormatter == null) {
      paramLineFormatter = BasicLineFormatter.INSTANCE;
    }
    this.lineFormatter = paramLineFormatter;
  }
  
  public HttpMessageWriter<HttpRequest> create(SessionOutputBuffer paramSessionOutputBuffer)
  {
    return new DefaultHttpRequestWriter(paramSessionOutputBuffer, this.lineFormatter);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/io/DefaultHttpRequestWriterFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */