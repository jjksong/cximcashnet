package cz.msebera.android.httpclient.impl.pool;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpConnectionFactory;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.impl.DefaultBHttpClientConnection;
import cz.msebera.android.httpclient.impl.DefaultBHttpClientConnectionFactory;
import cz.msebera.android.httpclient.params.HttpParamConfig;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.pool.ConnFactory;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

@Immutable
public class BasicConnFactory
  implements ConnFactory<HttpHost, HttpClientConnection>
{
  private final HttpConnectionFactory<? extends HttpClientConnection> connFactory;
  private final int connectTimeout;
  private final SocketFactory plainfactory;
  private final SocketConfig sconfig;
  private final SSLSocketFactory sslfactory;
  
  public BasicConnFactory()
  {
    this(null, null, 0, SocketConfig.DEFAULT, ConnectionConfig.DEFAULT);
  }
  
  public BasicConnFactory(int paramInt, SocketConfig paramSocketConfig, ConnectionConfig paramConnectionConfig)
  {
    this(null, null, paramInt, paramSocketConfig, paramConnectionConfig);
  }
  
  public BasicConnFactory(SocketConfig paramSocketConfig, ConnectionConfig paramConnectionConfig)
  {
    this(null, null, 0, paramSocketConfig, paramConnectionConfig);
  }
  
  @Deprecated
  public BasicConnFactory(HttpParams paramHttpParams)
  {
    this(null, paramHttpParams);
  }
  
  public BasicConnFactory(SocketFactory paramSocketFactory, SSLSocketFactory paramSSLSocketFactory, int paramInt, SocketConfig paramSocketConfig, ConnectionConfig paramConnectionConfig)
  {
    this.plainfactory = paramSocketFactory;
    this.sslfactory = paramSSLSocketFactory;
    this.connectTimeout = paramInt;
    if (paramSocketConfig == null) {
      paramSocketConfig = SocketConfig.DEFAULT;
    }
    this.sconfig = paramSocketConfig;
    if (paramConnectionConfig == null) {
      paramConnectionConfig = ConnectionConfig.DEFAULT;
    }
    this.connFactory = new DefaultBHttpClientConnectionFactory(paramConnectionConfig);
  }
  
  @Deprecated
  public BasicConnFactory(SSLSocketFactory paramSSLSocketFactory, HttpParams paramHttpParams)
  {
    Args.notNull(paramHttpParams, "HTTP params");
    this.plainfactory = null;
    this.sslfactory = paramSSLSocketFactory;
    this.connectTimeout = paramHttpParams.getIntParameter("http.connection.timeout", 0);
    this.sconfig = HttpParamConfig.getSocketConfig(paramHttpParams);
    this.connFactory = new DefaultBHttpClientConnectionFactory(HttpParamConfig.getConnectionConfig(paramHttpParams));
  }
  
  public HttpClientConnection create(HttpHost paramHttpHost)
    throws IOException
  {
    String str = paramHttpHost.getSchemeName();
    Object localObject;
    if ("http".equalsIgnoreCase(str))
    {
      localObject = this.plainfactory;
      if (localObject != null) {
        localObject = ((SocketFactory)localObject).createSocket();
      } else {
        localObject = new Socket();
      }
    }
    else
    {
      localObject = null;
    }
    if ("https".equalsIgnoreCase(str))
    {
      localObject = this.sslfactory;
      if (localObject == null) {
        localObject = SSLSocketFactory.getDefault();
      }
      localObject = ((SocketFactory)localObject).createSocket();
    }
    if (localObject != null)
    {
      str = paramHttpHost.getHostName();
      int j = paramHttpHost.getPort();
      int i = j;
      if (j == -1) {
        if (paramHttpHost.getSchemeName().equalsIgnoreCase("http"))
        {
          i = 80;
        }
        else
        {
          i = j;
          if (paramHttpHost.getSchemeName().equalsIgnoreCase("https")) {
            i = 443;
          }
        }
      }
      ((Socket)localObject).setSoTimeout(this.sconfig.getSoTimeout());
      ((Socket)localObject).setTcpNoDelay(this.sconfig.isTcpNoDelay());
      j = this.sconfig.getSoLinger();
      if (j >= 0)
      {
        boolean bool;
        if (j > 0) {
          bool = true;
        } else {
          bool = false;
        }
        ((Socket)localObject).setSoLinger(bool, j);
      }
      ((Socket)localObject).setKeepAlive(this.sconfig.isSoKeepAlive());
      ((Socket)localObject).connect(new InetSocketAddress(str, i), this.connectTimeout);
      return (HttpClientConnection)this.connFactory.createConnection((Socket)localObject);
    }
    paramHttpHost = new StringBuilder();
    paramHttpHost.append(str);
    paramHttpHost.append(" scheme is not supported");
    throw new IOException(paramHttpHost.toString());
  }
  
  @Deprecated
  protected HttpClientConnection create(Socket paramSocket, HttpParams paramHttpParams)
    throws IOException
  {
    paramHttpParams = new DefaultBHttpClientConnection(paramHttpParams.getIntParameter("http.socket.buffer-size", 8192));
    paramHttpParams.bind(paramSocket);
    return paramHttpParams;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/pool/BasicConnFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */