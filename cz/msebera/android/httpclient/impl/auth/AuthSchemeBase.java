package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.ChallengeState;
import cz.msebera.android.httpclient.auth.ContextAwareAuthScheme;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.Locale;

@NotThreadSafe
public abstract class AuthSchemeBase
  implements ContextAwareAuthScheme
{
  private ChallengeState challengeState;
  
  public AuthSchemeBase() {}
  
  @Deprecated
  public AuthSchemeBase(ChallengeState paramChallengeState)
  {
    this.challengeState = paramChallengeState;
  }
  
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    return authenticate(paramCredentials, paramHttpRequest);
  }
  
  public ChallengeState getChallengeState()
  {
    return this.challengeState;
  }
  
  public boolean isProxy()
  {
    ChallengeState localChallengeState = this.challengeState;
    boolean bool;
    if ((localChallengeState != null) && (localChallengeState == ChallengeState.PROXY)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected abstract void parseChallenge(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
    throws MalformedChallengeException;
  
  public void processChallenge(Header paramHeader)
    throws MalformedChallengeException
  {
    Args.notNull(paramHeader, "Header");
    Object localObject = paramHeader.getName();
    if (((String)localObject).equalsIgnoreCase("WWW-Authenticate"))
    {
      this.challengeState = ChallengeState.TARGET;
    }
    else
    {
      if (!((String)localObject).equalsIgnoreCase("Proxy-Authenticate")) {
        break label245;
      }
      this.challengeState = ChallengeState.PROXY;
    }
    if ((paramHeader instanceof FormattedHeader))
    {
      localObject = (FormattedHeader)paramHeader;
      paramHeader = ((FormattedHeader)localObject).getBuffer();
      i = ((FormattedHeader)localObject).getValuePos();
    }
    else
    {
      localObject = paramHeader.getValue();
      if (localObject == null) {
        break label235;
      }
      paramHeader = new CharArrayBuffer(((String)localObject).length());
      paramHeader.append((String)localObject);
    }
    for (int i = 0; (i < paramHeader.length()) && (HTTP.isWhitespace(paramHeader.charAt(i))); i++) {}
    for (int j = i; (j < paramHeader.length()) && (!HTTP.isWhitespace(paramHeader.charAt(j))); j++) {}
    localObject = paramHeader.substring(i, j);
    if (((String)localObject).equalsIgnoreCase(getSchemeName()))
    {
      parseChallenge(paramHeader, j, paramHeader.length());
      return;
    }
    paramHeader = new StringBuilder();
    paramHeader.append("Invalid scheme identifier: ");
    paramHeader.append((String)localObject);
    throw new MalformedChallengeException(paramHeader.toString());
    label235:
    throw new MalformedChallengeException("Header value is null");
    label245:
    paramHeader = new StringBuilder();
    paramHeader.append("Unexpected header name: ");
    paramHeader.append((String)localObject);
    throw new MalformedChallengeException(paramHeader.toString());
  }
  
  public String toString()
  {
    String str = getSchemeName();
    if (str != null) {
      return str.toUpperCase(Locale.ENGLISH);
    }
    return super.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/AuthSchemeBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */