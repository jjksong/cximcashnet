package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.extras.Base64;
import cz.msebera.android.httpclient.util.EncodingUtils;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Locale;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

@NotThreadSafe
final class NTLMEngineImpl
  implements NTLMEngine
{
  static final String DEFAULT_CHARSET = "ASCII";
  protected static final int FLAG_DOMAIN_PRESENT = 4096;
  protected static final int FLAG_REQUEST_128BIT_KEY_EXCH = 536870912;
  protected static final int FLAG_REQUEST_56BIT_ENCRYPTION = Integer.MIN_VALUE;
  protected static final int FLAG_REQUEST_ALWAYS_SIGN = 32768;
  protected static final int FLAG_REQUEST_EXPLICIT_KEY_EXCH = 1073741824;
  protected static final int FLAG_REQUEST_LAN_MANAGER_KEY = 128;
  protected static final int FLAG_REQUEST_NTLM2_SESSION = 524288;
  protected static final int FLAG_REQUEST_NTLMv1 = 512;
  protected static final int FLAG_REQUEST_SEAL = 32;
  protected static final int FLAG_REQUEST_SIGN = 16;
  protected static final int FLAG_REQUEST_TARGET = 4;
  protected static final int FLAG_REQUEST_UNICODE_ENCODING = 1;
  protected static final int FLAG_REQUEST_VERSION = 33554432;
  protected static final int FLAG_TARGETINFO_PRESENT = 8388608;
  protected static final int FLAG_WORKSTATION_PRESENT = 8192;
  private static final SecureRandom RND_GEN;
  private static final byte[] SIGNATURE;
  private String credentialCharset = "ASCII";
  
  static
  {
    try
    {
      SecureRandom localSecureRandom = SecureRandom.getInstance("SHA1PRNG");
    }
    catch (Exception localException)
    {
      arrayOfByte = null;
    }
    RND_GEN = arrayOfByte;
    byte[] arrayOfByte = EncodingUtils.getBytes("NTLMSSP", "ASCII");
    SIGNATURE = new byte[arrayOfByte.length + 1];
    System.arraycopy(arrayOfByte, 0, SIGNATURE, 0, arrayOfByte.length);
    SIGNATURE[arrayOfByte.length] = 0;
  }
  
  static int F(int paramInt1, int paramInt2, int paramInt3)
  {
    return (paramInt1 ^ 0xFFFFFFFF) & paramInt3 | paramInt2 & paramInt1;
  }
  
  static int G(int paramInt1, int paramInt2, int paramInt3)
  {
    return paramInt1 & paramInt3 | paramInt1 & paramInt2 | paramInt2 & paramInt3;
  }
  
  static int H(int paramInt1, int paramInt2, int paramInt3)
  {
    return paramInt1 ^ paramInt2 ^ paramInt3;
  }
  
  static byte[] RC4(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    throws NTLMEngineException
  {
    try
    {
      Cipher localCipher = Cipher.getInstance("RC4");
      SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
      localSecretKeySpec.<init>(paramArrayOfByte2, "RC4");
      localCipher.init(1, localSecretKeySpec);
      paramArrayOfByte1 = localCipher.doFinal(paramArrayOfByte1);
      return paramArrayOfByte1;
    }
    catch (Exception paramArrayOfByte1)
    {
      throw new NTLMEngineException(paramArrayOfByte1.getMessage(), paramArrayOfByte1);
    }
  }
  
  private static String convertDomain(String paramString)
  {
    return stripDotSuffix(paramString);
  }
  
  private static String convertHost(String paramString)
  {
    return stripDotSuffix(paramString);
  }
  
  private static byte[] createBlob(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
  {
    byte[] arrayOfByte1 = new byte[4];
    byte[] tmp7_5 = arrayOfByte1;
    tmp7_5[0] = 1;
    byte[] tmp12_7 = tmp7_5;
    tmp12_7[1] = 1;
    byte[] tmp17_12 = tmp12_7;
    tmp17_12[2] = 0;
    byte[] tmp22_17 = tmp17_12;
    tmp22_17[3] = 0;
    tmp22_17;
    byte[] arrayOfByte4 = new byte[4];
    byte[] tmp35_33 = arrayOfByte4;
    tmp35_33[0] = 0;
    byte[] tmp40_35 = tmp35_33;
    tmp40_35[1] = 0;
    byte[] tmp45_40 = tmp40_35;
    tmp45_40[2] = 0;
    byte[] tmp50_45 = tmp45_40;
    tmp50_45[3] = 0;
    tmp50_45;
    byte[] arrayOfByte2 = new byte[4];
    byte[] tmp63_61 = arrayOfByte2;
    tmp63_61[0] = 0;
    byte[] tmp68_63 = tmp63_61;
    tmp68_63[1] = 0;
    byte[] tmp73_68 = tmp68_63;
    tmp73_68[2] = 0;
    byte[] tmp78_73 = tmp73_68;
    tmp78_73[3] = 0;
    tmp78_73;
    byte[] arrayOfByte3 = new byte[4];
    byte[] tmp91_89 = arrayOfByte3;
    tmp91_89[0] = 0;
    byte[] tmp96_91 = tmp91_89;
    tmp96_91[1] = 0;
    byte[] tmp101_96 = tmp96_91;
    tmp101_96[2] = 0;
    byte[] tmp106_101 = tmp101_96;
    tmp106_101[3] = 0;
    tmp106_101;
    byte[] arrayOfByte5 = new byte[arrayOfByte1.length + arrayOfByte4.length + paramArrayOfByte3.length + 8 + arrayOfByte2.length + paramArrayOfByte2.length + arrayOfByte3.length];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte5, 0, arrayOfByte1.length);
    int i = arrayOfByte1.length + 0;
    System.arraycopy(arrayOfByte4, 0, arrayOfByte5, i, arrayOfByte4.length);
    i += arrayOfByte4.length;
    System.arraycopy(paramArrayOfByte3, 0, arrayOfByte5, i, paramArrayOfByte3.length);
    i += paramArrayOfByte3.length;
    System.arraycopy(paramArrayOfByte1, 0, arrayOfByte5, i, 8);
    i += 8;
    System.arraycopy(arrayOfByte2, 0, arrayOfByte5, i, arrayOfByte2.length);
    i += arrayOfByte2.length;
    System.arraycopy(paramArrayOfByte2, 0, arrayOfByte5, i, paramArrayOfByte2.length);
    System.arraycopy(arrayOfByte3, 0, arrayOfByte5, i + paramArrayOfByte2.length, arrayOfByte3.length);
    i = arrayOfByte3.length;
    return arrayOfByte5;
  }
  
  private static Key createDESKey(byte[] paramArrayOfByte, int paramInt)
  {
    byte[] arrayOfByte = new byte[7];
    System.arraycopy(paramArrayOfByte, paramInt, arrayOfByte, 0, 7);
    paramArrayOfByte = new byte[8];
    paramArrayOfByte[0] = arrayOfByte[0];
    paramArrayOfByte[1] = ((byte)(arrayOfByte[0] << 7 | (arrayOfByte[1] & 0xFF) >>> 1));
    paramArrayOfByte[2] = ((byte)(arrayOfByte[1] << 6 | (arrayOfByte[2] & 0xFF) >>> 2));
    paramArrayOfByte[3] = ((byte)(arrayOfByte[2] << 5 | (arrayOfByte[3] & 0xFF) >>> 3));
    paramArrayOfByte[4] = ((byte)(arrayOfByte[3] << 4 | (arrayOfByte[4] & 0xFF) >>> 4));
    paramArrayOfByte[5] = ((byte)(arrayOfByte[4] << 3 | (arrayOfByte[5] & 0xFF) >>> 5));
    paramArrayOfByte[6] = ((byte)(arrayOfByte[5] << 2 | (arrayOfByte[6] & 0xFF) >>> 6));
    paramArrayOfByte[7] = ((byte)(arrayOfByte[6] << 1));
    oddParity(paramArrayOfByte);
    return new SecretKeySpec(paramArrayOfByte, "DES");
  }
  
  static byte[] hmacMD5(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    throws NTLMEngineException
  {
    paramArrayOfByte2 = new HMACMD5(paramArrayOfByte2);
    paramArrayOfByte2.update(paramArrayOfByte1);
    return paramArrayOfByte2.getOutput();
  }
  
  private static byte[] lmHash(String paramString)
    throws NTLMEngineException
  {
    try
    {
      paramString = paramString.toUpperCase(Locale.ENGLISH).getBytes("US-ASCII");
      int i = Math.min(paramString.length, 14);
      Object localObject = new byte[14];
      System.arraycopy(paramString, 0, localObject, 0, i);
      paramString = createDESKey((byte[])localObject, 0);
      localObject = createDESKey((byte[])localObject, 7);
      byte[] arrayOfByte = "KGS!@#$%".getBytes("US-ASCII");
      Cipher localCipher = Cipher.getInstance("DES/ECB/NoPadding");
      localCipher.init(1, paramString);
      paramString = localCipher.doFinal(arrayOfByte);
      localCipher.init(1, (Key)localObject);
      arrayOfByte = localCipher.doFinal(arrayOfByte);
      localObject = new byte[16];
      System.arraycopy(paramString, 0, localObject, 0, 8);
      System.arraycopy(arrayOfByte, 0, localObject, 8, 8);
      return (byte[])localObject;
    }
    catch (Exception paramString)
    {
      throw new NTLMEngineException(paramString.getMessage(), paramString);
    }
  }
  
  private static byte[] lmResponse(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    throws NTLMEngineException
  {
    try
    {
      Object localObject1 = new byte[21];
      System.arraycopy(paramArrayOfByte1, 0, localObject1, 0, 16);
      paramArrayOfByte1 = createDESKey((byte[])localObject1, 0);
      Object localObject2 = createDESKey((byte[])localObject1, 7);
      localObject1 = createDESKey((byte[])localObject1, 14);
      Cipher localCipher = Cipher.getInstance("DES/ECB/NoPadding");
      localCipher.init(1, paramArrayOfByte1);
      paramArrayOfByte1 = localCipher.doFinal(paramArrayOfByte2);
      localCipher.init(1, (Key)localObject2);
      localObject2 = localCipher.doFinal(paramArrayOfByte2);
      localCipher.init(1, (Key)localObject1);
      paramArrayOfByte2 = localCipher.doFinal(paramArrayOfByte2);
      localObject1 = new byte[24];
      System.arraycopy(paramArrayOfByte1, 0, localObject1, 0, 8);
      System.arraycopy(localObject2, 0, localObject1, 8, 8);
      System.arraycopy(paramArrayOfByte2, 0, localObject1, 16, 8);
      return (byte[])localObject1;
    }
    catch (Exception paramArrayOfByte1)
    {
      throw new NTLMEngineException(paramArrayOfByte1.getMessage(), paramArrayOfByte1);
    }
  }
  
  private static byte[] lmv2Hash(String paramString1, String paramString2, byte[] paramArrayOfByte)
    throws NTLMEngineException
  {
    try
    {
      HMACMD5 localHMACMD5 = new cz/msebera/android/httpclient/impl/auth/NTLMEngineImpl$HMACMD5;
      localHMACMD5.<init>(paramArrayOfByte);
      localHMACMD5.update(paramString2.toUpperCase(Locale.ENGLISH).getBytes("UnicodeLittleUnmarked"));
      if (paramString1 != null) {
        localHMACMD5.update(paramString1.toUpperCase(Locale.ENGLISH).getBytes("UnicodeLittleUnmarked"));
      }
      paramString1 = localHMACMD5.getOutput();
      return paramString1;
    }
    catch (UnsupportedEncodingException paramString1)
    {
      paramString2 = new StringBuilder();
      paramString2.append("Unicode not supported! ");
      paramString2.append(paramString1.getMessage());
      throw new NTLMEngineException(paramString2.toString(), paramString1);
    }
  }
  
  private static byte[] lmv2Response(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
    throws NTLMEngineException
  {
    paramArrayOfByte1 = new HMACMD5(paramArrayOfByte1);
    paramArrayOfByte1.update(paramArrayOfByte2);
    paramArrayOfByte1.update(paramArrayOfByte3);
    paramArrayOfByte1 = paramArrayOfByte1.getOutput();
    paramArrayOfByte2 = new byte[paramArrayOfByte1.length + paramArrayOfByte3.length];
    System.arraycopy(paramArrayOfByte1, 0, paramArrayOfByte2, 0, paramArrayOfByte1.length);
    System.arraycopy(paramArrayOfByte3, 0, paramArrayOfByte2, paramArrayOfByte1.length, paramArrayOfByte3.length);
    return paramArrayOfByte2;
  }
  
  private static byte[] makeRandomChallenge()
    throws NTLMEngineException
  {
    SecureRandom localSecureRandom = RND_GEN;
    if (localSecureRandom != null)
    {
      byte[] arrayOfByte = new byte[8];
      try
      {
        RND_GEN.nextBytes(arrayOfByte);
        return arrayOfByte;
      }
      finally {}
    }
    throw new NTLMEngineException("Random generator not available");
  }
  
  private static byte[] makeSecondaryKey()
    throws NTLMEngineException
  {
    SecureRandom localSecureRandom = RND_GEN;
    if (localSecureRandom != null)
    {
      byte[] arrayOfByte = new byte[16];
      try
      {
        RND_GEN.nextBytes(arrayOfByte);
        return arrayOfByte;
      }
      finally {}
    }
    throw new NTLMEngineException("Random generator not available");
  }
  
  static byte[] ntlm2SessionResponse(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
    throws NTLMEngineException
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      localMessageDigest.update(paramArrayOfByte2);
      localMessageDigest.update(paramArrayOfByte3);
      paramArrayOfByte2 = localMessageDigest.digest();
      paramArrayOfByte3 = new byte[8];
      System.arraycopy(paramArrayOfByte2, 0, paramArrayOfByte3, 0, 8);
      paramArrayOfByte1 = lmResponse(paramArrayOfByte1, paramArrayOfByte3);
      return paramArrayOfByte1;
    }
    catch (Exception paramArrayOfByte1)
    {
      if ((paramArrayOfByte1 instanceof NTLMEngineException)) {
        throw ((NTLMEngineException)paramArrayOfByte1);
      }
      throw new NTLMEngineException(paramArrayOfByte1.getMessage(), paramArrayOfByte1);
    }
  }
  
  private static byte[] ntlmHash(String paramString)
    throws NTLMEngineException
  {
    try
    {
      localObject = paramString.getBytes("UnicodeLittleUnmarked");
      paramString = new cz/msebera/android/httpclient/impl/auth/NTLMEngineImpl$MD4;
      paramString.<init>();
      paramString.update((byte[])localObject);
      paramString = paramString.getOutput();
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Unicode not supported: ");
      ((StringBuilder)localObject).append(paramString.getMessage());
      throw new NTLMEngineException(((StringBuilder)localObject).toString(), paramString);
    }
  }
  
  private static byte[] ntlmv2Hash(String paramString1, String paramString2, byte[] paramArrayOfByte)
    throws NTLMEngineException
  {
    try
    {
      HMACMD5 localHMACMD5 = new cz/msebera/android/httpclient/impl/auth/NTLMEngineImpl$HMACMD5;
      localHMACMD5.<init>(paramArrayOfByte);
      localHMACMD5.update(paramString2.toUpperCase(Locale.ENGLISH).getBytes("UnicodeLittleUnmarked"));
      if (paramString1 != null) {
        localHMACMD5.update(paramString1.getBytes("UnicodeLittleUnmarked"));
      }
      paramString1 = localHMACMD5.getOutput();
      return paramString1;
    }
    catch (UnsupportedEncodingException paramString1)
    {
      paramString2 = new StringBuilder();
      paramString2.append("Unicode not supported! ");
      paramString2.append(paramString1.getMessage());
      throw new NTLMEngineException(paramString2.toString(), paramString1);
    }
  }
  
  private static void oddParity(byte[] paramArrayOfByte)
  {
    for (int i = 0; i < paramArrayOfByte.length; i++)
    {
      int j = paramArrayOfByte[i];
      if (((j >>> 1 ^ j >>> 7 ^ j >>> 6 ^ j >>> 5 ^ j >>> 4 ^ j >>> 3 ^ j >>> 2) & 0x1) == 0) {
        j = 1;
      } else {
        j = 0;
      }
      if (j != 0) {
        paramArrayOfByte[i] = ((byte)(paramArrayOfByte[i] | 0x1));
      } else {
        paramArrayOfByte[i] = ((byte)(paramArrayOfByte[i] & 0xFFFFFFFE));
      }
    }
  }
  
  private static byte[] readSecurityBuffer(byte[] paramArrayOfByte, int paramInt)
    throws NTLMEngineException
  {
    int i = readUShort(paramArrayOfByte, paramInt);
    paramInt = readULong(paramArrayOfByte, paramInt + 4);
    if (paramArrayOfByte.length >= paramInt + i)
    {
      byte[] arrayOfByte = new byte[i];
      System.arraycopy(paramArrayOfByte, paramInt, arrayOfByte, 0, i);
      return arrayOfByte;
    }
    throw new NTLMEngineException("NTLM authentication - buffer too small for data item");
  }
  
  private static int readULong(byte[] paramArrayOfByte, int paramInt)
    throws NTLMEngineException
  {
    if (paramArrayOfByte.length >= paramInt + 4)
    {
      int i = paramArrayOfByte[paramInt];
      int j = paramArrayOfByte[(paramInt + 1)];
      int k = paramArrayOfByte[(paramInt + 2)];
      return (paramArrayOfByte[(paramInt + 3)] & 0xFF) << 24 | i & 0xFF | (j & 0xFF) << 8 | (k & 0xFF) << 16;
    }
    throw new NTLMEngineException("NTLM authentication - buffer too small for DWORD");
  }
  
  private static int readUShort(byte[] paramArrayOfByte, int paramInt)
    throws NTLMEngineException
  {
    if (paramArrayOfByte.length >= paramInt + 2)
    {
      int i = paramArrayOfByte[paramInt];
      return (paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8 | i & 0xFF;
    }
    throw new NTLMEngineException("NTLM authentication - buffer too small for WORD");
  }
  
  static int rotintlft(int paramInt1, int paramInt2)
  {
    return paramInt1 >>> 32 - paramInt2 | paramInt1 << paramInt2;
  }
  
  private static String stripDotSuffix(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    int i = paramString.indexOf(".");
    if (i != -1) {
      return paramString.substring(0, i);
    }
    return paramString;
  }
  
  static void writeULong(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[paramInt2] = ((byte)(paramInt1 & 0xFF));
    paramArrayOfByte[(paramInt2 + 1)] = ((byte)(paramInt1 >> 8 & 0xFF));
    paramArrayOfByte[(paramInt2 + 2)] = ((byte)(paramInt1 >> 16 & 0xFF));
    paramArrayOfByte[(paramInt2 + 3)] = ((byte)(paramInt1 >> 24 & 0xFF));
  }
  
  public String generateType1Msg(String paramString1, String paramString2)
    throws NTLMEngineException
  {
    return getType1Message(paramString2, paramString1);
  }
  
  public String generateType3Msg(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    throws NTLMEngineException
  {
    paramString5 = new Type2Message(paramString5);
    return getType3Message(paramString1, paramString2, paramString4, paramString3, paramString5.getChallenge(), paramString5.getFlags(), paramString5.getTarget(), paramString5.getTargetInfo());
  }
  
  String getCredentialCharset()
  {
    return this.credentialCharset;
  }
  
  final String getResponseFor(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    throws NTLMEngineException
  {
    if ((paramString1 != null) && (!paramString1.trim().equals("")))
    {
      paramString1 = new Type2Message(paramString1);
      paramString1 = getType3Message(paramString2, paramString3, paramString4, paramString5, paramString1.getChallenge(), paramString1.getFlags(), paramString1.getTarget(), paramString1.getTargetInfo());
    }
    else
    {
      paramString1 = getType1Message(paramString4, paramString5);
    }
    return paramString1;
  }
  
  String getType1Message(String paramString1, String paramString2)
    throws NTLMEngineException
  {
    return new Type1Message(paramString2, paramString1).getResponse();
  }
  
  String getType3Message(String paramString1, String paramString2, String paramString3, String paramString4, byte[] paramArrayOfByte1, int paramInt, String paramString5, byte[] paramArrayOfByte2)
    throws NTLMEngineException
  {
    return new Type3Message(paramString4, paramString3, paramString1, paramString2, paramArrayOfByte1, paramInt, paramString5, paramArrayOfByte2).getResponse();
  }
  
  void setCredentialCharset(String paramString)
  {
    this.credentialCharset = paramString;
  }
  
  protected static class CipherGen
  {
    protected final byte[] challenge;
    protected byte[] clientChallenge;
    protected byte[] clientChallenge2;
    protected final String domain;
    protected byte[] lanManagerSessionKey = null;
    protected byte[] lm2SessionResponse = null;
    protected byte[] lmHash = null;
    protected byte[] lmResponse = null;
    protected byte[] lmUserSessionKey = null;
    protected byte[] lmv2Hash = null;
    protected byte[] lmv2Response = null;
    protected byte[] ntlm2SessionResponse = null;
    protected byte[] ntlm2SessionResponseUserSessionKey = null;
    protected byte[] ntlmHash = null;
    protected byte[] ntlmResponse = null;
    protected byte[] ntlmUserSessionKey = null;
    protected byte[] ntlmv2Blob = null;
    protected byte[] ntlmv2Hash = null;
    protected byte[] ntlmv2Response = null;
    protected byte[] ntlmv2UserSessionKey = null;
    protected final String password;
    protected byte[] secondaryKey;
    protected final String target;
    protected final byte[] targetInformation;
    protected byte[] timestamp;
    protected final String user;
    
    public CipherGen(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte1, String paramString4, byte[] paramArrayOfByte2)
    {
      this(paramString1, paramString2, paramString3, paramArrayOfByte1, paramString4, paramArrayOfByte2, null, null, null, null);
    }
    
    public CipherGen(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte1, String paramString4, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3, byte[] paramArrayOfByte4, byte[] paramArrayOfByte5, byte[] paramArrayOfByte6)
    {
      this.domain = paramString1;
      this.target = paramString4;
      this.user = paramString2;
      this.password = paramString3;
      this.challenge = paramArrayOfByte1;
      this.targetInformation = paramArrayOfByte2;
      this.clientChallenge = paramArrayOfByte3;
      this.clientChallenge2 = paramArrayOfByte4;
      this.secondaryKey = paramArrayOfByte5;
      this.timestamp = paramArrayOfByte6;
    }
    
    public byte[] getClientChallenge()
      throws NTLMEngineException
    {
      if (this.clientChallenge == null) {
        this.clientChallenge = NTLMEngineImpl.access$000();
      }
      return this.clientChallenge;
    }
    
    public byte[] getClientChallenge2()
      throws NTLMEngineException
    {
      if (this.clientChallenge2 == null) {
        this.clientChallenge2 = NTLMEngineImpl.access$000();
      }
      return this.clientChallenge2;
    }
    
    public byte[] getLM2SessionResponse()
      throws NTLMEngineException
    {
      if (this.lm2SessionResponse == null)
      {
        byte[] arrayOfByte2 = getClientChallenge();
        this.lm2SessionResponse = new byte[24];
        System.arraycopy(arrayOfByte2, 0, this.lm2SessionResponse, 0, arrayOfByte2.length);
        byte[] arrayOfByte1 = this.lm2SessionResponse;
        Arrays.fill(arrayOfByte1, arrayOfByte2.length, arrayOfByte1.length, (byte)0);
      }
      return this.lm2SessionResponse;
    }
    
    public byte[] getLMHash()
      throws NTLMEngineException
    {
      if (this.lmHash == null) {
        this.lmHash = NTLMEngineImpl.lmHash(this.password);
      }
      return this.lmHash;
    }
    
    public byte[] getLMResponse()
      throws NTLMEngineException
    {
      if (this.lmResponse == null) {
        this.lmResponse = NTLMEngineImpl.lmResponse(getLMHash(), this.challenge);
      }
      return this.lmResponse;
    }
    
    public byte[] getLMUserSessionKey()
      throws NTLMEngineException
    {
      if (this.lmUserSessionKey == null)
      {
        this.lmUserSessionKey = new byte[16];
        System.arraycopy(getLMHash(), 0, this.lmUserSessionKey, 0, 8);
        Arrays.fill(this.lmUserSessionKey, 8, 16, (byte)0);
      }
      return this.lmUserSessionKey;
    }
    
    public byte[] getLMv2Hash()
      throws NTLMEngineException
    {
      if (this.lmv2Hash == null) {
        this.lmv2Hash = NTLMEngineImpl.lmv2Hash(this.domain, this.user, getNTLMHash());
      }
      return this.lmv2Hash;
    }
    
    public byte[] getLMv2Response()
      throws NTLMEngineException
    {
      if (this.lmv2Response == null) {
        this.lmv2Response = NTLMEngineImpl.lmv2Response(getLMv2Hash(), this.challenge, getClientChallenge());
      }
      return this.lmv2Response;
    }
    
    public byte[] getLanManagerSessionKey()
      throws NTLMEngineException
    {
      if (this.lanManagerSessionKey == null) {
        try
        {
          Object localObject1 = new byte[14];
          System.arraycopy(getLMHash(), 0, localObject1, 0, 8);
          Arrays.fill((byte[])localObject1, 8, localObject1.length, (byte)-67);
          Object localObject2 = NTLMEngineImpl.createDESKey((byte[])localObject1, 0);
          localObject1 = NTLMEngineImpl.createDESKey((byte[])localObject1, 7);
          byte[] arrayOfByte = new byte[8];
          System.arraycopy(getLMResponse(), 0, arrayOfByte, 0, arrayOfByte.length);
          Cipher localCipher = Cipher.getInstance("DES/ECB/NoPadding");
          localCipher.init(1, (Key)localObject2);
          localObject2 = localCipher.doFinal(arrayOfByte);
          localCipher = Cipher.getInstance("DES/ECB/NoPadding");
          localCipher.init(1, (Key)localObject1);
          localObject1 = localCipher.doFinal(arrayOfByte);
          this.lanManagerSessionKey = new byte[16];
          System.arraycopy(localObject2, 0, this.lanManagerSessionKey, 0, localObject2.length);
          System.arraycopy(localObject1, 0, this.lanManagerSessionKey, localObject2.length, localObject1.length);
        }
        catch (Exception localException)
        {
          throw new NTLMEngineException(localException.getMessage(), localException);
        }
      }
      return this.lanManagerSessionKey;
    }
    
    public byte[] getNTLM2SessionResponse()
      throws NTLMEngineException
    {
      if (this.ntlm2SessionResponse == null) {
        this.ntlm2SessionResponse = NTLMEngineImpl.ntlm2SessionResponse(getNTLMHash(), this.challenge, getClientChallenge());
      }
      return this.ntlm2SessionResponse;
    }
    
    public byte[] getNTLM2SessionResponseUserSessionKey()
      throws NTLMEngineException
    {
      if (this.ntlm2SessionResponseUserSessionKey == null)
      {
        byte[] arrayOfByte1 = getLM2SessionResponse();
        byte[] arrayOfByte3 = this.challenge;
        byte[] arrayOfByte2 = new byte[arrayOfByte3.length + arrayOfByte1.length];
        System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, arrayOfByte3.length);
        System.arraycopy(arrayOfByte1, 0, arrayOfByte2, this.challenge.length, arrayOfByte1.length);
        this.ntlm2SessionResponseUserSessionKey = NTLMEngineImpl.hmacMD5(arrayOfByte2, getNTLMUserSessionKey());
      }
      return this.ntlm2SessionResponseUserSessionKey;
    }
    
    public byte[] getNTLMHash()
      throws NTLMEngineException
    {
      if (this.ntlmHash == null) {
        this.ntlmHash = NTLMEngineImpl.ntlmHash(this.password);
      }
      return this.ntlmHash;
    }
    
    public byte[] getNTLMResponse()
      throws NTLMEngineException
    {
      if (this.ntlmResponse == null) {
        this.ntlmResponse = NTLMEngineImpl.lmResponse(getNTLMHash(), this.challenge);
      }
      return this.ntlmResponse;
    }
    
    public byte[] getNTLMUserSessionKey()
      throws NTLMEngineException
    {
      if (this.ntlmUserSessionKey == null)
      {
        NTLMEngineImpl.MD4 localMD4 = new NTLMEngineImpl.MD4();
        localMD4.update(getNTLMHash());
        this.ntlmUserSessionKey = localMD4.getOutput();
      }
      return this.ntlmUserSessionKey;
    }
    
    public byte[] getNTLMv2Blob()
      throws NTLMEngineException
    {
      if (this.ntlmv2Blob == null) {
        this.ntlmv2Blob = NTLMEngineImpl.createBlob(getClientChallenge2(), this.targetInformation, getTimestamp());
      }
      return this.ntlmv2Blob;
    }
    
    public byte[] getNTLMv2Hash()
      throws NTLMEngineException
    {
      if (this.ntlmv2Hash == null) {
        this.ntlmv2Hash = NTLMEngineImpl.ntlmv2Hash(this.domain, this.user, getNTLMHash());
      }
      return this.ntlmv2Hash;
    }
    
    public byte[] getNTLMv2Response()
      throws NTLMEngineException
    {
      if (this.ntlmv2Response == null) {
        this.ntlmv2Response = NTLMEngineImpl.lmv2Response(getNTLMv2Hash(), this.challenge, getNTLMv2Blob());
      }
      return this.ntlmv2Response;
    }
    
    public byte[] getNTLMv2UserSessionKey()
      throws NTLMEngineException
    {
      if (this.ntlmv2UserSessionKey == null)
      {
        byte[] arrayOfByte1 = getNTLMv2Hash();
        byte[] arrayOfByte2 = new byte[16];
        System.arraycopy(getNTLMv2Response(), 0, arrayOfByte2, 0, 16);
        this.ntlmv2UserSessionKey = NTLMEngineImpl.hmacMD5(arrayOfByte2, arrayOfByte1);
      }
      return this.ntlmv2UserSessionKey;
    }
    
    public byte[] getSecondaryKey()
      throws NTLMEngineException
    {
      if (this.secondaryKey == null) {
        this.secondaryKey = NTLMEngineImpl.access$100();
      }
      return this.secondaryKey;
    }
    
    public byte[] getTimestamp()
    {
      if (this.timestamp == null)
      {
        long l = (System.currentTimeMillis() + 11644473600000L) * 10000L;
        this.timestamp = new byte[8];
        for (int i = 0; i < 8; i++)
        {
          this.timestamp[i] = ((byte)(int)l);
          l >>>= 8;
        }
      }
      return this.timestamp;
    }
  }
  
  static class HMACMD5
  {
    protected byte[] ipad;
    protected MessageDigest md5;
    protected byte[] opad;
    
    HMACMD5(byte[] paramArrayOfByte)
      throws NTLMEngineException
    {
      try
      {
        this.md5 = MessageDigest.getInstance("MD5");
        this.ipad = new byte[64];
        this.opad = new byte[64];
        int i = paramArrayOfByte.length;
        int j = i;
        localObject = paramArrayOfByte;
        if (i > 64)
        {
          this.md5.update(paramArrayOfByte);
          localObject = this.md5.digest();
          j = localObject.length;
        }
        int k;
        for (i = 0;; i++)
        {
          k = i;
          if (i >= j) {
            break;
          }
          this.ipad[i] = ((byte)(0x36 ^ localObject[i]));
          this.opad[i] = ((byte)(0x5C ^ localObject[i]));
        }
        while (k < 64)
        {
          this.ipad[k] = 54;
          this.opad[k] = 92;
          k++;
        }
        this.md5.reset();
        this.md5.update(this.ipad);
        return;
      }
      catch (Exception paramArrayOfByte)
      {
        Object localObject = new StringBuilder();
        ((StringBuilder)localObject).append("Error getting md5 message digest implementation: ");
        ((StringBuilder)localObject).append(paramArrayOfByte.getMessage());
        throw new NTLMEngineException(((StringBuilder)localObject).toString(), paramArrayOfByte);
      }
    }
    
    byte[] getOutput()
    {
      byte[] arrayOfByte = this.md5.digest();
      this.md5.update(this.opad);
      return this.md5.digest(arrayOfByte);
    }
    
    void update(byte[] paramArrayOfByte)
    {
      this.md5.update(paramArrayOfByte);
    }
    
    void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      this.md5.update(paramArrayOfByte, paramInt1, paramInt2);
    }
  }
  
  static class MD4
  {
    protected int A = 1732584193;
    protected int B = -271733879;
    protected int C = -1732584194;
    protected int D = 271733878;
    protected long count = 0L;
    protected byte[] dataBuffer = new byte[64];
    
    byte[] getOutput()
    {
      int i = (int)(this.count & 0x3F);
      if (i < 56) {
        i = 56 - i;
      } else {
        i = 120 - i;
      }
      byte[] arrayOfByte = new byte[i + 8];
      arrayOfByte[0] = Byte.MIN_VALUE;
      for (int j = 0; j < 8; j++) {
        arrayOfByte[(i + j)] = ((byte)(int)(this.count * 8L >>> j * 8));
      }
      update(arrayOfByte);
      arrayOfByte = new byte[16];
      NTLMEngineImpl.writeULong(arrayOfByte, this.A, 0);
      NTLMEngineImpl.writeULong(arrayOfByte, this.B, 4);
      NTLMEngineImpl.writeULong(arrayOfByte, this.C, 8);
      NTLMEngineImpl.writeULong(arrayOfByte, this.D, 12);
      return arrayOfByte;
    }
    
    protected void processBuffer()
    {
      int[] arrayOfInt = new int[16];
      for (int i = 0; i < 16; i++)
      {
        byte[] arrayOfByte = this.dataBuffer;
        j = i * 4;
        arrayOfInt[i] = ((arrayOfByte[j] & 0xFF) + ((arrayOfByte[(j + 1)] & 0xFF) << 8) + ((arrayOfByte[(j + 2)] & 0xFF) << 16) + ((arrayOfByte[(j + 3)] & 0xFF) << 24));
      }
      int m = this.A;
      int k = this.B;
      int j = this.C;
      i = this.D;
      round1(arrayOfInt);
      round2(arrayOfInt);
      round3(arrayOfInt);
      this.A += m;
      this.B += k;
      this.C += j;
      this.D += i;
    }
    
    protected void round1(int[] paramArrayOfInt)
    {
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.F(this.B, this.C, this.D) + paramArrayOfInt[0], 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.F(this.A, this.B, this.C) + paramArrayOfInt[1], 7);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.F(this.D, this.A, this.B) + paramArrayOfInt[2], 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.F(this.C, this.D, this.A) + paramArrayOfInt[3], 19);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.F(this.B, this.C, this.D) + paramArrayOfInt[4], 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.F(this.A, this.B, this.C) + paramArrayOfInt[5], 7);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.F(this.D, this.A, this.B) + paramArrayOfInt[6], 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.F(this.C, this.D, this.A) + paramArrayOfInt[7], 19);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.F(this.B, this.C, this.D) + paramArrayOfInt[8], 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.F(this.A, this.B, this.C) + paramArrayOfInt[9], 7);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.F(this.D, this.A, this.B) + paramArrayOfInt[10], 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.F(this.C, this.D, this.A) + paramArrayOfInt[11], 19);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.F(this.B, this.C, this.D) + paramArrayOfInt[12], 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.F(this.A, this.B, this.C) + paramArrayOfInt[13], 7);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.F(this.D, this.A, this.B) + paramArrayOfInt[14], 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.F(this.C, this.D, this.A) + paramArrayOfInt[15], 19);
    }
    
    protected void round2(int[] paramArrayOfInt)
    {
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.G(this.B, this.C, this.D) + paramArrayOfInt[0] + 1518500249, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.G(this.A, this.B, this.C) + paramArrayOfInt[4] + 1518500249, 5);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.G(this.D, this.A, this.B) + paramArrayOfInt[8] + 1518500249, 9);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.G(this.C, this.D, this.A) + paramArrayOfInt[12] + 1518500249, 13);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.G(this.B, this.C, this.D) + paramArrayOfInt[1] + 1518500249, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.G(this.A, this.B, this.C) + paramArrayOfInt[5] + 1518500249, 5);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.G(this.D, this.A, this.B) + paramArrayOfInt[9] + 1518500249, 9);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.G(this.C, this.D, this.A) + paramArrayOfInt[13] + 1518500249, 13);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.G(this.B, this.C, this.D) + paramArrayOfInt[2] + 1518500249, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.G(this.A, this.B, this.C) + paramArrayOfInt[6] + 1518500249, 5);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.G(this.D, this.A, this.B) + paramArrayOfInt[10] + 1518500249, 9);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.G(this.C, this.D, this.A) + paramArrayOfInt[14] + 1518500249, 13);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.G(this.B, this.C, this.D) + paramArrayOfInt[3] + 1518500249, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.G(this.A, this.B, this.C) + paramArrayOfInt[7] + 1518500249, 5);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.G(this.D, this.A, this.B) + paramArrayOfInt[11] + 1518500249, 9);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.G(this.C, this.D, this.A) + paramArrayOfInt[15] + 1518500249, 13);
    }
    
    protected void round3(int[] paramArrayOfInt)
    {
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.H(this.B, this.C, this.D) + paramArrayOfInt[0] + 1859775393, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.H(this.A, this.B, this.C) + paramArrayOfInt[8] + 1859775393, 9);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.H(this.D, this.A, this.B) + paramArrayOfInt[4] + 1859775393, 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.H(this.C, this.D, this.A) + paramArrayOfInt[12] + 1859775393, 15);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.H(this.B, this.C, this.D) + paramArrayOfInt[2] + 1859775393, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.H(this.A, this.B, this.C) + paramArrayOfInt[10] + 1859775393, 9);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.H(this.D, this.A, this.B) + paramArrayOfInt[6] + 1859775393, 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.H(this.C, this.D, this.A) + paramArrayOfInt[14] + 1859775393, 15);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.H(this.B, this.C, this.D) + paramArrayOfInt[1] + 1859775393, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.H(this.A, this.B, this.C) + paramArrayOfInt[9] + 1859775393, 9);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.H(this.D, this.A, this.B) + paramArrayOfInt[5] + 1859775393, 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.H(this.C, this.D, this.A) + paramArrayOfInt[13] + 1859775393, 15);
      this.A = NTLMEngineImpl.rotintlft(this.A + NTLMEngineImpl.H(this.B, this.C, this.D) + paramArrayOfInt[3] + 1859775393, 3);
      this.D = NTLMEngineImpl.rotintlft(this.D + NTLMEngineImpl.H(this.A, this.B, this.C) + paramArrayOfInt[11] + 1859775393, 9);
      this.C = NTLMEngineImpl.rotintlft(this.C + NTLMEngineImpl.H(this.D, this.A, this.B) + paramArrayOfInt[7] + 1859775393, 11);
      this.B = NTLMEngineImpl.rotintlft(this.B + NTLMEngineImpl.H(this.C, this.D, this.A) + paramArrayOfInt[15] + 1859775393, 15);
    }
    
    void update(byte[] paramArrayOfByte)
    {
      int i = (int)(this.count & 0x3F);
      int j = 0;
      int k;
      byte[] arrayOfByte;
      for (;;)
      {
        k = paramArrayOfByte.length;
        arrayOfByte = this.dataBuffer;
        if (k - j + i < arrayOfByte.length) {
          break;
        }
        k = arrayOfByte.length - i;
        System.arraycopy(paramArrayOfByte, j, arrayOfByte, i, k);
        this.count += k;
        j += k;
        processBuffer();
        i = 0;
      }
      if (j < paramArrayOfByte.length)
      {
        k = paramArrayOfByte.length - j;
        System.arraycopy(paramArrayOfByte, j, arrayOfByte, i, k);
        this.count += k;
      }
    }
  }
  
  static class NTLMMessage
  {
    private int currentOutputPosition;
    private byte[] messageContents = null;
    
    NTLMMessage()
    {
      this.currentOutputPosition = 0;
    }
    
    NTLMMessage(String paramString, int paramInt)
      throws NTLMEngineException
    {
      int i = 0;
      this.currentOutputPosition = 0;
      this.messageContents = Base64.decode(EncodingUtils.getBytes(paramString, "ASCII"), 2);
      if (this.messageContents.length >= NTLMEngineImpl.SIGNATURE.length)
      {
        while (i < NTLMEngineImpl.SIGNATURE.length) {
          if (this.messageContents[i] == NTLMEngineImpl.SIGNATURE[i]) {
            i++;
          } else {
            throw new NTLMEngineException("NTLM message expected - instead got unrecognized bytes");
          }
        }
        i = readULong(NTLMEngineImpl.SIGNATURE.length);
        if (i == paramInt)
        {
          this.currentOutputPosition = this.messageContents.length;
          return;
        }
        paramString = new StringBuilder();
        paramString.append("NTLM type ");
        paramString.append(Integer.toString(paramInt));
        paramString.append(" message expected - instead got type ");
        paramString.append(Integer.toString(i));
        throw new NTLMEngineException(paramString.toString());
      }
      throw new NTLMEngineException("NTLM message decoding error - packet too short");
    }
    
    protected void addByte(byte paramByte)
    {
      byte[] arrayOfByte = this.messageContents;
      int i = this.currentOutputPosition;
      arrayOfByte[i] = paramByte;
      this.currentOutputPosition = (i + 1);
    }
    
    protected void addBytes(byte[] paramArrayOfByte)
    {
      if (paramArrayOfByte == null) {
        return;
      }
      int k = paramArrayOfByte.length;
      for (int j = 0; j < k; j++)
      {
        int i = paramArrayOfByte[j];
        byte[] arrayOfByte = this.messageContents;
        int m = this.currentOutputPosition;
        arrayOfByte[m] = i;
        this.currentOutputPosition = (m + 1);
      }
    }
    
    protected void addULong(int paramInt)
    {
      addByte((byte)(paramInt & 0xFF));
      addByte((byte)(paramInt >> 8 & 0xFF));
      addByte((byte)(paramInt >> 16 & 0xFF));
      addByte((byte)(paramInt >> 24 & 0xFF));
    }
    
    protected void addUShort(int paramInt)
    {
      addByte((byte)(paramInt & 0xFF));
      addByte((byte)(paramInt >> 8 & 0xFF));
    }
    
    protected int getMessageLength()
    {
      return this.currentOutputPosition;
    }
    
    protected int getPreambleLength()
    {
      return NTLMEngineImpl.SIGNATURE.length + 4;
    }
    
    String getResponse()
    {
      byte[] arrayOfByte2 = this.messageContents;
      int j = arrayOfByte2.length;
      int i = this.currentOutputPosition;
      byte[] arrayOfByte1 = arrayOfByte2;
      if (j > i)
      {
        arrayOfByte1 = new byte[i];
        System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, i);
      }
      return EncodingUtils.getAsciiString(Base64.encode(arrayOfByte1, 2));
    }
    
    protected void prepareResponse(int paramInt1, int paramInt2)
    {
      this.messageContents = new byte[paramInt1];
      this.currentOutputPosition = 0;
      addBytes(NTLMEngineImpl.SIGNATURE);
      addULong(paramInt2);
    }
    
    protected byte readByte(int paramInt)
      throws NTLMEngineException
    {
      byte[] arrayOfByte = this.messageContents;
      if (arrayOfByte.length >= paramInt + 1) {
        return arrayOfByte[paramInt];
      }
      throw new NTLMEngineException("NTLM: Message too short");
    }
    
    protected void readBytes(byte[] paramArrayOfByte, int paramInt)
      throws NTLMEngineException
    {
      byte[] arrayOfByte = this.messageContents;
      if (arrayOfByte.length >= paramArrayOfByte.length + paramInt)
      {
        System.arraycopy(arrayOfByte, paramInt, paramArrayOfByte, 0, paramArrayOfByte.length);
        return;
      }
      throw new NTLMEngineException("NTLM: Message too short");
    }
    
    protected byte[] readSecurityBuffer(int paramInt)
      throws NTLMEngineException
    {
      return NTLMEngineImpl.readSecurityBuffer(this.messageContents, paramInt);
    }
    
    protected int readULong(int paramInt)
      throws NTLMEngineException
    {
      return NTLMEngineImpl.readULong(this.messageContents, paramInt);
    }
    
    protected int readUShort(int paramInt)
      throws NTLMEngineException
    {
      return NTLMEngineImpl.readUShort(this.messageContents, paramInt);
    }
  }
  
  static class Type1Message
    extends NTLMEngineImpl.NTLMMessage
  {
    protected byte[] domainBytes;
    protected byte[] hostBytes;
    
    Type1Message(String paramString1, String paramString2)
      throws NTLMEngineException
    {
      try
      {
        String str2 = NTLMEngineImpl.convertHost(paramString2);
        String str1 = NTLMEngineImpl.convertDomain(paramString1);
        paramString2 = null;
        if (str2 != null) {
          paramString1 = str2.getBytes("ASCII");
        } else {
          paramString1 = null;
        }
        this.hostBytes = paramString1;
        paramString1 = paramString2;
        if (str1 != null) {
          paramString1 = str1.toUpperCase(Locale.ENGLISH).getBytes("ASCII");
        }
        this.domainBytes = paramString1;
        return;
      }
      catch (UnsupportedEncodingException paramString2)
      {
        paramString1 = new StringBuilder();
        paramString1.append("Unicode unsupported: ");
        paramString1.append(paramString2.getMessage());
        throw new NTLMEngineException(paramString1.toString(), paramString2);
      }
    }
    
    String getResponse()
    {
      prepareResponse(40, 1);
      addULong(-1576500735);
      addUShort(0);
      addUShort(0);
      addULong(40);
      addUShort(0);
      addUShort(0);
      addULong(40);
      addUShort(261);
      addULong(2600);
      addUShort(3840);
      return super.getResponse();
    }
  }
  
  static class Type2Message
    extends NTLMEngineImpl.NTLMMessage
  {
    protected byte[] challenge = new byte[8];
    protected int flags;
    protected String target;
    protected byte[] targetInfo;
    
    Type2Message(String paramString)
      throws NTLMEngineException
    {
      super(2);
      readBytes(this.challenge, 24);
      this.flags = readULong(20);
      if ((this.flags & 0x1) != 0)
      {
        this.target = null;
        if (getMessageLength() >= 20)
        {
          byte[] arrayOfByte = readSecurityBuffer(12);
          if (arrayOfByte.length != 0) {
            try
            {
              paramString = new java/lang/String;
              paramString.<init>(arrayOfByte, "UnicodeLittleUnmarked");
              this.target = paramString;
            }
            catch (UnsupportedEncodingException paramString)
            {
              throw new NTLMEngineException(paramString.getMessage(), paramString);
            }
          }
        }
        this.targetInfo = null;
        if (getMessageLength() >= 48)
        {
          paramString = readSecurityBuffer(40);
          if (paramString.length != 0) {
            this.targetInfo = paramString;
          }
        }
        return;
      }
      paramString = new StringBuilder();
      paramString.append("NTLM type 2 message indicates no support for Unicode. Flags are: ");
      paramString.append(Integer.toString(this.flags));
      throw new NTLMEngineException(paramString.toString());
    }
    
    byte[] getChallenge()
    {
      return this.challenge;
    }
    
    int getFlags()
    {
      return this.flags;
    }
    
    String getTarget()
    {
      return this.target;
    }
    
    byte[] getTargetInfo()
    {
      return this.targetInfo;
    }
  }
  
  static class Type3Message
    extends NTLMEngineImpl.NTLMMessage
  {
    protected byte[] domainBytes;
    protected byte[] hostBytes;
    protected byte[] lmResp;
    protected byte[] ntResp;
    protected byte[] sessionKey;
    protected int type2Flags;
    protected byte[] userBytes;
    
    Type3Message(String paramString1, String paramString2, String paramString3, String paramString4, byte[] paramArrayOfByte1, int paramInt, String paramString5, byte[] paramArrayOfByte2)
      throws NTLMEngineException
    {
      this.type2Flags = paramInt;
      String str2 = NTLMEngineImpl.convertHost(paramString2);
      String str1 = NTLMEngineImpl.convertDomain(paramString1);
      paramString4 = new NTLMEngineImpl.CipherGen(str1, paramString3, paramString4, paramArrayOfByte1, paramString5, paramArrayOfByte2);
      if (((0x800000 & paramInt) != 0) && (paramArrayOfByte2 != null) && (paramString5 != null)) {}
      try
      {
        this.ntResp = paramString4.getNTLMv2Response();
        this.lmResp = paramString4.getLMv2Response();
        if ((paramInt & 0x80) != 0) {
          paramString1 = paramString4.getLanManagerSessionKey();
        } else {
          paramString1 = paramString4.getNTLMv2UserSessionKey();
        }
      }
      catch (NTLMEngineException paramString1)
      {
        this.ntResp = new byte[0];
        this.lmResp = paramString4.getLMResponse();
        if ((paramInt & 0x80) == 0) {
          break label238;
        }
        paramString1 = paramString4.getLanManagerSessionKey();
        break label244;
        paramString1 = paramString4.getLMUserSessionKey();
      }
      if ((0x80000 & paramInt) != 0)
      {
        this.ntResp = paramString4.getNTLM2SessionResponse();
        this.lmResp = paramString4.getLM2SessionResponse();
        if ((paramInt & 0x80) != 0) {
          paramString1 = paramString4.getLanManagerSessionKey();
        } else {
          paramString1 = paramString4.getNTLM2SessionResponseUserSessionKey();
        }
      }
      else
      {
        this.ntResp = paramString4.getNTLMResponse();
        this.lmResp = paramString4.getLMResponse();
        if ((paramInt & 0x80) != 0) {
          paramString1 = paramString4.getLanManagerSessionKey();
        } else {
          paramString1 = paramString4.getNTLMUserSessionKey();
        }
      }
      label238:
      label244:
      paramString2 = null;
      if ((paramInt & 0x10) != 0)
      {
        if ((paramInt & 0x40000000) != 0) {
          this.sessionKey = NTLMEngineImpl.RC4(paramString4.getSecondaryKey(), paramString1);
        } else {
          this.sessionKey = paramString1;
        }
      }
      else {
        this.sessionKey = null;
      }
      if (str2 != null) {
        try
        {
          paramString1 = str2.getBytes("UnicodeLittleUnmarked");
        }
        catch (UnsupportedEncodingException paramString2)
        {
          break label355;
        }
      } else {
        paramString1 = null;
      }
      this.hostBytes = paramString1;
      paramString1 = paramString2;
      if (str1 != null) {
        paramString1 = str1.toUpperCase(Locale.ENGLISH).getBytes("UnicodeLittleUnmarked");
      }
      this.domainBytes = paramString1;
      this.userBytes = paramString3.getBytes("UnicodeLittleUnmarked");
      return;
      label355:
      paramString1 = new StringBuilder();
      paramString1.append("Unicode not supported: ");
      paramString1.append(paramString2.getMessage());
      throw new NTLMEngineException(paramString1.toString(), paramString2);
    }
    
    String getResponse()
    {
      int n = this.ntResp.length;
      int m = this.lmResp.length;
      byte[] arrayOfByte = this.domainBytes;
      int k = 0;
      if (arrayOfByte != null) {
        i = arrayOfByte.length;
      } else {
        i = 0;
      }
      arrayOfByte = this.hostBytes;
      int j;
      if (arrayOfByte != null) {
        j = arrayOfByte.length;
      } else {
        j = 0;
      }
      int i1 = this.userBytes.length;
      arrayOfByte = this.sessionKey;
      if (arrayOfByte != null) {
        k = arrayOfByte.length;
      }
      int i3 = m + 72;
      int i5 = i3 + n;
      int i6 = i5 + i;
      int i4 = i6 + i1;
      int i2 = i4 + j;
      prepareResponse(i2 + k, 3);
      addUShort(m);
      addUShort(m);
      addULong(72);
      addUShort(n);
      addUShort(n);
      addULong(i3);
      addUShort(i);
      addUShort(i);
      addULong(i5);
      addUShort(i1);
      addUShort(i1);
      addULong(i6);
      addUShort(j);
      addUShort(j);
      addULong(i4);
      addUShort(k);
      addUShort(k);
      addULong(i2);
      int i = this.type2Flags;
      addULong(i & 0x4 | i & 0x80 | i & 0x200 | 0x80000 & i | 0x2000000 | 0x8000 & i | i & 0x20 | i & 0x10 | 0x20000000 & i | 0x80000000 & i | 0x40000000 & i | 0x800000 & i | i & 0x1);
      addUShort(261);
      addULong(2600);
      addUShort(3840);
      addBytes(this.lmResp);
      addBytes(this.ntResp);
      addBytes(this.domainBytes);
      addBytes(this.userBytes);
      addBytes(this.hostBytes);
      arrayOfByte = this.sessionKey;
      if (arrayOfByte != null) {
        addBytes(arrayOfByte);
      }
      return super.getResponse();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/NTLMEngineImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */