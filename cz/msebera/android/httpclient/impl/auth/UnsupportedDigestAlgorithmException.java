package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class UnsupportedDigestAlgorithmException
  extends RuntimeException
{
  private static final long serialVersionUID = 319558534317118022L;
  
  public UnsupportedDigestAlgorithmException() {}
  
  public UnsupportedDigestAlgorithmException(String paramString)
  {
    super(paramString);
  }
  
  public UnsupportedDigestAlgorithmException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/UnsupportedDigestAlgorithmException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */