package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.ChallengeState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.message.BasicHeaderValueFormatter;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.message.BufferedHeader;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import cz.msebera.android.httpclient.util.EncodingUtils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

@NotThreadSafe
public class DigestScheme
  extends RFC2617Scheme
{
  private static final char[] HEXADECIMAL = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  private static final int QOP_AUTH = 2;
  private static final int QOP_AUTH_INT = 1;
  private static final int QOP_MISSING = 0;
  private static final int QOP_UNKNOWN = -1;
  private String a1;
  private String a2;
  private String cnonce;
  private boolean complete;
  private String lastNonce;
  private long nounceCount;
  
  public DigestScheme()
  {
    this(Consts.ASCII);
  }
  
  @Deprecated
  public DigestScheme(ChallengeState paramChallengeState)
  {
    super(paramChallengeState);
  }
  
  public DigestScheme(Charset paramCharset)
  {
    super(paramCharset);
    this.complete = false;
  }
  
  public static String createCnonce()
  {
    SecureRandom localSecureRandom = new SecureRandom();
    byte[] arrayOfByte = new byte[8];
    localSecureRandom.nextBytes(arrayOfByte);
    return encode(arrayOfByte);
  }
  
  private Header createDigestHeader(Credentials paramCredentials, HttpRequest paramHttpRequest)
    throws AuthenticationException
  {
    String str3 = getParameter("uri");
    String str4 = getParameter("realm");
    String str2 = getParameter("nonce");
    String str1 = getParameter("opaque");
    String str7 = getParameter("methodname");
    Object localObject2 = getParameter("algorithm");
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = "MD5";
    }
    HashSet localHashSet = new HashSet(8);
    localObject2 = getParameter("qop");
    Object localObject3;
    int i;
    if (localObject2 != null)
    {
      localObject3 = new StringTokenizer((String)localObject2, ",");
      while (((StringTokenizer)localObject3).hasMoreTokens()) {
        localHashSet.add(((StringTokenizer)localObject3).nextToken().trim().toLowerCase(Locale.ENGLISH));
      }
      if (((paramHttpRequest instanceof HttpEntityEnclosingRequest)) && (localHashSet.contains("auth-int"))) {
        i = 1;
      } else if (localHashSet.contains("auth")) {
        i = 2;
      } else {
        i = -1;
      }
    }
    else
    {
      i = 0;
    }
    if (i != -1)
    {
      localObject3 = getParameter("charset");
      localObject2 = localObject3;
      if (localObject3 == null) {
        localObject2 = "ISO-8859-1";
      }
      if (((String)localObject1).equalsIgnoreCase("MD5-sess")) {
        localObject3 = "MD5";
      } else {
        localObject3 = localObject1;
      }
      try
      {
        Object localObject4 = createMessageDigest((String)localObject3);
        String str6 = paramCredentials.getUserPrincipal().getName();
        paramCredentials = paramCredentials.getPassword();
        if (str2.equals(this.lastNonce))
        {
          this.nounceCount += 1L;
        }
        else
        {
          this.nounceCount = 1L;
          this.cnonce = null;
          this.lastNonce = str2;
        }
        StringBuilder localStringBuilder = new StringBuilder(256);
        localObject3 = new Formatter(localStringBuilder, Locale.US);
        ((Formatter)localObject3).format("%08x", new Object[] { Long.valueOf(this.nounceCount) });
        ((Formatter)localObject3).close();
        String str5 = localStringBuilder.toString();
        if (this.cnonce == null) {
          this.cnonce = createCnonce();
        }
        this.a1 = null;
        this.a2 = null;
        if (((String)localObject1).equalsIgnoreCase("MD5-sess"))
        {
          localStringBuilder.setLength(0);
          localStringBuilder.append(str6);
          localStringBuilder.append(':');
          localStringBuilder.append(str4);
          localStringBuilder.append(':');
          localStringBuilder.append(paramCredentials);
          paramCredentials = encode(((MessageDigest)localObject4).digest(EncodingUtils.getBytes(localStringBuilder.toString(), (String)localObject2)));
          localStringBuilder.setLength(0);
          localStringBuilder.append(paramCredentials);
          localStringBuilder.append(':');
          localStringBuilder.append(str2);
          localStringBuilder.append(':');
          localStringBuilder.append(this.cnonce);
          this.a1 = localStringBuilder.toString();
        }
        else
        {
          localStringBuilder.setLength(0);
          localStringBuilder.append(str6);
          localStringBuilder.append(':');
          localStringBuilder.append(str4);
          localStringBuilder.append(':');
          localStringBuilder.append(paramCredentials);
          this.a1 = localStringBuilder.toString();
        }
        localObject3 = localObject4;
        localObject4 = encode(((MessageDigest)localObject3).digest(EncodingUtils.getBytes(this.a1, (String)localObject2)));
        if (i == 2)
        {
          paramCredentials = new StringBuilder();
          paramCredentials.append(str7);
          paramCredentials.append(':');
          paramCredentials.append(str3);
          this.a2 = paramCredentials.toString();
        }
        else if (i == 1)
        {
          if ((paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
            paramCredentials = ((HttpEntityEnclosingRequest)paramHttpRequest).getEntity();
          } else {
            paramCredentials = null;
          }
          if ((paramCredentials != null) && (!paramCredentials.isRepeatable()))
          {
            if (localHashSet.contains("auth"))
            {
              paramCredentials = new StringBuilder();
              paramCredentials.append(str7);
              paramCredentials.append(':');
              paramCredentials.append(str3);
              this.a2 = paramCredentials.toString();
              i = 2;
            }
            else
            {
              throw new AuthenticationException("Qop auth-int cannot be used with a non-repeatable entity");
            }
          }
          else
          {
            paramHttpRequest = new HttpEntityDigester((MessageDigest)localObject3);
            if (paramCredentials != null) {}
            try
            {
              paramCredentials.writeTo(paramHttpRequest);
              paramHttpRequest.close();
              paramCredentials = new StringBuilder();
              paramCredentials.append(str7);
              paramCredentials.append(':');
              paramCredentials.append(str3);
              paramCredentials.append(':');
              paramCredentials.append(encode(paramHttpRequest.getDigest()));
              this.a2 = paramCredentials.toString();
            }
            catch (IOException paramCredentials)
            {
              throw new AuthenticationException("I/O error reading entity content", paramCredentials);
            }
          }
        }
        else
        {
          paramCredentials = new StringBuilder();
          paramCredentials.append(str7);
          paramCredentials.append(':');
          paramCredentials.append(str3);
          this.a2 = paramCredentials.toString();
        }
        paramHttpRequest = encode(((MessageDigest)localObject3).digest(EncodingUtils.getBytes(this.a2, (String)localObject2)));
        if (i == 0)
        {
          localStringBuilder.setLength(0);
          localStringBuilder.append((String)localObject4);
          localStringBuilder.append(':');
          localStringBuilder.append(str2);
          localStringBuilder.append(':');
          localStringBuilder.append(paramHttpRequest);
          paramCredentials = localStringBuilder.toString();
        }
        else
        {
          localStringBuilder.setLength(0);
          localStringBuilder.append((String)localObject4);
          localStringBuilder.append(':');
          localStringBuilder.append(str2);
          localStringBuilder.append(':');
          localStringBuilder.append(str5);
          localStringBuilder.append(':');
          localStringBuilder.append(this.cnonce);
          localStringBuilder.append(':');
          if (i == 1) {
            paramCredentials = "auth-int";
          } else {
            paramCredentials = "auth";
          }
          localStringBuilder.append(paramCredentials);
          localStringBuilder.append(':');
          localStringBuilder.append(paramHttpRequest);
          paramCredentials = localStringBuilder.toString();
        }
        paramCredentials = encode(((MessageDigest)localObject3).digest(EncodingUtils.getAsciiBytes(paramCredentials)));
        paramHttpRequest = new CharArrayBuffer(128);
        if (isProxy()) {
          paramHttpRequest.append("Proxy-Authorization");
        } else {
          paramHttpRequest.append("Authorization");
        }
        paramHttpRequest.append(": Digest ");
        localObject2 = new ArrayList(20);
        ((List)localObject2).add(new BasicNameValuePair("username", str6));
        ((List)localObject2).add(new BasicNameValuePair("realm", str4));
        ((List)localObject2).add(new BasicNameValuePair("nonce", str2));
        ((List)localObject2).add(new BasicNameValuePair("uri", str3));
        ((List)localObject2).add(new BasicNameValuePair("response", paramCredentials));
        if (i != 0)
        {
          if (i == 1) {
            paramCredentials = "auth-int";
          } else {
            paramCredentials = "auth";
          }
          ((List)localObject2).add(new BasicNameValuePair("qop", paramCredentials));
          ((List)localObject2).add(new BasicNameValuePair("nc", str5));
          ((List)localObject2).add(new BasicNameValuePair("cnonce", this.cnonce));
        }
        ((List)localObject2).add(new BasicNameValuePair("algorithm", (String)localObject1));
        if (str1 != null) {
          ((List)localObject2).add(new BasicNameValuePair("opaque", str1));
        }
        for (i = 0; i < ((List)localObject2).size(); i++)
        {
          paramCredentials = (BasicNameValuePair)((List)localObject2).get(i);
          if (i > 0) {
            paramHttpRequest.append(", ");
          }
          localObject1 = paramCredentials.getName();
          int j;
          if ((!"nc".equals(localObject1)) && (!"qop".equals(localObject1)) && (!"algorithm".equals(localObject1))) {
            j = 0;
          } else {
            j = 1;
          }
          BasicHeaderValueFormatter.INSTANCE.formatNameValuePair(paramHttpRequest, paramCredentials, j ^ 0x1);
        }
        return new BufferedHeader(paramHttpRequest);
      }
      catch (UnsupportedDigestAlgorithmException paramCredentials)
      {
        paramCredentials = new StringBuilder();
        paramCredentials.append("Unsuppported digest algorithm: ");
        paramCredentials.append((String)localObject3);
        throw new AuthenticationException(paramCredentials.toString());
      }
    }
    paramCredentials = new StringBuilder();
    paramCredentials.append("None of the qop methods is supported: ");
    paramCredentials.append((String)localObject2);
    throw new AuthenticationException(paramCredentials.toString());
  }
  
  private static MessageDigest createMessageDigest(String paramString)
    throws UnsupportedDigestAlgorithmException
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance(paramString);
      return localMessageDigest;
    }
    catch (Exception localException)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Unsupported algorithm in HTTP Digest authentication: ");
      localStringBuilder.append(paramString);
      throw new UnsupportedDigestAlgorithmException(localStringBuilder.toString());
    }
  }
  
  static String encode(byte[] paramArrayOfByte)
  {
    int j = paramArrayOfByte.length;
    char[] arrayOfChar2 = new char[j * 2];
    for (int i = 0; i < j; i++)
    {
      int m = paramArrayOfByte[i];
      int n = paramArrayOfByte[i];
      int k = i * 2;
      char[] arrayOfChar1 = HEXADECIMAL;
      arrayOfChar2[k] = arrayOfChar1[((n & 0xF0) >> 4)];
      arrayOfChar2[(k + 1)] = arrayOfChar1[(m & 0xF)];
    }
    return new String(arrayOfChar2);
  }
  
  @Deprecated
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest)
    throws AuthenticationException
  {
    return authenticate(paramCredentials, paramHttpRequest, new BasicHttpContext());
  }
  
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    Args.notNull(paramCredentials, "Credentials");
    Args.notNull(paramHttpRequest, "HTTP request");
    if (getParameter("realm") != null)
    {
      if (getParameter("nonce") != null)
      {
        getParameters().put("methodname", paramHttpRequest.getRequestLine().getMethod());
        getParameters().put("uri", paramHttpRequest.getRequestLine().getUri());
        if (getParameter("charset") == null) {
          getParameters().put("charset", getCredentialsCharset(paramHttpRequest));
        }
        return createDigestHeader(paramCredentials, paramHttpRequest);
      }
      throw new AuthenticationException("missing nonce in challenge");
    }
    throw new AuthenticationException("missing realm in challenge");
  }
  
  String getA1()
  {
    return this.a1;
  }
  
  String getA2()
  {
    return this.a2;
  }
  
  String getCnonce()
  {
    return this.cnonce;
  }
  
  public String getSchemeName()
  {
    return "digest";
  }
  
  public boolean isComplete()
  {
    if ("true".equalsIgnoreCase(getParameter("stale"))) {
      return false;
    }
    return this.complete;
  }
  
  public boolean isConnectionBased()
  {
    return false;
  }
  
  public void overrideParamter(String paramString1, String paramString2)
  {
    getParameters().put(paramString1, paramString2);
  }
  
  public void processChallenge(Header paramHeader)
    throws MalformedChallengeException
  {
    super.processChallenge(paramHeader);
    this.complete = true;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DIGEST [complete=");
    localStringBuilder.append(this.complete);
    localStringBuilder.append(", nonce=");
    localStringBuilder.append(this.lastNonce);
    localStringBuilder.append(", nc=");
    localStringBuilder.append(this.nounceCount);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/DigestScheme.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */