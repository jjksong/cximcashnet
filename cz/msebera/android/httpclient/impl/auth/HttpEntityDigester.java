package cz.msebera.android.httpclient.impl.auth;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;

class HttpEntityDigester
  extends OutputStream
{
  private boolean closed;
  private byte[] digest;
  private final MessageDigest digester;
  
  HttpEntityDigester(MessageDigest paramMessageDigest)
  {
    this.digester = paramMessageDigest;
    this.digester.reset();
  }
  
  public void close()
    throws IOException
  {
    if (this.closed) {
      return;
    }
    this.closed = true;
    this.digest = this.digester.digest();
    super.close();
  }
  
  public byte[] getDigest()
  {
    return this.digest;
  }
  
  public void write(int paramInt)
    throws IOException
  {
    if (!this.closed)
    {
      this.digester.update((byte)paramInt);
      return;
    }
    throw new IOException("Stream has been already closed");
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (!this.closed)
    {
      this.digester.update(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    throw new IOException("Stream has been already closed");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/HttpEntityDigester.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */