package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.auth.AuthOption;
import cz.msebera.android.httpclient.auth.AuthProtocolState;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.ContextAwareAuthScheme;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;

public class HttpAuthenticator
{
  public HttpClientAndroidLog log;
  
  public HttpAuthenticator()
  {
    this(null);
  }
  
  public HttpAuthenticator(HttpClientAndroidLog paramHttpClientAndroidLog)
  {
    if (paramHttpClientAndroidLog == null) {
      paramHttpClientAndroidLog = new HttpClientAndroidLog(getClass());
    }
    this.log = paramHttpClientAndroidLog;
  }
  
  private Header doAuth(AuthScheme paramAuthScheme, Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    if ((paramAuthScheme instanceof ContextAwareAuthScheme)) {
      return ((ContextAwareAuthScheme)paramAuthScheme).authenticate(paramCredentials, paramHttpRequest, paramHttpContext);
    }
    return paramAuthScheme.authenticate(paramCredentials, paramHttpRequest);
  }
  
  private void ensureAuthScheme(AuthScheme paramAuthScheme)
  {
    Asserts.notNull(paramAuthScheme, "Auth scheme");
  }
  
  public void generateAuthResponse(HttpRequest paramHttpRequest, AuthState paramAuthState, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    Object localObject1 = paramAuthState.getCredentials();
    int i = 1.$SwitchMap$cz$msebera$android$httpclient$auth$AuthProtocolState[paramAuthState.getState().ordinal()];
    if (i != 1)
    {
      switch (i)
      {
      default: 
        break;
      case 4: 
        return;
      case 3: 
        ensureAuthScheme(localAuthScheme);
        if (!localAuthScheme.isConnectionBased()) {
          break;
        }
        return;
      }
    }
    else
    {
      Queue localQueue = paramAuthState.getAuthOptions();
      if (localQueue != null)
      {
        while (!localQueue.isEmpty())
        {
          localObject1 = (AuthOption)localQueue.remove();
          localAuthScheme = ((AuthOption)localObject1).getAuthScheme();
          Credentials localCredentials = ((AuthOption)localObject1).getCredentials();
          paramAuthState.update(localAuthScheme, localCredentials);
          Object localObject2;
          if (this.log.isDebugEnabled())
          {
            localObject2 = this.log;
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append("Generating response to an authentication challenge using ");
            ((StringBuilder)localObject1).append(localAuthScheme.getSchemeName());
            ((StringBuilder)localObject1).append(" scheme");
            ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
          }
          try
          {
            paramHttpRequest.addHeader(doAuth(localAuthScheme, localCredentials, paramHttpRequest, paramHttpContext));
          }
          catch (AuthenticationException localAuthenticationException) {}
          if (this.log.isWarnEnabled())
          {
            localObject1 = this.log;
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append(localAuthScheme);
            ((StringBuilder)localObject2).append(" authentication error: ");
            ((StringBuilder)localObject2).append(localAuthenticationException.getMessage());
            ((HttpClientAndroidLog)localObject1).warn(((StringBuilder)localObject2).toString());
          }
        }
        return;
      }
      ensureAuthScheme(localAuthScheme);
    }
    if (localAuthScheme != null) {
      try
      {
        paramHttpRequest.addHeader(doAuth(localAuthScheme, (Credentials)localObject1, paramHttpRequest, paramHttpContext));
      }
      catch (AuthenticationException paramAuthState)
      {
        if (this.log.isErrorEnabled())
        {
          paramHttpContext = this.log;
          paramHttpRequest = new StringBuilder();
          paramHttpRequest.append(localAuthScheme);
          paramHttpRequest.append(" authentication error: ");
          paramHttpRequest.append(paramAuthState.getMessage());
          paramHttpContext.error(paramHttpRequest.toString());
        }
      }
    }
  }
  
  public boolean handleAuthChallenge(HttpHost paramHttpHost, HttpResponse paramHttpResponse, AuthenticationStrategy paramAuthenticationStrategy, AuthState paramAuthState, HttpContext paramHttpContext)
  {
    try
    {
      if (this.log.isDebugEnabled())
      {
        localObject2 = this.log;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(paramHttpHost.toHostString());
        ((StringBuilder)localObject1).append(" requested authentication");
        ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
      }
      Object localObject2 = paramAuthenticationStrategy.getChallenges(paramHttpHost, paramHttpResponse, paramHttpContext);
      if (((Map)localObject2).isEmpty())
      {
        this.log.debug("Response contains no authentication challenges");
        return false;
      }
      Object localObject1 = paramAuthState.getAuthScheme();
      switch (paramAuthState.getState())
      {
      default: 
        break;
      case ???: 
        return false;
      case ???: 
        paramAuthState.reset();
        break;
      case ???: 
      case ???: 
        if (localObject1 == null)
        {
          this.log.debug("Auth scheme is null");
          paramAuthenticationStrategy.authFailed(paramHttpHost, null, paramHttpContext);
          paramAuthState.reset();
          paramAuthState.setState(AuthProtocolState.FAILURE);
          return false;
        }
        break;
      }
      if (localObject1 != null)
      {
        Header localHeader = (Header)((Map)localObject2).get(((AuthScheme)localObject1).getSchemeName().toLowerCase(Locale.ENGLISH));
        if (localHeader != null)
        {
          this.log.debug("Authorization challenge processed");
          ((AuthScheme)localObject1).processChallenge(localHeader);
          if (((AuthScheme)localObject1).isComplete())
          {
            this.log.debug("Authentication failed");
            paramAuthenticationStrategy.authFailed(paramHttpHost, paramAuthState.getAuthScheme(), paramHttpContext);
            paramAuthState.reset();
            paramAuthState.setState(AuthProtocolState.FAILURE);
            return false;
          }
          paramAuthState.setState(AuthProtocolState.HANDSHAKE);
          return true;
        }
        paramAuthState.reset();
      }
      paramHttpHost = paramAuthenticationStrategy.select((Map)localObject2, paramHttpHost, paramHttpResponse, paramHttpContext);
      if ((paramHttpHost != null) && (!paramHttpHost.isEmpty()))
      {
        if (this.log.isDebugEnabled())
        {
          paramHttpResponse = this.log;
          paramAuthenticationStrategy = new java/lang/StringBuilder;
          paramAuthenticationStrategy.<init>();
          paramAuthenticationStrategy.append("Selected authentication options: ");
          paramAuthenticationStrategy.append(paramHttpHost);
          paramHttpResponse.debug(paramAuthenticationStrategy.toString());
        }
        paramAuthState.setState(AuthProtocolState.CHALLENGED);
        paramAuthState.update(paramHttpHost);
        return true;
      }
      return false;
    }
    catch (MalformedChallengeException paramHttpResponse)
    {
      if (this.log.isWarnEnabled())
      {
        paramAuthenticationStrategy = this.log;
        paramHttpHost = new StringBuilder();
        paramHttpHost.append("Malformed challenge: ");
        paramHttpHost.append(paramHttpResponse.getMessage());
        paramAuthenticationStrategy.warn(paramHttpHost.toString());
      }
      paramAuthState.reset();
    }
    return false;
  }
  
  public boolean isAuthenticationRequested(HttpHost paramHttpHost, HttpResponse paramHttpResponse, AuthenticationStrategy paramAuthenticationStrategy, AuthState paramAuthState, HttpContext paramHttpContext)
  {
    if (paramAuthenticationStrategy.isAuthenticationRequested(paramHttpHost, paramHttpResponse, paramHttpContext))
    {
      this.log.debug("Authentication required");
      if (paramAuthState.getState() == AuthProtocolState.SUCCESS) {
        paramAuthenticationStrategy.authFailed(paramHttpHost, paramAuthState.getAuthScheme(), paramHttpContext);
      }
      return true;
    }
    switch (paramAuthState.getState())
    {
    default: 
      paramAuthState.setState(AuthProtocolState.UNCHALLENGED);
      break;
    case ???: 
    case ???: 
      this.log.debug("Authentication succeeded");
      paramAuthState.setState(AuthProtocolState.SUCCESS);
      paramAuthenticationStrategy.authSucceeded(paramHttpHost, paramAuthState.getAuthScheme(), paramHttpContext);
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/auth/HttpAuthenticator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */