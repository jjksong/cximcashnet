package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.config.ConnectionConfig;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;

public final class ConnSupport
{
  public static CharsetDecoder createDecoder(ConnectionConfig paramConnectionConfig)
  {
    if (paramConnectionConfig == null) {
      return null;
    }
    Object localObject2 = paramConnectionConfig.getCharset();
    Object localObject1 = paramConnectionConfig.getMalformedInputAction();
    CodingErrorAction localCodingErrorAction = paramConnectionConfig.getUnmappableInputAction();
    if (localObject2 != null)
    {
      localObject2 = ((Charset)localObject2).newDecoder();
      if (localObject1 != null) {
        paramConnectionConfig = (ConnectionConfig)localObject1;
      } else {
        paramConnectionConfig = CodingErrorAction.REPORT;
      }
      localObject1 = ((CharsetDecoder)localObject2).onMalformedInput(paramConnectionConfig);
      if (localCodingErrorAction != null) {
        paramConnectionConfig = localCodingErrorAction;
      } else {
        paramConnectionConfig = CodingErrorAction.REPORT;
      }
      return ((CharsetDecoder)localObject1).onUnmappableCharacter(paramConnectionConfig);
    }
    return null;
  }
  
  public static CharsetEncoder createEncoder(ConnectionConfig paramConnectionConfig)
  {
    if (paramConnectionConfig == null) {
      return null;
    }
    Object localObject2 = paramConnectionConfig.getCharset();
    if (localObject2 != null)
    {
      Object localObject1 = paramConnectionConfig.getMalformedInputAction();
      CodingErrorAction localCodingErrorAction = paramConnectionConfig.getUnmappableInputAction();
      localObject2 = ((Charset)localObject2).newEncoder();
      if (localObject1 != null) {
        paramConnectionConfig = (ConnectionConfig)localObject1;
      } else {
        paramConnectionConfig = CodingErrorAction.REPORT;
      }
      localObject1 = ((CharsetEncoder)localObject2).onMalformedInput(paramConnectionConfig);
      if (localCodingErrorAction != null) {
        paramConnectionConfig = localCodingErrorAction;
      } else {
        paramConnectionConfig = CodingErrorAction.REPORT;
      }
      return ((CharsetEncoder)localObject1).onUnmappableCharacter(paramConnectionConfig);
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/ConnSupport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */