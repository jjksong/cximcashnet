package cz.msebera.android.httpclient.impl;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.TokenIterator;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.message.BasicTokenIterator;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class DefaultConnectionReuseStrategy
  implements ConnectionReuseStrategy
{
  public static final DefaultConnectionReuseStrategy INSTANCE = new DefaultConnectionReuseStrategy();
  
  private boolean canResponseHaveBody(HttpResponse paramHttpResponse)
  {
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    boolean bool;
    if ((i >= 200) && (i != 204) && (i != 304) && (i != 205)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected TokenIterator createTokenIterator(HeaderIterator paramHeaderIterator)
  {
    return new BasicTokenIterator(paramHeaderIterator);
  }
  
  public boolean keepAlive(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    Args.notNull(paramHttpContext, "HTTP context");
    ProtocolVersion localProtocolVersion = paramHttpResponse.getStatusLine().getProtocolVersion();
    paramHttpContext = paramHttpResponse.getFirstHeader("Transfer-Encoding");
    int i;
    if (paramHttpContext != null)
    {
      if (!"chunked".equalsIgnoreCase(paramHttpContext.getValue())) {
        return false;
      }
    }
    else if (canResponseHaveBody(paramHttpResponse))
    {
      paramHttpContext = paramHttpResponse.getHeaders("Content-Length");
      if (paramHttpContext.length == 1)
      {
        paramHttpContext = paramHttpContext[0];
        try
        {
          i = Integer.parseInt(paramHttpContext.getValue());
          if (i >= 0) {
            break label104;
          }
          return false;
        }
        catch (NumberFormatException paramHttpResponse)
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    label104:
    HeaderIterator localHeaderIterator = paramHttpResponse.headerIterator("Connection");
    paramHttpContext = localHeaderIterator;
    if (!localHeaderIterator.hasNext()) {
      paramHttpContext = paramHttpResponse.headerIterator("Proxy-Connection");
    }
    if (paramHttpContext.hasNext()) {
      try
      {
        paramHttpContext = createTokenIterator(paramHttpContext);
        i = 0;
        while (paramHttpContext.hasNext())
        {
          paramHttpResponse = paramHttpContext.nextToken();
          if ("Close".equalsIgnoreCase(paramHttpResponse)) {
            return false;
          }
          boolean bool = "Keep-Alive".equalsIgnoreCase(paramHttpResponse);
          if (bool) {
            i = 1;
          }
        }
        if (i != 0) {
          return true;
        }
      }
      catch (ParseException paramHttpResponse)
      {
        return false;
      }
    }
    return localProtocolVersion.lessEquals(HttpVersion.HTTP_1_0) ^ true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/DefaultConnectionReuseStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */