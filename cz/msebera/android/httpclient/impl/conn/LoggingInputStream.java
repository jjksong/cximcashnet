package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
class LoggingInputStream
  extends InputStream
{
  private final InputStream in;
  private final Wire wire;
  
  public LoggingInputStream(InputStream paramInputStream, Wire paramWire)
  {
    this.in = paramInputStream;
    this.wire = paramWire;
  }
  
  public int available()
    throws IOException
  {
    try
    {
      int i = this.in.available();
      return i;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[available] I/O error : ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.input(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public void close()
    throws IOException
  {
    try
    {
      this.in.close();
      return;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[close] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.input(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public void mark(int paramInt)
  {
    super.mark(paramInt);
  }
  
  public boolean markSupported()
  {
    return false;
  }
  
  public int read()
    throws IOException
  {
    try
    {
      int i = this.in.read();
      if (i == -1) {
        this.wire.input("end of stream");
      } else {
        this.wire.input(i);
      }
      return i;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[read] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.input(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      int i = this.in.read(paramArrayOfByte);
      if (i == -1) {
        this.wire.input("end of stream");
      } else if (i > 0) {
        this.wire.input(paramArrayOfByte, 0, i);
      }
      return i;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("[read] I/O error: ");
      paramArrayOfByte.append(localIOException.getMessage());
      localWire.input(paramArrayOfByte.toString());
      throw localIOException;
    }
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    try
    {
      paramInt2 = this.in.read(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt2 == -1) {
        this.wire.input("end of stream");
      } else if (paramInt2 > 0) {
        this.wire.input(paramArrayOfByte, paramInt1, paramInt2);
      }
      return paramInt2;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("[read] I/O error: ");
      paramArrayOfByte.append(localIOException.getMessage());
      localWire.input(paramArrayOfByte.toString());
      throw localIOException;
    }
  }
  
  public void reset()
    throws IOException
  {
    super.reset();
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    try
    {
      paramLong = super.skip(paramLong);
      return paramLong;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[skip] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.input(localStringBuilder.toString());
      throw localIOException;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/LoggingInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */