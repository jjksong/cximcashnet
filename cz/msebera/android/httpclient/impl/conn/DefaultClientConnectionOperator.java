package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.HttpInetSocketAddress;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeLayeredSocketFactory;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.conn.scheme.SchemeSocketFactory;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

@Deprecated
@ThreadSafe
public class DefaultClientConnectionOperator
  implements ClientConnectionOperator
{
  protected final DnsResolver dnsResolver;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  protected final SchemeRegistry schemeRegistry;
  
  public DefaultClientConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = new SystemDefaultDnsResolver();
  }
  
  public DefaultClientConnectionOperator(SchemeRegistry paramSchemeRegistry, DnsResolver paramDnsResolver)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    Args.notNull(paramDnsResolver, "DNS resolver");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = paramDnsResolver;
  }
  
  private SchemeRegistry getSchemeRegistry(HttpContext paramHttpContext)
  {
    SchemeRegistry localSchemeRegistry = (SchemeRegistry)paramHttpContext.getAttribute("http.scheme-registry");
    paramHttpContext = localSchemeRegistry;
    if (localSchemeRegistry == null) {
      paramHttpContext = this.schemeRegistry;
    }
    return paramHttpContext;
  }
  
  public OperatedClientConnection createConnection()
  {
    return new DefaultClientConnection();
  }
  
  public void openConnection(OperatedClientConnection paramOperatedClientConnection, HttpHost paramHttpHost, InetAddress paramInetAddress, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramOperatedClientConnection, "Connection");
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpParams, "HTTP parameters");
    Asserts.check(paramOperatedClientConnection.isOpen() ^ true, "Connection must not be open");
    Object localObject1 = getSchemeRegistry(paramHttpContext).getScheme(paramHttpHost.getSchemeName());
    SchemeSocketFactory localSchemeSocketFactory = ((Scheme)localObject1).getSchemeSocketFactory();
    InetAddress[] arrayOfInetAddress = resolveHostname(paramHttpHost.getHostName());
    int k = ((Scheme)localObject1).resolvePort(paramHttpHost.getPort());
    int i = 0;
    for (;;)
    {
      Object localObject3 = paramInetAddress;
      if (i < arrayOfInetAddress.length)
      {
        localObject1 = arrayOfInetAddress[i];
        int j;
        if (i == arrayOfInetAddress.length - 1) {
          j = 1;
        } else {
          j = 0;
        }
        Object localObject2 = localSchemeSocketFactory.createSocket(paramHttpParams);
        paramOperatedClientConnection.opening((Socket)localObject2, paramHttpHost);
        HttpInetSocketAddress localHttpInetSocketAddress = new HttpInetSocketAddress(paramHttpHost, (InetAddress)localObject1, k);
        localObject1 = null;
        if (localObject3 != null) {
          localObject1 = new InetSocketAddress((InetAddress)localObject3, 0);
        }
        if (this.log.isDebugEnabled())
        {
          localObject3 = this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Connecting to ");
          localStringBuilder.append(localHttpInetSocketAddress);
          ((HttpClientAndroidLog)localObject3).debug(localStringBuilder.toString());
        }
        try
        {
          localObject3 = localSchemeSocketFactory.connectSocket((Socket)localObject2, localHttpInetSocketAddress, (InetSocketAddress)localObject1, paramHttpParams);
          localObject1 = localObject2;
          if (localObject2 != localObject3)
          {
            paramOperatedClientConnection.opening((Socket)localObject3, paramHttpHost);
            localObject1 = localObject3;
          }
          prepareSocket((Socket)localObject1, paramHttpContext, paramHttpParams);
          paramOperatedClientConnection.openCompleted(localSchemeSocketFactory.isSecure((Socket)localObject1), paramHttpParams);
          return;
        }
        catch (ConnectTimeoutException localConnectTimeoutException)
        {
          if (j != 0) {
            throw localConnectTimeoutException;
          }
        }
        catch (ConnectException localConnectException)
        {
          HttpClientAndroidLog localHttpClientAndroidLog;
          if (j == 0)
          {
            if (this.log.isDebugEnabled())
            {
              localHttpClientAndroidLog = this.log;
              localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append("Connect to ");
              ((StringBuilder)localObject2).append(localHttpInetSocketAddress);
              ((StringBuilder)localObject2).append(" timed out. ");
              ((StringBuilder)localObject2).append("Connection will be retried using another IP address");
              localHttpClientAndroidLog.debug(((StringBuilder)localObject2).toString());
            }
            i++;
          }
          else
          {
            throw localHttpClientAndroidLog;
          }
        }
      }
    }
  }
  
  protected void prepareSocket(Socket paramSocket, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    paramSocket.setTcpNoDelay(HttpConnectionParams.getTcpNoDelay(paramHttpParams));
    paramSocket.setSoTimeout(HttpConnectionParams.getSoTimeout(paramHttpParams));
    int i = HttpConnectionParams.getLinger(paramHttpParams);
    if (i >= 0)
    {
      boolean bool;
      if (i > 0) {
        bool = true;
      } else {
        bool = false;
      }
      paramSocket.setSoLinger(bool, i);
    }
  }
  
  protected InetAddress[] resolveHostname(String paramString)
    throws UnknownHostException
  {
    return this.dnsResolver.resolve(paramString);
  }
  
  public void updateSecureConnection(OperatedClientConnection paramOperatedClientConnection, HttpHost paramHttpHost, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramOperatedClientConnection, "Connection");
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpParams, "Parameters");
    Asserts.check(paramOperatedClientConnection.isOpen(), "Connection must be open");
    Object localObject = getSchemeRegistry(paramHttpContext).getScheme(paramHttpHost.getSchemeName());
    Asserts.check(((Scheme)localObject).getSchemeSocketFactory() instanceof SchemeLayeredSocketFactory, "Socket factory must implement SchemeLayeredSocketFactory");
    SchemeLayeredSocketFactory localSchemeLayeredSocketFactory = (SchemeLayeredSocketFactory)((Scheme)localObject).getSchemeSocketFactory();
    localObject = localSchemeLayeredSocketFactory.createLayeredSocket(paramOperatedClientConnection.getSocket(), paramHttpHost.getHostName(), ((Scheme)localObject).resolvePort(paramHttpHost.getPort()), paramHttpParams);
    prepareSocket((Socket)localObject, paramHttpContext, paramHttpParams);
    paramOperatedClientConnection.update((Socket)localObject, paramHttpHost, localSchemeLayeredSocketFactory.isSecure((Socket)localObject), paramHttpParams);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/DefaultClientConnectionOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */