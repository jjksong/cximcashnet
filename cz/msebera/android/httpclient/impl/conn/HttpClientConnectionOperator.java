package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.HttpHostConnectException;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.UnsupportedSchemeException;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.LayeredConnectionSocketFactory;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.net.SocketTimeoutException;

@Immutable
class HttpClientConnectionOperator
{
  static final String SOCKET_FACTORY_REGISTRY = "http.socket-factory-registry";
  private final DnsResolver dnsResolver;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final SchemePortResolver schemePortResolver;
  private final Lookup<ConnectionSocketFactory> socketFactoryRegistry;
  
  HttpClientConnectionOperator(Lookup<ConnectionSocketFactory> paramLookup, SchemePortResolver paramSchemePortResolver, DnsResolver paramDnsResolver)
  {
    Args.notNull(paramLookup, "Socket factory registry");
    this.socketFactoryRegistry = paramLookup;
    if (paramSchemePortResolver == null) {
      paramSchemePortResolver = DefaultSchemePortResolver.INSTANCE;
    }
    this.schemePortResolver = paramSchemePortResolver;
    if (paramDnsResolver == null) {
      paramDnsResolver = SystemDefaultDnsResolver.INSTANCE;
    }
    this.dnsResolver = paramDnsResolver;
  }
  
  private Lookup<ConnectionSocketFactory> getSocketFactoryRegistry(HttpContext paramHttpContext)
  {
    Lookup localLookup = (Lookup)paramHttpContext.getAttribute("http.socket-factory-registry");
    paramHttpContext = localLookup;
    if (localLookup == null) {
      paramHttpContext = this.socketFactoryRegistry;
    }
    return paramHttpContext;
  }
  
  public void connect(ManagedHttpClientConnection paramManagedHttpClientConnection, HttpHost paramHttpHost, InetSocketAddress paramInetSocketAddress, int paramInt, SocketConfig paramSocketConfig, HttpContext paramHttpContext)
    throws IOException
  {
    ConnectionSocketFactory localConnectionSocketFactory = (ConnectionSocketFactory)getSocketFactoryRegistry(paramHttpContext).lookup(paramHttpHost.getSchemeName());
    if (localConnectionSocketFactory != null)
    {
      InetAddress[] arrayOfInetAddress;
      if (paramHttpHost.getAddress() != null)
      {
        arrayOfInetAddress = new InetAddress[1];
        arrayOfInetAddress[0] = paramHttpHost.getAddress();
      }
      else
      {
        arrayOfInetAddress = this.dnsResolver.resolve(paramHttpHost.getHostName());
      }
      int i = this.schemePortResolver.resolve(paramHttpHost);
      int j = 0;
      while (j < arrayOfInetAddress.length)
      {
        Object localObject1 = arrayOfInetAddress[j];
        int k;
        if (j == arrayOfInetAddress.length - 1) {
          k = 1;
        } else {
          k = 0;
        }
        Object localObject2 = localConnectionSocketFactory.createSocket(paramHttpContext);
        ((Socket)localObject2).setSoTimeout(paramSocketConfig.getSoTimeout());
        ((Socket)localObject2).setReuseAddress(paramSocketConfig.isSoReuseAddress());
        ((Socket)localObject2).setTcpNoDelay(paramSocketConfig.isTcpNoDelay());
        ((Socket)localObject2).setKeepAlive(paramSocketConfig.isSoKeepAlive());
        int m = paramSocketConfig.getSoLinger();
        if (m >= 0)
        {
          boolean bool;
          if (m > 0) {
            bool = true;
          } else {
            bool = false;
          }
          ((Socket)localObject2).setSoLinger(bool, m);
        }
        paramManagedHttpClientConnection.bind((Socket)localObject2);
        localObject1 = new InetSocketAddress((InetAddress)localObject1, i);
        Object localObject3;
        if (this.log.isDebugEnabled())
        {
          localObject3 = this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Connecting to ");
          localStringBuilder.append(localObject1);
          ((HttpClientAndroidLog)localObject3).debug(localStringBuilder.toString());
        }
        try
        {
          paramManagedHttpClientConnection.bind(localConnectionSocketFactory.connectSocket(paramInt, (Socket)localObject2, paramHttpHost, (InetSocketAddress)localObject1, paramInetSocketAddress, paramHttpContext));
          if (this.log.isDebugEnabled())
          {
            localObject2 = this.log;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            ((StringBuilder)localObject3).append("Connection established ");
            ((StringBuilder)localObject3).append(paramManagedHttpClientConnection);
            ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject3).toString());
          }
          return;
        }
        catch (NoRouteToHostException localNoRouteToHostException)
        {
          if (k != 0) {
            throw localNoRouteToHostException;
          }
        }
        catch (ConnectException localConnectException)
        {
          if (k != 0)
          {
            if ("Connection timed out".equals(localConnectException.getMessage())) {
              throw new ConnectTimeoutException(localConnectException, paramHttpHost, arrayOfInetAddress);
            }
            throw new HttpHostConnectException(localConnectException, paramHttpHost, arrayOfInetAddress);
          }
        }
        catch (SocketTimeoutException localSocketTimeoutException)
        {
          HttpClientAndroidLog localHttpClientAndroidLog;
          if (k == 0)
          {
            if (this.log.isDebugEnabled())
            {
              localHttpClientAndroidLog = this.log;
              localObject3 = new StringBuilder();
              ((StringBuilder)localObject3).append("Connect to ");
              ((StringBuilder)localObject3).append(localObject1);
              ((StringBuilder)localObject3).append(" timed out. ");
              ((StringBuilder)localObject3).append("Connection will be retried using another IP address");
              localHttpClientAndroidLog.debug(((StringBuilder)localObject3).toString());
            }
            j++;
          }
          else
          {
            throw new ConnectTimeoutException(localHttpClientAndroidLog, paramHttpHost, arrayOfInetAddress);
          }
        }
      }
      return;
    }
    paramManagedHttpClientConnection = new StringBuilder();
    paramManagedHttpClientConnection.append(paramHttpHost.getSchemeName());
    paramManagedHttpClientConnection.append(" protocol is not supported");
    throw new UnsupportedSchemeException(paramManagedHttpClientConnection.toString());
  }
  
  public void upgrade(ManagedHttpClientConnection paramManagedHttpClientConnection, HttpHost paramHttpHost, HttpContext paramHttpContext)
    throws IOException
  {
    Object localObject = (ConnectionSocketFactory)getSocketFactoryRegistry(HttpClientContext.adapt(paramHttpContext)).lookup(paramHttpHost.getSchemeName());
    if (localObject != null)
    {
      if ((localObject instanceof LayeredConnectionSocketFactory))
      {
        LayeredConnectionSocketFactory localLayeredConnectionSocketFactory = (LayeredConnectionSocketFactory)localObject;
        localObject = paramManagedHttpClientConnection.getSocket();
        int i = this.schemePortResolver.resolve(paramHttpHost);
        paramManagedHttpClientConnection.bind(localLayeredConnectionSocketFactory.createLayeredSocket((Socket)localObject, paramHttpHost.getHostName(), i, paramHttpContext));
        return;
      }
      paramManagedHttpClientConnection = new StringBuilder();
      paramManagedHttpClientConnection.append(paramHttpHost.getSchemeName());
      paramManagedHttpClientConnection.append(" protocol does not support connection upgrade");
      throw new UnsupportedSchemeException(paramManagedHttpClientConnection.toString());
    }
    paramManagedHttpClientConnection = new StringBuilder();
    paramManagedHttpClientConnection.append(paramHttpHost.getSchemeName());
    paramManagedHttpClientConnection.append(" protocol is not supported");
    throw new UnsupportedSchemeException(paramManagedHttpClientConnection.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/HttpClientConnectionOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */