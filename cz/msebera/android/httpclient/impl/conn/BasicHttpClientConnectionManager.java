package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.config.Registry;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.conn.ConnectionRequest;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.HttpConnectionFactory;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.PlainConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Closeable;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@ThreadSafe
public class BasicHttpClientConnectionManager
  implements HttpClientConnectionManager, Closeable
{
  @GuardedBy("this")
  private ManagedHttpClientConnection conn;
  @GuardedBy("this")
  private ConnectionConfig connConfig;
  private final HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> connFactory;
  private final HttpClientConnectionOperator connectionOperator;
  @GuardedBy("this")
  private long expiry;
  private final AtomicBoolean isShutdown;
  @GuardedBy("this")
  private boolean leased;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  private HttpRoute route;
  @GuardedBy("this")
  private SocketConfig socketConfig;
  @GuardedBy("this")
  private Object state;
  @GuardedBy("this")
  private long updated;
  
  public BasicHttpClientConnectionManager()
  {
    this(getDefaultRegistry(), null, null, null);
  }
  
  public BasicHttpClientConnectionManager(Lookup<ConnectionSocketFactory> paramLookup)
  {
    this(paramLookup, null, null, null);
  }
  
  public BasicHttpClientConnectionManager(Lookup<ConnectionSocketFactory> paramLookup, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory)
  {
    this(paramLookup, paramHttpConnectionFactory, null, null);
  }
  
  public BasicHttpClientConnectionManager(Lookup<ConnectionSocketFactory> paramLookup, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory, SchemePortResolver paramSchemePortResolver, DnsResolver paramDnsResolver)
  {
    this.connectionOperator = new HttpClientConnectionOperator(paramLookup, paramSchemePortResolver, paramDnsResolver);
    if (paramHttpConnectionFactory == null) {
      paramHttpConnectionFactory = ManagedHttpClientConnectionFactory.INSTANCE;
    }
    this.connFactory = paramHttpConnectionFactory;
    this.expiry = Long.MAX_VALUE;
    this.socketConfig = SocketConfig.DEFAULT;
    this.connConfig = ConnectionConfig.DEFAULT;
    this.isShutdown = new AtomicBoolean(false);
  }
  
  private void checkExpiry()
  {
    if ((this.conn != null) && (System.currentTimeMillis() >= this.expiry))
    {
      if (this.log.isDebugEnabled())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Connection expired @ ");
        localStringBuilder.append(new Date(this.expiry));
        localHttpClientAndroidLog.debug(localStringBuilder.toString());
      }
      closeConnection();
    }
  }
  
  private void closeConnection()
  {
    if (this.conn != null)
    {
      this.log.debug("Closing connection");
      try
      {
        this.conn.close();
      }
      catch (IOException localIOException)
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("I/O exception closing connection", localIOException);
        }
      }
      this.conn = null;
    }
  }
  
  private static Registry<ConnectionSocketFactory> getDefaultRegistry()
  {
    return RegistryBuilder.create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", SSLConnectionSocketFactory.getSocketFactory()).build();
  }
  
  private void shutdownConnection()
  {
    if (this.conn != null)
    {
      this.log.debug("Shutting down connection");
      try
      {
        this.conn.shutdown();
      }
      catch (IOException localIOException)
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("I/O exception shutting down connection", localIOException);
        }
      }
      this.conn = null;
    }
  }
  
  public void close()
  {
    shutdown();
  }
  
  public void closeExpiredConnections()
  {
    try
    {
      boolean bool = this.isShutdown.get();
      if (bool) {
        return;
      }
      if (!this.leased) {
        checkExpiry();
      }
      return;
    }
    finally {}
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      Args.notNull(paramTimeUnit, "Time unit");
      boolean bool = this.isShutdown.get();
      if (bool) {
        return;
      }
      if (!this.leased)
      {
        long l = paramTimeUnit.toMillis(paramLong);
        paramLong = l;
        if (l < 0L) {
          paramLong = 0L;
        }
        l = System.currentTimeMillis();
        if (this.updated <= l - paramLong) {
          closeConnection();
        }
      }
      return;
    }
    finally {}
  }
  
  public void connect(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, int paramInt, HttpContext paramHttpContext)
    throws IOException
  {
    Args.notNull(paramHttpClientConnection, "Connection");
    Args.notNull(paramHttpRoute, "HTTP route");
    boolean bool;
    if (paramHttpClientConnection == this.conn) {
      bool = true;
    } else {
      bool = false;
    }
    Asserts.check(bool, "Connection not obtained from this manager");
    if (paramHttpRoute.getProxyHost() != null) {
      paramHttpClientConnection = paramHttpRoute.getProxyHost();
    } else {
      paramHttpClientConnection = paramHttpRoute.getTargetHost();
    }
    paramHttpRoute = paramHttpRoute.getLocalSocketAddress();
    this.connectionOperator.connect(this.conn, paramHttpClientConnection, paramHttpRoute, paramInt, this.socketConfig, paramHttpContext);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  HttpClientConnection getConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    try
    {
      boolean bool1 = this.isShutdown.get();
      boolean bool2 = false;
      if (!bool1) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      Asserts.check(bool1, "Connection manager has been shut down");
      if (this.log.isDebugEnabled())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("Get connection for route ");
        localStringBuilder.append(paramHttpRoute);
        localHttpClientAndroidLog.debug(localStringBuilder.toString());
      }
      bool1 = bool2;
      if (!this.leased) {
        bool1 = true;
      }
      Asserts.check(bool1, "Connection is still allocated");
      if ((!LangUtils.equals(this.route, paramHttpRoute)) || (!LangUtils.equals(this.state, paramObject))) {
        closeConnection();
      }
      this.route = paramHttpRoute;
      this.state = paramObject;
      checkExpiry();
      if (this.conn == null) {
        this.conn = ((ManagedHttpClientConnection)this.connFactory.create(paramHttpRoute, this.connConfig));
      }
      this.leased = true;
      paramHttpRoute = this.conn;
      return paramHttpRoute;
    }
    finally {}
  }
  
  public ConnectionConfig getConnectionConfig()
  {
    try
    {
      ConnectionConfig localConnectionConfig = this.connConfig;
      return localConnectionConfig;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  HttpRoute getRoute()
  {
    return this.route;
  }
  
  public SocketConfig getSocketConfig()
  {
    try
    {
      SocketConfig localSocketConfig = this.socketConfig;
      return localSocketConfig;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  Object getState()
  {
    return this.state;
  }
  
  /* Error */
  public void releaseConnection(HttpClientConnection paramHttpClientConnection, Object paramObject, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ldc -28
    //   5: invokestatic 216	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   8: pop
    //   9: aload_1
    //   10: aload_0
    //   11: getfield 109	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:conn	Lcz/msebera/android/httpclient/conn/ManagedHttpClientConnection;
    //   14: if_acmpne +9 -> 23
    //   17: iconst_1
    //   18: istore 6
    //   20: goto +6 -> 26
    //   23: iconst_0
    //   24: istore 6
    //   26: iload 6
    //   28: ldc -24
    //   30: invokestatic 238	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   33: aload_0
    //   34: getfield 66	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   37: invokevirtual 119	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   40: ifeq +45 -> 85
    //   43: aload_0
    //   44: getfield 66	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   47: astore 8
    //   49: new 121	java/lang/StringBuilder
    //   52: astore 7
    //   54: aload 7
    //   56: invokespecial 122	java/lang/StringBuilder:<init>	()V
    //   59: aload 7
    //   61: ldc_w 295
    //   64: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload 7
    //   70: aload_1
    //   71: invokevirtual 136	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: aload 8
    //   77: aload 7
    //   79: invokevirtual 140	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   82: invokevirtual 143	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   85: aload_0
    //   86: getfield 105	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:isShutdown	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   89: invokevirtual 202	java/util/concurrent/atomic/AtomicBoolean:get	()Z
    //   92: istore 6
    //   94: iload 6
    //   96: ifeq +6 -> 102
    //   99: aload_0
    //   100: monitorexit
    //   101: return
    //   102: aload_0
    //   103: invokestatic 115	java/lang/System:currentTimeMillis	()J
    //   106: putfield 224	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:updated	J
    //   109: aload_0
    //   110: getfield 109	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:conn	Lcz/msebera/android/httpclient/conn/ManagedHttpClientConnection;
    //   113: invokeinterface 298 1 0
    //   118: ifne +28 -> 146
    //   121: aload_0
    //   122: aconst_null
    //   123: putfield 109	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:conn	Lcz/msebera/android/httpclient/conn/ManagedHttpClientConnection;
    //   126: aload_0
    //   127: aconst_null
    //   128: putfield 270	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:route	Lcz/msebera/android/httpclient/conn/routing/HttpRoute;
    //   131: aload_0
    //   132: aconst_null
    //   133: putfield 109	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:conn	Lcz/msebera/android/httpclient/conn/ManagedHttpClientConnection;
    //   136: aload_0
    //   137: ldc2_w 82
    //   140: putfield 85	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:expiry	J
    //   143: goto +144 -> 287
    //   146: aload_0
    //   147: aload_2
    //   148: putfield 278	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:state	Ljava/lang/Object;
    //   151: aload_0
    //   152: getfield 66	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   155: invokevirtual 119	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   158: ifeq +98 -> 256
    //   161: lload_3
    //   162: lconst_0
    //   163: lcmp
    //   164: ifle +48 -> 212
    //   167: new 121	java/lang/StringBuilder
    //   170: astore_1
    //   171: aload_1
    //   172: invokespecial 122	java/lang/StringBuilder:<init>	()V
    //   175: aload_1
    //   176: ldc_w 300
    //   179: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload_1
    //   184: lload_3
    //   185: invokevirtual 303	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload_1
    //   190: ldc_w 305
    //   193: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_1
    //   198: aload 5
    //   200: invokevirtual 136	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_1
    //   205: invokevirtual 140	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   208: astore_1
    //   209: goto +7 -> 216
    //   212: ldc_w 307
    //   215: astore_1
    //   216: aload_0
    //   217: getfield 66	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   220: astore_2
    //   221: new 121	java/lang/StringBuilder
    //   224: astore 7
    //   226: aload 7
    //   228: invokespecial 122	java/lang/StringBuilder:<init>	()V
    //   231: aload 7
    //   233: ldc_w 309
    //   236: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: pop
    //   240: aload 7
    //   242: aload_1
    //   243: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload_2
    //   248: aload 7
    //   250: invokevirtual 140	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   253: invokevirtual 143	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   256: lload_3
    //   257: lconst_0
    //   258: lcmp
    //   259: ifle +21 -> 280
    //   262: aload_0
    //   263: aload_0
    //   264: getfield 224	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:updated	J
    //   267: aload 5
    //   269: lload_3
    //   270: invokevirtual 222	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   273: ladd
    //   274: putfield 85	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:expiry	J
    //   277: goto +10 -> 287
    //   280: aload_0
    //   281: ldc2_w 82
    //   284: putfield 85	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:expiry	J
    //   287: aload_0
    //   288: iconst_0
    //   289: putfield 204	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:leased	Z
    //   292: aload_0
    //   293: monitorexit
    //   294: return
    //   295: astore_1
    //   296: aload_0
    //   297: iconst_0
    //   298: putfield 204	cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager:leased	Z
    //   301: aload_1
    //   302: athrow
    //   303: astore_1
    //   304: aload_0
    //   305: monitorexit
    //   306: aload_1
    //   307: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	308	0	this	BasicHttpClientConnectionManager
    //   0	308	1	paramHttpClientConnection	HttpClientConnection
    //   0	308	2	paramObject	Object
    //   0	308	3	paramLong	long
    //   0	308	5	paramTimeUnit	TimeUnit
    //   18	77	6	bool	boolean
    //   52	197	7	localStringBuilder	StringBuilder
    //   47	29	8	localHttpClientAndroidLog	HttpClientAndroidLog
    // Exception table:
    //   from	to	target	type
    //   102	143	295	finally
    //   146	161	295	finally
    //   167	209	295	finally
    //   216	256	295	finally
    //   262	277	295	finally
    //   280	287	295	finally
    //   2	17	303	finally
    //   26	85	303	finally
    //   85	94	303	finally
    //   287	292	303	finally
    //   296	303	303	finally
  }
  
  public final ConnectionRequest requestConnection(final HttpRoute paramHttpRoute, final Object paramObject)
  {
    Args.notNull(paramHttpRoute, "Route");
    new ConnectionRequest()
    {
      public boolean cancel()
      {
        return false;
      }
      
      public HttpClientConnection get(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        return BasicHttpClientConnectionManager.this.getConnection(paramHttpRoute, paramObject);
      }
    };
  }
  
  public void routeComplete(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws IOException
  {}
  
  public void setConnectionConfig(ConnectionConfig paramConnectionConfig)
  {
    if (paramConnectionConfig == null) {}
    try
    {
      paramConnectionConfig = ConnectionConfig.DEFAULT;
      this.connConfig = paramConnectionConfig;
      return;
    }
    finally {}
  }
  
  public void setSocketConfig(SocketConfig paramSocketConfig)
  {
    if (paramSocketConfig == null) {}
    try
    {
      paramSocketConfig = SocketConfig.DEFAULT;
      this.socketConfig = paramSocketConfig;
      return;
    }
    finally {}
  }
  
  public void shutdown()
  {
    try
    {
      if (this.isShutdown.compareAndSet(false, true)) {
        shutdownConnection();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void upgrade(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws IOException
  {
    Args.notNull(paramHttpClientConnection, "Connection");
    Args.notNull(paramHttpRoute, "HTTP route");
    boolean bool;
    if (paramHttpClientConnection == this.conn) {
      bool = true;
    } else {
      bool = false;
    }
    Asserts.check(bool, "Connection not obtained from this manager");
    this.connectionOperator.upgrade(this.conn, paramHttpRoute.getTargetHost(), paramHttpContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/BasicHttpClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */