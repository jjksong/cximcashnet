package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.io.IOException;
import java.io.OutputStream;

@NotThreadSafe
class LoggingOutputStream
  extends OutputStream
{
  private final OutputStream out;
  private final Wire wire;
  
  public LoggingOutputStream(OutputStream paramOutputStream, Wire paramWire)
  {
    this.out = paramOutputStream;
    this.wire = paramWire;
  }
  
  public void close()
    throws IOException
  {
    try
    {
      this.out.close();
      return;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[close] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.output(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    try
    {
      this.out.flush();
      return;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[flush] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.output(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public void write(int paramInt)
    throws IOException
  {
    try
    {
      this.wire.output(paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[write] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      localWire.output(localStringBuilder.toString());
      throw localIOException;
    }
  }
  
  public void write(byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      this.wire.output(paramArrayOfByte);
      this.out.write(paramArrayOfByte);
      return;
    }
    catch (IOException paramArrayOfByte)
    {
      Wire localWire = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[write] I/O error: ");
      localStringBuilder.append(paramArrayOfByte.getMessage());
      localWire.output(localStringBuilder.toString());
      throw paramArrayOfByte;
    }
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    try
    {
      this.wire.output(paramArrayOfByte, paramInt1, paramInt2);
      this.out.write(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (IOException localIOException)
    {
      paramArrayOfByte = this.wire;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[write] I/O error: ");
      localStringBuilder.append(localIOException.getMessage());
      paramArrayOfByte.output(localStringBuilder.toString());
      throw localIOException;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/LoggingOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */