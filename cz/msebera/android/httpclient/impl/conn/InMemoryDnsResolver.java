package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryDnsResolver
  implements DnsResolver
{
  private final Map<String, InetAddress[]> dnsMap = new ConcurrentHashMap();
  public HttpClientAndroidLog log = new HttpClientAndroidLog(InMemoryDnsResolver.class);
  
  public void add(String paramString, InetAddress... paramVarArgs)
  {
    Args.notNull(paramString, "Host name");
    Args.notNull(paramVarArgs, "Array of IP addresses");
    this.dnsMap.put(paramString, paramVarArgs);
  }
  
  public InetAddress[] resolve(String paramString)
    throws UnknownHostException
  {
    Object localObject = (InetAddress[])this.dnsMap.get(paramString);
    if (this.log.isInfoEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Resolving ");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" to ");
      localStringBuilder.append(Arrays.deepToString((Object[])localObject));
      localHttpClientAndroidLog.info(localStringBuilder.toString());
    }
    if (localObject != null) {
      return (InetAddress[])localObject;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(" cannot be resolved");
    throw new UnknownHostException(((StringBuilder)localObject).toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/InMemoryDnsResolver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */