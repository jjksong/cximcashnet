package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.conn.HttpConnectionFactory;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.io.DefaultHttpRequestWriterFactory;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.concurrent.atomic.AtomicLong;

@Immutable
public class ManagedHttpClientConnectionFactory
  implements HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection>
{
  private static final AtomicLong COUNTER = new AtomicLong();
  public static final ManagedHttpClientConnectionFactory INSTANCE = new ManagedHttpClientConnectionFactory();
  public HttpClientAndroidLog headerlog = new HttpClientAndroidLog("cz.msebera.android.httpclient.headers");
  public HttpClientAndroidLog log = new HttpClientAndroidLog(DefaultManagedHttpClientConnection.class);
  private final HttpMessageWriterFactory<HttpRequest> requestWriterFactory;
  private final HttpMessageParserFactory<HttpResponse> responseParserFactory;
  public HttpClientAndroidLog wirelog = new HttpClientAndroidLog("cz.msebera.android.httpclient.wire");
  
  public ManagedHttpClientConnectionFactory()
  {
    this(null, null);
  }
  
  public ManagedHttpClientConnectionFactory(HttpMessageParserFactory<HttpResponse> paramHttpMessageParserFactory)
  {
    this(null, paramHttpMessageParserFactory);
  }
  
  public ManagedHttpClientConnectionFactory(HttpMessageWriterFactory<HttpRequest> paramHttpMessageWriterFactory, HttpMessageParserFactory<HttpResponse> paramHttpMessageParserFactory)
  {
    if (paramHttpMessageWriterFactory == null) {
      paramHttpMessageWriterFactory = DefaultHttpRequestWriterFactory.INSTANCE;
    }
    this.requestWriterFactory = paramHttpMessageWriterFactory;
    if (paramHttpMessageParserFactory == null) {
      paramHttpMessageParserFactory = DefaultHttpResponseParserFactory.INSTANCE;
    }
    this.responseParserFactory = paramHttpMessageParserFactory;
  }
  
  public ManagedHttpClientConnection create(HttpRoute paramHttpRoute, ConnectionConfig paramConnectionConfig)
  {
    if (paramConnectionConfig != null) {
      paramHttpRoute = paramConnectionConfig;
    } else {
      paramHttpRoute = ConnectionConfig.DEFAULT;
    }
    Object localObject3 = paramHttpRoute.getCharset();
    if (paramHttpRoute.getMalformedInputAction() != null) {
      paramConnectionConfig = paramHttpRoute.getMalformedInputAction();
    } else {
      paramConnectionConfig = CodingErrorAction.REPORT;
    }
    Object localObject1;
    if (paramHttpRoute.getUnmappableInputAction() != null) {
      localObject1 = paramHttpRoute.getUnmappableInputAction();
    } else {
      localObject1 = CodingErrorAction.REPORT;
    }
    if (localObject3 != null)
    {
      localObject2 = ((Charset)localObject3).newDecoder();
      ((CharsetDecoder)localObject2).onMalformedInput(paramConnectionConfig);
      ((CharsetDecoder)localObject2).onUnmappableCharacter((CodingErrorAction)localObject1);
      localObject3 = ((Charset)localObject3).newEncoder();
      ((CharsetEncoder)localObject3).onMalformedInput(paramConnectionConfig);
      ((CharsetEncoder)localObject3).onUnmappableCharacter((CodingErrorAction)localObject1);
      localObject1 = localObject3;
      paramConnectionConfig = (ConnectionConfig)localObject2;
    }
    else
    {
      paramConnectionConfig = null;
      localObject1 = paramConnectionConfig;
    }
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("http-outgoing-");
    ((StringBuilder)localObject2).append(Long.toString(COUNTER.getAndIncrement()));
    return new LoggingManagedHttpClientConnection(((StringBuilder)localObject2).toString(), this.log, this.headerlog, this.wirelog, paramHttpRoute.getBufferSize(), paramHttpRoute.getFragmentSizeHint(), paramConnectionConfig, (CharsetEncoder)localObject1, paramHttpRoute.getMessageConstraints(), null, null, this.requestWriterFactory, this.responseParserFactory);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/ManagedHttpClientConnectionFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */