package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Immutable
public class SystemDefaultRoutePlanner
  extends DefaultRoutePlanner
{
  private final ProxySelector proxySelector;
  
  public SystemDefaultRoutePlanner(SchemePortResolver paramSchemePortResolver, ProxySelector paramProxySelector)
  {
    super(paramSchemePortResolver);
    if (paramProxySelector == null) {
      paramProxySelector = ProxySelector.getDefault();
    }
    this.proxySelector = paramProxySelector;
  }
  
  public SystemDefaultRoutePlanner(ProxySelector paramProxySelector)
  {
    this(null, paramProxySelector);
  }
  
  private Proxy chooseProxy(List<Proxy> paramList)
  {
    Object localObject = null;
    for (int i = 0; (localObject == null) && (i < paramList.size()); i++)
    {
      Proxy localProxy = (Proxy)paramList.get(i);
      switch (localProxy.type())
      {
      default: 
        break;
      case ???: 
      case ???: 
        localObject = localProxy;
      }
    }
    paramList = (List<Proxy>)localObject;
    if (localObject == null) {
      paramList = Proxy.NO_PROXY;
    }
    return paramList;
  }
  
  private String getHost(InetSocketAddress paramInetSocketAddress)
  {
    if (paramInetSocketAddress.isUnresolved()) {
      paramInetSocketAddress = paramInetSocketAddress.getHostName();
    } else {
      paramInetSocketAddress = paramInetSocketAddress.getAddress().getHostAddress();
    }
    return paramInetSocketAddress;
  }
  
  protected HttpHost determineProxy(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    try
    {
      paramHttpRequest = new URI(paramHttpHost.toURI());
      paramHttpRequest = chooseProxy(this.proxySelector.select(paramHttpRequest));
      paramHttpHost = null;
      if (paramHttpRequest.type() == Proxy.Type.HTTP) {
        if ((paramHttpRequest.address() instanceof InetSocketAddress))
        {
          paramHttpHost = (InetSocketAddress)paramHttpRequest.address();
          paramHttpHost = new HttpHost(getHost(paramHttpHost), paramHttpHost.getPort());
        }
        else
        {
          paramHttpHost = new StringBuilder();
          paramHttpHost.append("Unable to handle non-Inet proxy address: ");
          paramHttpHost.append(paramHttpRequest.address());
          throw new HttpException(paramHttpHost.toString());
        }
      }
      return paramHttpHost;
    }
    catch (URISyntaxException paramHttpContext)
    {
      paramHttpRequest = new StringBuilder();
      paramHttpRequest.append("Cannot convert host to URI: ");
      paramHttpRequest.append(paramHttpHost);
      throw new HttpException(paramHttpRequest.toString(), paramHttpContext);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/SystemDefaultRoutePlanner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */