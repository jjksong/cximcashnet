package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.params.ConnRouteParams;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Deprecated
@NotThreadSafe
public class ProxySelectorRoutePlanner
  implements HttpRoutePlanner
{
  protected ProxySelector proxySelector;
  protected final SchemeRegistry schemeRegistry;
  
  public ProxySelectorRoutePlanner(SchemeRegistry paramSchemeRegistry, ProxySelector paramProxySelector)
  {
    Args.notNull(paramSchemeRegistry, "SchemeRegistry");
    this.schemeRegistry = paramSchemeRegistry;
    this.proxySelector = paramProxySelector;
  }
  
  protected Proxy chooseProxy(List<Proxy> paramList, HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
  {
    Args.notEmpty(paramList, "List of proxies");
    paramHttpHost = null;
    for (int i = 0; (paramHttpHost == null) && (i < paramList.size()); i++)
    {
      paramHttpRequest = (Proxy)paramList.get(i);
      switch (paramHttpRequest.type())
      {
      default: 
        break;
      case ???: 
      case ???: 
        paramHttpHost = paramHttpRequest;
      }
    }
    paramList = paramHttpHost;
    if (paramHttpHost == null) {
      paramList = Proxy.NO_PROXY;
    }
    return paramList;
  }
  
  protected HttpHost determineProxy(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    ProxySelector localProxySelector2 = this.proxySelector;
    ProxySelector localProxySelector1 = localProxySelector2;
    if (localProxySelector2 == null) {
      localProxySelector1 = ProxySelector.getDefault();
    }
    localProxySelector2 = null;
    if (localProxySelector1 == null) {
      return null;
    }
    try
    {
      URI localURI = new URI(paramHttpHost.toURI());
      paramHttpRequest = chooseProxy(localProxySelector1.select(localURI), paramHttpHost, paramHttpRequest, paramHttpContext);
      paramHttpHost = localProxySelector2;
      if (paramHttpRequest.type() == Proxy.Type.HTTP) {
        if ((paramHttpRequest.address() instanceof InetSocketAddress))
        {
          paramHttpHost = (InetSocketAddress)paramHttpRequest.address();
          paramHttpHost = new HttpHost(getHost(paramHttpHost), paramHttpHost.getPort());
        }
        else
        {
          paramHttpHost = new StringBuilder();
          paramHttpHost.append("Unable to handle non-Inet proxy address: ");
          paramHttpHost.append(paramHttpRequest.address());
          throw new HttpException(paramHttpHost.toString());
        }
      }
      return paramHttpHost;
    }
    catch (URISyntaxException paramHttpRequest)
    {
      paramHttpContext = new StringBuilder();
      paramHttpContext.append("Cannot convert host to URI: ");
      paramHttpContext.append(paramHttpHost);
      throw new HttpException(paramHttpContext.toString(), paramHttpRequest);
    }
  }
  
  public HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Object localObject = ConnRouteParams.getForcedRoute(paramHttpRequest.getParams());
    if (localObject != null) {
      return (HttpRoute)localObject;
    }
    Asserts.notNull(paramHttpHost, "Target host");
    localObject = ConnRouteParams.getLocalAddress(paramHttpRequest.getParams());
    paramHttpRequest = determineProxy(paramHttpHost, paramHttpRequest, paramHttpContext);
    boolean bool = this.schemeRegistry.getScheme(paramHttpHost.getSchemeName()).isLayered();
    if (paramHttpRequest == null) {
      paramHttpHost = new HttpRoute(paramHttpHost, (InetAddress)localObject, bool);
    } else {
      paramHttpHost = new HttpRoute(paramHttpHost, (InetAddress)localObject, paramHttpRequest, bool);
    }
    return paramHttpHost;
  }
  
  protected String getHost(InetSocketAddress paramInetSocketAddress)
  {
    if (paramInetSocketAddress.isUnresolved()) {
      paramInetSocketAddress = paramInetSocketAddress.getHostName();
    } else {
      paramInetSocketAddress = paramInetSocketAddress.getAddress().getHostAddress();
    }
    return paramInetSocketAddress;
  }
  
  public ProxySelector getProxySelector()
  {
    return this.proxySelector;
  }
  
  public void setProxySelector(ProxySelector paramProxySelector)
  {
    this.proxySelector = paramProxySelector;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/ProxySelectorRoutePlanner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */