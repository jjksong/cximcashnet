package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.AbstractConnPool;
import cz.msebera.android.httpclient.pool.ConnFactory;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Deprecated
class HttpConnPool
  extends AbstractConnPool<HttpRoute, OperatedClientConnection, HttpPoolEntry>
{
  private static final AtomicLong COUNTER = new AtomicLong();
  public HttpClientAndroidLog log;
  private final long timeToLive;
  private final TimeUnit tunit;
  
  public HttpConnPool(HttpClientAndroidLog paramHttpClientAndroidLog, ClientConnectionOperator paramClientConnectionOperator, int paramInt1, int paramInt2, long paramLong, TimeUnit paramTimeUnit)
  {
    super(new InternalConnFactory(paramClientConnectionOperator), paramInt1, paramInt2);
    this.log = paramHttpClientAndroidLog;
    this.timeToLive = paramLong;
    this.tunit = paramTimeUnit;
  }
  
  protected HttpPoolEntry createEntry(HttpRoute paramHttpRoute, OperatedClientConnection paramOperatedClientConnection)
  {
    String str = Long.toString(COUNTER.getAndIncrement());
    return new HttpPoolEntry(this.log, str, paramHttpRoute, paramOperatedClientConnection, this.timeToLive, this.tunit);
  }
  
  static class InternalConnFactory
    implements ConnFactory<HttpRoute, OperatedClientConnection>
  {
    private final ClientConnectionOperator connOperator;
    
    InternalConnFactory(ClientConnectionOperator paramClientConnectionOperator)
    {
      this.connOperator = paramClientConnectionOperator;
    }
    
    public OperatedClientConnection create(HttpRoute paramHttpRoute)
      throws IOException
    {
      return this.connOperator.createConnection();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/HttpConnPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */