package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Deprecated
@ThreadSafe
public class SingleClientConnManager
  implements ClientConnectionManager
{
  public static final String MISUSE_MESSAGE = "Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";
  protected final boolean alwaysShutDown;
  protected final ClientConnectionOperator connOperator;
  @GuardedBy("this")
  protected volatile long connectionExpiresTime;
  protected volatile boolean isShutDown;
  @GuardedBy("this")
  protected volatile long lastReleaseTime;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  protected volatile ConnAdapter managedConn;
  protected final SchemeRegistry schemeRegistry;
  @GuardedBy("this")
  protected volatile PoolEntry uniquePoolEntry;
  
  public SingleClientConnManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public SingleClientConnManager(SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.schemeRegistry = paramSchemeRegistry;
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
    this.uniquePoolEntry = new PoolEntry();
    this.managedConn = null;
    this.lastReleaseTime = -1L;
    this.alwaysShutDown = false;
    this.isShutDown = false;
  }
  
  @Deprecated
  public SingleClientConnManager(HttpParams paramHttpParams, SchemeRegistry paramSchemeRegistry)
  {
    this(paramSchemeRegistry);
  }
  
  protected final void assertStillUp()
    throws IllegalStateException
  {
    Asserts.check(this.isShutDown ^ true, "Manager is shut down");
  }
  
  public void closeExpiredConnections()
  {
    long l = this.connectionExpiresTime;
    if (System.currentTimeMillis() >= l) {
      closeIdleConnections(0L, TimeUnit.MILLISECONDS);
    }
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    assertStillUp();
    Args.notNull(paramTimeUnit, "Time unit");
    try
    {
      if ((this.managedConn == null) && (this.uniquePoolEntry.connection.isOpen()))
      {
        long l1 = System.currentTimeMillis();
        long l2 = paramTimeUnit.toMillis(paramLong);
        paramLong = this.lastReleaseTime;
        if (paramLong <= l1 - l2) {
          try
          {
            this.uniquePoolEntry.close();
          }
          catch (IOException paramTimeUnit)
          {
            this.log.debug("Problem closing idle connection.", paramTimeUnit);
          }
        }
      }
      return;
    }
    finally {}
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  public ManagedClientConnection getConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "Route");
    assertStillUp();
    if (this.log.isDebugEnabled())
    {
      paramObject = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Get connection for route ");
      localStringBuilder.append(paramHttpRoute);
      ((HttpClientAndroidLog)paramObject).debug(localStringBuilder.toString());
    }
    try
    {
      paramObject = this.managedConn;
      int k = 1;
      int i = 0;
      boolean bool;
      if (paramObject == null) {
        bool = true;
      } else {
        bool = false;
      }
      Asserts.check(bool, "Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
      closeExpiredConnections();
      int j;
      if (this.uniquePoolEntry.connection.isOpen())
      {
        paramObject = this.uniquePoolEntry.tracker;
        if (paramObject != null)
        {
          bool = ((RouteTracker)paramObject).toRoute().equals(paramHttpRoute);
          if (bool)
          {
            j = 0;
            break label157;
          }
        }
        j = 1;
      }
      else
      {
        j = 0;
        i = 1;
      }
      label157:
      if (j != 0) {
        try
        {
          this.uniquePoolEntry.shutdown();
          i = k;
        }
        catch (IOException paramObject)
        {
          this.log.debug("Problem shutting down connection.", (Throwable)paramObject);
          i = k;
        }
      }
      if (i != 0)
      {
        paramObject = new cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
        ((PoolEntry)paramObject).<init>(this);
        this.uniquePoolEntry = ((PoolEntry)paramObject);
      }
      paramObject = new cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
      ((ConnAdapter)paramObject).<init>(this, this.uniquePoolEntry, paramHttpRoute);
      this.managedConn = ((ConnAdapter)paramObject);
      paramHttpRoute = this.managedConn;
      return paramHttpRoute;
    }
    finally {}
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  /* Error */
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 10
    //   4: ldc -34
    //   6: invokestatic 223	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   9: aload_0
    //   10: invokevirtual 132	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:assertStillUp	()V
    //   13: aload_0
    //   14: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   17: invokevirtual 175	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   20: ifeq +43 -> 63
    //   23: aload_0
    //   24: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   27: astore 6
    //   29: new 177	java/lang/StringBuilder
    //   32: dup
    //   33: invokespecial 178	java/lang/StringBuilder:<init>	()V
    //   36: astore 7
    //   38: aload 7
    //   40: ldc -31
    //   42: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 7
    //   48: aload_1
    //   49: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: aload 6
    //   55: aload 7
    //   57: invokevirtual 191	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokevirtual 193	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   63: aload_1
    //   64: checkcast 10	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter
    //   67: astore_1
    //   68: aload_1
    //   69: monitorenter
    //   70: aload_1
    //   71: getfield 229	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:poolEntry	Lcz/msebera/android/httpclient/impl/conn/AbstractPoolEntry;
    //   74: ifnonnull +6 -> 80
    //   77: aload_1
    //   78: monitorexit
    //   79: return
    //   80: aload_1
    //   81: invokevirtual 233	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:getManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   84: aload_0
    //   85: if_acmpne +9 -> 94
    //   88: iconst_1
    //   89: istore 5
    //   91: goto +6 -> 97
    //   94: iconst_0
    //   95: istore 5
    //   97: iload 5
    //   99: ldc -21
    //   101: invokestatic 108	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   104: aload_1
    //   105: invokevirtual 236	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:isOpen	()Z
    //   108: ifeq +40 -> 148
    //   111: aload_0
    //   112: getfield 93	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:alwaysShutDown	Z
    //   115: ifne +10 -> 125
    //   118: aload_1
    //   119: invokevirtual 239	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:isMarkedReusable	()Z
    //   122: ifne +26 -> 148
    //   125: aload_0
    //   126: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   129: invokevirtual 175	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   132: ifeq +12 -> 144
    //   135: aload_0
    //   136: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   139: ldc -15
    //   141: invokevirtual 193	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   144: aload_1
    //   145: invokevirtual 242	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:shutdown	()V
    //   148: aload_1
    //   149: invokevirtual 245	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:detach	()V
    //   152: aload_0
    //   153: monitorenter
    //   154: aload_0
    //   155: aconst_null
    //   156: putfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   159: aload_0
    //   160: invokestatic 118	java/lang/System:currentTimeMillis	()J
    //   163: putfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   166: lload_2
    //   167: lconst_0
    //   168: lcmp
    //   169: ifle +21 -> 190
    //   172: aload_0
    //   173: aload 4
    //   175: lload_2
    //   176: invokevirtual 148	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   179: aload_0
    //   180: getfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   183: ladd
    //   184: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   187: goto +10 -> 197
    //   190: aload_0
    //   191: ldc2_w 246
    //   194: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   197: aload_0
    //   198: monitorexit
    //   199: goto +89 -> 288
    //   202: astore 4
    //   204: aload_0
    //   205: monitorexit
    //   206: aload 4
    //   208: athrow
    //   209: astore 6
    //   211: goto +87 -> 298
    //   214: astore 6
    //   216: aload_0
    //   217: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   220: invokevirtual 175	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   223: ifeq +14 -> 237
    //   226: aload_0
    //   227: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   230: ldc -7
    //   232: aload 6
    //   234: invokevirtual 157	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   237: aload_1
    //   238: invokevirtual 245	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:detach	()V
    //   241: aload_0
    //   242: monitorenter
    //   243: aload_0
    //   244: aconst_null
    //   245: putfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   248: aload_0
    //   249: invokestatic 118	java/lang/System:currentTimeMillis	()J
    //   252: putfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   255: lload_2
    //   256: lconst_0
    //   257: lcmp
    //   258: ifle +21 -> 279
    //   261: aload_0
    //   262: aload 4
    //   264: lload_2
    //   265: invokevirtual 148	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   268: aload_0
    //   269: getfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   272: ladd
    //   273: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   276: goto +10 -> 286
    //   279: aload_0
    //   280: ldc2_w 246
    //   283: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   286: aload_0
    //   287: monitorexit
    //   288: aload_1
    //   289: monitorexit
    //   290: return
    //   291: astore 4
    //   293: aload_0
    //   294: monitorexit
    //   295: aload 4
    //   297: athrow
    //   298: aload_1
    //   299: invokevirtual 245	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:detach	()V
    //   302: aload_0
    //   303: monitorenter
    //   304: aload_0
    //   305: aconst_null
    //   306: putfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   309: aload_0
    //   310: invokestatic 118	java/lang/System:currentTimeMillis	()J
    //   313: putfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   316: lload_2
    //   317: lconst_0
    //   318: lcmp
    //   319: ifle +21 -> 340
    //   322: aload_0
    //   323: aload 4
    //   325: lload_2
    //   326: invokevirtual 148	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   329: aload_0
    //   330: getfield 91	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:lastReleaseTime	J
    //   333: ladd
    //   334: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   337: goto +10 -> 347
    //   340: aload_0
    //   341: ldc2_w 246
    //   344: putfield 112	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:connectionExpiresTime	J
    //   347: aload_0
    //   348: monitorexit
    //   349: aload 6
    //   351: athrow
    //   352: astore 4
    //   354: aload_0
    //   355: monitorexit
    //   356: aload 4
    //   358: athrow
    //   359: astore 4
    //   361: aload_1
    //   362: monitorexit
    //   363: aload 4
    //   365: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	366	0	this	SingleClientConnManager
    //   0	366	1	paramManagedClientConnection	ManagedClientConnection
    //   0	366	2	paramLong	long
    //   0	366	4	paramTimeUnit	TimeUnit
    //   89	9	5	bool	boolean
    //   27	27	6	localHttpClientAndroidLog	HttpClientAndroidLog
    //   209	1	6	localObject	Object
    //   214	136	6	localIOException	IOException
    //   36	20	7	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   154	166	202	finally
    //   172	187	202	finally
    //   190	197	202	finally
    //   197	199	202	finally
    //   204	206	202	finally
    //   104	125	209	finally
    //   125	144	209	finally
    //   144	148	209	finally
    //   216	237	209	finally
    //   104	125	214	java/io/IOException
    //   125	144	214	java/io/IOException
    //   144	148	214	java/io/IOException
    //   243	255	291	finally
    //   261	276	291	finally
    //   279	286	291	finally
    //   286	288	291	finally
    //   293	295	291	finally
    //   304	316	352	finally
    //   322	337	352	finally
    //   340	347	352	finally
    //   347	349	352	finally
    //   354	356	352	finally
    //   70	79	359	finally
    //   80	88	359	finally
    //   97	104	359	finally
    //   148	154	359	finally
    //   206	209	359	finally
    //   237	243	359	finally
    //   288	290	359	finally
    //   295	298	359	finally
    //   298	304	359	finally
    //   349	352	359	finally
    //   356	359	359	finally
    //   361	363	359	finally
  }
  
  public final ClientConnectionRequest requestConnection(final HttpRoute paramHttpRoute, final Object paramObject)
  {
    new ClientConnectionRequest()
    {
      public void abortRequest() {}
      
      public ManagedClientConnection getConnection(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        return SingleClientConnManager.this.getConnection(paramHttpRoute, paramObject);
      }
    };
  }
  
  /* Error */
  protected void revokeConnection()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnonnull +4 -> 10
    //   9: return
    //   10: aload_1
    //   11: invokevirtual 245	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter:detach	()V
    //   14: aload_0
    //   15: monitorenter
    //   16: aload_0
    //   17: getfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   20: invokevirtual 212	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry:shutdown	()V
    //   23: goto +19 -> 42
    //   26: astore_1
    //   27: goto +18 -> 45
    //   30: astore_1
    //   31: aload_0
    //   32: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   35: ldc_w 257
    //   38: aload_1
    //   39: invokevirtual 157	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: aload_0
    //   46: monitorexit
    //   47: aload_1
    //   48: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	49	0	this	SingleClientConnManager
    //   4	7	1	localConnAdapter	ConnAdapter
    //   26	1	1	localObject	Object
    //   30	18	1	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   16	23	26	finally
    //   31	42	26	finally
    //   42	44	26	finally
    //   45	47	26	finally
    //   16	23	30	java/io/IOException
  }
  
  /* Error */
  public void shutdown()
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: putfield 95	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:isShutDown	Z
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   11: ifnull +10 -> 21
    //   14: aload_0
    //   15: getfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   18: invokevirtual 212	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry:shutdown	()V
    //   21: aload_0
    //   22: aconst_null
    //   23: putfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   26: aload_0
    //   27: aconst_null
    //   28: putfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   31: goto +27 -> 58
    //   34: astore_1
    //   35: goto +26 -> 61
    //   38: astore_1
    //   39: aload_0
    //   40: getfield 64	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   43: ldc_w 259
    //   46: aload_1
    //   47: invokevirtual 157	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   50: aload_0
    //   51: aconst_null
    //   52: putfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   55: goto -29 -> 26
    //   58: aload_0
    //   59: monitorexit
    //   60: return
    //   61: aload_0
    //   62: aconst_null
    //   63: putfield 85	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:uniquePoolEntry	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$PoolEntry;
    //   66: aload_0
    //   67: aconst_null
    //   68: putfield 87	cz/msebera/android/httpclient/impl/conn/SingleClientConnManager:managedConn	Lcz/msebera/android/httpclient/impl/conn/SingleClientConnManager$ConnAdapter;
    //   71: aload_1
    //   72: athrow
    //   73: astore_1
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	78	0	this	SingleClientConnManager
    //   34	1	1	localObject1	Object
    //   38	34	1	localIOException	IOException
    //   73	4	1	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   7	21	34	finally
    //   39	50	34	finally
    //   7	21	38	java/io/IOException
    //   21	26	73	finally
    //   26	31	73	finally
    //   50	55	73	finally
    //   58	60	73	finally
    //   61	73	73	finally
    //   74	76	73	finally
  }
  
  protected class ConnAdapter
    extends AbstractPooledConnAdapter
  {
    protected ConnAdapter(SingleClientConnManager.PoolEntry paramPoolEntry, HttpRoute paramHttpRoute)
    {
      super(paramPoolEntry);
      markReusable();
      paramPoolEntry.route = paramHttpRoute;
    }
  }
  
  protected class PoolEntry
    extends AbstractPoolEntry
  {
    protected PoolEntry()
    {
      super(null);
    }
    
    protected void close()
      throws IOException
    {
      shutdownEntry();
      if (this.connection.isOpen()) {
        this.connection.close();
      }
    }
    
    protected void shutdown()
      throws IOException
    {
      shutdownEntry();
      if (this.connection.isOpen()) {
        this.connection.shutdown();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/SingleClientConnManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */