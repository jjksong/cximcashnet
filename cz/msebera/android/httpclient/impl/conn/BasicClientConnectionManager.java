package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Deprecated
@ThreadSafe
public class BasicClientConnectionManager
  implements ClientConnectionManager
{
  private static final AtomicLong COUNTER = new AtomicLong();
  public static final String MISUSE_MESSAGE = "Invalid use of BasicClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";
  @GuardedBy("this")
  private ManagedClientConnectionImpl conn;
  private final ClientConnectionOperator connOperator;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  private HttpPoolEntry poolEntry;
  private final SchemeRegistry schemeRegistry;
  @GuardedBy("this")
  private volatile boolean shutdown;
  
  public BasicClientConnectionManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public BasicClientConnectionManager(SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.schemeRegistry = paramSchemeRegistry;
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
  }
  
  private void assertNotShutdown()
  {
    Asserts.check(this.shutdown ^ true, "Connection manager has been shut down");
  }
  
  private void shutdownConnection(HttpClientConnection paramHttpClientConnection)
  {
    try
    {
      paramHttpClientConnection.shutdown();
    }
    catch (IOException paramHttpClientConnection)
    {
      if (this.log.isDebugEnabled()) {
        this.log.debug("I/O exception shutting down connection", paramHttpClientConnection);
      }
    }
  }
  
  public void closeExpiredConnections()
  {
    try
    {
      assertNotShutdown();
      long l = System.currentTimeMillis();
      if ((this.poolEntry != null) && (this.poolEntry.isExpired(l)))
      {
        this.poolEntry.close();
        this.poolEntry.getTracker().reset();
      }
      return;
    }
    finally {}
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramTimeUnit, "Time unit");
    try
    {
      assertNotShutdown();
      long l = paramTimeUnit.toMillis(paramLong);
      paramLong = l;
      if (l < 0L) {
        paramLong = 0L;
      }
      l = System.currentTimeMillis();
      if ((this.poolEntry != null) && (this.poolEntry.getUpdated() <= l - paramLong))
      {
        this.poolEntry.close();
        this.poolEntry.getTracker().reset();
      }
      return;
    }
    finally {}
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  ManagedClientConnection getConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "Route");
    try
    {
      assertNotShutdown();
      Object localObject;
      if (this.log.isDebugEnabled())
      {
        localObject = this.log;
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>();
        ((StringBuilder)paramObject).append("Get connection for route ");
        ((StringBuilder)paramObject).append(paramHttpRoute);
        ((HttpClientAndroidLog)localObject).debug(((StringBuilder)paramObject).toString());
      }
      boolean bool;
      if (this.conn == null) {
        bool = true;
      } else {
        bool = false;
      }
      Asserts.check(bool, "Invalid use of BasicClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
      if ((this.poolEntry != null) && (!this.poolEntry.getPlannedRoute().equals(paramHttpRoute)))
      {
        this.poolEntry.close();
        this.poolEntry = null;
      }
      if (this.poolEntry == null)
      {
        String str = Long.toString(COUNTER.getAndIncrement());
        paramObject = this.connOperator.createConnection();
        localObject = new cz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
        ((HttpPoolEntry)localObject).<init>(this.log, str, paramHttpRoute, (OperatedClientConnection)paramObject, 0L, TimeUnit.MILLISECONDS);
        this.poolEntry = ((HttpPoolEntry)localObject);
      }
      long l = System.currentTimeMillis();
      if (this.poolEntry.isExpired(l))
      {
        this.poolEntry.close();
        this.poolEntry.getTracker().reset();
      }
      paramHttpRoute = new cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
      paramHttpRoute.<init>(this, this.connOperator, this.poolEntry);
      this.conn = paramHttpRoute;
      paramHttpRoute = this.conn;
      return paramHttpRoute;
    }
    finally {}
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    Args.check(paramManagedClientConnection instanceof ManagedClientConnectionImpl, "Connection class mismatch, connection not obtained from this manager");
    synchronized ((ManagedClientConnectionImpl)paramManagedClientConnection)
    {
      Object localObject;
      if (this.log.isDebugEnabled())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("Releasing connection ");
        ((StringBuilder)localObject).append(paramManagedClientConnection);
        localHttpClientAndroidLog.debug(((StringBuilder)localObject).toString());
      }
      if (???.getPoolEntry() == null) {
        return;
      }
      boolean bool;
      if (???.getManager() == this) {
        bool = true;
      } else {
        bool = false;
      }
      Asserts.check(bool, "Connection not obtained from this manager");
      try
      {
        if (this.shutdown)
        {
          shutdownConnection(???);
          return;
        }
        try
        {
          if ((???.isOpen()) && (!???.isMarkedReusable())) {
            shutdownConnection(???);
          }
          if (???.isMarkedReusable())
          {
            localObject = this.poolEntry;
            if (paramTimeUnit != null) {
              paramManagedClientConnection = paramTimeUnit;
            } else {
              paramManagedClientConnection = TimeUnit.MILLISECONDS;
            }
            ((HttpPoolEntry)localObject).updateExpiry(paramLong, paramManagedClientConnection);
            if (this.log.isDebugEnabled())
            {
              if (paramLong > 0L)
              {
                paramManagedClientConnection = new java/lang/StringBuilder;
                paramManagedClientConnection.<init>();
                paramManagedClientConnection.append("for ");
                paramManagedClientConnection.append(paramLong);
                paramManagedClientConnection.append(" ");
                paramManagedClientConnection.append(paramTimeUnit);
                paramManagedClientConnection = paramManagedClientConnection.toString();
              }
              else
              {
                paramManagedClientConnection = "indefinitely";
              }
              localObject = this.log;
              paramTimeUnit = new java/lang/StringBuilder;
              paramTimeUnit.<init>();
              paramTimeUnit.append("Connection can be kept alive ");
              paramTimeUnit.append(paramManagedClientConnection);
              ((HttpClientAndroidLog)localObject).debug(paramTimeUnit.toString());
            }
          }
          ???.detach();
          this.conn = null;
          if (this.poolEntry.isClosed()) {
            this.poolEntry = null;
          }
          return;
        }
        finally
        {
          ???.detach();
          this.conn = null;
          if (this.poolEntry.isClosed()) {
            this.poolEntry = null;
          }
        }
        paramManagedClientConnection = finally;
      }
      finally {}
    }
  }
  
  public final ClientConnectionRequest requestConnection(final HttpRoute paramHttpRoute, final Object paramObject)
  {
    new ClientConnectionRequest()
    {
      public void abortRequest() {}
      
      public ManagedClientConnection getConnection(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        return BasicClientConnectionManager.this.getConnection(paramHttpRoute, paramObject);
      }
    };
  }
  
  /* Error */
  public void shutdown()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield 81	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:shutdown	Z
    //   7: aload_0
    //   8: getfield 118	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   11: ifnull +10 -> 21
    //   14: aload_0
    //   15: getfield 118	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   18: invokevirtual 127	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:close	()V
    //   21: aload_0
    //   22: aconst_null
    //   23: putfield 118	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   26: aload_0
    //   27: aconst_null
    //   28: putfield 183	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield 118	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   40: aload_0
    //   41: aconst_null
    //   42: putfield 183	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   45: aload_1
    //   46: athrow
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	52	0	this	BasicClientConnectionManager
    //   34	12	1	localObject1	Object
    //   47	4	1	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   7	21	34	finally
    //   2	7	47	finally
    //   21	33	47	finally
    //   35	47	47	finally
    //   48	50	47	finally
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */