package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.params.ConnManagerParams;
import cz.msebera.android.httpclient.conn.params.ConnPerRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

@Deprecated
public class ConnPoolByRoute
  extends AbstractConnPool
{
  protected final ConnPerRoute connPerRoute;
  private final long connTTL;
  private final TimeUnit connTTLTimeUnit;
  protected final Queue<BasicPoolEntry> freeConnections;
  protected final Set<BasicPoolEntry> leasedConnections;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  protected volatile int maxTotalConnections;
  protected volatile int numConnections;
  protected final ClientConnectionOperator operator;
  private final Lock poolLock;
  protected final Map<HttpRoute, RouteSpecificPool> routeToPool;
  protected volatile boolean shutdown;
  protected final Queue<WaitingThread> waitingThreads;
  
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, ConnPerRoute paramConnPerRoute, int paramInt)
  {
    this(paramClientConnectionOperator, paramConnPerRoute, paramInt, -1L, TimeUnit.MILLISECONDS);
  }
  
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, ConnPerRoute paramConnPerRoute, int paramInt, long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramClientConnectionOperator, "Connection operator");
    Args.notNull(paramConnPerRoute, "Connections per route");
    this.poolLock = this.poolLock;
    this.leasedConnections = this.leasedConnections;
    this.operator = paramClientConnectionOperator;
    this.connPerRoute = paramConnPerRoute;
    this.maxTotalConnections = paramInt;
    this.freeConnections = createFreeConnQueue();
    this.waitingThreads = createWaitingThreadQueue();
    this.routeToPool = createRouteToPoolMap();
    this.connTTL = paramLong;
    this.connTTLTimeUnit = paramTimeUnit;
  }
  
  @Deprecated
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, HttpParams paramHttpParams)
  {
    this(paramClientConnectionOperator, ConnManagerParams.getMaxConnectionsPerRoute(paramHttpParams), ConnManagerParams.getMaxTotalConnections(paramHttpParams));
  }
  
  private void closeConnection(BasicPoolEntry paramBasicPoolEntry)
  {
    paramBasicPoolEntry = paramBasicPoolEntry.getConnection();
    if (paramBasicPoolEntry != null) {
      try
      {
        paramBasicPoolEntry.close();
      }
      catch (IOException paramBasicPoolEntry)
      {
        this.log.debug("I/O error closing connection", paramBasicPoolEntry);
      }
    }
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    long l = System.currentTimeMillis();
    this.poolLock.lock();
    try
    {
      Iterator localIterator = this.freeConnections.iterator();
      while (localIterator.hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)localIterator.next();
        if (localBasicPoolEntry.isExpired(l))
        {
          if (this.log.isDebugEnabled())
          {
            HttpClientAndroidLog localHttpClientAndroidLog = this.log;
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Closing connection expired @ ");
            Date localDate = new java/util/Date;
            localDate.<init>(localBasicPoolEntry.getExpiry());
            localStringBuilder.append(localDate);
            localHttpClientAndroidLog.debug(localStringBuilder.toString());
          }
          localIterator.remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramTimeUnit, "Time unit");
    if (paramLong <= 0L) {
      paramLong = 0L;
    }
    Object localObject1;
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject1 = this.log;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Closing connections idle longer than ");
      ((StringBuilder)localObject2).append(paramLong);
      ((StringBuilder)localObject2).append(" ");
      ((StringBuilder)localObject2).append(paramTimeUnit);
      ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
    }
    long l = System.currentTimeMillis();
    paramLong = paramTimeUnit.toMillis(paramLong);
    this.poolLock.lock();
    try
    {
      localObject1 = this.freeConnections.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)((Iterator)localObject1).next();
        if (localBasicPoolEntry.getUpdated() <= l - paramLong)
        {
          if (this.log.isDebugEnabled())
          {
            paramTimeUnit = this.log;
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Closing connection last used @ ");
            localObject2 = new java/util/Date;
            ((Date)localObject2).<init>(localBasicPoolEntry.getUpdated());
            localStringBuilder.append(localObject2);
            paramTimeUnit.debug(localStringBuilder.toString());
          }
          ((Iterator)localObject1).remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected BasicPoolEntry createEntry(RouteSpecificPool paramRouteSpecificPool, ClientConnectionOperator paramClientConnectionOperator)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Creating new connection [");
      localStringBuilder.append(paramRouteSpecificPool.getRoute());
      localStringBuilder.append("]");
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    paramClientConnectionOperator = new BasicPoolEntry(paramClientConnectionOperator, paramRouteSpecificPool.getRoute(), this.connTTL, this.connTTLTimeUnit);
    this.poolLock.lock();
    try
    {
      paramRouteSpecificPool.createdEntry(paramClientConnectionOperator);
      this.numConnections += 1;
      this.leasedConnections.add(paramClientConnectionOperator);
      return paramClientConnectionOperator;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected Queue<BasicPoolEntry> createFreeConnQueue()
  {
    return new LinkedList();
  }
  
  protected Map<HttpRoute, RouteSpecificPool> createRouteToPoolMap()
  {
    return new HashMap();
  }
  
  protected Queue<WaitingThread> createWaitingThreadQueue()
  {
    return new LinkedList();
  }
  
  public void deleteClosedConnections()
  {
    this.poolLock.lock();
    try
    {
      Iterator localIterator = this.freeConnections.iterator();
      while (localIterator.hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)localIterator.next();
        if (!localBasicPoolEntry.getConnection().isOpen())
        {
          localIterator.remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected void deleteEntry(BasicPoolEntry paramBasicPoolEntry)
  {
    HttpRoute localHttpRoute = paramBasicPoolEntry.getPlannedRoute();
    Object localObject;
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Deleting connection [");
      ((StringBuilder)localObject).append(localHttpRoute);
      ((StringBuilder)localObject).append("][");
      ((StringBuilder)localObject).append(paramBasicPoolEntry.getState());
      ((StringBuilder)localObject).append("]");
      localHttpClientAndroidLog.debug(((StringBuilder)localObject).toString());
    }
    this.poolLock.lock();
    try
    {
      closeConnection(paramBasicPoolEntry);
      localObject = getRoutePool(localHttpRoute, true);
      ((RouteSpecificPool)localObject).deleteEntry(paramBasicPoolEntry);
      this.numConnections -= 1;
      if (((RouteSpecificPool)localObject).isUnused()) {
        this.routeToPool.remove(localHttpRoute);
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected void deleteLeastUsedEntry()
  {
    this.poolLock.lock();
    try
    {
      BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)this.freeConnections.remove();
      if (localBasicPoolEntry != null) {
        deleteEntry(localBasicPoolEntry);
      } else if (this.log.isDebugEnabled()) {
        this.log.debug("No free connection to delete");
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public void freeEntry(BasicPoolEntry paramBasicPoolEntry, boolean paramBoolean, long paramLong, TimeUnit paramTimeUnit)
  {
    HttpRoute localHttpRoute = paramBasicPoolEntry.getPlannedRoute();
    Object localObject1;
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject1 = this.log;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Releasing connection [");
      ((StringBuilder)localObject2).append(localHttpRoute);
      ((StringBuilder)localObject2).append("][");
      ((StringBuilder)localObject2).append(paramBasicPoolEntry.getState());
      ((StringBuilder)localObject2).append("]");
      ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
    }
    this.poolLock.lock();
    try
    {
      if (this.shutdown)
      {
        closeConnection(paramBasicPoolEntry);
        return;
      }
      this.leasedConnections.remove(paramBasicPoolEntry);
      localObject2 = getRoutePool(localHttpRoute, true);
      if ((paramBoolean) && (((RouteSpecificPool)localObject2).getCapacity() >= 0))
      {
        if (this.log.isDebugEnabled())
        {
          if (paramLong > 0L)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("for ");
            ((StringBuilder)localObject1).append(paramLong);
            ((StringBuilder)localObject1).append(" ");
            ((StringBuilder)localObject1).append(paramTimeUnit);
            localObject1 = ((StringBuilder)localObject1).toString();
          }
          else
          {
            localObject1 = "indefinitely";
          }
          HttpClientAndroidLog localHttpClientAndroidLog = this.log;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Pooling connection [");
          localStringBuilder.append(localHttpRoute);
          localStringBuilder.append("][");
          localStringBuilder.append(paramBasicPoolEntry.getState());
          localStringBuilder.append("]; keep alive ");
          localStringBuilder.append((String)localObject1);
          localHttpClientAndroidLog.debug(localStringBuilder.toString());
        }
        ((RouteSpecificPool)localObject2).freeEntry(paramBasicPoolEntry);
        paramBasicPoolEntry.updateExpiry(paramLong, paramTimeUnit);
        this.freeConnections.add(paramBasicPoolEntry);
      }
      else
      {
        closeConnection(paramBasicPoolEntry);
        ((RouteSpecificPool)localObject2).dropEntry();
        this.numConnections -= 1;
      }
      notifyWaitingThread((RouteSpecificPool)localObject2);
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public int getConnectionsInPool()
  {
    this.poolLock.lock();
    try
    {
      int i = this.numConnections;
      return i;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public int getConnectionsInPool(HttpRoute paramHttpRoute)
  {
    this.poolLock.lock();
    int i = 0;
    try
    {
      paramHttpRoute = getRoutePool(paramHttpRoute, false);
      if (paramHttpRoute != null) {
        i = paramHttpRoute.getEntryCount();
      }
      return i;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected BasicPoolEntry getEntryBlocking(HttpRoute paramHttpRoute, Object paramObject, long paramLong, TimeUnit paramTimeUnit, WaitingThreadAborter paramWaitingThreadAborter)
    throws ConnectionPoolTimeoutException, InterruptedException
  {
    Object localObject2 = null;
    Date localDate;
    if (paramLong > 0L) {
      localDate = new Date(System.currentTimeMillis() + paramTimeUnit.toMillis(paramLong));
    } else {
      localDate = null;
    }
    this.poolLock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool = getRoutePool(paramHttpRoute, true);
      Object localObject1 = null;
      paramTimeUnit = (TimeUnit)localObject2;
      for (;;)
      {
        localObject2 = paramTimeUnit;
        if (paramTimeUnit == null)
        {
          boolean bool = this.shutdown;
          int i = 0;
          if (!bool) {
            bool = true;
          } else {
            bool = false;
          }
          Asserts.check(bool, "Connection pool shut down");
          if (this.log.isDebugEnabled())
          {
            paramTimeUnit = this.log;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append("[");
            ((StringBuilder)localObject2).append(paramHttpRoute);
            ((StringBuilder)localObject2).append("] total kept alive: ");
            ((StringBuilder)localObject2).append(this.freeConnections.size());
            ((StringBuilder)localObject2).append(", total issued: ");
            ((StringBuilder)localObject2).append(this.leasedConnections.size());
            ((StringBuilder)localObject2).append(", total allocated: ");
            ((StringBuilder)localObject2).append(this.numConnections);
            ((StringBuilder)localObject2).append(" out of ");
            ((StringBuilder)localObject2).append(this.maxTotalConnections);
            paramTimeUnit.debug(((StringBuilder)localObject2).toString());
          }
          localObject2 = getFreeEntry(localRouteSpecificPool, paramObject);
          if (localObject2 == null)
          {
            if (localRouteSpecificPool.getCapacity() > 0) {
              i = 1;
            }
            if (this.log.isDebugEnabled())
            {
              paramTimeUnit = this.log;
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              ((StringBuilder)localObject3).append("Available capacity: ");
              ((StringBuilder)localObject3).append(localRouteSpecificPool.getCapacity());
              ((StringBuilder)localObject3).append(" out of ");
              ((StringBuilder)localObject3).append(localRouteSpecificPool.getMaxEntries());
              ((StringBuilder)localObject3).append(" [");
              ((StringBuilder)localObject3).append(paramHttpRoute);
              ((StringBuilder)localObject3).append("][");
              ((StringBuilder)localObject3).append(paramObject);
              ((StringBuilder)localObject3).append("]");
              paramTimeUnit.debug(((StringBuilder)localObject3).toString());
            }
            if ((i != 0) && (this.numConnections < this.maxTotalConnections))
            {
              paramTimeUnit = createEntry(localRouteSpecificPool, this.operator);
              continue;
            }
            if ((i != 0) && (!this.freeConnections.isEmpty()))
            {
              deleteLeastUsedEntry();
              localRouteSpecificPool = getRoutePool(paramHttpRoute, true);
              paramTimeUnit = createEntry(localRouteSpecificPool, this.operator);
              continue;
            }
            if (this.log.isDebugEnabled())
            {
              localObject3 = this.log;
              paramTimeUnit = new java/lang/StringBuilder;
              paramTimeUnit.<init>();
              paramTimeUnit.append("Need to wait for connection [");
              paramTimeUnit.append(paramHttpRoute);
              paramTimeUnit.append("][");
              paramTimeUnit.append(paramObject);
              paramTimeUnit.append("]");
              ((HttpClientAndroidLog)localObject3).debug(paramTimeUnit.toString());
            }
            Object localObject3 = localObject1;
            if (localObject1 == null)
            {
              localObject3 = newWaitingThread(this.poolLock.newCondition(), localRouteSpecificPool);
              paramWaitingThreadAborter.setWaitingThread((WaitingThread)localObject3);
            }
            try
            {
              localRouteSpecificPool.queueThread((WaitingThread)localObject3);
              this.waitingThreads.add(localObject3);
              bool = ((WaitingThread)localObject3).await(localDate);
              localRouteSpecificPool.removeThread((WaitingThread)localObject3);
              this.waitingThreads.remove(localObject3);
              paramTimeUnit = (TimeUnit)localObject2;
              localObject1 = localObject3;
              if (!bool)
              {
                paramTimeUnit = (TimeUnit)localObject2;
                localObject1 = localObject3;
                if (localDate != null) {
                  if (localDate.getTime() > System.currentTimeMillis())
                  {
                    paramTimeUnit = (TimeUnit)localObject2;
                    localObject1 = localObject3;
                  }
                  else
                  {
                    paramHttpRoute = new cz/msebera/android/httpclient/conn/ConnectionPoolTimeoutException;
                    paramHttpRoute.<init>("Timeout waiting for connection from pool");
                    throw paramHttpRoute;
                  }
                }
              }
            }
            finally
            {
              localRouteSpecificPool.removeThread((WaitingThread)localObject3);
              this.waitingThreads.remove(localObject3);
            }
          }
        }
      }
      return (BasicPoolEntry)localObject2;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected BasicPoolEntry getFreeEntry(RouteSpecificPool paramRouteSpecificPool, Object paramObject)
  {
    this.poolLock.lock();
    int i = 0;
    BasicPoolEntry localBasicPoolEntry = null;
    while (i == 0) {
      try
      {
        localBasicPoolEntry = paramRouteSpecificPool.allocEntry(paramObject);
        Object localObject1;
        Object localObject2;
        if (localBasicPoolEntry != null)
        {
          if (this.log.isDebugEnabled())
          {
            localObject1 = this.log;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append("Getting free connection [");
            ((StringBuilder)localObject2).append(paramRouteSpecificPool.getRoute());
            ((StringBuilder)localObject2).append("][");
            ((StringBuilder)localObject2).append(paramObject);
            ((StringBuilder)localObject2).append("]");
            ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
          }
          this.freeConnections.remove(localBasicPoolEntry);
          if (localBasicPoolEntry.isExpired(System.currentTimeMillis()))
          {
            if (this.log.isDebugEnabled())
            {
              localObject2 = this.log;
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append("Closing expired free connection [");
              ((StringBuilder)localObject1).append(paramRouteSpecificPool.getRoute());
              ((StringBuilder)localObject1).append("][");
              ((StringBuilder)localObject1).append(paramObject);
              ((StringBuilder)localObject1).append("]");
              ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
            }
            closeConnection(localBasicPoolEntry);
            paramRouteSpecificPool.dropEntry();
            this.numConnections -= 1;
          }
          else
          {
            this.leasedConnections.add(localBasicPoolEntry);
            i = 1;
          }
        }
        else
        {
          if (this.log.isDebugEnabled())
          {
            localObject2 = this.log;
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("No free connections [");
            ((StringBuilder)localObject1).append(paramRouteSpecificPool.getRoute());
            ((StringBuilder)localObject1).append("][");
            ((StringBuilder)localObject1).append(paramObject);
            ((StringBuilder)localObject1).append("]");
            ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
          }
          i = 1;
        }
      }
      finally
      {
        this.poolLock.unlock();
      }
    }
    this.poolLock.unlock();
    return localBasicPoolEntry;
  }
  
  protected Lock getLock()
  {
    return this.poolLock;
  }
  
  public int getMaxTotalConnections()
  {
    return this.maxTotalConnections;
  }
  
  protected RouteSpecificPool getRoutePool(HttpRoute paramHttpRoute, boolean paramBoolean)
  {
    this.poolLock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool2 = (RouteSpecificPool)this.routeToPool.get(paramHttpRoute);
      RouteSpecificPool localRouteSpecificPool1 = localRouteSpecificPool2;
      if (localRouteSpecificPool2 == null)
      {
        localRouteSpecificPool1 = localRouteSpecificPool2;
        if (paramBoolean)
        {
          localRouteSpecificPool1 = newRouteSpecificPool(paramHttpRoute);
          this.routeToPool.put(paramHttpRoute, localRouteSpecificPool1);
        }
      }
      return localRouteSpecificPool1;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected void handleLostEntry(HttpRoute paramHttpRoute)
  {
    this.poolLock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool = getRoutePool(paramHttpRoute, true);
      localRouteSpecificPool.dropEntry();
      if (localRouteSpecificPool.isUnused()) {
        this.routeToPool.remove(paramHttpRoute);
      }
      this.numConnections -= 1;
      notifyWaitingThread(localRouteSpecificPool);
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected RouteSpecificPool newRouteSpecificPool(HttpRoute paramHttpRoute)
  {
    return new RouteSpecificPool(paramHttpRoute, this.connPerRoute);
  }
  
  protected WaitingThread newWaitingThread(Condition paramCondition, RouteSpecificPool paramRouteSpecificPool)
  {
    return new WaitingThread(paramCondition, paramRouteSpecificPool);
  }
  
  protected void notifyWaitingThread(RouteSpecificPool paramRouteSpecificPool)
  {
    this.poolLock.lock();
    if (paramRouteSpecificPool != null) {}
    try
    {
      if (paramRouteSpecificPool.hasThread())
      {
        if (this.log.isDebugEnabled())
        {
          HttpClientAndroidLog localHttpClientAndroidLog = this.log;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Notifying thread waiting on pool [");
          localStringBuilder.append(paramRouteSpecificPool.getRoute());
          localStringBuilder.append("]");
          localHttpClientAndroidLog.debug(localStringBuilder.toString());
        }
        paramRouteSpecificPool = paramRouteSpecificPool.nextThread();
      }
      else if (!this.waitingThreads.isEmpty())
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("Notifying thread waiting on any pool");
        }
        paramRouteSpecificPool = (WaitingThread)this.waitingThreads.remove();
      }
      else
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("Notifying no-one, there are no waiting threads");
        }
        paramRouteSpecificPool = null;
      }
      if (paramRouteSpecificPool != null) {
        paramRouteSpecificPool.wakeup();
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public PoolEntryRequest requestPoolEntry(final HttpRoute paramHttpRoute, final Object paramObject)
  {
    new PoolEntryRequest()
    {
      public void abortRequest()
      {
        ConnPoolByRoute.this.poolLock.lock();
        try
        {
          this.val$aborter.abort();
          return;
        }
        finally
        {
          ConnPoolByRoute.this.poolLock.unlock();
        }
      }
      
      public BasicPoolEntry getPoolEntry(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
        throws InterruptedException, ConnectionPoolTimeoutException
      {
        return ConnPoolByRoute.this.getEntryBlocking(paramHttpRoute, paramObject, paramAnonymousLong, paramAnonymousTimeUnit, this.val$aborter);
      }
    };
  }
  
  public void setMaxTotalConnections(int paramInt)
  {
    this.poolLock.lock();
    try
    {
      this.maxTotalConnections = paramInt;
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public void shutdown()
  {
    this.poolLock.lock();
    try
    {
      boolean bool = this.shutdown;
      if (bool) {
        return;
      }
      this.shutdown = true;
      Object localObject1 = this.leasedConnections.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject3 = (BasicPoolEntry)((Iterator)localObject1).next();
        ((Iterator)localObject1).remove();
        closeConnection((BasicPoolEntry)localObject3);
      }
      Iterator localIterator = this.freeConnections.iterator();
      while (localIterator.hasNext())
      {
        localObject1 = (BasicPoolEntry)localIterator.next();
        localIterator.remove();
        if (this.log.isDebugEnabled())
        {
          HttpClientAndroidLog localHttpClientAndroidLog = this.log;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          ((StringBuilder)localObject3).append("Closing connection [");
          ((StringBuilder)localObject3).append(((BasicPoolEntry)localObject1).getPlannedRoute());
          ((StringBuilder)localObject3).append("][");
          ((StringBuilder)localObject3).append(((BasicPoolEntry)localObject1).getState());
          ((StringBuilder)localObject3).append("]");
          localHttpClientAndroidLog.debug(((StringBuilder)localObject3).toString());
        }
        closeConnection((BasicPoolEntry)localObject1);
      }
      Object localObject3 = this.waitingThreads.iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject1 = (WaitingThread)((Iterator)localObject3).next();
        ((Iterator)localObject3).remove();
        ((WaitingThread)localObject1).wakeup();
      }
      this.routeToPool.clear();
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */