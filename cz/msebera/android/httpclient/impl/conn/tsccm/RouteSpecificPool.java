package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.params.ConnPerRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;

@Deprecated
public class RouteSpecificPool
{
  protected final ConnPerRoute connPerRoute;
  protected final LinkedList<BasicPoolEntry> freeEntries;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  protected final int maxEntries;
  protected int numEntries;
  protected final HttpRoute route;
  protected final Queue<WaitingThread> waitingThreads;
  
  @Deprecated
  public RouteSpecificPool(HttpRoute paramHttpRoute, int paramInt)
  {
    this.route = paramHttpRoute;
    this.maxEntries = paramInt;
    this.connPerRoute = new ConnPerRoute()
    {
      public int getMaxForRoute(HttpRoute paramAnonymousHttpRoute)
      {
        return RouteSpecificPool.this.maxEntries;
      }
    };
    this.freeEntries = new LinkedList();
    this.waitingThreads = new LinkedList();
    this.numEntries = 0;
  }
  
  public RouteSpecificPool(HttpRoute paramHttpRoute, ConnPerRoute paramConnPerRoute)
  {
    this.route = paramHttpRoute;
    this.connPerRoute = paramConnPerRoute;
    this.maxEntries = paramConnPerRoute.getMaxForRoute(paramHttpRoute);
    this.freeEntries = new LinkedList();
    this.waitingThreads = new LinkedList();
    this.numEntries = 0;
  }
  
  public BasicPoolEntry allocEntry(Object paramObject)
  {
    Object localObject;
    if (!this.freeEntries.isEmpty())
    {
      localObject = this.freeEntries;
      localObject = ((LinkedList)localObject).listIterator(((LinkedList)localObject).size());
      while (((ListIterator)localObject).hasPrevious())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)((ListIterator)localObject).previous();
        if ((localBasicPoolEntry.getState() == null) || (LangUtils.equals(paramObject, localBasicPoolEntry.getState())))
        {
          ((ListIterator)localObject).remove();
          return localBasicPoolEntry;
        }
      }
    }
    if ((getCapacity() == 0) && (!this.freeEntries.isEmpty()))
    {
      paramObject = (BasicPoolEntry)this.freeEntries.remove();
      ((BasicPoolEntry)paramObject).shutdownEntry();
      localObject = ((BasicPoolEntry)paramObject).getConnection();
      try
      {
        ((OperatedClientConnection)localObject).close();
      }
      catch (IOException localIOException)
      {
        this.log.debug("I/O error closing connection", localIOException);
      }
      return (BasicPoolEntry)paramObject;
    }
    return null;
  }
  
  public void createdEntry(BasicPoolEntry paramBasicPoolEntry)
  {
    Args.check(this.route.equals(paramBasicPoolEntry.getPlannedRoute()), "Entry not planned for this pool");
    this.numEntries += 1;
  }
  
  public boolean deleteEntry(BasicPoolEntry paramBasicPoolEntry)
  {
    boolean bool = this.freeEntries.remove(paramBasicPoolEntry);
    if (bool) {
      this.numEntries -= 1;
    }
    return bool;
  }
  
  public void dropEntry()
  {
    boolean bool;
    if (this.numEntries > 0) {
      bool = true;
    } else {
      bool = false;
    }
    Asserts.check(bool, "There is no entry that could be dropped");
    this.numEntries -= 1;
  }
  
  public void freeEntry(BasicPoolEntry paramBasicPoolEntry)
  {
    int i = this.numEntries;
    if (i >= 1)
    {
      if (i > this.freeEntries.size())
      {
        this.freeEntries.add(paramBasicPoolEntry);
        return;
      }
      paramBasicPoolEntry = new StringBuilder();
      paramBasicPoolEntry.append("No entry allocated from this pool. ");
      paramBasicPoolEntry.append(this.route);
      throw new IllegalStateException(paramBasicPoolEntry.toString());
    }
    paramBasicPoolEntry = new StringBuilder();
    paramBasicPoolEntry.append("No entry created for this pool. ");
    paramBasicPoolEntry.append(this.route);
    throw new IllegalStateException(paramBasicPoolEntry.toString());
  }
  
  public int getCapacity()
  {
    return this.connPerRoute.getMaxForRoute(this.route) - this.numEntries;
  }
  
  public final int getEntryCount()
  {
    return this.numEntries;
  }
  
  public final int getMaxEntries()
  {
    return this.maxEntries;
  }
  
  public final HttpRoute getRoute()
  {
    return this.route;
  }
  
  public boolean hasThread()
  {
    return this.waitingThreads.isEmpty() ^ true;
  }
  
  public boolean isUnused()
  {
    int i = this.numEntries;
    boolean bool = true;
    if ((i >= 1) || (!this.waitingThreads.isEmpty())) {
      bool = false;
    }
    return bool;
  }
  
  public WaitingThread nextThread()
  {
    return (WaitingThread)this.waitingThreads.peek();
  }
  
  public void queueThread(WaitingThread paramWaitingThread)
  {
    Args.notNull(paramWaitingThread, "Waiting thread");
    this.waitingThreads.add(paramWaitingThread);
  }
  
  public void removeThread(WaitingThread paramWaitingThread)
  {
    if (paramWaitingThread == null) {
      return;
    }
    this.waitingThreads.remove(paramWaitingThread);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */