package cz.msebera.android.httpclient.impl.conn.tsccm;

@Deprecated
public class WaitingThreadAborter
{
  private boolean aborted;
  private WaitingThread waitingThread;
  
  public void abort()
  {
    this.aborted = true;
    WaitingThread localWaitingThread = this.waitingThread;
    if (localWaitingThread != null) {
      localWaitingThread.interrupt();
    }
  }
  
  public void setWaitingThread(WaitingThread paramWaitingThread)
  {
    this.waitingThread = paramWaitingThread;
    if (this.aborted) {
      paramWaitingThread.interrupt();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThreadAborter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */