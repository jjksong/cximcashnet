package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.params.ConnPerRouteBean;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.conn.DefaultClientConnectionOperator;
import cz.msebera.android.httpclient.impl.conn.SchemeRegistryFactory;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.util.concurrent.TimeUnit;

@Deprecated
@ThreadSafe
public class ThreadSafeClientConnManager
  implements ClientConnectionManager
{
  protected final ClientConnectionOperator connOperator;
  protected final ConnPerRouteBean connPerRoute;
  protected final AbstractConnPool connectionPool;
  public HttpClientAndroidLog log;
  protected final ConnPoolByRoute pool;
  protected final SchemeRegistry schemeRegistry;
  
  public ThreadSafeClientConnManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public ThreadSafeClientConnManager(SchemeRegistry paramSchemeRegistry)
  {
    this(paramSchemeRegistry, -1L, TimeUnit.MILLISECONDS);
  }
  
  public ThreadSafeClientConnManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit)
  {
    this(paramSchemeRegistry, paramLong, paramTimeUnit, new ConnPerRouteBean());
  }
  
  public ThreadSafeClientConnManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit, ConnPerRouteBean paramConnPerRouteBean)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.log = new HttpClientAndroidLog(getClass());
    this.schemeRegistry = paramSchemeRegistry;
    this.connPerRoute = paramConnPerRouteBean;
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
    this.pool = createConnectionPool(paramLong, paramTimeUnit);
    this.connectionPool = this.pool;
  }
  
  @Deprecated
  public ThreadSafeClientConnManager(HttpParams paramHttpParams, SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.log = new HttpClientAndroidLog(getClass());
    this.schemeRegistry = paramSchemeRegistry;
    this.connPerRoute = new ConnPerRouteBean();
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
    this.pool = ((ConnPoolByRoute)createConnectionPool(paramHttpParams));
    this.connectionPool = this.pool;
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    this.pool.closeExpiredConnections();
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Closing connections idle longer than ");
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" ");
      localStringBuilder.append(paramTimeUnit);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    this.pool.closeIdleConnections(paramLong, paramTimeUnit);
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry);
  }
  
  @Deprecated
  protected AbstractConnPool createConnectionPool(HttpParams paramHttpParams)
  {
    return new ConnPoolByRoute(this.connOperator, paramHttpParams);
  }
  
  protected ConnPoolByRoute createConnectionPool(long paramLong, TimeUnit paramTimeUnit)
  {
    return new ConnPoolByRoute(this.connOperator, this.connPerRoute, 20, paramLong, paramTimeUnit);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  public int getConnectionsInPool()
  {
    return this.pool.getConnectionsInPool();
  }
  
  public int getConnectionsInPool(HttpRoute paramHttpRoute)
  {
    return this.pool.getConnectionsInPool(paramHttpRoute);
  }
  
  public int getDefaultMaxPerRoute()
  {
    return this.connPerRoute.getDefaultMaxPerRoute();
  }
  
  public int getMaxForRoute(HttpRoute paramHttpRoute)
  {
    return this.connPerRoute.getMaxForRoute(paramHttpRoute);
  }
  
  public int getMaxTotal()
  {
    return this.pool.getMaxTotalConnections();
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  /* Error */
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 176
    //   4: ldc -78
    //   6: invokestatic 182	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   9: aload_1
    //   10: checkcast 176	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter
    //   13: astore 6
    //   15: aload 6
    //   17: invokevirtual 186	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:getPoolEntry	()Lcz/msebera/android/httpclient/impl/conn/AbstractPoolEntry;
    //   20: ifnull +28 -> 48
    //   23: aload 6
    //   25: invokevirtual 190	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:getManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   28: aload_0
    //   29: if_acmpne +9 -> 38
    //   32: iconst_1
    //   33: istore 5
    //   35: goto +6 -> 41
    //   38: iconst_0
    //   39: istore 5
    //   41: iload 5
    //   43: ldc -64
    //   45: invokestatic 195	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   48: aload 6
    //   50: monitorenter
    //   51: aload 6
    //   53: invokevirtual 186	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:getPoolEntry	()Lcz/msebera/android/httpclient/impl/conn/AbstractPoolEntry;
    //   56: checkcast 197	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry
    //   59: astore 7
    //   61: aload 7
    //   63: ifnonnull +7 -> 70
    //   66: aload 6
    //   68: monitorexit
    //   69: return
    //   70: aload 6
    //   72: invokevirtual 200	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:isOpen	()Z
    //   75: ifeq +16 -> 91
    //   78: aload 6
    //   80: invokevirtual 203	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:isMarkedReusable	()Z
    //   83: ifne +8 -> 91
    //   86: aload 6
    //   88: invokevirtual 204	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:shutdown	()V
    //   91: aload 6
    //   93: invokevirtual 203	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:isMarkedReusable	()Z
    //   96: istore 5
    //   98: aload_0
    //   99: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   102: invokevirtual 111	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   105: ifeq +29 -> 134
    //   108: iload 5
    //   110: ifeq +15 -> 125
    //   113: aload_0
    //   114: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   117: ldc -50
    //   119: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   122: goto +12 -> 134
    //   125: aload_0
    //   126: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   129: ldc -48
    //   131: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   134: aload 6
    //   136: invokevirtual 211	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:detach	()V
    //   139: aload_0
    //   140: getfield 88	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:pool	Lcz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute;
    //   143: astore_1
    //   144: aload_1
    //   145: aload 7
    //   147: iload 5
    //   149: lload_2
    //   150: aload 4
    //   152: invokevirtual 215	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;ZJLjava/util/concurrent/TimeUnit;)V
    //   155: goto +84 -> 239
    //   158: astore_1
    //   159: goto +84 -> 243
    //   162: astore_1
    //   163: aload_0
    //   164: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   167: invokevirtual 111	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   170: ifeq +13 -> 183
    //   173: aload_0
    //   174: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   177: ldc -39
    //   179: aload_1
    //   180: invokevirtual 220	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   183: aload 6
    //   185: invokevirtual 203	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:isMarkedReusable	()Z
    //   188: istore 5
    //   190: aload_0
    //   191: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   194: invokevirtual 111	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   197: ifeq +29 -> 226
    //   200: iload 5
    //   202: ifeq +15 -> 217
    //   205: aload_0
    //   206: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   209: ldc -50
    //   211: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   214: goto +12 -> 226
    //   217: aload_0
    //   218: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   221: ldc -48
    //   223: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   226: aload 6
    //   228: invokevirtual 211	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:detach	()V
    //   231: aload_0
    //   232: getfield 88	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:pool	Lcz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute;
    //   235: astore_1
    //   236: goto -92 -> 144
    //   239: aload 6
    //   241: monitorexit
    //   242: return
    //   243: aload 6
    //   245: invokevirtual 203	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:isMarkedReusable	()Z
    //   248: istore 5
    //   250: aload_0
    //   251: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   254: invokevirtual 111	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   257: ifeq +29 -> 286
    //   260: iload 5
    //   262: ifeq +15 -> 277
    //   265: aload_0
    //   266: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   269: ldc -50
    //   271: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   274: goto +12 -> 286
    //   277: aload_0
    //   278: getfield 72	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   281: ldc -48
    //   283: invokevirtual 103	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   286: aload 6
    //   288: invokevirtual 211	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPooledConnAdapter:detach	()V
    //   291: aload_0
    //   292: getfield 88	cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager:pool	Lcz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute;
    //   295: aload 7
    //   297: iload 5
    //   299: lload_2
    //   300: aload 4
    //   302: invokevirtual 215	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;ZJLjava/util/concurrent/TimeUnit;)V
    //   305: aload_1
    //   306: athrow
    //   307: astore_1
    //   308: aload 6
    //   310: monitorexit
    //   311: aload_1
    //   312: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	313	0	this	ThreadSafeClientConnManager
    //   0	313	1	paramManagedClientConnection	ManagedClientConnection
    //   0	313	2	paramLong	long
    //   0	313	4	paramTimeUnit	TimeUnit
    //   33	265	5	bool	boolean
    //   13	296	6	localBasicPooledConnAdapter	BasicPooledConnAdapter
    //   59	237	7	localBasicPoolEntry	BasicPoolEntry
    // Exception table:
    //   from	to	target	type
    //   70	91	158	finally
    //   163	183	158	finally
    //   70	91	162	java/io/IOException
    //   51	61	307	finally
    //   66	69	307	finally
    //   91	108	307	finally
    //   113	122	307	finally
    //   125	134	307	finally
    //   134	144	307	finally
    //   144	155	307	finally
    //   183	200	307	finally
    //   205	214	307	finally
    //   217	226	307	finally
    //   226	236	307	finally
    //   239	242	307	finally
    //   243	260	307	finally
    //   265	274	307	finally
    //   277	286	307	finally
    //   286	307	307	finally
    //   308	311	307	finally
  }
  
  public ClientConnectionRequest requestConnection(final HttpRoute paramHttpRoute, Object paramObject)
  {
    new ClientConnectionRequest()
    {
      public void abortRequest()
      {
        this.val$poolRequest.abortRequest();
      }
      
      public ManagedClientConnection getConnection(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
        throws InterruptedException, ConnectionPoolTimeoutException
      {
        Args.notNull(paramHttpRoute, "Route");
        if (ThreadSafeClientConnManager.this.log.isDebugEnabled())
        {
          HttpClientAndroidLog localHttpClientAndroidLog = ThreadSafeClientConnManager.this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Get connection: ");
          localStringBuilder.append(paramHttpRoute);
          localStringBuilder.append(", timeout = ");
          localStringBuilder.append(paramAnonymousLong);
          localHttpClientAndroidLog.debug(localStringBuilder.toString());
        }
        paramAnonymousTimeUnit = this.val$poolRequest.getPoolEntry(paramAnonymousLong, paramAnonymousTimeUnit);
        return new BasicPooledConnAdapter(ThreadSafeClientConnManager.this, paramAnonymousTimeUnit);
      }
    };
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    this.connPerRoute.setDefaultMaxPerRoute(paramInt);
  }
  
  public void setMaxForRoute(HttpRoute paramHttpRoute, int paramInt)
  {
    this.connPerRoute.setMaxForRoute(paramHttpRoute, paramInt);
  }
  
  public void setMaxTotal(int paramInt)
  {
    this.pool.setMaxTotalConnections(paramInt);
  }
  
  public void shutdown()
  {
    this.log.debug("Shutting down");
    this.pool.shutdown();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */