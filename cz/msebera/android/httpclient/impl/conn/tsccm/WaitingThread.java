package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.util.Args;
import java.util.Date;
import java.util.concurrent.locks.Condition;

@Deprecated
public class WaitingThread
{
  private boolean aborted;
  private final Condition cond;
  private final RouteSpecificPool pool;
  private Thread waiter;
  
  public WaitingThread(Condition paramCondition, RouteSpecificPool paramRouteSpecificPool)
  {
    Args.notNull(paramCondition, "Condition");
    this.cond = paramCondition;
    this.pool = paramRouteSpecificPool;
  }
  
  public boolean await(Date paramDate)
    throws InterruptedException
  {
    if (this.waiter == null)
    {
      if (!this.aborted)
      {
        this.waiter = Thread.currentThread();
        if (paramDate != null) {}
        try
        {
          boolean bool1 = this.cond.awaitUntil(paramDate);
          break label50;
          this.cond.await();
          bool1 = true;
          label50:
          boolean bool2 = this.aborted;
          if (!bool2) {
            return bool1;
          }
          paramDate = new java/lang/InterruptedException;
          paramDate.<init>("Operation interrupted");
          throw paramDate;
        }
        finally
        {
          this.waiter = null;
        }
      }
      throw new InterruptedException("Operation interrupted");
    }
    paramDate = new StringBuilder();
    paramDate.append("A thread is already waiting on this object.\ncaller: ");
    paramDate.append(Thread.currentThread());
    paramDate.append("\nwaiter: ");
    paramDate.append(this.waiter);
    throw new IllegalStateException(paramDate.toString());
  }
  
  public final Condition getCondition()
  {
    return this.cond;
  }
  
  public final RouteSpecificPool getPool()
  {
    return this.pool;
  }
  
  public final Thread getThread()
  {
    return this.waiter;
  }
  
  public void interrupt()
  {
    this.aborted = true;
    this.cond.signalAll();
  }
  
  public void wakeup()
  {
    if (this.waiter != null)
    {
      this.cond.signalAll();
      return;
    }
    throw new IllegalStateException("Nobody waiting on this object.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */