package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.AbstractConnPool;
import cz.msebera.android.httpclient.pool.ConnFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@ThreadSafe
class CPool
  extends AbstractConnPool<HttpRoute, ManagedHttpClientConnection, CPoolEntry>
{
  private static final AtomicLong COUNTER = new AtomicLong();
  public HttpClientAndroidLog log = new HttpClientAndroidLog(CPool.class);
  private final long timeToLive;
  private final TimeUnit tunit;
  
  public CPool(ConnFactory<HttpRoute, ManagedHttpClientConnection> paramConnFactory, int paramInt1, int paramInt2, long paramLong, TimeUnit paramTimeUnit)
  {
    super(paramConnFactory, paramInt1, paramInt2);
    this.timeToLive = paramLong;
    this.tunit = paramTimeUnit;
  }
  
  protected CPoolEntry createEntry(HttpRoute paramHttpRoute, ManagedHttpClientConnection paramManagedHttpClientConnection)
  {
    String str = Long.toString(COUNTER.getAndIncrement());
    return new CPoolEntry(this.log, str, paramHttpRoute, paramManagedHttpClientConnection, this.timeToLive, this.tunit);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/CPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */