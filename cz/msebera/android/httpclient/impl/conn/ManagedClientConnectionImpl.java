package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpConnectionMetrics;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

@Deprecated
@NotThreadSafe
class ManagedClientConnectionImpl
  implements ManagedClientConnection
{
  private volatile long duration;
  private final ClientConnectionManager manager;
  private final ClientConnectionOperator operator;
  private volatile HttpPoolEntry poolEntry;
  private volatile boolean reusable;
  
  ManagedClientConnectionImpl(ClientConnectionManager paramClientConnectionManager, ClientConnectionOperator paramClientConnectionOperator, HttpPoolEntry paramHttpPoolEntry)
  {
    Args.notNull(paramClientConnectionManager, "Connection manager");
    Args.notNull(paramClientConnectionOperator, "Connection operator");
    Args.notNull(paramHttpPoolEntry, "HTTP pool entry");
    this.manager = paramClientConnectionManager;
    this.operator = paramClientConnectionOperator;
    this.poolEntry = paramHttpPoolEntry;
    this.reusable = false;
    this.duration = Long.MAX_VALUE;
  }
  
  private OperatedClientConnection ensureConnection()
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    if (localHttpPoolEntry != null) {
      return (OperatedClientConnection)localHttpPoolEntry.getConnection();
    }
    throw new ConnectionShutdownException();
  }
  
  private HttpPoolEntry ensurePoolEntry()
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    if (localHttpPoolEntry != null) {
      return localHttpPoolEntry;
    }
    throw new ConnectionShutdownException();
  }
  
  private OperatedClientConnection getConnection()
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    if (localHttpPoolEntry == null) {
      return null;
    }
    return (OperatedClientConnection)localHttpPoolEntry.getConnection();
  }
  
  /* Error */
  public void abortConnection()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 41	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   6: ifnonnull +6 -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: iconst_0
    //   14: putfield 43	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:reusable	Z
    //   17: aload_0
    //   18: getfield 41	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   21: invokevirtual 56	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:getConnection	()Ljava/lang/Object;
    //   24: checkcast 58	cz/msebera/android/httpclient/conn/OperatedClientConnection
    //   27: astore_1
    //   28: aload_1
    //   29: invokeinterface 69 1 0
    //   34: aload_0
    //   35: getfield 37	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:manager	Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   38: aload_0
    //   39: aload_0
    //   40: getfield 47	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:duration	J
    //   43: getstatic 75	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   46: invokeinterface 81 5 0
    //   51: aload_0
    //   52: aconst_null
    //   53: putfield 41	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   56: aload_0
    //   57: monitorexit
    //   58: return
    //   59: astore_1
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    //   64: astore_1
    //   65: goto -31 -> 34
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	68	0	this	ManagedClientConnectionImpl
    //   27	2	1	localOperatedClientConnection	OperatedClientConnection
    //   59	4	1	localObject	Object
    //   64	1	1	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   2	11	59	finally
    //   12	28	59	finally
    //   28	34	59	finally
    //   34	58	59	finally
    //   60	62	59	finally
    //   28	34	64	java/io/IOException
  }
  
  public void bind(Socket paramSocket)
    throws IOException
  {
    throw new UnsupportedOperationException();
  }
  
  public void close()
    throws IOException
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    if (localHttpPoolEntry != null)
    {
      OperatedClientConnection localOperatedClientConnection = (OperatedClientConnection)localHttpPoolEntry.getConnection();
      localHttpPoolEntry.getTracker().reset();
      localOperatedClientConnection.close();
    }
  }
  
  HttpPoolEntry detach()
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    this.poolEntry = null;
    return localHttpPoolEntry;
  }
  
  public void flush()
    throws IOException
  {
    ensureConnection().flush();
  }
  
  public Object getAttribute(String paramString)
  {
    OperatedClientConnection localOperatedClientConnection = ensureConnection();
    if ((localOperatedClientConnection instanceof HttpContext)) {
      return ((HttpContext)localOperatedClientConnection).getAttribute(paramString);
    }
    return null;
  }
  
  public String getId()
  {
    return null;
  }
  
  public InetAddress getLocalAddress()
  {
    return ensureConnection().getLocalAddress();
  }
  
  public int getLocalPort()
  {
    return ensureConnection().getLocalPort();
  }
  
  public ClientConnectionManager getManager()
  {
    return this.manager;
  }
  
  public HttpConnectionMetrics getMetrics()
  {
    return ensureConnection().getMetrics();
  }
  
  HttpPoolEntry getPoolEntry()
  {
    return this.poolEntry;
  }
  
  public InetAddress getRemoteAddress()
  {
    return ensureConnection().getRemoteAddress();
  }
  
  public int getRemotePort()
  {
    return ensureConnection().getRemotePort();
  }
  
  public HttpRoute getRoute()
  {
    return ensurePoolEntry().getEffectiveRoute();
  }
  
  public SSLSession getSSLSession()
  {
    Object localObject = ensureConnection().getSocket();
    if ((localObject instanceof SSLSocket)) {
      localObject = ((SSLSocket)localObject).getSession();
    } else {
      localObject = null;
    }
    return (SSLSession)localObject;
  }
  
  public Socket getSocket()
  {
    return ensureConnection().getSocket();
  }
  
  public int getSocketTimeout()
  {
    return ensureConnection().getSocketTimeout();
  }
  
  public Object getState()
  {
    return ensurePoolEntry().getState();
  }
  
  public boolean isMarkedReusable()
  {
    return this.reusable;
  }
  
  public boolean isOpen()
  {
    OperatedClientConnection localOperatedClientConnection = getConnection();
    if (localOperatedClientConnection != null) {
      return localOperatedClientConnection.isOpen();
    }
    return false;
  }
  
  public boolean isResponseAvailable(int paramInt)
    throws IOException
  {
    return ensureConnection().isResponseAvailable(paramInt);
  }
  
  public boolean isSecure()
  {
    return ensureConnection().isSecure();
  }
  
  public boolean isStale()
  {
    OperatedClientConnection localOperatedClientConnection = getConnection();
    if (localOperatedClientConnection != null) {
      return localOperatedClientConnection.isStale();
    }
    return true;
  }
  
  public void layerProtocol(HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpParams, "HTTP parameters");
    try
    {
      if (this.poolEntry != null)
      {
        Object localObject = this.poolEntry.getTracker();
        Asserts.notNull(localObject, "Route tracker");
        Asserts.check(((RouteTracker)localObject).isConnected(), "Connection not open");
        Asserts.check(((RouteTracker)localObject).isTunnelled(), "Protocol layering without a tunnel not supported");
        boolean bool;
        if (!((RouteTracker)localObject).isLayered()) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Multiple protocol layering not supported");
        localObject = ((RouteTracker)localObject).getTargetHost();
        OperatedClientConnection localOperatedClientConnection = (OperatedClientConnection)this.poolEntry.getConnection();
        this.operator.updateSecureConnection(localOperatedClientConnection, (HttpHost)localObject, paramHttpContext, paramHttpParams);
        try
        {
          if (this.poolEntry != null)
          {
            this.poolEntry.getTracker().layerProtocol(localOperatedClientConnection.isSecure());
            return;
          }
          paramHttpContext = new java/io/InterruptedIOException;
          paramHttpContext.<init>();
          throw paramHttpContext;
        }
        finally {}
      }
      paramHttpContext = new cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException;
      paramHttpContext.<init>();
      throw paramHttpContext;
    }
    finally {}
  }
  
  public void markReusable()
  {
    this.reusable = true;
  }
  
  public void open(HttpRoute paramHttpRoute, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpRoute, "Route");
    Args.notNull(paramHttpParams, "HTTP parameters");
    try
    {
      if (this.poolEntry != null)
      {
        Object localObject = this.poolEntry.getTracker();
        Asserts.notNull(localObject, "Route tracker");
        boolean bool;
        if (!((RouteTracker)localObject).isConnected()) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Connection already open");
        OperatedClientConnection localOperatedClientConnection = (OperatedClientConnection)this.poolEntry.getConnection();
        HttpHost localHttpHost = paramHttpRoute.getProxyHost();
        ClientConnectionOperator localClientConnectionOperator = this.operator;
        if (localHttpHost != null) {
          localObject = localHttpHost;
        } else {
          localObject = paramHttpRoute.getTargetHost();
        }
        localClientConnectionOperator.openConnection(localOperatedClientConnection, (HttpHost)localObject, paramHttpRoute.getLocalAddress(), paramHttpContext, paramHttpParams);
        try
        {
          if (this.poolEntry != null)
          {
            paramHttpRoute = this.poolEntry.getTracker();
            if (localHttpHost == null) {
              paramHttpRoute.connectTarget(localOperatedClientConnection.isSecure());
            } else {
              paramHttpRoute.connectProxy(localHttpHost, localOperatedClientConnection.isSecure());
            }
            return;
          }
          paramHttpRoute = new java/io/InterruptedIOException;
          paramHttpRoute.<init>();
          throw paramHttpRoute;
        }
        finally {}
      }
      paramHttpRoute = new cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException;
      paramHttpRoute.<init>();
      throw paramHttpRoute;
    }
    finally {}
  }
  
  public void receiveResponseEntity(HttpResponse paramHttpResponse)
    throws HttpException, IOException
  {
    ensureConnection().receiveResponseEntity(paramHttpResponse);
  }
  
  public HttpResponse receiveResponseHeader()
    throws HttpException, IOException
  {
    return ensureConnection().receiveResponseHeader();
  }
  
  public void releaseConnection()
  {
    try
    {
      if (this.poolEntry == null) {
        return;
      }
      this.manager.releaseConnection(this, this.duration, TimeUnit.MILLISECONDS);
      this.poolEntry = null;
      return;
    }
    finally {}
  }
  
  public Object removeAttribute(String paramString)
  {
    OperatedClientConnection localOperatedClientConnection = ensureConnection();
    if ((localOperatedClientConnection instanceof HttpContext)) {
      return ((HttpContext)localOperatedClientConnection).removeAttribute(paramString);
    }
    return null;
  }
  
  public void sendRequestEntity(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    throws HttpException, IOException
  {
    ensureConnection().sendRequestEntity(paramHttpEntityEnclosingRequest);
  }
  
  public void sendRequestHeader(HttpRequest paramHttpRequest)
    throws HttpException, IOException
  {
    ensureConnection().sendRequestHeader(paramHttpRequest);
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    OperatedClientConnection localOperatedClientConnection = ensureConnection();
    if ((localOperatedClientConnection instanceof HttpContext)) {
      ((HttpContext)localOperatedClientConnection).setAttribute(paramString, paramObject);
    }
  }
  
  public void setIdleDuration(long paramLong, TimeUnit paramTimeUnit)
  {
    if (paramLong > 0L) {
      this.duration = paramTimeUnit.toMillis(paramLong);
    } else {
      this.duration = -1L;
    }
  }
  
  public void setSocketTimeout(int paramInt)
  {
    ensureConnection().setSocketTimeout(paramInt);
  }
  
  public void setState(Object paramObject)
  {
    ensurePoolEntry().setState(paramObject);
  }
  
  public void shutdown()
    throws IOException
  {
    HttpPoolEntry localHttpPoolEntry = this.poolEntry;
    if (localHttpPoolEntry != null)
    {
      OperatedClientConnection localOperatedClientConnection = (OperatedClientConnection)localHttpPoolEntry.getConnection();
      localHttpPoolEntry.getTracker().reset();
      localOperatedClientConnection.shutdown();
    }
  }
  
  public void tunnelProxy(HttpHost paramHttpHost, boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpHost, "Next proxy");
    Args.notNull(paramHttpParams, "HTTP parameters");
    try
    {
      if (this.poolEntry != null)
      {
        Object localObject = this.poolEntry.getTracker();
        Asserts.notNull(localObject, "Route tracker");
        Asserts.check(((RouteTracker)localObject).isConnected(), "Connection not open");
        localObject = (OperatedClientConnection)this.poolEntry.getConnection();
        ((OperatedClientConnection)localObject).update(null, paramHttpHost, paramBoolean, paramHttpParams);
        try
        {
          if (this.poolEntry != null)
          {
            this.poolEntry.getTracker().tunnelProxy(paramHttpHost, paramBoolean);
            return;
          }
          paramHttpHost = new java/io/InterruptedIOException;
          paramHttpHost.<init>();
          throw paramHttpHost;
        }
        finally {}
      }
      paramHttpHost = new cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException;
      paramHttpHost.<init>();
      throw paramHttpHost;
    }
    finally {}
  }
  
  public void tunnelTarget(boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpParams, "HTTP parameters");
    try
    {
      if (this.poolEntry != null)
      {
        Object localObject = this.poolEntry.getTracker();
        Asserts.notNull(localObject, "Route tracker");
        Asserts.check(((RouteTracker)localObject).isConnected(), "Connection not open");
        boolean bool;
        if (!((RouteTracker)localObject).isTunnelled()) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Connection is already tunnelled");
        localObject = ((RouteTracker)localObject).getTargetHost();
        OperatedClientConnection localOperatedClientConnection = (OperatedClientConnection)this.poolEntry.getConnection();
        localOperatedClientConnection.update(null, (HttpHost)localObject, paramBoolean, paramHttpParams);
        try
        {
          if (this.poolEntry != null)
          {
            this.poolEntry.getTracker().tunnelTarget(paramBoolean);
            return;
          }
          paramHttpParams = new java/io/InterruptedIOException;
          paramHttpParams.<init>();
          throw paramHttpParams;
        }
        finally {}
      }
      paramHttpParams = new cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException;
      paramHttpParams.<init>();
      throw paramHttpParams;
    }
    finally {}
  }
  
  public void unmarkReusable()
  {
    this.reusable = false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */