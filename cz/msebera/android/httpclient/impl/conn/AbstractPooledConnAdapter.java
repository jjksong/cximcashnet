package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.IOException;

@Deprecated
public abstract class AbstractPooledConnAdapter
  extends AbstractClientConnAdapter
{
  protected volatile AbstractPoolEntry poolEntry;
  
  protected AbstractPooledConnAdapter(ClientConnectionManager paramClientConnectionManager, AbstractPoolEntry paramAbstractPoolEntry)
  {
    super(paramClientConnectionManager, paramAbstractPoolEntry.connection);
    this.poolEntry = paramAbstractPoolEntry;
  }
  
  @Deprecated
  protected final void assertAttached()
  {
    if (this.poolEntry != null) {
      return;
    }
    throw new ConnectionShutdownException();
  }
  
  protected void assertValid(AbstractPoolEntry paramAbstractPoolEntry)
  {
    if ((!isReleased()) && (paramAbstractPoolEntry != null)) {
      return;
    }
    throw new ConnectionShutdownException();
  }
  
  public void close()
    throws IOException
  {
    Object localObject = getPoolEntry();
    if (localObject != null) {
      ((AbstractPoolEntry)localObject).shutdownEntry();
    }
    localObject = getWrappedConnection();
    if (localObject != null) {
      ((OperatedClientConnection)localObject).close();
    }
  }
  
  protected void detach()
  {
    try
    {
      this.poolEntry = null;
      super.detach();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public String getId()
  {
    return null;
  }
  
  @Deprecated
  protected AbstractPoolEntry getPoolEntry()
  {
    return this.poolEntry;
  }
  
  public HttpRoute getRoute()
  {
    Object localObject = getPoolEntry();
    assertValid((AbstractPoolEntry)localObject);
    if (((AbstractPoolEntry)localObject).tracker == null) {
      localObject = null;
    } else {
      localObject = ((AbstractPoolEntry)localObject).tracker.toRoute();
    }
    return (HttpRoute)localObject;
  }
  
  public Object getState()
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    return localAbstractPoolEntry.getState();
  }
  
  public void layerProtocol(HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    localAbstractPoolEntry.layerProtocol(paramHttpContext, paramHttpParams);
  }
  
  public void open(HttpRoute paramHttpRoute, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    localAbstractPoolEntry.open(paramHttpRoute, paramHttpContext, paramHttpParams);
  }
  
  public void setState(Object paramObject)
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    localAbstractPoolEntry.setState(paramObject);
  }
  
  public void shutdown()
    throws IOException
  {
    Object localObject = getPoolEntry();
    if (localObject != null) {
      ((AbstractPoolEntry)localObject).shutdownEntry();
    }
    localObject = getWrappedConnection();
    if (localObject != null) {
      ((OperatedClientConnection)localObject).shutdown();
    }
  }
  
  public void tunnelProxy(HttpHost paramHttpHost, boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    localAbstractPoolEntry.tunnelProxy(paramHttpHost, paramBoolean, paramHttpParams);
  }
  
  public void tunnelTarget(boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    AbstractPoolEntry localAbstractPoolEntry = getPoolEntry();
    assertValid(localAbstractPoolEntry);
    localAbstractPoolEntry.tunnelTarget(paramBoolean, paramHttpParams);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/AbstractPooledConnAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */