package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.UnsupportedSchemeException;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class DefaultSchemePortResolver
  implements SchemePortResolver
{
  public static final DefaultSchemePortResolver INSTANCE = new DefaultSchemePortResolver();
  
  public int resolve(HttpHost paramHttpHost)
    throws UnsupportedSchemeException
  {
    Args.notNull(paramHttpHost, "HTTP host");
    int i = paramHttpHost.getPort();
    if (i > 0) {
      return i;
    }
    paramHttpHost = paramHttpHost.getSchemeName();
    if (paramHttpHost.equalsIgnoreCase("http")) {
      return 80;
    }
    if (paramHttpHost.equalsIgnoreCase("https")) {
      return 443;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramHttpHost);
    localStringBuilder.append(" protocol is not supported");
    throw new UnsupportedSchemeException(localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/DefaultSchemePortResolver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */