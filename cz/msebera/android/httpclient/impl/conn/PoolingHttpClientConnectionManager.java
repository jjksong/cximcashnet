package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.config.Registry;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException;
import cz.msebera.android.httpclient.conn.ConnectionRequest;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.HttpConnectionFactory;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.PlainConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.ConnFactory;
import cz.msebera.android.httpclient.pool.ConnPoolControl;
import cz.msebera.android.httpclient.pool.PoolStats;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

@ThreadSafe
public class PoolingHttpClientConnectionManager
  implements HttpClientConnectionManager, ConnPoolControl<HttpRoute>, Closeable
{
  private final ConfigData configData = new ConfigData();
  private final HttpClientConnectionOperator connectionOperator;
  private final AtomicBoolean isShutDown;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final CPool pool;
  
  public PoolingHttpClientConnectionManager()
  {
    this(getDefaultRegistry());
  }
  
  public PoolingHttpClientConnectionManager(long paramLong, TimeUnit paramTimeUnit)
  {
    this(getDefaultRegistry(), null, null, null, paramLong, paramTimeUnit);
  }
  
  public PoolingHttpClientConnectionManager(Registry<ConnectionSocketFactory> paramRegistry)
  {
    this(paramRegistry, null, null);
  }
  
  public PoolingHttpClientConnectionManager(Registry<ConnectionSocketFactory> paramRegistry, DnsResolver paramDnsResolver)
  {
    this(paramRegistry, null, paramDnsResolver);
  }
  
  public PoolingHttpClientConnectionManager(Registry<ConnectionSocketFactory> paramRegistry, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory)
  {
    this(paramRegistry, paramHttpConnectionFactory, null);
  }
  
  public PoolingHttpClientConnectionManager(Registry<ConnectionSocketFactory> paramRegistry, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory, DnsResolver paramDnsResolver)
  {
    this(paramRegistry, paramHttpConnectionFactory, null, paramDnsResolver, -1L, TimeUnit.MILLISECONDS);
  }
  
  public PoolingHttpClientConnectionManager(Registry<ConnectionSocketFactory> paramRegistry, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory, SchemePortResolver paramSchemePortResolver, DnsResolver paramDnsResolver, long paramLong, TimeUnit paramTimeUnit)
  {
    this.pool = new CPool(new InternalConnectionFactory(this.configData, paramHttpConnectionFactory), 2, 20, paramLong, paramTimeUnit);
    this.connectionOperator = new HttpClientConnectionOperator(paramRegistry, paramSchemePortResolver, paramDnsResolver);
    this.isShutDown = new AtomicBoolean(false);
  }
  
  public PoolingHttpClientConnectionManager(HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory)
  {
    this(getDefaultRegistry(), paramHttpConnectionFactory, null);
  }
  
  PoolingHttpClientConnectionManager(CPool paramCPool, Lookup<ConnectionSocketFactory> paramLookup, SchemePortResolver paramSchemePortResolver, DnsResolver paramDnsResolver)
  {
    this.pool = paramCPool;
    this.connectionOperator = new HttpClientConnectionOperator(paramLookup, paramSchemePortResolver, paramDnsResolver);
    this.isShutDown = new AtomicBoolean(false);
  }
  
  private String format(HttpRoute paramHttpRoute, Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[route: ");
    localStringBuilder.append(paramHttpRoute);
    localStringBuilder.append("]");
    if (paramObject != null)
    {
      localStringBuilder.append("[state: ");
      localStringBuilder.append(paramObject);
      localStringBuilder.append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String format(CPoolEntry paramCPoolEntry)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[id: ");
    localStringBuilder.append(paramCPoolEntry.getId());
    localStringBuilder.append("]");
    localStringBuilder.append("[route: ");
    localStringBuilder.append(paramCPoolEntry.getRoute());
    localStringBuilder.append("]");
    paramCPoolEntry = paramCPoolEntry.getState();
    if (paramCPoolEntry != null)
    {
      localStringBuilder.append("[state: ");
      localStringBuilder.append(paramCPoolEntry);
      localStringBuilder.append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String formatStats(HttpRoute paramHttpRoute)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    PoolStats localPoolStats = this.pool.getTotalStats();
    paramHttpRoute = this.pool.getStats(paramHttpRoute);
    localStringBuilder.append("[total kept alive: ");
    localStringBuilder.append(localPoolStats.getAvailable());
    localStringBuilder.append("; ");
    localStringBuilder.append("route allocated: ");
    localStringBuilder.append(paramHttpRoute.getLeased() + paramHttpRoute.getAvailable());
    localStringBuilder.append(" of ");
    localStringBuilder.append(paramHttpRoute.getMax());
    localStringBuilder.append("; ");
    localStringBuilder.append("total allocated: ");
    localStringBuilder.append(localPoolStats.getLeased() + localPoolStats.getAvailable());
    localStringBuilder.append(" of ");
    localStringBuilder.append(localPoolStats.getMax());
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  private static Registry<ConnectionSocketFactory> getDefaultRegistry()
  {
    return RegistryBuilder.create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", SSLConnectionSocketFactory.getSocketFactory()).build();
  }
  
  public void close()
  {
    shutdown();
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    this.pool.closeExpired();
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Closing connections idle longer than ");
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" ");
      localStringBuilder.append(paramTimeUnit);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    this.pool.closeIdle(paramLong, paramTimeUnit);
  }
  
  public void connect(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, int paramInt, HttpContext paramHttpContext)
    throws IOException
  {
    Args.notNull(paramHttpClientConnection, "Managed Connection");
    Args.notNull(paramHttpRoute, "HTTP route");
    try
    {
      ManagedHttpClientConnection localManagedHttpClientConnection = (ManagedHttpClientConnection)CPoolProxy.getPoolEntry(paramHttpClientConnection).getConnection();
      HttpHost localHttpHost;
      if (paramHttpRoute.getProxyHost() != null) {
        localHttpHost = paramHttpRoute.getProxyHost();
      } else {
        localHttpHost = paramHttpRoute.getTargetHost();
      }
      InetSocketAddress localInetSocketAddress = paramHttpRoute.getLocalSocketAddress();
      paramHttpRoute = this.configData.getSocketConfig(localHttpHost);
      paramHttpClientConnection = paramHttpRoute;
      if (paramHttpRoute == null) {
        paramHttpClientConnection = this.configData.getDefaultSocketConfig();
      }
      if (paramHttpClientConnection == null) {
        paramHttpClientConnection = SocketConfig.DEFAULT;
      }
      this.connectionOperator.connect(localManagedHttpClientConnection, localHttpHost, localInetSocketAddress, paramInt, paramHttpClientConnection, paramHttpContext);
      return;
    }
    finally {}
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  public ConnectionConfig getConnectionConfig(HttpHost paramHttpHost)
  {
    return this.configData.getConnectionConfig(paramHttpHost);
  }
  
  public ConnectionConfig getDefaultConnectionConfig()
  {
    return this.configData.getDefaultConnectionConfig();
  }
  
  public int getDefaultMaxPerRoute()
  {
    return this.pool.getDefaultMaxPerRoute();
  }
  
  public SocketConfig getDefaultSocketConfig()
  {
    return this.configData.getDefaultSocketConfig();
  }
  
  public int getMaxPerRoute(HttpRoute paramHttpRoute)
  {
    return this.pool.getMaxPerRoute(paramHttpRoute);
  }
  
  public int getMaxTotal()
  {
    return this.pool.getMaxTotal();
  }
  
  public SocketConfig getSocketConfig(HttpHost paramHttpHost)
  {
    return this.configData.getSocketConfig(paramHttpHost);
  }
  
  public PoolStats getStats(HttpRoute paramHttpRoute)
  {
    return this.pool.getStats(paramHttpRoute);
  }
  
  public PoolStats getTotalStats()
  {
    return this.pool.getTotalStats();
  }
  
  protected HttpClientConnection leaseConnection(Future<CPoolEntry> paramFuture, long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, ConnectionPoolTimeoutException
  {
    try
    {
      paramTimeUnit = (CPoolEntry)paramFuture.get(paramLong, paramTimeUnit);
      if ((paramTimeUnit != null) && (!paramFuture.isCancelled()))
      {
        boolean bool;
        if (paramTimeUnit.getConnection() != null) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Pool entry with no connection");
        if (this.log.isDebugEnabled())
        {
          paramFuture = this.log;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Connection leased: ");
          localStringBuilder.append(format(paramTimeUnit));
          localStringBuilder.append(formatStats((HttpRoute)paramTimeUnit.getRoute()));
          paramFuture.debug(localStringBuilder.toString());
        }
        return CPoolProxy.newProxy(paramTimeUnit);
      }
      paramFuture = new java/lang/InterruptedException;
      paramFuture.<init>();
      throw paramFuture;
    }
    catch (TimeoutException paramFuture)
    {
      throw new ConnectionPoolTimeoutException("Timeout waiting for connection from pool");
    }
  }
  
  /* Error */
  public void releaseConnection(HttpClientConnection paramHttpClientConnection, Object paramObject, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 369
    //   4: invokestatic 248	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   7: pop
    //   8: aload_1
    //   9: monitorenter
    //   10: aload_1
    //   11: invokestatic 372	cz/msebera/android/httpclient/impl/conn/CPoolProxy:detach	(Lcz/msebera/android/httpclient/HttpClientConnection;)Lcz/msebera/android/httpclient/impl/conn/CPoolEntry;
    //   14: astore 10
    //   16: aload 10
    //   18: ifnonnull +6 -> 24
    //   21: aload_1
    //   22: monitorexit
    //   23: return
    //   24: aload 10
    //   26: invokevirtual 259	cz/msebera/android/httpclient/impl/conn/CPoolEntry:getConnection	()Ljava/lang/Object;
    //   29: checkcast 261	cz/msebera/android/httpclient/conn/ManagedHttpClientConnection
    //   32: astore 11
    //   34: iconst_1
    //   35: istore 9
    //   37: iconst_1
    //   38: istore 8
    //   40: aload 11
    //   42: invokeinterface 375 1 0
    //   47: ifeq +177 -> 224
    //   50: aload 5
    //   52: ifnull +6 -> 58
    //   55: goto +8 -> 63
    //   58: getstatic 61	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   61: astore 5
    //   63: aload 10
    //   65: aload_2
    //   66: invokevirtual 378	cz/msebera/android/httpclient/impl/conn/CPoolEntry:setState	(Ljava/lang/Object;)V
    //   69: aload 10
    //   71: lload_3
    //   72: aload 5
    //   74: invokevirtual 381	cz/msebera/android/httpclient/impl/conn/CPoolEntry:updateExpiry	(JLjava/util/concurrent/TimeUnit;)V
    //   77: aload_0
    //   78: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   81: invokevirtual 226	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   84: ifeq +140 -> 224
    //   87: lload_3
    //   88: lconst_0
    //   89: lcmp
    //   90: ifle +67 -> 157
    //   93: new 111	java/lang/StringBuilder
    //   96: astore_2
    //   97: aload_2
    //   98: invokespecial 112	java/lang/StringBuilder:<init>	()V
    //   101: aload_2
    //   102: ldc_w 383
    //   105: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload 5
    //   111: lload_3
    //   112: invokevirtual 387	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   115: lstore_3
    //   116: lload_3
    //   117: l2d
    //   118: dstore 6
    //   120: dload 6
    //   122: invokestatic 393	java/lang/Double:isNaN	(D)Z
    //   125: pop
    //   126: dload 6
    //   128: ldc2_w 394
    //   131: ddiv
    //   132: dstore 6
    //   134: aload_2
    //   135: dload 6
    //   137: invokevirtual 398	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   140: pop
    //   141: aload_2
    //   142: ldc_w 400
    //   145: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload_2
    //   150: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   153: astore_2
    //   154: goto +7 -> 161
    //   157: ldc_w 402
    //   160: astore_2
    //   161: aload_0
    //   162: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   165: astore 5
    //   167: new 111	java/lang/StringBuilder
    //   170: astore 12
    //   172: aload 12
    //   174: invokespecial 112	java/lang/StringBuilder:<init>	()V
    //   177: aload 12
    //   179: ldc_w 404
    //   182: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: aload 12
    //   188: aload_0
    //   189: aload 10
    //   191: invokespecial 352	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/CPoolEntry;)Ljava/lang/String;
    //   194: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   197: pop
    //   198: aload 12
    //   200: ldc_w 406
    //   203: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: pop
    //   207: aload 12
    //   209: aload_2
    //   210: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload 5
    //   216: aload 12
    //   218: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   221: invokevirtual 218	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   224: aload_0
    //   225: getfield 88	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/CPool;
    //   228: astore_2
    //   229: aload 11
    //   231: invokeinterface 375 1 0
    //   236: ifeq +14 -> 250
    //   239: aload 10
    //   241: invokevirtual 409	cz/msebera/android/httpclient/impl/conn/CPoolEntry:isRouteComplete	()Z
    //   244: ifeq +6 -> 250
    //   247: goto +6 -> 253
    //   250: iconst_0
    //   251: istore 8
    //   253: aload_2
    //   254: aload 10
    //   256: iload 8
    //   258: invokevirtual 413	cz/msebera/android/httpclient/impl/conn/CPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   261: aload_0
    //   262: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   265: invokevirtual 226	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   268: ifeq +62 -> 330
    //   271: aload_0
    //   272: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   275: astore 5
    //   277: new 111	java/lang/StringBuilder
    //   280: astore_2
    //   281: aload_2
    //   282: invokespecial 112	java/lang/StringBuilder:<init>	()V
    //   285: aload_2
    //   286: ldc_w 415
    //   289: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: aload_2
    //   294: aload_0
    //   295: aload 10
    //   297: invokespecial 352	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/CPoolEntry;)Ljava/lang/String;
    //   300: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: aload_2
    //   305: aload_0
    //   306: aload 10
    //   308: invokevirtual 141	cz/msebera/android/httpclient/impl/conn/CPoolEntry:getRoute	()Ljava/lang/Object;
    //   311: checkcast 263	cz/msebera/android/httpclient/conn/routing/HttpRoute
    //   314: invokespecial 354	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:formatStats	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;)Ljava/lang/String;
    //   317: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   320: pop
    //   321: aload 5
    //   323: aload_2
    //   324: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   327: invokevirtual 218	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   330: aload_1
    //   331: monitorexit
    //   332: return
    //   333: astore_2
    //   334: aload_0
    //   335: getfield 88	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/CPool;
    //   338: astore 5
    //   340: aload 11
    //   342: invokeinterface 375 1 0
    //   347: ifeq +18 -> 365
    //   350: aload 10
    //   352: invokevirtual 409	cz/msebera/android/httpclient/impl/conn/CPoolEntry:isRouteComplete	()Z
    //   355: ifeq +10 -> 365
    //   358: iload 9
    //   360: istore 8
    //   362: goto +6 -> 368
    //   365: iconst_0
    //   366: istore 8
    //   368: aload 5
    //   370: aload 10
    //   372: iload 8
    //   374: invokevirtual 413	cz/msebera/android/httpclient/impl/conn/CPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   377: aload_0
    //   378: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   381: invokevirtual 226	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   384: ifeq +68 -> 452
    //   387: aload_0
    //   388: getfield 75	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   391: astore 11
    //   393: new 111	java/lang/StringBuilder
    //   396: astore 5
    //   398: aload 5
    //   400: invokespecial 112	java/lang/StringBuilder:<init>	()V
    //   403: aload 5
    //   405: ldc_w 415
    //   408: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   411: pop
    //   412: aload 5
    //   414: aload_0
    //   415: aload 10
    //   417: invokespecial 352	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/CPoolEntry;)Ljava/lang/String;
    //   420: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   423: pop
    //   424: aload 5
    //   426: aload_0
    //   427: aload 10
    //   429: invokevirtual 141	cz/msebera/android/httpclient/impl/conn/CPoolEntry:getRoute	()Ljava/lang/Object;
    //   432: checkcast 263	cz/msebera/android/httpclient/conn/routing/HttpRoute
    //   435: invokespecial 354	cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager:formatStats	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;)Ljava/lang/String;
    //   438: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   441: pop
    //   442: aload 11
    //   444: aload 5
    //   446: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   449: invokevirtual 218	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   452: aload_2
    //   453: athrow
    //   454: astore_2
    //   455: aload_1
    //   456: monitorexit
    //   457: aload_2
    //   458: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	459	0	this	PoolingHttpClientConnectionManager
    //   0	459	1	paramHttpClientConnection	HttpClientConnection
    //   0	459	2	paramObject	Object
    //   0	459	3	paramLong	long
    //   0	459	5	paramTimeUnit	TimeUnit
    //   118	18	6	d	double
    //   38	335	8	bool1	boolean
    //   35	324	9	bool2	boolean
    //   14	414	10	localCPoolEntry	CPoolEntry
    //   32	411	11	localObject	Object
    //   170	47	12	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   40	50	333	finally
    //   58	63	333	finally
    //   63	87	333	finally
    //   93	116	333	finally
    //   134	154	333	finally
    //   161	224	333	finally
    //   10	16	454	finally
    //   21	23	454	finally
    //   24	34	454	finally
    //   224	247	454	finally
    //   253	330	454	finally
    //   330	332	454	finally
    //   334	358	454	finally
    //   368	452	454	finally
    //   452	454	454	finally
    //   455	457	454	finally
  }
  
  public ConnectionRequest requestConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Connection request: ");
      localStringBuilder.append(format(paramHttpRoute, paramObject));
      localStringBuilder.append(formatStats(paramHttpRoute));
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    new ConnectionRequest()
    {
      public boolean cancel()
      {
        return this.val$future.cancel(true);
      }
      
      public HttpClientConnection get(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
        throws InterruptedException, ExecutionException, ConnectionPoolTimeoutException
      {
        return PoolingHttpClientConnectionManager.this.leaseConnection(this.val$future, paramAnonymousLong, paramAnonymousTimeUnit);
      }
    };
  }
  
  public void routeComplete(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws IOException
  {
    Args.notNull(paramHttpClientConnection, "Managed Connection");
    Args.notNull(paramHttpRoute, "HTTP route");
    try
    {
      CPoolProxy.getPoolEntry(paramHttpClientConnection).markRouteComplete();
      return;
    }
    finally {}
  }
  
  public void setConnectionConfig(HttpHost paramHttpHost, ConnectionConfig paramConnectionConfig)
  {
    this.configData.setConnectionConfig(paramHttpHost, paramConnectionConfig);
  }
  
  public void setDefaultConnectionConfig(ConnectionConfig paramConnectionConfig)
  {
    this.configData.setDefaultConnectionConfig(paramConnectionConfig);
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    this.pool.setDefaultMaxPerRoute(paramInt);
  }
  
  public void setDefaultSocketConfig(SocketConfig paramSocketConfig)
  {
    this.configData.setDefaultSocketConfig(paramSocketConfig);
  }
  
  public void setMaxPerRoute(HttpRoute paramHttpRoute, int paramInt)
  {
    this.pool.setMaxPerRoute(paramHttpRoute, paramInt);
  }
  
  public void setMaxTotal(int paramInt)
  {
    this.pool.setMaxTotal(paramInt);
  }
  
  public void setSocketConfig(HttpHost paramHttpHost, SocketConfig paramSocketConfig)
  {
    this.configData.setSocketConfig(paramHttpHost, paramSocketConfig);
  }
  
  public void shutdown()
  {
    if (this.isShutDown.compareAndSet(false, true))
    {
      this.log.debug("Connection manager is shutting down");
      try
      {
        this.pool.shutdown();
      }
      catch (IOException localIOException)
      {
        this.log.debug("I/O exception shutting down connection manager", localIOException);
      }
      this.log.debug("Connection manager shut down");
    }
  }
  
  public void upgrade(HttpClientConnection paramHttpClientConnection, HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws IOException
  {
    Args.notNull(paramHttpClientConnection, "Managed Connection");
    Args.notNull(paramHttpRoute, "HTTP route");
    try
    {
      ManagedHttpClientConnection localManagedHttpClientConnection = (ManagedHttpClientConnection)CPoolProxy.getPoolEntry(paramHttpClientConnection).getConnection();
      this.connectionOperator.upgrade(localManagedHttpClientConnection, paramHttpRoute.getTargetHost(), paramHttpContext);
      return;
    }
    finally {}
  }
  
  static class ConfigData
  {
    private final Map<HttpHost, ConnectionConfig> connectionConfigMap = new ConcurrentHashMap();
    private volatile ConnectionConfig defaultConnectionConfig;
    private volatile SocketConfig defaultSocketConfig;
    private final Map<HttpHost, SocketConfig> socketConfigMap = new ConcurrentHashMap();
    
    public ConnectionConfig getConnectionConfig(HttpHost paramHttpHost)
    {
      return (ConnectionConfig)this.connectionConfigMap.get(paramHttpHost);
    }
    
    public ConnectionConfig getDefaultConnectionConfig()
    {
      return this.defaultConnectionConfig;
    }
    
    public SocketConfig getDefaultSocketConfig()
    {
      return this.defaultSocketConfig;
    }
    
    public SocketConfig getSocketConfig(HttpHost paramHttpHost)
    {
      return (SocketConfig)this.socketConfigMap.get(paramHttpHost);
    }
    
    public void setConnectionConfig(HttpHost paramHttpHost, ConnectionConfig paramConnectionConfig)
    {
      this.connectionConfigMap.put(paramHttpHost, paramConnectionConfig);
    }
    
    public void setDefaultConnectionConfig(ConnectionConfig paramConnectionConfig)
    {
      this.defaultConnectionConfig = paramConnectionConfig;
    }
    
    public void setDefaultSocketConfig(SocketConfig paramSocketConfig)
    {
      this.defaultSocketConfig = paramSocketConfig;
    }
    
    public void setSocketConfig(HttpHost paramHttpHost, SocketConfig paramSocketConfig)
    {
      this.socketConfigMap.put(paramHttpHost, paramSocketConfig);
    }
  }
  
  static class InternalConnectionFactory
    implements ConnFactory<HttpRoute, ManagedHttpClientConnection>
  {
    private final PoolingHttpClientConnectionManager.ConfigData configData;
    private final HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> connFactory;
    
    InternalConnectionFactory(PoolingHttpClientConnectionManager.ConfigData paramConfigData, HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory)
    {
      if (paramConfigData == null) {
        paramConfigData = new PoolingHttpClientConnectionManager.ConfigData();
      }
      this.configData = paramConfigData;
      if (paramHttpConnectionFactory == null) {
        paramHttpConnectionFactory = ManagedHttpClientConnectionFactory.INSTANCE;
      }
      this.connFactory = paramHttpConnectionFactory;
    }
    
    public ManagedHttpClientConnection create(HttpRoute paramHttpRoute)
      throws IOException
    {
      if (paramHttpRoute.getProxyHost() != null) {
        localObject2 = this.configData.getConnectionConfig(paramHttpRoute.getProxyHost());
      } else {
        localObject2 = null;
      }
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = this.configData.getConnectionConfig(paramHttpRoute.getTargetHost());
      }
      Object localObject2 = localObject1;
      if (localObject1 == null) {
        localObject2 = this.configData.getDefaultConnectionConfig();
      }
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = ConnectionConfig.DEFAULT;
      }
      return (ManagedHttpClientConnection)this.connFactory.create(paramHttpRoute, (ConnectionConfig)localObject1);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/PoolingHttpClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */