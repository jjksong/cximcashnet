package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseFactory;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.SocketHttpClientConnection;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

@Deprecated
@NotThreadSafe
public class DefaultClientConnection
  extends SocketHttpClientConnection
  implements OperatedClientConnection, ManagedHttpClientConnection, HttpContext
{
  private final Map<String, Object> attributes = new HashMap();
  private boolean connSecure;
  public HttpClientAndroidLog headerLog = new HttpClientAndroidLog("cz.msebera.android.httpclient.headers");
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private volatile boolean shutdown;
  private volatile Socket socket;
  private HttpHost targetHost;
  public HttpClientAndroidLog wireLog = new HttpClientAndroidLog("cz.msebera.android.httpclient.wire");
  
  public void bind(Socket paramSocket)
    throws IOException
  {
    bind(paramSocket, new BasicHttpParams());
  }
  
  public void close()
    throws IOException
  {
    try
    {
      super.close();
      if (this.log.isDebugEnabled())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("Connection ");
        localStringBuilder.append(this);
        localStringBuilder.append(" closed");
        localHttpClientAndroidLog.debug(localStringBuilder.toString());
      }
    }
    catch (IOException localIOException)
    {
      this.log.debug("I/O error closing connection", localIOException);
    }
  }
  
  protected HttpMessageParser<HttpResponse> createResponseParser(SessionInputBuffer paramSessionInputBuffer, HttpResponseFactory paramHttpResponseFactory, HttpParams paramHttpParams)
  {
    return new DefaultHttpResponseParser(paramSessionInputBuffer, null, paramHttpResponseFactory, paramHttpParams);
  }
  
  protected SessionInputBuffer createSessionInputBuffer(Socket paramSocket, int paramInt, HttpParams paramHttpParams)
    throws IOException
  {
    if (paramInt <= 0) {
      paramInt = 8192;
    }
    SessionInputBuffer localSessionInputBuffer = super.createSessionInputBuffer(paramSocket, paramInt, paramHttpParams);
    paramSocket = localSessionInputBuffer;
    if (this.wireLog.isDebugEnabled()) {
      paramSocket = new LoggingSessionInputBuffer(localSessionInputBuffer, new Wire(this.wireLog), HttpProtocolParams.getHttpElementCharset(paramHttpParams));
    }
    return paramSocket;
  }
  
  protected SessionOutputBuffer createSessionOutputBuffer(Socket paramSocket, int paramInt, HttpParams paramHttpParams)
    throws IOException
  {
    if (paramInt <= 0) {
      paramInt = 8192;
    }
    SessionOutputBuffer localSessionOutputBuffer = super.createSessionOutputBuffer(paramSocket, paramInt, paramHttpParams);
    paramSocket = localSessionOutputBuffer;
    if (this.wireLog.isDebugEnabled()) {
      paramSocket = new LoggingSessionOutputBuffer(localSessionOutputBuffer, new Wire(this.wireLog), HttpProtocolParams.getHttpElementCharset(paramHttpParams));
    }
    return paramSocket;
  }
  
  public Object getAttribute(String paramString)
  {
    return this.attributes.get(paramString);
  }
  
  public String getId()
  {
    return null;
  }
  
  public SSLSession getSSLSession()
  {
    if ((this.socket instanceof SSLSocket)) {
      return ((SSLSocket)this.socket).getSession();
    }
    return null;
  }
  
  public final Socket getSocket()
  {
    return this.socket;
  }
  
  public final HttpHost getTargetHost()
  {
    return this.targetHost;
  }
  
  public final boolean isSecure()
  {
    return this.connSecure;
  }
  
  public void openCompleted(boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpParams, "Parameters");
    assertNotOpen();
    this.connSecure = paramBoolean;
    bind(this.socket, paramHttpParams);
  }
  
  public void opening(Socket paramSocket, HttpHost paramHttpHost)
    throws IOException
  {
    assertNotOpen();
    this.socket = paramSocket;
    this.targetHost = paramHttpHost;
    if (!this.shutdown) {
      return;
    }
    paramSocket.close();
    throw new InterruptedIOException("Connection already shutdown");
  }
  
  public HttpResponse receiveResponseHeader()
    throws HttpException, IOException
  {
    HttpResponse localHttpResponse = super.receiveResponseHeader();
    Object localObject1;
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject1 = this.log;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Receiving response: ");
      ((StringBuilder)localObject2).append(localHttpResponse.getStatusLine());
      ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
    }
    if (this.headerLog.isDebugEnabled())
    {
      localObject2 = this.headerLog;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("<< ");
      ((StringBuilder)localObject1).append(localHttpResponse.getStatusLine().toString());
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
      for (Object localObject3 : localHttpResponse.getAllHeaders())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.headerLog;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("<< ");
        ((StringBuilder)localObject1).append(localObject3.toString());
        localHttpClientAndroidLog.debug(((StringBuilder)localObject1).toString());
      }
    }
    return localHttpResponse;
  }
  
  public Object removeAttribute(String paramString)
  {
    return this.attributes.remove(paramString);
  }
  
  public void sendRequestHeader(HttpRequest paramHttpRequest)
    throws HttpException, IOException
  {
    Object localObject2;
    Object localObject1;
    if (this.log.isDebugEnabled())
    {
      localObject2 = this.log;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Sending request: ");
      ((StringBuilder)localObject1).append(paramHttpRequest.getRequestLine());
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
    }
    super.sendRequestHeader(paramHttpRequest);
    if (this.headerLog.isDebugEnabled())
    {
      localObject1 = this.headerLog;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(">> ");
      ((StringBuilder)localObject2).append(paramHttpRequest.getRequestLine().toString());
      ((HttpClientAndroidLog)localObject1).debug(((StringBuilder)localObject2).toString());
      for (paramHttpRequest : paramHttpRequest.getAllHeaders())
      {
        localObject1 = this.headerLog;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(">> ");
        localStringBuilder.append(paramHttpRequest.toString());
        ((HttpClientAndroidLog)localObject1).debug(localStringBuilder.toString());
      }
    }
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    this.attributes.put(paramString, paramObject);
  }
  
  public void shutdown()
    throws IOException
  {
    this.shutdown = true;
    try
    {
      super.shutdown();
      if (this.log.isDebugEnabled())
      {
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("Connection ");
        ((StringBuilder)localObject).append(this);
        ((StringBuilder)localObject).append(" shut down");
        localHttpClientAndroidLog.debug(((StringBuilder)localObject).toString());
      }
      Object localObject = this.socket;
      if (localObject != null) {
        ((Socket)localObject).close();
      }
    }
    catch (IOException localIOException)
    {
      this.log.debug("I/O error shutting down connection", localIOException);
    }
  }
  
  public void update(Socket paramSocket, HttpHost paramHttpHost, boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    assertOpen();
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpParams, "Parameters");
    if (paramSocket != null)
    {
      this.socket = paramSocket;
      bind(paramSocket, paramHttpParams);
    }
    this.targetHost = paramHttpHost;
    this.connSecure = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/DefaultClientConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */