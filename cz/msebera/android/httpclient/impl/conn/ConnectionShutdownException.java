package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class ConnectionShutdownException
  extends IllegalStateException
{
  private static final long serialVersionUID = 5868657401162844497L;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/ConnectionShutdownException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */