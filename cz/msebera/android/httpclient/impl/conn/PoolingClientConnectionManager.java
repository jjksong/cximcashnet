package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.ConnPoolControl;
import cz.msebera.android.httpclient.pool.PoolStats;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Deprecated
@ThreadSafe
public class PoolingClientConnectionManager
  implements ClientConnectionManager, ConnPoolControl<HttpRoute>
{
  private final DnsResolver dnsResolver;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ClientConnectionOperator operator;
  private final HttpConnPool pool;
  private final SchemeRegistry schemeRegistry;
  
  public PoolingClientConnectionManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry)
  {
    this(paramSchemeRegistry, -1L, TimeUnit.MILLISECONDS);
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit)
  {
    this(paramSchemeRegistry, paramLong, paramTimeUnit, new SystemDefaultDnsResolver());
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit, DnsResolver paramDnsResolver)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    Args.notNull(paramDnsResolver, "DNS resolver");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = paramDnsResolver;
    this.operator = createConnectionOperator(paramSchemeRegistry);
    this.pool = new HttpConnPool(this.log, this.operator, 2, 20, paramLong, paramTimeUnit);
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, DnsResolver paramDnsResolver)
  {
    this(paramSchemeRegistry, -1L, TimeUnit.MILLISECONDS, paramDnsResolver);
  }
  
  private String format(HttpRoute paramHttpRoute, Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[route: ");
    localStringBuilder.append(paramHttpRoute);
    localStringBuilder.append("]");
    if (paramObject != null)
    {
      localStringBuilder.append("[state: ");
      localStringBuilder.append(paramObject);
      localStringBuilder.append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String format(HttpPoolEntry paramHttpPoolEntry)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[id: ");
    localStringBuilder.append(paramHttpPoolEntry.getId());
    localStringBuilder.append("]");
    localStringBuilder.append("[route: ");
    localStringBuilder.append(paramHttpPoolEntry.getRoute());
    localStringBuilder.append("]");
    paramHttpPoolEntry = paramHttpPoolEntry.getState();
    if (paramHttpPoolEntry != null)
    {
      localStringBuilder.append("[state: ");
      localStringBuilder.append(paramHttpPoolEntry);
      localStringBuilder.append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String formatStats(HttpRoute paramHttpRoute)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    PoolStats localPoolStats = this.pool.getTotalStats();
    paramHttpRoute = this.pool.getStats(paramHttpRoute);
    localStringBuilder.append("[total kept alive: ");
    localStringBuilder.append(localPoolStats.getAvailable());
    localStringBuilder.append("; ");
    localStringBuilder.append("route allocated: ");
    localStringBuilder.append(paramHttpRoute.getLeased() + paramHttpRoute.getAvailable());
    localStringBuilder.append(" of ");
    localStringBuilder.append(paramHttpRoute.getMax());
    localStringBuilder.append("; ");
    localStringBuilder.append("total allocated: ");
    localStringBuilder.append(localPoolStats.getLeased() + localPoolStats.getAvailable());
    localStringBuilder.append(" of ");
    localStringBuilder.append(localPoolStats.getMax());
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    this.pool.closeExpired();
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Closing connections idle longer than ");
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" ");
      localStringBuilder.append(paramTimeUnit);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    this.pool.closeIdle(paramLong, paramTimeUnit);
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry, this.dnsResolver);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  public int getDefaultMaxPerRoute()
  {
    return this.pool.getDefaultMaxPerRoute();
  }
  
  public int getMaxPerRoute(HttpRoute paramHttpRoute)
  {
    return this.pool.getMaxPerRoute(paramHttpRoute);
  }
  
  public int getMaxTotal()
  {
    return this.pool.getMaxTotal();
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  public PoolStats getStats(HttpRoute paramHttpRoute)
  {
    return this.pool.getStats(paramHttpRoute);
  }
  
  public PoolStats getTotalStats()
  {
    return this.pool.getTotalStats();
  }
  
  ManagedClientConnection leaseConnection(Future<HttpPoolEntry> paramFuture, long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ConnectionPoolTimeoutException
  {
    try
    {
      paramTimeUnit = (HttpPoolEntry)paramFuture.get(paramLong, paramTimeUnit);
      if ((paramTimeUnit != null) && (!paramFuture.isCancelled()))
      {
        boolean bool;
        if (paramTimeUnit.getConnection() != null) {
          bool = true;
        } else {
          bool = false;
        }
        Asserts.check(bool, "Pool entry with no connection");
        if (this.log.isDebugEnabled())
        {
          paramFuture = this.log;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Connection leased: ");
          localStringBuilder.append(format(paramTimeUnit));
          localStringBuilder.append(formatStats((HttpRoute)paramTimeUnit.getRoute()));
          paramFuture.debug(localStringBuilder.toString());
        }
        return new ManagedClientConnectionImpl(this, this.operator, paramTimeUnit);
      }
      paramFuture = new java/lang/InterruptedException;
      paramFuture.<init>();
      throw paramFuture;
    }
    catch (TimeoutException paramFuture)
    {
      throw new ConnectionPoolTimeoutException("Timeout waiting for connection from pool");
    }
    catch (ExecutionException paramFuture)
    {
      paramTimeUnit = paramFuture.getCause();
      if (paramTimeUnit != null) {
        paramFuture = paramTimeUnit;
      }
      this.log.error("Unexpected exception leasing connection from pool", paramFuture);
      throw new InterruptedException();
    }
  }
  
  /* Error */
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 260
    //   4: ldc_w 287
    //   7: invokestatic 288	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   10: aload_1
    //   11: checkcast 260	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl
    //   14: astore 6
    //   16: aload 6
    //   18: invokevirtual 292	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:getManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   21: aload_0
    //   22: if_acmpne +9 -> 31
    //   25: iconst_1
    //   26: istore 5
    //   28: goto +6 -> 34
    //   31: iconst_0
    //   32: istore 5
    //   34: iload 5
    //   36: ldc_w 294
    //   39: invokestatic 252	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   42: aload 6
    //   44: monitorenter
    //   45: aload 6
    //   47: invokevirtual 298	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:detach	()Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   50: astore 7
    //   52: aload 7
    //   54: ifnonnull +7 -> 61
    //   57: aload 6
    //   59: monitorexit
    //   60: return
    //   61: aload 6
    //   63: invokevirtual 301	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isOpen	()Z
    //   66: ifeq +45 -> 111
    //   69: aload 6
    //   71: invokevirtual 304	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   74: istore 5
    //   76: iload 5
    //   78: ifne +33 -> 111
    //   81: aload 6
    //   83: invokevirtual 305	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:shutdown	()V
    //   86: goto +25 -> 111
    //   89: astore_1
    //   90: aload_0
    //   91: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   94: invokevirtual 180	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   97: ifeq +14 -> 111
    //   100: aload_0
    //   101: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   104: ldc_w 307
    //   107: aload_1
    //   108: invokevirtual 309	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   111: aload 6
    //   113: invokevirtual 304	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   116: ifeq +152 -> 268
    //   119: aload 4
    //   121: ifnull +9 -> 130
    //   124: aload 4
    //   126: astore_1
    //   127: goto +7 -> 134
    //   130: getstatic 43	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   133: astore_1
    //   134: aload 7
    //   136: lload_2
    //   137: aload_1
    //   138: invokevirtual 312	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:updateExpiry	(JLjava/util/concurrent/TimeUnit;)V
    //   141: aload_0
    //   142: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   145: invokevirtual 180	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   148: ifeq +120 -> 268
    //   151: lload_2
    //   152: lconst_0
    //   153: lcmp
    //   154: ifle +47 -> 201
    //   157: new 97	java/lang/StringBuilder
    //   160: astore_1
    //   161: aload_1
    //   162: invokespecial 98	java/lang/StringBuilder:<init>	()V
    //   165: aload_1
    //   166: ldc_w 314
    //   169: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload_1
    //   174: lload_2
    //   175: invokevirtual 185	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   178: pop
    //   179: aload_1
    //   180: ldc -69
    //   182: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: aload_1
    //   187: aload 4
    //   189: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload_1
    //   194: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   197: astore_1
    //   198: goto +7 -> 205
    //   201: ldc_w 316
    //   204: astore_1
    //   205: aload_0
    //   206: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   209: astore 8
    //   211: new 97	java/lang/StringBuilder
    //   214: astore 4
    //   216: aload 4
    //   218: invokespecial 98	java/lang/StringBuilder:<init>	()V
    //   221: aload 4
    //   223: ldc_w 318
    //   226: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   229: pop
    //   230: aload 4
    //   232: aload_0
    //   233: aload 7
    //   235: invokespecial 256	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;)Ljava/lang/String;
    //   238: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: aload 4
    //   244: ldc_w 320
    //   247: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload 4
    //   253: aload_1
    //   254: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   257: pop
    //   258: aload 8
    //   260: aload 4
    //   262: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   265: invokevirtual 171	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   268: aload_0
    //   269: getfield 92	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/HttpConnPool;
    //   272: aload 7
    //   274: aload 6
    //   276: invokevirtual 304	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   279: invokevirtual 324	cz/msebera/android/httpclient/impl/conn/HttpConnPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   282: aload_0
    //   283: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   286: invokevirtual 180	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   289: ifeq +62 -> 351
    //   292: aload_0
    //   293: getfield 65	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   296: astore 4
    //   298: new 97	java/lang/StringBuilder
    //   301: astore_1
    //   302: aload_1
    //   303: invokespecial 98	java/lang/StringBuilder:<init>	()V
    //   306: aload_1
    //   307: ldc_w 326
    //   310: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   313: pop
    //   314: aload_1
    //   315: aload_0
    //   316: aload 7
    //   318: invokespecial 256	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;)Ljava/lang/String;
    //   321: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   324: pop
    //   325: aload_1
    //   326: aload_0
    //   327: aload 7
    //   329: invokevirtual 127	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:getRoute	()Ljava/lang/Object;
    //   332: checkcast 213	cz/msebera/android/httpclient/conn/routing/HttpRoute
    //   335: invokespecial 258	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:formatStats	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;)Ljava/lang/String;
    //   338: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload 4
    //   344: aload_1
    //   345: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   348: invokevirtual 171	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   351: aload 6
    //   353: monitorexit
    //   354: return
    //   355: astore_1
    //   356: aload_0
    //   357: getfield 92	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/HttpConnPool;
    //   360: aload 7
    //   362: aload 6
    //   364: invokevirtual 304	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   367: invokevirtual 324	cz/msebera/android/httpclient/impl/conn/HttpConnPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   370: aload_1
    //   371: athrow
    //   372: astore_1
    //   373: aload 6
    //   375: monitorexit
    //   376: aload_1
    //   377: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	378	0	this	PoolingClientConnectionManager
    //   0	378	1	paramManagedClientConnection	ManagedClientConnection
    //   0	378	2	paramLong	long
    //   0	378	4	paramTimeUnit	TimeUnit
    //   26	51	5	bool	boolean
    //   14	360	6	localManagedClientConnectionImpl	ManagedClientConnectionImpl
    //   50	311	7	localHttpPoolEntry	HttpPoolEntry
    //   209	50	8	localHttpClientAndroidLog	HttpClientAndroidLog
    // Exception table:
    //   from	to	target	type
    //   81	86	89	java/io/IOException
    //   61	76	355	finally
    //   81	86	355	finally
    //   90	111	355	finally
    //   111	119	355	finally
    //   130	134	355	finally
    //   134	151	355	finally
    //   157	198	355	finally
    //   205	268	355	finally
    //   45	52	372	finally
    //   57	60	372	finally
    //   268	351	372	finally
    //   351	354	372	finally
    //   356	372	372	finally
    //   373	376	372	finally
  }
  
  public ClientConnectionRequest requestConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Connection request: ");
      localStringBuilder.append(format(paramHttpRoute, paramObject));
      localStringBuilder.append(formatStats(paramHttpRoute));
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    new ClientConnectionRequest()
    {
      public void abortRequest()
      {
        this.val$future.cancel(true);
      }
      
      public ManagedClientConnection getConnection(long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
        throws InterruptedException, ConnectionPoolTimeoutException
      {
        return PoolingClientConnectionManager.this.leaseConnection(this.val$future, paramAnonymousLong, paramAnonymousTimeUnit);
      }
    };
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    this.pool.setDefaultMaxPerRoute(paramInt);
  }
  
  public void setMaxPerRoute(HttpRoute paramHttpRoute, int paramInt)
  {
    this.pool.setMaxPerRoute(paramHttpRoute, paramInt);
  }
  
  public void setMaxTotal(int paramInt)
  {
    this.pool.setMaxTotal(paramInt);
  }
  
  public void shutdown()
  {
    this.log.debug("Connection manager is shutting down");
    try
    {
      this.pool.shutdown();
    }
    catch (IOException localIOException)
    {
      this.log.debug("I/O exception shutting down connection manager", localIOException);
    }
    this.log.debug("Connection manager shut down");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */