package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.entity.ContentLengthStrategy;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.HttpMessageWriterFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

@NotThreadSafe
class LoggingManagedHttpClientConnection
  extends DefaultManagedHttpClientConnection
{
  private final HttpClientAndroidLog headerlog;
  public HttpClientAndroidLog log;
  private final Wire wire;
  
  public LoggingManagedHttpClientConnection(String paramString, HttpClientAndroidLog paramHttpClientAndroidLog1, HttpClientAndroidLog paramHttpClientAndroidLog2, HttpClientAndroidLog paramHttpClientAndroidLog3, int paramInt1, int paramInt2, CharsetDecoder paramCharsetDecoder, CharsetEncoder paramCharsetEncoder, MessageConstraints paramMessageConstraints, ContentLengthStrategy paramContentLengthStrategy1, ContentLengthStrategy paramContentLengthStrategy2, HttpMessageWriterFactory<HttpRequest> paramHttpMessageWriterFactory, HttpMessageParserFactory<HttpResponse> paramHttpMessageParserFactory)
  {
    super(paramString, paramInt1, paramInt2, paramCharsetDecoder, paramCharsetEncoder, paramMessageConstraints, paramContentLengthStrategy1, paramContentLengthStrategy2, paramHttpMessageWriterFactory, paramHttpMessageParserFactory);
    this.log = paramHttpClientAndroidLog1;
    this.headerlog = paramHttpClientAndroidLog2;
    this.wire = new Wire(paramHttpClientAndroidLog3, paramString);
  }
  
  public void close()
    throws IOException
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(getId());
      localStringBuilder.append(": Close connection");
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    super.close();
  }
  
  protected InputStream getSocketInputStream(Socket paramSocket)
    throws IOException
  {
    InputStream localInputStream = super.getSocketInputStream(paramSocket);
    paramSocket = localInputStream;
    if (this.wire.enabled()) {
      paramSocket = new LoggingInputStream(localInputStream, this.wire);
    }
    return paramSocket;
  }
  
  protected OutputStream getSocketOutputStream(Socket paramSocket)
    throws IOException
  {
    OutputStream localOutputStream = super.getSocketOutputStream(paramSocket);
    paramSocket = localOutputStream;
    if (this.wire.enabled()) {
      paramSocket = new LoggingOutputStream(localOutputStream, this.wire);
    }
    return paramSocket;
  }
  
  protected void onRequestSubmitted(HttpRequest paramHttpRequest)
  {
    if ((paramHttpRequest != null) && (this.headerlog.isDebugEnabled()))
    {
      Object localObject2 = this.headerlog;
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(getId());
      ((StringBuilder)localObject1).append(" >> ");
      ((StringBuilder)localObject1).append(paramHttpRequest.getRequestLine().toString());
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
      for (Object localObject3 : paramHttpRequest.getAllHeaders())
      {
        localObject1 = this.headerlog;
        paramHttpRequest = new StringBuilder();
        paramHttpRequest.append(getId());
        paramHttpRequest.append(" >> ");
        paramHttpRequest.append(localObject3.toString());
        ((HttpClientAndroidLog)localObject1).debug(paramHttpRequest.toString());
      }
    }
  }
  
  protected void onResponseReceived(HttpResponse paramHttpResponse)
  {
    if ((paramHttpResponse != null) && (this.headerlog.isDebugEnabled()))
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.headerlog;
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(getId());
      ((StringBuilder)localObject1).append(" << ");
      ((StringBuilder)localObject1).append(paramHttpResponse.getStatusLine().toString());
      localHttpClientAndroidLog.debug(((StringBuilder)localObject1).toString());
      for (Object localObject2 : paramHttpResponse.getAllHeaders())
      {
        localHttpClientAndroidLog = this.headerlog;
        paramHttpResponse = new StringBuilder();
        paramHttpResponse.append(getId());
        paramHttpResponse.append(" << ");
        paramHttpResponse.append(localObject2.toString());
        localHttpClientAndroidLog.debug(paramHttpResponse.toString());
      }
    }
  }
  
  public void shutdown()
    throws IOException
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(getId());
      localStringBuilder.append(": Shutdown connection");
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    super.shutdown();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/LoggingManagedHttpClientConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */