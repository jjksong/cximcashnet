package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.PoolEntry;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@ThreadSafe
class CPoolEntry
  extends PoolEntry<HttpRoute, ManagedHttpClientConnection>
{
  public HttpClientAndroidLog log;
  private volatile boolean routeComplete;
  
  public CPoolEntry(HttpClientAndroidLog paramHttpClientAndroidLog, String paramString, HttpRoute paramHttpRoute, ManagedHttpClientConnection paramManagedHttpClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    super(paramString, paramHttpRoute, paramManagedHttpClientConnection, paramLong, paramTimeUnit);
    this.log = paramHttpClientAndroidLog;
  }
  
  public void close()
  {
    try
    {
      closeConnection();
    }
    catch (IOException localIOException)
    {
      this.log.debug("I/O error closing connection", localIOException);
    }
  }
  
  public void closeConnection()
    throws IOException
  {
    ((HttpClientConnection)getConnection()).close();
  }
  
  public boolean isClosed()
  {
    return ((HttpClientConnection)getConnection()).isOpen() ^ true;
  }
  
  public boolean isExpired(long paramLong)
  {
    boolean bool = super.isExpired(paramLong);
    if ((bool) && (this.log.isDebugEnabled()))
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Connection ");
      localStringBuilder.append(this);
      localStringBuilder.append(" expired @ ");
      localStringBuilder.append(new Date(getExpiry()));
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    return bool;
  }
  
  public boolean isRouteComplete()
  {
    return this.routeComplete;
  }
  
  public void markRouteComplete()
  {
    this.routeComplete = true;
  }
  
  public void shutdownConnection()
    throws IOException
  {
    ((HttpClientConnection)getConnection()).shutdown();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/CPoolEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */