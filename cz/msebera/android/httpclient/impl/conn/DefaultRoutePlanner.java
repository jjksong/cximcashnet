package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.conn.SchemePortResolver;
import cz.msebera.android.httpclient.conn.UnsupportedSchemeException;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.net.InetAddress;

@Immutable
public class DefaultRoutePlanner
  implements HttpRoutePlanner
{
  private final SchemePortResolver schemePortResolver;
  
  public DefaultRoutePlanner(SchemePortResolver paramSchemePortResolver)
  {
    if (paramSchemePortResolver == null) {
      paramSchemePortResolver = DefaultSchemePortResolver.INSTANCE;
    }
    this.schemePortResolver = paramSchemePortResolver;
  }
  
  protected HttpHost determineProxy(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    return null;
  }
  
  public HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    Args.notNull(paramHttpRequest, "Request");
    if (paramHttpHost != null)
    {
      Object localObject = HttpClientContext.adapt(paramHttpContext).getRequestConfig();
      InetAddress localInetAddress = ((RequestConfig)localObject).getLocalAddress();
      HttpHost localHttpHost = ((RequestConfig)localObject).getProxy();
      localObject = localHttpHost;
      if (localHttpHost == null) {
        localObject = determineProxy(paramHttpHost, paramHttpRequest, paramHttpContext);
      }
      paramHttpRequest = paramHttpHost;
      if (paramHttpHost.getPort() <= 0) {
        try
        {
          paramHttpRequest = new HttpHost(paramHttpHost.getHostName(), this.schemePortResolver.resolve(paramHttpHost), paramHttpHost.getSchemeName());
        }
        catch (UnsupportedSchemeException paramHttpHost)
        {
          throw new HttpException(paramHttpHost.getMessage());
        }
      }
      boolean bool = paramHttpRequest.getSchemeName().equalsIgnoreCase("https");
      if (localObject == null) {
        return new HttpRoute(paramHttpRequest, localInetAddress, bool);
      }
      return new HttpRoute(paramHttpRequest, localInetAddress, (HttpHost)localObject, bool);
    }
    throw new ProtocolException("Target host is not specified");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/DefaultRoutePlanner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */