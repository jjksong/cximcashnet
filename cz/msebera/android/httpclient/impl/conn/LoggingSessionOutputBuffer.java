package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;
import cz.msebera.android.httpclient.io.SessionOutputBuffer;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.nio.charset.Charset;

@Deprecated
@Immutable
public class LoggingSessionOutputBuffer
  implements SessionOutputBuffer
{
  private final String charset;
  private final SessionOutputBuffer out;
  private final Wire wire;
  
  public LoggingSessionOutputBuffer(SessionOutputBuffer paramSessionOutputBuffer, Wire paramWire)
  {
    this(paramSessionOutputBuffer, paramWire, null);
  }
  
  public LoggingSessionOutputBuffer(SessionOutputBuffer paramSessionOutputBuffer, Wire paramWire, String paramString)
  {
    this.out = paramSessionOutputBuffer;
    this.wire = paramWire;
    if (paramString == null) {
      paramString = Consts.ASCII.name();
    }
    this.charset = paramString;
  }
  
  public void flush()
    throws IOException
  {
    this.out.flush();
  }
  
  public HttpTransportMetrics getMetrics()
  {
    return this.out.getMetrics();
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.out.write(paramInt);
    if (this.wire.enabled()) {
      this.wire.output(paramInt);
    }
  }
  
  public void write(byte[] paramArrayOfByte)
    throws IOException
  {
    this.out.write(paramArrayOfByte);
    if (this.wire.enabled()) {
      this.wire.output(paramArrayOfByte);
    }
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.out.write(paramArrayOfByte, paramInt1, paramInt2);
    if (this.wire.enabled()) {
      this.wire.output(paramArrayOfByte, paramInt1, paramInt2);
    }
  }
  
  public void writeLine(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    this.out.writeLine(paramCharArrayBuffer);
    if (this.wire.enabled())
    {
      paramCharArrayBuffer = new String(paramCharArrayBuffer.buffer(), 0, paramCharArrayBuffer.length());
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramCharArrayBuffer);
      localStringBuilder.append("\r\n");
      paramCharArrayBuffer = localStringBuilder.toString();
      this.wire.output(paramCharArrayBuffer.getBytes(this.charset));
    }
  }
  
  public void writeLine(String paramString)
    throws IOException
  {
    this.out.writeLine(paramString);
    if (this.wire.enabled())
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append("\r\n");
      paramString = localStringBuilder.toString();
      this.wire.output(paramString.getBytes(this.charset));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/LoggingSessionOutputBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */