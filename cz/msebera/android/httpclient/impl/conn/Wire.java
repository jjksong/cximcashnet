package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Immutable
public class Wire
{
  private final String id;
  public HttpClientAndroidLog log;
  
  public Wire(HttpClientAndroidLog paramHttpClientAndroidLog)
  {
    this(paramHttpClientAndroidLog, "");
  }
  
  public Wire(HttpClientAndroidLog paramHttpClientAndroidLog, String paramString)
  {
    this.log = paramHttpClientAndroidLog;
    this.id = paramString;
  }
  
  private void wire(String paramString, InputStream paramInputStream)
    throws IOException
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    for (;;)
    {
      int i = paramInputStream.read();
      if (i == -1) {
        break;
      }
      if (i == 13)
      {
        localStringBuilder1.append("[\\r]");
      }
      else if (i == 10)
      {
        localStringBuilder1.append("[\\n]\"");
        localStringBuilder1.insert(0, "\"");
        localStringBuilder1.insert(0, paramString);
        HttpClientAndroidLog localHttpClientAndroidLog = this.log;
        StringBuilder localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append(this.id);
        localStringBuilder2.append(" ");
        localStringBuilder2.append(localStringBuilder1.toString());
        localHttpClientAndroidLog.debug(localStringBuilder2.toString());
        localStringBuilder1.setLength(0);
      }
      else if ((i >= 32) && (i <= 127))
      {
        localStringBuilder1.append((char)i);
      }
      else
      {
        localStringBuilder1.append("[0x");
        localStringBuilder1.append(Integer.toHexString(i));
        localStringBuilder1.append("]");
      }
    }
    if (localStringBuilder1.length() > 0)
    {
      localStringBuilder1.append('"');
      localStringBuilder1.insert(0, '"');
      localStringBuilder1.insert(0, paramString);
      paramInputStream = this.log;
      paramString = new StringBuilder();
      paramString.append(this.id);
      paramString.append(" ");
      paramString.append(localStringBuilder1.toString());
      paramInputStream.debug(paramString.toString());
    }
  }
  
  public boolean enabled()
  {
    return this.log.isDebugEnabled();
  }
  
  public void input(int paramInt)
    throws IOException
  {
    input(new byte[] { (byte)paramInt });
  }
  
  public void input(InputStream paramInputStream)
    throws IOException
  {
    Args.notNull(paramInputStream, "Input");
    wire("<< ", paramInputStream);
  }
  
  public void input(String paramString)
    throws IOException
  {
    Args.notNull(paramString, "Input");
    input(paramString.getBytes());
  }
  
  public void input(byte[] paramArrayOfByte)
    throws IOException
  {
    Args.notNull(paramArrayOfByte, "Input");
    wire("<< ", new ByteArrayInputStream(paramArrayOfByte));
  }
  
  public void input(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    Args.notNull(paramArrayOfByte, "Input");
    wire("<< ", new ByteArrayInputStream(paramArrayOfByte, paramInt1, paramInt2));
  }
  
  public void output(int paramInt)
    throws IOException
  {
    output(new byte[] { (byte)paramInt });
  }
  
  public void output(InputStream paramInputStream)
    throws IOException
  {
    Args.notNull(paramInputStream, "Output");
    wire(">> ", paramInputStream);
  }
  
  public void output(String paramString)
    throws IOException
  {
    Args.notNull(paramString, "Output");
    output(paramString.getBytes());
  }
  
  public void output(byte[] paramArrayOfByte)
    throws IOException
  {
    Args.notNull(paramArrayOfByte, "Output");
    wire(">> ", new ByteArrayInputStream(paramArrayOfByte));
  }
  
  public void output(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    Args.notNull(paramArrayOfByte, "Output");
    wire(">> ", new ByteArrayInputStream(paramArrayOfByte, paramInt1, paramInt2));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/Wire.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */