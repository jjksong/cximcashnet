package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpConnectionMetrics;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLSession;

@NotThreadSafe
class CPoolProxy
  implements ManagedHttpClientConnection, HttpContext
{
  private volatile CPoolEntry poolEntry;
  
  CPoolProxy(CPoolEntry paramCPoolEntry)
  {
    this.poolEntry = paramCPoolEntry;
  }
  
  public static CPoolEntry detach(HttpClientConnection paramHttpClientConnection)
  {
    return getProxy(paramHttpClientConnection).detach();
  }
  
  public static CPoolEntry getPoolEntry(HttpClientConnection paramHttpClientConnection)
  {
    paramHttpClientConnection = getProxy(paramHttpClientConnection).getPoolEntry();
    if (paramHttpClientConnection != null) {
      return paramHttpClientConnection;
    }
    throw new ConnectionShutdownException();
  }
  
  private static CPoolProxy getProxy(HttpClientConnection paramHttpClientConnection)
  {
    if (CPoolProxy.class.isInstance(paramHttpClientConnection)) {
      return (CPoolProxy)CPoolProxy.class.cast(paramHttpClientConnection);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Unexpected connection proxy class: ");
    localStringBuilder.append(paramHttpClientConnection.getClass());
    throw new IllegalStateException(localStringBuilder.toString());
  }
  
  public static HttpClientConnection newProxy(CPoolEntry paramCPoolEntry)
  {
    return new CPoolProxy(paramCPoolEntry);
  }
  
  public void bind(Socket paramSocket)
    throws IOException
  {
    getValidConnection().bind(paramSocket);
  }
  
  public void close()
    throws IOException
  {
    CPoolEntry localCPoolEntry = this.poolEntry;
    if (localCPoolEntry != null) {
      localCPoolEntry.closeConnection();
    }
  }
  
  CPoolEntry detach()
  {
    CPoolEntry localCPoolEntry = this.poolEntry;
    this.poolEntry = null;
    return localCPoolEntry;
  }
  
  public void flush()
    throws IOException
  {
    getValidConnection().flush();
  }
  
  public Object getAttribute(String paramString)
  {
    ManagedHttpClientConnection localManagedHttpClientConnection = getValidConnection();
    if ((localManagedHttpClientConnection instanceof HttpContext)) {
      return ((HttpContext)localManagedHttpClientConnection).getAttribute(paramString);
    }
    return null;
  }
  
  ManagedHttpClientConnection getConnection()
  {
    CPoolEntry localCPoolEntry = this.poolEntry;
    if (localCPoolEntry == null) {
      return null;
    }
    return (ManagedHttpClientConnection)localCPoolEntry.getConnection();
  }
  
  public String getId()
  {
    return getValidConnection().getId();
  }
  
  public InetAddress getLocalAddress()
  {
    return getValidConnection().getLocalAddress();
  }
  
  public int getLocalPort()
  {
    return getValidConnection().getLocalPort();
  }
  
  public HttpConnectionMetrics getMetrics()
  {
    return getValidConnection().getMetrics();
  }
  
  CPoolEntry getPoolEntry()
  {
    return this.poolEntry;
  }
  
  public InetAddress getRemoteAddress()
  {
    return getValidConnection().getRemoteAddress();
  }
  
  public int getRemotePort()
  {
    return getValidConnection().getRemotePort();
  }
  
  public SSLSession getSSLSession()
  {
    return getValidConnection().getSSLSession();
  }
  
  public Socket getSocket()
  {
    return getValidConnection().getSocket();
  }
  
  public int getSocketTimeout()
  {
    return getValidConnection().getSocketTimeout();
  }
  
  ManagedHttpClientConnection getValidConnection()
  {
    ManagedHttpClientConnection localManagedHttpClientConnection = getConnection();
    if (localManagedHttpClientConnection != null) {
      return localManagedHttpClientConnection;
    }
    throw new ConnectionShutdownException();
  }
  
  public boolean isOpen()
  {
    CPoolEntry localCPoolEntry = this.poolEntry;
    if (localCPoolEntry != null) {
      return localCPoolEntry.isClosed() ^ true;
    }
    return false;
  }
  
  public boolean isResponseAvailable(int paramInt)
    throws IOException
  {
    return getValidConnection().isResponseAvailable(paramInt);
  }
  
  public boolean isStale()
  {
    ManagedHttpClientConnection localManagedHttpClientConnection = getConnection();
    if (localManagedHttpClientConnection != null) {
      return localManagedHttpClientConnection.isStale();
    }
    return true;
  }
  
  public void receiveResponseEntity(HttpResponse paramHttpResponse)
    throws HttpException, IOException
  {
    getValidConnection().receiveResponseEntity(paramHttpResponse);
  }
  
  public HttpResponse receiveResponseHeader()
    throws HttpException, IOException
  {
    return getValidConnection().receiveResponseHeader();
  }
  
  public Object removeAttribute(String paramString)
  {
    ManagedHttpClientConnection localManagedHttpClientConnection = getValidConnection();
    if ((localManagedHttpClientConnection instanceof HttpContext)) {
      return ((HttpContext)localManagedHttpClientConnection).removeAttribute(paramString);
    }
    return null;
  }
  
  public void sendRequestEntity(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    throws HttpException, IOException
  {
    getValidConnection().sendRequestEntity(paramHttpEntityEnclosingRequest);
  }
  
  public void sendRequestHeader(HttpRequest paramHttpRequest)
    throws HttpException, IOException
  {
    getValidConnection().sendRequestHeader(paramHttpRequest);
  }
  
  public void setAttribute(String paramString, Object paramObject)
  {
    ManagedHttpClientConnection localManagedHttpClientConnection = getValidConnection();
    if ((localManagedHttpClientConnection instanceof HttpContext)) {
      ((HttpContext)localManagedHttpClientConnection).setAttribute(paramString, paramObject);
    }
  }
  
  public void setSocketTimeout(int paramInt)
  {
    getValidConnection().setSocketTimeout(paramInt);
  }
  
  public void shutdown()
    throws IOException
  {
    CPoolEntry localCPoolEntry = this.poolEntry;
    if (localCPoolEntry != null) {
      localCPoolEntry.shutdownConnection();
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("CPoolProxy{");
    ManagedHttpClientConnection localManagedHttpClientConnection = getConnection();
    if (localManagedHttpClientConnection != null) {
      localStringBuilder.append(localManagedHttpClientConnection);
    } else {
      localStringBuilder.append("detached");
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/CPoolProxy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */