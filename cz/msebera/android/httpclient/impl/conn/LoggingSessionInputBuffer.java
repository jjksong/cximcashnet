package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.io.EofSensor;
import cz.msebera.android.httpclient.io.HttpTransportMetrics;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.IOException;
import java.nio.charset.Charset;

@Deprecated
@Immutable
public class LoggingSessionInputBuffer
  implements SessionInputBuffer, EofSensor
{
  private final String charset;
  private final EofSensor eofSensor;
  private final SessionInputBuffer in;
  private final Wire wire;
  
  public LoggingSessionInputBuffer(SessionInputBuffer paramSessionInputBuffer, Wire paramWire)
  {
    this(paramSessionInputBuffer, paramWire, null);
  }
  
  public LoggingSessionInputBuffer(SessionInputBuffer paramSessionInputBuffer, Wire paramWire, String paramString)
  {
    this.in = paramSessionInputBuffer;
    if ((paramSessionInputBuffer instanceof EofSensor)) {
      paramSessionInputBuffer = (EofSensor)paramSessionInputBuffer;
    } else {
      paramSessionInputBuffer = null;
    }
    this.eofSensor = paramSessionInputBuffer;
    this.wire = paramWire;
    if (paramString == null) {
      paramString = Consts.ASCII.name();
    }
    this.charset = paramString;
  }
  
  public HttpTransportMetrics getMetrics()
  {
    return this.in.getMetrics();
  }
  
  public boolean isDataAvailable(int paramInt)
    throws IOException
  {
    return this.in.isDataAvailable(paramInt);
  }
  
  public boolean isEof()
  {
    EofSensor localEofSensor = this.eofSensor;
    if (localEofSensor != null) {
      return localEofSensor.isEof();
    }
    return false;
  }
  
  public int read()
    throws IOException
  {
    int i = this.in.read();
    if ((this.wire.enabled()) && (i != -1)) {
      this.wire.input(i);
    }
    return i;
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    int i = this.in.read(paramArrayOfByte);
    if ((this.wire.enabled()) && (i > 0)) {
      this.wire.input(paramArrayOfByte, 0, i);
    }
    return i;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    paramInt2 = this.in.read(paramArrayOfByte, paramInt1, paramInt2);
    if ((this.wire.enabled()) && (paramInt2 > 0)) {
      this.wire.input(paramArrayOfByte, paramInt1, paramInt2);
    }
    return paramInt2;
  }
  
  public int readLine(CharArrayBuffer paramCharArrayBuffer)
    throws IOException
  {
    int j = this.in.readLine(paramCharArrayBuffer);
    if ((this.wire.enabled()) && (j >= 0))
    {
      int i = paramCharArrayBuffer.length();
      String str = new String(paramCharArrayBuffer.buffer(), i - j, j);
      paramCharArrayBuffer = new StringBuilder();
      paramCharArrayBuffer.append(str);
      paramCharArrayBuffer.append("\r\n");
      paramCharArrayBuffer = paramCharArrayBuffer.toString();
      this.wire.input(paramCharArrayBuffer.getBytes(this.charset));
    }
    return j;
  }
  
  public String readLine()
    throws IOException
  {
    String str = this.in.readLine();
    if ((this.wire.enabled()) && (str != null))
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("\r\n");
      localObject = ((StringBuilder)localObject).toString();
      this.wire.input(((String)localObject).getBytes(this.charset));
    }
    return str;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/LoggingSessionInputBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */