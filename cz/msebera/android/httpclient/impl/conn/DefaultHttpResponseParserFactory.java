package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseFactory;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.impl.DefaultHttpResponseFactory;
import cz.msebera.android.httpclient.io.HttpMessageParser;
import cz.msebera.android.httpclient.io.HttpMessageParserFactory;
import cz.msebera.android.httpclient.io.SessionInputBuffer;
import cz.msebera.android.httpclient.message.BasicLineParser;
import cz.msebera.android.httpclient.message.LineParser;

@Immutable
public class DefaultHttpResponseParserFactory
  implements HttpMessageParserFactory<HttpResponse>
{
  public static final DefaultHttpResponseParserFactory INSTANCE = new DefaultHttpResponseParserFactory();
  private final LineParser lineParser;
  private final HttpResponseFactory responseFactory;
  
  public DefaultHttpResponseParserFactory()
  {
    this(null, null);
  }
  
  public DefaultHttpResponseParserFactory(HttpResponseFactory paramHttpResponseFactory)
  {
    this(null, paramHttpResponseFactory);
  }
  
  public DefaultHttpResponseParserFactory(LineParser paramLineParser, HttpResponseFactory paramHttpResponseFactory)
  {
    if (paramLineParser == null) {
      paramLineParser = BasicLineParser.INSTANCE;
    }
    this.lineParser = paramLineParser;
    if (paramHttpResponseFactory == null) {
      paramHttpResponseFactory = DefaultHttpResponseFactory.INSTANCE;
    }
    this.responseFactory = paramHttpResponseFactory;
  }
  
  public HttpMessageParser<HttpResponse> create(SessionInputBuffer paramSessionInputBuffer, MessageConstraints paramMessageConstraints)
  {
    return new DefaultHttpResponseParser(paramSessionInputBuffer, this.lineParser, this.responseFactory, paramMessageConstraints);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/DefaultHttpResponseParserFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */