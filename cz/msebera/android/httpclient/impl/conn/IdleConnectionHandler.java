package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpConnection;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Deprecated
public class IdleConnectionHandler
{
  private final Map<HttpConnection, TimeValues> connectionToTimes = new HashMap();
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  public void add(HttpConnection paramHttpConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    long l = System.currentTimeMillis();
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Adding connection at: ");
      localStringBuilder.append(l);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    this.connectionToTimes.put(paramHttpConnection, new TimeValues(l, paramLong, paramTimeUnit));
  }
  
  public void closeExpiredConnections()
  {
    long l = System.currentTimeMillis();
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject2 = this.log;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Checking for expired connections, now: ");
      ((StringBuilder)localObject1).append(l);
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
    }
    Object localObject1 = this.connectionToTimes.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject3 = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = (HttpConnection)((Map.Entry)localObject3).getKey();
      localObject3 = (TimeValues)((Map.Entry)localObject3).getValue();
      if (((TimeValues)localObject3).timeExpires <= l)
      {
        if (this.log.isDebugEnabled())
        {
          HttpClientAndroidLog localHttpClientAndroidLog = this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Closing connection, expired @: ");
          localStringBuilder.append(((TimeValues)localObject3).timeExpires);
          localHttpClientAndroidLog.debug(localStringBuilder.toString());
        }
        try
        {
          ((HttpConnection)localObject2).close();
        }
        catch (IOException localIOException)
        {
          this.log.debug("I/O error closing connection", localIOException);
        }
      }
    }
  }
  
  public void closeIdleConnections(long paramLong)
  {
    long l = System.currentTimeMillis() - paramLong;
    Object localObject2;
    if (this.log.isDebugEnabled())
    {
      localObject2 = this.log;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Checking for connections, idle timeout: ");
      ((StringBuilder)localObject1).append(l);
      ((HttpClientAndroidLog)localObject2).debug(((StringBuilder)localObject1).toString());
    }
    Object localObject1 = this.connectionToTimes.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject3 = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = (HttpConnection)((Map.Entry)localObject3).getKey();
      paramLong = ((TimeValues)((Map.Entry)localObject3).getValue()).timeAdded;
      if (paramLong <= l)
      {
        if (this.log.isDebugEnabled())
        {
          localObject3 = this.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Closing idle connection, connection time: ");
          localStringBuilder.append(paramLong);
          ((HttpClientAndroidLog)localObject3).debug(localStringBuilder.toString());
        }
        try
        {
          ((HttpConnection)localObject2).close();
        }
        catch (IOException localIOException)
        {
          this.log.debug("I/O error closing connection", localIOException);
        }
      }
    }
  }
  
  public boolean remove(HttpConnection paramHttpConnection)
  {
    paramHttpConnection = (TimeValues)this.connectionToTimes.remove(paramHttpConnection);
    boolean bool = true;
    if (paramHttpConnection == null)
    {
      this.log.warn("Removing a connection that never existed!");
      return true;
    }
    if (System.currentTimeMillis() > paramHttpConnection.timeExpires) {
      bool = false;
    }
    return bool;
  }
  
  public void removeAll()
  {
    this.connectionToTimes.clear();
  }
  
  private static class TimeValues
  {
    private final long timeAdded;
    private final long timeExpires;
    
    TimeValues(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      this.timeAdded = paramLong1;
      if (paramLong2 > 0L) {
        this.timeExpires = (paramLong1 + paramTimeUnit.toMillis(paramLong2));
      } else {
        this.timeExpires = Long.MAX_VALUE;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/conn/IdleConnectionHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */