package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.cookie.SetCookie2;
import java.util.Date;

@NotThreadSafe
public class BasicClientCookie2
  extends BasicClientCookie
  implements SetCookie2
{
  private static final long serialVersionUID = -7744598295706617057L;
  private String commentURL;
  private boolean discard;
  private int[] ports;
  
  public BasicClientCookie2(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    BasicClientCookie2 localBasicClientCookie2 = (BasicClientCookie2)super.clone();
    int[] arrayOfInt = this.ports;
    if (arrayOfInt != null) {
      localBasicClientCookie2.ports = ((int[])arrayOfInt.clone());
    }
    return localBasicClientCookie2;
  }
  
  public String getCommentURL()
  {
    return this.commentURL;
  }
  
  public int[] getPorts()
  {
    return this.ports;
  }
  
  public boolean isExpired(Date paramDate)
  {
    boolean bool;
    if ((!this.discard) && (!super.isExpired(paramDate))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isPersistent()
  {
    boolean bool;
    if ((!this.discard) && (super.isPersistent())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void setCommentURL(String paramString)
  {
    this.commentURL = paramString;
  }
  
  public void setDiscard(boolean paramBoolean)
  {
    this.discard = paramBoolean;
  }
  
  public void setPorts(int[] paramArrayOfInt)
  {
    this.ports = paramArrayOfInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BasicClientCookie2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */