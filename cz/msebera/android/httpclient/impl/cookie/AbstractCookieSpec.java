package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieSpec;
import cz.msebera.android.httpclient.util.Args;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@NotThreadSafe
public abstract class AbstractCookieSpec
  implements CookieSpec
{
  private final Map<String, CookieAttributeHandler> attribHandlerMap = new HashMap(10);
  
  protected CookieAttributeHandler findAttribHandler(String paramString)
  {
    return (CookieAttributeHandler)this.attribHandlerMap.get(paramString);
  }
  
  protected CookieAttributeHandler getAttribHandler(String paramString)
  {
    Object localObject = findAttribHandler(paramString);
    if (localObject != null) {
      return (CookieAttributeHandler)localObject;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Handler not registered for ");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(" attribute.");
    throw new IllegalStateException(((StringBuilder)localObject).toString());
  }
  
  protected Collection<CookieAttributeHandler> getAttribHandlers()
  {
    return this.attribHandlerMap.values();
  }
  
  public void registerAttribHandler(String paramString, CookieAttributeHandler paramCookieAttributeHandler)
  {
    Args.notNull(paramString, "Attribute name");
    Args.notNull(paramCookieAttributeHandler, "Attribute handler");
    this.attribHandlerMap.put(paramString, paramCookieAttributeHandler);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/AbstractCookieSpec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */