package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.client.utils.Punycode;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class PublicSuffixFilter
  implements CookieAttributeHandler
{
  private Set<String> exceptions;
  private Set<String> suffixes;
  private final CookieAttributeHandler wrapped;
  
  public PublicSuffixFilter(CookieAttributeHandler paramCookieAttributeHandler)
  {
    this.wrapped = paramCookieAttributeHandler;
  }
  
  private boolean isForPublicSuffix(Cookie paramCookie)
  {
    Object localObject = paramCookie.getDomain();
    paramCookie = (Cookie)localObject;
    if (((String)localObject).startsWith(".")) {
      paramCookie = ((String)localObject).substring(1);
    }
    paramCookie = Punycode.toUnicode(paramCookie);
    localObject = this.exceptions;
    if ((localObject != null) && (((Set)localObject).contains(paramCookie))) {
      return false;
    }
    if (this.suffixes == null) {
      return false;
    }
    do
    {
      if (this.suffixes.contains(paramCookie)) {
        return true;
      }
      localObject = paramCookie;
      if (paramCookie.startsWith("*.")) {
        localObject = paramCookie.substring(2);
      }
      int i = ((String)localObject).indexOf('.');
      if (i == -1) {
        break;
      }
      paramCookie = new StringBuilder();
      paramCookie.append("*");
      paramCookie.append(((String)localObject).substring(i));
      localObject = paramCookie.toString();
      paramCookie = (Cookie)localObject;
    } while (((String)localObject).length() > 0);
    return false;
  }
  
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    if (isForPublicSuffix(paramCookie)) {
      return false;
    }
    return this.wrapped.match(paramCookie, paramCookieOrigin);
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    this.wrapped.parse(paramSetCookie, paramString);
  }
  
  public void setExceptions(Collection<String> paramCollection)
  {
    this.exceptions = new HashSet(paramCollection);
  }
  
  public void setPublicSuffixes(Collection<String> paramCollection)
  {
    this.suffixes = new HashSet(paramCollection);
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    this.wrapped.validate(paramCookie, paramCookieOrigin);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/PublicSuffixFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */