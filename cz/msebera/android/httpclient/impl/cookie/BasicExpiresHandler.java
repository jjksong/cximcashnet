package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import java.util.Date;

@Immutable
public class BasicExpiresHandler
  extends AbstractCookieAttributeHandler
{
  private final String[] datepatterns;
  
  public BasicExpiresHandler(String[] paramArrayOfString)
  {
    Args.notNull(paramArrayOfString, "Array of date patterns");
    this.datepatterns = paramArrayOfString;
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null)
    {
      Date localDate = DateUtils.parseDate(paramString, this.datepatterns);
      if (localDate != null)
      {
        paramSetCookie.setExpiryDate(localDate);
        return;
      }
      paramSetCookie = new StringBuilder();
      paramSetCookie.append("Unable to parse expires attribute: ");
      paramSetCookie.append(paramString);
      throw new MalformedCookieException(paramSetCookie.toString());
    }
    throw new MalformedCookieException("Missing value for expires attribute");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BasicExpiresHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */