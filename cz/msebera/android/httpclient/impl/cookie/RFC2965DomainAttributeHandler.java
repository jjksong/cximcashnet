package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.ClientCookie;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import java.util.Locale;

@Immutable
public class RFC2965DomainAttributeHandler
  implements CookieAttributeHandler
{
  public boolean domainMatch(String paramString1, String paramString2)
  {
    boolean bool;
    if ((!paramString1.equals(paramString2)) && ((!paramString2.startsWith(".")) || (!paramString1.endsWith(paramString2)))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    paramCookieOrigin = paramCookieOrigin.getHost().toLowerCase(Locale.ENGLISH);
    paramCookie = paramCookie.getDomain();
    boolean bool2 = domainMatch(paramCookieOrigin, paramCookie);
    boolean bool1 = false;
    if (!bool2) {
      return false;
    }
    if (paramCookieOrigin.substring(0, paramCookieOrigin.length() - paramCookie.length()).indexOf('.') == -1) {
      bool1 = true;
    }
    return bool1;
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null)
    {
      if (paramString.trim().length() != 0)
      {
        String str2 = paramString.toLowerCase(Locale.ENGLISH);
        String str1 = str2;
        if (!paramString.startsWith("."))
        {
          paramString = new StringBuilder();
          paramString.append('.');
          paramString.append(str2);
          str1 = paramString.toString();
        }
        paramSetCookie.setDomain(str1);
        return;
      }
      throw new MalformedCookieException("Blank value for domain attribute");
    }
    throw new MalformedCookieException("Missing value for domain attribute");
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    paramCookieOrigin = paramCookieOrigin.getHost().toLowerCase(Locale.ENGLISH);
    if (paramCookie.getDomain() != null)
    {
      Object localObject = paramCookie.getDomain().toLowerCase(Locale.ENGLISH);
      if (((paramCookie instanceof ClientCookie)) && (((ClientCookie)paramCookie).containsAttribute("domain")))
      {
        if (((String)localObject).startsWith("."))
        {
          int i = ((String)localObject).indexOf('.', 1);
          if (((i >= 0) && (i != ((String)localObject).length() - 1)) || (((String)localObject).equals(".local")))
          {
            if (domainMatch(paramCookieOrigin, (String)localObject))
            {
              if (paramCookieOrigin.substring(0, paramCookieOrigin.length() - ((String)localObject).length()).indexOf('.') != -1)
              {
                paramCookieOrigin = new StringBuilder();
                paramCookieOrigin.append("Domain attribute \"");
                paramCookieOrigin.append(paramCookie.getDomain());
                paramCookieOrigin.append("\" violates RFC 2965: ");
                paramCookieOrigin.append("effective host minus domain may not contain any dots");
                throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
              }
            }
            else
            {
              paramCookieOrigin = new StringBuilder();
              paramCookieOrigin.append("Domain attribute \"");
              paramCookieOrigin.append(paramCookie.getDomain());
              paramCookieOrigin.append("\" violates RFC 2965: effective host name does not ");
              paramCookieOrigin.append("domain-match domain attribute.");
              throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
            }
          }
          else
          {
            paramCookieOrigin = new StringBuilder();
            paramCookieOrigin.append("Domain attribute \"");
            paramCookieOrigin.append(paramCookie.getDomain());
            paramCookieOrigin.append("\" violates RFC 2965: the value contains no embedded dots ");
            paramCookieOrigin.append("and the value is not .local");
            throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
          }
        }
        else
        {
          paramCookieOrigin = new StringBuilder();
          paramCookieOrigin.append("Domain attribute \"");
          paramCookieOrigin.append(paramCookie.getDomain());
          paramCookieOrigin.append("\" violates RFC 2109: domain must start with a dot");
          throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
        }
      }
      else {
        if (!paramCookie.getDomain().equals(paramCookieOrigin)) {
          break label365;
        }
      }
      return;
      label365:
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Illegal domain attribute: \"");
      ((StringBuilder)localObject).append(paramCookie.getDomain());
      ((StringBuilder)localObject).append("\".");
      ((StringBuilder)localObject).append("Domain of origin: \"");
      ((StringBuilder)localObject).append(paramCookieOrigin);
      ((StringBuilder)localObject).append("\"");
      throw new CookieRestrictionViolationException(((StringBuilder)localObject).toString());
    }
    throw new CookieRestrictionViolationException("Invalid cookie state: domain not specified");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/RFC2965DomainAttributeHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */