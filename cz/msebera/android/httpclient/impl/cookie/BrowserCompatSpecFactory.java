package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.CookieSpec;
import cz.msebera.android.httpclient.cookie.CookieSpecFactory;
import cz.msebera.android.httpclient.cookie.CookieSpecProvider;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.util.Collection;

@Immutable
public class BrowserCompatSpecFactory
  implements CookieSpecFactory, CookieSpecProvider
{
  private final String[] datepatterns;
  private final SecurityLevel securityLevel;
  
  public BrowserCompatSpecFactory()
  {
    this(null, SecurityLevel.SECURITYLEVEL_DEFAULT);
  }
  
  public BrowserCompatSpecFactory(String[] paramArrayOfString)
  {
    this(null, SecurityLevel.SECURITYLEVEL_DEFAULT);
  }
  
  public BrowserCompatSpecFactory(String[] paramArrayOfString, SecurityLevel paramSecurityLevel)
  {
    this.datepatterns = paramArrayOfString;
    this.securityLevel = paramSecurityLevel;
  }
  
  public CookieSpec create(HttpContext paramHttpContext)
  {
    return new BrowserCompatSpec(this.datepatterns);
  }
  
  public CookieSpec newInstance(HttpParams paramHttpParams)
  {
    Object localObject = null;
    if (paramHttpParams != null)
    {
      Collection localCollection = (Collection)paramHttpParams.getParameter("http.protocol.cookie-datepatterns");
      paramHttpParams = (HttpParams)localObject;
      if (localCollection != null) {
        paramHttpParams = (String[])localCollection.toArray(new String[localCollection.size()]);
      }
      return new BrowserCompatSpec(paramHttpParams, this.securityLevel);
    }
    return new BrowserCompatSpec(null, this.securityLevel);
  }
  
  public static enum SecurityLevel
  {
    SECURITYLEVEL_DEFAULT,  SECURITYLEVEL_IE_MEDIUM;
    
    private SecurityLevel() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BrowserCompatSpecFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */