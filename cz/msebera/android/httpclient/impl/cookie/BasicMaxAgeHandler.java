package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import java.util.Date;

@Immutable
public class BasicMaxAgeHandler
  extends AbstractCookieAttributeHandler
{
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null) {
      try
      {
        int i = Integer.parseInt(paramString);
        if (i >= 0)
        {
          paramSetCookie.setExpiryDate(new Date(System.currentTimeMillis() + i * 1000L));
          return;
        }
        paramSetCookie = new StringBuilder();
        paramSetCookie.append("Negative max-age attribute: ");
        paramSetCookie.append(paramString);
        throw new MalformedCookieException(paramSetCookie.toString());
      }
      catch (NumberFormatException paramSetCookie)
      {
        paramSetCookie = new StringBuilder();
        paramSetCookie.append("Invalid max-age attribute: ");
        paramSetCookie.append(paramString);
        throw new MalformedCookieException(paramSetCookie.toString());
      }
    }
    throw new MalformedCookieException("Missing value for max-age attribute");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BasicMaxAgeHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */