package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class RFC2109VersionHandler
  extends AbstractCookieAttributeHandler
{
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null)
    {
      if (paramString.trim().length() != 0) {
        try
        {
          paramSetCookie.setVersion(Integer.parseInt(paramString));
          return;
        }
        catch (NumberFormatException paramSetCookie)
        {
          paramString = new StringBuilder();
          paramString.append("Invalid version: ");
          paramString.append(paramSetCookie.getMessage());
          throw new MalformedCookieException(paramString.toString());
        }
      }
      throw new MalformedCookieException("Blank value for version attribute");
    }
    throw new MalformedCookieException("Missing value for version attribute");
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    if (paramCookie.getVersion() >= 0) {
      return;
    }
    throw new CookieRestrictionViolationException("Cookie version may not be negative");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/RFC2109VersionHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */