package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieSpec;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie2;
import cz.msebera.android.httpclient.message.ParserCursor;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.Iterator;
import java.util.List;

@NotThreadSafe
public class BestMatchSpec
  implements CookieSpec
{
  private BrowserCompatSpec compat;
  private final String[] datepatterns;
  private RFC2109Spec obsoleteStrict;
  private final boolean oneHeader;
  private RFC2965Spec strict;
  
  public BestMatchSpec()
  {
    this(null, false);
  }
  
  public BestMatchSpec(String[] paramArrayOfString, boolean paramBoolean)
  {
    if (paramArrayOfString == null) {
      paramArrayOfString = null;
    } else {
      paramArrayOfString = (String[])paramArrayOfString.clone();
    }
    this.datepatterns = paramArrayOfString;
    this.oneHeader = paramBoolean;
  }
  
  private BrowserCompatSpec getCompat()
  {
    if (this.compat == null) {
      this.compat = new BrowserCompatSpec(this.datepatterns);
    }
    return this.compat;
  }
  
  private RFC2109Spec getObsoleteStrict()
  {
    if (this.obsoleteStrict == null) {
      this.obsoleteStrict = new RFC2109Spec(this.datepatterns, this.oneHeader);
    }
    return this.obsoleteStrict;
  }
  
  private RFC2965Spec getStrict()
  {
    if (this.strict == null) {
      this.strict = new RFC2965Spec(this.datepatterns, this.oneHeader);
    }
    return this.strict;
  }
  
  public List<Header> formatCookies(List<Cookie> paramList)
  {
    Args.notNull(paramList, "List of cookies");
    Iterator localIterator = paramList.iterator();
    int j = Integer.MAX_VALUE;
    int k = 1;
    while (localIterator.hasNext())
    {
      Cookie localCookie = (Cookie)localIterator.next();
      int i = k;
      if (!(localCookie instanceof SetCookie2)) {
        i = 0;
      }
      k = i;
      if (localCookie.getVersion() < j)
      {
        j = localCookie.getVersion();
        k = i;
      }
    }
    if (j > 0)
    {
      if (k != 0) {
        return getStrict().formatCookies(paramList);
      }
      return getObsoleteStrict().formatCookies(paramList);
    }
    return getCompat().formatCookies(paramList);
  }
  
  public int getVersion()
  {
    return getStrict().getVersion();
  }
  
  public Header getVersionHeader()
  {
    return getStrict().getVersionHeader();
  }
  
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    if (paramCookie.getVersion() > 0)
    {
      if ((paramCookie instanceof SetCookie2)) {
        return getStrict().match(paramCookie, paramCookieOrigin);
      }
      return getObsoleteStrict().match(paramCookie, paramCookieOrigin);
    }
    return getCompat().match(paramCookie, paramCookieOrigin);
  }
  
  public List<Cookie> parse(Header paramHeader, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramHeader, "Header");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    Object localObject2 = paramHeader.getElements();
    int m = localObject2.length;
    int i = 0;
    int j = 0;
    int k = 0;
    Object localObject1;
    while (i < m)
    {
      localObject1 = localObject2[i];
      if (((HeaderElement)localObject1).getParameterByName("version") != null) {
        k = 1;
      }
      if (((HeaderElement)localObject1).getParameterByName("expires") != null) {
        j = 1;
      }
      i++;
    }
    if ((j == 0) && (k != 0))
    {
      if ("Set-Cookie2".equals(paramHeader.getName())) {
        return getStrict().parse((HeaderElement[])localObject2, paramCookieOrigin);
      }
      return getObsoleteStrict().parse((HeaderElement[])localObject2, paramCookieOrigin);
    }
    localObject2 = NetscapeDraftHeaderParser.DEFAULT;
    if ((paramHeader instanceof FormattedHeader))
    {
      localObject1 = (FormattedHeader)paramHeader;
      paramHeader = ((FormattedHeader)localObject1).getBuffer();
      localObject1 = new ParserCursor(((FormattedHeader)localObject1).getValuePos(), paramHeader.length());
    }
    else
    {
      localObject1 = paramHeader.getValue();
      if (localObject1 == null) {
        break label253;
      }
      paramHeader = new CharArrayBuffer(((String)localObject1).length());
      paramHeader.append((String)localObject1);
      localObject1 = new ParserCursor(0, paramHeader.length());
    }
    paramHeader = ((NetscapeDraftHeaderParser)localObject2).parseHeader(paramHeader, (ParserCursor)localObject1);
    return getCompat().parse(new HeaderElement[] { paramHeader }, paramCookieOrigin);
    label253:
    throw new MalformedCookieException("Header value is null");
  }
  
  public String toString()
  {
    return "best-match";
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    if (paramCookie.getVersion() > 0)
    {
      if ((paramCookie instanceof SetCookie2)) {
        getStrict().validate(paramCookie, paramCookieOrigin);
      } else {
        getObsoleteStrict().validate(paramCookie, paramCookieOrigin);
      }
    }
    else {
      getCompat().validate(paramCookie, paramCookieOrigin);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BestMatchSpec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */