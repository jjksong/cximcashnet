package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@NotThreadSafe
public abstract class CookieSpecBase
  extends AbstractCookieSpec
{
  protected static String getDefaultDomain(CookieOrigin paramCookieOrigin)
  {
    return paramCookieOrigin.getHost();
  }
  
  protected static String getDefaultPath(CookieOrigin paramCookieOrigin)
  {
    String str = paramCookieOrigin.getPath();
    int j = str.lastIndexOf('/');
    paramCookieOrigin = str;
    if (j >= 0)
    {
      int i = j;
      if (j == 0) {
        i = 1;
      }
      paramCookieOrigin = str.substring(0, i);
    }
    return paramCookieOrigin;
  }
  
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    Iterator localIterator = getAttribHandlers().iterator();
    while (localIterator.hasNext()) {
      if (!((CookieAttributeHandler)localIterator.next()).match(paramCookie, paramCookieOrigin)) {
        return false;
      }
    }
    return true;
  }
  
  protected List<Cookie> parse(HeaderElement[] paramArrayOfHeaderElement, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    ArrayList localArrayList = new ArrayList(paramArrayOfHeaderElement.length);
    int k = paramArrayOfHeaderElement.length;
    int i = 0;
    while (i < k)
    {
      Object localObject2 = paramArrayOfHeaderElement[i];
      String str = ((HeaderElement)localObject2).getName();
      Object localObject1 = ((HeaderElement)localObject2).getValue();
      if ((str != null) && (str.length() != 0))
      {
        localObject1 = new BasicClientCookie(str, (String)localObject1);
        ((BasicClientCookie)localObject1).setPath(getDefaultPath(paramCookieOrigin));
        ((BasicClientCookie)localObject1).setDomain(getDefaultDomain(paramCookieOrigin));
        localObject2 = ((HeaderElement)localObject2).getParameters();
        for (int j = localObject2.length - 1; j >= 0; j--)
        {
          str = localObject2[j];
          Object localObject3 = str.getName().toLowerCase(Locale.ENGLISH);
          ((BasicClientCookie)localObject1).setAttribute((String)localObject3, str.getValue());
          localObject3 = findAttribHandler((String)localObject3);
          if (localObject3 != null) {
            ((CookieAttributeHandler)localObject3).parse((SetCookie)localObject1, str.getValue());
          }
        }
        localArrayList.add(localObject1);
        i++;
      }
      else
      {
        throw new MalformedCookieException("Cookie name may not be empty");
      }
    }
    return localArrayList;
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    Iterator localIterator = getAttribHandlers().iterator();
    while (localIterator.hasNext()) {
      ((CookieAttributeHandler)localIterator.next()).validate(paramCookie, paramCookieOrigin);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/CookieSpecBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */