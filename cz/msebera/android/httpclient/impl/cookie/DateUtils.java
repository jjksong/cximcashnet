package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.util.Date;
import java.util.TimeZone;

@Deprecated
@Immutable
public final class DateUtils
{
  public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
  public static final String PATTERN_ASCTIME = "EEE MMM d HH:mm:ss yyyy";
  public static final String PATTERN_RFC1036 = "EEE, dd-MMM-yy HH:mm:ss zzz";
  public static final String PATTERN_RFC1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";
  
  public static String formatDate(Date paramDate)
  {
    return cz.msebera.android.httpclient.client.utils.DateUtils.formatDate(paramDate);
  }
  
  public static String formatDate(Date paramDate, String paramString)
  {
    return cz.msebera.android.httpclient.client.utils.DateUtils.formatDate(paramDate, paramString);
  }
  
  public static Date parseDate(String paramString)
    throws DateParseException
  {
    return parseDate(paramString, null, null);
  }
  
  public static Date parseDate(String paramString, String[] paramArrayOfString)
    throws DateParseException
  {
    return parseDate(paramString, paramArrayOfString, null);
  }
  
  public static Date parseDate(String paramString, String[] paramArrayOfString, Date paramDate)
    throws DateParseException
  {
    paramArrayOfString = cz.msebera.android.httpclient.client.utils.DateUtils.parseDate(paramString, paramArrayOfString, paramDate);
    if (paramArrayOfString != null) {
      return paramArrayOfString;
    }
    paramArrayOfString = new StringBuilder();
    paramArrayOfString.append("Unable to parse the date ");
    paramArrayOfString.append(paramString);
    throw new DateParseException(paramArrayOfString.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/DateUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */