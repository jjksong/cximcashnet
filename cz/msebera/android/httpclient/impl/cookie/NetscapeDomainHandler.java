package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.util.Args;
import java.util.Locale;
import java.util.StringTokenizer;

@Immutable
public class NetscapeDomainHandler
  extends BasicDomainHandler
{
  private static boolean isSpecialDomain(String paramString)
  {
    paramString = paramString.toUpperCase(Locale.ENGLISH);
    boolean bool;
    if ((!paramString.endsWith(".COM")) && (!paramString.endsWith(".EDU")) && (!paramString.endsWith(".NET")) && (!paramString.endsWith(".GOV")) && (!paramString.endsWith(".MIL")) && (!paramString.endsWith(".ORG")) && (!paramString.endsWith(".INT"))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    paramCookieOrigin = paramCookieOrigin.getHost();
    paramCookie = paramCookie.getDomain();
    if (paramCookie == null) {
      return false;
    }
    return paramCookieOrigin.endsWith(paramCookie);
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    super.validate(paramCookie, paramCookieOrigin);
    paramCookieOrigin = paramCookieOrigin.getHost();
    paramCookie = paramCookie.getDomain();
    if (paramCookieOrigin.contains("."))
    {
      int i = new StringTokenizer(paramCookie, ".").countTokens();
      if (isSpecialDomain(paramCookie))
      {
        if (i < 2)
        {
          paramCookieOrigin = new StringBuilder();
          paramCookieOrigin.append("Domain attribute \"");
          paramCookieOrigin.append(paramCookie);
          paramCookieOrigin.append("\" violates the Netscape cookie specification for ");
          paramCookieOrigin.append("special domains");
          throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
        }
      }
      else if (i < 3)
      {
        paramCookieOrigin = new StringBuilder();
        paramCookieOrigin.append("Domain attribute \"");
        paramCookieOrigin.append(paramCookie);
        paramCookieOrigin.append("\" violates the Netscape cookie specification");
        throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/NetscapeDomainHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */