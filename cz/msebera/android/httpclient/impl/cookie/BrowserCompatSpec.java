package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.message.BasicHeaderElement;
import cz.msebera.android.httpclient.message.BasicHeaderValueFormatter;
import cz.msebera.android.httpclient.message.BufferedHeader;
import cz.msebera.android.httpclient.message.ParserCursor;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import cz.msebera.android.httpclient.util.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@NotThreadSafe
public class BrowserCompatSpec
  extends CookieSpecBase
{
  private static final String[] DEFAULT_DATE_PATTERNS = { "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z" };
  private final String[] datepatterns;
  
  public BrowserCompatSpec()
  {
    this(null, BrowserCompatSpecFactory.SecurityLevel.SECURITYLEVEL_DEFAULT);
  }
  
  public BrowserCompatSpec(String[] paramArrayOfString)
  {
    this(paramArrayOfString, BrowserCompatSpecFactory.SecurityLevel.SECURITYLEVEL_DEFAULT);
  }
  
  public BrowserCompatSpec(String[] paramArrayOfString, BrowserCompatSpecFactory.SecurityLevel paramSecurityLevel)
  {
    if (paramArrayOfString != null) {
      this.datepatterns = ((String[])paramArrayOfString.clone());
    } else {
      this.datepatterns = DEFAULT_DATE_PATTERNS;
    }
    switch (paramSecurityLevel)
    {
    default: 
      throw new RuntimeException("Unknown security level");
    case ???: 
      registerAttribHandler("path", new BasicPathHandler()
      {
        public void validate(Cookie paramAnonymousCookie, CookieOrigin paramAnonymousCookieOrigin)
          throws MalformedCookieException
        {}
      });
      break;
    case ???: 
      registerAttribHandler("path", new BasicPathHandler());
    }
    registerAttribHandler("domain", new BasicDomainHandler());
    registerAttribHandler("max-age", new BasicMaxAgeHandler());
    registerAttribHandler("secure", new BasicSecureHandler());
    registerAttribHandler("comment", new BasicCommentHandler());
    registerAttribHandler("expires", new BasicExpiresHandler(this.datepatterns));
    registerAttribHandler("version", new BrowserCompatVersionAttributeHandler());
  }
  
  private static boolean isQuoteEnclosed(String paramString)
  {
    boolean bool;
    if ((paramString != null) && (paramString.startsWith("\"")) && (paramString.endsWith("\""))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public List<Header> formatCookies(List<Cookie> paramList)
  {
    Args.notEmpty(paramList, "List of cookies");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramList.size() * 20);
    localCharArrayBuffer.append("Cookie");
    localCharArrayBuffer.append(": ");
    for (int i = 0; i < paramList.size(); i++)
    {
      Cookie localCookie = (Cookie)paramList.get(i);
      if (i > 0) {
        localCharArrayBuffer.append("; ");
      }
      String str2 = localCookie.getName();
      String str1 = localCookie.getValue();
      if ((localCookie.getVersion() > 0) && (!isQuoteEnclosed(str1)))
      {
        BasicHeaderValueFormatter.INSTANCE.formatHeaderElement(localCharArrayBuffer, new BasicHeaderElement(str2, str1), false);
      }
      else
      {
        localCharArrayBuffer.append(str2);
        localCharArrayBuffer.append("=");
        if (str1 != null) {
          localCharArrayBuffer.append(str1);
        }
      }
    }
    paramList = new ArrayList(1);
    paramList.add(new BufferedHeader(localCharArrayBuffer));
    return paramList;
  }
  
  public int getVersion()
  {
    return 0;
  }
  
  public Header getVersionHeader()
  {
    return null;
  }
  
  public List<Cookie> parse(Header paramHeader, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramHeader, "Header");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    if (paramHeader.getName().equalsIgnoreCase("Set-Cookie"))
    {
      Object localObject1 = paramHeader.getElements();
      int m = localObject1.length;
      int j = 0;
      int i = 0;
      int k = 0;
      while (j < m)
      {
        localObject2 = localObject1[j];
        if (((HeaderElement)localObject2).getParameterByName("version") != null) {
          k = 1;
        }
        if (((HeaderElement)localObject2).getParameterByName("expires") != null) {
          i = 1;
        }
        j++;
      }
      if ((i == 0) && (k != 0)) {
        return parse((HeaderElement[])localObject1, paramCookieOrigin);
      }
      Object localObject2 = NetscapeDraftHeaderParser.DEFAULT;
      if ((paramHeader instanceof FormattedHeader))
      {
        paramHeader = (FormattedHeader)paramHeader;
        localObject1 = paramHeader.getBuffer();
        paramHeader = new ParserCursor(paramHeader.getValuePos(), ((CharArrayBuffer)localObject1).length());
      }
      else
      {
        paramHeader = paramHeader.getValue();
        if (paramHeader == null) {
          break label386;
        }
        localObject1 = new CharArrayBuffer(paramHeader.length());
        ((CharArrayBuffer)localObject1).append(paramHeader);
        paramHeader = new ParserCursor(0, ((CharArrayBuffer)localObject1).length());
      }
      localObject1 = ((NetscapeDraftHeaderParser)localObject2).parseHeader((CharArrayBuffer)localObject1, paramHeader);
      paramHeader = ((HeaderElement)localObject1).getName();
      localObject2 = ((HeaderElement)localObject1).getValue();
      if ((paramHeader != null) && (!TextUtils.isBlank(paramHeader)))
      {
        paramHeader = new BasicClientCookie(paramHeader, (String)localObject2);
        paramHeader.setPath(getDefaultPath(paramCookieOrigin));
        paramHeader.setDomain(getDefaultDomain(paramCookieOrigin));
        paramCookieOrigin = ((HeaderElement)localObject1).getParameters();
        for (j = paramCookieOrigin.length - 1; j >= 0; j--)
        {
          localObject1 = paramCookieOrigin[j];
          localObject2 = ((NameValuePair)localObject1).getName().toLowerCase(Locale.ENGLISH);
          paramHeader.setAttribute((String)localObject2, ((NameValuePair)localObject1).getValue());
          localObject2 = findAttribHandler((String)localObject2);
          if (localObject2 != null) {
            ((CookieAttributeHandler)localObject2).parse(paramHeader, ((NameValuePair)localObject1).getValue());
          }
        }
        if (i != 0) {
          paramHeader.setVersion(0);
        }
        return Collections.singletonList(paramHeader);
      }
      throw new MalformedCookieException("Cookie name may not be empty");
      label386:
      throw new MalformedCookieException("Header value is null");
    }
    paramCookieOrigin = new StringBuilder();
    paramCookieOrigin.append("Unrecognized cookie header '");
    paramCookieOrigin.append(paramHeader.toString());
    paramCookieOrigin.append("'");
    throw new MalformedCookieException(paramCookieOrigin.toString());
  }
  
  public String toString()
  {
    return "compatibility";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BrowserCompatSpec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */