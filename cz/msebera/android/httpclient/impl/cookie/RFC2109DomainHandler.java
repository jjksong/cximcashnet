package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import java.util.Locale;

@Immutable
public class RFC2109DomainHandler
  implements CookieAttributeHandler
{
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    paramCookieOrigin = paramCookieOrigin.getHost();
    paramCookie = paramCookie.getDomain();
    boolean bool2 = false;
    if (paramCookie == null) {
      return false;
    }
    boolean bool1;
    if (!paramCookieOrigin.equals(paramCookie))
    {
      bool1 = bool2;
      if (paramCookie.startsWith("."))
      {
        bool1 = bool2;
        if (!paramCookieOrigin.endsWith(paramCookie)) {}
      }
    }
    else
    {
      bool1 = true;
    }
    return bool1;
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null)
    {
      if (paramString.trim().length() != 0)
      {
        paramSetCookie.setDomain(paramString);
        return;
      }
      throw new MalformedCookieException("Blank value for domain attribute");
    }
    throw new MalformedCookieException("Missing value for domain attribute");
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    paramCookieOrigin = paramCookieOrigin.getHost();
    paramCookie = paramCookie.getDomain();
    if (paramCookie != null)
    {
      if (!paramCookie.equals(paramCookieOrigin))
      {
        Object localObject;
        if (paramCookie.indexOf('.') != -1)
        {
          if (paramCookie.startsWith("."))
          {
            int i = paramCookie.indexOf('.', 1);
            if ((i >= 0) && (i != paramCookie.length() - 1))
            {
              localObject = paramCookieOrigin.toLowerCase(Locale.ENGLISH);
              if (((String)localObject).endsWith(paramCookie))
              {
                if (((String)localObject).substring(0, ((String)localObject).length() - paramCookie.length()).indexOf('.') != -1)
                {
                  paramCookieOrigin = new StringBuilder();
                  paramCookieOrigin.append("Domain attribute \"");
                  paramCookieOrigin.append(paramCookie);
                  paramCookieOrigin.append("\" violates RFC 2109: host minus domain may not contain any dots");
                  throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
                }
              }
              else
              {
                paramCookieOrigin = new StringBuilder();
                paramCookieOrigin.append("Illegal domain attribute \"");
                paramCookieOrigin.append(paramCookie);
                paramCookieOrigin.append("\". Domain of origin: \"");
                paramCookieOrigin.append((String)localObject);
                paramCookieOrigin.append("\"");
                throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
              }
            }
            else
            {
              paramCookieOrigin = new StringBuilder();
              paramCookieOrigin.append("Domain attribute \"");
              paramCookieOrigin.append(paramCookie);
              paramCookieOrigin.append("\" violates RFC 2109: domain must contain an embedded dot");
              throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
            }
          }
          else
          {
            paramCookieOrigin = new StringBuilder();
            paramCookieOrigin.append("Domain attribute \"");
            paramCookieOrigin.append(paramCookie);
            paramCookieOrigin.append("\" violates RFC 2109: domain must start with a dot");
            throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
          }
        }
        else
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("Domain attribute \"");
          ((StringBuilder)localObject).append(paramCookie);
          ((StringBuilder)localObject).append("\" does not match the host \"");
          ((StringBuilder)localObject).append(paramCookieOrigin);
          ((StringBuilder)localObject).append("\"");
          throw new CookieRestrictionViolationException(((StringBuilder)localObject).toString());
        }
      }
      return;
    }
    throw new CookieRestrictionViolationException("Cookie domain may not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/RFC2109DomainHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */