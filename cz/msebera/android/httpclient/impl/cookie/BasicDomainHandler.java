package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;

@Immutable
public class BasicDomainHandler
  implements CookieAttributeHandler
{
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    String str = paramCookieOrigin.getHost();
    paramCookieOrigin = paramCookie.getDomain();
    boolean bool = false;
    if (paramCookieOrigin == null) {
      return false;
    }
    if (str.equals(paramCookieOrigin)) {
      return true;
    }
    paramCookie = paramCookieOrigin;
    if (!paramCookieOrigin.startsWith("."))
    {
      paramCookie = new StringBuilder();
      paramCookie.append('.');
      paramCookie.append(paramCookieOrigin);
      paramCookie = paramCookie.toString();
    }
    if ((str.endsWith(paramCookie)) || (str.equals(paramCookie.substring(1)))) {
      bool = true;
    }
    return bool;
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (paramString != null)
    {
      if (paramString.trim().length() != 0)
      {
        paramSetCookie.setDomain(paramString);
        return;
      }
      throw new MalformedCookieException("Blank value for domain attribute");
    }
    throw new MalformedCookieException("Missing value for domain attribute");
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    String str = paramCookieOrigin.getHost();
    paramCookieOrigin = paramCookie.getDomain();
    if (paramCookieOrigin != null)
    {
      if (str.contains("."))
      {
        if (!str.endsWith(paramCookieOrigin))
        {
          paramCookie = paramCookieOrigin;
          if (paramCookieOrigin.startsWith(".")) {
            paramCookie = paramCookieOrigin.substring(1, paramCookieOrigin.length());
          }
          if (!str.equals(paramCookie))
          {
            paramCookieOrigin = new StringBuilder();
            paramCookieOrigin.append("Illegal domain attribute \"");
            paramCookieOrigin.append(paramCookie);
            paramCookieOrigin.append("\". Domain of origin: \"");
            paramCookieOrigin.append(str);
            paramCookieOrigin.append("\"");
            throw new CookieRestrictionViolationException(paramCookieOrigin.toString());
          }
        }
      }
      else {
        if (!str.equals(paramCookieOrigin)) {
          break label141;
        }
      }
      return;
      label141:
      paramCookie = new StringBuilder();
      paramCookie.append("Illegal domain attribute \"");
      paramCookie.append(paramCookieOrigin);
      paramCookie.append("\". Domain of origin: \"");
      paramCookie.append(str);
      paramCookie.append("\"");
      throw new CookieRestrictionViolationException(paramCookie.toString());
    }
    throw new CookieRestrictionViolationException("Cookie domain may not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BasicDomainHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */