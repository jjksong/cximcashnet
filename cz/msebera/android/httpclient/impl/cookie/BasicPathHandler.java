package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieAttributeHandler;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieRestrictionViolationException;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.cookie.SetCookie;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.TextUtils;

@Immutable
public class BasicPathHandler
  implements CookieAttributeHandler
{
  public boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    Args.notNull(paramCookie, "Cookie");
    Args.notNull(paramCookieOrigin, "Cookie origin");
    String str = paramCookieOrigin.getPath();
    paramCookieOrigin = paramCookie.getPath();
    paramCookie = paramCookieOrigin;
    if (paramCookieOrigin == null) {
      paramCookie = "/";
    }
    paramCookieOrigin = paramCookie;
    if (paramCookie.length() > 1)
    {
      paramCookieOrigin = paramCookie;
      if (paramCookie.endsWith("/")) {
        paramCookieOrigin = paramCookie.substring(0, paramCookie.length() - 1);
      }
    }
    boolean bool2 = str.startsWith(paramCookieOrigin);
    boolean bool1 = bool2;
    if (bool2)
    {
      bool1 = bool2;
      if (str.length() != paramCookieOrigin.length())
      {
        bool1 = bool2;
        if (!paramCookieOrigin.endsWith("/")) {
          if (str.charAt(paramCookieOrigin.length()) == '/') {
            bool1 = true;
          } else {
            bool1 = false;
          }
        }
      }
    }
    return bool1;
  }
  
  public void parse(SetCookie paramSetCookie, String paramString)
    throws MalformedCookieException
  {
    Args.notNull(paramSetCookie, "Cookie");
    if (TextUtils.isBlank(paramString)) {
      paramString = "/";
    }
    paramSetCookie.setPath(paramString);
  }
  
  public void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
    throws MalformedCookieException
  {
    if (match(paramCookie, paramCookieOrigin)) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Illegal path attribute \"");
    localStringBuilder.append(paramCookie.getPath());
    localStringBuilder.append("\". Path of origin: \"");
    localStringBuilder.append(paramCookieOrigin.getPath());
    localStringBuilder.append("\"");
    throw new CookieRestrictionViolationException(localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/BasicPathHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */