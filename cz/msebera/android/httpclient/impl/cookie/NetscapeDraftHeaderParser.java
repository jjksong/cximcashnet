package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.message.BasicHeaderElement;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.message.ParserCursor;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.ArrayList;
import java.util.List;

@Immutable
public class NetscapeDraftHeaderParser
{
  public static final NetscapeDraftHeaderParser DEFAULT = new NetscapeDraftHeaderParser();
  
  private NameValuePair parseNameValuePair(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    int j = paramParserCursor.getPos();
    int i = paramParserCursor.getPos();
    int n = paramParserCursor.getUpperBound();
    for (;;)
    {
      m = 1;
      if (j >= n) {
        break;
      }
      k = paramCharArrayBuffer.charAt(j);
      if (k == 61) {
        break;
      }
      if (k == 59)
      {
        k = 1;
        break label67;
      }
      j++;
    }
    int k = 0;
    label67:
    String str;
    if (j == n)
    {
      str = paramCharArrayBuffer.substringTrimmed(i, n);
      k = 1;
    }
    else
    {
      str = paramCharArrayBuffer.substringTrimmed(i, j);
      j++;
    }
    if (k != 0)
    {
      paramParserCursor.updatePos(j);
      return new BasicNameValuePair(str, null);
    }
    for (i = j; i < n; i++) {
      if (paramCharArrayBuffer.charAt(i) == ';')
      {
        k = m;
        break;
      }
    }
    while ((j < i) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(j)))) {
      j++;
    }
    for (int m = i; (m > j) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(m - 1))); m--) {}
    paramCharArrayBuffer = paramCharArrayBuffer.substring(j, m);
    j = i;
    if (k != 0) {
      j = i + 1;
    }
    paramParserCursor.updatePos(j);
    return new BasicNameValuePair(str, paramCharArrayBuffer);
  }
  
  public HeaderElement parseHeader(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    NameValuePair localNameValuePair = parseNameValuePair(paramCharArrayBuffer, paramParserCursor);
    ArrayList localArrayList = new ArrayList();
    while (!paramParserCursor.atEnd()) {
      localArrayList.add(parseNameValuePair(paramCharArrayBuffer, paramParserCursor));
    }
    return new BasicHeaderElement(localNameValuePair.getName(), localNameValuePair.getValue(), (NameValuePair[])localArrayList.toArray(new NameValuePair[localArrayList.size()]));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/impl/cookie/NetscapeDraftHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */