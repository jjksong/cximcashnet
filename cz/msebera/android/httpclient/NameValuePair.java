package cz.msebera.android.httpclient;

public abstract interface NameValuePair
{
  public abstract String getName();
  
  public abstract String getValue();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/NameValuePair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */