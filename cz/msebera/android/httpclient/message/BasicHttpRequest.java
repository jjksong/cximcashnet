package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;

@NotThreadSafe
public class BasicHttpRequest
  extends AbstractHttpMessage
  implements HttpRequest
{
  private final String method;
  private RequestLine requestline;
  private final String uri;
  
  public BasicHttpRequest(RequestLine paramRequestLine)
  {
    this.requestline = ((RequestLine)Args.notNull(paramRequestLine, "Request line"));
    this.method = paramRequestLine.getMethod();
    this.uri = paramRequestLine.getUri();
  }
  
  public BasicHttpRequest(String paramString1, String paramString2)
  {
    this.method = ((String)Args.notNull(paramString1, "Method name"));
    this.uri = ((String)Args.notNull(paramString2, "Request URI"));
    this.requestline = null;
  }
  
  public BasicHttpRequest(String paramString1, String paramString2, ProtocolVersion paramProtocolVersion)
  {
    this(new BasicRequestLine(paramString1, paramString2, paramProtocolVersion));
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    return getRequestLine().getProtocolVersion();
  }
  
  public RequestLine getRequestLine()
  {
    if (this.requestline == null) {
      this.requestline = new BasicRequestLine(this.method, this.uri, HttpVersion.HTTP_1_1);
    }
    return this.requestline;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.method);
    localStringBuilder.append(' ');
    localStringBuilder.append(this.uri);
    localStringBuilder.append(' ');
    localStringBuilder.append(this.headergroup);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */