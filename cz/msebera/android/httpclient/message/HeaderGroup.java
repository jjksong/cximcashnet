package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@NotThreadSafe
public class HeaderGroup
  implements Cloneable, Serializable
{
  private static final long serialVersionUID = 2608834160639271617L;
  private final List<Header> headers = new ArrayList(16);
  
  public void addHeader(Header paramHeader)
  {
    if (paramHeader == null) {
      return;
    }
    this.headers.add(paramHeader);
  }
  
  public void clear()
  {
    this.headers.clear();
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public boolean containsHeader(String paramString)
  {
    for (int i = 0; i < this.headers.size(); i++) {
      if (((Header)this.headers.get(i)).getName().equalsIgnoreCase(paramString)) {
        return true;
      }
    }
    return false;
  }
  
  public HeaderGroup copy()
  {
    HeaderGroup localHeaderGroup = new HeaderGroup();
    localHeaderGroup.headers.addAll(this.headers);
    return localHeaderGroup;
  }
  
  public Header[] getAllHeaders()
  {
    List localList = this.headers;
    return (Header[])localList.toArray(new Header[localList.size()]);
  }
  
  public Header getCondensedHeader(String paramString)
  {
    Header[] arrayOfHeader = getHeaders(paramString);
    if (arrayOfHeader.length == 0) {
      return null;
    }
    int j = arrayOfHeader.length;
    int i = 1;
    if (j == 1) {
      return arrayOfHeader[0];
    }
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(128);
    localCharArrayBuffer.append(arrayOfHeader[0].getValue());
    while (i < arrayOfHeader.length)
    {
      localCharArrayBuffer.append(", ");
      localCharArrayBuffer.append(arrayOfHeader[i].getValue());
      i++;
    }
    return new BasicHeader(paramString.toLowerCase(Locale.ENGLISH), localCharArrayBuffer.toString());
  }
  
  public Header getFirstHeader(String paramString)
  {
    for (int i = 0; i < this.headers.size(); i++)
    {
      Header localHeader = (Header)this.headers.get(i);
      if (localHeader.getName().equalsIgnoreCase(paramString)) {
        return localHeader;
      }
    }
    return null;
  }
  
  public Header[] getHeaders(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < this.headers.size(); i++)
    {
      Header localHeader = (Header)this.headers.get(i);
      if (localHeader.getName().equalsIgnoreCase(paramString)) {
        localArrayList.add(localHeader);
      }
    }
    return (Header[])localArrayList.toArray(new Header[localArrayList.size()]);
  }
  
  public Header getLastHeader(String paramString)
  {
    for (int i = this.headers.size() - 1; i >= 0; i--)
    {
      Header localHeader = (Header)this.headers.get(i);
      if (localHeader.getName().equalsIgnoreCase(paramString)) {
        return localHeader;
      }
    }
    return null;
  }
  
  public HeaderIterator iterator()
  {
    return new BasicListHeaderIterator(this.headers, null);
  }
  
  public HeaderIterator iterator(String paramString)
  {
    return new BasicListHeaderIterator(this.headers, paramString);
  }
  
  public void removeHeader(Header paramHeader)
  {
    if (paramHeader == null) {
      return;
    }
    this.headers.remove(paramHeader);
  }
  
  public void setHeaders(Header[] paramArrayOfHeader)
  {
    clear();
    if (paramArrayOfHeader == null) {
      return;
    }
    Collections.addAll(this.headers, paramArrayOfHeader);
  }
  
  public String toString()
  {
    return this.headers.toString();
  }
  
  public void updateHeader(Header paramHeader)
  {
    if (paramHeader == null) {
      return;
    }
    for (int i = 0; i < this.headers.size(); i++) {
      if (((Header)this.headers.get(i)).getName().equalsIgnoreCase(paramHeader.getName()))
      {
        this.headers.set(i, paramHeader);
        return;
      }
    }
    this.headers.add(paramHeader);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/HeaderGroup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */