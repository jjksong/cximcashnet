package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.util.List;
import java.util.NoSuchElementException;

@NotThreadSafe
public class BasicListHeaderIterator
  implements HeaderIterator
{
  protected final List<Header> allHeaders;
  protected int currentIndex;
  protected String headerName;
  protected int lastIndex;
  
  public BasicListHeaderIterator(List<Header> paramList, String paramString)
  {
    this.allHeaders = ((List)Args.notNull(paramList, "Header list"));
    this.headerName = paramString;
    this.currentIndex = findNext(-1);
    this.lastIndex = -1;
  }
  
  protected boolean filterHeader(int paramInt)
  {
    if (this.headerName == null) {
      return true;
    }
    String str = ((Header)this.allHeaders.get(paramInt)).getName();
    return this.headerName.equalsIgnoreCase(str);
  }
  
  protected int findNext(int paramInt)
  {
    if (paramInt < -1) {
      return -1;
    }
    int i = this.allHeaders.size();
    for (boolean bool = false; (!bool) && (paramInt < i - 1); bool = filterHeader(paramInt)) {
      paramInt++;
    }
    if (!bool) {
      paramInt = -1;
    }
    return paramInt;
  }
  
  public boolean hasNext()
  {
    boolean bool;
    if (this.currentIndex >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final Object next()
    throws NoSuchElementException
  {
    return nextHeader();
  }
  
  public Header nextHeader()
    throws NoSuchElementException
  {
    int i = this.currentIndex;
    if (i >= 0)
    {
      this.lastIndex = i;
      this.currentIndex = findNext(i);
      return (Header)this.allHeaders.get(i);
    }
    throw new NoSuchElementException("Iteration already finished.");
  }
  
  public void remove()
    throws UnsupportedOperationException
  {
    boolean bool;
    if (this.lastIndex >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    Asserts.check(bool, "No header to remove");
    this.allHeaders.remove(this.lastIndex);
    this.lastIndex = -1;
    this.currentIndex -= 1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicListHeaderIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */