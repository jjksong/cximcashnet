package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HeaderElementIterator;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.NoSuchElementException;

@NotThreadSafe
public class BasicHeaderElementIterator
  implements HeaderElementIterator
{
  private CharArrayBuffer buffer = null;
  private HeaderElement currentElement = null;
  private ParserCursor cursor = null;
  private final HeaderIterator headerIt;
  private final HeaderValueParser parser;
  
  public BasicHeaderElementIterator(HeaderIterator paramHeaderIterator)
  {
    this(paramHeaderIterator, BasicHeaderValueParser.INSTANCE);
  }
  
  public BasicHeaderElementIterator(HeaderIterator paramHeaderIterator, HeaderValueParser paramHeaderValueParser)
  {
    this.headerIt = ((HeaderIterator)Args.notNull(paramHeaderIterator, "Header iterator"));
    this.parser = ((HeaderValueParser)Args.notNull(paramHeaderValueParser, "Parser"));
  }
  
  private void bufferHeaderValue()
  {
    this.cursor = null;
    this.buffer = null;
    while (this.headerIt.hasNext())
    {
      Object localObject = this.headerIt.nextHeader();
      if ((localObject instanceof FormattedHeader))
      {
        localObject = (FormattedHeader)localObject;
        this.buffer = ((FormattedHeader)localObject).getBuffer();
        this.cursor = new ParserCursor(0, this.buffer.length());
        this.cursor.updatePos(((FormattedHeader)localObject).getValuePos());
      }
      else
      {
        localObject = ((Header)localObject).getValue();
        if (localObject != null)
        {
          this.buffer = new CharArrayBuffer(((String)localObject).length());
          this.buffer.append((String)localObject);
          this.cursor = new ParserCursor(0, this.buffer.length());
        }
      }
    }
  }
  
  private void parseNextElement()
  {
    for (;;)
    {
      if ((!this.headerIt.hasNext()) && (this.cursor == null)) {
        return;
      }
      Object localObject = this.cursor;
      if ((localObject == null) || (((ParserCursor)localObject).atEnd())) {
        bufferHeaderValue();
      }
      if (this.cursor != null)
      {
        while (!this.cursor.atEnd())
        {
          localObject = this.parser.parseHeaderElement(this.buffer, this.cursor);
          if ((((HeaderElement)localObject).getName().length() != 0) || (((HeaderElement)localObject).getValue() != null))
          {
            this.currentElement = ((HeaderElement)localObject);
            return;
          }
        }
        if (this.cursor.atEnd())
        {
          this.cursor = null;
          this.buffer = null;
        }
      }
    }
  }
  
  public boolean hasNext()
  {
    if (this.currentElement == null) {
      parseNextElement();
    }
    boolean bool;
    if (this.currentElement != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final Object next()
    throws NoSuchElementException
  {
    return nextElement();
  }
  
  public HeaderElement nextElement()
    throws NoSuchElementException
  {
    if (this.currentElement == null) {
      parseNextElement();
    }
    HeaderElement localHeaderElement = this.currentElement;
    if (localHeaderElement != null)
    {
      this.currentElement = null;
      return localHeaderElement;
    }
    throw new NoSuchElementException("No more header elements available");
  }
  
  public void remove()
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException("Remove not supported");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHeaderElementIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */