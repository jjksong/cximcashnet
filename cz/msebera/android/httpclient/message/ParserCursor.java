package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;

@NotThreadSafe
public class ParserCursor
{
  private final int lowerBound;
  private int pos;
  private final int upperBound;
  
  public ParserCursor(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      if (paramInt1 <= paramInt2)
      {
        this.lowerBound = paramInt1;
        this.upperBound = paramInt2;
        this.pos = paramInt1;
        return;
      }
      throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
    }
    throw new IndexOutOfBoundsException("Lower bound cannot be negative");
  }
  
  public boolean atEnd()
  {
    boolean bool;
    if (this.pos >= this.upperBound) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int getLowerBound()
  {
    return this.lowerBound;
  }
  
  public int getPos()
  {
    return this.pos;
  }
  
  public int getUpperBound()
  {
    return this.upperBound;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append('[');
    localStringBuilder.append(Integer.toString(this.lowerBound));
    localStringBuilder.append('>');
    localStringBuilder.append(Integer.toString(this.pos));
    localStringBuilder.append('>');
    localStringBuilder.append(Integer.toString(this.upperBound));
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
  
  public void updatePos(int paramInt)
  {
    if (paramInt >= this.lowerBound)
    {
      if (paramInt <= this.upperBound)
      {
        this.pos = paramInt;
        return;
      }
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("pos: ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" > upperBound: ");
      localStringBuilder.append(this.upperBound);
      throw new IndexOutOfBoundsException(localStringBuilder.toString());
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("pos: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" < lowerBound: ");
    localStringBuilder.append(this.lowerBound);
    throw new IndexOutOfBoundsException(localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/ParserCursor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */