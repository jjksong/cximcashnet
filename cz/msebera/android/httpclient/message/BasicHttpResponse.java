package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.ReasonPhraseCatalog;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.Locale;

@NotThreadSafe
public class BasicHttpResponse
  extends AbstractHttpMessage
  implements HttpResponse
{
  private int code;
  private HttpEntity entity;
  private Locale locale;
  private final ReasonPhraseCatalog reasonCatalog;
  private String reasonPhrase;
  private StatusLine statusline;
  private ProtocolVersion ver;
  
  public BasicHttpResponse(ProtocolVersion paramProtocolVersion, int paramInt, String paramString)
  {
    Args.notNegative(paramInt, "Status code");
    this.statusline = null;
    this.ver = paramProtocolVersion;
    this.code = paramInt;
    this.reasonPhrase = paramString;
    this.reasonCatalog = null;
    this.locale = null;
  }
  
  public BasicHttpResponse(StatusLine paramStatusLine)
  {
    this.statusline = ((StatusLine)Args.notNull(paramStatusLine, "Status line"));
    this.ver = paramStatusLine.getProtocolVersion();
    this.code = paramStatusLine.getStatusCode();
    this.reasonPhrase = paramStatusLine.getReasonPhrase();
    this.reasonCatalog = null;
    this.locale = null;
  }
  
  public BasicHttpResponse(StatusLine paramStatusLine, ReasonPhraseCatalog paramReasonPhraseCatalog, Locale paramLocale)
  {
    this.statusline = ((StatusLine)Args.notNull(paramStatusLine, "Status line"));
    this.ver = paramStatusLine.getProtocolVersion();
    this.code = paramStatusLine.getStatusCode();
    this.reasonPhrase = paramStatusLine.getReasonPhrase();
    this.reasonCatalog = paramReasonPhraseCatalog;
    this.locale = paramLocale;
  }
  
  public HttpEntity getEntity()
  {
    return this.entity;
  }
  
  public Locale getLocale()
  {
    return this.locale;
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    return this.ver;
  }
  
  protected String getReason(int paramInt)
  {
    ReasonPhraseCatalog localReasonPhraseCatalog = this.reasonCatalog;
    Object localObject;
    if (localReasonPhraseCatalog != null)
    {
      localObject = this.locale;
      if (localObject == null) {
        localObject = Locale.getDefault();
      }
      localObject = localReasonPhraseCatalog.getReason(paramInt, (Locale)localObject);
    }
    else
    {
      localObject = null;
    }
    return (String)localObject;
  }
  
  public StatusLine getStatusLine()
  {
    if (this.statusline == null)
    {
      Object localObject = this.ver;
      if (localObject == null) {
        localObject = HttpVersion.HTTP_1_1;
      }
      int i = this.code;
      String str = this.reasonPhrase;
      if (str == null) {
        str = getReason(i);
      }
      this.statusline = new BasicStatusLine((ProtocolVersion)localObject, i, str);
    }
    return this.statusline;
  }
  
  public void setEntity(HttpEntity paramHttpEntity)
  {
    this.entity = paramHttpEntity;
  }
  
  public void setLocale(Locale paramLocale)
  {
    this.locale = ((Locale)Args.notNull(paramLocale, "Locale"));
    this.statusline = null;
  }
  
  public void setReasonPhrase(String paramString)
  {
    this.statusline = null;
    this.reasonPhrase = paramString;
  }
  
  public void setStatusCode(int paramInt)
  {
    Args.notNegative(paramInt, "Status code");
    this.statusline = null;
    this.code = paramInt;
    this.reasonPhrase = null;
  }
  
  public void setStatusLine(ProtocolVersion paramProtocolVersion, int paramInt)
  {
    Args.notNegative(paramInt, "Status code");
    this.statusline = null;
    this.ver = paramProtocolVersion;
    this.code = paramInt;
    this.reasonPhrase = null;
  }
  
  public void setStatusLine(ProtocolVersion paramProtocolVersion, int paramInt, String paramString)
  {
    Args.notNegative(paramInt, "Status code");
    this.statusline = null;
    this.ver = paramProtocolVersion;
    this.code = paramInt;
    this.reasonPhrase = paramString;
  }
  
  public void setStatusLine(StatusLine paramStatusLine)
  {
    this.statusline = ((StatusLine)Args.notNull(paramStatusLine, "Status line"));
    this.ver = paramStatusLine.getProtocolVersion();
    this.code = paramStatusLine.getStatusCode();
    this.reasonPhrase = paramStatusLine.getReasonPhrase();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getStatusLine());
    localStringBuilder.append(' ');
    localStringBuilder.append(this.headergroup);
    if (this.entity != null)
    {
      localStringBuilder.append(' ');
      localStringBuilder.append(this.entity);
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHttpResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */