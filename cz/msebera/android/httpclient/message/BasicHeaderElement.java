package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;

@NotThreadSafe
public class BasicHeaderElement
  implements HeaderElement, Cloneable
{
  private final String name;
  private final NameValuePair[] parameters;
  private final String value;
  
  public BasicHeaderElement(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, null);
  }
  
  public BasicHeaderElement(String paramString1, String paramString2, NameValuePair[] paramArrayOfNameValuePair)
  {
    this.name = ((String)Args.notNull(paramString1, "Name"));
    this.value = paramString2;
    if (paramArrayOfNameValuePair != null) {
      this.parameters = paramArrayOfNameValuePair;
    } else {
      this.parameters = new NameValuePair[0];
    }
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof HeaderElement))
    {
      paramObject = (BasicHeaderElement)paramObject;
      if ((!this.name.equals(((BasicHeaderElement)paramObject).name)) || (!LangUtils.equals(this.value, ((BasicHeaderElement)paramObject).value)) || (!LangUtils.equals(this.parameters, ((BasicHeaderElement)paramObject).parameters))) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public NameValuePair getParameter(int paramInt)
  {
    return this.parameters[paramInt];
  }
  
  public NameValuePair getParameterByName(String paramString)
  {
    Args.notNull(paramString, "Name");
    for (NameValuePair localNameValuePair : this.parameters) {
      if (localNameValuePair.getName().equalsIgnoreCase(paramString)) {
        return localNameValuePair;
      }
    }
    paramString = null;
    return paramString;
  }
  
  public int getParameterCount()
  {
    return this.parameters.length;
  }
  
  public NameValuePair[] getParameters()
  {
    return (NameValuePair[])this.parameters.clone();
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public int hashCode()
  {
    int j = LangUtils.hashCode(LangUtils.hashCode(17, this.name), this.value);
    NameValuePair[] arrayOfNameValuePair = this.parameters;
    int k = arrayOfNameValuePair.length;
    for (int i = 0; i < k; i++) {
      j = LangUtils.hashCode(j, arrayOfNameValuePair[i]);
    }
    return j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.name);
    if (this.value != null)
    {
      localStringBuilder.append("=");
      localStringBuilder.append(this.value);
    }
    for (NameValuePair localNameValuePair : this.parameters)
    {
      localStringBuilder.append("; ");
      localStringBuilder.append(localNameValuePair);
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHeaderElement.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */