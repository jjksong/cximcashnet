package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.io.Serializable;

@NotThreadSafe
public class BufferedHeader
  implements FormattedHeader, Cloneable, Serializable
{
  private static final long serialVersionUID = -2768352615787625448L;
  private final CharArrayBuffer buffer;
  private final String name;
  private final int valuePos;
  
  public BufferedHeader(CharArrayBuffer paramCharArrayBuffer)
    throws ParseException
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    int i = paramCharArrayBuffer.indexOf(58);
    if (i != -1)
    {
      localObject = paramCharArrayBuffer.substringTrimmed(0, i);
      if (((String)localObject).length() != 0)
      {
        this.buffer = paramCharArrayBuffer;
        this.name = ((String)localObject);
        this.valuePos = (i + 1);
        return;
      }
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Invalid header: ");
      ((StringBuilder)localObject).append(paramCharArrayBuffer.toString());
      throw new ParseException(((StringBuilder)localObject).toString());
    }
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Invalid header: ");
    ((StringBuilder)localObject).append(paramCharArrayBuffer.toString());
    throw new ParseException(((StringBuilder)localObject).toString());
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public CharArrayBuffer getBuffer()
  {
    return this.buffer;
  }
  
  public HeaderElement[] getElements()
    throws ParseException
  {
    ParserCursor localParserCursor = new ParserCursor(0, this.buffer.length());
    localParserCursor.updatePos(this.valuePos);
    return BasicHeaderValueParser.INSTANCE.parseElements(this.buffer, localParserCursor);
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getValue()
  {
    CharArrayBuffer localCharArrayBuffer = this.buffer;
    return localCharArrayBuffer.substringTrimmed(this.valuePos, localCharArrayBuffer.length());
  }
  
  public int getValuePos()
  {
    return this.valuePos;
  }
  
  public String toString()
  {
    return this.buffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BufferedHeader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */