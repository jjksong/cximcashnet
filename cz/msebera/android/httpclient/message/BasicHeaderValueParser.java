package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.ArrayList;
import java.util.List;

@Immutable
public class BasicHeaderValueParser
  implements HeaderValueParser
{
  private static final char[] ALL_DELIMITERS = { 59, 44 };
  @Deprecated
  public static final BasicHeaderValueParser DEFAULT = new BasicHeaderValueParser();
  private static final char ELEM_DELIMITER = ',';
  public static final BasicHeaderValueParser INSTANCE = new BasicHeaderValueParser();
  private static final char PARAM_DELIMITER = ';';
  
  private static boolean isOneOf(char paramChar, char[] paramArrayOfChar)
  {
    if (paramArrayOfChar != null)
    {
      int j = paramArrayOfChar.length;
      for (int i = 0; i < j; i++) {
        if (paramChar == paramArrayOfChar[i]) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static HeaderElement[] parseElements(String paramString, HeaderValueParser paramHeaderValueParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramHeaderValueParser == null) {
      paramHeaderValueParser = INSTANCE;
    }
    return paramHeaderValueParser.parseElements(localCharArrayBuffer, paramString);
  }
  
  public static HeaderElement parseHeaderElement(String paramString, HeaderValueParser paramHeaderValueParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramHeaderValueParser == null) {
      paramHeaderValueParser = INSTANCE;
    }
    return paramHeaderValueParser.parseHeaderElement(localCharArrayBuffer, paramString);
  }
  
  public static NameValuePair parseNameValuePair(String paramString, HeaderValueParser paramHeaderValueParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramHeaderValueParser == null) {
      paramHeaderValueParser = INSTANCE;
    }
    return paramHeaderValueParser.parseNameValuePair(localCharArrayBuffer, paramString);
  }
  
  public static NameValuePair[] parseParameters(String paramString, HeaderValueParser paramHeaderValueParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramHeaderValueParser == null) {
      paramHeaderValueParser = INSTANCE;
    }
    return paramHeaderValueParser.parseParameters(localCharArrayBuffer, paramString);
  }
  
  protected HeaderElement createHeaderElement(String paramString1, String paramString2, NameValuePair[] paramArrayOfNameValuePair)
  {
    return new BasicHeaderElement(paramString1, paramString2, paramArrayOfNameValuePair);
  }
  
  protected NameValuePair createNameValuePair(String paramString1, String paramString2)
  {
    return new BasicNameValuePair(paramString1, paramString2);
  }
  
  public HeaderElement[] parseElements(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    ArrayList localArrayList = new ArrayList();
    while (!paramParserCursor.atEnd())
    {
      HeaderElement localHeaderElement = parseHeaderElement(paramCharArrayBuffer, paramParserCursor);
      if ((localHeaderElement.getName().length() != 0) || (localHeaderElement.getValue() != null)) {
        localArrayList.add(localHeaderElement);
      }
    }
    return (HeaderElement[])localArrayList.toArray(new HeaderElement[localArrayList.size()]);
  }
  
  public HeaderElement parseHeaderElement(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    NameValuePair localNameValuePair = parseNameValuePair(paramCharArrayBuffer, paramParserCursor);
    if ((!paramParserCursor.atEnd()) && (paramCharArrayBuffer.charAt(paramParserCursor.getPos() - 1) != ',')) {
      paramCharArrayBuffer = parseParameters(paramCharArrayBuffer, paramParserCursor);
    } else {
      paramCharArrayBuffer = null;
    }
    return createHeaderElement(localNameValuePair.getName(), localNameValuePair.getValue(), paramCharArrayBuffer);
  }
  
  public NameValuePair parseNameValuePair(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    return parseNameValuePair(paramCharArrayBuffer, paramParserCursor, ALL_DELIMITERS);
  }
  
  public NameValuePair parseNameValuePair(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor, char[] paramArrayOfChar)
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    int j = paramParserCursor.getPos();
    int i = paramParserCursor.getPos();
    int i3 = paramParserCursor.getUpperBound();
    int i2;
    char c;
    for (;;)
    {
      i2 = 1;
      if (j >= i3) {
        break;
      }
      c = paramCharArrayBuffer.charAt(j);
      if (c == '=') {
        break;
      }
      if (isOneOf(c, paramArrayOfChar))
      {
        m = 1;
        break label84;
      }
      j++;
    }
    int m = 0;
    label84:
    String str;
    if (j == i3)
    {
      str = paramCharArrayBuffer.substringTrimmed(i, i3);
      m = 1;
    }
    else
    {
      str = paramCharArrayBuffer.substringTrimmed(i, j);
      j++;
    }
    if (m != 0)
    {
      paramParserCursor.updatePos(j);
      return createNameValuePair(str, null);
    }
    i = j;
    int k = 0;
    for (int i1 = 0; i < i3; i1 = n)
    {
      c = paramCharArrayBuffer.charAt(i);
      n = i1;
      if (c == '"')
      {
        n = i1;
        if (k == 0) {
          n = i1 ^ 0x1;
        }
      }
      if ((n == 0) && (k == 0) && (isOneOf(c, paramArrayOfChar)))
      {
        m = i2;
        break;
      }
      if (k != 0) {
        k = 0;
      } else if ((n != 0) && (c == '\\')) {
        k = 1;
      } else {
        k = 0;
      }
      i++;
    }
    while ((j < i) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(j)))) {
      j++;
    }
    for (k = i; (k > j) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(k - 1))); k--) {}
    i1 = j;
    int n = k;
    if (k - j >= 2)
    {
      i1 = j;
      n = k;
      if (paramCharArrayBuffer.charAt(j) == '"')
      {
        i1 = j;
        n = k;
        if (paramCharArrayBuffer.charAt(k - 1) == '"')
        {
          i1 = j + 1;
          n = k - 1;
        }
      }
    }
    paramCharArrayBuffer = paramCharArrayBuffer.substring(i1, n);
    j = i;
    if (m != 0) {
      j = i + 1;
    }
    paramParserCursor.updatePos(j);
    return createNameValuePair(str, paramCharArrayBuffer);
  }
  
  public NameValuePair[] parseParameters(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    int i = paramParserCursor.getPos();
    int j = paramParserCursor.getUpperBound();
    while ((i < j) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(i)))) {
      i++;
    }
    paramParserCursor.updatePos(i);
    if (paramParserCursor.atEnd()) {
      return new NameValuePair[0];
    }
    ArrayList localArrayList = new ArrayList();
    do
    {
      if (paramParserCursor.atEnd()) {
        break;
      }
      localArrayList.add(parseNameValuePair(paramCharArrayBuffer, paramParserCursor));
    } while (paramCharArrayBuffer.charAt(paramParserCursor.getPos() - 1) != ',');
    return (NameValuePair[])localArrayList.toArray(new NameValuePair[localArrayList.size()]);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHeaderValueParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */