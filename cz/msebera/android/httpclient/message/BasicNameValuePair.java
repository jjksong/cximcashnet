package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Serializable;

@Immutable
public class BasicNameValuePair
  implements NameValuePair, Cloneable, Serializable
{
  private static final long serialVersionUID = -6437800749411518984L;
  private final String name;
  private final String value;
  
  public BasicNameValuePair(String paramString1, String paramString2)
  {
    this.name = ((String)Args.notNull(paramString1, "Name"));
    this.value = paramString2;
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof NameValuePair))
    {
      paramObject = (BasicNameValuePair)paramObject;
      if ((!this.name.equals(((BasicNameValuePair)paramObject).name)) || (!LangUtils.equals(this.value, ((BasicNameValuePair)paramObject).value))) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public int hashCode()
  {
    return LangUtils.hashCode(LangUtils.hashCode(17, this.name), this.value);
  }
  
  public String toString()
  {
    if (this.value == null) {
      return this.name;
    }
    StringBuilder localStringBuilder = new StringBuilder(this.name.length() + 1 + this.value.length());
    localStringBuilder.append(this.name);
    localStringBuilder.append("=");
    localStringBuilder.append(this.value);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicNameValuePair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */