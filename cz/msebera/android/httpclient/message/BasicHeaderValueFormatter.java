package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;

@Immutable
public class BasicHeaderValueFormatter
  implements HeaderValueFormatter
{
  @Deprecated
  public static final BasicHeaderValueFormatter DEFAULT = new BasicHeaderValueFormatter();
  public static final BasicHeaderValueFormatter INSTANCE = new BasicHeaderValueFormatter();
  public static final String SEPARATORS = " ;,:@()<>\\\"/[]?={}\t";
  public static final String UNSAFE_CHARS = "\"\\";
  
  public static String formatElements(HeaderElement[] paramArrayOfHeaderElement, boolean paramBoolean, HeaderValueFormatter paramHeaderValueFormatter)
  {
    if (paramHeaderValueFormatter == null) {
      paramHeaderValueFormatter = INSTANCE;
    }
    return paramHeaderValueFormatter.formatElements(null, paramArrayOfHeaderElement, paramBoolean).toString();
  }
  
  public static String formatHeaderElement(HeaderElement paramHeaderElement, boolean paramBoolean, HeaderValueFormatter paramHeaderValueFormatter)
  {
    if (paramHeaderValueFormatter == null) {
      paramHeaderValueFormatter = INSTANCE;
    }
    return paramHeaderValueFormatter.formatHeaderElement(null, paramHeaderElement, paramBoolean).toString();
  }
  
  public static String formatNameValuePair(NameValuePair paramNameValuePair, boolean paramBoolean, HeaderValueFormatter paramHeaderValueFormatter)
  {
    if (paramHeaderValueFormatter == null) {
      paramHeaderValueFormatter = INSTANCE;
    }
    return paramHeaderValueFormatter.formatNameValuePair(null, paramNameValuePair, paramBoolean).toString();
  }
  
  public static String formatParameters(NameValuePair[] paramArrayOfNameValuePair, boolean paramBoolean, HeaderValueFormatter paramHeaderValueFormatter)
  {
    if (paramHeaderValueFormatter == null) {
      paramHeaderValueFormatter = INSTANCE;
    }
    return paramHeaderValueFormatter.formatParameters(null, paramArrayOfNameValuePair, paramBoolean).toString();
  }
  
  protected void doFormatValue(CharArrayBuffer paramCharArrayBuffer, String paramString, boolean paramBoolean)
  {
    int j = 0;
    boolean bool = paramBoolean;
    if (!paramBoolean)
    {
      for (i = 0; (i < paramString.length()) && (!paramBoolean); i++) {
        paramBoolean = isSeparator(paramString.charAt(i));
      }
      bool = paramBoolean;
    }
    int i = j;
    if (bool) {
      paramCharArrayBuffer.append('"');
    }
    for (i = j; i < paramString.length(); i++)
    {
      char c = paramString.charAt(i);
      if (isUnsafe(c)) {
        paramCharArrayBuffer.append('\\');
      }
      paramCharArrayBuffer.append(c);
    }
    if (bool) {
      paramCharArrayBuffer.append('"');
    }
  }
  
  protected int estimateElementsLen(HeaderElement[] paramArrayOfHeaderElement)
  {
    int j = 0;
    if ((paramArrayOfHeaderElement != null) && (paramArrayOfHeaderElement.length >= 1))
    {
      int i = (paramArrayOfHeaderElement.length - 1) * 2;
      int k = paramArrayOfHeaderElement.length;
      while (j < k)
      {
        i += estimateHeaderElementLen(paramArrayOfHeaderElement[j]);
        j++;
      }
      return i;
    }
    return 0;
  }
  
  protected int estimateHeaderElementLen(HeaderElement paramHeaderElement)
  {
    int k = 0;
    if (paramHeaderElement == null) {
      return 0;
    }
    int j = paramHeaderElement.getName().length();
    String str = paramHeaderElement.getValue();
    int i = j;
    if (str != null) {
      i = j + (str.length() + 3);
    }
    int m = paramHeaderElement.getParameterCount();
    j = i;
    if (m > 0) {
      for (;;)
      {
        j = i;
        if (k >= m) {
          break;
        }
        i += estimateNameValuePairLen(paramHeaderElement.getParameter(k)) + 2;
        k++;
      }
    }
    return j;
  }
  
  protected int estimateNameValuePairLen(NameValuePair paramNameValuePair)
  {
    if (paramNameValuePair == null) {
      return 0;
    }
    int j = paramNameValuePair.getName().length();
    paramNameValuePair = paramNameValuePair.getValue();
    int i = j;
    if (paramNameValuePair != null) {
      i = j + (paramNameValuePair.length() + 3);
    }
    return i;
  }
  
  protected int estimateParametersLen(NameValuePair[] paramArrayOfNameValuePair)
  {
    int j = 0;
    if ((paramArrayOfNameValuePair != null) && (paramArrayOfNameValuePair.length >= 1))
    {
      int i = (paramArrayOfNameValuePair.length - 1) * 2;
      int k = paramArrayOfNameValuePair.length;
      while (j < k)
      {
        i += estimateNameValuePairLen(paramArrayOfNameValuePair[j]);
        j++;
      }
      return i;
    }
    return 0;
  }
  
  public CharArrayBuffer formatElements(CharArrayBuffer paramCharArrayBuffer, HeaderElement[] paramArrayOfHeaderElement, boolean paramBoolean)
  {
    Args.notNull(paramArrayOfHeaderElement, "Header element array");
    int i = estimateElementsLen(paramArrayOfHeaderElement);
    if (paramCharArrayBuffer == null) {
      paramCharArrayBuffer = new CharArrayBuffer(i);
    } else {
      paramCharArrayBuffer.ensureCapacity(i);
    }
    for (i = 0; i < paramArrayOfHeaderElement.length; i++)
    {
      if (i > 0) {
        paramCharArrayBuffer.append(", ");
      }
      formatHeaderElement(paramCharArrayBuffer, paramArrayOfHeaderElement[i], paramBoolean);
    }
    return paramCharArrayBuffer;
  }
  
  public CharArrayBuffer formatHeaderElement(CharArrayBuffer paramCharArrayBuffer, HeaderElement paramHeaderElement, boolean paramBoolean)
  {
    Args.notNull(paramHeaderElement, "Header element");
    int i = estimateHeaderElementLen(paramHeaderElement);
    if (paramCharArrayBuffer == null) {
      paramCharArrayBuffer = new CharArrayBuffer(i);
    } else {
      paramCharArrayBuffer.ensureCapacity(i);
    }
    paramCharArrayBuffer.append(paramHeaderElement.getName());
    String str = paramHeaderElement.getValue();
    if (str != null)
    {
      paramCharArrayBuffer.append('=');
      doFormatValue(paramCharArrayBuffer, str, paramBoolean);
    }
    int j = paramHeaderElement.getParameterCount();
    if (j > 0) {
      for (i = 0; i < j; i++)
      {
        paramCharArrayBuffer.append("; ");
        formatNameValuePair(paramCharArrayBuffer, paramHeaderElement.getParameter(i), paramBoolean);
      }
    }
    return paramCharArrayBuffer;
  }
  
  public CharArrayBuffer formatNameValuePair(CharArrayBuffer paramCharArrayBuffer, NameValuePair paramNameValuePair, boolean paramBoolean)
  {
    Args.notNull(paramNameValuePair, "Name / value pair");
    int i = estimateNameValuePairLen(paramNameValuePair);
    if (paramCharArrayBuffer == null) {
      paramCharArrayBuffer = new CharArrayBuffer(i);
    } else {
      paramCharArrayBuffer.ensureCapacity(i);
    }
    paramCharArrayBuffer.append(paramNameValuePair.getName());
    paramNameValuePair = paramNameValuePair.getValue();
    if (paramNameValuePair != null)
    {
      paramCharArrayBuffer.append('=');
      doFormatValue(paramCharArrayBuffer, paramNameValuePair, paramBoolean);
    }
    return paramCharArrayBuffer;
  }
  
  public CharArrayBuffer formatParameters(CharArrayBuffer paramCharArrayBuffer, NameValuePair[] paramArrayOfNameValuePair, boolean paramBoolean)
  {
    Args.notNull(paramArrayOfNameValuePair, "Header parameter array");
    int i = estimateParametersLen(paramArrayOfNameValuePair);
    if (paramCharArrayBuffer == null) {
      paramCharArrayBuffer = new CharArrayBuffer(i);
    } else {
      paramCharArrayBuffer.ensureCapacity(i);
    }
    for (i = 0; i < paramArrayOfNameValuePair.length; i++)
    {
      if (i > 0) {
        paramCharArrayBuffer.append("; ");
      }
      formatNameValuePair(paramCharArrayBuffer, paramArrayOfNameValuePair[i], paramBoolean);
    }
    return paramCharArrayBuffer;
  }
  
  protected boolean isSeparator(char paramChar)
  {
    boolean bool;
    if (" ;,:@()<>\\\"/[]?={}\t".indexOf(paramChar) >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected boolean isUnsafe(char paramChar)
  {
    boolean bool;
    if ("\"\\".indexOf(paramChar) >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHeaderValueFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */