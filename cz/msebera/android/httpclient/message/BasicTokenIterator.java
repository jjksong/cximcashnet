package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.TokenIterator;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.NoSuchElementException;

@NotThreadSafe
public class BasicTokenIterator
  implements TokenIterator
{
  public static final String HTTP_SEPARATORS = " ,;=()<>@:\\\"/[]?{}\t";
  protected String currentHeader;
  protected String currentToken;
  protected final HeaderIterator headerIt;
  protected int searchPos;
  
  public BasicTokenIterator(HeaderIterator paramHeaderIterator)
  {
    this.headerIt = ((HeaderIterator)Args.notNull(paramHeaderIterator, "Header iterator"));
    this.searchPos = findNext(-1);
  }
  
  protected String createToken(String paramString, int paramInt1, int paramInt2)
  {
    return paramString.substring(paramInt1, paramInt2);
  }
  
  protected int findNext(int paramInt)
    throws ParseException
  {
    if (paramInt < 0)
    {
      if (!this.headerIt.hasNext()) {
        return -1;
      }
      this.currentHeader = this.headerIt.nextHeader().getValue();
      paramInt = 0;
    }
    else
    {
      paramInt = findTokenSeparator(paramInt);
    }
    paramInt = findTokenStart(paramInt);
    if (paramInt < 0)
    {
      this.currentToken = null;
      return -1;
    }
    int i = findTokenEnd(paramInt);
    this.currentToken = createToken(this.currentHeader, paramInt, i);
    return i;
  }
  
  protected int findTokenEnd(int paramInt)
  {
    Args.notNegative(paramInt, "Search position");
    int i = this.currentHeader.length();
    paramInt++;
    while ((paramInt < i) && (isTokenChar(this.currentHeader.charAt(paramInt)))) {
      paramInt++;
    }
    return paramInt;
  }
  
  protected int findTokenSeparator(int paramInt)
  {
    paramInt = Args.notNegative(paramInt, "Search position");
    int j = this.currentHeader.length();
    int i = 0;
    while ((i == 0) && (paramInt < j))
    {
      char c = this.currentHeader.charAt(paramInt);
      if (isTokenSeparator(c))
      {
        i = 1;
      }
      else if (isWhitespace(c))
      {
        paramInt++;
      }
      else
      {
        if (isTokenChar(c))
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("Tokens without separator (pos ");
          localStringBuilder.append(paramInt);
          localStringBuilder.append("): ");
          localStringBuilder.append(this.currentHeader);
          throw new ParseException(localStringBuilder.toString());
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Invalid character after token (pos ");
        localStringBuilder.append(paramInt);
        localStringBuilder.append("): ");
        localStringBuilder.append(this.currentHeader);
        throw new ParseException(localStringBuilder.toString());
      }
    }
    return paramInt;
  }
  
  protected int findTokenStart(int paramInt)
  {
    paramInt = Args.notNegative(paramInt, "Search position");
    int j = 0;
    while (j == 0)
    {
      Object localObject = this.currentHeader;
      if (localObject == null) {
        break;
      }
      int m = ((String)localObject).length();
      int i = j;
      int k = paramInt;
      while ((i == 0) && (k < m))
      {
        char c = this.currentHeader.charAt(k);
        if ((!isTokenSeparator(c)) && (!isWhitespace(c)))
        {
          if (isTokenChar(this.currentHeader.charAt(k)))
          {
            i = 1;
          }
          else
          {
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append("Invalid character before token (pos ");
            ((StringBuilder)localObject).append(k);
            ((StringBuilder)localObject).append("): ");
            ((StringBuilder)localObject).append(this.currentHeader);
            throw new ParseException(((StringBuilder)localObject).toString());
          }
        }
        else {
          k++;
        }
      }
      paramInt = k;
      j = i;
      if (i == 0) {
        if (this.headerIt.hasNext())
        {
          this.currentHeader = this.headerIt.nextHeader().getValue();
          paramInt = 0;
          j = i;
        }
        else
        {
          this.currentHeader = null;
          paramInt = k;
          j = i;
        }
      }
    }
    if (j == 0) {
      paramInt = -1;
    }
    return paramInt;
  }
  
  public boolean hasNext()
  {
    boolean bool;
    if (this.currentToken != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected boolean isHttpSeparator(char paramChar)
  {
    boolean bool;
    if (" ,;=()<>@:\\\"/[]?{}\t".indexOf(paramChar) >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected boolean isTokenChar(char paramChar)
  {
    if (Character.isLetterOrDigit(paramChar)) {
      return true;
    }
    if (Character.isISOControl(paramChar)) {
      return false;
    }
    return !isHttpSeparator(paramChar);
  }
  
  protected boolean isTokenSeparator(char paramChar)
  {
    boolean bool;
    if (paramChar == ',') {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected boolean isWhitespace(char paramChar)
  {
    boolean bool;
    if ((paramChar != '\t') && (!Character.isSpaceChar(paramChar))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public final Object next()
    throws NoSuchElementException, ParseException
  {
    return nextToken();
  }
  
  public String nextToken()
    throws NoSuchElementException, ParseException
  {
    String str = this.currentToken;
    if (str != null)
    {
      this.searchPos = findNext(this.searchPos);
      return str;
    }
    throw new NoSuchElementException("Iteration already finished.");
  }
  
  public final void remove()
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException("Removing tokens is not supported.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicTokenIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */