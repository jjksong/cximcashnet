package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;

@Immutable
public class BasicLineParser
  implements LineParser
{
  @Deprecated
  public static final BasicLineParser DEFAULT = new BasicLineParser();
  public static final BasicLineParser INSTANCE = new BasicLineParser();
  protected final ProtocolVersion protocol;
  
  public BasicLineParser()
  {
    this(null);
  }
  
  public BasicLineParser(ProtocolVersion paramProtocolVersion)
  {
    if (paramProtocolVersion == null) {
      paramProtocolVersion = HttpVersion.HTTP_1_1;
    }
    this.protocol = paramProtocolVersion;
  }
  
  public static Header parseHeader(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    if (paramLineParser == null) {
      paramLineParser = INSTANCE;
    }
    return paramLineParser.parseHeader(localCharArrayBuffer);
  }
  
  public static ProtocolVersion parseProtocolVersion(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramLineParser == null) {
      paramLineParser = INSTANCE;
    }
    return paramLineParser.parseProtocolVersion(localCharArrayBuffer, paramString);
  }
  
  public static RequestLine parseRequestLine(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramLineParser == null) {
      paramLineParser = INSTANCE;
    }
    return paramLineParser.parseRequestLine(localCharArrayBuffer, paramString);
  }
  
  public static StatusLine parseStatusLine(String paramString, LineParser paramLineParser)
    throws ParseException
  {
    Args.notNull(paramString, "Value");
    CharArrayBuffer localCharArrayBuffer = new CharArrayBuffer(paramString.length());
    localCharArrayBuffer.append(paramString);
    paramString = new ParserCursor(0, paramString.length());
    if (paramLineParser == null) {
      paramLineParser = INSTANCE;
    }
    return paramLineParser.parseStatusLine(localCharArrayBuffer, paramString);
  }
  
  protected ProtocolVersion createProtocolVersion(int paramInt1, int paramInt2)
  {
    return this.protocol.forVersion(paramInt1, paramInt2);
  }
  
  protected RequestLine createRequestLine(String paramString1, String paramString2, ProtocolVersion paramProtocolVersion)
  {
    return new BasicRequestLine(paramString1, paramString2, paramProtocolVersion);
  }
  
  protected StatusLine createStatusLine(ProtocolVersion paramProtocolVersion, int paramInt, String paramString)
  {
    return new BasicStatusLine(paramProtocolVersion, paramInt, paramString);
  }
  
  public boolean hasProtocolVersion(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    int j = paramParserCursor.getPos();
    paramParserCursor = this.protocol.getProtocol();
    int k = paramParserCursor.length();
    if (paramCharArrayBuffer.length() < k + 4) {
      return false;
    }
    int i;
    if (j < 0)
    {
      i = paramCharArrayBuffer.length() - 4 - k;
    }
    else
    {
      i = j;
      if (j == 0) {
        for (;;)
        {
          i = j;
          if (j >= paramCharArrayBuffer.length()) {
            break;
          }
          i = j;
          if (!HTTP.isWhitespace(paramCharArrayBuffer.charAt(j))) {
            break;
          }
          j++;
        }
      }
    }
    int m = i + k;
    if (m + 4 > paramCharArrayBuffer.length()) {
      return false;
    }
    boolean bool1 = true;
    for (j = 0; (bool1) && (j < k); j++) {
      if (paramCharArrayBuffer.charAt(i + j) == paramParserCursor.charAt(j)) {
        bool1 = true;
      } else {
        bool1 = false;
      }
    }
    boolean bool2 = bool1;
    if (bool1) {
      if (paramCharArrayBuffer.charAt(m) == '/') {
        bool2 = true;
      } else {
        bool2 = false;
      }
    }
    return bool2;
  }
  
  public Header parseHeader(CharArrayBuffer paramCharArrayBuffer)
    throws ParseException
  {
    return new BufferedHeader(paramCharArrayBuffer);
  }
  
  public ProtocolVersion parseProtocolVersion(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    String str = this.protocol.getProtocol();
    int i1 = str.length();
    int m = paramParserCursor.getPos();
    int k = paramParserCursor.getUpperBound();
    skipWhitespace(paramCharArrayBuffer, paramParserCursor);
    int n = paramParserCursor.getPos();
    int i2 = n + i1;
    if (i2 + 4 <= k)
    {
      int i = 1;
      for (int j = 0; (i != 0) && (j < i1); j++) {
        if (paramCharArrayBuffer.charAt(n + j) == str.charAt(j)) {
          i = 1;
        } else {
          i = 0;
        }
      }
      j = i;
      if (i != 0) {
        if (paramCharArrayBuffer.charAt(i2) == '/') {
          j = 1;
        } else {
          j = 0;
        }
      }
      if (j != 0)
      {
        j = n + (i1 + 1);
        i = paramCharArrayBuffer.indexOf(46, j, k);
        if (i != -1) {
          try
          {
            n = Integer.parseInt(paramCharArrayBuffer.substringTrimmed(j, i));
            i1 = i + 1;
            j = paramCharArrayBuffer.indexOf(32, i1, k);
            i = j;
            if (j == -1) {
              i = k;
            }
            try
            {
              j = Integer.parseInt(paramCharArrayBuffer.substringTrimmed(i1, i));
              paramParserCursor.updatePos(i);
              return createProtocolVersion(n, j);
            }
            catch (NumberFormatException paramParserCursor)
            {
              paramParserCursor = new StringBuilder();
              paramParserCursor.append("Invalid protocol minor version number: ");
              paramParserCursor.append(paramCharArrayBuffer.substring(m, k));
              throw new ParseException(paramParserCursor.toString());
            }
            paramParserCursor = new StringBuilder();
          }
          catch (NumberFormatException paramParserCursor)
          {
            paramParserCursor = new StringBuilder();
            paramParserCursor.append("Invalid protocol major version number: ");
            paramParserCursor.append(paramCharArrayBuffer.substring(m, k));
            throw new ParseException(paramParserCursor.toString());
          }
        }
        paramParserCursor.append("Invalid protocol version number: ");
        paramParserCursor.append(paramCharArrayBuffer.substring(m, k));
        throw new ParseException(paramParserCursor.toString());
      }
      paramParserCursor = new StringBuilder();
      paramParserCursor.append("Not a valid protocol version: ");
      paramParserCursor.append(paramCharArrayBuffer.substring(m, k));
      throw new ParseException(paramParserCursor.toString());
    }
    paramParserCursor = new StringBuilder();
    paramParserCursor.append("Not a valid protocol version: ");
    paramParserCursor.append(paramCharArrayBuffer.substring(m, k));
    throw new ParseException(paramParserCursor.toString());
  }
  
  public RequestLine parseRequestLine(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    Args.notNull(paramCharArrayBuffer, "Char array buffer");
    Args.notNull(paramParserCursor, "Parser cursor");
    int j = paramParserCursor.getPos();
    int i = paramParserCursor.getUpperBound();
    try
    {
      skipWhitespace(paramCharArrayBuffer, paramParserCursor);
      int k = paramParserCursor.getPos();
      int m = paramCharArrayBuffer.indexOf(32, k, i);
      if (m >= 0)
      {
        String str1 = paramCharArrayBuffer.substringTrimmed(k, m);
        paramParserCursor.updatePos(m);
        skipWhitespace(paramCharArrayBuffer, paramParserCursor);
        m = paramParserCursor.getPos();
        k = paramCharArrayBuffer.indexOf(32, m, i);
        if (k >= 0)
        {
          String str2 = paramCharArrayBuffer.substringTrimmed(m, k);
          paramParserCursor.updatePos(k);
          localObject = parseProtocolVersion(paramCharArrayBuffer, paramParserCursor);
          skipWhitespace(paramCharArrayBuffer, paramParserCursor);
          if (paramParserCursor.atEnd()) {
            return createRequestLine(str1, str2, (ProtocolVersion)localObject);
          }
          paramParserCursor = new cz/msebera/android/httpclient/ParseException;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("Invalid request line: ");
          ((StringBuilder)localObject).append(paramCharArrayBuffer.substring(j, i));
          paramParserCursor.<init>(((StringBuilder)localObject).toString());
          throw paramParserCursor;
        }
        localObject = new cz/msebera/android/httpclient/ParseException;
        paramParserCursor = new java/lang/StringBuilder;
        paramParserCursor.<init>();
        paramParserCursor.append("Invalid request line: ");
        paramParserCursor.append(paramCharArrayBuffer.substring(j, i));
        ((ParseException)localObject).<init>(paramParserCursor.toString());
        throw ((Throwable)localObject);
      }
      paramParserCursor = new cz/msebera/android/httpclient/ParseException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("Invalid request line: ");
      ((StringBuilder)localObject).append(paramCharArrayBuffer.substring(j, i));
      paramParserCursor.<init>(((StringBuilder)localObject).toString());
      throw paramParserCursor;
    }
    catch (IndexOutOfBoundsException paramParserCursor)
    {
      paramParserCursor = new StringBuilder();
      paramParserCursor.append("Invalid request line: ");
      paramParserCursor.append(paramCharArrayBuffer.substring(j, i));
      throw new ParseException(paramParserCursor.toString());
    }
  }
  
  /* Error */
  public StatusLine parseStatusLine(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
    throws ParseException
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 111
    //   3: invokestatic 47	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_2
    //   8: ldc 113
    //   10: invokestatic 47	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   13: pop
    //   14: aload_2
    //   15: invokevirtual 116	cz/msebera/android/httpclient/message/ParserCursor:getPos	()I
    //   18: istore 6
    //   20: aload_2
    //   21: invokevirtual 142	cz/msebera/android/httpclient/message/ParserCursor:getUpperBound	()I
    //   24: istore 5
    //   26: aload_0
    //   27: aload_1
    //   28: aload_2
    //   29: invokevirtual 190	cz/msebera/android/httpclient/message/BasicLineParser:parseProtocolVersion	(Lcz/msebera/android/httpclient/util/CharArrayBuffer;Lcz/msebera/android/httpclient/message/ParserCursor;)Lcz/msebera/android/httpclient/ProtocolVersion;
    //   32: astore 8
    //   34: aload_0
    //   35: aload_1
    //   36: aload_2
    //   37: invokevirtual 146	cz/msebera/android/httpclient/message/BasicLineParser:skipWhitespace	(Lcz/msebera/android/httpclient/util/CharArrayBuffer;Lcz/msebera/android/httpclient/message/ParserCursor;)V
    //   40: aload_2
    //   41: invokevirtual 116	cz/msebera/android/httpclient/message/ParserCursor:getPos	()I
    //   44: istore 7
    //   46: aload_1
    //   47: bipush 32
    //   49: iload 7
    //   51: iload 5
    //   53: invokevirtual 150	cz/msebera/android/httpclient/util/CharArrayBuffer:indexOf	(III)I
    //   56: istore 4
    //   58: iload 4
    //   60: istore_3
    //   61: iload 4
    //   63: ifge +6 -> 69
    //   66: iload 5
    //   68: istore_3
    //   69: aload_1
    //   70: iload 7
    //   72: iload_3
    //   73: invokevirtual 154	cz/msebera/android/httpclient/util/CharArrayBuffer:substringTrimmed	(II)Ljava/lang/String;
    //   76: astore_2
    //   77: iconst_0
    //   78: istore 4
    //   80: iload 4
    //   82: aload_2
    //   83: invokevirtual 55	java/lang/String:length	()I
    //   86: if_icmpge +66 -> 152
    //   89: aload_2
    //   90: iload 4
    //   92: invokevirtual 132	java/lang/String:charAt	(I)C
    //   95: invokestatic 203	java/lang/Character:isDigit	(C)Z
    //   98: ifeq +9 -> 107
    //   101: iinc 4 1
    //   104: goto -24 -> 80
    //   107: new 39	cz/msebera/android/httpclient/ParseException
    //   110: astore 8
    //   112: new 167	java/lang/StringBuilder
    //   115: astore_2
    //   116: aload_2
    //   117: invokespecial 168	java/lang/StringBuilder:<init>	()V
    //   120: aload_2
    //   121: ldc -51
    //   123: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload_2
    //   128: aload_1
    //   129: iload 6
    //   131: iload 5
    //   133: invokevirtual 176	cz/msebera/android/httpclient/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   136: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload 8
    //   142: aload_2
    //   143: invokevirtual 179	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   146: invokespecial 181	cz/msebera/android/httpclient/ParseException:<init>	(Ljava/lang/String;)V
    //   149: aload 8
    //   151: athrow
    //   152: aload_2
    //   153: invokestatic 160	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   156: istore 4
    //   158: iload_3
    //   159: iload 5
    //   161: if_icmpge +14 -> 175
    //   164: aload_1
    //   165: iload_3
    //   166: iload 5
    //   168: invokevirtual 154	cz/msebera/android/httpclient/util/CharArrayBuffer:substringTrimmed	(II)Ljava/lang/String;
    //   171: astore_2
    //   172: goto +6 -> 178
    //   175: ldc -49
    //   177: astore_2
    //   178: aload_0
    //   179: aload 8
    //   181: iload 4
    //   183: aload_2
    //   184: invokevirtual 209	cz/msebera/android/httpclient/message/BasicLineParser:createStatusLine	(Lcz/msebera/android/httpclient/ProtocolVersion;ILjava/lang/String;)Lcz/msebera/android/httpclient/StatusLine;
    //   187: areturn
    //   188: astore_2
    //   189: new 39	cz/msebera/android/httpclient/ParseException
    //   192: astore 8
    //   194: new 167	java/lang/StringBuilder
    //   197: astore_2
    //   198: aload_2
    //   199: invokespecial 168	java/lang/StringBuilder:<init>	()V
    //   202: aload_2
    //   203: ldc -51
    //   205: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: pop
    //   209: aload_2
    //   210: aload_1
    //   211: iload 6
    //   213: iload 5
    //   215: invokevirtual 176	cz/msebera/android/httpclient/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   218: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload 8
    //   224: aload_2
    //   225: invokevirtual 179	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   228: invokespecial 181	cz/msebera/android/httpclient/ParseException:<init>	(Ljava/lang/String;)V
    //   231: aload 8
    //   233: athrow
    //   234: astore_2
    //   235: new 167	java/lang/StringBuilder
    //   238: dup
    //   239: invokespecial 168	java/lang/StringBuilder:<init>	()V
    //   242: astore_2
    //   243: aload_2
    //   244: ldc -45
    //   246: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   249: pop
    //   250: aload_2
    //   251: aload_1
    //   252: iload 6
    //   254: iload 5
    //   256: invokevirtual 176	cz/msebera/android/httpclient/util/CharArrayBuffer:substring	(II)Ljava/lang/String;
    //   259: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: new 39	cz/msebera/android/httpclient/ParseException
    //   266: dup
    //   267: aload_2
    //   268: invokevirtual 179	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   271: invokespecial 181	cz/msebera/android/httpclient/ParseException:<init>	(Ljava/lang/String;)V
    //   274: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	275	0	this	BasicLineParser
    //   0	275	1	paramCharArrayBuffer	CharArrayBuffer
    //   0	275	2	paramParserCursor	ParserCursor
    //   60	106	3	i	int
    //   56	126	4	j	int
    //   24	231	5	k	int
    //   18	235	6	m	int
    //   44	27	7	n	int
    //   32	200	8	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   152	158	188	java/lang/NumberFormatException
    //   26	58	234	java/lang/IndexOutOfBoundsException
    //   69	77	234	java/lang/IndexOutOfBoundsException
    //   80	101	234	java/lang/IndexOutOfBoundsException
    //   107	152	234	java/lang/IndexOutOfBoundsException
    //   152	158	234	java/lang/IndexOutOfBoundsException
    //   164	172	234	java/lang/IndexOutOfBoundsException
    //   178	188	234	java/lang/IndexOutOfBoundsException
    //   189	234	234	java/lang/IndexOutOfBoundsException
  }
  
  protected void skipWhitespace(CharArrayBuffer paramCharArrayBuffer, ParserCursor paramParserCursor)
  {
    int i = paramParserCursor.getPos();
    int j = paramParserCursor.getUpperBound();
    while ((i < j) && (HTTP.isWhitespace(paramCharArrayBuffer.charAt(i)))) {
      i++;
    }
    paramParserCursor.updatePos(i);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicLineParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */