package cz.msebera.android.httpclient.message;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.util.NoSuchElementException;

@NotThreadSafe
public class BasicHeaderIterator
  implements HeaderIterator
{
  protected final Header[] allHeaders;
  protected int currentIndex;
  protected String headerName;
  
  public BasicHeaderIterator(Header[] paramArrayOfHeader, String paramString)
  {
    this.allHeaders = ((Header[])Args.notNull(paramArrayOfHeader, "Header array"));
    this.headerName = paramString;
    this.currentIndex = findNext(-1);
  }
  
  protected boolean filterHeader(int paramInt)
  {
    String str = this.headerName;
    boolean bool;
    if ((str != null) && (!str.equalsIgnoreCase(this.allHeaders[paramInt].getName()))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  protected int findNext(int paramInt)
  {
    if (paramInt < -1) {
      return -1;
    }
    int i = this.allHeaders.length;
    for (boolean bool = false; (!bool) && (paramInt < i - 1); bool = filterHeader(paramInt)) {
      paramInt++;
    }
    if (!bool) {
      paramInt = -1;
    }
    return paramInt;
  }
  
  public boolean hasNext()
  {
    boolean bool;
    if (this.currentIndex >= 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final Object next()
    throws NoSuchElementException
  {
    return nextHeader();
  }
  
  public Header nextHeader()
    throws NoSuchElementException
  {
    int i = this.currentIndex;
    if (i >= 0)
    {
      this.currentIndex = findNext(i);
      return this.allHeaders[i];
    }
    throw new NoSuchElementException("Iteration already finished.");
  }
  
  public void remove()
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException("Removing headers is not supported.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/message/BasicHeaderIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */