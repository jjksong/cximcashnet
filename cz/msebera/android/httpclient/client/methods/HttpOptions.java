package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.util.Args;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

@NotThreadSafe
public class HttpOptions
  extends HttpRequestBase
{
  public static final String METHOD_NAME = "OPTIONS";
  
  public HttpOptions() {}
  
  public HttpOptions(String paramString)
  {
    setURI(URI.create(paramString));
  }
  
  public HttpOptions(URI paramURI)
  {
    setURI(paramURI);
  }
  
  public Set<String> getAllowedMethods(HttpResponse paramHttpResponse)
  {
    Args.notNull(paramHttpResponse, "HTTP response");
    paramHttpResponse = paramHttpResponse.headerIterator("Allow");
    HashSet localHashSet = new HashSet();
    while (paramHttpResponse.hasNext())
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpResponse.nextHeader().getElements();
      int j = arrayOfHeaderElement.length;
      for (int i = 0; i < j; i++) {
        localHashSet.add(arrayOfHeaderElement[i].getName());
      }
    }
    return localHashSet;
  }
  
  public String getMethod()
  {
    return "OPTIONS";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/HttpOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */