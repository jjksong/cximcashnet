package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.message.AbstractHttpMessage;
import cz.msebera.android.httpclient.message.BasicRequestLine;
import cz.msebera.android.httpclient.params.HttpParams;
import java.net.URI;

@NotThreadSafe
public class HttpRequestWrapper
  extends AbstractHttpMessage
  implements HttpUriRequest
{
  private final String method;
  private final HttpRequest original;
  private URI uri;
  private ProtocolVersion version;
  
  private HttpRequestWrapper(HttpRequest paramHttpRequest)
  {
    this.original = paramHttpRequest;
    this.version = this.original.getRequestLine().getProtocolVersion();
    this.method = this.original.getRequestLine().getMethod();
    if ((paramHttpRequest instanceof HttpUriRequest)) {
      this.uri = ((HttpUriRequest)paramHttpRequest).getURI();
    } else {
      this.uri = null;
    }
    setHeaders(paramHttpRequest.getAllHeaders());
  }
  
  public static HttpRequestWrapper wrap(HttpRequest paramHttpRequest)
  {
    if (paramHttpRequest == null) {
      return null;
    }
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      return new HttpEntityEnclosingRequestWrapper((HttpEntityEnclosingRequest)paramHttpRequest);
    }
    return new HttpRequestWrapper(paramHttpRequest);
  }
  
  public void abort()
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException();
  }
  
  public String getMethod()
  {
    return this.method;
  }
  
  public HttpRequest getOriginal()
  {
    return this.original;
  }
  
  @Deprecated
  public HttpParams getParams()
  {
    if (this.params == null) {
      this.params = this.original.getParams().copy();
    }
    return this.params;
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    ProtocolVersion localProtocolVersion = this.version;
    if (localProtocolVersion == null) {
      localProtocolVersion = this.original.getProtocolVersion();
    }
    return localProtocolVersion;
  }
  
  public RequestLine getRequestLine()
  {
    Object localObject1 = this.uri;
    if (localObject1 != null) {
      localObject1 = ((URI)localObject1).toASCIIString();
    } else {
      localObject1 = this.original.getRequestLine().getUri();
    }
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      if (((String)localObject1).length() != 0) {}
    }
    else
    {
      localObject2 = "/";
    }
    return new BasicRequestLine(this.method, (String)localObject2, getProtocolVersion());
  }
  
  public URI getURI()
  {
    return this.uri;
  }
  
  public boolean isAborted()
  {
    return false;
  }
  
  public void setProtocolVersion(ProtocolVersion paramProtocolVersion)
  {
    this.version = paramProtocolVersion;
  }
  
  public void setURI(URI paramURI)
  {
    this.uri = paramURI;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getRequestLine());
    localStringBuilder.append(" ");
    localStringBuilder.append(this.headergroup);
    return localStringBuilder.toString();
  }
  
  static class HttpEntityEnclosingRequestWrapper
    extends HttpRequestWrapper
    implements HttpEntityEnclosingRequest
  {
    private HttpEntity entity;
    
    public HttpEntityEnclosingRequestWrapper(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
    {
      super(null);
      this.entity = paramHttpEntityEnclosingRequest.getEntity();
    }
    
    public boolean expectContinue()
    {
      Header localHeader = getFirstHeader("Expect");
      boolean bool;
      if ((localHeader != null) && ("100-continue".equalsIgnoreCase(localHeader.getValue()))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public HttpEntity getEntity()
    {
      return this.entity;
    }
    
    public void setEntity(HttpEntity paramHttpEntity)
    {
      this.entity = paramHttpEntity;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/HttpRequestWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */