package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.HttpResponse;
import java.io.Closeable;

public abstract interface CloseableHttpResponse
  extends HttpResponse, Closeable
{}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/CloseableHttpResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */