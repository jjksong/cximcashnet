package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.client.config.RequestConfig;

public abstract interface Configurable
{
  public abstract RequestConfig getConfig();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/Configurable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */