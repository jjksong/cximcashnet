package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.message.HeaderGroup;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.Args;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@NotThreadSafe
public class RequestBuilder
{
  private RequestConfig config;
  private HttpEntity entity;
  private HeaderGroup headergroup;
  private String method;
  private LinkedList<NameValuePair> parameters;
  private URI uri;
  private ProtocolVersion version;
  
  RequestBuilder()
  {
    this(null);
  }
  
  RequestBuilder(String paramString)
  {
    this.method = paramString;
  }
  
  public static RequestBuilder copy(HttpRequest paramHttpRequest)
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    return new RequestBuilder().doCopy(paramHttpRequest);
  }
  
  public static RequestBuilder create(String paramString)
  {
    Args.notBlank(paramString, "HTTP method");
    return new RequestBuilder(paramString);
  }
  
  public static RequestBuilder delete()
  {
    return new RequestBuilder("DELETE");
  }
  
  private RequestBuilder doCopy(HttpRequest paramHttpRequest)
  {
    if (paramHttpRequest == null) {
      return this;
    }
    this.method = paramHttpRequest.getRequestLine().getMethod();
    this.version = paramHttpRequest.getRequestLine().getProtocolVersion();
    if ((paramHttpRequest instanceof HttpUriRequest)) {
      this.uri = ((HttpUriRequest)paramHttpRequest).getURI();
    } else {
      this.uri = URI.create(paramHttpRequest.getRequestLine().getUri());
    }
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.clear();
    this.headergroup.setHeaders(paramHttpRequest.getAllHeaders());
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      this.entity = ((HttpEntityEnclosingRequest)paramHttpRequest).getEntity();
    } else {
      this.entity = null;
    }
    if ((paramHttpRequest instanceof Configurable)) {
      this.config = ((Configurable)paramHttpRequest).getConfig();
    } else {
      this.config = null;
    }
    this.parameters = null;
    return this;
  }
  
  public static RequestBuilder get()
  {
    return new RequestBuilder("GET");
  }
  
  public static RequestBuilder head()
  {
    return new RequestBuilder("HEAD");
  }
  
  public static RequestBuilder options()
  {
    return new RequestBuilder("OPTIONS");
  }
  
  public static RequestBuilder post()
  {
    return new RequestBuilder("POST");
  }
  
  public static RequestBuilder put()
  {
    return new RequestBuilder("PUT");
  }
  
  public static RequestBuilder trace()
  {
    return new RequestBuilder("TRACE");
  }
  
  public RequestBuilder addHeader(Header paramHeader)
  {
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.addHeader(paramHeader);
    return this;
  }
  
  public RequestBuilder addHeader(String paramString1, String paramString2)
  {
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.addHeader(new BasicHeader(paramString1, paramString2));
    return this;
  }
  
  public RequestBuilder addParameter(NameValuePair paramNameValuePair)
  {
    Args.notNull(paramNameValuePair, "Name value pair");
    if (this.parameters == null) {
      this.parameters = new LinkedList();
    }
    this.parameters.add(paramNameValuePair);
    return this;
  }
  
  public RequestBuilder addParameter(String paramString1, String paramString2)
  {
    return addParameter(new BasicNameValuePair(paramString1, paramString2));
  }
  
  public RequestBuilder addParameters(NameValuePair... paramVarArgs)
  {
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++) {
      addParameter(paramVarArgs[i]);
    }
    return this;
  }
  
  public HttpUriRequest build()
  {
    Object localObject1 = this.uri;
    if (localObject1 == null) {
      localObject1 = URI.create("/");
    }
    HttpEntity localHttpEntity = this.entity;
    LinkedList localLinkedList = this.parameters;
    Object localObject2 = localObject1;
    Object localObject4 = localHttpEntity;
    if (localLinkedList != null)
    {
      localObject2 = localObject1;
      localObject4 = localHttpEntity;
      if (!localLinkedList.isEmpty()) {
        if ((localHttpEntity == null) && (("POST".equalsIgnoreCase(this.method)) || ("PUT".equalsIgnoreCase(this.method))))
        {
          localObject4 = new UrlEncodedFormEntity(this.parameters, HTTP.DEF_CONTENT_CHARSET);
          localObject2 = localObject1;
        }
        else
        {
          try
          {
            localObject2 = new cz/msebera/android/httpclient/client/utils/URIBuilder;
            ((URIBuilder)localObject2).<init>((URI)localObject1);
            localObject2 = ((URIBuilder)localObject2).addParameters(this.parameters).build();
            localObject4 = localHttpEntity;
          }
          catch (URISyntaxException localURISyntaxException)
          {
            localObject4 = localHttpEntity;
            localObject3 = localObject1;
          }
        }
      }
    }
    if (localObject4 == null)
    {
      localObject1 = new InternalRequest(this.method);
    }
    else
    {
      localObject1 = new InternalEntityEclosingRequest(this.method);
      ((InternalEntityEclosingRequest)localObject1).setEntity((HttpEntity)localObject4);
    }
    ((HttpRequestBase)localObject1).setProtocolVersion(this.version);
    ((HttpRequestBase)localObject1).setURI((URI)localObject3);
    Object localObject3 = this.headergroup;
    if (localObject3 != null) {
      ((HttpRequestBase)localObject1).setHeaders(((HeaderGroup)localObject3).getAllHeaders());
    }
    ((HttpRequestBase)localObject1).setConfig(this.config);
    return (HttpUriRequest)localObject1;
  }
  
  public RequestConfig getConfig()
  {
    return this.config;
  }
  
  public HttpEntity getEntity()
  {
    return this.entity;
  }
  
  public Header getFirstHeader(String paramString)
  {
    HeaderGroup localHeaderGroup = this.headergroup;
    if (localHeaderGroup != null) {
      paramString = localHeaderGroup.getFirstHeader(paramString);
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  public Header[] getHeaders(String paramString)
  {
    HeaderGroup localHeaderGroup = this.headergroup;
    if (localHeaderGroup != null) {
      paramString = localHeaderGroup.getHeaders(paramString);
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  public Header getLastHeader(String paramString)
  {
    HeaderGroup localHeaderGroup = this.headergroup;
    if (localHeaderGroup != null) {
      paramString = localHeaderGroup.getLastHeader(paramString);
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  public String getMethod()
  {
    return this.method;
  }
  
  public List<NameValuePair> getParameters()
  {
    Object localObject = this.parameters;
    if (localObject != null) {
      localObject = new ArrayList((Collection)localObject);
    } else {
      localObject = new ArrayList();
    }
    return (List<NameValuePair>)localObject;
  }
  
  public URI getUri()
  {
    return this.uri;
  }
  
  public ProtocolVersion getVersion()
  {
    return this.version;
  }
  
  public RequestBuilder removeHeader(Header paramHeader)
  {
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.removeHeader(paramHeader);
    return this;
  }
  
  public RequestBuilder removeHeaders(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = this.headergroup;
      if (localObject != null)
      {
        localObject = ((HeaderGroup)localObject).iterator();
        while (((HeaderIterator)localObject).hasNext()) {
          if (paramString.equalsIgnoreCase(((HeaderIterator)localObject).nextHeader().getName())) {
            ((HeaderIterator)localObject).remove();
          }
        }
        return this;
      }
    }
    return this;
  }
  
  public RequestBuilder setConfig(RequestConfig paramRequestConfig)
  {
    this.config = paramRequestConfig;
    return this;
  }
  
  public RequestBuilder setEntity(HttpEntity paramHttpEntity)
  {
    this.entity = paramHttpEntity;
    return this;
  }
  
  public RequestBuilder setHeader(Header paramHeader)
  {
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.updateHeader(paramHeader);
    return this;
  }
  
  public RequestBuilder setHeader(String paramString1, String paramString2)
  {
    if (this.headergroup == null) {
      this.headergroup = new HeaderGroup();
    }
    this.headergroup.updateHeader(new BasicHeader(paramString1, paramString2));
    return this;
  }
  
  public RequestBuilder setUri(String paramString)
  {
    if (paramString != null) {
      paramString = URI.create(paramString);
    } else {
      paramString = null;
    }
    this.uri = paramString;
    return this;
  }
  
  public RequestBuilder setUri(URI paramURI)
  {
    this.uri = paramURI;
    return this;
  }
  
  public RequestBuilder setVersion(ProtocolVersion paramProtocolVersion)
  {
    this.version = paramProtocolVersion;
    return this;
  }
  
  static class InternalEntityEclosingRequest
    extends HttpEntityEnclosingRequestBase
  {
    private final String method;
    
    InternalEntityEclosingRequest(String paramString)
    {
      this.method = paramString;
    }
    
    public String getMethod()
    {
      return this.method;
    }
  }
  
  static class InternalRequest
    extends HttpRequestBase
  {
    private final String method;
    
    InternalRequest(String paramString)
    {
      this.method = paramString;
    }
    
    public String getMethod()
    {
      return this.method;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/RequestBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */