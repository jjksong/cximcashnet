package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.message.BasicRequestLine;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import java.net.URI;

@NotThreadSafe
public abstract class HttpRequestBase
  extends AbstractExecutionAwareRequest
  implements HttpUriRequest, Configurable
{
  private RequestConfig config;
  private URI uri;
  private ProtocolVersion version;
  
  public RequestConfig getConfig()
  {
    return this.config;
  }
  
  public abstract String getMethod();
  
  public ProtocolVersion getProtocolVersion()
  {
    ProtocolVersion localProtocolVersion = this.version;
    if (localProtocolVersion == null) {
      localProtocolVersion = HttpProtocolParams.getVersion(getParams());
    }
    return localProtocolVersion;
  }
  
  public RequestLine getRequestLine()
  {
    String str = getMethod();
    ProtocolVersion localProtocolVersion = getProtocolVersion();
    Object localObject1 = getURI();
    if (localObject1 != null) {
      localObject1 = ((URI)localObject1).toASCIIString();
    } else {
      localObject1 = null;
    }
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      if (((String)localObject1).length() != 0) {}
    }
    else
    {
      localObject2 = "/";
    }
    return new BasicRequestLine(str, (String)localObject2, localProtocolVersion);
  }
  
  public URI getURI()
  {
    return this.uri;
  }
  
  public void releaseConnection()
  {
    reset();
  }
  
  public void setConfig(RequestConfig paramRequestConfig)
  {
    this.config = paramRequestConfig;
  }
  
  public void setProtocolVersion(ProtocolVersion paramProtocolVersion)
  {
    this.version = paramProtocolVersion;
  }
  
  public void setURI(URI paramURI)
  {
    this.uri = paramURI;
  }
  
  public void started() {}
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getMethod());
    localStringBuilder.append(" ");
    localStringBuilder.append(getURI());
    localStringBuilder.append(" ");
    localStringBuilder.append(getProtocolVersion());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/methods/HttpRequestBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */