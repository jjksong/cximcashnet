package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.util.InetAddressUtils;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@NotThreadSafe
public class URIBuilder
{
  private String encodedAuthority;
  private String encodedFragment;
  private String encodedPath;
  private String encodedQuery;
  private String encodedSchemeSpecificPart;
  private String encodedUserInfo;
  private String fragment;
  private String host;
  private String path;
  private int port;
  private String query;
  private List<NameValuePair> queryParams;
  private String scheme;
  private String userInfo;
  
  public URIBuilder()
  {
    this.port = -1;
  }
  
  public URIBuilder(String paramString)
    throws URISyntaxException
  {
    digestURI(new URI(paramString));
  }
  
  public URIBuilder(URI paramURI)
  {
    digestURI(paramURI);
  }
  
  private String buildString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    String str = this.scheme;
    if (str != null)
    {
      localStringBuilder.append(str);
      localStringBuilder.append(':');
    }
    str = this.encodedSchemeSpecificPart;
    if (str != null)
    {
      localStringBuilder.append(str);
    }
    else
    {
      if (this.encodedAuthority != null)
      {
        localStringBuilder.append("//");
        localStringBuilder.append(this.encodedAuthority);
      }
      else if (this.host != null)
      {
        localStringBuilder.append("//");
        str = this.encodedUserInfo;
        if (str != null)
        {
          localStringBuilder.append(str);
          localStringBuilder.append("@");
        }
        else
        {
          str = this.userInfo;
          if (str != null)
          {
            localStringBuilder.append(encodeUserInfo(str));
            localStringBuilder.append("@");
          }
        }
        if (InetAddressUtils.isIPv6Address(this.host))
        {
          localStringBuilder.append("[");
          localStringBuilder.append(this.host);
          localStringBuilder.append("]");
        }
        else
        {
          localStringBuilder.append(this.host);
        }
        if (this.port >= 0)
        {
          localStringBuilder.append(":");
          localStringBuilder.append(this.port);
        }
      }
      str = this.encodedPath;
      if (str != null)
      {
        localStringBuilder.append(normalizePath(str));
      }
      else
      {
        str = this.path;
        if (str != null) {
          localStringBuilder.append(encodePath(normalizePath(str)));
        }
      }
      if (this.encodedQuery != null)
      {
        localStringBuilder.append("?");
        localStringBuilder.append(this.encodedQuery);
      }
      else if (this.queryParams != null)
      {
        localStringBuilder.append("?");
        localStringBuilder.append(encodeUrlForm(this.queryParams));
      }
      else if (this.query != null)
      {
        localStringBuilder.append("?");
        localStringBuilder.append(encodeUric(this.query));
      }
    }
    if (this.encodedFragment != null)
    {
      localStringBuilder.append("#");
      localStringBuilder.append(this.encodedFragment);
    }
    else if (this.fragment != null)
    {
      localStringBuilder.append("#");
      localStringBuilder.append(encodeUric(this.fragment));
    }
    return localStringBuilder.toString();
  }
  
  private void digestURI(URI paramURI)
  {
    this.scheme = paramURI.getScheme();
    this.encodedSchemeSpecificPart = paramURI.getRawSchemeSpecificPart();
    this.encodedAuthority = paramURI.getRawAuthority();
    this.host = paramURI.getHost();
    this.port = paramURI.getPort();
    this.encodedUserInfo = paramURI.getRawUserInfo();
    this.userInfo = paramURI.getUserInfo();
    this.encodedPath = paramURI.getRawPath();
    this.path = paramURI.getPath();
    this.encodedQuery = paramURI.getRawQuery();
    this.queryParams = parseQuery(paramURI.getRawQuery(), Consts.UTF_8);
    this.encodedFragment = paramURI.getRawFragment();
    this.fragment = paramURI.getFragment();
  }
  
  private String encodePath(String paramString)
  {
    return URLEncodedUtils.encPath(paramString, Consts.UTF_8);
  }
  
  private String encodeUric(String paramString)
  {
    return URLEncodedUtils.encUric(paramString, Consts.UTF_8);
  }
  
  private String encodeUrlForm(List<NameValuePair> paramList)
  {
    return URLEncodedUtils.format(paramList, Consts.UTF_8);
  }
  
  private String encodeUserInfo(String paramString)
  {
    return URLEncodedUtils.encUserInfo(paramString, Consts.UTF_8);
  }
  
  private static String normalizePath(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    for (int i = 0; (i < paramString.length()) && (paramString.charAt(i) == '/'); i++) {}
    String str = paramString;
    if (i > 1) {
      str = paramString.substring(i - 1);
    }
    return str;
  }
  
  private List<NameValuePair> parseQuery(String paramString, Charset paramCharset)
  {
    if ((paramString != null) && (paramString.length() > 0)) {
      return URLEncodedUtils.parse(paramString, paramCharset);
    }
    return null;
  }
  
  public URIBuilder addParameter(String paramString1, String paramString2)
  {
    if (this.queryParams == null) {
      this.queryParams = new ArrayList();
    }
    this.queryParams.add(new BasicNameValuePair(paramString1, paramString2));
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.query = null;
    return this;
  }
  
  public URIBuilder addParameters(List<NameValuePair> paramList)
  {
    if (this.queryParams == null) {
      this.queryParams = new ArrayList();
    }
    this.queryParams.addAll(paramList);
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.query = null;
    return this;
  }
  
  public URI build()
    throws URISyntaxException
  {
    return new URI(buildString());
  }
  
  public URIBuilder clearParameters()
  {
    this.queryParams = null;
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    return this;
  }
  
  public String getFragment()
  {
    return this.fragment;
  }
  
  public String getHost()
  {
    return this.host;
  }
  
  public String getPath()
  {
    return this.path;
  }
  
  public int getPort()
  {
    return this.port;
  }
  
  public List<NameValuePair> getQueryParams()
  {
    List localList = this.queryParams;
    if (localList != null) {
      return new ArrayList(localList);
    }
    return new ArrayList();
  }
  
  public String getScheme()
  {
    return this.scheme;
  }
  
  public String getUserInfo()
  {
    return this.userInfo;
  }
  
  public boolean isAbsolute()
  {
    boolean bool;
    if (this.scheme != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isOpaque()
  {
    boolean bool;
    if (this.path == null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public URIBuilder removeQuery()
  {
    this.queryParams = null;
    this.query = null;
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    return this;
  }
  
  public URIBuilder setCustomQuery(String paramString)
  {
    this.query = paramString;
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.queryParams = null;
    return this;
  }
  
  public URIBuilder setFragment(String paramString)
  {
    this.fragment = paramString;
    this.encodedFragment = null;
    return this;
  }
  
  public URIBuilder setHost(String paramString)
  {
    this.host = paramString;
    this.encodedSchemeSpecificPart = null;
    this.encodedAuthority = null;
    return this;
  }
  
  public URIBuilder setParameter(String paramString1, String paramString2)
  {
    if (this.queryParams == null) {
      this.queryParams = new ArrayList();
    }
    if (!this.queryParams.isEmpty())
    {
      Iterator localIterator = this.queryParams.iterator();
      while (localIterator.hasNext()) {
        if (((NameValuePair)localIterator.next()).getName().equals(paramString1)) {
          localIterator.remove();
        }
      }
    }
    this.queryParams.add(new BasicNameValuePair(paramString1, paramString2));
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.query = null;
    return this;
  }
  
  public URIBuilder setParameters(List<NameValuePair> paramList)
  {
    List localList = this.queryParams;
    if (localList == null) {
      this.queryParams = new ArrayList();
    } else {
      localList.clear();
    }
    this.queryParams.addAll(paramList);
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.query = null;
    return this;
  }
  
  public URIBuilder setParameters(NameValuePair... paramVarArgs)
  {
    Object localObject = this.queryParams;
    if (localObject == null) {
      this.queryParams = new ArrayList();
    } else {
      ((List)localObject).clear();
    }
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      localObject = paramVarArgs[i];
      this.queryParams.add(localObject);
    }
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    this.query = null;
    return this;
  }
  
  public URIBuilder setPath(String paramString)
  {
    this.path = paramString;
    this.encodedSchemeSpecificPart = null;
    this.encodedPath = null;
    return this;
  }
  
  public URIBuilder setPort(int paramInt)
  {
    int i = paramInt;
    if (paramInt < 0) {
      i = -1;
    }
    this.port = i;
    this.encodedSchemeSpecificPart = null;
    this.encodedAuthority = null;
    return this;
  }
  
  @Deprecated
  public URIBuilder setQuery(String paramString)
  {
    this.queryParams = parseQuery(paramString, Consts.UTF_8);
    this.query = null;
    this.encodedQuery = null;
    this.encodedSchemeSpecificPart = null;
    return this;
  }
  
  public URIBuilder setScheme(String paramString)
  {
    this.scheme = paramString;
    return this;
  }
  
  public URIBuilder setUserInfo(String paramString)
  {
    this.userInfo = paramString;
    this.encodedSchemeSpecificPart = null;
    this.encodedAuthority = null;
    this.encodedUserInfo = null;
    return this;
  }
  
  public URIBuilder setUserInfo(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(':');
    localStringBuilder.append(paramString2);
    return setUserInfo(localStringBuilder.toString());
  }
  
  public String toString()
  {
    return buildString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/URIBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */