package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Immutable
public class CloneUtils
{
  public static Object clone(Object paramObject)
    throws CloneNotSupportedException
  {
    return cloneObject(paramObject);
  }
  
  public static <T> T cloneObject(T paramT)
    throws CloneNotSupportedException
  {
    if (paramT == null) {
      return null;
    }
    if ((paramT instanceof Cloneable))
    {
      Object localObject = paramT.getClass();
      try
      {
        localObject = ((Class)localObject).getMethod("clone", (Class[])null);
        try
        {
          paramT = ((Method)localObject).invoke(paramT, (Object[])null);
          return paramT;
        }
        catch (IllegalAccessException paramT)
        {
          throw new IllegalAccessError(paramT.getMessage());
        }
        catch (InvocationTargetException paramT)
        {
          paramT = paramT.getCause();
          if ((paramT instanceof CloneNotSupportedException)) {
            throw ((CloneNotSupportedException)paramT);
          }
          throw new Error("Unexpected exception", paramT);
        }
        throw new CloneNotSupportedException();
      }
      catch (NoSuchMethodException paramT)
      {
        throw new NoSuchMethodError(paramT.getMessage());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/CloneUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */