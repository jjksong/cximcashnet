package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.Closeable;
import java.io.IOException;

public class HttpClientUtils
{
  public static void closeQuietly(HttpResponse paramHttpResponse)
  {
    if (paramHttpResponse != null)
    {
      paramHttpResponse = paramHttpResponse.getEntity();
      if (paramHttpResponse == null) {}
    }
    try
    {
      EntityUtils.consume(paramHttpResponse);
      return;
    }
    catch (IOException paramHttpResponse)
    {
      for (;;) {}
    }
  }
  
  public static void closeQuietly(HttpClient paramHttpClient)
  {
    if ((paramHttpClient != null) && ((paramHttpClient instanceof Closeable))) {}
    try
    {
      ((Closeable)paramHttpClient).close();
      return;
    }
    catch (IOException paramHttpClient)
    {
      for (;;) {}
    }
  }
  
  public static void closeQuietly(CloseableHttpResponse paramCloseableHttpResponse)
  {
    if (paramCloseableHttpResponse != null) {
      try
      {
        EntityUtils.consume(paramCloseableHttpResponse.getEntity());
        return;
      }
      finally
      {
        try
        {
          paramCloseableHttpResponse.close();
        }
        catch (IOException paramCloseableHttpResponse)
        {
          for (;;) {}
        }
        localObject = finally;
        paramCloseableHttpResponse.close();
        throw ((Throwable)localObject);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/HttpClientUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */