package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import java.lang.ref.SoftReference;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

@Immutable
public final class DateUtils
{
  private static final String[] DEFAULT_PATTERNS = { "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy" };
  private static final Date DEFAULT_TWO_DIGIT_YEAR_START;
  public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
  public static final String PATTERN_ASCTIME = "EEE MMM d HH:mm:ss yyyy";
  public static final String PATTERN_RFC1036 = "EEE, dd-MMM-yy HH:mm:ss zzz";
  public static final String PATTERN_RFC1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";
  
  static
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTimeZone(GMT);
    localCalendar.set(2000, 0, 1, 0, 0, 0);
    localCalendar.set(14, 0);
    DEFAULT_TWO_DIGIT_YEAR_START = localCalendar.getTime();
  }
  
  public static void clearThreadLocal() {}
  
  public static String formatDate(Date paramDate)
  {
    return formatDate(paramDate, "EEE, dd MMM yyyy HH:mm:ss zzz");
  }
  
  public static String formatDate(Date paramDate, String paramString)
  {
    Args.notNull(paramDate, "Date");
    Args.notNull(paramString, "Pattern");
    return DateFormatHolder.formatFor(paramString).format(paramDate);
  }
  
  public static Date parseDate(String paramString)
  {
    return parseDate(paramString, null, null);
  }
  
  public static Date parseDate(String paramString, String[] paramArrayOfString)
  {
    return parseDate(paramString, paramArrayOfString, null);
  }
  
  public static Date parseDate(String paramString, String[] paramArrayOfString, Date paramDate)
  {
    Args.notNull(paramString, "Date value");
    if (paramArrayOfString == null) {
      paramArrayOfString = DEFAULT_PATTERNS;
    }
    if (paramDate == null) {
      paramDate = DEFAULT_TWO_DIGIT_YEAR_START;
    }
    String str = paramString;
    if (paramString.length() > 1)
    {
      str = paramString;
      if (paramString.startsWith("'"))
      {
        str = paramString;
        if (paramString.endsWith("'")) {
          str = paramString.substring(1, paramString.length() - 1);
        }
      }
    }
    int j = paramArrayOfString.length;
    for (int i = 0; i < j; i++)
    {
      Object localObject = DateFormatHolder.formatFor(paramArrayOfString[i]);
      ((SimpleDateFormat)localObject).set2DigitYearStart(paramDate);
      paramString = new ParsePosition(0);
      localObject = ((SimpleDateFormat)localObject).parse(str, paramString);
      if (paramString.getIndex() != 0) {
        return (Date)localObject;
      }
    }
    return null;
  }
  
  static final class DateFormatHolder
  {
    private static final ThreadLocal<SoftReference<Map<String, SimpleDateFormat>>> THREADLOCAL_FORMATS = new ThreadLocal()
    {
      protected SoftReference<Map<String, SimpleDateFormat>> initialValue()
      {
        return new SoftReference(new HashMap());
      }
    };
    
    public static void clearThreadLocal()
    {
      THREADLOCAL_FORMATS.remove();
    }
    
    public static SimpleDateFormat formatFor(String paramString)
    {
      Object localObject2 = (Map)((SoftReference)THREADLOCAL_FORMATS.get()).get();
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new HashMap();
        THREADLOCAL_FORMATS.set(new SoftReference(localObject1));
      }
      SimpleDateFormat localSimpleDateFormat = (SimpleDateFormat)((Map)localObject1).get(paramString);
      localObject2 = localSimpleDateFormat;
      if (localSimpleDateFormat == null)
      {
        localObject2 = new SimpleDateFormat(paramString, Locale.US);
        ((SimpleDateFormat)localObject2).setTimeZone(TimeZone.getTimeZone("GMT"));
        ((Map)localObject1).put(paramString, localObject2);
      }
      return (SimpleDateFormat)localObject2;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/DateUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */