package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.annotation.Immutable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Immutable
public class JdkIdn
  implements Idn
{
  private final Method toUnicode;
  
  public JdkIdn()
    throws ClassNotFoundException
  {
    Class localClass = Class.forName("java.net.IDN");
    try
    {
      this.toUnicode = localClass.getMethod("toUnicode", new Class[] { String.class });
      return;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      throw new IllegalStateException(localNoSuchMethodException.getMessage(), localNoSuchMethodException);
    }
    catch (SecurityException localSecurityException)
    {
      throw new IllegalStateException(localSecurityException.getMessage(), localSecurityException);
    }
  }
  
  public String toUnicode(String paramString)
  {
    try
    {
      paramString = (String)this.toUnicode.invoke(null, new Object[] { paramString });
      return paramString;
    }
    catch (InvocationTargetException paramString)
    {
      paramString = paramString.getCause();
      throw new RuntimeException(paramString.getMessage(), paramString);
    }
    catch (IllegalAccessException paramString)
    {
      throw new IllegalStateException(paramString.getMessage(), paramString);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/JdkIdn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */