package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public class Punycode
{
  private static final Idn impl;
  
  static
  {
    Rfc3492Idn localRfc3492Idn;
    try
    {
      JdkIdn localJdkIdn = new cz/msebera/android/httpclient/client/utils/JdkIdn;
      localJdkIdn.<init>();
    }
    catch (Exception localException)
    {
      localRfc3492Idn = new Rfc3492Idn();
    }
    impl = localRfc3492Idn;
  }
  
  public static String toUnicode(String paramString)
  {
    return impl.toUnicode(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/Punycode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */