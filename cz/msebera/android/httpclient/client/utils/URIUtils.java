package cz.msebera.android.httpclient.client.utils;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

@Immutable
public class URIUtils
{
  @Deprecated
  public static URI createURI(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4, String paramString5)
    throws URISyntaxException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramString2 != null)
    {
      if (paramString1 != null)
      {
        localStringBuilder.append(paramString1);
        localStringBuilder.append("://");
      }
      localStringBuilder.append(paramString2);
      if (paramInt > 0)
      {
        localStringBuilder.append(':');
        localStringBuilder.append(paramInt);
      }
    }
    if ((paramString3 == null) || (!paramString3.startsWith("/"))) {
      localStringBuilder.append('/');
    }
    if (paramString3 != null) {
      localStringBuilder.append(paramString3);
    }
    if (paramString4 != null)
    {
      localStringBuilder.append('?');
      localStringBuilder.append(paramString4);
    }
    if (paramString5 != null)
    {
      localStringBuilder.append('#');
      localStringBuilder.append(paramString5);
    }
    return new URI(localStringBuilder.toString());
  }
  
  public static HttpHost extractHost(URI paramURI)
  {
    Object localObject3 = null;
    if (paramURI == null) {
      return null;
    }
    Object localObject2 = localObject3;
    int j;
    int i;
    Object localObject1;
    int k;
    int n;
    int m;
    if (paramURI.isAbsolute())
    {
      j = paramURI.getPort();
      localObject2 = paramURI.getHost();
      i = j;
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        String str = paramURI.getAuthority();
        i = j;
        localObject1 = str;
        if (str != null)
        {
          k = str.indexOf('@');
          localObject2 = str;
          if (k >= 0)
          {
            i = str.length();
            k++;
            if (i > k) {
              localObject2 = str.substring(k);
            } else {
              localObject2 = null;
            }
          }
          i = j;
          localObject1 = localObject2;
          if (localObject2 != null)
          {
            n = ((String)localObject2).indexOf(':');
            i = j;
            localObject1 = localObject2;
            if (n >= 0)
            {
              m = n + 1;
              i = m;
              k = 0;
              while ((i < ((String)localObject2).length()) && (Character.isDigit(((String)localObject2).charAt(i))))
              {
                k++;
                i++;
              }
              i = j;
              if (k <= 0) {}
            }
          }
        }
      }
    }
    try
    {
      i = Integer.parseInt(((String)localObject2).substring(m, k + m));
      localObject1 = ((String)localObject2).substring(0, n);
      paramURI = paramURI.getScheme();
      localObject2 = localObject3;
      if (!TextUtils.isBlank((CharSequence)localObject1)) {
        localObject2 = new HttpHost((String)localObject1, i, paramURI);
      }
      return (HttpHost)localObject2;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        i = j;
      }
    }
  }
  
  private static URI normalizeSyntax(URI paramURI)
  {
    if ((!paramURI.isOpaque()) && (paramURI.getAuthority() != null))
    {
      Args.check(paramURI.isAbsolute(), "Base URI must be absolute");
      Object localObject1;
      if (paramURI.getPath() == null) {
        localObject1 = "";
      } else {
        localObject1 = paramURI.getPath();
      }
      Object localObject4 = ((String)localObject1).split("/");
      Object localObject3 = new Stack();
      int j = localObject4.length;
      for (int i = 0; i < j; i++)
      {
        localObject2 = localObject4[i];
        if ((((String)localObject2).length() != 0) && (!".".equals(localObject2))) {
          if ("..".equals(localObject2))
          {
            if (!((Stack)localObject3).isEmpty()) {
              ((Stack)localObject3).pop();
            }
          }
          else {
            ((Stack)localObject3).push(localObject2);
          }
        }
      }
      Object localObject2 = new StringBuilder();
      localObject3 = ((Stack)localObject3).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = (String)((Iterator)localObject3).next();
        ((StringBuilder)localObject2).append('/');
        ((StringBuilder)localObject2).append((String)localObject4);
      }
      if (((String)localObject1).lastIndexOf('/') == ((String)localObject1).length() - 1) {
        ((StringBuilder)localObject2).append('/');
      }
      try
      {
        localObject4 = paramURI.getScheme().toLowerCase(Locale.ENGLISH);
        localObject3 = paramURI.getAuthority().toLowerCase(Locale.ENGLISH);
        localObject1 = new java/net/URI;
        ((URI)localObject1).<init>((String)localObject4, (String)localObject3, ((StringBuilder)localObject2).toString(), null, null);
        if ((paramURI.getQuery() == null) && (paramURI.getFragment() == null)) {
          return (URI)localObject1;
        }
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>(((URI)localObject1).toASCIIString());
        if (paramURI.getQuery() != null)
        {
          ((StringBuilder)localObject2).append('?');
          ((StringBuilder)localObject2).append(paramURI.getRawQuery());
        }
        if (paramURI.getFragment() != null)
        {
          ((StringBuilder)localObject2).append('#');
          ((StringBuilder)localObject2).append(paramURI.getRawFragment());
        }
        paramURI = URI.create(((StringBuilder)localObject2).toString());
        return paramURI;
      }
      catch (URISyntaxException paramURI)
      {
        throw new IllegalArgumentException(paramURI);
      }
    }
    return paramURI;
  }
  
  public static URI resolve(URI paramURI, HttpHost paramHttpHost, List<URI> paramList)
    throws URISyntaxException
  {
    Args.notNull(paramURI, "Request URI");
    if ((paramList != null) && (!paramList.isEmpty()))
    {
      URIBuilder localURIBuilder = new URIBuilder((URI)paramList.get(paramList.size() - 1));
      String str = localURIBuilder.getFragment();
      for (int i = paramList.size() - 1; (str == null) && (i >= 0); i--) {
        str = ((URI)paramList.get(i)).getFragment();
      }
      localURIBuilder.setFragment(str);
      paramList = localURIBuilder;
    }
    else
    {
      paramList = new URIBuilder(paramURI);
    }
    if (paramList.getFragment() == null) {
      paramList.setFragment(paramURI.getFragment());
    }
    if ((paramHttpHost != null) && (!paramList.isAbsolute()))
    {
      paramList.setScheme(paramHttpHost.getSchemeName());
      paramList.setHost(paramHttpHost.getHostName());
      paramList.setPort(paramHttpHost.getPort());
    }
    return paramList.build();
  }
  
  public static URI resolve(URI paramURI, String paramString)
  {
    return resolve(paramURI, URI.create(paramString));
  }
  
  public static URI resolve(URI paramURI1, URI paramURI2)
  {
    Args.notNull(paramURI1, "Base URI");
    Args.notNull(paramURI2, "Reference URI");
    String str = paramURI2.toString();
    if (str.startsWith("?")) {
      return resolveReferenceStartingWithQueryString(paramURI1, paramURI2);
    }
    int i;
    if (str.length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      paramURI2 = URI.create("#");
    }
    paramURI2 = paramURI1.resolve(paramURI2);
    paramURI1 = paramURI2;
    if (i != 0)
    {
      paramURI1 = paramURI2.toString();
      paramURI1 = URI.create(paramURI1.substring(0, paramURI1.indexOf('#')));
    }
    return normalizeSyntax(paramURI1);
  }
  
  private static URI resolveReferenceStartingWithQueryString(URI paramURI1, URI paramURI2)
  {
    Object localObject = paramURI1.toString();
    paramURI1 = (URI)localObject;
    if (((String)localObject).indexOf('?') > -1) {
      paramURI1 = ((String)localObject).substring(0, ((String)localObject).indexOf('?'));
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramURI1);
    ((StringBuilder)localObject).append(paramURI2.toString());
    return URI.create(((StringBuilder)localObject).toString());
  }
  
  public static URI rewriteURI(URI paramURI)
    throws URISyntaxException
  {
    Args.notNull(paramURI, "URI");
    if (paramURI.isOpaque()) {
      return paramURI;
    }
    paramURI = new URIBuilder(paramURI);
    if (paramURI.getUserInfo() != null) {
      paramURI.setUserInfo(null);
    }
    if (TextUtils.isEmpty(paramURI.getPath())) {
      paramURI.setPath("/");
    }
    if (paramURI.getHost() != null) {
      paramURI.setHost(paramURI.getHost().toLowerCase(Locale.ENGLISH));
    }
    paramURI.setFragment(null);
    return paramURI.build();
  }
  
  public static URI rewriteURI(URI paramURI, HttpHost paramHttpHost)
    throws URISyntaxException
  {
    return rewriteURI(paramURI, paramHttpHost, false);
  }
  
  public static URI rewriteURI(URI paramURI, HttpHost paramHttpHost, boolean paramBoolean)
    throws URISyntaxException
  {
    Args.notNull(paramURI, "URI");
    if (paramURI.isOpaque()) {
      return paramURI;
    }
    paramURI = new URIBuilder(paramURI);
    if (paramHttpHost != null)
    {
      paramURI.setScheme(paramHttpHost.getSchemeName());
      paramURI.setHost(paramHttpHost.getHostName());
      paramURI.setPort(paramHttpHost.getPort());
    }
    else
    {
      paramURI.setScheme(null);
      paramURI.setHost(null);
      paramURI.setPort(-1);
    }
    if (paramBoolean) {
      paramURI.setFragment(null);
    }
    if (TextUtils.isEmpty(paramURI.getPath())) {
      paramURI.setPath("/");
    }
    return paramURI.build();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/utils/URIUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */