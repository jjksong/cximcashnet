package cz.msebera.android.httpclient.client.entity;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.entity.AbstractHttpEntity;
import cz.msebera.android.httpclient.entity.BasicHttpEntity;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.FileEntity;
import cz.msebera.android.httpclient.entity.InputStreamEntity;
import cz.msebera.android.httpclient.entity.SerializableEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

@NotThreadSafe
public class EntityBuilder
{
  private byte[] binary;
  private boolean chunked;
  private String contentEncoding;
  private ContentType contentType;
  private File file;
  private boolean gzipCompress;
  private List<NameValuePair> parameters;
  private Serializable serializable;
  private InputStream stream;
  private String text;
  
  private void clearContent()
  {
    this.text = null;
    this.binary = null;
    this.stream = null;
    this.parameters = null;
    this.serializable = null;
    this.file = null;
  }
  
  public static EntityBuilder create()
  {
    return new EntityBuilder();
  }
  
  private ContentType getContentOrDefault(ContentType paramContentType)
  {
    ContentType localContentType = this.contentType;
    if (localContentType != null) {
      paramContentType = localContentType;
    }
    return paramContentType;
  }
  
  public HttpEntity build()
  {
    Object localObject1 = this.text;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = new StringEntity((String)localObject1, getContentOrDefault(ContentType.DEFAULT_TEXT));
    }
    else
    {
      localObject1 = this.binary;
      if (localObject1 != null)
      {
        localObject1 = new ByteArrayEntity((byte[])localObject1, getContentOrDefault(ContentType.DEFAULT_BINARY));
      }
      else
      {
        localObject1 = this.stream;
        if (localObject1 != null)
        {
          localObject1 = new InputStreamEntity((InputStream)localObject1, 1L, getContentOrDefault(ContentType.DEFAULT_BINARY));
        }
        else
        {
          localObject2 = this.parameters;
          if (localObject2 != null)
          {
            localObject1 = this.contentType;
            if (localObject1 != null) {
              localObject1 = ((ContentType)localObject1).getCharset();
            } else {
              localObject1 = null;
            }
            localObject1 = new UrlEncodedFormEntity((Iterable)localObject2, (Charset)localObject1);
          }
          else
          {
            localObject1 = this.serializable;
            if (localObject1 != null)
            {
              localObject1 = new SerializableEntity((Serializable)localObject1);
              ((AbstractHttpEntity)localObject1).setContentType(ContentType.DEFAULT_BINARY.toString());
            }
            else
            {
              localObject1 = this.file;
              if (localObject1 != null) {
                localObject1 = new FileEntity((File)localObject1, getContentOrDefault(ContentType.DEFAULT_BINARY));
              } else {
                localObject1 = new BasicHttpEntity();
              }
            }
          }
        }
      }
    }
    if (((AbstractHttpEntity)localObject1).getContentType() != null)
    {
      localObject2 = this.contentType;
      if (localObject2 != null) {
        ((AbstractHttpEntity)localObject1).setContentType(((ContentType)localObject2).toString());
      }
    }
    ((AbstractHttpEntity)localObject1).setContentEncoding(this.contentEncoding);
    ((AbstractHttpEntity)localObject1).setChunked(this.chunked);
    if (this.gzipCompress) {
      return new GzipCompressingEntity((HttpEntity)localObject1);
    }
    return (HttpEntity)localObject1;
  }
  
  public EntityBuilder chunked()
  {
    this.chunked = true;
    return this;
  }
  
  public byte[] getBinary()
  {
    return this.binary;
  }
  
  public String getContentEncoding()
  {
    return this.contentEncoding;
  }
  
  public ContentType getContentType()
  {
    return this.contentType;
  }
  
  public File getFile()
  {
    return this.file;
  }
  
  public List<NameValuePair> getParameters()
  {
    return this.parameters;
  }
  
  public Serializable getSerializable()
  {
    return this.serializable;
  }
  
  public InputStream getStream()
  {
    return this.stream;
  }
  
  public String getText()
  {
    return this.text;
  }
  
  public EntityBuilder gzipCompress()
  {
    this.gzipCompress = true;
    return this;
  }
  
  public boolean isChunked()
  {
    return this.chunked;
  }
  
  public boolean isGzipCompress()
  {
    return this.gzipCompress;
  }
  
  public EntityBuilder setBinary(byte[] paramArrayOfByte)
  {
    clearContent();
    this.binary = paramArrayOfByte;
    return this;
  }
  
  public EntityBuilder setContentEncoding(String paramString)
  {
    this.contentEncoding = paramString;
    return this;
  }
  
  public EntityBuilder setContentType(ContentType paramContentType)
  {
    this.contentType = paramContentType;
    return this;
  }
  
  public EntityBuilder setFile(File paramFile)
  {
    clearContent();
    this.file = paramFile;
    return this;
  }
  
  public EntityBuilder setParameters(List<NameValuePair> paramList)
  {
    clearContent();
    this.parameters = paramList;
    return this;
  }
  
  public EntityBuilder setParameters(NameValuePair... paramVarArgs)
  {
    return setParameters(Arrays.asList(paramVarArgs));
  }
  
  public EntityBuilder setSerializable(Serializable paramSerializable)
  {
    clearContent();
    this.serializable = paramSerializable;
    return this;
  }
  
  public EntityBuilder setStream(InputStream paramInputStream)
  {
    clearContent();
    this.stream = paramInputStream;
    return this;
  }
  
  public EntityBuilder setText(String paramString)
  {
    clearContent();
    this.text = paramString;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/entity/EntityBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */