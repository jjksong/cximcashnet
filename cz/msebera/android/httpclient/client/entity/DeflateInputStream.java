package cz.msebera.android.httpclient.client.entity;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class DeflateInputStream
  extends InputStream
{
  private InputStream sourceStream;
  
  /* Error */
  public DeflateInputStream(InputStream paramInputStream)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 18	java/io/InputStream:<init>	()V
    //   4: bipush 6
    //   6: newarray <illegal type>
    //   8: astore 5
    //   10: new 20	java/io/PushbackInputStream
    //   13: dup
    //   14: aload_1
    //   15: aload 5
    //   17: arraylength
    //   18: invokespecial 23	java/io/PushbackInputStream:<init>	(Ljava/io/InputStream;I)V
    //   21: astore 4
    //   23: aload 4
    //   25: aload 5
    //   27: invokevirtual 27	java/io/PushbackInputStream:read	([B)I
    //   30: istore_2
    //   31: iload_2
    //   32: iconst_m1
    //   33: if_icmpeq +196 -> 229
    //   36: iconst_1
    //   37: newarray <illegal type>
    //   39: astore 6
    //   41: new 29	java/util/zip/Inflater
    //   44: dup
    //   45: invokespecial 30	java/util/zip/Inflater:<init>	()V
    //   48: astore_1
    //   49: aload_1
    //   50: aload 6
    //   52: invokevirtual 33	java/util/zip/Inflater:inflate	([B)I
    //   55: istore_3
    //   56: iload_3
    //   57: ifne +51 -> 108
    //   60: aload_1
    //   61: invokevirtual 37	java/util/zip/Inflater:finished	()Z
    //   64: ifne +29 -> 93
    //   67: aload_1
    //   68: invokevirtual 40	java/util/zip/Inflater:needsDictionary	()Z
    //   71: ifeq +6 -> 77
    //   74: goto +34 -> 108
    //   77: aload_1
    //   78: invokevirtual 43	java/util/zip/Inflater:needsInput	()Z
    //   81: ifeq -32 -> 49
    //   84: aload_1
    //   85: aload 5
    //   87: invokevirtual 47	java/util/zip/Inflater:setInput	([B)V
    //   90: goto -41 -> 49
    //   93: new 13	java/io/IOException
    //   96: astore 6
    //   98: aload 6
    //   100: ldc 49
    //   102: invokespecial 52	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   105: aload 6
    //   107: athrow
    //   108: iload_3
    //   109: iconst_m1
    //   110: if_icmpeq +45 -> 155
    //   113: aload 4
    //   115: aload 5
    //   117: iconst_0
    //   118: iload_2
    //   119: invokevirtual 56	java/io/PushbackInputStream:unread	([BII)V
    //   122: new 6	cz/msebera/android/httpclient/client/entity/DeflateInputStream$DeflateStream
    //   125: astore 6
    //   127: new 29	java/util/zip/Inflater
    //   130: astore 7
    //   132: aload 7
    //   134: invokespecial 30	java/util/zip/Inflater:<init>	()V
    //   137: aload 6
    //   139: aload 4
    //   141: aload 7
    //   143: invokespecial 59	cz/msebera/android/httpclient/client/entity/DeflateInputStream$DeflateStream:<init>	(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
    //   146: aload_0
    //   147: aload 6
    //   149: putfield 61	cz/msebera/android/httpclient/client/entity/DeflateInputStream:sourceStream	Ljava/io/InputStream;
    //   152: goto +65 -> 217
    //   155: new 13	java/io/IOException
    //   158: astore 6
    //   160: aload 6
    //   162: ldc 49
    //   164: invokespecial 52	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   167: aload 6
    //   169: athrow
    //   170: astore 4
    //   172: goto +50 -> 222
    //   175: astore 6
    //   177: aload 4
    //   179: aload 5
    //   181: iconst_0
    //   182: iload_2
    //   183: invokevirtual 56	java/io/PushbackInputStream:unread	([BII)V
    //   186: new 6	cz/msebera/android/httpclient/client/entity/DeflateInputStream$DeflateStream
    //   189: astore 6
    //   191: new 29	java/util/zip/Inflater
    //   194: astore 5
    //   196: aload 5
    //   198: iconst_1
    //   199: invokespecial 64	java/util/zip/Inflater:<init>	(Z)V
    //   202: aload 6
    //   204: aload 4
    //   206: aload 5
    //   208: invokespecial 59	cz/msebera/android/httpclient/client/entity/DeflateInputStream$DeflateStream:<init>	(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
    //   211: aload_0
    //   212: aload 6
    //   214: putfield 61	cz/msebera/android/httpclient/client/entity/DeflateInputStream:sourceStream	Ljava/io/InputStream;
    //   217: aload_1
    //   218: invokevirtual 67	java/util/zip/Inflater:end	()V
    //   221: return
    //   222: aload_1
    //   223: invokevirtual 67	java/util/zip/Inflater:end	()V
    //   226: aload 4
    //   228: athrow
    //   229: new 13	java/io/IOException
    //   232: dup
    //   233: ldc 49
    //   235: invokespecial 52	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   238: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	239	0	this	DeflateInputStream
    //   0	239	1	paramInputStream	InputStream
    //   30	153	2	i	int
    //   55	56	3	j	int
    //   21	119	4	localPushbackInputStream	java.io.PushbackInputStream
    //   170	57	4	localInputStream	InputStream
    //   8	199	5	localObject1	Object
    //   39	129	6	localObject2	Object
    //   175	1	6	localDataFormatException	java.util.zip.DataFormatException
    //   189	24	6	localDeflateStream	DeflateStream
    //   130	12	7	localInflater	Inflater
    // Exception table:
    //   from	to	target	type
    //   49	56	170	finally
    //   60	74	170	finally
    //   77	90	170	finally
    //   93	108	170	finally
    //   113	152	170	finally
    //   155	170	170	finally
    //   177	217	170	finally
    //   49	56	175	java/util/zip/DataFormatException
    //   60	74	175	java/util/zip/DataFormatException
    //   77	90	175	java/util/zip/DataFormatException
    //   93	108	175	java/util/zip/DataFormatException
    //   113	152	175	java/util/zip/DataFormatException
    //   155	170	175	java/util/zip/DataFormatException
  }
  
  public int available()
    throws IOException
  {
    return this.sourceStream.available();
  }
  
  public void close()
    throws IOException
  {
    this.sourceStream.close();
  }
  
  public void mark(int paramInt)
  {
    this.sourceStream.mark(paramInt);
  }
  
  public boolean markSupported()
  {
    return this.sourceStream.markSupported();
  }
  
  public int read()
    throws IOException
  {
    return this.sourceStream.read();
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return this.sourceStream.read(paramArrayOfByte);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    return this.sourceStream.read(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public void reset()
    throws IOException
  {
    this.sourceStream.reset();
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    return this.sourceStream.skip(paramLong);
  }
  
  static class DeflateStream
    extends InflaterInputStream
  {
    private boolean closed = false;
    
    public DeflateStream(InputStream paramInputStream, Inflater paramInflater)
    {
      super(paramInflater);
    }
    
    public void close()
      throws IOException
    {
      if (this.closed) {
        return;
      }
      this.closed = true;
      this.inf.end();
      super.close();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/entity/DeflateInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */