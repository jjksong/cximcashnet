package cz.msebera.android.httpclient.client.entity;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.utils.URLEncodedUtils;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;

@NotThreadSafe
public class UrlEncodedFormEntity
  extends StringEntity
{
  public UrlEncodedFormEntity(Iterable<? extends NameValuePair> paramIterable)
  {
    this(paramIterable, null);
  }
  
  public UrlEncodedFormEntity(Iterable<? extends NameValuePair> paramIterable, Charset paramCharset)
  {
    super(URLEncodedUtils.format(paramIterable, localCharset), ContentType.create("application/x-www-form-urlencoded", paramCharset));
  }
  
  public UrlEncodedFormEntity(List<? extends NameValuePair> paramList)
    throws UnsupportedEncodingException
  {
    this(paramList, (Charset)null);
  }
  
  public UrlEncodedFormEntity(List<? extends NameValuePair> paramList, String paramString)
    throws UnsupportedEncodingException
  {
    super(URLEncodedUtils.format(paramList, str), ContentType.create("application/x-www-form-urlencoded", paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/entity/UrlEncodedFormEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */