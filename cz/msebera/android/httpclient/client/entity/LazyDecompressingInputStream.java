package cz.msebera.android.httpclient.client.entity;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import java.io.IOException;
import java.io.InputStream;

@NotThreadSafe
class LazyDecompressingInputStream
  extends InputStream
{
  private final DecompressingEntity decompressingEntity;
  private final InputStream wrappedStream;
  private InputStream wrapperStream;
  
  public LazyDecompressingInputStream(InputStream paramInputStream, DecompressingEntity paramDecompressingEntity)
  {
    this.wrappedStream = paramInputStream;
    this.decompressingEntity = paramDecompressingEntity;
  }
  
  private void initWrapper()
    throws IOException
  {
    if (this.wrapperStream == null) {
      this.wrapperStream = this.decompressingEntity.decorate(this.wrappedStream);
    }
  }
  
  public int available()
    throws IOException
  {
    initWrapper();
    return this.wrapperStream.available();
  }
  
  public void close()
    throws IOException
  {
    try
    {
      if (this.wrapperStream != null) {
        this.wrapperStream.close();
      }
      return;
    }
    finally
    {
      this.wrappedStream.close();
    }
  }
  
  public boolean markSupported()
  {
    return false;
  }
  
  public int read()
    throws IOException
  {
    initWrapper();
    return this.wrapperStream.read();
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    initWrapper();
    return this.wrapperStream.read(paramArrayOfByte);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    initWrapper();
    return this.wrapperStream.read(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    initWrapper();
    return this.wrapperStream.skip(paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/entity/LazyDecompressingInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */