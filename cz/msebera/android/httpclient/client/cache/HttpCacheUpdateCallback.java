package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;

public abstract interface HttpCacheUpdateCallback
{
  public abstract HttpCacheEntry update(HttpCacheEntry paramHttpCacheEntry)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheUpdateCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */