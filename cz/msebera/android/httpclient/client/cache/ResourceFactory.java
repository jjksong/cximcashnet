package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;
import java.io.InputStream;

public abstract interface ResourceFactory
{
  public abstract Resource copy(String paramString, Resource paramResource)
    throws IOException;
  
  public abstract Resource generate(String paramString, InputStream paramInputStream, InputLimit paramInputLimit)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/ResourceFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */