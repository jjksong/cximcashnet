package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;

public class HttpCacheEntrySerializationException
  extends IOException
{
  private static final long serialVersionUID = 9219188365878433519L;
  
  public HttpCacheEntrySerializationException(String paramString) {}
  
  public HttpCacheEntrySerializationException(String paramString, Throwable paramThrowable)
  {
    super(paramString);
    initCause(paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheEntrySerializationException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */