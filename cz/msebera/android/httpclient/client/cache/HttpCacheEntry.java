package cz.msebera.android.httpclient.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.ProtocolVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import cz.msebera.android.httpclient.message.HeaderGroup;
import cz.msebera.android.httpclient.util.Args;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Immutable
public class HttpCacheEntry
  implements Serializable
{
  private static final long serialVersionUID = -6300496422359477413L;
  private final Date date;
  private final Date requestDate;
  private final Resource resource;
  private final Date responseDate;
  private final HeaderGroup responseHeaders;
  private final StatusLine statusLine;
  private final Map<String, String> variantMap;
  
  public HttpCacheEntry(Date paramDate1, Date paramDate2, StatusLine paramStatusLine, Header[] paramArrayOfHeader, Resource paramResource)
  {
    this(paramDate1, paramDate2, paramStatusLine, paramArrayOfHeader, paramResource, new HashMap());
  }
  
  public HttpCacheEntry(Date paramDate1, Date paramDate2, StatusLine paramStatusLine, Header[] paramArrayOfHeader, Resource paramResource, Map<String, String> paramMap)
  {
    Args.notNull(paramDate1, "Request date");
    Args.notNull(paramDate2, "Response date");
    Args.notNull(paramStatusLine, "Status line");
    Args.notNull(paramArrayOfHeader, "Response headers");
    this.requestDate = paramDate1;
    this.responseDate = paramDate2;
    this.statusLine = paramStatusLine;
    this.responseHeaders = new HeaderGroup();
    this.responseHeaders.setHeaders(paramArrayOfHeader);
    this.resource = paramResource;
    if (paramMap != null) {
      paramDate1 = new HashMap(paramMap);
    } else {
      paramDate1 = null;
    }
    this.variantMap = paramDate1;
    this.date = parseDate();
  }
  
  private Date parseDate()
  {
    Header localHeader = getFirstHeader("Date");
    if (localHeader == null) {
      return null;
    }
    return DateUtils.parseDate(localHeader.getValue());
  }
  
  public Header[] getAllHeaders()
  {
    return this.responseHeaders.getAllHeaders();
  }
  
  public Date getDate()
  {
    return this.date;
  }
  
  public Header getFirstHeader(String paramString)
  {
    return this.responseHeaders.getFirstHeader(paramString);
  }
  
  public Header[] getHeaders(String paramString)
  {
    return this.responseHeaders.getHeaders(paramString);
  }
  
  public ProtocolVersion getProtocolVersion()
  {
    return this.statusLine.getProtocolVersion();
  }
  
  public String getReasonPhrase()
  {
    return this.statusLine.getReasonPhrase();
  }
  
  public Date getRequestDate()
  {
    return this.requestDate;
  }
  
  public Resource getResource()
  {
    return this.resource;
  }
  
  public Date getResponseDate()
  {
    return this.responseDate;
  }
  
  public int getStatusCode()
  {
    return this.statusLine.getStatusCode();
  }
  
  public StatusLine getStatusLine()
  {
    return this.statusLine;
  }
  
  public Map<String, String> getVariantMap()
  {
    return Collections.unmodifiableMap(this.variantMap);
  }
  
  public boolean hasVariants()
  {
    boolean bool;
    if (getFirstHeader("Vary") != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[request date=");
    localStringBuilder.append(this.requestDate);
    localStringBuilder.append("; response date=");
    localStringBuilder.append(this.responseDate);
    localStringBuilder.append("; statusLine=");
    localStringBuilder.append(this.statusLine);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */