package cz.msebera.android.httpclient.client.cache;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;

@NotThreadSafe
public class InputLimit
{
  private boolean reached;
  private final long value;
  
  public InputLimit(long paramLong)
  {
    this.value = paramLong;
    this.reached = false;
  }
  
  public long getValue()
  {
    return this.value;
  }
  
  public boolean isReached()
  {
    return this.reached;
  }
  
  public void reached()
  {
    this.reached = true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/InputLimit.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */