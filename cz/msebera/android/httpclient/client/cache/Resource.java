package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public abstract interface Resource
  extends Serializable
{
  public abstract void dispose();
  
  public abstract InputStream getInputStream()
    throws IOException;
  
  public abstract long length();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/Resource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */