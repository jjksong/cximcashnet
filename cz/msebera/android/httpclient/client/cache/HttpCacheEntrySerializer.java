package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract interface HttpCacheEntrySerializer
{
  public abstract HttpCacheEntry readFrom(InputStream paramInputStream)
    throws IOException;
  
  public abstract void writeTo(HttpCacheEntry paramHttpCacheEntry, OutputStream paramOutputStream)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheEntrySerializer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */