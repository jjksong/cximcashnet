package cz.msebera.android.httpclient.client.cache;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;

public abstract interface HttpCacheInvalidator
{
  public abstract void flushInvalidatedCacheEntries(HttpHost paramHttpHost, HttpRequest paramHttpRequest);
  
  public abstract void flushInvalidatedCacheEntries(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpResponse paramHttpResponse);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheInvalidator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */