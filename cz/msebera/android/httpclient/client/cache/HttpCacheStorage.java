package cz.msebera.android.httpclient.client.cache;

import java.io.IOException;

public abstract interface HttpCacheStorage
{
  public abstract HttpCacheEntry getEntry(String paramString)
    throws IOException;
  
  public abstract void putEntry(String paramString, HttpCacheEntry paramHttpCacheEntry)
    throws IOException;
  
  public abstract void removeEntry(String paramString)
    throws IOException;
  
  public abstract void updateEntry(String paramString, HttpCacheUpdateCallback paramHttpCacheUpdateCallback)
    throws IOException, HttpCacheUpdateException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheStorage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */