package cz.msebera.android.httpclient.client.cache;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;

@NotThreadSafe
public class HttpCacheContext
  extends HttpClientContext
{
  public static final String CACHE_RESPONSE_STATUS = "http.cache.response.status";
  
  public HttpCacheContext() {}
  
  public HttpCacheContext(HttpContext paramHttpContext)
  {
    super(paramHttpContext);
  }
  
  public static HttpCacheContext adapt(HttpContext paramHttpContext)
  {
    if ((paramHttpContext instanceof HttpCacheContext)) {
      return (HttpCacheContext)paramHttpContext;
    }
    return new HttpCacheContext(paramHttpContext);
  }
  
  public static HttpCacheContext create()
  {
    return new HttpCacheContext(new BasicHttpContext());
  }
  
  public CacheResponseStatus getCacheResponseStatus()
  {
    return (CacheResponseStatus)getAttribute("http.cache.response.status", CacheResponseStatus.class);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */