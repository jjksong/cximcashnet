package cz.msebera.android.httpclient.client.cache;

public class HttpCacheUpdateException
  extends Exception
{
  private static final long serialVersionUID = 823573584868632876L;
  
  public HttpCacheUpdateException(String paramString)
  {
    super(paramString);
  }
  
  public HttpCacheUpdateException(String paramString, Throwable paramThrowable)
  {
    super(paramString);
    initCause(paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/cache/HttpCacheUpdateException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */