package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

@Immutable
public class RequestDefaultHeaders
  implements HttpRequestInterceptor
{
  private final Collection<? extends Header> defaultHeaders;
  
  public RequestDefaultHeaders()
  {
    this(null);
  }
  
  public RequestDefaultHeaders(Collection<? extends Header> paramCollection)
  {
    this.defaultHeaders = paramCollection;
  }
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    if (paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("CONNECT")) {
      return;
    }
    Collection localCollection = (Collection)paramHttpRequest.getParams().getParameter("http.default-headers");
    paramHttpContext = localCollection;
    if (localCollection == null) {
      paramHttpContext = this.defaultHeaders;
    }
    if (paramHttpContext != null)
    {
      paramHttpContext = paramHttpContext.iterator();
      while (paramHttpContext.hasNext()) {
        paramHttpRequest.addHeader((Header)paramHttpContext.next());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/RequestDefaultHeaders.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */