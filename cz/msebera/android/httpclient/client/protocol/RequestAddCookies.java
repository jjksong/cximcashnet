package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.conn.routing.RouteInfo;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieSpec;
import cz.msebera.android.httpclient.cookie.CookieSpecProvider;
import cz.msebera.android.httpclient.cookie.SetCookie2;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.TextUtils;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Immutable
public class RequestAddCookies
  implements HttpRequestInterceptor
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    if (paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("CONNECT")) {
      return;
    }
    Object localObject6 = HttpClientContext.adapt(paramHttpContext);
    Object localObject5 = ((HttpClientContext)localObject6).getCookieStore();
    if (localObject5 == null)
    {
      this.log.debug("Cookie store not specified in HTTP context");
      return;
    }
    Object localObject7 = ((HttpClientContext)localObject6).getCookieSpecRegistry();
    if (localObject7 == null)
    {
      this.log.debug("CookieSpec registry not specified in HTTP context");
      return;
    }
    Object localObject9 = ((HttpClientContext)localObject6).getTargetHost();
    if (localObject9 == null)
    {
      this.log.debug("Target host not set in the context");
      return;
    }
    Object localObject8 = ((HttpClientContext)localObject6).getHttpRoute();
    if (localObject8 == null)
    {
      this.log.debug("Connection route not set in the context");
      return;
    }
    Object localObject1 = ((HttpClientContext)localObject6).getRequestConfig().getCookieSpec();
    Object localObject4 = localObject1;
    if (localObject1 == null) {
      localObject4 = "best-match";
    }
    if (this.log.isDebugEnabled())
    {
      localObject3 = this.log;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("CookieSpec selected: ");
      ((StringBuilder)localObject1).append((String)localObject4);
      ((HttpClientAndroidLog)localObject3).debug(((StringBuilder)localObject1).toString());
    }
    boolean bool = paramHttpRequest instanceof HttpUriRequest;
    Object localObject3 = null;
    if (bool) {
      localObject1 = ((HttpUriRequest)paramHttpRequest).getURI();
    } else {
      try
      {
        localObject1 = new java/net/URI;
        ((URI)localObject1).<init>(paramHttpRequest.getRequestLine().getUri());
      }
      catch (URISyntaxException localURISyntaxException)
      {
        localObject2 = null;
      }
    }
    if (localObject2 != null) {
      localObject3 = ((URI)localObject2).getPath();
    }
    Object localObject2 = ((HttpHost)localObject9).getHostName();
    int j = ((HttpHost)localObject9).getPort();
    int i = j;
    if (j < 0) {
      i = ((RouteInfo)localObject8).getTargetHost().getPort();
    }
    j = 0;
    if (i < 0) {
      i = 0;
    }
    if (TextUtils.isEmpty((CharSequence)localObject3)) {
      localObject3 = "/";
    }
    localObject2 = new CookieOrigin((String)localObject2, i, (String)localObject3, ((RouteInfo)localObject8).isSecure());
    localObject3 = (CookieSpecProvider)((Lookup)localObject7).lookup((String)localObject4);
    if (localObject3 != null)
    {
      localObject3 = ((CookieSpecProvider)localObject3).create((HttpContext)localObject6);
      localObject6 = new ArrayList(((CookieStore)localObject5).getCookies());
      localObject4 = new ArrayList();
      localObject5 = new Date();
      localObject6 = ((List)localObject6).iterator();
      while (((Iterator)localObject6).hasNext())
      {
        localObject7 = (Cookie)((Iterator)localObject6).next();
        if (!((Cookie)localObject7).isExpired((Date)localObject5))
        {
          if (((CookieSpec)localObject3).match((Cookie)localObject7, (CookieOrigin)localObject2))
          {
            if (this.log.isDebugEnabled())
            {
              localObject9 = this.log;
              localObject8 = new StringBuilder();
              ((StringBuilder)localObject8).append("Cookie ");
              ((StringBuilder)localObject8).append(localObject7);
              ((StringBuilder)localObject8).append(" match ");
              ((StringBuilder)localObject8).append(localObject2);
              ((HttpClientAndroidLog)localObject9).debug(((StringBuilder)localObject8).toString());
            }
            ((List)localObject4).add(localObject7);
          }
        }
        else if (this.log.isDebugEnabled())
        {
          localObject9 = this.log;
          localObject8 = new StringBuilder();
          ((StringBuilder)localObject8).append("Cookie ");
          ((StringBuilder)localObject8).append(localObject7);
          ((StringBuilder)localObject8).append(" expired");
          ((HttpClientAndroidLog)localObject9).debug(((StringBuilder)localObject8).toString());
        }
      }
      if (!((List)localObject4).isEmpty())
      {
        localObject5 = ((CookieSpec)localObject3).formatCookies((List)localObject4).iterator();
        while (((Iterator)localObject5).hasNext()) {
          paramHttpRequest.addHeader((Header)((Iterator)localObject5).next());
        }
      }
      int k = ((CookieSpec)localObject3).getVersion();
      if (k > 0)
      {
        localObject4 = ((List)localObject4).iterator();
        for (i = j; ((Iterator)localObject4).hasNext(); i = 1)
        {
          label696:
          localObject5 = (Cookie)((Iterator)localObject4).next();
          if ((k == ((Cookie)localObject5).getVersion()) && ((localObject5 instanceof SetCookie2))) {
            break label696;
          }
        }
        if (i != 0)
        {
          localObject4 = ((CookieSpec)localObject3).getVersionHeader();
          if (localObject4 != null) {
            paramHttpRequest.addHeader((Header)localObject4);
          }
        }
      }
      paramHttpContext.setAttribute("http.cookie-spec", localObject3);
      paramHttpContext.setAttribute("http.cookie-origin", localObject2);
      return;
    }
    paramHttpRequest = new StringBuilder();
    paramHttpRequest.append("Unsupported cookie policy: ");
    paramHttpRequest.append((String)localObject4);
    throw new HttpException(paramHttpRequest.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/RequestAddCookies.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */