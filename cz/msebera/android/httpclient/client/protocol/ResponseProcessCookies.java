package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderIterator;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.cookie.CookieOrigin;
import cz.msebera.android.httpclient.cookie.CookieSpec;
import cz.msebera.android.httpclient.cookie.MalformedCookieException;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Immutable
public class ResponseProcessCookies
  implements HttpResponseInterceptor
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private static String formatCooke(Cookie paramCookie)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramCookie.getName());
    localStringBuilder.append("=\"");
    String str = paramCookie.getValue();
    if (str != null)
    {
      Object localObject = str;
      if (str.length() > 100)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str.substring(0, 100));
        ((StringBuilder)localObject).append("...");
        localObject = ((StringBuilder)localObject).toString();
      }
      localStringBuilder.append((String)localObject);
    }
    localStringBuilder.append("\"");
    localStringBuilder.append(", version:");
    localStringBuilder.append(Integer.toString(paramCookie.getVersion()));
    localStringBuilder.append(", domain:");
    localStringBuilder.append(paramCookie.getDomain());
    localStringBuilder.append(", path:");
    localStringBuilder.append(paramCookie.getPath());
    localStringBuilder.append(", expiry:");
    localStringBuilder.append(paramCookie.getExpiryDate());
    return localStringBuilder.toString();
  }
  
  private void processCookies(HeaderIterator paramHeaderIterator, CookieSpec paramCookieSpec, CookieOrigin paramCookieOrigin, CookieStore paramCookieStore)
  {
    while (paramHeaderIterator.hasNext())
    {
      Header localHeader = paramHeaderIterator.nextHeader();
      try
      {
        Iterator localIterator = paramCookieSpec.parse(localHeader, paramCookieOrigin).iterator();
        while (localIterator.hasNext())
        {
          localObject1 = (Cookie)localIterator.next();
          try
          {
            paramCookieSpec.validate((Cookie)localObject1, paramCookieOrigin);
            paramCookieStore.addCookie((Cookie)localObject1);
            if (!this.log.isDebugEnabled()) {
              continue;
            }
            localObject2 = this.log;
            StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
            localStringBuilder1.<init>();
            localStringBuilder1.append("Cookie accepted [");
            localStringBuilder1.append(formatCooke((Cookie)localObject1));
            localStringBuilder1.append("]");
            ((HttpClientAndroidLog)localObject2).debug(localStringBuilder1.toString());
          }
          catch (MalformedCookieException localMalformedCookieException2) {}
          if (this.log.isWarnEnabled())
          {
            localObject2 = this.log;
            StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
            localStringBuilder2.<init>();
            localStringBuilder2.append("Cookie rejected [");
            localStringBuilder2.append(formatCooke((Cookie)localObject1));
            localStringBuilder2.append("] ");
            localStringBuilder2.append(localMalformedCookieException2.getMessage());
            ((HttpClientAndroidLog)localObject2).warn(localStringBuilder2.toString());
          }
        }
        if (!this.log.isWarnEnabled()) {
          continue;
        }
      }
      catch (MalformedCookieException localMalformedCookieException1) {}
      Object localObject1 = this.log;
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Invalid cookie header: \"");
      ((StringBuilder)localObject2).append(localHeader);
      ((StringBuilder)localObject2).append("\". ");
      ((StringBuilder)localObject2).append(localMalformedCookieException1.getMessage());
      ((HttpClientAndroidLog)localObject1).warn(((StringBuilder)localObject2).toString());
    }
  }
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    Object localObject = HttpClientContext.adapt(paramHttpContext);
    paramHttpContext = ((HttpClientContext)localObject).getCookieSpec();
    if (paramHttpContext == null)
    {
      this.log.debug("Cookie spec not specified in HTTP context");
      return;
    }
    CookieStore localCookieStore = ((HttpClientContext)localObject).getCookieStore();
    if (localCookieStore == null)
    {
      this.log.debug("Cookie store not specified in HTTP context");
      return;
    }
    localObject = ((HttpClientContext)localObject).getCookieOrigin();
    if (localObject == null)
    {
      this.log.debug("Cookie origin not specified in HTTP context");
      return;
    }
    processCookies(paramHttpResponse.headerIterator("Set-Cookie"), paramHttpContext, (CookieOrigin)localObject, localCookieStore);
    if (paramHttpContext.getVersion() > 0) {
      processCookies(paramHttpResponse.headerIterator("Set-Cookie2"), paramHttpContext, (CookieOrigin)localObject, localCookieStore);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/ResponseProcessCookies.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */