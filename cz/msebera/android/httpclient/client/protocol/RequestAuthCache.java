package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthProtocolState;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.client.AuthCache;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.conn.routing.RouteInfo;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class RequestAuthCache
  implements HttpRequestInterceptor
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private void doPreemptiveAuth(HttpHost paramHttpHost, AuthScheme paramAuthScheme, AuthState paramAuthState, CredentialsProvider paramCredentialsProvider)
  {
    String str = paramAuthScheme.getSchemeName();
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Re-using cached '");
      localStringBuilder.append(str);
      localStringBuilder.append("' auth scheme for ");
      localStringBuilder.append(paramHttpHost);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    paramHttpHost = paramCredentialsProvider.getCredentials(new AuthScope(paramHttpHost, AuthScope.ANY_REALM, str));
    if (paramHttpHost != null)
    {
      if ("BASIC".equalsIgnoreCase(paramAuthScheme.getSchemeName())) {
        paramAuthState.setState(AuthProtocolState.CHALLENGED);
      } else {
        paramAuthState.setState(AuthProtocolState.SUCCESS);
      }
      paramAuthState.update(paramAuthScheme, paramHttpHost);
    }
    else
    {
      this.log.debug("No credentials for preemptive authentication");
    }
  }
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    HttpClientContext localHttpClientContext = HttpClientContext.adapt(paramHttpContext);
    Object localObject = localHttpClientContext.getAuthCache();
    if (localObject == null)
    {
      this.log.debug("Auth cache not set in the context");
      return;
    }
    CredentialsProvider localCredentialsProvider = localHttpClientContext.getCredentialsProvider();
    if (localCredentialsProvider == null)
    {
      this.log.debug("Credentials provider not set in the context");
      return;
    }
    RouteInfo localRouteInfo = localHttpClientContext.getHttpRoute();
    if (localRouteInfo == null)
    {
      this.log.debug("Route info not set in the context");
      return;
    }
    paramHttpContext = localHttpClientContext.getTargetHost();
    if (paramHttpContext == null)
    {
      this.log.debug("Target host not set in the context");
      return;
    }
    paramHttpRequest = paramHttpContext;
    if (paramHttpContext.getPort() < 0) {
      paramHttpRequest = new HttpHost(paramHttpContext.getHostName(), localRouteInfo.getTargetHost().getPort(), paramHttpContext.getSchemeName());
    }
    paramHttpContext = localHttpClientContext.getTargetAuthState();
    if ((paramHttpContext != null) && (paramHttpContext.getState() == AuthProtocolState.UNCHALLENGED))
    {
      AuthScheme localAuthScheme = ((AuthCache)localObject).get(paramHttpRequest);
      if (localAuthScheme != null) {
        doPreemptiveAuth(paramHttpRequest, localAuthScheme, paramHttpContext, localCredentialsProvider);
      }
    }
    paramHttpRequest = localRouteInfo.getProxyHost();
    paramHttpContext = localHttpClientContext.getProxyAuthState();
    if ((paramHttpRequest != null) && (paramHttpContext != null) && (paramHttpContext.getState() == AuthProtocolState.UNCHALLENGED))
    {
      localObject = ((AuthCache)localObject).get(paramHttpRequest);
      if (localObject != null) {
        doPreemptiveAuth(paramHttpRequest, (AuthScheme)localObject, paramHttpContext, localCredentialsProvider);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/RequestAuthCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */