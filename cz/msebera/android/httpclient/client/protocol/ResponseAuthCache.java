package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.client.AuthCache;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.client.BasicAuthCache;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Deprecated
@Immutable
public class ResponseAuthCache
  implements HttpResponseInterceptor
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private void cache(AuthCache paramAuthCache, HttpHost paramHttpHost, AuthScheme paramAuthScheme)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Caching '");
      localStringBuilder.append(paramAuthScheme.getSchemeName());
      localStringBuilder.append("' auth scheme for ");
      localStringBuilder.append(paramHttpHost);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    paramAuthCache.put(paramHttpHost, paramAuthScheme);
  }
  
  private boolean isCachable(AuthState paramAuthState)
  {
    paramAuthState = paramAuthState.getAuthScheme();
    boolean bool = false;
    if ((paramAuthState != null) && (paramAuthState.isComplete()))
    {
      paramAuthState = paramAuthState.getSchemeName();
      if ((paramAuthState.equalsIgnoreCase("Basic")) || (paramAuthState.equalsIgnoreCase("Digest"))) {
        bool = true;
      }
      return bool;
    }
    return false;
  }
  
  private void uncache(AuthCache paramAuthCache, HttpHost paramHttpHost, AuthScheme paramAuthScheme)
  {
    if (this.log.isDebugEnabled())
    {
      HttpClientAndroidLog localHttpClientAndroidLog = this.log;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Removing from cache '");
      localStringBuilder.append(paramAuthScheme.getSchemeName());
      localStringBuilder.append("' auth scheme for ");
      localStringBuilder.append(paramHttpHost);
      localHttpClientAndroidLog.debug(localStringBuilder.toString());
    }
    paramAuthCache.remove(paramHttpHost);
  }
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    Object localObject1 = (AuthCache)paramHttpContext.getAttribute("http.auth.auth-cache");
    HttpHost localHttpHost = (HttpHost)paramHttpContext.getAttribute("http.target_host");
    Object localObject3 = (AuthState)paramHttpContext.getAttribute("http.auth.target-scope");
    paramHttpResponse = (HttpResponse)localObject1;
    if (localHttpHost != null)
    {
      paramHttpResponse = (HttpResponse)localObject1;
      if (localObject3 != null)
      {
        if (this.log.isDebugEnabled())
        {
          localObject2 = this.log;
          paramHttpResponse = new StringBuilder();
          paramHttpResponse.append("Target auth state: ");
          paramHttpResponse.append(((AuthState)localObject3).getState());
          ((HttpClientAndroidLog)localObject2).debug(paramHttpResponse.toString());
        }
        paramHttpResponse = (HttpResponse)localObject1;
        if (isCachable((AuthState)localObject3))
        {
          paramHttpResponse = (SchemeRegistry)paramHttpContext.getAttribute("http.scheme-registry");
          localObject2 = localHttpHost;
          if (localHttpHost.getPort() < 0)
          {
            paramHttpResponse = paramHttpResponse.getScheme(localHttpHost);
            localObject2 = new HttpHost(localHttpHost.getHostName(), paramHttpResponse.resolvePort(localHttpHost.getPort()), localHttpHost.getSchemeName());
          }
          paramHttpResponse = (HttpResponse)localObject1;
          if (localObject1 == null)
          {
            paramHttpResponse = new BasicAuthCache();
            paramHttpContext.setAttribute("http.auth.auth-cache", paramHttpResponse);
          }
          switch (localObject3.getState())
          {
          default: 
            break;
          case ???: 
            uncache(paramHttpResponse, (HttpHost)localObject2, ((AuthState)localObject3).getAuthScheme());
            break;
          case ???: 
            cache(paramHttpResponse, (HttpHost)localObject2, ((AuthState)localObject3).getAuthScheme());
          }
        }
      }
    }
    localHttpHost = (HttpHost)paramHttpContext.getAttribute("http.proxy_host");
    Object localObject2 = (AuthState)paramHttpContext.getAttribute("http.auth.proxy-scope");
    if ((localHttpHost != null) && (localObject2 != null))
    {
      if (this.log.isDebugEnabled())
      {
        localObject3 = this.log;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("Proxy auth state: ");
        ((StringBuilder)localObject1).append(((AuthState)localObject2).getState());
        ((HttpClientAndroidLog)localObject3).debug(((StringBuilder)localObject1).toString());
      }
      if (isCachable((AuthState)localObject2))
      {
        localObject1 = paramHttpResponse;
        if (paramHttpResponse == null)
        {
          localObject1 = new BasicAuthCache();
          paramHttpContext.setAttribute("http.auth.auth-cache", localObject1);
        }
        switch (localObject2.getState())
        {
        default: 
          break;
        case ???: 
          uncache((AuthCache)localObject1, localHttpHost, ((AuthState)localObject2).getAuthScheme());
          break;
        case ???: 
          cache((AuthCache)localObject1, localHttpHost, ((AuthState)localObject2).getAuthScheme());
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/ResponseAuthCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */