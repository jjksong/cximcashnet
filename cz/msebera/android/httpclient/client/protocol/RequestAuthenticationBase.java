package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.auth.AuthOption;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.ContextAwareAuthScheme;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Asserts;
import java.util.Queue;

@Deprecated
abstract class RequestAuthenticationBase
  implements HttpRequestInterceptor
{
  final HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private Header authenticate(AuthScheme paramAuthScheme, Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    Asserts.notNull(paramAuthScheme, "Auth scheme");
    if ((paramAuthScheme instanceof ContextAwareAuthScheme)) {
      return ((ContextAwareAuthScheme)paramAuthScheme).authenticate(paramCredentials, paramHttpRequest, paramHttpContext);
    }
    return paramAuthScheme.authenticate(paramCredentials, paramHttpRequest);
  }
  
  private void ensureAuthScheme(AuthScheme paramAuthScheme)
  {
    Asserts.notNull(paramAuthScheme, "Auth scheme");
  }
  
  void process(AuthState paramAuthState, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
  {
    AuthScheme localAuthScheme = paramAuthState.getAuthScheme();
    Object localObject = paramAuthState.getCredentials();
    switch (paramAuthState.getState())
    {
    default: 
      break;
    case ???: 
      Queue localQueue = paramAuthState.getAuthOptions();
      if (localQueue != null)
      {
        while (!localQueue.isEmpty())
        {
          localObject = (AuthOption)localQueue.remove();
          localAuthScheme = ((AuthOption)localObject).getAuthScheme();
          localObject = ((AuthOption)localObject).getCredentials();
          paramAuthState.update(localAuthScheme, (Credentials)localObject);
          HttpClientAndroidLog localHttpClientAndroidLog;
          StringBuilder localStringBuilder;
          if (this.log.isDebugEnabled())
          {
            localHttpClientAndroidLog = this.log;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("Generating response to an authentication challenge using ");
            localStringBuilder.append(localAuthScheme.getSchemeName());
            localStringBuilder.append(" scheme");
            localHttpClientAndroidLog.debug(localStringBuilder.toString());
          }
          try
          {
            paramHttpRequest.addHeader(authenticate(localAuthScheme, (Credentials)localObject, paramHttpRequest, paramHttpContext));
          }
          catch (AuthenticationException localAuthenticationException) {}
          if (this.log.isWarnEnabled())
          {
            localHttpClientAndroidLog = this.log;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append(localAuthScheme);
            localStringBuilder.append(" authentication error: ");
            localStringBuilder.append(localAuthenticationException.getMessage());
            localHttpClientAndroidLog.warn(localStringBuilder.toString());
          }
        }
        return;
      }
      ensureAuthScheme(localAuthScheme);
      break;
    case ???: 
      ensureAuthScheme(localAuthScheme);
      if (localAuthScheme.isConnectionBased()) {
        return;
      }
      break;
    case ???: 
      return;
    }
    if (localAuthScheme != null) {
      try
      {
        paramHttpRequest.addHeader(authenticate(localAuthScheme, localAuthenticationException, paramHttpRequest, paramHttpContext));
      }
      catch (AuthenticationException paramAuthState)
      {
        if (this.log.isErrorEnabled())
        {
          paramHttpRequest = this.log;
          paramHttpContext = new StringBuilder();
          paramHttpContext.append(localAuthScheme);
          paramHttpContext.append(" authentication error: ");
          paramHttpContext.append(paramAuthState.getMessage());
          paramHttpRequest.error(paramHttpContext.toString());
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/RequestAuthenticationBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */