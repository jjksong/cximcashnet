package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.entity.DeflateDecompressingEntity;
import cz.msebera.android.httpclient.client.entity.GzipDecompressingEntity;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.IOException;
import java.util.Locale;

@Immutable
public class ResponseContentEncoding
  implements HttpResponseInterceptor
{
  public static final String UNCOMPRESSED = "http.client.response.uncompressed";
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    paramHttpContext = paramHttpResponse.getEntity();
    if ((paramHttpContext != null) && (paramHttpContext.getContentLength() != 0L))
    {
      paramHttpContext = paramHttpContext.getContentEncoding();
      if (paramHttpContext != null)
      {
        paramHttpContext = paramHttpContext.getElements();
        int j = paramHttpContext.length;
        int i = 1;
        if (j > 0)
        {
          paramHttpContext = paramHttpContext[0];
          String str = paramHttpContext.getName().toLowerCase(Locale.ENGLISH);
          if ((!"gzip".equals(str)) && (!"x-gzip".equals(str)))
          {
            if ("deflate".equals(str))
            {
              paramHttpResponse.setEntity(new DeflateDecompressingEntity(paramHttpResponse.getEntity()));
            }
            else
            {
              if ("identity".equals(str)) {
                return;
              }
              paramHttpResponse = new StringBuilder();
              paramHttpResponse.append("Unsupported Content-Coding: ");
              paramHttpResponse.append(paramHttpContext.getName());
              throw new HttpException(paramHttpResponse.toString());
            }
          }
          else {
            paramHttpResponse.setEntity(new GzipDecompressingEntity(paramHttpResponse.getEntity()));
          }
        }
        else
        {
          i = 0;
        }
        if (i != 0)
        {
          paramHttpResponse.removeHeaders("Content-Length");
          paramHttpResponse.removeHeaders("Content-Encoding");
          paramHttpResponse.removeHeaders("Content-MD5");
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/ResponseContentEncoding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */