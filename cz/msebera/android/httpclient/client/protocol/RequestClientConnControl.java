package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.conn.routing.RouteInfo;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Immutable
public class RequestClientConnControl
  implements HttpRequestInterceptor
{
  private static final String PROXY_CONN_DIRECTIVE = "Proxy-Connection";
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    if (paramHttpRequest.getRequestLine().getMethod().equalsIgnoreCase("CONNECT"))
    {
      paramHttpRequest.setHeader("Proxy-Connection", "Keep-Alive");
      return;
    }
    paramHttpContext = HttpClientContext.adapt(paramHttpContext).getHttpRoute();
    if (paramHttpContext == null)
    {
      this.log.debug("Connection route not set in the context");
      return;
    }
    if (((paramHttpContext.getHopCount() == 1) || (paramHttpContext.isTunnelled())) && (!paramHttpRequest.containsHeader("Connection"))) {
      paramHttpRequest.addHeader("Connection", "Keep-Alive");
    }
    if ((paramHttpContext.getHopCount() == 2) && (!paramHttpContext.isTunnelled()) && (!paramHttpRequest.containsHeader("Proxy-Connection"))) {
      paramHttpRequest.addHeader("Proxy-Connection", "Keep-Alive");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/protocol/RequestClientConnControl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */