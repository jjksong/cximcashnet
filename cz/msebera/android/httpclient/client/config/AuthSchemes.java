package cz.msebera.android.httpclient.client.config;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public final class AuthSchemes
{
  public static final String BASIC = "Basic";
  public static final String DIGEST = "Digest";
  public static final String KERBEROS = "Kerberos";
  public static final String NTLM = "NTLM";
  public static final String SPNEGO = "negotiate";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/config/AuthSchemes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */