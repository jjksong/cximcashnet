package cz.msebera.android.httpclient.client.config;

import cz.msebera.android.httpclient.annotation.Immutable;

@Immutable
public final class CookieSpecs
{
  public static final String BEST_MATCH = "best-match";
  public static final String BROWSER_COMPATIBILITY = "compatibility";
  public static final String IGNORE_COOKIES = "ignoreCookies";
  public static final String NETSCAPE = "netscape";
  public static final String STANDARD = "standard";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/android/httpclient/client/config/CookieSpecs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */