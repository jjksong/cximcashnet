package cz.msebera.httpclient.android;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "cz.msebera.httpclient.android";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cz/msebera/httpclient/android/BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */