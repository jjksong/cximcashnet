package com.lljjcoder.style.cityjd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import java.util.List;

public class ProvinceAdapter
  extends BaseAdapter
{
  Context context;
  List<ProvinceBean> mProList;
  private int provinceIndex = -1;
  
  public ProvinceAdapter(Context paramContext, List<ProvinceBean> paramList)
  {
    this.context = paramContext;
    this.mProList = paramList;
  }
  
  public int getCount()
  {
    return this.mProList.size();
  }
  
  public ProvinceBean getItem(int paramInt)
  {
    return (ProvinceBean)this.mProList.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return Long.parseLong(((ProvinceBean)this.mProList.get(paramInt)).getId());
  }
  
  public int getSelectedPosition()
  {
    return this.provinceIndex;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = 0;
    if (paramView == null)
    {
      paramView = LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.pop_jdcitypicker_item, paramViewGroup, false);
      paramViewGroup = new Holder();
      paramViewGroup.name = ((TextView)paramView.findViewById(R.id.name));
      paramViewGroup.selectImg = ((ImageView)paramView.findViewById(R.id.selectImg));
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (Holder)paramView.getTag();
    }
    ProvinceBean localProvinceBean = getItem(paramInt);
    paramViewGroup.name.setText(localProvinceBean.getName());
    paramInt = this.provinceIndex;
    if ((paramInt != -1) && (((ProvinceBean)this.mProList.get(paramInt)).getId().equals(localProvinceBean.getId()))) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    paramViewGroup.name.setEnabled(paramInt ^ 0x1);
    paramViewGroup = paramViewGroup.selectImg;
    if (paramInt != 0) {
      paramInt = i;
    } else {
      paramInt = 8;
    }
    paramViewGroup.setVisibility(paramInt);
    return paramView;
  }
  
  public void updateSelectedPosition(int paramInt)
  {
    this.provinceIndex = paramInt;
  }
  
  class Holder
  {
    TextView name;
    ImageView selectImg;
    
    Holder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/cityjd/ProvinceAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */