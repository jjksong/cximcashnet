package com.lljjcoder.style.cityjd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import java.util.List;

public class CityAdapter
  extends BaseAdapter
{
  private int cityIndex = -1;
  Context context;
  List<CityBean> mCityList;
  
  public CityAdapter(Context paramContext, List<CityBean> paramList)
  {
    this.context = paramContext;
    this.mCityList = paramList;
  }
  
  public int getCount()
  {
    return this.mCityList.size();
  }
  
  public CityBean getItem(int paramInt)
  {
    return (CityBean)this.mCityList.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return Long.parseLong(((CityBean)this.mCityList.get(paramInt)).getId());
  }
  
  public int getSelectedPosition()
  {
    return this.cityIndex;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = 0;
    if (paramView == null)
    {
      paramView = LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.pop_jdcitypicker_item, paramViewGroup, false);
      paramViewGroup = new Holder();
      paramViewGroup.name = ((TextView)paramView.findViewById(R.id.name));
      paramViewGroup.selectImg = ((ImageView)paramView.findViewById(R.id.selectImg));
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (Holder)paramView.getTag();
    }
    CityBean localCityBean = getItem(paramInt);
    paramViewGroup.name.setText(localCityBean.getName());
    paramInt = this.cityIndex;
    if ((paramInt != -1) && (((CityBean)this.mCityList.get(paramInt)).getId().equals(localCityBean.getId()))) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    paramViewGroup.name.setEnabled(paramInt ^ 0x1);
    paramViewGroup = paramViewGroup.selectImg;
    if (paramInt != 0) {
      paramInt = i;
    } else {
      paramInt = 8;
    }
    paramViewGroup.setVisibility(paramInt);
    return paramView;
  }
  
  public void updateSelectedPosition(int paramInt)
  {
    this.cityIndex = paramInt;
  }
  
  class Holder
  {
    TextView name;
    ImageView selectImg;
    
    Holder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/cityjd/CityAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */