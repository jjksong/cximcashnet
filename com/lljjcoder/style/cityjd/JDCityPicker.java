package com.lljjcoder.style.cityjd;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.citywheel.CityParseHelper;
import com.lljjcoder.style.citylist.Toast.ToastUtils;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import com.lljjcoder.style.citypickerview.R.style;
import com.lljjcoder.utils.utils;
import java.util.ArrayList;
import java.util.List;

public class JDCityPicker
{
  private List<DistrictBean> areaList = null;
  private List<CityBean> cityList = null;
  private String colorAlert = "#ffff4444";
  private String colorSelected = "#ff181c20";
  private Context context;
  private AreaAdapter mAreaAdapter;
  private TextView mAreaTv;
  private OnCityItemClickListener mBaseListener;
  private CityAdapter mCityAdapter;
  private ListView mCityListView;
  private TextView mCityTv;
  private ImageView mCloseImg;
  private Handler mHandler = new Handler(new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default: 
        break;
      case 2: 
        JDCityPicker.access$1802(JDCityPicker.this, (List)paramAnonymousMessage.obj);
        JDCityPicker.this.mAreaAdapter.notifyDataSetChanged();
        if ((JDCityPicker.this.areaList != null) && (!JDCityPicker.this.areaList.isEmpty()))
        {
          JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mAreaAdapter);
          JDCityPicker.access$302(JDCityPicker.this, 2);
        }
        break;
      case 1: 
        JDCityPicker.access$1702(JDCityPicker.this, (List)paramAnonymousMessage.obj);
        JDCityPicker.this.mCityAdapter.notifyDataSetChanged();
        if ((JDCityPicker.this.cityList != null) && (!JDCityPicker.this.cityList.isEmpty()))
        {
          JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mCityAdapter);
          JDCityPicker.access$302(JDCityPicker.this, 1);
        }
        break;
      case 0: 
        JDCityPicker.access$1602(JDCityPicker.this, (List)paramAnonymousMessage.obj);
        JDCityPicker.this.mProvinceAdapter.notifyDataSetChanged();
        JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mProvinceAdapter);
        break;
      case -1: 
        JDCityPicker.access$1602(JDCityPicker.this, (List)paramAnonymousMessage.obj);
        JDCityPicker.this.mProvinceAdapter.notifyDataSetChanged();
        JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mProvinceAdapter);
      }
      paramAnonymousMessage = JDCityPicker.this;
      paramAnonymousMessage.updateTabsStyle(paramAnonymousMessage.tabIndex);
      JDCityPicker.this.updateIndicator();
      return true;
    }
  });
  private TextView mProTv;
  private ProvinceAdapter mProvinceAdapter;
  private View mSelectedLine;
  private CityParseHelper parseHelper;
  private View popview;
  private PopupWindow popwindow;
  private List<ProvinceBean> provinceList = null;
  private int tabIndex = 0;
  
  private void callback(DistrictBean paramDistrictBean)
  {
    Object localObject1 = this.provinceList;
    Object localObject3 = null;
    if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
    {
      localObject1 = this.mProvinceAdapter;
      if ((localObject1 != null) && (((ProvinceAdapter)localObject1).getSelectedPosition() != -1))
      {
        localObject1 = (ProvinceBean)this.provinceList.get(this.mProvinceAdapter.getSelectedPosition());
        break label63;
      }
    }
    localObject1 = null;
    label63:
    Object localObject4 = this.cityList;
    Object localObject2 = localObject3;
    if (localObject4 != null)
    {
      localObject2 = localObject3;
      if (!((List)localObject4).isEmpty())
      {
        localObject4 = this.mCityAdapter;
        localObject2 = localObject3;
        if (localObject4 != null)
        {
          localObject2 = localObject3;
          if (((CityAdapter)localObject4).getSelectedPosition() != -1) {
            localObject2 = (CityBean)this.cityList.get(this.mCityAdapter.getSelectedPosition());
          }
        }
      }
    }
    this.mBaseListener.onSelected((ProvinceBean)localObject1, (CityBean)localObject2, paramDistrictBean);
    hidePop();
  }
  
  private void hidePop()
  {
    if (isShow()) {
      this.popwindow.dismiss();
    }
  }
  
  private void initJDCityPickerPop()
  {
    this.tabIndex = 0;
    if (this.parseHelper == null) {
      this.parseHelper = new CityParseHelper();
    }
    if (this.parseHelper.getProvinceBeanArrayList().isEmpty())
    {
      ToastUtils.showLongToast(this.context, "请在Activity中增加init操作");
      return;
    }
    this.popview = LayoutInflater.from(this.context).inflate(R.layout.pop_jdcitypicker, null);
    this.mCityListView = ((ListView)this.popview.findViewById(R.id.city_listview));
    this.mProTv = ((TextView)this.popview.findViewById(R.id.province_tv));
    this.mCityTv = ((TextView)this.popview.findViewById(R.id.city_tv));
    this.mAreaTv = ((TextView)this.popview.findViewById(R.id.area_tv));
    this.mCloseImg = ((ImageView)this.popview.findViewById(R.id.close_img));
    this.mSelectedLine = this.popview.findViewById(R.id.selected_line);
    this.popwindow = new PopupWindow(this.popview, -1, -2);
    this.popwindow.setAnimationStyle(R.style.AnimBottom);
    this.popwindow.setBackgroundDrawable(new ColorDrawable());
    this.popwindow.setTouchable(true);
    this.popwindow.setOutsideTouchable(false);
    this.popwindow.setFocusable(true);
    this.popwindow.setOnDismissListener(new PopupWindow.OnDismissListener()
    {
      public void onDismiss()
      {
        utils.setBackgroundAlpha(JDCityPicker.this.context, 1.0F);
      }
    });
    this.mCloseImg.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        JDCityPicker.this.hidePop();
        utils.setBackgroundAlpha(JDCityPicker.this.context, 1.0F);
        if (JDCityPicker.this.mBaseListener != null) {
          JDCityPicker.this.mBaseListener.onCancel();
        }
      }
    });
    this.mProTv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        JDCityPicker.access$302(JDCityPicker.this, 0);
        if (JDCityPicker.this.mProvinceAdapter != null)
        {
          JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mProvinceAdapter);
          if (JDCityPicker.this.mProvinceAdapter.getSelectedPosition() != -1) {
            JDCityPicker.this.mCityListView.setSelection(JDCityPicker.this.mProvinceAdapter.getSelectedPosition());
          }
        }
        JDCityPicker.this.updateTabVisible();
        JDCityPicker.this.updateIndicator();
      }
    });
    this.mCityTv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        JDCityPicker.access$302(JDCityPicker.this, 1);
        if (JDCityPicker.this.mCityAdapter != null)
        {
          JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mCityAdapter);
          if (JDCityPicker.this.mCityAdapter.getSelectedPosition() != -1) {
            JDCityPicker.this.mCityListView.setSelection(JDCityPicker.this.mCityAdapter.getSelectedPosition());
          }
        }
        JDCityPicker.this.updateTabVisible();
        JDCityPicker.this.updateIndicator();
      }
    });
    this.mAreaTv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        JDCityPicker.access$302(JDCityPicker.this, 2);
        if (JDCityPicker.this.mAreaAdapter != null)
        {
          JDCityPicker.this.mCityListView.setAdapter(JDCityPicker.this.mAreaAdapter);
          if (JDCityPicker.this.mAreaAdapter.getSelectedPosition() != -1) {
            JDCityPicker.this.mCityListView.setSelection(JDCityPicker.this.mAreaAdapter.getSelectedPosition());
          }
        }
        JDCityPicker.this.updateTabVisible();
        JDCityPicker.this.updateIndicator();
      }
    });
    this.mCityListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        JDCityPicker.this.selectedList(paramAnonymousInt);
      }
    });
    utils.setBackgroundAlpha(this.context, 0.5F);
    updateIndicator();
    updateTabsStyle(-1);
    setProvinceListData();
  }
  
  private boolean isShow()
  {
    return this.popwindow.isShowing();
  }
  
  private void selectedList(int paramInt)
  {
    Object localObject1;
    TextView localTextView;
    Object localObject2;
    switch (this.tabIndex)
    {
    default: 
      break;
    case 2: 
      localObject1 = this.mAreaAdapter.getItem(paramInt);
      if (localObject1 != null) {
        callback((DistrictBean)localObject1);
      }
      break;
    case 1: 
      localObject1 = this.mCityAdapter.getItem(paramInt);
      if (localObject1 != null)
      {
        localTextView = this.mCityTv;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("");
        ((StringBuilder)localObject2).append(((CityBean)localObject1).getName());
        localTextView.setText(((StringBuilder)localObject2).toString());
        this.mAreaTv.setText("请选择");
        this.mCityAdapter.updateSelectedPosition(paramInt);
        this.mCityAdapter.notifyDataSetChanged();
        this.mAreaAdapter = new AreaAdapter(this.context, ((CityBean)localObject1).getCityList());
        localObject2 = this.mHandler;
        ((Handler)localObject2).sendMessage(Message.obtain((Handler)localObject2, 2, ((CityBean)localObject1).getCityList()));
      }
      break;
    case 0: 
      localObject1 = this.mProvinceAdapter.getItem(paramInt);
      if (localObject1 != null)
      {
        localTextView = this.mProTv;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("");
        ((StringBuilder)localObject2).append(((ProvinceBean)localObject1).getName());
        localTextView.setText(((StringBuilder)localObject2).toString());
        this.mCityTv.setText("请选择");
        this.mProvinceAdapter.updateSelectedPosition(paramInt);
        this.mProvinceAdapter.notifyDataSetChanged();
        this.mCityAdapter = new CityAdapter(this.context, ((ProvinceBean)localObject1).getCityList());
        localObject2 = this.mHandler;
        ((Handler)localObject2).sendMessage(Message.obtain((Handler)localObject2, 1, ((ProvinceBean)localObject1).getCityList()));
      }
      break;
    }
  }
  
  private void setProvinceListData()
  {
    this.provinceList = this.parseHelper.getProvinceBeanArrayList();
    List localList = this.provinceList;
    if ((localList != null) && (!localList.isEmpty()))
    {
      this.mProvinceAdapter = new ProvinceAdapter(this.context, this.provinceList);
      this.mCityListView.setAdapter(this.mProvinceAdapter);
      return;
    }
    ToastUtils.showLongToast(this.context, "解析本地城市数据失败！");
  }
  
  private AnimatorSet tabSelectedIndicatorAnimation(TextView paramTextView)
  {
    Object localObject1 = this.mSelectedLine;
    localObject1 = ObjectAnimator.ofFloat(localObject1, "X", new float[] { ((View)localObject1).getX(), paramTextView.getX() });
    final Object localObject2 = this.mSelectedLine.getLayoutParams();
    paramTextView = ValueAnimator.ofInt(new int[] { ((ViewGroup.LayoutParams)localObject2).width, paramTextView.getMeasuredWidth() });
    paramTextView.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        localObject2.width = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
        JDCityPicker.this.mSelectedLine.setLayoutParams(localObject2);
      }
    });
    localObject2 = new AnimatorSet();
    ((AnimatorSet)localObject2).setInterpolator(new FastOutSlowInInterpolator());
    ((AnimatorSet)localObject2).playTogether(new Animator[] { localObject1, paramTextView });
    return (AnimatorSet)localObject2;
  }
  
  private void updateIndicator()
  {
    this.popview.post(new Runnable()
    {
      public void run()
      {
        JDCityPicker localJDCityPicker;
        switch (JDCityPicker.this.tabIndex)
        {
        default: 
          break;
        case 2: 
          localJDCityPicker = JDCityPicker.this;
          localJDCityPicker.tabSelectedIndicatorAnimation(localJDCityPicker.mAreaTv).start();
          break;
        case 1: 
          localJDCityPicker = JDCityPicker.this;
          localJDCityPicker.tabSelectedIndicatorAnimation(localJDCityPicker.mCityTv).start();
          break;
        case 0: 
          localJDCityPicker = JDCityPicker.this;
          localJDCityPicker.tabSelectedIndicatorAnimation(localJDCityPicker.mProTv).start();
        }
      }
    });
  }
  
  private void updateTabVisible()
  {
    Object localObject1 = this.mProTv;
    Object localObject2 = this.provinceList;
    int j = 0;
    int i;
    if ((localObject2 != null) && (!((List)localObject2).isEmpty())) {
      i = 0;
    } else {
      i = 8;
    }
    ((TextView)localObject1).setVisibility(i);
    localObject2 = this.mCityTv;
    localObject1 = this.cityList;
    if ((localObject1 != null) && (!((List)localObject1).isEmpty())) {
      i = 0;
    } else {
      i = 8;
    }
    ((TextView)localObject2).setVisibility(i);
    localObject2 = this.mAreaTv;
    localObject1 = this.areaList;
    if (localObject1 != null)
    {
      i = j;
      if (!((List)localObject1).isEmpty()) {}
    }
    else
    {
      i = 8;
    }
    ((TextView)localObject2).setVisibility(i);
  }
  
  private void updateTabsStyle(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
      this.mProTv.setTextColor(Color.parseColor(this.colorSelected));
      this.mCityTv.setTextColor(Color.parseColor(this.colorSelected));
      this.mAreaTv.setTextColor(Color.parseColor(this.colorAlert));
      this.mProTv.setVisibility(0);
      this.mCityTv.setVisibility(0);
      this.mAreaTv.setVisibility(0);
      break;
    case 1: 
      this.mProTv.setTextColor(Color.parseColor(this.colorSelected));
      this.mCityTv.setTextColor(Color.parseColor(this.colorAlert));
      this.mProTv.setVisibility(0);
      this.mCityTv.setVisibility(0);
      this.mAreaTv.setVisibility(8);
      break;
    case 0: 
      this.mProTv.setTextColor(Color.parseColor(this.colorAlert));
      this.mProTv.setVisibility(0);
      this.mCityTv.setVisibility(8);
      this.mAreaTv.setVisibility(8);
      break;
    case -1: 
      this.mProTv.setTextColor(Color.parseColor(this.colorAlert));
      this.mProTv.setVisibility(0);
      this.mCityTv.setVisibility(8);
      this.mAreaTv.setVisibility(8);
    }
  }
  
  public void init(Context paramContext)
  {
    this.context = paramContext;
    this.parseHelper = new CityParseHelper();
    if (this.parseHelper.getProvinceBeanArrayList().isEmpty()) {
      this.parseHelper.initData(paramContext);
    }
  }
  
  public void setOnCityItemClickListener(OnCityItemClickListener paramOnCityItemClickListener)
  {
    this.mBaseListener = paramOnCityItemClickListener;
  }
  
  public void showCityPicker()
  {
    initJDCityPickerPop();
    if (!isShow()) {
      this.popwindow.showAtLocation(this.popview, 80, 0, 0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/cityjd/JDCityPicker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */