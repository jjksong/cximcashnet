package com.lljjcoder.style.citylist.utils;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.utils.utils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CityListLoader
{
  public static final String BUNDATA = "bundata";
  private static volatile CityListLoader instance;
  private static List<CityInfoBean> mCityListData = new ArrayList();
  private static List<CityInfoBean> mProListData = new ArrayList();
  
  public static CityListLoader getInstance()
  {
    if (instance == null) {
      try
      {
        if (instance == null)
        {
          CityListLoader localCityListLoader = new com/lljjcoder/style/citylist/utils/CityListLoader;
          localCityListLoader.<init>();
          instance = localCityListLoader;
        }
      }
      finally {}
    }
    return instance;
  }
  
  public List<CityInfoBean> getCityListData()
  {
    return mCityListData;
  }
  
  public List<CityInfoBean> getProListData()
  {
    return mProListData;
  }
  
  public void loadCityData(Context paramContext)
  {
    paramContext = utils.getJson(paramContext, "china_city_data.json");
    Object localObject = new TypeToken() {}.getType();
    paramContext = (ArrayList)new Gson().fromJson(paramContext, (Type)localObject);
    if ((paramContext != null) && (!paramContext.isEmpty()))
    {
      for (int i = 0; i < paramContext.size(); i++)
      {
        localObject = ((CityInfoBean)paramContext.get(i)).getCityList();
        for (int j = 0; j < ((ArrayList)localObject).size(); j++) {
          mCityListData.add(((ArrayList)localObject).get(j));
        }
      }
      return;
    }
  }
  
  public void loadProData(Context paramContext)
  {
    paramContext = utils.getJson(paramContext, "china_city_data.json");
    Type localType = new TypeToken() {}.getType();
    mProListData = (List)new Gson().fromJson(paramContext, localType);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/utils/CityListLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */