package com.lljjcoder.style.citylist.Toast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;

public class AlarmDailog
  extends Toast
{
  private Context context;
  private TextView noticeText;
  private Toast toast;
  
  public AlarmDailog(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    View localView = LayoutInflater.from(paramContext).inflate(R.layout.dialog_alarm_ui, null);
    this.noticeText = ((TextView)localView.findViewById(R.id.noticeText));
    this.toast = new Toast(paramContext);
    this.toast.setGravity(17, 0, 0);
    this.toast.setDuration(0);
    this.toast.setView(localView);
  }
  
  public void setShowText(String paramString)
  {
    this.noticeText.setText(paramString);
  }
  
  public void show()
  {
    this.toast.show();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/Toast/AlarmDailog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */