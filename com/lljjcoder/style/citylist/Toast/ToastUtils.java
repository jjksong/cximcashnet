package com.lljjcoder.style.citylist.Toast;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

public class ToastUtils
{
  private static AlarmDailog alarmDialog;
  
  public static void showLongToast(Context paramContext, String paramString)
  {
    if (alarmDialog != null) {
      alarmDialog = null;
    }
    alarmDialog = new AlarmDailog(paramContext);
    alarmDialog.setShowText(paramString);
    alarmDialog.show();
  }
  
  public static void showMomentToast(Activity paramActivity, Context paramContext, final String paramString)
  {
    paramActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (ToastUtils.alarmDialog == null)
        {
          ToastUtils.access$002(new AlarmDailog(this.val$context));
          ToastUtils.alarmDialog.setShowText(paramString);
          ToastUtils.alarmDialog.setDuration(0);
          ToastUtils.alarmDialog.show();
        }
        else
        {
          ToastUtils.alarmDialog.setShowText(paramString);
          ToastUtils.alarmDialog.show();
        }
        new Handler().postDelayed(new Runnable()
        {
          public void run()
          {
            if (ToastUtils.alarmDialog != null) {
              ToastUtils.alarmDialog.cancel();
            }
          }
        }, 2000L);
      }
    });
  }
  
  public static void showShortToast(Context paramContext, String paramString)
  {
    if (alarmDialog != null) {
      alarmDialog = null;
    }
    alarmDialog = new AlarmDailog(paramContext);
    alarmDialog.setShowText(paramString);
    alarmDialog.setDuration(0);
    alarmDialog.show();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/Toast/ToastUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */