package com.lljjcoder.style.citylist.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class CityInfoBean
  implements Parcelable
{
  public static final Parcelable.Creator<CityInfoBean> CREATOR = new Parcelable.Creator()
  {
    public CityInfoBean createFromParcel(Parcel paramAnonymousParcel)
    {
      return new CityInfoBean(paramAnonymousParcel);
    }
    
    public CityInfoBean[] newArray(int paramAnonymousInt)
    {
      return new CityInfoBean[paramAnonymousInt];
    }
  };
  private ArrayList<CityInfoBean> cityList;
  private String id;
  private String name;
  
  public CityInfoBean() {}
  
  protected CityInfoBean(Parcel paramParcel)
  {
    this.id = paramParcel.readString();
    this.name = paramParcel.readString();
    this.cityList = paramParcel.createTypedArrayList(CREATOR);
  }
  
  public static CityInfoBean findCity(List<CityInfoBean> paramList, String paramString)
  {
    int i = 0;
    try
    {
      while (i < paramList.size())
      {
        CityInfoBean localCityInfoBean = (CityInfoBean)paramList.get(i);
        boolean bool = paramString.equals(localCityInfoBean.getName());
        if (bool) {
          return localCityInfoBean;
        }
        i++;
      }
      return null;
    }
    catch (Exception paramList) {}
    return null;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public ArrayList<CityInfoBean> getCityList()
  {
    return this.cityList;
  }
  
  public String getId()
  {
    String str2 = this.id;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getName()
  {
    String str2 = this.name;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public void setCityList(ArrayList<CityInfoBean> paramArrayList)
  {
    this.cityList = paramArrayList;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.id);
    paramParcel.writeString(this.name);
    paramParcel.writeTypedList(this.cityList);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/bean/CityInfoBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */