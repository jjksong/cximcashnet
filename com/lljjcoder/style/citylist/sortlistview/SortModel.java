package com.lljjcoder.style.citylist.sortlistview;

public class SortModel
{
  private String name;
  private String sortLetters;
  
  public String getName()
  {
    return this.name;
  }
  
  public String getSortLetters()
  {
    return this.sortLetters;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setSortLetters(String paramString)
  {
    this.sortLetters = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/sortlistview/SortModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */