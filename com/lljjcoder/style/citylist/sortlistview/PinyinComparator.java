package com.lljjcoder.style.citylist.sortlistview;

import java.util.Comparator;

public class PinyinComparator
  implements Comparator<SortModel>
{
  public int compare(SortModel paramSortModel1, SortModel paramSortModel2)
  {
    if ((!paramSortModel1.getSortLetters().equals("@")) && (!paramSortModel2.getSortLetters().equals("#")))
    {
      if ((!paramSortModel1.getSortLetters().equals("#")) && (!paramSortModel2.getSortLetters().equals("@"))) {
        return paramSortModel1.getSortLetters().compareTo(paramSortModel2.getSortLetters());
      }
      return 1;
    }
    return -1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/sortlistview/PinyinComparator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */