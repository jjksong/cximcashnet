package com.lljjcoder.style.citylist.sortlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import java.util.List;

public class SortAdapter
  extends BaseAdapter
  implements SectionIndexer
{
  private List<SortModel> list = null;
  private Context mContext;
  
  public SortAdapter(Context paramContext, List<SortModel> paramList)
  {
    this.mContext = paramContext;
    this.list = paramList;
  }
  
  private String getAlpha(String paramString)
  {
    paramString = paramString.trim().substring(0, 1).toUpperCase();
    if (paramString.matches("[A-Z]")) {
      return paramString;
    }
    return "#";
  }
  
  public int getCount()
  {
    return this.list.size();
  }
  
  public Object getItem(int paramInt)
  {
    return this.list.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public int getPositionForSection(int paramInt)
  {
    for (int i = 0; i < getCount(); i++) {
      if (((SortModel)this.list.get(i)).getSortLetters().toUpperCase().charAt(0) == paramInt) {
        return i;
      }
    }
    return -1;
  }
  
  public int getSectionForPosition(int paramInt)
  {
    return ((SortModel)this.list.get(paramInt)).getSortLetters().charAt(0);
  }
  
  public Object[] getSections()
  {
    return null;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = new ViewHolder();
      paramViewGroup = LayoutInflater.from(this.mContext).inflate(R.layout.sortlistview_item, null);
      paramView.tvTitle = ((TextView)paramViewGroup.findViewById(R.id.title));
      paramView.tvLetter = ((TextView)paramViewGroup.findViewById(R.id.catalog));
      paramViewGroup.setTag(paramView);
    }
    else
    {
      localObject = (ViewHolder)paramView.getTag();
      paramViewGroup = paramView;
      paramView = (View)localObject;
    }
    Object localObject = this.list;
    if ((localObject != null) && (!((List)localObject).isEmpty()))
    {
      localObject = (SortModel)this.list.get(paramInt);
      if (paramInt == getPositionForSection(getSectionForPosition(paramInt)))
      {
        paramView.tvLetter.setVisibility(0);
        paramView.tvLetter.setText(((SortModel)localObject).getSortLetters());
      }
      else
      {
        paramView.tvLetter.setVisibility(8);
      }
      paramView.tvTitle.setText(((SortModel)this.list.get(paramInt)).getName());
    }
    return paramViewGroup;
  }
  
  public void updateListView(List<SortModel> paramList)
  {
    this.list = paramList;
    notifyDataSetChanged();
  }
  
  static final class ViewHolder
  {
    TextView tvLetter;
    TextView tvTitle;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/sortlistview/SortAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */