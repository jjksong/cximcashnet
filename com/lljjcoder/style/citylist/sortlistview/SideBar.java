package com.lljjcoder.style.citylist.sortlistview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.lljjcoder.style.citypickerview.R.drawable;

public class SideBar
  extends View
{
  public static String[] b = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#" };
  private int choose = -1;
  private TextView mTextDialog;
  private OnTouchingLetterChangedListener onTouchingLetterChangedListener;
  private Paint paint = new Paint();
  
  public SideBar(Context paramContext)
  {
    super(paramContext);
  }
  
  public SideBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SideBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    float f = paramMotionEvent.getY();
    int k = this.choose;
    OnTouchingLetterChangedListener localOnTouchingLetterChangedListener = this.onTouchingLetterChangedListener;
    int j = (int)(f / getHeight() * b.length);
    if (i != 1)
    {
      setBackgroundResource(R.drawable.sidebar_background);
      if ((k != j) && (j >= 0))
      {
        paramMotionEvent = b;
        if (j < paramMotionEvent.length)
        {
          if (localOnTouchingLetterChangedListener != null) {
            localOnTouchingLetterChangedListener.onTouchingLetterChanged(paramMotionEvent[j]);
          }
          paramMotionEvent = this.mTextDialog;
          if (paramMotionEvent != null)
          {
            paramMotionEvent.setText(b[j]);
            this.mTextDialog.setVisibility(0);
          }
          this.choose = j;
          invalidate();
        }
      }
    }
    else
    {
      setBackgroundDrawable(new ColorDrawable(0));
      this.choose = -1;
      invalidate();
      paramMotionEvent = this.mTextDialog;
      if (paramMotionEvent != null) {
        paramMotionEvent.setVisibility(4);
      }
    }
    return true;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = getHeight();
    int j = getWidth();
    int k = i / b.length;
    for (i = 0; i < b.length; i++)
    {
      this.paint.setColor(Color.rgb(33, 65, 98));
      this.paint.setTypeface(Typeface.DEFAULT_BOLD);
      this.paint.setAntiAlias(true);
      this.paint.setTextSize(20.0F);
      if (i == this.choose)
      {
        this.paint.setColor(Color.parseColor("#000000"));
        this.paint.setFakeBoldText(true);
      }
      float f3 = j / 2;
      float f1 = this.paint.measureText(b[i]) / 2.0F;
      float f2 = k * i + k;
      paramCanvas.drawText(b[i], f3 - f1, f2, this.paint);
      this.paint.reset();
    }
  }
  
  public void setOnTouchingLetterChangedListener(OnTouchingLetterChangedListener paramOnTouchingLetterChangedListener)
  {
    this.onTouchingLetterChangedListener = paramOnTouchingLetterChangedListener;
  }
  
  public void setTextView(TextView paramTextView)
  {
    this.mTextDialog = paramTextView;
  }
  
  public static abstract interface OnTouchingLetterChangedListener
  {
    public abstract void onTouchingLetterChanged(String paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/sortlistview/SideBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */