package com.lljjcoder.style.citylist.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;

public class CleanableEditView
  extends AppCompatEditText
  implements TextWatcher, View.OnFocusChangeListener
{
  private boolean hasFocus = false;
  private Drawable left;
  private Drawable right;
  private int xUp = 0;
  
  public CleanableEditView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CleanableEditView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 16842862);
  }
  
  public CleanableEditView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initWedgits(paramAttributeSet);
  }
  
  private void addListeners()
  {
    try
    {
      setOnFocusChangeListener(this);
      addTextChangedListener(this);
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  private void initDatas()
  {
    try
    {
      setCompoundDrawablesWithIntrinsicBounds(this.left, null, null, null);
      addListeners();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  private void initWedgits(AttributeSet paramAttributeSet)
  {
    try
    {
      this.left = getCompoundDrawables()[0];
      this.right = getCompoundDrawables()[2];
      initDatas();
    }
    catch (Exception paramAttributeSet)
    {
      paramAttributeSet.printStackTrace();
    }
  }
  
  public void afterTextChanged(Editable paramEditable) {}
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    try
    {
      this.hasFocus = paramBoolean;
      paramView = getText().toString();
      if (paramBoolean) {
        if (paramView.equalsIgnoreCase("")) {
          setCompoundDrawablesWithIntrinsicBounds(this.left, null, null, null);
        } else {
          setCompoundDrawablesWithIntrinsicBounds(this.left, null, this.right, null);
        }
      }
      if (!paramBoolean) {
        setCompoundDrawablesWithIntrinsicBounds(this.left, null, null, null);
      }
    }
    catch (Exception paramView)
    {
      paramView.printStackTrace();
    }
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.hasFocus) {
      if (TextUtils.isEmpty(paramCharSequence))
      {
        setCompoundDrawablesWithIntrinsicBounds(this.left, null, null, null);
      }
      else
      {
        if (this.right == null) {
          this.right = getCompoundDrawables()[2];
        }
        setCompoundDrawablesWithIntrinsicBounds(this.left, null, this.right, null);
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    try
    {
      if (paramMotionEvent.getAction() == 1)
      {
        this.xUp = ((int)paramMotionEvent.getX());
        if ((getWidth() - this.xUp <= getCompoundPaddingRight()) && (!TextUtils.isEmpty(getText().toString()))) {
          setText("");
        }
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public String text_String()
  {
    return getText().toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/widget/CleanableEditView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */