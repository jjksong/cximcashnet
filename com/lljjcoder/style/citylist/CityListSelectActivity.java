package com.lljjcoder.style.citylist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.style.citylist.sortlistview.CharacterParser;
import com.lljjcoder.style.citylist.sortlistview.PinyinComparator;
import com.lljjcoder.style.citylist.sortlistview.SideBar;
import com.lljjcoder.style.citylist.sortlistview.SideBar.OnTouchingLetterChangedListener;
import com.lljjcoder.style.citylist.sortlistview.SortAdapter;
import com.lljjcoder.style.citylist.sortlistview.SortModel;
import com.lljjcoder.style.citylist.utils.CityListLoader;
import com.lljjcoder.style.citylist.widget.CleanableEditView;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import com.lljjcoder.utils.PinYinUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CityListSelectActivity
  extends AppCompatActivity
{
  public static final int CITY_SELECT_RESULT_FRAG = 50;
  public static List<CityInfoBean> sCityInfoBeanList = new ArrayList();
  public SortAdapter adapter;
  private CharacterParser characterParser;
  private CityInfoBean cityInfoBean = new CityInfoBean();
  private List<CityInfoBean> cityListInfo = new ArrayList();
  ImageView imgBack;
  CleanableEditView mCityTextSearch;
  TextView mCurrentCity;
  TextView mCurrentCityTag;
  TextView mDialog;
  TextView mLocalCity;
  TextView mLocalCityTag;
  public PinYinUtils mPinYinUtils = new PinYinUtils();
  SideBar mSidrbar;
  private PinyinComparator pinyinComparator;
  ListView sortListView;
  private List<SortModel> sourceDateList;
  
  private List<SortModel> filledData(List<CityInfoBean> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramList.size(); i++)
    {
      Object localObject1 = (CityInfoBean)paramList.get(i);
      if (localObject1 != null)
      {
        Object localObject2 = new SortModel();
        String str = ((CityInfoBean)localObject1).getName();
        if ((!TextUtils.isEmpty(str)) && (str.length() > 0))
        {
          if (str.equals("重庆市")) {
            localObject1 = "chong";
          } else if (str.equals("长沙市")) {
            localObject1 = "chang";
          } else if (str.equals("长春市")) {
            localObject1 = "chang";
          } else {
            localObject1 = this.mPinYinUtils.getStringPinYin(str.substring(0, 1));
          }
          if (!TextUtils.isEmpty((CharSequence)localObject1))
          {
            ((SortModel)localObject2).setName(str);
            localObject1 = ((String)localObject1).substring(0, 1).toUpperCase();
            if (((String)localObject1).matches("[A-Z]")) {
              ((SortModel)localObject2).setSortLetters(((String)localObject1).toUpperCase());
            } else {
              ((SortModel)localObject2).setSortLetters("#");
            }
            localArrayList.add(localObject2);
          }
          else
          {
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append("null,cityName:-> ");
            ((StringBuilder)localObject2).append(str);
            ((StringBuilder)localObject2).append("       pinyin:-> ");
            ((StringBuilder)localObject2).append((String)localObject1);
            Log.d("citypicker_log", ((StringBuilder)localObject2).toString());
          }
        }
      }
    }
    return localArrayList;
  }
  
  private void filterData(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject;
    if (TextUtils.isEmpty(paramString))
    {
      localObject = this.sourceDateList;
    }
    else
    {
      localArrayList.clear();
      Iterator localIterator = this.sourceDateList.iterator();
      for (;;)
      {
        localObject = localArrayList;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject = (SortModel)localIterator.next();
        String str = ((SortModel)localObject).getName();
        if ((str.contains(paramString)) || (this.characterParser.getSelling(str).startsWith(paramString))) {
          localArrayList.add(localObject);
        }
      }
    }
    Collections.sort((List)localObject, this.pinyinComparator);
    this.adapter.updateListView((List)localObject);
  }
  
  private void initList()
  {
    this.sourceDateList = new ArrayList();
    this.adapter = new SortAdapter(this, this.sourceDateList);
    this.sortListView.setAdapter(this.adapter);
    this.characterParser = CharacterParser.getInstance();
    this.pinyinComparator = new PinyinComparator();
    this.mSidrbar.setTextView(this.mDialog);
    this.mSidrbar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener()
    {
      public void onTouchingLetterChanged(String paramAnonymousString)
      {
        int i = CityListSelectActivity.this.adapter.getPositionForSection(paramAnonymousString.charAt(0));
        if (i != -1) {
          CityListSelectActivity.this.sortListView.setSelection(i);
        }
      }
    });
    this.sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousView = ((SortModel)CityListSelectActivity.this.adapter.getItem(paramAnonymousInt)).getName();
        paramAnonymousAdapterView = CityListSelectActivity.this;
        CityListSelectActivity.access$002(paramAnonymousAdapterView, CityInfoBean.findCity(paramAnonymousAdapterView.cityListInfo, paramAnonymousView));
        paramAnonymousAdapterView = new Intent();
        paramAnonymousView = new Bundle();
        paramAnonymousView.putParcelable("cityinfo", CityListSelectActivity.this.cityInfoBean);
        paramAnonymousAdapterView.putExtras(paramAnonymousView);
        CityListSelectActivity.this.setResult(-1, paramAnonymousAdapterView);
        CityListSelectActivity.this.finish();
      }
    });
    this.mCityTextSearch.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        CityListSelectActivity.this.filterData(paramAnonymousCharSequence.toString());
      }
    });
  }
  
  private void initView()
  {
    this.mCityTextSearch = ((CleanableEditView)findViewById(R.id.cityInputText));
    this.mCurrentCityTag = ((TextView)findViewById(R.id.currentCityTag));
    this.mCurrentCity = ((TextView)findViewById(R.id.currentCity));
    this.mLocalCityTag = ((TextView)findViewById(R.id.localCityTag));
    this.mLocalCity = ((TextView)findViewById(R.id.localCity));
    this.sortListView = ((ListView)findViewById(R.id.country_lvcountry));
    this.mDialog = ((TextView)findViewById(R.id.dialog));
    this.mSidrbar = ((SideBar)findViewById(R.id.sidrbar));
    this.imgBack = ((ImageView)findViewById(R.id.imgBack));
    this.imgBack.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CityListSelectActivity.this.finish();
      }
    });
  }
  
  private void setCityData(List<CityInfoBean> paramList)
  {
    this.cityListInfo = paramList;
    if (this.cityListInfo == null) {
      return;
    }
    int j = paramList.size();
    String[] arrayOfString = new String[j];
    for (int i = 0; i < j; i++) {
      arrayOfString[i] = ((CityInfoBean)paramList.get(i)).getName();
    }
    this.sourceDateList.addAll(filledData(paramList));
    Collections.sort(this.sourceDateList, this.pinyinComparator);
    this.adapter.notifyDataSetChanged();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_city_list_select);
    initView();
    initList();
    setCityData(CityListLoader.getInstance().getCityListData());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citylist/CityListSelectActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */