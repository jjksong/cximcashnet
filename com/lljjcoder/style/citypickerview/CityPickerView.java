package com.lljjcoder.style.citypickerview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.citywheel.CityConfig;
import com.lljjcoder.citywheel.CityConfig.WheelType;
import com.lljjcoder.citywheel.CityParseHelper;
import com.lljjcoder.style.citylist.Toast.ToastUtils;
import com.lljjcoder.style.citypickerview.widget.CanShow;
import com.lljjcoder.style.citypickerview.widget.wheel.OnWheelChangedListener;
import com.lljjcoder.style.citypickerview.widget.wheel.WheelView;
import com.lljjcoder.style.citypickerview.widget.wheel.adapters.ArrayWheelAdapter;
import com.lljjcoder.style.citypickerview.widget.wheel.adapters.WheelViewAdapter;
import com.lljjcoder.utils.utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CityPickerView
  implements CanShow, OnWheelChangedListener
{
  private String TAG = "citypicker_log";
  private CityConfig config;
  private Context context;
  private OnCityItemClickListener mBaseListener;
  private RelativeLayout mRelativeTitleBg;
  private TextView mTvCancel;
  private TextView mTvOK;
  private TextView mTvTitle;
  private WheelView mViewCity;
  private WheelView mViewDistrict;
  private WheelView mViewProvince;
  private CityParseHelper parseHelper;
  private View popview;
  private PopupWindow popwindow;
  private ProvinceBean[] proArra;
  
  private ProvinceBean[] getProArrData(ProvinceBean[] paramArrayOfProvinceBean)
  {
    ArrayList localArrayList = new ArrayList();
    int j = 0;
    for (int i = 0; i < paramArrayOfProvinceBean.length; i++) {
      localArrayList.add(paramArrayOfProvinceBean[i]);
    }
    if (!this.config.isShowGAT())
    {
      localArrayList.remove(localArrayList.size() - 1);
      localArrayList.remove(localArrayList.size() - 1);
      localArrayList.remove(localArrayList.size() - 1);
    }
    this.proArra = new ProvinceBean[localArrayList.size()];
    for (i = j; i < localArrayList.size(); i++) {
      this.proArra[i] = ((ProvinceBean)localArrayList.get(i));
    }
    return this.proArra;
  }
  
  private void initCityPickerPopwindow()
  {
    if (this.config != null)
    {
      if (this.parseHelper == null) {
        this.parseHelper = new CityParseHelper();
      }
      if (this.parseHelper.getProvinceBeanArrayList().isEmpty())
      {
        ToastUtils.showLongToast(this.context, "请在Activity中增加init操作");
        return;
      }
      this.popview = LayoutInflater.from(this.context).inflate(R.layout.pop_citypicker, null);
      this.mViewProvince = ((WheelView)this.popview.findViewById(R.id.id_province));
      this.mViewCity = ((WheelView)this.popview.findViewById(R.id.id_city));
      this.mViewDistrict = ((WheelView)this.popview.findViewById(R.id.id_district));
      this.mRelativeTitleBg = ((RelativeLayout)this.popview.findViewById(R.id.rl_title));
      this.mTvOK = ((TextView)this.popview.findViewById(R.id.tv_confirm));
      this.mTvTitle = ((TextView)this.popview.findViewById(R.id.tv_title));
      this.mTvCancel = ((TextView)this.popview.findViewById(R.id.tv_cancel));
      this.popwindow = new PopupWindow(this.popview, -1, -2);
      this.popwindow.setAnimationStyle(R.style.AnimBottom);
      this.popwindow.setBackgroundDrawable(new ColorDrawable());
      this.popwindow.setTouchable(true);
      this.popwindow.setOutsideTouchable(false);
      this.popwindow.setFocusable(true);
      this.popwindow.setOnDismissListener(new PopupWindow.OnDismissListener()
      {
        public void onDismiss()
        {
          if (CityPickerView.this.config.isShowBackground()) {
            utils.setBackgroundAlpha(CityPickerView.this.context, 1.0F);
          }
        }
      });
      StringBuilder localStringBuilder;
      if (!TextUtils.isEmpty(this.config.getTitleBackgroundColorStr())) {
        if (this.config.getTitleBackgroundColorStr().startsWith("#"))
        {
          this.mRelativeTitleBg.setBackgroundColor(Color.parseColor(this.config.getTitleBackgroundColorStr()));
        }
        else
        {
          localObject = this.mRelativeTitleBg;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("#");
          localStringBuilder.append(this.config.getTitleBackgroundColorStr());
          ((RelativeLayout)localObject).setBackgroundColor(Color.parseColor(localStringBuilder.toString()));
        }
      }
      if (!TextUtils.isEmpty(this.config.getTitle())) {
        this.mTvTitle.setText(this.config.getTitle());
      }
      if (this.config.getTitleTextSize() > 0) {
        this.mTvTitle.setTextSize(this.config.getTitleTextSize());
      }
      if (!TextUtils.isEmpty(this.config.getTitleTextColorStr())) {
        if (this.config.getTitleTextColorStr().startsWith("#"))
        {
          this.mTvTitle.setTextColor(Color.parseColor(this.config.getTitleTextColorStr()));
        }
        else
        {
          localObject = this.mTvTitle;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("#");
          localStringBuilder.append(this.config.getTitleTextColorStr());
          ((TextView)localObject).setTextColor(Color.parseColor(localStringBuilder.toString()));
        }
      }
      if (!TextUtils.isEmpty(this.config.getConfirmTextColorStr())) {
        if (this.config.getConfirmTextColorStr().startsWith("#"))
        {
          this.mTvOK.setTextColor(Color.parseColor(this.config.getConfirmTextColorStr()));
        }
        else
        {
          localObject = this.mTvOK;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("#");
          localStringBuilder.append(this.config.getConfirmTextColorStr());
          ((TextView)localObject).setTextColor(Color.parseColor(localStringBuilder.toString()));
        }
      }
      if (!TextUtils.isEmpty(this.config.getConfirmText())) {
        this.mTvOK.setText(this.config.getConfirmText());
      }
      if (this.config.getConfirmTextSize() > 0) {
        this.mTvOK.setTextSize(this.config.getConfirmTextSize());
      }
      if (!TextUtils.isEmpty(this.config.getCancelTextColorStr())) {
        if (this.config.getCancelTextColorStr().startsWith("#"))
        {
          this.mTvCancel.setTextColor(Color.parseColor(this.config.getCancelTextColorStr()));
        }
        else
        {
          localObject = this.mTvCancel;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("#");
          localStringBuilder.append(this.config.getCancelTextColorStr());
          ((TextView)localObject).setTextColor(Color.parseColor(localStringBuilder.toString()));
        }
      }
      if (!TextUtils.isEmpty(this.config.getCancelText())) {
        this.mTvCancel.setText(this.config.getCancelText());
      }
      if (this.config.getCancelTextSize() > 0) {
        this.mTvCancel.setTextSize(this.config.getCancelTextSize());
      }
      if (this.config.getWheelType() == CityConfig.WheelType.PRO)
      {
        this.mViewCity.setVisibility(8);
        this.mViewDistrict.setVisibility(8);
      }
      else if (this.config.getWheelType() == CityConfig.WheelType.PRO_CITY)
      {
        this.mViewDistrict.setVisibility(8);
      }
      else
      {
        this.mViewProvince.setVisibility(0);
        this.mViewCity.setVisibility(0);
        this.mViewDistrict.setVisibility(0);
      }
      this.mViewProvince.addChangingListener(this);
      this.mViewCity.addChangingListener(this);
      this.mViewDistrict.addChangingListener(this);
      this.mTvCancel.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          CityPickerView.this.mBaseListener.onCancel();
          CityPickerView.this.hide();
        }
      });
      this.mTvOK.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (CityPickerView.this.parseHelper != null)
          {
            if (CityPickerView.this.config.getWheelType() == CityConfig.WheelType.PRO) {
              CityPickerView.this.mBaseListener.onSelected(CityPickerView.this.parseHelper.getProvinceBean(), new CityBean(), new DistrictBean());
            } else if (CityPickerView.this.config.getWheelType() == CityConfig.WheelType.PRO_CITY) {
              CityPickerView.this.mBaseListener.onSelected(CityPickerView.this.parseHelper.getProvinceBean(), CityPickerView.this.parseHelper.getCityBean(), new DistrictBean());
            } else {
              CityPickerView.this.mBaseListener.onSelected(CityPickerView.this.parseHelper.getProvinceBean(), CityPickerView.this.parseHelper.getCityBean(), CityPickerView.this.parseHelper.getDistrictBean());
            }
          }
          else {
            CityPickerView.this.mBaseListener.onSelected(new ProvinceBean(), new CityBean(), new DistrictBean());
          }
          CityPickerView.this.hide();
        }
      });
      setUpData();
      Object localObject = this.config;
      if ((localObject != null) && (((CityConfig)localObject).isShowBackground())) {
        utils.setBackgroundAlpha(this.context, 0.5F);
      }
      return;
    }
    throw new IllegalArgumentException("please set config first...");
  }
  
  private void setUpData()
  {
    Object localObject = this.parseHelper;
    if ((localObject != null) && (this.config != null))
    {
      getProArrData(((CityParseHelper)localObject).getProvinceBeenArray());
      if ((!TextUtils.isEmpty(this.config.getDefaultProvinceName())) && (this.proArra.length > 0)) {
        for (i = 0;; i++)
        {
          localObject = this.proArra;
          if (i >= localObject.length) {
            break;
          }
          if (localObject[i].getName().contains(this.config.getDefaultProvinceName())) {
            break label92;
          }
        }
      }
      int i = -1;
      label92:
      localObject = new ArrayWheelAdapter(this.context, this.proArra);
      this.mViewProvince.setViewAdapter((WheelViewAdapter)localObject);
      if ((this.config.getCustomItemLayout() != CityConfig.NONE) && (this.config.getCustomItemTextViewId() != CityConfig.NONE))
      {
        ((ArrayWheelAdapter)localObject).setItemResource(this.config.getCustomItemLayout().intValue());
        ((ArrayWheelAdapter)localObject).setItemTextResource(this.config.getCustomItemTextViewId().intValue());
      }
      else
      {
        ((ArrayWheelAdapter)localObject).setItemResource(R.layout.default_item_city);
        ((ArrayWheelAdapter)localObject).setItemTextResource(R.id.default_item_city_name_tv);
      }
      if (-1 != i) {
        this.mViewProvince.setCurrentItem(i);
      }
      this.mViewProvince.setVisibleItems(this.config.getVisibleItems());
      this.mViewCity.setVisibleItems(this.config.getVisibleItems());
      this.mViewDistrict.setVisibleItems(this.config.getVisibleItems());
      this.mViewProvince.setCyclic(this.config.isProvinceCyclic());
      this.mViewCity.setCyclic(this.config.isCityCyclic());
      this.mViewDistrict.setCyclic(this.config.isDistrictCyclic());
      this.mViewProvince.setDrawShadows(this.config.isDrawShadows());
      this.mViewCity.setDrawShadows(this.config.isDrawShadows());
      this.mViewDistrict.setDrawShadows(this.config.isDrawShadows());
      this.mViewProvince.setLineColorStr(this.config.getLineColor());
      this.mViewProvince.setLineWidth(this.config.getLineHeigh());
      this.mViewCity.setLineColorStr(this.config.getLineColor());
      this.mViewCity.setLineWidth(this.config.getLineHeigh());
      this.mViewDistrict.setLineColorStr(this.config.getLineColor());
      this.mViewDistrict.setLineWidth(this.config.getLineHeigh());
      updateCities();
      updateAreas();
      return;
    }
  }
  
  private void updateAreas()
  {
    int i = this.mViewCity.getCurrentItem();
    if ((this.parseHelper.getPro_CityMap() != null) && (this.parseHelper.getCity_DisMap() != null))
    {
      if ((this.config.getWheelType() == CityConfig.WheelType.PRO_CITY) || (this.config.getWheelType() == CityConfig.WheelType.PRO_CITY_DIS))
      {
        CityBean localCityBean = ((CityBean[])this.parseHelper.getPro_CityMap().get(this.parseHelper.getProvinceBean().getName()))[i];
        this.parseHelper.setCityBean(localCityBean);
        if (this.config.getWheelType() == CityConfig.WheelType.PRO_CITY_DIS)
        {
          Object localObject1 = this.parseHelper.getCity_DisMap();
          Object localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append(this.parseHelper.getProvinceBean().getName());
          ((StringBuilder)localObject2).append(localCityBean.getName());
          localObject2 = (DistrictBean[])((Map)localObject1).get(((StringBuilder)localObject2).toString());
          if (localObject2 == null) {
            return;
          }
          if ((!TextUtils.isEmpty(this.config.getDefaultDistrict())) && (localObject2.length > 0)) {
            for (i = 0; i < localObject2.length; i++) {
              if (this.config.getDefaultDistrict().contains(localObject2[i].getName())) {
                break label230;
              }
            }
          }
          i = -1;
          label230:
          localObject1 = new ArrayWheelAdapter(this.context, (Object[])localObject2);
          if ((this.config.getCustomItemLayout() != CityConfig.NONE) && (this.config.getCustomItemTextViewId() != CityConfig.NONE))
          {
            ((ArrayWheelAdapter)localObject1).setItemResource(this.config.getCustomItemLayout().intValue());
            ((ArrayWheelAdapter)localObject1).setItemTextResource(this.config.getCustomItemTextViewId().intValue());
          }
          else
          {
            ((ArrayWheelAdapter)localObject1).setItemResource(R.layout.default_item_city);
            ((ArrayWheelAdapter)localObject1).setItemTextResource(R.id.default_item_city_name_tv);
          }
          this.mViewDistrict.setViewAdapter((WheelViewAdapter)localObject1);
          localObject1 = null;
          if (this.parseHelper.getDisMap() == null) {
            return;
          }
          if (-1 != i)
          {
            this.mViewDistrict.setCurrentItem(i);
            localObject2 = this.parseHelper.getDisMap();
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append(this.parseHelper.getProvinceBean().getName());
            ((StringBuilder)localObject1).append(localCityBean.getName());
            ((StringBuilder)localObject1).append(this.config.getDefaultDistrict());
            localObject1 = (DistrictBean)((Map)localObject2).get(((StringBuilder)localObject1).toString());
          }
          else
          {
            this.mViewDistrict.setCurrentItem(0);
            if (localObject2.length > 0) {
              localObject1 = localObject2[0];
            }
          }
          this.parseHelper.setDistrictBean((DistrictBean)localObject1);
        }
      }
      return;
    }
  }
  
  private void updateCities()
  {
    if ((this.parseHelper != null) && (this.config != null))
    {
      int i = this.mViewProvince.getCurrentItem();
      Object localObject = this.proArra[i];
      this.parseHelper.setProvinceBean((ProvinceBean)localObject);
      if (this.parseHelper.getPro_CityMap() == null) {
        return;
      }
      localObject = (CityBean[])this.parseHelper.getPro_CityMap().get(((ProvinceBean)localObject).getName());
      if (localObject == null) {
        return;
      }
      if ((!TextUtils.isEmpty(this.config.getDefaultCityName())) && (localObject.length > 0)) {
        for (i = 0; i < localObject.length; i++) {
          if (this.config.getDefaultCityName().contains(localObject[i].getName())) {
            break label132;
          }
        }
      }
      i = -1;
      label132:
      localObject = new ArrayWheelAdapter(this.context, (Object[])localObject);
      if ((this.config.getCustomItemLayout() != CityConfig.NONE) && (this.config.getCustomItemTextViewId() != CityConfig.NONE))
      {
        ((ArrayWheelAdapter)localObject).setItemResource(this.config.getCustomItemLayout().intValue());
        ((ArrayWheelAdapter)localObject).setItemTextResource(this.config.getCustomItemTextViewId().intValue());
      }
      else
      {
        ((ArrayWheelAdapter)localObject).setItemResource(R.layout.default_item_city);
        ((ArrayWheelAdapter)localObject).setItemTextResource(R.id.default_item_city_name_tv);
      }
      this.mViewCity.setViewAdapter((WheelViewAdapter)localObject);
      if (-1 != i) {
        this.mViewCity.setCurrentItem(i);
      } else {
        this.mViewCity.setCurrentItem(0);
      }
      updateAreas();
      return;
    }
  }
  
  public void hide()
  {
    if (isShow()) {
      this.popwindow.dismiss();
    }
  }
  
  public void init(Context paramContext)
  {
    this.context = paramContext;
    this.parseHelper = new CityParseHelper();
    if (this.parseHelper.getProvinceBeanArrayList().isEmpty()) {
      this.parseHelper.initData(paramContext);
    }
  }
  
  public boolean isShow()
  {
    return this.popwindow.isShowing();
  }
  
  public void onChanged(WheelView paramWheelView, int paramInt1, int paramInt2)
  {
    if (paramWheelView == this.mViewProvince)
    {
      updateCities();
    }
    else if (paramWheelView == this.mViewCity)
    {
      updateAreas();
    }
    else if (paramWheelView == this.mViewDistrict)
    {
      paramWheelView = this.parseHelper;
      if ((paramWheelView != null) && (paramWheelView.getCity_DisMap() != null))
      {
        Map localMap = this.parseHelper.getCity_DisMap();
        paramWheelView = new StringBuilder();
        paramWheelView.append(this.parseHelper.getProvinceBean().getName());
        paramWheelView.append(this.parseHelper.getCityBean().getName());
        paramWheelView = ((DistrictBean[])localMap.get(paramWheelView.toString()))[paramInt2];
        this.parseHelper.setDistrictBean(paramWheelView);
      }
    }
  }
  
  public void setConfig(CityConfig paramCityConfig)
  {
    this.config = paramCityConfig;
  }
  
  public void setOnCityItemClickListener(OnCityItemClickListener paramOnCityItemClickListener)
  {
    this.mBaseListener = paramOnCityItemClickListener;
  }
  
  public void showCityPicker()
  {
    initCityPickerPopwindow();
    if (!isShow()) {
      this.popwindow.showAtLocation(this.popview, 80, 0, 0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/CityPickerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */