package com.lljjcoder.style.citypickerview.model;

import java.util.List;

public class CityModel
{
  private List<DistrictModel> districtList;
  private String name;
  
  public CityModel() {}
  
  public CityModel(String paramString, List<DistrictModel> paramList)
  {
    this.name = paramString;
    this.districtList = paramList;
  }
  
  public List<DistrictModel> getDistrictList()
  {
    return this.districtList;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setDistrictList(List<DistrictModel> paramList)
  {
    this.districtList = paramList;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("CityModel [name=");
    localStringBuilder.append(this.name);
    localStringBuilder.append(", districtList=");
    localStringBuilder.append(this.districtList);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/model/CityModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */