package com.lljjcoder.style.citypickerview.model;

public class DistrictModel
{
  private String name;
  private String zipcode;
  
  public DistrictModel() {}
  
  public DistrictModel(String paramString1, String paramString2)
  {
    this.name = paramString1;
    this.zipcode = paramString2;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getZipcode()
  {
    return this.zipcode;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setZipcode(String paramString)
  {
    this.zipcode = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DistrictModel [name=");
    localStringBuilder.append(this.name);
    localStringBuilder.append(", zipcode=");
    localStringBuilder.append(this.zipcode);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/model/DistrictModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */