package com.lljjcoder.style.citypickerview.model;

import java.util.List;

public class ProvinceModel
{
  private List<CityModel> cityList;
  private String name;
  
  public ProvinceModel() {}
  
  public ProvinceModel(String paramString, List<CityModel> paramList)
  {
    this.name = paramString;
    this.cityList = paramList;
  }
  
  public List<CityModel> getCityList()
  {
    return this.cityList;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setCityList(List<CityModel> paramList)
  {
    this.cityList = paramList;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ProvinceModel [name=");
    localStringBuilder.append(this.name);
    localStringBuilder.append(", cityList=");
    localStringBuilder.append(this.cityList);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/model/ProvinceModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */