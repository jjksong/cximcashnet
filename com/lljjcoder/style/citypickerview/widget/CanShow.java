package com.lljjcoder.style.citypickerview.widget;

public abstract interface CanShow
{
  public abstract void hide();
  
  public abstract boolean isShow();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/CanShow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */