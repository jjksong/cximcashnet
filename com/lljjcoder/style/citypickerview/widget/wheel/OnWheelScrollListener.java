package com.lljjcoder.style.citypickerview.widget.wheel;

public abstract interface OnWheelScrollListener
{
  public abstract void onScrollingFinished(WheelView paramWheelView);
  
  public abstract void onScrollingStarted(WheelView paramWheelView);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/OnWheelScrollListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */