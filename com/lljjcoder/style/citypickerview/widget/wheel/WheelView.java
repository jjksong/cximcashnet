package com.lljjcoder.style.citypickerview.widget.wheel;

import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import com.lljjcoder.style.citypickerview.R.drawable;
import com.lljjcoder.style.citypickerview.widget.wheel.adapters.WheelViewAdapter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class WheelView
  extends View
{
  private static final int DEF_VISIBLE_ITEMS = 5;
  private static final int ITEM_OFFSET_PERCENT = 0;
  private static final int PADDING = 5;
  private int[] SHADOWS_COLORS = { -269882903, -806753815, 1072294377 };
  private GradientDrawable bottomShadow;
  private Drawable centerDrawable;
  private List<OnWheelChangedListener> changingListeners = new LinkedList();
  private List<OnWheelClickedListener> clickingListeners = new LinkedList();
  private int currentItem = 0;
  private DataSetObserver dataObserver = new DataSetObserver()
  {
    public void onChanged()
    {
      WheelView.this.invalidateWheel(false);
    }
    
    public void onInvalidated()
    {
      WheelView.this.invalidateWheel(true);
    }
  };
  private boolean drawShadows = true;
  private int firstItem;
  boolean isCyclic = false;
  private boolean isScrollingPerformed;
  private int itemHeight = 0;
  private LinearLayout itemsLayout;
  private String lineColorStr = "#C7C7C7";
  private int lineWidth = 3;
  private WheelRecycle recycle = new WheelRecycle(this);
  private WheelScroller scroller;
  WheelScroller.ScrollingListener scrollingListener = new WheelScroller.ScrollingListener()
  {
    public void onFinished()
    {
      if (WheelView.this.isScrollingPerformed)
      {
        WheelView.this.notifyScrollingListenersAboutEnd();
        WheelView.access$002(WheelView.this, false);
      }
      WheelView.access$202(WheelView.this, 0);
      WheelView.this.invalidate();
    }
    
    public void onJustify()
    {
      if (Math.abs(WheelView.this.scrollingOffset) > 1) {
        WheelView.this.scroller.scroll(WheelView.this.scrollingOffset, 0);
      }
    }
    
    public void onScroll(int paramAnonymousInt)
    {
      WheelView.this.doScroll(paramAnonymousInt);
      int i = WheelView.this.getHeight();
      if (WheelView.this.scrollingOffset > i)
      {
        WheelView.access$202(WheelView.this, i);
        WheelView.this.scroller.stopScrolling();
      }
      else
      {
        paramAnonymousInt = WheelView.this.scrollingOffset;
        i = -i;
        if (paramAnonymousInt < i)
        {
          WheelView.access$202(WheelView.this, i);
          WheelView.this.scroller.stopScrolling();
        }
      }
    }
    
    public void onStarted()
    {
      WheelView.access$002(WheelView.this, true);
      WheelView.this.notifyScrollingListenersAboutStart();
    }
  };
  private List<OnWheelScrollListener> scrollingListeners = new LinkedList();
  private int scrollingOffset;
  private GradientDrawable topShadow;
  private WheelViewAdapter viewAdapter;
  private int visibleItems = 5;
  private int wheelBackground = R.drawable.wheel_bg;
  private int wheelForeground = R.drawable.wheel_val;
  
  public WheelView(Context paramContext)
  {
    super(paramContext);
    initData(paramContext);
  }
  
  public WheelView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initData(paramContext);
  }
  
  public WheelView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initData(paramContext);
  }
  
  private boolean addViewItem(int paramInt, boolean paramBoolean)
  {
    View localView = getItemView(paramInt);
    if (localView != null)
    {
      if (paramBoolean) {
        this.itemsLayout.addView(localView, 0);
      } else {
        this.itemsLayout.addView(localView);
      }
      return true;
    }
    return false;
  }
  
  private void buildViewForMeasuring()
  {
    LinearLayout localLinearLayout = this.itemsLayout;
    if (localLinearLayout != null) {
      this.recycle.recycleItems(localLinearLayout, this.firstItem, new ItemsRange());
    } else {
      createItemsLayout();
    }
    int j = this.visibleItems / 2;
    for (int i = this.currentItem + j; i >= this.currentItem - j; i--) {
      if (addViewItem(i, true)) {
        this.firstItem = i;
      }
    }
  }
  
  private int calculateLayoutWidth(int paramInt1, int paramInt2)
  {
    initResourcesIfNecessary();
    this.itemsLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    this.itemsLayout.measure(View.MeasureSpec.makeMeasureSpec(paramInt1, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    int i = this.itemsLayout.getMeasuredWidth();
    if (paramInt2 != 1073741824)
    {
      i = Math.max(i + 10, getSuggestedMinimumWidth());
      if ((paramInt2 != Integer.MIN_VALUE) || (paramInt1 >= i)) {
        paramInt1 = i;
      }
    }
    this.itemsLayout.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 - 10, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
    return paramInt1;
  }
  
  private void createItemsLayout()
  {
    if (this.itemsLayout == null)
    {
      this.itemsLayout = new LinearLayout(getContext());
      this.itemsLayout.setOrientation(1);
    }
  }
  
  private void doScroll(int paramInt)
  {
    this.scrollingOffset += paramInt;
    int n = getItemHeight();
    int j = this.scrollingOffset / n;
    int k = this.currentItem - j;
    int i1 = this.viewAdapter.getItemsCount();
    paramInt = this.scrollingOffset % n;
    int m = paramInt;
    if (Math.abs(paramInt) <= n / 2) {
      m = 0;
    }
    int i;
    if ((this.isCyclic) && (i1 > 0))
    {
      if (m > 0)
      {
        i = k - 1;
        paramInt = j + 1;
      }
      else
      {
        paramInt = j;
        i = k;
        if (m < 0)
        {
          i = k + 1;
          paramInt = j - 1;
        }
      }
      while (i < 0) {
        i += i1;
      }
      i %= i1;
    }
    else if (k < 0)
    {
      paramInt = this.currentItem;
      i = 0;
    }
    else if (k >= i1)
    {
      paramInt = this.currentItem - i1 + 1;
      i = i1 - 1;
    }
    else if ((k > 0) && (m > 0))
    {
      i = k - 1;
      paramInt = j + 1;
    }
    else
    {
      paramInt = j;
      i = k;
      if (k < i1 - 1)
      {
        paramInt = j;
        i = k;
        if (m < 0)
        {
          i = k + 1;
          paramInt = j - 1;
        }
      }
    }
    j = this.scrollingOffset;
    if (i != this.currentItem) {
      setCurrentItem(i, false);
    } else {
      invalidate();
    }
    this.scrollingOffset = (j - paramInt * n);
    if (this.scrollingOffset > getHeight()) {
      this.scrollingOffset = (this.scrollingOffset % getHeight() + getHeight());
    }
  }
  
  private void drawCenterRect(Canvas paramCanvas)
  {
    int i = getHeight() / 2;
    double d = getItemHeight() / 2;
    Double.isNaN(d);
    int j = (int)(d * 1.2D);
    Paint localPaint = new Paint();
    if (getLineColorStr().startsWith("#"))
    {
      localPaint.setColor(Color.parseColor(getLineColorStr()));
    }
    else
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("#");
      localStringBuilder.append(getLineColorStr());
      localPaint.setColor(Color.parseColor(localStringBuilder.toString()));
    }
    if (getLineWidth() > 3) {
      localPaint.setStrokeWidth(getLineWidth());
    } else {
      localPaint.setStrokeWidth(3.0F);
    }
    float f = i - j;
    paramCanvas.drawLine(0.0F, f, getWidth(), f, localPaint);
    f = i + j;
    paramCanvas.drawLine(0.0F, f, getWidth(), f, localPaint);
  }
  
  private void drawItems(Canvas paramCanvas)
  {
    paramCanvas.save();
    paramCanvas.translate(5.0F, -((this.currentItem - this.firstItem) * getItemHeight() + (getItemHeight() - getHeight()) / 2) + this.scrollingOffset);
    this.itemsLayout.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  private void drawShadows(Canvas paramCanvas)
  {
    int i;
    if (getVisibleItems() == 2) {
      i = 1;
    } else {
      i = getVisibleItems() / 2;
    }
    i *= getItemHeight();
    this.topShadow.setBounds(0, 0, getWidth(), i);
    this.topShadow.draw(paramCanvas);
    this.bottomShadow.setBounds(0, getHeight() - i, getWidth(), getHeight());
    this.bottomShadow.draw(paramCanvas);
  }
  
  private int getDesiredHeight(LinearLayout paramLinearLayout)
  {
    if ((paramLinearLayout != null) && (paramLinearLayout.getChildAt(0) != null)) {
      this.itemHeight = paramLinearLayout.getChildAt(0).getMeasuredHeight();
    }
    int i = this.itemHeight;
    return Math.max(this.visibleItems * i - i * 0 / 50, getSuggestedMinimumHeight());
  }
  
  private int getItemHeight()
  {
    int i = this.itemHeight;
    if (i != 0) {
      return i;
    }
    LinearLayout localLinearLayout = this.itemsLayout;
    if ((localLinearLayout != null) && (localLinearLayout.getChildAt(0) != null))
    {
      this.itemHeight = this.itemsLayout.getChildAt(0).getHeight();
      return this.itemHeight;
    }
    return getHeight() / this.visibleItems;
  }
  
  private View getItemView(int paramInt)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() != 0))
    {
      int j = this.viewAdapter.getItemsCount();
      int i = paramInt;
      if (!isValidItemIndex(paramInt)) {
        return this.viewAdapter.getEmptyItem(this.recycle.getEmptyItem(), this.itemsLayout);
      }
      while (i < 0) {
        i += j;
      }
      return this.viewAdapter.getItem(i % j, this.recycle.getItem(), this.itemsLayout);
    }
    return null;
  }
  
  private ItemsRange getItemsRange()
  {
    if (getItemHeight() == 0) {
      return null;
    }
    int i = this.currentItem;
    for (int j = 1; getItemHeight() * j < getHeight(); j += 2) {
      i--;
    }
    int n = this.scrollingOffset;
    int m = i;
    int k = j;
    if (n != 0)
    {
      k = i;
      if (n > 0) {
        k = i - 1;
      }
      i = this.scrollingOffset / getItemHeight();
      m = k - i;
      double d2 = j + 1;
      double d1 = Math.asin(i);
      Double.isNaN(d2);
      k = (int)(d2 + d1);
    }
    return new ItemsRange(m, k);
  }
  
  private void initData(Context paramContext)
  {
    this.scroller = new WheelScroller(getContext(), this.scrollingListener);
  }
  
  private void initResourcesIfNecessary()
  {
    if (this.centerDrawable == null) {
      this.centerDrawable = getContext().getResources().getDrawable(this.wheelForeground);
    }
    if (this.topShadow == null) {
      this.topShadow = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, this.SHADOWS_COLORS);
    }
    if (this.bottomShadow == null) {
      this.bottomShadow = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, this.SHADOWS_COLORS);
    }
    setBackgroundResource(this.wheelBackground);
  }
  
  private boolean isValidItemIndex(int paramInt)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    boolean bool;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() > 0) && ((this.isCyclic) || ((paramInt >= 0) && (paramInt < this.viewAdapter.getItemsCount())))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void layout(int paramInt1, int paramInt2)
  {
    this.itemsLayout.layout(0, 0, paramInt1 - 10, paramInt2);
  }
  
  private boolean rebuildItems()
  {
    ItemsRange localItemsRange = getItemsRange();
    LinearLayout localLinearLayout = this.itemsLayout;
    boolean bool1;
    if (localLinearLayout != null)
    {
      i = this.recycle.recycleItems(localLinearLayout, this.firstItem, localItemsRange);
      if (this.firstItem != i) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      this.firstItem = i;
    }
    else
    {
      createItemsLayout();
      bool1 = true;
    }
    boolean bool2 = bool1;
    if (!bool1) {
      if ((this.firstItem == localItemsRange.getFirst()) && (this.itemsLayout.getChildCount() == localItemsRange.getCount())) {
        bool2 = false;
      } else {
        bool2 = true;
      }
    }
    if ((this.firstItem > localItemsRange.getFirst()) && (this.firstItem <= localItemsRange.getLast())) {
      i = this.firstItem - 1;
    }
    while ((i >= localItemsRange.getFirst()) && (addViewItem(i, true)))
    {
      this.firstItem = i;
      i--;
      continue;
      this.firstItem = localItemsRange.getFirst();
    }
    int j = this.firstItem;
    int i = this.itemsLayout.getChildCount();
    while (i < localItemsRange.getCount())
    {
      int k = j;
      if (!addViewItem(this.firstItem + i, false))
      {
        k = j;
        if (this.itemsLayout.getChildCount() == 0) {
          k = j + 1;
        }
      }
      i++;
      j = k;
    }
    this.firstItem = j;
    return bool2;
  }
  
  private void updateView()
  {
    if (rebuildItems())
    {
      calculateLayoutWidth(getWidth(), 1073741824);
      layout(getWidth(), getHeight());
    }
  }
  
  public void addChangingListener(OnWheelChangedListener paramOnWheelChangedListener)
  {
    this.changingListeners.add(paramOnWheelChangedListener);
  }
  
  public void addClickingListener(OnWheelClickedListener paramOnWheelClickedListener)
  {
    this.clickingListeners.add(paramOnWheelClickedListener);
  }
  
  public void addScrollingListener(OnWheelScrollListener paramOnWheelScrollListener)
  {
    this.scrollingListeners.add(paramOnWheelScrollListener);
  }
  
  public boolean drawShadows()
  {
    return this.drawShadows;
  }
  
  public int getCurrentItem()
  {
    return this.currentItem;
  }
  
  public String getLineColorStr()
  {
    String str2 = this.lineColorStr;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public int getLineWidth()
  {
    return this.lineWidth;
  }
  
  public WheelViewAdapter getViewAdapter()
  {
    return this.viewAdapter;
  }
  
  public int getVisibleItems()
  {
    return this.visibleItems;
  }
  
  public void invalidateWheel(boolean paramBoolean)
  {
    LinearLayout localLinearLayout;
    if (paramBoolean)
    {
      this.recycle.clearAll();
      localLinearLayout = this.itemsLayout;
      if (localLinearLayout != null) {
        localLinearLayout.removeAllViews();
      }
      this.scrollingOffset = 0;
    }
    else
    {
      localLinearLayout = this.itemsLayout;
      if (localLinearLayout != null) {
        this.recycle.recycleItems(localLinearLayout, this.firstItem, new ItemsRange());
      }
    }
    invalidate();
  }
  
  public boolean isCyclic()
  {
    return this.isCyclic;
  }
  
  protected void notifyChangingListeners(int paramInt1, int paramInt2)
  {
    Iterator localIterator = this.changingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelChangedListener)localIterator.next()).onChanged(this, paramInt1, paramInt2);
    }
  }
  
  protected void notifyClickListenersAboutClick(int paramInt)
  {
    Iterator localIterator = this.clickingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelClickedListener)localIterator.next()).onItemClicked(this, paramInt);
    }
  }
  
  protected void notifyScrollingListenersAboutEnd()
  {
    Iterator localIterator = this.scrollingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelScrollListener)localIterator.next()).onScrollingFinished(this);
    }
  }
  
  protected void notifyScrollingListenersAboutStart()
  {
    Iterator localIterator = this.scrollingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelScrollListener)localIterator.next()).onScrollingStarted(this);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() > 0))
    {
      updateView();
      drawItems(paramCanvas);
      drawCenterRect(paramCanvas);
    }
    if (this.drawShadows) {
      drawShadows(paramCanvas);
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    layout(paramInt3 - paramInt1, paramInt4 - paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.getMode(paramInt1);
    int i = View.MeasureSpec.getMode(paramInt2);
    int k = View.MeasureSpec.getSize(paramInt1);
    paramInt1 = View.MeasureSpec.getSize(paramInt2);
    buildViewForMeasuring();
    j = calculateLayoutWidth(k, j);
    if (i != 1073741824)
    {
      paramInt2 = getDesiredHeight(this.itemsLayout);
      if (i == Integer.MIN_VALUE) {
        paramInt1 = Math.min(paramInt2, paramInt1);
      } else {
        paramInt1 = paramInt2;
      }
    }
    setMeasuredDimension(j, paramInt1);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((isEnabled()) && (getViewAdapter() != null))
    {
      switch (paramMotionEvent.getAction())
      {
      default: 
        break;
      case 2: 
        if (getParent() != null) {
          getParent().requestDisallowInterceptTouchEvent(true);
        }
        break;
      case 1: 
        if (!this.isScrollingPerformed)
        {
          int i = (int)paramMotionEvent.getY() - getHeight() / 2;
          if (i > 0) {
            i += getItemHeight() / 2;
          } else {
            i -= getItemHeight() / 2;
          }
          i /= getItemHeight();
          if ((i != 0) && (isValidItemIndex(this.currentItem + i))) {
            notifyClickListenersAboutClick(this.currentItem + i);
          }
        }
        break;
      }
      return this.scroller.onTouchEvent(paramMotionEvent);
    }
    return true;
  }
  
  public void removeChangingListener(OnWheelChangedListener paramOnWheelChangedListener)
  {
    this.changingListeners.remove(paramOnWheelChangedListener);
  }
  
  public void removeClickingListener(OnWheelClickedListener paramOnWheelClickedListener)
  {
    this.clickingListeners.remove(paramOnWheelClickedListener);
  }
  
  public void removeScrollingListener(OnWheelScrollListener paramOnWheelScrollListener)
  {
    this.scrollingListeners.remove(paramOnWheelScrollListener);
  }
  
  public void scroll(int paramInt1, int paramInt2)
  {
    int j = getItemHeight();
    int i = this.scrollingOffset;
    this.scroller.scroll(paramInt1 * j - i, paramInt2);
  }
  
  public void setCurrentItem(int paramInt)
  {
    setCurrentItem(paramInt, false);
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() != 0))
    {
      int k = this.viewAdapter.getItemsCount();
      int i;
      if (paramInt >= 0)
      {
        i = paramInt;
        if (paramInt < k) {}
      }
      else
      {
        if (!this.isCyclic) {
          break label181;
        }
        while (paramInt < 0) {
          paramInt += k;
        }
        i = paramInt % k;
      }
      int m = this.currentItem;
      if (i != m) {
        if (paramBoolean)
        {
          int j = i - m;
          paramInt = j;
          if (this.isCyclic)
          {
            i = k + Math.min(i, m) - Math.max(i, this.currentItem);
            paramInt = j;
            if (i < Math.abs(j)) {
              if (j < 0) {
                paramInt = i;
              } else {
                paramInt = -i;
              }
            }
          }
          scroll(paramInt, 0);
        }
        else
        {
          this.scrollingOffset = 0;
          this.currentItem = i;
          notifyChangingListeners(m, this.currentItem);
          invalidate();
        }
      }
      return;
      label181:
      return;
    }
  }
  
  public void setCyclic(boolean paramBoolean)
  {
    this.isCyclic = paramBoolean;
    invalidateWheel(false);
  }
  
  public void setDrawShadows(boolean paramBoolean)
  {
    this.drawShadows = paramBoolean;
  }
  
  public void setInterpolator(Interpolator paramInterpolator)
  {
    this.scroller.setInterpolator(paramInterpolator);
  }
  
  public void setLineColorStr(String paramString)
  {
    this.lineColorStr = paramString;
  }
  
  public void setLineWidth(int paramInt)
  {
    this.lineWidth = paramInt;
  }
  
  public void setShadowColor(int paramInt1, int paramInt2, int paramInt3)
  {
    this.SHADOWS_COLORS = new int[] { paramInt1, paramInt2, paramInt3 };
  }
  
  public void setViewAdapter(WheelViewAdapter paramWheelViewAdapter)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if (localWheelViewAdapter != null) {
      localWheelViewAdapter.unregisterDataSetObserver(this.dataObserver);
    }
    this.viewAdapter = paramWheelViewAdapter;
    paramWheelViewAdapter = this.viewAdapter;
    if (paramWheelViewAdapter != null) {
      paramWheelViewAdapter.registerDataSetObserver(this.dataObserver);
    }
    invalidateWheel(true);
  }
  
  public void setVisibleItems(int paramInt)
  {
    this.visibleItems = paramInt;
  }
  
  public void setWheelBackground(int paramInt)
  {
    this.wheelBackground = paramInt;
    setBackgroundResource(this.wheelBackground);
  }
  
  public void setWheelForeground(int paramInt)
  {
    this.wheelForeground = paramInt;
    this.centerDrawable = getContext().getResources().getDrawable(this.wheelForeground);
  }
  
  public void stopScrolling()
  {
    this.scroller.stopScrolling();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/WheelView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */