package com.lljjcoder.style.citypickerview.widget.wheel.adapters;

import android.content.Context;
import com.lljjcoder.style.citypickerview.widget.wheel.WheelAdapter;

public class AdapterWheel
  extends AbstractWheelTextAdapter
{
  private WheelAdapter adapter;
  
  public AdapterWheel(Context paramContext, WheelAdapter paramWheelAdapter)
  {
    super(paramContext);
    this.adapter = paramWheelAdapter;
  }
  
  public WheelAdapter getAdapter()
  {
    return this.adapter;
  }
  
  protected CharSequence getItemText(int paramInt)
  {
    return this.adapter.getItem(paramInt);
  }
  
  public int getItemsCount()
  {
    return this.adapter.getItemsCount();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/adapters/AdapterWheel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */