package com.lljjcoder.style.citypickerview.widget.wheel;

public abstract interface OnWheelClickedListener
{
  public abstract void onItemClicked(WheelView paramWheelView, int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/OnWheelClickedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */