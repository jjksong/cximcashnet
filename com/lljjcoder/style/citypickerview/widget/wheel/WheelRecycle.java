package com.lljjcoder.style.citypickerview.widget.wheel;

import android.view.View;
import android.widget.LinearLayout;
import com.lljjcoder.style.citypickerview.widget.wheel.adapters.WheelViewAdapter;
import java.util.LinkedList;
import java.util.List;

public class WheelRecycle
{
  private List<View> emptyItems;
  private List<View> items;
  private WheelView wheel;
  
  public WheelRecycle(WheelView paramWheelView)
  {
    this.wheel = paramWheelView;
  }
  
  private List<View> addView(View paramView, List<View> paramList)
  {
    Object localObject = paramList;
    if (paramList == null) {
      localObject = new LinkedList();
    }
    ((List)localObject).add(paramView);
    return (List<View>)localObject;
  }
  
  private View getCachedView(List<View> paramList)
  {
    if ((paramList != null) && (paramList.size() > 0))
    {
      View localView = (View)paramList.get(0);
      paramList.remove(0);
      return localView;
    }
    return null;
  }
  
  private void recycleView(View paramView, int paramInt)
  {
    int j = this.wheel.getViewAdapter().getItemsCount();
    int i;
    if (paramInt >= 0)
    {
      i = paramInt;
      if (paramInt < j) {}
    }
    else
    {
      i = paramInt;
      if (!this.wheel.isCyclic())
      {
        this.emptyItems = addView(paramView, this.emptyItems);
        return;
      }
    }
    while (i < 0) {
      i += j;
    }
    this.items = addView(paramView, this.items);
  }
  
  public void clearAll()
  {
    List localList = this.items;
    if (localList != null) {
      localList.clear();
    }
    localList = this.emptyItems;
    if (localList != null) {
      localList.clear();
    }
  }
  
  public View getEmptyItem()
  {
    return getCachedView(this.emptyItems);
  }
  
  public View getItem()
  {
    return getCachedView(this.items);
  }
  
  public int recycleItems(LinearLayout paramLinearLayout, int paramInt, ItemsRange paramItemsRange)
  {
    int m = 0;
    int i = paramInt;
    while (m < paramLinearLayout.getChildCount())
    {
      int k;
      int j;
      if (!paramItemsRange.contains(i))
      {
        recycleView(paramLinearLayout.getChildAt(m), i);
        paramLinearLayout.removeViewAt(m);
        k = m;
        j = paramInt;
        if (m == 0)
        {
          j = paramInt + 1;
          k = m;
        }
      }
      else
      {
        k = m + 1;
        j = paramInt;
      }
      i++;
      m = k;
      paramInt = j;
    }
    return paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/WheelRecycle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */