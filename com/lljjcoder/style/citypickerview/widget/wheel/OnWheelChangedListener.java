package com.lljjcoder.style.citypickerview.widget.wheel;

public abstract interface OnWheelChangedListener
{
  public abstract void onChanged(WheelView paramWheelView, int paramInt1, int paramInt2);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/wheel/OnWheelChangedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */