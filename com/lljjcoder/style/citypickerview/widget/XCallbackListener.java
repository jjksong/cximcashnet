package com.lljjcoder.style.citypickerview.widget;

public abstract class XCallbackListener
{
  public void call(Object... paramVarArgs)
  {
    callback(paramVarArgs);
  }
  
  protected abstract void callback(Object... paramVarArgs);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citypickerview/widget/XCallbackListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */