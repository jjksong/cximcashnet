package com.lljjcoder.style.citythreelist;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CityBean
  implements Parcelable
{
  public static final Parcelable.Creator<CityBean> CREATOR = new Parcelable.Creator()
  {
    public CityBean createFromParcel(Parcel paramAnonymousParcel)
    {
      return new CityBean(paramAnonymousParcel);
    }
    
    public CityBean[] newArray(int paramAnonymousInt)
    {
      return new CityBean[paramAnonymousInt];
    }
  };
  private String id;
  private String name;
  
  public CityBean() {}
  
  protected CityBean(Parcel paramParcel)
  {
    this.id = paramParcel.readString();
    this.name = paramParcel.readString();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String getId()
  {
    String str2 = this.id;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getName()
  {
    String str2 = this.name;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.id);
    paramParcel.writeString(this.name);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citythreelist/CityBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */