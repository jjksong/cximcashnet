package com.lljjcoder.style.citythreelist;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import java.util.ArrayList;
import java.util.List;

public class CityAdapter
  extends RecyclerView.Adapter<MyViewHolder>
{
  List<CityInfoBean> cityList = new ArrayList();
  Context context;
  private OnItemSelectedListener mOnItemClickListener;
  
  public CityAdapter(Context paramContext, List<CityInfoBean> paramList)
  {
    this.cityList = paramList;
    this.context = paramContext;
  }
  
  public int getItemCount()
  {
    return this.cityList.size();
  }
  
  public void onBindViewHolder(MyViewHolder paramMyViewHolder, final int paramInt)
  {
    paramMyViewHolder.tv.setText(((CityInfoBean)this.cityList.get(paramInt)).getName());
    paramMyViewHolder.tv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if ((CityAdapter.this.mOnItemClickListener != null) && (paramInt < CityAdapter.this.cityList.size())) {
          CityAdapter.this.mOnItemClickListener.onItemSelected(paramAnonymousView, paramInt);
        }
      }
    });
  }
  
  public MyViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new MyViewHolder(LayoutInflater.from(this.context).inflate(R.layout.item_citylist, paramViewGroup, false));
  }
  
  public void setOnItemClickListener(OnItemSelectedListener paramOnItemSelectedListener)
  {
    this.mOnItemClickListener = paramOnItemSelectedListener;
  }
  
  class MyViewHolder
    extends RecyclerView.ViewHolder
  {
    TextView tv;
    
    public MyViewHolder(View paramView)
    {
      super();
      this.tv = ((TextView)paramView.findViewById(R.id.default_item_city_name_tv));
    }
  }
  
  public static abstract interface OnItemSelectedListener
  {
    public abstract void onItemSelected(View paramView, int paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citythreelist/CityAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */