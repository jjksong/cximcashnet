package com.lljjcoder.style.citythreelist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import com.lljjcoder.widget.RecycleViewDividerForList;
import java.util.ArrayList;
import java.util.List;

public class CityActivity
  extends Activity
{
  private CityBean area = new CityBean();
  private CityBean cityBean = new CityBean();
  private String cityName = "";
  private TextView mCityNameTv;
  private RecyclerView mCityRecyclerView;
  private ImageView mImgBack;
  private CityInfoBean mProInfo = null;
  
  private void initView()
  {
    this.mImgBack = ((ImageView)findViewById(R.id.img_left));
    this.mCityNameTv = ((TextView)findViewById(R.id.cityname_tv));
    this.mImgBack.setVisibility(0);
    this.mImgBack.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CityActivity.this.finish();
      }
    });
    this.mCityNameTv = ((TextView)findViewById(R.id.cityname_tv));
    this.mCityRecyclerView = ((RecyclerView)findViewById(R.id.city_recyclerview));
    this.mCityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    this.mCityRecyclerView.addItemDecoration(new RecycleViewDividerForList(this, 0, true));
  }
  
  private void setData(CityInfoBean paramCityInfoBean)
  {
    if ((paramCityInfoBean != null) && (paramCityInfoBean.getCityList().size() > 0))
    {
      final Object localObject = this.mCityNameTv;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("");
      localStringBuilder.append(paramCityInfoBean.getName());
      ((TextView)localObject).setText(localStringBuilder.toString());
      localObject = paramCityInfoBean.getCityList();
      if (localObject == null) {
        return;
      }
      paramCityInfoBean = new CityAdapter(this, (List)localObject);
      this.mCityRecyclerView.setAdapter(paramCityInfoBean);
      paramCityInfoBean.setOnItemClickListener(new CityAdapter.OnItemSelectedListener()
      {
        public void onItemSelected(View paramAnonymousView, int paramAnonymousInt)
        {
          CityActivity.this.cityBean.setId(((CityInfoBean)localObject.get(paramAnonymousInt)).getId());
          CityActivity.this.cityBean.setName(((CityInfoBean)localObject.get(paramAnonymousInt)).getName());
          paramAnonymousView = new Intent(CityActivity.this, AreaActivity.class);
          paramAnonymousView.putExtra("bundata", (Parcelable)localObject.get(paramAnonymousInt));
          CityActivity.this.startActivityForResult(paramAnonymousView, 1001);
        }
      });
    }
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt2 == 1001) && (paramIntent != null))
    {
      this.area = ((CityBean)paramIntent.getParcelableExtra("area"));
      paramIntent = new Intent();
      paramIntent.putExtra("city", this.cityBean);
      paramIntent.putExtra("area", this.area);
      setResult(-1, paramIntent);
      finish();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_citylist);
    this.mProInfo = ((CityInfoBean)getIntent().getParcelableExtra("bundata"));
    initView();
    setData(this.mProInfo);
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citythreelist/CityActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */