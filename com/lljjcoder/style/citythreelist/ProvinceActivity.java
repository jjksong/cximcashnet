package com.lljjcoder.style.citythreelist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.style.citylist.utils.CityListLoader;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import com.lljjcoder.widget.RecycleViewDividerForList;
import java.util.List;

public class ProvinceActivity
  extends Activity
{
  public static final int RESULT_DATA = 1001;
  private TextView mCityNameTv;
  private RecyclerView mCityRecyclerView;
  private CityBean provinceBean = new CityBean();
  
  private void initView()
  {
    this.mCityNameTv = ((TextView)findViewById(R.id.cityname_tv));
    this.mCityNameTv.setText("选择省份");
    this.mCityRecyclerView = ((RecyclerView)findViewById(R.id.city_recyclerview));
    this.mCityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    this.mCityRecyclerView.addItemDecoration(new RecycleViewDividerForList(this, 0, true));
  }
  
  private void setData()
  {
    final List localList = CityListLoader.getInstance().getProListData();
    if (localList == null) {
      return;
    }
    CityAdapter localCityAdapter = new CityAdapter(this, localList);
    this.mCityRecyclerView.setAdapter(localCityAdapter);
    localCityAdapter.setOnItemClickListener(new CityAdapter.OnItemSelectedListener()
    {
      public void onItemSelected(View paramAnonymousView, int paramAnonymousInt)
      {
        ProvinceActivity.this.provinceBean.setId(((CityInfoBean)localList.get(paramAnonymousInt)).getId());
        ProvinceActivity.this.provinceBean.setName(((CityInfoBean)localList.get(paramAnonymousInt)).getName());
        paramAnonymousView = new Intent(ProvinceActivity.this, CityActivity.class);
        paramAnonymousView.putExtra("bundata", (Parcelable)localList.get(paramAnonymousInt));
        ProvinceActivity.this.startActivityForResult(paramAnonymousView, 1001);
      }
    });
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 1001) && (paramIntent != null))
    {
      CityBean localCityBean1 = (CityBean)paramIntent.getParcelableExtra("area");
      CityBean localCityBean2 = (CityBean)paramIntent.getParcelableExtra("city");
      paramIntent = new Intent();
      paramIntent.putExtra("province", this.provinceBean);
      paramIntent.putExtra("city", localCityBean2);
      paramIntent.putExtra("area", localCityBean1);
      setResult(-1, paramIntent);
      finish();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_citylist);
    initView();
    setData();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citythreelist/ProvinceActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */