package com.lljjcoder.style.citythreelist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.lljjcoder.style.citylist.bean.CityInfoBean;
import com.lljjcoder.style.citypickerview.R.id;
import com.lljjcoder.style.citypickerview.R.layout;
import com.lljjcoder.widget.RecycleViewDividerForList;
import java.util.ArrayList;
import java.util.List;

public class AreaActivity
  extends Activity
{
  private CityBean areaBean = new CityBean();
  private TextView mCityNameTv;
  private RecyclerView mCityRecyclerView;
  private ImageView mImgBack;
  private CityInfoBean mProCityInfo = null;
  
  private void initView()
  {
    this.mImgBack = ((ImageView)findViewById(R.id.img_left));
    this.mCityNameTv = ((TextView)findViewById(R.id.cityname_tv));
    this.mImgBack.setVisibility(0);
    this.mImgBack.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        AreaActivity.this.finish();
      }
    });
    this.mCityRecyclerView = ((RecyclerView)findViewById(R.id.city_recyclerview));
    this.mCityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    this.mCityRecyclerView.addItemDecoration(new RecycleViewDividerForList(this, 0, true));
  }
  
  private void setData()
  {
    Object localObject1 = this.mProCityInfo;
    if ((localObject1 != null) && (((CityInfoBean)localObject1).getCityList().size() > 0))
    {
      final Object localObject2 = this.mCityNameTv;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("");
      ((StringBuilder)localObject1).append(this.mProCityInfo.getName());
      ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
      localObject2 = this.mProCityInfo.getCityList();
      if (localObject2 == null) {
        return;
      }
      localObject1 = new CityAdapter(this, (List)localObject2);
      this.mCityRecyclerView.setAdapter((RecyclerView.Adapter)localObject1);
      ((CityAdapter)localObject1).setOnItemClickListener(new CityAdapter.OnItemSelectedListener()
      {
        public void onItemSelected(View paramAnonymousView, int paramAnonymousInt)
        {
          AreaActivity.this.areaBean.setName(((CityInfoBean)localObject2.get(paramAnonymousInt)).getName());
          AreaActivity.this.areaBean.setId(((CityInfoBean)localObject2.get(paramAnonymousInt)).getId());
          paramAnonymousView = new Intent();
          paramAnonymousView.putExtra("area", AreaActivity.this.areaBean);
          AreaActivity.this.setResult(1001, paramAnonymousView);
          AreaActivity.this.finish();
        }
      });
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_citylist);
    this.mProCityInfo = ((CityInfoBean)getIntent().getParcelableExtra("bundata"));
    initView();
    setData();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/style/citythreelist/AreaActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */