package com.lljjcoder.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public class RecycleViewDividerForList
  extends RecyclerView.ItemDecoration
{
  private static final int[] ATTRS = { 16843284 };
  private Drawable mDivider;
  private int mDividerHeight = 2;
  private boolean mLastLineShow = true;
  private int mOrientation;
  private Paint mPaint;
  
  public RecycleViewDividerForList(Context paramContext, int paramInt)
  {
    if ((paramInt != 1) && (paramInt != 0)) {
      throw new IllegalArgumentException("请输入正确的参数！");
    }
    this.mOrientation = paramInt;
    paramContext = paramContext.obtainStyledAttributes(ATTRS);
    this.mDivider = paramContext.getDrawable(0);
    paramContext.recycle();
  }
  
  public RecycleViewDividerForList(Context paramContext, int paramInt1, int paramInt2)
  {
    this(paramContext, paramInt1);
    this.mDivider = ContextCompat.getDrawable(paramContext, paramInt2);
  }
  
  public RecycleViewDividerForList(Context paramContext, int paramInt1, int paramInt2, int paramInt3)
  {
    this(paramContext, paramInt1);
    this.mPaint = new Paint(1);
    this.mPaint.setColor(paramInt3);
    this.mPaint.setStyle(Paint.Style.FILL);
  }
  
  public RecycleViewDividerForList(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if ((paramInt != 1) && (paramInt != 0)) {
      throw new IllegalArgumentException("请输入正确的参数！");
    }
    this.mOrientation = paramInt;
    this.mLastLineShow = paramBoolean;
    paramContext = paramContext.obtainStyledAttributes(ATTRS);
    this.mDivider = paramContext.getDrawable(0);
    paramContext.recycle();
  }
  
  private void drawHorizontal(Canvas paramCanvas, RecyclerView paramRecyclerView)
  {
    int k = paramRecyclerView.getPaddingLeft();
    int m = paramRecyclerView.getMeasuredWidth() - paramRecyclerView.getPaddingRight();
    int j = paramRecyclerView.getChildCount();
    for (int i = 0; i < j - (this.mLastLineShow ^ true); i++)
    {
      View localView = paramRecyclerView.getChildAt(i);
      Object localObject = (RecyclerView.LayoutParams)localView.getLayoutParams();
      int n = localView.getBottom() + ((RecyclerView.LayoutParams)localObject).bottomMargin;
      int i1 = this.mDividerHeight + n;
      localObject = this.mDivider;
      if (localObject != null)
      {
        ((Drawable)localObject).setBounds(k, n, m, i1);
        this.mDivider.draw(paramCanvas);
      }
      localObject = this.mPaint;
      if (localObject != null) {
        paramCanvas.drawRect(k, n, m, i1, (Paint)localObject);
      }
    }
  }
  
  private void drawVertical(Canvas paramCanvas, RecyclerView paramRecyclerView)
  {
    int m = paramRecyclerView.getPaddingTop();
    int k = paramRecyclerView.getMeasuredHeight() - paramRecyclerView.getPaddingBottom();
    int j = paramRecyclerView.getChildCount();
    for (int i = 0; i < j; i++)
    {
      View localView = paramRecyclerView.getChildAt(i);
      Object localObject = (RecyclerView.LayoutParams)localView.getLayoutParams();
      int n = localView.getRight() + ((RecyclerView.LayoutParams)localObject).rightMargin;
      int i1 = this.mDividerHeight + n;
      localObject = this.mDivider;
      if (localObject != null)
      {
        ((Drawable)localObject).setBounds(n, m, i1, k);
        this.mDivider.draw(paramCanvas);
      }
      localObject = this.mPaint;
      if (localObject != null) {
        paramCanvas.drawRect(n, m, i1, k, (Paint)localObject);
      }
    }
  }
  
  public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    paramRect.set(0, 0, 0, this.mDividerHeight);
  }
  
  public void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.onDraw(paramCanvas, paramRecyclerView, paramState);
    if (this.mOrientation == 1) {
      drawVertical(paramCanvas, paramRecyclerView);
    } else {
      drawHorizontal(paramCanvas, paramRecyclerView);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/widget/RecycleViewDividerForList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */