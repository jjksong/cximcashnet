package com.lljjcoder.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;

public class ProvinceBean
  implements Parcelable
{
  public static final Parcelable.Creator<ProvinceBean> CREATOR = new Parcelable.Creator()
  {
    public ProvinceBean createFromParcel(Parcel paramAnonymousParcel)
    {
      return new ProvinceBean(paramAnonymousParcel);
    }
    
    public ProvinceBean[] newArray(int paramAnonymousInt)
    {
      return new ProvinceBean[paramAnonymousInt];
    }
  };
  private ArrayList<CityBean> cityList;
  private String id;
  private String name;
  
  public ProvinceBean() {}
  
  protected ProvinceBean(Parcel paramParcel)
  {
    this.id = paramParcel.readString();
    this.name = paramParcel.readString();
    this.cityList = paramParcel.createTypedArrayList(CityBean.CREATOR);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public ArrayList<CityBean> getCityList()
  {
    return this.cityList;
  }
  
  public String getId()
  {
    String str2 = this.id;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getName()
  {
    String str2 = this.name;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public void setCityList(ArrayList<CityBean> paramArrayList)
  {
    this.cityList = paramArrayList;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public String toString()
  {
    return this.name;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.id);
    paramParcel.writeString(this.name);
    paramParcel.writeTypedList(this.cityList);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/bean/ProvinceBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */