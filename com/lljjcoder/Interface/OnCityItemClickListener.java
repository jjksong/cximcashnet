package com.lljjcoder.Interface;

import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;

public abstract class OnCityItemClickListener
{
  public void onCancel() {}
  
  public void onSelected(ProvinceBean paramProvinceBean, CityBean paramCityBean, DistrictBean paramDistrictBean) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/Interface/OnCityItemClickListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */