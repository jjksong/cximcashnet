package com.lljjcoder.citywheel;

public class CityConfig
{
  public static final Integer NONE = Integer.valueOf(64425);
  private String cancelText = "取消";
  private String cancelTextColorStr = "#000000";
  private int cancelTextSize = 16;
  private String confirmText = "确定";
  private String confirmTextColorStr = "#0000FF";
  private int confirmTextSize = 16;
  private Integer customItemLayout;
  private Integer customItemTextViewId;
  private String defaultCityName = "杭州";
  private String defaultDistrict = "滨江区";
  private String defaultProvinceName = "浙江";
  private boolean drawShadows = true;
  private boolean isCityCyclic = true;
  private boolean isDistrictCyclic = true;
  private boolean isProvinceCyclic = true;
  private boolean isShowBackground = true;
  private String lineColor = "#C7C7C7";
  private int lineHeigh = 3;
  private String mTitle = "选择地区";
  public WheelType mWheelType = WheelType.PRO_CITY_DIS;
  private boolean showGAT = false;
  private String titleBackgroundColorStr = "#E9E9E9";
  private String titleTextColorStr = "#585858";
  private int titleTextSize = 18;
  private int visibleItems = 5;
  
  public CityConfig(Builder paramBuilder)
  {
    this.titleBackgroundColorStr = paramBuilder.titleBackgroundColorStr;
    this.mTitle = paramBuilder.mTitle;
    this.titleTextColorStr = paramBuilder.titleTextColorStr;
    this.titleTextSize = paramBuilder.titleTextSize;
    this.cancelTextColorStr = paramBuilder.cancelTextColorStr;
    this.cancelText = paramBuilder.cancelText;
    this.cancelTextSize = paramBuilder.cancelTextSize;
    this.confirmTextColorStr = paramBuilder.confirmTextColorStr;
    this.confirmText = paramBuilder.confirmText;
    this.confirmTextSize = paramBuilder.confirmTextSize;
    this.visibleItems = paramBuilder.visibleItems;
    this.isProvinceCyclic = paramBuilder.isProvinceCyclic;
    this.isDistrictCyclic = paramBuilder.isDistrictCyclic;
    this.isCityCyclic = paramBuilder.isCityCyclic;
    this.defaultDistrict = paramBuilder.defaultDistrict;
    this.defaultCityName = paramBuilder.defaultCityName;
    this.defaultProvinceName = paramBuilder.defaultProvinceName;
    this.mWheelType = paramBuilder.mWheelType;
    this.isShowBackground = paramBuilder.isShowBackground;
    this.customItemLayout = paramBuilder.customItemLayout;
    this.customItemTextViewId = paramBuilder.customItemTextViewId;
    this.drawShadows = paramBuilder.drawShadows;
    this.lineColor = paramBuilder.lineColor;
    this.lineHeigh = paramBuilder.lineHeigh;
    this.showGAT = paramBuilder.showGAT;
  }
  
  public String getCancelText()
  {
    String str2 = this.cancelText;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getCancelTextColorStr()
  {
    String str2 = this.cancelTextColorStr;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public int getCancelTextSize()
  {
    return this.cancelTextSize;
  }
  
  public String getConfirmText()
  {
    String str2 = this.confirmText;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getConfirmTextColorStr()
  {
    String str2 = this.confirmTextColorStr;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public int getConfirmTextSize()
  {
    return this.confirmTextSize;
  }
  
  public Integer getCustomItemLayout()
  {
    Integer localInteger2 = this.customItemLayout;
    Integer localInteger1 = localInteger2;
    if (localInteger2 == null) {
      localInteger1 = NONE;
    }
    return localInteger1;
  }
  
  public Integer getCustomItemTextViewId()
  {
    Integer localInteger2 = this.customItemTextViewId;
    Integer localInteger1 = localInteger2;
    if (localInteger2 == null) {
      localInteger1 = NONE;
    }
    return localInteger1;
  }
  
  public String getDefaultCityName()
  {
    String str2 = this.defaultCityName;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getDefaultDistrict()
  {
    String str2 = this.defaultDistrict;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getDefaultProvinceName()
  {
    String str2 = this.defaultProvinceName;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getLineColor()
  {
    String str2 = this.lineColor;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public int getLineHeigh()
  {
    return this.lineHeigh;
  }
  
  public String getTitle()
  {
    String str2 = this.mTitle;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getTitleBackgroundColorStr()
  {
    String str2 = this.titleBackgroundColorStr;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public String getTitleTextColorStr()
  {
    String str2 = this.titleTextColorStr;
    String str1 = str2;
    if (str2 == null) {
      str1 = "";
    }
    return str1;
  }
  
  public int getTitleTextSize()
  {
    return this.titleTextSize;
  }
  
  public int getVisibleItems()
  {
    return this.visibleItems;
  }
  
  public WheelType getWheelType()
  {
    return this.mWheelType;
  }
  
  public boolean isCityCyclic()
  {
    return this.isCityCyclic;
  }
  
  public boolean isDistrictCyclic()
  {
    return this.isDistrictCyclic;
  }
  
  public boolean isDrawShadows()
  {
    return this.drawShadows;
  }
  
  public boolean isProvinceCyclic()
  {
    return this.isProvinceCyclic;
  }
  
  public boolean isShowBackground()
  {
    return this.isShowBackground;
  }
  
  public boolean isShowGAT()
  {
    return this.showGAT;
  }
  
  public void setCancelText(String paramString)
  {
    this.cancelText = paramString;
  }
  
  public void setCancelTextColorStr(String paramString)
  {
    this.cancelTextColorStr = paramString;
  }
  
  public void setCancelTextSize(int paramInt)
  {
    this.cancelTextSize = paramInt;
  }
  
  public void setCityCyclic(boolean paramBoolean)
  {
    this.isCityCyclic = paramBoolean;
  }
  
  public void setConfirmText(String paramString)
  {
    this.confirmText = paramString;
  }
  
  public void setConfirmTextColorStr(String paramString)
  {
    this.confirmTextColorStr = paramString;
  }
  
  public void setConfirmTextSize(int paramInt)
  {
    this.confirmTextSize = paramInt;
  }
  
  public void setCustomItemLayout(int paramInt)
  {
    this.customItemLayout = Integer.valueOf(paramInt);
  }
  
  public void setCustomItemTextViewId(Integer paramInteger)
  {
    this.customItemTextViewId = paramInteger;
  }
  
  public void setDefaultCityName(String paramString)
  {
    this.defaultCityName = paramString;
  }
  
  public void setDefaultDistrict(String paramString)
  {
    this.defaultDistrict = paramString;
  }
  
  public void setDefaultProvinceName(String paramString)
  {
    this.defaultProvinceName = paramString;
  }
  
  public void setDistrictCyclic(boolean paramBoolean)
  {
    this.isDistrictCyclic = paramBoolean;
  }
  
  public void setDrawShadows(boolean paramBoolean)
  {
    this.drawShadows = paramBoolean;
  }
  
  public void setLineColor(String paramString)
  {
    this.lineColor = paramString;
  }
  
  public void setLineHeigh(int paramInt)
  {
    this.lineHeigh = paramInt;
  }
  
  public void setProvinceCyclic(boolean paramBoolean)
  {
    this.isProvinceCyclic = paramBoolean;
  }
  
  public void setShowBackground(boolean paramBoolean)
  {
    this.isShowBackground = paramBoolean;
  }
  
  public void setShowGAT(boolean paramBoolean)
  {
    this.showGAT = paramBoolean;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setTitleBackgroundColorStr(String paramString)
  {
    this.titleBackgroundColorStr = paramString;
  }
  
  public void setTitleTextColorStr(String paramString)
  {
    this.titleTextColorStr = paramString;
  }
  
  public void setTitleTextSize(int paramInt)
  {
    this.titleTextSize = paramInt;
  }
  
  public void setVisibleItems(int paramInt)
  {
    this.visibleItems = paramInt;
  }
  
  public static class Builder
  {
    private String cancelText = "取消";
    private String cancelTextColorStr = "#000000";
    private int cancelTextSize = 16;
    private String confirmText = "确定";
    private String confirmTextColorStr = "#0000FF";
    private int confirmTextSize = 16;
    private Integer customItemLayout;
    private Integer customItemTextViewId;
    private String defaultCityName = "杭州";
    private String defaultDistrict = "滨江区";
    private String defaultProvinceName = "浙江";
    private boolean drawShadows = true;
    private boolean isCityCyclic = true;
    private boolean isDistrictCyclic = true;
    private boolean isProvinceCyclic = true;
    private boolean isShowBackground = true;
    private String lineColor = "#C7C7C7";
    private int lineHeigh = 3;
    private String mTitle = "选择地区";
    private CityConfig.WheelType mWheelType = CityConfig.WheelType.PRO_CITY_DIS;
    private boolean showGAT = false;
    private String titleBackgroundColorStr = "#E9E9E9";
    private String titleTextColorStr = "#585858";
    private int titleTextSize = 18;
    private int visibleItems = 5;
    
    public CityConfig build()
    {
      return new CityConfig(this);
    }
    
    public Builder cancelText(String paramString)
    {
      this.cancelText = paramString;
      return this;
    }
    
    public Builder cancelTextColor(String paramString)
    {
      this.cancelTextColorStr = paramString;
      return this;
    }
    
    public Builder cancelTextSize(int paramInt)
    {
      this.cancelTextSize = paramInt;
      return this;
    }
    
    public Builder city(String paramString)
    {
      this.defaultCityName = paramString;
      return this;
    }
    
    public Builder cityCyclic(boolean paramBoolean)
    {
      this.isCityCyclic = paramBoolean;
      return this;
    }
    
    public Builder confirTextColor(String paramString)
    {
      this.confirmTextColorStr = paramString;
      return this;
    }
    
    public Builder confirmText(String paramString)
    {
      this.confirmText = paramString;
      return this;
    }
    
    public Builder confirmTextSize(int paramInt)
    {
      this.confirmTextSize = paramInt;
      return this;
    }
    
    public Builder district(String paramString)
    {
      this.defaultDistrict = paramString;
      return this;
    }
    
    public Builder districtCyclic(boolean paramBoolean)
    {
      this.isDistrictCyclic = paramBoolean;
      return this;
    }
    
    public Builder drawShadows(boolean paramBoolean)
    {
      this.drawShadows = paramBoolean;
      return this;
    }
    
    public Builder province(String paramString)
    {
      this.defaultProvinceName = paramString;
      return this;
    }
    
    public Builder provinceCyclic(boolean paramBoolean)
    {
      this.isProvinceCyclic = paramBoolean;
      return this;
    }
    
    public Builder setCityWheelType(CityConfig.WheelType paramWheelType)
    {
      this.mWheelType = paramWheelType;
      return this;
    }
    
    public Builder setCustomItemLayout(Integer paramInteger)
    {
      this.customItemLayout = paramInteger;
      return this;
    }
    
    public Builder setCustomItemTextViewId(Integer paramInteger)
    {
      this.customItemTextViewId = paramInteger;
      return this;
    }
    
    public Builder setLineColor(String paramString)
    {
      this.lineColor = paramString;
      return this;
    }
    
    public Builder setLineHeigh(int paramInt)
    {
      this.lineHeigh = paramInt;
      return this;
    }
    
    public Builder setShowGAT(boolean paramBoolean)
    {
      this.showGAT = paramBoolean;
      return this;
    }
    
    public Builder showBackground(boolean paramBoolean)
    {
      this.isShowBackground = paramBoolean;
      return this;
    }
    
    public Builder title(String paramString)
    {
      this.mTitle = paramString;
      return this;
    }
    
    public Builder titleBackgroundColor(String paramString)
    {
      this.titleBackgroundColorStr = paramString;
      return this;
    }
    
    public Builder titleTextColor(String paramString)
    {
      this.titleTextColorStr = paramString;
      return this;
    }
    
    public Builder titleTextSize(int paramInt)
    {
      this.titleTextSize = paramInt;
      return this;
    }
    
    public Builder visibleItemsCount(int paramInt)
    {
      this.visibleItems = paramInt;
      return this;
    }
  }
  
  public static enum WheelType
  {
    PRO,  PRO_CITY,  PRO_CITY_DIS;
    
    private WheelType() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/citywheel/CityConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */