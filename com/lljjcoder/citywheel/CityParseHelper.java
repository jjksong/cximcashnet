package com.lljjcoder.citywheel;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.utils.utils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CityParseHelper
{
  private CityBean mCityBean;
  private ArrayList<ArrayList<CityBean>> mCityBeanArrayList;
  private Map<String, DistrictBean[]> mCity_DisMap = new HashMap();
  private Map<String, DistrictBean> mDisMap = new HashMap();
  private DistrictBean mDistrictBean;
  private ArrayList<ArrayList<ArrayList<DistrictBean>>> mDistrictBeanArrayList;
  private Map<String, CityBean[]> mPro_CityMap = new HashMap();
  private ProvinceBean mProvinceBean;
  private ArrayList<ProvinceBean> mProvinceBeanArrayList = new ArrayList();
  private ProvinceBean[] mProvinceBeenArray;
  
  public CityBean getCityBean()
  {
    return this.mCityBean;
  }
  
  public ArrayList<ArrayList<CityBean>> getCityBeanArrayList()
  {
    return this.mCityBeanArrayList;
  }
  
  public Map<String, DistrictBean[]> getCity_DisMap()
  {
    return this.mCity_DisMap;
  }
  
  public Map<String, DistrictBean> getDisMap()
  {
    return this.mDisMap;
  }
  
  public DistrictBean getDistrictBean()
  {
    return this.mDistrictBean;
  }
  
  public ArrayList<ArrayList<ArrayList<DistrictBean>>> getDistrictBeanArrayList()
  {
    return this.mDistrictBeanArrayList;
  }
  
  public Map<String, CityBean[]> getPro_CityMap()
  {
    return this.mPro_CityMap;
  }
  
  public ProvinceBean getProvinceBean()
  {
    return this.mProvinceBean;
  }
  
  public ArrayList<ProvinceBean> getProvinceBeanArrayList()
  {
    return this.mProvinceBeanArrayList;
  }
  
  public ProvinceBean[] getProvinceBeenArray()
  {
    return this.mProvinceBeenArray;
  }
  
  public void initData(Context paramContext)
  {
    paramContext = utils.getJson(paramContext, "china_city_data.json");
    Object localObject1 = new TypeToken() {}.getType();
    this.mProvinceBeanArrayList = ((ArrayList)new Gson().fromJson(paramContext, (Type)localObject1));
    paramContext = this.mProvinceBeanArrayList;
    if ((paramContext != null) && (!paramContext.isEmpty()))
    {
      this.mCityBeanArrayList = new ArrayList(this.mProvinceBeanArrayList.size());
      this.mDistrictBeanArrayList = new ArrayList(this.mProvinceBeanArrayList.size());
      paramContext = this.mProvinceBeanArrayList;
      if ((paramContext != null) && (!paramContext.isEmpty()))
      {
        this.mProvinceBean = ((ProvinceBean)this.mProvinceBeanArrayList.get(0));
        paramContext = this.mProvinceBean.getCityList();
        if ((paramContext != null) && (!paramContext.isEmpty()) && (paramContext.size() > 0))
        {
          this.mCityBean = ((CityBean)paramContext.get(0));
          paramContext = this.mCityBean.getCityList();
          if ((paramContext != null) && (!paramContext.isEmpty()) && (paramContext.size() > 0)) {
            this.mDistrictBean = ((DistrictBean)paramContext.get(0));
          }
        }
      }
      this.mProvinceBeenArray = new ProvinceBean[this.mProvinceBeanArrayList.size()];
      for (int i = 0; i < this.mProvinceBeanArrayList.size(); i++)
      {
        localObject1 = (ProvinceBean)this.mProvinceBeanArrayList.get(i);
        paramContext = ((ProvinceBean)localObject1).getCityList();
        CityBean[] arrayOfCityBean = new CityBean[paramContext.size()];
        for (int j = 0; j < paramContext.size(); j++)
        {
          arrayOfCityBean[j] = ((CityBean)paramContext.get(j));
          ArrayList localArrayList = ((CityBean)paramContext.get(j)).getCityList();
          if (localArrayList == null) {
            break;
          }
          localObject2 = new DistrictBean[localArrayList.size()];
          for (int k = 0; k < localArrayList.size(); k++)
          {
            DistrictBean localDistrictBean = (DistrictBean)localArrayList.get(k);
            localObject4 = this.mDisMap;
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append(((ProvinceBean)localObject1).getName());
            ((StringBuilder)localObject3).append(arrayOfCityBean[j].getName());
            ((StringBuilder)localObject3).append(((DistrictBean)localArrayList.get(k)).getName());
            ((Map)localObject4).put(((StringBuilder)localObject3).toString(), localDistrictBean);
            localObject2[k] = localDistrictBean;
          }
          Object localObject3 = this.mCity_DisMap;
          Object localObject4 = new StringBuilder();
          ((StringBuilder)localObject4).append(((ProvinceBean)localObject1).getName());
          ((StringBuilder)localObject4).append(arrayOfCityBean[j].getName());
          ((Map)localObject3).put(((StringBuilder)localObject4).toString(), localObject2);
        }
        this.mPro_CityMap.put(((ProvinceBean)localObject1).getName(), arrayOfCityBean);
        this.mCityBeanArrayList.add(paramContext);
        Object localObject2 = new ArrayList(paramContext.size());
        for (j = 0; j < paramContext.size(); j++) {
          ((ArrayList)localObject2).add(((CityBean)paramContext.get(j)).getCityList());
        }
        this.mDistrictBeanArrayList.add(localObject2);
        this.mProvinceBeenArray[i] = localObject1;
      }
      return;
    }
  }
  
  public void setCityBean(CityBean paramCityBean)
  {
    this.mCityBean = paramCityBean;
  }
  
  public void setCityBeanArrayList(ArrayList<ArrayList<CityBean>> paramArrayList)
  {
    this.mCityBeanArrayList = paramArrayList;
  }
  
  public void setCity_DisMap(Map<String, DistrictBean[]> paramMap)
  {
    this.mCity_DisMap = paramMap;
  }
  
  public void setDisMap(Map<String, DistrictBean> paramMap)
  {
    this.mDisMap = paramMap;
  }
  
  public void setDistrictBean(DistrictBean paramDistrictBean)
  {
    this.mDistrictBean = paramDistrictBean;
  }
  
  public void setDistrictBeanArrayList(ArrayList<ArrayList<ArrayList<DistrictBean>>> paramArrayList)
  {
    this.mDistrictBeanArrayList = paramArrayList;
  }
  
  public void setPro_CityMap(Map<String, CityBean[]> paramMap)
  {
    this.mPro_CityMap = paramMap;
  }
  
  public void setProvinceBean(ProvinceBean paramProvinceBean)
  {
    this.mProvinceBean = paramProvinceBean;
  }
  
  public void setProvinceBeanArrayList(ArrayList<ProvinceBean> paramArrayList)
  {
    this.mProvinceBeanArrayList = paramArrayList;
  }
  
  public void setProvinceBeenArray(ProvinceBean[] paramArrayOfProvinceBean)
  {
    this.mProvinceBeenArray = paramArrayOfProvinceBean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/citywheel/CityParseHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */