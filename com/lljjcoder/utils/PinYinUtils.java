package com.lljjcoder.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinYinUtils
{
  private HanyuPinyinOutputFormat format = null;
  private String[] pinyin;
  
  public PinYinUtils()
  {
    this.format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
    this.pinyin = null;
  }
  
  public String getCharPinYin(char paramChar)
  {
    try
    {
      this.pinyin = PinyinHelper.toHanyuPinyinStringArray(paramChar, this.format);
    }
    catch (BadHanyuPinyinOutputFormatCombination localBadHanyuPinyinOutputFormatCombination)
    {
      localBadHanyuPinyinOutputFormatCombination.printStackTrace();
    }
    String[] arrayOfString = this.pinyin;
    if (arrayOfString == null) {
      return null;
    }
    return arrayOfString[0];
  }
  
  public String getStringPinYin(String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; i < paramString.length(); i++)
    {
      String str = getCharPinYin(paramString.charAt(i));
      if (str == null) {
        localStringBuffer.append(paramString.charAt(i));
      } else {
        localStringBuffer.append(str);
      }
    }
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/utils/PinYinUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */