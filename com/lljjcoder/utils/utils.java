package com.lljjcoder.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.Window;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class utils
{
  String cityJsonStr = "";
  
  private static void applyDim(Activity paramActivity, float paramFloat)
  {
    if (Build.VERSION.SDK_INT >= 18)
    {
      paramActivity = (ViewGroup)paramActivity.getWindow().getDecorView().getRootView();
      ColorDrawable localColorDrawable = new ColorDrawable(-16777216);
      localColorDrawable.setBounds(0, 0, paramActivity.getWidth(), paramActivity.getHeight());
      localColorDrawable.setAlpha((int)(paramFloat * 255.0F));
      paramActivity.getOverlay().add(localColorDrawable);
    }
  }
  
  private static void clearDim(Activity paramActivity)
  {
    if (Build.VERSION.SDK_INT >= 18) {
      ((ViewGroup)paramActivity.getWindow().getDecorView().getRootView()).getOverlay().clear();
    }
  }
  
  public static String getJson(Context paramContext, String paramString)
  {
    localStringBuilder = new StringBuilder();
    try
    {
      AssetManager localAssetManager = paramContext.getAssets();
      paramContext = new java/io/BufferedReader;
      InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
      localInputStreamReader.<init>(localAssetManager.open(paramString));
      paramContext.<init>(localInputStreamReader);
      for (;;)
      {
        paramString = paramContext.readLine();
        if (paramString == null) {
          break;
        }
        localStringBuilder.append(paramString);
      }
      return localStringBuilder.toString();
    }
    catch (IOException paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  public static void setBackgroundAlpha(Context paramContext, float paramFloat)
  {
    if (paramFloat == 1.0F) {
      clearDim((Activity)paramContext);
    } else {
      applyDim((Activity)paramContext, paramFloat);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/lljjcoder/utils/utils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */