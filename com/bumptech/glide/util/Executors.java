package com.bumptech.glide.util;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public final class Executors
{
  private static final Executor DIRECT_EXECUTOR = new Executor()
  {
    public void execute(@NonNull Runnable paramAnonymousRunnable)
    {
      paramAnonymousRunnable.run();
    }
  };
  private static final Executor MAIN_THREAD_EXECUTOR = new Executor()
  {
    private final Handler handler = new Handler(Looper.getMainLooper());
    
    public void execute(@NonNull Runnable paramAnonymousRunnable)
    {
      this.handler.post(paramAnonymousRunnable);
    }
  };
  
  public static Executor directExecutor()
  {
    return DIRECT_EXECUTOR;
  }
  
  public static Executor mainThreadExecutor()
  {
    return MAIN_THREAD_EXECUTOR;
  }
  
  @VisibleForTesting
  public static void shutdownAndAwaitTermination(ExecutorService paramExecutorService)
  {
    paramExecutorService.shutdownNow();
    try
    {
      if (!paramExecutorService.awaitTermination(5L, TimeUnit.SECONDS))
      {
        paramExecutorService.shutdownNow();
        if (!paramExecutorService.awaitTermination(5L, TimeUnit.SECONDS))
        {
          RuntimeException localRuntimeException = new java/lang/RuntimeException;
          localRuntimeException.<init>("Failed to shutdown");
          throw localRuntimeException;
        }
      }
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      paramExecutorService.shutdownNow();
      Thread.currentThread().interrupt();
      throw new RuntimeException(localInterruptedException);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/util/Executors.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */