package com.bumptech.glide.util;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.SystemClock;

public final class LogTime
{
  private static final double MILLIS_MULTIPLIER;
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    double d = 1.0D;
    if (i >= 17) {
      d = 1.0D / Math.pow(10.0D, 6.0D);
    }
    MILLIS_MULTIPLIER = d;
  }
  
  public static double getElapsedMillis(long paramLong)
  {
    double d2 = getLogTime() - paramLong;
    double d1 = MILLIS_MULTIPLIER;
    Double.isNaN(d2);
    return d2 * d1;
  }
  
  @TargetApi(17)
  public static long getLogTime()
  {
    if (Build.VERSION.SDK_INT >= 17) {
      return SystemClock.elapsedRealtimeNanos();
    }
    return SystemClock.uptimeMillis();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/util/LogTime.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */