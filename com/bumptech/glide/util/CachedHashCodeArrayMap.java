package com.bumptech.glide.util;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.SimpleArrayMap;

public final class CachedHashCodeArrayMap<K, V>
  extends ArrayMap<K, V>
{
  private int hashCode;
  
  public void clear()
  {
    this.hashCode = 0;
    super.clear();
  }
  
  public int hashCode()
  {
    if (this.hashCode == 0) {
      this.hashCode = super.hashCode();
    }
    return this.hashCode;
  }
  
  public V put(K paramK, V paramV)
  {
    this.hashCode = 0;
    return (V)super.put(paramK, paramV);
  }
  
  public void putAll(SimpleArrayMap<? extends K, ? extends V> paramSimpleArrayMap)
  {
    this.hashCode = 0;
    super.putAll(paramSimpleArrayMap);
  }
  
  public V removeAt(int paramInt)
  {
    this.hashCode = 0;
    return (V)super.removeAt(paramInt);
  }
  
  public V setValueAt(int paramInt, V paramV)
  {
    this.hashCode = 0;
    return (V)super.setValueAt(paramInt, paramV);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/util/CachedHashCodeArrayMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */