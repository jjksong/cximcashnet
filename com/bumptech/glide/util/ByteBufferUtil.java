package com.bumptech.glide.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

public final class ByteBufferUtil
{
  private static final AtomicReference<byte[]> BUFFER_REF = new AtomicReference();
  private static final int BUFFER_SIZE = 16384;
  
  /* Error */
  @NonNull
  public static ByteBuffer fromFile(@NonNull java.io.File paramFile)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: invokevirtual 38	java/io/File:length	()J
    //   7: lstore_1
    //   8: lload_1
    //   9: ldc2_w 39
    //   12: lcmp
    //   13: ifgt +81 -> 94
    //   16: lload_1
    //   17: lconst_0
    //   18: lcmp
    //   19: ifeq +63 -> 82
    //   22: new 42	java/io/RandomAccessFile
    //   25: astore_3
    //   26: aload_3
    //   27: aload_0
    //   28: ldc 44
    //   30: invokespecial 47	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   33: aload_3
    //   34: invokevirtual 51	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   37: astore_0
    //   38: aload_0
    //   39: getstatic 57	java/nio/channels/FileChannel$MapMode:READ_ONLY	Ljava/nio/channels/FileChannel$MapMode;
    //   42: lconst_0
    //   43: lload_1
    //   44: invokevirtual 63	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   47: invokevirtual 69	java/nio/MappedByteBuffer:load	()Ljava/nio/MappedByteBuffer;
    //   50: astore 4
    //   52: aload_0
    //   53: ifnull +7 -> 60
    //   56: aload_0
    //   57: invokevirtual 72	java/nio/channels/FileChannel:close	()V
    //   60: aload_3
    //   61: invokevirtual 73	java/io/RandomAccessFile:close	()V
    //   64: aload 4
    //   66: areturn
    //   67: astore 5
    //   69: aload_0
    //   70: astore 4
    //   72: aload 5
    //   74: astore_0
    //   75: goto +34 -> 109
    //   78: astore_0
    //   79: goto +30 -> 109
    //   82: new 31	java/io/IOException
    //   85: astore_0
    //   86: aload_0
    //   87: ldc 75
    //   89: invokespecial 78	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   92: aload_0
    //   93: athrow
    //   94: new 31	java/io/IOException
    //   97: astore_0
    //   98: aload_0
    //   99: ldc 80
    //   101: invokespecial 78	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   104: aload_0
    //   105: athrow
    //   106: astore_0
    //   107: aconst_null
    //   108: astore_3
    //   109: aload 4
    //   111: ifnull +13 -> 124
    //   114: aload 4
    //   116: invokevirtual 72	java/nio/channels/FileChannel:close	()V
    //   119: goto +5 -> 124
    //   122: astore 4
    //   124: aload_3
    //   125: ifnull +7 -> 132
    //   128: aload_3
    //   129: invokevirtual 73	java/io/RandomAccessFile:close	()V
    //   132: aload_0
    //   133: athrow
    //   134: astore_0
    //   135: goto -75 -> 60
    //   138: astore_0
    //   139: goto -75 -> 64
    //   142: astore_3
    //   143: goto -11 -> 132
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	146	0	paramFile	java.io.File
    //   7	37	1	l	long
    //   25	104	3	localRandomAccessFile	java.io.RandomAccessFile
    //   142	1	3	localIOException1	IOException
    //   1	114	4	localObject1	Object
    //   122	1	4	localIOException2	IOException
    //   67	6	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   38	52	67	finally
    //   33	38	78	finally
    //   3	8	106	finally
    //   22	33	106	finally
    //   82	94	106	finally
    //   94	106	106	finally
    //   114	119	122	java/io/IOException
    //   56	60	134	java/io/IOException
    //   60	64	138	java/io/IOException
    //   128	132	142	java/io/IOException
  }
  
  @NonNull
  public static ByteBuffer fromStream(@NonNull InputStream paramInputStream)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(16384);
    byte[] arrayOfByte2 = (byte[])BUFFER_REF.getAndSet(null);
    byte[] arrayOfByte1 = arrayOfByte2;
    if (arrayOfByte2 == null) {
      arrayOfByte1 = new byte['䀀'];
    }
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte1);
      if (i < 0) {
        break;
      }
      localByteArrayOutputStream.write(arrayOfByte1, 0, i);
    }
    BUFFER_REF.set(arrayOfByte1);
    paramInputStream = localByteArrayOutputStream.toByteArray();
    return (ByteBuffer)ByteBuffer.allocateDirect(paramInputStream.length).put(paramInputStream).position(0);
  }
  
  @Nullable
  private static SafeArray getSafeArray(@NonNull ByteBuffer paramByteBuffer)
  {
    if ((!paramByteBuffer.isReadOnly()) && (paramByteBuffer.hasArray())) {
      return new SafeArray(paramByteBuffer.array(), paramByteBuffer.arrayOffset(), paramByteBuffer.limit());
    }
    return null;
  }
  
  @NonNull
  public static byte[] toBytes(@NonNull ByteBuffer paramByteBuffer)
  {
    Object localObject = getSafeArray(paramByteBuffer);
    if ((localObject != null) && (((SafeArray)localObject).offset == 0) && (((SafeArray)localObject).limit == ((SafeArray)localObject).data.length))
    {
      paramByteBuffer = paramByteBuffer.array();
    }
    else
    {
      localObject = paramByteBuffer.asReadOnlyBuffer();
      paramByteBuffer = new byte[((ByteBuffer)localObject).limit()];
      ((ByteBuffer)localObject).position(0);
      ((ByteBuffer)localObject).get(paramByteBuffer);
    }
    return paramByteBuffer;
  }
  
  /* Error */
  public static void toFile(@NonNull ByteBuffer paramByteBuffer, @NonNull java.io.File paramFile)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: invokevirtual 128	java/nio/ByteBuffer:position	(I)Ljava/nio/Buffer;
    //   5: pop
    //   6: aconst_null
    //   7: astore_2
    //   8: aconst_null
    //   9: astore 4
    //   11: new 42	java/io/RandomAccessFile
    //   14: astore_3
    //   15: aload_3
    //   16: aload_1
    //   17: ldc -83
    //   19: invokespecial 47	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   22: aload 4
    //   24: astore_1
    //   25: aload_3
    //   26: invokevirtual 51	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   29: astore_2
    //   30: aload_2
    //   31: astore_1
    //   32: aload_2
    //   33: aload_0
    //   34: invokevirtual 176	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   37: pop
    //   38: aload_2
    //   39: astore_1
    //   40: aload_2
    //   41: iconst_0
    //   42: invokevirtual 180	java/nio/channels/FileChannel:force	(Z)V
    //   45: aload_2
    //   46: astore_1
    //   47: aload_2
    //   48: invokevirtual 72	java/nio/channels/FileChannel:close	()V
    //   51: aload_2
    //   52: astore_1
    //   53: aload_3
    //   54: invokevirtual 73	java/io/RandomAccessFile:close	()V
    //   57: aload_2
    //   58: ifnull +7 -> 65
    //   61: aload_2
    //   62: invokevirtual 72	java/nio/channels/FileChannel:close	()V
    //   65: aload_3
    //   66: invokevirtual 73	java/io/RandomAccessFile:close	()V
    //   69: return
    //   70: astore_0
    //   71: aload_1
    //   72: astore_2
    //   73: aload_3
    //   74: astore_1
    //   75: goto +6 -> 81
    //   78: astore_0
    //   79: aconst_null
    //   80: astore_1
    //   81: aload_2
    //   82: ifnull +11 -> 93
    //   85: aload_2
    //   86: invokevirtual 72	java/nio/channels/FileChannel:close	()V
    //   89: goto +4 -> 93
    //   92: astore_2
    //   93: aload_1
    //   94: ifnull +7 -> 101
    //   97: aload_1
    //   98: invokevirtual 73	java/io/RandomAccessFile:close	()V
    //   101: aload_0
    //   102: athrow
    //   103: astore_0
    //   104: goto -39 -> 65
    //   107: astore_0
    //   108: goto -39 -> 69
    //   111: astore_1
    //   112: goto -11 -> 101
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	115	0	paramByteBuffer	ByteBuffer
    //   0	115	1	paramFile	java.io.File
    //   7	79	2	localObject1	Object
    //   92	1	2	localIOException	IOException
    //   14	60	3	localRandomAccessFile	java.io.RandomAccessFile
    //   9	14	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   25	30	70	finally
    //   32	38	70	finally
    //   40	45	70	finally
    //   47	51	70	finally
    //   53	57	70	finally
    //   11	22	78	finally
    //   85	89	92	java/io/IOException
    //   61	65	103	java/io/IOException
    //   65	69	107	java/io/IOException
    //   97	101	111	java/io/IOException
  }
  
  @NonNull
  public static InputStream toStream(@NonNull ByteBuffer paramByteBuffer)
  {
    return new ByteBufferStream(paramByteBuffer);
  }
  
  public static void toStream(@NonNull ByteBuffer paramByteBuffer, @NonNull OutputStream paramOutputStream)
    throws IOException
  {
    Object localObject = getSafeArray(paramByteBuffer);
    if (localObject != null)
    {
      paramOutputStream.write(((SafeArray)localObject).data, ((SafeArray)localObject).offset, ((SafeArray)localObject).offset + ((SafeArray)localObject).limit);
    }
    else
    {
      byte[] arrayOfByte = (byte[])BUFFER_REF.getAndSet(null);
      localObject = arrayOfByte;
      if (arrayOfByte == null) {
        localObject = new byte['䀀'];
      }
      while (paramByteBuffer.remaining() > 0)
      {
        int i = Math.min(paramByteBuffer.remaining(), localObject.length);
        paramByteBuffer.get((byte[])localObject, 0, i);
        paramOutputStream.write((byte[])localObject, 0, i);
      }
      BUFFER_REF.set(localObject);
    }
  }
  
  private static class ByteBufferStream
    extends InputStream
  {
    private static final int UNSET = -1;
    @NonNull
    private final ByteBuffer byteBuffer;
    private int markPos = -1;
    
    ByteBufferStream(@NonNull ByteBuffer paramByteBuffer)
    {
      this.byteBuffer = paramByteBuffer;
    }
    
    public int available()
    {
      return this.byteBuffer.remaining();
    }
    
    public void mark(int paramInt)
    {
      try
      {
        this.markPos = this.byteBuffer.position();
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
    
    public boolean markSupported()
    {
      return true;
    }
    
    public int read()
    {
      if (!this.byteBuffer.hasRemaining()) {
        return -1;
      }
      return this.byteBuffer.get();
    }
    
    public int read(@NonNull byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if (!this.byteBuffer.hasRemaining()) {
        return -1;
      }
      paramInt2 = Math.min(paramInt2, available());
      this.byteBuffer.get(paramArrayOfByte, paramInt1, paramInt2);
      return paramInt2;
    }
    
    public void reset()
      throws IOException
    {
      try
      {
        if (this.markPos != -1)
        {
          this.byteBuffer.position(this.markPos);
          return;
        }
        IOException localIOException = new java/io/IOException;
        localIOException.<init>("Cannot reset to unset mark position");
        throw localIOException;
      }
      finally {}
    }
    
    public long skip(long paramLong)
      throws IOException
    {
      if (!this.byteBuffer.hasRemaining()) {
        return -1L;
      }
      paramLong = Math.min(paramLong, available());
      ByteBuffer localByteBuffer = this.byteBuffer;
      localByteBuffer.position((int)(localByteBuffer.position() + paramLong));
      return paramLong;
    }
  }
  
  static final class SafeArray
  {
    final byte[] data;
    final int limit;
    final int offset;
    
    SafeArray(@NonNull byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      this.data = paramArrayOfByte;
      this.offset = paramInt1;
      this.limit = paramInt2;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/util/ByteBufferUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */