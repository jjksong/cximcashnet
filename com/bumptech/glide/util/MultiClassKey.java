package com.bumptech.glide.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class MultiClassKey
{
  private Class<?> first;
  private Class<?> second;
  private Class<?> third;
  
  public MultiClassKey() {}
  
  public MultiClassKey(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2)
  {
    set(paramClass1, paramClass2);
  }
  
  public MultiClassKey(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2, @Nullable Class<?> paramClass3)
  {
    set(paramClass1, paramClass2, paramClass3);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (MultiClassKey)paramObject;
      if (!this.first.equals(((MultiClassKey)paramObject).first)) {
        return false;
      }
      if (!this.second.equals(((MultiClassKey)paramObject).second)) {
        return false;
      }
      return Util.bothNullOrEqual(this.third, ((MultiClassKey)paramObject).third);
    }
    return false;
  }
  
  public int hashCode()
  {
    int k = this.first.hashCode();
    int j = this.second.hashCode();
    Class localClass = this.third;
    int i;
    if (localClass != null) {
      i = localClass.hashCode();
    } else {
      i = 0;
    }
    return (k * 31 + j) * 31 + i;
  }
  
  public void set(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2)
  {
    set(paramClass1, paramClass2, null);
  }
  
  public void set(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2, @Nullable Class<?> paramClass3)
  {
    this.first = paramClass1;
    this.second = paramClass2;
    this.third = paramClass3;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MultiClassKey{first=");
    localStringBuilder.append(this.first);
    localStringBuilder.append(", second=");
    localStringBuilder.append(this.second);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/util/MultiClassKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */