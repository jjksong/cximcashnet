package com.bumptech.glide.gifdecoder;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class StandardGifDecoder
  implements GifDecoder
{
  private static final int BYTES_PER_INTEGER = 4;
  @ColorInt
  private static final int COLOR_TRANSPARENT_BLACK = 0;
  private static final int INITIAL_FRAME_POINTER = -1;
  private static final int MASK_INT_LOWEST_BYTE = 255;
  private static final int MAX_STACK_SIZE = 4096;
  private static final int NULL_CODE = -1;
  private static final String TAG = "StandardGifDecoder";
  @ColorInt
  private int[] act;
  @NonNull
  private Bitmap.Config bitmapConfig = Bitmap.Config.ARGB_8888;
  private final GifDecoder.BitmapProvider bitmapProvider;
  private byte[] block;
  private int downsampledHeight;
  private int downsampledWidth;
  private int framePointer;
  private GifHeader header;
  @Nullable
  private Boolean isFirstFrameTransparent;
  private byte[] mainPixels;
  @ColorInt
  private int[] mainScratch;
  private GifHeaderParser parser;
  @ColorInt
  private final int[] pct = new int['Ā'];
  private byte[] pixelStack;
  private short[] prefix;
  private Bitmap previousImage;
  private ByteBuffer rawData;
  private int sampleSize;
  private boolean savePrevious;
  private int status;
  private byte[] suffix;
  
  public StandardGifDecoder(@NonNull GifDecoder.BitmapProvider paramBitmapProvider)
  {
    this.bitmapProvider = paramBitmapProvider;
    this.header = new GifHeader();
  }
  
  public StandardGifDecoder(@NonNull GifDecoder.BitmapProvider paramBitmapProvider, GifHeader paramGifHeader, ByteBuffer paramByteBuffer)
  {
    this(paramBitmapProvider, paramGifHeader, paramByteBuffer, 1);
  }
  
  public StandardGifDecoder(@NonNull GifDecoder.BitmapProvider paramBitmapProvider, GifHeader paramGifHeader, ByteBuffer paramByteBuffer, int paramInt)
  {
    this(paramBitmapProvider);
    setData(paramGifHeader, paramByteBuffer, paramInt);
  }
  
  @ColorInt
  private int averageColorsNear(int paramInt1, int paramInt2, int paramInt3)
  {
    int i1 = paramInt1;
    int n = 0;
    int m = 0;
    int k = 0;
    int j = 0;
    byte[] arrayOfByte;
    int i6;
    int i4;
    int i3;
    for (int i = 0; i1 < this.sampleSize + paramInt1; i = i2)
    {
      arrayOfByte = this.mainPixels;
      if ((i1 >= arrayOfByte.length) || (i1 >= paramInt2)) {
        break;
      }
      i2 = arrayOfByte[i1];
      int i7 = this.act[(i2 & 0xFF)];
      i6 = n;
      i5 = m;
      i4 = k;
      i3 = j;
      i2 = i;
      if (i7 != 0)
      {
        i6 = n + (i7 >> 24 & 0xFF);
        i5 = m + (i7 >> 16 & 0xFF);
        i4 = k + (i7 >> 8 & 0xFF);
        i3 = j + (i7 & 0xFF);
        i2 = i + 1;
      }
      i1++;
      n = i6;
      m = i5;
      k = i4;
      j = i3;
    }
    int i5 = paramInt1 + paramInt3;
    paramInt1 = i5;
    int i2 = n;
    while (paramInt1 < this.sampleSize + i5)
    {
      arrayOfByte = this.mainPixels;
      if ((paramInt1 >= arrayOfByte.length) || (paramInt1 >= paramInt2)) {
        break;
      }
      paramInt3 = arrayOfByte[paramInt1];
      i6 = this.act[(paramInt3 & 0xFF)];
      i4 = i2;
      i3 = m;
      i1 = k;
      n = j;
      paramInt3 = i;
      if (i6 != 0)
      {
        i4 = i2 + (i6 >> 24 & 0xFF);
        i3 = m + (i6 >> 16 & 0xFF);
        i1 = k + (i6 >> 8 & 0xFF);
        n = j + (i6 & 0xFF);
        paramInt3 = i + 1;
      }
      paramInt1++;
      i2 = i4;
      m = i3;
      k = i1;
      j = n;
      i = paramInt3;
    }
    if (i == 0) {
      return 0;
    }
    return i2 / i << 24 | m / i << 16 | k / i << 8 | j / i;
  }
  
  private void copyCopyIntoScratchRobust(GifFrame paramGifFrame)
  {
    int[] arrayOfInt1 = this.mainScratch;
    int i6 = paramGifFrame.ih / this.sampleSize;
    int i5 = paramGifFrame.iy / this.sampleSize;
    int i4 = paramGifFrame.iw / this.sampleSize;
    int i10 = paramGifFrame.ix / this.sampleSize;
    int i3;
    if (this.framePointer == 0) {
      i3 = 1;
    } else {
      i3 = 0;
    }
    int i12 = this.sampleSize;
    int i13 = this.downsampledWidth;
    int i11 = this.downsampledHeight;
    byte[] arrayOfByte = this.mainPixels;
    int[] arrayOfInt2 = this.act;
    Object localObject1 = this.isFirstFrameTransparent;
    int j = 0;
    int m = 0;
    int i1 = 1;
    int i2;
    for (int n = 8; m < i6; n = i2)
    {
      int i;
      int k;
      if (paramGifFrame.interlace)
      {
        i = j;
        i2 = i1;
        k = n;
        if (j >= i6)
        {
          i2 = i1 + 1;
          switch (i2)
          {
          default: 
            i = j;
            k = n;
            break;
          case 4: 
            i = 1;
            k = 2;
            break;
          case 3: 
            i = 2;
            k = 4;
            break;
          case 2: 
            i = 4;
            k = n;
          }
        }
        n = i + k;
        j = i;
        i = n;
        i1 = i2;
        i2 = k;
      }
      else
      {
        i = j;
        j = m;
        i2 = n;
      }
      j += i5;
      if (i12 == 1) {
        k = 1;
      } else {
        k = 0;
      }
      if (j < i11)
      {
        j *= i13;
        int i7 = j + i10;
        n = i7 + i4;
        int i8 = j + i13;
        j = n;
        if (i8 < n) {
          j = i8;
        }
        i8 = m * i12 * paramGifFrame.iw;
        if (k != 0)
        {
          k = i7;
          while (k < j)
          {
            n = arrayOfInt2[(arrayOfByte[i8] & 0xFF)];
            Object localObject2;
            if (n != 0)
            {
              arrayOfInt1[k] = n;
              localObject2 = localObject1;
            }
            else
            {
              localObject2 = localObject1;
              if (i3 != 0)
              {
                localObject2 = localObject1;
                if (localObject1 == null) {
                  localObject2 = Boolean.valueOf(true);
                }
              }
            }
            i8 += i12;
            k++;
            localObject1 = localObject2;
          }
        }
        else
        {
          int i9 = i7;
          n = i8;
          k = j;
          while (i9 < k)
          {
            int i14 = averageColorsNear(n, (j - i7) * i12 + i8, paramGifFrame.iw);
            if (i14 != 0) {
              arrayOfInt1[i9] = i14;
            } else if ((i3 != 0) && (localObject1 == null)) {
              localObject1 = Boolean.valueOf(true);
            }
            n += i12;
            i9++;
          }
        }
      }
      m++;
      j = i;
    }
    if (this.isFirstFrameTransparent == null)
    {
      boolean bool;
      if (localObject1 == null) {
        bool = false;
      } else {
        bool = ((Boolean)localObject1).booleanValue();
      }
      this.isFirstFrameTransparent = Boolean.valueOf(bool);
    }
  }
  
  private void copyIntoScratchFast(GifFrame paramGifFrame)
  {
    Object localObject = paramGifFrame;
    int[] arrayOfInt = this.mainScratch;
    int i6 = ((GifFrame)localObject).ih;
    int i7 = ((GifFrame)localObject).iy;
    int i5 = ((GifFrame)localObject).iw;
    int i4 = ((GifFrame)localObject).ix;
    int i;
    if (this.framePointer == 0) {
      i = 1;
    } else {
      i = 0;
    }
    int i8 = this.downsampledWidth;
    byte[] arrayOfByte = this.mainPixels;
    localObject = this.act;
    int j = 0;
    int m = -1;
    while (j < i6)
    {
      int k = (j + i7) * i8;
      int i1 = k + i4;
      int n = i1 + i5;
      int i2 = k + i8;
      k = n;
      if (i2 < n) {
        k = i2;
      }
      n = paramGifFrame.iw * j;
      while (i1 < k)
      {
        int i3 = arrayOfByte[n];
        int i9 = i3 & 0xFF;
        i2 = m;
        if (i9 != m)
        {
          i2 = localObject[i9];
          if (i2 != 0)
          {
            arrayOfInt[i1] = i2;
            i2 = m;
          }
          else
          {
            i2 = i3;
          }
        }
        n++;
        i1++;
        m = i2;
      }
      j++;
    }
    boolean bool;
    if ((this.isFirstFrameTransparent == null) && (i != 0) && (m != -1)) {
      bool = true;
    } else {
      bool = false;
    }
    this.isFirstFrameTransparent = Boolean.valueOf(bool);
  }
  
  private void decodeBitmapData(GifFrame paramGifFrame)
  {
    StandardGifDecoder localStandardGifDecoder = this;
    if (paramGifFrame != null) {
      localStandardGifDecoder.rawData.position(paramGifFrame.bufferFrameStart);
    }
    int i10;
    if (paramGifFrame == null)
    {
      i10 = localStandardGifDecoder.header.width * localStandardGifDecoder.header.height;
    }
    else
    {
      j = paramGifFrame.iw;
      i10 = paramGifFrame.ih * j;
    }
    paramGifFrame = localStandardGifDecoder.mainPixels;
    if ((paramGifFrame == null) || (paramGifFrame.length < i10)) {
      localStandardGifDecoder.mainPixels = localStandardGifDecoder.bitmapProvider.obtainByteArray(i10);
    }
    byte[] arrayOfByte3 = localStandardGifDecoder.mainPixels;
    if (localStandardGifDecoder.prefix == null) {
      localStandardGifDecoder.prefix = new short['က'];
    }
    short[] arrayOfShort = localStandardGifDecoder.prefix;
    if (localStandardGifDecoder.suffix == null) {
      localStandardGifDecoder.suffix = new byte['က'];
    }
    byte[] arrayOfByte1 = localStandardGifDecoder.suffix;
    if (localStandardGifDecoder.pixelStack == null) {
      localStandardGifDecoder.pixelStack = new byte['ခ'];
    }
    byte[] arrayOfByte4 = localStandardGifDecoder.pixelStack;
    int j = readByte();
    int i17 = 1 << j;
    int i9 = i17 + 2;
    int i12 = j + 1;
    int i11 = (1 << i12) - 1;
    int i13 = 0;
    for (j = 0; j < i17; j++)
    {
      arrayOfShort[j] = 0;
      arrayOfByte1[j] = ((byte)j);
    }
    byte[] arrayOfByte2 = localStandardGifDecoder.block;
    int m = i12;
    int i2 = i9;
    int i4 = i11;
    int i5 = 0;
    int i3 = 0;
    int i8 = 0;
    int i7 = 0;
    int k = 0;
    int n = -1;
    int i1 = 0;
    int i6 = 0;
    j = i13;
    paramGifFrame = localStandardGifDecoder;
    while (j < i10)
    {
      i13 = i5;
      if (i5 == 0)
      {
        i13 = readBlock();
        if (i13 <= 0)
        {
          paramGifFrame.status = 3;
          break;
        }
        i7 = 0;
      }
      i8 += ((arrayOfByte2[i7] & 0xFF) << i3);
      int i14 = i7 + 1;
      int i15 = i13 - 1;
      i7 = i3 + 8;
      i3 = n;
      i5 = i1;
      n = i2;
      i1 = k;
      i2 = i5;
      k = n;
      n = j;
      j = i3;
      i5 = i12;
      i3 = i7;
      for (;;)
      {
        if (i3 < m) {
          break label778;
        }
        i7 = i8 & i4;
        i8 >>= m;
        i3 -= m;
        if (i7 == i17)
        {
          m = i5;
          k = i9;
          i4 = i11;
          j = -1;
        }
        else
        {
          if (i7 == i17 + 1)
          {
            i7 = n;
            n = i1;
            i13 = k;
            i1 = i2;
            i2 = j;
            i12 = i5;
            j = i7;
            i5 = i15;
            i7 = i14;
            k = n;
            n = i2;
            i2 = i13;
            break;
          }
          if (j == -1)
          {
            arrayOfByte3[i1] = arrayOfByte1[i7];
            i1++;
            n++;
            j = i7;
            i2 = j;
            paramGifFrame = this;
          }
          else
          {
            i12 = k;
            if (i7 >= i12)
            {
              arrayOfByte4[i6] = ((byte)i2);
              k = i6 + 1;
              i2 = j;
            }
            else
            {
              i2 = i7;
              k = i6;
            }
            while (i2 >= i17)
            {
              arrayOfByte4[k] = arrayOfByte1[i2];
              k++;
              i2 = arrayOfShort[i2];
            }
            int i16 = arrayOfByte1[i2] & 0xFF;
            int i = (byte)i16;
            arrayOfByte3[i1] = i;
            i1++;
            n++;
            while (k > 0)
            {
              k--;
              arrayOfByte3[i1] = arrayOfByte4[k];
              i1++;
              n++;
            }
            i2 = i12;
            i6 = m;
            i13 = i4;
            if (i12 < 4096)
            {
              arrayOfShort[i12] = ((short)j);
              arrayOfByte1[i12] = i;
              j = i12 + 1;
              i2 = j;
              i6 = m;
              i13 = i4;
              if ((j & i4) == 0)
              {
                i2 = j;
                i6 = m;
                i13 = i4;
                if (j < 4096)
                {
                  i6 = m + 1;
                  i13 = i4 + j;
                  i2 = j;
                }
              }
            }
            j = i7;
            i7 = i16;
            paramGifFrame = this;
            m = i6;
            i6 = k;
            i4 = i13;
            k = i2;
            i2 = i7;
          }
        }
      }
      label778:
      i7 = n;
      i13 = k;
      n = j;
      paramGifFrame = this;
      i12 = i5;
      j = i7;
      i5 = i15;
      i7 = i14;
      k = i1;
      i1 = i2;
      i2 = i13;
    }
    Arrays.fill(arrayOfByte3, k, i10, (byte)0);
  }
  
  @NonNull
  private GifHeaderParser getHeaderParser()
  {
    if (this.parser == null) {
      this.parser = new GifHeaderParser();
    }
    return this.parser;
  }
  
  private Bitmap getNextBitmap()
  {
    Object localObject = this.isFirstFrameTransparent;
    if ((localObject != null) && (!((Boolean)localObject).booleanValue())) {
      localObject = this.bitmapConfig;
    } else {
      localObject = Bitmap.Config.ARGB_8888;
    }
    localObject = this.bitmapProvider.obtain(this.downsampledWidth, this.downsampledHeight, (Bitmap.Config)localObject);
    ((Bitmap)localObject).setHasAlpha(true);
    return (Bitmap)localObject;
  }
  
  private int readBlock()
  {
    int i = readByte();
    if (i <= 0) {
      return i;
    }
    ByteBuffer localByteBuffer = this.rawData;
    localByteBuffer.get(this.block, 0, Math.min(i, localByteBuffer.remaining()));
    return i;
  }
  
  private int readByte()
  {
    return this.rawData.get() & 0xFF;
  }
  
  private Bitmap setPixels(GifFrame paramGifFrame1, GifFrame paramGifFrame2)
  {
    int[] arrayOfInt = this.mainScratch;
    int i = 0;
    if (paramGifFrame2 == null)
    {
      Bitmap localBitmap = this.previousImage;
      if (localBitmap != null) {
        this.bitmapProvider.release(localBitmap);
      }
      this.previousImage = null;
      Arrays.fill(arrayOfInt, 0);
    }
    if ((paramGifFrame2 != null) && (paramGifFrame2.dispose == 3) && (this.previousImage == null)) {
      Arrays.fill(arrayOfInt, 0);
    }
    if ((paramGifFrame2 != null) && (paramGifFrame2.dispose > 0))
    {
      if (paramGifFrame2.dispose == 2)
      {
        int j;
        if (!paramGifFrame1.transparency)
        {
          j = this.header.bgColor;
          if ((paramGifFrame1.lct != null) && (this.header.bgIndex == paramGifFrame1.transIndex)) {
            j = i;
          }
        }
        else
        {
          j = i;
          if (this.framePointer == 0)
          {
            this.isFirstFrameTransparent = Boolean.valueOf(true);
            j = i;
          }
        }
        int i1 = paramGifFrame2.ih / this.sampleSize;
        int k = paramGifFrame2.iy / this.sampleSize;
        int n = paramGifFrame2.iw / this.sampleSize;
        i = paramGifFrame2.ix / this.sampleSize;
        int i2 = this.downsampledWidth;
        int m = k * i2 + i;
        i = m;
        while (i < i1 * i2 + m)
        {
          for (k = i; k < i + n; k++) {
            arrayOfInt[k] = j;
          }
          i += this.downsampledWidth;
        }
      }
      if (paramGifFrame2.dispose == 3)
      {
        paramGifFrame2 = this.previousImage;
        if (paramGifFrame2 != null)
        {
          i = this.downsampledWidth;
          paramGifFrame2.getPixels(arrayOfInt, 0, i, 0, 0, i, this.downsampledHeight);
        }
      }
    }
    decodeBitmapData(paramGifFrame1);
    if ((!paramGifFrame1.interlace) && (this.sampleSize == 1)) {
      copyIntoScratchFast(paramGifFrame1);
    } else {
      copyCopyIntoScratchRobust(paramGifFrame1);
    }
    if ((this.savePrevious) && ((paramGifFrame1.dispose == 0) || (paramGifFrame1.dispose == 1)))
    {
      if (this.previousImage == null) {
        this.previousImage = getNextBitmap();
      }
      paramGifFrame1 = this.previousImage;
      i = this.downsampledWidth;
      paramGifFrame1.setPixels(arrayOfInt, 0, i, 0, 0, i, this.downsampledHeight);
    }
    paramGifFrame1 = getNextBitmap();
    i = this.downsampledWidth;
    paramGifFrame1.setPixels(arrayOfInt, 0, i, 0, 0, i, this.downsampledHeight);
    return paramGifFrame1;
  }
  
  public void advance()
  {
    this.framePointer = ((this.framePointer + 1) % this.header.frameCount);
  }
  
  public void clear()
  {
    this.header = null;
    Object localObject = this.mainPixels;
    if (localObject != null) {
      this.bitmapProvider.release((byte[])localObject);
    }
    localObject = this.mainScratch;
    if (localObject != null) {
      this.bitmapProvider.release((int[])localObject);
    }
    localObject = this.previousImage;
    if (localObject != null) {
      this.bitmapProvider.release((Bitmap)localObject);
    }
    this.previousImage = null;
    this.rawData = null;
    this.isFirstFrameTransparent = null;
    localObject = this.block;
    if (localObject != null) {
      this.bitmapProvider.release((byte[])localObject);
    }
  }
  
  public int getByteSize()
  {
    return this.rawData.limit() + this.mainPixels.length + this.mainScratch.length * 4;
  }
  
  public int getCurrentFrameIndex()
  {
    return this.framePointer;
  }
  
  @NonNull
  public ByteBuffer getData()
  {
    return this.rawData;
  }
  
  public int getDelay(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.header.frameCount)) {
      paramInt = ((GifFrame)this.header.frames.get(paramInt)).delay;
    } else {
      paramInt = -1;
    }
    return paramInt;
  }
  
  public int getFrameCount()
  {
    return this.header.frameCount;
  }
  
  public int getHeight()
  {
    return this.header.height;
  }
  
  @Deprecated
  public int getLoopCount()
  {
    if (this.header.loopCount == -1) {
      return 1;
    }
    return this.header.loopCount;
  }
  
  public int getNetscapeLoopCount()
  {
    return this.header.loopCount;
  }
  
  public int getNextDelay()
  {
    if (this.header.frameCount > 0)
    {
      int i = this.framePointer;
      if (i >= 0) {
        return getDelay(i);
      }
    }
    return 0;
  }
  
  @Nullable
  public Bitmap getNextFrame()
  {
    try
    {
      Object localObject3;
      Object localObject1;
      if ((this.header.frameCount <= 0) || (this.framePointer < 0))
      {
        if (Log.isLoggable(TAG, 3))
        {
          localObject3 = TAG;
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("Unable to decode frame, frameCount=");
          ((StringBuilder)localObject1).append(this.header.frameCount);
          ((StringBuilder)localObject1).append(", framePointer=");
          ((StringBuilder)localObject1).append(this.framePointer);
          Log.d((String)localObject3, ((StringBuilder)localObject1).toString());
        }
        this.status = 1;
      }
      if ((this.status != 1) && (this.status != 2))
      {
        this.status = 0;
        if (this.block == null) {
          this.block = this.bitmapProvider.obtainByteArray(255);
        }
        GifFrame localGifFrame = (GifFrame)this.header.frames.get(this.framePointer);
        int i = this.framePointer - 1;
        if (i >= 0) {
          localObject1 = (GifFrame)this.header.frames.get(i);
        } else {
          localObject1 = null;
        }
        if (localGifFrame.lct != null) {
          localObject3 = localGifFrame.lct;
        } else {
          localObject3 = this.header.gct;
        }
        this.act = ((int[])localObject3);
        if (this.act == null)
        {
          if (Log.isLoggable(TAG, 3))
          {
            localObject3 = TAG;
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("No valid color table found for frame #");
            ((StringBuilder)localObject1).append(this.framePointer);
            Log.d((String)localObject3, ((StringBuilder)localObject1).toString());
          }
          this.status = 1;
          return null;
        }
        if (localGifFrame.transparency)
        {
          System.arraycopy(this.act, 0, this.pct, 0, this.act.length);
          this.act = this.pct;
          this.act[localGifFrame.transIndex] = 0;
        }
        localObject1 = setPixels(localGifFrame, (GifFrame)localObject1);
        return (Bitmap)localObject1;
      }
      if (Log.isLoggable(TAG, 3))
      {
        localObject1 = TAG;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append("Unable to decode frame, status=");
        ((StringBuilder)localObject3).append(this.status);
        Log.d((String)localObject1, ((StringBuilder)localObject3).toString());
      }
      return null;
    }
    finally {}
  }
  
  public int getStatus()
  {
    return this.status;
  }
  
  public int getTotalIterationCount()
  {
    if (this.header.loopCount == -1) {
      return 1;
    }
    if (this.header.loopCount == 0) {
      return 0;
    }
    return this.header.loopCount + 1;
  }
  
  public int getWidth()
  {
    return this.header.width;
  }
  
  public int read(@Nullable InputStream paramInputStream, int paramInt)
  {
    if (paramInputStream != null)
    {
      if (paramInt > 0) {
        paramInt += 4096;
      } else {
        paramInt = 16384;
      }
      try
      {
        ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
        localByteArrayOutputStream.<init>(paramInt);
        byte[] arrayOfByte = new byte['䀀'];
        for (;;)
        {
          paramInt = paramInputStream.read(arrayOfByte, 0, arrayOfByte.length);
          if (paramInt == -1) {
            break;
          }
          localByteArrayOutputStream.write(arrayOfByte, 0, paramInt);
        }
        localByteArrayOutputStream.flush();
        read(localByteArrayOutputStream.toByteArray());
      }
      catch (IOException localIOException)
      {
        Log.w(TAG, "Error reading data from stream", localIOException);
      }
    }
    this.status = 2;
    if (paramInputStream != null) {
      try
      {
        paramInputStream.close();
      }
      catch (IOException paramInputStream)
      {
        Log.w(TAG, "Error closing stream", paramInputStream);
      }
    }
    return this.status;
  }
  
  public int read(@Nullable byte[] paramArrayOfByte)
  {
    try
    {
      this.header = getHeaderParser().setData(paramArrayOfByte).parseHeader();
      if (paramArrayOfByte != null) {
        setData(this.header, paramArrayOfByte);
      }
      int i = this.status;
      return i;
    }
    finally {}
  }
  
  public void resetFrameIndex()
  {
    this.framePointer = -1;
  }
  
  public void setData(@NonNull GifHeader paramGifHeader, @NonNull ByteBuffer paramByteBuffer)
  {
    try
    {
      setData(paramGifHeader, paramByteBuffer, 1);
      return;
    }
    finally
    {
      paramGifHeader = finally;
      throw paramGifHeader;
    }
  }
  
  public void setData(@NonNull GifHeader paramGifHeader, @NonNull ByteBuffer paramByteBuffer, int paramInt)
  {
    if (paramInt > 0) {
      try
      {
        paramInt = Integer.highestOneBit(paramInt);
        this.status = 0;
        this.header = paramGifHeader;
        this.framePointer = -1;
        this.rawData = paramByteBuffer.asReadOnlyBuffer();
        this.rawData.position(0);
        this.rawData.order(ByteOrder.LITTLE_ENDIAN);
        this.savePrevious = false;
        paramByteBuffer = paramGifHeader.frames.iterator();
        while (paramByteBuffer.hasNext()) {
          if (((GifFrame)paramByteBuffer.next()).dispose == 3) {
            this.savePrevious = true;
          }
        }
        this.sampleSize = paramInt;
        this.downsampledWidth = (paramGifHeader.width / paramInt);
        this.downsampledHeight = (paramGifHeader.height / paramInt);
        this.mainPixels = this.bitmapProvider.obtainByteArray(paramGifHeader.width * paramGifHeader.height);
        this.mainScratch = this.bitmapProvider.obtainIntArray(this.downsampledWidth * this.downsampledHeight);
        return;
      }
      finally
      {
        break label211;
      }
    }
    paramGifHeader = new java/lang/IllegalArgumentException;
    paramByteBuffer = new java/lang/StringBuilder;
    paramByteBuffer.<init>();
    paramByteBuffer.append("Sample size must be >=0, not: ");
    paramByteBuffer.append(paramInt);
    paramGifHeader.<init>(paramByteBuffer.toString());
    throw paramGifHeader;
    label211:
    throw paramGifHeader;
  }
  
  public void setData(@NonNull GifHeader paramGifHeader, @NonNull byte[] paramArrayOfByte)
  {
    try
    {
      setData(paramGifHeader, ByteBuffer.wrap(paramArrayOfByte));
      return;
    }
    finally
    {
      paramGifHeader = finally;
      throw paramGifHeader;
    }
  }
  
  public void setDefaultBitmapConfig(@NonNull Bitmap.Config paramConfig)
  {
    if ((paramConfig != Bitmap.Config.ARGB_8888) && (paramConfig != Bitmap.Config.RGB_565))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Unsupported format: ");
      localStringBuilder.append(paramConfig);
      localStringBuilder.append(", must be one of ");
      localStringBuilder.append(Bitmap.Config.ARGB_8888);
      localStringBuilder.append(" or ");
      localStringBuilder.append(Bitmap.Config.RGB_565);
      throw new IllegalArgumentException(localStringBuilder.toString());
    }
    this.bitmapConfig = paramConfig;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/gifdecoder/StandardGifDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */