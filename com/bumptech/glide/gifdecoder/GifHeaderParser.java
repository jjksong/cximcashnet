package com.bumptech.glide.gifdecoder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

public class GifHeaderParser
{
  static final int DEFAULT_FRAME_DELAY = 10;
  private static final int DESCRIPTOR_MASK_INTERLACE_FLAG = 64;
  private static final int DESCRIPTOR_MASK_LCT_FLAG = 128;
  private static final int DESCRIPTOR_MASK_LCT_SIZE = 7;
  private static final int EXTENSION_INTRODUCER = 33;
  private static final int GCE_DISPOSAL_METHOD_SHIFT = 2;
  private static final int GCE_MASK_DISPOSAL_METHOD = 28;
  private static final int GCE_MASK_TRANSPARENT_COLOR_FLAG = 1;
  private static final int IMAGE_SEPARATOR = 44;
  private static final int LABEL_APPLICATION_EXTENSION = 255;
  private static final int LABEL_COMMENT_EXTENSION = 254;
  private static final int LABEL_GRAPHIC_CONTROL_EXTENSION = 249;
  private static final int LABEL_PLAIN_TEXT_EXTENSION = 1;
  private static final int LSD_MASK_GCT_FLAG = 128;
  private static final int LSD_MASK_GCT_SIZE = 7;
  private static final int MASK_INT_LOWEST_BYTE = 255;
  private static final int MAX_BLOCK_SIZE = 256;
  static final int MIN_FRAME_DELAY = 2;
  private static final String TAG = "GifHeaderParser";
  private static final int TRAILER = 59;
  private final byte[] block = new byte['Ā'];
  private int blockSize = 0;
  private GifHeader header;
  private ByteBuffer rawData;
  
  private boolean err()
  {
    boolean bool;
    if (this.header.status != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private int read()
  {
    int i;
    try
    {
      i = this.rawData.get();
      i &= 0xFF;
    }
    catch (Exception localException)
    {
      this.header.status = 1;
      i = 0;
    }
    return i;
  }
  
  private void readBitmap()
  {
    this.header.currentFrame.ix = readShort();
    this.header.currentFrame.iy = readShort();
    this.header.currentFrame.iw = readShort();
    this.header.currentFrame.ih = readShort();
    int j = read();
    boolean bool = false;
    int i;
    if ((j & 0x80) != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int k = (int)Math.pow(2.0D, (j & 0x7) + 1);
    Object localObject = this.header.currentFrame;
    if ((j & 0x40) != 0) {
      bool = true;
    }
    ((GifFrame)localObject).interlace = bool;
    if (i != 0) {
      this.header.currentFrame.lct = readColorTable(k);
    } else {
      this.header.currentFrame.lct = null;
    }
    this.header.currentFrame.bufferFrameStart = this.rawData.position();
    skipImageData();
    if (err()) {
      return;
    }
    localObject = this.header;
    ((GifHeader)localObject).frameCount += 1;
    this.header.frames.add(this.header.currentFrame);
  }
  
  private void readBlock()
  {
    this.blockSize = read();
    if (this.blockSize > 0)
    {
      int j = 0;
      int i = 0;
      for (;;)
      {
        int k = i;
        try
        {
          if (j < this.blockSize)
          {
            k = i;
            i = this.blockSize - j;
            k = i;
            this.rawData.get(this.block, j, i);
            j += i;
          }
        }
        catch (Exception localException)
        {
          if (Log.isLoggable("GifHeaderParser", 3))
          {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("Error Reading Block n: ");
            localStringBuilder.append(j);
            localStringBuilder.append(" count: ");
            localStringBuilder.append(k);
            localStringBuilder.append(" blockSize: ");
            localStringBuilder.append(this.blockSize);
            Log.d("GifHeaderParser", localStringBuilder.toString(), localException);
          }
          this.header.status = 1;
        }
      }
    }
  }
  
  @Nullable
  private int[] readColorTable(int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt * 3];
    int[] arrayOfInt2 = null;
    arrayOfInt1 = arrayOfInt2;
    try
    {
      this.rawData.get(arrayOfByte);
      arrayOfInt1 = arrayOfInt2;
      arrayOfInt2 = new int['Ā'];
      int i = 0;
      int j = 0;
      for (;;)
      {
        arrayOfInt1 = arrayOfInt2;
        if (i >= paramInt) {
          break;
        }
        int k = j + 1;
        j = arrayOfByte[j];
        int m = k + 1;
        arrayOfInt2[i] = ((j & 0xFF) << 16 | 0xFF000000 | (arrayOfByte[k] & 0xFF) << 8 | arrayOfByte[m] & 0xFF);
        j = m + 1;
        i++;
      }
      return arrayOfInt1;
    }
    catch (BufferUnderflowException localBufferUnderflowException)
    {
      if (Log.isLoggable("GifHeaderParser", 3)) {
        Log.d("GifHeaderParser", "Format Error Reading Color Table", localBufferUnderflowException);
      }
      this.header.status = 1;
    }
  }
  
  private void readContents()
  {
    readContents(Integer.MAX_VALUE);
  }
  
  private void readContents(int paramInt)
  {
    int i = 0;
    while ((i == 0) && (!err()) && (this.header.frameCount <= paramInt))
    {
      int j = read();
      if (j != 33)
      {
        if (j != 44)
        {
          if (j != 59) {
            this.header.status = 1;
          } else {
            i = 1;
          }
        }
        else
        {
          if (this.header.currentFrame == null) {
            this.header.currentFrame = new GifFrame();
          }
          readBitmap();
        }
      }
      else
      {
        j = read();
        if (j != 1)
        {
          if (j != 249)
          {
            switch (j)
            {
            default: 
              skip();
              break;
            case 255: 
              readBlock();
              StringBuilder localStringBuilder = new StringBuilder();
              for (j = 0; j < 11; j++) {
                localStringBuilder.append((char)this.block[j]);
              }
              if (localStringBuilder.toString().equals("NETSCAPE2.0"))
              {
                readNetscapeExt();
                continue;
              }
              skip();
              break;
            case 254: 
              skip();
              break;
            }
          }
          else
          {
            this.header.currentFrame = new GifFrame();
            readGraphicControlExt();
          }
        }
        else {
          skip();
        }
      }
    }
  }
  
  private void readGraphicControlExt()
  {
    read();
    int i = read();
    this.header.currentFrame.dispose = ((i & 0x1C) >> 2);
    int j = this.header.currentFrame.dispose;
    boolean bool = true;
    if (j == 0) {
      this.header.currentFrame.dispose = 1;
    }
    GifFrame localGifFrame = this.header.currentFrame;
    if ((i & 0x1) == 0) {
      bool = false;
    }
    localGifFrame.transparency = bool;
    j = readShort();
    i = j;
    if (j < 2) {
      i = 10;
    }
    this.header.currentFrame.delay = (i * 10);
    this.header.currentFrame.transIndex = read();
    read();
  }
  
  private void readHeader()
  {
    Object localObject = new StringBuilder();
    for (int i = 0; i < 6; i++) {
      ((StringBuilder)localObject).append((char)read());
    }
    if (!((StringBuilder)localObject).toString().startsWith("GIF"))
    {
      this.header.status = 1;
      return;
    }
    readLSD();
    if ((this.header.gctFlag) && (!err()))
    {
      localObject = this.header;
      ((GifHeader)localObject).gct = readColorTable(((GifHeader)localObject).gctSize);
      localObject = this.header;
      ((GifHeader)localObject).bgColor = localObject.gct[this.header.bgIndex];
    }
  }
  
  private void readLSD()
  {
    this.header.width = readShort();
    this.header.height = readShort();
    int i = read();
    GifHeader localGifHeader = this.header;
    boolean bool;
    if ((i & 0x80) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    localGifHeader.gctFlag = bool;
    this.header.gctSize = ((int)Math.pow(2.0D, (i & 0x7) + 1));
    this.header.bgIndex = read();
    this.header.pixelAspect = read();
  }
  
  private void readNetscapeExt()
  {
    do
    {
      readBlock();
      byte[] arrayOfByte = this.block;
      if (arrayOfByte[0] == 1)
      {
        int j = arrayOfByte[1];
        int i = arrayOfByte[2];
        this.header.loopCount = ((i & 0xFF) << 8 | j & 0xFF);
      }
    } while ((this.blockSize > 0) && (!err()));
  }
  
  private int readShort()
  {
    return this.rawData.getShort();
  }
  
  private void reset()
  {
    this.rawData = null;
    Arrays.fill(this.block, (byte)0);
    this.header = new GifHeader();
    this.blockSize = 0;
  }
  
  private void skip()
  {
    int i;
    do
    {
      i = read();
      int j = Math.min(this.rawData.position() + i, this.rawData.limit());
      this.rawData.position(j);
    } while (i > 0);
  }
  
  private void skipImageData()
  {
    read();
    skip();
  }
  
  public void clear()
  {
    this.rawData = null;
    this.header = null;
  }
  
  public boolean isAnimated()
  {
    readHeader();
    if (!err()) {
      readContents(2);
    }
    int i = this.header.frameCount;
    boolean bool = true;
    if (i <= 1) {
      bool = false;
    }
    return bool;
  }
  
  @NonNull
  public GifHeader parseHeader()
  {
    if (this.rawData != null)
    {
      if (err()) {
        return this.header;
      }
      readHeader();
      if (!err())
      {
        readContents();
        if (this.header.frameCount < 0) {
          this.header.status = 1;
        }
      }
      return this.header;
    }
    throw new IllegalStateException("You must call setData() before parseHeader()");
  }
  
  public GifHeaderParser setData(@NonNull ByteBuffer paramByteBuffer)
  {
    reset();
    this.rawData = paramByteBuffer.asReadOnlyBuffer();
    this.rawData.position(0);
    this.rawData.order(ByteOrder.LITTLE_ENDIAN);
    return this;
  }
  
  public GifHeaderParser setData(@Nullable byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null)
    {
      setData(ByteBuffer.wrap(paramArrayOfByte));
    }
    else
    {
      this.rawData = null;
      this.header.status = 2;
    }
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/gifdecoder/GifHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */