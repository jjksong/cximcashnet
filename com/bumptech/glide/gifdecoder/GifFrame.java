package com.bumptech.glide.gifdecoder;

import android.support.annotation.ColorInt;

class GifFrame
{
  static final int DISPOSAL_BACKGROUND = 2;
  static final int DISPOSAL_NONE = 1;
  static final int DISPOSAL_PREVIOUS = 3;
  static final int DISPOSAL_UNSPECIFIED = 0;
  int bufferFrameStart;
  int delay;
  int dispose;
  int ih;
  boolean interlace;
  int iw;
  int ix;
  int iy;
  @ColorInt
  int[] lct;
  int transIndex;
  boolean transparency;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/gifdecoder/GifFrame.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */