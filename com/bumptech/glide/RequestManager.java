package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.CheckResult;
import android.support.annotation.DrawableRes;
import android.support.annotation.GuardedBy;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.view.View;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.manager.ConnectivityMonitor.ConnectivityListener;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.manager.RequestManagerTreeNode;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.manager.TargetTracker;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.util.Util;
import java.io.File;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class RequestManager
  implements LifecycleListener, ModelTypes<RequestBuilder<Drawable>>
{
  private static final RequestOptions DECODE_TYPE_BITMAP = (RequestOptions)RequestOptions.decodeTypeOf(Bitmap.class).lock();
  private static final RequestOptions DECODE_TYPE_GIF = (RequestOptions)RequestOptions.decodeTypeOf(GifDrawable.class).lock();
  private static final RequestOptions DOWNLOAD_ONLY_OPTIONS = (RequestOptions)((RequestOptions)RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.DATA).priority(Priority.LOW)).skipMemoryCache(true);
  private final Runnable addSelfToLifecycle = new Runnable()
  {
    public void run()
    {
      RequestManager.this.lifecycle.addListener(RequestManager.this);
    }
  };
  private final ConnectivityMonitor connectivityMonitor;
  protected final Context context;
  private final CopyOnWriteArrayList<RequestListener<Object>> defaultRequestListeners;
  protected final Glide glide;
  final Lifecycle lifecycle;
  private final Handler mainHandler = new Handler(Looper.getMainLooper());
  @GuardedBy("this")
  private RequestOptions requestOptions;
  @GuardedBy("this")
  private final RequestTracker requestTracker;
  @GuardedBy("this")
  private final TargetTracker targetTracker = new TargetTracker();
  @GuardedBy("this")
  private final RequestManagerTreeNode treeNode;
  
  public RequestManager(@NonNull Glide paramGlide, @NonNull Lifecycle paramLifecycle, @NonNull RequestManagerTreeNode paramRequestManagerTreeNode, @NonNull Context paramContext)
  {
    this(paramGlide, paramLifecycle, paramRequestManagerTreeNode, new RequestTracker(), paramGlide.getConnectivityMonitorFactory(), paramContext);
  }
  
  RequestManager(Glide paramGlide, Lifecycle paramLifecycle, RequestManagerTreeNode paramRequestManagerTreeNode, RequestTracker paramRequestTracker, ConnectivityMonitorFactory paramConnectivityMonitorFactory, Context paramContext)
  {
    this.glide = paramGlide;
    this.lifecycle = paramLifecycle;
    this.treeNode = paramRequestManagerTreeNode;
    this.requestTracker = paramRequestTracker;
    this.context = paramContext;
    this.connectivityMonitor = paramConnectivityMonitorFactory.build(paramContext.getApplicationContext(), new RequestManagerConnectivityListener(paramRequestTracker));
    if (Util.isOnBackgroundThread()) {
      this.mainHandler.post(this.addSelfToLifecycle);
    } else {
      paramLifecycle.addListener(this);
    }
    paramLifecycle.addListener(this.connectivityMonitor);
    this.defaultRequestListeners = new CopyOnWriteArrayList(paramGlide.getGlideContext().getDefaultRequestListeners());
    setRequestOptions(paramGlide.getGlideContext().getDefaultRequestOptions());
    paramGlide.registerRequestManager(this);
  }
  
  private void untrackOrDelegate(@NonNull Target<?> paramTarget)
  {
    if ((!untrack(paramTarget)) && (!this.glide.removeFromManagers(paramTarget)) && (paramTarget.getRequest() != null))
    {
      Request localRequest = paramTarget.getRequest();
      paramTarget.setRequest(null);
      localRequest.clear();
    }
  }
  
  private void updateRequestOptions(@NonNull RequestOptions paramRequestOptions)
  {
    try
    {
      this.requestOptions = ((RequestOptions)this.requestOptions.apply(paramRequestOptions));
      return;
    }
    finally
    {
      paramRequestOptions = finally;
      throw paramRequestOptions;
    }
  }
  
  public RequestManager addDefaultRequestListener(RequestListener<Object> paramRequestListener)
  {
    this.defaultRequestListeners.add(paramRequestListener);
    return this;
  }
  
  @NonNull
  public RequestManager applyDefaultRequestOptions(@NonNull RequestOptions paramRequestOptions)
  {
    try
    {
      updateRequestOptions(paramRequestOptions);
      return this;
    }
    finally
    {
      paramRequestOptions = finally;
      throw paramRequestOptions;
    }
  }
  
  @CheckResult
  @NonNull
  public <ResourceType> RequestBuilder<ResourceType> as(@NonNull Class<ResourceType> paramClass)
  {
    return new RequestBuilder(this.glide, this, paramClass, this.context);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Bitmap> asBitmap()
  {
    return as(Bitmap.class).apply(DECODE_TYPE_BITMAP);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> asDrawable()
  {
    return as(Drawable.class);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<File> asFile()
  {
    return as(File.class).apply(RequestOptions.skipMemoryCacheOf(true));
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<GifDrawable> asGif()
  {
    return as(GifDrawable.class).apply(DECODE_TYPE_GIF);
  }
  
  public void clear(@NonNull View paramView)
  {
    clear(new ClearTarget(paramView));
  }
  
  public void clear(@Nullable Target<?> paramTarget)
  {
    if (paramTarget == null) {
      return;
    }
    try
    {
      untrackOrDelegate(paramTarget);
      return;
    }
    finally
    {
      paramTarget = finally;
      throw paramTarget;
    }
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<File> download(@Nullable Object paramObject)
  {
    return downloadOnly().load(paramObject);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<File> downloadOnly()
  {
    return as(File.class).apply(DOWNLOAD_ONLY_OPTIONS);
  }
  
  List<RequestListener<Object>> getDefaultRequestListeners()
  {
    return this.defaultRequestListeners;
  }
  
  RequestOptions getDefaultRequestOptions()
  {
    try
    {
      RequestOptions localRequestOptions = this.requestOptions;
      return localRequestOptions;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  @NonNull
  <T> TransitionOptions<?, T> getDefaultTransitionOptions(Class<T> paramClass)
  {
    return this.glide.getGlideContext().getDefaultTransitionOptions(paramClass);
  }
  
  public boolean isPaused()
  {
    try
    {
      boolean bool = this.requestTracker.isPaused();
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable Bitmap paramBitmap)
  {
    return asDrawable().load(paramBitmap);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable Drawable paramDrawable)
  {
    return asDrawable().load(paramDrawable);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable Uri paramUri)
  {
    return asDrawable().load(paramUri);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable File paramFile)
  {
    return asDrawable().load(paramFile);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@DrawableRes @Nullable @RawRes Integer paramInteger)
  {
    return asDrawable().load(paramInteger);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable Object paramObject)
  {
    return asDrawable().load(paramObject);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable String paramString)
  {
    return asDrawable().load(paramString);
  }
  
  @Deprecated
  @CheckResult
  public RequestBuilder<Drawable> load(@Nullable URL paramURL)
  {
    return asDrawable().load(paramURL);
  }
  
  @CheckResult
  @NonNull
  public RequestBuilder<Drawable> load(@Nullable byte[] paramArrayOfByte)
  {
    return asDrawable().load(paramArrayOfByte);
  }
  
  public void onDestroy()
  {
    try
    {
      this.targetTracker.onDestroy();
      Iterator localIterator = this.targetTracker.getAll().iterator();
      while (localIterator.hasNext()) {
        clear((Target)localIterator.next());
      }
      this.targetTracker.clear();
      this.requestTracker.clearRequests();
      this.lifecycle.removeListener(this);
      this.lifecycle.removeListener(this.connectivityMonitor);
      this.mainHandler.removeCallbacks(this.addSelfToLifecycle);
      this.glide.unregisterRequestManager(this);
      return;
    }
    finally {}
  }
  
  public void onStart()
  {
    try
    {
      resumeRequests();
      this.targetTracker.onStart();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void onStop()
  {
    try
    {
      pauseRequests();
      this.targetTracker.onStop();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void pauseAllRequests()
  {
    try
    {
      this.requestTracker.pauseAllRequests();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void pauseRequests()
  {
    try
    {
      this.requestTracker.pauseRequests();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void pauseRequestsRecursive()
  {
    try
    {
      pauseRequests();
      Iterator localIterator = this.treeNode.getDescendants().iterator();
      while (localIterator.hasNext()) {
        ((RequestManager)localIterator.next()).pauseRequests();
      }
      return;
    }
    finally {}
  }
  
  public void resumeRequests()
  {
    try
    {
      this.requestTracker.resumeRequests();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void resumeRequestsRecursive()
  {
    try
    {
      Util.assertMainThread();
      resumeRequests();
      Iterator localIterator = this.treeNode.getDescendants().iterator();
      while (localIterator.hasNext()) {
        ((RequestManager)localIterator.next()).resumeRequests();
      }
      return;
    }
    finally {}
  }
  
  @NonNull
  public RequestManager setDefaultRequestOptions(@NonNull RequestOptions paramRequestOptions)
  {
    try
    {
      setRequestOptions(paramRequestOptions);
      return this;
    }
    finally
    {
      paramRequestOptions = finally;
      throw paramRequestOptions;
    }
  }
  
  protected void setRequestOptions(@NonNull RequestOptions paramRequestOptions)
  {
    try
    {
      this.requestOptions = ((RequestOptions)((RequestOptions)paramRequestOptions.clone()).autoClone());
      return;
    }
    finally
    {
      paramRequestOptions = finally;
      throw paramRequestOptions;
    }
  }
  
  public String toString()
  {
    try
    {
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(super.toString());
      ((StringBuilder)localObject1).append("{tracker=");
      ((StringBuilder)localObject1).append(this.requestTracker);
      ((StringBuilder)localObject1).append(", treeNode=");
      ((StringBuilder)localObject1).append(this.treeNode);
      ((StringBuilder)localObject1).append("}");
      localObject1 = ((StringBuilder)localObject1).toString();
      return (String)localObject1;
    }
    finally
    {
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  void track(@NonNull Target<?> paramTarget, @NonNull Request paramRequest)
  {
    try
    {
      this.targetTracker.track(paramTarget);
      this.requestTracker.runRequest(paramRequest);
      return;
    }
    finally
    {
      paramTarget = finally;
      throw paramTarget;
    }
  }
  
  boolean untrack(@NonNull Target<?> paramTarget)
  {
    try
    {
      Request localRequest = paramTarget.getRequest();
      if (localRequest == null) {
        return true;
      }
      if (this.requestTracker.clearRemoveAndRecycle(localRequest))
      {
        this.targetTracker.untrack(paramTarget);
        paramTarget.setRequest(null);
        return true;
      }
      return false;
    }
    finally {}
  }
  
  private static class ClearTarget
    extends ViewTarget<View, Object>
  {
    ClearTarget(@NonNull View paramView)
    {
      super();
    }
    
    public void onResourceReady(@NonNull Object paramObject, @Nullable Transition<? super Object> paramTransition) {}
  }
  
  private class RequestManagerConnectivityListener
    implements ConnectivityMonitor.ConnectivityListener
  {
    @GuardedBy("RequestManager.this")
    private final RequestTracker requestTracker;
    
    RequestManagerConnectivityListener(RequestTracker paramRequestTracker)
    {
      this.requestTracker = paramRequestTracker;
    }
    
    public void onConnectivityChanged(boolean paramBoolean)
    {
      if (paramBoolean) {
        synchronized (RequestManager.this)
        {
          this.requestTracker.restartRequests();
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/RequestManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */