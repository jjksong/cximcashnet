package com.bumptech.glide.provider;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.load.ResourceEncoder;
import java.util.ArrayList;
import java.util.List;

public class ResourceEncoderRegistry
{
  private final List<Entry<?>> encoders = new ArrayList();
  
  public <Z> void append(@NonNull Class<Z> paramClass, @NonNull ResourceEncoder<Z> paramResourceEncoder)
  {
    try
    {
      List localList = this.encoders;
      Entry localEntry = new com/bumptech/glide/provider/ResourceEncoderRegistry$Entry;
      localEntry.<init>(paramClass, paramResourceEncoder);
      localList.add(localEntry);
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  @Nullable
  public <Z> ResourceEncoder<Z> get(@NonNull Class<Z> paramClass)
  {
    int i = 0;
    try
    {
      int j = this.encoders.size();
      while (i < j)
      {
        Entry localEntry = (Entry)this.encoders.get(i);
        if (localEntry.handles(paramClass))
        {
          paramClass = localEntry.encoder;
          return paramClass;
        }
        i++;
      }
      return null;
    }
    finally {}
  }
  
  public <Z> void prepend(@NonNull Class<Z> paramClass, @NonNull ResourceEncoder<Z> paramResourceEncoder)
  {
    try
    {
      List localList = this.encoders;
      Entry localEntry = new com/bumptech/glide/provider/ResourceEncoderRegistry$Entry;
      localEntry.<init>(paramClass, paramResourceEncoder);
      localList.add(0, localEntry);
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  private static final class Entry<T>
  {
    final ResourceEncoder<T> encoder;
    private final Class<T> resourceClass;
    
    Entry(@NonNull Class<T> paramClass, @NonNull ResourceEncoder<T> paramResourceEncoder)
    {
      this.resourceClass = paramClass;
      this.encoder = paramResourceEncoder;
    }
    
    boolean handles(@NonNull Class<?> paramClass)
    {
      return this.resourceClass.isAssignableFrom(paramClass);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/provider/ResourceEncoderRegistry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */