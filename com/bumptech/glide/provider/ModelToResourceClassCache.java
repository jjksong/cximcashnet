package com.bumptech.glide.provider;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.bumptech.glide.util.MultiClassKey;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ModelToResourceClassCache
{
  private final ArrayMap<MultiClassKey, List<Class<?>>> registeredResourceClassCache = new ArrayMap();
  private final AtomicReference<MultiClassKey> resourceClassKeyRef = new AtomicReference();
  
  public void clear()
  {
    synchronized (this.registeredResourceClassCache)
    {
      this.registeredResourceClassCache.clear();
      return;
    }
  }
  
  @Nullable
  public List<Class<?>> get(@NonNull Class<?> paramClass1, @NonNull Class<?> arg2, @NonNull Class<?> paramClass3)
  {
    MultiClassKey localMultiClassKey = (MultiClassKey)this.resourceClassKeyRef.getAndSet(null);
    if (localMultiClassKey == null)
    {
      paramClass1 = new MultiClassKey(paramClass1, ???, paramClass3);
    }
    else
    {
      localMultiClassKey.set(paramClass1, ???, paramClass3);
      paramClass1 = localMultiClassKey;
    }
    synchronized (this.registeredResourceClassCache)
    {
      paramClass3 = (List)this.registeredResourceClassCache.get(paramClass1);
      this.resourceClassKeyRef.set(paramClass1);
      return paramClass3;
    }
  }
  
  public void put(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2, @NonNull Class<?> paramClass3, @NonNull List<Class<?>> paramList)
  {
    synchronized (this.registeredResourceClassCache)
    {
      ArrayMap localArrayMap2 = this.registeredResourceClassCache;
      MultiClassKey localMultiClassKey = new com/bumptech/glide/util/MultiClassKey;
      localMultiClassKey.<init>(paramClass1, paramClass2, paramClass3);
      localArrayMap2.put(localMultiClassKey, paramList);
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/provider/ModelToResourceClassCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */