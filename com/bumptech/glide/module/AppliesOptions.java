package com.bumptech.glide.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.bumptech.glide.GlideBuilder;

@Deprecated
abstract interface AppliesOptions
{
  public abstract void applyOptions(@NonNull Context paramContext, @NonNull GlideBuilder paramGlideBuilder);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/module/AppliesOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */