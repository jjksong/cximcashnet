package com.bumptech.glide.load.data;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.IOException;
import java.io.OutputStream;

public final class BufferedOutputStream
  extends OutputStream
{
  private ArrayPool arrayPool;
  private byte[] buffer;
  private int index;
  @NonNull
  private final OutputStream out;
  
  public BufferedOutputStream(@NonNull OutputStream paramOutputStream, @NonNull ArrayPool paramArrayPool)
  {
    this(paramOutputStream, paramArrayPool, 65536);
  }
  
  @VisibleForTesting
  BufferedOutputStream(@NonNull OutputStream paramOutputStream, ArrayPool paramArrayPool, int paramInt)
  {
    this.out = paramOutputStream;
    this.arrayPool = paramArrayPool;
    this.buffer = ((byte[])paramArrayPool.get(paramInt, byte[].class));
  }
  
  private void flushBuffer()
    throws IOException
  {
    int i = this.index;
    if (i > 0)
    {
      this.out.write(this.buffer, 0, i);
      this.index = 0;
    }
  }
  
  private void maybeFlushBuffer()
    throws IOException
  {
    if (this.index == this.buffer.length) {
      flushBuffer();
    }
  }
  
  private void release()
  {
    byte[] arrayOfByte = this.buffer;
    if (arrayOfByte != null)
    {
      this.arrayPool.put(arrayOfByte);
      this.buffer = null;
    }
  }
  
  public void close()
    throws IOException
  {
    try
    {
      flush();
      this.out.close();
      release();
      return;
    }
    finally
    {
      this.out.close();
    }
  }
  
  public void flush()
    throws IOException
  {
    flushBuffer();
    this.out.flush();
  }
  
  public void write(int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = this.buffer;
    int i = this.index;
    this.index = (i + 1);
    arrayOfByte[i] = ((byte)paramInt);
    maybeFlushBuffer();
  }
  
  public void write(@NonNull byte[] paramArrayOfByte)
    throws IOException
  {
    write(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void write(@NonNull byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = 0;
    int j;
    do
    {
      int k = paramInt2 - i;
      j = paramInt1 + i;
      if ((this.index == 0) && (k >= this.buffer.length))
      {
        this.out.write(paramArrayOfByte, j, k);
        return;
      }
      k = Math.min(k, this.buffer.length - this.index);
      System.arraycopy(paramArrayOfByte, j, this.buffer, this.index, k);
      this.index += k;
      j = i + k;
      maybeFlushBuffer();
      i = j;
    } while (j < paramInt2);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/data/BufferedOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */