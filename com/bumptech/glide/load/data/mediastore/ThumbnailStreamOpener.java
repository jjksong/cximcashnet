package com.bumptech.glide.load.data.mediastore;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

class ThumbnailStreamOpener
{
  private static final FileService DEFAULT_SERVICE = new FileService();
  private static final String TAG = "ThumbStreamOpener";
  private final ArrayPool byteArrayPool;
  private final ContentResolver contentResolver;
  private final List<ImageHeaderParser> parsers;
  private final ThumbnailQuery query;
  private final FileService service;
  
  ThumbnailStreamOpener(List<ImageHeaderParser> paramList, FileService paramFileService, ThumbnailQuery paramThumbnailQuery, ArrayPool paramArrayPool, ContentResolver paramContentResolver)
  {
    this.service = paramFileService;
    this.query = paramThumbnailQuery;
    this.byteArrayPool = paramArrayPool;
    this.contentResolver = paramContentResolver;
    this.parsers = paramList;
  }
  
  ThumbnailStreamOpener(List<ImageHeaderParser> paramList, ThumbnailQuery paramThumbnailQuery, ArrayPool paramArrayPool, ContentResolver paramContentResolver)
  {
    this(paramList, DEFAULT_SERVICE, paramThumbnailQuery, paramArrayPool, paramContentResolver);
  }
  
  @Nullable
  private String getPath(@NonNull Uri paramUri)
  {
    paramUri = this.query.query(paramUri);
    if (paramUri != null) {
      try
      {
        if (paramUri.moveToFirst())
        {
          String str = paramUri.getString(0);
          return str;
        }
      }
      finally
      {
        if (paramUri != null) {
          paramUri.close();
        }
      }
    }
    if (paramUri != null) {
      paramUri.close();
    }
    return null;
  }
  
  private boolean isValid(File paramFile)
  {
    boolean bool;
    if ((this.service.exists(paramFile)) && (0L < this.service.length(paramFile))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  /* Error */
  int getOrientation(Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: aconst_null
    //   7: astore_3
    //   8: aload_0
    //   9: getfield 40	com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener:contentResolver	Landroid/content/ContentResolver;
    //   12: aload_1
    //   13: invokevirtual 93	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   16: astore 6
    //   18: aload 6
    //   20: astore_3
    //   21: aload 6
    //   23: astore 4
    //   25: aload 6
    //   27: astore 5
    //   29: aload_0
    //   30: getfield 42	com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener:parsers	Ljava/util/List;
    //   33: aload 6
    //   35: aload_0
    //   36: getfield 38	com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener:byteArrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   39: invokestatic 98	com/bumptech/glide/load/ImageHeaderParserUtils:getOrientation	(Ljava/util/List;Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)I
    //   42: istore_2
    //   43: aload 6
    //   45: ifnull +8 -> 53
    //   48: aload 6
    //   50: invokevirtual 101	java/io/InputStream:close	()V
    //   53: iload_2
    //   54: ireturn
    //   55: astore_1
    //   56: goto +91 -> 147
    //   59: astore 6
    //   61: goto +9 -> 70
    //   64: astore 6
    //   66: aload 5
    //   68: astore 4
    //   70: aload 4
    //   72: astore_3
    //   73: ldc 10
    //   75: iconst_3
    //   76: invokestatic 107	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   79: ifeq +56 -> 135
    //   82: aload 4
    //   84: astore_3
    //   85: new 109	java/lang/StringBuilder
    //   88: astore 5
    //   90: aload 4
    //   92: astore_3
    //   93: aload 5
    //   95: invokespecial 110	java/lang/StringBuilder:<init>	()V
    //   98: aload 4
    //   100: astore_3
    //   101: aload 5
    //   103: ldc 112
    //   105: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload 4
    //   111: astore_3
    //   112: aload 5
    //   114: aload_1
    //   115: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   118: pop
    //   119: aload 4
    //   121: astore_3
    //   122: ldc 10
    //   124: aload 5
    //   126: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   129: aload 6
    //   131: invokestatic 127	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   134: pop
    //   135: aload 4
    //   137: ifnull +8 -> 145
    //   140: aload 4
    //   142: invokevirtual 101	java/io/InputStream:close	()V
    //   145: iconst_m1
    //   146: ireturn
    //   147: aload_3
    //   148: ifnull +7 -> 155
    //   151: aload_3
    //   152: invokevirtual 101	java/io/InputStream:close	()V
    //   155: aload_1
    //   156: athrow
    //   157: astore_1
    //   158: goto -105 -> 53
    //   161: astore_1
    //   162: goto -17 -> 145
    //   165: astore_3
    //   166: goto -11 -> 155
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	169	0	this	ThumbnailStreamOpener
    //   0	169	1	paramUri	Uri
    //   42	12	2	i	int
    //   7	145	3	localObject1	Object
    //   165	1	3	localIOException1	java.io.IOException
    //   1	140	4	localObject2	Object
    //   4	121	5	localObject3	Object
    //   16	33	6	localInputStream	InputStream
    //   59	1	6	localNullPointerException	NullPointerException
    //   64	66	6	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   8	18	55	finally
    //   29	43	55	finally
    //   73	82	55	finally
    //   85	90	55	finally
    //   93	98	55	finally
    //   101	109	55	finally
    //   112	119	55	finally
    //   122	135	55	finally
    //   8	18	59	java/lang/NullPointerException
    //   29	43	59	java/lang/NullPointerException
    //   8	18	64	java/io/IOException
    //   29	43	64	java/io/IOException
    //   48	53	157	java/io/IOException
    //   140	145	161	java/io/IOException
    //   151	155	165	java/io/IOException
  }
  
  public InputStream open(Uri paramUri)
    throws FileNotFoundException
  {
    Object localObject1 = getPath(paramUri);
    if (TextUtils.isEmpty((CharSequence)localObject1)) {
      return null;
    }
    localObject1 = this.service.get((String)localObject1);
    if (!isValid((File)localObject1)) {
      return null;
    }
    localObject1 = Uri.fromFile((File)localObject1);
    try
    {
      localObject2 = this.contentResolver.openInputStream((Uri)localObject1);
      return (InputStream)localObject2;
    }
    catch (NullPointerException localNullPointerException)
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("NPE opening uri: ");
      ((StringBuilder)localObject2).append(paramUri);
      ((StringBuilder)localObject2).append(" -> ");
      ((StringBuilder)localObject2).append(localObject1);
      throw ((FileNotFoundException)new FileNotFoundException(((StringBuilder)localObject2).toString()).initCause(localNullPointerException));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */