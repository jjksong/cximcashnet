package com.bumptech.glide.load.data.mediastore;

import android.database.Cursor;
import android.net.Uri;

abstract interface ThumbnailQuery
{
  public abstract Cursor query(Uri paramUri);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/data/mediastore/ThumbnailQuery.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */