package com.bumptech.glide.load.data;

import android.support.annotation.NonNull;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ExifOrientationStream
  extends FilterInputStream
{
  private static final byte[] EXIF_SEGMENT = { -1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, 18, 0, 2, 0, 0, 0, 1, 0 };
  private static final int ORIENTATION_POSITION = SEGMENT_LENGTH + 2;
  private static final int SEGMENT_LENGTH = EXIF_SEGMENT.length;
  private static final int SEGMENT_START_POSITION = 2;
  private final byte orientation;
  private int position;
  
  public ExifOrientationStream(InputStream paramInputStream, int paramInt)
  {
    super(paramInputStream);
    if ((paramInt >= -1) && (paramInt <= 8))
    {
      this.orientation = ((byte)paramInt);
      return;
    }
    paramInputStream = new StringBuilder();
    paramInputStream.append("Cannot add invalid orientation: ");
    paramInputStream.append(paramInt);
    throw new IllegalArgumentException(paramInputStream.toString());
  }
  
  public void mark(int paramInt)
  {
    throw new UnsupportedOperationException();
  }
  
  public boolean markSupported()
  {
    return false;
  }
  
  public int read()
    throws IOException
  {
    int j = this.position;
    if (j >= 2)
    {
      i = ORIENTATION_POSITION;
      if (j <= i)
      {
        if (j == i)
        {
          i = this.orientation;
          break label55;
        }
        i = EXIF_SEGMENT[(j - 2)] & 0xFF;
        break label55;
      }
    }
    int i = super.read();
    label55:
    if (i != -1) {
      this.position += 1;
    }
    return i;
  }
  
  public int read(@NonNull byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = this.position;
    int j = ORIENTATION_POSITION;
    if (i > j)
    {
      paramInt1 = super.read(paramArrayOfByte, paramInt1, paramInt2);
    }
    else if (i == j)
    {
      paramArrayOfByte[paramInt1] = this.orientation;
      paramInt1 = 1;
    }
    else if (i < 2)
    {
      paramInt1 = super.read(paramArrayOfByte, paramInt1, 2 - i);
    }
    else
    {
      paramInt2 = Math.min(j - i, paramInt2);
      System.arraycopy(EXIF_SEGMENT, this.position - 2, paramArrayOfByte, paramInt1, paramInt2);
      paramInt1 = paramInt2;
    }
    if (paramInt1 > 0) {
      this.position += paramInt1;
    }
    return paramInt1;
  }
  
  public void reset()
    throws IOException
  {
    throw new UnsupportedOperationException();
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    paramLong = super.skip(paramLong);
    if (paramLong > 0L) {
      this.position = ((int)(this.position + paramLong));
    }
    return paramLong;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/data/ExifOrientationStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */