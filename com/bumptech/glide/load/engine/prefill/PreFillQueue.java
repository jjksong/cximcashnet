package com.bumptech.glide.load.engine.prefill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class PreFillQueue
{
  private final Map<PreFillType, Integer> bitmapsPerType;
  private int bitmapsRemaining;
  private int keyIndex;
  private final List<PreFillType> keyList;
  
  public PreFillQueue(Map<PreFillType, Integer> paramMap)
  {
    this.bitmapsPerType = paramMap;
    this.keyList = new ArrayList(paramMap.keySet());
    Iterator localIterator = paramMap.values().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Integer)localIterator.next();
      this.bitmapsRemaining += paramMap.intValue();
    }
  }
  
  public int getSize()
  {
    return this.bitmapsRemaining;
  }
  
  public boolean isEmpty()
  {
    boolean bool;
    if (this.bitmapsRemaining == 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public PreFillType remove()
  {
    PreFillType localPreFillType = (PreFillType)this.keyList.get(this.keyIndex);
    Integer localInteger = (Integer)this.bitmapsPerType.get(localPreFillType);
    if (localInteger.intValue() == 1)
    {
      this.bitmapsPerType.remove(localPreFillType);
      this.keyList.remove(this.keyIndex);
    }
    else
    {
      this.bitmapsPerType.put(localPreFillType, Integer.valueOf(localInteger.intValue() - 1));
    }
    this.bitmapsRemaining -= 1;
    int i;
    if (this.keyList.isEmpty()) {
      i = 0;
    } else {
      i = (this.keyIndex + 1) % this.keyList.size();
    }
    this.keyIndex = i;
    return localPreFillType;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/prefill/PreFillQueue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */