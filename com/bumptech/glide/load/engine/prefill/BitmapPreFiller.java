package com.bumptech.glide.load.engine.prefill;

import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.util.Util;
import java.util.HashMap;
import java.util.Map;

public final class BitmapPreFiller
{
  private final BitmapPool bitmapPool;
  private BitmapPreFillRunner current;
  private final DecodeFormat defaultFormat;
  private final Handler handler = new Handler(Looper.getMainLooper());
  private final MemoryCache memoryCache;
  
  public BitmapPreFiller(MemoryCache paramMemoryCache, BitmapPool paramBitmapPool, DecodeFormat paramDecodeFormat)
  {
    this.memoryCache = paramMemoryCache;
    this.bitmapPool = paramBitmapPool;
    this.defaultFormat = paramDecodeFormat;
  }
  
  private static int getSizeInBytes(PreFillType paramPreFillType)
  {
    return Util.getBitmapByteSize(paramPreFillType.getWidth(), paramPreFillType.getHeight(), paramPreFillType.getConfig());
  }
  
  @VisibleForTesting
  PreFillQueue generateAllocationOrder(PreFillType... paramVarArgs)
  {
    long l2 = this.memoryCache.getMaxSize();
    long l1 = this.memoryCache.getCurrentSize();
    long l3 = this.bitmapPool.getMaxSize();
    int m = paramVarArgs.length;
    int k = 0;
    int j = 0;
    int i = 0;
    while (j < m)
    {
      i += paramVarArgs[j].getWeight();
      j++;
    }
    float f = (float)(l2 - l1 + l3) / i;
    HashMap localHashMap = new HashMap();
    j = paramVarArgs.length;
    for (i = k; i < j; i++)
    {
      PreFillType localPreFillType = paramVarArgs[i];
      localHashMap.put(localPreFillType, Integer.valueOf(Math.round(localPreFillType.getWeight() * f) / getSizeInBytes(localPreFillType)));
    }
    return new PreFillQueue(localHashMap);
  }
  
  public void preFill(PreFillType.Builder... paramVarArgs)
  {
    Object localObject = this.current;
    if (localObject != null) {
      ((BitmapPreFillRunner)localObject).cancel();
    }
    PreFillType[] arrayOfPreFillType = new PreFillType[paramVarArgs.length];
    for (int i = 0; i < paramVarArgs.length; i++)
    {
      PreFillType.Builder localBuilder = paramVarArgs[i];
      if (localBuilder.getConfig() == null)
      {
        if (this.defaultFormat == DecodeFormat.PREFER_ARGB_8888) {
          localObject = Bitmap.Config.ARGB_8888;
        } else {
          localObject = Bitmap.Config.RGB_565;
        }
        localBuilder.setConfig((Bitmap.Config)localObject);
      }
      arrayOfPreFillType[i] = localBuilder.build();
    }
    paramVarArgs = generateAllocationOrder(arrayOfPreFillType);
    this.current = new BitmapPreFillRunner(this.bitmapPool, this.memoryCache, paramVarArgs);
    this.handler.post(this.current);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/prefill/BitmapPreFiller.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */