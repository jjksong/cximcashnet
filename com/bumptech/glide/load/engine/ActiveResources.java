package com.bumptech.glide.load.engine;

import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

final class ActiveResources
{
  @VisibleForTesting
  final Map<Key, ResourceWeakReference> activeEngineResources = new HashMap();
  @Nullable
  private volatile DequeuedResourceCallback cb;
  private final boolean isActiveResourceRetentionAllowed;
  private volatile boolean isShutdown;
  private EngineResource.ResourceListener listener;
  private final Executor monitorClearedResourcesExecutor;
  private final ReferenceQueue<EngineResource<?>> resourceReferenceQueue = new ReferenceQueue();
  
  ActiveResources(boolean paramBoolean)
  {
    this(paramBoolean, java.util.concurrent.Executors.newSingleThreadExecutor(new ThreadFactory()
    {
      public Thread newThread(@NonNull final Runnable paramAnonymousRunnable)
      {
        new Thread(new Runnable()
        {
          public void run()
          {
            Process.setThreadPriority(10);
            paramAnonymousRunnable.run();
          }
        }, "glide-active-resources");
      }
    }));
  }
  
  @VisibleForTesting
  ActiveResources(boolean paramBoolean, Executor paramExecutor)
  {
    this.isActiveResourceRetentionAllowed = paramBoolean;
    this.monitorClearedResourcesExecutor = paramExecutor;
    paramExecutor.execute(new Runnable()
    {
      public void run()
      {
        ActiveResources.this.cleanReferenceQueue();
      }
    });
  }
  
  void activate(Key paramKey, EngineResource<?> paramEngineResource)
  {
    try
    {
      ResourceWeakReference localResourceWeakReference = new com/bumptech/glide/load/engine/ActiveResources$ResourceWeakReference;
      localResourceWeakReference.<init>(paramKey, paramEngineResource, this.resourceReferenceQueue, this.isActiveResourceRetentionAllowed);
      paramKey = (ResourceWeakReference)this.activeEngineResources.put(paramKey, localResourceWeakReference);
      if (paramKey != null) {
        paramKey.reset();
      }
      return;
    }
    finally {}
  }
  
  void cleanReferenceQueue()
  {
    while (!this.isShutdown) {
      try
      {
        cleanupActiveReference((ResourceWeakReference)this.resourceReferenceQueue.remove());
        DequeuedResourceCallback localDequeuedResourceCallback = this.cb;
        if (localDequeuedResourceCallback != null) {
          localDequeuedResourceCallback.onResourceDequeued();
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        Thread.currentThread().interrupt();
      }
    }
  }
  
  void cleanupActiveReference(@NonNull ResourceWeakReference paramResourceWeakReference)
  {
    synchronized (this.listener)
    {
      try
      {
        this.activeEngineResources.remove(paramResourceWeakReference.key);
        if ((paramResourceWeakReference.isCacheable) && (paramResourceWeakReference.resource != null))
        {
          EngineResource localEngineResource = new com/bumptech/glide/load/engine/EngineResource;
          localEngineResource.<init>(paramResourceWeakReference.resource, true, false);
          localEngineResource.setResourceListener(paramResourceWeakReference.key, this.listener);
          this.listener.onResourceReleased(paramResourceWeakReference.key, localEngineResource);
          return;
        }
        return;
      }
      finally {}
    }
  }
  
  void deactivate(Key paramKey)
  {
    try
    {
      paramKey = (ResourceWeakReference)this.activeEngineResources.remove(paramKey);
      if (paramKey != null) {
        paramKey.reset();
      }
      return;
    }
    finally {}
  }
  
  @Nullable
  EngineResource<?> get(Key paramKey)
  {
    try
    {
      paramKey = (ResourceWeakReference)this.activeEngineResources.get(paramKey);
      if (paramKey == null) {
        return null;
      }
      EngineResource localEngineResource = (EngineResource)paramKey.get();
      if (localEngineResource == null) {
        cleanupActiveReference(paramKey);
      }
      return localEngineResource;
    }
    finally {}
  }
  
  @VisibleForTesting
  void setDequeuedResourceCallback(DequeuedResourceCallback paramDequeuedResourceCallback)
  {
    this.cb = paramDequeuedResourceCallback;
  }
  
  /* Error */
  void setListener(EngineResource.ResourceListener paramResourceListener)
  {
    // Byte code:
    //   0: aload_1
    //   1: monitorenter
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: aload_1
    //   6: putfield 119	com/bumptech/glide/load/engine/ActiveResources:listener	Lcom/bumptech/glide/load/engine/EngineResource$ResourceListener;
    //   9: aload_0
    //   10: monitorexit
    //   11: aload_1
    //   12: monitorexit
    //   13: return
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    //   19: astore_2
    //   20: aload_1
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	24	0	this	ActiveResources
    //   0	24	1	paramResourceListener	EngineResource.ResourceListener
    //   14	4	2	localObject1	Object
    //   19	4	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   4	11	14	finally
    //   15	17	14	finally
    //   2	4	19	finally
    //   11	13	19	finally
    //   17	19	19	finally
    //   20	22	19	finally
  }
  
  @VisibleForTesting
  void shutdown()
  {
    this.isShutdown = true;
    Executor localExecutor = this.monitorClearedResourcesExecutor;
    if ((localExecutor instanceof ExecutorService)) {
      com.bumptech.glide.util.Executors.shutdownAndAwaitTermination((ExecutorService)localExecutor);
    }
  }
  
  @VisibleForTesting
  static abstract interface DequeuedResourceCallback
  {
    public abstract void onResourceDequeued();
  }
  
  @VisibleForTesting
  static final class ResourceWeakReference
    extends WeakReference<EngineResource<?>>
  {
    final boolean isCacheable;
    final Key key;
    @Nullable
    Resource<?> resource;
    
    ResourceWeakReference(@NonNull Key paramKey, @NonNull EngineResource<?> paramEngineResource, @NonNull ReferenceQueue<? super EngineResource<?>> paramReferenceQueue, boolean paramBoolean)
    {
      super(paramReferenceQueue);
      this.key = ((Key)Preconditions.checkNotNull(paramKey));
      if ((paramEngineResource.isCacheable()) && (paramBoolean)) {
        paramKey = (Resource)Preconditions.checkNotNull(paramEngineResource.getResource());
      } else {
        paramKey = null;
      }
      this.resource = paramKey;
      this.isCacheable = paramEngineResource.isCacheable();
    }
    
    void reset()
    {
      this.resource = null;
      clear();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/ActiveResources.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */