package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;

class ResourceRecycler
{
  private final Handler handler = new Handler(Looper.getMainLooper(), new ResourceRecyclerCallback());
  private boolean isRecycling;
  
  void recycle(Resource<?> paramResource)
  {
    try
    {
      if (this.isRecycling)
      {
        this.handler.obtainMessage(1, paramResource).sendToTarget();
      }
      else
      {
        this.isRecycling = true;
        paramResource.recycle();
        this.isRecycling = false;
      }
      return;
    }
    finally {}
  }
  
  private static final class ResourceRecyclerCallback
    implements Handler.Callback
  {
    static final int RECYCLE_RESOURCE = 1;
    
    public boolean handleMessage(Message paramMessage)
    {
      if (paramMessage.what == 1)
      {
        ((Resource)paramMessage.obj).recycle();
        return true;
      }
      return false;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/ResourceRecycler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */