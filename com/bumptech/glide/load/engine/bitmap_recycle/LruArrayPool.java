package com.bumptech.glide.load.engine.bitmap_recycle;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

public final class LruArrayPool
  implements ArrayPool
{
  private static final int DEFAULT_SIZE = 4194304;
  @VisibleForTesting
  static final int MAX_OVER_SIZE_MULTIPLE = 8;
  private static final int SINGLE_ARRAY_MAX_SIZE_DIVISOR = 2;
  private final Map<Class<?>, ArrayAdapterInterface<?>> adapters = new HashMap();
  private int currentSize;
  private final GroupedLinkedMap<Key, Object> groupedMap = new GroupedLinkedMap();
  private final KeyPool keyPool = new KeyPool();
  private final int maxSize;
  private final Map<Class<?>, NavigableMap<Integer, Integer>> sortedSizes = new HashMap();
  
  @VisibleForTesting
  public LruArrayPool()
  {
    this.maxSize = 4194304;
  }
  
  public LruArrayPool(int paramInt)
  {
    this.maxSize = paramInt;
  }
  
  private void decrementArrayOfSize(int paramInt, Class<?> paramClass)
  {
    NavigableMap localNavigableMap = getSizesForAdapter(paramClass);
    paramClass = (Integer)localNavigableMap.get(Integer.valueOf(paramInt));
    if (paramClass != null)
    {
      if (paramClass.intValue() == 1) {
        localNavigableMap.remove(Integer.valueOf(paramInt));
      } else {
        localNavigableMap.put(Integer.valueOf(paramInt), Integer.valueOf(paramClass.intValue() - 1));
      }
      return;
    }
    paramClass = new StringBuilder();
    paramClass.append("Tried to decrement empty size, size: ");
    paramClass.append(paramInt);
    paramClass.append(", this: ");
    paramClass.append(this);
    throw new NullPointerException(paramClass.toString());
  }
  
  private void evict()
  {
    evictToSize(this.maxSize);
  }
  
  private void evictToSize(int paramInt)
  {
    while (this.currentSize > paramInt)
    {
      Object localObject = this.groupedMap.removeLast();
      Preconditions.checkNotNull(localObject);
      ArrayAdapterInterface localArrayAdapterInterface = getAdapterFromObject(localObject);
      this.currentSize -= localArrayAdapterInterface.getArrayLength(localObject) * localArrayAdapterInterface.getElementSizeInBytes();
      decrementArrayOfSize(localArrayAdapterInterface.getArrayLength(localObject), localObject.getClass());
      if (Log.isLoggable(localArrayAdapterInterface.getTag(), 2))
      {
        String str = localArrayAdapterInterface.getTag();
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("evicted: ");
        localStringBuilder.append(localArrayAdapterInterface.getArrayLength(localObject));
        Log.v(str, localStringBuilder.toString());
      }
    }
  }
  
  private <T> ArrayAdapterInterface<T> getAdapterFromObject(T paramT)
  {
    return getAdapterFromType(paramT.getClass());
  }
  
  private <T> ArrayAdapterInterface<T> getAdapterFromType(Class<T> paramClass)
  {
    ArrayAdapterInterface localArrayAdapterInterface = (ArrayAdapterInterface)this.adapters.get(paramClass);
    Object localObject = localArrayAdapterInterface;
    if (localArrayAdapterInterface == null)
    {
      if (paramClass.equals(int[].class))
      {
        localObject = new IntegerArrayAdapter();
      }
      else
      {
        if (!paramClass.equals(byte[].class)) {
          break label72;
        }
        localObject = new ByteArrayAdapter();
      }
      this.adapters.put(paramClass, localObject);
      return (ArrayAdapterInterface<T>)localObject;
      label72:
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("No array pool found for: ");
      ((StringBuilder)localObject).append(paramClass.getSimpleName());
      throw new IllegalArgumentException(((StringBuilder)localObject).toString());
    }
    return (ArrayAdapterInterface<T>)localObject;
  }
  
  @Nullable
  private <T> T getArrayForKey(Key paramKey)
  {
    return (T)this.groupedMap.get(paramKey);
  }
  
  private <T> T getForKey(Key paramKey, Class<T> paramClass)
  {
    ArrayAdapterInterface localArrayAdapterInterface = getAdapterFromType(paramClass);
    Object localObject = getArrayForKey(paramKey);
    if (localObject != null)
    {
      this.currentSize -= localArrayAdapterInterface.getArrayLength(localObject) * localArrayAdapterInterface.getElementSizeInBytes();
      decrementArrayOfSize(localArrayAdapterInterface.getArrayLength(localObject), paramClass);
    }
    paramClass = (Class<T>)localObject;
    if (localObject == null)
    {
      if (Log.isLoggable(localArrayAdapterInterface.getTag(), 2))
      {
        localObject = localArrayAdapterInterface.getTag();
        paramClass = new StringBuilder();
        paramClass.append("Allocated ");
        paramClass.append(paramKey.size);
        paramClass.append(" bytes");
        Log.v((String)localObject, paramClass.toString());
      }
      paramClass = localArrayAdapterInterface.newArray(paramKey.size);
    }
    return paramClass;
  }
  
  private NavigableMap<Integer, Integer> getSizesForAdapter(Class<?> paramClass)
  {
    NavigableMap localNavigableMap = (NavigableMap)this.sortedSizes.get(paramClass);
    Object localObject = localNavigableMap;
    if (localNavigableMap == null)
    {
      localObject = new TreeMap();
      this.sortedSizes.put(paramClass, localObject);
    }
    return (NavigableMap<Integer, Integer>)localObject;
  }
  
  private boolean isNoMoreThanHalfFull()
  {
    int i = this.currentSize;
    boolean bool;
    if ((i != 0) && (this.maxSize / i < 2)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isSmallEnoughForReuse(int paramInt)
  {
    boolean bool;
    if (paramInt <= this.maxSize / 2) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean mayFillRequest(int paramInt, Integer paramInteger)
  {
    boolean bool;
    if ((paramInteger != null) && ((isNoMoreThanHalfFull()) || (paramInteger.intValue() <= paramInt * 8))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void clearMemory()
  {
    try
    {
      evictToSize(0);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public <T> T get(int paramInt, Class<T> paramClass)
  {
    try
    {
      Object localObject = (Integer)getSizesForAdapter(paramClass).ceilingKey(Integer.valueOf(paramInt));
      if (mayFillRequest(paramInt, (Integer)localObject)) {
        localObject = this.keyPool.get(((Integer)localObject).intValue(), paramClass);
      } else {
        localObject = this.keyPool.get(paramInt, paramClass);
      }
      paramClass = getForKey((Key)localObject, paramClass);
      return paramClass;
    }
    finally {}
  }
  
  int getCurrentSize()
  {
    Iterator localIterator2 = this.sortedSizes.keySet().iterator();
    int i = 0;
    if (localIterator2.hasNext())
    {
      Class localClass = (Class)localIterator2.next();
      Iterator localIterator1 = ((NavigableMap)this.sortedSizes.get(localClass)).keySet().iterator();
      int j = i;
      for (;;)
      {
        i = j;
        if (!localIterator1.hasNext()) {
          break;
        }
        Integer localInteger = (Integer)localIterator1.next();
        ArrayAdapterInterface localArrayAdapterInterface = getAdapterFromType(localClass);
        j += localInteger.intValue() * ((Integer)((NavigableMap)this.sortedSizes.get(localClass)).get(localInteger)).intValue() * localArrayAdapterInterface.getElementSizeInBytes();
      }
    }
    return i;
  }
  
  public <T> T getExact(int paramInt, Class<T> paramClass)
  {
    try
    {
      paramClass = getForKey(this.keyPool.get(paramInt, paramClass), paramClass);
      return paramClass;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  public <T> void put(T paramT)
  {
    try
    {
      Object localObject2 = paramT.getClass();
      Object localObject1 = getAdapterFromType((Class)localObject2);
      int i = ((ArrayAdapterInterface)localObject1).getArrayLength(paramT);
      int j = ((ArrayAdapterInterface)localObject1).getElementSizeInBytes() * i;
      boolean bool = isSmallEnoughForReuse(j);
      if (!bool) {
        return;
      }
      localObject1 = this.keyPool.get(i, (Class)localObject2);
      this.groupedMap.put((Poolable)localObject1, paramT);
      localObject2 = getSizesForAdapter((Class)localObject2);
      paramT = (Integer)((NavigableMap)localObject2).get(Integer.valueOf(((Key)localObject1).size));
      int k = ((Key)localObject1).size;
      i = 1;
      if (paramT != null) {
        i = 1 + paramT.intValue();
      }
      ((NavigableMap)localObject2).put(Integer.valueOf(k), Integer.valueOf(i));
      this.currentSize += j;
      evict();
      return;
    }
    finally {}
  }
  
  @Deprecated
  public <T> void put(T paramT, Class<T> paramClass)
  {
    put(paramT);
  }
  
  public void trimMemory(int paramInt)
  {
    if (paramInt >= 40) {
      try
      {
        clearMemory();
      }
      finally
      {
        break label44;
      }
    } else if ((paramInt >= 20) || (paramInt == 15)) {
      evictToSize(this.maxSize / 2);
    }
    return;
    label44:
    throw ((Throwable)localObject);
  }
  
  private static final class Key
    implements Poolable
  {
    private Class<?> arrayClass;
    private final LruArrayPool.KeyPool pool;
    int size;
    
    Key(LruArrayPool.KeyPool paramKeyPool)
    {
      this.pool = paramKeyPool;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool1 = paramObject instanceof Key;
      boolean bool2 = false;
      if (bool1)
      {
        paramObject = (Key)paramObject;
        bool1 = bool2;
        if (this.size == ((Key)paramObject).size)
        {
          bool1 = bool2;
          if (this.arrayClass == ((Key)paramObject).arrayClass) {
            bool1 = true;
          }
        }
        return bool1;
      }
      return false;
    }
    
    public int hashCode()
    {
      int j = this.size;
      Class localClass = this.arrayClass;
      int i;
      if (localClass != null) {
        i = localClass.hashCode();
      } else {
        i = 0;
      }
      return j * 31 + i;
    }
    
    void init(int paramInt, Class<?> paramClass)
    {
      this.size = paramInt;
      this.arrayClass = paramClass;
    }
    
    public void offer()
    {
      this.pool.offer(this);
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Key{size=");
      localStringBuilder.append(this.size);
      localStringBuilder.append("array=");
      localStringBuilder.append(this.arrayClass);
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
  
  private static final class KeyPool
    extends BaseKeyPool<LruArrayPool.Key>
  {
    protected LruArrayPool.Key create()
    {
      return new LruArrayPool.Key(this);
    }
    
    LruArrayPool.Key get(int paramInt, Class<?> paramClass)
    {
      LruArrayPool.Key localKey = (LruArrayPool.Key)get();
      localKey.init(paramInt, paramClass);
      return localKey;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/bitmap_recycle/LruArrayPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */