package com.bumptech.glide.load.engine.bitmap_recycle;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class LruBitmapPool
  implements BitmapPool
{
  private static final Bitmap.Config DEFAULT_CONFIG = Bitmap.Config.ARGB_8888;
  private static final String TAG = "LruBitmapPool";
  private final Set<Bitmap.Config> allowedConfigs;
  private long currentSize;
  private int evictions;
  private int hits;
  private final long initialMaxSize;
  private long maxSize;
  private int misses;
  private int puts;
  private final LruPoolStrategy strategy;
  private final BitmapTracker tracker;
  
  public LruBitmapPool(long paramLong)
  {
    this(paramLong, getDefaultStrategy(), getDefaultAllowedConfigs());
  }
  
  LruBitmapPool(long paramLong, LruPoolStrategy paramLruPoolStrategy, Set<Bitmap.Config> paramSet)
  {
    this.initialMaxSize = paramLong;
    this.maxSize = paramLong;
    this.strategy = paramLruPoolStrategy;
    this.allowedConfigs = paramSet;
    this.tracker = new NullBitmapTracker();
  }
  
  public LruBitmapPool(long paramLong, Set<Bitmap.Config> paramSet)
  {
    this(paramLong, getDefaultStrategy(), paramSet);
  }
  
  @TargetApi(26)
  private static void assertNotHardwareConfig(Bitmap.Config paramConfig)
  {
    if (Build.VERSION.SDK_INT < 26) {
      return;
    }
    if (paramConfig != Bitmap.Config.HARDWARE) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Cannot create a mutable Bitmap with config: ");
    localStringBuilder.append(paramConfig);
    localStringBuilder.append(". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  @NonNull
  private static Bitmap createBitmap(int paramInt1, int paramInt2, @Nullable Bitmap.Config paramConfig)
  {
    if (paramConfig == null) {
      paramConfig = DEFAULT_CONFIG;
    }
    return Bitmap.createBitmap(paramInt1, paramInt2, paramConfig);
  }
  
  private void dump()
  {
    if (Log.isLoggable("LruBitmapPool", 2)) {
      dumpUnchecked();
    }
  }
  
  private void dumpUnchecked()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Hits=");
    localStringBuilder.append(this.hits);
    localStringBuilder.append(", misses=");
    localStringBuilder.append(this.misses);
    localStringBuilder.append(", puts=");
    localStringBuilder.append(this.puts);
    localStringBuilder.append(", evictions=");
    localStringBuilder.append(this.evictions);
    localStringBuilder.append(", currentSize=");
    localStringBuilder.append(this.currentSize);
    localStringBuilder.append(", maxSize=");
    localStringBuilder.append(this.maxSize);
    localStringBuilder.append("\nStrategy=");
    localStringBuilder.append(this.strategy);
    Log.v("LruBitmapPool", localStringBuilder.toString());
  }
  
  private void evict()
  {
    trimToSize(this.maxSize);
  }
  
  @TargetApi(26)
  private static Set<Bitmap.Config> getDefaultAllowedConfigs()
  {
    HashSet localHashSet = new HashSet(Arrays.asList(Bitmap.Config.values()));
    if (Build.VERSION.SDK_INT >= 19) {
      localHashSet.add(null);
    }
    if (Build.VERSION.SDK_INT >= 26) {
      localHashSet.remove(Bitmap.Config.HARDWARE);
    }
    return Collections.unmodifiableSet(localHashSet);
  }
  
  private static LruPoolStrategy getDefaultStrategy()
  {
    Object localObject;
    if (Build.VERSION.SDK_INT >= 19) {
      localObject = new SizeConfigStrategy();
    } else {
      localObject = new AttributeStrategy();
    }
    return (LruPoolStrategy)localObject;
  }
  
  @Nullable
  private Bitmap getDirtyOrNull(int paramInt1, int paramInt2, @Nullable Bitmap.Config paramConfig)
  {
    try
    {
      assertNotHardwareConfig(paramConfig);
      Object localObject2 = this.strategy;
      if (paramConfig != null) {
        localObject1 = paramConfig;
      } else {
        localObject1 = DEFAULT_CONFIG;
      }
      Object localObject1 = ((LruPoolStrategy)localObject2).get(paramInt1, paramInt2, (Bitmap.Config)localObject1);
      if (localObject1 == null)
      {
        if (Log.isLoggable("LruBitmapPool", 3))
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append("Missing bitmap=");
          ((StringBuilder)localObject2).append(this.strategy.logBitmap(paramInt1, paramInt2, paramConfig));
          Log.d("LruBitmapPool", ((StringBuilder)localObject2).toString());
        }
        this.misses += 1;
      }
      else
      {
        this.hits += 1;
        this.currentSize -= this.strategy.getSize((Bitmap)localObject1);
        this.tracker.remove((Bitmap)localObject1);
        normalize((Bitmap)localObject1);
      }
      if (Log.isLoggable("LruBitmapPool", 2))
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append("Get bitmap=");
        ((StringBuilder)localObject2).append(this.strategy.logBitmap(paramInt1, paramInt2, paramConfig));
        Log.v("LruBitmapPool", ((StringBuilder)localObject2).toString());
      }
      dump();
      return (Bitmap)localObject1;
    }
    finally {}
  }
  
  @TargetApi(19)
  private static void maybeSetPreMultiplied(Bitmap paramBitmap)
  {
    if (Build.VERSION.SDK_INT >= 19) {
      paramBitmap.setPremultiplied(true);
    }
  }
  
  private static void normalize(Bitmap paramBitmap)
  {
    paramBitmap.setHasAlpha(true);
    maybeSetPreMultiplied(paramBitmap);
  }
  
  private void trimToSize(long paramLong)
  {
    try
    {
      while (this.currentSize > paramLong)
      {
        Bitmap localBitmap = this.strategy.removeLast();
        if (localBitmap == null)
        {
          if (Log.isLoggable("LruBitmapPool", 5))
          {
            Log.w("LruBitmapPool", "Size mismatch, resetting");
            dumpUnchecked();
          }
          this.currentSize = 0L;
          return;
        }
        this.tracker.remove(localBitmap);
        this.currentSize -= this.strategy.getSize(localBitmap);
        this.evictions += 1;
        if (Log.isLoggable("LruBitmapPool", 3))
        {
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Evicting bitmap=");
          localStringBuilder.append(this.strategy.logBitmap(localBitmap));
          Log.d("LruBitmapPool", localStringBuilder.toString());
        }
        dump();
        localBitmap.recycle();
      }
      return;
    }
    finally {}
  }
  
  public void clearMemory()
  {
    if (Log.isLoggable("LruBitmapPool", 3)) {
      Log.d("LruBitmapPool", "clearMemory");
    }
    trimToSize(0L);
  }
  
  @NonNull
  public Bitmap get(int paramInt1, int paramInt2, Bitmap.Config paramConfig)
  {
    Bitmap localBitmap = getDirtyOrNull(paramInt1, paramInt2, paramConfig);
    if (localBitmap != null)
    {
      localBitmap.eraseColor(0);
      paramConfig = localBitmap;
    }
    else
    {
      paramConfig = createBitmap(paramInt1, paramInt2, paramConfig);
    }
    return paramConfig;
  }
  
  @NonNull
  public Bitmap getDirty(int paramInt1, int paramInt2, Bitmap.Config paramConfig)
  {
    Bitmap localBitmap2 = getDirtyOrNull(paramInt1, paramInt2, paramConfig);
    Bitmap localBitmap1 = localBitmap2;
    if (localBitmap2 == null) {
      localBitmap1 = createBitmap(paramInt1, paramInt2, paramConfig);
    }
    return localBitmap1;
  }
  
  public long getMaxSize()
  {
    return this.maxSize;
  }
  
  public void put(Bitmap paramBitmap)
  {
    if (paramBitmap != null) {
      try
      {
        if (!paramBitmap.isRecycled())
        {
          StringBuilder localStringBuilder;
          if ((paramBitmap.isMutable()) && (this.strategy.getSize(paramBitmap) <= this.maxSize) && (this.allowedConfigs.contains(paramBitmap.getConfig())))
          {
            int i = this.strategy.getSize(paramBitmap);
            this.strategy.put(paramBitmap);
            this.tracker.add(paramBitmap);
            this.puts += 1;
            this.currentSize += i;
            if (Log.isLoggable("LruBitmapPool", 2))
            {
              localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>();
              localStringBuilder.append("Put bitmap in pool=");
              localStringBuilder.append(this.strategy.logBitmap(paramBitmap));
              Log.v("LruBitmapPool", localStringBuilder.toString());
            }
            dump();
            evict();
            return;
          }
          if (Log.isLoggable("LruBitmapPool", 2))
          {
            localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Reject bitmap from pool, bitmap: ");
            localStringBuilder.append(this.strategy.logBitmap(paramBitmap));
            localStringBuilder.append(", is mutable: ");
            localStringBuilder.append(paramBitmap.isMutable());
            localStringBuilder.append(", is allowed config: ");
            localStringBuilder.append(this.allowedConfigs.contains(paramBitmap.getConfig()));
            Log.v("LruBitmapPool", localStringBuilder.toString());
          }
          paramBitmap.recycle();
          return;
        }
        paramBitmap = new java/lang/IllegalStateException;
        paramBitmap.<init>("Cannot pool recycled bitmap");
        throw paramBitmap;
      }
      finally
      {
        break label301;
      }
    }
    paramBitmap = new java/lang/NullPointerException;
    paramBitmap.<init>("Bitmap must not be null");
    throw paramBitmap;
    label301:
    throw paramBitmap;
  }
  
  public void setSizeMultiplier(float paramFloat)
  {
    try
    {
      this.maxSize = Math.round((float)this.initialMaxSize * paramFloat);
      evict();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  @SuppressLint({"InlinedApi"})
  public void trimMemory(int paramInt)
  {
    if (Log.isLoggable("LruBitmapPool", 3))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("trimMemory, level=");
      localStringBuilder.append(paramInt);
      Log.d("LruBitmapPool", localStringBuilder.toString());
    }
    if (paramInt >= 40) {
      clearMemory();
    } else if ((paramInt >= 20) || (paramInt == 15)) {
      trimToSize(getMaxSize() / 2L);
    }
  }
  
  private static abstract interface BitmapTracker
  {
    public abstract void add(Bitmap paramBitmap);
    
    public abstract void remove(Bitmap paramBitmap);
  }
  
  private static final class NullBitmapTracker
    implements LruBitmapPool.BitmapTracker
  {
    public void add(Bitmap paramBitmap) {}
    
    public void remove(Bitmap paramBitmap) {}
  }
  
  private static class ThrowingBitmapTracker
    implements LruBitmapPool.BitmapTracker
  {
    private final Set<Bitmap> bitmaps = Collections.synchronizedSet(new HashSet());
    
    public void add(Bitmap paramBitmap)
    {
      if (!this.bitmaps.contains(paramBitmap))
      {
        this.bitmaps.add(paramBitmap);
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Can't add already added bitmap: ");
      localStringBuilder.append(paramBitmap);
      localStringBuilder.append(" [");
      localStringBuilder.append(paramBitmap.getWidth());
      localStringBuilder.append("x");
      localStringBuilder.append(paramBitmap.getHeight());
      localStringBuilder.append("]");
      throw new IllegalStateException(localStringBuilder.toString());
    }
    
    public void remove(Bitmap paramBitmap)
    {
      if (this.bitmaps.contains(paramBitmap))
      {
        this.bitmaps.remove(paramBitmap);
        return;
      }
      throw new IllegalStateException("Cannot remove bitmap not in tracker");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/bitmap_recycle/LruBitmapPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */