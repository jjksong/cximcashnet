package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;

public abstract interface Resource<Z>
{
  @NonNull
  public abstract Z get();
  
  @NonNull
  public abstract Class<Z> getResourceClass();
  
  public abstract int getSize();
  
  public abstract void recycle();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/Resource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */