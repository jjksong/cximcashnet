package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.DataFetcher.DataCallback;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoader.LoadData;
import java.io.File;
import java.util.List;

class ResourceCacheGenerator
  implements DataFetcherGenerator, DataFetcher.DataCallback<Object>
{
  private File cacheFile;
  private final DataFetcherGenerator.FetcherReadyCallback cb;
  private ResourceCacheKey currentKey;
  private final DecodeHelper<?> helper;
  private volatile ModelLoader.LoadData<?> loadData;
  private int modelLoaderIndex;
  private List<ModelLoader<File, ?>> modelLoaders;
  private int resourceClassIndex = -1;
  private int sourceIdIndex;
  private Key sourceKey;
  
  ResourceCacheGenerator(DecodeHelper<?> paramDecodeHelper, DataFetcherGenerator.FetcherReadyCallback paramFetcherReadyCallback)
  {
    this.helper = paramDecodeHelper;
    this.cb = paramFetcherReadyCallback;
  }
  
  private boolean hasNextModelLoader()
  {
    boolean bool;
    if (this.modelLoaderIndex < this.modelLoaders.size()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void cancel()
  {
    ModelLoader.LoadData localLoadData = this.loadData;
    if (localLoadData != null) {
      localLoadData.fetcher.cancel();
    }
  }
  
  public void onDataReady(Object paramObject)
  {
    this.cb.onDataFetcherReady(this.sourceKey, paramObject, this.loadData.fetcher, DataSource.RESOURCE_DISK_CACHE, this.currentKey);
  }
  
  public void onLoadFailed(@NonNull Exception paramException)
  {
    this.cb.onDataFetcherFailed(this.currentKey, paramException, this.loadData.fetcher, DataSource.RESOURCE_DISK_CACHE);
  }
  
  public boolean startNext()
  {
    Object localObject1 = this.helper.getCacheKeys();
    boolean bool2 = ((List)localObject1).isEmpty();
    boolean bool1 = false;
    if (bool2) {
      return false;
    }
    List localList = this.helper.getRegisteredResourceClasses();
    if (localList.isEmpty())
    {
      if (File.class.equals(this.helper.getTranscodeClass())) {
        return false;
      }
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Failed to find any load path from ");
      ((StringBuilder)localObject1).append(this.helper.getModelClass());
      ((StringBuilder)localObject1).append(" to ");
      ((StringBuilder)localObject1).append(this.helper.getTranscodeClass());
      throw new IllegalStateException(((StringBuilder)localObject1).toString());
    }
    for (;;)
    {
      if ((this.modelLoaders != null) && (hasNextModelLoader()))
      {
        this.loadData = null;
        while ((!bool1) && (hasNextModelLoader()))
        {
          localObject1 = this.modelLoaders;
          int i = this.modelLoaderIndex;
          this.modelLoaderIndex = (i + 1);
          this.loadData = ((ModelLoader)((List)localObject1).get(i)).buildLoadData(this.cacheFile, this.helper.getWidth(), this.helper.getHeight(), this.helper.getOptions());
          if ((this.loadData != null) && (this.helper.hasLoadPath(this.loadData.fetcher.getDataClass())))
          {
            this.loadData.fetcher.loadData(this.helper.getPriority(), this);
            bool1 = true;
          }
        }
        return bool1;
      }
      this.resourceClassIndex += 1;
      if (this.resourceClassIndex >= localList.size())
      {
        this.sourceIdIndex += 1;
        if (this.sourceIdIndex >= ((List)localObject1).size()) {
          return false;
        }
        this.resourceClassIndex = 0;
      }
      Key localKey = (Key)((List)localObject1).get(this.sourceIdIndex);
      Object localObject2 = (Class)localList.get(this.resourceClassIndex);
      Transformation localTransformation = this.helper.getTransformation((Class)localObject2);
      this.currentKey = new ResourceCacheKey(this.helper.getArrayPool(), localKey, this.helper.getSignature(), this.helper.getWidth(), this.helper.getHeight(), localTransformation, (Class)localObject2, this.helper.getOptions());
      this.cacheFile = this.helper.getDiskCache().get(this.currentKey);
      localObject2 = this.cacheFile;
      if (localObject2 != null)
      {
        this.sourceKey = localKey;
        this.modelLoaders = this.helper.getModelLoaders((File)localObject2);
        this.modelLoaderIndex = 0;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/ResourceCacheGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */