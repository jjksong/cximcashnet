package com.bumptech.glide.load.engine.executor;

import android.os.Build.VERSION;
import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class RuntimeCompat
{
  private static final String CPU_LOCATION = "/sys/devices/system/cpu/";
  private static final String CPU_NAME_REGEX = "cpu[0-9]+";
  private static final String TAG = "GlideRuntimeCompat";
  
  static int availableProcessors()
  {
    int j = Runtime.getRuntime().availableProcessors();
    int i = j;
    if (Build.VERSION.SDK_INT < 17) {
      i = Math.max(getCoreCountPre17(), j);
    }
    return i;
  }
  
  /* Error */
  private static int getCoreCountPre17()
  {
    // Byte code:
    //   0: invokestatic 54	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
    //   3: astore_2
    //   4: new 56	java/io/File
    //   7: astore_1
    //   8: aload_1
    //   9: ldc 10
    //   11: invokespecial 59	java/io/File:<init>	(Ljava/lang/String;)V
    //   14: ldc 13
    //   16: invokestatic 65	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   19: astore 4
    //   21: new 6	com/bumptech/glide/load/engine/executor/RuntimeCompat$1
    //   24: astore_3
    //   25: aload_3
    //   26: aload 4
    //   28: invokespecial 68	com/bumptech/glide/load/engine/executor/RuntimeCompat$1:<init>	(Ljava/util/regex/Pattern;)V
    //   31: aload_1
    //   32: aload_3
    //   33: invokevirtual 72	java/io/File:listFiles	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   36: astore_1
    //   37: aload_2
    //   38: invokestatic 76	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   41: goto +33 -> 74
    //   44: astore_1
    //   45: goto +47 -> 92
    //   48: astore_1
    //   49: ldc 16
    //   51: bipush 6
    //   53: invokestatic 82	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   56: ifeq +12 -> 68
    //   59: ldc 16
    //   61: ldc 84
    //   63: aload_1
    //   64: invokestatic 88	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   67: pop
    //   68: aload_2
    //   69: invokestatic 76	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   72: aconst_null
    //   73: astore_1
    //   74: aload_1
    //   75: ifnull +9 -> 84
    //   78: aload_1
    //   79: arraylength
    //   80: istore_0
    //   81: goto +5 -> 86
    //   84: iconst_0
    //   85: istore_0
    //   86: iconst_1
    //   87: iload_0
    //   88: invokestatic 46	java/lang/Math:max	(II)I
    //   91: ireturn
    //   92: aload_2
    //   93: invokestatic 76	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   96: aload_1
    //   97: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   80	8	0	i	int
    //   7	30	1	localObject1	Object
    //   44	1	1	localObject2	Object
    //   48	16	1	localThrowable	Throwable
    //   73	24	1	localObject3	Object
    //   3	90	2	localThreadPolicy	android.os.StrictMode.ThreadPolicy
    //   24	9	3	local1	1
    //   19	8	4	localPattern	Pattern
    // Exception table:
    //   from	to	target	type
    //   4	37	44	finally
    //   49	68	44	finally
    //   4	37	48	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/executor/RuntimeCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */