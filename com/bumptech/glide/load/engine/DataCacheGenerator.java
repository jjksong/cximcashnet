package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.DataFetcher.DataCallback;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoader.LoadData;
import java.io.File;
import java.util.List;

class DataCacheGenerator
  implements DataFetcherGenerator, DataFetcher.DataCallback<Object>
{
  private File cacheFile;
  private final List<Key> cacheKeys;
  private final DataFetcherGenerator.FetcherReadyCallback cb;
  private final DecodeHelper<?> helper;
  private volatile ModelLoader.LoadData<?> loadData;
  private int modelLoaderIndex;
  private List<ModelLoader<File, ?>> modelLoaders;
  private int sourceIdIndex = -1;
  private Key sourceKey;
  
  DataCacheGenerator(DecodeHelper<?> paramDecodeHelper, DataFetcherGenerator.FetcherReadyCallback paramFetcherReadyCallback)
  {
    this(paramDecodeHelper.getCacheKeys(), paramDecodeHelper, paramFetcherReadyCallback);
  }
  
  DataCacheGenerator(List<Key> paramList, DecodeHelper<?> paramDecodeHelper, DataFetcherGenerator.FetcherReadyCallback paramFetcherReadyCallback)
  {
    this.cacheKeys = paramList;
    this.helper = paramDecodeHelper;
    this.cb = paramFetcherReadyCallback;
  }
  
  private boolean hasNextModelLoader()
  {
    boolean bool;
    if (this.modelLoaderIndex < this.modelLoaders.size()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void cancel()
  {
    ModelLoader.LoadData localLoadData = this.loadData;
    if (localLoadData != null) {
      localLoadData.fetcher.cancel();
    }
  }
  
  public void onDataReady(Object paramObject)
  {
    this.cb.onDataFetcherReady(this.sourceKey, paramObject, this.loadData.fetcher, DataSource.DATA_DISK_CACHE, this.sourceKey);
  }
  
  public void onLoadFailed(@NonNull Exception paramException)
  {
    this.cb.onDataFetcherFailed(this.sourceKey, paramException, this.loadData.fetcher, DataSource.DATA_DISK_CACHE);
  }
  
  public boolean startNext()
  {
    for (;;)
    {
      Object localObject1 = this.modelLoaders;
      boolean bool = false;
      if ((localObject1 != null) && (hasNextModelLoader()))
      {
        this.loadData = null;
        while ((!bool) && (hasNextModelLoader()))
        {
          localObject1 = this.modelLoaders;
          int i = this.modelLoaderIndex;
          this.modelLoaderIndex = (i + 1);
          this.loadData = ((ModelLoader)((List)localObject1).get(i)).buildLoadData(this.cacheFile, this.helper.getWidth(), this.helper.getHeight(), this.helper.getOptions());
          if ((this.loadData != null) && (this.helper.hasLoadPath(this.loadData.fetcher.getDataClass())))
          {
            this.loadData.fetcher.loadData(this.helper.getPriority(), this);
            bool = true;
          }
        }
        return bool;
      }
      this.sourceIdIndex += 1;
      if (this.sourceIdIndex >= this.cacheKeys.size()) {
        return false;
      }
      localObject1 = (Key)this.cacheKeys.get(this.sourceIdIndex);
      Object localObject2 = new DataCacheKey((Key)localObject1, this.helper.getSignature());
      this.cacheFile = this.helper.getDiskCache().get((Key)localObject2);
      localObject2 = this.cacheFile;
      if (localObject2 != null)
      {
        this.sourceKey = ((Key)localObject1);
        this.modelLoaders = this.helper.getModelLoaders((File)localObject2);
        this.modelLoaderIndex = 0;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/DataCacheGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */