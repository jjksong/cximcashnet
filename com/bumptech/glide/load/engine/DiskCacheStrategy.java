package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;

public abstract class DiskCacheStrategy
{
  public static final DiskCacheStrategy ALL = new DiskCacheStrategy()
  {
    public boolean decodeCachedData()
    {
      return true;
    }
    
    public boolean decodeCachedResource()
    {
      return true;
    }
    
    public boolean isDataCacheable(DataSource paramAnonymousDataSource)
    {
      boolean bool;
      if (paramAnonymousDataSource == DataSource.REMOTE) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public boolean isResourceCacheable(boolean paramAnonymousBoolean, DataSource paramAnonymousDataSource, EncodeStrategy paramAnonymousEncodeStrategy)
    {
      if ((paramAnonymousDataSource != DataSource.RESOURCE_DISK_CACHE) && (paramAnonymousDataSource != DataSource.MEMORY_CACHE)) {
        paramAnonymousBoolean = true;
      } else {
        paramAnonymousBoolean = false;
      }
      return paramAnonymousBoolean;
    }
  };
  public static final DiskCacheStrategy AUTOMATIC = new DiskCacheStrategy()
  {
    public boolean decodeCachedData()
    {
      return true;
    }
    
    public boolean decodeCachedResource()
    {
      return true;
    }
    
    public boolean isDataCacheable(DataSource paramAnonymousDataSource)
    {
      boolean bool;
      if (paramAnonymousDataSource == DataSource.REMOTE) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public boolean isResourceCacheable(boolean paramAnonymousBoolean, DataSource paramAnonymousDataSource, EncodeStrategy paramAnonymousEncodeStrategy)
    {
      if (((paramAnonymousBoolean) && (paramAnonymousDataSource == DataSource.DATA_DISK_CACHE)) || ((paramAnonymousDataSource == DataSource.LOCAL) && (paramAnonymousEncodeStrategy == EncodeStrategy.TRANSFORMED))) {
        paramAnonymousBoolean = true;
      } else {
        paramAnonymousBoolean = false;
      }
      return paramAnonymousBoolean;
    }
  };
  public static final DiskCacheStrategy DATA;
  public static final DiskCacheStrategy NONE = new DiskCacheStrategy()
  {
    public boolean decodeCachedData()
    {
      return false;
    }
    
    public boolean decodeCachedResource()
    {
      return false;
    }
    
    public boolean isDataCacheable(DataSource paramAnonymousDataSource)
    {
      return false;
    }
    
    public boolean isResourceCacheable(boolean paramAnonymousBoolean, DataSource paramAnonymousDataSource, EncodeStrategy paramAnonymousEncodeStrategy)
    {
      return false;
    }
  };
  public static final DiskCacheStrategy RESOURCE;
  
  static
  {
    DATA = new DiskCacheStrategy()
    {
      public boolean decodeCachedData()
      {
        return true;
      }
      
      public boolean decodeCachedResource()
      {
        return false;
      }
      
      public boolean isDataCacheable(DataSource paramAnonymousDataSource)
      {
        boolean bool;
        if ((paramAnonymousDataSource != DataSource.DATA_DISK_CACHE) && (paramAnonymousDataSource != DataSource.MEMORY_CACHE)) {
          bool = true;
        } else {
          bool = false;
        }
        return bool;
      }
      
      public boolean isResourceCacheable(boolean paramAnonymousBoolean, DataSource paramAnonymousDataSource, EncodeStrategy paramAnonymousEncodeStrategy)
      {
        return false;
      }
    };
    RESOURCE = new DiskCacheStrategy()
    {
      public boolean decodeCachedData()
      {
        return false;
      }
      
      public boolean decodeCachedResource()
      {
        return true;
      }
      
      public boolean isDataCacheable(DataSource paramAnonymousDataSource)
      {
        return false;
      }
      
      public boolean isResourceCacheable(boolean paramAnonymousBoolean, DataSource paramAnonymousDataSource, EncodeStrategy paramAnonymousEncodeStrategy)
      {
        if ((paramAnonymousDataSource != DataSource.RESOURCE_DISK_CACHE) && (paramAnonymousDataSource != DataSource.MEMORY_CACHE)) {
          paramAnonymousBoolean = true;
        } else {
          paramAnonymousBoolean = false;
        }
        return paramAnonymousBoolean;
      }
    };
  }
  
  public abstract boolean decodeCachedData();
  
  public abstract boolean decodeCachedResource();
  
  public abstract boolean isDataCacheable(DataSource paramDataSource);
  
  public abstract boolean isResourceCacheable(boolean paramBoolean, DataSource paramDataSource, EncodeStrategy paramEncodeStrategy);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/DiskCacheStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */