package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;
import com.bumptech.glide.load.Key;
import java.security.MessageDigest;

final class DataCacheKey
  implements Key
{
  private final Key signature;
  private final Key sourceKey;
  
  DataCacheKey(Key paramKey1, Key paramKey2)
  {
    this.sourceKey = paramKey1;
    this.signature = paramKey2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof DataCacheKey;
    boolean bool2 = false;
    if (bool1)
    {
      paramObject = (DataCacheKey)paramObject;
      bool1 = bool2;
      if (this.sourceKey.equals(((DataCacheKey)paramObject).sourceKey))
      {
        bool1 = bool2;
        if (this.signature.equals(((DataCacheKey)paramObject).signature)) {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  Key getSourceKey()
  {
    return this.sourceKey;
  }
  
  public int hashCode()
  {
    return this.sourceKey.hashCode() * 31 + this.signature.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DataCacheKey{sourceKey=");
    localStringBuilder.append(this.sourceKey);
    localStringBuilder.append(", signature=");
    localStringBuilder.append(this.signature);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void updateDiskCacheKey(@NonNull MessageDigest paramMessageDigest)
  {
    this.sourceKey.updateDiskCacheKey(paramMessageDigest);
    this.signature.updateDiskCacheKey(paramMessageDigest);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/DataCacheKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */