package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;

class EngineResource<Z>
  implements Resource<Z>
{
  private int acquired;
  private final boolean isCacheable;
  private final boolean isRecyclable;
  private boolean isRecycled;
  private Key key;
  private ResourceListener listener;
  private final Resource<Z> resource;
  
  EngineResource(Resource<Z> paramResource, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.resource = ((Resource)Preconditions.checkNotNull(paramResource));
    this.isCacheable = paramBoolean1;
    this.isRecyclable = paramBoolean2;
  }
  
  void acquire()
  {
    try
    {
      if (!this.isRecycled)
      {
        this.acquired += 1;
        return;
      }
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Cannot acquire a recycled resource");
      throw localIllegalStateException;
    }
    finally {}
  }
  
  @NonNull
  public Z get()
  {
    return (Z)this.resource.get();
  }
  
  Resource<Z> getResource()
  {
    return this.resource;
  }
  
  @NonNull
  public Class<Z> getResourceClass()
  {
    return this.resource.getResourceClass();
  }
  
  public int getSize()
  {
    return this.resource.getSize();
  }
  
  boolean isCacheable()
  {
    return this.isCacheable;
  }
  
  public void recycle()
  {
    try
    {
      if (this.acquired <= 0)
      {
        if (!this.isRecycled)
        {
          this.isRecycled = true;
          if (this.isRecyclable) {
            this.resource.recycle();
          }
          return;
        }
        localIllegalStateException = new java/lang/IllegalStateException;
        localIllegalStateException.<init>("Cannot recycle a resource that has already been recycled");
        throw localIllegalStateException;
      }
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Cannot recycle a resource while it is still acquired");
      throw localIllegalStateException;
    }
    finally {}
  }
  
  void release()
  {
    synchronized (this.listener)
    {
      try
      {
        if (this.acquired > 0)
        {
          int i = this.acquired - 1;
          this.acquired = i;
          if (i == 0) {
            this.listener.onResourceReleased(this.key, this);
          }
          return;
        }
        IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
        localIllegalStateException.<init>("Cannot release a recycled or not yet acquired resource");
        throw localIllegalStateException;
      }
      finally {}
    }
  }
  
  void setResourceListener(Key paramKey, ResourceListener paramResourceListener)
  {
    try
    {
      this.key = paramKey;
      this.listener = paramResourceListener;
      return;
    }
    finally
    {
      paramKey = finally;
      throw paramKey;
    }
  }
  
  public String toString()
  {
    try
    {
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append("EngineResource{isCacheable=");
      ((StringBuilder)localObject1).append(this.isCacheable);
      ((StringBuilder)localObject1).append(", listener=");
      ((StringBuilder)localObject1).append(this.listener);
      ((StringBuilder)localObject1).append(", key=");
      ((StringBuilder)localObject1).append(this.key);
      ((StringBuilder)localObject1).append(", acquired=");
      ((StringBuilder)localObject1).append(this.acquired);
      ((StringBuilder)localObject1).append(", isRecycled=");
      ((StringBuilder)localObject1).append(this.isRecycled);
      ((StringBuilder)localObject1).append(", resource=");
      ((StringBuilder)localObject1).append(this.resource);
      ((StringBuilder)localObject1).append('}');
      localObject1 = ((StringBuilder)localObject1).toString();
      return (String)localObject1;
    }
    finally
    {
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  static abstract interface ResourceListener
  {
    public abstract void onResourceReleased(Key paramKey, EngineResource<?> paramEngineResource);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/EngineResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */