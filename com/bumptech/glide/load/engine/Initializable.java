package com.bumptech.glide.load.engine;

public abstract interface Initializable
{
  public abstract void initialize();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/Initializable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */