package com.bumptech.glide.load.engine;

final class CallbackException
  extends RuntimeException
{
  private static final long serialVersionUID = -7530898992688511851L;
  
  CallbackException(Throwable paramThrowable)
  {
    super("Unexpected exception thrown by non-Glide code", paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/CallbackException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */