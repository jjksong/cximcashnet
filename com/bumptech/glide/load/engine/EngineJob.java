package com.bumptech.glide.load.engine;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.util.Pools.Pool;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.pool.FactoryPools.Poolable;
import com.bumptech.glide.util.pool.StateVerifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

class EngineJob<R>
  implements DecodeJob.Callback<R>, FactoryPools.Poolable
{
  private static final EngineResourceFactory DEFAULT_FACTORY = new EngineResourceFactory();
  private final GlideExecutor animationExecutor;
  final ResourceCallbacksAndExecutors cbs = new ResourceCallbacksAndExecutors();
  DataSource dataSource;
  private DecodeJob<R> decodeJob;
  private final GlideExecutor diskCacheExecutor;
  EngineResource<?> engineResource;
  private final EngineResourceFactory engineResourceFactory;
  GlideException exception;
  private boolean hasLoadFailed;
  private boolean hasResource;
  private boolean isCacheable;
  private volatile boolean isCancelled;
  private Key key;
  private final EngineJobListener listener;
  private boolean onlyRetrieveFromCache;
  private final AtomicInteger pendingCallbacks = new AtomicInteger();
  private final Pools.Pool<EngineJob<?>> pool;
  private Resource<?> resource;
  private final GlideExecutor sourceExecutor;
  private final GlideExecutor sourceUnlimitedExecutor;
  private final StateVerifier stateVerifier = StateVerifier.newInstance();
  private boolean useAnimationPool;
  private boolean useUnlimitedSourceGeneratorPool;
  
  EngineJob(GlideExecutor paramGlideExecutor1, GlideExecutor paramGlideExecutor2, GlideExecutor paramGlideExecutor3, GlideExecutor paramGlideExecutor4, EngineJobListener paramEngineJobListener, Pools.Pool<EngineJob<?>> paramPool)
  {
    this(paramGlideExecutor1, paramGlideExecutor2, paramGlideExecutor3, paramGlideExecutor4, paramEngineJobListener, paramPool, DEFAULT_FACTORY);
  }
  
  @VisibleForTesting
  EngineJob(GlideExecutor paramGlideExecutor1, GlideExecutor paramGlideExecutor2, GlideExecutor paramGlideExecutor3, GlideExecutor paramGlideExecutor4, EngineJobListener paramEngineJobListener, Pools.Pool<EngineJob<?>> paramPool, EngineResourceFactory paramEngineResourceFactory)
  {
    this.diskCacheExecutor = paramGlideExecutor1;
    this.sourceExecutor = paramGlideExecutor2;
    this.sourceUnlimitedExecutor = paramGlideExecutor3;
    this.animationExecutor = paramGlideExecutor4;
    this.listener = paramEngineJobListener;
    this.pool = paramPool;
    this.engineResourceFactory = paramEngineResourceFactory;
  }
  
  private GlideExecutor getActiveSourceExecutor()
  {
    GlideExecutor localGlideExecutor;
    if (this.useUnlimitedSourceGeneratorPool) {
      localGlideExecutor = this.sourceUnlimitedExecutor;
    } else if (this.useAnimationPool) {
      localGlideExecutor = this.animationExecutor;
    } else {
      localGlideExecutor = this.sourceExecutor;
    }
    return localGlideExecutor;
  }
  
  private boolean isDone()
  {
    boolean bool;
    if ((!this.hasLoadFailed) && (!this.hasResource) && (!this.isCancelled)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private void release()
  {
    try
    {
      if (this.key != null)
      {
        this.cbs.clear();
        this.key = null;
        this.engineResource = null;
        this.resource = null;
        this.hasLoadFailed = false;
        this.isCancelled = false;
        this.hasResource = false;
        this.decodeJob.release(false);
        this.decodeJob = null;
        this.exception = null;
        this.dataSource = null;
        this.pool.release(this);
        return;
      }
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>();
      throw localIllegalArgumentException;
    }
    finally {}
  }
  
  void addCallback(ResourceCallback paramResourceCallback, Executor paramExecutor)
  {
    try
    {
      this.stateVerifier.throwIfRecycled();
      this.cbs.add(paramResourceCallback, paramExecutor);
      boolean bool2 = this.hasResource;
      boolean bool1 = true;
      Object localObject;
      if (bool2)
      {
        incrementPendingCallbacks(1);
        localObject = new com/bumptech/glide/load/engine/EngineJob$CallResourceReady;
        ((CallResourceReady)localObject).<init>(this, paramResourceCallback);
        paramExecutor.execute((Runnable)localObject);
      }
      else if (this.hasLoadFailed)
      {
        incrementPendingCallbacks(1);
        localObject = new com/bumptech/glide/load/engine/EngineJob$CallLoadFailed;
        ((CallLoadFailed)localObject).<init>(this, paramResourceCallback);
        paramExecutor.execute((Runnable)localObject);
      }
      else
      {
        if (this.isCancelled) {
          bool1 = false;
        }
        Preconditions.checkArgument(bool1, "Cannot add callbacks to a cancelled EngineJob");
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  void callCallbackOnLoadFailed(ResourceCallback paramResourceCallback)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: aload_0
    //   4: getfield 147	com/bumptech/glide/load/engine/EngineJob:exception	Lcom/bumptech/glide/load/engine/GlideException;
    //   7: invokeinterface 197 2 0
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: goto +15 -> 31
    //   19: astore_1
    //   20: new 199	com/bumptech/glide/load/engine/CallbackException
    //   23: astore_2
    //   24: aload_2
    //   25: aload_1
    //   26: invokespecial 202	com/bumptech/glide/load/engine/CallbackException:<init>	(Ljava/lang/Throwable;)V
    //   29: aload_2
    //   30: athrow
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_1
    //   34: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	35	0	this	EngineJob
    //   0	35	1	paramResourceCallback	ResourceCallback
    //   23	7	2	localCallbackException	CallbackException
    // Exception table:
    //   from	to	target	type
    //   2	12	15	finally
    //   20	31	15	finally
    //   2	12	19	java/lang/Throwable
  }
  
  /* Error */
  void callCallbackOnResourceReady(ResourceCallback paramResourceCallback)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: aload_0
    //   4: getfield 136	com/bumptech/glide/load/engine/EngineJob:engineResource	Lcom/bumptech/glide/load/engine/EngineResource;
    //   7: aload_0
    //   8: getfield 149	com/bumptech/glide/load/engine/EngineJob:dataSource	Lcom/bumptech/glide/load/DataSource;
    //   11: invokeinterface 207 3 0
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: goto +15 -> 35
    //   23: astore_1
    //   24: new 199	com/bumptech/glide/load/engine/CallbackException
    //   27: astore_2
    //   28: aload_2
    //   29: aload_1
    //   30: invokespecial 202	com/bumptech/glide/load/engine/CallbackException:<init>	(Ljava/lang/Throwable;)V
    //   33: aload_2
    //   34: athrow
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	39	0	this	EngineJob
    //   0	39	1	paramResourceCallback	ResourceCallback
    //   27	7	2	localCallbackException	CallbackException
    // Exception table:
    //   from	to	target	type
    //   2	16	19	finally
    //   24	35	19	finally
    //   2	16	23	java/lang/Throwable
  }
  
  void cancel()
  {
    if (isDone()) {
      return;
    }
    this.isCancelled = true;
    this.decodeJob.cancel();
    this.listener.onEngineJobCancelled(this, this.key);
  }
  
  void decrementPendingCallbacks()
  {
    try
    {
      this.stateVerifier.throwIfRecycled();
      Preconditions.checkArgument(isDone(), "Not yet complete!");
      int i = this.pendingCallbacks.decrementAndGet();
      boolean bool;
      if (i >= 0) {
        bool = true;
      } else {
        bool = false;
      }
      Preconditions.checkArgument(bool, "Can't decrement below 0");
      if (i == 0)
      {
        if (this.engineResource != null) {
          this.engineResource.release();
        }
        release();
      }
      return;
    }
    finally {}
  }
  
  @NonNull
  public StateVerifier getVerifier()
  {
    return this.stateVerifier;
  }
  
  void incrementPendingCallbacks(int paramInt)
  {
    try
    {
      Preconditions.checkArgument(isDone(), "Not yet complete!");
      if ((this.pendingCallbacks.getAndAdd(paramInt) == 0) && (this.engineResource != null)) {
        this.engineResource.acquire();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  @VisibleForTesting
  EngineJob<R> init(Key paramKey, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    try
    {
      this.key = paramKey;
      this.isCacheable = paramBoolean1;
      this.useUnlimitedSourceGeneratorPool = paramBoolean2;
      this.useAnimationPool = paramBoolean3;
      this.onlyRetrieveFromCache = paramBoolean4;
      return this;
    }
    finally
    {
      paramKey = finally;
      throw paramKey;
    }
  }
  
  boolean isCancelled()
  {
    try
    {
      boolean bool = this.isCancelled;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  void notifyCallbacksOfException()
  {
    try
    {
      this.stateVerifier.throwIfRecycled();
      if (this.isCancelled)
      {
        release();
        return;
      }
      if (!this.cbs.isEmpty())
      {
        if (!this.hasLoadFailed)
        {
          this.hasLoadFailed = true;
          localObject1 = this.key;
          Object localObject3 = this.cbs.copy();
          incrementPendingCallbacks(((ResourceCallbacksAndExecutors)localObject3).size() + 1);
          this.listener.onEngineJobComplete(this, (Key)localObject1, null);
          localObject1 = ((ResourceCallbacksAndExecutors)localObject3).iterator();
          while (((Iterator)localObject1).hasNext())
          {
            localObject3 = (ResourceCallbackAndExecutor)((Iterator)localObject1).next();
            ((ResourceCallbackAndExecutor)localObject3).executor.execute(new CallLoadFailed(((ResourceCallbackAndExecutor)localObject3).cb));
          }
          decrementPendingCallbacks();
          return;
        }
        localObject1 = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject1).<init>("Already failed once");
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("Received an exception without any callbacks to notify");
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  void notifyCallbacksOfResult()
  {
    try
    {
      this.stateVerifier.throwIfRecycled();
      if (this.isCancelled)
      {
        this.resource.recycle();
        release();
        return;
      }
      if (!this.cbs.isEmpty())
      {
        if (!this.hasResource)
        {
          this.engineResource = this.engineResourceFactory.build(this.resource, this.isCacheable);
          this.hasResource = true;
          localObject1 = this.cbs.copy();
          incrementPendingCallbacks(((ResourceCallbacksAndExecutors)localObject1).size() + 1);
          Object localObject3 = this.key;
          EngineResource localEngineResource = this.engineResource;
          this.listener.onEngineJobComplete(this, (Key)localObject3, localEngineResource);
          localObject3 = ((ResourceCallbacksAndExecutors)localObject1).iterator();
          while (((Iterator)localObject3).hasNext())
          {
            localObject1 = (ResourceCallbackAndExecutor)((Iterator)localObject3).next();
            ((ResourceCallbackAndExecutor)localObject1).executor.execute(new CallResourceReady(((ResourceCallbackAndExecutor)localObject1).cb));
          }
          decrementPendingCallbacks();
          return;
        }
        localObject1 = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject1).<init>("Already have resource");
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("Received a resource without any callbacks to notify");
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public void onLoadFailed(GlideException paramGlideException)
  {
    try
    {
      this.exception = paramGlideException;
      notifyCallbacksOfException();
      return;
    }
    finally {}
  }
  
  public void onResourceReady(Resource<R> paramResource, DataSource paramDataSource)
  {
    try
    {
      this.resource = paramResource;
      this.dataSource = paramDataSource;
      notifyCallbacksOfResult();
      return;
    }
    finally {}
  }
  
  boolean onlyRetrieveFromCache()
  {
    return this.onlyRetrieveFromCache;
  }
  
  void removeCallback(ResourceCallback paramResourceCallback)
  {
    try
    {
      this.stateVerifier.throwIfRecycled();
      this.cbs.remove(paramResourceCallback);
      if (this.cbs.isEmpty())
      {
        cancel();
        int i;
        if ((!this.hasResource) && (!this.hasLoadFailed)) {
          i = 0;
        } else {
          i = 1;
        }
        if ((i != 0) && (this.pendingCallbacks.get() == 0)) {
          release();
        }
      }
      return;
    }
    finally {}
  }
  
  public void reschedule(DecodeJob<?> paramDecodeJob)
  {
    getActiveSourceExecutor().execute(paramDecodeJob);
  }
  
  public void start(DecodeJob<R> paramDecodeJob)
  {
    try
    {
      this.decodeJob = paramDecodeJob;
      GlideExecutor localGlideExecutor;
      if (paramDecodeJob.willDecodeFromCache()) {
        localGlideExecutor = this.diskCacheExecutor;
      } else {
        localGlideExecutor = getActiveSourceExecutor();
      }
      localGlideExecutor.execute(paramDecodeJob);
      return;
    }
    finally {}
  }
  
  private class CallLoadFailed
    implements Runnable
  {
    private final ResourceCallback cb;
    
    CallLoadFailed(ResourceCallback paramResourceCallback)
    {
      this.cb = paramResourceCallback;
    }
    
    public void run()
    {
      synchronized (EngineJob.this)
      {
        if (EngineJob.this.cbs.contains(this.cb)) {
          EngineJob.this.callCallbackOnLoadFailed(this.cb);
        }
        EngineJob.this.decrementPendingCallbacks();
        return;
      }
    }
  }
  
  private class CallResourceReady
    implements Runnable
  {
    private final ResourceCallback cb;
    
    CallResourceReady(ResourceCallback paramResourceCallback)
    {
      this.cb = paramResourceCallback;
    }
    
    public void run()
    {
      synchronized (EngineJob.this)
      {
        if (EngineJob.this.cbs.contains(this.cb))
        {
          EngineJob.this.engineResource.acquire();
          EngineJob.this.callCallbackOnResourceReady(this.cb);
          EngineJob.this.removeCallback(this.cb);
        }
        EngineJob.this.decrementPendingCallbacks();
        return;
      }
    }
  }
  
  @VisibleForTesting
  static class EngineResourceFactory
  {
    public <R> EngineResource<R> build(Resource<R> paramResource, boolean paramBoolean)
    {
      return new EngineResource(paramResource, paramBoolean, true);
    }
  }
  
  static final class ResourceCallbackAndExecutor
  {
    final ResourceCallback cb;
    final Executor executor;
    
    ResourceCallbackAndExecutor(ResourceCallback paramResourceCallback, Executor paramExecutor)
    {
      this.cb = paramResourceCallback;
      this.executor = paramExecutor;
    }
    
    public boolean equals(Object paramObject)
    {
      if ((paramObject instanceof ResourceCallbackAndExecutor))
      {
        paramObject = (ResourceCallbackAndExecutor)paramObject;
        return this.cb.equals(((ResourceCallbackAndExecutor)paramObject).cb);
      }
      return false;
    }
    
    public int hashCode()
    {
      return this.cb.hashCode();
    }
  }
  
  static final class ResourceCallbacksAndExecutors
    implements Iterable<EngineJob.ResourceCallbackAndExecutor>
  {
    private final List<EngineJob.ResourceCallbackAndExecutor> callbacksAndExecutors;
    
    ResourceCallbacksAndExecutors()
    {
      this(new ArrayList(2));
    }
    
    ResourceCallbacksAndExecutors(List<EngineJob.ResourceCallbackAndExecutor> paramList)
    {
      this.callbacksAndExecutors = paramList;
    }
    
    private static EngineJob.ResourceCallbackAndExecutor defaultCallbackAndExecutor(ResourceCallback paramResourceCallback)
    {
      return new EngineJob.ResourceCallbackAndExecutor(paramResourceCallback, Executors.directExecutor());
    }
    
    void add(ResourceCallback paramResourceCallback, Executor paramExecutor)
    {
      this.callbacksAndExecutors.add(new EngineJob.ResourceCallbackAndExecutor(paramResourceCallback, paramExecutor));
    }
    
    void clear()
    {
      this.callbacksAndExecutors.clear();
    }
    
    boolean contains(ResourceCallback paramResourceCallback)
    {
      return this.callbacksAndExecutors.contains(defaultCallbackAndExecutor(paramResourceCallback));
    }
    
    ResourceCallbacksAndExecutors copy()
    {
      return new ResourceCallbacksAndExecutors(new ArrayList(this.callbacksAndExecutors));
    }
    
    boolean isEmpty()
    {
      return this.callbacksAndExecutors.isEmpty();
    }
    
    @NonNull
    public Iterator<EngineJob.ResourceCallbackAndExecutor> iterator()
    {
      return this.callbacksAndExecutors.iterator();
    }
    
    void remove(ResourceCallback paramResourceCallback)
    {
      this.callbacksAndExecutors.remove(defaultCallbackAndExecutor(paramResourceCallback));
    }
    
    int size()
    {
      return this.callbacksAndExecutors.size();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/EngineJob.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */