package com.bumptech.glide.load.engine.cache;

import android.content.Context;
import android.support.annotation.Nullable;
import java.io.File;

public final class ExternalPreferredCacheDiskCacheFactory
  extends DiskLruCacheFactory
{
  public ExternalPreferredCacheDiskCacheFactory(Context paramContext)
  {
    this(paramContext, "image_manager_disk_cache", 262144000L);
  }
  
  public ExternalPreferredCacheDiskCacheFactory(Context paramContext, long paramLong)
  {
    this(paramContext, "image_manager_disk_cache", paramLong);
  }
  
  public ExternalPreferredCacheDiskCacheFactory(Context paramContext, final String paramString, long paramLong)
  {
    super(new DiskLruCacheFactory.CacheDirectoryGetter()
    {
      @Nullable
      private File getInternalCacheDirectory()
      {
        File localFile = ExternalPreferredCacheDiskCacheFactory.this.getCacheDir();
        if (localFile == null) {
          return null;
        }
        String str = paramString;
        if (str != null) {
          return new File(localFile, str);
        }
        return localFile;
      }
      
      public File getCacheDirectory()
      {
        Object localObject = getInternalCacheDirectory();
        if ((localObject != null) && (((File)localObject).exists())) {
          return (File)localObject;
        }
        File localFile = ExternalPreferredCacheDiskCacheFactory.this.getExternalCacheDir();
        if ((localFile != null) && (localFile.canWrite()))
        {
          localObject = paramString;
          if (localObject != null) {
            return new File(localFile, (String)localObject);
          }
          return localFile;
        }
        return (File)localObject;
      }
    }, paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/cache/ExternalPreferredCacheDiskCacheFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */