package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.util.Preconditions;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class DiskCacheWriteLocker
{
  private final Map<String, WriteLock> locks = new HashMap();
  private final WriteLockPool writeLockPool = new WriteLockPool();
  
  void acquire(String paramString)
  {
    try
    {
      WriteLock localWriteLock2 = (WriteLock)this.locks.get(paramString);
      WriteLock localWriteLock1 = localWriteLock2;
      if (localWriteLock2 == null)
      {
        localWriteLock1 = this.writeLockPool.obtain();
        this.locks.put(paramString, localWriteLock1);
      }
      localWriteLock1.interestedThreads += 1;
      localWriteLock1.lock.lock();
      return;
    }
    finally {}
  }
  
  void release(String paramString)
  {
    try
    {
      WriteLock localWriteLock = (WriteLock)Preconditions.checkNotNull(this.locks.get(paramString));
      if (localWriteLock.interestedThreads >= 1)
      {
        localWriteLock.interestedThreads -= 1;
        if (localWriteLock.interestedThreads == 0)
        {
          localObject2 = (WriteLock)this.locks.remove(paramString);
          if (localObject2.equals(localWriteLock))
          {
            this.writeLockPool.offer((WriteLock)localObject2);
          }
          else
          {
            IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("Removed the wrong lock, expected to remove: ");
            ((StringBuilder)localObject1).append(localWriteLock);
            ((StringBuilder)localObject1).append(", but actually removed: ");
            ((StringBuilder)localObject1).append(localObject2);
            ((StringBuilder)localObject1).append(", safeKey: ");
            ((StringBuilder)localObject1).append(paramString);
            localIllegalStateException.<init>(((StringBuilder)localObject1).toString());
            throw localIllegalStateException;
          }
        }
        localWriteLock.lock.unlock();
        return;
      }
      Object localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append("Cannot release a lock that is not held, safeKey: ");
      ((StringBuilder)localObject2).append(paramString);
      ((StringBuilder)localObject2).append(", interestedThreads: ");
      ((StringBuilder)localObject2).append(localWriteLock.interestedThreads);
      ((IllegalStateException)localObject1).<init>(((StringBuilder)localObject2).toString());
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  private static class WriteLock
  {
    int interestedThreads;
    final Lock lock = new ReentrantLock();
  }
  
  private static class WriteLockPool
  {
    private static final int MAX_POOL_SIZE = 10;
    private final Queue<DiskCacheWriteLocker.WriteLock> pool = new ArrayDeque();
    
    DiskCacheWriteLocker.WriteLock obtain()
    {
      synchronized (this.pool)
      {
        DiskCacheWriteLocker.WriteLock localWriteLock = (DiskCacheWriteLocker.WriteLock)this.pool.poll();
        ??? = localWriteLock;
        if (localWriteLock == null) {
          ??? = new DiskCacheWriteLocker.WriteLock();
        }
        return (DiskCacheWriteLocker.WriteLock)???;
      }
    }
    
    void offer(DiskCacheWriteLocker.WriteLock paramWriteLock)
    {
      synchronized (this.pool)
      {
        if (this.pool.size() < 10) {
          this.pool.offer(paramWriteLock);
        }
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/cache/DiskCacheWriteLocker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */