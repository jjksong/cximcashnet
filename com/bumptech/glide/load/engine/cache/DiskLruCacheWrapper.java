package com.bumptech.glide.load.engine.cache;

import android.util.Log;
import com.bumptech.glide.disklrucache.DiskLruCache;
import com.bumptech.glide.disklrucache.DiskLruCache.Value;
import com.bumptech.glide.load.Key;
import java.io.File;
import java.io.IOException;

public class DiskLruCacheWrapper
  implements DiskCache
{
  private static final int APP_VERSION = 1;
  private static final String TAG = "DiskLruCacheWrapper";
  private static final int VALUE_COUNT = 1;
  private static DiskLruCacheWrapper wrapper;
  private final File directory;
  private DiskLruCache diskLruCache;
  private final long maxSize;
  private final SafeKeyGenerator safeKeyGenerator;
  private final DiskCacheWriteLocker writeLocker = new DiskCacheWriteLocker();
  
  @Deprecated
  protected DiskLruCacheWrapper(File paramFile, long paramLong)
  {
    this.directory = paramFile;
    this.maxSize = paramLong;
    this.safeKeyGenerator = new SafeKeyGenerator();
  }
  
  public static DiskCache create(File paramFile, long paramLong)
  {
    return new DiskLruCacheWrapper(paramFile, paramLong);
  }
  
  @Deprecated
  public static DiskCache get(File paramFile, long paramLong)
  {
    try
    {
      if (wrapper == null)
      {
        DiskLruCacheWrapper localDiskLruCacheWrapper = new com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper;
        localDiskLruCacheWrapper.<init>(paramFile, paramLong);
        wrapper = localDiskLruCacheWrapper;
      }
      paramFile = wrapper;
      return paramFile;
    }
    finally {}
  }
  
  private DiskLruCache getDiskCache()
    throws IOException
  {
    try
    {
      if (this.diskLruCache == null) {
        this.diskLruCache = DiskLruCache.open(this.directory, 1, 1, this.maxSize);
      }
      DiskLruCache localDiskLruCache = this.diskLruCache;
      return localDiskLruCache;
    }
    finally {}
  }
  
  private void resetDiskCache()
  {
    try
    {
      this.diskLruCache = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  /* Error */
  public void clear()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial 72	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:getDiskCache	()Lcom/bumptech/glide/disklrucache/DiskLruCache;
    //   6: invokevirtual 75	com/bumptech/glide/disklrucache/DiskLruCache:delete	()V
    //   9: aload_0
    //   10: invokespecial 77	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:resetDiskCache	()V
    //   13: goto +29 -> 42
    //   16: astore_1
    //   17: goto +28 -> 45
    //   20: astore_1
    //   21: ldc 13
    //   23: iconst_5
    //   24: invokestatic 83	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   27: ifeq -18 -> 9
    //   30: ldc 13
    //   32: ldc 85
    //   34: aload_1
    //   35: invokestatic 89	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   38: pop
    //   39: goto -30 -> 9
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: aload_0
    //   46: invokespecial 77	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:resetDiskCache	()V
    //   49: aload_1
    //   50: athrow
    //   51: astore_1
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	56	0	this	DiskLruCacheWrapper
    //   16	1	1	localObject1	Object
    //   20	30	1	localIOException	IOException
    //   51	4	1	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   2	9	16	finally
    //   21	39	16	finally
    //   2	9	20	java/io/IOException
    //   9	13	51	finally
    //   45	51	51	finally
  }
  
  public void delete(Key paramKey)
  {
    paramKey = this.safeKeyGenerator.getSafeKey(paramKey);
    try
    {
      getDiskCache().remove(paramKey);
    }
    catch (IOException paramKey)
    {
      if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
        Log.w("DiskLruCacheWrapper", "Unable to delete from disk cache", paramKey);
      }
    }
  }
  
  public File get(Key paramKey)
  {
    Object localObject = this.safeKeyGenerator.getSafeKey(paramKey);
    if (Log.isLoggable("DiskLruCacheWrapper", 2))
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("Get: Obtained: ");
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(" for for Key: ");
      localStringBuilder.append(paramKey);
      Log.v("DiskLruCacheWrapper", localStringBuilder.toString());
    }
    StringBuilder localStringBuilder = null;
    try
    {
      localObject = getDiskCache().get((String)localObject);
      paramKey = localStringBuilder;
      if (localObject != null) {
        paramKey = ((DiskLruCache.Value)localObject).getFile(0);
      }
    }
    catch (IOException localIOException)
    {
      paramKey = localStringBuilder;
      if (Log.isLoggable("DiskLruCacheWrapper", 5))
      {
        Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", localIOException);
        paramKey = localStringBuilder;
      }
    }
    return paramKey;
  }
  
  /* Error */
  public void put(Key paramKey, DiskCache.Writer paramWriter)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 46	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:safeKeyGenerator	Lcom/bumptech/glide/load/engine/cache/SafeKeyGenerator;
    //   4: aload_1
    //   5: invokevirtual 94	com/bumptech/glide/load/engine/cache/SafeKeyGenerator:getSafeKey	(Lcom/bumptech/glide/load/Key;)Ljava/lang/String;
    //   8: astore_3
    //   9: aload_0
    //   10: getfield 37	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:writeLocker	Lcom/bumptech/glide/load/engine/cache/DiskCacheWriteLocker;
    //   13: aload_3
    //   14: invokevirtual 140	com/bumptech/glide/load/engine/cache/DiskCacheWriteLocker:acquire	(Ljava/lang/String;)V
    //   17: ldc 13
    //   19: iconst_2
    //   20: invokestatic 83	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   23: ifeq +54 -> 77
    //   26: new 103	java/lang/StringBuilder
    //   29: astore 4
    //   31: aload 4
    //   33: invokespecial 104	java/lang/StringBuilder:<init>	()V
    //   36: aload 4
    //   38: ldc -114
    //   40: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload 4
    //   46: aload_3
    //   47: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload 4
    //   53: ldc 112
    //   55: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload 4
    //   61: aload_1
    //   62: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: ldc 13
    //   68: aload 4
    //   70: invokevirtual 119	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   73: invokestatic 123	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   76: pop
    //   77: aload_0
    //   78: invokespecial 72	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:getDiskCache	()Lcom/bumptech/glide/disklrucache/DiskLruCache;
    //   81: astore_1
    //   82: aload_1
    //   83: aload_3
    //   84: invokevirtual 126	com/bumptech/glide/disklrucache/DiskLruCache:get	(Ljava/lang/String;)Lcom/bumptech/glide/disklrucache/DiskLruCache$Value;
    //   87: astore 4
    //   89: aload 4
    //   91: ifnull +12 -> 103
    //   94: aload_0
    //   95: getfield 37	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:writeLocker	Lcom/bumptech/glide/load/engine/cache/DiskCacheWriteLocker;
    //   98: aload_3
    //   99: invokevirtual 145	com/bumptech/glide/load/engine/cache/DiskCacheWriteLocker:release	(Ljava/lang/String;)V
    //   102: return
    //   103: aload_1
    //   104: aload_3
    //   105: invokevirtual 149	com/bumptech/glide/disklrucache/DiskLruCache:edit	(Ljava/lang/String;)Lcom/bumptech/glide/disklrucache/DiskLruCache$Editor;
    //   108: astore_1
    //   109: aload_1
    //   110: ifnull +35 -> 145
    //   113: aload_2
    //   114: aload_1
    //   115: iconst_0
    //   116: invokevirtual 152	com/bumptech/glide/disklrucache/DiskLruCache$Editor:getFile	(I)Ljava/io/File;
    //   119: invokeinterface 158 2 0
    //   124: ifeq +7 -> 131
    //   127: aload_1
    //   128: invokevirtual 161	com/bumptech/glide/disklrucache/DiskLruCache$Editor:commit	()V
    //   131: aload_1
    //   132: invokevirtual 164	com/bumptech/glide/disklrucache/DiskLruCache$Editor:abortUnlessCommitted	()V
    //   135: goto +64 -> 199
    //   138: astore_2
    //   139: aload_1
    //   140: invokevirtual 164	com/bumptech/glide/disklrucache/DiskLruCache$Editor:abortUnlessCommitted	()V
    //   143: aload_2
    //   144: athrow
    //   145: new 166	java/lang/IllegalStateException
    //   148: astore_1
    //   149: new 103	java/lang/StringBuilder
    //   152: astore_2
    //   153: aload_2
    //   154: invokespecial 104	java/lang/StringBuilder:<init>	()V
    //   157: aload_2
    //   158: ldc -88
    //   160: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload_2
    //   165: aload_3
    //   166: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload_1
    //   171: aload_2
    //   172: invokevirtual 119	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   175: invokespecial 170	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   178: aload_1
    //   179: athrow
    //   180: astore_1
    //   181: ldc 13
    //   183: iconst_5
    //   184: invokestatic 83	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   187: ifeq +12 -> 199
    //   190: ldc 13
    //   192: ldc -84
    //   194: aload_1
    //   195: invokestatic 89	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   198: pop
    //   199: aload_0
    //   200: getfield 37	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:writeLocker	Lcom/bumptech/glide/load/engine/cache/DiskCacheWriteLocker;
    //   203: aload_3
    //   204: invokevirtual 145	com/bumptech/glide/load/engine/cache/DiskCacheWriteLocker:release	(Ljava/lang/String;)V
    //   207: return
    //   208: astore_1
    //   209: aload_0
    //   210: getfield 37	com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper:writeLocker	Lcom/bumptech/glide/load/engine/cache/DiskCacheWriteLocker;
    //   213: aload_3
    //   214: invokevirtual 145	com/bumptech/glide/load/engine/cache/DiskCacheWriteLocker:release	(Ljava/lang/String;)V
    //   217: aload_1
    //   218: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	219	0	this	DiskLruCacheWrapper
    //   0	219	1	paramKey	Key
    //   0	219	2	paramWriter	DiskCache.Writer
    //   8	206	3	str	String
    //   29	61	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   113	131	138	finally
    //   77	89	180	java/io/IOException
    //   103	109	180	java/io/IOException
    //   131	135	180	java/io/IOException
    //   139	145	180	java/io/IOException
    //   145	180	180	java/io/IOException
    //   17	77	208	finally
    //   77	89	208	finally
    //   103	109	208	finally
    //   131	135	208	finally
    //   139	145	208	finally
    //   145	180	208	finally
    //   181	199	208	finally
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */