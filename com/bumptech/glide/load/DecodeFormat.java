package com.bumptech.glide.load;

public enum DecodeFormat
{
  public static final DecodeFormat DEFAULT;
  
  static
  {
    DecodeFormat localDecodeFormat = PREFER_ARGB_8888;
    $VALUES = new DecodeFormat[] { localDecodeFormat, PREFER_RGB_565 };
    DEFAULT = localDecodeFormat;
  }
  
  private DecodeFormat() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/DecodeFormat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */