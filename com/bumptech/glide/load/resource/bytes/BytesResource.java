package com.bumptech.glide.load.resource.bytes;

import android.support.annotation.NonNull;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;

public class BytesResource
  implements Resource<byte[]>
{
  private final byte[] bytes;
  
  public BytesResource(byte[] paramArrayOfByte)
  {
    this.bytes = ((byte[])Preconditions.checkNotNull(paramArrayOfByte));
  }
  
  @NonNull
  public byte[] get()
  {
    return this.bytes;
  }
  
  @NonNull
  public Class<byte[]> getResourceClass()
  {
    return byte[].class;
  }
  
  public int getSize()
  {
    return this.bytes.length;
  }
  
  public void recycle() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bytes/BytesResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */