package com.bumptech.glide.load.resource.gif;

import android.support.annotation.NonNull;
import android.util.Log;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;

public class GifDrawableEncoder
  implements ResourceEncoder<GifDrawable>
{
  private static final String TAG = "GifEncoder";
  
  public boolean encode(@NonNull Resource<GifDrawable> paramResource, @NonNull File paramFile, @NonNull Options paramOptions)
  {
    paramResource = (GifDrawable)paramResource.get();
    boolean bool;
    try
    {
      ByteBufferUtil.toFile(paramResource.getBuffer(), paramFile);
      bool = true;
    }
    catch (IOException paramResource)
    {
      if (Log.isLoggable("GifEncoder", 5)) {
        Log.w("GifEncoder", "Failed to encode GIF drawable data", paramResource);
      }
      bool = false;
    }
    return bool;
  }
  
  @NonNull
  public EncodeStrategy getEncodeStrategy(@NonNull Options paramOptions)
  {
    return EncodeStrategy.SOURCE;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/gif/GifDrawableEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */