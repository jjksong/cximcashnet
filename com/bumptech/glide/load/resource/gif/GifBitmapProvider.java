package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.gifdecoder.GifDecoder.BitmapProvider;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public final class GifBitmapProvider
  implements GifDecoder.BitmapProvider
{
  @Nullable
  private final ArrayPool arrayPool;
  private final BitmapPool bitmapPool;
  
  public GifBitmapProvider(BitmapPool paramBitmapPool)
  {
    this(paramBitmapPool, null);
  }
  
  public GifBitmapProvider(BitmapPool paramBitmapPool, @Nullable ArrayPool paramArrayPool)
  {
    this.bitmapPool = paramBitmapPool;
    this.arrayPool = paramArrayPool;
  }
  
  @NonNull
  public Bitmap obtain(int paramInt1, int paramInt2, @NonNull Bitmap.Config paramConfig)
  {
    return this.bitmapPool.getDirty(paramInt1, paramInt2, paramConfig);
  }
  
  @NonNull
  public byte[] obtainByteArray(int paramInt)
  {
    ArrayPool localArrayPool = this.arrayPool;
    if (localArrayPool == null) {
      return new byte[paramInt];
    }
    return (byte[])localArrayPool.get(paramInt, byte[].class);
  }
  
  @NonNull
  public int[] obtainIntArray(int paramInt)
  {
    ArrayPool localArrayPool = this.arrayPool;
    if (localArrayPool == null) {
      return new int[paramInt];
    }
    return (int[])localArrayPool.get(paramInt, int[].class);
  }
  
  public void release(@NonNull Bitmap paramBitmap)
  {
    this.bitmapPool.put(paramBitmap);
  }
  
  public void release(@NonNull byte[] paramArrayOfByte)
  {
    ArrayPool localArrayPool = this.arrayPool;
    if (localArrayPool == null) {
      return;
    }
    localArrayPool.put(paramArrayOfByte);
  }
  
  public void release(@NonNull int[] paramArrayOfInt)
  {
    ArrayPool localArrayPool = this.arrayPool;
    if (localArrayPool == null) {
      return;
    }
    localArrayPool.put(paramArrayOfInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/gif/GifBitmapProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */