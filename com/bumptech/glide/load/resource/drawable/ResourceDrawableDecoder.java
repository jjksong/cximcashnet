package com.bumptech.glide.load.resource.drawable;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.util.List;

public class ResourceDrawableDecoder
  implements ResourceDecoder<Uri, Drawable>
{
  private static final String ANDROID_PACKAGE_NAME = "android";
  private static final int ID_PATH_SEGMENTS = 1;
  private static final int MISSING_RESOURCE_ID = 0;
  private static final int NAME_PATH_SEGMENT_INDEX = 1;
  private static final int NAME_URI_PATH_SEGMENTS = 2;
  private static final int RESOURCE_ID_SEGMENT_INDEX = 0;
  private static final int TYPE_PATH_SEGMENT_INDEX = 0;
  private final Context context;
  
  public ResourceDrawableDecoder(Context paramContext)
  {
    this.context = paramContext.getApplicationContext();
  }
  
  @NonNull
  private Context findContextForPackage(Uri paramUri, String paramString)
  {
    if (paramString.equals(this.context.getPackageName())) {
      return this.context;
    }
    try
    {
      Context localContext = this.context.createPackageContext(paramString, 0);
      return localContext;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      if (paramString.contains(this.context.getPackageName())) {
        return this.context;
      }
      paramString = new StringBuilder();
      paramString.append("Failed to obtain context or unrecognized Uri format for: ");
      paramString.append(paramUri);
      throw new IllegalArgumentException(paramString.toString(), localNameNotFoundException);
    }
  }
  
  @DrawableRes
  private int findResourceIdFromResourceIdUri(Uri paramUri)
  {
    Object localObject = paramUri.getPathSegments();
    try
    {
      int i = Integer.parseInt((String)((List)localObject).get(0));
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Unrecognized Uri format: ");
      ((StringBuilder)localObject).append(paramUri);
      throw new IllegalArgumentException(((StringBuilder)localObject).toString(), localNumberFormatException);
    }
  }
  
  @DrawableRes
  private int findResourceIdFromTypeAndNameResourceUri(Context paramContext, Uri paramUri)
  {
    Object localObject = paramUri.getPathSegments();
    String str2 = paramUri.getAuthority();
    String str1 = (String)((List)localObject).get(0);
    localObject = (String)((List)localObject).get(1);
    int j = paramContext.getResources().getIdentifier((String)localObject, str1, str2);
    int i = j;
    if (j == 0) {
      i = Resources.getSystem().getIdentifier((String)localObject, str1, "android");
    }
    if (i != 0) {
      return i;
    }
    paramContext = new StringBuilder();
    paramContext.append("Failed to find resource id for: ");
    paramContext.append(paramUri);
    throw new IllegalArgumentException(paramContext.toString());
  }
  
  @DrawableRes
  private int findResourceIdFromUri(Context paramContext, Uri paramUri)
  {
    List localList = paramUri.getPathSegments();
    if (localList.size() == 2) {
      return findResourceIdFromTypeAndNameResourceUri(paramContext, paramUri);
    }
    if (localList.size() == 1) {
      return findResourceIdFromResourceIdUri(paramUri);
    }
    paramContext = new StringBuilder();
    paramContext.append("Unrecognized Uri format: ");
    paramContext.append(paramUri);
    throw new IllegalArgumentException(paramContext.toString());
  }
  
  @Nullable
  public Resource<Drawable> decode(@NonNull Uri paramUri, int paramInt1, int paramInt2, @NonNull Options paramOptions)
  {
    paramOptions = findContextForPackage(paramUri, paramUri.getAuthority());
    paramInt1 = findResourceIdFromUri(paramOptions, paramUri);
    return NonOwnedDrawableResource.newInstance(DrawableDecoderCompat.getDrawable(this.context, paramOptions, paramInt1));
  }
  
  public boolean handles(@NonNull Uri paramUri, @NonNull Options paramOptions)
  {
    return paramUri.getScheme().equals("android.resource");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/drawable/ResourceDrawableDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */