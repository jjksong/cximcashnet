package com.bumptech.glide.load.resource.drawable;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.annotation.NonNull;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.Preconditions;

public abstract class DrawableResource<T extends Drawable>
  implements Resource<T>, Initializable
{
  protected final T drawable;
  
  public DrawableResource(T paramT)
  {
    this.drawable = ((Drawable)Preconditions.checkNotNull(paramT));
  }
  
  @NonNull
  public final T get()
  {
    Drawable.ConstantState localConstantState = this.drawable.getConstantState();
    if (localConstantState == null) {
      return this.drawable;
    }
    return localConstantState.newDrawable();
  }
  
  public void initialize()
  {
    Drawable localDrawable = this.drawable;
    if ((localDrawable instanceof BitmapDrawable)) {
      ((BitmapDrawable)localDrawable).getBitmap().prepareToDraw();
    } else if ((localDrawable instanceof GifDrawable)) {
      ((GifDrawable)localDrawable).getFirstFrame().prepareToDraw();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/drawable/DrawableResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */