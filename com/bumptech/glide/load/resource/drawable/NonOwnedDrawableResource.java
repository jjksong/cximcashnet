package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.load.engine.Resource;

final class NonOwnedDrawableResource
  extends DrawableResource<Drawable>
{
  private NonOwnedDrawableResource(Drawable paramDrawable)
  {
    super(paramDrawable);
  }
  
  @Nullable
  static Resource<Drawable> newInstance(@Nullable Drawable paramDrawable)
  {
    if (paramDrawable != null) {
      paramDrawable = new NonOwnedDrawableResource(paramDrawable);
    } else {
      paramDrawable = null;
    }
    return paramDrawable;
  }
  
  @NonNull
  public Class<Drawable> getResourceClass()
  {
    return this.drawable.getClass();
  }
  
  public int getSize()
  {
    return Math.max(1, this.drawable.getIntrinsicWidth() * this.drawable.getIntrinsicHeight() * 4);
  }
  
  public void recycle() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/drawable/NonOwnedDrawableResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */