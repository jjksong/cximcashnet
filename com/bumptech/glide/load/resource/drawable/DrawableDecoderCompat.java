package com.bumptech.glide.load.resource.drawable;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.view.ContextThemeWrapper;

public final class DrawableDecoderCompat
{
  private static volatile boolean shouldCallAppCompatResources = true;
  
  public static Drawable getDrawable(Context paramContext, @DrawableRes int paramInt, @Nullable Resources.Theme paramTheme)
  {
    return getDrawable(paramContext, paramContext, paramInt, paramTheme);
  }
  
  public static Drawable getDrawable(Context paramContext1, Context paramContext2, @DrawableRes int paramInt)
  {
    return getDrawable(paramContext1, paramContext2, paramInt, null);
  }
  
  private static Drawable getDrawable(Context paramContext1, Context paramContext2, @DrawableRes int paramInt, @Nullable Resources.Theme paramTheme)
  {
    try
    {
      if (shouldCallAppCompatResources)
      {
        Drawable localDrawable = loadDrawableV7(paramContext2, paramInt, paramTheme);
        return localDrawable;
      }
    }
    catch (Resources.NotFoundException paramContext1) {}catch (IllegalStateException paramTheme)
    {
      if (!paramContext1.getPackageName().equals(paramContext2.getPackageName())) {
        return ContextCompat.getDrawable(paramContext2, paramInt);
      }
      throw paramTheme;
    }
    catch (NoClassDefFoundError paramContext1)
    {
      shouldCallAppCompatResources = false;
    }
    if (paramTheme == null) {
      paramTheme = paramContext2.getTheme();
    }
    return loadDrawableV4(paramContext2, paramInt, paramTheme);
  }
  
  private static Drawable loadDrawableV4(Context paramContext, @DrawableRes int paramInt, @Nullable Resources.Theme paramTheme)
  {
    return ResourcesCompat.getDrawable(paramContext.getResources(), paramInt, paramTheme);
  }
  
  private static Drawable loadDrawableV7(Context paramContext, @DrawableRes int paramInt, @Nullable Resources.Theme paramTheme)
  {
    Object localObject = paramContext;
    if (paramTheme != null) {
      localObject = new ContextThemeWrapper(paramContext, paramTheme);
    }
    return AppCompatResources.getDrawable((Context)localObject, paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/drawable/DrawableDecoderCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */