package com.bumptech.glide.load.resource.file;

import com.bumptech.glide.load.resource.SimpleResource;
import java.io.File;

public class FileResource
  extends SimpleResource<File>
{
  public FileResource(File paramFile)
  {
    super(paramFile);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/file/FileResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */