package com.bumptech.glide.load.resource.transcode;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TranscoderRegistry
{
  private final List<Entry<?, ?>> transcoders = new ArrayList();
  
  @NonNull
  public <Z, R> ResourceTranscoder<Z, R> get(@NonNull Class<Z> paramClass, @NonNull Class<R> paramClass1)
  {
    try
    {
      if (paramClass1.isAssignableFrom(paramClass))
      {
        paramClass = UnitTranscoder.get();
        return paramClass;
      }
      Object localObject2 = this.transcoders.iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject1 = (Entry)((Iterator)localObject2).next();
        if (((Entry)localObject1).handles(paramClass, paramClass1))
        {
          paramClass = ((Entry)localObject1).transcoder;
          return paramClass;
        }
      }
      Object localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append("No transcoder registered to transcode from ");
      ((StringBuilder)localObject2).append(paramClass);
      ((StringBuilder)localObject2).append(" to ");
      ((StringBuilder)localObject2).append(paramClass1);
      ((IllegalArgumentException)localObject1).<init>(((StringBuilder)localObject2).toString());
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  @NonNull
  public <Z, R> List<Class<R>> getTranscodeClasses(@NonNull Class<Z> paramClass, @NonNull Class<R> paramClass1)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      if (paramClass1.isAssignableFrom(paramClass))
      {
        localArrayList.add(paramClass1);
        return localArrayList;
      }
      Iterator localIterator = this.transcoders.iterator();
      while (localIterator.hasNext()) {
        if (((Entry)localIterator.next()).handles(paramClass, paramClass1)) {
          localArrayList.add(paramClass1);
        }
      }
      return localArrayList;
    }
    finally {}
  }
  
  public <Z, R> void register(@NonNull Class<Z> paramClass, @NonNull Class<R> paramClass1, @NonNull ResourceTranscoder<Z, R> paramResourceTranscoder)
  {
    try
    {
      List localList = this.transcoders;
      Entry localEntry = new com/bumptech/glide/load/resource/transcode/TranscoderRegistry$Entry;
      localEntry.<init>(paramClass, paramClass1, paramResourceTranscoder);
      localList.add(localEntry);
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  private static final class Entry<Z, R>
  {
    private final Class<Z> fromClass;
    private final Class<R> toClass;
    final ResourceTranscoder<Z, R> transcoder;
    
    Entry(@NonNull Class<Z> paramClass, @NonNull Class<R> paramClass1, @NonNull ResourceTranscoder<Z, R> paramResourceTranscoder)
    {
      this.fromClass = paramClass;
      this.toClass = paramClass1;
      this.transcoder = paramResourceTranscoder;
    }
    
    public boolean handles(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2)
    {
      boolean bool;
      if ((this.fromClass.isAssignableFrom(paramClass1)) && (paramClass2.isAssignableFrom(this.toClass))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/transcode/TranscoderRegistry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */