package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;

public class BitmapEncoder
  implements ResourceEncoder<Bitmap>
{
  public static final Option<Bitmap.CompressFormat> COMPRESSION_FORMAT = Option.memory("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
  public static final Option<Integer> COMPRESSION_QUALITY = Option.memory("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", Integer.valueOf(90));
  private static final String TAG = "BitmapEncoder";
  @Nullable
  private final ArrayPool arrayPool;
  
  @Deprecated
  public BitmapEncoder()
  {
    this.arrayPool = null;
  }
  
  public BitmapEncoder(@NonNull ArrayPool paramArrayPool)
  {
    this.arrayPool = paramArrayPool;
  }
  
  private Bitmap.CompressFormat getFormat(Bitmap paramBitmap, Options paramOptions)
  {
    paramOptions = (Bitmap.CompressFormat)paramOptions.get(COMPRESSION_FORMAT);
    if (paramOptions != null) {
      return paramOptions;
    }
    if (paramBitmap.hasAlpha()) {
      return Bitmap.CompressFormat.PNG;
    }
    return Bitmap.CompressFormat.JPEG;
  }
  
  /* Error */
  public boolean encode(@NonNull com.bumptech.glide.load.engine.Resource<Bitmap> paramResource, @NonNull java.io.File paramFile, @NonNull Options paramOptions)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface 87 1 0
    //   6: checkcast 67	android/graphics/Bitmap
    //   9: astore 14
    //   11: aload_0
    //   12: aload 14
    //   14: aload_3
    //   15: invokespecial 89	com/bumptech/glide/load/resource/bitmap/BitmapEncoder:getFormat	(Landroid/graphics/Bitmap;Lcom/bumptech/glide/load/Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   18: astore 13
    //   20: ldc 91
    //   22: aload 14
    //   24: invokevirtual 95	android/graphics/Bitmap:getWidth	()I
    //   27: invokestatic 29	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   30: aload 14
    //   32: invokevirtual 98	android/graphics/Bitmap:getHeight	()I
    //   35: invokestatic 29	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   38: aload 13
    //   40: invokestatic 104	com/bumptech/glide/util/pool/GlideTrace:beginSectionFormat	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   43: invokestatic 110	com/bumptech/glide/util/LogTime:getLogTime	()J
    //   46: lstore 7
    //   48: aload_3
    //   49: getstatic 37	com/bumptech/glide/load/resource/bitmap/BitmapEncoder:COMPRESSION_QUALITY	Lcom/bumptech/glide/load/Option;
    //   52: invokevirtual 63	com/bumptech/glide/load/Options:get	(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
    //   55: checkcast 25	java/lang/Integer
    //   58: invokevirtual 113	java/lang/Integer:intValue	()I
    //   61: istore 4
    //   63: iconst_0
    //   64: istore 5
    //   66: iconst_0
    //   67: istore 6
    //   69: aconst_null
    //   70: astore 12
    //   72: aconst_null
    //   73: astore 11
    //   75: aload 11
    //   77: astore 9
    //   79: aload 12
    //   81: astore 10
    //   83: new 115	java/io/FileOutputStream
    //   86: astore_1
    //   87: aload 11
    //   89: astore 9
    //   91: aload 12
    //   93: astore 10
    //   95: aload_1
    //   96: aload_2
    //   97: invokespecial 118	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   100: aload_0
    //   101: getfield 51	com/bumptech/glide/load/resource/bitmap/BitmapEncoder:arrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   104: ifnull +21 -> 125
    //   107: new 120	com/bumptech/glide/load/data/BufferedOutputStream
    //   110: astore_2
    //   111: aload_2
    //   112: aload_1
    //   113: aload_0
    //   114: getfield 51	com/bumptech/glide/load/resource/bitmap/BitmapEncoder:arrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   117: invokespecial 123	com/bumptech/glide/load/data/BufferedOutputStream:<init>	(Ljava/io/OutputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)V
    //   120: aload_2
    //   121: astore_1
    //   122: goto +3 -> 125
    //   125: aload_1
    //   126: astore 9
    //   128: aload_1
    //   129: astore 10
    //   131: aload 14
    //   133: aload 13
    //   135: iload 4
    //   137: aload_1
    //   138: invokevirtual 127	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   141: pop
    //   142: aload_1
    //   143: astore 9
    //   145: aload_1
    //   146: astore 10
    //   148: aload_1
    //   149: invokevirtual 132	java/io/OutputStream:close	()V
    //   152: iconst_1
    //   153: istore 5
    //   155: aload_1
    //   156: invokevirtual 132	java/io/OutputStream:close	()V
    //   159: goto +57 -> 216
    //   162: astore_2
    //   163: aload_1
    //   164: astore 9
    //   166: goto +167 -> 333
    //   169: astore_2
    //   170: goto +11 -> 181
    //   173: astore_2
    //   174: goto +159 -> 333
    //   177: astore_2
    //   178: aload 10
    //   180: astore_1
    //   181: aload_1
    //   182: astore 9
    //   184: ldc 16
    //   186: iconst_3
    //   187: invokestatic 138	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   190: ifeq +15 -> 205
    //   193: aload_1
    //   194: astore 9
    //   196: ldc 16
    //   198: ldc -116
    //   200: aload_2
    //   201: invokestatic 144	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   204: pop
    //   205: aload_1
    //   206: ifnull +10 -> 216
    //   209: iload 6
    //   211: istore 5
    //   213: goto -58 -> 155
    //   216: ldc 16
    //   218: iconst_2
    //   219: invokestatic 138	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   222: ifeq +105 -> 327
    //   225: new 146	java/lang/StringBuilder
    //   228: astore_1
    //   229: aload_1
    //   230: invokespecial 147	java/lang/StringBuilder:<init>	()V
    //   233: aload_1
    //   234: ldc -107
    //   236: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: pop
    //   240: aload_1
    //   241: aload 13
    //   243: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload_1
    //   248: ldc -98
    //   250: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   253: pop
    //   254: aload_1
    //   255: aload 14
    //   257: invokestatic 164	com/bumptech/glide/util/Util:getBitmapByteSize	(Landroid/graphics/Bitmap;)I
    //   260: invokevirtual 167	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   263: pop
    //   264: aload_1
    //   265: ldc -87
    //   267: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload_1
    //   272: lload 7
    //   274: invokestatic 173	com/bumptech/glide/util/LogTime:getElapsedMillis	(J)D
    //   277: invokevirtual 176	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   280: pop
    //   281: aload_1
    //   282: ldc -78
    //   284: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: pop
    //   288: aload_1
    //   289: aload_3
    //   290: getstatic 44	com/bumptech/glide/load/resource/bitmap/BitmapEncoder:COMPRESSION_FORMAT	Lcom/bumptech/glide/load/Option;
    //   293: invokevirtual 63	com/bumptech/glide/load/Options:get	(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
    //   296: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_1
    //   301: ldc -76
    //   303: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload_1
    //   308: aload 14
    //   310: invokevirtual 71	android/graphics/Bitmap:hasAlpha	()Z
    //   313: invokevirtual 183	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   316: pop
    //   317: ldc 16
    //   319: aload_1
    //   320: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   323: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   326: pop
    //   327: invokestatic 194	com/bumptech/glide/util/pool/GlideTrace:endSection	()V
    //   330: iload 5
    //   332: ireturn
    //   333: aload 9
    //   335: ifnull +8 -> 343
    //   338: aload 9
    //   340: invokevirtual 132	java/io/OutputStream:close	()V
    //   343: aload_2
    //   344: athrow
    //   345: astore_1
    //   346: invokestatic 194	com/bumptech/glide/util/pool/GlideTrace:endSection	()V
    //   349: aload_1
    //   350: athrow
    //   351: astore_1
    //   352: goto -136 -> 216
    //   355: astore_1
    //   356: goto -13 -> 343
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	359	0	this	BitmapEncoder
    //   0	359	1	paramResource	com.bumptech.glide.load.engine.Resource<Bitmap>
    //   0	359	2	paramFile	java.io.File
    //   0	359	3	paramOptions	Options
    //   61	75	4	i	int
    //   64	267	5	bool1	boolean
    //   67	143	6	bool2	boolean
    //   46	227	7	l	long
    //   77	262	9	localObject1	Object
    //   81	98	10	localObject2	Object
    //   73	15	11	localObject3	Object
    //   70	22	12	localObject4	Object
    //   18	224	13	localCompressFormat	Bitmap.CompressFormat
    //   9	300	14	localBitmap	Bitmap
    // Exception table:
    //   from	to	target	type
    //   100	120	162	finally
    //   100	120	169	java/io/IOException
    //   83	87	173	finally
    //   95	100	173	finally
    //   131	142	173	finally
    //   148	152	173	finally
    //   184	193	173	finally
    //   196	205	173	finally
    //   83	87	177	java/io/IOException
    //   95	100	177	java/io/IOException
    //   131	142	177	java/io/IOException
    //   148	152	177	java/io/IOException
    //   43	63	345	finally
    //   155	159	345	finally
    //   216	327	345	finally
    //   338	343	345	finally
    //   343	345	345	finally
    //   155	159	351	java/io/IOException
    //   338	343	355	java/io/IOException
  }
  
  @NonNull
  public EncodeStrategy getEncodeStrategy(@NonNull Options paramOptions)
  {
    return EncodeStrategy.TRANSFORMED;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/BitmapEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */