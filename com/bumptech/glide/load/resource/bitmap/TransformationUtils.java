package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class TransformationUtils
{
  private static final Lock BITMAP_DRAWABLE_LOCK;
  private static final Paint CIRCLE_CROP_BITMAP_PAINT;
  private static final int CIRCLE_CROP_PAINT_FLAGS = 7;
  private static final Paint CIRCLE_CROP_SHAPE_PAINT;
  private static final Paint DEFAULT_PAINT = new Paint(6);
  private static final Set<String> MODELS_REQUIRING_BITMAP_LOCK;
  public static final int PAINT_FLAGS = 6;
  private static final String TAG = "TransformationUtils";
  
  static
  {
    CIRCLE_CROP_SHAPE_PAINT = new Paint(7);
    MODELS_REQUIRING_BITMAP_LOCK = new HashSet(Arrays.asList(new String[] { "XT1085", "XT1092", "XT1093", "XT1094", "XT1095", "XT1096", "XT1097", "XT1098", "XT1031", "XT1028", "XT937C", "XT1032", "XT1008", "XT1033", "XT1035", "XT1034", "XT939G", "XT1039", "XT1040", "XT1042", "XT1045", "XT1063", "XT1064", "XT1068", "XT1069", "XT1072", "XT1077", "XT1078", "XT1079" }));
    Object localObject;
    if (MODELS_REQUIRING_BITMAP_LOCK.contains(Build.MODEL)) {
      localObject = new ReentrantLock();
    } else {
      localObject = new NoLock();
    }
    BITMAP_DRAWABLE_LOCK = (Lock)localObject;
    CIRCLE_CROP_BITMAP_PAINT = new Paint(7);
    CIRCLE_CROP_BITMAP_PAINT.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
  }
  
  private static void applyMatrix(@NonNull Bitmap paramBitmap1, @NonNull Bitmap paramBitmap2, Matrix paramMatrix)
  {
    BITMAP_DRAWABLE_LOCK.lock();
    try
    {
      Canvas localCanvas = new android/graphics/Canvas;
      localCanvas.<init>(paramBitmap2);
      localCanvas.drawBitmap(paramBitmap1, paramMatrix, DEFAULT_PAINT);
      clear(localCanvas);
      return;
    }
    finally
    {
      BITMAP_DRAWABLE_LOCK.unlock();
    }
  }
  
  public static Bitmap centerCrop(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if ((paramBitmap.getWidth() == paramInt1) && (paramBitmap.getHeight() == paramInt2)) {
      return paramBitmap;
    }
    Matrix localMatrix = new Matrix();
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    float f2 = 0.0F;
    float f3;
    float f1;
    if (i * paramInt2 > j * paramInt1)
    {
      f3 = paramInt2 / paramBitmap.getHeight();
      f1 = (paramInt1 - paramBitmap.getWidth() * f3) * 0.5F;
    }
    else
    {
      f3 = paramInt1 / paramBitmap.getWidth();
      f2 = (paramInt2 - paramBitmap.getHeight() * f3) * 0.5F;
      f1 = 0.0F;
    }
    localMatrix.setScale(f3, f3);
    localMatrix.postTranslate((int)(f1 + 0.5F), (int)(f2 + 0.5F));
    paramBitmapPool = paramBitmapPool.get(paramInt1, paramInt2, getNonNullConfig(paramBitmap));
    setAlpha(paramBitmap, paramBitmapPool);
    applyMatrix(paramBitmap, paramBitmapPool, localMatrix);
    return paramBitmapPool;
  }
  
  public static Bitmap centerInside(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if ((paramBitmap.getWidth() <= paramInt1) && (paramBitmap.getHeight() <= paramInt2))
    {
      if (Log.isLoggable("TransformationUtils", 2)) {
        Log.v("TransformationUtils", "requested target size larger or equal to input, returning input");
      }
      return paramBitmap;
    }
    if (Log.isLoggable("TransformationUtils", 2)) {
      Log.v("TransformationUtils", "requested target size too big for input, fit centering instead");
    }
    return fitCenter(paramBitmapPool, paramBitmap, paramInt1, paramInt2);
  }
  
  public static Bitmap circleCrop(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    paramInt2 = Math.min(paramInt1, paramInt2);
    float f2 = paramInt2;
    float f1 = f2 / 2.0F;
    paramInt1 = paramBitmap.getWidth();
    int i = paramBitmap.getHeight();
    float f3 = paramInt1;
    float f5 = f2 / f3;
    float f4 = i;
    f5 = Math.max(f5, f2 / f4);
    f3 *= f5;
    f4 = f5 * f4;
    f5 = (f2 - f3) / 2.0F;
    f2 = (f2 - f4) / 2.0F;
    RectF localRectF = new RectF(f5, f2, f3 + f5, f4 + f2);
    Bitmap localBitmap1 = getAlphaSafeBitmap(paramBitmapPool, paramBitmap);
    Bitmap localBitmap2 = paramBitmapPool.get(paramInt2, paramInt2, getAlphaSafeConfig(paramBitmap));
    localBitmap2.setHasAlpha(true);
    BITMAP_DRAWABLE_LOCK.lock();
    try
    {
      Canvas localCanvas = new android/graphics/Canvas;
      localCanvas.<init>(localBitmap2);
      localCanvas.drawCircle(f1, f1, f1, CIRCLE_CROP_SHAPE_PAINT);
      localCanvas.drawBitmap(localBitmap1, null, localRectF, CIRCLE_CROP_BITMAP_PAINT);
      clear(localCanvas);
      BITMAP_DRAWABLE_LOCK.unlock();
      if (!localBitmap1.equals(paramBitmap)) {
        paramBitmapPool.put(localBitmap1);
      }
      return localBitmap2;
    }
    finally
    {
      BITMAP_DRAWABLE_LOCK.unlock();
    }
  }
  
  private static void clear(Canvas paramCanvas)
  {
    paramCanvas.setBitmap(null);
  }
  
  public static Bitmap fitCenter(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if ((paramBitmap.getWidth() == paramInt1) && (paramBitmap.getHeight() == paramInt2))
    {
      if (Log.isLoggable("TransformationUtils", 2)) {
        Log.v("TransformationUtils", "requested target size matches input, returning input");
      }
      return paramBitmap;
    }
    float f = Math.min(paramInt1 / paramBitmap.getWidth(), paramInt2 / paramBitmap.getHeight());
    int j = Math.round(paramBitmap.getWidth() * f);
    int i = Math.round(paramBitmap.getHeight() * f);
    if ((paramBitmap.getWidth() == j) && (paramBitmap.getHeight() == i))
    {
      if (Log.isLoggable("TransformationUtils", 2)) {
        Log.v("TransformationUtils", "adjusted target size matches input, returning input");
      }
      return paramBitmap;
    }
    paramBitmapPool = paramBitmapPool.get((int)(paramBitmap.getWidth() * f), (int)(paramBitmap.getHeight() * f), getNonNullConfig(paramBitmap));
    setAlpha(paramBitmap, paramBitmapPool);
    if (Log.isLoggable("TransformationUtils", 2))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("request: ");
      ((StringBuilder)localObject).append(paramInt1);
      ((StringBuilder)localObject).append("x");
      ((StringBuilder)localObject).append(paramInt2);
      Log.v("TransformationUtils", ((StringBuilder)localObject).toString());
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("toFit:   ");
      ((StringBuilder)localObject).append(paramBitmap.getWidth());
      ((StringBuilder)localObject).append("x");
      ((StringBuilder)localObject).append(paramBitmap.getHeight());
      Log.v("TransformationUtils", ((StringBuilder)localObject).toString());
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("toReuse: ");
      ((StringBuilder)localObject).append(paramBitmapPool.getWidth());
      ((StringBuilder)localObject).append("x");
      ((StringBuilder)localObject).append(paramBitmapPool.getHeight());
      Log.v("TransformationUtils", ((StringBuilder)localObject).toString());
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("minPct:   ");
      ((StringBuilder)localObject).append(f);
      Log.v("TransformationUtils", ((StringBuilder)localObject).toString());
    }
    Object localObject = new Matrix();
    ((Matrix)localObject).setScale(f, f);
    applyMatrix(paramBitmap, paramBitmapPool, (Matrix)localObject);
    return paramBitmapPool;
  }
  
  private static Bitmap getAlphaSafeBitmap(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap)
  {
    Bitmap.Config localConfig = getAlphaSafeConfig(paramBitmap);
    if (localConfig.equals(paramBitmap.getConfig())) {
      return paramBitmap;
    }
    paramBitmapPool = paramBitmapPool.get(paramBitmap.getWidth(), paramBitmap.getHeight(), localConfig);
    new Canvas(paramBitmapPool).drawBitmap(paramBitmap, 0.0F, 0.0F, null);
    return paramBitmapPool;
  }
  
  @NonNull
  private static Bitmap.Config getAlphaSafeConfig(@NonNull Bitmap paramBitmap)
  {
    if ((Build.VERSION.SDK_INT >= 26) && (Bitmap.Config.RGBA_F16.equals(paramBitmap.getConfig()))) {
      return Bitmap.Config.RGBA_F16;
    }
    return Bitmap.Config.ARGB_8888;
  }
  
  public static Lock getBitmapDrawableLock()
  {
    return BITMAP_DRAWABLE_LOCK;
  }
  
  public static int getExifOrientationDegrees(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      paramInt = 0;
      break;
    case 7: 
    case 8: 
      paramInt = 270;
      break;
    case 5: 
    case 6: 
      paramInt = 90;
      break;
    case 3: 
    case 4: 
      paramInt = 180;
    }
    return paramInt;
  }
  
  @NonNull
  private static Bitmap.Config getNonNullConfig(@NonNull Bitmap paramBitmap)
  {
    if (paramBitmap.getConfig() != null) {
      paramBitmap = paramBitmap.getConfig();
    } else {
      paramBitmap = Bitmap.Config.ARGB_8888;
    }
    return paramBitmap;
  }
  
  @VisibleForTesting
  static void initializeMatrixForRotation(int paramInt, Matrix paramMatrix)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 8: 
      paramMatrix.setRotate(-90.0F);
      break;
    case 7: 
      paramMatrix.setRotate(-90.0F);
      paramMatrix.postScale(-1.0F, 1.0F);
      break;
    case 6: 
      paramMatrix.setRotate(90.0F);
      break;
    case 5: 
      paramMatrix.setRotate(90.0F);
      paramMatrix.postScale(-1.0F, 1.0F);
      break;
    case 4: 
      paramMatrix.setRotate(180.0F);
      paramMatrix.postScale(-1.0F, 1.0F);
      break;
    case 3: 
      paramMatrix.setRotate(180.0F);
      break;
    case 2: 
      paramMatrix.setScale(-1.0F, 1.0F);
    }
  }
  
  public static boolean isExifOrientationRequired(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static Bitmap rotateImage(@NonNull Bitmap paramBitmap, int paramInt)
  {
    Object localObject = paramBitmap;
    if (paramInt != 0) {
      try
      {
        localObject = new android/graphics/Matrix;
        ((Matrix)localObject).<init>();
        ((Matrix)localObject).setRotate(paramInt);
        localObject = Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), (Matrix)localObject, true);
      }
      catch (Exception localException)
      {
        localObject = paramBitmap;
        if (Log.isLoggable("TransformationUtils", 6))
        {
          Log.e("TransformationUtils", "Exception when trying to orient image", localException);
          localObject = paramBitmap;
        }
      }
    }
    return (Bitmap)localObject;
  }
  
  public static Bitmap rotateImageExif(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt)
  {
    if (!isExifOrientationRequired(paramInt)) {
      return paramBitmap;
    }
    Matrix localMatrix = new Matrix();
    initializeMatrixForRotation(paramInt, localMatrix);
    RectF localRectF = new RectF(0.0F, 0.0F, paramBitmap.getWidth(), paramBitmap.getHeight());
    localMatrix.mapRect(localRectF);
    paramBitmapPool = paramBitmapPool.get(Math.round(localRectF.width()), Math.round(localRectF.height()), getNonNullConfig(paramBitmap));
    localMatrix.postTranslate(-localRectF.left, -localRectF.top);
    paramBitmapPool.setHasAlpha(paramBitmap.hasAlpha());
    applyMatrix(paramBitmap, paramBitmapPool, localMatrix);
    return paramBitmapPool;
  }
  
  public static Bitmap roundedCorners(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt)
  {
    boolean bool;
    if (paramInt > 0) {
      bool = true;
    } else {
      bool = false;
    }
    Preconditions.checkArgument(bool, "roundingRadius must be greater than 0.");
    Object localObject1 = getAlphaSafeConfig(paramBitmap);
    Bitmap localBitmap1 = getAlphaSafeBitmap(paramBitmapPool, paramBitmap);
    Bitmap localBitmap2 = paramBitmapPool.get(localBitmap1.getWidth(), localBitmap1.getHeight(), (Bitmap.Config)localObject1);
    localBitmap2.setHasAlpha(true);
    Object localObject2 = new BitmapShader(localBitmap1, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
    localObject1 = new Paint();
    ((Paint)localObject1).setAntiAlias(true);
    ((Paint)localObject1).setShader((Shader)localObject2);
    RectF localRectF = new RectF(0.0F, 0.0F, localBitmap2.getWidth(), localBitmap2.getHeight());
    BITMAP_DRAWABLE_LOCK.lock();
    try
    {
      localObject2 = new android/graphics/Canvas;
      ((Canvas)localObject2).<init>(localBitmap2);
      ((Canvas)localObject2).drawColor(0, PorterDuff.Mode.CLEAR);
      float f = paramInt;
      ((Canvas)localObject2).drawRoundRect(localRectF, f, f, (Paint)localObject1);
      clear((Canvas)localObject2);
      BITMAP_DRAWABLE_LOCK.unlock();
      if (!localBitmap1.equals(paramBitmap)) {
        paramBitmapPool.put(localBitmap1);
      }
      return localBitmap2;
    }
    finally
    {
      BITMAP_DRAWABLE_LOCK.unlock();
    }
  }
  
  @Deprecated
  public static Bitmap roundedCorners(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3)
  {
    return roundedCorners(paramBitmapPool, paramBitmap, paramInt3);
  }
  
  public static void setAlpha(Bitmap paramBitmap1, Bitmap paramBitmap2)
  {
    paramBitmap2.setHasAlpha(paramBitmap1.hasAlpha());
  }
  
  private static final class NoLock
    implements Lock
  {
    public void lock() {}
    
    public void lockInterruptibly()
      throws InterruptedException
    {}
    
    @NonNull
    public Condition newCondition()
    {
      throw new UnsupportedOperationException("Should not be called");
    }
    
    public boolean tryLock()
    {
      return true;
    }
    
    public boolean tryLock(long paramLong, @NonNull TimeUnit paramTimeUnit)
      throws InterruptedException
    {
      return true;
    }
    
    public void unlock() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/TransformationUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */