package com.bumptech.glide.load.resource.bitmap;

import android.support.annotation.NonNull;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParser.ImageType;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public final class DefaultImageHeaderParser
  implements ImageHeaderParser
{
  private static final int[] BYTES_PER_FORMAT = { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8 };
  static final int EXIF_MAGIC_NUMBER = 65496;
  static final int EXIF_SEGMENT_TYPE = 225;
  private static final int GIF_HEADER = 4671814;
  private static final int INTEL_TIFF_MAGIC_NUMBER = 18761;
  private static final String JPEG_EXIF_SEGMENT_PREAMBLE = "Exif\000\000";
  static final byte[] JPEG_EXIF_SEGMENT_PREAMBLE_BYTES = "Exif\000\000".getBytes(Charset.forName("UTF-8"));
  private static final int MARKER_EOI = 217;
  private static final int MOTOROLA_TIFF_MAGIC_NUMBER = 19789;
  private static final int ORIENTATION_TAG_TYPE = 274;
  private static final int PNG_HEADER = -1991225785;
  private static final int RIFF_HEADER = 1380533830;
  private static final int SEGMENT_SOS = 218;
  static final int SEGMENT_START_ID = 255;
  private static final String TAG = "DfltImageHeaderParser";
  private static final int VP8_HEADER = 1448097792;
  private static final int VP8_HEADER_MASK = -256;
  private static final int VP8_HEADER_TYPE_EXTENDED = 88;
  private static final int VP8_HEADER_TYPE_LOSSLESS = 76;
  private static final int VP8_HEADER_TYPE_MASK = 255;
  private static final int WEBP_EXTENDED_ALPHA_FLAG = 16;
  private static final int WEBP_HEADER = 1464156752;
  private static final int WEBP_LOSSLESS_ALPHA_FLAG = 8;
  
  private static int calcTagOffset(int paramInt1, int paramInt2)
  {
    return paramInt1 + 2 + paramInt2 * 12;
  }
  
  private int getOrientation(Reader paramReader, ArrayPool paramArrayPool)
    throws IOException
  {
    int i = paramReader.getUInt16();
    if (!handles(i))
    {
      if (Log.isLoggable("DfltImageHeaderParser", 3))
      {
        paramReader = new StringBuilder();
        paramReader.append("Parser doesn't handle magic number: ");
        paramReader.append(i);
        Log.d("DfltImageHeaderParser", paramReader.toString());
      }
      return -1;
    }
    i = moveToExifSegmentAndGetLength(paramReader);
    if (i == -1)
    {
      if (Log.isLoggable("DfltImageHeaderParser", 3)) {
        Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
      }
      return -1;
    }
    byte[] arrayOfByte = (byte[])paramArrayPool.get(i, byte[].class);
    try
    {
      i = parseExifSegment(paramReader, arrayOfByte, i);
      return i;
    }
    finally
    {
      paramArrayPool.put(arrayOfByte);
    }
  }
  
  @NonNull
  private ImageHeaderParser.ImageType getType(Reader paramReader)
    throws IOException
  {
    int i = paramReader.getUInt16();
    if (i == 65496) {
      return ImageHeaderParser.ImageType.JPEG;
    }
    i = i << 16 & 0xFFFF0000 | paramReader.getUInt16() & 0xFFFF;
    if (i == -1991225785)
    {
      paramReader.skip(21L);
      if (paramReader.getByte() >= 3) {
        paramReader = ImageHeaderParser.ImageType.PNG_A;
      } else {
        paramReader = ImageHeaderParser.ImageType.PNG;
      }
      return paramReader;
    }
    if (i >> 8 == 4671814) {
      return ImageHeaderParser.ImageType.GIF;
    }
    if (i != 1380533830) {
      return ImageHeaderParser.ImageType.UNKNOWN;
    }
    paramReader.skip(4L);
    if ((paramReader.getUInt16() << 16 & 0xFFFF0000 | paramReader.getUInt16() & 0xFFFF) != 1464156752) {
      return ImageHeaderParser.ImageType.UNKNOWN;
    }
    i = paramReader.getUInt16() << 16 & 0xFFFF0000 | paramReader.getUInt16() & 0xFFFF;
    if ((i & 0xFF00) != 1448097792) {
      return ImageHeaderParser.ImageType.UNKNOWN;
    }
    i &= 0xFF;
    if (i == 88)
    {
      paramReader.skip(4L);
      if ((paramReader.getByte() & 0x10) != 0) {
        paramReader = ImageHeaderParser.ImageType.WEBP_A;
      } else {
        paramReader = ImageHeaderParser.ImageType.WEBP;
      }
      return paramReader;
    }
    if (i == 76)
    {
      paramReader.skip(4L);
      if ((paramReader.getByte() & 0x8) != 0) {
        paramReader = ImageHeaderParser.ImageType.WEBP_A;
      } else {
        paramReader = ImageHeaderParser.ImageType.WEBP;
      }
      return paramReader;
    }
    return ImageHeaderParser.ImageType.WEBP;
  }
  
  private static boolean handles(int paramInt)
  {
    boolean bool;
    if (((paramInt & 0xFFD8) != 65496) && (paramInt != 19789) && (paramInt != 18761)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean hasJpegExifPreamble(byte[] paramArrayOfByte, int paramInt)
  {
    boolean bool1;
    if ((paramArrayOfByte != null) && (paramInt > JPEG_EXIF_SEGMENT_PREAMBLE_BYTES.length)) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    boolean bool2 = bool1;
    if (bool1) {
      for (paramInt = 0;; paramInt++)
      {
        byte[] arrayOfByte = JPEG_EXIF_SEGMENT_PREAMBLE_BYTES;
        bool2 = bool1;
        if (paramInt >= arrayOfByte.length) {
          break;
        }
        if (paramArrayOfByte[paramInt] != arrayOfByte[paramInt])
        {
          bool2 = false;
          break;
        }
      }
    }
    return bool2;
  }
  
  private int moveToExifSegmentAndGetLength(Reader paramReader)
    throws IOException
  {
    int i;
    int j;
    long l1;
    long l2;
    do
    {
      i = paramReader.getUInt8();
      if (i != 255)
      {
        if (Log.isLoggable("DfltImageHeaderParser", 3))
        {
          paramReader = new StringBuilder();
          paramReader.append("Unknown segmentId=");
          paramReader.append(i);
          Log.d("DfltImageHeaderParser", paramReader.toString());
        }
        return -1;
      }
      i = paramReader.getUInt8();
      if (i == 218) {
        return -1;
      }
      if (i == 217)
      {
        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
          Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
        }
        return -1;
      }
      j = paramReader.getUInt16() - 2;
      if (i == 225) {
        break;
      }
      l1 = j;
      l2 = paramReader.skip(l1);
    } while (l2 == l1);
    if (Log.isLoggable("DfltImageHeaderParser", 3))
    {
      paramReader = new StringBuilder();
      paramReader.append("Unable to skip enough data, type: ");
      paramReader.append(i);
      paramReader.append(", wanted to skip: ");
      paramReader.append(j);
      paramReader.append(", but actually skipped: ");
      paramReader.append(l2);
      Log.d("DfltImageHeaderParser", paramReader.toString());
    }
    return -1;
    return j;
  }
  
  private static int parseExifSegment(RandomAccessReader paramRandomAccessReader)
  {
    int i = paramRandomAccessReader.getInt16(6);
    Object localObject;
    if (i != 18761)
    {
      if (i != 19789)
      {
        if (Log.isLoggable("DfltImageHeaderParser", 3))
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("Unknown endianness = ");
          ((StringBuilder)localObject).append(i);
          Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
        }
        localObject = ByteOrder.BIG_ENDIAN;
      }
      else
      {
        localObject = ByteOrder.BIG_ENDIAN;
      }
    }
    else {
      localObject = ByteOrder.LITTLE_ENDIAN;
    }
    paramRandomAccessReader.order((ByteOrder)localObject);
    int k = paramRandomAccessReader.getInt32(10) + 6;
    int j = paramRandomAccessReader.getInt16(k);
    for (i = 0; i < j; i++)
    {
      int n = calcTagOffset(k, i);
      int m = paramRandomAccessReader.getInt16(n);
      if (m == 274)
      {
        int i2 = paramRandomAccessReader.getInt16(n + 2);
        if ((i2 >= 1) && (i2 <= 12))
        {
          int i1 = paramRandomAccessReader.getInt32(n + 4);
          if (i1 < 0)
          {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
              Log.d("DfltImageHeaderParser", "Negative tiff component count");
            }
          }
          else
          {
            if (Log.isLoggable("DfltImageHeaderParser", 3))
            {
              localObject = new StringBuilder();
              ((StringBuilder)localObject).append("Got tagIndex=");
              ((StringBuilder)localObject).append(i);
              ((StringBuilder)localObject).append(" tagType=");
              ((StringBuilder)localObject).append(m);
              ((StringBuilder)localObject).append(" formatCode=");
              ((StringBuilder)localObject).append(i2);
              ((StringBuilder)localObject).append(" componentCount=");
              ((StringBuilder)localObject).append(i1);
              Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
            }
            i1 += BYTES_PER_FORMAT[i2];
            if (i1 > 4)
            {
              if (Log.isLoggable("DfltImageHeaderParser", 3))
              {
                localObject = new StringBuilder();
                ((StringBuilder)localObject).append("Got byte count > 4, not orientation, continuing, formatCode=");
                ((StringBuilder)localObject).append(i2);
                Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
              }
            }
            else
            {
              n += 8;
              if ((n >= 0) && (n <= paramRandomAccessReader.length()))
              {
                if ((i1 >= 0) && (i1 + n <= paramRandomAccessReader.length())) {
                  return paramRandomAccessReader.getInt16(n);
                }
                if (Log.isLoggable("DfltImageHeaderParser", 3))
                {
                  localObject = new StringBuilder();
                  ((StringBuilder)localObject).append("Illegal number of bytes for TI tag data tagType=");
                  ((StringBuilder)localObject).append(m);
                  Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
                }
              }
              else if (Log.isLoggable("DfltImageHeaderParser", 3))
              {
                localObject = new StringBuilder();
                ((StringBuilder)localObject).append("Illegal tagValueOffset=");
                ((StringBuilder)localObject).append(n);
                ((StringBuilder)localObject).append(" tagType=");
                ((StringBuilder)localObject).append(m);
                Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
              }
            }
          }
        }
        else if (Log.isLoggable("DfltImageHeaderParser", 3))
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("Got invalid format code = ");
          ((StringBuilder)localObject).append(i2);
          Log.d("DfltImageHeaderParser", ((StringBuilder)localObject).toString());
        }
      }
    }
    return -1;
  }
  
  private int parseExifSegment(Reader paramReader, byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    int i = paramReader.read(paramArrayOfByte, paramInt);
    if (i != paramInt)
    {
      if (Log.isLoggable("DfltImageHeaderParser", 3))
      {
        paramReader = new StringBuilder();
        paramReader.append("Unable to read exif segment data, length: ");
        paramReader.append(paramInt);
        paramReader.append(", actually read: ");
        paramReader.append(i);
        Log.d("DfltImageHeaderParser", paramReader.toString());
      }
      return -1;
    }
    if (hasJpegExifPreamble(paramArrayOfByte, paramInt)) {
      return parseExifSegment(new RandomAccessReader(paramArrayOfByte, paramInt));
    }
    if (Log.isLoggable("DfltImageHeaderParser", 3)) {
      Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
    }
    return -1;
  }
  
  public int getOrientation(@NonNull InputStream paramInputStream, @NonNull ArrayPool paramArrayPool)
    throws IOException
  {
    return getOrientation(new StreamReader((InputStream)Preconditions.checkNotNull(paramInputStream)), (ArrayPool)Preconditions.checkNotNull(paramArrayPool));
  }
  
  public int getOrientation(@NonNull ByteBuffer paramByteBuffer, @NonNull ArrayPool paramArrayPool)
    throws IOException
  {
    return getOrientation(new ByteBufferReader((ByteBuffer)Preconditions.checkNotNull(paramByteBuffer)), (ArrayPool)Preconditions.checkNotNull(paramArrayPool));
  }
  
  @NonNull
  public ImageHeaderParser.ImageType getType(@NonNull InputStream paramInputStream)
    throws IOException
  {
    return getType(new StreamReader((InputStream)Preconditions.checkNotNull(paramInputStream)));
  }
  
  @NonNull
  public ImageHeaderParser.ImageType getType(@NonNull ByteBuffer paramByteBuffer)
    throws IOException
  {
    return getType(new ByteBufferReader((ByteBuffer)Preconditions.checkNotNull(paramByteBuffer)));
  }
  
  private static final class ByteBufferReader
    implements DefaultImageHeaderParser.Reader
  {
    private final ByteBuffer byteBuffer;
    
    ByteBufferReader(ByteBuffer paramByteBuffer)
    {
      this.byteBuffer = paramByteBuffer;
      paramByteBuffer.order(ByteOrder.BIG_ENDIAN);
    }
    
    public int getByte()
    {
      if (this.byteBuffer.remaining() < 1) {
        return -1;
      }
      return this.byteBuffer.get();
    }
    
    public int getUInt16()
    {
      return getByte() << 8 & 0xFF00 | getByte() & 0xFF;
    }
    
    public short getUInt8()
    {
      return (short)(getByte() & 0xFF);
    }
    
    public int read(byte[] paramArrayOfByte, int paramInt)
    {
      paramInt = Math.min(paramInt, this.byteBuffer.remaining());
      if (paramInt == 0) {
        return -1;
      }
      this.byteBuffer.get(paramArrayOfByte, 0, paramInt);
      return paramInt;
    }
    
    public long skip(long paramLong)
    {
      int i = (int)Math.min(this.byteBuffer.remaining(), paramLong);
      ByteBuffer localByteBuffer = this.byteBuffer;
      localByteBuffer.position(localByteBuffer.position() + i);
      return i;
    }
  }
  
  private static final class RandomAccessReader
  {
    private final ByteBuffer data;
    
    RandomAccessReader(byte[] paramArrayOfByte, int paramInt)
    {
      this.data = ((ByteBuffer)ByteBuffer.wrap(paramArrayOfByte).order(ByteOrder.BIG_ENDIAN).limit(paramInt));
    }
    
    private boolean isAvailable(int paramInt1, int paramInt2)
    {
      boolean bool;
      if (this.data.remaining() - paramInt1 >= paramInt2) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    short getInt16(int paramInt)
    {
      short s;
      if (isAvailable(paramInt, 2)) {
        s = this.data.getShort(paramInt);
      } else {
        s = -1;
      }
      return s;
    }
    
    int getInt32(int paramInt)
    {
      if (isAvailable(paramInt, 4)) {
        paramInt = this.data.getInt(paramInt);
      } else {
        paramInt = -1;
      }
      return paramInt;
    }
    
    int length()
    {
      return this.data.remaining();
    }
    
    void order(ByteOrder paramByteOrder)
    {
      this.data.order(paramByteOrder);
    }
  }
  
  private static abstract interface Reader
  {
    public abstract int getByte()
      throws IOException;
    
    public abstract int getUInt16()
      throws IOException;
    
    public abstract short getUInt8()
      throws IOException;
    
    public abstract int read(byte[] paramArrayOfByte, int paramInt)
      throws IOException;
    
    public abstract long skip(long paramLong)
      throws IOException;
  }
  
  private static final class StreamReader
    implements DefaultImageHeaderParser.Reader
  {
    private final InputStream is;
    
    StreamReader(InputStream paramInputStream)
    {
      this.is = paramInputStream;
    }
    
    public int getByte()
      throws IOException
    {
      return this.is.read();
    }
    
    public int getUInt16()
      throws IOException
    {
      return this.is.read() << 8 & 0xFF00 | this.is.read() & 0xFF;
    }
    
    public short getUInt8()
      throws IOException
    {
      return (short)(this.is.read() & 0xFF);
    }
    
    public int read(byte[] paramArrayOfByte, int paramInt)
      throws IOException
    {
      int i = paramInt;
      while (i > 0)
      {
        int j = this.is.read(paramArrayOfByte, paramInt - i, i);
        if (j == -1) {
          break;
        }
        i -= j;
      }
      return paramInt - i;
    }
    
    public long skip(long paramLong)
      throws IOException
    {
      if (paramLong < 0L) {
        return 0L;
      }
      long l1 = paramLong;
      while (l1 > 0L)
      {
        long l2 = this.is.skip(l1);
        if (l2 > 0L)
        {
          l1 -= l2;
        }
        else
        {
          if (this.is.read() == -1) {
            break;
          }
          l1 -= 1L;
        }
      }
      return paramLong - l1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/DefaultImageHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */