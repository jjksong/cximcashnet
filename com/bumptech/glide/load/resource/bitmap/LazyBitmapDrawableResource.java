package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;

public final class LazyBitmapDrawableResource
  implements Resource<BitmapDrawable>, Initializable
{
  private final Resource<Bitmap> bitmapResource;
  private final Resources resources;
  
  private LazyBitmapDrawableResource(@NonNull Resources paramResources, @NonNull Resource<Bitmap> paramResource)
  {
    this.resources = ((Resources)Preconditions.checkNotNull(paramResources));
    this.bitmapResource = ((Resource)Preconditions.checkNotNull(paramResource));
  }
  
  @Nullable
  public static Resource<BitmapDrawable> obtain(@NonNull Resources paramResources, @Nullable Resource<Bitmap> paramResource)
  {
    if (paramResource == null) {
      return null;
    }
    return new LazyBitmapDrawableResource(paramResources, paramResource);
  }
  
  @Deprecated
  public static LazyBitmapDrawableResource obtain(Context paramContext, Bitmap paramBitmap)
  {
    return (LazyBitmapDrawableResource)obtain(paramContext.getResources(), BitmapResource.obtain(paramBitmap, Glide.get(paramContext).getBitmapPool()));
  }
  
  @Deprecated
  public static LazyBitmapDrawableResource obtain(Resources paramResources, BitmapPool paramBitmapPool, Bitmap paramBitmap)
  {
    return (LazyBitmapDrawableResource)obtain(paramResources, BitmapResource.obtain(paramBitmap, paramBitmapPool));
  }
  
  @NonNull
  public BitmapDrawable get()
  {
    return new BitmapDrawable(this.resources, (Bitmap)this.bitmapResource.get());
  }
  
  @NonNull
  public Class<BitmapDrawable> getResourceClass()
  {
    return BitmapDrawable.class;
  }
  
  public int getSize()
  {
    return this.bitmapResource.getSize();
  }
  
  public void initialize()
  {
    Resource localResource = this.bitmapResource;
    if ((localResource instanceof Initializable)) {
      ((Initializable)localResource).initialize();
    }
  }
  
  public void recycle()
  {
    this.bitmapResource.recycle();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/LazyBitmapDrawableResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */