package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build.VERSION;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Option.CacheKeyUpdater;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class VideoDecoder<T>
  implements ResourceDecoder<T, Bitmap>
{
  private static final MediaMetadataRetrieverFactory DEFAULT_FACTORY = new MediaMetadataRetrieverFactory();
  public static final long DEFAULT_FRAME = -1L;
  @VisibleForTesting
  static final int DEFAULT_FRAME_OPTION = 2;
  public static final Option<Integer> FRAME_OPTION;
  private static final String TAG = "VideoDecoder";
  public static final Option<Long> TARGET_FRAME = Option.disk("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", Long.valueOf(-1L), new Option.CacheKeyUpdater()
  {
    private final ByteBuffer buffer = ByteBuffer.allocate(8);
    
    public void update(@NonNull byte[] arg1, @NonNull Long paramAnonymousLong, @NonNull MessageDigest paramAnonymousMessageDigest)
    {
      paramAnonymousMessageDigest.update(???);
      synchronized (this.buffer)
      {
        this.buffer.position(0);
        paramAnonymousMessageDigest.update(this.buffer.putLong(paramAnonymousLong.longValue()).array());
        return;
      }
    }
  });
  private final BitmapPool bitmapPool;
  private final MediaMetadataRetrieverFactory factory;
  private final MediaMetadataRetrieverInitializer<T> initializer;
  
  static
  {
    FRAME_OPTION = Option.disk("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", Integer.valueOf(2), new Option.CacheKeyUpdater()
    {
      private final ByteBuffer buffer = ByteBuffer.allocate(4);
      
      public void update(@NonNull byte[] arg1, @NonNull Integer paramAnonymousInteger, @NonNull MessageDigest paramAnonymousMessageDigest)
      {
        if (paramAnonymousInteger == null) {
          return;
        }
        paramAnonymousMessageDigest.update(???);
        synchronized (this.buffer)
        {
          this.buffer.position(0);
          paramAnonymousMessageDigest.update(this.buffer.putInt(paramAnonymousInteger.intValue()).array());
          return;
        }
      }
    });
  }
  
  VideoDecoder(BitmapPool paramBitmapPool, MediaMetadataRetrieverInitializer<T> paramMediaMetadataRetrieverInitializer)
  {
    this(paramBitmapPool, paramMediaMetadataRetrieverInitializer, DEFAULT_FACTORY);
  }
  
  @VisibleForTesting
  VideoDecoder(BitmapPool paramBitmapPool, MediaMetadataRetrieverInitializer<T> paramMediaMetadataRetrieverInitializer, MediaMetadataRetrieverFactory paramMediaMetadataRetrieverFactory)
  {
    this.bitmapPool = paramBitmapPool;
    this.initializer = paramMediaMetadataRetrieverInitializer;
    this.factory = paramMediaMetadataRetrieverFactory;
  }
  
  public static ResourceDecoder<AssetFileDescriptor, Bitmap> asset(BitmapPool paramBitmapPool)
  {
    return new VideoDecoder(paramBitmapPool, new AssetFileDescriptorInitializer(null));
  }
  
  @Nullable
  private static Bitmap decodeFrame(MediaMetadataRetriever paramMediaMetadataRetriever, long paramLong, int paramInt1, int paramInt2, int paramInt3, DownsampleStrategy paramDownsampleStrategy)
  {
    if ((Build.VERSION.SDK_INT >= 27) && (paramInt2 != Integer.MIN_VALUE) && (paramInt3 != Integer.MIN_VALUE) && (paramDownsampleStrategy != DownsampleStrategy.NONE)) {
      paramDownsampleStrategy = decodeScaledFrame(paramMediaMetadataRetriever, paramLong, paramInt1, paramInt2, paramInt3, paramDownsampleStrategy);
    } else {
      paramDownsampleStrategy = null;
    }
    Object localObject = paramDownsampleStrategy;
    if (paramDownsampleStrategy == null) {
      localObject = decodeOriginalFrame(paramMediaMetadataRetriever, paramLong, paramInt1);
    }
    return (Bitmap)localObject;
  }
  
  private static Bitmap decodeOriginalFrame(MediaMetadataRetriever paramMediaMetadataRetriever, long paramLong, int paramInt)
  {
    return paramMediaMetadataRetriever.getFrameAtTime(paramLong, paramInt);
  }
  
  @TargetApi(27)
  private static Bitmap decodeScaledFrame(MediaMetadataRetriever paramMediaMetadataRetriever, long paramLong, int paramInt1, int paramInt2, int paramInt3, DownsampleStrategy paramDownsampleStrategy)
  {
    try
    {
      int m = Integer.parseInt(paramMediaMetadataRetriever.extractMetadata(18));
      int i = Integer.parseInt(paramMediaMetadataRetriever.extractMetadata(19));
      int n = Integer.parseInt(paramMediaMetadataRetriever.extractMetadata(24));
      int k;
      int j;
      if (n != 90)
      {
        k = m;
        j = i;
        if (n != 270) {}
      }
      else
      {
        j = m;
        k = i;
      }
      float f = paramDownsampleStrategy.getScaleFactor(k, j, paramInt2, paramInt3);
      paramMediaMetadataRetriever = paramMediaMetadataRetriever.getScaledFrameAtTime(paramLong, paramInt1, Math.round(k * f), Math.round(f * j));
      return paramMediaMetadataRetriever;
    }
    catch (Throwable paramMediaMetadataRetriever)
    {
      if (Log.isLoggable("VideoDecoder", 3)) {
        Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", paramMediaMetadataRetriever);
      }
    }
    return null;
  }
  
  public static ResourceDecoder<ParcelFileDescriptor, Bitmap> parcel(BitmapPool paramBitmapPool)
  {
    return new VideoDecoder(paramBitmapPool, new ParcelFileDescriptorInitializer());
  }
  
  /* Error */
  public com.bumptech.glide.load.engine.Resource<Bitmap> decode(@NonNull T paramT, int paramInt1, int paramInt2, @NonNull Options paramOptions)
    throws java.io.IOException
  {
    // Byte code:
    //   0: aload 4
    //   2: getstatic 69	com/bumptech/glide/load/resource/bitmap/VideoDecoder:TARGET_FRAME	Lcom/bumptech/glide/load/Option;
    //   5: invokevirtual 189	com/bumptech/glide/load/Options:get	(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
    //   8: checkcast 54	java/lang/Long
    //   11: invokevirtual 193	java/lang/Long:longValue	()J
    //   14: lstore 5
    //   16: lload 5
    //   18: lconst_0
    //   19: lcmp
    //   20: ifge +49 -> 69
    //   23: lload 5
    //   25: ldc2_w 28
    //   28: lcmp
    //   29: ifne +6 -> 35
    //   32: goto +37 -> 69
    //   35: new 195	java/lang/StringBuilder
    //   38: dup
    //   39: invokespecial 196	java/lang/StringBuilder:<init>	()V
    //   42: astore_1
    //   43: aload_1
    //   44: ldc -58
    //   46: invokevirtual 202	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_1
    //   51: lload 5
    //   53: invokevirtual 205	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: new 207	java/lang/IllegalArgumentException
    //   60: dup
    //   61: aload_1
    //   62: invokevirtual 211	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   65: invokespecial 214	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   68: athrow
    //   69: aload 4
    //   71: getstatic 79	com/bumptech/glide/load/resource/bitmap/VideoDecoder:FRAME_OPTION	Lcom/bumptech/glide/load/Option;
    //   74: invokevirtual 189	com/bumptech/glide/load/Options:get	(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
    //   77: checkcast 73	java/lang/Integer
    //   80: astore 8
    //   82: aload 8
    //   84: astore 7
    //   86: aload 8
    //   88: ifnonnull +9 -> 97
    //   91: iconst_2
    //   92: invokestatic 76	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   95: astore 7
    //   97: aload 4
    //   99: getstatic 217	com/bumptech/glide/load/resource/bitmap/DownsampleStrategy:OPTION	Lcom/bumptech/glide/load/Option;
    //   102: invokevirtual 189	com/bumptech/glide/load/Options:get	(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
    //   105: checkcast 117	com/bumptech/glide/load/resource/bitmap/DownsampleStrategy
    //   108: astore 4
    //   110: aload 4
    //   112: ifnonnull +11 -> 123
    //   115: getstatic 220	com/bumptech/glide/load/resource/bitmap/DownsampleStrategy:DEFAULT	Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;
    //   118: astore 4
    //   120: goto +3 -> 123
    //   123: aload_0
    //   124: getfield 96	com/bumptech/glide/load/resource/bitmap/VideoDecoder:factory	Lcom/bumptech/glide/load/resource/bitmap/VideoDecoder$MediaMetadataRetrieverFactory;
    //   127: invokevirtual 224	com/bumptech/glide/load/resource/bitmap/VideoDecoder$MediaMetadataRetrieverFactory:build	()Landroid/media/MediaMetadataRetriever;
    //   130: astore 8
    //   132: aload_0
    //   133: getfield 94	com/bumptech/glide/load/resource/bitmap/VideoDecoder:initializer	Lcom/bumptech/glide/load/resource/bitmap/VideoDecoder$MediaMetadataRetrieverInitializer;
    //   136: aload 8
    //   138: aload_1
    //   139: invokeinterface 228 3 0
    //   144: aload 8
    //   146: lload 5
    //   148: aload 7
    //   150: invokevirtual 232	java/lang/Integer:intValue	()I
    //   153: iload_2
    //   154: iload_3
    //   155: aload 4
    //   157: invokestatic 234	com/bumptech/glide/load/resource/bitmap/VideoDecoder:decodeFrame	(Landroid/media/MediaMetadataRetriever;JIIILcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;)Landroid/graphics/Bitmap;
    //   160: astore_1
    //   161: aload 8
    //   163: invokevirtual 237	android/media/MediaMetadataRetriever:release	()V
    //   166: aload_1
    //   167: aload_0
    //   168: getfield 92	com/bumptech/glide/load/resource/bitmap/VideoDecoder:bitmapPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;
    //   171: invokestatic 243	com/bumptech/glide/load/resource/bitmap/BitmapResource:obtain	(Landroid/graphics/Bitmap;Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;)Lcom/bumptech/glide/load/resource/bitmap/BitmapResource;
    //   174: areturn
    //   175: astore_1
    //   176: goto +17 -> 193
    //   179: astore 4
    //   181: new 180	java/io/IOException
    //   184: astore_1
    //   185: aload_1
    //   186: aload 4
    //   188: invokespecial 246	java/io/IOException:<init>	(Ljava/lang/Throwable;)V
    //   191: aload_1
    //   192: athrow
    //   193: aload 8
    //   195: invokevirtual 237	android/media/MediaMetadataRetriever:release	()V
    //   198: aload_1
    //   199: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	200	0	this	VideoDecoder
    //   0	200	1	paramT	T
    //   0	200	2	paramInt1	int
    //   0	200	3	paramInt2	int
    //   0	200	4	paramOptions	Options
    //   14	133	5	l	long
    //   84	65	7	localObject1	Object
    //   80	114	8	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   132	161	175	finally
    //   181	193	175	finally
    //   132	161	179	java/lang/RuntimeException
  }
  
  public boolean handles(@NonNull T paramT, @NonNull Options paramOptions)
  {
    return true;
  }
  
  private static final class AssetFileDescriptorInitializer
    implements VideoDecoder.MediaMetadataRetrieverInitializer<AssetFileDescriptor>
  {
    public void initialize(MediaMetadataRetriever paramMediaMetadataRetriever, AssetFileDescriptor paramAssetFileDescriptor)
    {
      paramMediaMetadataRetriever.setDataSource(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), paramAssetFileDescriptor.getLength());
    }
  }
  
  @VisibleForTesting
  static class MediaMetadataRetrieverFactory
  {
    public MediaMetadataRetriever build()
    {
      return new MediaMetadataRetriever();
    }
  }
  
  @VisibleForTesting
  static abstract interface MediaMetadataRetrieverInitializer<T>
  {
    public abstract void initialize(MediaMetadataRetriever paramMediaMetadataRetriever, T paramT);
  }
  
  static final class ParcelFileDescriptorInitializer
    implements VideoDecoder.MediaMetadataRetrieverInitializer<ParcelFileDescriptor>
  {
    public void initialize(MediaMetadataRetriever paramMediaMetadataRetriever, ParcelFileDescriptor paramParcelFileDescriptor)
    {
      paramMediaMetadataRetriever.setDataSource(paramParcelFileDescriptor.getFileDescriptor());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/VideoDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */