package com.bumptech.glide.load.resource.bitmap;

import android.media.ExifInterface;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParser.ImageType;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

@RequiresApi(27)
public final class ExifInterfaceImageHeaderParser
  implements ImageHeaderParser
{
  public int getOrientation(@NonNull InputStream paramInputStream, @NonNull ArrayPool paramArrayPool)
    throws IOException
  {
    int i = new ExifInterface(paramInputStream).getAttributeInt("Orientation", 1);
    if (i == 0) {
      return -1;
    }
    return i;
  }
  
  public int getOrientation(@NonNull ByteBuffer paramByteBuffer, @NonNull ArrayPool paramArrayPool)
    throws IOException
  {
    return getOrientation(ByteBufferUtil.toStream(paramByteBuffer), paramArrayPool);
  }
  
  @NonNull
  public ImageHeaderParser.ImageType getType(@NonNull InputStream paramInputStream)
    throws IOException
  {
    return ImageHeaderParser.ImageType.UNKNOWN;
  }
  
  @NonNull
  public ImageHeaderParser.ImageType getType(@NonNull ByteBuffer paramByteBuffer)
    throws IOException
  {
    return ImageHeaderParser.ImageType.UNKNOWN;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/ExifInterfaceImageHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */