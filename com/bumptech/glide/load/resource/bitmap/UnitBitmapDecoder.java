package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Util;

public final class UnitBitmapDecoder
  implements ResourceDecoder<Bitmap, Bitmap>
{
  public Resource<Bitmap> decode(@NonNull Bitmap paramBitmap, int paramInt1, int paramInt2, @NonNull Options paramOptions)
  {
    return new NonOwnedBitmapResource(paramBitmap);
  }
  
  public boolean handles(@NonNull Bitmap paramBitmap, @NonNull Options paramOptions)
  {
    return true;
  }
  
  private static final class NonOwnedBitmapResource
    implements Resource<Bitmap>
  {
    private final Bitmap bitmap;
    
    NonOwnedBitmapResource(@NonNull Bitmap paramBitmap)
    {
      this.bitmap = paramBitmap;
    }
    
    @NonNull
    public Bitmap get()
    {
      return this.bitmap;
    }
    
    @NonNull
    public Class<Bitmap> getResourceClass()
    {
      return Bitmap.class;
    }
    
    public int getSize()
    {
      return Util.getBitmapByteSize(this.bitmap);
    }
    
    public void recycle() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/UnitBitmapDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */