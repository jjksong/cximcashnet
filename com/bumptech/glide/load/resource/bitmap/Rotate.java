package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class Rotate
  extends BitmapTransformation
{
  private static final String ID = "com.bumptech.glide.load.resource.bitmap.Rotate";
  private static final byte[] ID_BYTES = "com.bumptech.glide.load.resource.bitmap.Rotate".getBytes(CHARSET);
  private final int degreesToRotate;
  
  public Rotate(int paramInt)
  {
    this.degreesToRotate = paramInt;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = paramObject instanceof Rotate;
    boolean bool1 = false;
    if (bool2)
    {
      paramObject = (Rotate)paramObject;
      if (this.degreesToRotate == ((Rotate)paramObject).degreesToRotate) {
        bool1 = true;
      }
      return bool1;
    }
    return false;
  }
  
  public int hashCode()
  {
    return Util.hashCode("com.bumptech.glide.load.resource.bitmap.Rotate".hashCode(), Util.hashCode(this.degreesToRotate));
  }
  
  protected Bitmap transform(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    return TransformationUtils.rotateImage(paramBitmap, this.degreesToRotate);
  }
  
  public void updateDiskCacheKey(@NonNull MessageDigest paramMessageDigest)
  {
    paramMessageDigest.update(ID_BYTES);
    paramMessageDigest.update(ByteBuffer.allocate(4).putInt(this.degreesToRotate).array());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/Rotate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */