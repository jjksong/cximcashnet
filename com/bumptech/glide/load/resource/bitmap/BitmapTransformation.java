package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;

public abstract class BitmapTransformation
  implements Transformation<Bitmap>
{
  protected abstract Bitmap transform(@NonNull BitmapPool paramBitmapPool, @NonNull Bitmap paramBitmap, int paramInt1, int paramInt2);
  
  @NonNull
  public final Resource<Bitmap> transform(@NonNull Context paramContext, @NonNull Resource<Bitmap> paramResource, int paramInt1, int paramInt2)
  {
    if (Util.isValidDimensions(paramInt1, paramInt2))
    {
      BitmapPool localBitmapPool = Glide.get(paramContext).getBitmapPool();
      paramContext = (Bitmap)paramResource.get();
      int i = paramInt1;
      if (paramInt1 == Integer.MIN_VALUE) {
        i = paramContext.getWidth();
      }
      paramInt1 = paramInt2;
      if (paramInt2 == Integer.MIN_VALUE) {
        paramInt1 = paramContext.getHeight();
      }
      Bitmap localBitmap = transform(localBitmapPool, paramContext, i, paramInt1);
      if (!paramContext.equals(localBitmap)) {
        paramResource = BitmapResource.obtain(localBitmap, localBitmapPool);
      }
      return paramResource;
    }
    paramContext = new StringBuilder();
    paramContext.append("Cannot apply transformation on width: ");
    paramContext.append(paramInt1);
    paramContext.append(" or height: ");
    paramContext.append(paramInt2);
    paramContext.append(" less than or equal to zero and not Target.SIZE_ORIGINAL");
    throw new IllegalArgumentException(paramContext.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/resource/bitmap/BitmapTransformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */