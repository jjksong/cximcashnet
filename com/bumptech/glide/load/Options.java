package com.bumptech.glide.load;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import java.security.MessageDigest;

public final class Options
  implements Key
{
  private final ArrayMap<Option<?>, Object> values = new CachedHashCodeArrayMap();
  
  private static <T> void updateDiskCacheKey(@NonNull Option<T> paramOption, @NonNull Object paramObject, @NonNull MessageDigest paramMessageDigest)
  {
    paramOption.update(paramObject, paramMessageDigest);
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof Options))
    {
      paramObject = (Options)paramObject;
      return this.values.equals(((Options)paramObject).values);
    }
    return false;
  }
  
  @Nullable
  public <T> T get(@NonNull Option<T> paramOption)
  {
    if (this.values.containsKey(paramOption)) {
      paramOption = this.values.get(paramOption);
    } else {
      paramOption = paramOption.getDefaultValue();
    }
    return paramOption;
  }
  
  public int hashCode()
  {
    return this.values.hashCode();
  }
  
  public void putAll(@NonNull Options paramOptions)
  {
    this.values.putAll(paramOptions.values);
  }
  
  @NonNull
  public <T> Options set(@NonNull Option<T> paramOption, @NonNull T paramT)
  {
    this.values.put(paramOption, paramT);
    return this;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Options{values=");
    localStringBuilder.append(this.values);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void updateDiskCacheKey(@NonNull MessageDigest paramMessageDigest)
  {
    for (int i = 0; i < this.values.size(); i++) {
      updateDiskCacheKey((Option)this.values.keyAt(i), this.values.valueAt(i), paramMessageDigest);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/Options.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */