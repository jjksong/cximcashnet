package com.bumptech.glide.load;

import android.support.annotation.NonNull;
import java.io.File;

public abstract interface Encoder<T>
{
  public abstract boolean encode(@NonNull T paramT, @NonNull File paramFile, @NonNull Options paramOptions);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/Encoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */