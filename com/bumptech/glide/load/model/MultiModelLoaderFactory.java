package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.util.Pools.Pool;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MultiModelLoaderFactory
{
  private static final Factory DEFAULT_FACTORY = new Factory();
  private static final ModelLoader<Object, Object> EMPTY_MODEL_LOADER = new EmptyModelLoader();
  private final Set<Entry<?, ?>> alreadyUsedEntries = new HashSet();
  private final List<Entry<?, ?>> entries = new ArrayList();
  private final Factory factory;
  private final Pools.Pool<List<Throwable>> throwableListPool;
  
  public MultiModelLoaderFactory(@NonNull Pools.Pool<List<Throwable>> paramPool)
  {
    this(paramPool, DEFAULT_FACTORY);
  }
  
  @VisibleForTesting
  MultiModelLoaderFactory(@NonNull Pools.Pool<List<Throwable>> paramPool, @NonNull Factory paramFactory)
  {
    this.throwableListPool = paramPool;
    this.factory = paramFactory;
  }
  
  private <Model, Data> void add(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> paramModelLoaderFactory, boolean paramBoolean)
  {
    paramClass1 = new Entry(paramClass, paramClass1, paramModelLoaderFactory);
    paramClass = this.entries;
    int i;
    if (paramBoolean) {
      i = paramClass.size();
    } else {
      i = 0;
    }
    paramClass.add(i, paramClass1);
  }
  
  @NonNull
  private <Model, Data> ModelLoader<Model, Data> build(@NonNull Entry<?, ?> paramEntry)
  {
    return (ModelLoader)Preconditions.checkNotNull(paramEntry.factory.build(this));
  }
  
  @NonNull
  private static <Model, Data> ModelLoader<Model, Data> emptyModelLoader()
  {
    return EMPTY_MODEL_LOADER;
  }
  
  @NonNull
  private <Model, Data> ModelLoaderFactory<Model, Data> getFactory(@NonNull Entry<?, ?> paramEntry)
  {
    return paramEntry.factory;
  }
  
  <Model, Data> void append(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> paramModelLoaderFactory)
  {
    try
    {
      add(paramClass, paramClass1, paramModelLoaderFactory, true);
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  /* Error */
  @NonNull
  public <Model, Data> ModelLoader<Model, Data> build(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 51	java/util/ArrayList
    //   5: astore 4
    //   7: aload 4
    //   9: invokespecial 52	java/util/ArrayList:<init>	()V
    //   12: aload_0
    //   13: getfield 54	com/bumptech/glide/load/model/MultiModelLoaderFactory:entries	Ljava/util/List;
    //   16: invokeinterface 116 1 0
    //   21: astore 6
    //   23: iconst_0
    //   24: istore_3
    //   25: aload 6
    //   27: invokeinterface 122 1 0
    //   32: ifeq +85 -> 117
    //   35: aload 6
    //   37: invokeinterface 126 1 0
    //   42: checkcast 9	com/bumptech/glide/load/model/MultiModelLoaderFactory$Entry
    //   45: astore 5
    //   47: aload_0
    //   48: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   51: aload 5
    //   53: invokeinterface 132 2 0
    //   58: ifeq +8 -> 66
    //   61: iconst_1
    //   62: istore_3
    //   63: goto -38 -> 25
    //   66: aload 5
    //   68: aload_1
    //   69: aload_2
    //   70: invokevirtual 136	com/bumptech/glide/load/model/MultiModelLoaderFactory$Entry:handles	(Ljava/lang/Class;Ljava/lang/Class;)Z
    //   73: ifeq -48 -> 25
    //   76: aload_0
    //   77: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   80: aload 5
    //   82: invokeinterface 138 2 0
    //   87: pop
    //   88: aload 4
    //   90: aload_0
    //   91: aload 5
    //   93: invokespecial 140	com/bumptech/glide/load/model/MultiModelLoaderFactory:build	(Lcom/bumptech/glide/load/model/MultiModelLoaderFactory$Entry;)Lcom/bumptech/glide/load/model/ModelLoader;
    //   96: invokeinterface 141 2 0
    //   101: pop
    //   102: aload_0
    //   103: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   106: aload 5
    //   108: invokeinterface 144 2 0
    //   113: pop
    //   114: goto -89 -> 25
    //   117: aload 4
    //   119: invokeinterface 76 1 0
    //   124: iconst_1
    //   125: if_icmple +21 -> 146
    //   128: aload_0
    //   129: getfield 63	com/bumptech/glide/load/model/MultiModelLoaderFactory:factory	Lcom/bumptech/glide/load/model/MultiModelLoaderFactory$Factory;
    //   132: aload 4
    //   134: aload_0
    //   135: getfield 61	com/bumptech/glide/load/model/MultiModelLoaderFactory:throwableListPool	Landroid/support/v4/util/Pools$Pool;
    //   138: invokevirtual 147	com/bumptech/glide/load/model/MultiModelLoaderFactory$Factory:build	(Ljava/util/List;Landroid/support/v4/util/Pools$Pool;)Lcom/bumptech/glide/load/model/MultiModelLoader;
    //   141: astore_1
    //   142: aload_0
    //   143: monitorexit
    //   144: aload_1
    //   145: areturn
    //   146: aload 4
    //   148: invokeinterface 76 1 0
    //   153: iconst_1
    //   154: if_icmpne +19 -> 173
    //   157: aload 4
    //   159: iconst_0
    //   160: invokeinterface 151 2 0
    //   165: checkcast 98	com/bumptech/glide/load/model/ModelLoader
    //   168: astore_1
    //   169: aload_0
    //   170: monitorexit
    //   171: aload_1
    //   172: areturn
    //   173: iload_3
    //   174: ifeq +11 -> 185
    //   177: invokestatic 153	com/bumptech/glide/load/model/MultiModelLoaderFactory:emptyModelLoader	()Lcom/bumptech/glide/load/model/ModelLoader;
    //   180: astore_1
    //   181: aload_0
    //   182: monitorexit
    //   183: aload_1
    //   184: areturn
    //   185: new 155	com/bumptech/glide/Registry$NoModelLoaderAvailableException
    //   188: astore 4
    //   190: aload 4
    //   192: aload_1
    //   193: aload_2
    //   194: invokespecial 158	com/bumptech/glide/Registry$NoModelLoaderAvailableException:<init>	(Ljava/lang/Class;Ljava/lang/Class;)V
    //   197: aload 4
    //   199: athrow
    //   200: astore_1
    //   201: goto +15 -> 216
    //   204: astore_1
    //   205: aload_0
    //   206: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   209: invokeinterface 161 1 0
    //   214: aload_1
    //   215: athrow
    //   216: aload_0
    //   217: monitorexit
    //   218: aload_1
    //   219: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	220	0	this	MultiModelLoaderFactory
    //   0	220	1	paramClass	Class<Model>
    //   0	220	2	paramClass1	Class<Data>
    //   24	150	3	i	int
    //   5	193	4	localObject	Object
    //   45	62	5	localEntry	Entry
    //   21	15	6	localIterator	Iterator
    // Exception table:
    //   from	to	target	type
    //   2	23	200	finally
    //   25	61	200	finally
    //   66	114	200	finally
    //   117	142	200	finally
    //   146	169	200	finally
    //   177	181	200	finally
    //   185	200	200	finally
    //   205	216	200	finally
    //   2	23	204	java/lang/Throwable
    //   25	61	204	java/lang/Throwable
    //   66	114	204	java/lang/Throwable
    //   117	142	204	java/lang/Throwable
    //   146	169	204	java/lang/Throwable
    //   177	181	204	java/lang/Throwable
    //   185	200	204	java/lang/Throwable
  }
  
  /* Error */
  @NonNull
  <Model> List<ModelLoader<Model, ?>> build(@NonNull Class<Model> paramClass)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 51	java/util/ArrayList
    //   5: astore_2
    //   6: aload_2
    //   7: invokespecial 52	java/util/ArrayList:<init>	()V
    //   10: aload_0
    //   11: getfield 54	com/bumptech/glide/load/model/MultiModelLoaderFactory:entries	Ljava/util/List;
    //   14: invokeinterface 116 1 0
    //   19: astore 4
    //   21: aload 4
    //   23: invokeinterface 122 1 0
    //   28: ifeq +75 -> 103
    //   31: aload 4
    //   33: invokeinterface 126 1 0
    //   38: checkcast 9	com/bumptech/glide/load/model/MultiModelLoaderFactory$Entry
    //   41: astore_3
    //   42: aload_0
    //   43: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   46: aload_3
    //   47: invokeinterface 132 2 0
    //   52: ifeq +6 -> 58
    //   55: goto -34 -> 21
    //   58: aload_3
    //   59: aload_1
    //   60: invokevirtual 166	com/bumptech/glide/load/model/MultiModelLoaderFactory$Entry:handles	(Ljava/lang/Class;)Z
    //   63: ifeq -42 -> 21
    //   66: aload_0
    //   67: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   70: aload_3
    //   71: invokeinterface 138 2 0
    //   76: pop
    //   77: aload_2
    //   78: aload_0
    //   79: aload_3
    //   80: invokespecial 140	com/bumptech/glide/load/model/MultiModelLoaderFactory:build	(Lcom/bumptech/glide/load/model/MultiModelLoaderFactory$Entry;)Lcom/bumptech/glide/load/model/ModelLoader;
    //   83: invokeinterface 141 2 0
    //   88: pop
    //   89: aload_0
    //   90: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   93: aload_3
    //   94: invokeinterface 144 2 0
    //   99: pop
    //   100: goto -79 -> 21
    //   103: aload_0
    //   104: monitorexit
    //   105: aload_2
    //   106: areturn
    //   107: astore_1
    //   108: goto +15 -> 123
    //   111: astore_1
    //   112: aload_0
    //   113: getfield 59	com/bumptech/glide/load/model/MultiModelLoaderFactory:alreadyUsedEntries	Ljava/util/Set;
    //   116: invokeinterface 161 1 0
    //   121: aload_1
    //   122: athrow
    //   123: aload_0
    //   124: monitorexit
    //   125: aload_1
    //   126: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	this	MultiModelLoaderFactory
    //   0	127	1	paramClass	Class<Model>
    //   5	101	2	localArrayList	ArrayList
    //   41	53	3	localEntry	Entry
    //   19	13	4	localIterator	Iterator
    // Exception table:
    //   from	to	target	type
    //   2	21	107	finally
    //   21	55	107	finally
    //   58	100	107	finally
    //   112	123	107	finally
    //   2	21	111	java/lang/Throwable
    //   21	55	111	java/lang/Throwable
    //   58	100	111	java/lang/Throwable
  }
  
  @NonNull
  List<Class<?>> getDataClasses(@NonNull Class<?> paramClass)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Iterator localIterator = this.entries.iterator();
      while (localIterator.hasNext())
      {
        Entry localEntry = (Entry)localIterator.next();
        if ((!localArrayList.contains(localEntry.dataClass)) && (localEntry.handles(paramClass))) {
          localArrayList.add(localEntry.dataClass);
        }
      }
      return localArrayList;
    }
    finally {}
  }
  
  <Model, Data> void prepend(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> paramModelLoaderFactory)
  {
    try
    {
      add(paramClass, paramClass1, paramModelLoaderFactory, false);
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  @NonNull
  <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> remove(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Iterator localIterator = this.entries.iterator();
      while (localIterator.hasNext())
      {
        Entry localEntry = (Entry)localIterator.next();
        if (localEntry.handles(paramClass, paramClass1))
        {
          localIterator.remove();
          localArrayList.add(getFactory(localEntry));
        }
      }
      return localArrayList;
    }
    finally {}
  }
  
  @NonNull
  <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> replace(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> paramModelLoaderFactory)
  {
    try
    {
      List localList = remove(paramClass, paramClass1);
      append(paramClass, paramClass1, paramModelLoaderFactory);
      return localList;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  private static class EmptyModelLoader
    implements ModelLoader<Object, Object>
  {
    @Nullable
    public ModelLoader.LoadData<Object> buildLoadData(@NonNull Object paramObject, int paramInt1, int paramInt2, @NonNull Options paramOptions)
    {
      return null;
    }
    
    public boolean handles(@NonNull Object paramObject)
    {
      return false;
    }
  }
  
  private static class Entry<Model, Data>
  {
    final Class<Data> dataClass;
    final ModelLoaderFactory<? extends Model, ? extends Data> factory;
    private final Class<Model> modelClass;
    
    public Entry(@NonNull Class<Model> paramClass, @NonNull Class<Data> paramClass1, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> paramModelLoaderFactory)
    {
      this.modelClass = paramClass;
      this.dataClass = paramClass1;
      this.factory = paramModelLoaderFactory;
    }
    
    public boolean handles(@NonNull Class<?> paramClass)
    {
      return this.modelClass.isAssignableFrom(paramClass);
    }
    
    public boolean handles(@NonNull Class<?> paramClass1, @NonNull Class<?> paramClass2)
    {
      boolean bool;
      if ((handles(paramClass1)) && (this.dataClass.isAssignableFrom(paramClass2))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
  
  static class Factory
  {
    @NonNull
    public <Model, Data> MultiModelLoader<Model, Data> build(@NonNull List<ModelLoader<Model, Data>> paramList, @NonNull Pools.Pool<List<Throwable>> paramPool)
    {
      return new MultiModelLoader(paramList, paramPool);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/MultiModelLoaderFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */