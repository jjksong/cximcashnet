package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;

public abstract interface ModelLoaderFactory<T, Y>
{
  @NonNull
  public abstract ModelLoader<T, Y> build(@NonNull MultiModelLoaderFactory paramMultiModelLoaderFactory);
  
  public abstract void teardown();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/ModelLoaderFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */