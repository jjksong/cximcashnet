package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class LazyHeaders
  implements Headers
{
  private volatile Map<String, String> combinedHeaders;
  private final Map<String, List<LazyHeaderFactory>> headers;
  
  LazyHeaders(Map<String, List<LazyHeaderFactory>> paramMap)
  {
    this.headers = Collections.unmodifiableMap(paramMap);
  }
  
  @NonNull
  private String buildHeaderValue(@NonNull List<LazyHeaderFactory> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = paramList.size();
    for (int i = 0; i < j; i++)
    {
      String str = ((LazyHeaderFactory)paramList.get(i)).buildHeader();
      if (!TextUtils.isEmpty(str))
      {
        localStringBuilder.append(str);
        if (i != paramList.size() - 1) {
          localStringBuilder.append(',');
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  private Map<String, String> generateHeaders()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.headers.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str = buildHeaderValue((List)localEntry.getValue());
      if (!TextUtils.isEmpty(str)) {
        localHashMap.put(localEntry.getKey(), str);
      }
    }
    return localHashMap;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof LazyHeaders))
    {
      paramObject = (LazyHeaders)paramObject;
      return this.headers.equals(((LazyHeaders)paramObject).headers);
    }
    return false;
  }
  
  public Map<String, String> getHeaders()
  {
    if (this.combinedHeaders == null) {
      try
      {
        if (this.combinedHeaders == null) {
          this.combinedHeaders = Collections.unmodifiableMap(generateHeaders());
        }
      }
      finally {}
    }
    return this.combinedHeaders;
  }
  
  public int hashCode()
  {
    return this.headers.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("LazyHeaders{headers=");
    localStringBuilder.append(this.headers);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public static final class Builder
  {
    private static final Map<String, List<LazyHeaderFactory>> DEFAULT_HEADERS;
    private static final String DEFAULT_USER_AGENT = ;
    private static final String USER_AGENT_HEADER = "User-Agent";
    private boolean copyOnModify = true;
    private Map<String, List<LazyHeaderFactory>> headers = DEFAULT_HEADERS;
    private boolean isUserAgentDefault = true;
    
    static
    {
      HashMap localHashMap = new HashMap(2);
      if (!TextUtils.isEmpty(DEFAULT_USER_AGENT)) {
        localHashMap.put("User-Agent", Collections.singletonList(new LazyHeaders.StringHeaderFactory(DEFAULT_USER_AGENT)));
      }
      DEFAULT_HEADERS = Collections.unmodifiableMap(localHashMap);
    }
    
    private Map<String, List<LazyHeaderFactory>> copyHeaders()
    {
      HashMap localHashMap = new HashMap(this.headers.size());
      Iterator localIterator = this.headers.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        ArrayList localArrayList = new ArrayList((Collection)localEntry.getValue());
        localHashMap.put(localEntry.getKey(), localArrayList);
      }
      return localHashMap;
    }
    
    private void copyIfNecessary()
    {
      if (this.copyOnModify)
      {
        this.copyOnModify = false;
        this.headers = copyHeaders();
      }
    }
    
    private List<LazyHeaderFactory> getFactories(String paramString)
    {
      List localList = (List)this.headers.get(paramString);
      Object localObject = localList;
      if (localList == null)
      {
        localObject = new ArrayList();
        this.headers.put(paramString, localObject);
      }
      return (List<LazyHeaderFactory>)localObject;
    }
    
    @VisibleForTesting
    static String getSanitizedUserAgent()
    {
      String str = System.getProperty("http.agent");
      if (TextUtils.isEmpty(str)) {
        return str;
      }
      int j = str.length();
      StringBuilder localStringBuilder = new StringBuilder(str.length());
      for (int i = 0; i < j; i++)
      {
        char c = str.charAt(i);
        if (((c > '\037') || (c == '\t')) && (c < '')) {
          localStringBuilder.append(c);
        } else {
          localStringBuilder.append('?');
        }
      }
      return localStringBuilder.toString();
    }
    
    public Builder addHeader(@NonNull String paramString, @NonNull LazyHeaderFactory paramLazyHeaderFactory)
    {
      if ((this.isUserAgentDefault) && ("User-Agent".equalsIgnoreCase(paramString))) {
        return setHeader(paramString, paramLazyHeaderFactory);
      }
      copyIfNecessary();
      getFactories(paramString).add(paramLazyHeaderFactory);
      return this;
    }
    
    public Builder addHeader(@NonNull String paramString1, @NonNull String paramString2)
    {
      return addHeader(paramString1, new LazyHeaders.StringHeaderFactory(paramString2));
    }
    
    public LazyHeaders build()
    {
      this.copyOnModify = true;
      return new LazyHeaders(this.headers);
    }
    
    public Builder setHeader(@NonNull String paramString, @Nullable LazyHeaderFactory paramLazyHeaderFactory)
    {
      copyIfNecessary();
      if (paramLazyHeaderFactory == null)
      {
        this.headers.remove(paramString);
      }
      else
      {
        List localList = getFactories(paramString);
        localList.clear();
        localList.add(paramLazyHeaderFactory);
      }
      if ((this.isUserAgentDefault) && ("User-Agent".equalsIgnoreCase(paramString))) {
        this.isUserAgentDefault = false;
      }
      return this;
    }
    
    public Builder setHeader(@NonNull String paramString1, @Nullable String paramString2)
    {
      if (paramString2 == null) {
        paramString2 = null;
      } else {
        paramString2 = new LazyHeaders.StringHeaderFactory(paramString2);
      }
      return setHeader(paramString1, paramString2);
    }
  }
  
  static final class StringHeaderFactory
    implements LazyHeaderFactory
  {
    @NonNull
    private final String value;
    
    StringHeaderFactory(@NonNull String paramString)
    {
      this.value = paramString;
    }
    
    public String buildHeader()
    {
      return this.value;
    }
    
    public boolean equals(Object paramObject)
    {
      if ((paramObject instanceof StringHeaderFactory))
      {
        paramObject = (StringHeaderFactory)paramObject;
        return this.value.equals(((StringHeaderFactory)paramObject).value);
      }
      return false;
    }
    
    public int hashCode()
    {
      return this.value.hashCode();
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("StringHeaderFactory{value='");
      localStringBuilder.append(this.value);
      localStringBuilder.append('\'');
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/LazyHeaders.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */