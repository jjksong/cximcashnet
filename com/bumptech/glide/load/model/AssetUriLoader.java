package com.bumptech.glide.load.model;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.FileDescriptorAssetPathFetcher;
import com.bumptech.glide.load.data.StreamAssetPathFetcher;
import com.bumptech.glide.signature.ObjectKey;
import java.io.InputStream;
import java.util.List;

public class AssetUriLoader<Data>
  implements ModelLoader<Uri, Data>
{
  private static final String ASSET_PATH_SEGMENT = "android_asset";
  private static final String ASSET_PREFIX = "file:///android_asset/";
  private static final int ASSET_PREFIX_LENGTH = 22;
  private final AssetManager assetManager;
  private final AssetFetcherFactory<Data> factory;
  
  public AssetUriLoader(AssetManager paramAssetManager, AssetFetcherFactory<Data> paramAssetFetcherFactory)
  {
    this.assetManager = paramAssetManager;
    this.factory = paramAssetFetcherFactory;
  }
  
  public ModelLoader.LoadData<Data> buildLoadData(@NonNull Uri paramUri, int paramInt1, int paramInt2, @NonNull Options paramOptions)
  {
    paramOptions = paramUri.toString().substring(ASSET_PREFIX_LENGTH);
    return new ModelLoader.LoadData(new ObjectKey(paramUri), this.factory.buildFetcher(this.assetManager, paramOptions));
  }
  
  public boolean handles(@NonNull Uri paramUri)
  {
    boolean bool3 = "file".equals(paramUri.getScheme());
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (bool3)
    {
      bool1 = bool2;
      if (!paramUri.getPathSegments().isEmpty())
      {
        bool1 = bool2;
        if ("android_asset".equals(paramUri.getPathSegments().get(0))) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public static abstract interface AssetFetcherFactory<Data>
  {
    public abstract DataFetcher<Data> buildFetcher(AssetManager paramAssetManager, String paramString);
  }
  
  public static class FileDescriptorFactory
    implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, AssetUriLoader.AssetFetcherFactory<ParcelFileDescriptor>
  {
    private final AssetManager assetManager;
    
    public FileDescriptorFactory(AssetManager paramAssetManager)
    {
      this.assetManager = paramAssetManager;
    }
    
    @NonNull
    public ModelLoader<Uri, ParcelFileDescriptor> build(MultiModelLoaderFactory paramMultiModelLoaderFactory)
    {
      return new AssetUriLoader(this.assetManager, this);
    }
    
    public DataFetcher<ParcelFileDescriptor> buildFetcher(AssetManager paramAssetManager, String paramString)
    {
      return new FileDescriptorAssetPathFetcher(paramAssetManager, paramString);
    }
    
    public void teardown() {}
  }
  
  public static class StreamFactory
    implements ModelLoaderFactory<Uri, InputStream>, AssetUriLoader.AssetFetcherFactory<InputStream>
  {
    private final AssetManager assetManager;
    
    public StreamFactory(AssetManager paramAssetManager)
    {
      this.assetManager = paramAssetManager;
    }
    
    @NonNull
    public ModelLoader<Uri, InputStream> build(MultiModelLoaderFactory paramMultiModelLoaderFactory)
    {
      return new AssetUriLoader(this.assetManager, this);
    }
    
    public DataFetcher<InputStream> buildFetcher(AssetManager paramAssetManager, String paramString)
    {
      return new StreamAssetPathFetcher(paramAssetManager, paramString);
    }
    
    public void teardown() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/AssetUriLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */