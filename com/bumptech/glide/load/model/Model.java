package com.bumptech.glide.load.model;

import android.support.annotation.Nullable;

public abstract interface Model
{
  public abstract boolean isEquivalentTo(@Nullable Object paramObject);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/Model.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */