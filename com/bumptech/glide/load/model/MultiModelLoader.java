package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pools.Pool;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.DataFetcher.DataCallback;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MultiModelLoader<Model, Data>
  implements ModelLoader<Model, Data>
{
  private final Pools.Pool<List<Throwable>> exceptionListPool;
  private final List<ModelLoader<Model, Data>> modelLoaders;
  
  MultiModelLoader(@NonNull List<ModelLoader<Model, Data>> paramList, @NonNull Pools.Pool<List<Throwable>> paramPool)
  {
    this.modelLoaders = paramList;
    this.exceptionListPool = paramPool;
  }
  
  public ModelLoader.LoadData<Data> buildLoadData(@NonNull Model paramModel, int paramInt1, int paramInt2, @NonNull Options paramOptions)
  {
    int j = this.modelLoaders.size();
    ArrayList localArrayList = new ArrayList(j);
    Object localObject3 = null;
    int i = 0;
    Object localObject2;
    for (Object localObject1 = null; i < j; localObject1 = localObject2)
    {
      Object localObject4 = (ModelLoader)this.modelLoaders.get(i);
      localObject2 = localObject1;
      if (((ModelLoader)localObject4).handles(paramModel))
      {
        localObject4 = ((ModelLoader)localObject4).buildLoadData(paramModel, paramInt1, paramInt2, paramOptions);
        localObject2 = localObject1;
        if (localObject4 != null)
        {
          localObject2 = ((ModelLoader.LoadData)localObject4).sourceKey;
          localArrayList.add(((ModelLoader.LoadData)localObject4).fetcher);
        }
      }
      i++;
    }
    paramModel = (Model)localObject3;
    if (!localArrayList.isEmpty())
    {
      paramModel = (Model)localObject3;
      if (localObject1 != null) {
        paramModel = new ModelLoader.LoadData((Key)localObject1, new MultiFetcher(localArrayList, this.exceptionListPool));
      }
    }
    return paramModel;
  }
  
  public boolean handles(@NonNull Model paramModel)
  {
    Iterator localIterator = this.modelLoaders.iterator();
    while (localIterator.hasNext()) {
      if (((ModelLoader)localIterator.next()).handles(paramModel)) {
        return true;
      }
    }
    return false;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MultiModelLoader{modelLoaders=");
    localStringBuilder.append(Arrays.toString(this.modelLoaders.toArray()));
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  static class MultiFetcher<Data>
    implements DataFetcher<Data>, DataFetcher.DataCallback<Data>
  {
    private DataFetcher.DataCallback<? super Data> callback;
    private int currentIndex;
    @Nullable
    private List<Throwable> exceptions;
    private final List<DataFetcher<Data>> fetchers;
    private boolean isCancelled;
    private Priority priority;
    private final Pools.Pool<List<Throwable>> throwableListPool;
    
    MultiFetcher(@NonNull List<DataFetcher<Data>> paramList, @NonNull Pools.Pool<List<Throwable>> paramPool)
    {
      this.throwableListPool = paramPool;
      Preconditions.checkNotEmpty(paramList);
      this.fetchers = paramList;
      this.currentIndex = 0;
    }
    
    private void startNextOrFail()
    {
      if (this.isCancelled) {
        return;
      }
      if (this.currentIndex < this.fetchers.size() - 1)
      {
        this.currentIndex += 1;
        loadData(this.priority, this.callback);
      }
      else
      {
        Preconditions.checkNotNull(this.exceptions);
        this.callback.onLoadFailed(new GlideException("Fetch failed", new ArrayList(this.exceptions)));
      }
    }
    
    public void cancel()
    {
      this.isCancelled = true;
      Iterator localIterator = this.fetchers.iterator();
      while (localIterator.hasNext()) {
        ((DataFetcher)localIterator.next()).cancel();
      }
    }
    
    public void cleanup()
    {
      Object localObject = this.exceptions;
      if (localObject != null) {
        this.throwableListPool.release(localObject);
      }
      this.exceptions = null;
      localObject = this.fetchers.iterator();
      while (((Iterator)localObject).hasNext()) {
        ((DataFetcher)((Iterator)localObject).next()).cleanup();
      }
    }
    
    @NonNull
    public Class<Data> getDataClass()
    {
      return ((DataFetcher)this.fetchers.get(0)).getDataClass();
    }
    
    @NonNull
    public DataSource getDataSource()
    {
      return ((DataFetcher)this.fetchers.get(0)).getDataSource();
    }
    
    public void loadData(@NonNull Priority paramPriority, @NonNull DataFetcher.DataCallback<? super Data> paramDataCallback)
    {
      this.priority = paramPriority;
      this.callback = paramDataCallback;
      this.exceptions = ((List)this.throwableListPool.acquire());
      ((DataFetcher)this.fetchers.get(this.currentIndex)).loadData(paramPriority, this);
      if (this.isCancelled) {
        cancel();
      }
    }
    
    public void onDataReady(@Nullable Data paramData)
    {
      if (paramData != null) {
        this.callback.onDataReady(paramData);
      } else {
        startNextOrFail();
      }
    }
    
    public void onLoadFailed(@NonNull Exception paramException)
    {
      ((List)Preconditions.checkNotNull(this.exceptions)).add(paramException);
      startNextOrFail();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/MultiModelLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */