package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;
import android.util.Log;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ByteBufferEncoder
  implements Encoder<ByteBuffer>
{
  private static final String TAG = "ByteBufferEncoder";
  
  public boolean encode(@NonNull ByteBuffer paramByteBuffer, @NonNull File paramFile, @NonNull Options paramOptions)
  {
    boolean bool;
    try
    {
      ByteBufferUtil.toFile(paramByteBuffer, paramFile);
      bool = true;
    }
    catch (IOException paramByteBuffer)
    {
      if (Log.isLoggable("ByteBufferEncoder", 3)) {
        Log.d("ByteBufferEncoder", "Failed to write data", paramByteBuffer);
      }
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/ByteBufferEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */