package com.bumptech.glide.load.model;

import android.support.annotation.Nullable;

public abstract interface LazyHeaderFactory
{
  @Nullable
  public abstract String buildHeader();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/LazyHeaderFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */