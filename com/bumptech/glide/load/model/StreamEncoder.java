package com.bumptech.glide.load.model;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.InputStream;

public class StreamEncoder
  implements Encoder<InputStream>
{
  private static final String TAG = "StreamEncoder";
  private final ArrayPool byteArrayPool;
  
  public StreamEncoder(ArrayPool paramArrayPool)
  {
    this.byteArrayPool = paramArrayPool;
  }
  
  /* Error */
  public boolean encode(@android.support.annotation.NonNull InputStream paramInputStream, @android.support.annotation.NonNull java.io.File paramFile, @android.support.annotation.NonNull com.bumptech.glide.load.Options paramOptions)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 20	com/bumptech/glide/load/model/StreamEncoder:byteArrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   4: ldc 27
    //   6: ldc 29
    //   8: invokeinterface 35 3 0
    //   13: checkcast 29	[B
    //   16: astore 11
    //   18: iconst_0
    //   19: istore 7
    //   21: iconst_0
    //   22: istore 6
    //   24: aconst_null
    //   25: astore 9
    //   27: aconst_null
    //   28: astore 10
    //   30: aload 10
    //   32: astore_3
    //   33: new 37	java/io/FileOutputStream
    //   36: astore 8
    //   38: aload 10
    //   40: astore_3
    //   41: aload 8
    //   43: aload_2
    //   44: invokespecial 40	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   47: aload_1
    //   48: aload 11
    //   50: invokevirtual 46	java/io/InputStream:read	([B)I
    //   53: istore 4
    //   55: iload 4
    //   57: iconst_m1
    //   58: if_icmpeq +16 -> 74
    //   61: aload 8
    //   63: aload 11
    //   65: iconst_0
    //   66: iload 4
    //   68: invokevirtual 52	java/io/OutputStream:write	([BII)V
    //   71: goto -24 -> 47
    //   74: aload 8
    //   76: invokevirtual 55	java/io/OutputStream:close	()V
    //   79: iconst_1
    //   80: istore 5
    //   82: iconst_1
    //   83: istore 6
    //   85: aload 8
    //   87: invokevirtual 55	java/io/OutputStream:close	()V
    //   90: iload 6
    //   92: istore 5
    //   94: goto +67 -> 161
    //   97: astore_1
    //   98: aload 8
    //   100: astore_3
    //   101: goto +74 -> 175
    //   104: astore_2
    //   105: aload 8
    //   107: astore_1
    //   108: goto +11 -> 119
    //   111: astore_1
    //   112: goto +63 -> 175
    //   115: astore_2
    //   116: aload 9
    //   118: astore_1
    //   119: aload_1
    //   120: astore_3
    //   121: ldc 11
    //   123: iconst_3
    //   124: invokestatic 61	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   127: ifeq +14 -> 141
    //   130: aload_1
    //   131: astore_3
    //   132: ldc 11
    //   134: ldc 63
    //   136: aload_2
    //   137: invokestatic 67	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   140: pop
    //   141: iload 6
    //   143: istore 5
    //   145: aload_1
    //   146: ifnull +15 -> 161
    //   149: iload 7
    //   151: istore 5
    //   153: aload_1
    //   154: invokevirtual 55	java/io/OutputStream:close	()V
    //   157: iload 6
    //   159: istore 5
    //   161: aload_0
    //   162: getfield 20	com/bumptech/glide/load/model/StreamEncoder:byteArrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   165: aload 11
    //   167: invokeinterface 71 2 0
    //   172: iload 5
    //   174: ireturn
    //   175: aload_3
    //   176: ifnull +7 -> 183
    //   179: aload_3
    //   180: invokevirtual 55	java/io/OutputStream:close	()V
    //   183: aload_0
    //   184: getfield 20	com/bumptech/glide/load/model/StreamEncoder:byteArrayPool	Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
    //   187: aload 11
    //   189: invokeinterface 71 2 0
    //   194: aload_1
    //   195: athrow
    //   196: astore_1
    //   197: goto -36 -> 161
    //   200: astore_2
    //   201: goto -18 -> 183
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	this	StreamEncoder
    //   0	204	1	paramInputStream	InputStream
    //   0	204	2	paramFile	java.io.File
    //   0	204	3	paramOptions	com.bumptech.glide.load.Options
    //   53	14	4	i	int
    //   80	93	5	bool1	boolean
    //   22	136	6	bool2	boolean
    //   19	131	7	bool3	boolean
    //   36	70	8	localFileOutputStream	java.io.FileOutputStream
    //   25	92	9	localObject1	Object
    //   28	11	10	localObject2	Object
    //   16	172	11	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   47	55	97	finally
    //   61	71	97	finally
    //   74	79	97	finally
    //   47	55	104	java/io/IOException
    //   61	71	104	java/io/IOException
    //   74	79	104	java/io/IOException
    //   33	38	111	finally
    //   41	47	111	finally
    //   121	130	111	finally
    //   132	141	111	finally
    //   33	38	115	java/io/IOException
    //   41	47	115	java/io/IOException
    //   85	90	196	java/io/IOException
    //   153	157	196	java/io/IOException
    //   179	183	200	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/StreamEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */