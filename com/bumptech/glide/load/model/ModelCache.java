package com.bumptech.glide.load.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Util;
import java.util.Queue;

public class ModelCache<A, B>
{
  private static final int DEFAULT_SIZE = 250;
  private final LruCache<ModelKey<A>, B> cache;
  
  public ModelCache()
  {
    this(250L);
  }
  
  public ModelCache(long paramLong)
  {
    this.cache = new LruCache(paramLong)
    {
      protected void onItemEvicted(@NonNull ModelCache.ModelKey<A> paramAnonymousModelKey, @Nullable B paramAnonymousB)
      {
        paramAnonymousModelKey.release();
      }
    };
  }
  
  public void clear()
  {
    this.cache.clearMemory();
  }
  
  @Nullable
  public B get(A paramA, int paramInt1, int paramInt2)
  {
    paramA = ModelKey.get(paramA, paramInt1, paramInt2);
    Object localObject = this.cache.get(paramA);
    paramA.release();
    return (B)localObject;
  }
  
  public void put(A paramA, int paramInt1, int paramInt2, B paramB)
  {
    paramA = ModelKey.get(paramA, paramInt1, paramInt2);
    this.cache.put(paramA, paramB);
  }
  
  @VisibleForTesting
  static final class ModelKey<A>
  {
    private static final Queue<ModelKey<?>> KEY_QUEUE = Util.createQueue(0);
    private int height;
    private A model;
    private int width;
    
    static <A> ModelKey<A> get(A paramA, int paramInt1, int paramInt2)
    {
      synchronized (KEY_QUEUE)
      {
        ModelKey localModelKey = (ModelKey)KEY_QUEUE.poll();
        ??? = localModelKey;
        if (localModelKey == null) {
          ??? = new ModelKey();
        }
        ((ModelKey)???).init(paramA, paramInt1, paramInt2);
        return (ModelKey<A>)???;
      }
    }
    
    private void init(A paramA, int paramInt1, int paramInt2)
    {
      this.model = paramA;
      this.width = paramInt1;
      this.height = paramInt2;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool1 = paramObject instanceof ModelKey;
      boolean bool2 = false;
      if (bool1)
      {
        paramObject = (ModelKey)paramObject;
        bool1 = bool2;
        if (this.width == ((ModelKey)paramObject).width)
        {
          bool1 = bool2;
          if (this.height == ((ModelKey)paramObject).height)
          {
            bool1 = bool2;
            if (this.model.equals(((ModelKey)paramObject).model)) {
              bool1 = true;
            }
          }
        }
        return bool1;
      }
      return false;
    }
    
    public int hashCode()
    {
      return (this.height * 31 + this.width) * 31 + this.model.hashCode();
    }
    
    public void release()
    {
      synchronized (KEY_QUEUE)
      {
        KEY_QUEUE.offer(this);
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/load/model/ModelCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */