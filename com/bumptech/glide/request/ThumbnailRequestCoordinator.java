package com.bumptech.glide.request;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class ThumbnailRequestCoordinator
  implements RequestCoordinator, Request
{
  private Request full;
  private boolean isRunning;
  @Nullable
  private final RequestCoordinator parent;
  private Request thumb;
  
  @VisibleForTesting
  ThumbnailRequestCoordinator()
  {
    this(null);
  }
  
  public ThumbnailRequestCoordinator(@Nullable RequestCoordinator paramRequestCoordinator)
  {
    this.parent = paramRequestCoordinator;
  }
  
  private boolean parentCanNotifyCleared()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canNotifyCleared(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentCanNotifyStatusChanged()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canNotifyStatusChanged(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentCanSetImage()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canSetImage(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentIsAnyResourceSet()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (localRequestCoordinator.isAnyResourceSet())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void begin()
  {
    this.isRunning = true;
    if ((!this.full.isComplete()) && (!this.thumb.isRunning())) {
      this.thumb.begin();
    }
    if ((this.isRunning) && (!this.full.isRunning())) {
      this.full.begin();
    }
  }
  
  public boolean canNotifyCleared(Request paramRequest)
  {
    boolean bool;
    if ((parentCanNotifyCleared()) && (paramRequest.equals(this.full))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean canNotifyStatusChanged(Request paramRequest)
  {
    boolean bool;
    if ((parentCanNotifyStatusChanged()) && (paramRequest.equals(this.full)) && (!isAnyResourceSet())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean canSetImage(Request paramRequest)
  {
    boolean bool;
    if ((parentCanSetImage()) && ((paramRequest.equals(this.full)) || (!this.full.isResourceSet()))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void clear()
  {
    this.isRunning = false;
    this.thumb.clear();
    this.full.clear();
  }
  
  public boolean isAnyResourceSet()
  {
    boolean bool;
    if ((!parentIsAnyResourceSet()) && (!isResourceSet())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isCleared()
  {
    return this.full.isCleared();
  }
  
  public boolean isComplete()
  {
    boolean bool;
    if ((!this.full.isComplete()) && (!this.thumb.isComplete())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isEquivalentTo(Request paramRequest)
  {
    boolean bool1 = paramRequest instanceof ThumbnailRequestCoordinator;
    boolean bool2 = false;
    if (bool1)
    {
      paramRequest = (ThumbnailRequestCoordinator)paramRequest;
      Request localRequest = this.full;
      if (localRequest == null)
      {
        bool1 = bool2;
        if (paramRequest.full != null) {
          break label96;
        }
      }
      else
      {
        bool1 = bool2;
        if (!localRequest.isEquivalentTo(paramRequest.full)) {
          break label96;
        }
      }
      localRequest = this.thumb;
      if (localRequest == null)
      {
        bool1 = bool2;
        if (paramRequest.thumb != null) {
          break label96;
        }
      }
      else
      {
        bool1 = bool2;
        if (!localRequest.isEquivalentTo(paramRequest.thumb)) {
          break label96;
        }
      }
      bool1 = true;
      label96:
      return bool1;
    }
    return false;
  }
  
  public boolean isFailed()
  {
    return this.full.isFailed();
  }
  
  public boolean isResourceSet()
  {
    boolean bool;
    if ((!this.full.isResourceSet()) && (!this.thumb.isResourceSet())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isRunning()
  {
    return this.full.isRunning();
  }
  
  public void onRequestFailed(Request paramRequest)
  {
    if (!paramRequest.equals(this.full)) {
      return;
    }
    paramRequest = this.parent;
    if (paramRequest != null) {
      paramRequest.onRequestFailed(this);
    }
  }
  
  public void onRequestSuccess(Request paramRequest)
  {
    if (paramRequest.equals(this.thumb)) {
      return;
    }
    paramRequest = this.parent;
    if (paramRequest != null) {
      paramRequest.onRequestSuccess(this);
    }
    if (!this.thumb.isComplete()) {
      this.thumb.clear();
    }
  }
  
  public void recycle()
  {
    this.full.recycle();
    this.thumb.recycle();
  }
  
  public void setRequests(Request paramRequest1, Request paramRequest2)
  {
    this.full = paramRequest1;
    this.thumb = paramRequest2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/ThumbnailRequestCoordinator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */