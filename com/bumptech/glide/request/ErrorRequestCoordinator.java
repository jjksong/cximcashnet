package com.bumptech.glide.request;

import android.support.annotation.Nullable;

public final class ErrorRequestCoordinator
  implements RequestCoordinator, Request
{
  private Request error;
  @Nullable
  private final RequestCoordinator parent;
  private Request primary;
  
  public ErrorRequestCoordinator(@Nullable RequestCoordinator paramRequestCoordinator)
  {
    this.parent = paramRequestCoordinator;
  }
  
  private boolean isValidRequest(Request paramRequest)
  {
    boolean bool;
    if ((!paramRequest.equals(this.primary)) && ((!this.primary.isFailed()) || (!paramRequest.equals(this.error)))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentCanNotifyCleared()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canNotifyCleared(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentCanNotifyStatusChanged()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canNotifyStatusChanged(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentCanSetImage()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (!localRequestCoordinator.canSetImage(this))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean parentIsAnyResourceSet()
  {
    RequestCoordinator localRequestCoordinator = this.parent;
    boolean bool;
    if ((localRequestCoordinator != null) && (localRequestCoordinator.isAnyResourceSet())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void begin()
  {
    if (!this.primary.isRunning()) {
      this.primary.begin();
    }
  }
  
  public boolean canNotifyCleared(Request paramRequest)
  {
    boolean bool;
    if ((parentCanNotifyCleared()) && (isValidRequest(paramRequest))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean canNotifyStatusChanged(Request paramRequest)
  {
    boolean bool;
    if ((parentCanNotifyStatusChanged()) && (isValidRequest(paramRequest))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean canSetImage(Request paramRequest)
  {
    boolean bool;
    if ((parentCanSetImage()) && (isValidRequest(paramRequest))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void clear()
  {
    this.primary.clear();
    if (this.error.isRunning()) {
      this.error.clear();
    }
  }
  
  public boolean isAnyResourceSet()
  {
    boolean bool;
    if ((!parentIsAnyResourceSet()) && (!isResourceSet())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isCleared()
  {
    Request localRequest;
    if (this.primary.isFailed()) {
      localRequest = this.error;
    } else {
      localRequest = this.primary;
    }
    return localRequest.isCleared();
  }
  
  public boolean isComplete()
  {
    Request localRequest;
    if (this.primary.isFailed()) {
      localRequest = this.error;
    } else {
      localRequest = this.primary;
    }
    return localRequest.isComplete();
  }
  
  public boolean isEquivalentTo(Request paramRequest)
  {
    boolean bool1 = paramRequest instanceof ErrorRequestCoordinator;
    boolean bool2 = false;
    if (bool1)
    {
      paramRequest = (ErrorRequestCoordinator)paramRequest;
      bool1 = bool2;
      if (this.primary.isEquivalentTo(paramRequest.primary))
      {
        bool1 = bool2;
        if (this.error.isEquivalentTo(paramRequest.error)) {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  public boolean isFailed()
  {
    boolean bool;
    if ((this.primary.isFailed()) && (this.error.isFailed())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isResourceSet()
  {
    Request localRequest;
    if (this.primary.isFailed()) {
      localRequest = this.error;
    } else {
      localRequest = this.primary;
    }
    return localRequest.isResourceSet();
  }
  
  public boolean isRunning()
  {
    Request localRequest;
    if (this.primary.isFailed()) {
      localRequest = this.error;
    } else {
      localRequest = this.primary;
    }
    return localRequest.isRunning();
  }
  
  public void onRequestFailed(Request paramRequest)
  {
    if (!paramRequest.equals(this.error))
    {
      if (!this.error.isRunning()) {
        this.error.begin();
      }
      return;
    }
    paramRequest = this.parent;
    if (paramRequest != null) {
      paramRequest.onRequestFailed(this);
    }
  }
  
  public void onRequestSuccess(Request paramRequest)
  {
    paramRequest = this.parent;
    if (paramRequest != null) {
      paramRequest.onRequestSuccess(this);
    }
  }
  
  public void recycle()
  {
    this.primary.recycle();
    this.error.recycle();
  }
  
  public void setRequests(Request paramRequest1, Request paramRequest2)
  {
    this.primary = paramRequest1;
    this.error = paramRequest2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/ErrorRequestCoordinator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */