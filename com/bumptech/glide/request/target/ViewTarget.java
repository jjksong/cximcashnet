package com.bumptech.glide.request.target;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
public abstract class ViewTarget<T extends View, Z>
  extends BaseTarget<Z>
{
  private static final String TAG = "ViewTarget";
  private static boolean isTagUsedAtLeastOnce;
  @Nullable
  private static Integer tagId;
  @Nullable
  private View.OnAttachStateChangeListener attachStateListener;
  private boolean isAttachStateListenerAdded;
  private boolean isClearedByUs;
  private final SizeDeterminer sizeDeterminer;
  protected final T view;
  
  public ViewTarget(@NonNull T paramT)
  {
    this.view = ((View)Preconditions.checkNotNull(paramT));
    this.sizeDeterminer = new SizeDeterminer(paramT);
  }
  
  @Deprecated
  public ViewTarget(@NonNull T paramT, boolean paramBoolean)
  {
    this(paramT);
    if (paramBoolean) {
      waitForLayout();
    }
  }
  
  @Nullable
  private Object getTag()
  {
    Integer localInteger = tagId;
    if (localInteger == null) {
      return this.view.getTag();
    }
    return this.view.getTag(localInteger.intValue());
  }
  
  private void maybeAddAttachStateListener()
  {
    View.OnAttachStateChangeListener localOnAttachStateChangeListener = this.attachStateListener;
    if ((localOnAttachStateChangeListener != null) && (!this.isAttachStateListenerAdded))
    {
      this.view.addOnAttachStateChangeListener(localOnAttachStateChangeListener);
      this.isAttachStateListenerAdded = true;
      return;
    }
  }
  
  private void maybeRemoveAttachStateListener()
  {
    View.OnAttachStateChangeListener localOnAttachStateChangeListener = this.attachStateListener;
    if ((localOnAttachStateChangeListener != null) && (this.isAttachStateListenerAdded))
    {
      this.view.removeOnAttachStateChangeListener(localOnAttachStateChangeListener);
      this.isAttachStateListenerAdded = false;
      return;
    }
  }
  
  private void setTag(@Nullable Object paramObject)
  {
    Integer localInteger = tagId;
    if (localInteger == null)
    {
      isTagUsedAtLeastOnce = true;
      this.view.setTag(paramObject);
    }
    else
    {
      this.view.setTag(localInteger.intValue(), paramObject);
    }
  }
  
  public static void setTagId(int paramInt)
  {
    if ((tagId == null) && (!isTagUsedAtLeastOnce))
    {
      tagId = Integer.valueOf(paramInt);
      return;
    }
    throw new IllegalArgumentException("You cannot set the tag id more than once or change the tag id after the first request has been made");
  }
  
  @NonNull
  public final ViewTarget<T, Z> clearOnDetach()
  {
    if (this.attachStateListener != null) {
      return this;
    }
    this.attachStateListener = new View.OnAttachStateChangeListener()
    {
      public void onViewAttachedToWindow(View paramAnonymousView)
      {
        ViewTarget.this.resumeMyRequest();
      }
      
      public void onViewDetachedFromWindow(View paramAnonymousView)
      {
        ViewTarget.this.pauseMyRequest();
      }
    };
    maybeAddAttachStateListener();
    return this;
  }
  
  @Nullable
  public Request getRequest()
  {
    Object localObject = getTag();
    if (localObject != null)
    {
      if ((localObject instanceof Request)) {
        localObject = (Request)localObject;
      } else {
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
      }
    }
    else {
      localObject = null;
    }
    return (Request)localObject;
  }
  
  @CallSuper
  public void getSize(@NonNull SizeReadyCallback paramSizeReadyCallback)
  {
    this.sizeDeterminer.getSize(paramSizeReadyCallback);
  }
  
  @NonNull
  public T getView()
  {
    return this.view;
  }
  
  @CallSuper
  public void onLoadCleared(@Nullable Drawable paramDrawable)
  {
    super.onLoadCleared(paramDrawable);
    this.sizeDeterminer.clearCallbacksAndListener();
    if (!this.isClearedByUs) {
      maybeRemoveAttachStateListener();
    }
  }
  
  @CallSuper
  public void onLoadStarted(@Nullable Drawable paramDrawable)
  {
    super.onLoadStarted(paramDrawable);
    maybeAddAttachStateListener();
  }
  
  void pauseMyRequest()
  {
    Request localRequest = getRequest();
    if (localRequest != null)
    {
      this.isClearedByUs = true;
      localRequest.clear();
      this.isClearedByUs = false;
    }
  }
  
  @CallSuper
  public void removeCallback(@NonNull SizeReadyCallback paramSizeReadyCallback)
  {
    this.sizeDeterminer.removeCallback(paramSizeReadyCallback);
  }
  
  void resumeMyRequest()
  {
    Request localRequest = getRequest();
    if ((localRequest != null) && (localRequest.isCleared())) {
      localRequest.begin();
    }
  }
  
  public void setRequest(@Nullable Request paramRequest)
  {
    setTag(paramRequest);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Target for: ");
    localStringBuilder.append(this.view);
    return localStringBuilder.toString();
  }
  
  @NonNull
  public final ViewTarget<T, Z> waitForLayout()
  {
    this.sizeDeterminer.waitForLayout = true;
    return this;
  }
  
  @VisibleForTesting
  static final class SizeDeterminer
  {
    private static final int PENDING_SIZE = 0;
    @Nullable
    @VisibleForTesting
    static Integer maxDisplayLength;
    private final List<SizeReadyCallback> cbs = new ArrayList();
    @Nullable
    private SizeDeterminerLayoutListener layoutListener;
    private final View view;
    boolean waitForLayout;
    
    SizeDeterminer(@NonNull View paramView)
    {
      this.view = paramView;
    }
    
    private static int getMaxDisplayLength(@NonNull Context paramContext)
    {
      if (maxDisplayLength == null)
      {
        Display localDisplay = ((WindowManager)Preconditions.checkNotNull((WindowManager)paramContext.getSystemService("window"))).getDefaultDisplay();
        paramContext = new Point();
        localDisplay.getSize(paramContext);
        maxDisplayLength = Integer.valueOf(Math.max(paramContext.x, paramContext.y));
      }
      return maxDisplayLength.intValue();
    }
    
    private int getTargetDimen(int paramInt1, int paramInt2, int paramInt3)
    {
      int i = paramInt2 - paramInt3;
      if (i > 0) {
        return i;
      }
      if ((this.waitForLayout) && (this.view.isLayoutRequested())) {
        return 0;
      }
      paramInt1 -= paramInt3;
      if (paramInt1 > 0) {
        return paramInt1;
      }
      if ((!this.view.isLayoutRequested()) && (paramInt2 == -2))
      {
        if (Log.isLoggable("ViewTarget", 4)) {
          Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use .override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
        }
        return getMaxDisplayLength(this.view.getContext());
      }
      return 0;
    }
    
    private int getTargetHeight()
    {
      int j = this.view.getPaddingTop();
      int k = this.view.getPaddingBottom();
      ViewGroup.LayoutParams localLayoutParams = this.view.getLayoutParams();
      int i;
      if (localLayoutParams != null) {
        i = localLayoutParams.height;
      } else {
        i = 0;
      }
      return getTargetDimen(this.view.getHeight(), i, j + k);
    }
    
    private int getTargetWidth()
    {
      int k = this.view.getPaddingLeft();
      int j = this.view.getPaddingRight();
      ViewGroup.LayoutParams localLayoutParams = this.view.getLayoutParams();
      int i;
      if (localLayoutParams != null) {
        i = localLayoutParams.width;
      } else {
        i = 0;
      }
      return getTargetDimen(this.view.getWidth(), i, k + j);
    }
    
    private boolean isDimensionValid(int paramInt)
    {
      boolean bool;
      if ((paramInt <= 0) && (paramInt != Integer.MIN_VALUE)) {
        bool = false;
      } else {
        bool = true;
      }
      return bool;
    }
    
    private boolean isViewStateAndSizeValid(int paramInt1, int paramInt2)
    {
      boolean bool;
      if ((isDimensionValid(paramInt1)) && (isDimensionValid(paramInt2))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    private void notifyCbs(int paramInt1, int paramInt2)
    {
      Iterator localIterator = new ArrayList(this.cbs).iterator();
      while (localIterator.hasNext()) {
        ((SizeReadyCallback)localIterator.next()).onSizeReady(paramInt1, paramInt2);
      }
    }
    
    void checkCurrentDimens()
    {
      if (this.cbs.isEmpty()) {
        return;
      }
      int i = getTargetWidth();
      int j = getTargetHeight();
      if (!isViewStateAndSizeValid(i, j)) {
        return;
      }
      notifyCbs(i, j);
      clearCallbacksAndListener();
    }
    
    void clearCallbacksAndListener()
    {
      ViewTreeObserver localViewTreeObserver = this.view.getViewTreeObserver();
      if (localViewTreeObserver.isAlive()) {
        localViewTreeObserver.removeOnPreDrawListener(this.layoutListener);
      }
      this.layoutListener = null;
      this.cbs.clear();
    }
    
    void getSize(@NonNull SizeReadyCallback paramSizeReadyCallback)
    {
      int i = getTargetWidth();
      int j = getTargetHeight();
      if (isViewStateAndSizeValid(i, j))
      {
        paramSizeReadyCallback.onSizeReady(i, j);
        return;
      }
      if (!this.cbs.contains(paramSizeReadyCallback)) {
        this.cbs.add(paramSizeReadyCallback);
      }
      if (this.layoutListener == null)
      {
        paramSizeReadyCallback = this.view.getViewTreeObserver();
        this.layoutListener = new SizeDeterminerLayoutListener(this);
        paramSizeReadyCallback.addOnPreDrawListener(this.layoutListener);
      }
    }
    
    void removeCallback(@NonNull SizeReadyCallback paramSizeReadyCallback)
    {
      this.cbs.remove(paramSizeReadyCallback);
    }
    
    private static final class SizeDeterminerLayoutListener
      implements ViewTreeObserver.OnPreDrawListener
    {
      private final WeakReference<ViewTarget.SizeDeterminer> sizeDeterminerRef;
      
      SizeDeterminerLayoutListener(@NonNull ViewTarget.SizeDeterminer paramSizeDeterminer)
      {
        this.sizeDeterminerRef = new WeakReference(paramSizeDeterminer);
      }
      
      public boolean onPreDraw()
      {
        if (Log.isLoggable("ViewTarget", 2))
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("OnGlobalLayoutListener called attachStateListener=");
          ((StringBuilder)localObject).append(this);
          Log.v("ViewTarget", ((StringBuilder)localObject).toString());
        }
        Object localObject = (ViewTarget.SizeDeterminer)this.sizeDeterminerRef.get();
        if (localObject != null) {
          ((ViewTarget.SizeDeterminer)localObject).checkCurrentDimens();
        }
        return true;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/target/ViewTarget.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */