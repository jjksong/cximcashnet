package com.bumptech.glide.request.target;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.transition.Transition.ViewAdapter;

public abstract class ImageViewTarget<Z>
  extends ViewTarget<ImageView, Z>
  implements Transition.ViewAdapter
{
  @Nullable
  private Animatable animatable;
  
  public ImageViewTarget(ImageView paramImageView)
  {
    super(paramImageView);
  }
  
  @Deprecated
  public ImageViewTarget(ImageView paramImageView, boolean paramBoolean)
  {
    super(paramImageView, paramBoolean);
  }
  
  private void maybeUpdateAnimatable(@Nullable Z paramZ)
  {
    if ((paramZ instanceof Animatable))
    {
      this.animatable = ((Animatable)paramZ);
      this.animatable.start();
    }
    else
    {
      this.animatable = null;
    }
  }
  
  private void setResourceInternal(@Nullable Z paramZ)
  {
    setResource(paramZ);
    maybeUpdateAnimatable(paramZ);
  }
  
  @Nullable
  public Drawable getCurrentDrawable()
  {
    return ((ImageView)this.view).getDrawable();
  }
  
  public void onLoadCleared(@Nullable Drawable paramDrawable)
  {
    super.onLoadCleared(paramDrawable);
    Animatable localAnimatable = this.animatable;
    if (localAnimatable != null) {
      localAnimatable.stop();
    }
    setResourceInternal(null);
    setDrawable(paramDrawable);
  }
  
  public void onLoadFailed(@Nullable Drawable paramDrawable)
  {
    super.onLoadFailed(paramDrawable);
    setResourceInternal(null);
    setDrawable(paramDrawable);
  }
  
  public void onLoadStarted(@Nullable Drawable paramDrawable)
  {
    super.onLoadStarted(paramDrawable);
    setResourceInternal(null);
    setDrawable(paramDrawable);
  }
  
  public void onResourceReady(@NonNull Z paramZ, @Nullable Transition<? super Z> paramTransition)
  {
    if ((paramTransition != null) && (paramTransition.transition(paramZ, this))) {
      maybeUpdateAnimatable(paramZ);
    } else {
      setResourceInternal(paramZ);
    }
  }
  
  public void onStart()
  {
    Animatable localAnimatable = this.animatable;
    if (localAnimatable != null) {
      localAnimatable.start();
    }
  }
  
  public void onStop()
  {
    Animatable localAnimatable = this.animatable;
    if (localAnimatable != null) {
      localAnimatable.stop();
    }
  }
  
  public void setDrawable(Drawable paramDrawable)
  {
    ((ImageView)this.view).setImageDrawable(paramDrawable);
  }
  
  protected abstract void setResource(@Nullable Z paramZ);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/target/ImageViewTarget.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */