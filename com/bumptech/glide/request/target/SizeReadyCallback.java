package com.bumptech.glide.request.target;

public abstract interface SizeReadyCallback
{
  public abstract void onSizeReady(int paramInt1, int paramInt2);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/target/SizeReadyCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */