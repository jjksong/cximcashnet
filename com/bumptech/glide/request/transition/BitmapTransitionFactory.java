package com.bumptech.glide.request.transition;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

public class BitmapTransitionFactory
  extends BitmapContainerTransitionFactory<Bitmap>
{
  public BitmapTransitionFactory(@NonNull TransitionFactory<Drawable> paramTransitionFactory)
  {
    super(paramTransitionFactory);
  }
  
  @NonNull
  protected Bitmap getBitmap(@NonNull Bitmap paramBitmap)
  {
    return paramBitmap;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/transition/BitmapTransitionFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */