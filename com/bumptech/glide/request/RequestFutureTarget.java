package com.bumptech.glide.request;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.util.Util;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestFutureTarget<R>
  implements FutureTarget<R>, RequestListener<R>
{
  private static final Waiter DEFAULT_WAITER = new Waiter();
  private final boolean assertBackgroundThread;
  @Nullable
  private GlideException exception;
  private final int height;
  private boolean isCancelled;
  private boolean loadFailed;
  @Nullable
  private Request request;
  @Nullable
  private R resource;
  private boolean resultReceived;
  private final Waiter waiter;
  private final int width;
  
  public RequestFutureTarget(int paramInt1, int paramInt2)
  {
    this(paramInt1, paramInt2, true, DEFAULT_WAITER);
  }
  
  RequestFutureTarget(int paramInt1, int paramInt2, boolean paramBoolean, Waiter paramWaiter)
  {
    this.width = paramInt1;
    this.height = paramInt2;
    this.assertBackgroundThread = paramBoolean;
    this.waiter = paramWaiter;
  }
  
  private R doGet(Long paramLong)
    throws ExecutionException, InterruptedException, TimeoutException
  {
    try
    {
      if ((this.assertBackgroundThread) && (!isDone())) {
        Util.assertBackgroundThread();
      }
      if (!this.isCancelled)
      {
        if (!this.loadFailed)
        {
          if (this.resultReceived)
          {
            paramLong = this.resource;
            return paramLong;
          }
          if (paramLong == null)
          {
            this.waiter.waitForTimeout(this, 0L);
          }
          else if (paramLong.longValue() > 0L)
          {
            long l1 = System.currentTimeMillis();
            long l2 = paramLong.longValue() + l1;
            while ((!isDone()) && (l1 < l2))
            {
              this.waiter.waitForTimeout(this, l2 - l1);
              l1 = System.currentTimeMillis();
            }
          }
          if (!Thread.interrupted())
          {
            if (!this.loadFailed)
            {
              if (!this.isCancelled)
              {
                if (this.resultReceived)
                {
                  paramLong = this.resource;
                  return paramLong;
                }
                paramLong = new java/util/concurrent/TimeoutException;
                paramLong.<init>();
                throw paramLong;
              }
              paramLong = new java/util/concurrent/CancellationException;
              paramLong.<init>();
              throw paramLong;
            }
            paramLong = new java/util/concurrent/ExecutionException;
            paramLong.<init>(this.exception);
            throw paramLong;
          }
          paramLong = new java/lang/InterruptedException;
          paramLong.<init>();
          throw paramLong;
        }
        paramLong = new java/util/concurrent/ExecutionException;
        paramLong.<init>(this.exception);
        throw paramLong;
      }
      paramLong = new java/util/concurrent/CancellationException;
      paramLong.<init>();
      throw paramLong;
    }
    finally {}
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    try
    {
      boolean bool = isDone();
      if (bool) {
        return false;
      }
      this.isCancelled = true;
      this.waiter.notifyAll(this);
      if ((paramBoolean) && (this.request != null))
      {
        this.request.clear();
        this.request = null;
      }
      return true;
    }
    finally {}
  }
  
  public R get()
    throws InterruptedException, ExecutionException
  {
    try
    {
      Object localObject = doGet(null);
      return (R)localObject;
    }
    catch (TimeoutException localTimeoutException)
    {
      throw new AssertionError(localTimeoutException);
    }
  }
  
  public R get(long paramLong, @NonNull TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    return (R)doGet(Long.valueOf(paramTimeUnit.toMillis(paramLong)));
  }
  
  @Nullable
  public Request getRequest()
  {
    try
    {
      Request localRequest = this.request;
      return localRequest;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void getSize(@NonNull SizeReadyCallback paramSizeReadyCallback)
  {
    paramSizeReadyCallback.onSizeReady(this.width, this.height);
  }
  
  public boolean isCancelled()
  {
    try
    {
      boolean bool = this.isCancelled;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean isDone()
  {
    try
    {
      if ((!this.isCancelled) && (!this.resultReceived))
      {
        bool = this.loadFailed;
        if (!bool)
        {
          bool = false;
          break label35;
        }
      }
      boolean bool = true;
      label35:
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void onDestroy() {}
  
  public void onLoadCleared(@Nullable Drawable paramDrawable) {}
  
  public void onLoadFailed(@Nullable Drawable paramDrawable) {}
  
  public boolean onLoadFailed(@Nullable GlideException paramGlideException, Object paramObject, Target<R> paramTarget, boolean paramBoolean)
  {
    try
    {
      this.loadFailed = true;
      this.exception = paramGlideException;
      this.waiter.notifyAll(this);
      return false;
    }
    finally
    {
      paramGlideException = finally;
      throw paramGlideException;
    }
  }
  
  public void onLoadStarted(@Nullable Drawable paramDrawable) {}
  
  public void onResourceReady(@NonNull R paramR, @Nullable Transition<? super R> paramTransition) {}
  
  public boolean onResourceReady(R paramR, Object paramObject, Target<R> paramTarget, DataSource paramDataSource, boolean paramBoolean)
  {
    try
    {
      this.resultReceived = true;
      this.resource = paramR;
      this.waiter.notifyAll(this);
      return false;
    }
    finally
    {
      paramR = finally;
      throw paramR;
    }
  }
  
  public void onStart() {}
  
  public void onStop() {}
  
  public void removeCallback(@NonNull SizeReadyCallback paramSizeReadyCallback) {}
  
  public void setRequest(@Nullable Request paramRequest)
  {
    try
    {
      this.request = paramRequest;
      return;
    }
    finally
    {
      paramRequest = finally;
      throw paramRequest;
    }
  }
  
  @VisibleForTesting
  static class Waiter
  {
    void notifyAll(Object paramObject)
    {
      paramObject.notifyAll();
    }
    
    void waitForTimeout(Object paramObject, long paramLong)
      throws InterruptedException
    {
      paramObject.wait(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/request/RequestFutureTarget.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */