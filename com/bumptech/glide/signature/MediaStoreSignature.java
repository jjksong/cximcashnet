package com.bumptech.glide.signature;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.load.Key;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class MediaStoreSignature
  implements Key
{
  private final long dateModified;
  @NonNull
  private final String mimeType;
  private final int orientation;
  
  public MediaStoreSignature(@Nullable String paramString, long paramLong, int paramInt)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    this.mimeType = str;
    this.dateModified = paramLong;
    this.orientation = paramInt;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (MediaStoreSignature)paramObject;
      if (this.dateModified != ((MediaStoreSignature)paramObject).dateModified) {
        return false;
      }
      if (this.orientation != ((MediaStoreSignature)paramObject).orientation) {
        return false;
      }
      return this.mimeType.equals(((MediaStoreSignature)paramObject).mimeType);
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = this.mimeType.hashCode();
    long l = this.dateModified;
    return (i * 31 + (int)(l ^ l >>> 32)) * 31 + this.orientation;
  }
  
  public void updateDiskCacheKey(@NonNull MessageDigest paramMessageDigest)
  {
    paramMessageDigest.update(ByteBuffer.allocate(12).putLong(this.dateModified).putInt(this.orientation).array());
    paramMessageDigest.update(this.mimeType.getBytes(CHARSET));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/signature/MediaStoreSignature.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */