package com.bumptech.glide.manager;

import android.support.annotation.NonNull;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.Util;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public final class TargetTracker
  implements LifecycleListener
{
  private final Set<Target<?>> targets = Collections.newSetFromMap(new WeakHashMap());
  
  public void clear()
  {
    this.targets.clear();
  }
  
  @NonNull
  public List<Target<?>> getAll()
  {
    return Util.getSnapshot(this.targets);
  }
  
  public void onDestroy()
  {
    Iterator localIterator = Util.getSnapshot(this.targets).iterator();
    while (localIterator.hasNext()) {
      ((Target)localIterator.next()).onDestroy();
    }
  }
  
  public void onStart()
  {
    Iterator localIterator = Util.getSnapshot(this.targets).iterator();
    while (localIterator.hasNext()) {
      ((Target)localIterator.next()).onStart();
    }
  }
  
  public void onStop()
  {
    Iterator localIterator = Util.getSnapshot(this.targets).iterator();
    while (localIterator.hasNext()) {
      ((Target)localIterator.next()).onStop();
    }
  }
  
  public void track(@NonNull Target<?> paramTarget)
  {
    this.targets.add(paramTarget);
  }
  
  public void untrack(@NonNull Target<?> paramTarget)
  {
    this.targets.remove(paramTarget);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/manager/TargetTracker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */