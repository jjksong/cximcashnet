package com.bumptech.glide.manager;

import android.support.annotation.NonNull;

public abstract interface Lifecycle
{
  public abstract void addListener(@NonNull LifecycleListener paramLifecycleListener);
  
  public abstract void removeListener(@NonNull LifecycleListener paramLifecycleListener);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/manager/Lifecycle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */