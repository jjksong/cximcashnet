package com.bumptech.glide.manager;

public abstract interface LifecycleListener
{
  public abstract void onDestroy();
  
  public abstract void onStart();
  
  public abstract void onStop();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/manager/LifecycleListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */