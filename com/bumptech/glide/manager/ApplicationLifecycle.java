package com.bumptech.glide.manager;

import android.support.annotation.NonNull;

class ApplicationLifecycle
  implements Lifecycle
{
  public void addListener(@NonNull LifecycleListener paramLifecycleListener)
  {
    paramLifecycleListener.onStart();
  }
  
  public void removeListener(@NonNull LifecycleListener paramLifecycleListener) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/manager/ApplicationLifecycle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */