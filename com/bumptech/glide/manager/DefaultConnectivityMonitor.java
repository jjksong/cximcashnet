package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;
import com.bumptech.glide.util.Preconditions;

final class DefaultConnectivityMonitor
  implements ConnectivityMonitor
{
  private static final String TAG = "ConnectivityMonitor";
  private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver()
  {
    public void onReceive(@NonNull Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      boolean bool = DefaultConnectivityMonitor.this.isConnected;
      paramAnonymousIntent = DefaultConnectivityMonitor.this;
      paramAnonymousIntent.isConnected = paramAnonymousIntent.isConnected(paramAnonymousContext);
      if (bool != DefaultConnectivityMonitor.this.isConnected)
      {
        if (Log.isLoggable("ConnectivityMonitor", 3))
        {
          paramAnonymousContext = new StringBuilder();
          paramAnonymousContext.append("connectivity changed, isConnected: ");
          paramAnonymousContext.append(DefaultConnectivityMonitor.this.isConnected);
          Log.d("ConnectivityMonitor", paramAnonymousContext.toString());
        }
        DefaultConnectivityMonitor.this.listener.onConnectivityChanged(DefaultConnectivityMonitor.this.isConnected);
      }
    }
  };
  private final Context context;
  boolean isConnected;
  private boolean isRegistered;
  final ConnectivityMonitor.ConnectivityListener listener;
  
  DefaultConnectivityMonitor(@NonNull Context paramContext, @NonNull ConnectivityMonitor.ConnectivityListener paramConnectivityListener)
  {
    this.context = paramContext.getApplicationContext();
    this.listener = paramConnectivityListener;
  }
  
  private void register()
  {
    if (this.isRegistered) {
      return;
    }
    this.isConnected = isConnected(this.context);
    try
    {
      Context localContext = this.context;
      BroadcastReceiver localBroadcastReceiver = this.connectivityReceiver;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>("android.net.conn.CONNECTIVITY_CHANGE");
      localContext.registerReceiver(localBroadcastReceiver, localIntentFilter);
      this.isRegistered = true;
    }
    catch (SecurityException localSecurityException)
    {
      if (Log.isLoggable("ConnectivityMonitor", 5)) {
        Log.w("ConnectivityMonitor", "Failed to register", localSecurityException);
      }
    }
  }
  
  private void unregister()
  {
    if (!this.isRegistered) {
      return;
    }
    this.context.unregisterReceiver(this.connectivityReceiver);
    this.isRegistered = false;
  }
  
  @SuppressLint({"MissingPermission"})
  boolean isConnected(@NonNull Context paramContext)
  {
    paramContext = (ConnectivityManager)Preconditions.checkNotNull((ConnectivityManager)paramContext.getSystemService("connectivity"));
    boolean bool = true;
    try
    {
      paramContext = paramContext.getActiveNetworkInfo();
      if ((paramContext == null) || (!paramContext.isConnected())) {
        bool = false;
      }
      return bool;
    }
    catch (RuntimeException paramContext)
    {
      if (Log.isLoggable("ConnectivityMonitor", 5)) {
        Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", paramContext);
      }
    }
    return true;
  }
  
  public void onDestroy() {}
  
  public void onStart()
  {
    register();
  }
  
  public void onStop()
  {
    unregister();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/manager/DefaultConnectivityMonitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */