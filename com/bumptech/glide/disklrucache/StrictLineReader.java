package com.bumptech.glide.disklrucache;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

class StrictLineReader
  implements Closeable
{
  private static final byte CR = 13;
  private static final byte LF = 10;
  private byte[] buf;
  private final Charset charset;
  private int end;
  private final InputStream in;
  private int pos;
  
  public StrictLineReader(InputStream paramInputStream, int paramInt, Charset paramCharset)
  {
    if ((paramInputStream != null) && (paramCharset != null))
    {
      if (paramInt >= 0)
      {
        if (paramCharset.equals(Util.US_ASCII))
        {
          this.in = paramInputStream;
          this.charset = paramCharset;
          this.buf = new byte[paramInt];
          return;
        }
        throw new IllegalArgumentException("Unsupported encoding");
      }
      throw new IllegalArgumentException("capacity <= 0");
    }
    throw new NullPointerException();
  }
  
  public StrictLineReader(InputStream paramInputStream, Charset paramCharset)
  {
    this(paramInputStream, 8192, paramCharset);
  }
  
  private void fillBuf()
    throws IOException
  {
    InputStream localInputStream = this.in;
    byte[] arrayOfByte = this.buf;
    int i = localInputStream.read(arrayOfByte, 0, arrayOfByte.length);
    if (i != -1)
    {
      this.pos = 0;
      this.end = i;
      return;
    }
    throw new EOFException();
  }
  
  public void close()
    throws IOException
  {
    synchronized (this.in)
    {
      if (this.buf != null)
      {
        this.buf = null;
        this.in.close();
      }
      return;
    }
  }
  
  public boolean hasUnterminatedLine()
  {
    boolean bool;
    if (this.end == -1) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public String readLine()
    throws IOException
  {
    synchronized (this.in)
    {
      if (this.buf != null)
      {
        if (this.pos >= this.end) {
          fillBuf();
        }
        for (int i = this.pos; i != this.end; i++) {
          if (this.buf[i] == 10)
          {
            int j;
            if (i != this.pos)
            {
              localObject1 = this.buf;
              j = i - 1;
              if (localObject1[j] == 13) {}
            }
            else
            {
              j = i;
            }
            localObject1 = new java/lang/String;
            ((String)localObject1).<init>(this.buf, this.pos, j - this.pos, this.charset.name());
            this.pos = (i + 1);
            return (String)localObject1;
          }
        }
        localObject1 = new com/bumptech/glide/disklrucache/StrictLineReader$1;
        ((1)localObject1).<init>(this, this.end - this.pos + 80);
        for (;;)
        {
          ((ByteArrayOutputStream)localObject1).write(this.buf, this.pos, this.end - this.pos);
          this.end = -1;
          fillBuf();
          for (i = this.pos; i != this.end; i++) {
            if (this.buf[i] == 10)
            {
              if (i != this.pos) {
                ((ByteArrayOutputStream)localObject1).write(this.buf, this.pos, i - this.pos);
              }
              this.pos = (i + 1);
              localObject1 = ((ByteArrayOutputStream)localObject1).toString();
              return (String)localObject1;
            }
          }
        }
      }
      Object localObject1 = new java/io/IOException;
      ((IOException)localObject1).<init>("LineReader is closed");
      throw ((Throwable)localObject1);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/disklrucache/StrictLineReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */