package com.bumptech.glide.disklrucache;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class DiskLruCache
  implements Closeable
{
  static final long ANY_SEQUENCE_NUMBER = -1L;
  private static final String CLEAN = "CLEAN";
  private static final String DIRTY = "DIRTY";
  static final String JOURNAL_FILE = "journal";
  static final String JOURNAL_FILE_BACKUP = "journal.bkp";
  static final String JOURNAL_FILE_TEMP = "journal.tmp";
  static final String MAGIC = "libcore.io.DiskLruCache";
  private static final String READ = "READ";
  private static final String REMOVE = "REMOVE";
  static final String VERSION_1 = "1";
  private final int appVersion;
  private final Callable<Void> cleanupCallable = new Callable()
  {
    public Void call()
      throws Exception
    {
      synchronized (DiskLruCache.this)
      {
        if (DiskLruCache.this.journalWriter == null) {
          return null;
        }
        DiskLruCache.this.trimToSize();
        if (DiskLruCache.this.journalRebuildRequired())
        {
          DiskLruCache.this.rebuildJournal();
          DiskLruCache.access$502(DiskLruCache.this, 0);
        }
        return null;
      }
    }
  };
  private final File directory;
  final ThreadPoolExecutor executorService = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new DiskLruCacheThreadFactory(null));
  private final File journalFile;
  private final File journalFileBackup;
  private final File journalFileTmp;
  private Writer journalWriter;
  private final LinkedHashMap<String, Entry> lruEntries = new LinkedHashMap(0, 0.75F, true);
  private long maxSize;
  private long nextSequenceNumber = 0L;
  private int redundantOpCount;
  private long size = 0L;
  private final int valueCount;
  
  private DiskLruCache(File paramFile, int paramInt1, int paramInt2, long paramLong)
  {
    this.directory = paramFile;
    this.appVersion = paramInt1;
    this.journalFile = new File(paramFile, "journal");
    this.journalFileTmp = new File(paramFile, "journal.tmp");
    this.journalFileBackup = new File(paramFile, "journal.bkp");
    this.valueCount = paramInt2;
    this.maxSize = paramLong;
  }
  
  private void checkNotClosed()
  {
    if (this.journalWriter != null) {
      return;
    }
    throw new IllegalStateException("cache is closed");
  }
  
  private void completeEdit(Editor paramEditor, boolean paramBoolean)
    throws IOException
  {
    try
    {
      Object localObject = paramEditor.entry;
      if (((Entry)localObject).currentEditor == paramEditor)
      {
        int k = 0;
        int j = k;
        if (paramBoolean)
        {
          j = k;
          if (!((Entry)localObject).readable)
          {
            for (int i = 0;; i++)
            {
              j = k;
              if (i >= this.valueCount) {
                break label132;
              }
              if (paramEditor.written[i] == 0) {
                break;
              }
              if (!((Entry)localObject).getDirtyFile(i).exists())
              {
                paramEditor.abort();
                return;
              }
            }
            paramEditor.abort();
            paramEditor = new java/lang/IllegalStateException;
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append("Newly created entry didn't create value for index ");
            ((StringBuilder)localObject).append(i);
            paramEditor.<init>(((StringBuilder)localObject).toString());
            throw paramEditor;
          }
        }
        label132:
        long l1;
        while (j < this.valueCount)
        {
          paramEditor = ((Entry)localObject).getDirtyFile(j);
          if (paramBoolean)
          {
            if (paramEditor.exists())
            {
              File localFile = ((Entry)localObject).getCleanFile(j);
              paramEditor.renameTo(localFile);
              l1 = localObject.lengths[j];
              long l2 = localFile.length();
              ((Entry)localObject).lengths[j] = l2;
              this.size = (this.size - l1 + l2);
            }
          }
          else {
            deleteIfExists(paramEditor);
          }
          j++;
        }
        this.redundantOpCount += 1;
        Entry.access$802((Entry)localObject, null);
        if ((((Entry)localObject).readable | paramBoolean))
        {
          Entry.access$702((Entry)localObject, true);
          this.journalWriter.append("CLEAN");
          this.journalWriter.append(' ');
          this.journalWriter.append(((Entry)localObject).key);
          this.journalWriter.append(((Entry)localObject).getLengths());
          this.journalWriter.append('\n');
          if (paramBoolean)
          {
            l1 = this.nextSequenceNumber;
            this.nextSequenceNumber = (1L + l1);
            Entry.access$1302((Entry)localObject, l1);
          }
        }
        else
        {
          this.lruEntries.remove(((Entry)localObject).key);
          this.journalWriter.append("REMOVE");
          this.journalWriter.append(' ');
          this.journalWriter.append(((Entry)localObject).key);
          this.journalWriter.append('\n');
        }
        this.journalWriter.flush();
        if ((this.size > this.maxSize) || (journalRebuildRequired())) {
          this.executorService.submit(this.cleanupCallable);
        }
        return;
      }
      paramEditor = new java/lang/IllegalStateException;
      paramEditor.<init>();
      throw paramEditor;
    }
    finally {}
  }
  
  private static void deleteIfExists(File paramFile)
    throws IOException
  {
    if ((paramFile.exists()) && (!paramFile.delete())) {
      throw new IOException();
    }
  }
  
  private Editor edit(String paramString, long paramLong)
    throws IOException
  {
    try
    {
      checkNotClosed();
      Entry localEntry = (Entry)this.lruEntries.get(paramString);
      if (paramLong != -1L) {
        if (localEntry != null)
        {
          long l = localEntry.sequenceNumber;
          if (l == paramLong) {}
        }
        else
        {
          return null;
        }
      }
      if (localEntry == null)
      {
        localEntry = new com/bumptech/glide/disklrucache/DiskLruCache$Entry;
        localEntry.<init>(this, paramString, null);
        this.lruEntries.put(paramString, localEntry);
      }
      else
      {
        localEditor = localEntry.currentEditor;
        if (localEditor != null) {
          return null;
        }
      }
      Editor localEditor = new com/bumptech/glide/disklrucache/DiskLruCache$Editor;
      localEditor.<init>(this, localEntry, null);
      Entry.access$802(localEntry, localEditor);
      this.journalWriter.append("DIRTY");
      this.journalWriter.append(' ');
      this.journalWriter.append(paramString);
      this.journalWriter.append('\n');
      this.journalWriter.flush();
      return localEditor;
    }
    finally {}
  }
  
  private static String inputStreamToString(InputStream paramInputStream)
    throws IOException
  {
    return Util.readFully(new InputStreamReader(paramInputStream, Util.UTF_8));
  }
  
  private boolean journalRebuildRequired()
  {
    int i = this.redundantOpCount;
    boolean bool;
    if ((i >= 2000) && (i >= this.lruEntries.size())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public static DiskLruCache open(File paramFile, int paramInt1, int paramInt2, long paramLong)
    throws IOException
  {
    if (paramLong > 0L)
    {
      if (paramInt2 > 0)
      {
        Object localObject1 = new File(paramFile, "journal.bkp");
        if (((File)localObject1).exists())
        {
          localObject2 = new File(paramFile, "journal");
          if (((File)localObject2).exists()) {
            ((File)localObject1).delete();
          } else {
            renameTo((File)localObject1, (File)localObject2, false);
          }
        }
        Object localObject2 = new DiskLruCache(paramFile, paramInt1, paramInt2, paramLong);
        if (((DiskLruCache)localObject2).journalFile.exists()) {
          try
          {
            ((DiskLruCache)localObject2).readJournal();
            ((DiskLruCache)localObject2).processJournal();
            return (DiskLruCache)localObject2;
          }
          catch (IOException localIOException)
          {
            PrintStream localPrintStream = System.out;
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append("DiskLruCache ");
            ((StringBuilder)localObject1).append(paramFile);
            ((StringBuilder)localObject1).append(" is corrupt: ");
            ((StringBuilder)localObject1).append(localIOException.getMessage());
            ((StringBuilder)localObject1).append(", removing");
            localPrintStream.println(((StringBuilder)localObject1).toString());
            ((DiskLruCache)localObject2).delete();
          }
        }
        paramFile.mkdirs();
        paramFile = new DiskLruCache(paramFile, paramInt1, paramInt2, paramLong);
        paramFile.rebuildJournal();
        return paramFile;
      }
      throw new IllegalArgumentException("valueCount <= 0");
    }
    throw new IllegalArgumentException("maxSize <= 0");
  }
  
  private void processJournal()
    throws IOException
  {
    deleteIfExists(this.journalFileTmp);
    Iterator localIterator = this.lruEntries.values().iterator();
    while (localIterator.hasNext())
    {
      Entry localEntry = (Entry)localIterator.next();
      Editor localEditor = localEntry.currentEditor;
      int j = 0;
      int i = 0;
      if (localEditor == null)
      {
        while (i < this.valueCount)
        {
          this.size += localEntry.lengths[i];
          i++;
        }
      }
      else
      {
        Entry.access$802(localEntry, null);
        for (i = j; i < this.valueCount; i++)
        {
          deleteIfExists(localEntry.getCleanFile(i));
          deleteIfExists(localEntry.getDirtyFile(i));
        }
        localIterator.remove();
      }
    }
  }
  
  private void readJournal()
    throws IOException
  {
    StrictLineReader localStrictLineReader = new StrictLineReader(new FileInputStream(this.journalFile), Util.US_ASCII);
    try
    {
      Object localObject2 = localStrictLineReader.readLine();
      Object localObject3 = localStrictLineReader.readLine();
      Object localObject4 = localStrictLineReader.readLine();
      String str2 = localStrictLineReader.readLine();
      String str1 = localStrictLineReader.readLine();
      BufferedWriter localBufferedWriter;
      if (("libcore.io.DiskLruCache".equals(localObject2)) && ("1".equals(localObject3)) && (Integer.toString(this.appVersion).equals(localObject4)) && (Integer.toString(this.valueCount).equals(str2)))
      {
        boolean bool = "".equals(str1);
        if (bool)
        {
          int i = 0;
          try
          {
            for (;;)
            {
              readJournalLine(localStrictLineReader.readLine());
              i++;
            }
            localIOException = new java/io/IOException;
          }
          catch (EOFException localEOFException)
          {
            this.redundantOpCount = (i - this.lruEntries.size());
            if (localStrictLineReader.hasUnterminatedLine())
            {
              rebuildJournal();
            }
            else
            {
              localBufferedWriter = new java/io/BufferedWriter;
              localObject2 = new java/io/OutputStreamWriter;
              localObject3 = new java/io/FileOutputStream;
              ((FileOutputStream)localObject3).<init>(this.journalFile, true);
              ((OutputStreamWriter)localObject2).<init>((OutputStream)localObject3, Util.US_ASCII);
              localBufferedWriter.<init>((Writer)localObject2);
              this.journalWriter = localBufferedWriter;
            }
            return;
          }
        }
      }
      IOException localIOException;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      ((StringBuilder)localObject4).append("unexpected journal header: [");
      ((StringBuilder)localObject4).append((String)localObject2);
      ((StringBuilder)localObject4).append(", ");
      ((StringBuilder)localObject4).append((String)localObject3);
      ((StringBuilder)localObject4).append(", ");
      ((StringBuilder)localObject4).append(str2);
      ((StringBuilder)localObject4).append(", ");
      ((StringBuilder)localObject4).append(localBufferedWriter);
      ((StringBuilder)localObject4).append("]");
      localIOException.<init>(((StringBuilder)localObject4).toString());
      throw localIOException;
    }
    finally
    {
      Util.closeQuietly(localStrictLineReader);
    }
  }
  
  private void readJournalLine(String paramString)
    throws IOException
  {
    int i = paramString.indexOf(' ');
    if (i != -1)
    {
      int j = i + 1;
      int k = paramString.indexOf(' ', j);
      if (k == -1)
      {
        localObject2 = paramString.substring(j);
        localObject1 = localObject2;
        if (i == 6)
        {
          localObject1 = localObject2;
          if (paramString.startsWith("REMOVE")) {
            this.lruEntries.remove(localObject2);
          }
        }
      }
      else
      {
        localObject1 = paramString.substring(j, k);
      }
      Entry localEntry = (Entry)this.lruEntries.get(localObject1);
      Object localObject2 = localEntry;
      if (localEntry == null)
      {
        localObject2 = new Entry((String)localObject1, null);
        this.lruEntries.put(localObject1, localObject2);
      }
      if ((k != -1) && (i == 5) && (paramString.startsWith("CLEAN")))
      {
        paramString = paramString.substring(k + 1).split(" ");
        Entry.access$702((Entry)localObject2, true);
        Entry.access$802((Entry)localObject2, null);
        ((Entry)localObject2).setLengths(paramString);
      }
      else if ((k == -1) && (i == 5) && (paramString.startsWith("DIRTY")))
      {
        Entry.access$802((Entry)localObject2, new Editor((Entry)localObject2, null));
      }
      else
      {
        if ((k != -1) || (i != 4) || (!paramString.startsWith("READ"))) {
          break label248;
        }
      }
      return;
      label248:
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("unexpected journal line: ");
      ((StringBuilder)localObject1).append(paramString);
      throw new IOException(((StringBuilder)localObject1).toString());
    }
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("unexpected journal line: ");
    ((StringBuilder)localObject1).append(paramString);
    throw new IOException(((StringBuilder)localObject1).toString());
  }
  
  /* Error */
  private void rebuildJournal()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 137	com/bumptech/glide/disklrucache/DiskLruCache:journalWriter	Ljava/io/Writer;
    //   6: ifnull +10 -> 16
    //   9: aload_0
    //   10: getfield 137	com/bumptech/glide/disklrucache/DiskLruCache:journalWriter	Ljava/io/Writer;
    //   13: invokevirtual 489	java/io/Writer:close	()V
    //   16: new 433	java/io/BufferedWriter
    //   19: astore_1
    //   20: new 435	java/io/OutputStreamWriter
    //   23: astore_2
    //   24: new 437	java/io/FileOutputStream
    //   27: astore_3
    //   28: aload_3
    //   29: aload_0
    //   30: getfield 126	com/bumptech/glide/disklrucache/DiskLruCache:journalFileTmp	Ljava/io/File;
    //   33: invokespecial 490	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   36: aload_2
    //   37: aload_3
    //   38: getstatic 408	com/bumptech/glide/disklrucache/Util:US_ASCII	Ljava/nio/charset/Charset;
    //   41: invokespecial 443	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   44: aload_1
    //   45: aload_2
    //   46: invokespecial 446	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   49: aload_1
    //   50: ldc 41
    //   52: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   55: aload_1
    //   56: ldc_w 495
    //   59: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   62: aload_1
    //   63: ldc 48
    //   65: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   68: aload_1
    //   69: ldc_w 495
    //   72: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   75: aload_1
    //   76: aload_0
    //   77: getfield 117	com/bumptech/glide/disklrucache/DiskLruCache:appVersion	I
    //   80: invokestatic 423	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   83: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   86: aload_1
    //   87: ldc_w 495
    //   90: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   93: aload_1
    //   94: aload_0
    //   95: getfield 130	com/bumptech/glide/disklrucache/DiskLruCache:valueCount	I
    //   98: invokestatic 423	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   101: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   104: aload_1
    //   105: ldc_w 495
    //   108: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   111: aload_1
    //   112: ldc_w 495
    //   115: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   118: aload_0
    //   119: getfield 85	com/bumptech/glide/disklrucache/DiskLruCache:lruEntries	Ljava/util/LinkedHashMap;
    //   122: invokevirtual 380	java/util/LinkedHashMap:values	()Ljava/util/Collection;
    //   125: invokeinterface 386 1 0
    //   130: astore_2
    //   131: aload_2
    //   132: invokeinterface 391 1 0
    //   137: ifeq +128 -> 265
    //   140: aload_2
    //   141: invokeinterface 395 1 0
    //   146: checkcast 16	com/bumptech/glide/disklrucache/DiskLruCache$Entry
    //   149: astore_3
    //   150: aload_3
    //   151: invokestatic 195	com/bumptech/glide/disklrucache/DiskLruCache$Entry:access$800	(Lcom/bumptech/glide/disklrucache/DiskLruCache$Entry;)Lcom/bumptech/glide/disklrucache/DiskLruCache$Editor;
    //   154: ifnull +52 -> 206
    //   157: new 215	java/lang/StringBuilder
    //   160: astore 4
    //   162: aload 4
    //   164: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   167: aload 4
    //   169: ldc_w 497
    //   172: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: pop
    //   176: aload 4
    //   178: aload_3
    //   179: invokestatic 268	com/bumptech/glide/disklrucache/DiskLruCache$Entry:access$1200	(Lcom/bumptech/glide/disklrucache/DiskLruCache$Entry;)Ljava/lang/String;
    //   182: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: aload 4
    //   188: bipush 10
    //   190: invokevirtual 500	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: aload_1
    //   195: aload 4
    //   197: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   200: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   203: goto -72 -> 131
    //   206: new 215	java/lang/StringBuilder
    //   209: astore 4
    //   211: aload 4
    //   213: invokespecial 216	java/lang/StringBuilder:<init>	()V
    //   216: aload 4
    //   218: ldc_w 502
    //   221: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: aload 4
    //   227: aload_3
    //   228: invokestatic 268	com/bumptech/glide/disklrucache/DiskLruCache$Entry:access$1200	(Lcom/bumptech/glide/disklrucache/DiskLruCache$Entry;)Ljava/lang/String;
    //   231: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload 4
    //   237: aload_3
    //   238: invokevirtual 271	com/bumptech/glide/disklrucache/DiskLruCache$Entry:getLengths	()Ljava/lang/String;
    //   241: invokevirtual 222	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload 4
    //   247: bipush 10
    //   249: invokevirtual 500	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: aload_1
    //   254: aload 4
    //   256: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   259: invokevirtual 493	java/io/Writer:write	(Ljava/lang/String;)V
    //   262: goto -131 -> 131
    //   265: aload_1
    //   266: invokevirtual 489	java/io/Writer:close	()V
    //   269: aload_0
    //   270: getfield 124	com/bumptech/glide/disklrucache/DiskLruCache:journalFile	Ljava/io/File;
    //   273: invokevirtual 210	java/io/File:exists	()Z
    //   276: ifeq +15 -> 291
    //   279: aload_0
    //   280: getfield 124	com/bumptech/glide/disklrucache/DiskLruCache:journalFile	Ljava/io/File;
    //   283: aload_0
    //   284: getfield 128	com/bumptech/glide/disklrucache/DiskLruCache:journalFileBackup	Ljava/io/File;
    //   287: iconst_1
    //   288: invokestatic 333	com/bumptech/glide/disklrucache/DiskLruCache:renameTo	(Ljava/io/File;Ljava/io/File;Z)V
    //   291: aload_0
    //   292: getfield 126	com/bumptech/glide/disklrucache/DiskLruCache:journalFileTmp	Ljava/io/File;
    //   295: aload_0
    //   296: getfield 124	com/bumptech/glide/disklrucache/DiskLruCache:journalFile	Ljava/io/File;
    //   299: iconst_0
    //   300: invokestatic 333	com/bumptech/glide/disklrucache/DiskLruCache:renameTo	(Ljava/io/File;Ljava/io/File;Z)V
    //   303: aload_0
    //   304: getfield 128	com/bumptech/glide/disklrucache/DiskLruCache:journalFileBackup	Ljava/io/File;
    //   307: invokevirtual 290	java/io/File:delete	()Z
    //   310: pop
    //   311: new 433	java/io/BufferedWriter
    //   314: astore_2
    //   315: new 435	java/io/OutputStreamWriter
    //   318: astore_3
    //   319: new 437	java/io/FileOutputStream
    //   322: astore_1
    //   323: aload_1
    //   324: aload_0
    //   325: getfield 124	com/bumptech/glide/disklrucache/DiskLruCache:journalFile	Ljava/io/File;
    //   328: iconst_1
    //   329: invokespecial 440	java/io/FileOutputStream:<init>	(Ljava/io/File;Z)V
    //   332: aload_3
    //   333: aload_1
    //   334: getstatic 408	com/bumptech/glide/disklrucache/Util:US_ASCII	Ljava/nio/charset/Charset;
    //   337: invokespecial 443	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   340: aload_2
    //   341: aload_3
    //   342: invokespecial 446	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   345: aload_0
    //   346: aload_2
    //   347: putfield 137	com/bumptech/glide/disklrucache/DiskLruCache:journalWriter	Ljava/io/Writer;
    //   350: aload_0
    //   351: monitorexit
    //   352: return
    //   353: astore_2
    //   354: aload_1
    //   355: invokevirtual 489	java/io/Writer:close	()V
    //   358: aload_2
    //   359: athrow
    //   360: astore_1
    //   361: aload_0
    //   362: monitorexit
    //   363: aload_1
    //   364: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	365	0	this	DiskLruCache
    //   19	336	1	localObject1	Object
    //   360	4	1	localObject2	Object
    //   23	324	2	localObject3	Object
    //   353	6	2	localObject4	Object
    //   27	315	3	localObject5	Object
    //   160	95	4	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   49	131	353	finally
    //   131	203	353	finally
    //   206	262	353	finally
    //   2	16	360	finally
    //   16	49	360	finally
    //   265	291	360	finally
    //   291	350	360	finally
    //   354	360	360	finally
  }
  
  private static void renameTo(File paramFile1, File paramFile2, boolean paramBoolean)
    throws IOException
  {
    if (paramBoolean) {
      deleteIfExists(paramFile2);
    }
    if (paramFile1.renameTo(paramFile2)) {
      return;
    }
    throw new IOException();
  }
  
  private void trimToSize()
    throws IOException
  {
    while (this.size > this.maxSize) {
      remove((String)((Map.Entry)this.lruEntries.entrySet().iterator().next()).getKey());
    }
  }
  
  public void close()
    throws IOException
  {
    try
    {
      Object localObject1 = this.journalWriter;
      if (localObject1 == null) {
        return;
      }
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>(this.lruEntries.values());
      Iterator localIterator = ((ArrayList)localObject1).iterator();
      while (localIterator.hasNext())
      {
        localObject1 = (Entry)localIterator.next();
        if (((Entry)localObject1).currentEditor != null) {
          ((Entry)localObject1).currentEditor.abort();
        }
      }
      trimToSize();
      this.journalWriter.close();
      this.journalWriter = null;
      return;
    }
    finally {}
  }
  
  public void delete()
    throws IOException
  {
    close();
    Util.deleteContents(this.directory);
  }
  
  public Editor edit(String paramString)
    throws IOException
  {
    return edit(paramString, -1L);
  }
  
  public void flush()
    throws IOException
  {
    try
    {
      checkNotClosed();
      trimToSize();
      this.journalWriter.flush();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public Value get(String paramString)
    throws IOException
  {
    try
    {
      checkNotClosed();
      Entry localEntry = (Entry)this.lruEntries.get(paramString);
      if (localEntry == null) {
        return null;
      }
      boolean bool = localEntry.readable;
      if (!bool) {
        return null;
      }
      File[] arrayOfFile = localEntry.cleanFiles;
      int j = arrayOfFile.length;
      for (int i = 0; i < j; i++)
      {
        bool = arrayOfFile[i].exists();
        if (!bool) {
          return null;
        }
      }
      this.redundantOpCount += 1;
      this.journalWriter.append("READ");
      this.journalWriter.append(' ');
      this.journalWriter.append(paramString);
      this.journalWriter.append('\n');
      if (journalRebuildRequired()) {
        this.executorService.submit(this.cleanupCallable);
      }
      paramString = new Value(paramString, localEntry.sequenceNumber, localEntry.cleanFiles, localEntry.lengths, null);
      return paramString;
    }
    finally {}
  }
  
  public File getDirectory()
  {
    return this.directory;
  }
  
  public long getMaxSize()
  {
    try
    {
      long l = this.maxSize;
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean isClosed()
  {
    try
    {
      Writer localWriter = this.journalWriter;
      boolean bool;
      if (localWriter == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean remove(String paramString)
    throws IOException
  {
    try
    {
      checkNotClosed();
      Object localObject = (Entry)this.lruEntries.get(paramString);
      int i = 0;
      if ((localObject != null) && (((Entry)localObject).currentEditor == null))
      {
        while (i < this.valueCount)
        {
          File localFile = ((Entry)localObject).getCleanFile(i);
          if ((localFile.exists()) && (!localFile.delete()))
          {
            paramString = new java/io/IOException;
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append("failed to delete ");
            ((StringBuilder)localObject).append(localFile);
            paramString.<init>(((StringBuilder)localObject).toString());
            throw paramString;
          }
          this.size -= localObject.lengths[i];
          ((Entry)localObject).lengths[i] = 0L;
          i++;
        }
        this.redundantOpCount += 1;
        this.journalWriter.append("REMOVE");
        this.journalWriter.append(' ');
        this.journalWriter.append(paramString);
        this.journalWriter.append('\n');
        this.lruEntries.remove(paramString);
        if (journalRebuildRequired()) {
          this.executorService.submit(this.cleanupCallable);
        }
        return true;
      }
      return false;
    }
    finally {}
  }
  
  public void setMaxSize(long paramLong)
  {
    try
    {
      this.maxSize = paramLong;
      this.executorService.submit(this.cleanupCallable);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public long size()
  {
    try
    {
      long l = this.size;
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private static final class DiskLruCacheThreadFactory
    implements ThreadFactory
  {
    public Thread newThread(Runnable paramRunnable)
    {
      try
      {
        Thread localThread = new java/lang/Thread;
        localThread.<init>(paramRunnable, "glide-disk-lru-cache-thread");
        localThread.setPriority(1);
        return localThread;
      }
      finally
      {
        paramRunnable = finally;
        throw paramRunnable;
      }
    }
  }
  
  public final class Editor
  {
    private boolean committed;
    private final DiskLruCache.Entry entry;
    private final boolean[] written;
    
    private Editor(DiskLruCache.Entry paramEntry)
    {
      this.entry = paramEntry;
      if (DiskLruCache.Entry.access$700(paramEntry)) {
        this$1 = null;
      } else {
        this$1 = new boolean[DiskLruCache.this.valueCount];
      }
      this.written = DiskLruCache.this;
    }
    
    private InputStream newInputStream(int paramInt)
      throws IOException
    {
      synchronized (DiskLruCache.this)
      {
        if (DiskLruCache.Entry.access$800(this.entry) == this)
        {
          if (!DiskLruCache.Entry.access$700(this.entry)) {
            return null;
          }
          try
          {
            FileInputStream localFileInputStream = new java/io/FileInputStream;
            localFileInputStream.<init>(this.entry.getCleanFile(paramInt));
            return localFileInputStream;
          }
          catch (FileNotFoundException localFileNotFoundException)
          {
            return null;
          }
        }
        IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
        localIllegalStateException.<init>();
        throw localIllegalStateException;
      }
    }
    
    public void abort()
      throws IOException
    {
      DiskLruCache.this.completeEdit(this, false);
    }
    
    public void abortUnlessCommitted()
    {
      if (!this.committed) {}
      try
      {
        abort();
        return;
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
    
    public void commit()
      throws IOException
    {
      DiskLruCache.this.completeEdit(this, true);
      this.committed = true;
    }
    
    public File getFile(int paramInt)
      throws IOException
    {
      synchronized (DiskLruCache.this)
      {
        if (DiskLruCache.Entry.access$800(this.entry) == this)
        {
          if (!DiskLruCache.Entry.access$700(this.entry)) {
            this.written[paramInt] = true;
          }
          localObject1 = this.entry.getDirtyFile(paramInt);
          if (!DiskLruCache.this.directory.exists()) {
            DiskLruCache.this.directory.mkdirs();
          }
          return (File)localObject1;
        }
        Object localObject1 = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    
    public String getString(int paramInt)
      throws IOException
    {
      Object localObject = newInputStream(paramInt);
      if (localObject != null) {
        localObject = DiskLruCache.inputStreamToString((InputStream)localObject);
      } else {
        localObject = null;
      }
      return (String)localObject;
    }
    
    /* Error */
    public void set(int paramInt, String paramString)
      throws IOException
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore_3
      //   2: new 107	java/io/FileOutputStream
      //   5: astore 5
      //   7: aload 5
      //   9: aload_0
      //   10: iload_1
      //   11: invokevirtual 109	com/bumptech/glide/disklrucache/DiskLruCache$Editor:getFile	(I)Ljava/io/File;
      //   14: invokespecial 110	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
      //   17: new 112	java/io/OutputStreamWriter
      //   20: astore 4
      //   22: aload 4
      //   24: aload 5
      //   26: getstatic 118	com/bumptech/glide/disklrucache/Util:UTF_8	Ljava/nio/charset/Charset;
      //   29: invokespecial 121	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
      //   32: aload 4
      //   34: aload_2
      //   35: invokevirtual 127	java/io/Writer:write	(Ljava/lang/String;)V
      //   38: aload 4
      //   40: invokestatic 131	com/bumptech/glide/disklrucache/Util:closeQuietly	(Ljava/io/Closeable;)V
      //   43: return
      //   44: astore_2
      //   45: aload 4
      //   47: astore_3
      //   48: goto +4 -> 52
      //   51: astore_2
      //   52: aload_3
      //   53: invokestatic 131	com/bumptech/glide/disklrucache/Util:closeQuietly	(Ljava/io/Closeable;)V
      //   56: aload_2
      //   57: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	58	0	this	Editor
      //   0	58	1	paramInt	int
      //   0	58	2	paramString	String
      //   1	52	3	localObject	Object
      //   20	26	4	localOutputStreamWriter	OutputStreamWriter
      //   5	20	5	localFileOutputStream	FileOutputStream
      // Exception table:
      //   from	to	target	type
      //   32	38	44	finally
      //   2	32	51	finally
    }
  }
  
  private final class Entry
  {
    File[] cleanFiles;
    private DiskLruCache.Editor currentEditor;
    File[] dirtyFiles;
    private final String key;
    private final long[] lengths;
    private boolean readable;
    private long sequenceNumber;
    
    private Entry(String paramString)
    {
      this.key = paramString;
      this.lengths = new long[DiskLruCache.this.valueCount];
      this.cleanFiles = new File[DiskLruCache.this.valueCount];
      this.dirtyFiles = new File[DiskLruCache.this.valueCount];
      paramString = new StringBuilder(paramString);
      paramString.append('.');
      int j = paramString.length();
      for (int i = 0; i < DiskLruCache.this.valueCount; i++)
      {
        paramString.append(i);
        this.cleanFiles[i] = new File(DiskLruCache.this.directory, paramString.toString());
        paramString.append(".tmp");
        this.dirtyFiles[i] = new File(DiskLruCache.this.directory, paramString.toString());
        paramString.setLength(j);
      }
    }
    
    private IOException invalidLengths(String[] paramArrayOfString)
      throws IOException
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("unexpected journal line: ");
      localStringBuilder.append(Arrays.toString(paramArrayOfString));
      throw new IOException(localStringBuilder.toString());
    }
    
    private void setLengths(String[] paramArrayOfString)
      throws IOException
    {
      if (paramArrayOfString.length == DiskLruCache.this.valueCount)
      {
        int i = 0;
        try
        {
          while (i < paramArrayOfString.length)
          {
            this.lengths[i] = Long.parseLong(paramArrayOfString[i]);
            i++;
          }
          return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          throw invalidLengths(paramArrayOfString);
        }
      }
      throw invalidLengths(paramArrayOfString);
    }
    
    public File getCleanFile(int paramInt)
    {
      return this.cleanFiles[paramInt];
    }
    
    public File getDirtyFile(int paramInt)
    {
      return this.dirtyFiles[paramInt];
    }
    
    public String getLengths()
      throws IOException
    {
      StringBuilder localStringBuilder = new StringBuilder();
      for (long l : this.lengths)
      {
        localStringBuilder.append(' ');
        localStringBuilder.append(l);
      }
      return localStringBuilder.toString();
    }
  }
  
  public final class Value
  {
    private final File[] files;
    private final String key;
    private final long[] lengths;
    private final long sequenceNumber;
    
    private Value(String paramString, long paramLong, File[] paramArrayOfFile, long[] paramArrayOfLong)
    {
      this.key = paramString;
      this.sequenceNumber = paramLong;
      this.files = paramArrayOfFile;
      this.lengths = paramArrayOfLong;
    }
    
    public DiskLruCache.Editor edit()
      throws IOException
    {
      return DiskLruCache.this.edit(this.key, this.sequenceNumber);
    }
    
    public File getFile(int paramInt)
    {
      return this.files[paramInt];
    }
    
    public long getLength(int paramInt)
    {
      return this.lengths[paramInt];
    }
    
    public String getString(int paramInt)
      throws IOException
    {
      return DiskLruCache.inputStreamToString(new FileInputStream(this.files[paramInt]));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/bumptech/glide/disklrucache/DiskLruCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */