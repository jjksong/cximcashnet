package com.android.volley;

public class NoConnectionError
  extends NetworkError
{
  public NoConnectionError() {}
  
  public NoConnectionError(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/NoConnectionError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */