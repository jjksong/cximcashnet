package com.android.volley.toolbox;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.android.volley.VolleyError;

public class NetworkImageView
  extends ImageView
{
  private int mDefaultImageId;
  private int mErrorImageId;
  private ImageLoader.ImageContainer mImageContainer;
  private ImageLoader mImageLoader;
  private String mUrl;
  
  public NetworkImageView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void setDefaultImageOrNull()
  {
    int i = this.mDefaultImageId;
    if (i != 0) {
      setImageResource(i);
    } else {
      setImageBitmap(null);
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    invalidate();
  }
  
  public String getImageURL()
  {
    return this.mUrl;
  }
  
  void loadImageIfNecessary(final boolean paramBoolean)
  {
    int n = getWidth();
    int m = getHeight();
    Object localObject1 = getScaleType();
    Object localObject2 = getLayoutParams();
    int k = 1;
    int i;
    int j;
    if (localObject2 != null)
    {
      if (getLayoutParams().width == -2) {
        i = 1;
      } else {
        i = 0;
      }
      int i1;
      if (getLayoutParams().height == -2)
      {
        i1 = 1;
        j = i;
        i = i1;
      }
      else
      {
        i1 = 0;
        j = i;
        i = i1;
      }
    }
    else
    {
      j = 0;
      i = 0;
    }
    if ((j == 0) || (i == 0)) {
      k = 0;
    }
    if ((n == 0) && (m == 0) && (k == 0)) {
      return;
    }
    if (TextUtils.isEmpty(this.mUrl))
    {
      localObject1 = this.mImageContainer;
      if (localObject1 != null)
      {
        ((ImageLoader.ImageContainer)localObject1).cancelRequest();
        this.mImageContainer = null;
      }
      setDefaultImageOrNull();
      return;
    }
    localObject2 = this.mImageContainer;
    if ((localObject2 != null) && (((ImageLoader.ImageContainer)localObject2).getRequestUrl() != null))
    {
      if (this.mImageContainer.getRequestUrl().equals(this.mUrl)) {
        return;
      }
      this.mImageContainer.cancelRequest();
      setDefaultImageOrNull();
    }
    k = n;
    if (j != 0) {
      k = 0;
    }
    if (i != 0) {
      i = 0;
    } else {
      i = m;
    }
    this.mImageContainer = this.mImageLoader.get(this.mUrl, new ImageLoader.ImageListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (NetworkImageView.this.mErrorImageId != 0)
        {
          paramAnonymousVolleyError = NetworkImageView.this;
          paramAnonymousVolleyError.setImageResource(paramAnonymousVolleyError.mErrorImageId);
        }
      }
      
      public void onResponse(final ImageLoader.ImageContainer paramAnonymousImageContainer, boolean paramAnonymousBoolean)
      {
        if ((paramAnonymousBoolean) && (paramBoolean))
        {
          NetworkImageView.this.post(new Runnable()
          {
            public void run()
            {
              NetworkImageView.1.this.onResponse(paramAnonymousImageContainer, false);
            }
          });
          return;
        }
        if (paramAnonymousImageContainer.getBitmap() != null)
        {
          NetworkImageView.this.setImageBitmap(paramAnonymousImageContainer.getBitmap());
        }
        else if (NetworkImageView.this.mDefaultImageId != 0)
        {
          paramAnonymousImageContainer = NetworkImageView.this;
          paramAnonymousImageContainer.setImageResource(paramAnonymousImageContainer.mDefaultImageId);
        }
      }
    }, k, i, (ImageView.ScaleType)localObject1);
  }
  
  protected void onDetachedFromWindow()
  {
    ImageLoader.ImageContainer localImageContainer = this.mImageContainer;
    if (localImageContainer != null)
    {
      localImageContainer.cancelRequest();
      setImageBitmap(null);
      this.mImageContainer = null;
    }
    super.onDetachedFromWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    loadImageIfNecessary(true);
  }
  
  public void setDefaultImageResId(int paramInt)
  {
    this.mDefaultImageId = paramInt;
  }
  
  public void setErrorImageResId(int paramInt)
  {
    this.mErrorImageId = paramInt;
  }
  
  public void setImageUrl(String paramString, ImageLoader paramImageLoader)
  {
    this.mUrl = paramString;
    this.mImageLoader = paramImageLoader;
    loadImageIfNecessary(false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/NetworkImageView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */