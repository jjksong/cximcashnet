package com.android.volley.toolbox;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestFuture<T>
  implements Future<T>, Response.Listener<T>, Response.ErrorListener
{
  private VolleyError mException;
  private Request<?> mRequest;
  private T mResult;
  private boolean mResultReceived = false;
  
  private T doGet(Long paramLong)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    try
    {
      if (this.mException == null)
      {
        if (this.mResultReceived)
        {
          paramLong = this.mResult;
          return paramLong;
        }
        if (paramLong == null) {
          wait(0L);
        } else if (paramLong.longValue() > 0L) {
          wait(paramLong.longValue());
        }
        if (this.mException == null)
        {
          if (this.mResultReceived)
          {
            paramLong = this.mResult;
            return paramLong;
          }
          paramLong = new java/util/concurrent/TimeoutException;
          paramLong.<init>();
          throw paramLong;
        }
        paramLong = new java/util/concurrent/ExecutionException;
        paramLong.<init>(this.mException);
        throw paramLong;
      }
      paramLong = new java/util/concurrent/ExecutionException;
      paramLong.<init>(this.mException);
      throw paramLong;
    }
    finally {}
  }
  
  public static <E> RequestFuture<E> newFuture()
  {
    return new RequestFuture();
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    try
    {
      Request localRequest = this.mRequest;
      if (localRequest == null) {
        return false;
      }
      if (!isDone())
      {
        this.mRequest.cancel();
        return true;
      }
      return false;
    }
    finally {}
  }
  
  public T get()
    throws InterruptedException, ExecutionException
  {
    try
    {
      Object localObject = doGet(null);
      return (T)localObject;
    }
    catch (TimeoutException localTimeoutException)
    {
      throw new AssertionError(localTimeoutException);
    }
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    return (T)doGet(Long.valueOf(TimeUnit.MILLISECONDS.convert(paramLong, paramTimeUnit)));
  }
  
  public boolean isCancelled()
  {
    Request localRequest = this.mRequest;
    if (localRequest == null) {
      return false;
    }
    return localRequest.isCanceled();
  }
  
  public boolean isDone()
  {
    try
    {
      if ((!this.mResultReceived) && (this.mException == null))
      {
        bool = isCancelled();
        if (!bool)
        {
          bool = false;
          break label35;
        }
      }
      boolean bool = true;
      label35:
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void onErrorResponse(VolleyError paramVolleyError)
  {
    try
    {
      this.mException = paramVolleyError;
      notifyAll();
      return;
    }
    finally
    {
      paramVolleyError = finally;
      throw paramVolleyError;
    }
  }
  
  public void onResponse(T paramT)
  {
    try
    {
      this.mResultReceived = true;
      this.mResult = paramT;
      notifyAll();
      return;
    }
    finally
    {
      paramT = finally;
      throw paramT;
    }
  }
  
  public void setRequest(Request<?> paramRequest)
  {
    this.mRequest = paramRequest;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/RequestFuture.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */