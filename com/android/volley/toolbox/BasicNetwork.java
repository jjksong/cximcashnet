package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache.Entry;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.RedirectError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork
  implements Network
{
  protected static final boolean DEBUG = VolleyLog.DEBUG;
  private static int DEFAULT_POOL_SIZE = 4096;
  private static int SLOW_REQUEST_THRESHOLD_MS = 3000;
  protected final HttpStack mHttpStack;
  protected final ByteArrayPool mPool;
  
  public BasicNetwork(HttpStack paramHttpStack)
  {
    this(paramHttpStack, new ByteArrayPool(DEFAULT_POOL_SIZE));
  }
  
  public BasicNetwork(HttpStack paramHttpStack, ByteArrayPool paramByteArrayPool)
  {
    this.mHttpStack = paramHttpStack;
    this.mPool = paramByteArrayPool;
  }
  
  private void addCacheHeaders(Map<String, String> paramMap, Cache.Entry paramEntry)
  {
    if (paramEntry == null) {
      return;
    }
    if (paramEntry.etag != null) {
      paramMap.put("If-None-Match", paramEntry.etag);
    }
    if (paramEntry.lastModified > 0L) {
      paramMap.put("If-Modified-Since", DateUtils.formatDate(new Date(paramEntry.lastModified)));
    }
  }
  
  private static void attemptRetryOnException(String paramString, Request<?> paramRequest, VolleyError paramVolleyError)
    throws VolleyError
  {
    RetryPolicy localRetryPolicy = paramRequest.getRetryPolicy();
    int i = paramRequest.getTimeoutMs();
    try
    {
      localRetryPolicy.retry(paramVolleyError);
      paramRequest.addMarker(String.format("%s-retry [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      return;
    }
    catch (VolleyError paramVolleyError)
    {
      paramRequest.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      throw paramVolleyError;
    }
  }
  
  protected static Map<String, String> convertHeaders(Header[] paramArrayOfHeader)
  {
    TreeMap localTreeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
    for (int i = 0; i < paramArrayOfHeader.length; i++) {
      localTreeMap.put(paramArrayOfHeader[i].getName(), paramArrayOfHeader[i].getValue());
    }
    return localTreeMap;
  }
  
  private byte[] entityToBytes(HttpEntity paramHttpEntity)
    throws IOException, ServerError
  {
    PoolingByteArrayOutputStream localPoolingByteArrayOutputStream = new PoolingByteArrayOutputStream(this.mPool, (int)paramHttpEntity.getContentLength());
    byte[] arrayOfByte2 = null;
    byte[] arrayOfByte1 = arrayOfByte2;
    try
    {
      Object localObject2 = paramHttpEntity.getContent();
      if (localObject2 != null)
      {
        arrayOfByte1 = arrayOfByte2;
        arrayOfByte2 = this.mPool.getBuf(1024);
        for (;;)
        {
          arrayOfByte1 = arrayOfByte2;
          int i = ((InputStream)localObject2).read(arrayOfByte2);
          if (i == -1) {
            break;
          }
          arrayOfByte1 = arrayOfByte2;
          localPoolingByteArrayOutputStream.write(arrayOfByte2, 0, i);
        }
        arrayOfByte1 = arrayOfByte2;
        localObject2 = localPoolingByteArrayOutputStream.toByteArray();
        return (byte[])localObject2;
      }
      arrayOfByte1 = arrayOfByte2;
      localObject2 = new com/android/volley/ServerError;
      arrayOfByte1 = arrayOfByte2;
      ((ServerError)localObject2).<init>();
      arrayOfByte1 = arrayOfByte2;
      throw ((Throwable)localObject2);
    }
    finally
    {
      try
      {
        paramHttpEntity.consumeContent();
      }
      catch (IOException paramHttpEntity)
      {
        VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
      }
      this.mPool.returnBuf(arrayOfByte1);
      localPoolingByteArrayOutputStream.close();
    }
  }
  
  private void logSlowRequests(long paramLong, Request<?> paramRequest, byte[] paramArrayOfByte, StatusLine paramStatusLine)
  {
    if ((DEBUG) || (paramLong > SLOW_REQUEST_THRESHOLD_MS))
    {
      if (paramArrayOfByte != null) {
        paramArrayOfByte = Integer.valueOf(paramArrayOfByte.length);
      } else {
        paramArrayOfByte = "null";
      }
      VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", new Object[] { paramRequest, Long.valueOf(paramLong), paramArrayOfByte, Integer.valueOf(paramStatusLine.getStatusCode()), Integer.valueOf(paramRequest.getRetryPolicy().getCurrentRetryCount()) });
    }
  }
  
  protected void logError(String paramString1, String paramString2, long paramLong)
  {
    VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", new Object[] { paramString1, Long.valueOf(SystemClock.elapsedRealtime() - paramLong), paramString2 });
  }
  
  public NetworkResponse performRequest(Request<?> paramRequest)
    throws VolleyError
  {
    long l1 = SystemClock.elapsedRealtime();
    for (;;)
    {
      Object localObject1 = Collections.emptyMap();
      try
      {
        Object localObject7;
        Object localObject8;
        Object localObject6;
        try
        {
          Object localObject4 = new java/util/HashMap;
          ((HashMap)localObject4).<init>();
          addCacheHeaders((Map)localObject4, paramRequest.getCacheEntry());
          HttpResponse localHttpResponse = this.mHttpStack.performRequest(paramRequest, (Map)localObject4);
          try
          {
            localObject4 = localHttpResponse.getStatusLine();
            i = ((StatusLine)localObject4).getStatusCode();
            localObject7 = convertHeaders(localHttpResponse.getAllHeaders());
            if (i == 304) {
              try
              {
                localObject1 = paramRequest.getCacheEntry();
                if (localObject1 == null) {
                  return new NetworkResponse(304, null, (Map)localObject7, true, SystemClock.elapsedRealtime() - l1);
                }
                ((Cache.Entry)localObject1).responseHeaders.putAll((Map)localObject7);
                localObject1 = new NetworkResponse(304, ((Cache.Entry)localObject1).data, ((Cache.Entry)localObject1).responseHeaders, true, SystemClock.elapsedRealtime() - l1);
                return (NetworkResponse)localObject1;
              }
              catch (IOException localIOException1)
              {
                localObject4 = null;
                localObject8 = localHttpResponse;
              }
            }
            if ((i == 301) || (i == 302)) {}
            try
            {
              paramRequest.setRedirectUrl((String)((Map)localObject7).get("Location"));
              Object localObject2 = localHttpResponse.getEntity();
              if (localObject2 != null) {
                localObject2 = entityToBytes(localHttpResponse.getEntity());
              } else {
                localObject2 = new byte[0];
              }
              try
              {
                long l2 = SystemClock.elapsedRealtime();
                try
                {
                  logSlowRequests(l2 - l1, paramRequest, (byte[])localObject2, (StatusLine)localObject4);
                  if ((i >= 200) && (i <= 299)) {
                    return new NetworkResponse(i, (byte[])localObject2, (Map)localObject7, false, SystemClock.elapsedRealtime() - l1);
                  }
                  localObject4 = new java/io/IOException;
                  ((IOException)localObject4).<init>();
                  throw ((Throwable)localObject4);
                }
                catch (IOException localIOException3) {}
                localObject8 = localObject2;
              }
              catch (IOException localIOException4) {}
              localObject2 = localIOException4;
              localObject5 = localObject8;
            }
            catch (IOException localIOException2)
            {
              Object localObject5 = null;
            }
            localObject8 = localHttpResponse;
          }
          catch (IOException localIOException5)
          {
            localObject7 = localIOException2;
            Object localObject9 = null;
            localObject3 = localIOException5;
            localObject8 = localHttpResponse;
            localObject6 = localObject9;
          }
          if (localObject8 == null) {
            break label573;
          }
        }
        catch (IOException localIOException6)
        {
          localObject8 = null;
          localObject6 = localObject8;
          localObject7 = localObject3;
          localObject3 = localIOException6;
        }
        int i = ((HttpResponse)localObject8).getStatusLine().getStatusCode();
        if ((i != 301) && (i != 302)) {
          VolleyLog.e("Unexpected response code %d for %s", new Object[] { Integer.valueOf(i), paramRequest.getUrl() });
        } else {
          VolleyLog.e("Request at %s has been redirected to %s", new Object[] { paramRequest.getOriginUrl(), paramRequest.getUrl() });
        }
        if (localObject6 != null)
        {
          localObject3 = new NetworkResponse(i, (byte[])localObject6, (Map)localObject7, false, SystemClock.elapsedRealtime() - l1);
          if ((i != 401) && (i != 403))
          {
            if ((i != 301) && (i != 302)) {
              throw new ServerError((NetworkResponse)localObject3);
            }
            attemptRetryOnException("redirect", paramRequest, new RedirectError((NetworkResponse)localObject3));
          }
          else
          {
            attemptRetryOnException("auth", paramRequest, new AuthFailureError((NetworkResponse)localObject3));
          }
        }
        else
        {
          throw new NetworkError((Throwable)localObject3);
          label573:
          throw new NoConnectionError((Throwable)localObject3);
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        Object localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("Bad URL ");
        ((StringBuilder)localObject3).append(paramRequest.getUrl());
        throw new RuntimeException(((StringBuilder)localObject3).toString(), localMalformedURLException);
      }
      catch (ConnectTimeoutException localConnectTimeoutException)
      {
        attemptRetryOnException("connection", paramRequest, new TimeoutError());
      }
      catch (SocketTimeoutException localSocketTimeoutException)
      {
        attemptRetryOnException("socket", paramRequest, new TimeoutError());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/BasicNetwork.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */