package com.android.volley.toolbox;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import java.io.UnsupportedEncodingException;

public class StringRequest
  extends Request<String>
{
  private Response.Listener<String> mListener;
  
  public StringRequest(int paramInt, String paramString, Response.Listener<String> paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString, paramErrorListener);
    this.mListener = paramListener;
  }
  
  public StringRequest(String paramString, Response.Listener<String> paramListener, Response.ErrorListener paramErrorListener)
  {
    this(0, paramString, paramListener, paramErrorListener);
  }
  
  protected void deliverResponse(String paramString)
  {
    Response.Listener localListener = this.mListener;
    if (localListener != null) {
      localListener.onResponse(paramString);
    }
  }
  
  protected void onFinish()
  {
    super.onFinish();
    this.mListener = null;
  }
  
  protected Response<String> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    String str2;
    try
    {
      String str1 = new java/lang/String;
      str1.<init>(paramNetworkResponse.data, HttpHeaderParser.parseCharset(paramNetworkResponse.headers));
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str2 = new String(paramNetworkResponse.data);
    }
    return Response.success(str2, HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/StringRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */