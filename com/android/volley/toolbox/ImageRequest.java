package com.android.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.widget.ImageView.ScaleType;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;

public class ImageRequest
  extends Request<Bitmap>
{
  private static final float IMAGE_BACKOFF_MULT = 2.0F;
  private static final int IMAGE_MAX_RETRIES = 2;
  private static final int IMAGE_TIMEOUT_MS = 1000;
  private static final Object sDecodeLock = new Object();
  private final Bitmap.Config mDecodeConfig;
  private final Response.Listener<Bitmap> mListener;
  private final int mMaxHeight;
  private final int mMaxWidth;
  private ImageView.ScaleType mScaleType;
  
  @Deprecated
  public ImageRequest(String paramString, Response.Listener<Bitmap> paramListener, int paramInt1, int paramInt2, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    this(paramString, paramListener, paramInt1, paramInt2, ImageView.ScaleType.CENTER_INSIDE, paramConfig, paramErrorListener);
  }
  
  public ImageRequest(String paramString, Response.Listener<Bitmap> paramListener, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    super(0, paramString, paramErrorListener);
    setRetryPolicy(new DefaultRetryPolicy(1000, 2, 2.0F));
    this.mListener = paramListener;
    this.mDecodeConfig = paramConfig;
    this.mMaxWidth = paramInt1;
    this.mMaxHeight = paramInt2;
    this.mScaleType = paramScaleType;
  }
  
  private Response<Bitmap> doParse(NetworkResponse paramNetworkResponse)
  {
    Object localObject1 = paramNetworkResponse.data;
    Object localObject2 = new BitmapFactory.Options();
    if ((this.mMaxWidth == 0) && (this.mMaxHeight == 0))
    {
      ((BitmapFactory.Options)localObject2).inPreferredConfig = this.mDecodeConfig;
      localObject1 = BitmapFactory.decodeByteArray((byte[])localObject1, 0, localObject1.length, (BitmapFactory.Options)localObject2);
    }
    else
    {
      ((BitmapFactory.Options)localObject2).inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray((byte[])localObject1, 0, localObject1.length, (BitmapFactory.Options)localObject2);
      int i = ((BitmapFactory.Options)localObject2).outWidth;
      int m = ((BitmapFactory.Options)localObject2).outHeight;
      int k = getResizedDimension(this.mMaxWidth, this.mMaxHeight, i, m, this.mScaleType);
      int j = getResizedDimension(this.mMaxHeight, this.mMaxWidth, m, i, this.mScaleType);
      ((BitmapFactory.Options)localObject2).inJustDecodeBounds = false;
      ((BitmapFactory.Options)localObject2).inSampleSize = findBestSampleSize(i, m, k, j);
      localObject2 = BitmapFactory.decodeByteArray((byte[])localObject1, 0, localObject1.length, (BitmapFactory.Options)localObject2);
      localObject1 = localObject2;
      if (localObject2 != null) {
        if (((Bitmap)localObject2).getWidth() <= k)
        {
          localObject1 = localObject2;
          if (((Bitmap)localObject2).getHeight() <= j) {}
        }
        else
        {
          localObject1 = Bitmap.createScaledBitmap((Bitmap)localObject2, k, j, true);
          ((Bitmap)localObject2).recycle();
        }
      }
    }
    if (localObject1 == null) {
      return Response.error(new ParseError(paramNetworkResponse));
    }
    return Response.success(localObject1, HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
  }
  
  static int findBestSampleSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    double d2 = paramInt1;
    double d1 = paramInt3;
    Double.isNaN(d2);
    Double.isNaN(d1);
    d2 /= d1;
    d1 = paramInt2;
    double d3 = paramInt4;
    Double.isNaN(d1);
    Double.isNaN(d3);
    d1 = Math.min(d2, d1 / d3);
    float f2;
    for (float f1 = 1.0F;; f1 = f2)
    {
      f2 = 2.0F * f1;
      if (f2 > d1) {
        break;
      }
    }
    return (int)f1;
  }
  
  private static int getResizedDimension(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ImageView.ScaleType paramScaleType)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0)) {
      return paramInt3;
    }
    if (paramScaleType == ImageView.ScaleType.FIT_XY)
    {
      if (paramInt1 == 0) {
        return paramInt3;
      }
      return paramInt1;
    }
    if (paramInt1 == 0)
    {
      d2 = paramInt2;
      d1 = paramInt4;
      Double.isNaN(d2);
      Double.isNaN(d1);
      d1 = d2 / d1;
      d2 = paramInt3;
      Double.isNaN(d2);
      return (int)(d2 * d1);
    }
    if (paramInt2 == 0) {
      return paramInt1;
    }
    double d1 = paramInt4;
    double d2 = paramInt3;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    if (paramScaleType == ImageView.ScaleType.CENTER_CROP)
    {
      d2 = paramInt1;
      Double.isNaN(d2);
      d3 = paramInt2;
      if (d2 * d1 < d3)
      {
        Double.isNaN(d3);
        paramInt1 = (int)(d3 / d1);
      }
      return paramInt1;
    }
    double d3 = paramInt1;
    Double.isNaN(d3);
    d2 = paramInt2;
    if (d3 * d1 > d2)
    {
      Double.isNaN(d2);
      paramInt1 = (int)(d2 / d1);
    }
    return paramInt1;
  }
  
  protected void deliverResponse(Bitmap paramBitmap)
  {
    this.mListener.onResponse(paramBitmap);
  }
  
  public Request.Priority getPriority()
  {
    return Request.Priority.LOW;
  }
  
  protected Response<Bitmap> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    try
    {
      synchronized (sDecodeLock)
      {
        Response localResponse = doParse(paramNetworkResponse);
        return localResponse;
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      VolleyLog.e("Caught OOM for %d byte image, url=%s", new Object[] { Integer.valueOf(paramNetworkResponse.data.length), getUrl() });
      paramNetworkResponse = new com/android/volley/ParseError;
      paramNetworkResponse.<init>(localOutOfMemoryError);
      paramNetworkResponse = Response.error(paramNetworkResponse);
      return paramNetworkResponse;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/ImageRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */