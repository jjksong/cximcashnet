package com.android.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.http.AndroidHttpClient;
import android.os.Build.VERSION;
import com.android.volley.RequestQueue;
import java.io.File;

public class Volley
{
  private static final String DEFAULT_CACHE_DIR = "volley";
  
  public static RequestQueue newRequestQueue(Context paramContext)
  {
    return newRequestQueue(paramContext, null);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, int paramInt)
  {
    return newRequestQueue(paramContext, null, paramInt);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, HttpStack paramHttpStack)
  {
    return newRequestQueue(paramContext, paramHttpStack, -1);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, HttpStack paramHttpStack, int paramInt)
  {
    File localFile = new File(paramContext.getCacheDir(), "volley");
    Object localObject = "volley/0";
    try
    {
      String str = paramContext.getPackageName();
      PackageInfo localPackageInfo = paramContext.getPackageManager().getPackageInfo(str, 0);
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append(str);
      paramContext.append("/");
      paramContext.append(localPackageInfo.versionCode);
      paramContext = paramContext.toString();
      localObject = paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
    paramContext = paramHttpStack;
    if (paramHttpStack == null) {
      if (Build.VERSION.SDK_INT >= 9) {
        paramContext = new HurlStack();
      } else {
        paramContext = new HttpClientStack(AndroidHttpClient.newInstance((String)localObject));
      }
    }
    paramContext = new BasicNetwork(paramContext);
    if (paramInt <= -1) {
      paramContext = new RequestQueue(new DiskBasedCache(localFile), paramContext);
    } else {
      paramContext = new RequestQueue(new DiskBasedCache(localFile, paramInt), paramContext);
    }
    paramContext.start();
    return paramContext;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/Volley.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */