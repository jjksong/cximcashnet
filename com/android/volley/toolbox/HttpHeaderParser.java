package com.android.volley.toolbox;

import com.android.volley.Cache.Entry;
import com.android.volley.NetworkResponse;
import java.util.Date;
import java.util.Map;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;

public class HttpHeaderParser
{
  public static Cache.Entry parseCacheHeaders(NetworkResponse paramNetworkResponse)
  {
    long l6 = System.currentTimeMillis();
    Map localMap = paramNetworkResponse.headers;
    Object localObject1 = (String)localMap.get("Date");
    long l3;
    if (localObject1 != null) {
      l3 = parseDateAsEpoch((String)localObject1);
    } else {
      l3 = 0L;
    }
    localObject1 = (String)localMap.get("Cache-Control");
    int k = 0;
    int j = 0;
    if (localObject1 != null)
    {
      localObject1 = ((String)localObject1).split(",");
      l2 = 0L;
      i = 0;
      l1 = 0L;
      for (;;)
      {
        label83:
        if (j < localObject1.length)
        {
          localObject2 = localObject1[j].trim();
          if ((!((String)localObject2).equals("no-cache")) && (!((String)localObject2).equals("no-store")) && (!((String)localObject2).startsWith("max-age="))) {
            break;
          }
        }
      }
    }
    try
    {
      l4 = Long.parseLong(((String)localObject2).substring(8));
      l5 = l1;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        long l4 = l2;
        long l5 = l1;
      }
    }
    if (((String)localObject2).startsWith("stale-while-revalidate="))
    {
      l5 = Long.parseLong(((String)localObject2).substring(23));
      l4 = l2;
    }
    else if (!((String)localObject2).equals("must-revalidate"))
    {
      l4 = l2;
      l5 = l1;
      if (!((String)localObject2).equals("proxy-revalidate")) {}
    }
    else
    {
      i = 1;
      l5 = l1;
      l4 = l2;
    }
    j++;
    l2 = l4;
    l1 = l5;
    break label83;
    return null;
    k = 1;
    j = i;
    int i = k;
    break label253;
    l2 = 0L;
    l1 = 0L;
    i = 0;
    j = k;
    label253:
    localObject1 = (String)localMap.get("Expires");
    if (localObject1 != null) {
      l5 = parseDateAsEpoch((String)localObject1);
    } else {
      l5 = 0L;
    }
    localObject1 = (String)localMap.get("Last-Modified");
    if (localObject1 != null) {
      l4 = parseDateAsEpoch((String)localObject1);
    } else {
      l4 = 0L;
    }
    localObject1 = (String)localMap.get("ETag");
    if (i != 0)
    {
      l2 = l6 + l2 * 1000L;
      if (j != 0)
      {
        l1 = l2;
      }
      else
      {
        Long.signum(l1);
        l1 = l1 * 1000L + l2;
      }
    }
    else if ((l3 > 0L) && (l5 >= l3))
    {
      l1 = l5 - l3 + l6;
      l2 = l1;
    }
    else
    {
      l2 = 0L;
      l1 = l2;
    }
    Object localObject2 = new Cache.Entry();
    ((Cache.Entry)localObject2).data = paramNetworkResponse.data;
    ((Cache.Entry)localObject2).etag = ((String)localObject1);
    ((Cache.Entry)localObject2).softTtl = l2;
    ((Cache.Entry)localObject2).ttl = l1;
    ((Cache.Entry)localObject2).serverDate = l3;
    ((Cache.Entry)localObject2).lastModified = l4;
    ((Cache.Entry)localObject2).responseHeaders = localMap;
    return (Cache.Entry)localObject2;
  }
  
  public static String parseCharset(Map<String, String> paramMap)
  {
    return parseCharset(paramMap, "ISO-8859-1");
  }
  
  public static String parseCharset(Map<String, String> paramMap, String paramString)
  {
    paramMap = (String)paramMap.get("Content-Type");
    if (paramMap != null)
    {
      paramMap = paramMap.split(";");
      for (int i = 1; i < paramMap.length; i++)
      {
        String[] arrayOfString = paramMap[i].trim().split("=");
        if ((arrayOfString.length == 2) && (arrayOfString[0].equals("charset"))) {
          return arrayOfString[1];
        }
      }
    }
    return paramString;
  }
  
  public static long parseDateAsEpoch(String paramString)
  {
    try
    {
      long l = DateUtils.parseDate(paramString).getTime();
      return l;
    }
    catch (DateParseException paramString) {}
    return 0L;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/HttpHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */