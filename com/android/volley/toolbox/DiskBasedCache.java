package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.VolleyLog;
import java.io.EOFException;
import java.io.File;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DiskBasedCache
  implements Cache
{
  private static final int CACHE_MAGIC = 538247942;
  private static final int DEFAULT_DISK_USAGE_BYTES = 5242880;
  private static final float HYSTERESIS_FACTOR = 0.9F;
  private final Map<String, CacheHeader> mEntries = new LinkedHashMap(16, 0.75F, true);
  private final int mMaxCacheSizeInBytes;
  private final File mRootDirectory;
  private long mTotalSize = 0L;
  
  public DiskBasedCache(File paramFile)
  {
    this(paramFile, 5242880);
  }
  
  public DiskBasedCache(File paramFile, int paramInt)
  {
    this.mRootDirectory = paramFile;
    this.mMaxCacheSizeInBytes = paramInt;
  }
  
  private String getFilenameForKey(String paramString)
  {
    int i = paramString.length() / 2;
    int j = paramString.substring(0, i).hashCode();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(String.valueOf(j));
    localStringBuilder.append(String.valueOf(paramString.substring(i).hashCode()));
    return localStringBuilder.toString();
  }
  
  private void pruneIfNeeded(int paramInt)
  {
    long l2 = this.mTotalSize;
    long l1 = paramInt;
    if (l2 + l1 < this.mMaxCacheSizeInBytes) {
      return;
    }
    if (VolleyLog.DEBUG) {
      VolleyLog.v("Pruning old cache entries.", new Object[0]);
    }
    l2 = this.mTotalSize;
    long l3 = SystemClock.elapsedRealtime();
    Iterator localIterator = this.mEntries.entrySet().iterator();
    paramInt = 0;
    int i;
    for (;;)
    {
      i = paramInt;
      if (!localIterator.hasNext()) {
        break;
      }
      CacheHeader localCacheHeader = (CacheHeader)((Map.Entry)localIterator.next()).getValue();
      if (getFileForKey(localCacheHeader.key).delete()) {
        this.mTotalSize -= localCacheHeader.size;
      } else {
        VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", new Object[] { localCacheHeader.key, getFilenameForKey(localCacheHeader.key) });
      }
      localIterator.remove();
      paramInt++;
      if ((float)(this.mTotalSize + l1) < this.mMaxCacheSizeInBytes * 0.9F)
      {
        i = paramInt;
        break;
      }
    }
    if (VolleyLog.DEBUG) {
      VolleyLog.v("pruned %d files, %d bytes, %d ms", new Object[] { Integer.valueOf(i), Long.valueOf(this.mTotalSize - l2), Long.valueOf(SystemClock.elapsedRealtime() - l3) });
    }
  }
  
  private void putEntry(String paramString, CacheHeader paramCacheHeader)
  {
    if (!this.mEntries.containsKey(paramString))
    {
      this.mTotalSize += paramCacheHeader.size;
    }
    else
    {
      CacheHeader localCacheHeader = (CacheHeader)this.mEntries.get(paramString);
      this.mTotalSize += paramCacheHeader.size - localCacheHeader.size;
    }
    this.mEntries.put(paramString, paramCacheHeader);
  }
  
  private static int read(InputStream paramInputStream)
    throws IOException
  {
    int i = paramInputStream.read();
    if (i != -1) {
      return i;
    }
    throw new EOFException();
  }
  
  static int readInt(InputStream paramInputStream)
    throws IOException
  {
    int k = read(paramInputStream);
    int i = read(paramInputStream);
    int j = read(paramInputStream);
    return read(paramInputStream) << 24 | k << 0 | 0x0 | i << 8 | j << 16;
  }
  
  static long readLong(InputStream paramInputStream)
    throws IOException
  {
    return (read(paramInputStream) & 0xFF) << 0 | 0L | (read(paramInputStream) & 0xFF) << 8 | (read(paramInputStream) & 0xFF) << 16 | (read(paramInputStream) & 0xFF) << 24 | (read(paramInputStream) & 0xFF) << 32 | (read(paramInputStream) & 0xFF) << 40 | (read(paramInputStream) & 0xFF) << 48 | (0xFF & read(paramInputStream)) << 56;
  }
  
  static String readString(InputStream paramInputStream)
    throws IOException
  {
    return new String(streamToBytes(paramInputStream, (int)readLong(paramInputStream)), "UTF-8");
  }
  
  static Map<String, String> readStringStringMap(InputStream paramInputStream)
    throws IOException
  {
    int j = readInt(paramInputStream);
    Object localObject;
    if (j == 0) {
      localObject = Collections.emptyMap();
    } else {
      localObject = new HashMap(j);
    }
    for (int i = 0; i < j; i++) {
      ((Map)localObject).put(readString(paramInputStream).intern(), readString(paramInputStream).intern());
    }
    return (Map<String, String>)localObject;
  }
  
  private void removeEntry(String paramString)
  {
    CacheHeader localCacheHeader = (CacheHeader)this.mEntries.get(paramString);
    if (localCacheHeader != null)
    {
      this.mTotalSize -= localCacheHeader.size;
      this.mEntries.remove(paramString);
    }
  }
  
  private static byte[] streamToBytes(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    while (i < paramInt)
    {
      int j = paramInputStream.read(arrayOfByte, i, paramInt - i);
      if (j == -1) {
        break;
      }
      i += j;
    }
    if (i == paramInt) {
      return arrayOfByte;
    }
    paramInputStream = new StringBuilder();
    paramInputStream.append("Expected ");
    paramInputStream.append(paramInt);
    paramInputStream.append(" bytes, read ");
    paramInputStream.append(i);
    paramInputStream.append(" bytes");
    throw new IOException(paramInputStream.toString());
  }
  
  static void writeInt(OutputStream paramOutputStream, int paramInt)
    throws IOException
  {
    paramOutputStream.write(paramInt >> 0 & 0xFF);
    paramOutputStream.write(paramInt >> 8 & 0xFF);
    paramOutputStream.write(paramInt >> 16 & 0xFF);
    paramOutputStream.write(paramInt >> 24 & 0xFF);
  }
  
  static void writeLong(OutputStream paramOutputStream, long paramLong)
    throws IOException
  {
    paramOutputStream.write((byte)(int)(paramLong >>> 0));
    paramOutputStream.write((byte)(int)(paramLong >>> 8));
    paramOutputStream.write((byte)(int)(paramLong >>> 16));
    paramOutputStream.write((byte)(int)(paramLong >>> 24));
    paramOutputStream.write((byte)(int)(paramLong >>> 32));
    paramOutputStream.write((byte)(int)(paramLong >>> 40));
    paramOutputStream.write((byte)(int)(paramLong >>> 48));
    paramOutputStream.write((byte)(int)(paramLong >>> 56));
  }
  
  static void writeString(OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    paramString = paramString.getBytes("UTF-8");
    writeLong(paramOutputStream, paramString.length);
    paramOutputStream.write(paramString, 0, paramString.length);
  }
  
  static void writeStringStringMap(Map<String, String> paramMap, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramMap != null)
    {
      writeInt(paramOutputStream, paramMap.size());
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        writeString(paramOutputStream, (String)localEntry.getKey());
        writeString(paramOutputStream, (String)localEntry.getValue());
      }
    }
    writeInt(paramOutputStream, 0);
  }
  
  public void clear()
  {
    try
    {
      File[] arrayOfFile = this.mRootDirectory.listFiles();
      if (arrayOfFile != null)
      {
        int j = arrayOfFile.length;
        for (int i = 0; i < j; i++) {
          arrayOfFile[i].delete();
        }
      }
      this.mEntries.clear();
      this.mTotalSize = 0L;
      VolleyLog.d("Cache cleared.", new Object[0]);
      return;
    }
    finally {}
  }
  
  /* Error */
  public Cache.Entry get(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 47	com/android/volley/toolbox/DiskBasedCache:mEntries	Ljava/util/Map;
    //   6: aload_1
    //   7: invokeinterface 180 2 0
    //   12: checkcast 10	com/android/volley/toolbox/DiskBasedCache$CacheHeader
    //   15: astore 4
    //   17: aload 4
    //   19: ifnonnull +7 -> 26
    //   22: aload_0
    //   23: monitorexit
    //   24: aconst_null
    //   25: areturn
    //   26: aload_0
    //   27: aload_1
    //   28: invokevirtual 140	com/android/volley/toolbox/DiskBasedCache:getFileForKey	(Ljava/lang/String;)Ljava/io/File;
    //   31: astore 5
    //   33: new 13	com/android/volley/toolbox/DiskBasedCache$CountingInputStream
    //   36: astore_3
    //   37: new 301	java/io/BufferedInputStream
    //   40: astore 6
    //   42: new 303	java/io/FileInputStream
    //   45: astore_2
    //   46: aload_2
    //   47: aload 5
    //   49: invokespecial 305	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   52: aload 6
    //   54: aload_2
    //   55: invokespecial 308	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   58: aload_3
    //   59: aload 6
    //   61: aconst_null
    //   62: invokespecial 311	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:<init>	(Ljava/io/InputStream;Lcom/android/volley/toolbox/DiskBasedCache$1;)V
    //   65: aload_3
    //   66: astore_2
    //   67: aload_3
    //   68: invokestatic 315	com/android/volley/toolbox/DiskBasedCache$CacheHeader:readHeader	(Ljava/io/InputStream;)Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;
    //   71: pop
    //   72: aload_3
    //   73: astore_2
    //   74: aload 4
    //   76: aload_3
    //   77: aload 5
    //   79: invokevirtual 317	java/io/File:length	()J
    //   82: aload_3
    //   83: invokestatic 321	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:access$100	(Lcom/android/volley/toolbox/DiskBasedCache$CountingInputStream;)I
    //   86: i2l
    //   87: lsub
    //   88: l2i
    //   89: invokestatic 211	com/android/volley/toolbox/DiskBasedCache:streamToBytes	(Ljava/io/InputStream;I)[B
    //   92: invokevirtual 325	com/android/volley/toolbox/DiskBasedCache$CacheHeader:toCacheEntry	([B)Lcom/android/volley/Cache$Entry;
    //   95: astore 4
    //   97: aload_3
    //   98: invokevirtual 328	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   101: aload_0
    //   102: monitorexit
    //   103: aload 4
    //   105: areturn
    //   106: astore_1
    //   107: aload_0
    //   108: monitorexit
    //   109: aconst_null
    //   110: areturn
    //   111: astore 4
    //   113: goto +18 -> 131
    //   116: astore 4
    //   118: goto +72 -> 190
    //   121: astore_1
    //   122: aconst_null
    //   123: astore_2
    //   124: goto +122 -> 246
    //   127: astore 4
    //   129: aconst_null
    //   130: astore_3
    //   131: aload_3
    //   132: astore_2
    //   133: ldc_w 330
    //   136: iconst_2
    //   137: anewarray 4	java/lang/Object
    //   140: dup
    //   141: iconst_0
    //   142: aload 5
    //   144: invokevirtual 333	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   147: aastore
    //   148: dup
    //   149: iconst_1
    //   150: aload 4
    //   152: invokevirtual 334	java/lang/NegativeArraySizeException:toString	()Ljava/lang/String;
    //   155: aastore
    //   156: invokestatic 155	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   159: aload_3
    //   160: astore_2
    //   161: aload_0
    //   162: aload_1
    //   163: invokevirtual 336	com/android/volley/toolbox/DiskBasedCache:remove	(Ljava/lang/String;)V
    //   166: aload_3
    //   167: ifnull +15 -> 182
    //   170: aload_3
    //   171: invokevirtual 328	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   174: goto +8 -> 182
    //   177: astore_1
    //   178: aload_0
    //   179: monitorexit
    //   180: aconst_null
    //   181: areturn
    //   182: aload_0
    //   183: monitorexit
    //   184: aconst_null
    //   185: areturn
    //   186: astore 4
    //   188: aconst_null
    //   189: astore_3
    //   190: aload_3
    //   191: astore_2
    //   192: ldc_w 330
    //   195: iconst_2
    //   196: anewarray 4	java/lang/Object
    //   199: dup
    //   200: iconst_0
    //   201: aload 5
    //   203: invokevirtual 333	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   206: aastore
    //   207: dup
    //   208: iconst_1
    //   209: aload 4
    //   211: invokevirtual 337	java/io/IOException:toString	()Ljava/lang/String;
    //   214: aastore
    //   215: invokestatic 155	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   218: aload_3
    //   219: astore_2
    //   220: aload_0
    //   221: aload_1
    //   222: invokevirtual 336	com/android/volley/toolbox/DiskBasedCache:remove	(Ljava/lang/String;)V
    //   225: aload_3
    //   226: ifnull +15 -> 241
    //   229: aload_3
    //   230: invokevirtual 328	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   233: goto +8 -> 241
    //   236: astore_1
    //   237: aload_0
    //   238: monitorexit
    //   239: aconst_null
    //   240: areturn
    //   241: aload_0
    //   242: monitorexit
    //   243: aconst_null
    //   244: areturn
    //   245: astore_1
    //   246: aload_2
    //   247: ifnull +15 -> 262
    //   250: aload_2
    //   251: invokevirtual 328	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   254: goto +8 -> 262
    //   257: astore_1
    //   258: aload_0
    //   259: monitorexit
    //   260: aconst_null
    //   261: areturn
    //   262: aload_1
    //   263: athrow
    //   264: astore_1
    //   265: aload_0
    //   266: monitorexit
    //   267: aload_1
    //   268: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	269	0	this	DiskBasedCache
    //   0	269	1	paramString	String
    //   45	206	2	localObject1	Object
    //   36	194	3	localCountingInputStream	CountingInputStream
    //   15	89	4	localObject2	Object
    //   111	1	4	localNegativeArraySizeException1	NegativeArraySizeException
    //   116	1	4	localIOException1	IOException
    //   127	24	4	localNegativeArraySizeException2	NegativeArraySizeException
    //   186	24	4	localIOException2	IOException
    //   31	171	5	localFile	File
    //   40	20	6	localBufferedInputStream	java.io.BufferedInputStream
    // Exception table:
    //   from	to	target	type
    //   97	101	106	java/io/IOException
    //   67	72	111	java/lang/NegativeArraySizeException
    //   74	97	111	java/lang/NegativeArraySizeException
    //   67	72	116	java/io/IOException
    //   74	97	116	java/io/IOException
    //   33	65	121	finally
    //   33	65	127	java/lang/NegativeArraySizeException
    //   170	174	177	java/io/IOException
    //   33	65	186	java/io/IOException
    //   229	233	236	java/io/IOException
    //   67	72	245	finally
    //   74	97	245	finally
    //   133	159	245	finally
    //   161	166	245	finally
    //   192	218	245	finally
    //   220	225	245	finally
    //   250	254	257	java/io/IOException
    //   2	17	264	finally
    //   26	33	264	finally
    //   97	101	264	finally
    //   170	174	264	finally
    //   229	233	264	finally
    //   250	254	264	finally
    //   262	264	264	finally
  }
  
  public File getFileForKey(String paramString)
  {
    return new File(this.mRootDirectory, getFilenameForKey(paramString));
  }
  
  /* Error */
  public void initialize()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   6: invokevirtual 344	java/io/File:exists	()Z
    //   9: istore_3
    //   10: iconst_0
    //   11: istore_1
    //   12: iload_3
    //   13: ifne +36 -> 49
    //   16: aload_0
    //   17: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   20: invokevirtual 347	java/io/File:mkdirs	()Z
    //   23: ifne +23 -> 46
    //   26: ldc_w 349
    //   29: iconst_1
    //   30: anewarray 4	java/lang/Object
    //   33: dup
    //   34: iconst_0
    //   35: aload_0
    //   36: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   39: invokevirtual 333	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   42: aastore
    //   43: invokestatic 352	com/android/volley/VolleyLog:e	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: aload_0
    //   50: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   53: invokevirtual 292	java/io/File:listFiles	()[Ljava/io/File;
    //   56: astore 8
    //   58: aload 8
    //   60: ifnonnull +6 -> 66
    //   63: aload_0
    //   64: monitorexit
    //   65: return
    //   66: aload 8
    //   68: arraylength
    //   69: istore_2
    //   70: iload_1
    //   71: iload_2
    //   72: if_icmpge +171 -> 243
    //   75: aload 8
    //   77: iload_1
    //   78: aaload
    //   79: astore 9
    //   81: aconst_null
    //   82: astore 6
    //   84: aconst_null
    //   85: astore 7
    //   87: aload 7
    //   89: astore 4
    //   91: new 301	java/io/BufferedInputStream
    //   94: astore 5
    //   96: aload 7
    //   98: astore 4
    //   100: new 303	java/io/FileInputStream
    //   103: astore 10
    //   105: aload 7
    //   107: astore 4
    //   109: aload 10
    //   111: aload 9
    //   113: invokespecial 305	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   116: aload 7
    //   118: astore 4
    //   120: aload 5
    //   122: aload 10
    //   124: invokespecial 308	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   127: aload 5
    //   129: invokestatic 315	com/android/volley/toolbox/DiskBasedCache$CacheHeader:readHeader	(Ljava/io/InputStream;)Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;
    //   132: astore 4
    //   134: aload 4
    //   136: aload 9
    //   138: invokevirtual 317	java/io/File:length	()J
    //   141: putfield 148	com/android/volley/toolbox/DiskBasedCache$CacheHeader:size	J
    //   144: aload_0
    //   145: aload 4
    //   147: getfield 136	com/android/volley/toolbox/DiskBasedCache$CacheHeader:key	Ljava/lang/String;
    //   150: aload 4
    //   152: invokespecial 354	com/android/volley/toolbox/DiskBasedCache:putEntry	(Ljava/lang/String;Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;)V
    //   155: aload 5
    //   157: invokevirtual 355	java/io/BufferedInputStream:close	()V
    //   160: goto +77 -> 237
    //   163: astore 4
    //   165: aload 5
    //   167: astore 6
    //   169: aload 4
    //   171: astore 5
    //   173: aload 6
    //   175: astore 4
    //   177: goto +37 -> 214
    //   180: astore 4
    //   182: goto +14 -> 196
    //   185: astore 5
    //   187: goto +27 -> 214
    //   190: astore 4
    //   192: aload 6
    //   194: astore 5
    //   196: aload 9
    //   198: ifnull +29 -> 227
    //   201: aload 5
    //   203: astore 4
    //   205: aload 9
    //   207: invokevirtual 145	java/io/File:delete	()Z
    //   210: pop
    //   211: goto +16 -> 227
    //   214: aload 4
    //   216: ifnull +8 -> 224
    //   219: aload 4
    //   221: invokevirtual 355	java/io/BufferedInputStream:close	()V
    //   224: aload 5
    //   226: athrow
    //   227: aload 5
    //   229: ifnull +8 -> 237
    //   232: aload 5
    //   234: invokevirtual 355	java/io/BufferedInputStream:close	()V
    //   237: iinc 1 1
    //   240: goto -170 -> 70
    //   243: aload_0
    //   244: monitorexit
    //   245: return
    //   246: astore 4
    //   248: aload_0
    //   249: monitorexit
    //   250: aload 4
    //   252: athrow
    //   253: astore 4
    //   255: goto -18 -> 237
    //   258: astore 4
    //   260: goto -36 -> 224
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	263	0	this	DiskBasedCache
    //   11	227	1	i	int
    //   69	4	2	j	int
    //   9	4	3	bool	boolean
    //   89	62	4	localObject1	Object
    //   163	7	4	localObject2	Object
    //   175	1	4	localObject3	Object
    //   180	1	4	localIOException1	IOException
    //   190	1	4	localIOException2	IOException
    //   203	17	4	localObject4	Object
    //   246	5	4	localObject5	Object
    //   253	1	4	localIOException3	IOException
    //   258	1	4	localIOException4	IOException
    //   94	78	5	localObject6	Object
    //   185	1	5	localObject7	Object
    //   194	39	5	localObject8	Object
    //   82	111	6	localObject9	Object
    //   85	32	7	localObject10	Object
    //   56	20	8	arrayOfFile	File[]
    //   79	127	9	localFile	File
    //   103	20	10	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   127	155	163	finally
    //   127	155	180	java/io/IOException
    //   91	96	185	finally
    //   100	105	185	finally
    //   109	116	185	finally
    //   120	127	185	finally
    //   205	211	185	finally
    //   91	96	190	java/io/IOException
    //   100	105	190	java/io/IOException
    //   109	116	190	java/io/IOException
    //   120	127	190	java/io/IOException
    //   2	10	246	finally
    //   16	46	246	finally
    //   49	58	246	finally
    //   66	70	246	finally
    //   155	160	246	finally
    //   219	224	246	finally
    //   224	227	246	finally
    //   232	237	246	finally
    //   155	160	253	java/io/IOException
    //   232	237	253	java/io/IOException
    //   219	224	258	java/io/IOException
  }
  
  public void invalidate(String paramString, boolean paramBoolean)
  {
    try
    {
      Cache.Entry localEntry = get(paramString);
      if (localEntry != null)
      {
        localEntry.softTtl = 0L;
        if (paramBoolean) {
          localEntry.ttl = 0L;
        }
        put(paramString, localEntry);
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public void put(String paramString, Cache.Entry paramEntry)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: getfield 374	com/android/volley/Cache$Entry:data	[B
    //   7: arraylength
    //   8: invokespecial 376	com/android/volley/toolbox/DiskBasedCache:pruneIfNeeded	(I)V
    //   11: aload_0
    //   12: aload_1
    //   13: invokevirtual 140	com/android/volley/toolbox/DiskBasedCache:getFileForKey	(Ljava/lang/String;)Ljava/io/File;
    //   16: astore_3
    //   17: new 378	java/io/BufferedOutputStream
    //   20: astore 4
    //   22: new 380	java/io/FileOutputStream
    //   25: astore 5
    //   27: aload 5
    //   29: aload_3
    //   30: invokespecial 381	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   33: aload 4
    //   35: aload 5
    //   37: invokespecial 384	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   40: new 10	com/android/volley/toolbox/DiskBasedCache$CacheHeader
    //   43: astore 5
    //   45: aload 5
    //   47: aload_1
    //   48: aload_2
    //   49: invokespecial 386	com/android/volley/toolbox/DiskBasedCache$CacheHeader:<init>	(Ljava/lang/String;Lcom/android/volley/Cache$Entry;)V
    //   52: aload 5
    //   54: aload 4
    //   56: invokevirtual 390	com/android/volley/toolbox/DiskBasedCache$CacheHeader:writeHeader	(Ljava/io/OutputStream;)Z
    //   59: ifeq +27 -> 86
    //   62: aload 4
    //   64: aload_2
    //   65: getfield 374	com/android/volley/Cache$Entry:data	[B
    //   68: invokevirtual 393	java/io/BufferedOutputStream:write	([B)V
    //   71: aload 4
    //   73: invokevirtual 394	java/io/BufferedOutputStream:close	()V
    //   76: aload_0
    //   77: aload_1
    //   78: aload 5
    //   80: invokespecial 354	com/android/volley/toolbox/DiskBasedCache:putEntry	(Ljava/lang/String;Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;)V
    //   83: aload_0
    //   84: monitorexit
    //   85: return
    //   86: aload 4
    //   88: invokevirtual 394	java/io/BufferedOutputStream:close	()V
    //   91: ldc_w 396
    //   94: iconst_1
    //   95: anewarray 4	java/lang/Object
    //   98: dup
    //   99: iconst_0
    //   100: aload_3
    //   101: invokevirtual 333	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   104: aastore
    //   105: invokestatic 155	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   108: new 188	java/io/IOException
    //   111: astore_1
    //   112: aload_1
    //   113: invokespecial 397	java/io/IOException:<init>	()V
    //   116: aload_1
    //   117: athrow
    //   118: astore_1
    //   119: aload_3
    //   120: invokevirtual 145	java/io/File:delete	()Z
    //   123: ifne +20 -> 143
    //   126: ldc_w 399
    //   129: iconst_1
    //   130: anewarray 4	java/lang/Object
    //   133: dup
    //   134: iconst_0
    //   135: aload_3
    //   136: invokevirtual 333	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   139: aastore
    //   140: invokestatic 155	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   143: aload_0
    //   144: monitorexit
    //   145: return
    //   146: astore_1
    //   147: aload_0
    //   148: monitorexit
    //   149: aload_1
    //   150: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	DiskBasedCache
    //   0	151	1	paramString	String
    //   0	151	2	paramEntry	Cache.Entry
    //   16	120	3	localFile	File
    //   20	67	4	localBufferedOutputStream	java.io.BufferedOutputStream
    //   25	54	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   17	83	118	java/io/IOException
    //   86	118	118	java/io/IOException
    //   2	17	146	finally
    //   17	83	146	finally
    //   86	118	146	finally
    //   119	143	146	finally
  }
  
  public void remove(String paramString)
  {
    try
    {
      boolean bool = getFileForKey(paramString).delete();
      removeEntry(paramString);
      if (!bool) {
        VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", new Object[] { paramString, getFilenameForKey(paramString) });
      }
      return;
    }
    finally {}
  }
  
  static class CacheHeader
  {
    public String etag;
    public String key;
    public long lastModified;
    public Map<String, String> responseHeaders;
    public long serverDate;
    public long size;
    public long softTtl;
    public long ttl;
    
    private CacheHeader() {}
    
    public CacheHeader(String paramString, Cache.Entry paramEntry)
    {
      this.key = paramString;
      this.size = paramEntry.data.length;
      this.etag = paramEntry.etag;
      this.serverDate = paramEntry.serverDate;
      this.lastModified = paramEntry.lastModified;
      this.ttl = paramEntry.ttl;
      this.softTtl = paramEntry.softTtl;
      this.responseHeaders = paramEntry.responseHeaders;
    }
    
    public static CacheHeader readHeader(InputStream paramInputStream)
      throws IOException
    {
      CacheHeader localCacheHeader = new CacheHeader();
      if (DiskBasedCache.readInt(paramInputStream) == 538247942)
      {
        localCacheHeader.key = DiskBasedCache.readString(paramInputStream);
        localCacheHeader.etag = DiskBasedCache.readString(paramInputStream);
        if (localCacheHeader.etag.equals("")) {
          localCacheHeader.etag = null;
        }
        localCacheHeader.serverDate = DiskBasedCache.readLong(paramInputStream);
        localCacheHeader.lastModified = DiskBasedCache.readLong(paramInputStream);
        localCacheHeader.ttl = DiskBasedCache.readLong(paramInputStream);
        localCacheHeader.softTtl = DiskBasedCache.readLong(paramInputStream);
        localCacheHeader.responseHeaders = DiskBasedCache.readStringStringMap(paramInputStream);
        return localCacheHeader;
      }
      throw new IOException();
    }
    
    public Cache.Entry toCacheEntry(byte[] paramArrayOfByte)
    {
      Cache.Entry localEntry = new Cache.Entry();
      localEntry.data = paramArrayOfByte;
      localEntry.etag = this.etag;
      localEntry.serverDate = this.serverDate;
      localEntry.lastModified = this.lastModified;
      localEntry.ttl = this.ttl;
      localEntry.softTtl = this.softTtl;
      localEntry.responseHeaders = this.responseHeaders;
      return localEntry;
    }
    
    public boolean writeHeader(OutputStream paramOutputStream)
    {
      try
      {
        DiskBasedCache.writeInt(paramOutputStream, 538247942);
        DiskBasedCache.writeString(paramOutputStream, this.key);
        String str;
        if (this.etag == null) {
          str = "";
        } else {
          str = this.etag;
        }
        DiskBasedCache.writeString(paramOutputStream, str);
        DiskBasedCache.writeLong(paramOutputStream, this.serverDate);
        DiskBasedCache.writeLong(paramOutputStream, this.lastModified);
        DiskBasedCache.writeLong(paramOutputStream, this.ttl);
        DiskBasedCache.writeLong(paramOutputStream, this.softTtl);
        DiskBasedCache.writeStringStringMap(this.responseHeaders, paramOutputStream);
        paramOutputStream.flush();
        return true;
      }
      catch (IOException paramOutputStream)
      {
        VolleyLog.d("%s", new Object[] { paramOutputStream.toString() });
      }
      return false;
    }
  }
  
  private static class CountingInputStream
    extends FilterInputStream
  {
    private int bytesRead = 0;
    
    private CountingInputStream(InputStream paramInputStream)
    {
      super();
    }
    
    public int read()
      throws IOException
    {
      int i = super.read();
      if (i != -1) {
        this.bytesRead += 1;
      }
      return i;
    }
    
    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      paramInt1 = super.read(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt1 != -1) {
        this.bytesRead += paramInt1;
      }
      return paramInt1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/toolbox/DiskBasedCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */