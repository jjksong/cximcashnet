package com.android.volley;

public class ServerError
  extends VolleyError
{
  public ServerError() {}
  
  public ServerError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/ServerError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */