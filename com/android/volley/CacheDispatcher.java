package com.android.volley;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public class CacheDispatcher
  extends Thread
{
  private static final boolean DEBUG = VolleyLog.DEBUG;
  private final Cache mCache;
  private final BlockingQueue<Request<?>> mCacheQueue;
  private final ResponseDelivery mDelivery;
  private final BlockingQueue<Request<?>> mNetworkQueue;
  private volatile boolean mQuit = false;
  
  public CacheDispatcher(BlockingQueue<Request<?>> paramBlockingQueue1, BlockingQueue<Request<?>> paramBlockingQueue2, Cache paramCache, ResponseDelivery paramResponseDelivery)
  {
    this.mCacheQueue = paramBlockingQueue1;
    this.mNetworkQueue = paramBlockingQueue2;
    this.mCache = paramCache;
    this.mDelivery = paramResponseDelivery;
  }
  
  public void quit()
  {
    this.mQuit = true;
    interrupt();
  }
  
  public void run()
  {
    if (DEBUG) {
      VolleyLog.v("start new dispatcher", new Object[0]);
    }
    Process.setThreadPriority(10);
    this.mCache.initialize();
    try
    {
      do
      {
        for (;;)
        {
          Request localRequest = (Request)this.mCacheQueue.take();
          try
          {
            localRequest.addMarker("cache-queue-take");
            if (localRequest.isCanceled())
            {
              localRequest.finish("cache-discard-canceled");
            }
            else
            {
              Object localObject2 = this.mCache.get(localRequest.getCacheKey());
              if (localObject2 == null)
              {
                localRequest.addMarker("cache-miss");
                this.mNetworkQueue.put(localRequest);
              }
              else if (((Cache.Entry)localObject2).isExpired())
              {
                localRequest.addMarker("cache-hit-expired");
                localRequest.setCacheEntry((Cache.Entry)localObject2);
                this.mNetworkQueue.put(localRequest);
              }
              else
              {
                localRequest.addMarker("cache-hit");
                Object localObject1 = new com/android/volley/NetworkResponse;
                ((NetworkResponse)localObject1).<init>(((Cache.Entry)localObject2).data, ((Cache.Entry)localObject2).responseHeaders);
                localObject1 = localRequest.parseNetworkResponse((NetworkResponse)localObject1);
                localRequest.addMarker("cache-hit-parsed");
                if (!((Cache.Entry)localObject2).refreshNeeded())
                {
                  this.mDelivery.postResponse(localRequest, (Response)localObject1);
                }
                else
                {
                  localRequest.addMarker("cache-hit-refresh-needed");
                  localRequest.setCacheEntry((Cache.Entry)localObject2);
                  ((Response)localObject1).intermediate = true;
                  localObject2 = this.mDelivery;
                  Runnable local1 = new com/android/volley/CacheDispatcher$1;
                  local1.<init>(this, localRequest);
                  ((ResponseDelivery)localObject2).postResponse(localRequest, (Response)localObject1, local1);
                }
              }
            }
          }
          catch (Exception localException)
          {
            VolleyLog.e(localException, "Unhandled exception %s", new Object[] { localException.toString() });
          }
        }
      } while (!this.mQuit);
    }
    catch (InterruptedException localInterruptedException) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/CacheDispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */