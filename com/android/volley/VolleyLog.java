package com.android.volley;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class VolleyLog
{
  public static boolean DEBUG = Log.isLoggable(TAG, 2);
  public static String TAG = "Volley";
  
  private static String buildMessage(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs != null) {
      paramString = String.format(Locale.US, paramString, paramVarArgs);
    }
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().fillInStackTrace().getStackTrace();
    String str = "<unknown>";
    for (int i = 2;; i++)
    {
      paramVarArgs = str;
      if (i >= arrayOfStackTraceElement.length) {
        break;
      }
      if (!arrayOfStackTraceElement[i].getClass().equals(VolleyLog.class))
      {
        paramVarArgs = arrayOfStackTraceElement[i].getClassName();
        paramVarArgs = paramVarArgs.substring(paramVarArgs.lastIndexOf('.') + 1);
        str = paramVarArgs.substring(paramVarArgs.lastIndexOf('$') + 1);
        paramVarArgs = new StringBuilder();
        paramVarArgs.append(str);
        paramVarArgs.append(".");
        paramVarArgs.append(arrayOfStackTraceElement[i].getMethodName());
        paramVarArgs = paramVarArgs.toString();
        break;
      }
    }
    return String.format(Locale.US, "[%d] %s: %s", new Object[] { Long.valueOf(Thread.currentThread().getId()), paramVarArgs, paramString });
  }
  
  public static void d(String paramString, Object... paramVarArgs)
  {
    Log.d(TAG, buildMessage(paramString, paramVarArgs));
  }
  
  public static void e(String paramString, Object... paramVarArgs)
  {
    Log.e(TAG, buildMessage(paramString, paramVarArgs));
  }
  
  public static void e(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    Log.e(TAG, buildMessage(paramString, paramVarArgs), paramThrowable);
  }
  
  public static void setTag(String paramString)
  {
    d("Changing log tag to %s", new Object[] { paramString });
    TAG = paramString;
    DEBUG = Log.isLoggable(TAG, 2);
  }
  
  public static void v(String paramString, Object... paramVarArgs)
  {
    if (DEBUG) {
      Log.v(TAG, buildMessage(paramString, paramVarArgs));
    }
  }
  
  public static void wtf(String paramString, Object... paramVarArgs)
  {
    Log.wtf(TAG, buildMessage(paramString, paramVarArgs));
  }
  
  public static void wtf(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    Log.wtf(TAG, buildMessage(paramString, paramVarArgs), paramThrowable);
  }
  
  static class MarkerLog
  {
    public static final boolean ENABLED = VolleyLog.DEBUG;
    private static final long MIN_DURATION_FOR_LOGGING_MS = 0L;
    private boolean mFinished = false;
    private final List<Marker> mMarkers = new ArrayList();
    
    private long getTotalDuration()
    {
      if (this.mMarkers.size() == 0) {
        return 0L;
      }
      long l = ((Marker)this.mMarkers.get(0)).time;
      List localList = this.mMarkers;
      return ((Marker)localList.get(localList.size() - 1)).time - l;
    }
    
    public void add(String paramString, long paramLong)
    {
      try
      {
        if (!this.mFinished)
        {
          List localList = this.mMarkers;
          Marker localMarker = new com/android/volley/VolleyLog$MarkerLog$Marker;
          localMarker.<init>(paramString, paramLong, SystemClock.elapsedRealtime());
          localList.add(localMarker);
          return;
        }
        paramString = new java/lang/IllegalStateException;
        paramString.<init>("Marker added to finished log");
        throw paramString;
      }
      finally {}
    }
    
    protected void finalize()
      throws Throwable
    {
      if (!this.mFinished)
      {
        finish("Request on the loose");
        VolleyLog.e("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
      }
    }
    
    public void finish(String paramString)
    {
      try
      {
        this.mFinished = true;
        long l2 = getTotalDuration();
        if (l2 <= 0L) {
          return;
        }
        long l1 = ((Marker)this.mMarkers.get(0)).time;
        VolleyLog.d("(%-4d ms) %s", new Object[] { Long.valueOf(l2), paramString });
        Iterator localIterator = this.mMarkers.iterator();
        while (localIterator.hasNext())
        {
          paramString = (Marker)localIterator.next();
          l2 = paramString.time;
          VolleyLog.d("(+%-4d) [%2d] %s", new Object[] { Long.valueOf(l2 - l1), Long.valueOf(paramString.thread), paramString.name });
          l1 = l2;
        }
        return;
      }
      finally {}
    }
    
    private static class Marker
    {
      public final String name;
      public final long thread;
      public final long time;
      
      public Marker(String paramString, long paramLong1, long paramLong2)
      {
        this.name = paramString;
        this.thread = paramLong1;
        this.time = paramLong2;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/android/volley/VolleyLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */