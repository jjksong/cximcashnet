package com.google.gson;

import com.google.gson.internal..Gson.Preconditions;
import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class JsonPrimitive
  extends JsonElement
{
  private static final Class<?>[] PRIMITIVE_TYPES = { Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class };
  private Object value;
  
  public JsonPrimitive(Boolean paramBoolean)
  {
    setValue(paramBoolean);
  }
  
  public JsonPrimitive(Character paramCharacter)
  {
    setValue(paramCharacter);
  }
  
  public JsonPrimitive(Number paramNumber)
  {
    setValue(paramNumber);
  }
  
  JsonPrimitive(Object paramObject)
  {
    setValue(paramObject);
  }
  
  public JsonPrimitive(String paramString)
  {
    setValue(paramString);
  }
  
  private static boolean isIntegral(JsonPrimitive paramJsonPrimitive)
  {
    paramJsonPrimitive = paramJsonPrimitive.value;
    boolean bool2 = paramJsonPrimitive instanceof Number;
    boolean bool1 = false;
    if (bool2)
    {
      paramJsonPrimitive = (Number)paramJsonPrimitive;
      if (((paramJsonPrimitive instanceof BigInteger)) || ((paramJsonPrimitive instanceof Long)) || ((paramJsonPrimitive instanceof Integer)) || ((paramJsonPrimitive instanceof Short)) || ((paramJsonPrimitive instanceof Byte))) {
        bool1 = true;
      }
      return bool1;
    }
    return false;
  }
  
  private static boolean isPrimitiveOrString(Object paramObject)
  {
    if ((paramObject instanceof String)) {
      return true;
    }
    Class localClass = paramObject.getClass();
    paramObject = PRIMITIVE_TYPES;
    int j = paramObject.length;
    for (int i = 0; i < j; i++) {
      if (paramObject[i].isAssignableFrom(localClass)) {
        return true;
      }
    }
    return false;
  }
  
  public JsonPrimitive deepCopy()
  {
    return this;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool3 = true;
    boolean bool2 = true;
    boolean bool1 = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (JsonPrimitive)paramObject;
      if (this.value == null)
      {
        if (((JsonPrimitive)paramObject).value != null) {
          bool1 = false;
        }
        return bool1;
      }
      if ((isIntegral(this)) && (isIntegral((JsonPrimitive)paramObject)))
      {
        if (getAsNumber().longValue() == ((JsonPrimitive)paramObject).getAsNumber().longValue()) {
          bool1 = bool3;
        } else {
          bool1 = false;
        }
        return bool1;
      }
      if (((this.value instanceof Number)) && ((((JsonPrimitive)paramObject).value instanceof Number)))
      {
        double d2 = getAsNumber().doubleValue();
        double d1 = ((JsonPrimitive)paramObject).getAsNumber().doubleValue();
        bool1 = bool2;
        if (d2 != d1) {
          if ((Double.isNaN(d2)) && (Double.isNaN(d1))) {
            bool1 = bool2;
          } else {
            bool1 = false;
          }
        }
        return bool1;
      }
      return this.value.equals(((JsonPrimitive)paramObject).value);
    }
    return false;
  }
  
  public BigDecimal getAsBigDecimal()
  {
    Object localObject = this.value;
    if ((localObject instanceof BigDecimal)) {
      localObject = (BigDecimal)localObject;
    } else {
      localObject = new BigDecimal(localObject.toString());
    }
    return (BigDecimal)localObject;
  }
  
  public BigInteger getAsBigInteger()
  {
    Object localObject = this.value;
    if ((localObject instanceof BigInteger)) {
      localObject = (BigInteger)localObject;
    } else {
      localObject = new BigInteger(localObject.toString());
    }
    return (BigInteger)localObject;
  }
  
  public boolean getAsBoolean()
  {
    if (isBoolean()) {
      return getAsBooleanWrapper().booleanValue();
    }
    return Boolean.parseBoolean(getAsString());
  }
  
  Boolean getAsBooleanWrapper()
  {
    return (Boolean)this.value;
  }
  
  public byte getAsByte()
  {
    byte b;
    if (isNumber()) {
      b = getAsNumber().byteValue();
    } else {
      b = Byte.parseByte(getAsString());
    }
    return b;
  }
  
  public char getAsCharacter()
  {
    return getAsString().charAt(0);
  }
  
  public double getAsDouble()
  {
    double d;
    if (isNumber()) {
      d = getAsNumber().doubleValue();
    } else {
      d = Double.parseDouble(getAsString());
    }
    return d;
  }
  
  public float getAsFloat()
  {
    float f;
    if (isNumber()) {
      f = getAsNumber().floatValue();
    } else {
      f = Float.parseFloat(getAsString());
    }
    return f;
  }
  
  public int getAsInt()
  {
    int i;
    if (isNumber()) {
      i = getAsNumber().intValue();
    } else {
      i = Integer.parseInt(getAsString());
    }
    return i;
  }
  
  public long getAsLong()
  {
    long l;
    if (isNumber()) {
      l = getAsNumber().longValue();
    } else {
      l = Long.parseLong(getAsString());
    }
    return l;
  }
  
  public Number getAsNumber()
  {
    Object localObject = this.value;
    if ((localObject instanceof String)) {
      localObject = new LazilyParsedNumber((String)localObject);
    } else {
      localObject = (Number)localObject;
    }
    return (Number)localObject;
  }
  
  public short getAsShort()
  {
    short s;
    if (isNumber()) {
      s = getAsNumber().shortValue();
    } else {
      s = Short.parseShort(getAsString());
    }
    return s;
  }
  
  public String getAsString()
  {
    if (isNumber()) {
      return getAsNumber().toString();
    }
    if (isBoolean()) {
      return getAsBooleanWrapper().toString();
    }
    return (String)this.value;
  }
  
  public int hashCode()
  {
    if (this.value == null) {
      return 31;
    }
    long l;
    if (isIntegral(this))
    {
      l = getAsNumber().longValue();
      return (int)(l >>> 32 ^ l);
    }
    Object localObject = this.value;
    if ((localObject instanceof Number))
    {
      l = Double.doubleToLongBits(getAsNumber().doubleValue());
      return (int)(l >>> 32 ^ l);
    }
    return localObject.hashCode();
  }
  
  public boolean isBoolean()
  {
    return this.value instanceof Boolean;
  }
  
  public boolean isNumber()
  {
    return this.value instanceof Number;
  }
  
  public boolean isString()
  {
    return this.value instanceof String;
  }
  
  void setValue(Object paramObject)
  {
    if ((paramObject instanceof Character))
    {
      this.value = String.valueOf(((Character)paramObject).charValue());
    }
    else
    {
      boolean bool;
      if ((!(paramObject instanceof Number)) && (!isPrimitiveOrString(paramObject))) {
        bool = false;
      } else {
        bool = true;
      }
      .Gson.Preconditions.checkArgument(bool);
      this.value = paramObject;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/gson/JsonPrimitive.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */