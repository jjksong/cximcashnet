package com.google.gson;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public final class JsonParser
{
  /* Error */
  public JsonElement parse(JsonReader paramJsonReader)
    throws JsonIOException, JsonSyntaxException
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 25	com/google/gson/stream/JsonReader:isLenient	()Z
    //   4: istore_2
    //   5: aload_1
    //   6: iconst_1
    //   7: invokevirtual 29	com/google/gson/stream/JsonReader:setLenient	(Z)V
    //   10: aload_1
    //   11: invokestatic 33	com/google/gson/internal/Streams:parse	(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/JsonElement;
    //   14: astore_3
    //   15: aload_1
    //   16: iload_2
    //   17: invokevirtual 29	com/google/gson/stream/JsonReader:setLenient	(Z)V
    //   20: aload_3
    //   21: areturn
    //   22: astore_3
    //   23: goto +101 -> 124
    //   26: astore 4
    //   28: new 35	com/google/gson/JsonParseException
    //   31: astore 5
    //   33: new 37	java/lang/StringBuilder
    //   36: astore_3
    //   37: aload_3
    //   38: invokespecial 38	java/lang/StringBuilder:<init>	()V
    //   41: aload_3
    //   42: ldc 40
    //   44: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_3
    //   49: aload_1
    //   50: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload_3
    //   55: ldc 49
    //   57: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: pop
    //   61: aload 5
    //   63: aload_3
    //   64: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   67: aload 4
    //   69: invokespecial 56	com/google/gson/JsonParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   72: aload 5
    //   74: athrow
    //   75: astore 5
    //   77: new 35	com/google/gson/JsonParseException
    //   80: astore 4
    //   82: new 37	java/lang/StringBuilder
    //   85: astore_3
    //   86: aload_3
    //   87: invokespecial 38	java/lang/StringBuilder:<init>	()V
    //   90: aload_3
    //   91: ldc 40
    //   93: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_3
    //   98: aload_1
    //   99: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload_3
    //   104: ldc 49
    //   106: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: aload 4
    //   112: aload_3
    //   113: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: aload 5
    //   118: invokespecial 56	com/google/gson/JsonParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   121: aload 4
    //   123: athrow
    //   124: aload_1
    //   125: iload_2
    //   126: invokevirtual 29	com/google/gson/stream/JsonReader:setLenient	(Z)V
    //   129: aload_3
    //   130: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	131	0	this	JsonParser
    //   0	131	1	paramJsonReader	JsonReader
    //   4	122	2	bool	boolean
    //   14	7	3	localJsonElement	JsonElement
    //   22	1	3	localObject	Object
    //   36	94	3	localStringBuilder	StringBuilder
    //   26	42	4	localOutOfMemoryError	OutOfMemoryError
    //   80	42	4	localJsonParseException1	JsonParseException
    //   31	42	5	localJsonParseException2	JsonParseException
    //   75	42	5	localStackOverflowError	StackOverflowError
    // Exception table:
    //   from	to	target	type
    //   10	15	22	finally
    //   28	75	22	finally
    //   77	124	22	finally
    //   10	15	26	java/lang/OutOfMemoryError
    //   10	15	75	java/lang/StackOverflowError
  }
  
  public JsonElement parse(Reader paramReader)
    throws JsonIOException, JsonSyntaxException
  {
    try
    {
      JsonReader localJsonReader = new com/google/gson/stream/JsonReader;
      localJsonReader.<init>(paramReader);
      paramReader = parse(localJsonReader);
      if ((!paramReader.isJsonNull()) && (localJsonReader.peek() != JsonToken.END_DOCUMENT))
      {
        paramReader = new com/google/gson/JsonSyntaxException;
        paramReader.<init>("Did not consume the entire document.");
        throw paramReader;
      }
      return paramReader;
    }
    catch (NumberFormatException paramReader)
    {
      throw new JsonSyntaxException(paramReader);
    }
    catch (IOException paramReader)
    {
      throw new JsonIOException(paramReader);
    }
    catch (MalformedJsonException paramReader)
    {
      throw new JsonSyntaxException(paramReader);
    }
  }
  
  public JsonElement parse(String paramString)
    throws JsonSyntaxException
  {
    return parse(new StringReader(paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/gson/JsonParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */