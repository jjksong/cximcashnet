package com.google.gson;

import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import java.io.EOFException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser
  implements Iterator<JsonElement>
{
  private final Object lock;
  private final JsonReader parser;
  
  public JsonStreamParser(Reader paramReader)
  {
    this.parser = new JsonReader(paramReader);
    this.parser.setLenient(true);
    this.lock = new Object();
  }
  
  public JsonStreamParser(String paramString)
  {
    this(new StringReader(paramString));
  }
  
  /* Error */
  public boolean hasNext()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	com/google/gson/JsonStreamParser:lock	Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 22	com/google/gson/JsonStreamParser:parser	Lcom/google/gson/stream/JsonReader;
    //   11: invokevirtual 45	com/google/gson/stream/JsonReader:peek	()Lcom/google/gson/stream/JsonToken;
    //   14: astore_3
    //   15: getstatic 51	com/google/gson/stream/JsonToken:END_DOCUMENT	Lcom/google/gson/stream/JsonToken;
    //   18: astore 4
    //   20: aload_3
    //   21: aload 4
    //   23: if_acmpeq +8 -> 31
    //   26: iconst_1
    //   27: istore_1
    //   28: goto +5 -> 33
    //   31: iconst_0
    //   32: istore_1
    //   33: aload_2
    //   34: monitorexit
    //   35: iload_1
    //   36: ireturn
    //   37: astore_3
    //   38: goto +31 -> 69
    //   41: astore 4
    //   43: new 53	com/google/gson/JsonIOException
    //   46: astore_3
    //   47: aload_3
    //   48: aload 4
    //   50: invokespecial 56	com/google/gson/JsonIOException:<init>	(Ljava/lang/Throwable;)V
    //   53: aload_3
    //   54: athrow
    //   55: astore 4
    //   57: new 58	com/google/gson/JsonSyntaxException
    //   60: astore_3
    //   61: aload_3
    //   62: aload 4
    //   64: invokespecial 59	com/google/gson/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   67: aload_3
    //   68: athrow
    //   69: aload_2
    //   70: monitorexit
    //   71: aload_3
    //   72: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	73	0	this	JsonStreamParser
    //   27	9	1	bool	boolean
    //   4	66	2	localObject1	Object
    //   14	7	3	localJsonToken1	com.google.gson.stream.JsonToken
    //   37	1	3	localObject2	Object
    //   46	26	3	localObject3	Object
    //   18	4	4	localJsonToken2	com.google.gson.stream.JsonToken
    //   41	8	4	localIOException	java.io.IOException
    //   55	8	4	localMalformedJsonException	com.google.gson.stream.MalformedJsonException
    // Exception table:
    //   from	to	target	type
    //   7	20	37	finally
    //   33	35	37	finally
    //   43	55	37	finally
    //   57	69	37	finally
    //   69	71	37	finally
    //   7	20	41	java/io/IOException
    //   7	20	55	com/google/gson/stream/MalformedJsonException
  }
  
  public JsonElement next()
    throws JsonParseException
  {
    if (hasNext()) {
      try
      {
        localObject = Streams.parse(this.parser);
        return (JsonElement)localObject;
      }
      catch (JsonParseException localJsonParseException)
      {
        Object localObject = localJsonParseException;
        if ((localJsonParseException.getCause() instanceof EOFException)) {
          localObject = new NoSuchElementException();
        }
        throw ((Throwable)localObject);
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        throw new JsonParseException("Failed parsing JSON source to Json", localOutOfMemoryError);
      }
      catch (StackOverflowError localStackOverflowError)
      {
        throw new JsonParseException("Failed parsing JSON source to Json", localStackOverflowError);
      }
    }
    throw new NoSuchElementException();
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/gson/JsonStreamParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */