package com.google.zxing.multi.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.detector.FinderPattern;
import com.google.zxing.qrcode.detector.FinderPatternFinder;
import com.google.zxing.qrcode.detector.FinderPatternInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

final class MultiFinderPatternFinder
  extends FinderPatternFinder
{
  private static final float DIFF_MODSIZE_CUTOFF = 0.5F;
  private static final float DIFF_MODSIZE_CUTOFF_PERCENT = 0.05F;
  private static final FinderPatternInfo[] EMPTY_RESULT_ARRAY = new FinderPatternInfo[0];
  private static final float MAX_MODULE_COUNT_PER_EDGE = 180.0F;
  private static final float MIN_MODULE_COUNT_PER_EDGE = 9.0F;
  
  MultiFinderPatternFinder(BitMatrix paramBitMatrix)
  {
    super(paramBitMatrix);
  }
  
  MultiFinderPatternFinder(BitMatrix paramBitMatrix, ResultPointCallback paramResultPointCallback)
  {
    super(paramBitMatrix, paramResultPointCallback);
  }
  
  private FinderPattern[][] selectMutipleBestPatterns()
    throws NotFoundException
  {
    List localList = getPossibleCenters();
    int m = localList.size();
    if (m >= 3)
    {
      if (m == 3) {
        return new FinderPattern[][] { { (FinderPattern)localList.get(0), (FinderPattern)localList.get(1), (FinderPattern)localList.get(2) } };
      }
      Collections.sort(localList, new ModuleSizeComparator(null));
      ArrayList localArrayList = new ArrayList();
      for (int i = 0; i < m - 2; i++)
      {
        FinderPattern localFinderPattern2 = (FinderPattern)localList.get(i);
        if (localFinderPattern2 != null) {
          for (int j = i + 1; j < m - 1; j++)
          {
            FinderPattern localFinderPattern1 = (FinderPattern)localList.get(j);
            if (localFinderPattern1 != null)
            {
              float f1 = (localFinderPattern2.getEstimatedModuleSize() - localFinderPattern1.getEstimatedModuleSize()) / Math.min(localFinderPattern2.getEstimatedModuleSize(), localFinderPattern1.getEstimatedModuleSize());
              if ((Math.abs(localFinderPattern2.getEstimatedModuleSize() - localFinderPattern1.getEstimatedModuleSize()) > 0.5F) && (f1 >= 0.05F)) {
                break;
              }
              for (int k = j + 1; k < m; k++)
              {
                Object localObject = (FinderPattern)localList.get(k);
                if (localObject != null)
                {
                  f1 = (localFinderPattern1.getEstimatedModuleSize() - ((FinderPattern)localObject).getEstimatedModuleSize()) / Math.min(localFinderPattern1.getEstimatedModuleSize(), ((FinderPattern)localObject).getEstimatedModuleSize());
                  if ((Math.abs(localFinderPattern1.getEstimatedModuleSize() - ((FinderPattern)localObject).getEstimatedModuleSize()) > 0.5F) && (f1 >= 0.05F)) {
                    break;
                  }
                  FinderPattern[] arrayOfFinderPattern = new FinderPattern[3];
                  arrayOfFinderPattern[0] = localFinderPattern2;
                  arrayOfFinderPattern[1] = localFinderPattern1;
                  arrayOfFinderPattern[2] = localObject;
                  ResultPoint.orderBestPatterns(arrayOfFinderPattern);
                  localObject = new FinderPatternInfo(arrayOfFinderPattern);
                  float f3 = ResultPoint.distance(((FinderPatternInfo)localObject).getTopLeft(), ((FinderPatternInfo)localObject).getBottomLeft());
                  f1 = ResultPoint.distance(((FinderPatternInfo)localObject).getTopRight(), ((FinderPatternInfo)localObject).getBottomLeft());
                  float f2 = ResultPoint.distance(((FinderPatternInfo)localObject).getTopLeft(), ((FinderPatternInfo)localObject).getTopRight());
                  float f4 = (f3 + f2) / (localFinderPattern2.getEstimatedModuleSize() * 2.0F);
                  if ((f4 <= 180.0F) && (f4 >= 9.0F) && (Math.abs((f3 - f2) / Math.min(f3, f2)) < 0.1F))
                  {
                    f2 = (float)Math.sqrt(f3 * f3 + f2 * f2);
                    if (Math.abs((f1 - f2) / Math.min(f1, f2)) < 0.1F) {
                      localArrayList.add(arrayOfFinderPattern);
                    }
                  }
                }
              }
            }
          }
        }
      }
      if (!localArrayList.isEmpty()) {
        return (FinderPattern[][])localArrayList.toArray(new FinderPattern[localArrayList.size()][]);
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public FinderPatternInfo[] findMulti(Map<DecodeHintType, ?> paramMap)
    throws NotFoundException
  {
    int i1 = 0;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.TRY_HARDER))) {
      i = 1;
    } else {
      i = 0;
    }
    boolean bool;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.PURE_BARCODE))) {
      bool = true;
    } else {
      bool = false;
    }
    Object localObject = getImage();
    int i2 = ((BitMatrix)localObject).getHeight();
    int i3 = ((BitMatrix)localObject).getWidth();
    int k = (int)(i2 / 228.0F * 3.0F);
    if ((k < 3) || (i != 0)) {
      k = 3;
    }
    paramMap = new int[5];
    int m = k - 1;
    while (m < i2)
    {
      paramMap[0] = 0;
      paramMap[1] = 0;
      paramMap[2] = 0;
      paramMap[3] = 0;
      paramMap[4] = 0;
      int n = 0;
      i = 0;
      while (n < i3)
      {
        if (((BitMatrix)localObject).get(n, m))
        {
          j = i;
          if ((i & 0x1) == 1) {
            j = i + 1;
          }
          paramMap[j] += 1;
          i = j;
        }
        else if ((i & 0x1) == 0)
        {
          if (i == 4)
          {
            if ((foundPatternCross(paramMap)) && (handlePossibleCenter(paramMap, m, n, bool)))
            {
              paramMap[0] = 0;
              paramMap[1] = 0;
              paramMap[2] = 0;
              paramMap[3] = 0;
              paramMap[4] = 0;
              i = 0;
            }
            else
            {
              paramMap[0] = paramMap[2];
              paramMap[1] = paramMap[3];
              paramMap[2] = paramMap[4];
              paramMap[3] = 1;
              paramMap[4] = 0;
              i = 3;
            }
          }
          else
          {
            i++;
            paramMap[i] += 1;
          }
        }
        else
        {
          paramMap[i] += 1;
        }
        n++;
      }
      if (foundPatternCross(paramMap)) {
        handlePossibleCenter(paramMap, m, i3, bool);
      }
      m += k;
    }
    localObject = selectMutipleBestPatterns();
    ArrayList localArrayList = new ArrayList();
    int j = localObject.length;
    for (int i = i1; i < j; i++)
    {
      paramMap = localObject[i];
      ResultPoint.orderBestPatterns(paramMap);
      localArrayList.add(new FinderPatternInfo(paramMap));
    }
    if (localArrayList.isEmpty()) {
      return EMPTY_RESULT_ARRAY;
    }
    return (FinderPatternInfo[])localArrayList.toArray(new FinderPatternInfo[localArrayList.size()]);
  }
  
  private static final class ModuleSizeComparator
    implements Serializable, Comparator<FinderPattern>
  {
    public int compare(FinderPattern paramFinderPattern1, FinderPattern paramFinderPattern2)
    {
      double d = paramFinderPattern2.getEstimatedModuleSize() - paramFinderPattern1.getEstimatedModuleSize();
      if (d < 0.0D) {
        return -1;
      }
      if (d > 0.0D) {
        return 1;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */