package com.google.zxing.multi.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.qrcode.detector.Detector;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class MultiDetector
  extends Detector
{
  private static final DetectorResult[] EMPTY_DETECTOR_RESULTS = new DetectorResult[0];
  
  public MultiDetector(BitMatrix paramBitMatrix)
  {
    super(paramBitMatrix);
  }
  
  public DetectorResult[] detectMulti(Map<DecodeHintType, ?> paramMap)
    throws NotFoundException
  {
    BitMatrix localBitMatrix = getImage();
    if (paramMap == null) {
      localObject = null;
    } else {
      localObject = (ResultPointCallback)paramMap.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
    }
    Object localObject = new MultiFinderPatternFinder(localBitMatrix, (ResultPointCallback)localObject).findMulti(paramMap);
    int j;
    int i;
    if (localObject.length != 0)
    {
      paramMap = new ArrayList();
      j = localObject.length;
      i = 0;
    }
    for (;;)
    {
      if (i < j) {
        localBitMatrix = localObject[i];
      }
      try
      {
        paramMap.add(processFinderPatternInfo(localBitMatrix));
        i++;
        continue;
        if (paramMap.isEmpty()) {
          return EMPTY_DETECTOR_RESULTS;
        }
        return (DetectorResult[])paramMap.toArray(new DetectorResult[paramMap.size()]);
        throw NotFoundException.getNotFoundInstance();
      }
      catch (ReaderException localReaderException)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/multi/qrcode/detector/MultiDetector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */