package com.google.zxing;

public final class RGBLuminanceSource
  extends LuminanceSource
{
  private final int dataHeight;
  private final int dataWidth;
  private final int left;
  private final byte[] luminances;
  private final int top;
  
  public RGBLuminanceSource(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    super(paramInt1, paramInt2);
    this.dataWidth = paramInt1;
    this.dataHeight = paramInt2;
    int i = 0;
    this.left = 0;
    this.top = 0;
    paramInt2 = paramInt1 * paramInt2;
    this.luminances = new byte[paramInt2];
    for (paramInt1 = i; paramInt1 < paramInt2; paramInt1++)
    {
      i = paramArrayOfInt[paramInt1];
      this.luminances[paramInt1] = ((byte)(((i >> 16 & 0xFF) + (i >> 7 & 0x1FE) + (i & 0xFF)) / 4));
    }
  }
  
  private RGBLuminanceSource(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    super(paramInt5, paramInt6);
    if ((paramInt5 + paramInt3 <= paramInt1) && (paramInt6 + paramInt4 <= paramInt2))
    {
      this.luminances = paramArrayOfByte;
      this.dataWidth = paramInt1;
      this.dataHeight = paramInt2;
      this.left = paramInt3;
      this.top = paramInt4;
      return;
    }
    throw new IllegalArgumentException("Crop rectangle does not fit within image data.");
  }
  
  public LuminanceSource crop(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return new RGBLuminanceSource(this.luminances, this.dataWidth, this.dataHeight, this.left + paramInt1, this.top + paramInt2, paramInt3, paramInt4);
  }
  
  public byte[] getMatrix()
  {
    int n = getWidth();
    int m = getHeight();
    if ((n == this.dataWidth) && (m == this.dataHeight)) {
      return this.luminances;
    }
    int i1 = n * m;
    byte[] arrayOfByte = new byte[i1];
    int i = this.top;
    int i2 = this.dataWidth;
    int k = i * i2 + this.left;
    int j = 0;
    i = k;
    if (n == i2)
    {
      System.arraycopy(this.luminances, k, arrayOfByte, 0, i1);
      return arrayOfByte;
    }
    while (j < m)
    {
      System.arraycopy(this.luminances, i, arrayOfByte, j * n, n);
      i += this.dataWidth;
      j++;
    }
    return arrayOfByte;
  }
  
  public byte[] getRow(int paramInt, byte[] paramArrayOfByte)
  {
    if ((paramInt >= 0) && (paramInt < getHeight()))
    {
      int k = getWidth();
      byte[] arrayOfByte;
      if (paramArrayOfByte != null)
      {
        arrayOfByte = paramArrayOfByte;
        if (paramArrayOfByte.length >= k) {}
      }
      else
      {
        arrayOfByte = new byte[k];
      }
      int m = this.top;
      int i = this.dataWidth;
      int j = this.left;
      System.arraycopy(this.luminances, (paramInt + m) * i + j, arrayOfByte, 0, k);
      return arrayOfByte;
    }
    paramArrayOfByte = new StringBuilder("Requested row is outside the image: ");
    paramArrayOfByte.append(paramInt);
    throw new IllegalArgumentException(paramArrayOfByte.toString());
  }
  
  public boolean isCropSupported()
  {
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/RGBLuminanceSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */