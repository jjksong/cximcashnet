package com.google.zxing.integration.android;

public final class IntentResult
{
  private final String barcodeImagePath;
  private final String contents;
  private final String errorCorrectionLevel;
  private final String formatName;
  private final Integer orientation;
  private final byte[] rawBytes;
  
  IntentResult()
  {
    this(null, null, null, null, null, null);
  }
  
  IntentResult(String paramString1, String paramString2, byte[] paramArrayOfByte, Integer paramInteger, String paramString3, String paramString4)
  {
    this.contents = paramString1;
    this.formatName = paramString2;
    this.rawBytes = paramArrayOfByte;
    this.orientation = paramInteger;
    this.errorCorrectionLevel = paramString3;
    this.barcodeImagePath = paramString4;
  }
  
  public String getBarcodeImagePath()
  {
    return this.barcodeImagePath;
  }
  
  public String getContents()
  {
    return this.contents;
  }
  
  public String getErrorCorrectionLevel()
  {
    return this.errorCorrectionLevel;
  }
  
  public String getFormatName()
  {
    return this.formatName;
  }
  
  public Integer getOrientation()
  {
    return this.orientation;
  }
  
  public byte[] getRawBytes()
  {
    return this.rawBytes;
  }
  
  public String toString()
  {
    Object localObject = this.rawBytes;
    int i;
    if (localObject == null) {
      i = 0;
    } else {
      i = localObject.length;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Format: ");
    ((StringBuilder)localObject).append(this.formatName);
    ((StringBuilder)localObject).append('\n');
    ((StringBuilder)localObject).append("Contents: ");
    ((StringBuilder)localObject).append(this.contents);
    ((StringBuilder)localObject).append('\n');
    ((StringBuilder)localObject).append("Raw bytes: (");
    ((StringBuilder)localObject).append(i);
    ((StringBuilder)localObject).append(" bytes)\nOrientation: ");
    ((StringBuilder)localObject).append(this.orientation);
    ((StringBuilder)localObject).append('\n');
    ((StringBuilder)localObject).append("EC level: ");
    ((StringBuilder)localObject).append(this.errorCorrectionLevel);
    ((StringBuilder)localObject).append('\n');
    ((StringBuilder)localObject).append("Barcode image: ");
    ((StringBuilder)localObject).append(this.barcodeImagePath);
    ((StringBuilder)localObject).append('\n');
    return ((StringBuilder)localObject).toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/integration/android/IntentResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */