package com.google.zxing.integration.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.journeyapps.barcodescanner.CaptureActivity;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class IntentIntegrator
{
  public static final Collection<String> ALL_CODE_TYPES = null;
  public static final String CODE_128 = "CODE_128";
  public static final String CODE_39 = "CODE_39";
  public static final String CODE_93 = "CODE_93";
  public static final String DATA_MATRIX = "DATA_MATRIX";
  public static final String EAN_13 = "EAN_13";
  public static final String EAN_8 = "EAN_8";
  public static final String ITF = "ITF";
  public static final Collection<String> ONE_D_CODE_TYPES;
  public static final String PDF_417 = "PDF_417";
  public static final Collection<String> PRODUCT_CODE_TYPES = list(new String[] { "UPC_A", "UPC_E", "EAN_8", "EAN_13", "RSS_14" });
  public static final String QR_CODE = "QR_CODE";
  public static final int REQUEST_CODE = 49374;
  public static final String RSS_14 = "RSS_14";
  public static final String RSS_EXPANDED = "RSS_EXPANDED";
  private static final String TAG = "IntentIntegrator";
  public static final String UPC_A = "UPC_A";
  public static final String UPC_E = "UPC_E";
  private final Activity activity;
  private Class<?> captureActivity;
  private Collection<String> desiredBarcodeFormats;
  private android.app.Fragment fragment;
  private final Map<String, Object> moreExtras = new HashMap(3);
  private int requestCode = 49374;
  private android.support.v4.app.Fragment supportFragment;
  
  static
  {
    ONE_D_CODE_TYPES = list(new String[] { "UPC_A", "UPC_E", "EAN_8", "EAN_13", "RSS_14", "CODE_39", "CODE_93", "CODE_128", "ITF", "RSS_14", "RSS_EXPANDED" });
  }
  
  public IntentIntegrator(Activity paramActivity)
  {
    this.activity = paramActivity;
  }
  
  private void attachMoreExtras(Intent paramIntent)
  {
    Iterator localIterator = this.moreExtras.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = ((Map.Entry)localObject).getValue();
      if ((localObject instanceof Integer)) {
        paramIntent.putExtra(str, (Integer)localObject);
      } else if ((localObject instanceof Long)) {
        paramIntent.putExtra(str, (Long)localObject);
      } else if ((localObject instanceof Boolean)) {
        paramIntent.putExtra(str, (Boolean)localObject);
      } else if ((localObject instanceof Double)) {
        paramIntent.putExtra(str, (Double)localObject);
      } else if ((localObject instanceof Float)) {
        paramIntent.putExtra(str, (Float)localObject);
      } else if ((localObject instanceof Bundle)) {
        paramIntent.putExtra(str, (Bundle)localObject);
      } else {
        paramIntent.putExtra(str, localObject.toString());
      }
    }
  }
  
  @TargetApi(11)
  public static IntentIntegrator forFragment(android.app.Fragment paramFragment)
  {
    IntentIntegrator localIntentIntegrator = new IntentIntegrator(paramFragment.getActivity());
    localIntentIntegrator.fragment = paramFragment;
    return localIntentIntegrator;
  }
  
  public static IntentIntegrator forSupportFragment(android.support.v4.app.Fragment paramFragment)
  {
    IntentIntegrator localIntentIntegrator = new IntentIntegrator(paramFragment.getActivity());
    localIntentIntegrator.supportFragment = paramFragment;
    return localIntentIntegrator;
  }
  
  private static List<String> list(String... paramVarArgs)
  {
    return Collections.unmodifiableList(Arrays.asList(paramVarArgs));
  }
  
  public static IntentResult parseActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 49374) {
      return parseActivityResult(paramInt2, paramIntent);
    }
    return null;
  }
  
  public static IntentResult parseActivityResult(int paramInt, Intent paramIntent)
  {
    if (paramInt == -1)
    {
      String str1 = paramIntent.getStringExtra("SCAN_RESULT");
      String str2 = paramIntent.getStringExtra("SCAN_RESULT_FORMAT");
      byte[] arrayOfByte = paramIntent.getByteArrayExtra("SCAN_RESULT_BYTES");
      paramInt = paramIntent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
      Integer localInteger;
      if (paramInt == Integer.MIN_VALUE) {
        localInteger = null;
      } else {
        localInteger = Integer.valueOf(paramInt);
      }
      return new IntentResult(str1, str2, arrayOfByte, localInteger, paramIntent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL"), paramIntent.getStringExtra("SCAN_RESULT_IMAGE_PATH"));
    }
    return new IntentResult();
  }
  
  public final IntentIntegrator addExtra(String paramString, Object paramObject)
  {
    this.moreExtras.put(paramString, paramObject);
    return this;
  }
  
  public Intent createScanIntent()
  {
    Intent localIntent = new Intent(this.activity, getCaptureActivity());
    localIntent.setAction("com.google.zxing.client.android.SCAN");
    if (this.desiredBarcodeFormats != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = this.desiredBarcodeFormats.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (localStringBuilder.length() > 0) {
          localStringBuilder.append(',');
        }
        localStringBuilder.append(str);
      }
      localIntent.putExtra("SCAN_FORMATS", localStringBuilder.toString());
    }
    localIntent.addFlags(67108864);
    localIntent.addFlags(524288);
    attachMoreExtras(localIntent);
    return localIntent;
  }
  
  public Class<?> getCaptureActivity()
  {
    if (this.captureActivity == null) {
      this.captureActivity = getDefaultCaptureActivity();
    }
    return this.captureActivity;
  }
  
  protected Class<?> getDefaultCaptureActivity()
  {
    return CaptureActivity.class;
  }
  
  public Map<String, ?> getMoreExtras()
  {
    return this.moreExtras;
  }
  
  public final void initiateScan()
  {
    startActivityForResult(createScanIntent(), this.requestCode);
  }
  
  public final void initiateScan(Collection<String> paramCollection)
  {
    setDesiredBarcodeFormats(paramCollection);
    initiateScan();
  }
  
  public IntentIntegrator setBarcodeImageEnabled(boolean paramBoolean)
  {
    addExtra("BARCODE_IMAGE_ENABLED", Boolean.valueOf(paramBoolean));
    return this;
  }
  
  public IntentIntegrator setBeepEnabled(boolean paramBoolean)
  {
    addExtra("BEEP_ENABLED", Boolean.valueOf(paramBoolean));
    return this;
  }
  
  public IntentIntegrator setCameraId(int paramInt)
  {
    if (paramInt >= 0) {
      addExtra("SCAN_CAMERA_ID", Integer.valueOf(paramInt));
    }
    return this;
  }
  
  public IntentIntegrator setCaptureActivity(Class<?> paramClass)
  {
    this.captureActivity = paramClass;
    return this;
  }
  
  public IntentIntegrator setDesiredBarcodeFormats(Collection<String> paramCollection)
  {
    this.desiredBarcodeFormats = paramCollection;
    return this;
  }
  
  public IntentIntegrator setDesiredBarcodeFormats(String... paramVarArgs)
  {
    this.desiredBarcodeFormats = Arrays.asList(paramVarArgs);
    return this;
  }
  
  public IntentIntegrator setOrientationLocked(boolean paramBoolean)
  {
    addExtra("SCAN_ORIENTATION_LOCKED", Boolean.valueOf(paramBoolean));
    return this;
  }
  
  public final IntentIntegrator setPrompt(String paramString)
  {
    if (paramString != null) {
      addExtra("PROMPT_MESSAGE", paramString);
    }
    return this;
  }
  
  public IntentIntegrator setRequestCode(int paramInt)
  {
    if ((paramInt > 0) && (paramInt <= 65535))
    {
      this.requestCode = paramInt;
      return this;
    }
    throw new IllegalArgumentException("requestCode out of range");
  }
  
  public IntentIntegrator setTimeout(long paramLong)
  {
    addExtra("TIMEOUT", Long.valueOf(paramLong));
    return this;
  }
  
  protected void startActivity(Intent paramIntent)
  {
    Object localObject = this.fragment;
    if (localObject != null)
    {
      ((android.app.Fragment)localObject).startActivity(paramIntent);
    }
    else
    {
      localObject = this.supportFragment;
      if (localObject != null) {
        ((android.support.v4.app.Fragment)localObject).startActivity(paramIntent);
      } else {
        this.activity.startActivity(paramIntent);
      }
    }
  }
  
  protected void startActivityForResult(Intent paramIntent, int paramInt)
  {
    Object localObject = this.fragment;
    if (localObject != null)
    {
      ((android.app.Fragment)localObject).startActivityForResult(paramIntent, paramInt);
    }
    else
    {
      localObject = this.supportFragment;
      if (localObject != null) {
        ((android.support.v4.app.Fragment)localObject).startActivityForResult(paramIntent, paramInt);
      } else {
        this.activity.startActivityForResult(paramIntent, paramInt);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/integration/android/IntentIntegrator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */