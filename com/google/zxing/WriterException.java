package com.google.zxing;

public final class WriterException
  extends Exception
{
  public WriterException() {}
  
  public WriterException(String paramString)
  {
    super(paramString);
  }
  
  public WriterException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/WriterException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */