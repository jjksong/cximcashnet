package com.google.zxing;

public final class InvertedLuminanceSource
  extends LuminanceSource
{
  private final LuminanceSource delegate;
  
  public InvertedLuminanceSource(LuminanceSource paramLuminanceSource)
  {
    super(paramLuminanceSource.getWidth(), paramLuminanceSource.getHeight());
    this.delegate = paramLuminanceSource;
  }
  
  public LuminanceSource crop(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return new InvertedLuminanceSource(this.delegate.crop(paramInt1, paramInt2, paramInt3, paramInt4));
  }
  
  public byte[] getMatrix()
  {
    byte[] arrayOfByte2 = this.delegate.getMatrix();
    int j = getWidth() * getHeight();
    byte[] arrayOfByte1 = new byte[j];
    for (int i = 0; i < j; i++) {
      arrayOfByte1[i] = ((byte)(255 - (arrayOfByte2[i] & 0xFF)));
    }
    return arrayOfByte1;
  }
  
  public byte[] getRow(int paramInt, byte[] paramArrayOfByte)
  {
    paramArrayOfByte = this.delegate.getRow(paramInt, paramArrayOfByte);
    int i = getWidth();
    for (paramInt = 0; paramInt < i; paramInt++) {
      paramArrayOfByte[paramInt] = ((byte)(255 - (paramArrayOfByte[paramInt] & 0xFF)));
    }
    return paramArrayOfByte;
  }
  
  public LuminanceSource invert()
  {
    return this.delegate;
  }
  
  public boolean isCropSupported()
  {
    return this.delegate.isCropSupported();
  }
  
  public boolean isRotateSupported()
  {
    return this.delegate.isRotateSupported();
  }
  
  public LuminanceSource rotateCounterClockwise()
  {
    return new InvertedLuminanceSource(this.delegate.rotateCounterClockwise());
  }
  
  public LuminanceSource rotateCounterClockwise45()
  {
    return new InvertedLuminanceSource(this.delegate.rotateCounterClockwise45());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/InvertedLuminanceSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */