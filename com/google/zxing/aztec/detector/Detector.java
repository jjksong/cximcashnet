package com.google.zxing.aztec.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.aztec.AztecDetectorResult;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.GridSampler;
import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.common.detector.WhiteRectangleDetector;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import com.google.zxing.common.reedsolomon.ReedSolomonException;

public final class Detector
{
  private static final int[] EXPECTED_CORNER_BITS = { 3808, 476, 2107, 1799 };
  private boolean compact;
  private final BitMatrix image;
  private int nbCenterLayers;
  private int nbDataBlocks;
  private int nbLayers;
  private int shift;
  
  public Detector(BitMatrix paramBitMatrix)
  {
    this.image = paramBitMatrix;
  }
  
  private static float distance(ResultPoint paramResultPoint1, ResultPoint paramResultPoint2)
  {
    return MathUtils.distance(paramResultPoint1.getX(), paramResultPoint1.getY(), paramResultPoint2.getX(), paramResultPoint2.getY());
  }
  
  private static float distance(Point paramPoint1, Point paramPoint2)
  {
    return MathUtils.distance(paramPoint1.getX(), paramPoint1.getY(), paramPoint2.getX(), paramPoint2.getY());
  }
  
  private static ResultPoint[] expandSquare(ResultPoint[] paramArrayOfResultPoint, float paramFloat1, float paramFloat2)
  {
    paramFloat1 = paramFloat2 / (paramFloat1 * 2.0F);
    float f4 = paramArrayOfResultPoint[0].getX();
    float f5 = paramArrayOfResultPoint[2].getX();
    float f2 = paramArrayOfResultPoint[0].getY();
    float f3 = paramArrayOfResultPoint[2].getY();
    float f1 = (paramArrayOfResultPoint[0].getX() + paramArrayOfResultPoint[2].getX()) / 2.0F;
    paramFloat2 = (paramArrayOfResultPoint[0].getY() + paramArrayOfResultPoint[2].getY()) / 2.0F;
    f4 = (f4 - f5) * paramFloat1;
    f2 = (f2 - f3) * paramFloat1;
    ResultPoint localResultPoint1 = new ResultPoint(f1 + f4, paramFloat2 + f2);
    ResultPoint localResultPoint2 = new ResultPoint(f1 - f4, paramFloat2 - f2);
    f4 = paramArrayOfResultPoint[1].getX();
    f5 = paramArrayOfResultPoint[3].getX();
    f2 = paramArrayOfResultPoint[1].getY();
    f3 = paramArrayOfResultPoint[3].getY();
    f1 = (paramArrayOfResultPoint[1].getX() + paramArrayOfResultPoint[3].getX()) / 2.0F;
    paramFloat2 = (paramArrayOfResultPoint[1].getY() + paramArrayOfResultPoint[3].getY()) / 2.0F;
    f4 = (f4 - f5) * paramFloat1;
    paramFloat1 *= (f2 - f3);
    return new ResultPoint[] { localResultPoint1, new ResultPoint(f1 + f4, paramFloat2 + paramFloat1), localResultPoint2, new ResultPoint(f1 - f4, paramFloat2 - paramFloat1) };
  }
  
  private void extractParameters(ResultPoint[] paramArrayOfResultPoint)
    throws NotFoundException
  {
    int i = 0;
    if ((isValid(paramArrayOfResultPoint[0])) && (isValid(paramArrayOfResultPoint[1])) && (isValid(paramArrayOfResultPoint[2])) && (isValid(paramArrayOfResultPoint[3])))
    {
      int j = this.nbCenterLayers * 2;
      int[] arrayOfInt = new int[4];
      arrayOfInt[0] = sampleLine(paramArrayOfResultPoint[0], paramArrayOfResultPoint[1], j);
      arrayOfInt[1] = sampleLine(paramArrayOfResultPoint[1], paramArrayOfResultPoint[2], j);
      arrayOfInt[2] = sampleLine(paramArrayOfResultPoint[2], paramArrayOfResultPoint[3], j);
      arrayOfInt[3] = sampleLine(paramArrayOfResultPoint[3], paramArrayOfResultPoint[0], j);
      this.shift = getRotation(arrayOfInt, j);
      long l = 0L;
      while (i < 4)
      {
        j = arrayOfInt[((this.shift + i) % 4)];
        if (this.compact) {
          l = (l << 7) + (j >> 1 & 0x7F);
        } else {
          l = (l << 10) + ((j >> 2 & 0x3E0) + (j >> 1 & 0x1F));
        }
        i++;
      }
      i = getCorrectedParameterData(l, this.compact);
      if (this.compact)
      {
        this.nbLayers = ((i >> 6) + 1);
        this.nbDataBlocks = ((i & 0x3F) + 1);
        return;
      }
      this.nbLayers = ((i >> 11) + 1);
      this.nbDataBlocks = ((i & 0x7FF) + 1);
      return;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private ResultPoint[] getBullsEyeCorners(Point paramPoint)
    throws NotFoundException
  {
    this.nbCenterLayers = 1;
    Object localObject1 = paramPoint;
    Object localObject2 = localObject1;
    Object localObject3 = localObject2;
    boolean bool = true;
    while (this.nbCenterLayers < 9)
    {
      Point localPoint3 = getFirstDifferent(paramPoint, bool, 1, -1);
      Point localPoint2 = getFirstDifferent((Point)localObject1, bool, 1, 1);
      Point localPoint1 = getFirstDifferent((Point)localObject2, bool, -1, 1);
      Point localPoint4 = getFirstDifferent((Point)localObject3, bool, -1, -1);
      if (this.nbCenterLayers > 2)
      {
        double d = distance(localPoint4, localPoint3) * this.nbCenterLayers / (distance((Point)localObject3, paramPoint) * (this.nbCenterLayers + 2));
        if ((d < 0.75D) || (d > 1.25D) || (!isWhiteOrBlackRectangle(localPoint3, localPoint2, localPoint1, localPoint4))) {
          break;
        }
      }
      bool ^= true;
      this.nbCenterLayers += 1;
      localObject3 = localPoint4;
      paramPoint = localPoint3;
      localObject1 = localPoint2;
      localObject2 = localPoint1;
    }
    int i = this.nbCenterLayers;
    if ((i != 5) && (i != 7)) {
      throw NotFoundException.getNotFoundInstance();
    }
    if (this.nbCenterLayers == 5) {
      bool = true;
    } else {
      bool = false;
    }
    this.compact = bool;
    paramPoint = new ResultPoint(paramPoint.getX() + 0.5F, paramPoint.getY() - 0.5F);
    localObject1 = new ResultPoint(((Point)localObject1).getX() + 0.5F, ((Point)localObject1).getY() + 0.5F);
    localObject2 = new ResultPoint(((Point)localObject2).getX() - 0.5F, ((Point)localObject2).getY() + 0.5F);
    localObject3 = new ResultPoint(((Point)localObject3).getX() - 0.5F, ((Point)localObject3).getY() - 0.5F);
    i = this.nbCenterLayers;
    float f2 = i * 2 - 3;
    float f1 = i * 2;
    return expandSquare(new ResultPoint[] { paramPoint, localObject1, localObject2, localObject3 }, f2, f1);
  }
  
  private int getColor(Point paramPoint1, Point paramPoint2)
  {
    float f3 = distance(paramPoint1, paramPoint2);
    float f5 = (paramPoint2.getX() - paramPoint1.getX()) / f3;
    float f4 = (paramPoint2.getY() - paramPoint1.getY()) / f3;
    float f1 = paramPoint1.getX();
    float f2 = paramPoint1.getY();
    boolean bool2 = this.image.get(paramPoint1.getX(), paramPoint1.getY());
    int m = (int)Math.ceil(f3);
    boolean bool1 = false;
    int k = 0;
    int i;
    for (int j = 0; k < m; j = i)
    {
      f1 += f5;
      f2 += f4;
      i = j;
      if (this.image.get(MathUtils.round(f1), MathUtils.round(f2)) != bool2) {
        i = j + 1;
      }
      k++;
    }
    f1 = j / f3;
    if ((f1 > 0.1F) && (f1 < 0.9F)) {
      return 0;
    }
    if (f1 <= 0.1F) {
      bool1 = true;
    }
    if (bool1 == bool2) {
      return 1;
    }
    return -1;
  }
  
  private static int getCorrectedParameterData(long paramLong, boolean paramBoolean)
    throws NotFoundException
  {
    int j;
    int i;
    if (paramBoolean)
    {
      j = 7;
      i = 2;
    }
    else
    {
      j = 10;
      i = 4;
    }
    int[] arrayOfInt = new int[j];
    for (int k = j - 1; k >= 0; k--)
    {
      arrayOfInt[k] = ((int)paramLong & 0xF);
      paramLong >>= 4;
    }
    try
    {
      ReedSolomonDecoder localReedSolomonDecoder = new com/google/zxing/common/reedsolomon/ReedSolomonDecoder;
      localReedSolomonDecoder.<init>(GenericGF.AZTEC_PARAM);
      localReedSolomonDecoder.decode(arrayOfInt, j - i);
      k = 0;
      j = 0;
      while (k < i)
      {
        j = (j << 4) + arrayOfInt[k];
        k++;
      }
      return j;
    }
    catch (ReedSolomonException localReedSolomonException)
    {
      throw NotFoundException.getNotFoundInstance();
    }
  }
  
  private int getDimension()
  {
    if (this.compact) {
      return this.nbLayers * 4 + 11;
    }
    int i = this.nbLayers;
    if (i <= 4) {
      return i * 4 + 15;
    }
    return i * 4 + ((i - 4) / 8 + 1) * 2 + 15;
  }
  
  private Point getFirstDifferent(Point paramPoint, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    int j = paramPoint.getX() + paramInt1;
    int i = paramPoint.getY() + paramInt2;
    while ((isValid(j, i)) && (this.image.get(j, i) == paramBoolean))
    {
      j += paramInt1;
      i += paramInt2;
    }
    int k = j - paramInt1;
    j = i - paramInt2;
    i = k;
    while ((isValid(i, j)) && (this.image.get(i, j) == paramBoolean)) {
      i += paramInt1;
    }
    i -= paramInt1;
    paramInt1 = j;
    while ((isValid(i, paramInt1)) && (this.image.get(i, paramInt1) == paramBoolean)) {
      paramInt1 += paramInt2;
    }
    return new Point(i, paramInt1 - paramInt2);
  }
  
  private Point getMatrixCenter()
  {
    Object localObject2;
    ResultPoint localResultPoint2;
    ResultPoint localResultPoint3;
    int k;
    int i;
    Object localObject1;
    try
    {
      WhiteRectangleDetector localWhiteRectangleDetector = new com/google/zxing/common/detector/WhiteRectangleDetector;
      localWhiteRectangleDetector.<init>(this.image);
      localObject2 = localWhiteRectangleDetector.detect();
      localResultPoint2 = localObject2[0];
      localWhiteRectangleDetector = localObject2[1];
      localResultPoint3 = localObject2[2];
      localObject2 = localObject2[3];
    }
    catch (NotFoundException localNotFoundException1)
    {
      j = this.image.getWidth() / 2;
      k = this.image.getHeight() / 2;
      m = j + 7;
      i = k - 7;
      localResultPoint2 = getFirstDifferent(new Point(m, i), false, 1, -1).toResultPoint();
      k += 7;
      localObject1 = getFirstDifferent(new Point(m, k), false, 1, 1).toResultPoint();
      j -= 7;
      localResultPoint3 = getFirstDifferent(new Point(j, k), false, -1, 1).toResultPoint();
      localObject2 = getFirstDifferent(new Point(j, i), false, -1, -1).toResultPoint();
    }
    int j = MathUtils.round((localResultPoint2.getX() + ((ResultPoint)localObject2).getX() + ((ResultPoint)localObject1).getX() + localResultPoint3.getX()) / 4.0F);
    int m = MathUtils.round((localResultPoint2.getY() + ((ResultPoint)localObject2).getY() + ((ResultPoint)localObject1).getY() + localResultPoint3.getY()) / 4.0F);
    ResultPoint localResultPoint1;
    try
    {
      localObject1 = new com/google/zxing/common/detector/WhiteRectangleDetector;
      ((WhiteRectangleDetector)localObject1).<init>(this.image, 15, j, m);
      localObject2 = ((WhiteRectangleDetector)localObject1).detect();
      localResultPoint2 = localObject2[0];
      localObject1 = localObject2[1];
      localResultPoint3 = localObject2[2];
      localObject2 = localObject2[3];
    }
    catch (NotFoundException localNotFoundException2)
    {
      k = j + 7;
      i = m - 7;
      localResultPoint2 = getFirstDifferent(new Point(k, i), false, 1, -1).toResultPoint();
      m += 7;
      localResultPoint1 = getFirstDifferent(new Point(k, m), false, 1, 1).toResultPoint();
      j -= 7;
      localResultPoint3 = getFirstDifferent(new Point(j, m), false, -1, 1).toResultPoint();
      localObject2 = getFirstDifferent(new Point(j, i), false, -1, -1).toResultPoint();
    }
    return new Point(MathUtils.round((localResultPoint2.getX() + ((ResultPoint)localObject2).getX() + localResultPoint1.getX() + localResultPoint3.getX()) / 4.0F), MathUtils.round((localResultPoint2.getY() + ((ResultPoint)localObject2).getY() + localResultPoint1.getY() + localResultPoint3.getY()) / 4.0F));
  }
  
  private ResultPoint[] getMatrixCornerPoints(ResultPoint[] paramArrayOfResultPoint)
  {
    return expandSquare(paramArrayOfResultPoint, this.nbCenterLayers * 2, getDimension());
  }
  
  private static int getRotation(int[] paramArrayOfInt, int paramInt)
    throws NotFoundException
  {
    int m = paramArrayOfInt.length;
    int k = 0;
    int j = 0;
    int i = 0;
    while (j < m)
    {
      int n = paramArrayOfInt[j];
      i = (i << 3) + ((n >> paramInt - 2 << 1) + (n & 0x1));
      j++;
    }
    for (paramInt = k; paramInt < 4; paramInt++) {
      if (Integer.bitCount(EXPECTED_CORNER_BITS[paramInt] ^ ((i & 0x1) << 11) + (i >> 1)) <= 2) {
        return paramInt;
      }
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private boolean isValid(int paramInt1, int paramInt2)
  {
    return (paramInt1 >= 0) && (paramInt1 < this.image.getWidth()) && (paramInt2 > 0) && (paramInt2 < this.image.getHeight());
  }
  
  private boolean isValid(ResultPoint paramResultPoint)
  {
    return isValid(MathUtils.round(paramResultPoint.getX()), MathUtils.round(paramResultPoint.getY()));
  }
  
  private boolean isWhiteOrBlackRectangle(Point paramPoint1, Point paramPoint2, Point paramPoint3, Point paramPoint4)
  {
    paramPoint1 = new Point(paramPoint1.getX() - 3, paramPoint1.getY() + 3);
    paramPoint2 = new Point(paramPoint2.getX() - 3, paramPoint2.getY() - 3);
    paramPoint3 = new Point(paramPoint3.getX() + 3, paramPoint3.getY() - 3);
    paramPoint4 = new Point(paramPoint4.getX() + 3, paramPoint4.getY() + 3);
    int i = getColor(paramPoint4, paramPoint1);
    if (i == 0) {
      return false;
    }
    if (getColor(paramPoint1, paramPoint2) != i) {
      return false;
    }
    if (getColor(paramPoint2, paramPoint3) != i) {
      return false;
    }
    return getColor(paramPoint3, paramPoint4) == i;
  }
  
  private BitMatrix sampleGrid(BitMatrix paramBitMatrix, ResultPoint paramResultPoint1, ResultPoint paramResultPoint2, ResultPoint paramResultPoint3, ResultPoint paramResultPoint4)
    throws NotFoundException
  {
    GridSampler localGridSampler = GridSampler.getInstance();
    int j = getDimension();
    float f2 = j / 2.0F;
    int i = this.nbCenterLayers;
    float f1 = f2 - i;
    f2 += i;
    return localGridSampler.sampleGrid(paramBitMatrix, j, j, f1, f1, f2, f1, f2, f2, f1, f2, paramResultPoint1.getX(), paramResultPoint1.getY(), paramResultPoint2.getX(), paramResultPoint2.getY(), paramResultPoint3.getX(), paramResultPoint3.getY(), paramResultPoint4.getX(), paramResultPoint4.getY());
  }
  
  private int sampleLine(ResultPoint paramResultPoint1, ResultPoint paramResultPoint2, int paramInt)
  {
    float f5 = distance(paramResultPoint1, paramResultPoint2);
    float f4 = f5 / paramInt;
    float f3 = paramResultPoint1.getX();
    float f1 = paramResultPoint1.getY();
    float f2 = (paramResultPoint2.getX() - paramResultPoint1.getX()) * f4 / f5;
    f4 = f4 * (paramResultPoint2.getY() - paramResultPoint1.getY()) / f5;
    int i = 0;
    int k;
    for (int j = 0; i < paramInt; j = k)
    {
      paramResultPoint1 = this.image;
      f5 = i;
      k = j;
      if (paramResultPoint1.get(MathUtils.round(f5 * f2 + f3), MathUtils.round(f5 * f4 + f1))) {
        k = j | 1 << paramInt - i - 1;
      }
      i++;
    }
    return j;
  }
  
  public AztecDetectorResult detect()
    throws NotFoundException
  {
    return detect(false);
  }
  
  public AztecDetectorResult detect(boolean paramBoolean)
    throws NotFoundException
  {
    ResultPoint[] arrayOfResultPoint = getBullsEyeCorners(getMatrixCenter());
    if (paramBoolean)
    {
      localObject = arrayOfResultPoint[0];
      arrayOfResultPoint[0] = arrayOfResultPoint[2];
      arrayOfResultPoint[2] = localObject;
    }
    extractParameters(arrayOfResultPoint);
    Object localObject = this.image;
    int i = this.shift;
    return new AztecDetectorResult(sampleGrid((BitMatrix)localObject, arrayOfResultPoint[(i % 4)], arrayOfResultPoint[((i + 1) % 4)], arrayOfResultPoint[((i + 2) % 4)], arrayOfResultPoint[((i + 3) % 4)]), getMatrixCornerPoints(arrayOfResultPoint), this.compact, this.nbDataBlocks, this.nbLayers);
  }
  
  static final class Point
  {
    private final int x;
    private final int y;
    
    Point(int paramInt1, int paramInt2)
    {
      this.x = paramInt1;
      this.y = paramInt2;
    }
    
    int getX()
    {
      return this.x;
    }
    
    int getY()
    {
      return this.y;
    }
    
    ResultPoint toResultPoint()
    {
      return new ResultPoint(getX(), getY());
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("<");
      localStringBuilder.append(this.x);
      localStringBuilder.append(' ');
      localStringBuilder.append(this.y);
      localStringBuilder.append('>');
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/detector/Detector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */