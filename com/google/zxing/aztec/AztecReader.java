package com.google.zxing.aztec;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.aztec.decoder.Decoder;
import com.google.zxing.aztec.detector.Detector;
import com.google.zxing.common.DecoderResult;
import java.util.Map;

public final class AztecReader
  implements Reader
{
  public Result decode(BinaryBitmap paramBinaryBitmap)
    throws NotFoundException, FormatException
  {
    return decode(paramBinaryBitmap, null);
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, FormatException
  {
    Object localObject5 = new Detector(paramBinaryBitmap.getBlackMatrix());
    int i = 0;
    Object localObject4 = null;
    Object localObject3;
    try
    {
      localObject3 = ((Detector)localObject5).detect(false);
      paramBinaryBitmap = ((AztecDetectorResult)localObject3).getPoints();
      try
      {
        Object localObject1 = new com/google/zxing/aztec/decoder/Decoder;
        ((Decoder)localObject1).<init>();
        localObject1 = ((Decoder)localObject1).decode((AztecDetectorResult)localObject3);
        localObject3 = null;
        localObject4 = localObject1;
        localObject1 = localObject3;
      }
      catch (FormatException localFormatException1) {}catch (NotFoundException localNotFoundException1) {}
      Object localObject2;
      localObject3 = null;
    }
    catch (FormatException localFormatException2)
    {
      paramBinaryBitmap = null;
      localObject3 = localFormatException2;
      localObject2 = null;
    }
    catch (NotFoundException localNotFoundException2)
    {
      paramBinaryBitmap = null;
    }
    if (localObject4 == null)
    {
      try
      {
        localObject4 = ((Detector)localObject5).detect(true);
        paramBinaryBitmap = ((AztecDetectorResult)localObject4).getPoints();
        localObject5 = new com/google/zxing/aztec/decoder/Decoder;
        ((Decoder)localObject5).<init>();
        localObject4 = ((Decoder)localObject5).decode((AztecDetectorResult)localObject4);
      }
      catch (FormatException paramBinaryBitmap) {}catch (NotFoundException paramBinaryBitmap) {}
      if (localNotFoundException2 == null)
      {
        if (localObject3 != null) {
          throw ((Throwable)localObject3);
        }
        throw paramBinaryBitmap;
      }
      throw localNotFoundException2;
    }
    if (paramMap != null)
    {
      paramMap = (ResultPointCallback)paramMap.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
      if (paramMap != null)
      {
        int j = paramBinaryBitmap.length;
        while (i < j)
        {
          paramMap.foundPossibleResultPoint(paramBinaryBitmap[i]);
          i++;
        }
      }
    }
    paramBinaryBitmap = new Result(((DecoderResult)localObject4).getText(), ((DecoderResult)localObject4).getRawBytes(), ((DecoderResult)localObject4).getNumBits(), paramBinaryBitmap, BarcodeFormat.AZTEC, System.currentTimeMillis());
    paramMap = ((DecoderResult)localObject4).getByteSegments();
    if (paramMap != null) {
      paramBinaryBitmap.putMetadata(ResultMetadataType.BYTE_SEGMENTS, paramMap);
    }
    paramMap = ((DecoderResult)localObject4).getECLevel();
    if (paramMap != null) {
      paramBinaryBitmap.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL, paramMap);
    }
    return paramBinaryBitmap;
  }
  
  public void reset() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/AztecReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */