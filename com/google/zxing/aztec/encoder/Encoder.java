package com.google.zxing.aztec.encoder;

import com.google.zxing.common.BitArray;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonEncoder;

public final class Encoder
{
  public static final int DEFAULT_AZTEC_LAYERS = 0;
  public static final int DEFAULT_EC_PERCENT = 33;
  private static final int MAX_NB_BITS = 32;
  private static final int MAX_NB_BITS_COMPACT = 4;
  private static final int[] WORD_SIZE = { 4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12 };
  
  private static int[] bitsToWords(BitArray paramBitArray, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = new int[paramInt2];
    int m = paramBitArray.getSize() / paramInt1;
    for (paramInt2 = 0; paramInt2 < m; paramInt2++)
    {
      int j = 0;
      int i = 0;
      while (j < paramInt1)
      {
        int k;
        if (paramBitArray.get(paramInt2 * paramInt1 + j)) {
          k = 1 << paramInt1 - j - 1;
        } else {
          k = 0;
        }
        i |= k;
        j++;
      }
      arrayOfInt[paramInt2] = i;
    }
    return arrayOfInt;
  }
  
  private static void drawBullsEye(BitMatrix paramBitMatrix, int paramInt1, int paramInt2)
  {
    for (int i = 0; i < paramInt2; i += 2)
    {
      int k = paramInt1 - i;
      for (j = k;; j++)
      {
        int m = paramInt1 + i;
        if (j > m) {
          break;
        }
        paramBitMatrix.set(j, k);
        paramBitMatrix.set(j, m);
        paramBitMatrix.set(k, j);
        paramBitMatrix.set(m, j);
      }
    }
    int j = paramInt1 - paramInt2;
    paramBitMatrix.set(j, j);
    i = j + 1;
    paramBitMatrix.set(i, j);
    paramBitMatrix.set(j, i);
    paramInt1 += paramInt2;
    paramBitMatrix.set(paramInt1, j);
    paramBitMatrix.set(paramInt1, i);
    paramBitMatrix.set(paramInt1, paramInt1 - 1);
  }
  
  private static void drawModeMessage(BitMatrix paramBitMatrix, boolean paramBoolean, int paramInt, BitArray paramBitArray)
  {
    int j = paramInt / 2;
    paramInt = 0;
    int i = 0;
    if (paramBoolean)
    {
      for (paramInt = i; paramInt < 7; paramInt++)
      {
        i = j - 3 + paramInt;
        if (paramBitArray.get(paramInt)) {
          paramBitMatrix.set(i, j - 5);
        }
        if (paramBitArray.get(paramInt + 7)) {
          paramBitMatrix.set(j + 5, i);
        }
        if (paramBitArray.get(20 - paramInt)) {
          paramBitMatrix.set(i, j + 5);
        }
        if (paramBitArray.get(27 - paramInt)) {
          paramBitMatrix.set(j - 5, i);
        }
      }
      return;
    }
    while (paramInt < 10)
    {
      i = j - 5 + paramInt + paramInt / 5;
      if (paramBitArray.get(paramInt)) {
        paramBitMatrix.set(i, j - 7);
      }
      if (paramBitArray.get(paramInt + 10)) {
        paramBitMatrix.set(j + 7, i);
      }
      if (paramBitArray.get(29 - paramInt)) {
        paramBitMatrix.set(i, j + 7);
      }
      if (paramBitArray.get(39 - paramInt)) {
        paramBitMatrix.set(j - 7, i);
      }
      paramInt++;
    }
  }
  
  public static AztecCode encode(byte[] paramArrayOfByte)
  {
    return encode(paramArrayOfByte, 33, 0);
  }
  
  public static AztecCode encode(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Object localObject = new HighLevelEncoder(paramArrayOfByte).encode();
    paramInt1 = ((BitArray)localObject).getSize() * paramInt1 / 100;
    int k = 11;
    int i1 = paramInt1 + 11;
    int n = ((BitArray)localObject).getSize();
    paramInt1 = 32;
    boolean bool1;
    int j;
    int m;
    boolean bool2;
    if (paramInt2 != 0)
    {
      if (paramInt2 < 0) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      j = Math.abs(paramInt2);
      if (bool1) {
        paramInt1 = 4;
      }
      if (j <= paramInt1)
      {
        n = totalBitsInLayer(j, bool1);
        m = WORD_SIZE[j];
        localObject = stuffBits((BitArray)localObject, m);
        if (((BitArray)localObject).getSize() + i1 <= n - n % m)
        {
          paramArrayOfByte = (byte[])localObject;
          bool2 = bool1;
          i = n;
          paramInt1 = j;
          paramInt2 = m;
          if (bool1) {
            if (((BitArray)localObject).getSize() <= m << 6)
            {
              paramArrayOfByte = (byte[])localObject;
              bool2 = bool1;
              i = n;
              paramInt1 = j;
              paramInt2 = m;
            }
            else
            {
              throw new IllegalArgumentException("Data to large for user specified layer");
            }
          }
        }
        else
        {
          throw new IllegalArgumentException("Data to large for user specified layer");
        }
      }
      else
      {
        throw new IllegalArgumentException(String.format("Illegal value %s for layers", new Object[] { Integer.valueOf(paramInt2) }));
      }
    }
    else
    {
      paramArrayOfByte = null;
      paramInt2 = 0;
    }
    for (int i = 0; paramInt2 <= 32; i = paramInt1)
    {
      if (paramInt2 <= 3) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      if (bool1) {
        j = paramInt2 + 1;
      } else {
        j = paramInt2;
      }
      m = totalBitsInLayer(j, bool1);
      if (n + i1 <= m)
      {
        int[] arrayOfInt = WORD_SIZE;
        paramInt1 = i;
        if (i != arrayOfInt[j])
        {
          paramInt1 = arrayOfInt[j];
          paramArrayOfByte = stuffBits((BitArray)localObject, paramInt1);
        }
        if (((bool1) && (paramArrayOfByte.getSize() > paramInt1 << 6)) || (paramArrayOfByte.getSize() + i1 <= m - m % paramInt1))
        {
          i = m;
          paramInt2 = paramInt1;
          paramInt1 = j;
          bool2 = bool1;
          localObject = generateCheckWords(paramArrayOfByte, i, paramInt2);
          i1 = paramArrayOfByte.getSize() / paramInt2;
          BitArray localBitArray = generateModeMessage(bool2, paramInt1, i1);
          if (bool2) {
            paramInt2 = k;
          } else {
            paramInt2 = 14;
          }
          n = paramInt2 + (paramInt1 << 2);
          arrayOfInt = new int[n];
          if (bool2)
          {
            for (paramInt2 = 0; paramInt2 < arrayOfInt.length; paramInt2++) {
              arrayOfInt[paramInt2] = paramInt2;
            }
            paramInt2 = n;
          }
          else
          {
            k = n / 2;
            j = n + 1 + (k - 1) / 15 * 2;
            m = j / 2;
            for (i = 0;; i++)
            {
              paramInt2 = j;
              if (i >= k) {
                break;
              }
              paramInt2 = i / 15 + i;
              arrayOfInt[(k - i - 1)] = (m - paramInt2 - 1);
              arrayOfInt[(k + i)] = (paramInt2 + m + 1);
            }
          }
          paramArrayOfByte = new BitMatrix(paramInt2);
          j = 0;
          i = 0;
          int i2;
          int i3;
          while (j < paramInt1)
          {
            if (bool2) {
              k = 9;
            } else {
              k = 12;
            }
            i2 = (paramInt1 - j << 2) + k;
            for (k = 0;; k++)
            {
              m = 0;
              if (k >= i2) {
                break;
              }
              i3 = k << 1;
              while (m < 2)
              {
                int i4;
                if (((BitArray)localObject).get(i + i3 + m))
                {
                  i4 = j << 1;
                  paramArrayOfByte.set(arrayOfInt[(i4 + m)], arrayOfInt[(i4 + k)]);
                }
                if (((BitArray)localObject).get((i2 << 1) + i + i3 + m))
                {
                  i4 = j << 1;
                  paramArrayOfByte.set(arrayOfInt[(i4 + k)], arrayOfInt[(n - 1 - i4 - m)]);
                }
                if (((BitArray)localObject).get((i2 << 2) + i + i3 + m))
                {
                  i4 = n - 1 - (j << 1);
                  paramArrayOfByte.set(arrayOfInt[(i4 - m)], arrayOfInt[(i4 - k)]);
                }
                if (((BitArray)localObject).get(i2 * 6 + i + i3 + m))
                {
                  i4 = j << 1;
                  paramArrayOfByte.set(arrayOfInt[(n - 1 - i4 - k)], arrayOfInt[(i4 + m)]);
                }
                m++;
              }
            }
            i += (i2 << 3);
            j++;
          }
          drawModeMessage(paramArrayOfByte, bool2, paramInt2, localBitArray);
          if (bool2)
          {
            drawBullsEye(paramArrayOfByte, paramInt2 / 2, 5);
          }
          else
          {
            m = paramInt2 / 2;
            drawBullsEye(paramArrayOfByte, m, 7);
            j = 0;
            for (i = 0; j < n / 2 - 1; i += 16)
            {
              for (k = m & 0x1; k < paramInt2; k += 2)
              {
                i3 = m - i;
                paramArrayOfByte.set(i3, k);
                i2 = m + i;
                paramArrayOfByte.set(i2, k);
                paramArrayOfByte.set(k, i3);
                paramArrayOfByte.set(k, i2);
              }
              j += 15;
            }
          }
          localObject = new AztecCode();
          ((AztecCode)localObject).setCompact(bool2);
          ((AztecCode)localObject).setSize(paramInt2);
          ((AztecCode)localObject).setLayers(paramInt1);
          ((AztecCode)localObject).setCodeWords(i1);
          ((AztecCode)localObject).setMatrix(paramArrayOfByte);
          return (AztecCode)localObject;
        }
      }
      else
      {
        paramInt1 = i;
      }
      paramInt2++;
    }
    throw new IllegalArgumentException("Data too large for an Aztec code");
  }
  
  private static BitArray generateCheckWords(BitArray paramBitArray, int paramInt1, int paramInt2)
  {
    int i = paramBitArray.getSize() / paramInt2;
    Object localObject = new ReedSolomonEncoder(getGF(paramInt2));
    int j = paramInt1 / paramInt2;
    paramBitArray = bitsToWords(paramBitArray, paramInt2, j);
    ((ReedSolomonEncoder)localObject).encode(paramBitArray, j - i);
    localObject = new BitArray();
    i = 0;
    ((BitArray)localObject).appendBits(0, paramInt1 % paramInt2);
    j = paramBitArray.length;
    for (paramInt1 = i; paramInt1 < j; paramInt1++) {
      ((BitArray)localObject).appendBits(paramBitArray[paramInt1], paramInt2);
    }
    return (BitArray)localObject;
  }
  
  static BitArray generateModeMessage(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    BitArray localBitArray = new BitArray();
    if (paramBoolean)
    {
      localBitArray.appendBits(paramInt1 - 1, 2);
      localBitArray.appendBits(paramInt2 - 1, 6);
      localBitArray = generateCheckWords(localBitArray, 28, 4);
    }
    else
    {
      localBitArray.appendBits(paramInt1 - 1, 5);
      localBitArray.appendBits(paramInt2 - 1, 11);
      localBitArray = generateCheckWords(localBitArray, 40, 4);
    }
    return localBitArray;
  }
  
  private static GenericGF getGF(int paramInt)
  {
    if (paramInt != 4)
    {
      if (paramInt != 6)
      {
        if (paramInt != 8)
        {
          if (paramInt != 10)
          {
            if (paramInt == 12) {
              return GenericGF.AZTEC_DATA_12;
            }
            StringBuilder localStringBuilder = new StringBuilder("Unsupported word size ");
            localStringBuilder.append(paramInt);
            throw new IllegalArgumentException(localStringBuilder.toString());
          }
          return GenericGF.AZTEC_DATA_10;
        }
        return GenericGF.AZTEC_DATA_8;
      }
      return GenericGF.AZTEC_DATA_6;
    }
    return GenericGF.AZTEC_PARAM;
  }
  
  static BitArray stuffBits(BitArray paramBitArray, int paramInt)
  {
    BitArray localBitArray = new BitArray();
    int i1 = paramBitArray.getSize();
    int n = (1 << paramInt) - 2;
    int i = 0;
    while (i < i1)
    {
      int k = 0;
      int m;
      for (int j = 0; k < paramInt; j = m)
      {
        int i2 = i + k;
        if (i2 < i1)
        {
          m = j;
          if (!paramBitArray.get(i2)) {}
        }
        else
        {
          m = j | 1 << paramInt - 1 - k;
        }
        k++;
      }
      k = j & n;
      if (k == n)
      {
        localBitArray.appendBits(k, paramInt);
        i--;
      }
      else if (k == 0)
      {
        localBitArray.appendBits(j | 0x1, paramInt);
        i--;
      }
      else
      {
        localBitArray.appendBits(j, paramInt);
      }
      i += paramInt;
    }
    return localBitArray;
  }
  
  private static int totalBitsInLayer(int paramInt, boolean paramBoolean)
  {
    int i;
    if (paramBoolean) {
      i = 88;
    } else {
      i = 112;
    }
    return (i + (paramInt << 4)) * paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/encoder/Encoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */