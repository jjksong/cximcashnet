package com.google.zxing.aztec.encoder;

import com.google.zxing.common.BitArray;

final class BinaryShiftToken
  extends Token
{
  private final short binaryShiftByteCount;
  private final short binaryShiftStart;
  
  BinaryShiftToken(Token paramToken, int paramInt1, int paramInt2)
  {
    super(paramToken);
    this.binaryShiftStart = ((short)paramInt1);
    this.binaryShiftByteCount = ((short)paramInt2);
  }
  
  public void appendTo(BitArray paramBitArray, byte[] paramArrayOfByte)
  {
    for (int i = 0;; i++)
    {
      int j = this.binaryShiftByteCount;
      if (i >= j) {
        break;
      }
      if ((i == 0) || ((i == 31) && (j <= 62)))
      {
        paramBitArray.appendBits(31, 5);
        j = this.binaryShiftByteCount;
        if (j > 62) {
          paramBitArray.appendBits(j - 31, 16);
        } else if (i == 0) {
          paramBitArray.appendBits(Math.min(j, 31), 5);
        } else {
          paramBitArray.appendBits(j - 31, 5);
        }
      }
      paramBitArray.appendBits(paramArrayOfByte[(this.binaryShiftStart + i)], 8);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("<");
    localStringBuilder.append(this.binaryShiftStart);
    localStringBuilder.append("::");
    localStringBuilder.append(this.binaryShiftStart + this.binaryShiftByteCount - 1);
    localStringBuilder.append('>');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/encoder/BinaryShiftToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */