package com.google.zxing.aztec.encoder;

import com.google.zxing.common.BitArray;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class HighLevelEncoder
{
  private static final int[][] CHAR_MAP;
  static final int[][] LATCH_TABLE;
  static final int MODE_DIGIT = 2;
  static final int MODE_LOWER = 1;
  static final int MODE_MIXED = 3;
  static final String[] MODE_NAMES = { "UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT" };
  static final int MODE_PUNCT = 4;
  static final int MODE_UPPER = 0;
  static final int[][] SHIFT_TABLE;
  private final byte[] text;
  
  static
  {
    Object localObject = { 262158, 590300, 0, 590301, 932798 };
    int[] arrayOfInt = { 327711, 656380, 656382, 656381, 0 };
    LATCH_TABLE = new int[][] { { 0, 327708, 327710, 327709, 656318 }, { 590318, 0, 327710, 327709, 656318 }, localObject, { 327709, 327708, 656318, 0, 327710 }, arrayOfInt };
    localObject = (int[][])Array.newInstance(Integer.TYPE, new int[] { 5, 256 });
    CHAR_MAP = (int[][])localObject;
    localObject[0][32] = 1;
    for (int i = 65; i <= 90; i++) {
      CHAR_MAP[0][i] = (i - 65 + 2);
    }
    CHAR_MAP[1][32] = 1;
    for (i = 97; i <= 122; i++) {
      CHAR_MAP[1][i] = (i - 97 + 2);
    }
    CHAR_MAP[2][32] = 1;
    for (i = 48; i <= 57; i++) {
      CHAR_MAP[2][i] = (i - 48 + 2);
    }
    localObject = CHAR_MAP;
    localObject[2][44] = 12;
    localObject[2][46] = 13;
    for (i = 0; i < 28; i++) {
      CHAR_MAP[3][new int[] { 0, 32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 27, 28, 29, 30, 31, 64, 92, 94, 95, 96, 124, 126, 127 }[i]] = i;
    }
    localObject = new int[31];
    Object tmp538_537 = localObject;
    tmp538_537[0] = 0;
    Object tmp542_538 = tmp538_537;
    tmp542_538[1] = 13;
    Object tmp547_542 = tmp542_538;
    tmp547_542[2] = 0;
    Object tmp551_547 = tmp547_542;
    tmp551_547[3] = 0;
    Object tmp555_551 = tmp551_547;
    tmp555_551[4] = 0;
    Object tmp559_555 = tmp555_551;
    tmp559_555[5] = 0;
    Object tmp563_559 = tmp559_555;
    tmp563_559[6] = 33;
    Object tmp569_563 = tmp563_559;
    tmp569_563[7] = 39;
    Object tmp575_569 = tmp569_563;
    tmp575_569[8] = 35;
    Object tmp581_575 = tmp575_569;
    tmp581_575[9] = 36;
    Object tmp587_581 = tmp581_575;
    tmp587_581[10] = 37;
    Object tmp593_587 = tmp587_581;
    tmp593_587[11] = 38;
    Object tmp599_593 = tmp593_587;
    tmp599_593[12] = 39;
    Object tmp605_599 = tmp599_593;
    tmp605_599[13] = 40;
    Object tmp611_605 = tmp605_599;
    tmp611_605[14] = 41;
    Object tmp617_611 = tmp611_605;
    tmp617_611[15] = 42;
    Object tmp623_617 = tmp617_611;
    tmp623_617[16] = 43;
    Object tmp629_623 = tmp623_617;
    tmp629_623[17] = 44;
    Object tmp635_629 = tmp629_623;
    tmp635_629[18] = 45;
    Object tmp641_635 = tmp635_629;
    tmp641_635[19] = 46;
    Object tmp647_641 = tmp641_635;
    tmp647_641[20] = 47;
    Object tmp653_647 = tmp647_641;
    tmp653_647[21] = 58;
    Object tmp659_653 = tmp653_647;
    tmp659_653[22] = 59;
    Object tmp665_659 = tmp659_653;
    tmp665_659[23] = 60;
    Object tmp671_665 = tmp665_659;
    tmp671_665[24] = 61;
    Object tmp677_671 = tmp671_665;
    tmp677_671[25] = 62;
    Object tmp683_677 = tmp677_671;
    tmp683_677[26] = 63;
    Object tmp689_683 = tmp683_677;
    tmp689_683[27] = 91;
    Object tmp695_689 = tmp689_683;
    tmp695_689[28] = 93;
    Object tmp701_695 = tmp695_689;
    tmp701_695[29] = 123;
    Object tmp707_701 = tmp701_695;
    tmp707_701[30] = 125;
    tmp707_701;
    for (i = 0; i < 31; i++) {
      if (localObject[i] > 0) {
        CHAR_MAP[4][localObject[i]] = i;
      }
    }
    localObject = (int[][])Array.newInstance(Integer.TYPE, new int[] { 6, 6 });
    SHIFT_TABLE = (int[][])localObject;
    int j = localObject.length;
    for (i = 0; i < j; i++) {
      Arrays.fill(localObject[i], -1);
    }
    localObject = SHIFT_TABLE;
    localObject[0][4] = 0;
    localObject[1][4] = 0;
    localObject[1][0] = 28;
    localObject[3][4] = 0;
    localObject[2][4] = 0;
    localObject[2][0] = 15;
  }
  
  public HighLevelEncoder(byte[] paramArrayOfByte)
  {
    this.text = paramArrayOfByte;
  }
  
  private static Collection<State> simplifyStates(Iterable<State> paramIterable)
  {
    LinkedList localLinkedList = new LinkedList();
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      State localState1 = (State)paramIterable.next();
      int j = 1;
      Iterator localIterator = localLinkedList.iterator();
      int i;
      for (;;)
      {
        i = j;
        if (!localIterator.hasNext()) {
          break;
        }
        State localState2 = (State)localIterator.next();
        if (localState2.isBetterThanOrEqualTo(localState1))
        {
          i = 0;
          break;
        }
        if (localState1.isBetterThanOrEqualTo(localState2)) {
          localIterator.remove();
        }
      }
      if (i != 0) {
        localLinkedList.add(localState1);
      }
    }
    return localLinkedList;
  }
  
  private void updateStateForChar(State paramState, int paramInt, Collection<State> paramCollection)
  {
    int k = (char)(this.text[paramInt] & 0xFF);
    int i = CHAR_MAP[paramState.getMode()][k];
    int j = 0;
    if (i > 0) {
      i = 1;
    } else {
      i = 0;
    }
    Object localObject3;
    for (Object localObject2 = null; j <= 4; localObject2 = localObject3)
    {
      int m = CHAR_MAP[j][k];
      localObject3 = localObject2;
      if (m > 0)
      {
        Object localObject1 = localObject2;
        if (localObject2 == null) {
          localObject1 = paramState.endBinaryShift(paramInt);
        }
        if ((i == 0) || (j == paramState.getMode()) || (j == 2)) {
          paramCollection.add(((State)localObject1).latchAndAppend(j, m));
        }
        localObject3 = localObject1;
        if (i == 0)
        {
          localObject3 = localObject1;
          if (SHIFT_TABLE[paramState.getMode()][j] >= 0)
          {
            paramCollection.add(((State)localObject1).shiftAndAppend(j, m));
            localObject3 = localObject1;
          }
        }
      }
      j++;
    }
    if ((paramState.getBinaryShiftByteCount() > 0) || (CHAR_MAP[paramState.getMode()][k] == 0)) {
      paramCollection.add(paramState.addBinaryShiftChar(paramInt));
    }
  }
  
  private static void updateStateForPair(State paramState, int paramInt1, int paramInt2, Collection<State> paramCollection)
  {
    State localState = paramState.endBinaryShift(paramInt1);
    paramCollection.add(localState.latchAndAppend(4, paramInt2));
    if (paramState.getMode() != 4) {
      paramCollection.add(localState.shiftAndAppend(4, paramInt2));
    }
    if ((paramInt2 == 3) || (paramInt2 == 4)) {
      paramCollection.add(localState.latchAndAppend(2, 16 - paramInt2).latchAndAppend(2, 1));
    }
    if (paramState.getBinaryShiftByteCount() > 0) {
      paramCollection.add(paramState.addBinaryShiftChar(paramInt1).addBinaryShiftChar(paramInt1 + 1));
    }
  }
  
  private Collection<State> updateStateListForChar(Iterable<State> paramIterable, int paramInt)
  {
    LinkedList localLinkedList = new LinkedList();
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      updateStateForChar((State)paramIterable.next(), paramInt, localLinkedList);
    }
    return simplifyStates(localLinkedList);
  }
  
  private static Collection<State> updateStateListForPair(Iterable<State> paramIterable, int paramInt1, int paramInt2)
  {
    LinkedList localLinkedList = new LinkedList();
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      updateStateForPair((State)paramIterable.next(), paramInt1, paramInt2, localLinkedList);
    }
    return simplifyStates(localLinkedList);
  }
  
  public BitArray encode()
  {
    Object localObject = Collections.singletonList(State.INITIAL_STATE);
    for (int j = 0;; j++)
    {
      byte[] arrayOfByte = this.text;
      if (j >= arrayOfByte.length) {
        break;
      }
      int k = j + 1;
      int i;
      if (k < arrayOfByte.length) {
        i = arrayOfByte[k];
      } else {
        i = 0;
      }
      int m = this.text[j];
      if (m != 13)
      {
        if (m != 44)
        {
          if (m != 46)
          {
            if (m != 58) {
              i = 0;
            } else if (i == 32) {
              i = 5;
            } else {
              i = 0;
            }
          }
          else if (i == 32) {
            i = 3;
          } else {
            i = 0;
          }
        }
        else if (i == 32) {
          i = 4;
        } else {
          i = 0;
        }
      }
      else if (i == 10) {
        i = 2;
      } else {
        i = 0;
      }
      if (i > 0)
      {
        localObject = updateStateListForPair((Iterable)localObject, j, i);
        j = k;
      }
      else
      {
        localObject = updateStateListForChar((Iterable)localObject, j);
      }
    }
    ((State)Collections.min((Collection)localObject, new Comparator()
    {
      public int compare(State paramAnonymousState1, State paramAnonymousState2)
      {
        return paramAnonymousState1.getBitCount() - paramAnonymousState2.getBitCount();
      }
    })).toBitArray(this.text);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/encoder/HighLevelEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */