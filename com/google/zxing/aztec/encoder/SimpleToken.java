package com.google.zxing.aztec.encoder;

import com.google.zxing.common.BitArray;

final class SimpleToken
  extends Token
{
  private final short bitCount;
  private final short value;
  
  SimpleToken(Token paramToken, int paramInt1, int paramInt2)
  {
    super(paramToken);
    this.value = ((short)paramInt1);
    this.bitCount = ((short)paramInt2);
  }
  
  void appendTo(BitArray paramBitArray, byte[] paramArrayOfByte)
  {
    paramBitArray.appendBits(this.value, this.bitCount);
  }
  
  public String toString()
  {
    int j = this.value;
    int i = this.bitCount;
    StringBuilder localStringBuilder = new StringBuilder("<");
    localStringBuilder.append(Integer.toBinaryString(j & (1 << i) - 1 | 1 << i | 1 << this.bitCount).substring(1));
    localStringBuilder.append('>');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/encoder/SimpleToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */