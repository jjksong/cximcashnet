package com.google.zxing.aztec;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.aztec.encoder.AztecCode;
import com.google.zxing.aztec.encoder.Encoder;
import com.google.zxing.common.BitMatrix;
import java.nio.charset.Charset;
import java.util.Map;

public final class AztecWriter
  implements Writer
{
  private static final Charset DEFAULT_CHARSET = Charset.forName("ISO-8859-1");
  
  private static BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Charset paramCharset, int paramInt3, int paramInt4)
  {
    if (paramBarcodeFormat == BarcodeFormat.AZTEC) {
      return renderResult(Encoder.encode(paramString.getBytes(paramCharset), paramInt3, paramInt4), paramInt1, paramInt2);
    }
    paramString = new StringBuilder("Can only encode AZTEC, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
  
  private static BitMatrix renderResult(AztecCode paramAztecCode, int paramInt1, int paramInt2)
  {
    paramAztecCode = paramAztecCode.getMatrix();
    if (paramAztecCode != null)
    {
      int m = paramAztecCode.getWidth();
      int n = paramAztecCode.getHeight();
      paramInt1 = Math.max(paramInt1, m);
      int i = Math.max(paramInt2, n);
      int i1 = Math.min(paramInt1 / m, i / n);
      int k = (paramInt1 - m * i1) / 2;
      paramInt2 = (i - n * i1) / 2;
      BitMatrix localBitMatrix = new BitMatrix(paramInt1, i);
      paramInt1 = 0;
      while (paramInt1 < n)
      {
        i = k;
        int j = 0;
        while (j < m)
        {
          if (paramAztecCode.get(j, paramInt1)) {
            localBitMatrix.setRegion(i, paramInt2, i1, i1);
          }
          j++;
          i += i1;
        }
        paramInt1++;
        paramInt2 += i1;
      }
      return localBitMatrix;
    }
    throw new IllegalStateException();
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
  {
    return encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, null);
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
  {
    Charset localCharset = DEFAULT_CHARSET;
    int i = 33;
    int j;
    if (paramMap != null)
    {
      if (paramMap.containsKey(EncodeHintType.CHARACTER_SET)) {
        localCharset = Charset.forName(paramMap.get(EncodeHintType.CHARACTER_SET).toString());
      }
      if (paramMap.containsKey(EncodeHintType.ERROR_CORRECTION)) {
        i = Integer.parseInt(paramMap.get(EncodeHintType.ERROR_CORRECTION).toString());
      }
      if (paramMap.containsKey(EncodeHintType.AZTEC_LAYERS))
      {
        j = Integer.parseInt(paramMap.get(EncodeHintType.AZTEC_LAYERS).toString());
        paramMap = localCharset;
      }
      else
      {
        paramMap = localCharset;
        j = 0;
      }
    }
    else
    {
      paramMap = localCharset;
      i = 33;
      j = 0;
    }
    return encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap, i, j);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/aztec/AztecWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */