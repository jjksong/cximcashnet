package com.google.zxing.common;

import java.util.Arrays;

public final class BitArray
  implements Cloneable
{
  private int[] bits;
  private int size;
  
  public BitArray()
  {
    this.size = 0;
    this.bits = new int[1];
  }
  
  public BitArray(int paramInt)
  {
    this.size = paramInt;
    this.bits = makeArray(paramInt);
  }
  
  BitArray(int[] paramArrayOfInt, int paramInt)
  {
    this.bits = paramArrayOfInt;
    this.size = paramInt;
  }
  
  private void ensureCapacity(int paramInt)
  {
    if (paramInt > this.bits.length << 5)
    {
      int[] arrayOfInt2 = makeArray(paramInt);
      int[] arrayOfInt1 = this.bits;
      System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, arrayOfInt1.length);
      this.bits = arrayOfInt2;
    }
  }
  
  private static int[] makeArray(int paramInt)
  {
    return new int[(paramInt + 31) / 32];
  }
  
  public void appendBit(boolean paramBoolean)
  {
    ensureCapacity(this.size + 1);
    if (paramBoolean)
    {
      int[] arrayOfInt = this.bits;
      int i = this.size;
      int j = i / 32;
      arrayOfInt[j] = (1 << (i & 0x1F) | arrayOfInt[j]);
    }
    this.size += 1;
  }
  
  public void appendBitArray(BitArray paramBitArray)
  {
    int j = paramBitArray.size;
    ensureCapacity(this.size + j);
    for (int i = 0; i < j; i++) {
      appendBit(paramBitArray.get(i));
    }
  }
  
  public void appendBits(int paramInt1, int paramInt2)
  {
    if ((paramInt2 >= 0) && (paramInt2 <= 32))
    {
      ensureCapacity(this.size + paramInt2);
      while (paramInt2 > 0)
      {
        boolean bool = true;
        if ((paramInt1 >> paramInt2 - 1 & 0x1) != 1) {
          bool = false;
        }
        appendBit(bool);
        paramInt2--;
      }
      return;
    }
    throw new IllegalArgumentException("Num bits must be between 0 and 32");
  }
  
  public void clear()
  {
    int j = this.bits.length;
    for (int i = 0; i < j; i++) {
      this.bits[i] = 0;
    }
  }
  
  public BitArray clone()
  {
    return new BitArray((int[])this.bits.clone(), this.size);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof BitArray)) {
      return false;
    }
    paramObject = (BitArray)paramObject;
    return (this.size == ((BitArray)paramObject).size) && (Arrays.equals(this.bits, ((BitArray)paramObject).bits));
  }
  
  public void flip(int paramInt)
  {
    int[] arrayOfInt = this.bits;
    int i = paramInt / 32;
    arrayOfInt[i] = (1 << (paramInt & 0x1F) ^ arrayOfInt[i]);
  }
  
  public boolean get(int paramInt)
  {
    return (1 << (paramInt & 0x1F) & this.bits[(paramInt / 32)]) != 0;
  }
  
  public int[] getBitArray()
  {
    return this.bits;
  }
  
  public int getNextSet(int paramInt)
  {
    int i = this.size;
    if (paramInt >= i) {
      return i;
    }
    int j = paramInt / 32;
    i = ((1 << (paramInt & 0x1F)) - 1 ^ 0xFFFFFFFF) & this.bits[j];
    paramInt = j;
    while (i == 0)
    {
      paramInt++;
      int[] arrayOfInt = this.bits;
      if (paramInt == arrayOfInt.length) {
        return this.size;
      }
      i = arrayOfInt[paramInt];
    }
    paramInt = (paramInt << 5) + Integer.numberOfTrailingZeros(i);
    i = this.size;
    if (paramInt > i) {
      return i;
    }
    return paramInt;
  }
  
  public int getNextUnset(int paramInt)
  {
    int i = this.size;
    if (paramInt >= i) {
      return i;
    }
    i = paramInt / 32;
    int[] arrayOfInt;
    for (paramInt = ((1 << (paramInt & 0x1F)) - 1 ^ 0xFFFFFFFF) & (this.bits[i] ^ 0xFFFFFFFF); paramInt == 0; paramInt = arrayOfInt[i] ^ 0xFFFFFFFF)
    {
      i++;
      arrayOfInt = this.bits;
      if (i == arrayOfInt.length) {
        return this.size;
      }
    }
    paramInt = (i << 5) + Integer.numberOfTrailingZeros(paramInt);
    i = this.size;
    if (paramInt > i) {
      return i;
    }
    return paramInt;
  }
  
  public int getSize()
  {
    return this.size;
  }
  
  public int getSizeInBytes()
  {
    return (this.size + 7) / 8;
  }
  
  public int hashCode()
  {
    return this.size * 31 + Arrays.hashCode(this.bits);
  }
  
  public boolean isRange(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if ((paramInt2 >= paramInt1) && (paramInt1 >= 0) && (paramInt2 <= this.size))
    {
      if (paramInt2 == paramInt1) {
        return true;
      }
      int m = paramInt2 - 1;
      int k = paramInt1 / 32;
      int n = m / 32;
      for (int i = k; i <= n; i++)
      {
        int j = 31;
        if (i > k) {
          paramInt2 = 0;
        } else {
          paramInt2 = paramInt1 & 0x1F;
        }
        if (i >= n) {
          j = 0x1F & m;
        }
        j = (2 << j) - (1 << paramInt2);
        int i1 = this.bits[i];
        if (paramBoolean) {
          paramInt2 = j;
        } else {
          paramInt2 = 0;
        }
        if ((i1 & j) != paramInt2) {
          return false;
        }
      }
      return true;
    }
    throw new IllegalArgumentException();
  }
  
  public void reverse()
  {
    int[] arrayOfInt = new int[this.bits.length];
    int j = (this.size - 1) / 32;
    int k = j + 1;
    for (int i = 0; i < k; i++)
    {
      long l = this.bits[i];
      l = (l & 0x55555555) << 1 | l >> 1 & 0x55555555;
      l = (l & 0x33333333) << 2 | l >> 2 & 0x33333333;
      l = (l & 0xF0F0F0F) << 4 | l >> 4 & 0xF0F0F0F;
      l = (l & 0xFF00FF) << 8 | l >> 8 & 0xFF00FF;
      arrayOfInt[(j - i)] = ((int)((l & 0xFFFF) << 16 | l >> 16 & 0xFFFF));
    }
    j = this.size;
    i = k << 5;
    if (j != i)
    {
      int m = i - j;
      j = arrayOfInt[0] >>> m;
      for (i = 1; i < k; i++)
      {
        int n = arrayOfInt[i];
        arrayOfInt[(i - 1)] = (j | n << 32 - m);
        j = n >>> m;
      }
      arrayOfInt[(k - 1)] = j;
    }
    this.bits = arrayOfInt;
  }
  
  public void set(int paramInt)
  {
    int[] arrayOfInt = this.bits;
    int i = paramInt / 32;
    arrayOfInt[i] = (1 << (paramInt & 0x1F) | arrayOfInt[i]);
  }
  
  public void setBulk(int paramInt1, int paramInt2)
  {
    this.bits[(paramInt1 / 32)] = paramInt2;
  }
  
  public void setRange(int paramInt1, int paramInt2)
  {
    if ((paramInt2 >= paramInt1) && (paramInt1 >= 0) && (paramInt2 <= this.size))
    {
      if (paramInt2 == paramInt1) {
        return;
      }
      int m = paramInt2 - 1;
      int k = paramInt1 / 32;
      int n = m / 32;
      for (paramInt2 = k; paramInt2 <= n; paramInt2++)
      {
        int j = 31;
        int i;
        if (paramInt2 > k) {
          i = 0;
        } else {
          i = paramInt1 & 0x1F;
        }
        if (paramInt2 >= n) {
          j = 0x1F & m;
        }
        int[] arrayOfInt = this.bits;
        arrayOfInt[paramInt2] = ((2 << j) - (1 << i) | arrayOfInt[paramInt2]);
      }
      return;
    }
    throw new IllegalArgumentException();
  }
  
  public void toBytes(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    for (int i = 0; i < paramInt3; i++)
    {
      int m = 0;
      int j;
      for (int k = 0; m < 8; k = j)
      {
        j = k;
        if (get(paramInt1)) {
          j = k | 1 << 7 - m;
        }
        paramInt1++;
        m++;
      }
      paramArrayOfByte[(paramInt2 + i)] = ((byte)k);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(this.size);
    for (int i = 0; i < this.size; i++)
    {
      if ((i & 0x7) == 0) {
        localStringBuilder.append(' ');
      }
      char c;
      if (get(i)) {
        c = 'X';
      } else {
        c = '.';
      }
      localStringBuilder.append(c);
    }
    return localStringBuilder.toString();
  }
  
  public void xor(BitArray paramBitArray)
  {
    if (this.size == paramBitArray.size)
    {
      for (int i = 0;; i++)
      {
        int[] arrayOfInt = this.bits;
        if (i >= arrayOfInt.length) {
          break;
        }
        arrayOfInt[i] ^= paramBitArray.bits[i];
      }
      return;
    }
    throw new IllegalArgumentException("Sizes don't match");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/BitArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */