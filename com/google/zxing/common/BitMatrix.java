package com.google.zxing.common;

import java.util.Arrays;

public final class BitMatrix
  implements Cloneable
{
  private final int[] bits;
  private final int height;
  private final int rowSize;
  private final int width;
  
  public BitMatrix(int paramInt)
  {
    this(paramInt, paramInt);
  }
  
  public BitMatrix(int paramInt1, int paramInt2)
  {
    if ((paramInt1 > 0) && (paramInt2 > 0))
    {
      this.width = paramInt1;
      this.height = paramInt2;
      this.rowSize = ((paramInt1 + 31) / 32);
      this.bits = new int[this.rowSize * paramInt2];
      return;
    }
    throw new IllegalArgumentException("Both dimensions must be greater than 0");
  }
  
  private BitMatrix(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt)
  {
    this.width = paramInt1;
    this.height = paramInt2;
    this.rowSize = paramInt3;
    this.bits = paramArrayOfInt;
  }
  
  private String buildToString(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new StringBuilder(this.height * (this.width + 1));
    for (int i = 0; i < this.height; i++)
    {
      for (int j = 0; j < this.width; j++)
      {
        String str;
        if (get(j, i)) {
          str = paramString1;
        } else {
          str = paramString2;
        }
        localStringBuilder.append(str);
      }
      localStringBuilder.append(paramString3);
    }
    return localStringBuilder.toString();
  }
  
  public static BitMatrix parse(String paramString1, String paramString2, String paramString3)
  {
    if (paramString1 != null)
    {
      boolean[] arrayOfBoolean = new boolean[paramString1.length()];
      int i4 = 0;
      int k = 0;
      int j = 0;
      int n = 0;
      int i = -1;
      int m = 0;
      while (k < paramString1.length()) {
        if ((paramString1.charAt(k) != '\n') && (paramString1.charAt(k) != '\r'))
        {
          if (paramString1.substring(k, paramString2.length() + k).equals(paramString2))
          {
            k += paramString2.length();
            arrayOfBoolean[j] = true;
            j++;
          }
          else if (paramString1.substring(k, paramString3.length() + k).equals(paramString3))
          {
            k += paramString3.length();
            arrayOfBoolean[j] = false;
            j++;
          }
          else
          {
            paramString2 = new StringBuilder("illegal character encountered: ");
            paramString2.append(paramString1.substring(k));
            throw new IllegalArgumentException(paramString2.toString());
          }
        }
        else
        {
          i1 = n;
          int i2 = i;
          int i3 = m;
          if (j > n)
          {
            if (i == -1) {
              i = j - n;
            } else {
              if (j - n != i) {
                break label235;
              }
            }
            i3 = m + 1;
            i1 = j;
            i2 = i;
            break label245;
            label235:
            throw new IllegalArgumentException("row lengths do not match");
          }
          label245:
          k++;
          n = i1;
          i = i2;
          m = i3;
        }
      }
      k = i;
      int i1 = m;
      if (j > n)
      {
        if (i == -1) {
          i = j - n;
        } else {
          if (j - n != i) {
            break label311;
          }
        }
        i1 = m + 1;
        k = i;
        break label321;
        label311:
        throw new IllegalArgumentException("row lengths do not match");
      }
      label321:
      paramString1 = new BitMatrix(k, i1);
      for (i = i4; i < j; i++) {
        if (arrayOfBoolean[i] != 0) {
          paramString1.set(i % k, i / k);
        }
      }
      return paramString1;
    }
    throw new IllegalArgumentException();
  }
  
  public void clear()
  {
    int j = this.bits.length;
    for (int i = 0; i < j; i++) {
      this.bits[i] = 0;
    }
  }
  
  public BitMatrix clone()
  {
    return new BitMatrix(this.width, this.height, this.rowSize, (int[])this.bits.clone());
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof BitMatrix)) {
      return false;
    }
    paramObject = (BitMatrix)paramObject;
    return (this.width == ((BitMatrix)paramObject).width) && (this.height == ((BitMatrix)paramObject).height) && (this.rowSize == ((BitMatrix)paramObject).rowSize) && (Arrays.equals(this.bits, ((BitMatrix)paramObject).bits));
  }
  
  public void flip(int paramInt1, int paramInt2)
  {
    paramInt2 = paramInt2 * this.rowSize + paramInt1 / 32;
    int[] arrayOfInt = this.bits;
    arrayOfInt[paramInt2] = (1 << (paramInt1 & 0x1F) ^ arrayOfInt[paramInt2]);
  }
  
  public boolean get(int paramInt1, int paramInt2)
  {
    int i = this.rowSize;
    int j = paramInt1 / 32;
    return (this.bits[(paramInt2 * i + j)] >>> (paramInt1 & 0x1F) & 0x1) != 0;
  }
  
  public int[] getBottomRightOnBit()
  {
    for (int i = this.bits.length - 1; (i >= 0) && (this.bits[i] == 0); i--) {}
    if (i < 0) {
      return null;
    }
    int k = this.rowSize;
    int m = i / k;
    int n = this.bits[i];
    for (int j = 31; n >>> j == 0; j--) {}
    return new int[] { (i % k << 5) + j, m };
  }
  
  public int[] getEnclosingRectangle()
  {
    int j = this.width;
    int i1 = this.height;
    int k = -1;
    int m = -1;
    int i = 0;
    while (i < this.height)
    {
      int n = m;
      m = k;
      int i2 = 0;
      int i4;
      for (k = n;; k = i4)
      {
        n = this.rowSize;
        if (i2 >= n) {
          break;
        }
        int i8 = this.bits[(n * i + i2)];
        int i6 = j;
        int i5 = m;
        int i3 = i1;
        i4 = k;
        if (i8 != 0)
        {
          n = i1;
          if (i < i1) {
            n = i;
          }
          i1 = k;
          if (i > k) {
            i1 = i;
          }
          int i9 = i2 << 5;
          int i7 = 31;
          k = j;
          if (i9 < j)
          {
            for (k = 0; i8 << 31 - k == 0; k++) {}
            i3 = k + i9;
            k = j;
            if (i3 < j) {
              k = i3;
            }
          }
          i6 = k;
          i5 = m;
          i3 = n;
          i4 = i1;
          if (i9 + 31 > m)
          {
            for (j = i7; i8 >>> j == 0; j--) {}
            j = i9 + j;
            i6 = k;
            i5 = m;
            i3 = n;
            i4 = i1;
            if (j > m)
            {
              i5 = j;
              i4 = i1;
              i3 = n;
              i6 = k;
            }
          }
        }
        i2++;
        j = i6;
        m = i5;
        i1 = i3;
      }
      i++;
      n = m;
      m = k;
      k = n;
    }
    if ((k >= j) && (m >= i1)) {
      return new int[] { j, i1, k - j + 1, m - i1 + 1 };
    }
    return null;
  }
  
  public int getHeight()
  {
    return this.height;
  }
  
  public BitArray getRow(int paramInt, BitArray paramBitArray)
  {
    if ((paramBitArray != null) && (paramBitArray.getSize() >= this.width)) {
      paramBitArray.clear();
    } else {
      paramBitArray = new BitArray(this.width);
    }
    int j = this.rowSize;
    for (int i = 0; i < this.rowSize; i++) {
      paramBitArray.setBulk(i << 5, this.bits[(paramInt * j + i)]);
    }
    return paramBitArray;
  }
  
  public int getRowSize()
  {
    return this.rowSize;
  }
  
  public int[] getTopLeftOnBit()
  {
    for (int i = 0;; i++)
    {
      arrayOfInt = this.bits;
      if ((i >= arrayOfInt.length) || (arrayOfInt[i] != 0)) {
        break;
      }
    }
    int[] arrayOfInt = this.bits;
    if (i == arrayOfInt.length) {
      return null;
    }
    int n = this.rowSize;
    int k = i / n;
    int m = arrayOfInt[i];
    for (int j = 0; m << 31 - j == 0; j++) {}
    return new int[] { (i % n << 5) + j, k };
  }
  
  public int getWidth()
  {
    return this.width;
  }
  
  public int hashCode()
  {
    int i = this.width;
    return (((i * 31 + i) * 31 + this.height) * 31 + this.rowSize) * 31 + Arrays.hashCode(this.bits);
  }
  
  public void rotate180()
  {
    int i = getWidth();
    int j = getHeight();
    BitArray localBitArray2 = new BitArray(i);
    BitArray localBitArray1 = new BitArray(i);
    for (i = 0; i < (j + 1) / 2; i++)
    {
      localBitArray2 = getRow(i, localBitArray2);
      int k = j - 1 - i;
      localBitArray1 = getRow(k, localBitArray1);
      localBitArray2.reverse();
      localBitArray1.reverse();
      setRow(i, localBitArray1);
      setRow(k, localBitArray2);
    }
  }
  
  public void set(int paramInt1, int paramInt2)
  {
    paramInt2 = paramInt2 * this.rowSize + paramInt1 / 32;
    int[] arrayOfInt = this.bits;
    arrayOfInt[paramInt2] = (1 << (paramInt1 & 0x1F) | arrayOfInt[paramInt2]);
  }
  
  public void setRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt2 >= 0) && (paramInt1 >= 0))
    {
      if ((paramInt4 > 0) && (paramInt3 > 0))
      {
        int i = paramInt3 + paramInt1;
        paramInt4 += paramInt2;
        if ((paramInt4 <= this.height) && (i <= this.width))
        {
          while (paramInt2 < paramInt4)
          {
            int j = this.rowSize;
            for (paramInt3 = paramInt1; paramInt3 < i; paramInt3++)
            {
              int[] arrayOfInt = this.bits;
              int k = paramInt3 / 32 + j * paramInt2;
              arrayOfInt[k] |= 1 << (paramInt3 & 0x1F);
            }
            paramInt2++;
          }
          return;
        }
        throw new IllegalArgumentException("The region must fit inside the matrix");
      }
      throw new IllegalArgumentException("Height and width must be at least 1");
    }
    throw new IllegalArgumentException("Left and top must be nonnegative");
  }
  
  public void setRow(int paramInt, BitArray paramBitArray)
  {
    int[] arrayOfInt = paramBitArray.getBitArray();
    paramBitArray = this.bits;
    int i = this.rowSize;
    System.arraycopy(arrayOfInt, 0, paramBitArray, paramInt * i, i);
  }
  
  public String toString()
  {
    return toString("X ", "  ");
  }
  
  public String toString(String paramString1, String paramString2)
  {
    return buildToString(paramString1, paramString2, "\n");
  }
  
  @Deprecated
  public String toString(String paramString1, String paramString2, String paramString3)
  {
    return buildToString(paramString1, paramString2, paramString3);
  }
  
  public void unset(int paramInt1, int paramInt2)
  {
    paramInt2 = paramInt2 * this.rowSize + paramInt1 / 32;
    int[] arrayOfInt = this.bits;
    arrayOfInt[paramInt2] = ((1 << (paramInt1 & 0x1F) ^ 0xFFFFFFFF) & arrayOfInt[paramInt2]);
  }
  
  public void xor(BitMatrix paramBitMatrix)
  {
    if ((this.width == paramBitMatrix.getWidth()) && (this.height == paramBitMatrix.getHeight()) && (this.rowSize == paramBitMatrix.getRowSize()))
    {
      BitArray localBitArray = new BitArray(this.width / 32 + 1);
      for (int i = 0; i < this.height; i++)
      {
        int k = this.rowSize;
        int[] arrayOfInt2 = paramBitMatrix.getRow(i, localBitArray).getBitArray();
        for (int j = 0; j < this.rowSize; j++)
        {
          int[] arrayOfInt1 = this.bits;
          int m = k * i + j;
          arrayOfInt1[m] ^= arrayOfInt2[j];
        }
      }
      return;
    }
    throw new IllegalArgumentException("input matrix dimensions do not match");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/BitMatrix.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */