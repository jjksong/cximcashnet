package com.google.zxing.common;

import com.google.zxing.DecodeHintType;
import java.nio.charset.Charset;
import java.util.Map;

public final class StringUtils
{
  private static final boolean ASSUME_SHIFT_JIS;
  private static final String EUC_JP = "EUC_JP";
  public static final String GB2312 = "GB2312";
  private static final String ISO88591 = "ISO8859_1";
  private static final String PLATFORM_DEFAULT_ENCODING = Charset.defaultCharset().name();
  public static final String SHIFT_JIS = "SJIS";
  private static final String UTF8 = "UTF8";
  
  static
  {
    boolean bool;
    if ((!"SJIS".equalsIgnoreCase(PLATFORM_DEFAULT_ENCODING)) && (!"EUC_JP".equalsIgnoreCase(PLATFORM_DEFAULT_ENCODING))) {
      bool = false;
    } else {
      bool = true;
    }
    ASSUME_SHIFT_JIS = bool;
  }
  
  public static String guessEncoding(byte[] paramArrayOfByte, Map<DecodeHintType, ?> paramMap)
  {
    byte[] arrayOfByte = paramArrayOfByte;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.CHARACTER_SET))) {
      return paramMap.get(DecodeHintType.CHARACTER_SET).toString();
    }
    int i21 = arrayOfByte.length;
    int i = arrayOfByte.length;
    int i5 = 0;
    int i3;
    if ((i > 3) && (arrayOfByte[0] == -17) && (arrayOfByte[1] == -69) && (arrayOfByte[2] == -65)) {
      i3 = 1;
    } else {
      i3 = 0;
    }
    int i10 = 0;
    int i7 = 0;
    int i18 = 1;
    int m = 1;
    int n = 1;
    int i2 = 0;
    int i6 = 0;
    int i19 = 0;
    int i17 = 0;
    int i16 = 0;
    int i4 = 0;
    int i1 = 0;
    int i11 = 0;
    label381:
    int k;
    for (int i9 = 0; (i7 < i21) && ((i18 != 0) || (m != 0) || (n != 0)); i9 = k)
    {
      int i22 = paramArrayOfByte[i7] & 0xFF;
      int i8 = n;
      i = i2;
      int i12 = i19;
      int i13 = i17;
      int i14 = i16;
      if (n != 0)
      {
        if (i2 > 0)
        {
          i = i2;
          if ((i22 & 0x80) != 0)
          {
            i = i2 - 1;
            i8 = n;
            i12 = i19;
            i13 = i17;
            i14 = i16;
            break label381;
          }
        }
        else
        {
          i8 = n;
          i = i2;
          i12 = i19;
          i13 = i17;
          i14 = i16;
          if ((i22 & 0x80) == 0) {
            break label381;
          }
          i = i2;
          if ((i22 & 0x40) != 0)
          {
            i = i2 + 1;
            if ((i22 & 0x20) == 0)
            {
              i12 = i19 + 1;
              i8 = n;
              i13 = i17;
              i14 = i16;
              break label381;
            }
            i++;
            if ((i22 & 0x10) == 0)
            {
              i13 = i17 + 1;
              i8 = n;
              i12 = i19;
              i14 = i16;
              break label381;
            }
            j = i + 1;
            i = j;
            if ((i22 & 0x8) == 0)
            {
              i14 = i16 + 1;
              i8 = n;
              i = j;
              i12 = i19;
              i13 = i17;
              break label381;
            }
          }
        }
        i8 = 0;
        i14 = i16;
        i13 = i17;
        i12 = i19;
      }
      i2 = i18;
      int i15 = i1;
      if (i18 != 0) {
        if ((i22 > 127) && (i22 < 160))
        {
          i2 = 0;
          i15 = i1;
        }
        else
        {
          i2 = i18;
          i15 = i1;
          if (i22 > 159) {
            if ((i22 >= 192) && (i22 != 215))
            {
              i2 = i18;
              i15 = i1;
              if (i22 != 247) {}
            }
            else
            {
              i15 = i1 + 1;
              i2 = i18;
            }
          }
        }
      }
      i16 = i10;
      i17 = i5;
      n = m;
      i1 = i6;
      int i20 = i4;
      j = i11;
      k = i9;
      if (m != 0) {
        if (i6 > 0)
        {
          if ((i22 >= 64) && (i22 != 127) && (i22 <= 252))
          {
            i1 = i6 - 1;
            i16 = i10;
            i17 = i5;
            n = m;
            i20 = i4;
            j = i11;
            k = i9;
          }
          else
          {
            n = 0;
            i16 = i10;
            i17 = i5;
            i1 = i6;
            i20 = i4;
            j = i11;
            k = i9;
          }
        }
        else if ((i22 != 128) && (i22 != 160) && (i22 <= 239))
        {
          if ((i22 > 160) && (i22 < 224))
          {
            i16 = i10 + 1;
            j = i9 + 1;
            if (j > i4)
            {
              k = j;
              i4 = 0;
              i17 = i5;
              n = m;
              i1 = i6;
              i20 = j;
              j = i4;
            }
            else
            {
              k = j;
              j = 0;
              i17 = i5;
              n = m;
              i1 = i6;
              i20 = i4;
            }
          }
          else if (i22 > 127)
          {
            i1 = i6 + 1;
            j = i11 + 1;
            if (j > i5)
            {
              k = j;
              i5 = 0;
              i16 = i10;
              i17 = j;
              n = m;
              i20 = i4;
              j = k;
              k = i5;
            }
            else
            {
              k = 0;
              i16 = i10;
              i17 = i5;
              n = m;
              i20 = i4;
            }
          }
          else
          {
            j = 0;
            k = 0;
            i16 = i10;
            i17 = i5;
            n = m;
            i1 = i6;
            i20 = i4;
          }
        }
        else
        {
          n = 0;
          k = i9;
          j = i11;
          i20 = i4;
          i1 = i6;
          i17 = i5;
          i16 = i10;
        }
      }
      i7++;
      i10 = i16;
      i5 = i17;
      i18 = i2;
      m = n;
      n = i8;
      i2 = i;
      i6 = i1;
      i19 = i12;
      i17 = i13;
      i16 = i14;
      i4 = i20;
      i1 = i15;
      i11 = j;
    }
    i = n;
    if (n != 0)
    {
      i = n;
      if (i2 > 0) {
        i = 0;
      }
    }
    int j = m;
    if (m != 0)
    {
      j = m;
      if (i6 > 0) {
        j = 0;
      }
    }
    if ((i != 0) && ((i3 != 0) || (i19 + i17 + i16 > 0))) {
      return "UTF8";
    }
    if ((j != 0) && ((ASSUME_SHIFT_JIS) || (i4 >= 3) || (i5 >= 3))) {
      return "SJIS";
    }
    if ((i18 != 0) && (j != 0))
    {
      if (((i4 == 2) && (i10 == 2)) || (i1 * 10 >= i21)) {
        return "SJIS";
      }
      return "ISO8859_1";
    }
    if (i18 != 0) {
      return "ISO8859_1";
    }
    if (j != 0) {
      return "SJIS";
    }
    if (i != 0) {
      return "UTF8";
    }
    return PLATFORM_DEFAULT_ENCODING;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/StringUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */