package com.google.zxing.common;

public final class BitSource
{
  private int bitOffset;
  private int byteOffset;
  private final byte[] bytes;
  
  public BitSource(byte[] paramArrayOfByte)
  {
    this.bytes = paramArrayOfByte;
  }
  
  public int available()
  {
    return (this.bytes.length - this.byteOffset) * 8 - this.bitOffset;
  }
  
  public int getBitOffset()
  {
    return this.bitOffset;
  }
  
  public int getByteOffset()
  {
    return this.byteOffset;
  }
  
  public int readBits(int paramInt)
  {
    if ((paramInt > 0) && (paramInt <= 32) && (paramInt <= available()))
    {
      int i = this.bitOffset;
      byte[] arrayOfByte;
      if (i > 0)
      {
        j = 8 - i;
        if (paramInt < j) {
          i = paramInt;
        } else {
          i = j;
        }
        j -= i;
        arrayOfByte = this.bytes;
        int m = this.byteOffset;
        j = (255 >> 8 - i << j & arrayOfByte[m]) >> j;
        int k = paramInt - i;
        this.bitOffset += i;
        paramInt = j;
        i = k;
        if (this.bitOffset == 8)
        {
          this.bitOffset = 0;
          this.byteOffset = (m + 1);
          paramInt = j;
          i = k;
        }
      }
      else
      {
        j = 0;
        i = paramInt;
        paramInt = j;
      }
      int j = paramInt;
      if (i > 0)
      {
        while (i >= 8)
        {
          arrayOfByte = this.bytes;
          j = this.byteOffset;
          paramInt = paramInt << 8 | arrayOfByte[j] & 0xFF;
          this.byteOffset = (j + 1);
          i -= 8;
        }
        j = paramInt;
        if (i > 0)
        {
          j = 8 - i;
          j = paramInt << i | (255 >> j << j & this.bytes[this.byteOffset]) >> j;
          this.bitOffset += i;
        }
      }
      return j;
    }
    throw new IllegalArgumentException(String.valueOf(paramInt));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/BitSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */