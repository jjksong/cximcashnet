package com.google.zxing.common.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;

@Deprecated
public final class MonochromeRectangleDetector
{
  private static final int MAX_MODULES = 32;
  private final BitMatrix image;
  
  public MonochromeRectangleDetector(BitMatrix paramBitMatrix)
  {
    this.image = paramBitMatrix;
  }
  
  private int[] blackWhiteRange(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
  {
    int j = (paramInt3 + paramInt4) / 2;
    int i = j;
    while (i >= paramInt3) {
      if (paramBoolean ? this.image.get(i, paramInt1) : this.image.get(paramInt1, i))
      {
        i--;
      }
      else
      {
        int m = i;
        do
        {
          do
          {
            k = m - 1;
            if (k < paramInt3) {
              break label116;
            }
            if (!paramBoolean) {
              break;
            }
            m = k;
          } while (!this.image.get(k, paramInt1));
          break;
          m = k;
        } while (!this.image.get(paramInt1, k));
        label116:
        if ((k < paramInt3) || (i - k > paramInt2)) {
          break;
        }
        i = k;
      }
    }
    int k = i + 1;
    paramInt3 = j;
    while (paramInt3 < paramInt4) {
      if (paramBoolean ? this.image.get(paramInt3, paramInt1) : this.image.get(paramInt1, paramInt3))
      {
        paramInt3++;
      }
      else
      {
        j = paramInt3;
        do
        {
          do
          {
            i = j + 1;
            if (i >= paramInt4) {
              break label252;
            }
            if (!paramBoolean) {
              break;
            }
            j = i;
          } while (!this.image.get(i, paramInt1));
          break;
          j = i;
        } while (!this.image.get(paramInt1, i));
        label252:
        if ((i >= paramInt4) || (i - paramInt3 > paramInt2)) {
          break;
        }
        paramInt3 = i;
      }
    }
    paramInt1 = paramInt3 - 1;
    if (paramInt1 > k) {
      return new int[] { k, paramInt1 };
    }
    return null;
  }
  
  private ResultPoint findCornerFromCenter(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9)
    throws NotFoundException
  {
    int i = paramInt1;
    int j = paramInt5;
    int[] arrayOfInt;
    for (Object localObject = null; (j < paramInt8) && (j >= paramInt7) && (i < paramInt4) && (i >= paramInt3); localObject = arrayOfInt)
    {
      if (paramInt2 == 0) {
        arrayOfInt = blackWhiteRange(j, paramInt9, paramInt3, paramInt4, true);
      } else {
        arrayOfInt = blackWhiteRange(i, paramInt9, paramInt7, paramInt8, false);
      }
      if (arrayOfInt == null)
      {
        if (localObject != null)
        {
          paramInt4 = 1;
          paramInt3 = 1;
          if (paramInt2 == 0)
          {
            paramInt2 = j - paramInt6;
            if (localObject[0] < paramInt1)
            {
              if (localObject[1] > paramInt1)
              {
                paramInt1 = paramInt3;
                if (paramInt6 > 0) {
                  paramInt1 = 0;
                }
                return new ResultPoint(localObject[paramInt1], paramInt2);
              }
              return new ResultPoint(localObject[0], paramInt2);
            }
            return new ResultPoint(localObject[1], paramInt2);
          }
          paramInt1 = i - paramInt2;
          if (localObject[0] < paramInt5)
          {
            if (localObject[1] > paramInt5)
            {
              float f = paramInt1;
              paramInt1 = paramInt4;
              if (paramInt2 < 0) {
                paramInt1 = 0;
              }
              return new ResultPoint(f, localObject[paramInt1]);
            }
            return new ResultPoint(paramInt1, localObject[0]);
          }
          return new ResultPoint(paramInt1, localObject[1]);
        }
        throw NotFoundException.getNotFoundInstance();
      }
      j += paramInt6;
      i += paramInt2;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public ResultPoint[] detect()
    throws NotFoundException
  {
    int i = this.image.getHeight();
    int i5 = this.image.getWidth();
    int j = i / 2;
    int m = i5 / 2;
    int i2 = Math.max(1, i / 256);
    int i3 = Math.max(1, i5 / 256);
    int i1 = -i2;
    int k = m / 2;
    int n = (int)findCornerFromCenter(m, 0, 0, i5, j, i1, 0, i, k).getY() - 1;
    int i6 = -i3;
    int i4 = j / 2;
    ResultPoint localResultPoint1 = findCornerFromCenter(m, i6, 0, i5, j, 0, n, i, i4);
    i6 = (int)localResultPoint1.getX() - 1;
    ResultPoint localResultPoint2 = findCornerFromCenter(m, i3, i6, i5, j, 0, n, i, i4);
    i3 = (int)localResultPoint2.getX() + 1;
    ResultPoint localResultPoint3 = findCornerFromCenter(m, 0, i6, i3, j, i2, n, i, k);
    return new ResultPoint[] { findCornerFromCenter(m, 0, i6, i3, j, i1, n, (int)localResultPoint3.getY() + 1, m / 4), localResultPoint1, localResultPoint2, localResultPoint3 };
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/detector/MonochromeRectangleDetector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */