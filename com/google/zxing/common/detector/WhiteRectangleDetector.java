package com.google.zxing.common.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;

public final class WhiteRectangleDetector
{
  private static final int CORR = 1;
  private static final int INIT_SIZE = 10;
  private final int downInit;
  private final int height;
  private final BitMatrix image;
  private final int leftInit;
  private final int rightInit;
  private final int upInit;
  private final int width;
  
  public WhiteRectangleDetector(BitMatrix paramBitMatrix)
    throws NotFoundException
  {
    this(paramBitMatrix, 10, paramBitMatrix.getWidth() / 2, paramBitMatrix.getHeight() / 2);
  }
  
  public WhiteRectangleDetector(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, int paramInt3)
    throws NotFoundException
  {
    this.image = paramBitMatrix;
    this.height = paramBitMatrix.getHeight();
    this.width = paramBitMatrix.getWidth();
    paramInt1 /= 2;
    this.leftInit = (paramInt2 - paramInt1);
    this.rightInit = (paramInt2 + paramInt1);
    this.upInit = (paramInt3 - paramInt1);
    this.downInit = (paramInt3 + paramInt1);
    if ((this.upInit >= 0) && (this.leftInit >= 0) && (this.downInit < this.height) && (this.rightInit < this.width)) {
      return;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private ResultPoint[] centerEdges(ResultPoint paramResultPoint1, ResultPoint paramResultPoint2, ResultPoint paramResultPoint3, ResultPoint paramResultPoint4)
  {
    float f1 = paramResultPoint1.getX();
    float f5 = paramResultPoint1.getY();
    float f8 = paramResultPoint2.getX();
    float f3 = paramResultPoint2.getY();
    float f7 = paramResultPoint3.getX();
    float f4 = paramResultPoint3.getY();
    float f6 = paramResultPoint4.getX();
    float f2 = paramResultPoint4.getY();
    if (f1 < this.width / 2.0F) {
      return new ResultPoint[] { new ResultPoint(f6 - 1.0F, f2 + 1.0F), new ResultPoint(f8 + 1.0F, f3 + 1.0F), new ResultPoint(f7 - 1.0F, f4 - 1.0F), new ResultPoint(f1 + 1.0F, f5 - 1.0F) };
    }
    return new ResultPoint[] { new ResultPoint(f6 + 1.0F, f2 + 1.0F), new ResultPoint(f8 + 1.0F, f3 - 1.0F), new ResultPoint(f7 - 1.0F, f4 + 1.0F), new ResultPoint(f1 - 1.0F, f5 - 1.0F) };
  }
  
  private boolean containsBlackPoint(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    int i = paramInt1;
    if (paramBoolean) {
      while (paramInt1 <= paramInt2)
      {
        if (this.image.get(paramInt1, paramInt3)) {
          return true;
        }
        paramInt1++;
      }
    }
    while (i <= paramInt2)
    {
      if (this.image.get(paramInt3, i)) {
        return true;
      }
      i++;
    }
    return false;
  }
  
  private ResultPoint getBlackPointOnSegment(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    int j = MathUtils.round(MathUtils.distance(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    float f = j;
    paramFloat3 = (paramFloat3 - paramFloat1) / f;
    paramFloat4 = (paramFloat4 - paramFloat2) / f;
    for (int i = 0; i < j; i++)
    {
      f = i;
      int k = MathUtils.round(f * paramFloat3 + paramFloat1);
      int m = MathUtils.round(f * paramFloat4 + paramFloat2);
      if (this.image.get(k, m)) {
        return new ResultPoint(k, m);
      }
    }
    return null;
  }
  
  public ResultPoint[] detect()
    throws NotFoundException
  {
    int k = this.leftInit;
    int n = this.rightInit;
    int m = this.upInit;
    int j = this.downInit;
    int i12 = 0;
    int i11 = 1;
    int i9 = 1;
    int i8 = 0;
    int i6 = 0;
    int i4 = 0;
    int i2 = 0;
    int i3 = 0;
    int i1;
    int i7;
    int i5;
    int i;
    int i10;
    for (;;)
    {
      i1 = n;
      i7 = m;
      i5 = j;
      i = i12;
      i10 = k;
      if (i9 == 0) {
        break;
      }
      int i13 = 1;
      i1 = 0;
      i5 = i8;
      i = n;
      boolean bool;
      while (((i13 != 0) || (i5 == 0)) && (i < this.width))
      {
        bool = containsBlackPoint(m, j, i, false);
        if (bool)
        {
          i++;
          i5 = 1;
          i1 = 1;
          i13 = bool;
        }
        else
        {
          i13 = bool;
          if (i5 == 0)
          {
            i++;
            i13 = bool;
          }
        }
      }
      if (i >= this.width)
      {
        n = 1;
        i1 = i;
        i7 = m;
        i5 = j;
        i = n;
        i10 = k;
        break;
      }
      i13 = 1;
      while (((i13 != 0) || (i6 == 0)) && (j < this.height))
      {
        bool = containsBlackPoint(k, i, j, true);
        if (bool)
        {
          j++;
          i6 = 1;
          i1 = 1;
          i13 = bool;
        }
        else
        {
          i13 = bool;
          if (i6 == 0)
          {
            j++;
            i13 = bool;
          }
        }
      }
      if (j >= this.height)
      {
        n = 1;
        i1 = i;
        i7 = m;
        i5 = j;
        i = n;
        i10 = k;
        break;
      }
      i13 = 1;
      n = i1;
      while (((i13 != 0) || (i4 == 0)) && (k >= 0))
      {
        bool = containsBlackPoint(m, j, k, false);
        if (bool)
        {
          k--;
          i4 = 1;
          n = 1;
          i13 = bool;
        }
        else
        {
          i13 = bool;
          if (i4 == 0)
          {
            k--;
            i13 = bool;
          }
        }
      }
      if (k < 0)
      {
        n = 1;
        i1 = i;
        i7 = m;
        i5 = j;
        i = n;
        i10 = k;
        break;
      }
      i13 = 1;
      while (((i13 != 0) || (i3 == 0)) && (m >= 0))
      {
        bool = containsBlackPoint(k, i, m, true);
        if (bool)
        {
          m--;
          i3 = 1;
          n = 1;
          i13 = bool;
        }
        else
        {
          i13 = bool;
          if (i3 == 0)
          {
            m--;
            i13 = bool;
          }
        }
      }
      if (m < 0)
      {
        n = 1;
        i1 = i;
        i7 = m;
        i5 = j;
        i = n;
        i10 = k;
        break;
      }
      if (n != 0) {
        i2 = 1;
      }
      i9 = n;
      n = i;
      i8 = i5;
    }
    if ((i == 0) && (i2 != 0))
    {
      j = i1 - i10;
      ResultPoint localResultPoint4 = null;
      ResultPoint localResultPoint1 = null;
      for (i = 1; (localResultPoint1 == null) && (i < j); i++) {
        localResultPoint1 = getBlackPointOnSegment(i10, i5 - i, i10 + i, i5);
      }
      if (localResultPoint1 != null)
      {
        ResultPoint localResultPoint2 = null;
        for (i = 1; (localResultPoint2 == null) && (i < j); i++) {
          localResultPoint2 = getBlackPointOnSegment(i10, i7 + i, i10 + i, i7);
        }
        if (localResultPoint2 != null)
        {
          ResultPoint localResultPoint3 = null;
          for (i = 1; (localResultPoint3 == null) && (i < j); i++) {
            localResultPoint3 = getBlackPointOnSegment(i1, i7 + i, i1 - i, i7);
          }
          if (localResultPoint3 != null)
          {
            for (i = i11; (localResultPoint4 == null) && (i < j); i++) {
              localResultPoint4 = getBlackPointOnSegment(i1, i5 - i, i1 - i, i5);
            }
            if (localResultPoint4 != null) {
              return centerEdges(localResultPoint4, localResultPoint1, localResultPoint3, localResultPoint2);
            }
            throw NotFoundException.getNotFoundInstance();
          }
          throw NotFoundException.getNotFoundInstance();
        }
        throw NotFoundException.getNotFoundInstance();
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/detector/WhiteRectangleDetector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */