package com.google.zxing.common;

public final class PerspectiveTransform
{
  private final float a11;
  private final float a12;
  private final float a13;
  private final float a21;
  private final float a22;
  private final float a23;
  private final float a31;
  private final float a32;
  private final float a33;
  
  private PerspectiveTransform(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9)
  {
    this.a11 = paramFloat1;
    this.a12 = paramFloat4;
    this.a13 = paramFloat7;
    this.a21 = paramFloat2;
    this.a22 = paramFloat5;
    this.a23 = paramFloat8;
    this.a31 = paramFloat3;
    this.a32 = paramFloat6;
    this.a33 = paramFloat9;
  }
  
  public static PerspectiveTransform quadrilateralToQuadrilateral(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9, float paramFloat10, float paramFloat11, float paramFloat12, float paramFloat13, float paramFloat14, float paramFloat15, float paramFloat16)
  {
    PerspectiveTransform localPerspectiveTransform = quadrilateralToSquare(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8);
    return squareToQuadrilateral(paramFloat9, paramFloat10, paramFloat11, paramFloat12, paramFloat13, paramFloat14, paramFloat15, paramFloat16).times(localPerspectiveTransform);
  }
  
  public static PerspectiveTransform quadrilateralToSquare(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
  {
    return squareToQuadrilateral(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8).buildAdjoint();
  }
  
  public static PerspectiveTransform squareToQuadrilateral(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
  {
    float f3 = paramFloat1 - paramFloat3 + paramFloat5 - paramFloat7;
    float f1 = paramFloat2 - paramFloat4 + paramFloat6 - paramFloat8;
    if ((f3 == 0.0F) && (f1 == 0.0F)) {
      return new PerspectiveTransform(paramFloat3 - paramFloat1, paramFloat5 - paramFloat3, paramFloat1, paramFloat4 - paramFloat2, paramFloat6 - paramFloat4, paramFloat2, 0.0F, 0.0F, 1.0F);
    }
    float f2 = paramFloat3 - paramFloat5;
    float f4 = paramFloat7 - paramFloat5;
    paramFloat5 = paramFloat4 - paramFloat6;
    float f5 = paramFloat8 - paramFloat6;
    paramFloat6 = f2 * f5 - f4 * paramFloat5;
    f4 = (f5 * f3 - f4 * f1) / paramFloat6;
    paramFloat5 = (f2 * f1 - f3 * paramFloat5) / paramFloat6;
    return new PerspectiveTransform(f4 * paramFloat3 + (paramFloat3 - paramFloat1), paramFloat5 * paramFloat7 + (paramFloat7 - paramFloat1), paramFloat1, paramFloat4 - paramFloat2 + f4 * paramFloat4, paramFloat8 - paramFloat2 + paramFloat5 * paramFloat8, paramFloat2, f4, paramFloat5, 1.0F);
  }
  
  PerspectiveTransform buildAdjoint()
  {
    float f8 = this.a22;
    float f2 = this.a33;
    float f4 = this.a23;
    float f6 = this.a32;
    float f3 = this.a31;
    float f1 = this.a21;
    float f7 = this.a13;
    float f5 = this.a12;
    float f9 = this.a11;
    return new PerspectiveTransform(f8 * f2 - f4 * f6, f4 * f3 - f1 * f2, f1 * f6 - f8 * f3, f7 * f6 - f5 * f2, f2 * f9 - f7 * f3, f3 * f5 - f6 * f9, f5 * f4 - f7 * f8, f7 * f1 - f4 * f9, f9 * f8 - f5 * f1);
  }
  
  PerspectiveTransform times(PerspectiveTransform paramPerspectiveTransform)
  {
    float f3 = this.a11;
    float f1 = paramPerspectiveTransform.a11;
    float f8 = this.a21;
    float f7 = paramPerspectiveTransform.a12;
    float f17 = this.a31;
    float f13 = paramPerspectiveTransform.a13;
    float f2 = paramPerspectiveTransform.a21;
    float f10 = paramPerspectiveTransform.a22;
    float f14 = paramPerspectiveTransform.a23;
    float f9 = paramPerspectiveTransform.a31;
    float f15 = paramPerspectiveTransform.a32;
    float f18 = paramPerspectiveTransform.a33;
    float f6 = this.a12;
    float f4 = this.a22;
    float f11 = this.a32;
    float f12 = this.a13;
    float f5 = this.a23;
    float f16 = this.a33;
    return new PerspectiveTransform(f3 * f1 + f8 * f7 + f17 * f13, f3 * f2 + f8 * f10 + f17 * f14, f3 * f9 + f8 * f15 + f17 * f18, f6 * f1 + f4 * f7 + f11 * f13, f6 * f2 + f4 * f10 + f11 * f14, f11 * f18 + (f6 * f9 + f4 * f15), f13 * f16 + (f1 * f12 + f7 * f5), f2 * f12 + f10 * f5 + f14 * f16, f12 * f9 + f5 * f15 + f16 * f18);
  }
  
  public void transformPoints(float[] paramArrayOfFloat)
  {
    int j = paramArrayOfFloat.length;
    float f6 = this.a11;
    float f3 = this.a12;
    float f4 = this.a13;
    float f8 = this.a21;
    float f7 = this.a22;
    float f11 = this.a23;
    float f12 = this.a31;
    float f10 = this.a32;
    float f5 = this.a33;
    for (int i = 0; i < j; i += 2)
    {
      float f2 = paramArrayOfFloat[i];
      int k = i + 1;
      float f9 = paramArrayOfFloat[k];
      float f1 = f4 * f2 + f11 * f9 + f5;
      paramArrayOfFloat[i] = ((f6 * f2 + f8 * f9 + f12) / f1);
      paramArrayOfFloat[k] = ((f2 * f3 + f9 * f7 + f10) / f1);
    }
  }
  
  public void transformPoints(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    int j = paramArrayOfFloat1.length;
    for (int i = 0; i < j; i++)
    {
      float f2 = paramArrayOfFloat1[i];
      float f1 = paramArrayOfFloat2[i];
      float f3 = this.a13 * f2 + this.a23 * f1 + this.a33;
      paramArrayOfFloat1[i] = ((this.a11 * f2 + this.a21 * f1 + this.a31) / f3);
      paramArrayOfFloat2[i] = ((this.a12 * f2 + this.a22 * f1 + this.a32) / f3);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/PerspectiveTransform.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */