package com.google.zxing.common.reedsolomon;

public final class ReedSolomonException
  extends Exception
{
  public ReedSolomonException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/reedsolomon/ReedSolomonException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */