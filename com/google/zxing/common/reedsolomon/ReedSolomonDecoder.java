package com.google.zxing.common.reedsolomon;

public final class ReedSolomonDecoder
{
  private final GenericGF field;
  
  public ReedSolomonDecoder(GenericGF paramGenericGF)
  {
    this.field = paramGenericGF;
  }
  
  private int[] findErrorLocations(GenericGFPoly paramGenericGFPoly)
    throws ReedSolomonException
  {
    int m = paramGenericGFPoly.getDegree();
    int k = 0;
    int i = 1;
    if (m == 1) {
      return new int[] { paramGenericGFPoly.getCoefficient(1) };
    }
    int[] arrayOfInt = new int[m];
    while ((i < this.field.getSize()) && (k < m))
    {
      int j = k;
      if (paramGenericGFPoly.evaluateAt(i) == 0)
      {
        arrayOfInt[k] = this.field.inverse(i);
        j = k + 1;
      }
      i++;
      k = j;
    }
    if (k == m) {
      return arrayOfInt;
    }
    throw new ReedSolomonException("Error locator degree does not match number of roots");
  }
  
  private int[] findErrorMagnitudes(GenericGFPoly paramGenericGFPoly, int[] paramArrayOfInt)
  {
    int n = paramArrayOfInt.length;
    int[] arrayOfInt = new int[n];
    for (int i = 0; i < n; i++)
    {
      int i1 = this.field.inverse(paramArrayOfInt[i]);
      int j = 0;
      int m;
      for (int k = 1; j < n; k = m)
      {
        m = k;
        if (i != j)
        {
          m = this.field.multiply(paramArrayOfInt[j], i1);
          if ((m & 0x1) == 0) {
            m |= 0x1;
          } else {
            m &= 0xFFFFFFFE;
          }
          m = this.field.multiply(k, m);
        }
        j++;
      }
      arrayOfInt[i] = this.field.multiply(paramGenericGFPoly.evaluateAt(i1), this.field.inverse(k));
      if (this.field.getGeneratorBase() != 0) {
        arrayOfInt[i] = this.field.multiply(arrayOfInt[i], i1);
      }
    }
    return arrayOfInt;
  }
  
  private GenericGFPoly[] runEuclideanAlgorithm(GenericGFPoly paramGenericGFPoly1, GenericGFPoly paramGenericGFPoly2, int paramInt)
    throws ReedSolomonException
  {
    Object localObject2 = paramGenericGFPoly1;
    Object localObject1 = paramGenericGFPoly2;
    if (paramGenericGFPoly1.getDegree() < paramGenericGFPoly2.getDegree())
    {
      localObject1 = paramGenericGFPoly1;
      localObject2 = paramGenericGFPoly2;
    }
    GenericGFPoly localGenericGFPoly1 = this.field.getZero();
    paramGenericGFPoly2 = this.field.getOne();
    paramGenericGFPoly1 = (GenericGFPoly)localObject1;
    localObject1 = localObject2;
    localObject2 = localGenericGFPoly1;
    while (paramGenericGFPoly1.getDegree() >= paramInt / 2) {
      if (!paramGenericGFPoly1.isZero())
      {
        localGenericGFPoly1 = this.field.getZero();
        int i = paramGenericGFPoly1.getCoefficient(paramGenericGFPoly1.getDegree());
        int k = this.field.inverse(i);
        while ((((GenericGFPoly)localObject1).getDegree() >= paramGenericGFPoly1.getDegree()) && (!((GenericGFPoly)localObject1).isZero()))
        {
          int j = ((GenericGFPoly)localObject1).getDegree() - paramGenericGFPoly1.getDegree();
          i = this.field.multiply(((GenericGFPoly)localObject1).getCoefficient(((GenericGFPoly)localObject1).getDegree()), k);
          localGenericGFPoly1 = localGenericGFPoly1.addOrSubtract(this.field.buildMonomial(j, i));
          localObject1 = ((GenericGFPoly)localObject1).addOrSubtract(paramGenericGFPoly1.multiplyByMonomial(j, i));
        }
        GenericGFPoly localGenericGFPoly2 = localGenericGFPoly1.multiply(paramGenericGFPoly2).addOrSubtract((GenericGFPoly)localObject2);
        if (((GenericGFPoly)localObject1).getDegree() < paramGenericGFPoly1.getDegree())
        {
          localGenericGFPoly1 = paramGenericGFPoly1;
          paramGenericGFPoly1 = (GenericGFPoly)localObject1;
          localObject2 = paramGenericGFPoly2;
          paramGenericGFPoly2 = localGenericGFPoly2;
          localObject1 = localGenericGFPoly1;
        }
        else
        {
          throw new IllegalStateException("Division algorithm failed to reduce polynomial?");
        }
      }
      else
      {
        throw new ReedSolomonException("r_{i-1} was zero");
      }
    }
    paramInt = paramGenericGFPoly2.getCoefficient(0);
    if (paramInt != 0)
    {
      paramInt = this.field.inverse(paramInt);
      return new GenericGFPoly[] { paramGenericGFPoly2.multiply(paramInt), paramGenericGFPoly1.multiply(paramInt) };
    }
    throw new ReedSolomonException("sigmaTilde(0) was zero");
  }
  
  public void decode(int[] paramArrayOfInt, int paramInt)
    throws ReedSolomonException
  {
    Object localObject1 = new GenericGFPoly(this.field, paramArrayOfInt);
    Object localObject2 = new int[paramInt];
    int k = 0;
    int i = 0;
    int j = 1;
    while (i < paramInt)
    {
      GenericGF localGenericGF = this.field;
      int m = ((GenericGFPoly)localObject1).evaluateAt(localGenericGF.exp(localGenericGF.getGeneratorBase() + i));
      localObject2[(paramInt - 1 - i)] = m;
      if (m != 0) {
        j = 0;
      }
      i++;
    }
    if (j != 0) {
      return;
    }
    localObject1 = new GenericGFPoly(this.field, (int[])localObject2);
    localObject2 = runEuclideanAlgorithm(this.field.buildMonomial(paramInt, 1), (GenericGFPoly)localObject1, paramInt);
    localObject1 = localObject2[0];
    localObject2 = localObject2[1];
    localObject1 = findErrorLocations((GenericGFPoly)localObject1);
    localObject2 = findErrorMagnitudes((GenericGFPoly)localObject2, (int[])localObject1);
    paramInt = k;
    while (paramInt < localObject1.length)
    {
      i = paramArrayOfInt.length - 1 - this.field.log(localObject1[paramInt]);
      if (i >= 0)
      {
        paramArrayOfInt[i] = GenericGF.addOrSubtract(paramArrayOfInt[i], localObject2[paramInt]);
        paramInt++;
      }
      else
      {
        throw new ReedSolomonException("Bad error location");
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/reedsolomon/ReedSolomonDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */