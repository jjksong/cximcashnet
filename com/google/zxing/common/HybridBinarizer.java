package com.google.zxing.common;

import com.google.zxing.Binarizer;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import java.lang.reflect.Array;

public final class HybridBinarizer
  extends GlobalHistogramBinarizer
{
  private static final int BLOCK_SIZE = 8;
  private static final int BLOCK_SIZE_MASK = 7;
  private static final int BLOCK_SIZE_POWER = 3;
  private static final int MINIMUM_DIMENSION = 40;
  private static final int MIN_DYNAMIC_RANGE = 24;
  private BitMatrix matrix;
  
  public HybridBinarizer(LuminanceSource paramLuminanceSource)
  {
    super(paramLuminanceSource);
  }
  
  private static int[][] calculateBlackPoints(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int[][] arrayOfInt1 = (int[][])Array.newInstance(Integer.TYPE, new int[] { paramInt2, paramInt1 });
    for (int m = 0; m < paramInt2; m++)
    {
      int i = m << 3;
      int j = paramInt4 - 8;
      int n = i;
      if (i > j) {
        n = j;
      }
      for (int i1 = 0; i1 < paramInt1; i1++)
      {
        j = i1 << 3;
        int k = paramInt3 - 8;
        i = j;
        if (j > k) {
          i = k;
        }
        j = n * paramInt3 + i;
        i = 0;
        k = 0;
        int i2 = 0;
        int i3 = 255;
        while (i < 8)
        {
          int i4 = 0;
          int i5;
          int i6;
          while (i4 < 8)
          {
            i5 = paramArrayOfByte[(j + i4)] & 0xFF;
            i6 = k + i5;
            k = i3;
            if (i5 < i3) {
              k = i5;
            }
            i3 = i2;
            if (i5 > i2) {
              i3 = i5;
            }
            i4++;
            i2 = i3;
            i3 = k;
            k = i6;
          }
          if (i2 - i3 > 24)
          {
            i4 = j;
            int i7 = i;
            j = i7 + 1;
            i = i4 + paramInt3;
            if (j < 8)
            {
              i6 = 0;
              i5 = k;
              for (;;)
              {
                i7 = j;
                i4 = i;
                k = i5;
                if (i6 >= 8) {
                  break;
                }
                i5 += (paramArrayOfByte[(i + i6)] & 0xFF);
                i6++;
              }
            }
            i4 = j;
            j = i;
          }
          else
          {
            i4 = i;
          }
          i = i4 + 1;
          j += paramInt3;
        }
        i = k >> 6;
        if (i2 - i3 <= 24)
        {
          j = i3 / 2;
          i = j;
          if (m > 0)
          {
            i = j;
            if (i1 > 0)
            {
              i2 = m - 1;
              i = arrayOfInt1[i2][i1];
              int[] arrayOfInt = arrayOfInt1[m];
              k = i1 - 1;
              k = (i + arrayOfInt[k] * 2 + arrayOfInt1[i2][k]) / 4;
              i = j;
              if (i3 < k) {
                i = k;
              }
            }
          }
        }
        arrayOfInt1[m][i1] = i;
      }
    }
    return arrayOfInt1;
  }
  
  private static void calculateThresholdForBlock(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[][] paramArrayOfInt, BitMatrix paramBitMatrix)
  {
    for (int i = 0; i < paramInt2; i++)
    {
      int m = i << 3;
      int k = paramInt4 - 8;
      int j = m;
      if (m > k) {
        j = k;
      }
      for (k = 0; k < paramInt1; k++)
      {
        m = k << 3;
        int n = paramInt3 - 8;
        if (m > n) {
          m = n;
        }
        int i3 = cap(k, 2, paramInt1 - 3);
        int i2 = cap(i, 2, paramInt2 - 3);
        n = -2;
        int i1 = 0;
        while (n <= 2)
        {
          int[] arrayOfInt = paramArrayOfInt[(i2 + n)];
          i1 += arrayOfInt[(i3 - 2)] + arrayOfInt[(i3 - 1)] + arrayOfInt[i3] + arrayOfInt[(i3 + 1)] + arrayOfInt[(i3 + 2)];
          n++;
        }
        thresholdBlock(paramArrayOfByte, m, j, i1 / 25, paramInt3, paramBitMatrix);
      }
    }
  }
  
  private static int cap(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt1 < paramInt2) {
      return paramInt2;
    }
    if (paramInt1 > paramInt3) {
      return paramInt3;
    }
    return paramInt1;
  }
  
  private static void thresholdBlock(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, BitMatrix paramBitMatrix)
  {
    int i = paramInt2 * paramInt4 + paramInt1;
    int j = 0;
    while (j < 8)
    {
      for (int k = 0; k < 8; k++) {
        if ((paramArrayOfByte[(i + k)] & 0xFF) <= paramInt3) {
          paramBitMatrix.set(paramInt1 + k, paramInt2 + j);
        }
      }
      j++;
      i += paramInt4;
    }
  }
  
  public Binarizer createBinarizer(LuminanceSource paramLuminanceSource)
  {
    return new HybridBinarizer(paramLuminanceSource);
  }
  
  public BitMatrix getBlackMatrix()
    throws NotFoundException
  {
    Object localObject = this.matrix;
    if (localObject != null) {
      return (BitMatrix)localObject;
    }
    localObject = getLuminanceSource();
    int k = ((LuminanceSource)localObject).getWidth();
    int m = ((LuminanceSource)localObject).getHeight();
    if ((k >= 40) && (m >= 40))
    {
      byte[] arrayOfByte = ((LuminanceSource)localObject).getMatrix();
      int i = k >> 3;
      if ((k & 0x7) != 0) {
        i++;
      }
      int j = m >> 3;
      if ((m & 0x7) != 0) {
        j++;
      }
      localObject = calculateBlackPoints(arrayOfByte, i, j, k, m);
      BitMatrix localBitMatrix = new BitMatrix(k, m);
      calculateThresholdForBlock(arrayOfByte, i, j, k, m, (int[][])localObject, localBitMatrix);
      this.matrix = localBitMatrix;
    }
    else
    {
      this.matrix = super.getBlackMatrix();
    }
    return this.matrix;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/HybridBinarizer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */