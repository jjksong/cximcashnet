package com.google.zxing.common;

import com.google.zxing.Binarizer;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;

public class GlobalHistogramBinarizer
  extends Binarizer
{
  private static final byte[] EMPTY = new byte[0];
  private static final int LUMINANCE_BITS = 5;
  private static final int LUMINANCE_BUCKETS = 32;
  private static final int LUMINANCE_SHIFT = 3;
  private final int[] buckets = new int[32];
  private byte[] luminances = EMPTY;
  
  public GlobalHistogramBinarizer(LuminanceSource paramLuminanceSource)
  {
    super(paramLuminanceSource);
  }
  
  private static int estimateBlackPoint(int[] paramArrayOfInt)
    throws NotFoundException
  {
    int i3 = paramArrayOfInt.length;
    int i2 = 0;
    int j = 0;
    int n = 0;
    int m = 0;
    int i = 0;
    int i1;
    while (j < i3)
    {
      k = n;
      if (paramArrayOfInt[j] > n)
      {
        k = paramArrayOfInt[j];
        i = j;
      }
      i1 = m;
      if (paramArrayOfInt[j] > m) {
        i1 = paramArrayOfInt[j];
      }
      j++;
      n = k;
      m = i1;
    }
    j = 0;
    n = 0;
    int k = i2;
    while (k < i3)
    {
      i1 = k - i;
      i2 = paramArrayOfInt[k] * i1 * i1;
      i1 = n;
      if (i2 > n)
      {
        j = k;
        i1 = i2;
      }
      k++;
      n = i1;
    }
    n = j;
    k = i;
    if (i > j)
    {
      k = j;
      n = i;
    }
    if (n - k > i3 / 16)
    {
      i = n - 1;
      i2 = i;
      for (j = -1; i > k; j = i1)
      {
        i1 = i - k;
        i3 = i1 * i1 * (n - i) * (m - paramArrayOfInt[i]);
        i1 = j;
        if (i3 > j)
        {
          i2 = i;
          i1 = i3;
        }
        i--;
      }
      return i2 << 3;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private void initArrays(int paramInt)
  {
    if (this.luminances.length < paramInt) {
      this.luminances = new byte[paramInt];
    }
    for (paramInt = 0; paramInt < 32; paramInt++) {
      this.buckets[paramInt] = 0;
    }
  }
  
  public Binarizer createBinarizer(LuminanceSource paramLuminanceSource)
  {
    return new GlobalHistogramBinarizer(paramLuminanceSource);
  }
  
  public BitMatrix getBlackMatrix()
    throws NotFoundException
  {
    LuminanceSource localLuminanceSource = getLuminanceSource();
    int k = localLuminanceSource.getWidth();
    int m = localLuminanceSource.getHeight();
    BitMatrix localBitMatrix = new BitMatrix(k, m);
    initArrays(k);
    int[] arrayOfInt = this.buckets;
    int j;
    for (int i = 1; i < 5; i++)
    {
      arrayOfByte = localLuminanceSource.getRow(m * i / 5, this.luminances);
      n = (k << 2) / 5;
      for (j = k / 5; j < n; j++)
      {
        int i1 = (arrayOfByte[j] & 0xFF) >> 3;
        arrayOfInt[i1] += 1;
      }
    }
    int n = estimateBlackPoint(arrayOfInt);
    byte[] arrayOfByte = localLuminanceSource.getMatrix();
    for (i = 0; i < m; i++) {
      for (j = 0; j < k; j++) {
        if ((arrayOfByte[(i * k + j)] & 0xFF) < n) {
          localBitMatrix.set(j, i);
        }
      }
    }
    return localBitMatrix;
  }
  
  public BitArray getBlackRow(int paramInt, BitArray paramBitArray)
    throws NotFoundException
  {
    Object localObject = getLuminanceSource();
    int n = ((LuminanceSource)localObject).getWidth();
    if ((paramBitArray != null) && (paramBitArray.getSize() >= n)) {
      paramBitArray.clear();
    } else {
      paramBitArray = new BitArray(n);
    }
    initArrays(n);
    byte[] arrayOfByte = ((LuminanceSource)localObject).getRow(paramInt, this.luminances);
    localObject = this.buckets;
    int i = 0;
    for (paramInt = 0; paramInt < n; paramInt++)
    {
      j = (arrayOfByte[paramInt] & 0xFF) >> 3;
      localObject[j] += 1;
    }
    int i1 = estimateBlackPoint((int[])localObject);
    if (n < 3) {
      for (paramInt = i; paramInt < n; paramInt++) {
        if ((arrayOfByte[paramInt] & 0xFF) < i1) {
          paramBitArray.set(paramInt);
        }
      }
    }
    i = arrayOfByte[0];
    paramInt = arrayOfByte[1] & 0xFF;
    i &= 0xFF;
    int j = 1;
    while (j < n - 1)
    {
      int m = j + 1;
      int k = arrayOfByte[m] & 0xFF;
      if (((paramInt << 2) - i - k) / 2 < i1) {
        paramBitArray.set(j);
      }
      i = paramInt;
      j = m;
      paramInt = k;
    }
    return paramBitArray;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/common/GlobalHistogramBinarizer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */