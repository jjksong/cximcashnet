package com.google.zxing;

public abstract interface ResultPointCallback
{
  public abstract void foundPossibleResultPoint(ResultPoint paramResultPoint);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/ResultPointCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */