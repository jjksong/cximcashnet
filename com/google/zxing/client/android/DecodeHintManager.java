package com.google.zxing.client.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.google.zxing.DecodeHintType;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public final class DecodeHintManager
{
  private static final Pattern COMMA = Pattern.compile(",");
  private static final String TAG = "DecodeHintManager";
  
  public static Map<DecodeHintType, Object> parseDecodeHints(Intent paramIntent)
  {
    Object localObject2 = paramIntent.getExtras();
    if ((localObject2 != null) && (!((Bundle)localObject2).isEmpty()))
    {
      paramIntent = new EnumMap(DecodeHintType.class);
      for (Object localObject3 : DecodeHintType.values()) {
        if ((localObject3 != DecodeHintType.CHARACTER_SET) && (localObject3 != DecodeHintType.NEED_RESULT_POINT_CALLBACK) && (localObject3 != DecodeHintType.POSSIBLE_FORMATS))
        {
          String str = ((DecodeHintType)localObject3).name();
          if (((Bundle)localObject2).containsKey(str)) {
            if (((DecodeHintType)localObject3).getValueType().equals(Void.class))
            {
              paramIntent.put(localObject3, Boolean.TRUE);
            }
            else
            {
              Object localObject4 = ((Bundle)localObject2).get(str);
              if (((DecodeHintType)localObject3).getValueType().isInstance(localObject4))
              {
                paramIntent.put(localObject3, localObject4);
              }
              else
              {
                str = TAG;
                StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append("Ignoring hint ");
                localStringBuilder.append(localObject3);
                localStringBuilder.append(" because it is not assignable from ");
                localStringBuilder.append(localObject4);
                Log.w(str, localStringBuilder.toString());
              }
            }
          }
        }
      }
      localObject2 = TAG;
      ??? = new StringBuilder();
      ((StringBuilder)???).append("Hints from the Intent: ");
      ((StringBuilder)???).append(paramIntent);
      Log.i((String)localObject2, ((StringBuilder)???).toString());
      return paramIntent;
    }
    return null;
  }
  
  static Map<DecodeHintType, ?> parseDecodeHints(Uri paramUri)
  {
    paramUri = paramUri.getEncodedQuery();
    if ((paramUri != null) && (!paramUri.isEmpty()))
    {
      Map localMap = splitQuery(paramUri);
      EnumMap localEnumMap = new EnumMap(DecodeHintType.class);
      for (DecodeHintType localDecodeHintType : DecodeHintType.values()) {
        if ((localDecodeHintType != DecodeHintType.CHARACTER_SET) && (localDecodeHintType != DecodeHintType.NEED_RESULT_POINT_CALLBACK) && (localDecodeHintType != DecodeHintType.POSSIBLE_FORMATS))
        {
          localObject = (String)localMap.get(localDecodeHintType.name());
          if (localObject != null) {
            if (localDecodeHintType.getValueType().equals(Object.class))
            {
              localEnumMap.put(localDecodeHintType, localObject);
            }
            else if (localDecodeHintType.getValueType().equals(Void.class))
            {
              localEnumMap.put(localDecodeHintType, Boolean.TRUE);
            }
            else if (localDecodeHintType.getValueType().equals(String.class))
            {
              localEnumMap.put(localDecodeHintType, localObject);
            }
            else if (localDecodeHintType.getValueType().equals(Boolean.class))
            {
              if (((String)localObject).isEmpty()) {
                localEnumMap.put(localDecodeHintType, Boolean.TRUE);
              } else if ((!"0".equals(localObject)) && (!"false".equalsIgnoreCase((String)localObject)) && (!"no".equalsIgnoreCase((String)localObject))) {
                localEnumMap.put(localDecodeHintType, Boolean.TRUE);
              } else {
                localEnumMap.put(localDecodeHintType, Boolean.FALSE);
              }
            }
            else if (localDecodeHintType.getValueType().equals(int[].class))
            {
              paramUri = (Uri)localObject;
              if (!((String)localObject).isEmpty())
              {
                paramUri = (Uri)localObject;
                if (((String)localObject).charAt(((String)localObject).length() - 1) == ',') {
                  paramUri = ((String)localObject).substring(0, ((String)localObject).length() - 1);
                }
              }
              String[] arrayOfString = COMMA.split(paramUri);
              localObject = new int[arrayOfString.length];
              int j = 0;
              for (;;)
              {
                paramUri = (Uri)localObject;
                if (j < arrayOfString.length) {
                  try
                  {
                    localObject[j] = Integer.parseInt(arrayOfString[j]);
                    j++;
                  }
                  catch (NumberFormatException paramUri)
                  {
                    localObject = TAG;
                    paramUri = new StringBuilder();
                    paramUri.append("Skipping array of integers hint ");
                    paramUri.append(localDecodeHintType);
                    paramUri.append(" due to invalid numeric value: '");
                    paramUri.append(arrayOfString[j]);
                    paramUri.append('\'');
                    Log.w((String)localObject, paramUri.toString());
                    paramUri = null;
                  }
                }
              }
              if (paramUri != null) {
                localEnumMap.put(localDecodeHintType, paramUri);
              }
            }
            else
            {
              localObject = TAG;
              paramUri = new StringBuilder();
              paramUri.append("Unsupported hint type '");
              paramUri.append(localDecodeHintType);
              paramUri.append("' of type ");
              paramUri.append(localDecodeHintType.getValueType());
              Log.w((String)localObject, paramUri.toString());
            }
          }
        }
      }
      Object localObject = TAG;
      paramUri = new StringBuilder();
      paramUri.append("Hints from the URI: ");
      paramUri.append(localEnumMap);
      Log.i((String)localObject, paramUri.toString());
      return localEnumMap;
    }
    return null;
  }
  
  private static Map<String, String> splitQuery(String paramString)
  {
    HashMap localHashMap = new HashMap();
    int i = 0;
    while (i < paramString.length()) {
      if (paramString.charAt(i) == '&')
      {
        i++;
      }
      else
      {
        int k = paramString.indexOf('&', i);
        int j = paramString.indexOf('=', i);
        String str1;
        if (k < 0)
        {
          if (j < 0)
          {
            str1 = Uri.decode(paramString.substring(i).replace('+', ' '));
            paramString = "";
          }
          else
          {
            str1 = Uri.decode(paramString.substring(i, j).replace('+', ' '));
            paramString = Uri.decode(paramString.substring(j + 1).replace('+', ' '));
          }
          if (localHashMap.containsKey(str1)) {
            break;
          }
          localHashMap.put(str1, paramString);
          break;
        }
        if ((j >= 0) && (j <= k))
        {
          String str2 = Uri.decode(paramString.substring(i, j).replace('+', ' '));
          str1 = Uri.decode(paramString.substring(j + 1, k).replace('+', ' '));
          if (!localHashMap.containsKey(str2)) {
            localHashMap.put(str2, str1);
          }
          i = k + 1;
        }
        else
        {
          str1 = Uri.decode(paramString.substring(i, k).replace('+', ' '));
          if (!localHashMap.containsKey(str1)) {
            localHashMap.put(str1, "");
          }
          i = k + 1;
        }
      }
    }
    return localHashMap;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/DecodeHintManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */