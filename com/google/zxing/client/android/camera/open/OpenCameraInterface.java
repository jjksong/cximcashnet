package com.google.zxing.client.android.camera.open;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.util.Log;

public final class OpenCameraInterface
{
  public static final int NO_REQUESTED_CAMERA = -1;
  private static final String TAG = "com.google.zxing.client.android.camera.open.OpenCameraInterface";
  
  public static int getCameraId(int paramInt)
  {
    int k = Camera.getNumberOfCameras();
    if (k == 0)
    {
      Log.w(TAG, "No cameras!");
      return -1;
    }
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    int j = paramInt;
    if (i == 0) {
      for (paramInt = 0;; paramInt++)
      {
        j = paramInt;
        if (paramInt >= k) {
          break;
        }
        Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(paramInt, localCameraInfo);
        if (localCameraInfo.facing == 0)
        {
          j = paramInt;
          break;
        }
      }
    }
    if (j < k) {
      return j;
    }
    if (i != 0) {
      return -1;
    }
    return 0;
  }
  
  public static Camera open(int paramInt)
  {
    paramInt = getCameraId(paramInt);
    if (paramInt == -1) {
      return null;
    }
    return Camera.open(paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/camera/open/OpenCameraInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */