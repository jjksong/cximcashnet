package com.google.zxing.client.android.camera;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.hardware.Camera.Area;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import com.journeyapps.barcodescanner.camera.CameraSettings.FocusMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public final class CameraConfigurationUtils
{
  private static final int AREA_PER_1000 = 400;
  private static final float MAX_EXPOSURE_COMPENSATION = 1.5F;
  private static final int MAX_FPS = 20;
  private static final float MIN_EXPOSURE_COMPENSATION = 0.0F;
  private static final int MIN_FPS = 10;
  private static final Pattern SEMICOLON = Pattern.compile(";");
  private static final String TAG = "CameraConfiguration";
  
  @TargetApi(15)
  private static List<Camera.Area> buildMiddleArea(int paramInt)
  {
    int i = -paramInt;
    return Collections.singletonList(new Camera.Area(new Rect(i, i, paramInt, paramInt), 1));
  }
  
  public static String collectStats(Camera.Parameters paramParameters)
  {
    return collectStats(paramParameters.flatten());
  }
  
  public static String collectStats(CharSequence paramCharSequence)
  {
    StringBuilder localStringBuilder = new StringBuilder(1000);
    localStringBuilder.append("BOARD=");
    localStringBuilder.append(Build.BOARD);
    localStringBuilder.append('\n');
    localStringBuilder.append("BRAND=");
    localStringBuilder.append(Build.BRAND);
    localStringBuilder.append('\n');
    localStringBuilder.append("CPU_ABI=");
    localStringBuilder.append(Build.CPU_ABI);
    localStringBuilder.append('\n');
    localStringBuilder.append("DEVICE=");
    localStringBuilder.append(Build.DEVICE);
    localStringBuilder.append('\n');
    localStringBuilder.append("DISPLAY=");
    localStringBuilder.append(Build.DISPLAY);
    localStringBuilder.append('\n');
    localStringBuilder.append("FINGERPRINT=");
    localStringBuilder.append(Build.FINGERPRINT);
    localStringBuilder.append('\n');
    localStringBuilder.append("HOST=");
    localStringBuilder.append(Build.HOST);
    localStringBuilder.append('\n');
    localStringBuilder.append("ID=");
    localStringBuilder.append(Build.ID);
    localStringBuilder.append('\n');
    localStringBuilder.append("MANUFACTURER=");
    localStringBuilder.append(Build.MANUFACTURER);
    localStringBuilder.append('\n');
    localStringBuilder.append("MODEL=");
    localStringBuilder.append(Build.MODEL);
    localStringBuilder.append('\n');
    localStringBuilder.append("PRODUCT=");
    localStringBuilder.append(Build.PRODUCT);
    localStringBuilder.append('\n');
    localStringBuilder.append("TAGS=");
    localStringBuilder.append(Build.TAGS);
    localStringBuilder.append('\n');
    localStringBuilder.append("TIME=");
    localStringBuilder.append(Build.TIME);
    localStringBuilder.append('\n');
    localStringBuilder.append("TYPE=");
    localStringBuilder.append(Build.TYPE);
    localStringBuilder.append('\n');
    localStringBuilder.append("USER=");
    localStringBuilder.append(Build.USER);
    localStringBuilder.append('\n');
    localStringBuilder.append("VERSION.CODENAME=");
    localStringBuilder.append(Build.VERSION.CODENAME);
    localStringBuilder.append('\n');
    localStringBuilder.append("VERSION.INCREMENTAL=");
    localStringBuilder.append(Build.VERSION.INCREMENTAL);
    localStringBuilder.append('\n');
    localStringBuilder.append("VERSION.RELEASE=");
    localStringBuilder.append(Build.VERSION.RELEASE);
    localStringBuilder.append('\n');
    localStringBuilder.append("VERSION.SDK_INT=");
    localStringBuilder.append(Build.VERSION.SDK_INT);
    localStringBuilder.append('\n');
    if (paramCharSequence != null)
    {
      paramCharSequence = SEMICOLON.split(paramCharSequence);
      Arrays.sort(paramCharSequence);
      int j = paramCharSequence.length;
      for (int i = 0; i < j; i++)
      {
        localStringBuilder.append(paramCharSequence[i]);
        localStringBuilder.append('\n');
      }
    }
    return localStringBuilder.toString();
  }
  
  private static String findSettableValue(String paramString, Collection<String> paramCollection, String... paramVarArgs)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Requesting ");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(" value from among: ");
    ((StringBuilder)localObject).append(Arrays.toString(paramVarArgs));
    Log.i("CameraConfiguration", ((StringBuilder)localObject).toString());
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Supported ");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(" values: ");
    ((StringBuilder)localObject).append(paramCollection);
    Log.i("CameraConfiguration", ((StringBuilder)localObject).toString());
    if (paramCollection != null)
    {
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++)
      {
        localObject = paramVarArgs[i];
        if (paramCollection.contains(localObject))
        {
          paramCollection = new StringBuilder();
          paramCollection.append("Can set ");
          paramCollection.append(paramString);
          paramCollection.append(" to: ");
          paramCollection.append((String)localObject);
          Log.i("CameraConfiguration", paramCollection.toString());
          return (String)localObject;
        }
      }
    }
    Log.i("CameraConfiguration", "No supported values match");
    return null;
  }
  
  private static Integer indexOfClosestZoom(Camera.Parameters paramParameters, double paramDouble)
  {
    List localList = paramParameters.getZoomRatios();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Zoom ratios: ");
    localStringBuilder.append(localList);
    Log.i("CameraConfiguration", localStringBuilder.toString());
    int i = paramParameters.getMaxZoom();
    if ((localList != null) && (!localList.isEmpty()) && (localList.size() == i + 1))
    {
      i = 0;
      double d1 = Double.POSITIVE_INFINITY;
      int j = 0;
      while (i < localList.size())
      {
        double d2 = ((Integer)localList.get(i)).intValue();
        Double.isNaN(d2);
        double d3 = Math.abs(d2 - paramDouble * 100.0D);
        d2 = d1;
        if (d3 < d1)
        {
          j = i;
          d2 = d3;
        }
        i++;
        d1 = d2;
      }
      paramParameters = new StringBuilder();
      paramParameters.append("Chose zoom ratio of ");
      paramDouble = ((Integer)localList.get(j)).intValue();
      Double.isNaN(paramDouble);
      paramParameters.append(paramDouble / 100.0D);
      Log.i("CameraConfiguration", paramParameters.toString());
      return Integer.valueOf(j);
    }
    Log.w("CameraConfiguration", "Invalid zoom ratios!");
    return null;
  }
  
  public static void setBarcodeSceneMode(Camera.Parameters paramParameters)
  {
    if ("barcode".equals(paramParameters.getSceneMode()))
    {
      Log.i("CameraConfiguration", "Barcode scene mode already set");
      return;
    }
    String str = findSettableValue("scene mode", paramParameters.getSupportedSceneModes(), new String[] { "barcode" });
    if (str != null) {
      paramParameters.setSceneMode(str);
    }
  }
  
  public static void setBestExposure(Camera.Parameters paramParameters, boolean paramBoolean)
  {
    int j = paramParameters.getMinExposureCompensation();
    int i = paramParameters.getMaxExposureCompensation();
    float f2 = paramParameters.getExposureCompensationStep();
    if ((j != 0) || (i != 0))
    {
      float f1 = 0.0F;
      if (f2 > 0.0F)
      {
        if (!paramBoolean) {
          f1 = 1.5F;
        }
        int k = Math.round(f1 / f2);
        f1 = f2 * k;
        i = Math.max(Math.min(k, i), j);
        if (paramParameters.getExposureCompensation() == i)
        {
          paramParameters = new StringBuilder();
          paramParameters.append("Exposure compensation already set to ");
          paramParameters.append(i);
          paramParameters.append(" / ");
          paramParameters.append(f1);
          Log.i("CameraConfiguration", paramParameters.toString());
          return;
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Setting exposure compensation to ");
        localStringBuilder.append(i);
        localStringBuilder.append(" / ");
        localStringBuilder.append(f1);
        Log.i("CameraConfiguration", localStringBuilder.toString());
        paramParameters.setExposureCompensation(i);
        return;
      }
    }
    Log.i("CameraConfiguration", "Camera does not support exposure compensation");
  }
  
  public static void setBestPreviewFPS(Camera.Parameters paramParameters)
  {
    setBestPreviewFPS(paramParameters, 10, 20);
  }
  
  public static void setBestPreviewFPS(Camera.Parameters paramParameters, int paramInt1, int paramInt2)
  {
    Object localObject1 = paramParameters.getSupportedPreviewFpsRange();
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("Supported FPS ranges: ");
    ((StringBuilder)localObject2).append(toString((Collection)localObject1));
    Log.i("CameraConfiguration", ((StringBuilder)localObject2).toString());
    if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
    {
      localObject2 = null;
      Iterator localIterator = ((List)localObject1).iterator();
      int j;
      int i;
      do
      {
        localObject1 = localObject2;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject1 = (int[])localIterator.next();
        j = localObject1[0];
        i = localObject1[1];
      } while ((j < paramInt1 * 1000) || (i > paramInt2 * 1000));
      if (localObject1 == null)
      {
        Log.i("CameraConfiguration", "No suitable FPS range?");
      }
      else
      {
        localObject2 = new int[2];
        paramParameters.getPreviewFpsRange((int[])localObject2);
        if (Arrays.equals((int[])localObject2, (int[])localObject1))
        {
          paramParameters = new StringBuilder();
          paramParameters.append("FPS range already set to ");
          paramParameters.append(Arrays.toString((int[])localObject1));
          Log.i("CameraConfiguration", paramParameters.toString());
        }
        else
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append("Setting FPS range to ");
          ((StringBuilder)localObject2).append(Arrays.toString((int[])localObject1));
          Log.i("CameraConfiguration", ((StringBuilder)localObject2).toString());
          paramParameters.setPreviewFpsRange(localObject1[0], localObject1[1]);
        }
      }
    }
  }
  
  public static void setFocus(Camera.Parameters paramParameters, CameraSettings.FocusMode paramFocusMode, boolean paramBoolean)
  {
    List localList = paramParameters.getSupportedFocusModes();
    if ((!paramBoolean) && (paramFocusMode != CameraSettings.FocusMode.AUTO))
    {
      if (paramFocusMode == CameraSettings.FocusMode.CONTINUOUS) {
        paramFocusMode = findSettableValue("focus mode", localList, new String[] { "continuous-picture", "continuous-video", "auto" });
      } else if (paramFocusMode == CameraSettings.FocusMode.INFINITY) {
        paramFocusMode = findSettableValue("focus mode", localList, new String[] { "infinity" });
      } else if (paramFocusMode == CameraSettings.FocusMode.MACRO) {
        paramFocusMode = findSettableValue("focus mode", localList, new String[] { "macro" });
      } else {
        paramFocusMode = null;
      }
    }
    else {
      paramFocusMode = findSettableValue("focus mode", localList, new String[] { "auto" });
    }
    Object localObject = paramFocusMode;
    if (!paramBoolean)
    {
      localObject = paramFocusMode;
      if (paramFocusMode == null) {
        localObject = findSettableValue("focus mode", localList, new String[] { "macro", "edof" });
      }
    }
    if (localObject != null) {
      if (((String)localObject).equals(paramParameters.getFocusMode()))
      {
        paramParameters = new StringBuilder();
        paramParameters.append("Focus mode already set to ");
        paramParameters.append((String)localObject);
        Log.i("CameraConfiguration", paramParameters.toString());
      }
      else
      {
        paramParameters.setFocusMode((String)localObject);
      }
    }
  }
  
  @TargetApi(15)
  public static void setFocusArea(Camera.Parameters paramParameters)
  {
    if (paramParameters.getMaxNumFocusAreas() > 0)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Old focus areas: ");
      localStringBuilder.append(toString(paramParameters.getFocusAreas()));
      Log.i("CameraConfiguration", localStringBuilder.toString());
      List localList = buildMiddleArea(400);
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("Setting focus area to : ");
      localStringBuilder.append(toString(localList));
      Log.i("CameraConfiguration", localStringBuilder.toString());
      paramParameters.setFocusAreas(localList);
    }
    else
    {
      Log.i("CameraConfiguration", "Device does not support focus areas");
    }
  }
  
  public static void setInvertColor(Camera.Parameters paramParameters)
  {
    if ("negative".equals(paramParameters.getColorEffect()))
    {
      Log.i("CameraConfiguration", "Negative effect already set");
      return;
    }
    String str = findSettableValue("color effect", paramParameters.getSupportedColorEffects(), new String[] { "negative" });
    if (str != null) {
      paramParameters.setColorEffect(str);
    }
  }
  
  @TargetApi(15)
  public static void setMetering(Camera.Parameters paramParameters)
  {
    if (paramParameters.getMaxNumMeteringAreas() > 0)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Old metering areas: ");
      localStringBuilder.append(paramParameters.getMeteringAreas());
      Log.i("CameraConfiguration", localStringBuilder.toString());
      List localList = buildMiddleArea(400);
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("Setting metering area to : ");
      localStringBuilder.append(toString(localList));
      Log.i("CameraConfiguration", localStringBuilder.toString());
      paramParameters.setMeteringAreas(localList);
    }
    else
    {
      Log.i("CameraConfiguration", "Device does not support metering areas");
    }
  }
  
  public static void setTorch(Camera.Parameters paramParameters, boolean paramBoolean)
  {
    Object localObject = paramParameters.getSupportedFlashModes();
    if (paramBoolean) {
      localObject = findSettableValue("flash mode", (Collection)localObject, new String[] { "torch", "on" });
    } else {
      localObject = findSettableValue("flash mode", (Collection)localObject, new String[] { "off" });
    }
    if (localObject != null) {
      if (((String)localObject).equals(paramParameters.getFlashMode()))
      {
        paramParameters = new StringBuilder();
        paramParameters.append("Flash mode already set to ");
        paramParameters.append((String)localObject);
        Log.i("CameraConfiguration", paramParameters.toString());
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Setting flash mode to ");
        localStringBuilder.append((String)localObject);
        Log.i("CameraConfiguration", localStringBuilder.toString());
        paramParameters.setFlashMode((String)localObject);
      }
    }
  }
  
  @TargetApi(15)
  public static void setVideoStabilization(Camera.Parameters paramParameters)
  {
    if (paramParameters.isVideoStabilizationSupported())
    {
      if (paramParameters.getVideoStabilization())
      {
        Log.i("CameraConfiguration", "Video stabilization already enabled");
      }
      else
      {
        Log.i("CameraConfiguration", "Enabling video stabilization...");
        paramParameters.setVideoStabilization(true);
      }
    }
    else {
      Log.i("CameraConfiguration", "This device does not support video stabilization");
    }
  }
  
  public static void setZoom(Camera.Parameters paramParameters, double paramDouble)
  {
    if (paramParameters.isZoomSupported())
    {
      Integer localInteger = indexOfClosestZoom(paramParameters, paramDouble);
      if (localInteger == null) {
        return;
      }
      if (paramParameters.getZoom() == localInteger.intValue())
      {
        paramParameters = new StringBuilder();
        paramParameters.append("Zoom is already set to ");
        paramParameters.append(localInteger);
        Log.i("CameraConfiguration", paramParameters.toString());
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Setting zoom to ");
        localStringBuilder.append(localInteger);
        Log.i("CameraConfiguration", localStringBuilder.toString());
        paramParameters.setZoom(localInteger.intValue());
      }
    }
    else
    {
      Log.i("CameraConfiguration", "Zoom is not supported");
    }
  }
  
  @TargetApi(15)
  private static String toString(Iterable<Camera.Area> paramIterable)
  {
    if (paramIterable == null) {
      return null;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramIterable.iterator();
    while (localIterator.hasNext())
    {
      paramIterable = (Camera.Area)localIterator.next();
      localStringBuilder.append(paramIterable.rect);
      localStringBuilder.append(':');
      localStringBuilder.append(paramIterable.weight);
      localStringBuilder.append(' ');
    }
    return localStringBuilder.toString();
  }
  
  private static String toString(Collection<int[]> paramCollection)
  {
    if ((paramCollection != null) && (!paramCollection.isEmpty()))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append('[');
      paramCollection = paramCollection.iterator();
      while (paramCollection.hasNext())
      {
        localStringBuilder.append(Arrays.toString((int[])paramCollection.next()));
        if (paramCollection.hasNext()) {
          localStringBuilder.append(", ");
        }
      }
      localStringBuilder.append(']');
      return localStringBuilder.toString();
    }
    return "[]";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/camera/CameraConfigurationUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */