package com.google.zxing.client.android;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import com.journeyapps.barcodescanner.camera.CameraManager;
import com.journeyapps.barcodescanner.camera.CameraSettings;

public final class AmbientLightManager
  implements SensorEventListener
{
  private static final float BRIGHT_ENOUGH_LUX = 450.0F;
  private static final float TOO_DARK_LUX = 45.0F;
  private CameraManager cameraManager;
  private CameraSettings cameraSettings;
  private Context context;
  private Handler handler;
  private Sensor lightSensor;
  
  public AmbientLightManager(Context paramContext, CameraManager paramCameraManager, CameraSettings paramCameraSettings)
  {
    this.context = paramContext;
    this.cameraManager = paramCameraManager;
    this.cameraSettings = paramCameraSettings;
    this.handler = new Handler();
  }
  
  private void setTorch(final boolean paramBoolean)
  {
    this.handler.post(new Runnable()
    {
      public void run()
      {
        AmbientLightManager.this.cameraManager.setTorch(paramBoolean);
      }
    });
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    float f = paramSensorEvent.values[0];
    if (this.cameraManager != null) {
      if (f <= 45.0F) {
        setTorch(true);
      } else if (f >= 450.0F) {
        setTorch(false);
      }
    }
  }
  
  public void start()
  {
    if (this.cameraSettings.isAutoTorchEnabled())
    {
      SensorManager localSensorManager = (SensorManager)this.context.getSystemService("sensor");
      this.lightSensor = localSensorManager.getDefaultSensor(5);
      Sensor localSensor = this.lightSensor;
      if (localSensor != null) {
        localSensorManager.registerListener(this, localSensor, 3);
      }
    }
  }
  
  public void stop()
  {
    if (this.lightSensor != null)
    {
      ((SensorManager)this.context.getSystemService("sensor")).unregisterListener(this);
      this.lightSensor = null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/AmbientLightManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */