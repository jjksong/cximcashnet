package com.google.zxing.client.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Vibrator;
import android.util.Log;
import java.io.IOException;

public final class BeepManager
{
  private static final float BEEP_VOLUME = 0.1F;
  private static final String TAG = "BeepManager";
  private static final long VIBRATE_DURATION = 200L;
  private boolean beepEnabled = true;
  private final Context context;
  private boolean vibrateEnabled = false;
  
  public BeepManager(Activity paramActivity)
  {
    paramActivity.setVolumeControlStream(3);
    this.context = paramActivity.getApplicationContext();
  }
  
  public boolean isBeepEnabled()
  {
    return this.beepEnabled;
  }
  
  public boolean isVibrateEnabled()
  {
    return this.vibrateEnabled;
  }
  
  public MediaPlayer playBeepSound()
  {
    MediaPlayer localMediaPlayer = new MediaPlayer();
    localMediaPlayer.setAudioStreamType(3);
    localMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
    {
      public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
      {
        paramAnonymousMediaPlayer.stop();
        paramAnonymousMediaPlayer.release();
      }
    });
    localMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener()
    {
      public boolean onError(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        String str = BeepManager.TAG;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Failed to beep ");
        localStringBuilder.append(paramAnonymousInt1);
        localStringBuilder.append(", ");
        localStringBuilder.append(paramAnonymousInt2);
        Log.w(str, localStringBuilder.toString());
        paramAnonymousMediaPlayer.stop();
        paramAnonymousMediaPlayer.release();
        return true;
      }
    });
    try
    {
      AssetFileDescriptor localAssetFileDescriptor = this.context.getResources().openRawResourceFd(R.raw.zxing_beep);
      try
      {
        localMediaPlayer.setDataSource(localAssetFileDescriptor.getFileDescriptor(), localAssetFileDescriptor.getStartOffset(), localAssetFileDescriptor.getLength());
        localAssetFileDescriptor.close();
        localMediaPlayer.setVolume(0.1F, 0.1F);
        localMediaPlayer.prepare();
        return localMediaPlayer;
      }
      finally
      {
        localAssetFileDescriptor.close();
      }
      return null;
    }
    catch (IOException localIOException)
    {
      Log.w(TAG, localIOException);
      localMediaPlayer.release();
    }
  }
  
  @SuppressLint({"MissingPermission"})
  public void playBeepSoundAndVibrate()
  {
    try
    {
      if (this.beepEnabled) {
        playBeepSound();
      }
      if (this.vibrateEnabled)
      {
        Vibrator localVibrator = (Vibrator)this.context.getSystemService("vibrator");
        if (localVibrator != null) {
          localVibrator.vibrate(200L);
        }
      }
      return;
    }
    finally {}
  }
  
  public void setBeepEnabled(boolean paramBoolean)
  {
    this.beepEnabled = paramBoolean;
  }
  
  public void setVibrateEnabled(boolean paramBoolean)
  {
    this.vibrateEnabled = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/BeepManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */