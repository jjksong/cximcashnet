package com.google.zxing.client.android;

public final class R
{
  public static final class attr
  {
    public static final int zxing_framing_rect_height = 2130837988;
    public static final int zxing_framing_rect_width = 2130837989;
    public static final int zxing_possible_result_points = 2130837990;
    public static final int zxing_preview_scaling_strategy = 2130837991;
    public static final int zxing_result_view = 2130837992;
    public static final int zxing_scanner_layout = 2130837993;
    public static final int zxing_use_texture_view = 2130837994;
    public static final int zxing_viewfinder_laser = 2130837995;
    public static final int zxing_viewfinder_mask = 2130837996;
  }
  
  public static final class color
  {
    public static final int zxing_custom_possible_result_points = 2130968797;
    public static final int zxing_custom_result_view = 2130968798;
    public static final int zxing_custom_viewfinder_laser = 2130968799;
    public static final int zxing_custom_viewfinder_mask = 2130968800;
    public static final int zxing_possible_result_points = 2130968801;
    public static final int zxing_result_view = 2130968802;
    public static final int zxing_status_text = 2130968803;
    public static final int zxing_transparent = 2130968804;
    public static final int zxing_viewfinder_laser = 2130968805;
    public static final int zxing_viewfinder_mask = 2130968806;
  }
  
  public static final class id
  {
    public static final int centerCrop = 2131165322;
    public static final int fitCenter = 2131165436;
    public static final int fitXY = 2131165437;
    public static final int zxing_back_button = 2131165916;
    public static final int zxing_barcode_scanner = 2131165917;
    public static final int zxing_barcode_surface = 2131165918;
    public static final int zxing_camera_closed = 2131165919;
    public static final int zxing_camera_error = 2131165920;
    public static final int zxing_decode = 2131165921;
    public static final int zxing_decode_failed = 2131165922;
    public static final int zxing_decode_succeeded = 2131165923;
    public static final int zxing_possible_result_points = 2131165924;
    public static final int zxing_preview_failed = 2131165925;
    public static final int zxing_prewiew_size_ready = 2131165926;
    public static final int zxing_status_view = 2131165927;
    public static final int zxing_viewfinder_view = 2131165928;
  }
  
  public static final class layout
  {
    public static final int zxing_barcode_scanner = 2131296493;
    public static final int zxing_capture = 2131296494;
  }
  
  public static final class raw
  {
    public static final int zxing_beep = 2131492873;
  }
  
  public static final class string
  {
    public static final int zxing_app_name = 2131558560;
    public static final int zxing_button_ok = 2131558561;
    public static final int zxing_msg_camera_framework_bug = 2131558562;
    public static final int zxing_msg_default_status = 2131558563;
  }
  
  public static final class style
  {
    public static final int zxing_CaptureTheme = 2131624320;
  }
  
  public static final class styleable
  {
    public static final int[] zxing_camera_preview = { 2130837988, 2130837989, 2130837991, 2130837994 };
    public static final int zxing_camera_preview_zxing_framing_rect_height = 0;
    public static final int zxing_camera_preview_zxing_framing_rect_width = 1;
    public static final int zxing_camera_preview_zxing_preview_scaling_strategy = 2;
    public static final int zxing_camera_preview_zxing_use_texture_view = 3;
    public static final int[] zxing_finder = { 2130837990, 2130837992, 2130837995, 2130837996 };
    public static final int zxing_finder_zxing_possible_result_points = 0;
    public static final int zxing_finder_zxing_result_view = 1;
    public static final int zxing_finder_zxing_viewfinder_laser = 2;
    public static final int zxing_finder_zxing_viewfinder_mask = 3;
    public static final int[] zxing_view = { 2130837993 };
    public static final int zxing_view_zxing_scanner_layout = 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */