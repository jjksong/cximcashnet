package com.google.zxing.client.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

public final class InactivityTimer
{
  private static final long INACTIVITY_DELAY_MS = 300000L;
  private static final String TAG = "InactivityTimer";
  private Runnable callback;
  private final Context context;
  private Handler handler;
  private boolean onBattery;
  private final BroadcastReceiver powerStatusReceiver;
  private boolean registered = false;
  
  public InactivityTimer(Context paramContext, Runnable paramRunnable)
  {
    this.context = paramContext;
    this.callback = paramRunnable;
    this.powerStatusReceiver = new PowerStatusReceiver(null);
    this.handler = new Handler();
  }
  
  private void cancelCallback()
  {
    this.handler.removeCallbacksAndMessages(null);
  }
  
  private void onBattery(boolean paramBoolean)
  {
    this.onBattery = paramBoolean;
    if (this.registered) {
      activity();
    }
  }
  
  private void registerReceiver()
  {
    if (!this.registered)
    {
      this.context.registerReceiver(this.powerStatusReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
      this.registered = true;
    }
  }
  
  private void unregisterReceiver()
  {
    if (this.registered)
    {
      this.context.unregisterReceiver(this.powerStatusReceiver);
      this.registered = false;
    }
  }
  
  public void activity()
  {
    cancelCallback();
    if (this.onBattery) {
      this.handler.postDelayed(this.callback, 300000L);
    }
  }
  
  public void cancel()
  {
    cancelCallback();
    unregisterReceiver();
  }
  
  public void start()
  {
    registerReceiver();
    activity();
  }
  
  private final class PowerStatusReceiver
    extends BroadcastReceiver
  {
    private PowerStatusReceiver() {}
    
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if ("android.intent.action.BATTERY_CHANGED".equals(paramIntent.getAction()))
      {
        final boolean bool;
        if (paramIntent.getIntExtra("plugged", -1) <= 0) {
          bool = true;
        } else {
          bool = false;
        }
        InactivityTimer.this.handler.post(new Runnable()
        {
          public void run()
          {
            InactivityTimer.this.onBattery(bool);
          }
        });
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/android/InactivityTimer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */