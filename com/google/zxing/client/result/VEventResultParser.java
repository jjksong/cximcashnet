package com.google.zxing.client.result;

import com.google.zxing.Result;
import java.util.List;

public final class VEventResultParser
  extends ResultParser
{
  private static String matchSingleVCardPrefixedField(CharSequence paramCharSequence, String paramString, boolean paramBoolean)
  {
    paramCharSequence = VCardResultParser.matchSingleVCardPrefixedField(paramCharSequence, paramString, paramBoolean, false);
    if ((paramCharSequence != null) && (!paramCharSequence.isEmpty())) {
      return (String)paramCharSequence.get(0);
    }
    return null;
  }
  
  private static String[] matchVCardPrefixedField(CharSequence paramCharSequence, String paramString, boolean paramBoolean)
  {
    paramString = VCardResultParser.matchVCardPrefixedField(paramCharSequence, paramString, paramBoolean, false);
    if ((paramString != null) && (!paramString.isEmpty()))
    {
      int j = paramString.size();
      paramCharSequence = new String[j];
      for (int i = 0; i < j; i++) {
        paramCharSequence[i] = ((String)((List)paramString.get(i)).get(0));
      }
      return paramCharSequence;
    }
    return null;
  }
  
  private static String stripMailto(String paramString)
  {
    String str = paramString;
    if (paramString != null) {
      if (!paramString.startsWith("mailto:"))
      {
        str = paramString;
        if (!paramString.startsWith("MAILTO:")) {}
      }
      else
      {
        str = paramString.substring(7);
      }
    }
    return str;
  }
  
  public CalendarParsedResult parse(Result paramResult)
  {
    String str7 = getMassagedText(paramResult);
    if (str7.indexOf("BEGIN:VEVENT") < 0) {
      return null;
    }
    String str3 = matchSingleVCardPrefixedField("SUMMARY", str7, true);
    String str4 = matchSingleVCardPrefixedField("DTSTART", str7, true);
    if (str4 == null) {
      return null;
    }
    String str5 = matchSingleVCardPrefixedField("DTEND", str7, true);
    String str6 = matchSingleVCardPrefixedField("DURATION", str7, true);
    String str1 = matchSingleVCardPrefixedField("LOCATION", str7, true);
    paramResult = stripMailto(matchSingleVCardPrefixedField("ORGANIZER", str7, true));
    String[] arrayOfString = matchVCardPrefixedField("ATTENDEE", str7, true);
    int i;
    if (arrayOfString != null) {
      for (i = 0; i < arrayOfString.length; i++) {
        arrayOfString[i] = stripMailto(arrayOfString[i]);
      }
    }
    String str2 = matchSingleVCardPrefixedField("DESCRIPTION", str7, true);
    str7 = matchSingleVCardPrefixedField("GEO", str7, true);
    double d1 = NaN.0D;
    double d2;
    if (str7 == null)
    {
      d2 = NaN.0D;
    }
    else
    {
      i = str7.indexOf(';');
      if (i < 0) {
        return null;
      }
    }
    try
    {
      d1 = Double.parseDouble(str7.substring(0, i));
      d2 = Double.parseDouble(str7.substring(i + 1));
      try
      {
        paramResult = new CalendarParsedResult(str3, str4, str5, str6, str1, paramResult, arrayOfString, str2, d1, d2);
        return paramResult;
      }
      catch (IllegalArgumentException paramResult)
      {
        return null;
      }
      return null;
    }
    catch (NumberFormatException paramResult) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/VEventResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */