package com.google.zxing.client.result;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import java.util.HashMap;
import java.util.Map;

public final class ExpandedProductResultParser
  extends ResultParser
{
  private static String findAIvalue(int paramInt, String paramString)
  {
    if (paramString.charAt(paramInt) != '(') {
      return null;
    }
    paramString = paramString.substring(paramInt + 1);
    StringBuilder localStringBuilder = new StringBuilder();
    paramInt = 0;
    while (paramInt < paramString.length())
    {
      char c = paramString.charAt(paramInt);
      if (c == ')') {
        return localStringBuilder.toString();
      }
      if ((c >= '0') && (c <= '9'))
      {
        localStringBuilder.append(c);
        paramInt++;
      }
      else
      {
        return null;
      }
    }
    return localStringBuilder.toString();
  }
  
  private static String findValue(int paramInt, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    paramString = paramString.substring(paramInt);
    for (paramInt = 0; paramInt < paramString.length(); paramInt++)
    {
      char c = paramString.charAt(paramInt);
      if (c == '(')
      {
        if (findAIvalue(paramInt, paramString) != null) {
          break;
        }
        localStringBuilder.append('(');
      }
      else
      {
        localStringBuilder.append(c);
      }
    }
    return localStringBuilder.toString();
  }
  
  public ExpandedProductParsedResult parse(Result paramResult)
  {
    if (paramResult.getBarcodeFormat() != BarcodeFormat.RSS_EXPANDED) {
      return null;
    }
    String str1 = getMassagedText(paramResult);
    HashMap localHashMap = new HashMap();
    Object localObject13 = null;
    Object localObject2 = localObject13;
    Object localObject6 = localObject2;
    Object localObject7 = localObject6;
    Object localObject8 = localObject7;
    Object localObject9 = localObject8;
    Object localObject10 = localObject9;
    Object localObject4 = localObject10;
    Object localObject5 = localObject4;
    paramResult = (Result)localObject5;
    Object localObject1 = paramResult;
    Object localObject3 = localObject1;
    Object localObject11 = localObject3;
    int i = 0;
    Object localObject12 = localObject2;
    while (i < str1.length())
    {
      String str2 = findAIvalue(i, str1);
      if (str2 == null) {
        return null;
      }
      i += str2.length() + 2;
      localObject2 = findValue(i, str1);
      int j = i + ((String)localObject2).length();
      i = str2.hashCode();
      if (i != 1570)
      {
        if (i != 1572)
        {
          if (i != 1574)
          {
            switch (i)
            {
            default: 
              switch (i)
              {
              default: 
                switch (i)
                {
                default: 
                  switch (i)
                  {
                  default: 
                    switch (i)
                    {
                    default: 
                      switch (i)
                      {
                      default: 
                        break;
                      case 1575750: 
                        if (!str2.equals("3933")) {
                          break;
                        }
                        i = 34;
                        break;
                      case 1575749: 
                        if (!str2.equals("3932")) {
                          break;
                        }
                        i = 33;
                        break;
                      case 1575748: 
                        if (!str2.equals("3931")) {
                          break;
                        }
                        i = 32;
                        break;
                      case 1575747: 
                        if (!str2.equals("3930")) {
                          break;
                        }
                        i = 31;
                      }
                      break;
                    case 1575719: 
                      if (!str2.equals("3923")) {
                        break;
                      }
                      i = 30;
                      break;
                    case 1575718: 
                      if (!str2.equals("3922")) {
                        break;
                      }
                      i = 29;
                      break;
                    case 1575717: 
                      if (!str2.equals("3921")) {
                        break;
                      }
                      i = 28;
                      break;
                    case 1575716: 
                      if (!str2.equals("3920")) {
                        break;
                      }
                      i = 27;
                    }
                    break;
                  case 1568936: 
                    if (!str2.equals("3209")) {
                      break;
                    }
                    i = 26;
                    break;
                  case 1568935: 
                    if (!str2.equals("3208")) {
                      break;
                    }
                    i = 25;
                    break;
                  case 1568934: 
                    if (!str2.equals("3207")) {
                      break;
                    }
                    i = 24;
                    break;
                  case 1568933: 
                    if (!str2.equals("3206")) {
                      break;
                    }
                    i = 23;
                    break;
                  case 1568932: 
                    if (!str2.equals("3205")) {
                      break;
                    }
                    i = 22;
                    break;
                  case 1568931: 
                    if (!str2.equals("3204")) {
                      break;
                    }
                    i = 21;
                    break;
                  case 1568930: 
                    if (!str2.equals("3203")) {
                      break;
                    }
                    i = 20;
                    break;
                  case 1568929: 
                    if (!str2.equals("3202")) {
                      break;
                    }
                    i = 19;
                    break;
                  case 1568928: 
                    if (!str2.equals("3201")) {
                      break;
                    }
                    i = 18;
                    break;
                  case 1568927: 
                    if (!str2.equals("3200")) {
                      break;
                    }
                    i = 17;
                  }
                  break;
                case 1567975: 
                  if (!str2.equals("3109")) {
                    break;
                  }
                  i = 16;
                  break;
                case 1567974: 
                  if (!str2.equals("3108")) {
                    break;
                  }
                  i = 15;
                  break;
                case 1567973: 
                  if (!str2.equals("3107")) {
                    break;
                  }
                  i = 14;
                  break;
                case 1567972: 
                  if (!str2.equals("3106")) {
                    break;
                  }
                  i = 13;
                  break;
                case 1567971: 
                  if (!str2.equals("3105")) {
                    break;
                  }
                  i = 12;
                  break;
                case 1567970: 
                  if (!str2.equals("3104")) {
                    break;
                  }
                  i = 11;
                  break;
                case 1567969: 
                  if (!str2.equals("3103")) {
                    break;
                  }
                  i = 10;
                  break;
                case 1567968: 
                  if (!str2.equals("3102")) {
                    break;
                  }
                  i = 9;
                  break;
                case 1567967: 
                  if (!str2.equals("3101")) {
                    break;
                  }
                  i = 8;
                  break;
                case 1567966: 
                  if (!str2.equals("3100")) {
                    break;
                  }
                  i = 7;
                }
                break;
              case 1568: 
                if (!str2.equals("11")) {
                  break;
                }
                i = 3;
                break;
              case 1567: 
                if (!str2.equals("10")) {
                  break;
                }
                i = 2;
              }
              break;
            case 1537: 
              if (!str2.equals("01")) {
                break;
              }
              i = 1;
              break;
            case 1536: 
              if (!str2.equals("00")) {
                break;
              }
              i = 0;
              break;
            }
          }
          else if (str2.equals("17"))
          {
            i = 6;
            break label943;
          }
        }
        else if (str2.equals("15"))
        {
          i = 5;
          break label943;
        }
      }
      else if (str2.equals("13"))
      {
        i = 4;
        break label943;
      }
      i = -1;
      switch (i)
      {
      default: 
        localHashMap.put(str2, localObject2);
        i = j;
        break;
      case 31: 
      case 32: 
      case 33: 
      case 34: 
        if (((String)localObject2).length() < 4) {
          return null;
        }
        localObject1 = ((String)localObject2).substring(3);
        localObject11 = ((String)localObject2).substring(0, 3);
        localObject3 = str2.substring(3);
        i = j;
        break;
      case 27: 
      case 28: 
      case 29: 
      case 30: 
        localObject3 = str2.substring(3);
        localObject1 = localObject2;
        i = j;
        break;
      case 17: 
      case 18: 
      case 19: 
      case 20: 
      case 21: 
      case 22: 
      case 23: 
      case 24: 
      case 25: 
      case 26: 
        localObject5 = "LB";
        paramResult = str2.substring(3);
        i = j;
        localObject4 = localObject2;
        break;
      case 7: 
      case 8: 
      case 9: 
      case 10: 
      case 11: 
      case 12: 
      case 13: 
      case 14: 
      case 15: 
      case 16: 
        localObject5 = "KG";
        paramResult = str2.substring(3);
        i = j;
        localObject4 = localObject2;
        break;
      case 6: 
        i = j;
        localObject10 = localObject2;
        break;
      case 5: 
        i = j;
        localObject9 = localObject2;
        break;
      case 4: 
        i = j;
        localObject8 = localObject2;
        break;
      case 3: 
        i = j;
        localObject7 = localObject2;
        break;
      case 2: 
        i = j;
        localObject6 = localObject2;
        break;
      case 1: 
        i = j;
        localObject13 = localObject2;
        break;
      case 0: 
        label943:
        i = j;
        localObject12 = localObject2;
      }
    }
    return new ExpandedProductParsedResult(str1, (String)localObject13, (String)localObject12, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10, (String)localObject4, (String)localObject5, paramResult, (String)localObject1, (String)localObject3, (String)localObject11, localHashMap);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/ExpandedProductResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */