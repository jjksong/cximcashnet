package com.google.zxing.client.result;

import com.google.zxing.Result;

public final class WifiResultParser
  extends ResultParser
{
  public WifiParsedResult parse(Result paramResult)
  {
    String str3 = getMassagedText(paramResult);
    if (!str3.startsWith("WIFI:")) {
      return null;
    }
    String str2 = matchSinglePrefixedField("S:", str3, ';', false);
    if ((str2 != null) && (!str2.isEmpty()))
    {
      String str4 = matchSinglePrefixedField("P:", str3, ';', false);
      String str1 = matchSinglePrefixedField("T:", str3, ';', false);
      paramResult = str1;
      if (str1 == null) {
        paramResult = "nopass";
      }
      return new WifiParsedResult(paramResult, str2, str4, Boolean.parseBoolean(matchSinglePrefixedField("H:", str3, ';', false)));
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/WifiResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */