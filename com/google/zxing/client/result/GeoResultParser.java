package com.google.zxing.client.result;

import com.google.zxing.Result;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class GeoResultParser
  extends ResultParser
{
  private static final Pattern GEO_URL_PATTERN = Pattern.compile("geo:([\\-0-9.]+),([\\-0-9.]+)(?:,([\\-0-9.]+))?(?:\\?(.*))?", 2);
  
  public GeoParsedResult parse(Result paramResult)
  {
    paramResult = getMassagedText(paramResult);
    Matcher localMatcher = GEO_URL_PATTERN.matcher(paramResult);
    if (!localMatcher.matches()) {
      return null;
    }
    paramResult = localMatcher.group(4);
    try
    {
      double d2 = Double.parseDouble(localMatcher.group(1));
      if ((d2 <= 90.0D) && (d2 >= -90.0D))
      {
        double d3 = Double.parseDouble(localMatcher.group(2));
        if ((d3 <= 180.0D) && (d3 >= -180.0D))
        {
          String str = localMatcher.group(3);
          double d1 = 0.0D;
          if (str != null)
          {
            d1 = Double.parseDouble(localMatcher.group(3));
            if (d1 < 0.0D) {
              return null;
            }
          }
          return new GeoParsedResult(d2, d3, d1, paramResult);
        }
        return null;
      }
      return null;
    }
    catch (NumberFormatException paramResult) {}
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/GeoResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */