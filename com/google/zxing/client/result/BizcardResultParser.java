package com.google.zxing.client.result;

import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.List;

public final class BizcardResultParser
  extends AbstractDoCoMoResultParser
{
  private static String buildName(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return paramString2;
    }
    if (paramString2 == null) {
      return paramString1;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(' ');
    localStringBuilder.append(paramString2);
    return localStringBuilder.toString();
  }
  
  private static String[] buildPhoneNumbers(String paramString1, String paramString2, String paramString3)
  {
    ArrayList localArrayList = new ArrayList(3);
    if (paramString1 != null) {
      localArrayList.add(paramString1);
    }
    if (paramString2 != null) {
      localArrayList.add(paramString2);
    }
    if (paramString3 != null) {
      localArrayList.add(paramString3);
    }
    int i = localArrayList.size();
    if (i == 0) {
      return null;
    }
    return (String[])localArrayList.toArray(new String[i]);
  }
  
  public AddressBookParsedResult parse(Result paramResult)
  {
    String str6 = getMassagedText(paramResult);
    if (!str6.startsWith("BIZCARD:")) {
      return null;
    }
    String str5 = buildName(matchSingleDoCoMoPrefixedField("N:", str6, true), matchSingleDoCoMoPrefixedField("X:", str6, true));
    String str4 = matchSingleDoCoMoPrefixedField("T:", str6, true);
    String str3 = matchSingleDoCoMoPrefixedField("C:", str6, true);
    String[] arrayOfString = matchDoCoMoPrefixedField("A:", str6, true);
    paramResult = matchSingleDoCoMoPrefixedField("B:", str6, true);
    String str2 = matchSingleDoCoMoPrefixedField("M:", str6, true);
    String str1 = matchSingleDoCoMoPrefixedField("F:", str6, true);
    str6 = matchSingleDoCoMoPrefixedField("E:", str6, true);
    return new AddressBookParsedResult(maybeWrap(str5), null, null, buildPhoneNumbers(paramResult, str2, str1), null, maybeWrap(str6), null, null, null, arrayOfString, null, str3, null, str4, null, null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/BizcardResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */