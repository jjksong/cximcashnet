package com.google.zxing.client.result;

import com.google.zxing.Result;

public final class AddressBookDoCoMoResultParser
  extends AbstractDoCoMoResultParser
{
  private static String parseName(String paramString)
  {
    int i = paramString.indexOf(',');
    if (i >= 0)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString.substring(i + 1));
      localStringBuilder.append(' ');
      localStringBuilder.append(paramString.substring(0, i));
      return localStringBuilder.toString();
    }
    return paramString;
  }
  
  public AddressBookParsedResult parse(Result paramResult)
  {
    String str3 = getMassagedText(paramResult);
    if (!str3.startsWith("MECARD:")) {
      return null;
    }
    paramResult = matchDoCoMoPrefixedField("N:", str3, true);
    if (paramResult == null) {
      return null;
    }
    String str1 = parseName(paramResult[0]);
    String str4 = matchSingleDoCoMoPrefixedField("SOUND:", str3, true);
    String[] arrayOfString2 = matchDoCoMoPrefixedField("TEL:", str3, true);
    String[] arrayOfString1 = matchDoCoMoPrefixedField("EMAIL:", str3, true);
    String str2 = matchSingleDoCoMoPrefixedField("NOTE:", str3, false);
    String[] arrayOfString3 = matchDoCoMoPrefixedField("ADR:", str3, true);
    paramResult = matchSingleDoCoMoPrefixedField("BDAY:", str3, true);
    if (!isStringOfDigits(paramResult, 8)) {
      paramResult = null;
    }
    String[] arrayOfString4 = matchDoCoMoPrefixedField("URL:", str3, true);
    str3 = matchSingleDoCoMoPrefixedField("ORG:", str3, true);
    return new AddressBookParsedResult(maybeWrap(str1), null, str4, arrayOfString2, null, arrayOfString1, null, null, str2, arrayOfString3, null, str3, paramResult, null, arrayOfString4, null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/AddressBookDoCoMoResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */