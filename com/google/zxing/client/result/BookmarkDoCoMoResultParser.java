package com.google.zxing.client.result;

import com.google.zxing.Result;

public final class BookmarkDoCoMoResultParser
  extends AbstractDoCoMoResultParser
{
  public URIParsedResult parse(Result paramResult)
  {
    Object localObject = paramResult.getText();
    boolean bool = ((String)localObject).startsWith("MEBKM:");
    paramResult = null;
    if (!bool) {
      return null;
    }
    String str = matchSingleDoCoMoPrefixedField("TITLE:", (String)localObject, true);
    localObject = matchDoCoMoPrefixedField("URL:", (String)localObject, true);
    if (localObject == null) {
      return null;
    }
    localObject = localObject[0];
    if (URIResultParser.isBasicallyValidURI((String)localObject)) {
      paramResult = new URIParsedResult((String)localObject, str);
    }
    return paramResult;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/client/result/BookmarkDoCoMoResultParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */