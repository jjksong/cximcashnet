package com.google.zxing;

public final class ChecksumException
  extends ReaderException
{
  private static final ChecksumException INSTANCE;
  
  static
  {
    ChecksumException localChecksumException = new ChecksumException();
    INSTANCE = localChecksumException;
    localChecksumException.setStackTrace(NO_TRACE);
  }
  
  private ChecksumException() {}
  
  private ChecksumException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public static ChecksumException getChecksumInstance()
  {
    if (isStackTrace) {
      return new ChecksumException();
    }
    return INSTANCE;
  }
  
  public static ChecksumException getChecksumInstance(Throwable paramThrowable)
  {
    if (isStackTrace) {
      return new ChecksumException(paramThrowable);
    }
    return INSTANCE;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/ChecksumException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */