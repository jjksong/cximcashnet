package com.google.zxing;

public final class FormatException
  extends ReaderException
{
  private static final FormatException INSTANCE;
  
  static
  {
    FormatException localFormatException = new FormatException();
    INSTANCE = localFormatException;
    localFormatException.setStackTrace(NO_TRACE);
  }
  
  private FormatException() {}
  
  private FormatException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public static FormatException getFormatInstance()
  {
    if (isStackTrace) {
      return new FormatException();
    }
    return INSTANCE;
  }
  
  public static FormatException getFormatInstance(Throwable paramThrowable)
  {
    if (isStackTrace) {
      return new FormatException(paramThrowable);
    }
    return INSTANCE;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/FormatException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */