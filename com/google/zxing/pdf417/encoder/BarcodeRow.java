package com.google.zxing.pdf417.encoder;

final class BarcodeRow
{
  private int currentLocation;
  private final byte[] row;
  
  BarcodeRow(int paramInt)
  {
    this.row = new byte[paramInt];
    this.currentLocation = 0;
  }
  
  private void set(int paramInt, boolean paramBoolean)
  {
    this.row[paramInt] = ((byte)paramBoolean);
  }
  
  void addBar(boolean paramBoolean, int paramInt)
  {
    for (int i = 0; i < paramInt; i++)
    {
      int j = this.currentLocation;
      this.currentLocation = (j + 1);
      set(j, paramBoolean);
    }
  }
  
  byte[] getScaledRow(int paramInt)
  {
    byte[] arrayOfByte = new byte[this.row.length * paramInt];
    for (int i = 0; i < arrayOfByte.length; i++) {
      arrayOfByte[i] = this.row[(i / paramInt)];
    }
    return arrayOfByte;
  }
  
  void set(int paramInt, byte paramByte)
  {
    this.row[paramInt] = paramByte;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/encoder/BarcodeRow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */