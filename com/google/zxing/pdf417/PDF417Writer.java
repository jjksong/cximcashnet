package com.google.zxing.pdf417;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.pdf417.encoder.BarcodeMatrix;
import com.google.zxing.pdf417.encoder.Compaction;
import com.google.zxing.pdf417.encoder.Dimensions;
import com.google.zxing.pdf417.encoder.PDF417;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;

public final class PDF417Writer
  implements Writer
{
  static final int DEFAULT_ERROR_CORRECTION_LEVEL = 2;
  static final int WHITE_SPACE = 30;
  
  private static BitMatrix bitMatrixFromEncoder(PDF417 paramPDF417, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws WriterException
  {
    paramPDF417.generateBarcodeLogic(paramString, paramInt1);
    paramString = paramPDF417.getBarcodeMatrix().getScaledMatrix(1, 4);
    if (paramInt3 > paramInt2) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    }
    int i;
    if (paramString[0].length < paramString.length) {
      i = 1;
    } else {
      i = 0;
    }
    if ((paramInt1 ^ i) != 0)
    {
      paramString = rotateArray(paramString);
      paramInt1 = 1;
    }
    else
    {
      paramInt1 = 0;
    }
    paramInt2 /= paramString[0].length;
    paramInt3 /= paramString.length;
    if (paramInt2 >= paramInt3) {
      paramInt2 = paramInt3;
    }
    if (paramInt2 > 1)
    {
      paramString = paramPDF417.getBarcodeMatrix().getScaledMatrix(paramInt2, paramInt2 << 2);
      paramPDF417 = paramString;
      if (paramInt1 != 0) {
        paramPDF417 = rotateArray(paramString);
      }
      return bitMatrixFrombitArray(paramPDF417, paramInt4);
    }
    return bitMatrixFrombitArray(paramString, paramInt4);
  }
  
  private static BitMatrix bitMatrixFrombitArray(byte[][] paramArrayOfByte, int paramInt)
  {
    int j = paramArrayOfByte[0].length;
    int i = paramInt * 2;
    BitMatrix localBitMatrix = new BitMatrix(j + i, paramArrayOfByte.length + i);
    localBitMatrix.clear();
    i = localBitMatrix.getHeight() - paramInt - 1;
    j = 0;
    while (j < paramArrayOfByte.length)
    {
      for (int k = 0; k < paramArrayOfByte[0].length; k++) {
        if (paramArrayOfByte[j][k] == 1) {
          localBitMatrix.set(k + paramInt, i);
        }
      }
      j++;
      i--;
    }
    return localBitMatrix;
  }
  
  private static byte[][] rotateArray(byte[][] paramArrayOfByte)
  {
    byte[][] arrayOfByte = (byte[][])Array.newInstance(Byte.TYPE, new int[] { paramArrayOfByte[0].length, paramArrayOfByte.length });
    for (int i = 0; i < paramArrayOfByte.length; i++)
    {
      int k = paramArrayOfByte.length;
      for (int j = 0; j < paramArrayOfByte[0].length; j++) {
        arrayOfByte[j][(k - i - 1)] = paramArrayOfByte[i][j];
      }
    }
    return arrayOfByte;
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
    throws WriterException
  {
    return encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, null);
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (paramBarcodeFormat == BarcodeFormat.PDF_417)
    {
      PDF417 localPDF417 = new PDF417();
      int i = 30;
      int j = 2;
      if (paramMap != null)
      {
        if (paramMap.containsKey(EncodeHintType.PDF417_COMPACT)) {
          localPDF417.setCompact(Boolean.valueOf(paramMap.get(EncodeHintType.PDF417_COMPACT).toString()).booleanValue());
        }
        if (paramMap.containsKey(EncodeHintType.PDF417_COMPACTION)) {
          localPDF417.setCompaction(Compaction.valueOf(paramMap.get(EncodeHintType.PDF417_COMPACTION).toString()));
        }
        if (paramMap.containsKey(EncodeHintType.PDF417_DIMENSIONS))
        {
          paramBarcodeFormat = (Dimensions)paramMap.get(EncodeHintType.PDF417_DIMENSIONS);
          localPDF417.setDimensions(paramBarcodeFormat.getMaxCols(), paramBarcodeFormat.getMinCols(), paramBarcodeFormat.getMaxRows(), paramBarcodeFormat.getMinRows());
        }
        if (paramMap.containsKey(EncodeHintType.MARGIN)) {
          i = Integer.parseInt(paramMap.get(EncodeHintType.MARGIN).toString());
        }
        if (paramMap.containsKey(EncodeHintType.ERROR_CORRECTION)) {
          j = Integer.parseInt(paramMap.get(EncodeHintType.ERROR_CORRECTION).toString());
        }
        if (paramMap.containsKey(EncodeHintType.CHARACTER_SET)) {
          localPDF417.setEncoding(Charset.forName(paramMap.get(EncodeHintType.CHARACTER_SET).toString()));
        }
      }
      else
      {
        j = 2;
        i = 30;
      }
      return bitMatrixFromEncoder(localPDF417, paramString, j, paramInt1, paramInt2, i);
    }
    paramString = new StringBuilder("Can only encode PDF_417, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/PDF417Writer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */