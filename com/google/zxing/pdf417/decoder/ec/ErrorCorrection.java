package com.google.zxing.pdf417.decoder.ec;

import com.google.zxing.ChecksumException;

public final class ErrorCorrection
{
  private final ModulusGF field = ModulusGF.PDF417_GF;
  
  private int[] findErrorLocations(ModulusPoly paramModulusPoly)
    throws ChecksumException
  {
    int m = paramModulusPoly.getDegree();
    int[] arrayOfInt = new int[m];
    int j = 1;
    int i;
    for (int k = 0; (j < this.field.getSize()) && (k < m); k = i)
    {
      i = k;
      if (paramModulusPoly.evaluateAt(j) == 0)
      {
        arrayOfInt[k] = this.field.inverse(j);
        i = k + 1;
      }
      j++;
    }
    if (k == m) {
      return arrayOfInt;
    }
    throw ChecksumException.getChecksumInstance();
  }
  
  private int[] findErrorMagnitudes(ModulusPoly paramModulusPoly1, ModulusPoly paramModulusPoly2, int[] paramArrayOfInt)
  {
    int j = paramModulusPoly2.getDegree();
    int[] arrayOfInt = new int[j];
    for (int i = 1; i <= j; i++) {
      arrayOfInt[(j - i)] = this.field.multiply(i, paramModulusPoly2.getCoefficient(i));
    }
    paramModulusPoly2 = new ModulusPoly(this.field, arrayOfInt);
    j = paramArrayOfInt.length;
    arrayOfInt = new int[j];
    for (i = 0; i < j; i++)
    {
      int m = this.field.inverse(paramArrayOfInt[i]);
      int k = this.field.subtract(0, paramModulusPoly1.evaluateAt(m));
      m = this.field.inverse(paramModulusPoly2.evaluateAt(m));
      arrayOfInt[i] = this.field.multiply(k, m);
    }
    return arrayOfInt;
  }
  
  private ModulusPoly[] runEuclideanAlgorithm(ModulusPoly paramModulusPoly1, ModulusPoly paramModulusPoly2, int paramInt)
    throws ChecksumException
  {
    Object localObject2 = paramModulusPoly1;
    Object localObject1 = paramModulusPoly2;
    if (paramModulusPoly1.getDegree() < paramModulusPoly2.getDegree())
    {
      localObject1 = paramModulusPoly1;
      localObject2 = paramModulusPoly2;
    }
    ModulusPoly localModulusPoly2 = this.field.getZero();
    ModulusPoly localModulusPoly1 = this.field.getOne();
    paramModulusPoly1 = (ModulusPoly)localObject2;
    paramModulusPoly2 = (ModulusPoly)localObject1;
    localObject2 = localModulusPoly2;
    localObject1 = paramModulusPoly1;
    paramModulusPoly1 = localModulusPoly1;
    while (paramModulusPoly2.getDegree() >= paramInt / 2) {
      if (!paramModulusPoly2.isZero())
      {
        localModulusPoly1 = this.field.getZero();
        int i = paramModulusPoly2.getCoefficient(paramModulusPoly2.getDegree());
        int k = this.field.inverse(i);
        while ((((ModulusPoly)localObject1).getDegree() >= paramModulusPoly2.getDegree()) && (!((ModulusPoly)localObject1).isZero()))
        {
          int j = ((ModulusPoly)localObject1).getDegree() - paramModulusPoly2.getDegree();
          i = this.field.multiply(((ModulusPoly)localObject1).getCoefficient(((ModulusPoly)localObject1).getDegree()), k);
          localModulusPoly1 = localModulusPoly1.add(this.field.buildMonomial(j, i));
          localObject1 = ((ModulusPoly)localObject1).subtract(paramModulusPoly2.multiplyByMonomial(j, i));
        }
        localModulusPoly2 = localModulusPoly1.multiply(paramModulusPoly1).subtract((ModulusPoly)localObject2).negative();
        localModulusPoly1 = paramModulusPoly2;
        paramModulusPoly2 = (ModulusPoly)localObject1;
        localObject2 = paramModulusPoly1;
        paramModulusPoly1 = localModulusPoly2;
        localObject1 = localModulusPoly1;
      }
      else
      {
        throw ChecksumException.getChecksumInstance();
      }
    }
    paramInt = paramModulusPoly1.getCoefficient(0);
    if (paramInt != 0)
    {
      paramInt = this.field.inverse(paramInt);
      return new ModulusPoly[] { paramModulusPoly1.multiply(paramInt), paramModulusPoly2.multiply(paramInt) };
    }
    throw ChecksumException.getChecksumInstance();
  }
  
  public int decode(int[] paramArrayOfInt1, int paramInt, int[] paramArrayOfInt2)
    throws ChecksumException
  {
    Object localObject = new ModulusPoly(this.field, paramArrayOfInt1);
    int[] arrayOfInt = new int[paramInt];
    int k = 0;
    int i = paramInt;
    int j = 0;
    int m;
    while (i > 0)
    {
      m = ((ModulusPoly)localObject).evaluateAt(this.field.exp(i));
      arrayOfInt[(paramInt - i)] = m;
      if (m != 0) {
        j = 1;
      }
      i--;
    }
    if (j == 0) {
      return 0;
    }
    localObject = this.field.getOne();
    if (paramArrayOfInt2 != null)
    {
      j = paramArrayOfInt2.length;
      for (i = 0; i < j; i++)
      {
        m = paramArrayOfInt2[i];
        m = this.field.exp(paramArrayOfInt1.length - 1 - m);
        ModulusGF localModulusGF = this.field;
        localObject = ((ModulusPoly)localObject).multiply(new ModulusPoly(localModulusGF, new int[] { localModulusGF.subtract(0, m), 1 }));
      }
    }
    paramArrayOfInt2 = new ModulusPoly(this.field, arrayOfInt);
    localObject = runEuclideanAlgorithm(this.field.buildMonomial(paramInt, 1), paramArrayOfInt2, paramInt);
    paramArrayOfInt2 = localObject[0];
    arrayOfInt = localObject[1];
    localObject = findErrorLocations(paramArrayOfInt2);
    paramArrayOfInt2 = findErrorMagnitudes(arrayOfInt, paramArrayOfInt2, (int[])localObject);
    paramInt = k;
    while (paramInt < localObject.length)
    {
      i = paramArrayOfInt1.length - 1 - this.field.log(localObject[paramInt]);
      if (i >= 0)
      {
        paramArrayOfInt1[i] = this.field.subtract(paramArrayOfInt1[i], paramArrayOfInt2[paramInt]);
        paramInt++;
      }
      else
      {
        throw ChecksumException.getChecksumInstance();
      }
    }
    return localObject.length;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/ec/ErrorCorrection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */