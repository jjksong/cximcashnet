package com.google.zxing.pdf417.decoder.ec;

final class ModulusPoly
{
  private final int[] coefficients;
  private final ModulusGF field;
  
  ModulusPoly(ModulusGF paramModulusGF, int[] paramArrayOfInt)
  {
    if (paramArrayOfInt.length != 0)
    {
      this.field = paramModulusGF;
      int j = paramArrayOfInt.length;
      if ((j > 1) && (paramArrayOfInt[0] == 0))
      {
        for (int i = 1; (i < j) && (paramArrayOfInt[i] == 0); i++) {}
        if (i == j)
        {
          this.coefficients = new int[] { 0 };
          return;
        }
        this.coefficients = new int[j - i];
        paramModulusGF = this.coefficients;
        System.arraycopy(paramArrayOfInt, i, paramModulusGF, 0, paramModulusGF.length);
        return;
      }
      this.coefficients = paramArrayOfInt;
      return;
    }
    throw new IllegalArgumentException();
  }
  
  ModulusPoly add(ModulusPoly paramModulusPoly)
  {
    if (this.field.equals(paramModulusPoly.field))
    {
      if (isZero()) {
        return paramModulusPoly;
      }
      if (paramModulusPoly.isZero()) {
        return this;
      }
      int[] arrayOfInt1 = this.coefficients;
      int[] arrayOfInt3 = paramModulusPoly.coefficients;
      int[] arrayOfInt2 = arrayOfInt1;
      paramModulusPoly = arrayOfInt3;
      if (arrayOfInt1.length > arrayOfInt3.length)
      {
        arrayOfInt2 = arrayOfInt3;
        paramModulusPoly = arrayOfInt1;
      }
      arrayOfInt1 = new int[paramModulusPoly.length];
      int j = paramModulusPoly.length - arrayOfInt2.length;
      System.arraycopy(paramModulusPoly, 0, arrayOfInt1, 0, j);
      for (int i = j; i < paramModulusPoly.length; i++) {
        arrayOfInt1[i] = this.field.add(arrayOfInt2[(i - j)], paramModulusPoly[i]);
      }
      return new ModulusPoly(this.field, arrayOfInt1);
    }
    throw new IllegalArgumentException("ModulusPolys do not have same ModulusGF field");
  }
  
  int evaluateAt(int paramInt)
  {
    int i = 0;
    if (paramInt == 0) {
      return getCoefficient(0);
    }
    int j = 1;
    if (paramInt == 1)
    {
      localObject = this.coefficients;
      j = localObject.length;
      paramInt = 0;
      while (i < j)
      {
        k = localObject[i];
        paramInt = this.field.add(paramInt, k);
        i++;
      }
      return paramInt;
    }
    Object localObject = this.coefficients;
    int k = localObject[0];
    int m = localObject.length;
    i = j;
    j = k;
    while (i < m)
    {
      localObject = this.field;
      j = ((ModulusGF)localObject).add(((ModulusGF)localObject).multiply(paramInt, j), this.coefficients[i]);
      i++;
    }
    return j;
  }
  
  int getCoefficient(int paramInt)
  {
    int[] arrayOfInt = this.coefficients;
    return arrayOfInt[(arrayOfInt.length - 1 - paramInt)];
  }
  
  int[] getCoefficients()
  {
    return this.coefficients;
  }
  
  int getDegree()
  {
    return this.coefficients.length - 1;
  }
  
  boolean isZero()
  {
    return this.coefficients[0] == 0;
  }
  
  ModulusPoly multiply(int paramInt)
  {
    if (paramInt == 0) {
      return this.field.getZero();
    }
    if (paramInt == 1) {
      return this;
    }
    int j = this.coefficients.length;
    int[] arrayOfInt = new int[j];
    for (int i = 0; i < j; i++) {
      arrayOfInt[i] = this.field.multiply(this.coefficients[i], paramInt);
    }
    return new ModulusPoly(this.field, arrayOfInt);
  }
  
  ModulusPoly multiply(ModulusPoly paramModulusPoly)
  {
    if (this.field.equals(paramModulusPoly.field))
    {
      if ((!isZero()) && (!paramModulusPoly.isZero()))
      {
        int[] arrayOfInt1 = this.coefficients;
        int m = arrayOfInt1.length;
        int[] arrayOfInt2 = paramModulusPoly.coefficients;
        int k = arrayOfInt2.length;
        paramModulusPoly = new int[m + k - 1];
        for (int i = 0; i < m; i++)
        {
          int n = arrayOfInt1[i];
          for (int j = 0; j < k; j++)
          {
            int i1 = i + j;
            ModulusGF localModulusGF = this.field;
            paramModulusPoly[i1] = localModulusGF.add(paramModulusPoly[i1], localModulusGF.multiply(n, arrayOfInt2[j]));
          }
        }
        return new ModulusPoly(this.field, paramModulusPoly);
      }
      return this.field.getZero();
    }
    throw new IllegalArgumentException("ModulusPolys do not have same ModulusGF field");
  }
  
  ModulusPoly multiplyByMonomial(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      if (paramInt2 == 0) {
        return this.field.getZero();
      }
      int i = this.coefficients.length;
      int[] arrayOfInt = new int[paramInt1 + i];
      for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
        arrayOfInt[paramInt1] = this.field.multiply(this.coefficients[paramInt1], paramInt2);
      }
      return new ModulusPoly(this.field, arrayOfInt);
    }
    throw new IllegalArgumentException();
  }
  
  ModulusPoly negative()
  {
    int j = this.coefficients.length;
    int[] arrayOfInt = new int[j];
    for (int i = 0; i < j; i++) {
      arrayOfInt[i] = this.field.subtract(0, this.coefficients[i]);
    }
    return new ModulusPoly(this.field, arrayOfInt);
  }
  
  ModulusPoly subtract(ModulusPoly paramModulusPoly)
  {
    if (this.field.equals(paramModulusPoly.field))
    {
      if (paramModulusPoly.isZero()) {
        return this;
      }
      return add(paramModulusPoly.negative());
    }
    throw new IllegalArgumentException("ModulusPolys do not have same ModulusGF field");
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(getDegree() * 8);
    for (int i = getDegree(); i >= 0; i--)
    {
      int k = getCoefficient(i);
      if (k != 0)
      {
        int j;
        if (k < 0)
        {
          localStringBuilder.append(" - ");
          j = -k;
        }
        else
        {
          j = k;
          if (localStringBuilder.length() > 0)
          {
            localStringBuilder.append(" + ");
            j = k;
          }
        }
        if ((i == 0) || (j != 1)) {
          localStringBuilder.append(j);
        }
        if (i != 0) {
          if (i == 1)
          {
            localStringBuilder.append('x');
          }
          else
          {
            localStringBuilder.append("x^");
            localStringBuilder.append(i);
          }
        }
      }
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/ec/ModulusPoly.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */