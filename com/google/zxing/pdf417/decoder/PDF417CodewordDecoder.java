package com.google.zxing.pdf417.decoder;

import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.pdf417.PDF417Common;
import java.lang.reflect.Array;

final class PDF417CodewordDecoder
{
  private static final float[][] RATIOS_TABLE = (float[][])Array.newInstance(Float.TYPE, new int[] { PDF417Common.SYMBOL_TABLE.length, 8 });
  
  static
  {
    for (int i = 0; i < PDF417Common.SYMBOL_TABLE.length; i++)
    {
      int m = PDF417Common.SYMBOL_TABLE[i];
      int j = m & 0x1;
      int k = 0;
      while (k < 8)
      {
        float f = 0.0F;
        int n;
        for (;;)
        {
          n = m & 0x1;
          if (n != j) {
            break;
          }
          f += 1.0F;
          m >>= 1;
        }
        RATIOS_TABLE[i][(8 - k - 1)] = (f / 17.0F);
        k++;
        j = n;
      }
    }
  }
  
  private static int getBitValue(int[] paramArrayOfInt)
  {
    long l = 0L;
    for (int i = 0; i < paramArrayOfInt.length; i++) {
      for (int j = 0; j < paramArrayOfInt[i]; j++)
      {
        int k = 1;
        if (i % 2 != 0) {
          k = 0;
        }
        l = l << 1 | k;
      }
    }
    return (int)l;
  }
  
  private static int getClosestDecodedValue(int[] paramArrayOfInt)
  {
    int j = MathUtils.sum(paramArrayOfInt);
    float[] arrayOfFloat = new float[8];
    for (int i = 0; i < 8; i++) {
      arrayOfFloat[i] = (paramArrayOfInt[i] / j);
    }
    j = -1;
    i = 0;
    float f2;
    for (float f3 = Float.MAX_VALUE;; f3 = f2)
    {
      paramArrayOfInt = RATIOS_TABLE;
      if (i >= paramArrayOfInt.length) {
        break;
      }
      paramArrayOfInt = paramArrayOfInt[i];
      int k = 0;
      f2 = 0.0F;
      float f1;
      for (;;)
      {
        f1 = f2;
        if (k >= 8) {
          break;
        }
        f1 = paramArrayOfInt[k] - arrayOfFloat[k];
        f2 += f1 * f1;
        f1 = f2;
        if (f2 >= f3) {
          break;
        }
        k++;
      }
      f2 = f3;
      if (f1 < f3)
      {
        j = PDF417Common.SYMBOL_TABLE[i];
        f2 = f1;
      }
      i++;
    }
    return j;
  }
  
  private static int getDecodedCodewordValue(int[] paramArrayOfInt)
  {
    int i = getBitValue(paramArrayOfInt);
    if (PDF417Common.getCodeword(i) == -1) {
      return -1;
    }
    return i;
  }
  
  static int getDecodedValue(int[] paramArrayOfInt)
  {
    int i = getDecodedCodewordValue(sampleBitCounts(paramArrayOfInt));
    if (i != -1) {
      return i;
    }
    return getClosestDecodedValue(paramArrayOfInt);
  }
  
  private static int[] sampleBitCounts(int[] paramArrayOfInt)
  {
    float f3 = MathUtils.sum(paramArrayOfInt);
    int[] arrayOfInt = new int[8];
    int k = 0;
    int n = 0;
    int j;
    for (int i = 0; k < 17; i = j)
    {
      float f1 = f3 / 34.0F;
      float f2 = k * f3 / 17.0F;
      int m = n;
      j = i;
      if (paramArrayOfInt[i] + n <= f1 + f2)
      {
        m = n + paramArrayOfInt[i];
        j = i + 1;
      }
      arrayOfInt[j] += 1;
      k++;
      n = m;
    }
    return arrayOfInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/PDF417CodewordDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */