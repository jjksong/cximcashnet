package com.google.zxing.pdf417.decoder;

import java.util.Formatter;

class DetectionResultColumn
{
  private static final int MAX_NEARBY_DISTANCE = 5;
  private final BoundingBox boundingBox;
  private final Codeword[] codewords;
  
  DetectionResultColumn(BoundingBox paramBoundingBox)
  {
    this.boundingBox = new BoundingBox(paramBoundingBox);
    this.codewords = new Codeword[paramBoundingBox.getMaxY() - paramBoundingBox.getMinY() + 1];
  }
  
  final BoundingBox getBoundingBox()
  {
    return this.boundingBox;
  }
  
  final Codeword getCodeword(int paramInt)
  {
    return this.codewords[imageRowToCodewordIndex(paramInt)];
  }
  
  final Codeword getCodewordNearby(int paramInt)
  {
    Object localObject = getCodeword(paramInt);
    if (localObject != null) {
      return (Codeword)localObject;
    }
    for (int i = 1; i < 5; i++)
    {
      int j = imageRowToCodewordIndex(paramInt) - i;
      if (j >= 0)
      {
        localObject = this.codewords[j];
        if (localObject != null) {
          return (Codeword)localObject;
        }
      }
      j = imageRowToCodewordIndex(paramInt) + i;
      localObject = this.codewords;
      if (j < localObject.length)
      {
        localObject = localObject[j];
        if (localObject != null) {
          return (Codeword)localObject;
        }
      }
    }
    return null;
  }
  
  final Codeword[] getCodewords()
  {
    return this.codewords;
  }
  
  final int imageRowToCodewordIndex(int paramInt)
  {
    return paramInt - this.boundingBox.getMinY();
  }
  
  final void setCodeword(int paramInt, Codeword paramCodeword)
  {
    this.codewords[imageRowToCodewordIndex(paramInt)] = paramCodeword;
  }
  
  public String toString()
  {
    Formatter localFormatter = new Formatter();
    Object localObject1 = this.codewords;
    int k = localObject1.length;
    int j = 0;
    int i = 0;
    while (j < k)
    {
      Object localObject2 = localObject1[j];
      if (localObject2 == null)
      {
        localFormatter.format("%3d:    |   %n", new Object[] { Integer.valueOf(i) });
        i++;
      }
      else
      {
        localFormatter.format("%3d: %3d|%3d%n", new Object[] { Integer.valueOf(i), Integer.valueOf(((Codeword)localObject2).getRowNumber()), Integer.valueOf(((Codeword)localObject2).getValue()) });
        i++;
      }
      j++;
    }
    localObject1 = localFormatter.toString();
    localFormatter.close();
    return (String)localObject1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/DetectionResultColumn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */