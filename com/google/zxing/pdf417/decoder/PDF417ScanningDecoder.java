package com.google.zxing.pdf417.decoder;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.pdf417.PDF417Common;
import com.google.zxing.pdf417.decoder.ec.ErrorCorrection;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;

public final class PDF417ScanningDecoder
{
  private static final int CODEWORD_SKEW_SIZE = 2;
  private static final int MAX_EC_CODEWORDS = 512;
  private static final int MAX_ERRORS = 3;
  private static final ErrorCorrection errorCorrection = new ErrorCorrection();
  
  private static BoundingBox adjustBoundingBox(DetectionResultRowIndicatorColumn paramDetectionResultRowIndicatorColumn)
    throws NotFoundException
  {
    if (paramDetectionResultRowIndicatorColumn == null) {
      return null;
    }
    int[] arrayOfInt = paramDetectionResultRowIndicatorColumn.getRowHeights();
    if (arrayOfInt == null) {
      return null;
    }
    int i1 = getMax(arrayOfInt);
    int m = arrayOfInt.length;
    int n = 0;
    int k = 0;
    int j = 0;
    for (;;)
    {
      i = j;
      if (k >= m) {
        break;
      }
      int i2 = arrayOfInt[k];
      j += i1 - i2;
      i = j;
      if (i2 > 0) {
        break;
      }
      k++;
    }
    Codeword[] arrayOfCodeword = paramDetectionResultRowIndicatorColumn.getCodewords();
    j = 0;
    k = i;
    for (int i = j; (k > 0) && (arrayOfCodeword[i] == null); i++) {
      k--;
    }
    m = arrayOfInt.length - 1;
    j = n;
    for (;;)
    {
      i = j;
      if (m < 0) {
        break;
      }
      j += i1 - arrayOfInt[m];
      i = j;
      if (arrayOfInt[m] > 0) {
        break;
      }
      m--;
    }
    for (j = arrayOfCodeword.length - 1; (i > 0) && (arrayOfCodeword[j] == null); j--) {
      i--;
    }
    return paramDetectionResultRowIndicatorColumn.getBoundingBox().addMissingRows(k, i, paramDetectionResultRowIndicatorColumn.isLeft());
  }
  
  private static void adjustCodewordCount(DetectionResult paramDetectionResult, BarcodeValue[][] paramArrayOfBarcodeValue)
    throws NotFoundException
  {
    int[] arrayOfInt = paramArrayOfBarcodeValue[0][1].getValue();
    int i = paramDetectionResult.getBarcodeColumnCount() * paramDetectionResult.getBarcodeRowCount() - getNumberOfECCodeWords(paramDetectionResult.getBarcodeECLevel());
    if (arrayOfInt.length == 0)
    {
      if ((i > 0) && (i <= 928))
      {
        paramArrayOfBarcodeValue[0][1].setValue(i);
        return;
      }
      throw NotFoundException.getNotFoundInstance();
    }
    if (arrayOfInt[0] != i) {
      paramArrayOfBarcodeValue[0][1].setValue(i);
    }
  }
  
  private static int adjustCodewordStartColumn(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4)
  {
    if (paramBoolean) {
      i = -1;
    } else {
      i = 1;
    }
    int j = i;
    int i = 0;
    int k = paramInt3;
    while (i < 2)
    {
      while ((paramBoolean ? k >= paramInt1 : k < paramInt2) && (paramBoolean == paramBitMatrix.get(k, paramInt4)))
      {
        if (Math.abs(paramInt3 - k) > 2) {
          return paramInt3;
        }
        k += j;
      }
      j = -j;
      if (!paramBoolean) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }
      i++;
    }
    return k;
  }
  
  private static boolean checkCodewordSkew(int paramInt1, int paramInt2, int paramInt3)
  {
    return (paramInt2 - 2 <= paramInt1) && (paramInt1 <= paramInt3 + 2);
  }
  
  private static int correctErrors(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
    throws ChecksumException
  {
    if (((paramArrayOfInt2 == null) || (paramArrayOfInt2.length <= paramInt / 2 + 3)) && (paramInt >= 0) && (paramInt <= 512)) {
      return errorCorrection.decode(paramArrayOfInt1, paramInt, paramArrayOfInt2);
    }
    throw ChecksumException.getChecksumInstance();
  }
  
  private static BarcodeValue[][] createBarcodeMatrix(DetectionResult paramDetectionResult)
  {
    BarcodeValue[][] arrayOfBarcodeValue = (BarcodeValue[][])Array.newInstance(BarcodeValue.class, new int[] { paramDetectionResult.getBarcodeRowCount(), paramDetectionResult.getBarcodeColumnCount() + 2 });
    for (int i = 0; i < arrayOfBarcodeValue.length; i++) {
      for (j = 0; j < arrayOfBarcodeValue[i].length; j++) {
        arrayOfBarcodeValue[i][j] = new BarcodeValue();
      }
    }
    paramDetectionResult = paramDetectionResult.getDetectionResultColumns();
    int m = paramDetectionResult.length;
    int j = 0;
    i = 0;
    while (j < m)
    {
      Object localObject = paramDetectionResult[j];
      if (localObject != null) {
        for (localObject : ((DetectionResultColumn)localObject).getCodewords()) {
          if (localObject != null)
          {
            int i1 = ((Codeword)localObject).getRowNumber();
            if ((i1 >= 0) && (i1 < arrayOfBarcodeValue.length)) {
              arrayOfBarcodeValue[i1][i].setValue(((Codeword)localObject).getValue());
            }
          }
        }
      }
      i++;
      j++;
    }
    return arrayOfBarcodeValue;
  }
  
  private static DecoderResult createDecoderResult(DetectionResult paramDetectionResult)
    throws FormatException, ChecksumException, NotFoundException
  {
    Object localObject = createBarcodeMatrix(paramDetectionResult);
    adjustCodewordCount(paramDetectionResult, (BarcodeValue[][])localObject);
    ArrayList localArrayList2 = new ArrayList();
    int[] arrayOfInt1 = new int[paramDetectionResult.getBarcodeRowCount() * paramDetectionResult.getBarcodeColumnCount()];
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    int k = 0;
    for (int i = 0; i < paramDetectionResult.getBarcodeRowCount(); i++)
    {
      int m;
      for (int j = 0; j < paramDetectionResult.getBarcodeColumnCount(); j = m)
      {
        int[] arrayOfInt2 = localObject[i];
        m = j + 1;
        arrayOfInt2 = arrayOfInt2[m].getValue();
        j = paramDetectionResult.getBarcodeColumnCount() * i + j;
        if (arrayOfInt2.length == 0)
        {
          localArrayList2.add(Integer.valueOf(j));
        }
        else if (arrayOfInt2.length == 1)
        {
          arrayOfInt1[j] = arrayOfInt2[0];
        }
        else
        {
          localArrayList3.add(Integer.valueOf(j));
          localArrayList1.add(arrayOfInt2);
        }
      }
    }
    localObject = new int[localArrayList1.size()][];
    for (i = k; i < localObject.length; i++) {
      localObject[i] = ((int[])localArrayList1.get(i));
    }
    return createDecoderResultFromAmbiguousValues(paramDetectionResult.getBarcodeECLevel(), arrayOfInt1, PDF417Common.toIntArray(localArrayList2), PDF417Common.toIntArray(localArrayList3), (int[][])localObject);
  }
  
  private static DecoderResult createDecoderResultFromAmbiguousValues(int paramInt, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3, int[][] paramArrayOfInt)
    throws FormatException, ChecksumException
  {
    int[] arrayOfInt = new int[paramArrayOfInt3.length];
    int i = 100;
    while (i > 0)
    {
      for (int j = 0; j < arrayOfInt.length; j++) {
        paramArrayOfInt1[paramArrayOfInt3[j]] = paramArrayOfInt[j][arrayOfInt[j]];
      }
      try
      {
        DecoderResult localDecoderResult = decodeCodewords(paramArrayOfInt1, paramInt, paramArrayOfInt2);
        return localDecoderResult;
      }
      catch (ChecksumException localChecksumException)
      {
        if (arrayOfInt.length != 0)
        {
          j = 0;
          while (j < arrayOfInt.length) {
            if (arrayOfInt[j] < paramArrayOfInt[j].length - 1)
            {
              arrayOfInt[j] += 1;
            }
            else
            {
              arrayOfInt[j] = 0;
              if (j != arrayOfInt.length - 1) {
                j++;
              } else {
                throw ChecksumException.getChecksumInstance();
              }
            }
          }
          i--;
        }
        else
        {
          throw ChecksumException.getChecksumInstance();
        }
      }
    }
    throw ChecksumException.getChecksumInstance();
  }
  
  public static DecoderResult decode(BitMatrix paramBitMatrix, ResultPoint paramResultPoint1, ResultPoint paramResultPoint2, ResultPoint paramResultPoint3, ResultPoint paramResultPoint4, int paramInt1, int paramInt2)
    throws NotFoundException, FormatException, ChecksumException
  {
    BoundingBox localBoundingBox = new BoundingBox(paramBitMatrix, paramResultPoint1, paramResultPoint2, paramResultPoint3, paramResultPoint4);
    paramResultPoint2 = null;
    Object localObject = paramResultPoint2;
    paramResultPoint4 = (ResultPoint)localObject;
    int i = 0;
    while (i < 2)
    {
      if (paramResultPoint1 != null) {
        paramResultPoint2 = getRowIndicatorColumn(paramBitMatrix, localBoundingBox, paramResultPoint1, true, paramInt1, paramInt2);
      }
      if (paramResultPoint3 != null) {
        paramResultPoint4 = getRowIndicatorColumn(paramBitMatrix, localBoundingBox, paramResultPoint3, false, paramInt1, paramInt2);
      }
      localObject = merge(paramResultPoint2, paramResultPoint4);
      if (localObject != null)
      {
        if ((i == 0) && (((DetectionResult)localObject).getBoundingBox() != null) && ((((DetectionResult)localObject).getBoundingBox().getMinY() < localBoundingBox.getMinY()) || (((DetectionResult)localObject).getBoundingBox().getMaxY() > localBoundingBox.getMaxY())))
        {
          localBoundingBox = ((DetectionResult)localObject).getBoundingBox();
          i++;
        }
        else
        {
          ((DetectionResult)localObject).setBoundingBox(localBoundingBox);
        }
      }
      else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    int i1 = ((DetectionResult)localObject).getBarcodeColumnCount() + 1;
    ((DetectionResult)localObject).setDetectionResultColumn(0, paramResultPoint2);
    ((DetectionResult)localObject).setDetectionResultColumn(i1, paramResultPoint4);
    boolean bool1;
    if (paramResultPoint2 != null) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    i = 1;
    while (i <= i1)
    {
      int m;
      if (bool1) {
        m = i;
      } else {
        m = i1 - i;
      }
      int k = paramInt1;
      int j = paramInt2;
      if (((DetectionResult)localObject).getDetectionResultColumn(m) == null)
      {
        if ((m != 0) && (m != i1))
        {
          paramResultPoint1 = new DetectionResultColumn(localBoundingBox);
        }
        else
        {
          boolean bool2;
          if (m == 0) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          paramResultPoint1 = new DetectionResultRowIndicatorColumn(localBoundingBox, bool2);
        }
        ((DetectionResult)localObject).setDetectionResultColumn(m, paramResultPoint1);
        int n = localBoundingBox.getMinY();
        k = -1;
        j = paramInt1;
        paramInt1 = paramInt2;
        paramInt2 = k;
        while (n <= localBoundingBox.getMaxY())
        {
          k = getStartColumn((DetectionResult)localObject, m, n, bool1);
          if ((k >= 0) && (k <= localBoundingBox.getMaxX())) {
            break label379;
          }
          if (paramInt2 != -1)
          {
            k = paramInt2;
            label379:
            paramResultPoint2 = detectCodeword(paramBitMatrix, localBoundingBox.getMinX(), localBoundingBox.getMaxX(), bool1, k, n, j, paramInt1);
            if (paramResultPoint2 != null)
            {
              paramResultPoint1.setCodeword(n, paramResultPoint2);
              j = Math.min(j, paramResultPoint2.getWidth());
              paramInt1 = Math.max(paramInt1, paramResultPoint2.getWidth());
              paramInt2 = k;
            }
            else {}
          }
          n++;
        }
        k = j;
        j = paramInt1;
      }
      i++;
      paramInt1 = k;
      paramInt2 = j;
    }
    return createDecoderResult((DetectionResult)localObject);
  }
  
  private static DecoderResult decodeCodewords(int[] paramArrayOfInt1, int paramInt, int[] paramArrayOfInt2)
    throws FormatException, ChecksumException
  {
    if (paramArrayOfInt1.length != 0)
    {
      int i = 1 << paramInt + 1;
      int j = correctErrors(paramArrayOfInt1, paramArrayOfInt2, i);
      verifyCodewordCount(paramArrayOfInt1, i);
      paramArrayOfInt1 = DecodedBitStreamParser.decode(paramArrayOfInt1, String.valueOf(paramInt));
      paramArrayOfInt1.setErrorsCorrected(Integer.valueOf(j));
      paramArrayOfInt1.setErasures(Integer.valueOf(paramArrayOfInt2.length));
      return paramArrayOfInt1;
    }
    throw FormatException.getFormatInstance();
  }
  
  private static Codeword detectCodeword(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    paramInt3 = adjustCodewordStartColumn(paramBitMatrix, paramInt1, paramInt2, paramBoolean, paramInt3, paramInt4);
    paramBitMatrix = getModuleBitCount(paramBitMatrix, paramInt1, paramInt2, paramBoolean, paramInt3, paramInt4);
    if (paramBitMatrix == null) {
      return null;
    }
    paramInt4 = MathUtils.sum(paramBitMatrix);
    if (paramBoolean)
    {
      paramInt2 = paramInt3 + paramInt4;
      paramInt1 = paramInt3;
      paramInt3 = paramInt2;
    }
    else
    {
      for (paramInt1 = 0; paramInt1 < paramBitMatrix.length / 2; paramInt1++)
      {
        paramInt2 = paramBitMatrix[paramInt1];
        paramBitMatrix[paramInt1] = paramBitMatrix[(paramBitMatrix.length - 1 - paramInt1)];
        paramBitMatrix[(paramBitMatrix.length - 1 - paramInt1)] = paramInt2;
      }
      paramInt1 = paramInt3 - paramInt4;
    }
    if (!checkCodewordSkew(paramInt4, paramInt5, paramInt6)) {
      return null;
    }
    paramInt2 = PDF417CodewordDecoder.getDecodedValue(paramBitMatrix);
    paramInt4 = PDF417Common.getCodeword(paramInt2);
    if (paramInt4 == -1) {
      return null;
    }
    return new Codeword(paramInt1, paramInt3, getCodewordBucketNumber(paramInt2), paramInt4);
  }
  
  private static BarcodeMetadata getBarcodeMetadata(DetectionResultRowIndicatorColumn paramDetectionResultRowIndicatorColumn1, DetectionResultRowIndicatorColumn paramDetectionResultRowIndicatorColumn2)
  {
    if (paramDetectionResultRowIndicatorColumn1 != null)
    {
      paramDetectionResultRowIndicatorColumn1 = paramDetectionResultRowIndicatorColumn1.getBarcodeMetadata();
      if (paramDetectionResultRowIndicatorColumn1 != null)
      {
        if (paramDetectionResultRowIndicatorColumn2 != null)
        {
          paramDetectionResultRowIndicatorColumn2 = paramDetectionResultRowIndicatorColumn2.getBarcodeMetadata();
          if (paramDetectionResultRowIndicatorColumn2 != null)
          {
            if ((paramDetectionResultRowIndicatorColumn1.getColumnCount() != paramDetectionResultRowIndicatorColumn2.getColumnCount()) && (paramDetectionResultRowIndicatorColumn1.getErrorCorrectionLevel() != paramDetectionResultRowIndicatorColumn2.getErrorCorrectionLevel()) && (paramDetectionResultRowIndicatorColumn1.getRowCount() != paramDetectionResultRowIndicatorColumn2.getRowCount())) {
              return null;
            }
            return paramDetectionResultRowIndicatorColumn1;
          }
        }
        return paramDetectionResultRowIndicatorColumn1;
      }
    }
    if (paramDetectionResultRowIndicatorColumn2 == null) {
      return null;
    }
    return paramDetectionResultRowIndicatorColumn2.getBarcodeMetadata();
  }
  
  private static int[] getBitCountForCodeword(int paramInt)
  {
    int[] arrayOfInt = new int[8];
    int m = 0;
    int k;
    for (int j = 7;; j = k)
    {
      int n = paramInt & 0x1;
      int i = m;
      k = j;
      if (n != m)
      {
        k = j - 1;
        if (k >= 0) {
          i = n;
        } else {
          return arrayOfInt;
        }
      }
      arrayOfInt[k] += 1;
      paramInt >>= 1;
      m = i;
    }
  }
  
  private static int getCodewordBucketNumber(int paramInt)
  {
    return getCodewordBucketNumber(getBitCountForCodeword(paramInt));
  }
  
  private static int getCodewordBucketNumber(int[] paramArrayOfInt)
  {
    return (paramArrayOfInt[0] - paramArrayOfInt[2] + paramArrayOfInt[4] - paramArrayOfInt[6] + 9) % 9;
  }
  
  private static int getMax(int[] paramArrayOfInt)
  {
    int k = paramArrayOfInt.length;
    int j = -1;
    for (int i = 0; i < k; i++) {
      j = Math.max(j, paramArrayOfInt[i]);
    }
    return j;
  }
  
  private static int[] getModuleBitCount(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4)
  {
    int[] arrayOfInt = new int[8];
    int i;
    if (paramBoolean) {
      i = 1;
    } else {
      i = -1;
    }
    boolean bool = paramBoolean;
    int j = 0;
    while ((paramBoolean ? paramInt3 < paramInt2 : paramInt3 >= paramInt1) && (j < 8)) {
      if (paramBitMatrix.get(paramInt3, paramInt4) == bool)
      {
        arrayOfInt[j] += 1;
        paramInt3 += i;
      }
      else
      {
        j++;
        if (!bool) {
          bool = true;
        } else {
          bool = false;
        }
      }
    }
    if (j != 8)
    {
      if (paramBoolean) {
        paramInt1 = paramInt2;
      }
      if ((paramInt3 != paramInt1) || (j != 7)) {
        return null;
      }
    }
    return arrayOfInt;
  }
  
  private static int getNumberOfECCodeWords(int paramInt)
  {
    return 2 << paramInt;
  }
  
  private static DetectionResultRowIndicatorColumn getRowIndicatorColumn(BitMatrix paramBitMatrix, BoundingBox paramBoundingBox, ResultPoint paramResultPoint, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    DetectionResultRowIndicatorColumn localDetectionResultRowIndicatorColumn = new DetectionResultRowIndicatorColumn(paramBoundingBox, paramBoolean);
    for (int j = 0; j < 2; j++)
    {
      int k;
      if (j == 0) {
        k = 1;
      } else {
        k = -1;
      }
      int i = (int)paramResultPoint.getX();
      int m = (int)paramResultPoint.getY();
      while ((m <= paramBoundingBox.getMaxY()) && (m >= paramBoundingBox.getMinY()))
      {
        Codeword localCodeword = detectCodeword(paramBitMatrix, 0, paramBitMatrix.getWidth(), paramBoolean, i, m, paramInt1, paramInt2);
        if (localCodeword != null)
        {
          localDetectionResultRowIndicatorColumn.setCodeword(m, localCodeword);
          if (paramBoolean) {
            i = localCodeword.getStartX();
          } else {
            i = localCodeword.getEndX();
          }
        }
        m += k;
      }
    }
    return localDetectionResultRowIndicatorColumn;
  }
  
  private static int getStartColumn(DetectionResult paramDetectionResult, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i;
    if (paramBoolean) {
      i = 1;
    } else {
      i = -1;
    }
    Codeword localCodeword = null;
    int j = paramInt1 - i;
    if (isValidBarcodeColumn(paramDetectionResult, j)) {
      localCodeword = paramDetectionResult.getDetectionResultColumn(j).getCodeword(paramInt2);
    }
    if (localCodeword != null)
    {
      if (paramBoolean) {
        return localCodeword.getEndX();
      }
      return localCodeword.getStartX();
    }
    localCodeword = paramDetectionResult.getDetectionResultColumn(paramInt1).getCodewordNearby(paramInt2);
    if (localCodeword != null)
    {
      if (paramBoolean) {
        return localCodeword.getStartX();
      }
      return localCodeword.getEndX();
    }
    if (isValidBarcodeColumn(paramDetectionResult, j)) {
      localCodeword = paramDetectionResult.getDetectionResultColumn(j).getCodewordNearby(paramInt2);
    }
    if (localCodeword != null)
    {
      if (paramBoolean) {
        return localCodeword.getEndX();
      }
      return localCodeword.getStartX();
    }
    j = 0;
    paramInt2 = paramInt1;
    paramInt1 = j;
    for (;;)
    {
      j = paramInt2 - i;
      if (!isValidBarcodeColumn(paramDetectionResult, j)) {
        break;
      }
      for (localCodeword : paramDetectionResult.getDetectionResultColumn(j).getCodewords()) {
        if (localCodeword != null)
        {
          if (paramBoolean) {
            paramInt2 = localCodeword.getEndX();
          } else {
            paramInt2 = localCodeword.getStartX();
          }
          return paramInt2 + i * paramInt1 * (localCodeword.getEndX() - localCodeword.getStartX());
        }
      }
      paramInt1++;
      paramInt2 = j;
    }
    if (paramBoolean) {
      return paramDetectionResult.getBoundingBox().getMinX();
    }
    return paramDetectionResult.getBoundingBox().getMaxX();
  }
  
  private static boolean isValidBarcodeColumn(DetectionResult paramDetectionResult, int paramInt)
  {
    return (paramInt >= 0) && (paramInt <= paramDetectionResult.getBarcodeColumnCount() + 1);
  }
  
  private static DetectionResult merge(DetectionResultRowIndicatorColumn paramDetectionResultRowIndicatorColumn1, DetectionResultRowIndicatorColumn paramDetectionResultRowIndicatorColumn2)
    throws NotFoundException
  {
    if ((paramDetectionResultRowIndicatorColumn1 == null) && (paramDetectionResultRowIndicatorColumn2 == null)) {
      return null;
    }
    BarcodeMetadata localBarcodeMetadata = getBarcodeMetadata(paramDetectionResultRowIndicatorColumn1, paramDetectionResultRowIndicatorColumn2);
    if (localBarcodeMetadata == null) {
      return null;
    }
    return new DetectionResult(localBarcodeMetadata, BoundingBox.merge(adjustBoundingBox(paramDetectionResultRowIndicatorColumn1), adjustBoundingBox(paramDetectionResultRowIndicatorColumn2)));
  }
  
  public static String toString(BarcodeValue[][] paramArrayOfBarcodeValue)
  {
    Formatter localFormatter = new Formatter();
    for (int i = 0; i < paramArrayOfBarcodeValue.length; i++)
    {
      localFormatter.format("Row %2d: ", new Object[] { Integer.valueOf(i) });
      for (int j = 0; j < paramArrayOfBarcodeValue[i].length; j++)
      {
        BarcodeValue localBarcodeValue = paramArrayOfBarcodeValue[i][j];
        if (localBarcodeValue.getValue().length == 0) {
          localFormatter.format("        ", null);
        } else {
          localFormatter.format("%4d(%2d)", new Object[] { Integer.valueOf(localBarcodeValue.getValue()[0]), localBarcodeValue.getConfidence(localBarcodeValue.getValue()[0]) });
        }
      }
      localFormatter.format("%n", new Object[0]);
    }
    paramArrayOfBarcodeValue = localFormatter.toString();
    localFormatter.close();
    return paramArrayOfBarcodeValue;
  }
  
  private static void verifyCodewordCount(int[] paramArrayOfInt, int paramInt)
    throws FormatException
  {
    if (paramArrayOfInt.length >= 4)
    {
      int i = paramArrayOfInt[0];
      if (i <= paramArrayOfInt.length)
      {
        if (i == 0)
        {
          if (paramInt < paramArrayOfInt.length)
          {
            paramArrayOfInt[0] = (paramArrayOfInt.length - paramInt);
            return;
          }
          throw FormatException.getFormatInstance();
        }
        return;
      }
      throw FormatException.getFormatInstance();
    }
    throw FormatException.getFormatInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/PDF417ScanningDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */