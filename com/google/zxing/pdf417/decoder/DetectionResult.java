package com.google.zxing.pdf417.decoder;

import java.util.Formatter;

final class DetectionResult
{
  private static final int ADJUST_ROW_NUMBER_SKIP = 2;
  private final int barcodeColumnCount;
  private final BarcodeMetadata barcodeMetadata;
  private BoundingBox boundingBox;
  private final DetectionResultColumn[] detectionResultColumns;
  
  DetectionResult(BarcodeMetadata paramBarcodeMetadata, BoundingBox paramBoundingBox)
  {
    this.barcodeMetadata = paramBarcodeMetadata;
    this.barcodeColumnCount = paramBarcodeMetadata.getColumnCount();
    this.boundingBox = paramBoundingBox;
    this.detectionResultColumns = new DetectionResultColumn[this.barcodeColumnCount + 2];
  }
  
  private void adjustIndicatorColumnRowNumbers(DetectionResultColumn paramDetectionResultColumn)
  {
    if (paramDetectionResultColumn != null) {
      ((DetectionResultRowIndicatorColumn)paramDetectionResultColumn).adjustCompleteIndicatorColumnRowNumbers(this.barcodeMetadata);
    }
  }
  
  private static boolean adjustRowNumber(Codeword paramCodeword1, Codeword paramCodeword2)
  {
    if (paramCodeword2 == null) {
      return false;
    }
    if ((paramCodeword2.hasValidRowNumber()) && (paramCodeword2.getBucket() == paramCodeword1.getBucket()))
    {
      paramCodeword1.setRowNumber(paramCodeword2.getRowNumber());
      return true;
    }
    return false;
  }
  
  private static int adjustRowNumberIfValid(int paramInt1, int paramInt2, Codeword paramCodeword)
  {
    if (paramCodeword == null) {
      return paramInt2;
    }
    int i = paramInt2;
    if (!paramCodeword.hasValidRowNumber()) {
      if (paramCodeword.isValidRowNumber(paramInt1))
      {
        paramCodeword.setRowNumber(paramInt1);
        i = 0;
      }
      else
      {
        i = paramInt2 + 1;
      }
    }
    return i;
  }
  
  private int adjustRowNumbers()
  {
    int k = adjustRowNumbersByRow();
    if (k == 0) {
      return 0;
    }
    for (int i = 1; i < this.barcodeColumnCount + 1; i++)
    {
      Codeword[] arrayOfCodeword = this.detectionResultColumns[i].getCodewords();
      for (int j = 0; j < arrayOfCodeword.length; j++) {
        if ((arrayOfCodeword[j] != null) && (!arrayOfCodeword[j].hasValidRowNumber())) {
          adjustRowNumbers(i, j, arrayOfCodeword);
        }
      }
    }
    return k;
  }
  
  private void adjustRowNumbers(int paramInt1, int paramInt2, Codeword[] paramArrayOfCodeword)
  {
    Codeword localCodeword = paramArrayOfCodeword[paramInt2];
    Codeword[] arrayOfCodeword1 = this.detectionResultColumns[(paramInt1 - 1)].getCodewords();
    Object localObject = this.detectionResultColumns;
    paramInt1++;
    if (localObject[paramInt1] != null) {
      localObject = localObject[paramInt1].getCodewords();
    } else {
      localObject = arrayOfCodeword1;
    }
    Codeword[] arrayOfCodeword2 = new Codeword[14];
    arrayOfCodeword2[2] = arrayOfCodeword1[paramInt2];
    arrayOfCodeword2[3] = localObject[paramInt2];
    int i = 0;
    if (paramInt2 > 0)
    {
      paramInt1 = paramInt2 - 1;
      arrayOfCodeword2[0] = paramArrayOfCodeword[paramInt1];
      arrayOfCodeword2[4] = arrayOfCodeword1[paramInt1];
      arrayOfCodeword2[5] = localObject[paramInt1];
    }
    if (paramInt2 > 1)
    {
      paramInt1 = paramInt2 - 2;
      arrayOfCodeword2[8] = paramArrayOfCodeword[paramInt1];
      arrayOfCodeword2[10] = arrayOfCodeword1[paramInt1];
      arrayOfCodeword2[11] = localObject[paramInt1];
    }
    if (paramInt2 < paramArrayOfCodeword.length - 1)
    {
      paramInt1 = paramInt2 + 1;
      arrayOfCodeword2[1] = paramArrayOfCodeword[paramInt1];
      arrayOfCodeword2[6] = arrayOfCodeword1[paramInt1];
      arrayOfCodeword2[7] = localObject[paramInt1];
    }
    paramInt1 = i;
    if (paramInt2 < paramArrayOfCodeword.length - 2)
    {
      paramInt1 = paramInt2 + 2;
      arrayOfCodeword2[9] = paramArrayOfCodeword[paramInt1];
      arrayOfCodeword2[12] = arrayOfCodeword1[paramInt1];
      arrayOfCodeword2[13] = localObject[paramInt1];
    }
    for (paramInt1 = i; paramInt1 < 14; paramInt1++) {
      if (adjustRowNumber(localCodeword, arrayOfCodeword2[paramInt1])) {
        return;
      }
    }
  }
  
  private int adjustRowNumbersByRow()
  {
    adjustRowNumbersFromBothRI();
    return adjustRowNumbersFromLRI() + adjustRowNumbersFromRRI();
  }
  
  private void adjustRowNumbersFromBothRI()
  {
    Object localObject = this.detectionResultColumns;
    int i = 0;
    if ((localObject[0] != null) && (localObject[(this.barcodeColumnCount + 1)] != null))
    {
      Codeword[] arrayOfCodeword1 = localObject[0].getCodewords();
      Codeword[] arrayOfCodeword2 = this.detectionResultColumns[(this.barcodeColumnCount + 1)].getCodewords();
      while (i < arrayOfCodeword1.length)
      {
        if ((arrayOfCodeword1[i] != null) && (arrayOfCodeword2[i] != null) && (arrayOfCodeword1[i].getRowNumber() == arrayOfCodeword2[i].getRowNumber())) {
          for (int j = 1; j <= this.barcodeColumnCount; j++)
          {
            localObject = this.detectionResultColumns[j].getCodewords()[i];
            if (localObject != null)
            {
              ((Codeword)localObject).setRowNumber(arrayOfCodeword1[i].getRowNumber());
              if (!((Codeword)localObject).hasValidRowNumber()) {
                this.detectionResultColumns[j].getCodewords()[i] = null;
              }
            }
          }
        }
        i++;
      }
      return;
    }
  }
  
  private int adjustRowNumbersFromLRI()
  {
    Object localObject = this.detectionResultColumns;
    if (localObject[0] == null) {
      return 0;
    }
    localObject = localObject[0].getCodewords();
    int j = 0;
    int k;
    for (int i = 0; j < localObject.length; i = k)
    {
      k = i;
      if (localObject[j] != null)
      {
        int i2 = localObject[j].getRowNumber();
        k = 1;
        int i1 = 0;
        while ((k < this.barcodeColumnCount + 1) && (i1 < 2))
        {
          Codeword localCodeword = this.detectionResultColumns[k].getCodewords()[j];
          int n = i1;
          int m = i;
          if (localCodeword != null)
          {
            i1 = adjustRowNumberIfValid(i2, i1, localCodeword);
            n = i1;
            m = i;
            if (!localCodeword.hasValidRowNumber())
            {
              m = i + 1;
              n = i1;
            }
          }
          k++;
          i1 = n;
          i = m;
        }
        k = i;
      }
      j++;
    }
    return i;
  }
  
  private int adjustRowNumbersFromRRI()
  {
    Object localObject = this.detectionResultColumns;
    int i = this.barcodeColumnCount;
    if (localObject[(i + 1)] == null) {
      return 0;
    }
    localObject = localObject[(i + 1)].getCodewords();
    int j = 0;
    int k;
    for (i = 0; j < localObject.length; i = k)
    {
      k = i;
      if (localObject[j] != null)
      {
        int i2 = localObject[j].getRowNumber();
        k = this.barcodeColumnCount + 1;
        int i1 = 0;
        while ((k > 0) && (i1 < 2))
        {
          Codeword localCodeword = this.detectionResultColumns[k].getCodewords()[j];
          int n = i1;
          int m = i;
          if (localCodeword != null)
          {
            i1 = adjustRowNumberIfValid(i2, i1, localCodeword);
            n = i1;
            m = i;
            if (!localCodeword.hasValidRowNumber())
            {
              m = i + 1;
              n = i1;
            }
          }
          k--;
          i1 = n;
          i = m;
        }
        k = i;
      }
      j++;
    }
    return i;
  }
  
  int getBarcodeColumnCount()
  {
    return this.barcodeColumnCount;
  }
  
  int getBarcodeECLevel()
  {
    return this.barcodeMetadata.getErrorCorrectionLevel();
  }
  
  int getBarcodeRowCount()
  {
    return this.barcodeMetadata.getRowCount();
  }
  
  BoundingBox getBoundingBox()
  {
    return this.boundingBox;
  }
  
  DetectionResultColumn getDetectionResultColumn(int paramInt)
  {
    return this.detectionResultColumns[paramInt];
  }
  
  DetectionResultColumn[] getDetectionResultColumns()
  {
    adjustIndicatorColumnRowNumbers(this.detectionResultColumns[0]);
    adjustIndicatorColumnRowNumbers(this.detectionResultColumns[(this.barcodeColumnCount + 1)]);
    int j;
    for (int i = 928;; i = j)
    {
      j = adjustRowNumbers();
      if ((j <= 0) || (j >= i)) {
        break;
      }
    }
    return this.detectionResultColumns;
  }
  
  public void setBoundingBox(BoundingBox paramBoundingBox)
  {
    this.boundingBox = paramBoundingBox;
  }
  
  void setDetectionResultColumn(int paramInt, DetectionResultColumn paramDetectionResultColumn)
  {
    this.detectionResultColumns[paramInt] = paramDetectionResultColumn;
  }
  
  public String toString()
  {
    Object localObject2 = this.detectionResultColumns;
    Formatter localFormatter = localObject2[0];
    Object localObject1 = localFormatter;
    if (localFormatter == null) {
      localObject1 = localObject2[(this.barcodeColumnCount + 1)];
    }
    localFormatter = new Formatter();
    for (int i = 0; i < ((DetectionResultColumn)localObject1).getCodewords().length; i++)
    {
      localFormatter.format("CW %3d:", new Object[] { Integer.valueOf(i) });
      for (int j = 0; j < this.barcodeColumnCount + 2; j++)
      {
        localObject2 = this.detectionResultColumns;
        if (localObject2[j] == null)
        {
          localFormatter.format("    |   ", new Object[0]);
        }
        else
        {
          localObject2 = localObject2[j].getCodewords()[i];
          if (localObject2 == null) {
            localFormatter.format("    |   ", new Object[0]);
          } else {
            localFormatter.format(" %3d|%3d", new Object[] { Integer.valueOf(((Codeword)localObject2).getRowNumber()), Integer.valueOf(((Codeword)localObject2).getValue()) });
          }
        }
      }
      localFormatter.format("%n", new Object[0]);
    }
    localObject1 = localFormatter.toString();
    localFormatter.close();
    return (String)localObject1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/DetectionResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */