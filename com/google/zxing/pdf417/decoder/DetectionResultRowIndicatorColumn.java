package com.google.zxing.pdf417.decoder;

import com.google.zxing.ResultPoint;

final class DetectionResultRowIndicatorColumn
  extends DetectionResultColumn
{
  private final boolean isLeft;
  
  DetectionResultRowIndicatorColumn(BoundingBox paramBoundingBox, boolean paramBoolean)
  {
    super(paramBoundingBox);
    this.isLeft = paramBoolean;
  }
  
  private void adjustIncompleteIndicatorColumnRowNumbers(BarcodeMetadata paramBarcodeMetadata)
  {
    Object localObject = getBoundingBox();
    ResultPoint localResultPoint;
    if (this.isLeft) {
      localResultPoint = ((BoundingBox)localObject).getTopLeft();
    } else {
      localResultPoint = ((BoundingBox)localObject).getTopRight();
    }
    if (this.isLeft) {
      localObject = ((BoundingBox)localObject).getBottomLeft();
    } else {
      localObject = ((BoundingBox)localObject).getBottomRight();
    }
    int k = imageRowToCodewordIndex((int)localResultPoint.getY());
    int i3 = imageRowToCodewordIndex((int)((ResultPoint)localObject).getY());
    localObject = getCodewords();
    int i2 = -1;
    int n = 0;
    int i1;
    for (int m = 1; k < i3; m = i1)
    {
      int j = i2;
      int i = n;
      i1 = m;
      if (localObject[k] != null)
      {
        localResultPoint = localObject[k];
        localResultPoint.setRowNumberAsRowIndicatorColumn();
        i = localResultPoint.getRowNumber() - i2;
        if (i == 0)
        {
          i = n + 1;
          j = i2;
          i1 = m;
        }
        else if (i == 1)
        {
          i1 = Math.max(m, n);
          j = localResultPoint.getRowNumber();
          i = 1;
        }
        else if (localResultPoint.getRowNumber() >= paramBarcodeMetadata.getRowCount())
        {
          localObject[k] = null;
          j = i2;
          i = n;
          i1 = m;
        }
        else
        {
          j = localResultPoint.getRowNumber();
          i = 1;
          i1 = m;
        }
      }
      k++;
      i2 = j;
      n = i;
    }
  }
  
  private void removeIncorrectCodewords(Codeword[] paramArrayOfCodeword, BarcodeMetadata paramBarcodeMetadata)
  {
    for (int i = 0; i < paramArrayOfCodeword.length; i++)
    {
      Codeword localCodeword = paramArrayOfCodeword[i];
      if (paramArrayOfCodeword[i] != null)
      {
        int m = localCodeword.getValue() % 30;
        int k = localCodeword.getRowNumber();
        if (k > paramBarcodeMetadata.getRowCount())
        {
          paramArrayOfCodeword[i] = null;
        }
        else
        {
          int j = k;
          if (!this.isLeft) {
            j = k + 2;
          }
          switch (j % 3)
          {
          default: 
            break;
          case 2: 
            if (m + 1 != paramBarcodeMetadata.getColumnCount()) {
              paramArrayOfCodeword[i] = null;
            }
            break;
          case 1: 
            if ((m / 3 != paramBarcodeMetadata.getErrorCorrectionLevel()) || (m % 3 != paramBarcodeMetadata.getRowCountLowerPart())) {
              paramArrayOfCodeword[i] = null;
            }
            break;
          case 0: 
            if (m * 3 + 1 != paramBarcodeMetadata.getRowCountUpperPart()) {
              paramArrayOfCodeword[i] = null;
            }
            break;
          }
        }
      }
    }
  }
  
  private void setRowNumbers()
  {
    for (Codeword localCodeword : getCodewords()) {
      if (localCodeword != null) {
        localCodeword.setRowNumberAsRowIndicatorColumn();
      }
    }
  }
  
  void adjustCompleteIndicatorColumnRowNumbers(BarcodeMetadata paramBarcodeMetadata)
  {
    Codeword[] arrayOfCodeword = getCodewords();
    setRowNumbers();
    removeIncorrectCodewords(arrayOfCodeword, paramBarcodeMetadata);
    Object localObject2 = getBoundingBox();
    Object localObject1;
    if (this.isLeft) {
      localObject1 = ((BoundingBox)localObject2).getTopLeft();
    } else {
      localObject1 = ((BoundingBox)localObject2).getTopRight();
    }
    if (this.isLeft) {
      localObject2 = ((BoundingBox)localObject2).getBottomLeft();
    } else {
      localObject2 = ((BoundingBox)localObject2).getBottomRight();
    }
    int k = imageRowToCodewordIndex((int)((ResultPoint)localObject1).getY());
    int i3 = imageRowToCodewordIndex((int)((ResultPoint)localObject2).getY());
    int n = -1;
    int i1 = 0;
    int i2;
    for (int m = 1; k < i3; m = i2)
    {
      int j = n;
      int i = i1;
      i2 = m;
      if (arrayOfCodeword[k] != null)
      {
        localObject1 = arrayOfCodeword[k];
        i = ((Codeword)localObject1).getRowNumber() - n;
        if (i == 0)
        {
          i = i1 + 1;
          j = n;
          i2 = m;
        }
        else if (i == 1)
        {
          i2 = Math.max(m, i1);
          j = ((Codeword)localObject1).getRowNumber();
          i = 1;
        }
        else if ((i >= 0) && (((Codeword)localObject1).getRowNumber() < paramBarcodeMetadata.getRowCount()) && (i <= k))
        {
          j = i;
          if (m > 2) {
            j = i * (m - 2);
          }
          if (j >= k) {
            i = 1;
          } else {
            i = 0;
          }
          for (i2 = 1; (i2 <= j) && (i == 0); i2++) {
            if (arrayOfCodeword[(k - i2)] != null) {
              i = 1;
            } else {
              i = 0;
            }
          }
          if (i != 0)
          {
            arrayOfCodeword[k] = null;
            j = n;
            i = i1;
            i2 = m;
          }
          else
          {
            j = ((Codeword)localObject1).getRowNumber();
            i = 1;
            i2 = m;
          }
        }
        else
        {
          arrayOfCodeword[k] = null;
          i2 = m;
          i = i1;
          j = n;
        }
      }
      k++;
      n = j;
      i1 = i;
    }
  }
  
  BarcodeMetadata getBarcodeMetadata()
  {
    Codeword[] arrayOfCodeword = getCodewords();
    BarcodeValue localBarcodeValue3 = new BarcodeValue();
    BarcodeValue localBarcodeValue2 = new BarcodeValue();
    BarcodeValue localBarcodeValue1 = new BarcodeValue();
    Object localObject = new BarcodeValue();
    int m = arrayOfCodeword.length;
    for (int i = 0; i < m; i++)
    {
      Codeword localCodeword = arrayOfCodeword[i];
      if (localCodeword != null)
      {
        localCodeword.setRowNumberAsRowIndicatorColumn();
        int n = localCodeword.getValue() % 30;
        int k = localCodeword.getRowNumber();
        int j = k;
        if (!this.isLeft) {
          j = k + 2;
        }
        switch (j % 3)
        {
        default: 
          break;
        case 2: 
          localBarcodeValue3.setValue(n + 1);
          break;
        case 1: 
          ((BarcodeValue)localObject).setValue(n / 3);
          localBarcodeValue1.setValue(n % 3);
          break;
        case 0: 
          localBarcodeValue2.setValue(n * 3 + 1);
        }
      }
    }
    if ((localBarcodeValue3.getValue().length != 0) && (localBarcodeValue2.getValue().length != 0) && (localBarcodeValue1.getValue().length != 0) && (((BarcodeValue)localObject).getValue().length != 0) && (localBarcodeValue3.getValue()[0] > 0) && (localBarcodeValue2.getValue()[0] + localBarcodeValue1.getValue()[0] >= 3) && (localBarcodeValue2.getValue()[0] + localBarcodeValue1.getValue()[0] <= 90))
    {
      localObject = new BarcodeMetadata(localBarcodeValue3.getValue()[0], localBarcodeValue2.getValue()[0], localBarcodeValue1.getValue()[0], localObject.getValue()[0]);
      removeIncorrectCodewords(arrayOfCodeword, (BarcodeMetadata)localObject);
      return (BarcodeMetadata)localObject;
    }
    return null;
  }
  
  int[] getRowHeights()
  {
    Object localObject1 = getBarcodeMetadata();
    if (localObject1 == null) {
      return null;
    }
    adjustIncompleteIndicatorColumnRowNumbers((BarcodeMetadata)localObject1);
    int[] arrayOfInt = new int[((BarcodeMetadata)localObject1).getRowCount()];
    for (Object localObject2 : getCodewords()) {
      if (localObject2 != null)
      {
        int k = ((Codeword)localObject2).getRowNumber();
        if (k < arrayOfInt.length) {
          arrayOfInt[k] += 1;
        }
      }
    }
    return arrayOfInt;
  }
  
  boolean isLeft()
  {
    return this.isLeft;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("IsLeft: ");
    localStringBuilder.append(this.isLeft);
    localStringBuilder.append('\n');
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/decoder/DetectionResultRowIndicatorColumn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */