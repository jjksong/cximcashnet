package com.google.zxing.pdf417.detector;

import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
import java.util.List;

public final class PDF417DetectorResult
{
  private final BitMatrix bits;
  private final List<ResultPoint[]> points;
  
  public PDF417DetectorResult(BitMatrix paramBitMatrix, List<ResultPoint[]> paramList)
  {
    this.bits = paramBitMatrix;
    this.points = paramList;
  }
  
  public BitMatrix getBits()
  {
    return this.bits;
  }
  
  public List<ResultPoint[]> getPoints()
  {
    return this.points;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/pdf417/detector/PDF417DetectorResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */