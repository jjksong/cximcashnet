package com.google.zxing.qrcode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;
import java.util.Map;

public final class QRCodeWriter
  implements Writer
{
  private static final int QUIET_ZONE_SIZE = 4;
  
  private static BitMatrix renderResult(QRCode paramQRCode, int paramInt1, int paramInt2, int paramInt3)
  {
    paramQRCode = paramQRCode.getMatrix();
    if (paramQRCode != null)
    {
      int m = paramQRCode.getWidth();
      int k = paramQRCode.getHeight();
      paramInt3 <<= 1;
      int i = m + paramInt3;
      int j = paramInt3 + k;
      paramInt1 = Math.max(paramInt1, i);
      paramInt3 = Math.max(paramInt2, j);
      int n = Math.min(paramInt1 / i, paramInt3 / j);
      j = (paramInt1 - m * n) / 2;
      paramInt2 = (paramInt3 - k * n) / 2;
      BitMatrix localBitMatrix = new BitMatrix(paramInt1, paramInt3);
      paramInt1 = 0;
      while (paramInt1 < k)
      {
        paramInt3 = j;
        i = 0;
        while (i < m)
        {
          if (paramQRCode.get(i, paramInt1) == 1) {
            localBitMatrix.setRegion(paramInt3, paramInt2, n, n);
          }
          i++;
          paramInt3 += n;
        }
        paramInt1++;
        paramInt2 += n;
      }
      return localBitMatrix;
    }
    throw new IllegalStateException();
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
    throws WriterException
  {
    return encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, null);
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (!paramString.isEmpty())
    {
      if (paramBarcodeFormat == BarcodeFormat.QR_CODE)
      {
        if ((paramInt1 >= 0) && (paramInt2 >= 0))
        {
          paramBarcodeFormat = ErrorCorrectionLevel.L;
          int j = 4;
          int i = j;
          BarcodeFormat localBarcodeFormat = paramBarcodeFormat;
          if (paramMap != null)
          {
            if (paramMap.containsKey(EncodeHintType.ERROR_CORRECTION)) {
              paramBarcodeFormat = ErrorCorrectionLevel.valueOf(paramMap.get(EncodeHintType.ERROR_CORRECTION).toString());
            }
            i = j;
            localBarcodeFormat = paramBarcodeFormat;
            if (paramMap.containsKey(EncodeHintType.MARGIN))
            {
              i = Integer.parseInt(paramMap.get(EncodeHintType.MARGIN).toString());
              localBarcodeFormat = paramBarcodeFormat;
            }
          }
          return renderResult(Encoder.encode(paramString, localBarcodeFormat, paramMap), paramInt1, paramInt2, i);
        }
        paramString = new StringBuilder("Requested dimensions are too small: ");
        paramString.append(paramInt1);
        paramString.append('x');
        paramString.append(paramInt2);
        throw new IllegalArgumentException(paramString.toString());
      }
      paramString = new StringBuilder("Can only encode QR_CODE, but got ");
      paramString.append(paramBarcodeFormat);
      throw new IllegalArgumentException(paramString.toString());
    }
    throw new IllegalArgumentException("Found empty contents");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/QRCodeWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */