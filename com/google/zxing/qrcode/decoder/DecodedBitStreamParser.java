package com.google.zxing.qrcode.decoder;

import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.common.BitSource;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.common.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;

final class DecodedBitStreamParser
{
  private static final char[] ALPHANUMERIC_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();
  private static final int GB2312_SUBSET = 1;
  
  /* Error */
  static com.google.zxing.common.DecoderResult decode(byte[] paramArrayOfByte, Version paramVersion, ErrorCorrectionLevel paramErrorCorrectionLevel, Map<DecodeHintType, ?> paramMap)
    throws FormatException
  {
    // Byte code:
    //   0: new 33	com/google/zxing/common/BitSource
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 36	com/google/zxing/common/BitSource:<init>	([B)V
    //   8: astore 12
    //   10: new 38	java/lang/StringBuilder
    //   13: dup
    //   14: bipush 50
    //   16: invokespecial 41	java/lang/StringBuilder:<init>	(I)V
    //   19: astore 13
    //   21: new 43	java/util/ArrayList
    //   24: dup
    //   25: iconst_1
    //   26: invokespecial 44	java/util/ArrayList:<init>	(I)V
    //   29: astore 11
    //   31: aconst_null
    //   32: astore 10
    //   34: iconst_0
    //   35: istore 8
    //   37: iconst_m1
    //   38: istore 5
    //   40: iconst_m1
    //   41: istore 4
    //   43: aload 12
    //   45: invokevirtual 48	com/google/zxing/common/BitSource:available	()I
    //   48: iconst_4
    //   49: if_icmpge +11 -> 60
    //   52: getstatic 54	com/google/zxing/qrcode/decoder/Mode:TERMINATOR	Lcom/google/zxing/qrcode/decoder/Mode;
    //   55: astore 9
    //   57: goto +14 -> 71
    //   60: aload 12
    //   62: iconst_4
    //   63: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   66: invokestatic 62	com/google/zxing/qrcode/decoder/Mode:forBits	(I)Lcom/google/zxing/qrcode/decoder/Mode;
    //   69: astore 9
    //   71: aload 9
    //   73: getstatic 54	com/google/zxing/qrcode/decoder/Mode:TERMINATOR	Lcom/google/zxing/qrcode/decoder/Mode;
    //   76: if_acmpeq +252 -> 328
    //   79: aload 9
    //   81: getstatic 65	com/google/zxing/qrcode/decoder/Mode:FNC1_FIRST_POSITION	Lcom/google/zxing/qrcode/decoder/Mode;
    //   84: if_acmpeq +238 -> 322
    //   87: aload 9
    //   89: getstatic 68	com/google/zxing/qrcode/decoder/Mode:FNC1_SECOND_POSITION	Lcom/google/zxing/qrcode/decoder/Mode;
    //   92: if_acmpne +6 -> 98
    //   95: goto +227 -> 322
    //   98: aload 9
    //   100: getstatic 71	com/google/zxing/qrcode/decoder/Mode:STRUCTURED_APPEND	Lcom/google/zxing/qrcode/decoder/Mode;
    //   103: if_acmpne +38 -> 141
    //   106: aload 12
    //   108: invokevirtual 48	com/google/zxing/common/BitSource:available	()I
    //   111: bipush 16
    //   113: if_icmplt +24 -> 137
    //   116: aload 12
    //   118: bipush 8
    //   120: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   123: istore 5
    //   125: aload 12
    //   127: bipush 8
    //   129: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   132: istore 4
    //   134: goto +194 -> 328
    //   137: invokestatic 75	com/google/zxing/FormatException:getFormatInstance	()Lcom/google/zxing/FormatException;
    //   140: athrow
    //   141: aload 9
    //   143: getstatic 78	com/google/zxing/qrcode/decoder/Mode:ECI	Lcom/google/zxing/qrcode/decoder/Mode;
    //   146: if_acmpne +25 -> 171
    //   149: aload 12
    //   151: invokestatic 82	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:parseECIValue	(Lcom/google/zxing/common/BitSource;)I
    //   154: invokestatic 88	com/google/zxing/common/CharacterSetECI:getCharacterSetECIByValue	(I)Lcom/google/zxing/common/CharacterSetECI;
    //   157: astore 10
    //   159: aload 10
    //   161: ifnull +6 -> 167
    //   164: goto +164 -> 328
    //   167: invokestatic 75	com/google/zxing/FormatException:getFormatInstance	()Lcom/google/zxing/FormatException;
    //   170: athrow
    //   171: aload 9
    //   173: getstatic 91	com/google/zxing/qrcode/decoder/Mode:HANZI	Lcom/google/zxing/qrcode/decoder/Mode;
    //   176: if_acmpne +42 -> 218
    //   179: aload 12
    //   181: iconst_4
    //   182: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   185: istore 7
    //   187: aload 12
    //   189: aload 9
    //   191: aload_1
    //   192: invokevirtual 95	com/google/zxing/qrcode/decoder/Mode:getCharacterCountBits	(Lcom/google/zxing/qrcode/decoder/Version;)I
    //   195: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   198: istore 6
    //   200: iload 7
    //   202: iconst_1
    //   203: if_icmpne +12 -> 215
    //   206: aload 12
    //   208: aload 13
    //   210: iload 6
    //   212: invokestatic 99	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:decodeHanziSegment	(Lcom/google/zxing/common/BitSource;Ljava/lang/StringBuilder;I)V
    //   215: goto +113 -> 328
    //   218: aload 12
    //   220: aload 9
    //   222: aload_1
    //   223: invokevirtual 95	com/google/zxing/qrcode/decoder/Mode:getCharacterCountBits	(Lcom/google/zxing/qrcode/decoder/Version;)I
    //   226: invokevirtual 58	com/google/zxing/common/BitSource:readBits	(I)I
    //   229: istore 6
    //   231: aload 9
    //   233: getstatic 102	com/google/zxing/qrcode/decoder/Mode:NUMERIC	Lcom/google/zxing/qrcode/decoder/Mode;
    //   236: if_acmpne +15 -> 251
    //   239: aload 12
    //   241: aload 13
    //   243: iload 6
    //   245: invokestatic 105	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:decodeNumericSegment	(Lcom/google/zxing/common/BitSource;Ljava/lang/StringBuilder;I)V
    //   248: goto +80 -> 328
    //   251: aload 9
    //   253: getstatic 108	com/google/zxing/qrcode/decoder/Mode:ALPHANUMERIC	Lcom/google/zxing/qrcode/decoder/Mode;
    //   256: if_acmpne +17 -> 273
    //   259: aload 12
    //   261: aload 13
    //   263: iload 6
    //   265: iload 8
    //   267: invokestatic 112	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:decodeAlphanumericSegment	(Lcom/google/zxing/common/BitSource;Ljava/lang/StringBuilder;IZ)V
    //   270: goto +58 -> 328
    //   273: aload 9
    //   275: getstatic 115	com/google/zxing/qrcode/decoder/Mode:BYTE	Lcom/google/zxing/qrcode/decoder/Mode;
    //   278: if_acmpne +20 -> 298
    //   281: aload 12
    //   283: aload 13
    //   285: iload 6
    //   287: aload 10
    //   289: aload 11
    //   291: aload_3
    //   292: invokestatic 119	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:decodeByteSegment	(Lcom/google/zxing/common/BitSource;Ljava/lang/StringBuilder;ILcom/google/zxing/common/CharacterSetECI;Ljava/util/Collection;Ljava/util/Map;)V
    //   295: goto +33 -> 328
    //   298: aload 9
    //   300: getstatic 122	com/google/zxing/qrcode/decoder/Mode:KANJI	Lcom/google/zxing/qrcode/decoder/Mode;
    //   303: if_acmpne +15 -> 318
    //   306: aload 12
    //   308: aload 13
    //   310: iload 6
    //   312: invokestatic 125	com/google/zxing/qrcode/decoder/DecodedBitStreamParser:decodeKanjiSegment	(Lcom/google/zxing/common/BitSource;Ljava/lang/StringBuilder;I)V
    //   315: goto +13 -> 328
    //   318: invokestatic 75	com/google/zxing/FormatException:getFormatInstance	()Lcom/google/zxing/FormatException;
    //   321: athrow
    //   322: iconst_1
    //   323: istore 8
    //   325: goto +3 -> 328
    //   328: getstatic 54	com/google/zxing/qrcode/decoder/Mode:TERMINATOR	Lcom/google/zxing/qrcode/decoder/Mode;
    //   331: astore 14
    //   333: aload 9
    //   335: aload 14
    //   337: if_acmpne +57 -> 394
    //   340: aload 13
    //   342: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   345: astore_3
    //   346: aload 11
    //   348: invokeinterface 135 1 0
    //   353: ifeq +8 -> 361
    //   356: aconst_null
    //   357: astore_1
    //   358: goto +6 -> 364
    //   361: aload 11
    //   363: astore_1
    //   364: aload_2
    //   365: ifnonnull +8 -> 373
    //   368: aconst_null
    //   369: astore_2
    //   370: goto +8 -> 378
    //   373: aload_2
    //   374: invokevirtual 138	com/google/zxing/qrcode/decoder/ErrorCorrectionLevel:toString	()Ljava/lang/String;
    //   377: astore_2
    //   378: new 140	com/google/zxing/common/DecoderResult
    //   381: dup
    //   382: aload_0
    //   383: aload_3
    //   384: aload_1
    //   385: aload_2
    //   386: iload 5
    //   388: iload 4
    //   390: invokespecial 143	com/google/zxing/common/DecoderResult:<init>	([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;II)V
    //   393: areturn
    //   394: goto -351 -> 43
    //   397: astore_0
    //   398: invokestatic 75	com/google/zxing/FormatException:getFormatInstance	()Lcom/google/zxing/FormatException;
    //   401: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	402	0	paramArrayOfByte	byte[]
    //   0	402	1	paramVersion	Version
    //   0	402	2	paramErrorCorrectionLevel	ErrorCorrectionLevel
    //   0	402	3	paramMap	Map<DecodeHintType, ?>
    //   41	348	4	i	int
    //   38	349	5	j	int
    //   198	113	6	k	int
    //   185	19	7	m	int
    //   35	289	8	bool	boolean
    //   55	279	9	localMode1	Mode
    //   32	256	10	localCharacterSetECI	CharacterSetECI
    //   29	333	11	localArrayList	java.util.ArrayList
    //   8	299	12	localBitSource	BitSource
    //   19	322	13	localStringBuilder	StringBuilder
    //   331	5	14	localMode2	Mode
    // Exception table:
    //   from	to	target	type
    //   43	57	397	java/lang/IllegalArgumentException
    //   60	71	397	java/lang/IllegalArgumentException
    //   71	95	397	java/lang/IllegalArgumentException
    //   98	134	397	java/lang/IllegalArgumentException
    //   137	141	397	java/lang/IllegalArgumentException
    //   141	159	397	java/lang/IllegalArgumentException
    //   167	171	397	java/lang/IllegalArgumentException
    //   171	200	397	java/lang/IllegalArgumentException
    //   206	215	397	java/lang/IllegalArgumentException
    //   218	248	397	java/lang/IllegalArgumentException
    //   251	270	397	java/lang/IllegalArgumentException
    //   273	295	397	java/lang/IllegalArgumentException
    //   298	315	397	java/lang/IllegalArgumentException
    //   318	322	397	java/lang/IllegalArgumentException
    //   328	333	397	java/lang/IllegalArgumentException
  }
  
  private static void decodeAlphanumericSegment(BitSource paramBitSource, StringBuilder paramStringBuilder, int paramInt, boolean paramBoolean)
    throws FormatException
  {
    int i = paramStringBuilder.length();
    while (paramInt > 1) {
      if (paramBitSource.available() >= 11)
      {
        int j = paramBitSource.readBits(11);
        paramStringBuilder.append(toAlphaNumericChar(j / 45));
        paramStringBuilder.append(toAlphaNumericChar(j % 45));
        paramInt -= 2;
      }
      else
      {
        throw FormatException.getFormatInstance();
      }
    }
    if (paramInt == 1) {
      if (paramBitSource.available() >= 6) {
        paramStringBuilder.append(toAlphaNumericChar(paramBitSource.readBits(6)));
      } else {
        throw FormatException.getFormatInstance();
      }
    }
    if (paramBoolean) {
      for (paramInt = i; paramInt < paramStringBuilder.length(); paramInt++) {
        if (paramStringBuilder.charAt(paramInt) == '%')
        {
          if (paramInt < paramStringBuilder.length() - 1)
          {
            i = paramInt + 1;
            if (paramStringBuilder.charAt(i) == '%')
            {
              paramStringBuilder.deleteCharAt(i);
              continue;
            }
          }
          paramStringBuilder.setCharAt(paramInt, '\035');
        }
      }
    }
  }
  
  private static void decodeByteSegment(BitSource paramBitSource, StringBuilder paramStringBuilder, int paramInt, CharacterSetECI paramCharacterSetECI, Collection<byte[]> paramCollection, Map<DecodeHintType, ?> paramMap)
    throws FormatException
  {
    if (paramInt << 3 <= paramBitSource.available())
    {
      byte[] arrayOfByte = new byte[paramInt];
      for (int i = 0; i < paramInt; i++) {
        arrayOfByte[i] = ((byte)paramBitSource.readBits(8));
      }
      if (paramCharacterSetECI == null) {
        paramBitSource = StringUtils.guessEncoding(arrayOfByte, paramMap);
      } else {
        paramBitSource = paramCharacterSetECI.name();
      }
      try
      {
        paramCharacterSetECI = new java/lang/String;
        paramCharacterSetECI.<init>(arrayOfByte, paramBitSource);
        paramStringBuilder.append(paramCharacterSetECI);
        paramCollection.add(arrayOfByte);
        return;
      }
      catch (UnsupportedEncodingException paramBitSource)
      {
        throw FormatException.getFormatInstance();
      }
    }
    throw FormatException.getFormatInstance();
  }
  
  private static void decodeHanziSegment(BitSource paramBitSource, StringBuilder paramStringBuilder, int paramInt)
    throws FormatException
  {
    if (paramInt * 13 <= paramBitSource.available())
    {
      byte[] arrayOfByte = new byte[paramInt * 2];
      int i = 0;
      while (paramInt > 0)
      {
        int j = paramBitSource.readBits(13);
        j = j % 96 | j / 96 << 8;
        if (j < 959) {
          j += 41377;
        } else {
          j += 42657;
        }
        arrayOfByte[i] = ((byte)(j >> 8));
        arrayOfByte[(i + 1)] = ((byte)j);
        i += 2;
        paramInt--;
      }
      try
      {
        paramBitSource = new java/lang/String;
        paramBitSource.<init>(arrayOfByte, "GB2312");
        paramStringBuilder.append(paramBitSource);
        return;
      }
      catch (UnsupportedEncodingException paramBitSource)
      {
        throw FormatException.getFormatInstance();
      }
    }
    throw FormatException.getFormatInstance();
  }
  
  private static void decodeKanjiSegment(BitSource paramBitSource, StringBuilder paramStringBuilder, int paramInt)
    throws FormatException
  {
    if (paramInt * 13 <= paramBitSource.available())
    {
      byte[] arrayOfByte = new byte[paramInt * 2];
      int i = 0;
      while (paramInt > 0)
      {
        int j = paramBitSource.readBits(13);
        j = j % 192 | j / 192 << 8;
        if (j < 7936) {
          j += 33088;
        } else {
          j += 49472;
        }
        arrayOfByte[i] = ((byte)(j >> 8));
        arrayOfByte[(i + 1)] = ((byte)j);
        i += 2;
        paramInt--;
      }
      try
      {
        paramBitSource = new java/lang/String;
        paramBitSource.<init>(arrayOfByte, "SJIS");
        paramStringBuilder.append(paramBitSource);
        return;
      }
      catch (UnsupportedEncodingException paramBitSource)
      {
        throw FormatException.getFormatInstance();
      }
    }
    throw FormatException.getFormatInstance();
  }
  
  private static void decodeNumericSegment(BitSource paramBitSource, StringBuilder paramStringBuilder, int paramInt)
    throws FormatException
  {
    while (paramInt >= 3) {
      if (paramBitSource.available() >= 10)
      {
        int i = paramBitSource.readBits(10);
        if (i < 1000)
        {
          paramStringBuilder.append(toAlphaNumericChar(i / 100));
          paramStringBuilder.append(toAlphaNumericChar(i / 10 % 10));
          paramStringBuilder.append(toAlphaNumericChar(i % 10));
          paramInt -= 3;
        }
        else
        {
          throw FormatException.getFormatInstance();
        }
      }
      else
      {
        throw FormatException.getFormatInstance();
      }
    }
    if (paramInt == 2)
    {
      if (paramBitSource.available() >= 7)
      {
        paramInt = paramBitSource.readBits(7);
        if (paramInt < 100)
        {
          paramStringBuilder.append(toAlphaNumericChar(paramInt / 10));
          paramStringBuilder.append(toAlphaNumericChar(paramInt % 10));
          return;
        }
        throw FormatException.getFormatInstance();
      }
      throw FormatException.getFormatInstance();
    }
    if (paramInt == 1) {
      if (paramBitSource.available() >= 4)
      {
        paramInt = paramBitSource.readBits(4);
        if (paramInt < 10) {
          paramStringBuilder.append(toAlphaNumericChar(paramInt));
        } else {
          throw FormatException.getFormatInstance();
        }
      }
      else
      {
        throw FormatException.getFormatInstance();
      }
    }
  }
  
  private static int parseECIValue(BitSource paramBitSource)
    throws FormatException
  {
    int i = paramBitSource.readBits(8);
    if ((i & 0x80) == 0) {
      return i & 0x7F;
    }
    if ((i & 0xC0) == 128) {
      return paramBitSource.readBits(8) | (i & 0x3F) << 8;
    }
    if ((i & 0xE0) == 192) {
      return paramBitSource.readBits(16) | (i & 0x1F) << 16;
    }
    throw FormatException.getFormatInstance();
  }
  
  private static char toAlphaNumericChar(int paramInt)
    throws FormatException
  {
    char[] arrayOfChar = ALPHANUMERIC_CHARS;
    if (paramInt < arrayOfChar.length) {
      return arrayOfChar[paramInt];
    }
    throw FormatException.getFormatInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/DecodedBitStreamParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */