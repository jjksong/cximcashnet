package com.google.zxing.qrcode.decoder;

import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import java.util.Map;

public final class Decoder
{
  private final ReedSolomonDecoder rsDecoder = new ReedSolomonDecoder(GenericGF.QR_CODE_FIELD_256);
  
  private void correctErrors(byte[] paramArrayOfByte, int paramInt)
    throws ChecksumException
  {
    int k = paramArrayOfByte.length;
    int[] arrayOfInt = new int[k];
    int j = 0;
    for (int i = 0; i < k; i++) {
      paramArrayOfByte[i] &= 0xFF;
    }
    try
    {
      this.rsDecoder.decode(arrayOfInt, paramArrayOfByte.length - paramInt);
      for (i = j; i < paramInt; i++) {
        paramArrayOfByte[i] = ((byte)arrayOfInt[i]);
      }
      return;
    }
    catch (ReedSolomonException paramArrayOfByte)
    {
      throw ChecksumException.getChecksumInstance();
    }
  }
  
  private DecoderResult decode(BitMatrixParser paramBitMatrixParser, Map<DecodeHintType, ?> paramMap)
    throws FormatException, ChecksumException
  {
    Version localVersion = paramBitMatrixParser.readVersion();
    ErrorCorrectionLevel localErrorCorrectionLevel = paramBitMatrixParser.readFormatInformation().getErrorCorrectionLevel();
    paramBitMatrixParser = DataBlock.getDataBlocks(paramBitMatrixParser.readCodewords(), localVersion, localErrorCorrectionLevel);
    int k = paramBitMatrixParser.length;
    int j = 0;
    int i = 0;
    while (j < k)
    {
      i += paramBitMatrixParser[j].getNumDataCodewords();
      j++;
    }
    byte[] arrayOfByte1 = new byte[i];
    int m = paramBitMatrixParser.length;
    j = 0;
    i = 0;
    while (j < m)
    {
      Object localObject = paramBitMatrixParser[j];
      byte[] arrayOfByte2 = ((DataBlock)localObject).getCodewords();
      int n = ((DataBlock)localObject).getNumDataCodewords();
      correctErrors(arrayOfByte2, n);
      k = 0;
      while (k < n)
      {
        arrayOfByte1[i] = arrayOfByte2[k];
        k++;
        i++;
      }
      j++;
    }
    return DecodedBitStreamParser.decode(arrayOfByte1, localVersion, localErrorCorrectionLevel, paramMap);
  }
  
  public DecoderResult decode(BitMatrix paramBitMatrix)
    throws ChecksumException, FormatException
  {
    return decode(paramBitMatrix, null);
  }
  
  public DecoderResult decode(BitMatrix paramBitMatrix, Map<DecodeHintType, ?> paramMap)
    throws FormatException, ChecksumException
  {
    Object localObject2 = new BitMatrixParser(paramBitMatrix);
    paramBitMatrix = null;
    Object localObject1;
    try
    {
      DecoderResult localDecoderResult = decode((BitMatrixParser)localObject2, paramMap);
      return localDecoderResult;
    }
    catch (ChecksumException localChecksumException) {}catch (FormatException paramBitMatrix)
    {
      localObject1 = null;
    }
    try
    {
      ((BitMatrixParser)localObject2).remask();
      ((BitMatrixParser)localObject2).setMirror(true);
      ((BitMatrixParser)localObject2).readVersion();
      ((BitMatrixParser)localObject2).readFormatInformation();
      ((BitMatrixParser)localObject2).mirror();
      localObject2 = decode((BitMatrixParser)localObject2, paramMap);
      paramMap = new com/google/zxing/qrcode/decoder/QRCodeDecoderMetaData;
      paramMap.<init>(true);
      ((DecoderResult)localObject2).setOther(paramMap);
      return (DecoderResult)localObject2;
    }
    catch (ChecksumException paramMap) {}catch (FormatException paramMap) {}
    if (paramBitMatrix == null)
    {
      if (localObject1 != null) {
        throw ((Throwable)localObject1);
      }
      throw paramMap;
    }
    throw paramBitMatrix;
  }
  
  public DecoderResult decode(boolean[][] paramArrayOfBoolean)
    throws ChecksumException, FormatException
  {
    return decode(paramArrayOfBoolean, null);
  }
  
  public DecoderResult decode(boolean[][] paramArrayOfBoolean, Map<DecodeHintType, ?> paramMap)
    throws ChecksumException, FormatException
  {
    int k = paramArrayOfBoolean.length;
    BitMatrix localBitMatrix = new BitMatrix(k);
    for (int i = 0; i < k; i++) {
      for (int j = 0; j < k; j++) {
        if (paramArrayOfBoolean[i][j] != 0) {
          localBitMatrix.set(j, i);
        }
      }
    }
    return decode(localBitMatrix, paramMap);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/Decoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */