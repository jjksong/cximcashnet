package com.google.zxing.qrcode.decoder;

public enum ErrorCorrectionLevel
{
  private static final ErrorCorrectionLevel[] FOR_BITS;
  private final int bits;
  
  static
  {
    H = new ErrorCorrectionLevel("H", 3, 2);
    ErrorCorrectionLevel localErrorCorrectionLevel4 = L;
    ErrorCorrectionLevel localErrorCorrectionLevel2 = M;
    ErrorCorrectionLevel localErrorCorrectionLevel3 = Q;
    ErrorCorrectionLevel localErrorCorrectionLevel1 = H;
    $VALUES = new ErrorCorrectionLevel[] { localErrorCorrectionLevel4, localErrorCorrectionLevel2, localErrorCorrectionLevel3, localErrorCorrectionLevel1 };
    FOR_BITS = new ErrorCorrectionLevel[] { localErrorCorrectionLevel2, localErrorCorrectionLevel4, localErrorCorrectionLevel1, localErrorCorrectionLevel3 };
  }
  
  private ErrorCorrectionLevel(int paramInt)
  {
    this.bits = paramInt;
  }
  
  public static ErrorCorrectionLevel forBits(int paramInt)
  {
    if (paramInt >= 0)
    {
      ErrorCorrectionLevel[] arrayOfErrorCorrectionLevel = FOR_BITS;
      if (paramInt < arrayOfErrorCorrectionLevel.length) {
        return arrayOfErrorCorrectionLevel[paramInt];
      }
    }
    throw new IllegalArgumentException();
  }
  
  public int getBits()
  {
    return this.bits;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/ErrorCorrectionLevel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */