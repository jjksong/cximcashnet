package com.google.zxing.qrcode.decoder;

import com.google.zxing.common.BitMatrix;

 enum DataMask
{
  DATA_MASK_000,  DATA_MASK_001,  DATA_MASK_010,  DATA_MASK_011,  DATA_MASK_100,  DATA_MASK_101,  DATA_MASK_110,  DATA_MASK_111;
  
  private DataMask() {}
  
  abstract boolean isMasked(int paramInt1, int paramInt2);
  
  final void unmaskBitMatrix(BitMatrix paramBitMatrix, int paramInt)
  {
    for (int i = 0; i < paramInt; i++) {
      for (int j = 0; j < paramInt; j++) {
        if (isMasked(i, j)) {
          paramBitMatrix.flip(j, i);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/DataMask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */