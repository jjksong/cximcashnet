package com.google.zxing.qrcode.decoder;

final class FormatInformation
{
  private static final int[][] FORMAT_INFO_DECODE_LOOKUP;
  private static final int FORMAT_INFO_MASK_QR = 21522;
  private final byte dataMask;
  private final ErrorCorrectionLevel errorCorrectionLevel;
  
  static
  {
    int[] arrayOfInt1 = { 21522, 0 };
    int[] arrayOfInt2 = { 20773, 1 };
    int[] arrayOfInt3 = { 24188, 2 };
    int[] arrayOfInt4 = { 23371, 3 };
    int[] arrayOfInt5 = { 17913, 4 };
    int[] arrayOfInt6 = { 16590, 5 };
    int[] arrayOfInt7 = { 20375, 6 };
    int[] arrayOfInt8 = { 19104, 7 };
    int[] arrayOfInt9 = { 29427, 9 };
    int[] arrayOfInt10 = { 32170, 10 };
    int[] arrayOfInt11 = { 30877, 11 };
    int[] arrayOfInt12 = { 26159, 12 };
    int[] arrayOfInt13 = { 25368, 13 };
    int[] arrayOfInt14 = { 27713, 14 };
    int[] arrayOfInt15 = { 26998, 15 };
    int[] arrayOfInt16 = { 5054, 17 };
    int[] arrayOfInt17 = { 7399, 18 };
    int[] arrayOfInt18 = { 6608, 19 };
    int[] arrayOfInt19 = { 1890, 20 };
    int[] arrayOfInt20 = { 597, 21 };
    int[] arrayOfInt21 = { 3340, 22 };
    int[] arrayOfInt22 = { 2107, 23 };
    int[] arrayOfInt23 = { 13663, 24 };
    int[] arrayOfInt24 = { 12392, 25 };
    int[] arrayOfInt25 = { 16177, 26 };
    int[] arrayOfInt26 = { 14854, 27 };
    int[] arrayOfInt27 = { 9396, 28 };
    int[] arrayOfInt28 = { 8579, 29 };
    int[] arrayOfInt29 = { 11994, 30 };
    int[] arrayOfInt30 = { 11245, 31 };
    FORMAT_INFO_DECODE_LOOKUP = new int[][] { arrayOfInt1, arrayOfInt2, arrayOfInt3, arrayOfInt4, arrayOfInt5, arrayOfInt6, arrayOfInt7, arrayOfInt8, { 30660, 8 }, arrayOfInt9, arrayOfInt10, arrayOfInt11, arrayOfInt12, arrayOfInt13, arrayOfInt14, arrayOfInt15, { 5769, 16 }, arrayOfInt16, arrayOfInt17, arrayOfInt18, arrayOfInt19, arrayOfInt20, arrayOfInt21, arrayOfInt22, arrayOfInt23, arrayOfInt24, arrayOfInt25, arrayOfInt26, arrayOfInt27, arrayOfInt28, arrayOfInt29, arrayOfInt30 };
  }
  
  private FormatInformation(int paramInt)
  {
    this.errorCorrectionLevel = ErrorCorrectionLevel.forBits(paramInt >> 3 & 0x3);
    this.dataMask = ((byte)(paramInt & 0x7));
  }
  
  static FormatInformation decodeFormatInformation(int paramInt1, int paramInt2)
  {
    FormatInformation localFormatInformation = doDecodeFormatInformation(paramInt1, paramInt2);
    if (localFormatInformation != null) {
      return localFormatInformation;
    }
    return doDecodeFormatInformation(paramInt1 ^ 0x5412, paramInt2 ^ 0x5412);
  }
  
  private static FormatInformation doDecodeFormatInformation(int paramInt1, int paramInt2)
  {
    int[][] arrayOfInt1 = FORMAT_INFO_DECODE_LOOKUP;
    int i2 = arrayOfInt1.length;
    int n = 0;
    int m = Integer.MAX_VALUE;
    int j = 0;
    while (n < i2)
    {
      int[] arrayOfInt = arrayOfInt1[n];
      int i3 = arrayOfInt[0];
      if ((i3 != paramInt1) && (i3 != paramInt2))
      {
        int i1 = numBitsDiffering(paramInt1, i3);
        int i = m;
        int k = j;
        if (i1 < m)
        {
          k = arrayOfInt[1];
          i = i1;
        }
        m = i;
        j = k;
        if (paramInt1 != paramInt2)
        {
          i1 = numBitsDiffering(paramInt2, i3);
          m = i;
          j = k;
          if (i1 < i)
          {
            j = arrayOfInt[1];
            m = i1;
          }
        }
        n++;
      }
      else
      {
        return new FormatInformation(arrayOfInt[1]);
      }
    }
    if (m <= 3) {
      return new FormatInformation(j);
    }
    return null;
  }
  
  static int numBitsDiffering(int paramInt1, int paramInt2)
  {
    return Integer.bitCount(paramInt1 ^ paramInt2);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof FormatInformation)) {
      return false;
    }
    paramObject = (FormatInformation)paramObject;
    return (this.errorCorrectionLevel == ((FormatInformation)paramObject).errorCorrectionLevel) && (this.dataMask == ((FormatInformation)paramObject).dataMask);
  }
  
  byte getDataMask()
  {
    return this.dataMask;
  }
  
  ErrorCorrectionLevel getErrorCorrectionLevel()
  {
    return this.errorCorrectionLevel;
  }
  
  public int hashCode()
  {
    return this.errorCorrectionLevel.ordinal() << 3 | this.dataMask;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/FormatInformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */