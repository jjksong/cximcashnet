package com.google.zxing.qrcode.decoder;

final class DataBlock
{
  private final byte[] codewords;
  private final int numDataCodewords;
  
  private DataBlock(int paramInt, byte[] paramArrayOfByte)
  {
    this.numDataCodewords = paramInt;
    this.codewords = paramArrayOfByte;
  }
  
  static DataBlock[] getDataBlocks(byte[] paramArrayOfByte, Version paramVersion, ErrorCorrectionLevel paramErrorCorrectionLevel)
  {
    if (paramArrayOfByte.length == paramVersion.getTotalCodewords())
    {
      paramVersion = paramVersion.getECBlocksForLevel(paramErrorCorrectionLevel);
      paramErrorCorrectionLevel = paramVersion.getECBlocks();
      int k = paramErrorCorrectionLevel.length;
      int j = 0;
      int i = 0;
      while (j < k)
      {
        i += paramErrorCorrectionLevel[j].getCount();
        j++;
      }
      DataBlock[] arrayOfDataBlock = new DataBlock[i];
      int m = paramErrorCorrectionLevel.length;
      k = 0;
      for (j = 0; k < m; j = i)
      {
        Object localObject = paramErrorCorrectionLevel[k];
        i = j;
        j = 0;
        while (j < ((Version.ECB)localObject).getCount())
        {
          n = ((Version.ECB)localObject).getDataCodewords();
          arrayOfDataBlock[i] = new DataBlock(n, new byte[paramVersion.getECCodewordsPerBlock() + n]);
          j++;
          i++;
        }
        k++;
      }
      k = arrayOfDataBlock[0].codewords.length;
      for (i = arrayOfDataBlock.length - 1; (i >= 0) && (arrayOfDataBlock[i].codewords.length != k); i--) {}
      int i1 = i + 1;
      int n = k - paramVersion.getECCodewordsPerBlock();
      k = 0;
      i = 0;
      while (k < n)
      {
        m = 0;
        while (m < j)
        {
          arrayOfDataBlock[m].codewords[k] = paramArrayOfByte[i];
          m++;
          i++;
        }
        k++;
      }
      m = i1;
      for (k = i; m < j; k++)
      {
        arrayOfDataBlock[m].codewords[n] = paramArrayOfByte[k];
        m++;
      }
      int i2 = arrayOfDataBlock[0].codewords.length;
      for (i = n; i < i2; i++)
      {
        m = 0;
        while (m < j)
        {
          if (m < i1) {
            n = i;
          } else {
            n = i + 1;
          }
          arrayOfDataBlock[m].codewords[n] = paramArrayOfByte[k];
          m++;
          k++;
        }
      }
      return arrayOfDataBlock;
    }
    throw new IllegalArgumentException();
  }
  
  byte[] getCodewords()
  {
    return this.codewords;
  }
  
  int getNumDataCodewords()
  {
    return this.numDataCodewords;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/DataBlock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */