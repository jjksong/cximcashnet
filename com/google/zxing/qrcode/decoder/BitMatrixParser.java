package com.google.zxing.qrcode.decoder;

import com.google.zxing.FormatException;
import com.google.zxing.common.BitMatrix;

final class BitMatrixParser
{
  private final BitMatrix bitMatrix;
  private boolean mirror;
  private FormatInformation parsedFormatInfo;
  private Version parsedVersion;
  
  BitMatrixParser(BitMatrix paramBitMatrix)
    throws FormatException
  {
    int i = paramBitMatrix.getHeight();
    if ((i >= 21) && ((i & 0x3) == 1))
    {
      this.bitMatrix = paramBitMatrix;
      return;
    }
    throw FormatException.getFormatInstance();
  }
  
  private int copyBit(int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool;
    if (this.mirror) {
      bool = this.bitMatrix.get(paramInt2, paramInt1);
    } else {
      bool = this.bitMatrix.get(paramInt1, paramInt2);
    }
    if (bool) {
      return paramInt3 << 1 | 0x1;
    }
    return paramInt3 << 1;
  }
  
  void mirror()
  {
    int i;
    for (int j = 0; j < this.bitMatrix.getWidth(); j = i)
    {
      i = j + 1;
      for (int k = i; k < this.bitMatrix.getHeight(); k++) {
        if (this.bitMatrix.get(j, k) != this.bitMatrix.get(k, j))
        {
          this.bitMatrix.flip(k, j);
          this.bitMatrix.flip(j, k);
        }
      }
    }
  }
  
  byte[] readCodewords()
    throws FormatException
  {
    Object localObject = readFormatInformation();
    Version localVersion = readVersion();
    localObject = DataMask.values()[localObject.getDataMask()];
    int i8 = this.bitMatrix.getHeight();
    ((DataMask)localObject).unmaskBitMatrix(this.bitMatrix, i8);
    localObject = localVersion.buildFunctionPattern();
    byte[] arrayOfByte = new byte[localVersion.getTotalCodewords()];
    int i7 = i8 - 1;
    int m = i7;
    int i = 0;
    int i2 = 1;
    int j = 0;
    int k = 0;
    while (m > 0)
    {
      int i3 = m;
      if (m == 6) {
        i3 = m - 1;
      }
      m = i;
      int n = 0;
      i = k;
      k = m;
      m = n;
      while (m < i8)
      {
        int i4;
        if (i2 != 0) {
          i4 = i7 - m;
        } else {
          i4 = m;
        }
        n = j;
        int i1 = k;
        int i5 = 0;
        j = i;
        k = n;
        i = i1;
        while (i5 < 2)
        {
          int i9 = i3 - i5;
          int i6 = i;
          i1 = k;
          n = j;
          if (!((BitMatrix)localObject).get(i9, i4))
          {
            i1 = k + 1;
            j <<= 1;
            if (this.bitMatrix.get(i9, i4)) {
              j |= 0x1;
            }
            if (i1 == 8)
            {
              arrayOfByte[i] = ((byte)j);
              i6 = i + 1;
              i1 = 0;
              n = 0;
            }
            else
            {
              n = j;
              i6 = i;
            }
          }
          i5++;
          i = i6;
          k = i1;
          j = n;
        }
        m++;
        i1 = i;
        i = k;
        n = j;
        k = i1;
        j = i;
        i = n;
      }
      i2 ^= 0x1;
      m = i3 - 2;
      n = k;
      k = i;
      i = n;
    }
    if (i == localVersion.getTotalCodewords()) {
      return arrayOfByte;
    }
    throw FormatException.getFormatInstance();
  }
  
  FormatInformation readFormatInformation()
    throws FormatException
  {
    FormatInformation localFormatInformation = this.parsedFormatInfo;
    if (localFormatInformation != null) {
      return localFormatInformation;
    }
    int m = 0;
    int i = 0;
    int j = 0;
    while (i < 6)
    {
      j = copyBit(i, 8, j);
      i++;
    }
    j = copyBit(8, 7, copyBit(8, 8, copyBit(7, 8, j)));
    for (i = 5; i >= 0; i--) {
      j = copyBit(8, i, j);
    }
    int n = this.bitMatrix.getHeight();
    int k = n - 1;
    i = m;
    while (k >= n - 7)
    {
      i = copyBit(8, k, i);
      k--;
    }
    for (k = n - 8; k < n; k++) {
      i = copyBit(k, 8, i);
    }
    this.parsedFormatInfo = FormatInformation.decodeFormatInformation(j, i);
    localFormatInformation = this.parsedFormatInfo;
    if (localFormatInformation != null) {
      return localFormatInformation;
    }
    throw FormatException.getFormatInstance();
  }
  
  Version readVersion()
    throws FormatException
  {
    Version localVersion = this.parsedVersion;
    if (localVersion != null) {
      return localVersion;
    }
    int i1 = this.bitMatrix.getHeight();
    int i = (i1 - 17) / 4;
    if (i <= 6) {
      return Version.getVersionForNumber(i);
    }
    int i2 = i1 - 11;
    int n = 5;
    int m = 0;
    i = 5;
    int j = 0;
    int k;
    while (i >= 0)
    {
      for (k = i1 - 9; k >= i2; k--) {
        j = copyBit(k, i, j);
      }
      i--;
    }
    localVersion = Version.decodeVersionInformation(j);
    i = n;
    j = m;
    if (localVersion != null)
    {
      i = n;
      j = m;
      if (localVersion.getDimensionForVersion() == i1)
      {
        this.parsedVersion = localVersion;
        return localVersion;
      }
    }
    while (i >= 0)
    {
      for (k = i1 - 9; k >= i2; k--) {
        j = copyBit(i, k, j);
      }
      i--;
    }
    localVersion = Version.decodeVersionInformation(j);
    if ((localVersion != null) && (localVersion.getDimensionForVersion() == i1))
    {
      this.parsedVersion = localVersion;
      return localVersion;
    }
    throw FormatException.getFormatInstance();
  }
  
  void remask()
  {
    if (this.parsedFormatInfo == null) {
      return;
    }
    DataMask localDataMask = DataMask.values()[this.parsedFormatInfo.getDataMask()];
    int i = this.bitMatrix.getHeight();
    localDataMask.unmaskBitMatrix(this.bitMatrix, i);
  }
  
  void setMirror(boolean paramBoolean)
  {
    this.parsedVersion = null;
    this.parsedFormatInfo = null;
    this.mirror = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/decoder/BitMatrixParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */