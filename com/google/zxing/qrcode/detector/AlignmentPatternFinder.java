package com.google.zxing.qrcode.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class AlignmentPatternFinder
{
  private final int[] crossCheckStateCount;
  private final int height;
  private final BitMatrix image;
  private final float moduleSize;
  private final List<AlignmentPattern> possibleCenters;
  private final ResultPointCallback resultPointCallback;
  private final int startX;
  private final int startY;
  private final int width;
  
  AlignmentPatternFinder(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat, ResultPointCallback paramResultPointCallback)
  {
    this.image = paramBitMatrix;
    this.possibleCenters = new ArrayList(5);
    this.startX = paramInt1;
    this.startY = paramInt2;
    this.width = paramInt3;
    this.height = paramInt4;
    this.moduleSize = paramFloat;
    this.crossCheckStateCount = new int[3];
    this.resultPointCallback = paramResultPointCallback;
  }
  
  private static float centerFromEnd(int[] paramArrayOfInt, int paramInt)
  {
    return paramInt - paramArrayOfInt[2] - paramArrayOfInt[1] / 2.0F;
  }
  
  private float crossCheckVertical(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    BitMatrix localBitMatrix = this.image;
    int j = localBitMatrix.getHeight();
    int[] arrayOfInt = this.crossCheckStateCount;
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    for (int i = paramInt1; (i >= 0) && (localBitMatrix.get(paramInt2, i)) && (arrayOfInt[1] <= paramInt3); i--) {
      arrayOfInt[1] += 1;
    }
    if ((i >= 0) && (arrayOfInt[1] <= paramInt3))
    {
      while ((i >= 0) && (!localBitMatrix.get(paramInt2, i)) && (arrayOfInt[0] <= paramInt3))
      {
        arrayOfInt[0] += 1;
        i--;
      }
      if (arrayOfInt[0] > paramInt3) {
        return NaN.0F;
      }
      paramInt1++;
      while ((paramInt1 < j) && (localBitMatrix.get(paramInt2, paramInt1)) && (arrayOfInt[1] <= paramInt3))
      {
        arrayOfInt[1] += 1;
        paramInt1++;
      }
      if ((paramInt1 != j) && (arrayOfInt[1] <= paramInt3))
      {
        while ((paramInt1 < j) && (!localBitMatrix.get(paramInt2, paramInt1)) && (arrayOfInt[2] <= paramInt3))
        {
          arrayOfInt[2] += 1;
          paramInt1++;
        }
        if (arrayOfInt[2] > paramInt3) {
          return NaN.0F;
        }
        if (Math.abs(arrayOfInt[0] + arrayOfInt[1] + arrayOfInt[2] - paramInt4) * 5 >= paramInt4 * 2) {
          return NaN.0F;
        }
        if (foundPatternCross(arrayOfInt)) {
          return centerFromEnd(arrayOfInt, paramInt1);
        }
        return NaN.0F;
      }
      return NaN.0F;
    }
    return NaN.0F;
  }
  
  private boolean foundPatternCross(int[] paramArrayOfInt)
  {
    float f1 = this.moduleSize;
    float f2 = f1 / 2.0F;
    for (int i = 0; i < 3; i++) {
      if (Math.abs(f1 - paramArrayOfInt[i]) >= f2) {
        return false;
      }
    }
    return true;
  }
  
  private AlignmentPattern handlePossibleCenter(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    int j = paramArrayOfInt[0];
    int k = paramArrayOfInt[1];
    int i = paramArrayOfInt[2];
    float f3 = centerFromEnd(paramArrayOfInt, paramInt2);
    float f1 = crossCheckVertical(paramInt1, (int)f3, paramArrayOfInt[1] * 2, j + k + i);
    if (!Float.isNaN(f1))
    {
      float f2 = (paramArrayOfInt[0] + paramArrayOfInt[1] + paramArrayOfInt[2]) / 3.0F;
      paramArrayOfInt = this.possibleCenters.iterator();
      while (paramArrayOfInt.hasNext())
      {
        localAlignmentPattern = (AlignmentPattern)paramArrayOfInt.next();
        if (localAlignmentPattern.aboutEquals(f2, f1, f3)) {
          return localAlignmentPattern.combineEstimate(f1, f3, f2);
        }
      }
      AlignmentPattern localAlignmentPattern = new AlignmentPattern(f3, f1, f2);
      this.possibleCenters.add(localAlignmentPattern);
      paramArrayOfInt = this.resultPointCallback;
      if (paramArrayOfInt != null) {
        paramArrayOfInt.foundPossibleResultPoint(localAlignmentPattern);
      }
    }
    return null;
  }
  
  AlignmentPattern find()
    throws NotFoundException
  {
    int n = this.startX;
    int i1 = this.height;
    int i2 = this.width + n;
    int i4 = this.startY;
    int i3 = i1 / 2;
    int[] arrayOfInt = new int[3];
    for (int k = 0; k < i1; k++)
    {
      if ((k & 0x1) == 0) {
        i = (k + 1) / 2;
      } else {
        i = -((k + 1) / 2);
      }
      int i5 = i + (i4 + i3);
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 0;
      arrayOfInt[2] = 0;
      for (int j = n; (j < i2) && (!this.image.get(j, i5)); j++) {}
      int i = 0;
      AlignmentPattern localAlignmentPattern;
      for (int m = j; m < i2; m++) {
        if (this.image.get(m, i5))
        {
          if (i == 1)
          {
            arrayOfInt[1] += 1;
          }
          else if (i == 2)
          {
            if (foundPatternCross(arrayOfInt))
            {
              localAlignmentPattern = handlePossibleCenter(arrayOfInt, i5, m);
              if (localAlignmentPattern != null) {
                return localAlignmentPattern;
              }
            }
            arrayOfInt[0] = arrayOfInt[2];
            arrayOfInt[1] = 1;
            arrayOfInt[2] = 0;
            i = 1;
          }
          else
          {
            i++;
            arrayOfInt[i] += 1;
          }
        }
        else
        {
          j = i;
          if (i == 1) {
            j = i + 1;
          }
          arrayOfInt[j] += 1;
          i = j;
        }
      }
      if (foundPatternCross(arrayOfInt))
      {
        localAlignmentPattern = handlePossibleCenter(arrayOfInt, i5, i2);
        if (localAlignmentPattern != null) {
          return localAlignmentPattern;
        }
      }
    }
    if (!this.possibleCenters.isEmpty()) {
      return (AlignmentPattern)this.possibleCenters.get(0);
    }
    throw NotFoundException.getNotFoundInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/detector/AlignmentPatternFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */