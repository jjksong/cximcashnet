package com.google.zxing.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FinderPatternFinder
{
  private static final int CENTER_QUORUM = 2;
  protected static final int MAX_MODULES = 57;
  protected static final int MIN_SKIP = 3;
  private final int[] crossCheckStateCount;
  private boolean hasSkipped;
  private final BitMatrix image;
  private final List<FinderPattern> possibleCenters;
  private final ResultPointCallback resultPointCallback;
  
  public FinderPatternFinder(BitMatrix paramBitMatrix)
  {
    this(paramBitMatrix, null);
  }
  
  public FinderPatternFinder(BitMatrix paramBitMatrix, ResultPointCallback paramResultPointCallback)
  {
    this.image = paramBitMatrix;
    this.possibleCenters = new ArrayList();
    this.crossCheckStateCount = new int[5];
    this.resultPointCallback = paramResultPointCallback;
  }
  
  private static float centerFromEnd(int[] paramArrayOfInt, int paramInt)
  {
    return paramInt - paramArrayOfInt[4] - paramArrayOfInt[3] - paramArrayOfInt[2] / 2.0F;
  }
  
  private boolean crossCheckDiagonal(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int[] arrayOfInt = getCrossCheckStateCount();
    for (int i = 0; (paramInt1 >= i) && (paramInt2 >= i) && (this.image.get(paramInt2 - i, paramInt1 - i)); i++) {
      arrayOfInt[2] += 1;
    }
    if (paramInt1 >= i)
    {
      int j = i;
      if (paramInt2 >= i)
      {
        while ((paramInt1 >= j) && (paramInt2 >= j) && (!this.image.get(paramInt2 - j, paramInt1 - j)) && (arrayOfInt[1] <= paramInt3))
        {
          arrayOfInt[1] += 1;
          j++;
        }
        if ((paramInt1 >= j) && (paramInt2 >= j) && (arrayOfInt[1] <= paramInt3))
        {
          while ((paramInt1 >= j) && (paramInt2 >= j) && (this.image.get(paramInt2 - j, paramInt1 - j)) && (arrayOfInt[0] <= paramInt3))
          {
            arrayOfInt[0] += 1;
            j++;
          }
          if (arrayOfInt[0] > paramInt3) {
            return false;
          }
          int k = this.image.getHeight();
          int m = this.image.getWidth();
          int n;
          for (i = 1;; i++)
          {
            j = paramInt1 + i;
            if (j >= k) {
              break;
            }
            n = paramInt2 + i;
            if ((n >= m) || (!this.image.get(n, j))) {
              break;
            }
            arrayOfInt[2] += 1;
          }
          if (j < k)
          {
            j = i;
            if (paramInt2 + i < m)
            {
              for (;;)
              {
                i = paramInt1 + j;
                if (i >= k) {
                  break;
                }
                n = paramInt2 + j;
                if ((n >= m) || (this.image.get(n, i)) || (arrayOfInt[3] >= paramInt3)) {
                  break;
                }
                arrayOfInt[3] += 1;
                j++;
              }
              if ((i < k) && (paramInt2 + j < m) && (arrayOfInt[3] < paramInt3))
              {
                for (;;)
                {
                  n = paramInt1 + j;
                  if (n >= k) {
                    break;
                  }
                  i = paramInt2 + j;
                  if ((i >= m) || (!this.image.get(i, n)) || (arrayOfInt[4] >= paramInt3)) {
                    break;
                  }
                  arrayOfInt[4] += 1;
                  j++;
                }
                if (arrayOfInt[4] >= paramInt3) {
                  return false;
                }
                return (Math.abs(arrayOfInt[0] + arrayOfInt[1] + arrayOfInt[2] + arrayOfInt[3] + arrayOfInt[4] - paramInt4) < paramInt4 * 2) && (foundPatternCross(arrayOfInt));
              }
              return false;
            }
          }
          return false;
        }
        return false;
      }
    }
    return false;
  }
  
  private float crossCheckHorizontal(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    BitMatrix localBitMatrix = this.image;
    int k = localBitMatrix.getWidth();
    int[] arrayOfInt = getCrossCheckStateCount();
    for (int j = paramInt1; (j >= 0) && (localBitMatrix.get(j, paramInt2)); j--) {
      arrayOfInt[2] += 1;
    }
    int i = j;
    if (j < 0) {
      return NaN.0F;
    }
    while ((i >= 0) && (!localBitMatrix.get(i, paramInt2)) && (arrayOfInt[1] <= paramInt3))
    {
      arrayOfInt[1] += 1;
      i--;
    }
    if ((i >= 0) && (arrayOfInt[1] <= paramInt3))
    {
      while ((i >= 0) && (localBitMatrix.get(i, paramInt2)) && (arrayOfInt[0] <= paramInt3))
      {
        arrayOfInt[0] += 1;
        i--;
      }
      if (arrayOfInt[0] > paramInt3) {
        return NaN.0F;
      }
      for (i = paramInt1 + 1; (i < k) && (localBitMatrix.get(i, paramInt2)); i++) {
        arrayOfInt[2] += 1;
      }
      paramInt1 = i;
      if (i == k) {
        return NaN.0F;
      }
      while ((paramInt1 < k) && (!localBitMatrix.get(paramInt1, paramInt2)) && (arrayOfInt[3] < paramInt3))
      {
        arrayOfInt[3] += 1;
        paramInt1++;
      }
      if ((paramInt1 != k) && (arrayOfInt[3] < paramInt3))
      {
        while ((paramInt1 < k) && (localBitMatrix.get(paramInt1, paramInt2)) && (arrayOfInt[4] < paramInt3))
        {
          arrayOfInt[4] += 1;
          paramInt1++;
        }
        if (arrayOfInt[4] >= paramInt3) {
          return NaN.0F;
        }
        if (Math.abs(arrayOfInt[0] + arrayOfInt[1] + arrayOfInt[2] + arrayOfInt[3] + arrayOfInt[4] - paramInt4) * 5 >= paramInt4) {
          return NaN.0F;
        }
        if (foundPatternCross(arrayOfInt)) {
          return centerFromEnd(arrayOfInt, paramInt1);
        }
        return NaN.0F;
      }
      return NaN.0F;
    }
    return NaN.0F;
  }
  
  private float crossCheckVertical(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    BitMatrix localBitMatrix = this.image;
    int k = localBitMatrix.getHeight();
    int[] arrayOfInt = getCrossCheckStateCount();
    for (int i = paramInt1; (i >= 0) && (localBitMatrix.get(paramInt2, i)); i--) {
      arrayOfInt[2] += 1;
    }
    int j = i;
    if (i < 0) {
      return NaN.0F;
    }
    while ((j >= 0) && (!localBitMatrix.get(paramInt2, j)) && (arrayOfInt[1] <= paramInt3))
    {
      arrayOfInt[1] += 1;
      j--;
    }
    if ((j >= 0) && (arrayOfInt[1] <= paramInt3))
    {
      while ((j >= 0) && (localBitMatrix.get(paramInt2, j)) && (arrayOfInt[0] <= paramInt3))
      {
        arrayOfInt[0] += 1;
        j--;
      }
      if (arrayOfInt[0] > paramInt3) {
        return NaN.0F;
      }
      for (i = paramInt1 + 1; (i < k) && (localBitMatrix.get(paramInt2, i)); i++) {
        arrayOfInt[2] += 1;
      }
      paramInt1 = i;
      if (i == k) {
        return NaN.0F;
      }
      while ((paramInt1 < k) && (!localBitMatrix.get(paramInt2, paramInt1)) && (arrayOfInt[3] < paramInt3))
      {
        arrayOfInt[3] += 1;
        paramInt1++;
      }
      if ((paramInt1 != k) && (arrayOfInt[3] < paramInt3))
      {
        while ((paramInt1 < k) && (localBitMatrix.get(paramInt2, paramInt1)) && (arrayOfInt[4] < paramInt3))
        {
          arrayOfInt[4] += 1;
          paramInt1++;
        }
        if (arrayOfInt[4] >= paramInt3) {
          return NaN.0F;
        }
        if (Math.abs(arrayOfInt[0] + arrayOfInt[1] + arrayOfInt[2] + arrayOfInt[3] + arrayOfInt[4] - paramInt4) * 5 >= paramInt4 * 2) {
          return NaN.0F;
        }
        if (foundPatternCross(arrayOfInt)) {
          return centerFromEnd(arrayOfInt, paramInt1);
        }
        return NaN.0F;
      }
      return NaN.0F;
    }
    return NaN.0F;
  }
  
  private int findRowSkip()
  {
    if (this.possibleCenters.size() <= 1) {
      return 0;
    }
    Object localObject = null;
    Iterator localIterator = this.possibleCenters.iterator();
    while (localIterator.hasNext())
    {
      FinderPattern localFinderPattern = (FinderPattern)localIterator.next();
      if (localFinderPattern.getCount() >= 2) {
        if (localObject == null)
        {
          localObject = localFinderPattern;
        }
        else
        {
          this.hasSkipped = true;
          return (int)(Math.abs(((ResultPoint)localObject).getX() - localFinderPattern.getX()) - Math.abs(((ResultPoint)localObject).getY() - localFinderPattern.getY())) / 2;
        }
      }
    }
    return 0;
  }
  
  protected static boolean foundPatternCross(int[] paramArrayOfInt)
  {
    int j = 0;
    int i = 0;
    while (j < 5)
    {
      int k = paramArrayOfInt[j];
      if (k == 0) {
        return false;
      }
      i += k;
      j++;
    }
    if (i < 7) {
      return false;
    }
    float f2 = i / 7.0F;
    float f1 = f2 / 2.0F;
    return (Math.abs(f2 - paramArrayOfInt[0]) < f1) && (Math.abs(f2 - paramArrayOfInt[1]) < f1) && (Math.abs(f2 * 3.0F - paramArrayOfInt[2]) < 3.0F * f1) && (Math.abs(f2 - paramArrayOfInt[3]) < f1) && (Math.abs(f2 - paramArrayOfInt[4]) < f1);
  }
  
  private int[] getCrossCheckStateCount()
  {
    int[] arrayOfInt = this.crossCheckStateCount;
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    arrayOfInt[4] = 0;
    return arrayOfInt;
  }
  
  private boolean haveMultiplyConfirmedCenters()
  {
    int j = this.possibleCenters.size();
    Iterator localIterator = this.possibleCenters.iterator();
    float f2 = 0.0F;
    int i = 0;
    float f1 = 0.0F;
    while (localIterator.hasNext())
    {
      FinderPattern localFinderPattern = (FinderPattern)localIterator.next();
      if (localFinderPattern.getCount() >= 2)
      {
        i++;
        f1 += localFinderPattern.getEstimatedModuleSize();
      }
    }
    if (i < 3) {
      return false;
    }
    float f3 = f1 / j;
    localIterator = this.possibleCenters.iterator();
    while (localIterator.hasNext()) {
      f2 += Math.abs(((FinderPattern)localIterator.next()).getEstimatedModuleSize() - f3);
    }
    return f2 <= f1 * 0.05F;
  }
  
  private FinderPattern[] selectBestPatterns()
    throws NotFoundException
  {
    int i = this.possibleCenters.size();
    if (i >= 3)
    {
      float f3 = 0.0F;
      Object localObject;
      float f1;
      if (i > 3)
      {
        localObject = this.possibleCenters.iterator();
        float f2 = 0.0F;
        for (f1 = 0.0F; ((Iterator)localObject).hasNext(); f1 += f4 * f4)
        {
          f4 = ((FinderPattern)((Iterator)localObject).next()).getEstimatedModuleSize();
          f2 += f4;
        }
        float f4 = i;
        f2 /= f4;
        f1 = (float)Math.sqrt(f1 / f4 - f2 * f2);
        Collections.sort(this.possibleCenters, new FurthestFromAverageComparator(f2, null));
        f1 = Math.max(0.2F * f2, f1);
        int j;
        for (i = 0; (i < this.possibleCenters.size()) && (this.possibleCenters.size() > 3); i = j + 1)
        {
          j = i;
          if (Math.abs(((FinderPattern)this.possibleCenters.get(i)).getEstimatedModuleSize() - f2) > f1)
          {
            this.possibleCenters.remove(i);
            j = i - 1;
          }
        }
      }
      if (this.possibleCenters.size() > 3)
      {
        localObject = this.possibleCenters.iterator();
        for (f1 = f3; ((Iterator)localObject).hasNext(); f1 += ((FinderPattern)((Iterator)localObject).next()).getEstimatedModuleSize()) {}
        f1 /= this.possibleCenters.size();
        Collections.sort(this.possibleCenters, new CenterComparator(f1, null));
        localObject = this.possibleCenters;
        ((List)localObject).subList(3, ((List)localObject).size()).clear();
      }
      return new FinderPattern[] { (FinderPattern)this.possibleCenters.get(0), (FinderPattern)this.possibleCenters.get(1), (FinderPattern)this.possibleCenters.get(2) };
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  final FinderPatternInfo find(Map<DecodeHintType, ?> paramMap)
    throws NotFoundException
  {
    int j;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.TRY_HARDER))) {
      j = 1;
    } else {
      j = 0;
    }
    boolean bool3;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.PURE_BARCODE))) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    int i2 = this.image.getHeight();
    int i1 = this.image.getWidth();
    int i = i2 * 3 / 228;
    if ((i < 3) || (j != 0)) {
      i = 3;
    }
    paramMap = new int[5];
    int k = i - 1;
    boolean bool1 = false;
    while ((k < i2) && (!bool1))
    {
      paramMap[0] = 0;
      paramMap[1] = 0;
      paramMap[2] = 0;
      paramMap[3] = 0;
      paramMap[4] = 0;
      j = 0;
      int n = 0;
      int m = i;
      i = n;
      while (j < i1)
      {
        if (this.image.get(j, k))
        {
          n = i;
          if ((i & 0x1) == 1) {
            n = i + 1;
          }
          paramMap[n] += 1;
          i = n;
        }
        else if ((i & 0x1) == 0)
        {
          if (i == 4)
          {
            if (foundPatternCross(paramMap))
            {
              if (handlePossibleCenter(paramMap, k, j, bool3))
              {
                boolean bool2;
                if (this.hasSkipped)
                {
                  bool2 = haveMultiplyConfirmedCenters();
                  i = k;
                }
                else
                {
                  m = findRowSkip();
                  i = k;
                  bool2 = bool1;
                  if (m > paramMap[2])
                  {
                    i = k + (m - paramMap[2] - 2);
                    j = i1 - 1;
                    bool2 = bool1;
                  }
                }
                paramMap[0] = 0;
                paramMap[1] = 0;
                paramMap[2] = 0;
                paramMap[3] = 0;
                paramMap[4] = 0;
                n = 0;
                m = 2;
                k = i;
                i = n;
                bool1 = bool2;
              }
              else
              {
                paramMap[0] = paramMap[2];
                paramMap[1] = paramMap[3];
                paramMap[2] = paramMap[4];
                paramMap[3] = 1;
                paramMap[4] = 0;
                i = 3;
              }
            }
            else
            {
              paramMap[0] = paramMap[2];
              paramMap[1] = paramMap[3];
              paramMap[2] = paramMap[4];
              paramMap[3] = 1;
              paramMap[4] = 0;
              i = 3;
            }
          }
          else
          {
            i++;
            paramMap[i] += 1;
          }
        }
        else
        {
          paramMap[i] += 1;
        }
        j++;
      }
      if ((foundPatternCross(paramMap)) && (handlePossibleCenter(paramMap, k, i1, bool3)))
      {
        i = paramMap[0];
        if (this.hasSkipped) {
          bool1 = haveMultiplyConfirmedCenters();
        }
      }
      else
      {
        i = m;
      }
      k += i;
    }
    paramMap = selectBestPatterns();
    ResultPoint.orderBestPatterns(paramMap);
    return new FinderPatternInfo(paramMap);
  }
  
  protected final BitMatrix getImage()
  {
    return this.image;
  }
  
  protected final List<FinderPattern> getPossibleCenters()
  {
    return this.possibleCenters;
  }
  
  protected final boolean handlePossibleCenter(int[] paramArrayOfInt, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i = 0;
    int j = paramArrayOfInt[0] + paramArrayOfInt[1] + paramArrayOfInt[2] + paramArrayOfInt[3] + paramArrayOfInt[4];
    paramInt2 = (int)centerFromEnd(paramArrayOfInt, paramInt2);
    float f3 = crossCheckVertical(paramInt1, paramInt2, paramArrayOfInt[2], j);
    if (!Float.isNaN(f3))
    {
      paramInt1 = (int)f3;
      float f2 = crossCheckHorizontal(paramInt2, paramInt1, paramArrayOfInt[2], j);
      if ((!Float.isNaN(f2)) && ((!paramBoolean) || (crossCheckDiagonal(paramInt1, (int)f2, paramArrayOfInt[2], j))))
      {
        float f1 = j / 7.0F;
        for (paramInt1 = 0;; paramInt1++)
        {
          paramInt2 = i;
          if (paramInt1 >= this.possibleCenters.size()) {
            break;
          }
          paramArrayOfInt = (FinderPattern)this.possibleCenters.get(paramInt1);
          if (paramArrayOfInt.aboutEquals(f1, f3, f2))
          {
            this.possibleCenters.set(paramInt1, paramArrayOfInt.combineEstimate(f3, f2, f1));
            paramInt2 = 1;
            break;
          }
        }
        if (paramInt2 == 0)
        {
          paramArrayOfInt = new FinderPattern(f2, f3, f1);
          this.possibleCenters.add(paramArrayOfInt);
          ResultPointCallback localResultPointCallback = this.resultPointCallback;
          if (localResultPointCallback != null) {
            localResultPointCallback.foundPossibleResultPoint(paramArrayOfInt);
          }
        }
        return true;
      }
    }
    return false;
  }
  
  private static final class CenterComparator
    implements Serializable, Comparator<FinderPattern>
  {
    private final float average;
    
    private CenterComparator(float paramFloat)
    {
      this.average = paramFloat;
    }
    
    public int compare(FinderPattern paramFinderPattern1, FinderPattern paramFinderPattern2)
    {
      if (paramFinderPattern2.getCount() == paramFinderPattern1.getCount())
      {
        float f1 = Math.abs(paramFinderPattern2.getEstimatedModuleSize() - this.average);
        float f2 = Math.abs(paramFinderPattern1.getEstimatedModuleSize() - this.average);
        if (f1 < f2) {
          return 1;
        }
        if (f1 == f2) {
          return 0;
        }
        return -1;
      }
      return paramFinderPattern2.getCount() - paramFinderPattern1.getCount();
    }
  }
  
  private static final class FurthestFromAverageComparator
    implements Serializable, Comparator<FinderPattern>
  {
    private final float average;
    
    private FurthestFromAverageComparator(float paramFloat)
    {
      this.average = paramFloat;
    }
    
    public int compare(FinderPattern paramFinderPattern1, FinderPattern paramFinderPattern2)
    {
      float f2 = Math.abs(paramFinderPattern2.getEstimatedModuleSize() - this.average);
      float f1 = Math.abs(paramFinderPattern1.getEstimatedModuleSize() - this.average);
      if (f2 < f1) {
        return -1;
      }
      if (f2 == f1) {
        return 0;
      }
      return 1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/detector/FinderPatternFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */