package com.google.zxing.qrcode.detector;

import com.google.zxing.ResultPoint;

public final class FinderPattern
  extends ResultPoint
{
  private final int count;
  private final float estimatedModuleSize;
  
  FinderPattern(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this(paramFloat1, paramFloat2, paramFloat3, 1);
  }
  
  private FinderPattern(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
  {
    super(paramFloat1, paramFloat2);
    this.estimatedModuleSize = paramFloat3;
    this.count = paramInt;
  }
  
  boolean aboutEquals(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    if ((Math.abs(paramFloat2 - getY()) <= paramFloat1) && (Math.abs(paramFloat3 - getX()) <= paramFloat1))
    {
      paramFloat1 = Math.abs(paramFloat1 - this.estimatedModuleSize);
      return (paramFloat1 <= 1.0F) || (paramFloat1 <= this.estimatedModuleSize);
    }
    return false;
  }
  
  FinderPattern combineEstimate(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    int j = this.count;
    int i = j + 1;
    float f2 = j;
    float f1 = getX();
    float f3 = i;
    return new FinderPattern((f2 * f1 + paramFloat2) / f3, (this.count * getY() + paramFloat1) / f3, (this.count * this.estimatedModuleSize + paramFloat3) / f3, i);
  }
  
  int getCount()
  {
    return this.count;
  }
  
  public float getEstimatedModuleSize()
  {
    return this.estimatedModuleSize;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/qrcode/detector/FinderPattern.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */