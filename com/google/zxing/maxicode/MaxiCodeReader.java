package com.google.zxing.maxicode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.maxicode.decoder.Decoder;
import java.util.Map;

public final class MaxiCodeReader
  implements Reader
{
  private static final int MATRIX_HEIGHT = 33;
  private static final int MATRIX_WIDTH = 30;
  private static final ResultPoint[] NO_POINTS = new ResultPoint[0];
  private final Decoder decoder = new Decoder();
  
  private static BitMatrix extractPureBits(BitMatrix paramBitMatrix)
    throws NotFoundException
  {
    Object localObject = paramBitMatrix.getEnclosingRectangle();
    if (localObject != null)
    {
      int i1 = localObject[0];
      int n = localObject[1];
      int m = localObject[2];
      int k = localObject[3];
      localObject = new BitMatrix(30, 33);
      for (int i = 0; i < 33; i++)
      {
        int i2 = (i * k + k / 2) / 33;
        for (int j = 0; j < 30; j++) {
          if (paramBitMatrix.get((j * m + m / 2 + (i & 0x1) * m / 2) / 30 + i1, i2 + n)) {
            ((BitMatrix)localObject).set(j, i);
          }
        }
      }
      return (BitMatrix)localObject;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap)
    throws NotFoundException, ChecksumException, FormatException
  {
    return decode(paramBinaryBitmap, null);
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, ChecksumException, FormatException
  {
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.PURE_BARCODE)))
    {
      paramBinaryBitmap = extractPureBits(paramBinaryBitmap.getBlackMatrix());
      paramMap = this.decoder.decode(paramBinaryBitmap, paramMap);
      paramBinaryBitmap = new Result(paramMap.getText(), paramMap.getRawBytes(), NO_POINTS, BarcodeFormat.MAXICODE);
      paramMap = paramMap.getECLevel();
      if (paramMap != null) {
        paramBinaryBitmap.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL, paramMap);
      }
      return paramBinaryBitmap;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public void reset() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/maxicode/MaxiCodeReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */