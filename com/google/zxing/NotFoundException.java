package com.google.zxing;

public final class NotFoundException
  extends ReaderException
{
  private static final NotFoundException INSTANCE;
  
  static
  {
    NotFoundException localNotFoundException = new NotFoundException();
    INSTANCE = localNotFoundException;
    localNotFoundException.setStackTrace(NO_TRACE);
  }
  
  public static NotFoundException getNotFoundInstance()
  {
    return INSTANCE;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/NotFoundException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */