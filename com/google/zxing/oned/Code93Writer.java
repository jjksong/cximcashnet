package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Map;

public class Code93Writer
  extends OneDimensionalCodeWriter
{
  protected static int appendPattern(boolean[] paramArrayOfBoolean, int paramInt, int[] paramArrayOfInt, boolean paramBoolean)
  {
    int j = paramArrayOfInt.length;
    int i = paramInt;
    paramInt = 0;
    while (paramInt < j)
    {
      if (paramArrayOfInt[paramInt] != 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }
      paramArrayOfBoolean[i] = paramBoolean;
      paramInt++;
      i++;
    }
    return 9;
  }
  
  private static int computeChecksumIndex(String paramString, int paramInt)
  {
    int j = paramString.length() - 1;
    int k = 0;
    int i = 1;
    while (j >= 0)
    {
      k += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(paramString.charAt(j)) * i;
      int m = i + 1;
      i = m;
      if (m > paramInt) {
        i = 1;
      }
      j--;
    }
    return k % 47;
  }
  
  private static void toIntArray(int paramInt, int[] paramArrayOfInt)
  {
    for (int i = 0; i < 9; i++)
    {
      int j = 1;
      if ((1 << 8 - i & paramInt) == 0) {
        j = 0;
      }
      paramArrayOfInt[i] = j;
    }
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (paramBarcodeFormat == BarcodeFormat.CODE_93) {
      return super.encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap);
    }
    paramString = new StringBuilder("Can only encode CODE_93, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
  
  public boolean[] encode(String paramString)
  {
    int k = paramString.length();
    if (k <= 80)
    {
      int[] arrayOfInt = new int[9];
      boolean[] arrayOfBoolean = new boolean[(paramString.length() + 2 + 2) * 9 + 1];
      toIntArray(Code93Reader.CHARACTER_ENCODINGS[47], arrayOfInt);
      int j = 0;
      int i = appendPattern(arrayOfBoolean, 0, arrayOfInt, true);
      while (j < k)
      {
        int m = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(paramString.charAt(j));
        toIntArray(Code93Reader.CHARACTER_ENCODINGS[m], arrayOfInt);
        i += appendPattern(arrayOfBoolean, i, arrayOfInt, true);
        j++;
      }
      j = computeChecksumIndex(paramString, 20);
      toIntArray(Code93Reader.CHARACTER_ENCODINGS[j], arrayOfInt);
      i += appendPattern(arrayOfBoolean, i, arrayOfInt, true);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".charAt(j));
      j = computeChecksumIndex(localStringBuilder.toString(), 15);
      toIntArray(Code93Reader.CHARACTER_ENCODINGS[j], arrayOfInt);
      i += appendPattern(arrayOfBoolean, i, arrayOfInt, true);
      toIntArray(Code93Reader.CHARACTER_ENCODINGS[47], arrayOfInt);
      arrayOfBoolean[(i + appendPattern(arrayOfBoolean, i, arrayOfInt, true))] = true;
      return arrayOfBoolean;
    }
    paramString = new StringBuilder("Requested contents should be less than 80 digits long, but got ");
    paramString.append(k);
    throw new IllegalArgumentException(paramString.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/Code93Writer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */