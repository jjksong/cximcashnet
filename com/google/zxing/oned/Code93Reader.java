package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Arrays;
import java.util.Map;

public final class Code93Reader
  extends OneDReader
{
  private static final char[] ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".toCharArray();
  static final String ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*";
  private static final int ASTERISK_ENCODING = arrayOfInt[47];
  static final int[] CHARACTER_ENCODINGS;
  private final int[] counters = new int[6];
  private final StringBuilder decodeRowResult = new StringBuilder(20);
  
  static
  {
    int[] arrayOfInt = new int[48];
    arrayOfInt[0] = 'Ĕ';
    arrayOfInt[1] = 'ň';
    arrayOfInt[2] = 'ń';
    arrayOfInt[3] = 'ł';
    arrayOfInt[4] = 'Ĩ';
    arrayOfInt[5] = 'Ĥ';
    arrayOfInt[6] = 'Ģ';
    arrayOfInt[7] = 'Ő';
    arrayOfInt[8] = 'Ē';
    arrayOfInt[9] = 'Ċ';
    arrayOfInt[10] = 'ƨ';
    arrayOfInt[11] = 'Ƥ';
    arrayOfInt[12] = 'Ƣ';
    arrayOfInt[13] = 'Ɣ';
    arrayOfInt[14] = 'ƒ';
    arrayOfInt[15] = 'Ɗ';
    arrayOfInt[16] = 'Ũ';
    arrayOfInt[17] = 'Ť';
    arrayOfInt[18] = 'Ţ';
    arrayOfInt[19] = 'Ĵ';
    arrayOfInt[20] = 'Ě';
    arrayOfInt[21] = 'Ř';
    arrayOfInt[22] = 'Ō';
    arrayOfInt[23] = 'ņ';
    arrayOfInt[24] = 'Ĭ';
    arrayOfInt[25] = 'Ė';
    arrayOfInt[26] = 'ƴ';
    arrayOfInt[27] = 'Ʋ';
    arrayOfInt[28] = 'Ƭ';
    arrayOfInt[29] = 'Ʀ';
    arrayOfInt[30] = 'Ɩ';
    arrayOfInt[31] = 'ƚ';
    arrayOfInt[32] = 'Ŭ';
    arrayOfInt[33] = 'Ŧ';
    arrayOfInt[34] = 'Ķ';
    arrayOfInt[35] = 'ĺ';
    arrayOfInt[36] = 'Į';
    arrayOfInt[37] = 'ǔ';
    arrayOfInt[38] = 'ǒ';
    arrayOfInt[39] = 'Ǌ';
    arrayOfInt[40] = 'Ů';
    arrayOfInt[41] = 'Ŷ';
    arrayOfInt[42] = 'Ʈ';
    arrayOfInt[43] = 'Ħ';
    arrayOfInt[44] = 'ǚ';
    arrayOfInt[45] = 'ǖ';
    arrayOfInt[46] = 'Ĳ';
    arrayOfInt[47] = 'Ş';
    arrayOfInt;
    CHARACTER_ENCODINGS = arrayOfInt;
  }
  
  private static void checkChecksums(CharSequence paramCharSequence)
    throws ChecksumException
  {
    int i = paramCharSequence.length();
    checkOneChecksum(paramCharSequence, i - 2, 20);
    checkOneChecksum(paramCharSequence, i - 1, 15);
  }
  
  private static void checkOneChecksum(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    throws ChecksumException
  {
    int j = paramInt1 - 1;
    int k = 0;
    int i = 1;
    while (j >= 0)
    {
      k += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(paramCharSequence.charAt(j)) * i;
      int m = i + 1;
      i = m;
      if (m > paramInt2) {
        i = 1;
      }
      j--;
    }
    if (paramCharSequence.charAt(paramInt1) == ALPHABET[(k % 47)]) {
      return;
    }
    throw ChecksumException.getChecksumInstance();
  }
  
  private static String decodeExtended(CharSequence paramCharSequence)
    throws FormatException
  {
    int j = paramCharSequence.length();
    StringBuilder localStringBuilder = new StringBuilder(j);
    for (int i = 0; i < j; i++)
    {
      char c = paramCharSequence.charAt(i);
      if ((c >= 'a') && (c <= 'd'))
      {
        if (i < j - 1)
        {
          i++;
          int k = paramCharSequence.charAt(i);
          switch (c)
          {
          default: 
            c = '\000';
            break;
          case 'd': 
            if ((k >= 65) && (k <= 90)) {
              c = (char)(k + 32);
            } else {
              throw FormatException.getFormatInstance();
            }
            break;
          case 'c': 
            if ((k >= 65) && (k <= 79)) {
              c = (char)(k - 32);
            } else if (k == 90) {
              c = ':';
            } else {
              throw FormatException.getFormatInstance();
            }
            break;
          case 'b': 
            if ((k >= 65) && (k <= 69)) {
              c = (char)(k - 38);
            } else if ((k >= 70) && (k <= 74)) {
              c = (char)(k - 11);
            } else if ((k >= 75) && (k <= 79)) {
              c = (char)(k + 16);
            } else if ((k >= 80) && (k <= 83)) {
              c = (char)(k + 43);
            } else if ((k >= 84) && (k <= 90)) {
              c = '';
            } else {
              throw FormatException.getFormatInstance();
            }
            break;
          case 'a': 
            if ((k >= 65) && (k <= 90)) {
              c = (char)(k - 64);
            } else {
              throw FormatException.getFormatInstance();
            }
            break;
          }
          localStringBuilder.append(c);
        }
        else
        {
          throw FormatException.getFormatInstance();
        }
      }
      else {
        localStringBuilder.append(c);
      }
    }
    return localStringBuilder.toString();
  }
  
  private int[] findAsteriskPattern(BitArray paramBitArray)
    throws NotFoundException
  {
    int i2 = paramBitArray.getSize();
    int k = paramBitArray.getNextSet(0);
    Arrays.fill(this.counters, 0);
    int[] arrayOfInt = this.counters;
    int i1 = arrayOfInt.length;
    int i = k;
    int m = 0;
    int j = 0;
    while (k < i2)
    {
      int n;
      if ((paramBitArray.get(k) ^ m))
      {
        arrayOfInt[j] += 1;
        n = i;
      }
      else
      {
        int i3 = i1 - 1;
        if (j == i3)
        {
          if (toPattern(arrayOfInt) == ASTERISK_ENCODING) {
            return new int[] { i, k };
          }
          n = i + (arrayOfInt[0] + arrayOfInt[1]);
          i = i1 - 2;
          System.arraycopy(arrayOfInt, 2, arrayOfInt, 0, i);
          arrayOfInt[i] = 0;
          arrayOfInt[i3] = 0;
          i = j - 1;
          j = n;
        }
        else
        {
          n = j + 1;
          j = i;
          i = n;
        }
        arrayOfInt[i] = 1;
        m ^= 0x1;
        n = j;
        j = i;
      }
      k++;
      i = n;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static char patternToChar(int paramInt)
    throws NotFoundException
  {
    for (int i = 0;; i++)
    {
      int[] arrayOfInt = CHARACTER_ENCODINGS;
      if (i >= arrayOfInt.length) {
        break;
      }
      if (arrayOfInt[i] == paramInt) {
        return ALPHABET[i];
      }
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static int toPattern(int[] paramArrayOfInt)
  {
    int k = paramArrayOfInt.length;
    int i = 0;
    int j = 0;
    while (i < k)
    {
      j += paramArrayOfInt[i];
      i++;
    }
    int n = paramArrayOfInt.length;
    k = 0;
    i = 0;
    while (k < n)
    {
      int i1 = Math.round(paramArrayOfInt[k] * 9.0F / j);
      if ((i1 > 0) && (i1 <= 4))
      {
        if ((k & 0x1) == 0) {
          for (int m = 0; m < i1; m++) {
            i = i << 1 | 0x1;
          }
        } else {
          i <<= i1;
        }
        k++;
      }
      else
      {
        return -1;
      }
    }
    return i;
  }
  
  public Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, ChecksumException, FormatException
  {
    paramMap = findAsteriskPattern(paramBitArray);
    int i = paramBitArray.getNextSet(paramMap[1]);
    int n = paramBitArray.getSize();
    Object localObject2 = this.counters;
    Arrays.fill((int[])localObject2, 0);
    Object localObject1 = this.decodeRowResult;
    ((StringBuilder)localObject1).setLength(0);
    for (;;)
    {
      recordPattern(paramBitArray, i, (int[])localObject2);
      int j = toPattern((int[])localObject2);
      if (j < 0) {
        break;
      }
      char c = patternToChar(j);
      ((StringBuilder)localObject1).append(c);
      int m = localObject2.length;
      int k = i;
      for (j = 0; j < m; j++) {
        k += localObject2[j];
      }
      m = paramBitArray.getNextSet(k);
      if (c == '*')
      {
        ((StringBuilder)localObject1).deleteCharAt(((StringBuilder)localObject1).length() - 1);
        int i1 = localObject2.length;
        j = 0;
        k = 0;
        while (j < i1)
        {
          k += localObject2[j];
          j++;
        }
        if ((m != n) && (paramBitArray.get(m)))
        {
          if (((StringBuilder)localObject1).length() >= 2)
          {
            checkChecksums((CharSequence)localObject1);
            ((StringBuilder)localObject1).setLength(((StringBuilder)localObject1).length() - 2);
            paramBitArray = decodeExtended((CharSequence)localObject1);
            float f2 = (paramMap[1] + paramMap[0]) / 2.0F;
            float f1 = i;
            float f3 = k / 2.0F;
            float f4 = paramInt;
            paramMap = new ResultPoint(f2, f4);
            localObject2 = new ResultPoint(f1 + f3, f4);
            localObject1 = BarcodeFormat.CODE_93;
            return new Result(paramBitArray, null, new ResultPoint[] { paramMap, localObject2 }, (BarcodeFormat)localObject1);
          }
          throw NotFoundException.getNotFoundInstance();
        }
        throw NotFoundException.getNotFoundInstance();
      }
      i = m;
    }
    throw NotFoundException.getNotFoundInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/Code93Reader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */