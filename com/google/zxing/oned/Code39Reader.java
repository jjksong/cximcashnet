package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Arrays;
import java.util.Map;

public final class Code39Reader
  extends OneDReader
{
  static final String ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%";
  static final int ASTERISK_ENCODING = arrayOfInt[39];
  static final int[] CHARACTER_ENCODINGS;
  private static final String CHECK_DIGIT_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%";
  private final int[] counters;
  private final StringBuilder decodeRowResult;
  private final boolean extendedMode;
  private final boolean usingCheckDigit;
  
  static
  {
    int[] arrayOfInt = new int[44];
    arrayOfInt[0] = 52;
    arrayOfInt[1] = 'ġ';
    arrayOfInt[2] = 97;
    arrayOfInt[3] = 'Š';
    arrayOfInt[4] = 49;
    arrayOfInt[5] = 'İ';
    arrayOfInt[6] = 112;
    arrayOfInt[7] = 37;
    arrayOfInt[8] = 'Ĥ';
    arrayOfInt[9] = 100;
    arrayOfInt[10] = 'ĉ';
    arrayOfInt[11] = 73;
    arrayOfInt[12] = 'ň';
    arrayOfInt[13] = 25;
    arrayOfInt[14] = 'Ę';
    arrayOfInt[15] = 88;
    arrayOfInt[16] = 13;
    arrayOfInt[17] = 'Č';
    arrayOfInt[18] = 76;
    arrayOfInt[19] = 28;
    arrayOfInt[20] = 'ă';
    arrayOfInt[21] = 67;
    arrayOfInt[22] = 'ł';
    arrayOfInt[23] = 19;
    arrayOfInt[24] = 'Ē';
    arrayOfInt[25] = 82;
    arrayOfInt[26] = 7;
    arrayOfInt[27] = 'Ć';
    arrayOfInt[28] = 70;
    arrayOfInt[29] = 22;
    arrayOfInt[30] = 'Ɓ';
    arrayOfInt[31] = 'Á';
    arrayOfInt[32] = 'ǀ';
    arrayOfInt[33] = '';
    arrayOfInt[34] = 'Ɛ';
    arrayOfInt[35] = 'Ð';
    arrayOfInt[36] = '';
    arrayOfInt[37] = 'Ƅ';
    arrayOfInt[38] = 'Ä';
    arrayOfInt[39] = '';
    arrayOfInt[40] = '¨';
    arrayOfInt[41] = '¢';
    arrayOfInt[42] = '';
    arrayOfInt[43] = 42;
    arrayOfInt;
    CHARACTER_ENCODINGS = arrayOfInt;
  }
  
  public Code39Reader()
  {
    this(false);
  }
  
  public Code39Reader(boolean paramBoolean)
  {
    this(paramBoolean, false);
  }
  
  public Code39Reader(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.usingCheckDigit = paramBoolean1;
    this.extendedMode = paramBoolean2;
    this.decodeRowResult = new StringBuilder(20);
    this.counters = new int[9];
  }
  
  private static String decodeExtended(CharSequence paramCharSequence)
    throws FormatException
  {
    int j = paramCharSequence.length();
    StringBuilder localStringBuilder = new StringBuilder(j);
    int i = 0;
    while (i < j)
    {
      char c = paramCharSequence.charAt(i);
      if ((c != '+') && (c != '$') && (c != '%') && (c != '/'))
      {
        localStringBuilder.append(c);
      }
      else
      {
        i++;
        int k = paramCharSequence.charAt(i);
        if (c != '+')
        {
          if (c != '/') {
            switch (c)
            {
            default: 
              c = '\000';
              break;
            case '%': 
              if ((k >= 65) && (k <= 69))
              {
                c = (char)(k - 38);
                break;
              }
              if ((k >= 70) && (k <= 87))
              {
                c = (char)(k - 11);
                break;
              }
              throw FormatException.getFormatInstance();
            case '$': 
              if ((k >= 65) && (k <= 90))
              {
                c = (char)(k - 64);
                break;
              }
              throw FormatException.getFormatInstance();
            }
          } else if ((k >= 65) && (k <= 79)) {
            c = (char)(k - 32);
          } else if (k == 90) {
            c = ':';
          } else {
            throw FormatException.getFormatInstance();
          }
        }
        else
        {
          if ((k < 65) || (k > 90)) {
            break label276;
          }
          c = (char)(k + 32);
        }
        localStringBuilder.append(c);
      }
      i++;
      continue;
      label276:
      throw FormatException.getFormatInstance();
    }
    return localStringBuilder.toString();
  }
  
  private static int[] findAsteriskPattern(BitArray paramBitArray, int[] paramArrayOfInt)
    throws NotFoundException
  {
    int i2 = paramBitArray.getSize();
    int k = paramBitArray.getNextSet(0);
    int i1 = paramArrayOfInt.length;
    int i = k;
    int m = 0;
    int j = 0;
    while (k < i2)
    {
      int n;
      if ((paramBitArray.get(k) ^ m))
      {
        paramArrayOfInt[j] += 1;
        n = i;
      }
      else
      {
        int i3 = i1 - 1;
        if (j == i3)
        {
          if ((toNarrowWidePattern(paramArrayOfInt) == ASTERISK_ENCODING) && (paramBitArray.isRange(Math.max(0, i - (k - i) / 2), i, false))) {
            return new int[] { i, k };
          }
          n = i + (paramArrayOfInt[0] + paramArrayOfInt[1]);
          i = i1 - 2;
          System.arraycopy(paramArrayOfInt, 2, paramArrayOfInt, 0, i);
          paramArrayOfInt[i] = 0;
          paramArrayOfInt[i3] = 0;
          i = j - 1;
          j = n;
        }
        else
        {
          n = j + 1;
          j = i;
          i = n;
        }
        paramArrayOfInt[i] = 1;
        m ^= 0x1;
        n = j;
        j = i;
      }
      k++;
      i = n;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static char patternToChar(int paramInt)
    throws NotFoundException
  {
    for (int i = 0;; i++)
    {
      int[] arrayOfInt = CHARACTER_ENCODINGS;
      if (i >= arrayOfInt.length) {
        break;
      }
      if (arrayOfInt[i] == paramInt) {
        return "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".charAt(i);
      }
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static int toNarrowWidePattern(int[] paramArrayOfInt)
  {
    int i5 = paramArrayOfInt.length;
    int i4 = 0;
    int i;
    for (int j = 0;; j = i)
    {
      int i1 = paramArrayOfInt.length;
      int m = 0;
      for (i = Integer.MAX_VALUE; m < i1; i = k)
      {
        n = paramArrayOfInt[m];
        k = i;
        if (n < i)
        {
          k = i;
          if (n > j) {
            k = n;
          }
        }
        m++;
      }
      int n = 0;
      j = 0;
      m = 0;
      int i2;
      for (int k = 0; n < i5; k = i1)
      {
        int i6 = paramArrayOfInt[n];
        int i3 = j;
        i2 = m;
        i1 = k;
        if (i6 > i)
        {
          i2 = m | 1 << i5 - 1 - n;
          i3 = j + 1;
          i1 = k + i6;
        }
        n++;
        j = i3;
        m = i2;
      }
      if (j == 3)
      {
        n = j;
        j = i4;
        while ((j < i5) && (n > 0))
        {
          i2 = paramArrayOfInt[j];
          i1 = n;
          if (i2 > i)
          {
            i1 = n - 1;
            if (i2 << 1 >= k) {
              return -1;
            }
          }
          j++;
          n = i1;
        }
        return m;
      }
      if (j <= 3) {
        return -1;
      }
    }
  }
  
  public Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, ChecksumException, FormatException
  {
    Object localObject2 = this.counters;
    Arrays.fill((int[])localObject2, 0);
    Object localObject1 = this.decodeRowResult;
    ((StringBuilder)localObject1).setLength(0);
    paramMap = findAsteriskPattern(paramBitArray, (int[])localObject2);
    int i = paramBitArray.getNextSet(paramMap[1]);
    int n = paramBitArray.getSize();
    for (;;)
    {
      recordPattern(paramBitArray, i, (int[])localObject2);
      int j = toNarrowWidePattern((int[])localObject2);
      if (j < 0) {
        break;
      }
      char c = patternToChar(j);
      ((StringBuilder)localObject1).append(c);
      int m = localObject2.length;
      int k = i;
      for (j = 0; j < m; j++) {
        k += localObject2[j];
      }
      m = paramBitArray.getNextSet(k);
      if (c == '*')
      {
        ((StringBuilder)localObject1).setLength(((StringBuilder)localObject1).length() - 1);
        int i1 = localObject2.length;
        k = 0;
        j = 0;
        while (k < i1)
        {
          j += localObject2[k];
          k++;
        }
        if ((m != n) && (m - i - j << 1 < j)) {
          throw NotFoundException.getNotFoundInstance();
        }
        if (this.usingCheckDigit)
        {
          n = ((StringBuilder)localObject1).length() - 1;
          k = 0;
          m = 0;
          while (k < n)
          {
            m += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(this.decodeRowResult.charAt(k));
            k++;
          }
          if (((StringBuilder)localObject1).charAt(n) == "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".charAt(m % 43)) {
            ((StringBuilder)localObject1).setLength(n);
          } else {
            throw ChecksumException.getChecksumInstance();
          }
        }
        if (((StringBuilder)localObject1).length() != 0)
        {
          if (this.extendedMode) {
            paramBitArray = decodeExtended((CharSequence)localObject1);
          } else {
            paramBitArray = ((StringBuilder)localObject1).toString();
          }
          float f3 = (paramMap[1] + paramMap[0]) / 2.0F;
          float f2 = i;
          float f1 = j / 2.0F;
          float f4 = paramInt;
          paramMap = new ResultPoint(f3, f4);
          localObject2 = new ResultPoint(f2 + f1, f4);
          localObject1 = BarcodeFormat.CODE_39;
          return new Result(paramBitArray, null, new ResultPoint[] { paramMap, localObject2 }, (BarcodeFormat)localObject1);
        }
        throw NotFoundException.getNotFoundInstance();
      }
      i = m;
    }
    throw NotFoundException.getNotFoundInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/Code39Reader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */