package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;

public final class EAN8Reader
  extends UPCEANReader
{
  private final int[] decodeMiddleCounters = new int[4];
  
  protected int decodeMiddle(BitArray paramBitArray, int[] paramArrayOfInt, StringBuilder paramStringBuilder)
    throws NotFoundException
  {
    int[] arrayOfInt = this.decodeMiddleCounters;
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    int m = paramBitArray.getSize();
    int i = paramArrayOfInt[1];
    int n;
    int k;
    for (int j = 0; (j < 4) && (i < m); j++)
    {
      paramStringBuilder.append((char)(decodeDigit(paramBitArray, arrayOfInt, i, L_PATTERNS) + 48));
      n = arrayOfInt.length;
      for (k = 0; k < n; k++) {
        i += arrayOfInt[k];
      }
    }
    i = findGuardPattern(paramBitArray, i, true, MIDDLE_PATTERN)[1];
    for (j = 0; (j < 4) && (i < m); j++)
    {
      paramStringBuilder.append((char)(decodeDigit(paramBitArray, arrayOfInt, i, L_PATTERNS) + 48));
      n = arrayOfInt.length;
      for (k = 0; k < n; k++) {
        i += arrayOfInt[k];
      }
    }
    return i;
  }
  
  BarcodeFormat getBarcodeFormat()
  {
    return BarcodeFormat.EAN_8;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/EAN8Reader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */