package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Map;

public final class Code39Writer
  extends OneDimensionalCodeWriter
{
  private static void toIntArray(int paramInt, int[] paramArrayOfInt)
  {
    for (int i = 0; i < 9; i++)
    {
      int j = 1;
      if ((1 << 8 - i & paramInt) != 0) {
        j = 2;
      }
      paramArrayOfInt[i] = j;
    }
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (paramBarcodeFormat == BarcodeFormat.CODE_39) {
      return super.encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap);
    }
    paramString = new StringBuilder("Can only encode CODE_39, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
  
  public boolean[] encode(String paramString)
  {
    int m = paramString.length();
    if (m <= 80)
    {
      int[] arrayOfInt = new int[9];
      int i = m + 25;
      int j = 0;
      int k;
      while (j < m)
      {
        k = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(paramString.charAt(j));
        if (k >= 0)
        {
          toIntArray(Code39Reader.CHARACTER_ENCODINGS[k], arrayOfInt);
          for (k = 0; k < 9; k++) {
            i += arrayOfInt[k];
          }
          j++;
        }
        else
        {
          localObject = new StringBuilder("Bad contents: ");
          ((StringBuilder)localObject).append(paramString);
          throw new IllegalArgumentException(((StringBuilder)localObject).toString());
        }
      }
      boolean[] arrayOfBoolean = new boolean[i];
      toIntArray(Code39Reader.ASTERISK_ENCODING, arrayOfInt);
      i = appendPattern(arrayOfBoolean, 0, arrayOfInt, true);
      Object localObject = new int[1];
      localObject[0] = 1;
      j = i + appendPattern(arrayOfBoolean, i, (int[])localObject, false);
      for (i = 0; i < m; i++)
      {
        k = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(paramString.charAt(i));
        toIntArray(Code39Reader.CHARACTER_ENCODINGS[k], arrayOfInt);
        j += appendPattern(arrayOfBoolean, j, arrayOfInt, true);
        j += appendPattern(arrayOfBoolean, j, (int[])localObject, false);
      }
      toIntArray(Code39Reader.ASTERISK_ENCODING, arrayOfInt);
      appendPattern(arrayOfBoolean, j, arrayOfInt, true);
      return arrayOfBoolean;
    }
    paramString = new StringBuilder("Requested contents should be less than 80 digits long, but got ");
    paramString.append(m);
    throw new IllegalArgumentException(paramString.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/Code39Writer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */