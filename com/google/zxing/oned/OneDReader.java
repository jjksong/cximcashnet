package com.google.zxing.oned;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;

public abstract class OneDReader
  implements Reader
{
  private Result doDecode(BinaryBitmap paramBinaryBitmap, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException
  {
    int k = paramBinaryBitmap.getWidth();
    int m = paramBinaryBitmap.getHeight();
    Object localObject1 = new BitArray(k);
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.TRY_HARDER))) {
      i = 1;
    } else {
      i = 0;
    }
    int j;
    if (i != 0) {
      j = 8;
    } else {
      j = 5;
    }
    int i2 = Math.max(1, m >> j);
    if (i != 0) {
      j = m;
    } else {
      j = 15;
    }
    int n = 0;
    int i = k;
    k = n;
    while (k < j)
    {
      n = k + 1;
      int i1 = n / 2;
      if ((k & 0x1) == 0) {
        k = 1;
      } else {
        k = 0;
      }
      if (k != 0) {
        k = i1;
      } else {
        k = -i1;
      }
      i1 = k * i2 + (m >> 1);
      if ((i1 >= 0) && (i1 < m))
      {
        try
        {
          BitArray localBitArray = paramBinaryBitmap.getBlackRow(i1, (BitArray)localObject1);
          k = 0;
          label379:
          while (k < 2)
          {
            if (k == 1)
            {
              localBitArray.reverse();
              if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.NEED_RESULT_POINT_CALLBACK)))
              {
                localObject1 = new EnumMap(DecodeHintType.class);
                ((Map)localObject1).putAll(paramMap);
                ((Map)localObject1).remove(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
                paramMap = (Map<DecodeHintType, ?>)localObject1;
              }
            }
            try
            {
              Result localResult = decodeRow(i1, localBitArray, paramMap);
              ResultPoint[] arrayOfResultPoint;
              float f2;
              float f1;
              if (k == 1)
              {
                localResult.putMetadata(ResultMetadataType.ORIENTATION, Integer.valueOf(180));
                arrayOfResultPoint = localResult.getResultPoints();
                if (arrayOfResultPoint != null)
                {
                  localObject1 = new com/google/zxing/ResultPoint;
                  f2 = i;
                  f1 = arrayOfResultPoint[0].getX();
                }
              }
              k++;
            }
            catch (ReaderException localReaderException1)
            {
              try
              {
                ((ResultPoint)localObject1).<init>(f2 - f1 - 1.0F, arrayOfResultPoint[0].getY());
                arrayOfResultPoint[0] = localObject1;
              }
              catch (ReaderException localReaderException2)
              {
                Object localObject2;
                for (;;) {}
              }
              try
              {
                arrayOfResultPoint[1] = new ResultPoint(f2 - arrayOfResultPoint[1].getX() - 1.0F, arrayOfResultPoint[1].getY());
                return localResult;
              }
              catch (ReaderException localReaderException3)
              {
                break label379;
              }
              localReaderException1 = localReaderException1;
            }
          }
          localObject2 = localBitArray;
        }
        catch (NotFoundException localNotFoundException) {}
        k = n;
      }
      else {}
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  protected static float patternMatchVariance(int[] paramArrayOfInt1, int[] paramArrayOfInt2, float paramFloat)
  {
    int n = paramArrayOfInt1.length;
    int m = 0;
    int k = 0;
    int j = 0;
    int i = 0;
    while (k < n)
    {
      j += paramArrayOfInt1[k];
      i += paramArrayOfInt2[k];
      k++;
    }
    if (j < i) {
      return Float.POSITIVE_INFINITY;
    }
    float f3 = j;
    float f4 = f3 / i;
    float f1 = 0.0F;
    for (i = m; i < n; i++)
    {
      j = paramArrayOfInt1[i];
      float f5 = paramArrayOfInt2[i] * f4;
      float f2 = j;
      if (f2 > f5) {
        f2 -= f5;
      } else {
        f2 = f5 - f2;
      }
      if (f2 > paramFloat * f4) {
        return Float.POSITIVE_INFINITY;
      }
      f1 += f2;
    }
    return f1 / f3;
  }
  
  protected static void recordPattern(BitArray paramBitArray, int paramInt, int[] paramArrayOfInt)
    throws NotFoundException
  {
    int m = paramArrayOfInt.length;
    Arrays.fill(paramArrayOfInt, 0, m, 0);
    int k = paramBitArray.getSize();
    if (paramInt < k)
    {
      boolean bool = paramBitArray.get(paramInt) ^ true;
      int j = 0;
      int i = paramInt;
      paramInt = j;
      for (;;)
      {
        j = paramInt;
        if (i >= k) {
          break;
        }
        if ((paramBitArray.get(i) ^ bool))
        {
          paramArrayOfInt[paramInt] += 1;
        }
        else
        {
          paramInt++;
          j = paramInt;
          if (paramInt == m) {
            break;
          }
          paramArrayOfInt[paramInt] = 1;
          if (!bool) {
            bool = true;
          } else {
            bool = false;
          }
        }
        i++;
      }
      if ((j != m) && ((j != m - 1) || (i != k))) {
        throw NotFoundException.getNotFoundInstance();
      }
      return;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  protected static void recordPatternInReverse(BitArray paramBitArray, int paramInt, int[] paramArrayOfInt)
    throws NotFoundException
  {
    int i = paramArrayOfInt.length;
    boolean bool = paramBitArray.get(paramInt);
    while ((paramInt > 0) && (i >= 0))
    {
      int j = paramInt - 1;
      paramInt = j;
      if (paramBitArray.get(j) != bool)
      {
        i--;
        if (!bool)
        {
          bool = true;
          paramInt = j;
        }
        else
        {
          bool = false;
          paramInt = j;
        }
      }
    }
    if (i < 0)
    {
      recordPattern(paramBitArray, paramInt + 1, paramArrayOfInt);
      return;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap)
    throws NotFoundException, FormatException
  {
    return decode(paramBinaryBitmap, null);
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, FormatException
  {
    try
    {
      Result localResult = doDecode(paramBinaryBitmap, paramMap);
      return localResult;
    }
    catch (NotFoundException localNotFoundException)
    {
      int j = 0;
      int i;
      if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.TRY_HARDER))) {
        i = 1;
      } else {
        i = 0;
      }
      Object localObject;
      if ((i != 0) && (paramBinaryBitmap.isRotateSupported()))
      {
        paramBinaryBitmap = paramBinaryBitmap.rotateCounterClockwise();
        paramMap = doDecode(paramBinaryBitmap, paramMap);
        localObject = paramMap.getResultMetadata();
        int k = 270;
        i = k;
        if (localObject != null)
        {
          i = k;
          if (((Map)localObject).containsKey(ResultMetadataType.ORIENTATION)) {
            i = (((Integer)((Map)localObject).get(ResultMetadataType.ORIENTATION)).intValue() + 270) % 360;
          }
        }
        paramMap.putMetadata(ResultMetadataType.ORIENTATION, Integer.valueOf(i));
        localObject = paramMap.getResultPoints();
        if (localObject != null)
        {
          k = paramBinaryBitmap.getHeight();
          for (i = j; i < localObject.length; i++) {
            localObject[i] = new ResultPoint(k - localObject[i].getY() - 1.0F, localObject[i].getX());
          }
        }
        return paramMap;
      }
      throw ((Throwable)localObject);
    }
  }
  
  public abstract Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, ChecksumException, FormatException;
  
  public void reset() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/OneDReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */