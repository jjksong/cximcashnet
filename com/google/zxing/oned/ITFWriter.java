package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Map;

public final class ITFWriter
  extends OneDimensionalCodeWriter
{
  private static final int[] END_PATTERN = { 3, 1, 1 };
  private static final int[] START_PATTERN = { 1, 1, 1, 1 };
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (paramBarcodeFormat == BarcodeFormat.ITF) {
      return super.encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap);
    }
    paramString = new StringBuilder("Can only encode ITF, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
  
  public boolean[] encode(String paramString)
  {
    int m = paramString.length();
    if (m % 2 == 0)
    {
      if (m <= 80)
      {
        boolean[] arrayOfBoolean = new boolean[m * 9 + 9];
        int i = appendPattern(arrayOfBoolean, 0, START_PATTERN, true);
        for (int j = 0; j < m; j += 2)
        {
          int n = Character.digit(paramString.charAt(j), 10);
          int i1 = Character.digit(paramString.charAt(j + 1), 10);
          int[] arrayOfInt = new int[18];
          for (int k = 0; k < 5; k++)
          {
            int i2 = k * 2;
            arrayOfInt[i2] = ITFReader.PATTERNS[n][k];
            arrayOfInt[(i2 + 1)] = ITFReader.PATTERNS[i1][k];
          }
          i += appendPattern(arrayOfBoolean, i, arrayOfInt, true);
        }
        appendPattern(arrayOfBoolean, i, END_PATTERN, true);
        return arrayOfBoolean;
      }
      paramString = new StringBuilder("Requested contents should be less than 80 digits long, but got ");
      paramString.append(m);
      throw new IllegalArgumentException(paramString.toString());
    }
    throw new IllegalArgumentException("The length of the input should be even");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/ITFWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */