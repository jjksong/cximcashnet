package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class Code128Reader
  extends OneDReader
{
  private static final int CODE_CODE_A = 101;
  private static final int CODE_CODE_B = 100;
  private static final int CODE_CODE_C = 99;
  private static final int CODE_FNC_1 = 102;
  private static final int CODE_FNC_2 = 97;
  private static final int CODE_FNC_3 = 96;
  private static final int CODE_FNC_4_A = 101;
  private static final int CODE_FNC_4_B = 100;
  static final int[][] CODE_PATTERNS;
  private static final int CODE_SHIFT = 98;
  private static final int CODE_START_A = 103;
  private static final int CODE_START_B = 104;
  private static final int CODE_START_C = 105;
  private static final int CODE_STOP = 106;
  private static final float MAX_AVG_VARIANCE = 0.25F;
  private static final float MAX_INDIVIDUAL_VARIANCE = 0.7F;
  
  static
  {
    int[] arrayOfInt1 = { 2, 1, 2, 2, 2, 2 };
    int[] arrayOfInt2 = { 1, 2, 2, 2, 1, 3 };
    int[] arrayOfInt3 = { 1, 2, 2, 3, 1, 2 };
    int[] arrayOfInt4 = { 1, 2, 2, 2, 3, 1 };
    int[] arrayOfInt5 = { 2, 2, 3, 2, 1, 1 };
    int[] arrayOfInt6 = { 2, 2, 1, 1, 3, 2 };
    int[] arrayOfInt7 = { 2, 2, 1, 2, 3, 1 };
    int[] arrayOfInt8 = { 2, 2, 3, 1, 1, 2 };
    int[] arrayOfInt9 = { 3, 1, 2, 1, 3, 1 };
    int[] arrayOfInt10 = { 3, 1, 1, 2, 2, 2 };
    int[] arrayOfInt11 = { 3, 2, 1, 1, 2, 2 };
    int[] arrayOfInt12 = { 3, 1, 2, 2, 1, 2 };
    int[] arrayOfInt13 = { 2, 1, 2, 1, 2, 3 };
    int[] arrayOfInt14 = { 1, 3, 2, 3, 1, 1 };
    int[] arrayOfInt15 = { 2, 3, 1, 1, 1, 3 };
    int[] arrayOfInt16 = { 2, 3, 1, 3, 1, 1 };
    int[] arrayOfInt17 = { 1, 1, 3, 1, 2, 3 };
    int[] arrayOfInt18 = { 1, 1, 3, 3, 2, 1 };
    int[] arrayOfInt19 = { 2, 3, 1, 1, 3, 1 };
    int[] arrayOfInt20 = { 3, 1, 1, 3, 2, 1 };
    int[] arrayOfInt21 = { 3, 3, 2, 1, 1, 1 };
    int[] arrayOfInt22 = { 2, 2, 1, 4, 1, 1 };
    int[] arrayOfInt23 = { 4, 3, 1, 1, 1, 1 };
    int[] arrayOfInt24 = { 1, 1, 1, 2, 2, 4 };
    int[] arrayOfInt25 = { 1, 1, 1, 4, 2, 2 };
    int[] arrayOfInt26 = { 1, 2, 1, 4, 2, 1 };
    int[] arrayOfInt27 = { 1, 4, 1, 2, 2, 1 };
    int[] arrayOfInt28 = { 1, 1, 2, 2, 1, 4 };
    int[] arrayOfInt29 = { 1, 1, 2, 4, 1, 2 };
    int[] arrayOfInt30 = { 1, 2, 2, 4, 1, 1 };
    int[] arrayOfInt31 = { 2, 4, 1, 2, 1, 1 };
    int[] arrayOfInt32 = { 4, 1, 3, 1, 1, 1 };
    int[] arrayOfInt33 = { 2, 4, 1, 1, 1, 2 };
    int[] arrayOfInt34 = { 1, 1, 1, 2, 4, 2 };
    int[] arrayOfInt35 = { 1, 2, 1, 2, 4, 1 };
    int[] arrayOfInt36 = { 1, 2, 4, 1, 1, 2 };
    int[] arrayOfInt37 = { 2, 1, 2, 1, 4, 1 };
    int[] arrayOfInt38 = { 2, 1, 4, 1, 2, 1 };
    int[] arrayOfInt39 = { 1, 1, 1, 3, 4, 1 };
    int[] arrayOfInt40 = { 1, 1, 4, 1, 1, 3 };
    int[] arrayOfInt41 = { 3, 1, 1, 1, 4, 1 };
    int[] arrayOfInt42 = { 2, 1, 1, 2, 1, 4 };
    int[] arrayOfInt43 = { 2, 3, 3, 1, 1, 1, 2 };
    CODE_PATTERNS = new int[][] { arrayOfInt1, { 2, 2, 2, 1, 2, 2 }, { 2, 2, 2, 2, 2, 1 }, { 1, 2, 1, 2, 2, 3 }, { 1, 2, 1, 3, 2, 2 }, { 1, 3, 1, 2, 2, 2 }, arrayOfInt2, arrayOfInt3, { 1, 3, 2, 2, 1, 2 }, { 2, 2, 1, 2, 1, 3 }, { 2, 2, 1, 3, 1, 2 }, { 2, 3, 1, 2, 1, 2 }, { 1, 1, 2, 2, 3, 2 }, { 1, 2, 2, 1, 3, 2 }, arrayOfInt4, { 1, 1, 3, 2, 2, 2 }, { 1, 2, 3, 1, 2, 2 }, { 1, 2, 3, 2, 2, 1 }, arrayOfInt5, arrayOfInt6, arrayOfInt7, { 2, 1, 3, 2, 1, 2 }, arrayOfInt8, arrayOfInt9, arrayOfInt10, arrayOfInt11, { 3, 2, 1, 2, 2, 1 }, arrayOfInt12, { 3, 2, 2, 1, 1, 2 }, { 3, 2, 2, 2, 1, 1 }, arrayOfInt13, { 2, 1, 2, 3, 2, 1 }, { 2, 3, 2, 1, 2, 1 }, { 1, 1, 1, 3, 2, 3 }, { 1, 3, 1, 1, 2, 3 }, { 1, 3, 1, 3, 2, 1 }, { 1, 1, 2, 3, 1, 3 }, { 1, 3, 2, 1, 1, 3 }, arrayOfInt14, { 2, 1, 1, 3, 1, 3 }, arrayOfInt15, arrayOfInt16, { 1, 1, 2, 1, 3, 3 }, { 1, 1, 2, 3, 3, 1 }, { 1, 3, 2, 1, 3, 1 }, arrayOfInt17, arrayOfInt18, { 1, 3, 3, 1, 2, 1 }, { 3, 1, 3, 1, 2, 1 }, { 2, 1, 1, 3, 3, 1 }, arrayOfInt19, { 2, 1, 3, 1, 1, 3 }, { 2, 1, 3, 3, 1, 1 }, { 2, 1, 3, 1, 3, 1 }, { 3, 1, 1, 1, 2, 3 }, arrayOfInt20, { 3, 3, 1, 1, 2, 1 }, { 3, 1, 2, 1, 1, 3 }, { 3, 1, 2, 3, 1, 1 }, arrayOfInt21, { 3, 1, 4, 1, 1, 1 }, arrayOfInt22, arrayOfInt23, arrayOfInt24, arrayOfInt25, { 1, 2, 1, 1, 2, 4 }, arrayOfInt26, { 1, 4, 1, 1, 2, 2 }, arrayOfInt27, arrayOfInt28, arrayOfInt29, { 1, 2, 2, 1, 1, 4 }, arrayOfInt30, { 1, 4, 2, 1, 1, 2 }, { 1, 4, 2, 2, 1, 1 }, arrayOfInt31, { 2, 2, 1, 1, 1, 4 }, arrayOfInt32, arrayOfInt33, { 1, 3, 4, 1, 1, 1 }, arrayOfInt34, { 1, 2, 1, 1, 4, 2 }, arrayOfInt35, { 1, 1, 4, 2, 1, 2 }, arrayOfInt36, { 1, 2, 4, 2, 1, 1 }, { 4, 1, 1, 2, 1, 2 }, { 4, 2, 1, 1, 1, 2 }, { 4, 2, 1, 2, 1, 1 }, arrayOfInt37, arrayOfInt38, { 4, 1, 2, 1, 2, 1 }, { 1, 1, 1, 1, 4, 3 }, arrayOfInt39, { 1, 3, 1, 1, 4, 1 }, arrayOfInt40, { 1, 1, 4, 3, 1, 1 }, { 4, 1, 1, 1, 1, 3 }, { 4, 1, 1, 3, 1, 1 }, { 1, 1, 3, 1, 4, 1 }, { 1, 1, 4, 1, 3, 1 }, arrayOfInt41, { 4, 1, 1, 1, 3, 1 }, { 2, 1, 1, 4, 1, 2 }, arrayOfInt42, { 2, 1, 1, 2, 3, 2 }, arrayOfInt43 };
  }
  
  private static int decodeCode(BitArray paramBitArray, int[] paramArrayOfInt, int paramInt)
    throws NotFoundException
  {
    recordPattern(paramBitArray, paramInt, paramArrayOfInt);
    float f2 = 0.25F;
    int i = -1;
    paramInt = 0;
    for (;;)
    {
      paramBitArray = CODE_PATTERNS;
      if (paramInt >= paramBitArray.length) {
        break;
      }
      float f3 = patternMatchVariance(paramArrayOfInt, paramBitArray[paramInt], 0.7F);
      float f1 = f2;
      if (f3 < f2)
      {
        i = paramInt;
        f1 = f3;
      }
      paramInt++;
      f2 = f1;
    }
    if (i >= 0) {
      return i;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static int[] findStartPattern(BitArray paramBitArray)
    throws NotFoundException
  {
    int i2 = paramBitArray.getSize();
    int k = paramBitArray.getNextSet(0);
    int[] arrayOfInt = new int[6];
    int i = k;
    int m = 0;
    int n = 0;
    while (k < i2)
    {
      int j;
      if ((paramBitArray.get(k) ^ m))
      {
        arrayOfInt[n] += 1;
        j = i;
      }
      else
      {
        if (n == 5)
        {
          float f1 = 0.25F;
          j = 103;
          int i1 = -1;
          while (j <= 105)
          {
            float f3 = patternMatchVariance(arrayOfInt, CODE_PATTERNS[j], 0.7F);
            float f2 = f1;
            if (f3 < f1)
            {
              i1 = j;
              f2 = f3;
            }
            j++;
            f1 = f2;
          }
          if ((i1 >= 0) && (paramBitArray.isRange(Math.max(0, i - (k - i) / 2), i, false))) {
            return new int[] { i, k, i1 };
          }
          j = i + (arrayOfInt[0] + arrayOfInt[1]);
          System.arraycopy(arrayOfInt, 2, arrayOfInt, 0, 4);
          arrayOfInt[4] = 0;
          arrayOfInt[5] = 0;
          i = n - 1;
        }
        else
        {
          n++;
          j = i;
          i = n;
        }
        arrayOfInt[i] = 1;
        m ^= 0x1;
        n = i;
      }
      k++;
      i = j;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, FormatException, ChecksumException
  {
    int i5;
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.ASSUME_GS1))) {
      i5 = 1;
    } else {
      i5 = 0;
    }
    Object localObject3 = findStartPattern(paramBitArray);
    int i3 = localObject3[2];
    paramMap = new ArrayList(20);
    paramMap.add(Byte.valueOf((byte)i3));
    switch (i3)
    {
    default: 
      throw FormatException.getFormatInstance();
    case 105: 
      i = 99;
      break;
    case 104: 
      i = 100;
      break;
    case 103: 
      i = 101;
    }
    Object localObject1 = new StringBuilder(20);
    int i10 = localObject3[0];
    int i1 = localObject3[1];
    Object localObject2 = new int[6];
    int k = i;
    int i = 0;
    int i2 = 0;
    int i11 = 0;
    int n = 0;
    int j = 0;
    int i4 = 0;
    int m = 1;
    int i6 = 0;
    for (;;)
    {
      int i7 = j;
      if (i2 != 0) {
        break;
      }
      i11 = decodeCode(paramBitArray, (int[])localObject2, i1);
      paramMap.add(Byte.valueOf((byte)i11));
      if (i11 != 106) {
        m = 1;
      }
      int i9 = i3;
      int i8 = i4;
      if (i11 != 106)
      {
        i8 = i4 + 1;
        i9 = i3 + i8 * i11;
      }
      i3 = i1;
      for (j = 0; j < 6; j++) {
        i3 += localObject2[j];
      }
      switch (i11)
      {
      default: 
        switch (k)
        {
        default: 
          j = m;
        }
        break;
      case 103: 
      case 104: 
      case 105: 
        throw FormatException.getFormatInstance();
        if (i11 < 64)
        {
          if (i == n) {
            ((StringBuilder)localObject1).append((char)(i11 + 32));
          } else {
            ((StringBuilder)localObject1).append((char)(i11 + 32 + 128));
          }
          i = 0;
          j = 0;
        }
        else if (i11 < 96)
        {
          if (i == n) {
            ((StringBuilder)localObject1).append((char)(i11 - 64));
          } else {
            ((StringBuilder)localObject1).append((char)(i11 + 64));
          }
          i = 0;
          j = 0;
        }
        else
        {
          if (i11 != 106) {
            m = 0;
          }
          if (i11 != 106) {
            i4 = m;
          }
          switch (i11)
          {
          default: 
            j = i2;
            break;
          case 102: 
            j = i2;
            if (i5 != 0) {
              if (((StringBuilder)localObject1).length() == 0)
              {
                ((StringBuilder)localObject1).append("]C1");
                i4 = m;
              }
              else
              {
                ((StringBuilder)localObject1).append('\035');
                i4 = m;
              }
            }
            break;
          case 101: 
            if ((n == 0) && (i != 0))
            {
              i = 0;
              n = 1;
              j = 0;
            }
            else if ((n != 0) && (i != 0))
            {
              i = 0;
              n = 0;
              j = 0;
            }
            else
            {
              i = 0;
              j = 1;
            }
            break;
          case 100: 
            j = i;
            k = 100;
            i = 0;
            break;
          case 99: 
            j = i;
            k = 99;
            i = 0;
            break;
          case 98: 
            j = i;
            k = 100;
            i = 1;
            break label1174;
            j = 1;
            i4 = i;
            i = 0;
            i2 = j;
            j = i4;
            break label1174;
            if (i11 < 96)
            {
              if (i == n) {
                ((StringBuilder)localObject1).append((char)(i11 + 32));
              } else {
                ((StringBuilder)localObject1).append((char)(i11 + 32 + 128));
              }
              i = 0;
              j = 0;
            }
            else
            {
              if (i11 != 106) {
                m = 0;
              }
              if (i11 != 106)
              {
                i4 = m;
                switch (i11)
                {
                default: 
                  j = i2;
                  break;
                case 102: 
                  j = i2;
                  if (i5 == 0) {
                    break label962;
                  }
                  if (((StringBuilder)localObject1).length() == 0)
                  {
                    ((StringBuilder)localObject1).append("]C1");
                    i4 = m;
                  }
                  else
                  {
                    ((StringBuilder)localObject1).append('\035');
                    i4 = m;
                  }
                  break;
                case 101: 
                  j = i;
                  k = 101;
                  i = 0;
                  break;
                case 100: 
                  if ((n == 0) && (i != 0))
                  {
                    i = 0;
                    n = 1;
                    j = 0;
                  }
                  else if ((n != 0) && (i != 0))
                  {
                    i = 0;
                    n = 0;
                    j = 0;
                  }
                  else
                  {
                    i = 0;
                    j = 1;
                  }
                  break;
                case 99: 
                  j = i;
                  k = 99;
                  i = 0;
                  break;
                case 98: 
                  j = i;
                  k = 101;
                  i = 1;
                }
              }
            }
            break;
          }
          j = i;
          i = 0;
          m = i4;
          break label1174;
          j = 1;
          label962:
          i4 = i;
          i = 0;
          i2 = j;
          j = i4;
          break label1174;
          if (i11 < 100)
          {
            if (i11 < 10) {
              ((StringBuilder)localObject1).append('0');
            }
            ((StringBuilder)localObject1).append(i11);
            j = m;
          }
          else
          {
            if (i11 != 106) {
              m = 0;
            }
            if (i11 != 106)
            {
              switch (i11)
              {
              default: 
                j = m;
                break;
              case 102: 
                j = m;
                if (i5 == 0) {
                  break;
                }
                if (((StringBuilder)localObject1).length() == 0)
                {
                  ((StringBuilder)localObject1).append("]C1");
                  j = m;
                }
                else
                {
                  ((StringBuilder)localObject1).append('\035');
                  j = m;
                }
                break;
              case 101: 
                j = i;
                k = 101;
                i = 0;
                break;
              case 100: 
                j = i;
                k = 100;
                i = 0;
                break;
              }
            }
            else
            {
              j = i;
              i = 0;
              i2 = 1;
            }
          }
        }
        break;
      }
      i4 = 0;
      m = j;
      j = i;
      i = i4;
      label1174:
      if (i6 != 0) {
        if (k == 101) {
          k = 100;
        } else {
          k = 101;
        }
      }
      i6 = i;
      i = j;
      i10 = i1;
      i1 = i3;
      j = i11;
      i11 = i7;
      i3 = i9;
      i4 = i8;
    }
    i = paramBitArray.getNextUnset(i1);
    if (paramBitArray.isRange(i, Math.min(paramBitArray.getSize(), (i - i10) / 2 + i), false))
    {
      if ((i3 - i4 * i11) % 103 == i11)
      {
        i = ((StringBuilder)localObject1).length();
        if (i != 0)
        {
          if ((i > 0) && (m != 0)) {
            if (k == 99) {
              ((StringBuilder)localObject1).delete(i - 2, i);
            } else {
              ((StringBuilder)localObject1).delete(i - 1, i);
            }
          }
          float f4 = (localObject3[1] + localObject3[0]) / 2.0F;
          float f1 = i10;
          float f3 = (i1 - i10) / 2.0F;
          j = paramMap.size();
          paramBitArray = new byte[j];
          for (i = 0; i < j; i++) {
            paramBitArray[i] = ((Byte)paramMap.get(i)).byteValue();
          }
          paramMap = ((StringBuilder)localObject1).toString();
          float f2 = paramInt;
          localObject3 = new ResultPoint(f4, f2);
          localObject1 = new ResultPoint(f1 + f3, f2);
          localObject2 = BarcodeFormat.CODE_128;
          return new Result(paramMap, paramBitArray, new ResultPoint[] { localObject3, localObject1 }, (BarcodeFormat)localObject2);
        }
        throw NotFoundException.getNotFoundInstance();
      }
      throw ChecksumException.getChecksumInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/Code128Reader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */