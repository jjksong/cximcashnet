package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Map;

public final class UPCAWriter
  implements Writer
{
  private final EAN13Writer subWriter = new EAN13Writer();
  
  private static String preencode(String paramString)
  {
    int i = paramString.length();
    if (i == 11)
    {
      int j = 0;
      i = 0;
      while (j < 11)
      {
        int m = paramString.charAt(j);
        int k;
        if (j % 2 == 0) {
          k = 3;
        } else {
          k = 1;
        }
        i += (m - 48) * k;
        j++;
      }
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append((1000 - i) % 10);
      paramString = localStringBuilder.toString();
    }
    else
    {
      if (i != 12) {
        break label126;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder("0");
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
    label126:
    localStringBuilder = new StringBuilder("Requested contents should be 11 or 12 digits long, but got ");
    localStringBuilder.append(paramString.length());
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
    throws WriterException
  {
    return encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, null);
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    if (paramBarcodeFormat == BarcodeFormat.UPC_A) {
      return this.subWriter.encode(preencode(paramString), BarcodeFormat.EAN_13, paramInt1, paramInt2, paramMap);
    }
    paramString = new StringBuilder("Can only encode UPC-A, but got ");
    paramString.append(paramBarcodeFormat);
    throw new IllegalArgumentException(paramString.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/UPCAWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */