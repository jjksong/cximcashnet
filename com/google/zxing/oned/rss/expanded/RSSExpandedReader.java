package com.google.zxing.oned.rss.expanded;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.oned.rss.AbstractRSSReader;
import com.google.zxing.oned.rss.DataCharacter;
import com.google.zxing.oned.rss.FinderPattern;
import com.google.zxing.oned.rss.RSSUtils;
import com.google.zxing.oned.rss.expanded.decoders.AbstractExpandedDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class RSSExpandedReader
  extends AbstractRSSReader
{
  private static final int[] EVEN_TOTAL_SUBSET;
  private static final int[][] FINDER_PATTERNS;
  private static final int[][] FINDER_PATTERN_SEQUENCES;
  private static final int FINDER_PAT_A = 0;
  private static final int FINDER_PAT_B = 1;
  private static final int FINDER_PAT_C = 2;
  private static final int FINDER_PAT_D = 3;
  private static final int FINDER_PAT_E = 4;
  private static final int FINDER_PAT_F = 5;
  private static final int[] GSUM;
  private static final int MAX_PAIRS = 11;
  private static final int[] SYMBOL_WIDEST = { 7, 5, 4, 3, 1 };
  private static final int[][] WEIGHTS;
  private final List<ExpandedPair> pairs = new ArrayList(11);
  private final List<ExpandedRow> rows = new ArrayList();
  private final int[] startEnd = new int[2];
  private boolean startFromEven;
  
  static
  {
    EVEN_TOTAL_SUBSET = new int[] { 4, 20, 52, 104, 204 };
    GSUM = new int[] { 0, 348, 1388, 2948, 3988 };
    int[] arrayOfInt1 = { 3, 6, 4, 1 };
    int[] arrayOfInt2 = { 3, 4, 6, 1 };
    int[] arrayOfInt3 = { 2, 6, 5, 1 };
    FINDER_PATTERNS = new int[][] { { 1, 8, 4, 1 }, arrayOfInt1, arrayOfInt2, { 3, 2, 8, 1 }, arrayOfInt3, { 2, 2, 9, 1 } };
    arrayOfInt1 = new int[] { 1, 3, 9, 27, 81, 32, 96, 77 };
    arrayOfInt2 = new int[] { 20, 60, 180, 118, 143, 7, 21, 63 };
    arrayOfInt3 = new int[] { 193, 157, 49, 147, 19, 57, 171, 91 };
    int[] arrayOfInt4 = { 185, 133, 188, 142, 4, 12, 36, 108 };
    int[] arrayOfInt5 = { 46, 138, 203, 187, 139, 206, 196, 166 };
    int[] arrayOfInt6 = { 76, 17, 51, 153, 37, 111, 122, 155 };
    int[] arrayOfInt7 = { 43, 129, 176, 106, 107, 110, 119, 146 };
    int[] arrayOfInt8 = { 16, 48, 144, 10, 30, 90, 59, 177 };
    int[] arrayOfInt9 = { 109, 116, 137, 200, 178, 112, 125, 164 };
    int[] arrayOfInt10 = { 70, 210, 208, 202, 184, 130, 179, 115 };
    int[] arrayOfInt11 = { 134, 191, 151, 31, 93, 68, 204, 190 };
    int[] arrayOfInt12 = { 6, 18, 54, 162, 64, 192, 154, 40 };
    int[] arrayOfInt13 = { 120, 149, 25, 75, 14, 42, 126, 167 };
    int[] arrayOfInt14 = { 79, 26, 78, 23, 69, 207, 199, 175 };
    int[] arrayOfInt15 = { 103, 98, 83, 38, 114, 131, 182, 124 };
    int[] arrayOfInt16 = { 161, 61, 183, 127, 170, 88, 53, 159 };
    int[] arrayOfInt17 = { 55, 165, 73, 8, 24, 72, 5, 15 };
    WEIGHTS = new int[][] { arrayOfInt1, arrayOfInt2, { 189, 145, 13, 39, 117, 140, 209, 205 }, arrayOfInt3, { 62, 186, 136, 197, 169, 85, 44, 132 }, arrayOfInt4, { 113, 128, 173, 97, 80, 29, 87, 50 }, { 150, 28, 84, 41, 123, 158, 52, 156 }, arrayOfInt5, arrayOfInt6, arrayOfInt7, arrayOfInt8, arrayOfInt9, arrayOfInt10, arrayOfInt11, { 148, 22, 66, 198, 172, 94, 71, 2 }, arrayOfInt12, arrayOfInt13, arrayOfInt14, arrayOfInt15, arrayOfInt16, arrayOfInt17, { 45, 135, 194, 160, 58, 174, 100, 89 } };
    arrayOfInt1 = new int[] { 0, 4, 1, 3, 3, 5 };
    arrayOfInt2 = new int[] { 0, 0, 1, 1, 2, 2, 3, 3 };
    arrayOfInt3 = new int[] { 0, 0, 1, 1, 2, 2, 3, 4, 5, 5 };
    FINDER_PATTERN_SEQUENCES = new int[][] { { 0, 0 }, { 0, 1, 1 }, { 0, 2, 1, 3 }, { 0, 4, 1, 3, 2 }, arrayOfInt1, { 0, 4, 1, 3, 4, 5, 5 }, arrayOfInt2, { 0, 0, 1, 1, 2, 2, 3, 4, 4 }, arrayOfInt3, { 0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5 } };
  }
  
  private void adjustOddEvenCounts(int paramInt)
    throws NotFoundException
  {
    int i1 = MathUtils.sum(getOddCounts());
    int i2 = MathUtils.sum(getEvenCounts());
    int n = 0;
    int i;
    int j;
    if (i1 > 13)
    {
      i = 1;
      j = 0;
    }
    else if (i1 < 4)
    {
      i = 0;
      j = 1;
    }
    else
    {
      i = 0;
      j = 0;
    }
    int k;
    int m;
    if (i2 > 13)
    {
      k = 0;
      m = 1;
    }
    else if (i2 < 4)
    {
      k = 1;
      m = 0;
    }
    else
    {
      k = 0;
      m = 0;
    }
    int i3 = i1 + i2 - paramInt;
    if ((i1 & 0x1) == 1) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if ((i2 & 0x1) == 0) {
      n = 1;
    }
    if (i3 == 1)
    {
      if (paramInt != 0)
      {
        if (n == 0) {
          i = 1;
        } else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else if (n != 0) {
        m = 1;
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    else if (i3 == -1)
    {
      if (paramInt != 0)
      {
        if (n == 0) {
          j = 1;
        } else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else if (n != 0) {
        k = 1;
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    else
    {
      if (i3 != 0) {
        break label337;
      }
      if (paramInt != 0)
      {
        if (n != 0)
        {
          if (i1 < i2)
          {
            m = 1;
            j = 1;
          }
          else
          {
            k = 1;
            i = 1;
          }
        }
        else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else {
        if (n != 0) {
          break label333;
        }
      }
    }
    if (j != 0) {
      if (i == 0) {
        increment(getOddCounts(), getOddRoundingErrors());
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    if (i != 0) {
      decrement(getOddCounts(), getOddRoundingErrors());
    }
    if (k != 0) {
      if (m == 0) {
        increment(getEvenCounts(), getOddRoundingErrors());
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    if (m != 0) {
      decrement(getEvenCounts(), getEvenRoundingErrors());
    }
    return;
    label333:
    throw NotFoundException.getNotFoundInstance();
    label337:
    throw NotFoundException.getNotFoundInstance();
  }
  
  private boolean checkChecksum()
  {
    Object localObject = (ExpandedPair)this.pairs.get(0);
    DataCharacter localDataCharacter = ((ExpandedPair)localObject).getLeftChar();
    localObject = ((ExpandedPair)localObject).getRightChar();
    if (localObject == null) {
      return false;
    }
    int j = ((DataCharacter)localObject).getChecksumPortion();
    int k = 1;
    int i = 2;
    while (k < this.pairs.size())
    {
      localObject = (ExpandedPair)this.pairs.get(k);
      int n = j + ((ExpandedPair)localObject).getLeftChar().getChecksumPortion();
      int m = i + 1;
      localObject = ((ExpandedPair)localObject).getRightChar();
      j = n;
      i = m;
      if (localObject != null)
      {
        j = n + ((DataCharacter)localObject).getChecksumPortion();
        i = m + 1;
      }
      k++;
    }
    return (i - 4) * 211 + j % 211 == localDataCharacter.getValue();
  }
  
  private List<ExpandedPair> checkRows(List<ExpandedRow> paramList, int paramInt)
    throws NotFoundException
  {
    while (paramInt < this.rows.size())
    {
      Object localObject1 = (ExpandedRow)this.rows.get(paramInt);
      this.pairs.clear();
      Iterator localIterator = paramList.iterator();
      Object localObject2;
      while (localIterator.hasNext())
      {
        localObject2 = (ExpandedRow)localIterator.next();
        this.pairs.addAll(((ExpandedRow)localObject2).getPairs());
      }
      this.pairs.addAll(((ExpandedRow)localObject1).getPairs());
      if (isValidSequence(this.pairs))
      {
        if (checkChecksum()) {
          return this.pairs;
        }
        localObject2 = new ArrayList();
        ((List)localObject2).addAll(paramList);
        ((List)localObject2).add(localObject1);
      }
      try
      {
        localObject1 = checkRows((List)localObject2, paramInt + 1);
        return (List<ExpandedPair>)localObject1;
      }
      catch (NotFoundException localNotFoundException)
      {
        for (;;) {}
      }
      paramInt++;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private List<ExpandedPair> checkRows(boolean paramBoolean)
  {
    int i = this.rows.size();
    Object localObject1 = null;
    if (i > 25)
    {
      this.rows.clear();
      return null;
    }
    this.pairs.clear();
    if (paramBoolean) {
      Collections.reverse(this.rows);
    }
    try
    {
      Object localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject2 = checkRows((List)localObject2, 0);
      localObject1 = localObject2;
    }
    catch (NotFoundException localNotFoundException)
    {
      for (;;) {}
    }
    if (paramBoolean) {
      Collections.reverse(this.rows);
    }
    return (List<ExpandedPair>)localObject1;
  }
  
  static Result constructResult(List<ExpandedPair> paramList)
    throws NotFoundException, FormatException
  {
    String str = AbstractExpandedDecoder.createDecoder(BitArrayBuilder.buildBitArray(paramList)).parseInformation();
    ResultPoint[] arrayOfResultPoint = ((ExpandedPair)paramList.get(0)).getFinderPattern().getResultPoints();
    Object localObject1 = ((ExpandedPair)paramList.get(paramList.size() - 1)).getFinderPattern().getResultPoints();
    paramList = arrayOfResultPoint[0];
    ResultPoint localResultPoint = arrayOfResultPoint[1];
    arrayOfResultPoint = localObject1[0];
    Object localObject2 = localObject1[1];
    localObject1 = BarcodeFormat.RSS_EXPANDED;
    return new Result(str, null, new ResultPoint[] { paramList, localResultPoint, arrayOfResultPoint, localObject2 }, (BarcodeFormat)localObject1);
  }
  
  private void findNextPair(BitArray paramBitArray, List<ExpandedPair> paramList, int paramInt)
    throws NotFoundException
  {
    int[] arrayOfInt = getDecodeFinderCounters();
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    int i1 = paramBitArray.getSize();
    if (paramInt < 0) {
      if (paramList.isEmpty()) {
        paramInt = 0;
      } else {
        paramInt = ((ExpandedPair)paramList.get(paramList.size() - 1)).getFinderPattern().getStartEnd()[1];
      }
    }
    if (paramList.size() % 2 != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int k = i;
    if (this.startFromEven) {
      k = i ^ 0x1;
    }
    boolean bool;
    for (int i = 0; paramInt < i1; i = bool)
    {
      bool = paramBitArray.get(paramInt) ^ true;
      i = bool;
      if (!bool) {
        break;
      }
      paramInt++;
    }
    int j = paramInt;
    int n = 0;
    int m = paramInt;
    paramInt = j;
    j = n;
    while (m < i1)
    {
      if ((paramBitArray.get(m) ^ i))
      {
        arrayOfInt[j] += 1;
      }
      else
      {
        if (j == 3)
        {
          if (k != 0) {
            reverseCounters(arrayOfInt);
          }
          if (isFinderPattern(arrayOfInt))
          {
            paramBitArray = this.startEnd;
            paramBitArray[0] = paramInt;
            paramBitArray[1] = m;
            return;
          }
          if (k != 0) {
            reverseCounters(arrayOfInt);
          }
          paramInt += arrayOfInt[0] + arrayOfInt[1];
          arrayOfInt[0] = arrayOfInt[2];
          arrayOfInt[1] = arrayOfInt[3];
          arrayOfInt[2] = 0;
          arrayOfInt[3] = 0;
          j--;
        }
        else
        {
          j++;
        }
        arrayOfInt[j] = 1;
        if (i == 0) {
          i = 1;
        } else {
          i = 0;
        }
      }
      m++;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static int getNextSecondBar(BitArray paramBitArray, int paramInt)
  {
    if (paramBitArray.get(paramInt)) {
      paramInt = paramBitArray.getNextSet(paramBitArray.getNextUnset(paramInt));
    } else {
      paramInt = paramBitArray.getNextUnset(paramBitArray.getNextSet(paramInt));
    }
    return paramInt;
  }
  
  private static boolean isNotA1left(FinderPattern paramFinderPattern, boolean paramBoolean1, boolean paramBoolean2)
  {
    return (paramFinderPattern.getValue() != 0) || (!paramBoolean1) || (!paramBoolean2);
  }
  
  private static boolean isPartialRow(Iterable<ExpandedPair> paramIterable, Iterable<ExpandedRow> paramIterable1)
  {
    Iterator localIterator2 = paramIterable1.iterator();
    int i;
    label110:
    label121:
    do
    {
      boolean bool = localIterator2.hasNext();
      int j = 0;
      if (!bool) {
        break;
      }
      ExpandedRow localExpandedRow = (ExpandedRow)localIterator2.next();
      paramIterable1 = paramIterable.iterator();
      while (paramIterable1.hasNext())
      {
        ExpandedPair localExpandedPair = (ExpandedPair)paramIterable1.next();
        Iterator localIterator1 = localExpandedRow.getPairs().iterator();
        while (localIterator1.hasNext()) {
          if (localExpandedPair.equals((ExpandedPair)localIterator1.next()))
          {
            i = 1;
            break label110;
          }
        }
        i = 0;
        if (i == 0)
        {
          i = j;
          break label121;
        }
      }
      i = 1;
    } while (i == 0);
    return true;
    return false;
  }
  
  private static boolean isValidSequence(List<ExpandedPair> paramList)
  {
    label82:
    for (int[] arrayOfInt : FINDER_PATTERN_SEQUENCES) {
      if (paramList.size() <= arrayOfInt.length)
      {
        for (int j = 0; j < paramList.size(); j++) {
          if (((ExpandedPair)paramList.get(j)).getFinderPattern().getValue() != arrayOfInt[j])
          {
            j = 0;
            break label82;
          }
        }
        j = 1;
        if (j != 0) {
          return true;
        }
      }
    }
    return false;
  }
  
  private FinderPattern parseFoundFinderPattern(BitArray paramBitArray, int paramInt, boolean paramBoolean)
  {
    int i;
    int k;
    int j;
    if (paramBoolean)
    {
      for (i = this.startEnd[0] - 1; (i >= 0) && (!paramBitArray.get(i)); i--) {}
      k = i + 1;
      paramBitArray = this.startEnd;
      int m = paramBitArray[0];
      j = paramBitArray[1];
      i = k;
      k = m - k;
    }
    else
    {
      int[] arrayOfInt = this.startEnd;
      i = arrayOfInt[0];
      j = paramBitArray.getNextUnset(arrayOfInt[1] + 1);
      k = j - this.startEnd[1];
    }
    paramBitArray = getDecodeFinderCounters();
    System.arraycopy(paramBitArray, 0, paramBitArray, 1, paramBitArray.length - 1);
    paramBitArray[0] = k;
    try
    {
      k = parseFinderValue(paramBitArray, FINDER_PATTERNS);
      return new FinderPattern(k, new int[] { i, j }, i, j, paramInt);
    }
    catch (NotFoundException paramBitArray) {}
    return null;
  }
  
  private static void removePartialRows(List<ExpandedPair> paramList, List<ExpandedRow> paramList1)
  {
    paramList1 = paramList1.iterator();
    while (paramList1.hasNext())
    {
      Object localObject = (ExpandedRow)paramList1.next();
      if (((ExpandedRow)localObject).getPairs().size() != paramList.size())
      {
        Iterator localIterator2 = ((ExpandedRow)localObject).getPairs().iterator();
        int j;
        label129:
        do
        {
          boolean bool = localIterator2.hasNext();
          j = 0;
          i = 1;
          if (!bool) {
            break;
          }
          localObject = (ExpandedPair)localIterator2.next();
          Iterator localIterator1 = paramList.iterator();
          while (localIterator1.hasNext()) {
            if (((ExpandedPair)localObject).equals((ExpandedPair)localIterator1.next())) {
              break label129;
            }
          }
          i = 0;
        } while (i != 0);
        int i = j;
        break label140;
        i = 1;
        label140:
        if (i != 0) {
          paramList1.remove();
        }
      }
    }
  }
  
  private static void reverseCounters(int[] paramArrayOfInt)
  {
    int j = paramArrayOfInt.length;
    for (int i = 0; i < j / 2; i++)
    {
      int k = paramArrayOfInt[i];
      int m = j - i - 1;
      paramArrayOfInt[i] = paramArrayOfInt[m];
      paramArrayOfInt[m] = k;
    }
  }
  
  private void storeRow(int paramInt, boolean paramBoolean)
  {
    boolean bool3 = false;
    int i = 0;
    boolean bool1 = false;
    boolean bool2;
    for (;;)
    {
      bool2 = bool3;
      if (i >= this.rows.size()) {
        break;
      }
      ExpandedRow localExpandedRow = (ExpandedRow)this.rows.get(i);
      if (localExpandedRow.getRowNumber() > paramInt)
      {
        bool2 = localExpandedRow.isEquivalent(this.pairs);
        break;
      }
      bool1 = localExpandedRow.isEquivalent(this.pairs);
      i++;
    }
    if ((!bool2) && (!bool1))
    {
      if (isPartialRow(this.pairs, this.rows)) {
        return;
      }
      this.rows.add(i, new ExpandedRow(this.pairs, paramInt, paramBoolean));
      removePartialRows(this.pairs, this.rows);
      return;
    }
  }
  
  DataCharacter decodeDataCharacter(BitArray paramBitArray, FinderPattern paramFinderPattern, boolean paramBoolean1, boolean paramBoolean2)
    throws NotFoundException
  {
    int[] arrayOfInt1 = getDataCharacterCounters();
    arrayOfInt1[0] = 0;
    arrayOfInt1[1] = 0;
    arrayOfInt1[2] = 0;
    arrayOfInt1[3] = 0;
    arrayOfInt1[4] = 0;
    arrayOfInt1[5] = 0;
    arrayOfInt1[6] = 0;
    arrayOfInt1[7] = 0;
    int i;
    int j;
    int k;
    if (paramBoolean2)
    {
      recordPatternInReverse(paramBitArray, paramFinderPattern.getStartEnd()[0], arrayOfInt1);
    }
    else
    {
      recordPattern(paramBitArray, paramFinderPattern.getStartEnd()[1], arrayOfInt1);
      i = arrayOfInt1.length - 1;
      j = 0;
      while (j < i)
      {
        k = arrayOfInt1[j];
        arrayOfInt1[j] = arrayOfInt1[i];
        arrayOfInt1[i] = k;
        j++;
        i--;
      }
    }
    float f1 = MathUtils.sum(arrayOfInt1) / 17.0F;
    float f2 = (paramFinderPattern.getStartEnd()[1] - paramFinderPattern.getStartEnd()[0]) / 15.0F;
    if (Math.abs(f1 - f2) / f2 <= 0.3F)
    {
      int[] arrayOfInt2 = getOddCounts();
      int[] arrayOfInt3 = getEvenCounts();
      paramBitArray = getOddRoundingErrors();
      float[] arrayOfFloat = getEvenRoundingErrors();
      for (j = 0; j < arrayOfInt1.length; j++)
      {
        f2 = arrayOfInt1[j] * 1.0F / f1;
        k = (int)(0.5F + f2);
        if (k <= 0)
        {
          if (f2 >= 0.3F) {
            i = 1;
          } else {
            throw NotFoundException.getNotFoundInstance();
          }
        }
        else
        {
          i = k;
          if (k > 8) {
            if (f2 <= 8.7F) {
              i = 8;
            } else {
              throw NotFoundException.getNotFoundInstance();
            }
          }
        }
        k = j / 2;
        if ((j & 0x1) == 0)
        {
          arrayOfInt2[k] = i;
          paramBitArray[k] = (f2 - i);
        }
        else
        {
          arrayOfInt3[k] = i;
          arrayOfFloat[k] = (f2 - i);
        }
      }
      adjustOddEvenCounts(17);
      j = paramFinderPattern.getValue();
      if (paramBoolean1) {
        i = 0;
      } else {
        i = 2;
      }
      int i1 = j * 4 + i + (paramBoolean2 ^ true) - 1;
      k = arrayOfInt2.length - 1;
      i = 0;
      j = 0;
      while (k >= 0)
      {
        m = i;
        if (isNotA1left(paramFinderPattern, paramBoolean1, paramBoolean2))
        {
          m = WEIGHTS[i1][(k * 2)];
          m = i + arrayOfInt2[k] * m;
        }
        j += arrayOfInt2[k];
        k--;
        i = m;
      }
      int m = arrayOfInt3.length - 1;
      int n;
      for (k = 0; m >= 0; k = n)
      {
        n = k;
        if (isNotA1left(paramFinderPattern, paramBoolean1, paramBoolean2))
        {
          n = WEIGHTS[i1][(m * 2 + 1)];
          n = k + arrayOfInt3[m] * n;
        }
        m--;
      }
      if (((j & 0x1) == 0) && (j <= 13) && (j >= 4))
      {
        m = (13 - j) / 2;
        n = SYMBOL_WIDEST[m];
        j = RSSUtils.getRSSvalue(arrayOfInt2, n, true);
        n = RSSUtils.getRSSvalue(arrayOfInt3, 9 - n, false);
        return new DataCharacter(j * EVEN_TOTAL_SUBSET[m] + n + GSUM[m], i + k);
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, FormatException
  {
    this.pairs.clear();
    this.startFromEven = false;
    try
    {
      paramMap = constructResult(decodeRow2pairs(paramInt, paramBitArray));
      return paramMap;
    }
    catch (NotFoundException paramMap)
    {
      this.pairs.clear();
      this.startFromEven = true;
    }
    return constructResult(decodeRow2pairs(paramInt, paramBitArray));
  }
  
  /* Error */
  List<ExpandedPair> decodeRow2pairs(int paramInt, BitArray paramBitArray)
    throws NotFoundException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: aload_0
    //   3: getfield 61	com/google/zxing/oned/rss/expanded/RSSExpandedReader:pairs	Ljava/util/List;
    //   6: iload_1
    //   7: invokevirtual 364	com/google/zxing/oned/rss/expanded/RSSExpandedReader:retrieveNextPair	(Lcom/google/zxing/common/BitArray;Ljava/util/List;I)Lcom/google/zxing/oned/rss/expanded/ExpandedPair;
    //   10: astore 4
    //   12: aload_0
    //   13: getfield 61	com/google/zxing/oned/rss/expanded/RSSExpandedReader:pairs	Ljava/util/List;
    //   16: aload 4
    //   18: invokeinterface 168 2 0
    //   23: pop
    //   24: goto -24 -> 0
    //   27: astore_2
    //   28: aload_0
    //   29: getfield 61	com/google/zxing/oned/rss/expanded/RSSExpandedReader:pairs	Ljava/util/List;
    //   32: invokeinterface 237 1 0
    //   37: ifne +65 -> 102
    //   40: aload_0
    //   41: invokespecial 164	com/google/zxing/oned/rss/expanded/RSSExpandedReader:checkChecksum	()Z
    //   44: ifeq +8 -> 52
    //   47: aload_0
    //   48: getfield 61	com/google/zxing/oned/rss/expanded/RSSExpandedReader:pairs	Ljava/util/List;
    //   51: areturn
    //   52: aload_0
    //   53: getfield 64	com/google/zxing/oned/rss/expanded/RSSExpandedReader:rows	Ljava/util/List;
    //   56: invokeinterface 237 1 0
    //   61: istore_3
    //   62: aload_0
    //   63: iload_1
    //   64: iconst_0
    //   65: invokespecial 366	com/google/zxing/oned/rss/expanded/RSSExpandedReader:storeRow	(IZ)V
    //   68: iload_3
    //   69: iconst_1
    //   70: ixor
    //   71: ifeq +27 -> 98
    //   74: aload_0
    //   75: iconst_0
    //   76: invokespecial 368	com/google/zxing/oned/rss/expanded/RSSExpandedReader:checkRows	(Z)Ljava/util/List;
    //   79: astore_2
    //   80: aload_2
    //   81: ifnull +5 -> 86
    //   84: aload_2
    //   85: areturn
    //   86: aload_0
    //   87: iconst_1
    //   88: invokespecial 368	com/google/zxing/oned/rss/expanded/RSSExpandedReader:checkRows	(Z)Ljava/util/List;
    //   91: astore_2
    //   92: aload_2
    //   93: ifnull +5 -> 98
    //   96: aload_2
    //   97: areturn
    //   98: invokestatic 86	com/google/zxing/NotFoundException:getNotFoundInstance	()Lcom/google/zxing/NotFoundException;
    //   101: athrow
    //   102: aload_2
    //   103: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	104	0	this	RSSExpandedReader
    //   0	104	1	paramInt	int
    //   0	104	2	paramBitArray	BitArray
    //   61	10	3	bool	boolean
    //   10	7	4	localExpandedPair	ExpandedPair
    // Exception table:
    //   from	to	target	type
    //   0	24	27	com/google/zxing/NotFoundException
  }
  
  List<ExpandedRow> getRows()
  {
    return this.rows;
  }
  
  public void reset()
  {
    this.pairs.clear();
    this.rows.clear();
  }
  
  ExpandedPair retrieveNextPair(BitArray paramBitArray, List<ExpandedPair> paramList, int paramInt)
    throws NotFoundException
  {
    boolean bool1;
    if (paramList.size() % 2 == 0) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    boolean bool2 = bool1;
    if (this.startFromEven) {
      bool2 = bool1 ^ true;
    }
    int j = -1;
    int i = 1;
    FinderPattern localFinderPattern;
    int k;
    do
    {
      findNextPair(paramBitArray, paramList, j);
      localFinderPattern = parseFoundFinderPattern(paramBitArray, paramInt, bool2);
      if (localFinderPattern == null)
      {
        j = getNextSecondBar(paramBitArray, this.startEnd[0]);
        k = i;
      }
      else
      {
        k = 0;
      }
      i = k;
    } while (k != 0);
    DataCharacter localDataCharacter = decodeDataCharacter(paramBitArray, localFinderPattern, bool2, true);
    if ((!paramList.isEmpty()) && (((ExpandedPair)paramList.get(paramList.size() - 1)).mustBeLast())) {
      throw NotFoundException.getNotFoundInstance();
    }
    try
    {
      paramBitArray = decodeDataCharacter(paramBitArray, localFinderPattern, bool2, false);
    }
    catch (NotFoundException paramBitArray)
    {
      paramBitArray = null;
    }
    return new ExpandedPair(localDataCharacter, paramBitArray, localFinderPattern, true);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/expanded/RSSExpandedReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */