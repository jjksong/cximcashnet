package com.google.zxing.oned.rss.expanded.decoders;

import com.google.zxing.NotFoundException;

final class FieldParser
{
  private static final Object[][] FOUR_DIGIT_DATA_LENGTH;
  private static final Object[][] THREE_DIGIT_DATA_LENGTH;
  private static final Object[][] THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH;
  private static final Object[][] TWO_DIGIT_DATA_LENGTH;
  private static final Object VARIABLE_LENGTH = new Object();
  
  static
  {
    Object localObject10 = { "00", Integer.valueOf(18) };
    Object localObject11 = { "01", Integer.valueOf(14) };
    Object localObject3 = VARIABLE_LENGTH;
    Object localObject12 = { "13", Integer.valueOf(6) };
    Object localObject4 = VARIABLE_LENGTH;
    Object localObject9 = VARIABLE_LENGTH;
    Object localObject13 = { "30", VARIABLE_LENGTH, Integer.valueOf(8) };
    Object localObject5 = VARIABLE_LENGTH;
    Object[] arrayOfObject1 = { "90", VARIABLE_LENGTH, Integer.valueOf(30) };
    Object[] arrayOfObject2 = { "91", VARIABLE_LENGTH, Integer.valueOf(30) };
    Object localObject6 = VARIABLE_LENGTH;
    Object localObject1 = VARIABLE_LENGTH;
    Object localObject7 = VARIABLE_LENGTH;
    Object localObject2 = VARIABLE_LENGTH;
    Object[] arrayOfObject3 = { "96", VARIABLE_LENGTH, Integer.valueOf(30) };
    Object[] arrayOfObject4 = { "97", VARIABLE_LENGTH, Integer.valueOf(30) };
    Object[] arrayOfObject5 = { "98", VARIABLE_LENGTH, Integer.valueOf(30) };
    Object localObject8 = VARIABLE_LENGTH;
    TWO_DIGIT_DATA_LENGTH = new Object[][] { localObject10, localObject11, { "02", Integer.valueOf(14) }, { "10", localObject3, Integer.valueOf(20) }, { "11", Integer.valueOf(6) }, { "12", Integer.valueOf(6) }, localObject12, { "15", Integer.valueOf(6) }, { "17", Integer.valueOf(6) }, { "20", Integer.valueOf(2) }, { "21", localObject4, Integer.valueOf(20) }, { "22", localObject9, Integer.valueOf(29) }, localObject13, { "37", localObject5, Integer.valueOf(8) }, arrayOfObject1, arrayOfObject2, { "92", localObject6, Integer.valueOf(30) }, { "93", localObject1, Integer.valueOf(30) }, { "94", localObject7, Integer.valueOf(30) }, { "95", localObject2, Integer.valueOf(30) }, arrayOfObject3, arrayOfObject4, arrayOfObject5, { "99", localObject8, Integer.valueOf(30) } };
    localObject9 = VARIABLE_LENGTH;
    localObject6 = VARIABLE_LENGTH;
    localObject10 = VARIABLE_LENGTH;
    localObject4 = VARIABLE_LENGTH;
    localObject3 = VARIABLE_LENGTH;
    localObject11 = VARIABLE_LENGTH;
    localObject8 = VARIABLE_LENGTH;
    localObject1 = VARIABLE_LENGTH;
    localObject13 = VARIABLE_LENGTH;
    arrayOfObject1 = new Object[] { "402", Integer.valueOf(17) };
    localObject5 = VARIABLE_LENGTH;
    localObject2 = VARIABLE_LENGTH;
    localObject7 = VARIABLE_LENGTH;
    localObject12 = VARIABLE_LENGTH;
    THREE_DIGIT_DATA_LENGTH = new Object[][] { { "240", localObject9, Integer.valueOf(30) }, { "241", localObject6, Integer.valueOf(30) }, { "242", localObject10, Integer.valueOf(6) }, { "250", localObject4, Integer.valueOf(30) }, { "251", localObject3, Integer.valueOf(30) }, { "253", localObject11, Integer.valueOf(17) }, { "254", localObject8, Integer.valueOf(20) }, { "400", localObject1, Integer.valueOf(30) }, { "401", localObject13, Integer.valueOf(30) }, arrayOfObject1, { "403", localObject5, Integer.valueOf(30) }, { "410", Integer.valueOf(13) }, { "411", Integer.valueOf(13) }, { "412", Integer.valueOf(13) }, { "413", Integer.valueOf(13) }, { "414", Integer.valueOf(13) }, { "420", localObject2, Integer.valueOf(20) }, { "421", localObject7, Integer.valueOf(15) }, { "422", Integer.valueOf(3) }, { "423", localObject12, Integer.valueOf(15) }, { "424", Integer.valueOf(3) }, { "425", Integer.valueOf(3) }, { "426", Integer.valueOf(3) } };
    localObject2 = new Object[] { "311", Integer.valueOf(6) };
    localObject3 = new Object[] { "314", Integer.valueOf(6) };
    localObject4 = new Object[] { "316", Integer.valueOf(6) };
    localObject5 = new Object[] { "320", Integer.valueOf(6) };
    localObject6 = new Object[] { "321", Integer.valueOf(6) };
    localObject7 = new Object[] { "324", Integer.valueOf(6) };
    localObject8 = new Object[] { "328", Integer.valueOf(6) };
    localObject9 = new Object[] { "329", Integer.valueOf(6) };
    localObject10 = new Object[] { "331", Integer.valueOf(6) };
    localObject11 = new Object[] { "335", Integer.valueOf(6) };
    localObject12 = new Object[] { "342", Integer.valueOf(6) };
    localObject13 = new Object[] { "343", Integer.valueOf(6) };
    arrayOfObject1 = new Object[] { "346", Integer.valueOf(6) };
    arrayOfObject2 = new Object[] { "347", Integer.valueOf(6) };
    arrayOfObject3 = new Object[] { "349", Integer.valueOf(6) };
    arrayOfObject4 = new Object[] { "351", Integer.valueOf(6) };
    arrayOfObject5 = new Object[] { "352", Integer.valueOf(6) };
    Object[] arrayOfObject6 = { "353", Integer.valueOf(6) };
    Object[] arrayOfObject7 = { "357", Integer.valueOf(6) };
    Object[] arrayOfObject8 = { "360", Integer.valueOf(6) };
    Object[] arrayOfObject9 = { "362", Integer.valueOf(6) };
    Object[] arrayOfObject10 = { "364", Integer.valueOf(6) };
    Object[] arrayOfObject11 = { "368", Integer.valueOf(6) };
    Object[] arrayOfObject12 = { "369", Integer.valueOf(6) };
    Object[] arrayOfObject13 = { "390", VARIABLE_LENGTH, Integer.valueOf(15) };
    localObject1 = VARIABLE_LENGTH;
    Object[] arrayOfObject14 = { "392", VARIABLE_LENGTH, Integer.valueOf(15) };
    Object[] arrayOfObject15 = { "393", VARIABLE_LENGTH, Integer.valueOf(18) };
    Object[] arrayOfObject16 = { "703", VARIABLE_LENGTH, Integer.valueOf(30) };
    THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH = new Object[][] { { "310", Integer.valueOf(6) }, localObject2, { "312", Integer.valueOf(6) }, { "313", Integer.valueOf(6) }, localObject3, { "315", Integer.valueOf(6) }, localObject4, localObject5, localObject6, { "322", Integer.valueOf(6) }, { "323", Integer.valueOf(6) }, localObject7, { "325", Integer.valueOf(6) }, { "326", Integer.valueOf(6) }, { "327", Integer.valueOf(6) }, localObject8, localObject9, { "330", Integer.valueOf(6) }, localObject10, { "332", Integer.valueOf(6) }, { "333", Integer.valueOf(6) }, { "334", Integer.valueOf(6) }, localObject11, { "336", Integer.valueOf(6) }, { "340", Integer.valueOf(6) }, { "341", Integer.valueOf(6) }, localObject12, localObject13, { "344", Integer.valueOf(6) }, { "345", Integer.valueOf(6) }, arrayOfObject1, arrayOfObject2, { "348", Integer.valueOf(6) }, arrayOfObject3, { "350", Integer.valueOf(6) }, arrayOfObject4, arrayOfObject5, arrayOfObject6, { "354", Integer.valueOf(6) }, { "355", Integer.valueOf(6) }, { "356", Integer.valueOf(6) }, arrayOfObject7, arrayOfObject8, { "361", Integer.valueOf(6) }, arrayOfObject9, { "363", Integer.valueOf(6) }, arrayOfObject10, { "365", Integer.valueOf(6) }, { "366", Integer.valueOf(6) }, { "367", Integer.valueOf(6) }, arrayOfObject11, arrayOfObject12, arrayOfObject13, { "391", localObject1, Integer.valueOf(18) }, arrayOfObject14, arrayOfObject15, arrayOfObject16 };
    localObject1 = new Object[] { "7001", Integer.valueOf(13) };
    localObject2 = new Object[] { "7002", VARIABLE_LENGTH, Integer.valueOf(30) };
    localObject3 = new Object[] { "7003", Integer.valueOf(10) };
    localObject4 = new Object[] { "8001", Integer.valueOf(14) };
    localObject5 = new Object[] { "8002", VARIABLE_LENGTH, Integer.valueOf(20) };
    localObject6 = new Object[] { "8003", VARIABLE_LENGTH, Integer.valueOf(30) };
    localObject7 = new Object[] { "8004", VARIABLE_LENGTH, Integer.valueOf(30) };
    localObject8 = new Object[] { "8005", Integer.valueOf(6) };
    localObject9 = new Object[] { "8007", VARIABLE_LENGTH, Integer.valueOf(30) };
    localObject10 = new Object[] { "8008", VARIABLE_LENGTH, Integer.valueOf(12) };
    localObject11 = new Object[] { "8018", Integer.valueOf(18) };
    localObject12 = new Object[] { "8020", VARIABLE_LENGTH, Integer.valueOf(25) };
    localObject13 = new Object[] { "8100", Integer.valueOf(6) };
    arrayOfObject1 = new Object[] { "8101", Integer.valueOf(10) };
    arrayOfObject2 = new Object[] { "8102", Integer.valueOf(2) };
    arrayOfObject3 = new Object[] { "8110", VARIABLE_LENGTH, Integer.valueOf(70) };
    arrayOfObject4 = new Object[] { "8200", VARIABLE_LENGTH, Integer.valueOf(70) };
    FOUR_DIGIT_DATA_LENGTH = new Object[][] { localObject1, localObject2, localObject3, localObject4, localObject5, localObject6, localObject7, localObject8, { "8006", Integer.valueOf(18) }, localObject9, localObject10, localObject11, localObject12, localObject13, arrayOfObject1, arrayOfObject2, arrayOfObject3, arrayOfObject4 };
  }
  
  static String parseFieldsInGeneralPurpose(String paramString)
    throws NotFoundException
  {
    if (paramString.isEmpty()) {
      return null;
    }
    if (paramString.length() >= 2)
    {
      Object localObject2 = paramString.substring(0, 2);
      Object[][] arrayOfObject;
      for (arrayOfObject : TWO_DIGIT_DATA_LENGTH) {
        if (arrayOfObject[0].equals(localObject2))
        {
          if (arrayOfObject[1] == VARIABLE_LENGTH) {
            return processVariableAI(2, ((Integer)arrayOfObject[2]).intValue(), paramString);
          }
          return processFixedAI(2, ((Integer)arrayOfObject[1]).intValue(), paramString);
        }
      }
      if (paramString.length() >= 3)
      {
        ??? = paramString.substring(0, 3);
        for (localObject2 : THREE_DIGIT_DATA_LENGTH) {
          if (localObject2[0].equals(???))
          {
            if (localObject2[1] == VARIABLE_LENGTH) {
              return processVariableAI(3, ((Integer)localObject2[2]).intValue(), paramString);
            }
            return processFixedAI(3, ((Integer)localObject2[1]).intValue(), paramString);
          }
        }
        for (arrayOfObject : THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH) {
          if (arrayOfObject[0].equals(???))
          {
            if (arrayOfObject[1] == VARIABLE_LENGTH) {
              return processVariableAI(4, ((Integer)arrayOfObject[2]).intValue(), paramString);
            }
            return processFixedAI(4, ((Integer)arrayOfObject[1]).intValue(), paramString);
          }
        }
        if (paramString.length() >= 4)
        {
          ??? = paramString.substring(0, 4);
          for (arrayOfObject : FOUR_DIGIT_DATA_LENGTH) {
            if (arrayOfObject[0].equals(???))
            {
              if (arrayOfObject[1] == VARIABLE_LENGTH) {
                return processVariableAI(4, ((Integer)arrayOfObject[2]).intValue(), paramString);
              }
              return processFixedAI(4, ((Integer)arrayOfObject[1]).intValue(), paramString);
            }
          }
          throw NotFoundException.getNotFoundInstance();
        }
        throw NotFoundException.getNotFoundInstance();
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static String processFixedAI(int paramInt1, int paramInt2, String paramString)
    throws NotFoundException
  {
    if (paramString.length() >= paramInt1)
    {
      Object localObject = paramString.substring(0, paramInt1);
      int i = paramString.length();
      paramInt2 += paramInt1;
      if (i >= paramInt2)
      {
        String str = paramString.substring(paramInt1, paramInt2);
        paramString = paramString.substring(paramInt2);
        StringBuilder localStringBuilder = new StringBuilder("(");
        localStringBuilder.append((String)localObject);
        localStringBuilder.append(')');
        localStringBuilder.append(str);
        str = localStringBuilder.toString();
        paramString = parseFieldsInGeneralPurpose(paramString);
        if (paramString == null) {
          return str;
        }
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(paramString);
        return ((StringBuilder)localObject).toString();
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static String processVariableAI(int paramInt1, int paramInt2, String paramString)
    throws NotFoundException
  {
    Object localObject = paramString.substring(0, paramInt1);
    int j = paramString.length();
    int i = paramInt2 + paramInt1;
    paramInt2 = i;
    if (j < i) {
      paramInt2 = paramString.length();
    }
    String str = paramString.substring(paramInt1, paramInt2);
    paramString = paramString.substring(paramInt2);
    StringBuilder localStringBuilder = new StringBuilder("(");
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(')');
    localStringBuilder.append(str);
    str = localStringBuilder.toString();
    paramString = parseFieldsInGeneralPurpose(paramString);
    if (paramString == null) {
      return str;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(paramString);
    return ((StringBuilder)localObject).toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/expanded/decoders/FieldParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */