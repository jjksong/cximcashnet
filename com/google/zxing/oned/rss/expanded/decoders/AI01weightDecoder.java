package com.google.zxing.oned.rss.expanded.decoders;

import com.google.zxing.common.BitArray;

abstract class AI01weightDecoder
  extends AI01decoder
{
  AI01weightDecoder(BitArray paramBitArray)
  {
    super(paramBitArray);
  }
  
  protected abstract void addWeightCode(StringBuilder paramStringBuilder, int paramInt);
  
  protected abstract int checkWeight(int paramInt);
  
  final void encodeCompressedWeight(StringBuilder paramStringBuilder, int paramInt1, int paramInt2)
  {
    paramInt1 = getGeneralDecoder().extractNumericValueFromBitArray(paramInt1, paramInt2);
    addWeightCode(paramStringBuilder, paramInt1);
    int i = checkWeight(paramInt1);
    paramInt2 = 100000;
    for (paramInt1 = 0; paramInt1 < 5; paramInt1++)
    {
      if (i / paramInt2 == 0) {
        paramStringBuilder.append('0');
      }
      paramInt2 /= 10;
    }
    paramStringBuilder.append(i);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/expanded/decoders/AI01weightDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */