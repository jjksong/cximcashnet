package com.google.zxing.oned.rss.expanded;

import com.google.zxing.oned.rss.DataCharacter;
import com.google.zxing.oned.rss.FinderPattern;

final class ExpandedPair
{
  private final FinderPattern finderPattern;
  private final DataCharacter leftChar;
  private final boolean mayBeLast;
  private final DataCharacter rightChar;
  
  ExpandedPair(DataCharacter paramDataCharacter1, DataCharacter paramDataCharacter2, FinderPattern paramFinderPattern, boolean paramBoolean)
  {
    this.leftChar = paramDataCharacter1;
    this.rightChar = paramDataCharacter2;
    this.finderPattern = paramFinderPattern;
    this.mayBeLast = paramBoolean;
  }
  
  private static boolean equalsOrNull(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == null) {
      return paramObject2 == null;
    }
    return paramObject1.equals(paramObject2);
  }
  
  private static int hashNotNull(Object paramObject)
  {
    if (paramObject == null) {
      return 0;
    }
    return paramObject.hashCode();
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ExpandedPair)) {
      return false;
    }
    paramObject = (ExpandedPair)paramObject;
    return (equalsOrNull(this.leftChar, ((ExpandedPair)paramObject).leftChar)) && (equalsOrNull(this.rightChar, ((ExpandedPair)paramObject).rightChar)) && (equalsOrNull(this.finderPattern, ((ExpandedPair)paramObject).finderPattern));
  }
  
  FinderPattern getFinderPattern()
  {
    return this.finderPattern;
  }
  
  DataCharacter getLeftChar()
  {
    return this.leftChar;
  }
  
  DataCharacter getRightChar()
  {
    return this.rightChar;
  }
  
  public int hashCode()
  {
    return hashNotNull(this.leftChar) ^ hashNotNull(this.rightChar) ^ hashNotNull(this.finderPattern);
  }
  
  boolean mayBeLast()
  {
    return this.mayBeLast;
  }
  
  public boolean mustBeLast()
  {
    return this.rightChar == null;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[ ");
    localStringBuilder.append(this.leftChar);
    localStringBuilder.append(" , ");
    localStringBuilder.append(this.rightChar);
    localStringBuilder.append(" : ");
    Object localObject = this.finderPattern;
    if (localObject == null) {
      localObject = "null";
    } else {
      localObject = Integer.valueOf(((FinderPattern)localObject).getValue());
    }
    localStringBuilder.append(localObject);
    localStringBuilder.append(" ]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/expanded/ExpandedPair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */