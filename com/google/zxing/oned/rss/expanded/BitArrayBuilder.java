package com.google.zxing.oned.rss.expanded;

import com.google.zxing.common.BitArray;
import com.google.zxing.oned.rss.DataCharacter;
import java.util.List;

final class BitArrayBuilder
{
  static BitArray buildBitArray(List<ExpandedPair> paramList)
  {
    int j = (paramList.size() << 1) - 1;
    int i = j;
    if (((ExpandedPair)paramList.get(paramList.size() - 1)).getRightChar() == null) {
      i = j - 1;
    }
    BitArray localBitArray = new BitArray(i * 12);
    int k = ((ExpandedPair)paramList.get(0)).getRightChar().getValue();
    j = 11;
    i = 0;
    while (j >= 0)
    {
      if ((1 << j & k) != 0) {
        localBitArray.set(i);
      }
      i++;
      j--;
    }
    for (j = 1; j < paramList.size(); j++)
    {
      ExpandedPair localExpandedPair = (ExpandedPair)paramList.get(j);
      int m = localExpandedPair.getLeftChar().getValue();
      for (k = 11; k >= 0; k--)
      {
        if ((1 << k & m) != 0) {
          localBitArray.set(i);
        }
        i++;
      }
      if (localExpandedPair.getRightChar() != null)
      {
        m = localExpandedPair.getRightChar().getValue();
        for (k = 11; k >= 0; k--)
        {
          if ((1 << k & m) != 0) {
            localBitArray.set(i);
          }
          i++;
        }
      }
    }
    return localBitArray;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/expanded/BitArrayBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */