package com.google.zxing.oned.rss;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.oned.OneDReader;

public abstract class AbstractRSSReader
  extends OneDReader
{
  private static final float MAX_AVG_VARIANCE = 0.2F;
  private static final float MAX_FINDER_PATTERN_RATIO = 0.89285713F;
  private static final float MAX_INDIVIDUAL_VARIANCE = 0.45F;
  private static final float MIN_FINDER_PATTERN_RATIO = 0.7916667F;
  private final int[] dataCharacterCounters = new int[8];
  private final int[] decodeFinderCounters = new int[4];
  private final int[] evenCounts;
  private final float[] evenRoundingErrors = new float[4];
  private final int[] oddCounts;
  private final float[] oddRoundingErrors = new float[4];
  
  protected AbstractRSSReader()
  {
    int[] arrayOfInt = this.dataCharacterCounters;
    this.oddCounts = new int[arrayOfInt.length / 2];
    this.evenCounts = new int[arrayOfInt.length / 2];
  }
  
  @Deprecated
  protected static int count(int[] paramArrayOfInt)
  {
    return MathUtils.sum(paramArrayOfInt);
  }
  
  protected static void decrement(int[] paramArrayOfInt, float[] paramArrayOfFloat)
  {
    float f2 = paramArrayOfFloat[0];
    int i = 1;
    int j = 0;
    while (i < paramArrayOfInt.length)
    {
      float f1 = f2;
      if (paramArrayOfFloat[i] < f2)
      {
        f1 = paramArrayOfFloat[i];
        j = i;
      }
      i++;
      f2 = f1;
    }
    paramArrayOfInt[j] -= 1;
  }
  
  protected static void increment(int[] paramArrayOfInt, float[] paramArrayOfFloat)
  {
    float f1 = paramArrayOfFloat[0];
    int i = 1;
    int j = 0;
    while (i < paramArrayOfInt.length)
    {
      float f2 = f1;
      if (paramArrayOfFloat[i] > f1)
      {
        f2 = paramArrayOfFloat[i];
        j = i;
      }
      i++;
      f1 = f2;
    }
    paramArrayOfInt[j] += 1;
  }
  
  protected static boolean isFinderPattern(int[] paramArrayOfInt)
  {
    int j = paramArrayOfInt[0] + paramArrayOfInt[1];
    int i = paramArrayOfInt[2];
    int k = paramArrayOfInt[3];
    float f = j / (i + j + k);
    if ((f >= 0.7916667F) && (f <= 0.89285713F))
    {
      int i1 = Integer.MIN_VALUE;
      int i2 = paramArrayOfInt.length;
      k = 0;
      int m;
      for (i = Integer.MAX_VALUE; k < i2; i = m)
      {
        int n = paramArrayOfInt[k];
        j = i1;
        if (n > i1) {
          j = n;
        }
        m = i;
        if (n < i) {
          m = n;
        }
        k++;
        i1 = j;
      }
      return i1 < i * 10;
    }
    return false;
  }
  
  protected static int parseFinderValue(int[] paramArrayOfInt, int[][] paramArrayOfInt1)
    throws NotFoundException
  {
    for (int i = 0; i < paramArrayOfInt1.length; i++) {
      if (patternMatchVariance(paramArrayOfInt, paramArrayOfInt1[i], 0.45F) < 0.2F) {
        return i;
      }
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  protected final int[] getDataCharacterCounters()
  {
    return this.dataCharacterCounters;
  }
  
  protected final int[] getDecodeFinderCounters()
  {
    return this.decodeFinderCounters;
  }
  
  protected final int[] getEvenCounts()
  {
    return this.evenCounts;
  }
  
  protected final float[] getEvenRoundingErrors()
  {
    return this.evenRoundingErrors;
  }
  
  protected final int[] getOddCounts()
  {
    return this.oddCounts;
  }
  
  protected final float[] getOddRoundingErrors()
  {
    return this.oddRoundingErrors;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/AbstractRSSReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */