package com.google.zxing.oned.rss;

public final class RSSUtils
{
  private static int combins(int paramInt1, int paramInt2)
  {
    int k = paramInt1 - paramInt2;
    int i = k;
    int j = paramInt2;
    if (k > paramInt2)
    {
      j = k;
      i = paramInt2;
    }
    paramInt2 = 1;
    int m = 1;
    k = paramInt1;
    int n;
    for (paramInt1 = m;; paramInt1 = m)
    {
      m = paramInt2;
      n = paramInt1;
      if (k <= j) {
        break;
      }
      n = paramInt2 * k;
      paramInt2 = n;
      m = paramInt1;
      if (paramInt1 <= i)
      {
        paramInt2 = n / paramInt1;
        m = paramInt1 + 1;
      }
      k--;
    }
    while (n <= i)
    {
      m /= n;
      n++;
    }
    return m;
  }
  
  public static int getRSSvalue(int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
  {
    int[] arrayOfInt = paramArrayOfInt;
    int k = arrayOfInt.length;
    int j = 0;
    int i = 0;
    while (j < k)
    {
      i += arrayOfInt[j];
      j++;
    }
    int i4 = arrayOfInt.length;
    int n = 0;
    k = 0;
    j = 0;
    int i1 = i;
    for (i = k;; i = k)
    {
      int i6 = i4 - 1;
      if (n >= i6) {
        break;
      }
      int i5 = 1 << n;
      k = j | i5;
      j = i;
      int i2 = 1;
      i = k;
      while (i2 < paramArrayOfInt[n])
      {
        int i7 = i1 - i2;
        int i8 = i4 - n;
        int i3 = i8 - 2;
        int m = combins(i7 - 1, i3);
        k = m;
        if (paramBoolean)
        {
          k = m;
          if (i == 0)
          {
            int i9 = i8 - 1;
            k = m;
            if (i7 - i9 >= i9) {
              k = m - combins(i7 - i8, i3);
            }
          }
        }
        if (i8 - 1 > 1)
        {
          m = i7 - i3;
          i3 = 0;
          while (m > paramInt)
          {
            i3 += combins(i7 - m - 1, i8 - 3);
            m--;
          }
          m = k - i3 * (i6 - n);
        }
        else
        {
          m = k;
          if (i7 > paramInt) {
            m = k - 1;
          }
        }
        j += m;
        i2++;
        i &= (i5 ^ 0xFFFFFFFF);
      }
      i1 -= i2;
      n++;
      k = j;
      j = i;
    }
    return i;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/RSSUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */