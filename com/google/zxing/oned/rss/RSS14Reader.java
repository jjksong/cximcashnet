package com.google.zxing.oned.rss;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitArray;
import com.google.zxing.common.detector.MathUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class RSS14Reader
  extends AbstractRSSReader
{
  private static final int[][] FINDER_PATTERNS;
  private static final int[] INSIDE_GSUM;
  private static final int[] INSIDE_ODD_TOTAL_SUBSET;
  private static final int[] INSIDE_ODD_WIDEST;
  private static final int[] OUTSIDE_EVEN_TOTAL_SUBSET = { 1, 10, 34, 70, 126 };
  private static final int[] OUTSIDE_GSUM;
  private static final int[] OUTSIDE_ODD_WIDEST;
  private final List<Pair> possibleLeftPairs = new ArrayList();
  private final List<Pair> possibleRightPairs = new ArrayList();
  
  static
  {
    INSIDE_ODD_TOTAL_SUBSET = new int[] { 4, 20, 48, 81 };
    OUTSIDE_GSUM = new int[] { 0, 161, 961, 2015, 2715 };
    INSIDE_GSUM = new int[] { 0, 336, 1036, 1516 };
    OUTSIDE_ODD_WIDEST = new int[] { 8, 6, 4, 3, 1 };
    INSIDE_ODD_WIDEST = new int[] { 2, 4, 6, 8 };
    int[] arrayOfInt = { 2, 3, 8, 1 };
    FINDER_PATTERNS = new int[][] { { 3, 8, 2, 1 }, { 3, 5, 5, 1 }, { 3, 3, 7, 1 }, { 3, 1, 9, 1 }, { 2, 7, 4, 1 }, { 2, 5, 6, 1 }, arrayOfInt, { 1, 5, 7, 1 }, { 1, 3, 9, 1 } };
  }
  
  private static void addOrTally(Collection<Pair> paramCollection, Pair paramPair)
  {
    if (paramPair == null) {
      return;
    }
    int j = 0;
    Iterator localIterator = paramCollection.iterator();
    Pair localPair;
    do
    {
      i = j;
      if (!localIterator.hasNext()) {
        break;
      }
      localPair = (Pair)localIterator.next();
    } while (localPair.getValue() != paramPair.getValue());
    localPair.incrementCount();
    int i = 1;
    if (i == 0) {
      paramCollection.add(paramPair);
    }
  }
  
  private void adjustOddEvenCounts(boolean paramBoolean, int paramInt)
    throws NotFoundException
  {
    int i2 = MathUtils.sum(getOddCounts());
    int i3 = MathUtils.sum(getEvenCounts());
    int n = 0;
    int j;
    int i;
    int m;
    int k;
    if (paramBoolean)
    {
      if (i2 > 12)
      {
        j = 0;
        i = 1;
      }
      else if (i2 < 4)
      {
        j = 1;
        i = 0;
      }
      else
      {
        j = 0;
        i = 0;
      }
      if (i3 > 12)
      {
        m = i;
        i = 0;
        i1 = 1;
        k = j;
        j = i1;
      }
      else if (i3 < 4)
      {
        m = i;
        i = 1;
        i1 = 0;
        k = j;
        j = i1;
      }
      else
      {
        m = i;
        i = 0;
        i1 = 0;
        k = j;
        j = i1;
      }
    }
    else
    {
      if (i2 > 11)
      {
        k = 0;
        m = 1;
      }
      else if (i2 < 5)
      {
        k = 1;
        m = 0;
      }
      else
      {
        k = 0;
        m = 0;
      }
      if (i3 > 10)
      {
        i = 0;
        j = 1;
      }
      else if (i3 < 4)
      {
        i = 1;
        j = 0;
      }
      else
      {
        i = 0;
        j = 0;
      }
    }
    int i1 = i2 + i3 - paramInt;
    if ((i2 & 0x1) == paramBoolean) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if ((i3 & 0x1) == 1) {
      n = 1;
    }
    if (i1 == 1)
    {
      if (paramInt != 0)
      {
        if (n == 0) {
          m = 1;
        } else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else if (n != 0) {
        j = 1;
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    else if (i1 == -1)
    {
      if (paramInt != 0)
      {
        if (n == 0) {
          k = 1;
        } else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else if (n != 0) {
        i = 1;
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    else
    {
      if (i1 != 0) {
        break label453;
      }
      if (paramInt != 0)
      {
        if (n != 0)
        {
          if (i2 < i3)
          {
            k = 1;
            j = 1;
          }
          else
          {
            i = 1;
            m = 1;
          }
        }
        else {
          throw NotFoundException.getNotFoundInstance();
        }
      }
      else {
        if (n != 0) {
          break label449;
        }
      }
    }
    if (k != 0) {
      if (m == 0) {
        increment(getOddCounts(), getOddRoundingErrors());
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    if (m != 0) {
      decrement(getOddCounts(), getOddRoundingErrors());
    }
    if (i != 0) {
      if (j == 0) {
        increment(getEvenCounts(), getOddRoundingErrors());
      } else {
        throw NotFoundException.getNotFoundInstance();
      }
    }
    if (j != 0) {
      decrement(getEvenCounts(), getEvenRoundingErrors());
    }
    return;
    label449:
    throw NotFoundException.getNotFoundInstance();
    label453:
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static boolean checkChecksum(Pair paramPair1, Pair paramPair2)
  {
    int m = paramPair1.getChecksumPortion();
    int k = paramPair2.getChecksumPortion();
    int j = paramPair1.getFinderPattern().getValue() * 9 + paramPair2.getFinderPattern().getValue();
    int i = j;
    if (j > 72) {
      i = j - 1;
    }
    j = i;
    if (i > 8) {
      j = i - 1;
    }
    return (m + k * 16) % 79 == j;
  }
  
  private static Result constructResult(Pair paramPair1, Pair paramPair2)
  {
    Object localObject1 = String.valueOf(paramPair1.getValue() * 4537077L + paramPair2.getValue());
    StringBuilder localStringBuilder = new StringBuilder(14);
    for (int i = 13 - ((String)localObject1).length(); i > 0; i--) {
      localStringBuilder.append('0');
    }
    localStringBuilder.append((String)localObject1);
    int j = 0;
    i = 0;
    while (j < 13)
    {
      int m = localStringBuilder.charAt(j) - '0';
      int k = m;
      if ((j & 0x1) == 0) {
        k = m * 3;
      }
      i += k;
      j++;
    }
    j = 10 - i % 10;
    i = j;
    if (j == 10) {
      i = 0;
    }
    localStringBuilder.append(i);
    localObject1 = paramPair1.getFinderPattern().getResultPoints();
    Object localObject2 = paramPair2.getFinderPattern().getResultPoints();
    paramPair1 = String.valueOf(localStringBuilder.toString());
    paramPair2 = localObject1[0];
    localStringBuilder = localObject1[1];
    localObject1 = localObject2[0];
    Object localObject3 = localObject2[1];
    localObject2 = BarcodeFormat.RSS_14;
    return new Result(paramPair1, null, new ResultPoint[] { paramPair2, localStringBuilder, localObject1, localObject3 }, (BarcodeFormat)localObject2);
  }
  
  private DataCharacter decodeDataCharacter(BitArray paramBitArray, FinderPattern paramFinderPattern, boolean paramBoolean)
    throws NotFoundException
  {
    int[] arrayOfInt1 = getDataCharacterCounters();
    arrayOfInt1[0] = 0;
    arrayOfInt1[1] = 0;
    arrayOfInt1[2] = 0;
    arrayOfInt1[3] = 0;
    arrayOfInt1[4] = 0;
    arrayOfInt1[5] = 0;
    arrayOfInt1[6] = 0;
    arrayOfInt1[7] = 0;
    if (paramBoolean)
    {
      recordPatternInReverse(paramBitArray, paramFinderPattern.getStartEnd()[0], arrayOfInt1);
    }
    else
    {
      recordPattern(paramBitArray, paramFinderPattern.getStartEnd()[1] + 1, arrayOfInt1);
      i = arrayOfInt1.length - 1;
      j = 0;
      while (j < i)
      {
        k = arrayOfInt1[j];
        arrayOfInt1[j] = arrayOfInt1[i];
        arrayOfInt1[i] = k;
        j++;
        i--;
      }
    }
    if (paramBoolean) {
      j = 16;
    } else {
      j = 15;
    }
    float f1 = MathUtils.sum(arrayOfInt1) / j;
    int[] arrayOfInt2 = getOddCounts();
    paramBitArray = getEvenCounts();
    paramFinderPattern = getOddRoundingErrors();
    float[] arrayOfFloat = getEvenRoundingErrors();
    for (int k = 0; k < arrayOfInt1.length; k++)
    {
      float f2 = arrayOfInt1[k] / f1;
      m = (int)(0.5F + f2);
      if (m <= 0)
      {
        i = 1;
      }
      else
      {
        i = m;
        if (m > 8) {
          i = 8;
        }
      }
      m = k / 2;
      if ((k & 0x1) == 0)
      {
        arrayOfInt2[m] = i;
        paramFinderPattern[m] = (f2 - i);
      }
      else
      {
        paramBitArray[m] = i;
        arrayOfFloat[m] = (f2 - i);
      }
    }
    adjustOddEvenCounts(paramBoolean, j);
    k = arrayOfInt2.length - 1;
    int j = 0;
    int i = 0;
    while (k >= 0)
    {
      j = j * 9 + arrayOfInt2[k];
      i += arrayOfInt2[k];
      k--;
    }
    int n = paramBitArray.length - 1;
    int m = 0;
    k = 0;
    while (n >= 0)
    {
      m = m * 9 + paramBitArray[n];
      k += paramBitArray[n];
      n--;
    }
    j += m * 3;
    if (paramBoolean)
    {
      if (((i & 0x1) == 0) && (i <= 12) && (i >= 4))
      {
        i = (12 - i) / 2;
        m = OUTSIDE_ODD_WIDEST[i];
        k = RSSUtils.getRSSvalue(arrayOfInt2, m, false);
        m = RSSUtils.getRSSvalue(paramBitArray, 9 - m, true);
        return new DataCharacter(k * OUTSIDE_EVEN_TOTAL_SUBSET[i] + m + OUTSIDE_GSUM[i], j);
      }
      throw NotFoundException.getNotFoundInstance();
    }
    if (((k & 0x1) == 0) && (k <= 10) && (k >= 4))
    {
      m = (10 - k) / 2;
      i = INSIDE_ODD_WIDEST[m];
      k = RSSUtils.getRSSvalue(arrayOfInt2, i, true);
      return new DataCharacter(RSSUtils.getRSSvalue(paramBitArray, 9 - i, false) * INSIDE_ODD_TOTAL_SUBSET[m] + k + INSIDE_GSUM[m], j);
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private Pair decodePair(BitArray paramBitArray, boolean paramBoolean, int paramInt, Map<DecodeHintType, ?> paramMap)
  {
    try
    {
      Object localObject = findFinderPattern(paramBitArray, 0, paramBoolean);
      FinderPattern localFinderPattern = parseFoundFinderPattern(paramBitArray, paramInt, paramBoolean, (int[])localObject);
      if (paramMap == null) {
        paramMap = null;
      } else {
        paramMap = (ResultPointCallback)paramMap.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
      }
      if (paramMap != null)
      {
        float f2 = (localObject[0] + localObject[1]) / 2.0F;
        float f1 = f2;
        if (paramBoolean) {
          f1 = paramBitArray.getSize() - 1 - f2;
        }
        localObject = new com/google/zxing/ResultPoint;
        ((ResultPoint)localObject).<init>(f1, paramInt);
        paramMap.foundPossibleResultPoint((ResultPoint)localObject);
      }
      paramMap = decodeDataCharacter(paramBitArray, localFinderPattern, true);
      paramBitArray = decodeDataCharacter(paramBitArray, localFinderPattern, false);
      paramBitArray = new Pair(paramMap.getValue() * 1597 + paramBitArray.getValue(), paramMap.getChecksumPortion() + paramBitArray.getChecksumPortion() * 4, localFinderPattern);
      return paramBitArray;
    }
    catch (NotFoundException paramBitArray) {}
    return null;
  }
  
  private int[] findFinderPattern(BitArray paramBitArray, int paramInt, boolean paramBoolean)
    throws NotFoundException
  {
    int[] arrayOfInt = getDecodeFinderCounters();
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    int m = paramBitArray.getSize();
    boolean bool2;
    for (boolean bool1 = false; paramInt < m; bool1 = bool2)
    {
      bool2 = paramBitArray.get(paramInt) ^ true;
      bool1 = bool2;
      if (paramBoolean == bool2) {
        break;
      }
      paramInt++;
    }
    int k = paramInt;
    int i = 0;
    int j = paramInt;
    paramInt = k;
    paramBoolean = bool1;
    while (j < m)
    {
      if ((paramBitArray.get(j) ^ paramBoolean))
      {
        arrayOfInt[i] += 1;
      }
      else
      {
        if (i == 3)
        {
          if (isFinderPattern(arrayOfInt)) {
            return new int[] { paramInt, j };
          }
          paramInt += arrayOfInt[0] + arrayOfInt[1];
          arrayOfInt[0] = arrayOfInt[2];
          arrayOfInt[1] = arrayOfInt[3];
          arrayOfInt[2] = 0;
          arrayOfInt[3] = 0;
          i--;
        }
        else
        {
          i++;
        }
        arrayOfInt[i] = 1;
        if (!paramBoolean) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        }
      }
      j++;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private FinderPattern parseFoundFinderPattern(BitArray paramBitArray, int paramInt, boolean paramBoolean, int[] paramArrayOfInt)
    throws NotFoundException
  {
    boolean bool = paramBitArray.get(paramArrayOfInt[0]);
    for (int i = paramArrayOfInt[0] - 1; (i >= 0) && ((paramBitArray.get(i) ^ bool)); i--) {}
    int k = i + 1;
    i = paramArrayOfInt[0];
    int[] arrayOfInt = getDecodeFinderCounters();
    System.arraycopy(arrayOfInt, 0, arrayOfInt, 1, arrayOfInt.length - 1);
    arrayOfInt[0] = (i - k);
    int m = parseFinderValue(arrayOfInt, FINDER_PATTERNS);
    int j = paramArrayOfInt[1];
    if (paramBoolean)
    {
      i = paramBitArray.getSize();
      j = paramBitArray.getSize() - 1 - j;
      i = i - 1 - k;
    }
    else
    {
      i = k;
    }
    return new FinderPattern(m, new int[] { k, paramArrayOfInt[1] }, i, j, paramInt);
  }
  
  public Result decodeRow(int paramInt, BitArray paramBitArray, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException
  {
    Pair localPair = decodePair(paramBitArray, false, paramInt, paramMap);
    addOrTally(this.possibleLeftPairs, localPair);
    paramBitArray.reverse();
    paramMap = decodePair(paramBitArray, true, paramInt, paramMap);
    addOrTally(this.possibleRightPairs, paramMap);
    paramBitArray.reverse();
    Iterator localIterator = this.possibleLeftPairs.iterator();
    do
    {
      while (!paramMap.hasNext())
      {
        do
        {
          if (!localIterator.hasNext()) {
            break;
          }
          paramBitArray = (Pair)localIterator.next();
        } while (paramBitArray.getCount() <= 1);
        paramMap = this.possibleRightPairs.iterator();
      }
      localPair = (Pair)paramMap.next();
    } while ((localPair.getCount() <= 1) || (!checkChecksum(paramBitArray, localPair)));
    return constructResult(paramBitArray, localPair);
    throw NotFoundException.getNotFoundInstance();
  }
  
  public void reset()
  {
    this.possibleLeftPairs.clear();
    this.possibleRightPairs.clear();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/RSS14Reader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */