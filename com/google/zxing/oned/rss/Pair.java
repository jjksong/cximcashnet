package com.google.zxing.oned.rss;

final class Pair
  extends DataCharacter
{
  private int count;
  private final FinderPattern finderPattern;
  
  Pair(int paramInt1, int paramInt2, FinderPattern paramFinderPattern)
  {
    super(paramInt1, paramInt2);
    this.finderPattern = paramFinderPattern;
  }
  
  int getCount()
  {
    return this.count;
  }
  
  FinderPattern getFinderPattern()
  {
    return this.finderPattern;
  }
  
  void incrementCount()
  {
    this.count += 1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/Pair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */