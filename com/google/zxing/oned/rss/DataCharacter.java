package com.google.zxing.oned.rss;

public class DataCharacter
{
  private final int checksumPortion;
  private final int value;
  
  public DataCharacter(int paramInt1, int paramInt2)
  {
    this.value = paramInt1;
    this.checksumPortion = paramInt2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof DataCharacter)) {
      return false;
    }
    paramObject = (DataCharacter)paramObject;
    return (this.value == ((DataCharacter)paramObject).value) && (this.checksumPortion == ((DataCharacter)paramObject).checksumPortion);
  }
  
  public final int getChecksumPortion()
  {
    return this.checksumPortion;
  }
  
  public final int getValue()
  {
    return this.value;
  }
  
  public final int hashCode()
  {
    return this.value ^ this.checksumPortion;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.value);
    localStringBuilder.append("(");
    localStringBuilder.append(this.checksumPortion);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/oned/rss/DataCharacter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */