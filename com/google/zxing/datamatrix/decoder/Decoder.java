package com.google.zxing.datamatrix.decoder;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import com.google.zxing.common.reedsolomon.ReedSolomonException;

public final class Decoder
{
  private final ReedSolomonDecoder rsDecoder = new ReedSolomonDecoder(GenericGF.DATA_MATRIX_FIELD_256);
  
  private void correctErrors(byte[] paramArrayOfByte, int paramInt)
    throws ChecksumException
  {
    int k = paramArrayOfByte.length;
    int[] arrayOfInt = new int[k];
    int j = 0;
    for (int i = 0; i < k; i++) {
      paramArrayOfByte[i] &= 0xFF;
    }
    try
    {
      this.rsDecoder.decode(arrayOfInt, paramArrayOfByte.length - paramInt);
      for (i = j; i < paramInt; i++) {
        paramArrayOfByte[i] = ((byte)arrayOfInt[i]);
      }
      return;
    }
    catch (ReedSolomonException paramArrayOfByte)
    {
      throw ChecksumException.getChecksumInstance();
    }
  }
  
  public DecoderResult decode(BitMatrix paramBitMatrix)
    throws FormatException, ChecksumException
  {
    paramBitMatrix = new BitMatrixParser(paramBitMatrix);
    Object localObject1 = paramBitMatrix.getVersion();
    paramBitMatrix = DataBlock.getDataBlocks(paramBitMatrix.readCodewords(), (Version)localObject1);
    int k = paramBitMatrix.length;
    int j = 0;
    int i = 0;
    while (j < k)
    {
      i += paramBitMatrix[j].getNumDataCodewords();
      j++;
    }
    localObject1 = new byte[i];
    k = paramBitMatrix.length;
    for (i = 0; i < k; i++)
    {
      Object localObject2 = paramBitMatrix[i];
      byte[] arrayOfByte = ((DataBlock)localObject2).getCodewords();
      int m = ((DataBlock)localObject2).getNumDataCodewords();
      correctErrors(arrayOfByte, m);
      for (j = 0; j < m; j++) {
        localObject1[(j * k + i)] = arrayOfByte[j];
      }
    }
    return DecodedBitStreamParser.decode((byte[])localObject1);
  }
  
  public DecoderResult decode(boolean[][] paramArrayOfBoolean)
    throws FormatException, ChecksumException
  {
    int k = paramArrayOfBoolean.length;
    BitMatrix localBitMatrix = new BitMatrix(k);
    for (int i = 0; i < k; i++) {
      for (int j = 0; j < k; j++) {
        if (paramArrayOfBoolean[i][j] != 0) {
          localBitMatrix.set(j, i);
        }
      }
    }
    return decode(localBitMatrix);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/datamatrix/decoder/Decoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */