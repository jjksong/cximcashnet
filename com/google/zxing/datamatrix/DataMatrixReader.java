package com.google.zxing.datamatrix;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.datamatrix.decoder.Decoder;
import com.google.zxing.datamatrix.detector.Detector;
import java.util.List;
import java.util.Map;

public final class DataMatrixReader
  implements Reader
{
  private static final ResultPoint[] NO_POINTS = new ResultPoint[0];
  private final Decoder decoder = new Decoder();
  
  private static BitMatrix extractPureBits(BitMatrix paramBitMatrix)
    throws NotFoundException
  {
    int[] arrayOfInt = paramBitMatrix.getTopLeftOnBit();
    Object localObject = paramBitMatrix.getBottomRightOnBit();
    if ((arrayOfInt != null) && (localObject != null))
    {
      int k = moduleSize(arrayOfInt, paramBitMatrix);
      int m = arrayOfInt[1];
      int i = localObject[1];
      int n = arrayOfInt[0];
      int i1 = (localObject[0] - n + 1) / k;
      int i3 = (i - m + 1) / k;
      if ((i1 > 0) && (i3 > 0))
      {
        int i2 = k / 2;
        localObject = new BitMatrix(i1, i3);
        for (i = 0; i < i3; i++) {
          for (int j = 0; j < i1; j++) {
            if (paramBitMatrix.get(j * k + (n + i2), i * k + (m + i2))) {
              ((BitMatrix)localObject).set(j, i);
            }
          }
        }
        return (BitMatrix)localObject;
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static int moduleSize(int[] paramArrayOfInt, BitMatrix paramBitMatrix)
    throws NotFoundException
  {
    int k = paramBitMatrix.getWidth();
    int i = paramArrayOfInt[0];
    int j = paramArrayOfInt[1];
    while ((i < k) && (paramBitMatrix.get(i, j))) {
      i++;
    }
    if (i != k)
    {
      i -= paramArrayOfInt[0];
      if (i != 0) {
        return i;
      }
      throw NotFoundException.getNotFoundInstance();
    }
    throw NotFoundException.getNotFoundInstance();
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap)
    throws NotFoundException, ChecksumException, FormatException
  {
    return decode(paramBinaryBitmap, null);
  }
  
  public Result decode(BinaryBitmap paramBinaryBitmap, Map<DecodeHintType, ?> paramMap)
    throws NotFoundException, ChecksumException, FormatException
  {
    if ((paramMap != null) && (paramMap.containsKey(DecodeHintType.PURE_BARCODE)))
    {
      paramBinaryBitmap = extractPureBits(paramBinaryBitmap.getBlackMatrix());
      paramBinaryBitmap = this.decoder.decode(paramBinaryBitmap);
      paramMap = NO_POINTS;
    }
    else
    {
      paramMap = new Detector(paramBinaryBitmap.getBlackMatrix()).detect();
      paramBinaryBitmap = this.decoder.decode(paramMap.getBits());
      paramMap = paramMap.getPoints();
    }
    paramMap = new Result(paramBinaryBitmap.getText(), paramBinaryBitmap.getRawBytes(), paramMap, BarcodeFormat.DATA_MATRIX);
    List localList = paramBinaryBitmap.getByteSegments();
    if (localList != null) {
      paramMap.putMetadata(ResultMetadataType.BYTE_SEGMENTS, localList);
    }
    paramBinaryBitmap = paramBinaryBitmap.getECLevel();
    if (paramBinaryBitmap != null) {
      paramMap.putMetadata(ResultMetadataType.ERROR_CORRECTION_LEVEL, paramBinaryBitmap);
    }
    return paramMap;
  }
  
  public void reset() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/datamatrix/DataMatrixReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */