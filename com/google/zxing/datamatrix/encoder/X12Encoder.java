package com.google.zxing.datamatrix.encoder;

final class X12Encoder
  extends C40Encoder
{
  public void encode(EncoderContext paramEncoderContext)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    while (paramEncoderContext.hasMoreCharacters())
    {
      char c = paramEncoderContext.getCurrentChar();
      paramEncoderContext.pos += 1;
      encodeChar(c, localStringBuilder);
      if (localStringBuilder.length() % 3 == 0)
      {
        writeNextTriplet(paramEncoderContext, localStringBuilder);
        int i = HighLevelEncoder.lookAheadTest(paramEncoderContext.getMessage(), paramEncoderContext.pos, getEncodingMode());
        if (i != getEncodingMode()) {
          paramEncoderContext.signalEncoderChange(i);
        }
      }
    }
    handleEOD(paramEncoderContext, localStringBuilder);
  }
  
  int encodeChar(char paramChar, StringBuilder paramStringBuilder)
  {
    if (paramChar == '\r') {
      paramStringBuilder.append('\000');
    } else if (paramChar == '*') {
      paramStringBuilder.append('\001');
    } else if (paramChar == '>') {
      paramStringBuilder.append('\002');
    } else if (paramChar == ' ') {
      paramStringBuilder.append('\003');
    } else if ((paramChar >= '0') && (paramChar <= '9')) {
      paramStringBuilder.append((char)(paramChar - '0' + 4));
    } else if ((paramChar >= 'A') && (paramChar <= 'Z')) {
      paramStringBuilder.append((char)(paramChar - 'A' + 14));
    } else {
      HighLevelEncoder.illegalCharacter(paramChar);
    }
    return 1;
  }
  
  public int getEncodingMode()
  {
    return 3;
  }
  
  void handleEOD(EncoderContext paramEncoderContext, StringBuilder paramStringBuilder)
  {
    paramEncoderContext.updateSymbolInfo();
    int j = paramEncoderContext.getSymbolInfo().getDataCapacity() - paramEncoderContext.getCodewordCount();
    int i = paramStringBuilder.length();
    paramEncoderContext.pos -= i;
    if ((paramEncoderContext.getRemainingCharacters() > 1) || (j > 1) || (paramEncoderContext.getRemainingCharacters() != j)) {
      paramEncoderContext.writeCodeword('þ');
    }
    if (paramEncoderContext.getNewEncoding() < 0) {
      paramEncoderContext.signalEncoderChange(0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/datamatrix/encoder/X12Encoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */