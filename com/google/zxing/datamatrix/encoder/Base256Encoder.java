package com.google.zxing.datamatrix.encoder;

final class Base256Encoder
  implements Encoder
{
  private static char randomize255State(char paramChar, int paramInt)
  {
    paramChar += paramInt * 149 % 255 + 1;
    if (paramChar <= 'ÿ') {
      return (char)paramChar;
    }
    return (char)(paramChar - 'Ā');
  }
  
  public void encode(EncoderContext paramEncoderContext)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = 0;
    localStringBuilder.append('\000');
    while (paramEncoderContext.hasMoreCharacters())
    {
      localStringBuilder.append(paramEncoderContext.getCurrentChar());
      paramEncoderContext.pos += 1;
      i = HighLevelEncoder.lookAheadTest(paramEncoderContext.getMessage(), paramEncoderContext.pos, getEncodingMode());
      if (i != getEncodingMode()) {
        paramEncoderContext.signalEncoderChange(i);
      }
    }
    int k = localStringBuilder.length() - 1;
    int i = paramEncoderContext.getCodewordCount() + k + 1;
    paramEncoderContext.updateSymbolInfo(i);
    if (paramEncoderContext.getSymbolInfo().getDataCapacity() - i > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if ((paramEncoderContext.hasMoreCharacters()) || (i != 0)) {
      if (k <= 249)
      {
        localStringBuilder.setCharAt(0, (char)k);
      }
      else
      {
        if (k > 1555) {
          break label228;
        }
        localStringBuilder.setCharAt(0, (char)(k / 250 + 249));
        localStringBuilder.insert(1, (char)(k % 250));
      }
    }
    k = localStringBuilder.length();
    for (i = j; i < k; i++) {
      paramEncoderContext.writeCodeword(randomize255State(localStringBuilder.charAt(i), paramEncoderContext.getCodewordCount() + 1));
    }
    return;
    label228:
    paramEncoderContext = new StringBuilder("Message length not in valid ranges: ");
    paramEncoderContext.append(k);
    throw new IllegalStateException(paramEncoderContext.toString());
  }
  
  public int getEncodingMode()
  {
    return 5;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/google/zxing/datamatrix/encoder/Base256Encoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */