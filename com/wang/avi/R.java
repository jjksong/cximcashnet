package com.wang.avi;

public final class R
{
  public static final class attr
  {
    public static final int indicatorColor = 2130837721;
    public static final int indicatorName = 2130837722;
    public static final int maxHeight = 2130837814;
    public static final int maxWidth = 2130837816;
    public static final int minHeight = 2130837818;
    public static final int minWidth = 2130837819;
  }
  
  public static final class string
  {
    public static final int app_name = 2131558443;
  }
  
  public static final class style
  {
    public static final int AVLoadingIndicatorView = 2131623936;
    public static final int AVLoadingIndicatorView_Large = 2131623937;
    public static final int AVLoadingIndicatorView_Small = 2131623938;
  }
  
  public static final class styleable
  {
    public static final int[] AVLoadingIndicatorView = { 2130837721, 2130837722, 2130837814, 2130837816, 2130837818, 2130837819 };
    public static final int AVLoadingIndicatorView_indicatorColor = 0;
    public static final int AVLoadingIndicatorView_indicatorName = 1;
    public static final int AVLoadingIndicatorView_maxHeight = 2;
    public static final int AVLoadingIndicatorView_maxWidth = 3;
    public static final int AVLoadingIndicatorView_minHeight = 4;
    public static final int AVLoadingIndicatorView_minWidth = 5;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */