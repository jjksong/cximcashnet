package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class LineScaleIndicator
  extends Indicator
{
  public static final float SCALE = 1.0F;
  float[] scaleYFloats = { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f3 = getWidth() / 11;
    float f2 = getHeight() / 2;
    for (int i = 0; i < 5; i++)
    {
      paramCanvas.save();
      float f4 = i * 2 + 2;
      float f1 = f3 / 2.0F;
      paramCanvas.translate(f4 * f3 - f1, f2);
      paramCanvas.scale(1.0F, this.scaleYFloats[i]);
      paramCanvas.drawRoundRect(new RectF(-f3 / 2.0F, -getHeight() / 2.5F, f1, getHeight() / 2.5F), 5.0F, 5.0F, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 5; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F, 0.4F, 1.0F });
      localValueAnimator.setDuration(1000L);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new long[] { 100L, 200L, 300L, 400L, 500L }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          LineScaleIndicator.this.scaleYFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          LineScaleIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/LineScaleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */