package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallZigZagIndicator
  extends Indicator
{
  float[] translateX = new float[2];
  float[] translateY = new float[2];
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    for (int i = 0; i < 2; i++)
    {
      paramCanvas.save();
      paramCanvas.translate(this.translateX[i], this.translateY[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, getWidth() / 10, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f1 = getWidth() / 6;
    float f2 = getWidth() / 6;
    for (final int i = 0; i < 2; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { f1, getWidth() - f1, getWidth() / 2, f1 });
      if (i == 1) {
        localValueAnimator1 = ValueAnimator.ofFloat(new float[] { getWidth() - f1, f1, getWidth() / 2, getWidth() - f1 });
      }
      ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { f2, f2, getHeight() / 2, f2 });
      if (i == 1) {
        localValueAnimator2 = ValueAnimator.ofFloat(new float[] { getHeight() - f2, getHeight() - f2, getHeight() / 2, getHeight() - f2 });
      }
      localValueAnimator1.setDuration(1000L);
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setRepeatCount(-1);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallZigZagIndicator.this.translateX[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallZigZagIndicator.this.postInvalidate();
        }
      });
      localValueAnimator2.setDuration(1000L);
      localValueAnimator2.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallZigZagIndicator.this.translateY[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallZigZagIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallZigZagIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */