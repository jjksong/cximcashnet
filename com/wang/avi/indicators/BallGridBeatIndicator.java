package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallGridBeatIndicator
  extends Indicator
{
  public static final int ALPHA = 255;
  int[] alphas = { 255, 255, 255, 255, 255, 255, 255, 255, 255 };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f4 = (getWidth() - 16.0F) / 6.0F;
    float f1 = getWidth() / 2;
    float f2 = 2.0F * f4;
    float f6 = f2 + 4.0F;
    float f3 = getWidth() / 2;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++)
      {
        paramCanvas.save();
        float f7 = j;
        float f5 = i;
        paramCanvas.translate(f2 * f7 + (f1 - f6) + f7 * 4.0F, f2 * f5 + (f3 - f6) + f5 * 4.0F);
        paramPaint.setAlpha(this.alphas[(i * 3 + j)]);
        paramCanvas.drawCircle(0.0F, 0.0F, f4, paramPaint);
        paramCanvas.restore();
      }
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 9; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofInt(new int[] { 255, 168, 255 });
      localValueAnimator.setDuration(new int[] { 960, 930, 1190, 1130, 1340, 940, 1200, 820, 1190 }[i]);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new int[] { 360, 400, 680, 410, 710, 65386, -120, 10, 320 }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallGridBeatIndicator.this.alphas[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallGridBeatIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallGridBeatIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */