package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallClipRotatePulseIndicator
  extends Indicator
{
  float degrees;
  float scaleFloat1;
  float scaleFloat2;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f1 = getWidth() / 2;
    float f2 = getHeight() / 2;
    paramCanvas.save();
    paramCanvas.translate(f1, f2);
    float f3 = this.scaleFloat1;
    paramCanvas.scale(f3, f3);
    paramPaint.setStyle(Paint.Style.FILL);
    paramCanvas.drawCircle(0.0F, 0.0F, f1 / 2.5F, paramPaint);
    paramCanvas.restore();
    paramCanvas.translate(f1, f2);
    f3 = this.scaleFloat2;
    paramCanvas.scale(f3, f3);
    paramCanvas.rotate(this.degrees);
    paramPaint.setStrokeWidth(3.0F);
    paramPaint.setStyle(Paint.Style.STROKE);
    for (int i = 0; i < 2; i++) {
      paramCanvas.drawArc(new RectF(-f1 + 12.0F, -f2 + 12.0F, f1 - 12.0F, f2 - 12.0F), new float[] { 225.0F, 45.0F }[i], 90.0F, false, paramPaint);
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.3F, 1.0F });
    localValueAnimator1.setDuration(1000L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotatePulseIndicator.this.scaleFloat1 = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotatePulseIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator3 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.6F, 1.0F });
    localValueAnimator3.setDuration(1000L);
    localValueAnimator3.setRepeatCount(-1);
    addUpdateListener(localValueAnimator3, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotatePulseIndicator.this.scaleFloat2 = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotatePulseIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F });
    localValueAnimator2.setDuration(1000L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotatePulseIndicator.this.degrees = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotatePulseIndicator.this.postInvalidate();
      }
    });
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(localValueAnimator1);
    localArrayList.add(localValueAnimator3);
    localArrayList.add(localValueAnimator2);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallClipRotatePulseIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */