package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallPulseRiseIndicator
  extends Indicator
{
  private float degress;
  private Camera mCamera = new Camera();
  private Matrix mMatrix = new Matrix();
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    this.mMatrix.reset();
    this.mCamera.save();
    this.mCamera.rotateX(this.degress);
    this.mCamera.getMatrix(this.mMatrix);
    this.mCamera.restore();
    this.mMatrix.preTranslate(-centerX(), -centerY());
    this.mMatrix.postTranslate(centerX(), centerY());
    paramCanvas.concat(this.mMatrix);
    float f2 = getWidth() / 10;
    float f3 = getWidth() / 4;
    float f1 = 2.0F * f2;
    paramCanvas.drawCircle(f3, f1, f2, paramPaint);
    paramCanvas.drawCircle(getWidth() * 3 / 4, f1, f2, paramPaint);
    paramCanvas.drawCircle(f2, getHeight() - f1, f2, paramPaint);
    paramCanvas.drawCircle(getWidth() / 2, getHeight() - f1, f2, paramPaint);
    paramCanvas.drawCircle(getWidth() - f2, getHeight() - f1, f2, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 360.0F });
    addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallPulseRiseIndicator.access$002(BallPulseRiseIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        BallPulseRiseIndicator.this.postInvalidate();
      }
    });
    localValueAnimator.setInterpolator(new LinearInterpolator());
    localValueAnimator.setRepeatCount(-1);
    localValueAnimator.setDuration(1500L);
    localArrayList.add(localValueAnimator);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallPulseRiseIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */