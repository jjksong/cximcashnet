package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import java.util.ArrayList;

public class LineScalePulseOutIndicator
  extends LineScaleIndicator
{
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 5; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F, 0.3F, 1.0F });
      localValueAnimator.setDuration(900L);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new long[] { 500L, 250L, 0L, 250L, 500L }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          LineScalePulseOutIndicator.this.scaleYFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          LineScalePulseOutIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/LineScalePulseOutIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */