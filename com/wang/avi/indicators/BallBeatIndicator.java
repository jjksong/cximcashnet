package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallBeatIndicator
  extends Indicator
{
  public static final int ALPHA = 255;
  public static final float SCALE = 1.0F;
  int[] alphas = { 255, 255, 255 };
  private float[] scaleFloats = { 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f2 = (getWidth() - 8.0F) / 6.0F;
    float f5 = getWidth() / 2;
    float f4 = 2.0F * f2;
    float f3 = getHeight() / 2;
    for (int i = 0; i < 3; i++)
    {
      paramCanvas.save();
      float f1 = i;
      paramCanvas.translate(f4 * f1 + (f5 - (f4 + 4.0F)) + f1 * 4.0F, f3);
      float[] arrayOfFloat = this.scaleFloats;
      paramCanvas.scale(arrayOfFloat[i], arrayOfFloat[i]);
      paramPaint.setAlpha(this.alphas[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, f2, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    int[] arrayOfInt = new int[3];
    int[] tmp13_12 = arrayOfInt;
    tmp13_12[0] = 'Ş';
    int[] tmp19_13 = tmp13_12;
    tmp19_13[1] = 0;
    int[] tmp23_19 = tmp19_13;
    tmp23_19[2] = 'Ş';
    tmp23_19;
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.75F, 1.0F });
      localValueAnimator1.setDuration(700L);
      localValueAnimator1.setRepeatCount(-1);
      localValueAnimator1.setStartDelay(arrayOfInt[i]);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallBeatIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallBeatIndicator.this.postInvalidate();
        }
      });
      ValueAnimator localValueAnimator2 = ValueAnimator.ofInt(new int[] { 255, 51, 255 });
      localValueAnimator2.setDuration(700L);
      localValueAnimator2.setRepeatCount(-1);
      localValueAnimator2.setStartDelay(arrayOfInt[i]);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallBeatIndicator.this.alphas[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallBeatIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallBeatIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */