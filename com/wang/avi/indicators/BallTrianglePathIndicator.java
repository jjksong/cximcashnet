package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallTrianglePathIndicator
  extends Indicator
{
  float[] translateX = new float[3];
  float[] translateY = new float[3];
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setStrokeWidth(3.0F);
    paramPaint.setStyle(Paint.Style.STROKE);
    for (int i = 0; i < 3; i++)
    {
      paramCanvas.save();
      paramCanvas.translate(this.translateX[i], this.translateY[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, getWidth() / 10, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f2 = getWidth() / 5;
    float f1 = getWidth() / 5;
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { getWidth() / 2, getWidth() - f2, f2, getWidth() / 2 });
      if (i == 1) {
        localValueAnimator1 = ValueAnimator.ofFloat(new float[] { getWidth() - f2, f2, getWidth() / 2, getWidth() - f2 });
      } else if (i == 2) {
        localValueAnimator1 = ValueAnimator.ofFloat(new float[] { f2, getWidth() / 2, getWidth() - f2, f2 });
      }
      ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { f1, getHeight() - f1, getHeight() - f1, f1 });
      if (i == 1) {
        localValueAnimator2 = ValueAnimator.ofFloat(new float[] { getHeight() - f1, getHeight() - f1, f1, getHeight() - f1 });
      } else if (i == 2) {
        localValueAnimator2 = ValueAnimator.ofFloat(new float[] { getHeight() - f1, f1, getHeight() - f1, getHeight() - f1 });
      }
      localValueAnimator1.setDuration(2000L);
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setRepeatCount(-1);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallTrianglePathIndicator.this.translateX[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallTrianglePathIndicator.this.postInvalidate();
        }
      });
      localValueAnimator2.setDuration(2000L);
      localValueAnimator2.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallTrianglePathIndicator.this.translateY[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallTrianglePathIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallTrianglePathIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */