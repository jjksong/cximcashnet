package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class LineScalePartyIndicator
  extends Indicator
{
  public static final float SCALE = 1.0F;
  float[] scaleFloats = { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f3 = getWidth() / 9;
    float f4 = getHeight() / 2;
    for (int i = 0; i < 4; i++)
    {
      paramCanvas.save();
      float f1 = i * 2 + 2;
      float f2 = f3 / 2.0F;
      paramCanvas.translate(f1 * f3 - f2, f4);
      float[] arrayOfFloat = this.scaleFloats;
      paramCanvas.scale(arrayOfFloat[i], arrayOfFloat[i]);
      paramCanvas.drawRoundRect(new RectF(-f3 / 2.0F, -getHeight() / 2.5F, f2, getHeight() / 2.5F), 5.0F, 5.0F, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 4; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F, 0.4F, 1.0F });
      localValueAnimator.setDuration(new long[] { 1260L, 430L, 1010L, 730L }[i]);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new long[] { 770L, 290L, 280L, 740L }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          LineScalePartyIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          LineScalePartyIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/LineScalePartyIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */