package com.wang.avi.indicators;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class LineSpinFadeLoaderIndicator
  extends BallSpinFadeLoaderIndicator
{
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f1 = getWidth() / 10;
    for (int i = 0; i < 8; i++)
    {
      paramCanvas.save();
      int k = getWidth();
      int j = getHeight();
      float f2 = getWidth() / 2.5F;
      double d = i;
      Double.isNaN(d);
      BallSpinFadeLoaderIndicator.Point localPoint = circleAt(k, j, f2 - f1, 0.7853981633974483D * d);
      paramCanvas.translate(localPoint.x, localPoint.y);
      paramCanvas.scale(this.scaleFloats[i], this.scaleFloats[i]);
      paramCanvas.rotate(i * 45);
      paramPaint.setAlpha(this.alphas[i]);
      f2 = -f1;
      paramCanvas.drawRoundRect(new RectF(f2, f2 / 1.5F, f1 * 1.5F, f1 / 1.5F), 5.0F, 5.0F, paramPaint);
      paramCanvas.restore();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/LineSpinFadeLoaderIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */