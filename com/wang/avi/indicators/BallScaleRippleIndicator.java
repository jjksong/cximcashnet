package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

public class BallScaleRippleIndicator
  extends BallScaleIndicator
{
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setStyle(Paint.Style.STROKE);
    paramPaint.setStrokeWidth(3.0F);
    super.draw(paramCanvas, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    localValueAnimator1.setInterpolator(new LinearInterpolator());
    localValueAnimator1.setDuration(1000L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallScaleRippleIndicator.this.scale = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallScaleRippleIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator2 = ValueAnimator.ofInt(new int[] { 0, 255 });
    localValueAnimator2.setInterpolator(new LinearInterpolator());
    localValueAnimator2.setDuration(1000L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallScaleRippleIndicator.this.alpha = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
        BallScaleRippleIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator1);
    localArrayList.add(localValueAnimator2);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallScaleRippleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */