package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class TriangleSkewSpinIndicator
  extends Indicator
{
  private Camera mCamera = new Camera();
  private Matrix mMatrix = new Matrix();
  private float rotateX;
  private float rotateY;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    this.mMatrix.reset();
    this.mCamera.save();
    this.mCamera.rotateX(this.rotateX);
    this.mCamera.rotateY(this.rotateY);
    this.mCamera.getMatrix(this.mMatrix);
    this.mCamera.restore();
    this.mMatrix.preTranslate(-centerX(), -centerY());
    this.mMatrix.postTranslate(centerX(), centerY());
    paramCanvas.concat(this.mMatrix);
    Path localPath = new Path();
    localPath.moveTo(getWidth() / 5, getHeight() * 4 / 5);
    localPath.lineTo(getWidth() * 4 / 5, getHeight() * 4 / 5);
    localPath.lineTo(getWidth() / 2, getHeight() / 5);
    localPath.close();
    paramCanvas.drawPath(localPath, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 180.0F, 0.0F, 0.0F });
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        TriangleSkewSpinIndicator.access$002(TriangleSkewSpinIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        TriangleSkewSpinIndicator.this.postInvalidate();
      }
    });
    localValueAnimator2.setInterpolator(new LinearInterpolator());
    localValueAnimator2.setRepeatCount(-1);
    localValueAnimator2.setDuration(2500L);
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 0.0F, 180.0F, 180.0F, 0.0F });
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        TriangleSkewSpinIndicator.access$102(TriangleSkewSpinIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        TriangleSkewSpinIndicator.this.postInvalidate();
      }
    });
    localValueAnimator1.setInterpolator(new LinearInterpolator());
    localValueAnimator1.setRepeatCount(-1);
    localValueAnimator1.setDuration(2500L);
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/TriangleSkewSpinIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */