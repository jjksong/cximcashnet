package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallClipRotateMultipleIndicator
  extends Indicator
{
  float degrees;
  float scaleFloat = 1.0F;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setStrokeWidth(3.0F);
    paramPaint.setStyle(Paint.Style.STROKE);
    float f2 = getWidth() / 2;
    float f1 = getHeight() / 2;
    paramCanvas.save();
    paramCanvas.translate(f2, f1);
    float f3 = this.scaleFloat;
    paramCanvas.scale(f3, f3);
    paramCanvas.rotate(this.degrees);
    int j = 0;
    for (int i = 0; i < 2; i++) {
      paramCanvas.drawArc(new RectF(-f2 + 12.0F, -f1 + 12.0F, f2 - 12.0F, f1 - 12.0F), new float[] { 135.0F, -45.0F }[i], 90.0F, false, paramPaint);
    }
    paramCanvas.restore();
    paramCanvas.translate(f2, f1);
    f3 = this.scaleFloat;
    paramCanvas.scale(f3, f3);
    paramCanvas.rotate(-this.degrees);
    for (i = j; i < 2; i++) {
      paramCanvas.drawArc(new RectF(-f2 / 1.8F + 12.0F, -f1 / 1.8F + 12.0F, f2 / 1.8F - 12.0F, f1 / 1.8F - 12.0F), new float[] { 225.0F, 45.0F }[i], 90.0F, false, paramPaint);
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.6F, 1.0F });
    localValueAnimator2.setDuration(1000L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotateMultipleIndicator.this.scaleFloat = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotateMultipleIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F });
    localValueAnimator1.setDuration(1000L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotateMultipleIndicator.this.degrees = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotateMultipleIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallClipRotateMultipleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */