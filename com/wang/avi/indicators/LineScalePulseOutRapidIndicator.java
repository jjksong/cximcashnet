package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import java.util.ArrayList;

public class LineScalePulseOutRapidIndicator
  extends LineScaleIndicator
{
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 5; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F, 0.4F, 1.0F });
      localValueAnimator.setDuration(1000L);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new long[] { 400L, 200L, 0L, 200L, 400L }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          LineScalePulseOutRapidIndicator.this.scaleYFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          LineScalePulseOutRapidIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/LineScalePulseOutRapidIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */