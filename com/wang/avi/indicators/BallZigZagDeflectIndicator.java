package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

public class BallZigZagDeflectIndicator
  extends BallZigZagIndicator
{
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f2 = getWidth() / 6;
    float f1 = getWidth() / 6;
    for (final int i = 0; i < 2; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { f2, getWidth() - f2, f2, getWidth() - f2, f2 });
      if (i == 1) {
        localValueAnimator1 = ValueAnimator.ofFloat(new float[] { getWidth() - f2, f2, getWidth() - f2, f2, getWidth() - f2 });
      }
      ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { f1, f1, getHeight() - f1, getHeight() - f1, f1 });
      if (i == 1) {
        localValueAnimator2 = ValueAnimator.ofFloat(new float[] { getHeight() - f1, getHeight() - f1, f1, f1, getHeight() - f1 });
      }
      localValueAnimator1.setDuration(2000L);
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setRepeatCount(-1);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallZigZagDeflectIndicator.this.translateX[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallZigZagDeflectIndicator.this.postInvalidate();
        }
      });
      localValueAnimator2.setDuration(2000L);
      localValueAnimator2.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallZigZagDeflectIndicator.this.translateY[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallZigZagDeflectIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallZigZagDeflectIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */