package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class PacmanIndicator
  extends Indicator
{
  private int alpha;
  private float degrees1;
  private float degrees2;
  private float translateX;
  
  private void drawCircle(Canvas paramCanvas, Paint paramPaint)
  {
    float f = getWidth() / 11;
    paramPaint.setAlpha(this.alpha);
    paramCanvas.drawCircle(this.translateX, getHeight() / 2, f, paramPaint);
  }
  
  private void drawPacman(Canvas paramCanvas, Paint paramPaint)
  {
    float f1 = getWidth() / 2;
    float f2 = getHeight() / 2;
    paramCanvas.save();
    paramCanvas.translate(f1, f2);
    paramCanvas.rotate(this.degrees1);
    paramPaint.setAlpha(255);
    float f5 = -f1 / 1.7F;
    float f3 = -f2 / 1.7F;
    float f6 = f1 / 1.7F;
    float f4 = f2 / 1.7F;
    paramCanvas.drawArc(new RectF(f5, f3, f6, f4), 0.0F, 270.0F, true, paramPaint);
    paramCanvas.restore();
    paramCanvas.save();
    paramCanvas.translate(f1, f2);
    paramCanvas.rotate(this.degrees2);
    paramPaint.setAlpha(255);
    paramCanvas.drawArc(new RectF(f5, f3, f6, f4), 90.0F, 270.0F, true, paramPaint);
    paramCanvas.restore();
  }
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    drawPacman(paramCanvas, paramPaint);
    drawCircle(paramCanvas, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f = getWidth() / 11;
    ValueAnimator localValueAnimator4 = ValueAnimator.ofFloat(new float[] { getWidth() - f, getWidth() / 2 });
    localValueAnimator4.setDuration(650L);
    localValueAnimator4.setInterpolator(new LinearInterpolator());
    localValueAnimator4.setRepeatCount(-1);
    addUpdateListener(localValueAnimator4, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        PacmanIndicator.access$002(PacmanIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        PacmanIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator2 = ValueAnimator.ofInt(new int[] { 255, 122 });
    localValueAnimator2.setDuration(650L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        PacmanIndicator.access$102(PacmanIndicator.this, ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue());
        PacmanIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator3 = ValueAnimator.ofFloat(new float[] { 0.0F, 45.0F, 0.0F });
    localValueAnimator3.setDuration(650L);
    localValueAnimator3.setRepeatCount(-1);
    addUpdateListener(localValueAnimator3, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        PacmanIndicator.access$202(PacmanIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        PacmanIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, -45.0F, 0.0F });
    localValueAnimator1.setDuration(650L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        PacmanIndicator.access$302(PacmanIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        PacmanIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator4);
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator3);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/PacmanIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */