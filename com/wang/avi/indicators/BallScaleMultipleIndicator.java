package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallScaleMultipleIndicator
  extends Indicator
{
  int[] alphaInts = { 255, 255, 255 };
  float[] scaleFloats = { 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    for (int i = 0; i < 3; i++)
    {
      paramPaint.setAlpha(this.alphaInts[i]);
      float[] arrayOfFloat = this.scaleFloats;
      paramCanvas.scale(arrayOfFloat[i], arrayOfFloat[i], getWidth() / 2, getHeight() / 2);
      paramCanvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2 - 4.0F, paramPaint);
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    long[] arrayOfLong = new long[3];
    long[] tmp15_13 = arrayOfLong;
    tmp15_13[0] = 0L;
    long[] tmp19_15 = tmp15_13;
    tmp19_15[1] = 200L;
    long[] tmp25_19 = tmp19_15;
    tmp25_19[2] = 400L;
    tmp25_19;
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
      localValueAnimator2.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setDuration(1000L);
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallScaleMultipleIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallScaleMultipleIndicator.this.postInvalidate();
        }
      });
      localValueAnimator2.setStartDelay(arrayOfLong[i]);
      ValueAnimator localValueAnimator1 = ValueAnimator.ofInt(new int[] { 255, 0 });
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setDuration(1000L);
      localValueAnimator1.setRepeatCount(-1);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallScaleMultipleIndicator.this.alphaInts[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallScaleMultipleIndicator.this.postInvalidate();
        }
      });
      localValueAnimator2.setStartDelay(arrayOfLong[i]);
      localArrayList.add(localValueAnimator2);
      localArrayList.add(localValueAnimator1);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallScaleMultipleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */