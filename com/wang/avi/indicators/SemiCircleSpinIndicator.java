package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class SemiCircleSpinIndicator
  extends Indicator
{
  private float degress;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramCanvas.rotate(this.degress, centerX(), centerY());
    paramCanvas.drawArc(new RectF(0.0F, 0.0F, getWidth(), getHeight()), -60.0F, 120.0F, false, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F });
    addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        SemiCircleSpinIndicator.access$002(SemiCircleSpinIndicator.this, ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue());
        SemiCircleSpinIndicator.this.postInvalidate();
      }
    });
    localValueAnimator.setDuration(600L);
    localValueAnimator.setRepeatCount(-1);
    localArrayList.add(localValueAnimator);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/SemiCircleSpinIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */