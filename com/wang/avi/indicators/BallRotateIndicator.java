package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallRotateIndicator
  extends Indicator
{
  float degress;
  private Matrix mMatrix = new Matrix();
  float scaleFloat = 0.5F;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f1 = getWidth() / 10;
    float f2 = getWidth() / 2;
    float f3 = getHeight() / 2;
    paramCanvas.rotate(this.degress, centerX(), centerY());
    paramCanvas.save();
    float f4 = 2.0F * f1;
    paramCanvas.translate(f2 - f4 - f1, f3);
    float f5 = this.scaleFloat;
    paramCanvas.scale(f5, f5);
    paramCanvas.drawCircle(0.0F, 0.0F, f1, paramPaint);
    paramCanvas.restore();
    paramCanvas.save();
    paramCanvas.translate(f2, f3);
    f5 = this.scaleFloat;
    paramCanvas.scale(f5, f5);
    paramCanvas.drawCircle(0.0F, 0.0F, f1, paramPaint);
    paramCanvas.restore();
    paramCanvas.save();
    paramCanvas.translate(f2 + f4 + f1, f3);
    f2 = this.scaleFloat;
    paramCanvas.scale(f2, f2);
    paramCanvas.drawCircle(0.0F, 0.0F, f1, paramPaint);
    paramCanvas.restore();
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.5F, 1.0F, 0.5F });
    localValueAnimator1.setDuration(1000L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallRotateIndicator.this.scaleFloat = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallRotateIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F });
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallRotateIndicator.this.degress = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallRotateIndicator.this.postInvalidate();
      }
    });
    localValueAnimator2.setDuration(1000L);
    localValueAnimator2.setRepeatCount(-1);
    localArrayList.add(localValueAnimator1);
    localArrayList.add(localValueAnimator2);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallRotateIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */