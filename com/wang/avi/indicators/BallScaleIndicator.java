package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallScaleIndicator
  extends Indicator
{
  int alpha = 255;
  float scale = 1.0F;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setAlpha(this.alpha);
    float f = this.scale;
    paramCanvas.scale(f, f, getWidth() / 2, getHeight() / 2);
    paramPaint.setAlpha(this.alpha);
    paramCanvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2 - 4.0F, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    localValueAnimator2.setInterpolator(new LinearInterpolator());
    localValueAnimator2.setDuration(1000L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallScaleIndicator.this.scale = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallScaleIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator1 = ValueAnimator.ofInt(new int[] { 255, 0 });
    localValueAnimator1.setInterpolator(new LinearInterpolator());
    localValueAnimator1.setDuration(1000L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallScaleIndicator.this.alpha = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
        BallScaleIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallScaleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */