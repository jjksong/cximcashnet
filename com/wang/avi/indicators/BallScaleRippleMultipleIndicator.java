package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

public class BallScaleRippleMultipleIndicator
  extends BallScaleMultipleIndicator
{
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setStyle(Paint.Style.STROKE);
    paramPaint.setStrokeWidth(3.0F);
    super.draw(paramCanvas, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    long[] arrayOfLong = new long[3];
    long[] tmp14_13 = arrayOfLong;
    tmp14_13[0] = 0L;
    long[] tmp18_14 = tmp14_13;
    tmp18_14[1] = 200L;
    long[] tmp24_18 = tmp18_14;
    tmp24_18[2] = 400L;
    tmp24_18;
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setDuration(1000L);
      localValueAnimator1.setRepeatCount(-1);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallScaleRippleMultipleIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallScaleRippleMultipleIndicator.this.postInvalidate();
        }
      });
      localValueAnimator1.setStartDelay(arrayOfLong[i]);
      ValueAnimator localValueAnimator2 = ValueAnimator.ofInt(new int[] { 0, 255 });
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setDuration(1000L);
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallScaleRippleMultipleIndicator.this.alphaInts[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallScaleRippleMultipleIndicator.this.postInvalidate();
        }
      });
      localValueAnimator1.setStartDelay(arrayOfLong[i]);
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallScaleRippleMultipleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */