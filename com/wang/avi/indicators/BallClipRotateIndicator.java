package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallClipRotateIndicator
  extends Indicator
{
  float degrees;
  float scaleFloat = 1.0F;
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setStyle(Paint.Style.STROKE);
    paramPaint.setStrokeWidth(3.0F);
    float f3 = getWidth() / 2;
    float f1 = getHeight() / 2;
    paramCanvas.translate(f3, f1);
    float f2 = this.scaleFloat;
    paramCanvas.scale(f2, f2);
    paramCanvas.rotate(this.degrees);
    paramCanvas.drawArc(new RectF(-f3 + 12.0F, -f1 + 12.0F, f3 + 0.0F - 12.0F, f1 + 0.0F - 12.0F), -45.0F, 270.0F, false, paramPaint);
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.6F, 0.5F, 1.0F });
    localValueAnimator2.setDuration(750L);
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotateIndicator.this.scaleFloat = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotateIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F });
    localValueAnimator1.setDuration(750L);
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        BallClipRotateIndicator.this.degrees = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        BallClipRotateIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallClipRotateIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */