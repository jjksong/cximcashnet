package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallGridPulseIndicator
  extends Indicator
{
  public static final int ALPHA = 255;
  public static final float SCALE = 1.0F;
  int[] alphas = { 255, 255, 255, 255, 255, 255, 255, 255, 255 };
  float[] scaleFloats = { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f2 = (getWidth() - 16.0F) / 6.0F;
    float f7 = getWidth() / 2;
    float f5 = 2.0F * f2;
    float f6 = f5 + 4.0F;
    float f3 = getWidth() / 2;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++)
      {
        paramCanvas.save();
        float f1 = j;
        float f4 = i;
        paramCanvas.translate(f5 * f1 + (f7 - f6) + f1 * 4.0F, f5 * f4 + (f3 - f6) + f4 * 4.0F);
        float[] arrayOfFloat = this.scaleFloats;
        int k = i * 3 + j;
        paramCanvas.scale(arrayOfFloat[k], arrayOfFloat[k]);
        paramPaint.setAlpha(this.alphas[k]);
        paramCanvas.drawCircle(0.0F, 0.0F, f2, paramPaint);
        paramCanvas.restore();
      }
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    int[] arrayOfInt2 = new int[9];
    int[] tmp16_14 = arrayOfInt2;
    tmp16_14[0] = 'ː';
    int[] tmp22_16 = tmp16_14;
    tmp22_16[1] = 'ϼ';
    int[] tmp28_22 = tmp22_16;
    tmp28_22[2] = 'Ԁ';
    int[] tmp34_28 = tmp28_22;
    tmp34_28[3] = '֌';
    int[] tmp40_34 = tmp34_28;
    tmp40_34[4] = '֪';
    int[] tmp46_40 = tmp40_34;
    tmp46_40[5] = 'Ҝ';
    int[] tmp52_46 = tmp46_40;
    tmp52_46[6] = 'ͦ';
    int[] tmp59_52 = tmp52_46;
    tmp59_52[7] = '֪';
    int[] tmp66_59 = tmp59_52;
    tmp66_59[8] = 'Ф';
    tmp66_59;
    int[] arrayOfInt1 = new int[9];
    int[] tmp80_79 = arrayOfInt1;
    tmp80_79[0] = -60;
    int[] tmp85_80 = tmp80_79;
    tmp85_80[1] = 'ú';
    int[] tmp91_85 = tmp85_80;
    tmp91_85[2] = 'ｖ';
    int[] tmp97_91 = tmp91_85;
    tmp97_91[3] = 'Ǡ';
    int[] tmp103_97 = tmp97_91;
    tmp103_97[4] = 'Ķ';
    int[] tmp109_103 = tmp103_97;
    tmp109_103[5] = 30;
    int[] tmp114_109 = tmp109_103;
    tmp114_109[6] = 'ǌ';
    int[] tmp121_114 = tmp114_109;
    tmp121_114[7] = '̌';
    int[] tmp128_121 = tmp121_114;
    tmp128_121[8] = 'ǂ';
    tmp128_121;
    for (final int i = 0; i < 9; i++)
    {
      ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.5F, 1.0F });
      localValueAnimator1.setDuration(arrayOfInt2[i]);
      localValueAnimator1.setRepeatCount(-1);
      localValueAnimator1.setStartDelay(arrayOfInt1[i]);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallGridPulseIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallGridPulseIndicator.this.postInvalidate();
        }
      });
      ValueAnimator localValueAnimator2 = ValueAnimator.ofInt(new int[] { 255, 210, 122, 255 });
      localValueAnimator2.setDuration(arrayOfInt2[i]);
      localValueAnimator2.setRepeatCount(-1);
      localValueAnimator2.setStartDelay(arrayOfInt1[i]);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallGridPulseIndicator.this.alphas[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallGridPulseIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallGridPulseIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */