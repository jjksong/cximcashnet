package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallPulseIndicator
  extends Indicator
{
  public static final float SCALE = 1.0F;
  private float[] scaleFloats = { 1.0F, 1.0F, 1.0F };
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f2 = (Math.min(getWidth(), getHeight()) - 8.0F) / 6.0F;
    float f1 = getWidth() / 2;
    float f4 = 2.0F * f2;
    float f3 = getHeight() / 2;
    for (int i = 0; i < 3; i++)
    {
      paramCanvas.save();
      float f5 = i;
      paramCanvas.translate(f4 * f5 + (f1 - (f4 + 4.0F)) + f5 * 4.0F, f3);
      float[] arrayOfFloat = this.scaleFloats;
      paramCanvas.scale(arrayOfFloat[i], arrayOfFloat[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, f2, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F, 0.3F, 1.0F });
      localValueAnimator.setDuration(750L);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new int[] { 120, 240, 360 }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallPulseIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallPulseIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallPulseIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */