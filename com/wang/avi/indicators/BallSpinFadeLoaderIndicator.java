package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallSpinFadeLoaderIndicator
  extends Indicator
{
  public static final int ALPHA = 255;
  public static final float SCALE = 1.0F;
  int[] alphas = { 255, 255, 255, 255, 255, 255, 255, 255 };
  float[] scaleFloats = { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
  
  Point circleAt(int paramInt1, int paramInt2, float paramFloat, double paramDouble)
  {
    double d3 = paramInt1 / 2;
    double d1 = paramFloat;
    double d2 = Math.cos(paramDouble);
    Double.isNaN(d1);
    Double.isNaN(d3);
    paramFloat = (float)(d3 + d2 * d1);
    d2 = paramInt2 / 2;
    paramDouble = Math.sin(paramDouble);
    Double.isNaN(d1);
    Double.isNaN(d2);
    return new Point(paramFloat, (float)(d2 + d1 * paramDouble));
  }
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f2 = getWidth() / 10;
    for (int i = 0; i < 8; i++)
    {
      paramCanvas.save();
      int j = getWidth();
      int k = getHeight();
      float f1 = getWidth() / 2;
      double d = i;
      Double.isNaN(d);
      Object localObject = circleAt(j, k, f1 - f2, 0.7853981633974483D * d);
      paramCanvas.translate(((Point)localObject).x, ((Point)localObject).y);
      localObject = this.scaleFloats;
      paramCanvas.scale(localObject[i], localObject[i]);
      paramPaint.setAlpha(this.alphas[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, f2, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    int[] arrayOfInt = new int[9];
    int[] tmp15_14 = arrayOfInt;
    tmp15_14[0] = 0;
    int[] tmp19_15 = tmp15_14;
    tmp19_15[1] = 120;
    int[] tmp24_19 = tmp19_15;
    tmp24_19[2] = 'ð';
    int[] tmp30_24 = tmp24_19;
    tmp30_24[3] = 'Ũ';
    int[] tmp36_30 = tmp30_24;
    tmp36_30[4] = 'Ǡ';
    int[] tmp42_36 = tmp36_30;
    tmp42_36[5] = 'ɘ';
    int[] tmp48_42 = tmp42_36;
    tmp48_42[6] = 'ː';
    int[] tmp55_48 = tmp48_42;
    tmp55_48[7] = '̌';
    int[] tmp62_55 = tmp55_48;
    tmp62_55[8] = '͈';
    tmp62_55;
    for (final int i = 0; i < 8; i++)
    {
      ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.4F, 1.0F });
      localValueAnimator2.setDuration(1000L);
      localValueAnimator2.setRepeatCount(-1);
      localValueAnimator2.setStartDelay(arrayOfInt[i]);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallSpinFadeLoaderIndicator.this.scaleFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallSpinFadeLoaderIndicator.this.postInvalidate();
        }
      });
      ValueAnimator localValueAnimator1 = ValueAnimator.ofInt(new int[] { 255, 77, 255 });
      localValueAnimator1.setDuration(1000L);
      localValueAnimator1.setRepeatCount(-1);
      localValueAnimator1.setStartDelay(arrayOfInt[i]);
      addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallSpinFadeLoaderIndicator.this.alphas[i] = ((Integer)paramAnonymousValueAnimator.getAnimatedValue()).intValue();
          BallSpinFadeLoaderIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator2);
      localArrayList.add(localValueAnimator1);
    }
    return localArrayList;
  }
  
  final class Point
  {
    public float x;
    public float y;
    
    public Point(float paramFloat1, float paramFloat2)
    {
      this.x = paramFloat1;
      this.y = paramFloat2;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallSpinFadeLoaderIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */