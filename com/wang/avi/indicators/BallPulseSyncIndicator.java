package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class BallPulseSyncIndicator
  extends Indicator
{
  float[] translateYFloats = new float[3];
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f1 = (getWidth() - 8.0F) / 6.0F;
    float f4 = getWidth() / 2;
    float f3 = 2.0F * f1;
    for (int i = 0; i < 3; i++)
    {
      paramCanvas.save();
      float f2 = i;
      paramCanvas.translate(f3 * f2 + (f4 - (f3 + 4.0F)) + f2 * 4.0F, this.translateYFloats[i]);
      paramCanvas.drawCircle(0.0F, 0.0F, f1, paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f = (getWidth() - 8.0F) / 6.0F;
    for (final int i = 0; i < 3; i++)
    {
      ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { getHeight() / 2, getHeight() / 2 - 2.0F * f, getHeight() / 2 });
      localValueAnimator.setDuration(600L);
      localValueAnimator.setRepeatCount(-1);
      localValueAnimator.setStartDelay(new int[] { 70, 140, 210 }[i]);
      addUpdateListener(localValueAnimator, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          BallPulseSyncIndicator.this.translateYFloats[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          BallPulseSyncIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator);
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/BallPulseSyncIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */