package com.wang.avi.indicators;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.animation.LinearInterpolator;
import com.wang.avi.Indicator;
import java.util.ArrayList;

public class CubeTransitionIndicator
  extends Indicator
{
  float degrees;
  float scaleFloat = 1.0F;
  float[] translateX = new float[2];
  float[] translateY = new float[2];
  
  public void draw(Canvas paramCanvas, Paint paramPaint)
  {
    float f3 = getWidth() / 5;
    float f1 = getHeight() / 5;
    for (int i = 0; i < 2; i++)
    {
      paramCanvas.save();
      paramCanvas.translate(this.translateX[i], this.translateY[i]);
      paramCanvas.rotate(this.degrees);
      float f2 = this.scaleFloat;
      paramCanvas.scale(f2, f2);
      paramCanvas.drawRect(new RectF(-f3 / 2.0F, -f1 / 2.0F, f3 / 2.0F, f1 / 2.0F), paramPaint);
      paramCanvas.restore();
    }
  }
  
  public ArrayList<ValueAnimator> onCreateAnimators()
  {
    ArrayList localArrayList = new ArrayList();
    float f1 = getWidth() / 5;
    float f2 = getHeight() / 5;
    for (final int i = 0; i < 2; i++)
    {
      this.translateX[i] = f1;
      localValueAnimator1 = ValueAnimator.ofFloat(new float[] { f1, getWidth() - f1, getWidth() - f1, f1, f1 });
      if (i == 1) {
        localValueAnimator1 = ValueAnimator.ofFloat(new float[] { getWidth() - f1, f1, f1, getWidth() - f1, getWidth() - f1 });
      }
      localValueAnimator1.setInterpolator(new LinearInterpolator());
      localValueAnimator1.setDuration(1600L);
      localValueAnimator1.setRepeatCount(-1);
      localValueAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          CubeTransitionIndicator.this.translateX[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          CubeTransitionIndicator.this.postInvalidate();
        }
      });
      this.translateY[i] = f2;
      localValueAnimator2 = ValueAnimator.ofFloat(new float[] { f2, f2, getHeight() - f2, getHeight() - f2, f2 });
      if (i == 1) {
        localValueAnimator2 = ValueAnimator.ofFloat(new float[] { getHeight() - f2, getHeight() - f2, f2, f2, getHeight() - f2 });
      }
      localValueAnimator2.setDuration(1600L);
      localValueAnimator2.setInterpolator(new LinearInterpolator());
      localValueAnimator2.setRepeatCount(-1);
      addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          CubeTransitionIndicator.this.translateY[i] = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          CubeTransitionIndicator.this.postInvalidate();
        }
      });
      localArrayList.add(localValueAnimator1);
      localArrayList.add(localValueAnimator2);
    }
    ValueAnimator localValueAnimator2 = ValueAnimator.ofFloat(new float[] { 1.0F, 0.5F, 1.0F, 0.5F, 1.0F });
    localValueAnimator2.setDuration(1600L);
    localValueAnimator2.setInterpolator(new LinearInterpolator());
    localValueAnimator2.setRepeatCount(-1);
    addUpdateListener(localValueAnimator2, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        CubeTransitionIndicator.this.scaleFloat = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        CubeTransitionIndicator.this.postInvalidate();
      }
    });
    ValueAnimator localValueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, 180.0F, 360.0F, 540.0F, 720.0F });
    localValueAnimator1.setDuration(1600L);
    localValueAnimator1.setInterpolator(new LinearInterpolator());
    localValueAnimator1.setRepeatCount(-1);
    addUpdateListener(localValueAnimator1, new ValueAnimator.AnimatorUpdateListener()
    {
      public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
      {
        CubeTransitionIndicator.this.degrees = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
        CubeTransitionIndicator.this.postInvalidate();
      }
    });
    localArrayList.add(localValueAnimator2);
    localArrayList.add(localValueAnimator1);
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/indicators/CubeTransitionIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */