package com.wang.avi;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public abstract class Indicator
  extends Drawable
  implements Animatable
{
  private static final Rect ZERO_BOUNDS_RECT = new Rect();
  private int alpha = 255;
  protected Rect drawBounds = ZERO_BOUNDS_RECT;
  private ArrayList<ValueAnimator> mAnimators;
  private boolean mHasAnimators;
  private Paint mPaint = new Paint();
  private HashMap<ValueAnimator, ValueAnimator.AnimatorUpdateListener> mUpdateListeners = new HashMap();
  
  public Indicator()
  {
    this.mPaint.setColor(-1);
    this.mPaint.setStyle(Paint.Style.FILL);
    this.mPaint.setAntiAlias(true);
  }
  
  private void ensureAnimators()
  {
    if (!this.mHasAnimators)
    {
      this.mAnimators = onCreateAnimators();
      this.mHasAnimators = true;
    }
  }
  
  private boolean isStarted()
  {
    Iterator localIterator = this.mAnimators.iterator();
    if (localIterator.hasNext()) {
      return ((ValueAnimator)localIterator.next()).isStarted();
    }
    return false;
  }
  
  private void startAnimators()
  {
    for (int i = 0; i < this.mAnimators.size(); i++)
    {
      ValueAnimator localValueAnimator = (ValueAnimator)this.mAnimators.get(i);
      ValueAnimator.AnimatorUpdateListener localAnimatorUpdateListener = (ValueAnimator.AnimatorUpdateListener)this.mUpdateListeners.get(localValueAnimator);
      if (localAnimatorUpdateListener != null) {
        localValueAnimator.addUpdateListener(localAnimatorUpdateListener);
      }
      localValueAnimator.start();
    }
  }
  
  private void stopAnimators()
  {
    Object localObject = this.mAnimators;
    if (localObject != null)
    {
      localObject = ((ArrayList)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        ValueAnimator localValueAnimator = (ValueAnimator)((Iterator)localObject).next();
        if ((localValueAnimator != null) && (localValueAnimator.isStarted()))
        {
          localValueAnimator.removeAllUpdateListeners();
          localValueAnimator.end();
        }
      }
    }
  }
  
  public void addUpdateListener(ValueAnimator paramValueAnimator, ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener)
  {
    this.mUpdateListeners.put(paramValueAnimator, paramAnimatorUpdateListener);
  }
  
  public int centerX()
  {
    return this.drawBounds.centerX();
  }
  
  public int centerY()
  {
    return this.drawBounds.centerY();
  }
  
  public void draw(Canvas paramCanvas)
  {
    draw(paramCanvas, this.mPaint);
  }
  
  public abstract void draw(Canvas paramCanvas, Paint paramPaint);
  
  public float exactCenterX()
  {
    return this.drawBounds.exactCenterX();
  }
  
  public float exactCenterY()
  {
    return this.drawBounds.exactCenterY();
  }
  
  public int getAlpha()
  {
    return this.alpha;
  }
  
  public int getColor()
  {
    return this.mPaint.getColor();
  }
  
  public Rect getDrawBounds()
  {
    return this.drawBounds;
  }
  
  public int getHeight()
  {
    return this.drawBounds.height();
  }
  
  public int getOpacity()
  {
    return -1;
  }
  
  public int getWidth()
  {
    return this.drawBounds.width();
  }
  
  public boolean isRunning()
  {
    Iterator localIterator = this.mAnimators.iterator();
    if (localIterator.hasNext()) {
      return ((ValueAnimator)localIterator.next()).isRunning();
    }
    return false;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    super.onBoundsChange(paramRect);
    setDrawBounds(paramRect);
  }
  
  public abstract ArrayList<ValueAnimator> onCreateAnimators();
  
  public void postInvalidate()
  {
    invalidateSelf();
  }
  
  public void setAlpha(int paramInt)
  {
    this.alpha = paramInt;
  }
  
  public void setColor(int paramInt)
  {
    this.mPaint.setColor(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
  
  public void setDrawBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.drawBounds = new Rect(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setDrawBounds(Rect paramRect)
  {
    setDrawBounds(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public void start()
  {
    ensureAnimators();
    if (this.mAnimators == null) {
      return;
    }
    if (isStarted()) {
      return;
    }
    startAnimators();
    invalidateSelf();
  }
  
  public void stop()
  {
    stopAnimators();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/Indicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */