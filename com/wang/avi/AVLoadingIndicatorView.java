package com.wang.avi;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.wang.avi.indicators.BallPulseIndicator;

public class AVLoadingIndicatorView
  extends View
{
  private static final BallPulseIndicator DEFAULT_INDICATOR = new BallPulseIndicator();
  private static final int MIN_DELAY = 500;
  private static final int MIN_SHOW_TIME = 500;
  private static final String TAG = "AVLoadingIndicatorView";
  private final Runnable mDelayedHide = new Runnable()
  {
    public void run()
    {
      AVLoadingIndicatorView.access$002(AVLoadingIndicatorView.this, false);
      AVLoadingIndicatorView.access$102(AVLoadingIndicatorView.this, -1L);
      AVLoadingIndicatorView.this.setVisibility(8);
    }
  };
  private final Runnable mDelayedShow = new Runnable()
  {
    public void run()
    {
      AVLoadingIndicatorView.access$202(AVLoadingIndicatorView.this, false);
      if (!AVLoadingIndicatorView.this.mDismissed)
      {
        AVLoadingIndicatorView.access$102(AVLoadingIndicatorView.this, System.currentTimeMillis());
        AVLoadingIndicatorView.this.setVisibility(0);
      }
    }
  };
  private boolean mDismissed = false;
  private Indicator mIndicator;
  private int mIndicatorColor;
  int mMaxHeight;
  int mMaxWidth;
  int mMinHeight;
  int mMinWidth;
  private boolean mPostedHide = false;
  private boolean mPostedShow = false;
  private boolean mShouldStartAnimationDrawable;
  private long mStartTime = -1L;
  
  public AVLoadingIndicatorView(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null, 0, 0);
  }
  
  public AVLoadingIndicatorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet, 0, R.style.AVLoadingIndicatorView);
  }
  
  public AVLoadingIndicatorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet, paramInt, R.style.AVLoadingIndicatorView);
  }
  
  @TargetApi(21)
  public AVLoadingIndicatorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet, paramInt1, R.style.AVLoadingIndicatorView);
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    this.mMinWidth = 24;
    this.mMaxWidth = 48;
    this.mMinHeight = 24;
    this.mMaxHeight = 48;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AVLoadingIndicatorView, paramInt1, paramInt2);
    this.mMinWidth = paramAttributeSet.getDimensionPixelSize(R.styleable.AVLoadingIndicatorView_minWidth, this.mMinWidth);
    this.mMaxWidth = paramAttributeSet.getDimensionPixelSize(R.styleable.AVLoadingIndicatorView_maxWidth, this.mMaxWidth);
    this.mMinHeight = paramAttributeSet.getDimensionPixelSize(R.styleable.AVLoadingIndicatorView_minHeight, this.mMinHeight);
    this.mMaxHeight = paramAttributeSet.getDimensionPixelSize(R.styleable.AVLoadingIndicatorView_maxHeight, this.mMaxHeight);
    paramContext = paramAttributeSet.getString(R.styleable.AVLoadingIndicatorView_indicatorName);
    this.mIndicatorColor = paramAttributeSet.getColor(R.styleable.AVLoadingIndicatorView_indicatorColor, -1);
    setIndicator(paramContext);
    if (this.mIndicator == null) {
      setIndicator(DEFAULT_INDICATOR);
    }
    paramAttributeSet.recycle();
  }
  
  private void removeCallbacks()
  {
    removeCallbacks(this.mDelayedHide);
    removeCallbacks(this.mDelayedShow);
  }
  
  private void updateDrawableBounds(int paramInt1, int paramInt2)
  {
    int j = paramInt1 - (getPaddingRight() + getPaddingLeft());
    paramInt1 = paramInt2 - (getPaddingTop() + getPaddingBottom());
    Indicator localIndicator = this.mIndicator;
    if (localIndicator != null)
    {
      int i = localIndicator.getIntrinsicWidth();
      paramInt2 = this.mIndicator.getIntrinsicHeight();
      float f1 = i / paramInt2;
      float f3 = j;
      float f4 = paramInt1;
      float f2 = f3 / f4;
      i = 0;
      if (f1 != f2)
      {
        int k;
        if (f2 > f1)
        {
          k = (int)(f4 * f1);
          paramInt2 = (j - k) / 2;
          i = paramInt2;
          j = k + paramInt2;
          paramInt2 = 0;
        }
        else
        {
          k = (int)(f3 * (1.0F / f1));
          paramInt2 = (paramInt1 - k) / 2;
          paramInt1 = k + paramInt2;
        }
      }
      else
      {
        paramInt2 = 0;
      }
      this.mIndicator.setBounds(i, paramInt2, j, paramInt1);
    }
  }
  
  private void updateDrawableState()
  {
    int[] arrayOfInt = getDrawableState();
    Indicator localIndicator = this.mIndicator;
    if ((localIndicator != null) && (localIndicator.isStateful())) {
      this.mIndicator.setState(arrayOfInt);
    }
  }
  
  void drawTrack(Canvas paramCanvas)
  {
    Indicator localIndicator = this.mIndicator;
    if (localIndicator != null)
    {
      int i = paramCanvas.save();
      paramCanvas.translate(getPaddingLeft(), getPaddingTop());
      localIndicator.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
      if ((this.mShouldStartAnimationDrawable) && ((localIndicator instanceof Animatable)))
      {
        ((Animatable)localIndicator).start();
        this.mShouldStartAnimationDrawable = false;
      }
    }
  }
  
  @TargetApi(21)
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2)
  {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Indicator localIndicator = this.mIndicator;
    if (localIndicator != null) {
      localIndicator.setHotspot(paramFloat1, paramFloat2);
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    updateDrawableState();
  }
  
  public Indicator getIndicator()
  {
    return this.mIndicator;
  }
  
  public void hide()
  {
    this.mDismissed = true;
    removeCallbacks(this.mDelayedShow);
    long l2 = System.currentTimeMillis();
    long l1 = this.mStartTime;
    l2 -= l1;
    if ((l2 < 500L) && (l1 != -1L))
    {
      if (!this.mPostedHide)
      {
        postDelayed(this.mDelayedHide, 500L - l2);
        this.mPostedHide = true;
      }
    }
    else {
      setVisibility(8);
    }
  }
  
  public void invalidateDrawable(Drawable paramDrawable)
  {
    if (verifyDrawable(paramDrawable))
    {
      paramDrawable = paramDrawable.getBounds();
      int j = getScrollX() + getPaddingLeft();
      int i = getScrollY() + getPaddingTop();
      invalidate(paramDrawable.left + j, paramDrawable.top + i, paramDrawable.right + j, paramDrawable.bottom + i);
    }
    else
    {
      super.invalidateDrawable(paramDrawable);
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    startAnimation();
    removeCallbacks();
  }
  
  protected void onDetachedFromWindow()
  {
    stopAnimation();
    super.onDetachedFromWindow();
    removeCallbacks();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    try
    {
      super.onDraw(paramCanvas);
      drawTrack(paramCanvas);
      return;
    }
    finally
    {
      paramCanvas = finally;
      throw paramCanvas;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    try
    {
      Indicator localIndicator = this.mIndicator;
      int i;
      int j;
      if (localIndicator != null)
      {
        i = Math.max(this.mMinWidth, Math.min(this.mMaxWidth, localIndicator.getIntrinsicWidth()));
        j = Math.max(this.mMinHeight, Math.min(this.mMaxHeight, localIndicator.getIntrinsicHeight()));
      }
      else
      {
        j = 0;
        i = 0;
      }
      updateDrawableState();
      int i1 = getPaddingLeft();
      int k = getPaddingRight();
      int n = getPaddingTop();
      int m = getPaddingBottom();
      setMeasuredDimension(resolveSizeAndState(i + (i1 + k), paramInt1, 0), resolveSizeAndState(j + (n + m), paramInt2, 0));
      return;
    }
    finally {}
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    updateDrawableBounds(paramInt1, paramInt2);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    super.onVisibilityChanged(paramView, paramInt);
    if ((paramInt != 8) && (paramInt != 4)) {
      startAnimation();
    } else {
      stopAnimation();
    }
  }
  
  public void setIndicator(Indicator paramIndicator)
  {
    Indicator localIndicator = this.mIndicator;
    if (localIndicator != paramIndicator)
    {
      if (localIndicator != null)
      {
        localIndicator.setCallback(null);
        unscheduleDrawable(this.mIndicator);
      }
      this.mIndicator = paramIndicator;
      setIndicatorColor(this.mIndicatorColor);
      if (paramIndicator != null) {
        paramIndicator.setCallback(this);
      }
      postInvalidate();
    }
  }
  
  public void setIndicator(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    if (!paramString.contains("."))
    {
      localStringBuilder.append(getClass().getPackage().getName());
      localStringBuilder.append(".indicators");
      localStringBuilder.append(".");
    }
    localStringBuilder.append(paramString);
    try
    {
      setIndicator((Indicator)Class.forName(localStringBuilder.toString()).newInstance());
    }
    catch (IllegalAccessException paramString)
    {
      paramString.printStackTrace();
    }
    catch (InstantiationException paramString)
    {
      paramString.printStackTrace();
    }
    catch (ClassNotFoundException paramString)
    {
      Log.e("AVLoadingIndicatorView", "Didn't find your class , check the name again !");
    }
  }
  
  public void setIndicatorColor(int paramInt)
  {
    this.mIndicatorColor = paramInt;
    this.mIndicator.setColor(paramInt);
  }
  
  public void setVisibility(int paramInt)
  {
    if (getVisibility() != paramInt)
    {
      super.setVisibility(paramInt);
      if ((paramInt != 8) && (paramInt != 4)) {
        startAnimation();
      } else {
        stopAnimation();
      }
    }
  }
  
  public void show()
  {
    this.mStartTime = -1L;
    this.mDismissed = false;
    removeCallbacks(this.mDelayedHide);
    if (!this.mPostedShow)
    {
      postDelayed(this.mDelayedShow, 500L);
      this.mPostedShow = true;
    }
  }
  
  public void smoothToHide()
  {
    startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
    setVisibility(8);
  }
  
  public void smoothToShow()
  {
    startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
    setVisibility(0);
  }
  
  void startAnimation()
  {
    if (getVisibility() != 0) {
      return;
    }
    if ((this.mIndicator instanceof Animatable)) {
      this.mShouldStartAnimationDrawable = true;
    }
    postInvalidate();
  }
  
  void stopAnimation()
  {
    Indicator localIndicator = this.mIndicator;
    if ((localIndicator instanceof Animatable))
    {
      localIndicator.stop();
      this.mShouldStartAnimationDrawable = false;
    }
    postInvalidate();
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    boolean bool;
    if ((paramDrawable != this.mIndicator) && (!super.verifyDrawable(paramDrawable))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/wang/avi/AVLoadingIndicatorView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */