package com.hjq.permissions;

import java.util.List;

public abstract interface OnPermission
{
  public abstract void hasPermission(List<String> paramList, boolean paramBoolean);
  
  public abstract void noPermission(List<String> paramList, boolean paramBoolean);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hjq/permissions/OnPermission.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */