package com.hjq.permissions;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import java.util.List;

final class PermissionSettingPage
{
  private static final String MARK = Build.MANUFACTURER.toLowerCase();
  
  private static Intent google(Context paramContext)
  {
    Intent localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
    localIntent.setData(Uri.fromParts("package", paramContext.getPackageName(), null));
    return localIntent;
  }
  
  private static boolean hasIntent(Context paramContext, Intent paramIntent)
  {
    return paramContext.getPackageManager().queryIntentActivities(paramIntent, 65536).isEmpty() ^ true;
  }
  
  private static Intent huawei(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.permissionmanager.ui.MainActivity"));
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.addviewmonitor.AddViewMonitorActivity"));
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.notificationmanager.ui.NotificationManagmentActivity"));
    return localIntent;
  }
  
  private static Intent meizu(Context paramContext)
  {
    Intent localIntent = new Intent("com.meizu.safe.security.SHOW_APPSEC");
    localIntent.putExtra("packageName", paramContext.getPackageName());
    localIntent.setComponent(new ComponentName("com.meizu.safe", "com.meizu.safe.security.AppSecActivity"));
    return localIntent;
  }
  
  private static Intent oppo(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("packageName", paramContext.getPackageName());
    localIntent.setClassName("com.color.safecenter", "com.color.safecenter.permission.floatwindow.FloatWindowListActivity");
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setClassName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity");
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setClassName("com.oppo.safe", "com.oppo.safe.permission.PermissionAppListActivity");
    return localIntent;
  }
  
  static void start(Context paramContext, boolean paramBoolean)
  {
    Intent localIntent1;
    if (MARK.contains("huawei")) {
      localIntent1 = huawei(paramContext);
    } else if (MARK.contains("xiaomi")) {
      localIntent1 = xiaomi(paramContext);
    } else if (MARK.contains("oppo")) {
      localIntent1 = oppo(paramContext);
    } else if (MARK.contains("vivo")) {
      localIntent1 = vivo(paramContext);
    } else if (MARK.contains("meizu")) {
      localIntent1 = meizu(paramContext);
    } else {
      localIntent1 = null;
    }
    Intent localIntent2;
    if (localIntent1 != null)
    {
      localIntent2 = localIntent1;
      if (hasIntent(paramContext, localIntent1)) {}
    }
    else
    {
      localIntent2 = google(paramContext);
    }
    if (paramBoolean) {
      localIntent2.addFlags(268435456);
    }
    try
    {
      paramContext.startActivity(localIntent2);
    }
    catch (Exception localException)
    {
      paramContext.startActivity(google(paramContext));
    }
  }
  
  private static Intent vivo(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.setClassName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.FloatWindowManager");
    localIntent.putExtra("packagename", paramContext.getPackageName());
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.safeguard.SoftPermissionDetailActivity"));
    return localIntent;
  }
  
  private static Intent xiaomi(Context paramContext)
  {
    Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
    localIntent.putExtra("extra_pkgname", paramContext.getPackageName());
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setPackage("com.miui.securitycenter");
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity");
    if (hasIntent(paramContext, localIntent)) {
      return localIntent;
    }
    localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.PermissionsEditorActivity");
    return localIntent;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hjq/permissions/PermissionSettingPage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */