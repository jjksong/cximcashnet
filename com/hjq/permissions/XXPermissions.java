package com.hjq.permissions;

import android.app.Activity;
import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class XXPermissions
{
  private Activity mActivity;
  private boolean mConstant;
  private List<String> mPermissions;
  
  private XXPermissions(Activity paramActivity)
  {
    this.mActivity = paramActivity;
  }
  
  public static void gotoPermissionSettings(Context paramContext)
  {
    PermissionSettingPage.start(paramContext, false);
  }
  
  public static void gotoPermissionSettings(Context paramContext, boolean paramBoolean)
  {
    PermissionSettingPage.start(paramContext, paramBoolean);
  }
  
  public static boolean isHasPermission(Context paramContext, String... paramVarArgs)
  {
    paramContext = PermissionUtils.getFailPermissions(paramContext, Arrays.asList(paramVarArgs));
    boolean bool;
    if ((paramContext != null) && (!paramContext.isEmpty())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public static boolean isHasPermission(Context paramContext, String[]... paramVarArgs)
  {
    ArrayList localArrayList = new ArrayList();
    int j = paramVarArgs.length;
    boolean bool = false;
    for (int i = 0; i < j; i++) {
      localArrayList.addAll(Arrays.asList(paramVarArgs[i]));
    }
    paramContext = PermissionUtils.getFailPermissions(paramContext, localArrayList);
    if ((paramContext == null) || (paramContext.isEmpty())) {
      bool = true;
    }
    return bool;
  }
  
  public static XXPermissions with(Activity paramActivity)
  {
    return new XXPermissions(paramActivity);
  }
  
  public XXPermissions constantRequest()
  {
    this.mConstant = true;
    return this;
  }
  
  public XXPermissions permission(List<String> paramList)
  {
    List localList = this.mPermissions;
    if (localList == null) {
      this.mPermissions = paramList;
    } else {
      localList.addAll(paramList);
    }
    return this;
  }
  
  public XXPermissions permission(String... paramVarArgs)
  {
    if (this.mPermissions == null) {
      this.mPermissions = new ArrayList(paramVarArgs.length);
    }
    this.mPermissions.addAll(Arrays.asList(paramVarArgs));
    return this;
  }
  
  public XXPermissions permission(String[]... paramVarArgs)
  {
    Object localObject = this.mPermissions;
    int k = 0;
    if (localObject == null)
    {
      int m = paramVarArgs.length;
      i = 0;
      j = 0;
      while (i < m)
      {
        j += paramVarArgs[i].length;
        i++;
      }
      this.mPermissions = new ArrayList(j);
    }
    int j = paramVarArgs.length;
    for (int i = k; i < j; i++)
    {
      localObject = paramVarArgs[i];
      this.mPermissions.addAll(Arrays.asList((Object[])localObject));
    }
    return this;
  }
  
  public void request(OnPermission paramOnPermission)
  {
    Object localObject = this.mPermissions;
    if ((localObject == null) || (((List)localObject).isEmpty())) {
      this.mPermissions = PermissionUtils.getManifestPermissions(this.mActivity);
    }
    localObject = this.mPermissions;
    if ((localObject != null) && (!((List)localObject).isEmpty()))
    {
      localObject = this.mActivity;
      if (localObject != null)
      {
        if (paramOnPermission != null)
        {
          PermissionUtils.checkTargetSdkVersion((Context)localObject, this.mPermissions);
          localObject = PermissionUtils.getFailPermissions(this.mActivity, this.mPermissions);
          if ((localObject != null) && (!((ArrayList)localObject).isEmpty()))
          {
            PermissionUtils.checkPermissions(this.mActivity, this.mPermissions);
            PermissionFragment.newInstance(new ArrayList(this.mPermissions), this.mConstant).prepareRequest(this.mActivity, paramOnPermission);
          }
          else
          {
            paramOnPermission.hasPermission(this.mPermissions, true);
          }
          return;
        }
        throw new IllegalArgumentException("The permission request callback interface must be implemented");
      }
      throw new IllegalArgumentException("The activity is empty");
    }
    throw new IllegalArgumentException("The requested permission cannot be empty");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hjq/permissions/XXPermissions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */