package com.hjq.permissions;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class PermissionFragment
  extends Fragment
  implements Runnable
{
  private static final String PERMISSION_GROUP = "permission_group";
  private static final String REQUEST_CODE = "request_code";
  private static final String REQUEST_CONSTANT = "request_constant";
  private static final SparseArray<OnPermission> sContainer = new SparseArray();
  private boolean isBackCall;
  
  public static PermissionFragment newInstance(ArrayList<String> paramArrayList, boolean paramBoolean)
  {
    PermissionFragment localPermissionFragment = new PermissionFragment();
    Bundle localBundle = new Bundle();
    int i;
    do
    {
      i = new Random().nextInt(255);
    } while (sContainer.get(i) != null);
    localBundle.putInt("request_code", i);
    localBundle.putStringArrayList("permission_group", paramArrayList);
    localBundle.putBoolean("request_constant", paramBoolean);
    localPermissionFragment.setArguments(localBundle);
    return localPermissionFragment;
  }
  
  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getArguments().getStringArrayList("permission_group");
    if (paramBundle == null) {
      return;
    }
    if (((paramBundle.contains("android.permission.REQUEST_INSTALL_PACKAGES")) && (!PermissionUtils.isHasInstallPermission(getActivity()))) || ((paramBundle.contains("android.permission.SYSTEM_ALERT_WINDOW")) && (!PermissionUtils.isHasOverlaysPermission(getActivity()))))
    {
      if ((paramBundle.contains("android.permission.REQUEST_INSTALL_PACKAGES")) && (!PermissionUtils.isHasInstallPermission(getActivity())))
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("package:");
        localStringBuilder.append(getActivity().getPackageName());
        startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES", Uri.parse(localStringBuilder.toString())), getArguments().getInt("request_code"));
      }
      if ((paramBundle.contains("android.permission.SYSTEM_ALERT_WINDOW")) && (!PermissionUtils.isHasOverlaysPermission(getActivity())))
      {
        paramBundle = new StringBuilder();
        paramBundle.append("package:");
        paramBundle.append(getActivity().getPackageName());
        startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse(paramBundle.toString())), getArguments().getInt("request_code"));
      }
    }
    else
    {
      requestPermission();
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((!this.isBackCall) && (paramInt1 == getArguments().getInt("request_code")))
    {
      this.isBackCall = true;
      new Handler(Looper.getMainLooper()).postDelayed(this, 500L);
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    OnPermission localOnPermission = (OnPermission)sContainer.get(paramInt);
    if (localOnPermission == null) {
      return;
    }
    for (int i = 0; i < paramArrayOfString.length; i++)
    {
      if ("android.permission.REQUEST_INSTALL_PACKAGES".equals(paramArrayOfString[i])) {
        if (PermissionUtils.isHasInstallPermission(getActivity())) {
          paramArrayOfInt[i] = 0;
        } else {
          paramArrayOfInt[i] = -1;
        }
      }
      if ("android.permission.SYSTEM_ALERT_WINDOW".equals(paramArrayOfString[i])) {
        if (PermissionUtils.isHasOverlaysPermission(getActivity())) {
          paramArrayOfInt[i] = 0;
        } else {
          paramArrayOfInt[i] = -1;
        }
      }
      if (((paramArrayOfString[i].equals("android.permission.ANSWER_PHONE_CALLS")) || (paramArrayOfString[i].equals("android.permission.READ_PHONE_NUMBERS"))) && (!PermissionUtils.isOverOreo())) {
        paramArrayOfInt[i] = 0;
      }
    }
    List localList = PermissionUtils.getSucceedPermissions(paramArrayOfString, paramArrayOfInt);
    if (localList.size() == paramArrayOfString.length)
    {
      localOnPermission.hasPermission(localList, true);
    }
    else
    {
      paramArrayOfString = PermissionUtils.getFailPermissions(paramArrayOfString, paramArrayOfInt);
      if ((getArguments().getBoolean("request_constant")) && (PermissionUtils.isRequestDeniedPermission(getActivity(), paramArrayOfString)))
      {
        requestPermission();
        return;
      }
      localOnPermission.noPermission(paramArrayOfString, PermissionUtils.checkMorePermissionPermanentDenied(getActivity(), paramArrayOfString));
      if (!localList.isEmpty()) {
        localOnPermission.hasPermission(localList, false);
      }
    }
    sContainer.remove(paramInt);
    getFragmentManager().beginTransaction().remove(this).commit();
  }
  
  public void prepareRequest(Activity paramActivity, OnPermission paramOnPermission)
  {
    sContainer.put(getArguments().getInt("request_code"), paramOnPermission);
    paramActivity.getFragmentManager().beginTransaction().add(this, paramActivity.getClass().getName()).commit();
  }
  
  public void requestPermission()
  {
    if (PermissionUtils.isOverMarshmallow())
    {
      ArrayList localArrayList = getArguments().getStringArrayList("permission_group");
      requestPermissions((String[])localArrayList.toArray(new String[localArrayList.size() - 1]), getArguments().getInt("request_code"));
    }
  }
  
  public void run()
  {
    requestPermission();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hjq/permissions/PermissionFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */