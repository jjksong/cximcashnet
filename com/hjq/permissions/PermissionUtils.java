package com.hjq.permissions;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.provider.Settings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

final class PermissionUtils
{
  static boolean checkMorePermissionPermanentDenied(Activity paramActivity, List<String> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      String str = (String)paramList.next();
      if ((!str.equals("android.permission.REQUEST_INSTALL_PACKAGES")) && (!str.equals("android.permission.SYSTEM_ALERT_WINDOW")) && (checkSinglePermissionPermanentDenied(paramActivity, str))) {
        return true;
      }
    }
    return false;
  }
  
  static void checkPermissions(Activity paramActivity, List<String> paramList)
  {
    paramActivity = getManifestPermissions(paramActivity);
    if ((paramActivity != null) && (!paramActivity.isEmpty()))
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        paramList = (String)localIterator.next();
        if (!paramActivity.contains(paramList)) {
          throw new ManifestRegisterException(paramList);
        }
      }
      return;
    }
    throw new ManifestRegisterException(null);
  }
  
  static boolean checkSinglePermissionPermanentDenied(Activity paramActivity, String paramString)
  {
    if (((paramString.equals("android.permission.ANSWER_PHONE_CALLS")) || (paramString.equals("android.permission.READ_PHONE_NUMBERS"))) && (!isOverOreo())) {
      return false;
    }
    return (isOverMarshmallow()) && (paramActivity.checkSelfPermission(paramString) == -1) && (!paramActivity.shouldShowRequestPermissionRationale(paramString));
  }
  
  static void checkTargetSdkVersion(Context paramContext, List<String> paramList)
  {
    if ((!paramList.contains("android.permission.REQUEST_INSTALL_PACKAGES")) && (!paramList.contains("android.permission.ANSWER_PHONE_CALLS")) && (!paramList.contains("android.permission.READ_PHONE_NUMBERS")))
    {
      if (paramContext.getApplicationInfo().targetSdkVersion < 23) {
        throw new RuntimeException("The targetSdkVersion SDK must be 23 or more");
      }
    }
    else {
      if (paramContext.getApplicationInfo().targetSdkVersion < 26) {
        break label74;
      }
    }
    return;
    label74:
    throw new RuntimeException("The targetSdkVersion SDK must be 26 or more");
  }
  
  static ArrayList<String> getFailPermissions(Context paramContext, List<String> paramList)
  {
    boolean bool = isOverMarshmallow();
    Object localObject = null;
    if (!bool) {
      return null;
    }
    Iterator localIterator = paramList.iterator();
    paramList = (List<String>)localObject;
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (str.equals("android.permission.REQUEST_INSTALL_PACKAGES"))
      {
        if (!isHasInstallPermission(paramContext))
        {
          localObject = paramList;
          if (paramList == null) {
            localObject = new ArrayList();
          }
          ((ArrayList)localObject).add(str);
          paramList = (List<String>)localObject;
        }
      }
      else if (str.equals("android.permission.SYSTEM_ALERT_WINDOW"))
      {
        if (!isHasOverlaysPermission(paramContext))
        {
          localObject = paramList;
          if (paramList == null) {
            localObject = new ArrayList();
          }
          ((ArrayList)localObject).add(str);
          paramList = (List<String>)localObject;
        }
      }
      else if (((!str.equals("android.permission.ANSWER_PHONE_CALLS")) && (!str.equals("android.permission.READ_PHONE_NUMBERS"))) || ((isOverOreo()) && (paramContext.checkSelfPermission(str) == -1)))
      {
        localObject = paramList;
        if (paramList == null) {
          localObject = new ArrayList();
        }
        ((ArrayList)localObject).add(str);
        paramList = (List<String>)localObject;
      }
    }
    return paramList;
  }
  
  static List<String> getFailPermissions(String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramArrayOfInt.length; i++) {
      if (paramArrayOfInt[i] == -1) {
        localArrayList.add(paramArrayOfString[i]);
      }
    }
    return localArrayList;
  }
  
  static List<String> getManifestPermissions(Context paramContext)
  {
    try
    {
      paramContext = Arrays.asList(paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 4096).requestedPermissions);
      return paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext) {}
    return null;
  }
  
  static List<String> getSucceedPermissions(String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramArrayOfInt.length; i++) {
      if (paramArrayOfInt[i] == 0) {
        localArrayList.add(paramArrayOfString[i]);
      }
    }
    return localArrayList;
  }
  
  static boolean isHasInstallPermission(Context paramContext)
  {
    if (isOverOreo()) {
      return paramContext.getPackageManager().canRequestPackageInstalls();
    }
    return true;
  }
  
  static boolean isHasOverlaysPermission(Context paramContext)
  {
    if (isOverMarshmallow()) {
      return Settings.canDrawOverlays(paramContext);
    }
    return true;
  }
  
  static boolean isOverMarshmallow()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 23) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  static boolean isOverOreo()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 26) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  static boolean isRequestDeniedPermission(Activity paramActivity, List<String> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      paramList = (String)localIterator.next();
      if ((!paramList.equals("android.permission.REQUEST_INSTALL_PACKAGES")) && (!paramList.equals("android.permission.SYSTEM_ALERT_WINDOW")) && (!checkSinglePermissionPermanentDenied(paramActivity, paramList))) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hjq/permissions/PermissionUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */