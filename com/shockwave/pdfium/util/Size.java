package com.shockwave.pdfium.util;

public class Size
{
  private final int height;
  private final int width;
  
  public Size(int paramInt1, int paramInt2)
  {
    this.width = paramInt1;
    this.height = paramInt2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    if (paramObject == null) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof Size))
    {
      paramObject = (Size)paramObject;
      boolean bool1 = bool2;
      if (this.width == ((Size)paramObject).width)
      {
        bool1 = bool2;
        if (this.height == ((Size)paramObject).height) {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  public int getHeight()
  {
    return this.height;
  }
  
  public int getWidth()
  {
    return this.width;
  }
  
  public int hashCode()
  {
    int i = this.height;
    int j = this.width;
    return i ^ (j >>> 16 | j << 16);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.width);
    localStringBuilder.append("x");
    localStringBuilder.append(this.height);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/shockwave/pdfium/util/Size.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */