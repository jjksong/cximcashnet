package com.shockwave.pdfium.util;

public class SizeF
{
  private final float height;
  private final float width;
  
  public SizeF(float paramFloat1, float paramFloat2)
  {
    this.width = paramFloat1;
    this.height = paramFloat2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    if (paramObject == null) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof SizeF))
    {
      paramObject = (SizeF)paramObject;
      boolean bool1 = bool2;
      if (this.width == ((SizeF)paramObject).width)
      {
        bool1 = bool2;
        if (this.height == ((SizeF)paramObject).height) {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  public float getHeight()
  {
    return this.height;
  }
  
  public float getWidth()
  {
    return this.width;
  }
  
  public int hashCode()
  {
    return Float.floatToIntBits(this.width) ^ Float.floatToIntBits(this.height);
  }
  
  public Size toSize()
  {
    return new Size((int)this.width, (int)this.height);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.width);
    localStringBuilder.append("x");
    localStringBuilder.append(this.height);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/shockwave/pdfium/util/SizeF.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */