package com.shockwave.pdfium;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import com.shockwave.pdfium.util.Size;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PdfiumCore
{
  private static final Class FD_CLASS = FileDescriptor.class;
  private static final String FD_FIELD_NAME = "descriptor";
  private static final String TAG = "com.shockwave.pdfium.PdfiumCore";
  private static final Object lock = new Object();
  private static Field mFdField = null;
  private int mCurrentDpi;
  
  static
  {
    try
    {
      System.loadLibrary("c++_shared");
      System.loadLibrary("modpng");
      System.loadLibrary("modft2");
      System.loadLibrary("modpdfium");
      System.loadLibrary("jniPdfium");
    }
    catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
    {
      String str = TAG;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Native libraries failed to load - ");
      localStringBuilder.append(localUnsatisfiedLinkError);
      Log.e(str, localStringBuilder.toString());
    }
  }
  
  public PdfiumCore(Context paramContext)
  {
    this.mCurrentDpi = paramContext.getResources().getDisplayMetrics().densityDpi;
    Log.d(TAG, "Starting PdfiumAndroid 1.9.0");
  }
  
  public static int getNumFd(ParcelFileDescriptor paramParcelFileDescriptor)
  {
    try
    {
      if (mFdField == null)
      {
        mFdField = FD_CLASS.getDeclaredField("descriptor");
        mFdField.setAccessible(true);
      }
      int i = mFdField.getInt(paramParcelFileDescriptor.getFileDescriptor());
      return i;
    }
    catch (IllegalAccessException paramParcelFileDescriptor)
    {
      paramParcelFileDescriptor.printStackTrace();
      return -1;
    }
    catch (NoSuchFieldException paramParcelFileDescriptor)
    {
      paramParcelFileDescriptor.printStackTrace();
    }
    return -1;
  }
  
  private native void nativeCloseDocument(long paramLong);
  
  private native void nativeClosePage(long paramLong);
  
  private native void nativeClosePages(long[] paramArrayOfLong);
  
  private native long nativeGetBookmarkDestIndex(long paramLong1, long paramLong2);
  
  private native String nativeGetBookmarkTitle(long paramLong);
  
  private native Integer nativeGetDestPageIndex(long paramLong1, long paramLong2);
  
  private native String nativeGetDocumentMetaText(long paramLong, String paramString);
  
  private native Long nativeGetFirstChildBookmark(long paramLong, Long paramLong1);
  
  private native RectF nativeGetLinkRect(long paramLong);
  
  private native String nativeGetLinkURI(long paramLong1, long paramLong2);
  
  private native int nativeGetPageCount(long paramLong);
  
  private native int nativeGetPageHeightPixel(long paramLong, int paramInt);
  
  private native int nativeGetPageHeightPoint(long paramLong);
  
  private native long[] nativeGetPageLinks(long paramLong);
  
  private native Size nativeGetPageSizeByIndex(long paramLong, int paramInt1, int paramInt2);
  
  private native int nativeGetPageWidthPixel(long paramLong, int paramInt);
  
  private native int nativeGetPageWidthPoint(long paramLong);
  
  private native Long nativeGetSiblingBookmark(long paramLong1, long paramLong2);
  
  private native long nativeLoadPage(long paramLong, int paramInt);
  
  private native long[] nativeLoadPages(long paramLong, int paramInt1, int paramInt2);
  
  private native long nativeOpenDocument(int paramInt, String paramString);
  
  private native long nativeOpenMemDocument(byte[] paramArrayOfByte, String paramString);
  
  private native Point nativePageCoordsToDevice(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, double paramDouble1, double paramDouble2);
  
  private native void nativeRenderPage(long paramLong, Surface paramSurface, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean);
  
  private native void nativeRenderPageBitmap(long paramLong, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean);
  
  private void recursiveGetBookmark(List<PdfDocument.Bookmark> paramList, PdfDocument paramPdfDocument, long paramLong)
  {
    PdfDocument.Bookmark localBookmark = new PdfDocument.Bookmark();
    localBookmark.mNativePtr = paramLong;
    localBookmark.title = nativeGetBookmarkTitle(paramLong);
    localBookmark.pageIdx = nativeGetBookmarkDestIndex(paramPdfDocument.mNativeDocPtr, paramLong);
    paramList.add(localBookmark);
    Long localLong = nativeGetFirstChildBookmark(paramPdfDocument.mNativeDocPtr, Long.valueOf(paramLong));
    if (localLong != null) {
      recursiveGetBookmark(localBookmark.getChildren(), paramPdfDocument, localLong.longValue());
    }
    localLong = nativeGetSiblingBookmark(paramPdfDocument.mNativeDocPtr, paramLong);
    if (localLong != null) {
      recursiveGetBookmark(paramList, paramPdfDocument, localLong.longValue());
    }
  }
  
  public void closeDocument(PdfDocument paramPdfDocument)
  {
    synchronized (lock)
    {
      Iterator localIterator = paramPdfDocument.mNativePagesPtr.keySet().iterator();
      while (localIterator.hasNext())
      {
        localObject2 = (Integer)localIterator.next();
        nativeClosePage(((Long)paramPdfDocument.mNativePagesPtr.get(localObject2)).longValue());
      }
      paramPdfDocument.mNativePagesPtr.clear();
      nativeCloseDocument(paramPdfDocument.mNativeDocPtr);
      Object localObject2 = paramPdfDocument.parcelFileDescriptor;
      if (localObject2 == null) {}
    }
    try
    {
      paramPdfDocument.parcelFileDescriptor.close();
      paramPdfDocument.parcelFileDescriptor = null;
      return;
      paramPdfDocument = finally;
      throw paramPdfDocument;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public PdfDocument.Meta getDocumentMeta(PdfDocument paramPdfDocument)
  {
    synchronized (lock)
    {
      PdfDocument.Meta localMeta = new com/shockwave/pdfium/PdfDocument$Meta;
      localMeta.<init>();
      localMeta.title = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Title");
      localMeta.author = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Author");
      localMeta.subject = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Subject");
      localMeta.keywords = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Keywords");
      localMeta.creator = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Creator");
      localMeta.producer = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "Producer");
      localMeta.creationDate = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "CreationDate");
      localMeta.modDate = nativeGetDocumentMetaText(paramPdfDocument.mNativeDocPtr, "ModDate");
      return localMeta;
    }
  }
  
  public int getPageCount(PdfDocument paramPdfDocument)
  {
    synchronized (lock)
    {
      int i = nativeGetPageCount(paramPdfDocument.mNativeDocPtr);
      return i;
    }
  }
  
  public int getPageHeight(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      paramPdfDocument = (Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt));
      if (paramPdfDocument != null)
      {
        paramInt = nativeGetPageHeightPixel(paramPdfDocument.longValue(), this.mCurrentDpi);
        return paramInt;
      }
      return 0;
    }
  }
  
  public int getPageHeightPoint(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      paramPdfDocument = (Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt));
      if (paramPdfDocument != null)
      {
        paramInt = nativeGetPageHeightPoint(paramPdfDocument.longValue());
        return paramInt;
      }
      return 0;
    }
  }
  
  public List<PdfDocument.Link> getPageLinks(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Object localObject2 = (Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt));
      if (localObject2 == null) {
        return localArrayList;
      }
      for (long l : nativeGetPageLinks(((Long)localObject2).longValue()))
      {
        localObject2 = nativeGetDestPageIndex(paramPdfDocument.mNativeDocPtr, l);
        String str = nativeGetLinkURI(paramPdfDocument.mNativeDocPtr, l);
        RectF localRectF = nativeGetLinkRect(l);
        if ((localRectF != null) && ((localObject2 != null) || (str != null)))
        {
          PdfDocument.Link localLink = new com/shockwave/pdfium/PdfDocument$Link;
          localLink.<init>(localRectF, (Integer)localObject2, str);
          localArrayList.add(localLink);
        }
      }
      return localArrayList;
    }
  }
  
  public Size getPageSize(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      paramPdfDocument = nativeGetPageSizeByIndex(paramPdfDocument.mNativeDocPtr, paramInt, this.mCurrentDpi);
      return paramPdfDocument;
    }
  }
  
  public int getPageWidth(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      paramPdfDocument = (Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt));
      if (paramPdfDocument != null)
      {
        paramInt = nativeGetPageWidthPixel(paramPdfDocument.longValue(), this.mCurrentDpi);
        return paramInt;
      }
      return 0;
    }
  }
  
  public int getPageWidthPoint(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      paramPdfDocument = (Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt));
      if (paramPdfDocument != null)
      {
        paramInt = nativeGetPageWidthPoint(paramPdfDocument.longValue());
        return paramInt;
      }
      return 0;
    }
  }
  
  public List<PdfDocument.Bookmark> getTableOfContents(PdfDocument paramPdfDocument)
  {
    synchronized (lock)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Long localLong = nativeGetFirstChildBookmark(paramPdfDocument.mNativeDocPtr, null);
      if (localLong != null) {
        recursiveGetBookmark(localArrayList, paramPdfDocument, localLong.longValue());
      }
      return localArrayList;
    }
  }
  
  public Point mapPageCoordsToDevice(PdfDocument paramPdfDocument, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, double paramDouble1, double paramDouble2)
  {
    return nativePageCoordsToDevice(((Long)paramPdfDocument.mNativePagesPtr.get(Integer.valueOf(paramInt1))).longValue(), paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramDouble1, paramDouble2);
  }
  
  public RectF mapRectToDevice(PdfDocument paramPdfDocument, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, RectF paramRectF)
  {
    Point localPoint = mapPageCoordsToDevice(paramPdfDocument, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramRectF.left, paramRectF.top);
    paramPdfDocument = mapPageCoordsToDevice(paramPdfDocument, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramRectF.right, paramRectF.bottom);
    return new RectF(localPoint.x, localPoint.y, paramPdfDocument.x, paramPdfDocument.y);
  }
  
  public PdfDocument newDocument(ParcelFileDescriptor paramParcelFileDescriptor)
    throws IOException
  {
    return newDocument(paramParcelFileDescriptor, null);
  }
  
  public PdfDocument newDocument(ParcelFileDescriptor paramParcelFileDescriptor, String paramString)
    throws IOException
  {
    PdfDocument localPdfDocument = new PdfDocument();
    localPdfDocument.parcelFileDescriptor = paramParcelFileDescriptor;
    synchronized (lock)
    {
      localPdfDocument.mNativeDocPtr = nativeOpenDocument(getNumFd(paramParcelFileDescriptor), paramString);
      return localPdfDocument;
    }
  }
  
  public PdfDocument newDocument(byte[] paramArrayOfByte)
    throws IOException
  {
    return newDocument(paramArrayOfByte, null);
  }
  
  public PdfDocument newDocument(byte[] paramArrayOfByte, String paramString)
    throws IOException
  {
    PdfDocument localPdfDocument = new PdfDocument();
    synchronized (lock)
    {
      localPdfDocument.mNativeDocPtr = nativeOpenMemDocument(paramArrayOfByte, paramString);
      return localPdfDocument;
    }
  }
  
  public long openPage(PdfDocument paramPdfDocument, int paramInt)
  {
    synchronized (lock)
    {
      long l = nativeLoadPage(paramPdfDocument.mNativeDocPtr, paramInt);
      paramPdfDocument.mNativePagesPtr.put(Integer.valueOf(paramInt), Long.valueOf(l));
      return l;
    }
  }
  
  public long[] openPage(PdfDocument paramPdfDocument, int paramInt1, int paramInt2)
  {
    synchronized (lock)
    {
      long[] arrayOfLong = nativeLoadPages(paramPdfDocument.mNativeDocPtr, paramInt1, paramInt2);
      int k = arrayOfLong.length;
      int j = 0;
      int i = paramInt1;
      for (paramInt1 = j; paramInt1 < k; paramInt1++)
      {
        long l = arrayOfLong[paramInt1];
        if (i > paramInt2) {
          break;
        }
        paramPdfDocument.mNativePagesPtr.put(Integer.valueOf(i), Long.valueOf(l));
        i++;
      }
      return arrayOfLong;
    }
  }
  
  public void renderPage(PdfDocument paramPdfDocument, Surface paramSurface, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    renderPage(paramPdfDocument, paramSurface, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, false);
  }
  
  /* Error */
  public void renderPage(PdfDocument paramPdfDocument, Surface paramSurface, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean)
  {
    // Byte code:
    //   0: getstatic 72	com/shockwave/pdfium/PdfiumCore:lock	Ljava/lang/Object;
    //   3: astore 11
    //   5: aload 11
    //   7: monitorenter
    //   8: aload_1
    //   9: getfield 238	com/shockwave/pdfium/PdfDocument:mNativePagesPtr	Ljava/util/Map;
    //   12: iload_3
    //   13: invokestatic 334	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   16: invokeinterface 266 2 0
    //   21: checkcast 210	java/lang/Long
    //   24: invokevirtual 224	java/lang/Long:longValue	()J
    //   27: lstore 9
    //   29: aload_0
    //   30: lload 9
    //   32: aload_2
    //   33: aload_0
    //   34: getfield 95	com/shockwave/pdfium/PdfiumCore:mCurrentDpi	I
    //   37: iload 4
    //   39: iload 5
    //   41: iload 6
    //   43: iload 7
    //   45: iload 8
    //   47: invokespecial 444	com/shockwave/pdfium/PdfiumCore:nativeRenderPage	(JLandroid/view/Surface;IIIIIZ)V
    //   50: goto +48 -> 98
    //   53: astore_1
    //   54: goto +12 -> 66
    //   57: astore_1
    //   58: goto +26 -> 84
    //   61: astore_1
    //   62: goto +41 -> 103
    //   65: astore_1
    //   66: getstatic 45	com/shockwave/pdfium/PdfiumCore:TAG	Ljava/lang/String;
    //   69: ldc_w 446
    //   72: invokestatic 69	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: aload_1
    //   77: invokevirtual 447	java/lang/Exception:printStackTrace	()V
    //   80: goto +18 -> 98
    //   83: astore_1
    //   84: getstatic 45	com/shockwave/pdfium/PdfiumCore:TAG	Ljava/lang/String;
    //   87: ldc_w 449
    //   90: invokestatic 69	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   93: pop
    //   94: aload_1
    //   95: invokevirtual 450	java/lang/NullPointerException:printStackTrace	()V
    //   98: aload 11
    //   100: monitorexit
    //   101: return
    //   102: astore_1
    //   103: aload 11
    //   105: monitorexit
    //   106: aload_1
    //   107: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	108	0	this	PdfiumCore
    //   0	108	1	paramPdfDocument	PdfDocument
    //   0	108	2	paramSurface	Surface
    //   0	108	3	paramInt1	int
    //   0	108	4	paramInt2	int
    //   0	108	5	paramInt3	int
    //   0	108	6	paramInt4	int
    //   0	108	7	paramInt5	int
    //   0	108	8	paramBoolean	boolean
    //   27	4	9	l	long
    // Exception table:
    //   from	to	target	type
    //   29	50	53	java/lang/Exception
    //   29	50	57	java/lang/NullPointerException
    //   8	29	61	finally
    //   8	29	65	java/lang/Exception
    //   8	29	83	java/lang/NullPointerException
    //   29	50	102	finally
    //   66	80	102	finally
    //   84	98	102	finally
    //   98	101	102	finally
    //   103	106	102	finally
  }
  
  public void renderPageBitmap(PdfDocument paramPdfDocument, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    renderPageBitmap(paramPdfDocument, paramBitmap, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, false);
  }
  
  /* Error */
  public void renderPageBitmap(PdfDocument paramPdfDocument, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean)
  {
    // Byte code:
    //   0: getstatic 72	com/shockwave/pdfium/PdfiumCore:lock	Ljava/lang/Object;
    //   3: astore 11
    //   5: aload 11
    //   7: monitorenter
    //   8: aload_1
    //   9: getfield 238	com/shockwave/pdfium/PdfDocument:mNativePagesPtr	Ljava/util/Map;
    //   12: iload_3
    //   13: invokestatic 334	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   16: invokeinterface 266 2 0
    //   21: checkcast 210	java/lang/Long
    //   24: invokevirtual 224	java/lang/Long:longValue	()J
    //   27: lstore 9
    //   29: aload_0
    //   30: lload 9
    //   32: aload_2
    //   33: aload_0
    //   34: getfield 95	com/shockwave/pdfium/PdfiumCore:mCurrentDpi	I
    //   37: iload 4
    //   39: iload 5
    //   41: iload 6
    //   43: iload 7
    //   45: iload 8
    //   47: invokespecial 457	com/shockwave/pdfium/PdfiumCore:nativeRenderPageBitmap	(JLandroid/graphics/Bitmap;IIIIIZ)V
    //   50: goto +48 -> 98
    //   53: astore_1
    //   54: goto +12 -> 66
    //   57: astore_1
    //   58: goto +26 -> 84
    //   61: astore_1
    //   62: goto +41 -> 103
    //   65: astore_1
    //   66: getstatic 45	com/shockwave/pdfium/PdfiumCore:TAG	Ljava/lang/String;
    //   69: ldc_w 446
    //   72: invokestatic 69	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: aload_1
    //   77: invokevirtual 447	java/lang/Exception:printStackTrace	()V
    //   80: goto +18 -> 98
    //   83: astore_1
    //   84: getstatic 45	com/shockwave/pdfium/PdfiumCore:TAG	Ljava/lang/String;
    //   87: ldc_w 449
    //   90: invokestatic 69	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   93: pop
    //   94: aload_1
    //   95: invokevirtual 450	java/lang/NullPointerException:printStackTrace	()V
    //   98: aload 11
    //   100: monitorexit
    //   101: return
    //   102: astore_1
    //   103: aload 11
    //   105: monitorexit
    //   106: aload_1
    //   107: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	108	0	this	PdfiumCore
    //   0	108	1	paramPdfDocument	PdfDocument
    //   0	108	2	paramBitmap	Bitmap
    //   0	108	3	paramInt1	int
    //   0	108	4	paramInt2	int
    //   0	108	5	paramInt3	int
    //   0	108	6	paramInt4	int
    //   0	108	7	paramInt5	int
    //   0	108	8	paramBoolean	boolean
    //   27	4	9	l	long
    // Exception table:
    //   from	to	target	type
    //   29	50	53	java/lang/Exception
    //   29	50	57	java/lang/NullPointerException
    //   8	29	61	finally
    //   8	29	65	java/lang/Exception
    //   8	29	83	java/lang/NullPointerException
    //   29	50	102	finally
    //   66	80	102	finally
    //   84	98	102	finally
    //   98	101	102	finally
    //   103	106	102	finally
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/shockwave/pdfium/PdfiumCore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */