package com.shockwave.pdfium;

import android.graphics.RectF;
import android.os.ParcelFileDescriptor;
import android.support.v4.util.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PdfDocument
{
  long mNativeDocPtr;
  final Map<Integer, Long> mNativePagesPtr = new ArrayMap();
  ParcelFileDescriptor parcelFileDescriptor;
  
  public boolean hasPage(int paramInt)
  {
    return this.mNativePagesPtr.containsKey(Integer.valueOf(paramInt));
  }
  
  public static class Bookmark
  {
    private List<Bookmark> children = new ArrayList();
    long mNativePtr;
    long pageIdx;
    String title;
    
    public List<Bookmark> getChildren()
    {
      return this.children;
    }
    
    public long getPageIdx()
    {
      return this.pageIdx;
    }
    
    public String getTitle()
    {
      return this.title;
    }
    
    public boolean hasChildren()
    {
      return this.children.isEmpty() ^ true;
    }
  }
  
  public static class Link
  {
    private RectF bounds;
    private Integer destPageIdx;
    private String uri;
    
    public Link(RectF paramRectF, Integer paramInteger, String paramString)
    {
      this.bounds = paramRectF;
      this.destPageIdx = paramInteger;
      this.uri = paramString;
    }
    
    public RectF getBounds()
    {
      return this.bounds;
    }
    
    public Integer getDestPageIdx()
    {
      return this.destPageIdx;
    }
    
    public String getUri()
    {
      return this.uri;
    }
  }
  
  public static class Meta
  {
    String author;
    String creationDate;
    String creator;
    String keywords;
    String modDate;
    String producer;
    String subject;
    String title;
    
    public String getAuthor()
    {
      return this.author;
    }
    
    public String getCreationDate()
    {
      return this.creationDate;
    }
    
    public String getCreator()
    {
      return this.creator;
    }
    
    public String getKeywords()
    {
      return this.keywords;
    }
    
    public String getModDate()
    {
      return this.modDate;
    }
    
    public String getProducer()
    {
      return this.producer;
    }
    
    public String getSubject()
    {
      return this.subject;
    }
    
    public String getTitle()
    {
      return this.title;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/shockwave/pdfium/PdfDocument.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */