package com.shockwave.pdfium;

import java.io.IOException;

public class PdfPasswordException
  extends IOException
{
  public PdfPasswordException() {}
  
  public PdfPasswordException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/shockwave/pdfium/PdfPasswordException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */