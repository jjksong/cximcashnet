package com.longsh.optionframelibrary;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.longsh.optionframelibrary.adapter.CenterDialogAdapter;
import java.util.ArrayList;

public class OptionCenterDialog
{
  private AlertDialog albumDialog;
  private ListView dialog_lv;
  
  public void dismiss()
  {
    this.albumDialog.dismiss();
  }
  
  public void setItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    this.dialog_lv.setOnItemClickListener(paramOnItemClickListener);
  }
  
  public void show(Context paramContext, ArrayList<String> paramArrayList)
  {
    this.albumDialog = new AlertDialog.Builder(paramContext).create();
    this.albumDialog.setCanceledOnTouchOutside(true);
    this.albumDialog.setCancelable(true);
    View localView = LayoutInflater.from(paramContext).inflate(R.layout.__picker_dialog_photo_pager, null);
    this.albumDialog.show();
    this.albumDialog.setContentView(localView);
    this.albumDialog.getWindow().setGravity(17);
    this.albumDialog.getWindow().setBackgroundDrawableResource(R.drawable.__picker_bg_dialog);
    this.dialog_lv = ((ListView)localView.findViewById(R.id.dialog_lv));
    paramContext = new CenterDialogAdapter(paramContext, paramArrayList);
    this.dialog_lv.setAdapter(paramContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/longsh/optionframelibrary/OptionCenterDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */