package com.longsh.optionframelibrary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.longsh.optionframelibrary.R.drawable;
import com.longsh.optionframelibrary.R.id;
import com.longsh.optionframelibrary.R.layout;
import java.util.ArrayList;

public class CenterDialogAdapter
  extends BaseAdapter
{
  private ArrayList<String> lstImageItem;
  private Context mContext;
  
  public CenterDialogAdapter(Context paramContext, ArrayList<String> paramArrayList)
  {
    this.lstImageItem = paramArrayList;
    this.mContext = paramContext;
  }
  
  public int getCount()
  {
    return this.lstImageItem.size();
  }
  
  public Object getItem(int paramInt)
  {
    return this.lstImageItem.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramViewGroup = new ViewHolder();
      paramView = LayoutInflater.from(this.mContext).inflate(R.layout.__picker_item_dialog, null);
      paramViewGroup.tv_dialog = ((TextView)paramView.findViewById(R.id.tv_dialog));
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (ViewHolder)paramView.getTag();
    }
    if (paramInt == 0) {
      paramViewGroup.tv_dialog.setBackgroundResource(R.drawable.__picker_item_dialog_selector_top);
    } else if (paramInt == getCount() - 1) {
      paramViewGroup.tv_dialog.setBackgroundResource(R.drawable.__picker_item_dialog_selector_bottom);
    } else {
      paramViewGroup.tv_dialog.setBackgroundResource(R.drawable.__picker_item_dialog_selector);
    }
    paramViewGroup.tv_dialog.setText((CharSequence)this.lstImageItem.get(paramInt));
    return paramView;
  }
  
  class ViewHolder
  {
    TextView tv_dialog;
    
    ViewHolder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/longsh/optionframelibrary/adapter/CenterDialogAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */