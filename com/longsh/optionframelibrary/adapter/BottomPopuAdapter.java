package com.longsh.optionframelibrary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.longsh.optionframelibrary.R.id;
import com.longsh.optionframelibrary.R.layout;
import java.util.List;

public class BottomPopuAdapter
  extends BaseAdapter
{
  private List<String> lstImageItem;
  private Context mContext;
  public int po = 0;
  
  public BottomPopuAdapter(Context paramContext, List<String> paramList)
  {
    this.lstImageItem = paramList;
    this.mContext = paramContext;
  }
  
  public int getCount()
  {
    return this.lstImageItem.size();
  }
  
  public Object getItem(int paramInt)
  {
    return this.lstImageItem.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = new ViewHolder();
      paramViewGroup = LayoutInflater.from(this.mContext).inflate(R.layout.item_bottom_popu, null);
      paramView.addtext = ((TextView)paramViewGroup.findViewById(R.id.addtext));
      paramView.addlayout = ((LinearLayout)paramViewGroup.findViewById(R.id.addlayout));
      paramViewGroup.setTag(paramView);
    }
    else
    {
      localObject = (ViewHolder)paramView.getTag();
      paramViewGroup = paramView;
      paramView = (View)localObject;
    }
    Object localObject = (String)this.lstImageItem.get(paramInt);
    paramView.addtext.setText((CharSequence)localObject);
    return paramViewGroup;
  }
  
  class ViewHolder
  {
    LinearLayout addlayout;
    TextView addtext;
    
    ViewHolder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/longsh/optionframelibrary/adapter/BottomPopuAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */