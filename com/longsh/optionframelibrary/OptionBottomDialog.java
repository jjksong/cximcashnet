package com.longsh.optionframelibrary;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.longsh.optionframelibrary.adapter.BottomPopuAdapter;
import java.util.List;

public class OptionBottomDialog
  extends PopupWindow
{
  private final ListView listView;
  
  public OptionBottomDialog(Context paramContext, List<String> paramList)
  {
    super(paramContext);
    final Activity localActivity = (Activity)paramContext;
    View localView = ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(R.layout.pupview, null);
    setContentView(localView);
    setHeight(-1);
    setWidth(-1);
    ((RelativeLayout)localView.findViewById(R.id.review)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        OptionBottomDialog.this.dismiss();
      }
    });
    ((TextView)localView.findViewById(R.id.quxiao)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        OptionBottomDialog.this.dismiss();
      }
    });
    this.listView = ((ListView)localView.findViewById(R.id.list));
    paramContext = new BottomPopuAdapter(paramContext, paramList);
    this.listView.setAdapter(paramContext);
    setFocusable(true);
    setOutsideTouchable(true);
    setBackgroundDrawable(new BitmapDrawable());
    setAnimationStyle(R.style.Animation);
    showAtLocation(localActivity.getWindow().getDecorView(), 81, 0, 0);
    backgroundAlpha(0.65F, localActivity);
    setOnDismissListener(new PopupWindow.OnDismissListener()
    {
      public void onDismiss()
      {
        new Handler().postDelayed(new Runnable()
        {
          public void run()
          {
            OptionBottomDialog.this.backgroundAlpha(1.0F, OptionBottomDialog.3.this.val$activity);
          }
        }, 290L);
      }
    });
  }
  
  public void backgroundAlpha(float paramFloat, Activity paramActivity)
  {
    WindowManager.LayoutParams localLayoutParams = paramActivity.getWindow().getAttributes();
    localLayoutParams.alpha = paramFloat;
    paramActivity.getWindow().setAttributes(localLayoutParams);
  }
  
  public void setItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    this.listView.setOnItemClickListener(paramOnItemClickListener);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/longsh/optionframelibrary/OptionBottomDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */