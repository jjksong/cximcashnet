package com.longsh.optionframelibrary;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class OptionMaterialDialog
{
  private static final int BUTTON_BOTTOM = 9;
  private static final int BUTTON_TOP = 9;
  private AlertDialog mAlertDialog;
  private Drawable mBackgroundDrawable;
  private int mBackgroundResId = -1;
  private Builder mBuilder;
  private boolean mCancel;
  private Context mContext;
  private boolean mHasShow = false;
  private LinearLayout.LayoutParams mLayoutParams;
  private CharSequence mMessage;
  private View mMessageContentView;
  private int mMessageContentViewResId;
  private int mMessageResId;
  private Button mNegativeButton;
  private DialogInterface.OnDismissListener mOnDismissListener;
  private Button mPositiveButton;
  private int mTextColor = 0;
  private float mTextSize = -1.0F;
  private CharSequence mTitle;
  private int mTitleResId;
  private View mView;
  private int nId = -1;
  View.OnClickListener nListener;
  private String nText;
  private int nTextColor = 0;
  private float nTextSize = -1.0F;
  private int pId = -1;
  View.OnClickListener pListener;
  private String pText;
  private int pTextColor = 0;
  private float pTextSize = -1.0F;
  private int tTextColor = 0;
  private float tTextSize = -1.0F;
  
  public OptionMaterialDialog(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  private int dip2px(float paramFloat)
  {
    return (int)(paramFloat * this.mContext.getResources().getDisplayMetrics().density + 0.5F);
  }
  
  private static boolean isLollipop()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 21) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean isNullOrEmpty(String paramString)
  {
    boolean bool;
    if ((paramString != null) && (!paramString.isEmpty())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private void setListViewHeightBasedOnChildren(ListView paramListView)
  {
    ListAdapter localListAdapter = paramListView.getAdapter();
    if (localListAdapter == null) {
      return;
    }
    int j = 0;
    int i = 0;
    while (j < localListAdapter.getCount())
    {
      localObject = localListAdapter.getView(j, null, paramListView);
      ((View)localObject).measure(0, 0);
      i += ((View)localObject).getMeasuredHeight();
      j++;
    }
    Object localObject = paramListView.getLayoutParams();
    ((ViewGroup.LayoutParams)localObject).height = (i + paramListView.getDividerHeight() * (localListAdapter.getCount() - 1));
    paramListView.setLayoutParams((ViewGroup.LayoutParams)localObject);
  }
  
  public void dismiss()
  {
    this.mAlertDialog.dismiss();
  }
  
  public Button getNegativeButton()
  {
    return this.mNegativeButton;
  }
  
  public Button getPositiveButton()
  {
    return this.mPositiveButton;
  }
  
  public OptionMaterialDialog setBackground(Drawable paramDrawable)
  {
    this.mBackgroundDrawable = paramDrawable;
    paramDrawable = this.mBuilder;
    if (paramDrawable != null) {
      paramDrawable.setBackground(this.mBackgroundDrawable);
    }
    return this;
  }
  
  public OptionMaterialDialog setBackgroundResource(int paramInt)
  {
    this.mBackgroundResId = paramInt;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setBackgroundResource(this.mBackgroundResId);
    }
    return this;
  }
  
  public OptionMaterialDialog setCanceledOnTouchOutside(boolean paramBoolean)
  {
    this.mCancel = paramBoolean;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setCanceledOnTouchOutside(this.mCancel);
    }
    return this;
  }
  
  public OptionMaterialDialog setContentView(int paramInt)
  {
    this.mMessageContentViewResId = paramInt;
    this.mMessageContentView = null;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setContentView(paramInt);
    }
    return this;
  }
  
  public OptionMaterialDialog setContentView(View paramView)
  {
    this.mMessageContentView = paramView;
    this.mMessageContentViewResId = 0;
    paramView = this.mBuilder;
    if (paramView != null) {
      paramView.setContentView(this.mMessageContentView);
    }
    return this;
  }
  
  public OptionMaterialDialog setMessage(int paramInt)
  {
    this.mMessageResId = paramInt;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setMessage(paramInt);
    }
    return this;
  }
  
  public OptionMaterialDialog setMessage(CharSequence paramCharSequence)
  {
    this.mMessage = paramCharSequence;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setMessage(paramCharSequence);
    }
    return this;
  }
  
  public OptionMaterialDialog setMessageTextColor(@ColorRes int paramInt)
  {
    this.mTextColor = paramInt;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setMessageColor(paramInt);
    }
    return this;
  }
  
  public OptionMaterialDialog setMessageTextSize(float paramFloat)
  {
    this.mTextSize = paramFloat;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setMessageSize(paramFloat);
    }
    return this;
  }
  
  public OptionMaterialDialog setNegativeButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    this.nId = paramInt;
    this.nListener = paramOnClickListener;
    return this;
  }
  
  public OptionMaterialDialog setNegativeButton(String paramString, View.OnClickListener paramOnClickListener)
  {
    this.nText = paramString;
    this.nListener = paramOnClickListener;
    return this;
  }
  
  public OptionMaterialDialog setNegativeButtonTextColor(@ColorRes int paramInt)
  {
    this.nTextColor = paramInt;
    if (this.mBuilder != null)
    {
      Button localButton = this.mNegativeButton;
      if (localButton != null) {
        localButton.setTextColor(ContextCompat.getColor(this.mContext, paramInt));
      }
    }
    return this;
  }
  
  public OptionMaterialDialog setNegativeButtonTextSize(float paramFloat)
  {
    this.nTextSize = paramFloat;
    if (this.mBuilder != null)
    {
      Button localButton = this.mNegativeButton;
      if (localButton != null) {
        localButton.setTextSize(paramFloat);
      }
    }
    return this;
  }
  
  public OptionMaterialDialog setOnDismissListener(DialogInterface.OnDismissListener paramOnDismissListener)
  {
    this.mOnDismissListener = paramOnDismissListener;
    return this;
  }
  
  public OptionMaterialDialog setPositiveButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    this.pId = paramInt;
    this.pListener = paramOnClickListener;
    return this;
  }
  
  public OptionMaterialDialog setPositiveButton(String paramString, View.OnClickListener paramOnClickListener)
  {
    this.pText = paramString;
    this.pListener = paramOnClickListener;
    return this;
  }
  
  public OptionMaterialDialog setPositiveButtonTextColor(@ColorRes int paramInt)
  {
    this.pTextColor = paramInt;
    if (this.mBuilder != null)
    {
      Button localButton = this.mPositiveButton;
      if (localButton != null) {
        localButton.setTextColor(ContextCompat.getColor(this.mContext, paramInt));
      }
    }
    return this;
  }
  
  public OptionMaterialDialog setPositiveButtonTextSize(float paramFloat)
  {
    this.pTextSize = paramFloat;
    if (this.mBuilder != null)
    {
      Button localButton = this.mPositiveButton;
      if (localButton != null) {
        localButton.setTextSize(paramFloat);
      }
    }
    return this;
  }
  
  public OptionMaterialDialog setTitle(int paramInt)
  {
    this.mTitleResId = paramInt;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setTitle(paramInt);
    }
    return this;
  }
  
  public OptionMaterialDialog setTitle(CharSequence paramCharSequence)
  {
    this.mTitle = paramCharSequence;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setTitle(paramCharSequence);
    }
    return this;
  }
  
  public OptionMaterialDialog setTitleTextColor(@ColorRes int paramInt)
  {
    this.tTextColor = paramInt;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setTitleColor(paramInt);
    }
    return this;
  }
  
  public OptionMaterialDialog setTitleTextSize(float paramFloat)
  {
    this.tTextSize = paramFloat;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setTitleSize(paramFloat);
    }
    return this;
  }
  
  public OptionMaterialDialog setView(View paramView)
  {
    this.mView = paramView;
    Builder localBuilder = this.mBuilder;
    if (localBuilder != null) {
      localBuilder.setView(paramView);
    }
    return this;
  }
  
  public void show()
  {
    if (!this.mHasShow) {
      this.mBuilder = new Builder(null);
    } else {
      this.mAlertDialog.show();
    }
    this.mHasShow = true;
  }
  
  private class Builder
  {
    private Window mAlertDialogWindow;
    private LinearLayout mButtonLayout;
    private ViewGroup mMessageContentRoot;
    private TextView mMessageView;
    private TextView mTitleView;
    
    private Builder()
    {
      OptionMaterialDialog.access$102(OptionMaterialDialog.this, new AlertDialog.Builder(OptionMaterialDialog.this.mContext).create());
      OptionMaterialDialog.this.mAlertDialog.show();
      OptionMaterialDialog.this.mAlertDialog.getWindow().clearFlags(131080);
      OptionMaterialDialog.this.mAlertDialog.getWindow().setSoftInputMode(15);
      this.mAlertDialogWindow = OptionMaterialDialog.this.mAlertDialog.getWindow();
      this.mAlertDialogWindow.setBackgroundDrawable(new ColorDrawable(0));
      Object localObject = LayoutInflater.from(OptionMaterialDialog.this.mContext).inflate(R.layout.layout_material_dialog, null);
      ((View)localObject).setFocusable(true);
      ((View)localObject).setFocusableInTouchMode(true);
      this.mAlertDialogWindow.setBackgroundDrawableResource(R.drawable.material_dialog_window);
      this.mAlertDialogWindow.setContentView((View)localObject);
      this.mTitleView = ((TextView)this.mAlertDialogWindow.findViewById(R.id.title));
      this.mMessageView = ((TextView)this.mAlertDialogWindow.findViewById(R.id.message));
      this.mButtonLayout = ((LinearLayout)this.mAlertDialogWindow.findViewById(R.id.buttonLayout));
      OptionMaterialDialog.access$302(OptionMaterialDialog.this, (Button)this.mButtonLayout.findViewById(R.id.btn_p));
      OptionMaterialDialog.access$402(OptionMaterialDialog.this, (Button)this.mButtonLayout.findViewById(R.id.btn_n));
      this.mMessageContentRoot = ((ViewGroup)this.mAlertDialogWindow.findViewById(R.id.message_content_root));
      if (OptionMaterialDialog.this.mView != null)
      {
        localObject = (LinearLayout)this.mAlertDialogWindow.findViewById(R.id.contentView);
        ((LinearLayout)localObject).removeAllViews();
        ((LinearLayout)localObject).addView(OptionMaterialDialog.this.mView);
      }
      if (OptionMaterialDialog.this.mTitleResId != 0) {
        setTitle(OptionMaterialDialog.this.mTitleResId);
      }
      if (OptionMaterialDialog.this.mTitle != null) {
        setTitle(OptionMaterialDialog.this.mTitle);
      }
      if (OptionMaterialDialog.this.tTextSize != -1.0F) {
        setTitleSize(OptionMaterialDialog.this.tTextSize);
      }
      if (OptionMaterialDialog.this.tTextColor != 0) {
        setTitleColor(OptionMaterialDialog.this.tTextColor);
      }
      if ((OptionMaterialDialog.this.mTitle == null) && (OptionMaterialDialog.this.mTitleResId == 0)) {
        this.mTitleView.setVisibility(8);
      }
      if (OptionMaterialDialog.this.mMessageResId != 0) {
        setMessage(OptionMaterialDialog.this.mMessageResId);
      }
      if (OptionMaterialDialog.this.mTextColor != 0) {
        setMessageColor(OptionMaterialDialog.this.mTextColor);
      }
      if (OptionMaterialDialog.this.mMessage != null) {
        setMessage(OptionMaterialDialog.this.mMessage);
      }
      if (OptionMaterialDialog.this.mTextSize != -1.0F) {
        setMessageSize(OptionMaterialDialog.this.mTextSize);
      }
      if (OptionMaterialDialog.this.pTextColor != 0) {
        OptionMaterialDialog.this.mPositiveButton.setTextColor(ContextCompat.getColor(OptionMaterialDialog.this.mContext, OptionMaterialDialog.this.pTextColor));
      }
      if (OptionMaterialDialog.this.nTextColor != 0) {
        OptionMaterialDialog.this.mNegativeButton.setTextColor(ContextCompat.getColor(OptionMaterialDialog.this.mContext, OptionMaterialDialog.this.nTextColor));
      }
      if (OptionMaterialDialog.this.pTextSize != 0.0F) {
        OptionMaterialDialog.this.mPositiveButton.setTextSize(OptionMaterialDialog.this.pTextSize);
      }
      if (OptionMaterialDialog.this.nTextSize != 0.0F) {
        OptionMaterialDialog.this.mNegativeButton.setTextSize(OptionMaterialDialog.this.nTextSize);
      }
      if (OptionMaterialDialog.this.pId != -1)
      {
        OptionMaterialDialog.this.mPositiveButton.setVisibility(0);
        OptionMaterialDialog.this.mPositiveButton.setText(OptionMaterialDialog.this.pId);
        OptionMaterialDialog.this.mPositiveButton.setOnClickListener(OptionMaterialDialog.this.pListener);
        if (OptionMaterialDialog.access$1900()) {
          OptionMaterialDialog.this.mPositiveButton.setElevation(0.0F);
        }
      }
      if (OptionMaterialDialog.this.nId != -1)
      {
        OptionMaterialDialog.this.mNegativeButton.setVisibility(0);
        OptionMaterialDialog.this.mNegativeButton.setText(OptionMaterialDialog.this.nId);
        OptionMaterialDialog.this.mNegativeButton.setOnClickListener(OptionMaterialDialog.this.nListener);
        if (OptionMaterialDialog.access$1900()) {
          OptionMaterialDialog.this.mNegativeButton.setElevation(0.0F);
        }
      }
      if (!OptionMaterialDialog.this.isNullOrEmpty(OptionMaterialDialog.this.pText))
      {
        OptionMaterialDialog.this.mPositiveButton.setVisibility(0);
        OptionMaterialDialog.this.mPositiveButton.setText(OptionMaterialDialog.this.pText);
        OptionMaterialDialog.this.mPositiveButton.setOnClickListener(OptionMaterialDialog.this.pListener);
        if (OptionMaterialDialog.access$1900()) {
          OptionMaterialDialog.this.mPositiveButton.setElevation(0.0F);
        }
      }
      if (!OptionMaterialDialog.this.isNullOrEmpty(OptionMaterialDialog.this.nText))
      {
        OptionMaterialDialog.this.mNegativeButton.setVisibility(0);
        OptionMaterialDialog.this.mNegativeButton.setText(OptionMaterialDialog.this.nText);
        OptionMaterialDialog.this.mNegativeButton.setOnClickListener(OptionMaterialDialog.this.nListener);
        if (OptionMaterialDialog.access$1900()) {
          OptionMaterialDialog.this.mNegativeButton.setElevation(0.0F);
        }
      }
      if ((OptionMaterialDialog.this.isNullOrEmpty(OptionMaterialDialog.this.pText)) && (OptionMaterialDialog.this.pId == -1)) {
        OptionMaterialDialog.this.mPositiveButton.setVisibility(8);
      }
      if ((OptionMaterialDialog.this.isNullOrEmpty(OptionMaterialDialog.this.nText)) && (OptionMaterialDialog.this.nId == -1)) {
        OptionMaterialDialog.this.mNegativeButton.setVisibility(8);
      }
      if (OptionMaterialDialog.this.mBackgroundResId != -1) {
        ((LinearLayout)this.mAlertDialogWindow.findViewById(R.id.material_background)).setBackgroundResource(OptionMaterialDialog.this.mBackgroundResId);
      }
      if (OptionMaterialDialog.this.mBackgroundDrawable != null) {
        ((LinearLayout)this.mAlertDialogWindow.findViewById(R.id.material_background)).setBackground(OptionMaterialDialog.this.mBackgroundDrawable);
      }
      if (OptionMaterialDialog.this.mMessageContentView != null) {
        setContentView(OptionMaterialDialog.this.mMessageContentView);
      } else if (OptionMaterialDialog.this.mMessageContentViewResId != 0) {
        setContentView(OptionMaterialDialog.this.mMessageContentViewResId);
      }
      OptionMaterialDialog.this.mAlertDialog.setCanceledOnTouchOutside(OptionMaterialDialog.this.mCancel);
      OptionMaterialDialog.this.mAlertDialog.setCancelable(OptionMaterialDialog.this.mCancel);
      if (OptionMaterialDialog.this.mOnDismissListener != null) {
        OptionMaterialDialog.this.mAlertDialog.setOnDismissListener(OptionMaterialDialog.this.mOnDismissListener);
      }
    }
    
    public int dp2px(float paramFloat)
    {
      return (int)(paramFloat * OptionMaterialDialog.this.mContext.getResources().getDisplayMetrics().density + 0.5F);
    }
    
    public void setBackground(Drawable paramDrawable)
    {
      ((LinearLayout)this.mAlertDialogWindow.findViewById(R.id.material_background)).setBackground(paramDrawable);
    }
    
    public void setBackgroundResource(int paramInt)
    {
      ((LinearLayout)this.mAlertDialogWindow.findViewById(R.id.material_background)).setBackgroundResource(paramInt);
    }
    
    public void setCanceledOnTouchOutside(boolean paramBoolean)
    {
      OptionMaterialDialog.this.mAlertDialog.setCanceledOnTouchOutside(paramBoolean);
      OptionMaterialDialog.this.mAlertDialog.setCancelable(paramBoolean);
    }
    
    public void setContentView(int paramInt)
    {
      this.mMessageContentRoot.removeAllViews();
      LayoutInflater.from(this.mMessageContentRoot.getContext()).inflate(paramInt, this.mMessageContentRoot);
    }
    
    public void setContentView(View paramView)
    {
      paramView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
      if ((paramView instanceof ListView)) {
        OptionMaterialDialog.this.setListViewHeightBasedOnChildren((ListView)paramView);
      }
      LinearLayout localLinearLayout = (LinearLayout)this.mAlertDialogWindow.findViewById(R.id.message_content_view);
      if (localLinearLayout != null)
      {
        localLinearLayout.removeAllViews();
        localLinearLayout.addView(paramView);
      }
      for (int i = 0;; i++)
      {
        int j;
        if (localLinearLayout != null) {
          j = localLinearLayout.getChildCount();
        } else {
          j = 0;
        }
        if (i >= j) {
          break;
        }
        if ((localLinearLayout.getChildAt(i) instanceof AutoCompleteTextView))
        {
          paramView = (AutoCompleteTextView)localLinearLayout.getChildAt(i);
          paramView.setFocusable(true);
          paramView.requestFocus();
          paramView.setFocusableInTouchMode(true);
        }
      }
    }
    
    public void setMessage(int paramInt)
    {
      TextView localTextView = this.mMessageView;
      if (localTextView != null) {
        localTextView.setText(paramInt);
      }
    }
    
    public void setMessage(CharSequence paramCharSequence)
    {
      TextView localTextView = this.mMessageView;
      if (localTextView != null) {
        localTextView.setText(paramCharSequence);
      }
    }
    
    public void setMessageColor(int paramInt)
    {
      TextView localTextView = this.mMessageView;
      if (localTextView != null) {
        localTextView.setTextColor(ContextCompat.getColor(OptionMaterialDialog.this.mContext, paramInt));
      }
    }
    
    public void setMessageSize(float paramFloat)
    {
      TextView localTextView = this.mMessageView;
      if (localTextView != null) {
        localTextView.setTextSize(paramFloat);
      }
    }
    
    public void setNegativeButton(String paramString, View.OnClickListener paramOnClickListener)
    {
      Button localButton = new Button(OptionMaterialDialog.this.mContext);
      LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
      localButton.setLayoutParams(localLayoutParams);
      localButton.setBackgroundResource(R.drawable.material_card);
      localButton.setText(paramString);
      localButton.setTextColor(Color.argb(222, 0, 0, 0));
      localButton.setTextSize(14.0F);
      localButton.setGravity(17);
      localButton.setPadding(0, 0, 0, OptionMaterialDialog.this.dip2px(8.0F));
      localButton.setOnClickListener(paramOnClickListener);
      if (this.mButtonLayout.getChildCount() > 0)
      {
        localLayoutParams.setMargins(20, 0, 10, OptionMaterialDialog.this.dip2px(9.0F));
        localButton.setLayoutParams(localLayoutParams);
        this.mButtonLayout.addView(localButton, 1);
      }
      else
      {
        localButton.setLayoutParams(localLayoutParams);
        this.mButtonLayout.addView(localButton);
      }
    }
    
    public void setPositiveButton(String paramString, View.OnClickListener paramOnClickListener)
    {
      Button localButton = new Button(OptionMaterialDialog.this.mContext);
      localButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
      localButton.setBackgroundResource(R.drawable.material_card);
      localButton.setTextColor(Color.argb(255, 35, 159, 242));
      localButton.setText(paramString);
      localButton.setGravity(17);
      localButton.setTextSize(14.0F);
      localButton.setPadding(OptionMaterialDialog.this.dip2px(12.0F), 0, OptionMaterialDialog.this.dip2px(32.0F), OptionMaterialDialog.this.dip2px(9.0F));
      localButton.setOnClickListener(paramOnClickListener);
      this.mButtonLayout.addView(localButton);
    }
    
    public void setTitle(int paramInt)
    {
      this.mTitleView.setText(paramInt);
    }
    
    public void setTitle(CharSequence paramCharSequence)
    {
      this.mTitleView.setText(paramCharSequence);
    }
    
    public void setTitleColor(int paramInt)
    {
      TextView localTextView = this.mTitleView;
      if (localTextView != null) {
        localTextView.setTextColor(ContextCompat.getColor(OptionMaterialDialog.this.mContext, paramInt));
      }
    }
    
    public void setTitleSize(float paramFloat)
    {
      TextView localTextView = this.mTitleView;
      if (localTextView != null) {
        localTextView.setTextSize(paramFloat);
      }
    }
    
    public void setView(View paramView)
    {
      Object localObject = (LinearLayout)this.mAlertDialogWindow.findViewById(R.id.contentView);
      ((LinearLayout)localObject).removeAllViews();
      paramView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
      paramView.setOnFocusChangeListener(new View.OnFocusChangeListener()
      {
        public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
        {
          OptionMaterialDialog.Builder.this.mAlertDialogWindow.setSoftInputMode(5);
          ((InputMethodManager)OptionMaterialDialog.this.mContext.getSystemService("input_method")).toggleSoftInput(2, 1);
        }
      });
      ((LinearLayout)localObject).addView(paramView);
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        int k = 0;
        int i;
        for (int j = 0;; j++)
        {
          i = k;
          if (j >= paramView.getChildCount()) {
            break;
          }
          if ((paramView.getChildAt(j) instanceof EditText))
          {
            localObject = (EditText)paramView.getChildAt(j);
            ((EditText)localObject).setFocusable(true);
            ((EditText)localObject).requestFocus();
            ((EditText)localObject).setFocusableInTouchMode(true);
          }
        }
        while (i < paramView.getChildCount())
        {
          if ((paramView.getChildAt(i) instanceof AutoCompleteTextView))
          {
            localObject = (AutoCompleteTextView)paramView.getChildAt(i);
            ((AutoCompleteTextView)localObject).setFocusable(true);
            ((AutoCompleteTextView)localObject).requestFocus();
            ((AutoCompleteTextView)localObject).setFocusableInTouchMode(true);
          }
          i++;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/longsh/optionframelibrary/OptionMaterialDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */