package com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.WindowManager;

public class RotationListener
{
  private RotationCallback callback;
  private int lastRotation;
  private OrientationEventListener orientationEventListener;
  private WindowManager windowManager;
  
  public void listen(Context paramContext, RotationCallback paramRotationCallback)
  {
    stop();
    paramContext = paramContext.getApplicationContext();
    this.callback = paramRotationCallback;
    this.windowManager = ((WindowManager)paramContext.getSystemService("window"));
    this.orientationEventListener = new OrientationEventListener(paramContext, 3)
    {
      public void onOrientationChanged(int paramAnonymousInt)
      {
        WindowManager localWindowManager = RotationListener.this.windowManager;
        RotationCallback localRotationCallback = RotationListener.this.callback;
        if ((RotationListener.this.windowManager != null) && (localRotationCallback != null))
        {
          paramAnonymousInt = localWindowManager.getDefaultDisplay().getRotation();
          if (paramAnonymousInt != RotationListener.this.lastRotation)
          {
            RotationListener.access$202(RotationListener.this, paramAnonymousInt);
            localRotationCallback.onRotationChanged(paramAnonymousInt);
          }
        }
      }
    };
    this.orientationEventListener.enable();
    this.lastRotation = this.windowManager.getDefaultDisplay().getRotation();
  }
  
  public void stop()
  {
    OrientationEventListener localOrientationEventListener = this.orientationEventListener;
    if (localOrientationEventListener != null) {
      localOrientationEventListener.disable();
    }
    this.orientationEventListener = null;
    this.windowManager = null;
    this.callback = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/RotationListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */