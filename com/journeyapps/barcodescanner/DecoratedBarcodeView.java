package com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.DecodeFormatManager;
import com.google.zxing.client.android.DecodeHintManager;
import com.google.zxing.client.android.R.id;
import com.google.zxing.client.android.R.layout;
import com.google.zxing.client.android.R.styleable;
import com.journeyapps.barcodescanner.camera.CameraParametersCallback;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DecoratedBarcodeView
  extends FrameLayout
{
  private BarcodeView barcodeView;
  private TextView statusView;
  private TorchListener torchListener;
  private ViewfinderView viewFinder;
  
  public DecoratedBarcodeView(Context paramContext)
  {
    super(paramContext);
    initialize();
  }
  
  public DecoratedBarcodeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initialize(paramAttributeSet);
  }
  
  public DecoratedBarcodeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initialize(paramAttributeSet);
  }
  
  private void initialize()
  {
    initialize(null);
  }
  
  private void initialize(AttributeSet paramAttributeSet)
  {
    Object localObject = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.zxing_view);
    int i = ((TypedArray)localObject).getResourceId(R.styleable.zxing_view_zxing_scanner_layout, R.layout.zxing_barcode_scanner);
    ((TypedArray)localObject).recycle();
    inflate(getContext(), i, this);
    this.barcodeView = ((BarcodeView)findViewById(R.id.zxing_barcode_surface));
    localObject = this.barcodeView;
    if (localObject != null)
    {
      ((BarcodeView)localObject).initializeAttributes(paramAttributeSet);
      this.viewFinder = ((ViewfinderView)findViewById(R.id.zxing_viewfinder_view));
      paramAttributeSet = this.viewFinder;
      if (paramAttributeSet != null)
      {
        paramAttributeSet.setCameraPreview(this.barcodeView);
        this.statusView = ((TextView)findViewById(R.id.zxing_status_view));
        return;
      }
      throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.ViewfinderView on provided layout with the id \"zxing_viewfinder_view\".");
    }
    throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.BarcodeView on provided layout with the id \"zxing_barcode_surface\".");
  }
  
  public void changeCameraParameters(CameraParametersCallback paramCameraParametersCallback)
  {
    this.barcodeView.changeCameraParameters(paramCameraParametersCallback);
  }
  
  public void decodeContinuous(BarcodeCallback paramBarcodeCallback)
  {
    this.barcodeView.decodeContinuous(new WrappedCallback(paramBarcodeCallback));
  }
  
  public void decodeSingle(BarcodeCallback paramBarcodeCallback)
  {
    this.barcodeView.decodeSingle(new WrappedCallback(paramBarcodeCallback));
  }
  
  public BarcodeView getBarcodeView()
  {
    return (BarcodeView)findViewById(R.id.zxing_barcode_surface);
  }
  
  public TextView getStatusView()
  {
    return this.statusView;
  }
  
  public ViewfinderView getViewFinder()
  {
    return this.viewFinder;
  }
  
  public void initializeFromIntent(Intent paramIntent)
  {
    Set localSet = DecodeFormatManager.parseDecodeFormats(paramIntent);
    Map localMap = DecodeHintManager.parseDecodeHints(paramIntent);
    CameraSettings localCameraSettings = new CameraSettings();
    if (paramIntent.hasExtra("SCAN_CAMERA_ID"))
    {
      i = paramIntent.getIntExtra("SCAN_CAMERA_ID", -1);
      if (i >= 0) {
        localCameraSettings.setRequestedCameraId(i);
      }
    }
    String str = paramIntent.getStringExtra("PROMPT_MESSAGE");
    if (str != null) {
      setStatusText(str);
    }
    int i = paramIntent.getIntExtra("SCAN_TYPE", 0);
    paramIntent = paramIntent.getStringExtra("CHARACTER_SET");
    new MultiFormatReader().setHints(localMap);
    this.barcodeView.setCameraSettings(localCameraSettings);
    this.barcodeView.setDecoderFactory(new DefaultDecoderFactory(localSet, localMap, paramIntent, i));
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt != 27) && (paramInt != 80))
    {
      switch (paramInt)
      {
      default: 
        return super.onKeyDown(paramInt, paramKeyEvent);
      case 25: 
        setTorchOff();
        return true;
      }
      setTorchOn();
      return true;
    }
    return true;
  }
  
  public void pause()
  {
    this.barcodeView.pause();
  }
  
  public void pauseAndWait()
  {
    this.barcodeView.pauseAndWait();
  }
  
  public void resume()
  {
    this.barcodeView.resume();
  }
  
  public void setStatusText(String paramString)
  {
    TextView localTextView = this.statusView;
    if (localTextView != null) {
      localTextView.setText(paramString);
    }
  }
  
  public void setTorchListener(TorchListener paramTorchListener)
  {
    this.torchListener = paramTorchListener;
  }
  
  public void setTorchOff()
  {
    this.barcodeView.setTorch(false);
    TorchListener localTorchListener = this.torchListener;
    if (localTorchListener != null) {
      localTorchListener.onTorchOff();
    }
  }
  
  public void setTorchOn()
  {
    this.barcodeView.setTorch(true);
    TorchListener localTorchListener = this.torchListener;
    if (localTorchListener != null) {
      localTorchListener.onTorchOn();
    }
  }
  
  public static abstract interface TorchListener
  {
    public abstract void onTorchOff();
    
    public abstract void onTorchOn();
  }
  
  private class WrappedCallback
    implements BarcodeCallback
  {
    private BarcodeCallback delegate;
    
    public WrappedCallback(BarcodeCallback paramBarcodeCallback)
    {
      this.delegate = paramBarcodeCallback;
    }
    
    public void barcodeResult(BarcodeResult paramBarcodeResult)
    {
      this.delegate.barcodeResult(paramBarcodeResult);
    }
    
    public void possibleResultPoints(List<ResultPoint> paramList)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        ResultPoint localResultPoint = (ResultPoint)localIterator.next();
        DecoratedBarcodeView.this.viewFinder.addPossibleResultPoint(localResultPoint);
      }
      this.delegate.possibleResultPoints(paramList);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/DecoratedBarcodeView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */