package com.journeyapps.barcodescanner.camera;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import com.google.zxing.client.android.R.id;
import com.journeyapps.barcodescanner.Size;
import com.journeyapps.barcodescanner.Util;

public class CameraInstance
{
  private static final String TAG = "CameraInstance";
  private boolean cameraClosed = true;
  private CameraManager cameraManager;
  private CameraSettings cameraSettings = new CameraSettings();
  private CameraThread cameraThread;
  private Runnable closer = new Runnable()
  {
    public void run()
    {
      try
      {
        Log.d(CameraInstance.TAG, "Closing camera");
        CameraInstance.this.cameraManager.stopPreview();
        CameraInstance.this.cameraManager.close();
      }
      catch (Exception localException)
      {
        Log.e(CameraInstance.TAG, "Failed to close camera", localException);
      }
      CameraInstance.access$802(CameraInstance.this, true);
      CameraInstance.this.readyHandler.sendEmptyMessage(R.id.zxing_camera_closed);
      CameraInstance.this.cameraThread.decrementInstances();
    }
  };
  private Runnable configure = new Runnable()
  {
    public void run()
    {
      try
      {
        Log.d(CameraInstance.TAG, "Configuring camera");
        CameraInstance.this.cameraManager.configure();
        if (CameraInstance.this.readyHandler != null) {
          CameraInstance.this.readyHandler.obtainMessage(R.id.zxing_prewiew_size_ready, CameraInstance.this.getPreviewSize()).sendToTarget();
        }
      }
      catch (Exception localException)
      {
        CameraInstance.this.notifyError(localException);
        Log.e(CameraInstance.TAG, "Failed to configure camera", localException);
      }
    }
  };
  private DisplayConfiguration displayConfiguration;
  private Handler mainHandler;
  private boolean open = false;
  private Runnable opener = new Runnable()
  {
    public void run()
    {
      try
      {
        Log.d(CameraInstance.TAG, "Opening camera");
        CameraInstance.this.cameraManager.open();
      }
      catch (Exception localException)
      {
        CameraInstance.this.notifyError(localException);
        Log.e(CameraInstance.TAG, "Failed to open camera", localException);
      }
    }
  };
  private Runnable previewStarter = new Runnable()
  {
    public void run()
    {
      try
      {
        Log.d(CameraInstance.TAG, "Starting preview");
        CameraInstance.this.cameraManager.setPreviewDisplay(CameraInstance.this.surface);
        CameraInstance.this.cameraManager.startPreview();
      }
      catch (Exception localException)
      {
        CameraInstance.this.notifyError(localException);
        Log.e(CameraInstance.TAG, "Failed to start preview", localException);
      }
    }
  };
  private Handler readyHandler;
  private CameraSurface surface;
  
  public CameraInstance(Context paramContext)
  {
    Util.validateMainThread();
    this.cameraThread = CameraThread.getInstance();
    this.cameraManager = new CameraManager(paramContext);
    this.cameraManager.setCameraSettings(this.cameraSettings);
    this.mainHandler = new Handler();
  }
  
  public CameraInstance(CameraManager paramCameraManager)
  {
    Util.validateMainThread();
    this.cameraManager = paramCameraManager;
  }
  
  private Size getPreviewSize()
  {
    return this.cameraManager.getPreviewSize();
  }
  
  private void notifyError(Exception paramException)
  {
    Handler localHandler = this.readyHandler;
    if (localHandler != null) {
      localHandler.obtainMessage(R.id.zxing_camera_error, paramException).sendToTarget();
    }
  }
  
  private void validateOpen()
  {
    if (this.open) {
      return;
    }
    throw new IllegalStateException("CameraInstance is not open");
  }
  
  public void changeCameraParameters(final CameraParametersCallback paramCameraParametersCallback)
  {
    
    if (this.open) {
      this.cameraThread.enqueue(new Runnable()
      {
        public void run()
        {
          CameraInstance.this.cameraManager.changeCameraParameters(paramCameraParametersCallback);
        }
      });
    }
  }
  
  public void close()
  {
    
    if (this.open) {
      this.cameraThread.enqueue(this.closer);
    } else {
      this.cameraClosed = true;
    }
    this.open = false;
  }
  
  public void configureCamera()
  {
    Util.validateMainThread();
    validateOpen();
    this.cameraThread.enqueue(this.configure);
  }
  
  protected CameraManager getCameraManager()
  {
    return this.cameraManager;
  }
  
  public int getCameraRotation()
  {
    return this.cameraManager.getCameraRotation();
  }
  
  public CameraSettings getCameraSettings()
  {
    return this.cameraSettings;
  }
  
  protected CameraThread getCameraThread()
  {
    return this.cameraThread;
  }
  
  public DisplayConfiguration getDisplayConfiguration()
  {
    return this.displayConfiguration;
  }
  
  protected CameraSurface getSurface()
  {
    return this.surface;
  }
  
  public boolean isCameraClosed()
  {
    return this.cameraClosed;
  }
  
  public boolean isOpen()
  {
    return this.open;
  }
  
  public void open()
  {
    Util.validateMainThread();
    this.open = true;
    this.cameraClosed = false;
    this.cameraThread.incrementAndEnqueue(this.opener);
  }
  
  public void requestPreview(final PreviewCallback paramPreviewCallback)
  {
    this.mainHandler.post(new Runnable()
    {
      public void run()
      {
        if (!CameraInstance.this.open)
        {
          Log.d(CameraInstance.TAG, "Camera is closed, not requesting preview");
          return;
        }
        CameraInstance.this.cameraThread.enqueue(new Runnable()
        {
          public void run()
          {
            CameraInstance.this.cameraManager.requestPreviewFrame(CameraInstance.3.this.val$callback);
          }
        });
      }
    });
  }
  
  public void setCameraSettings(CameraSettings paramCameraSettings)
  {
    if (!this.open)
    {
      this.cameraSettings = paramCameraSettings;
      this.cameraManager.setCameraSettings(paramCameraSettings);
    }
  }
  
  public void setDisplayConfiguration(DisplayConfiguration paramDisplayConfiguration)
  {
    this.displayConfiguration = paramDisplayConfiguration;
    this.cameraManager.setDisplayConfiguration(paramDisplayConfiguration);
  }
  
  public void setReadyHandler(Handler paramHandler)
  {
    this.readyHandler = paramHandler;
  }
  
  public void setSurface(CameraSurface paramCameraSurface)
  {
    this.surface = paramCameraSurface;
  }
  
  public void setSurfaceHolder(SurfaceHolder paramSurfaceHolder)
  {
    setSurface(new CameraSurface(paramSurfaceHolder));
  }
  
  public void setTorch(final boolean paramBoolean)
  {
    
    if (this.open) {
      this.cameraThread.enqueue(new Runnable()
      {
        public void run()
        {
          CameraInstance.this.cameraManager.setTorch(paramBoolean);
        }
      });
    }
  }
  
  public void startPreview()
  {
    Util.validateMainThread();
    validateOpen();
    this.cameraThread.enqueue(this.previewStarter);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraInstance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */