package com.journeyapps.barcodescanner.camera;

import android.graphics.Rect;
import android.util.Log;
import com.journeyapps.barcodescanner.Size;

public class CenterCropStrategy
  extends PreviewScalingStrategy
{
  private static final String TAG = "CenterCropStrategy";
  
  protected float getScore(Size paramSize1, Size paramSize2)
  {
    if ((paramSize1.width > 0) && (paramSize1.height > 0))
    {
      Size localSize = paramSize1.scaleCrop(paramSize2);
      float f2 = localSize.width * 1.0F / paramSize1.width;
      float f1 = f2;
      if (f2 > 1.0F) {
        f1 = (float)Math.pow(1.0F / f2, 1.1D);
      }
      f2 = localSize.width * 1.0F / paramSize2.width + localSize.height * 1.0F / paramSize2.height;
      return f1 * (1.0F / f2 / f2);
    }
    return 0.0F;
  }
  
  public Rect scalePreview(Size paramSize1, Size paramSize2)
  {
    Size localSize = paramSize1.scaleCrop(paramSize2);
    String str = TAG;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Preview: ");
    localStringBuilder.append(paramSize1);
    localStringBuilder.append("; Scaled: ");
    localStringBuilder.append(localSize);
    localStringBuilder.append("; Want: ");
    localStringBuilder.append(paramSize2);
    Log.i(str, localStringBuilder.toString());
    int i = (localSize.width - paramSize2.width) / 2;
    int j = (localSize.height - paramSize2.height) / 2;
    return new Rect(-i, -j, localSize.width - i, localSize.height - j);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CenterCropStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */