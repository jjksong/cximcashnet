package com.journeyapps.barcodescanner.camera;

import com.journeyapps.barcodescanner.SourceData;

public abstract interface PreviewCallback
{
  public abstract void onPreview(SourceData paramSourceData);
  
  public abstract void onPreviewError(Exception paramException);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/PreviewCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */