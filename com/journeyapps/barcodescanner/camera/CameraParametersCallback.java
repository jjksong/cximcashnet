package com.journeyapps.barcodescanner.camera;

import android.hardware.Camera.Parameters;

public abstract interface CameraParametersCallback
{
  public abstract Camera.Parameters changeCameraParameters(Camera.Parameters paramParameters);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraParametersCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */