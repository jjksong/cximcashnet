package com.journeyapps.barcodescanner.camera;

import android.graphics.Rect;
import com.journeyapps.barcodescanner.Size;
import java.util.List;

public class DisplayConfiguration
{
  private static final String TAG = "DisplayConfiguration";
  private boolean center = false;
  private PreviewScalingStrategy previewScalingStrategy = new FitCenterStrategy();
  private int rotation;
  private Size viewfinderSize;
  
  public DisplayConfiguration(int paramInt)
  {
    this.rotation = paramInt;
  }
  
  public DisplayConfiguration(int paramInt, Size paramSize)
  {
    this.rotation = paramInt;
    this.viewfinderSize = paramSize;
  }
  
  public Size getBestPreviewSize(List<Size> paramList, boolean paramBoolean)
  {
    Size localSize = getDesiredPreviewSize(paramBoolean);
    return this.previewScalingStrategy.getBestPreviewSize(paramList, localSize);
  }
  
  public Size getDesiredPreviewSize(boolean paramBoolean)
  {
    Size localSize = this.viewfinderSize;
    if (localSize == null) {
      return null;
    }
    if (paramBoolean) {
      return localSize.rotate();
    }
    return localSize;
  }
  
  public PreviewScalingStrategy getPreviewScalingStrategy()
  {
    return this.previewScalingStrategy;
  }
  
  public int getRotation()
  {
    return this.rotation;
  }
  
  public Size getViewfinderSize()
  {
    return this.viewfinderSize;
  }
  
  public Rect scalePreview(Size paramSize)
  {
    return this.previewScalingStrategy.scalePreview(paramSize, this.viewfinderSize);
  }
  
  public void setPreviewScalingStrategy(PreviewScalingStrategy paramPreviewScalingStrategy)
  {
    this.previewScalingStrategy = paramPreviewScalingStrategy;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/DisplayConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */