package com.journeyapps.barcodescanner.camera;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.SurfaceHolder;
import com.google.zxing.client.android.AmbientLightManager;
import com.google.zxing.client.android.camera.CameraConfigurationUtils;
import com.google.zxing.client.android.camera.open.OpenCameraInterface;
import com.journeyapps.barcodescanner.Size;
import com.journeyapps.barcodescanner.SourceData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class CameraManager
{
  private static final String TAG = "CameraManager";
  private AmbientLightManager ambientLightManager;
  private AutoFocusManager autoFocusManager;
  private Camera camera;
  private Camera.CameraInfo cameraInfo;
  private final CameraPreviewCallback cameraPreviewCallback;
  private Context context;
  private String defaultParameters;
  private DisplayConfiguration displayConfiguration;
  private Size previewSize;
  private boolean previewing;
  private Size requestedPreviewSize;
  private int rotationDegrees = -1;
  private CameraSettings settings = new CameraSettings();
  
  public CameraManager(Context paramContext)
  {
    this.context = paramContext;
    this.cameraPreviewCallback = new CameraPreviewCallback();
  }
  
  private int calculateDisplayRotation()
  {
    int k = this.displayConfiguration.getRotation();
    int j = 0;
    int i = j;
    switch (k)
    {
    default: 
      i = j;
      break;
    case 3: 
      i = 270;
      break;
    case 2: 
      i = 180;
      break;
    case 1: 
      i = 90;
    }
    if (this.cameraInfo.facing == 1) {
      i = (360 - (this.cameraInfo.orientation + i) % 360) % 360;
    } else {
      i = (this.cameraInfo.orientation - i + 360) % 360;
    }
    String str = TAG;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Camera Display Orientation: ");
    localStringBuilder.append(i);
    Log.i(str, localStringBuilder.toString());
    return i;
  }
  
  private Camera.Parameters getDefaultCameraParameters()
  {
    Camera.Parameters localParameters = this.camera.getParameters();
    String str = this.defaultParameters;
    if (str == null) {
      this.defaultParameters = localParameters.flatten();
    } else {
      localParameters.unflatten(str);
    }
    return localParameters;
  }
  
  private static List<Size> getPreviewSizes(Camera.Parameters paramParameters)
  {
    Object localObject = paramParameters.getSupportedPreviewSizes();
    ArrayList localArrayList = new ArrayList();
    if (localObject == null)
    {
      paramParameters = paramParameters.getPreviewSize();
      if (paramParameters != null) {
        localArrayList.add(new Size(paramParameters.width, paramParameters.height));
      }
      return localArrayList;
    }
    paramParameters = ((List)localObject).iterator();
    while (paramParameters.hasNext())
    {
      localObject = (Camera.Size)paramParameters.next();
      localArrayList.add(new Size(((Camera.Size)localObject).width, ((Camera.Size)localObject).height));
    }
    return localArrayList;
  }
  
  private void setCameraDisplayOrientation(int paramInt)
  {
    this.camera.setDisplayOrientation(paramInt);
  }
  
  private void setDesiredParameters(boolean paramBoolean)
  {
    Camera.Parameters localParameters = getDefaultCameraParameters();
    if (localParameters == null)
    {
      Log.w(TAG, "Device error: no camera parameters are available. Proceeding without configuration.");
      return;
    }
    String str = TAG;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Initial camera parameters: ");
    ((StringBuilder)localObject).append(localParameters.flatten());
    Log.i(str, ((StringBuilder)localObject).toString());
    if (paramBoolean) {
      Log.w(TAG, "In camera config safe mode -- most settings will not be honored");
    }
    CameraConfigurationUtils.setFocus(localParameters, this.settings.getFocusMode(), paramBoolean);
    if (!paramBoolean)
    {
      CameraConfigurationUtils.setTorch(localParameters, false);
      if (this.settings.isScanInverted()) {
        CameraConfigurationUtils.setInvertColor(localParameters);
      }
      if (this.settings.isBarcodeSceneModeEnabled()) {
        CameraConfigurationUtils.setBarcodeSceneMode(localParameters);
      }
      if ((this.settings.isMeteringEnabled()) && (Build.VERSION.SDK_INT >= 15))
      {
        CameraConfigurationUtils.setVideoStabilization(localParameters);
        CameraConfigurationUtils.setFocusArea(localParameters);
        CameraConfigurationUtils.setMetering(localParameters);
      }
    }
    localObject = getPreviewSizes(localParameters);
    if (((List)localObject).size() == 0)
    {
      this.requestedPreviewSize = null;
    }
    else
    {
      this.requestedPreviewSize = this.displayConfiguration.getBestPreviewSize((List)localObject, isCameraRotated());
      localParameters.setPreviewSize(this.requestedPreviewSize.width, this.requestedPreviewSize.height);
    }
    if (Build.DEVICE.equals("glass-1")) {
      CameraConfigurationUtils.setBestPreviewFPS(localParameters);
    }
    str = TAG;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Final camera parameters: ");
    ((StringBuilder)localObject).append(localParameters.flatten());
    Log.i(str, ((StringBuilder)localObject).toString());
    this.camera.setParameters(localParameters);
  }
  
  private void setParameters()
  {
    try
    {
      this.rotationDegrees = calculateDisplayRotation();
      setCameraDisplayOrientation(this.rotationDegrees);
    }
    catch (Exception localException1)
    {
      Log.w(TAG, "Failed to set rotation.");
    }
    try
    {
      setDesiredParameters(false);
    }
    catch (Exception localException2)
    {
      try
      {
        setDesiredParameters(true);
      }
      catch (Exception localException3)
      {
        Log.w(TAG, "Camera rejected even safe-mode parameters! No configuration");
      }
    }
    Camera.Size localSize = this.camera.getParameters().getPreviewSize();
    if (localSize == null) {
      this.previewSize = this.requestedPreviewSize;
    } else {
      this.previewSize = new Size(localSize.width, localSize.height);
    }
    this.cameraPreviewCallback.setResolution(this.previewSize);
  }
  
  public void changeCameraParameters(CameraParametersCallback paramCameraParametersCallback)
  {
    Camera localCamera = this.camera;
    if (localCamera != null) {
      try
      {
        localCamera.setParameters(paramCameraParametersCallback.changeCameraParameters(localCamera.getParameters()));
      }
      catch (RuntimeException paramCameraParametersCallback)
      {
        Log.e(TAG, "Failed to change camera parameters", paramCameraParametersCallback);
      }
    }
  }
  
  public void close()
  {
    Camera localCamera = this.camera;
    if (localCamera != null)
    {
      localCamera.release();
      this.camera = null;
    }
  }
  
  public void configure()
  {
    if (this.camera != null)
    {
      setParameters();
      return;
    }
    throw new RuntimeException("Camera not open");
  }
  
  public Camera getCamera()
  {
    return this.camera;
  }
  
  public int getCameraRotation()
  {
    return this.rotationDegrees;
  }
  
  public CameraSettings getCameraSettings()
  {
    return this.settings;
  }
  
  public DisplayConfiguration getDisplayConfiguration()
  {
    return this.displayConfiguration;
  }
  
  public Size getNaturalPreviewSize()
  {
    return this.previewSize;
  }
  
  public Size getPreviewSize()
  {
    if (this.previewSize == null) {
      return null;
    }
    if (isCameraRotated()) {
      return this.previewSize.rotate();
    }
    return this.previewSize;
  }
  
  public boolean isCameraRotated()
  {
    int i = this.rotationDegrees;
    if (i != -1)
    {
      boolean bool;
      if (i % 180 != 0) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    throw new IllegalStateException("Rotation not calculated yet. Call configure() first.");
  }
  
  public boolean isOpen()
  {
    boolean bool;
    if (this.camera != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isTorchOn()
  {
    Object localObject = this.camera.getParameters();
    boolean bool2 = false;
    if (localObject != null)
    {
      localObject = ((Camera.Parameters)localObject).getFlashMode();
      boolean bool1 = bool2;
      if (localObject != null) {
        if (!"on".equals(localObject))
        {
          bool1 = bool2;
          if (!"torch".equals(localObject)) {}
        }
        else
        {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  public void open()
  {
    this.camera = OpenCameraInterface.open(this.settings.getRequestedCameraId());
    if (this.camera != null)
    {
      int i = OpenCameraInterface.getCameraId(this.settings.getRequestedCameraId());
      this.cameraInfo = new Camera.CameraInfo();
      Camera.getCameraInfo(i, this.cameraInfo);
      return;
    }
    throw new RuntimeException("Failed to open camera");
  }
  
  public void requestPreviewFrame(PreviewCallback paramPreviewCallback)
  {
    Camera localCamera = this.camera;
    if ((localCamera != null) && (this.previewing))
    {
      this.cameraPreviewCallback.setCallback(paramPreviewCallback);
      localCamera.setOneShotPreviewCallback(this.cameraPreviewCallback);
    }
  }
  
  public void setCameraSettings(CameraSettings paramCameraSettings)
  {
    this.settings = paramCameraSettings;
  }
  
  public void setDisplayConfiguration(DisplayConfiguration paramDisplayConfiguration)
  {
    this.displayConfiguration = paramDisplayConfiguration;
  }
  
  public void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
    throws IOException
  {
    setPreviewDisplay(new CameraSurface(paramSurfaceHolder));
  }
  
  public void setPreviewDisplay(CameraSurface paramCameraSurface)
    throws IOException
  {
    paramCameraSurface.setPreview(this.camera);
  }
  
  public void setTorch(boolean paramBoolean)
  {
    if (this.camera != null) {
      try
      {
        if (paramBoolean != isTorchOn())
        {
          if (this.autoFocusManager != null) {
            this.autoFocusManager.stop();
          }
          Camera.Parameters localParameters = this.camera.getParameters();
          CameraConfigurationUtils.setTorch(localParameters, paramBoolean);
          if (this.settings.isExposureEnabled()) {
            CameraConfigurationUtils.setBestExposure(localParameters, paramBoolean);
          }
          this.camera.setParameters(localParameters);
          if (this.autoFocusManager != null) {
            this.autoFocusManager.start();
          }
        }
      }
      catch (RuntimeException localRuntimeException)
      {
        Log.e(TAG, "Failed to set torch", localRuntimeException);
      }
    }
  }
  
  public void startPreview()
  {
    Camera localCamera = this.camera;
    if ((localCamera != null) && (!this.previewing))
    {
      localCamera.startPreview();
      this.previewing = true;
      this.autoFocusManager = new AutoFocusManager(this.camera, this.settings);
      this.ambientLightManager = new AmbientLightManager(this.context, this, this.settings);
      this.ambientLightManager.start();
    }
  }
  
  public void stopPreview()
  {
    Object localObject = this.autoFocusManager;
    if (localObject != null)
    {
      ((AutoFocusManager)localObject).stop();
      this.autoFocusManager = null;
    }
    localObject = this.ambientLightManager;
    if (localObject != null)
    {
      ((AmbientLightManager)localObject).stop();
      this.ambientLightManager = null;
    }
    localObject = this.camera;
    if ((localObject != null) && (this.previewing))
    {
      ((Camera)localObject).stopPreview();
      this.cameraPreviewCallback.setCallback(null);
      this.previewing = false;
    }
  }
  
  private final class CameraPreviewCallback
    implements Camera.PreviewCallback
  {
    private PreviewCallback callback;
    private Size resolution;
    
    public CameraPreviewCallback() {}
    
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
    {
      Size localSize = this.resolution;
      PreviewCallback localPreviewCallback = this.callback;
      if ((localSize != null) && (localPreviewCallback != null))
      {
        if (paramArrayOfByte != null)
        {
          try
          {
            int i = paramCamera.getParameters().getPreviewFormat();
            paramCamera = new com/journeyapps/barcodescanner/SourceData;
            paramCamera.<init>(paramArrayOfByte, localSize.width, localSize.height, i, CameraManager.this.getCameraRotation());
            localPreviewCallback.onPreview(paramCamera);
          }
          catch (RuntimeException paramArrayOfByte)
          {
            break label88;
          }
        }
        else
        {
          paramArrayOfByte = new java/lang/NullPointerException;
          paramArrayOfByte.<init>("No preview data received");
          throw paramArrayOfByte;
        }
        label88:
        Log.e(CameraManager.TAG, "Camera preview failed", paramArrayOfByte);
        localPreviewCallback.onPreviewError(paramArrayOfByte);
      }
      else
      {
        Log.d(CameraManager.TAG, "Got preview callback, but no handler or resolution available");
        if (localPreviewCallback != null) {
          localPreviewCallback.onPreviewError(new Exception("No resolution available"));
        }
      }
    }
    
    public void setCallback(PreviewCallback paramPreviewCallback)
    {
      this.callback = paramPreviewCallback;
    }
    
    public void setResolution(Size paramSize)
    {
      this.resolution = paramSize;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */