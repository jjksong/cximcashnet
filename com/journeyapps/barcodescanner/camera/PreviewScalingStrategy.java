package com.journeyapps.barcodescanner.camera;

import android.graphics.Rect;
import android.util.Log;
import com.journeyapps.barcodescanner.Size;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class PreviewScalingStrategy
{
  private static final String TAG = "PreviewScalingStrategy";
  
  public List<Size> getBestPreviewOrder(List<Size> paramList, final Size paramSize)
  {
    if (paramSize == null) {
      return paramList;
    }
    Collections.sort(paramList, new Comparator()
    {
      public int compare(Size paramAnonymousSize1, Size paramAnonymousSize2)
      {
        float f = PreviewScalingStrategy.this.getScore(paramAnonymousSize1, paramSize);
        return Float.compare(PreviewScalingStrategy.this.getScore(paramAnonymousSize2, paramSize), f);
      }
    });
    return paramList;
  }
  
  public Size getBestPreviewSize(List<Size> paramList, Size paramSize)
  {
    paramList = getBestPreviewOrder(paramList, paramSize);
    String str = TAG;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Viewfinder size: ");
    ((StringBuilder)localObject).append(paramSize);
    Log.i(str, ((StringBuilder)localObject).toString());
    localObject = TAG;
    paramSize = new StringBuilder();
    paramSize.append("Preview in order of preference: ");
    paramSize.append(paramList);
    Log.i((String)localObject, paramSize.toString());
    return (Size)paramList.get(0);
  }
  
  protected float getScore(Size paramSize1, Size paramSize2)
  {
    return 0.5F;
  }
  
  public abstract Rect scalePreview(Size paramSize1, Size paramSize2);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/PreviewScalingStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */