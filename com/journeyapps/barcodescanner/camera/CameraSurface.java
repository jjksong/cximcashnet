package com.journeyapps.barcodescanner.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import java.io.IOException;

public class CameraSurface
{
  private SurfaceHolder surfaceHolder;
  private SurfaceTexture surfaceTexture;
  
  public CameraSurface(SurfaceTexture paramSurfaceTexture)
  {
    if (paramSurfaceTexture != null)
    {
      this.surfaceTexture = paramSurfaceTexture;
      return;
    }
    throw new IllegalArgumentException("surfaceTexture may not be null");
  }
  
  public CameraSurface(SurfaceHolder paramSurfaceHolder)
  {
    if (paramSurfaceHolder != null)
    {
      this.surfaceHolder = paramSurfaceHolder;
      return;
    }
    throw new IllegalArgumentException("surfaceHolder may not be null");
  }
  
  public SurfaceHolder getSurfaceHolder()
  {
    return this.surfaceHolder;
  }
  
  public SurfaceTexture getSurfaceTexture()
  {
    return this.surfaceTexture;
  }
  
  public void setPreview(Camera paramCamera)
    throws IOException
  {
    SurfaceHolder localSurfaceHolder = this.surfaceHolder;
    if (localSurfaceHolder != null) {
      paramCamera.setPreviewDisplay(localSurfaceHolder);
    } else {
      paramCamera.setPreviewTexture(this.surfaceTexture);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraSurface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */