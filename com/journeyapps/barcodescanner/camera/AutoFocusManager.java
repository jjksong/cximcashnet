package com.journeyapps.barcodescanner.camera;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;

public final class AutoFocusManager
{
  private static final long AUTO_FOCUS_INTERVAL_MS = 2000L;
  private static final Collection<String> FOCUS_MODES_CALLING_AF = new ArrayList(2);
  private static final String TAG = "AutoFocusManager";
  private int MESSAGE_FOCUS;
  private final Camera.AutoFocusCallback autoFocusCallback;
  private final Camera camera;
  private final Handler.Callback focusHandlerCallback;
  private boolean focusing;
  private Handler handler;
  private boolean stopped;
  private final boolean useAutoFocus;
  
  static
  {
    FOCUS_MODES_CALLING_AF.add("auto");
    FOCUS_MODES_CALLING_AF.add("macro");
  }
  
  public AutoFocusManager(Camera paramCamera, CameraSettings paramCameraSettings)
  {
    boolean bool = true;
    this.MESSAGE_FOCUS = 1;
    this.focusHandlerCallback = new Handler.Callback()
    {
      public boolean handleMessage(Message paramAnonymousMessage)
      {
        if (paramAnonymousMessage.what == AutoFocusManager.this.MESSAGE_FOCUS)
        {
          AutoFocusManager.this.focus();
          return true;
        }
        return false;
      }
    };
    this.autoFocusCallback = new Camera.AutoFocusCallback()
    {
      public void onAutoFocus(boolean paramAnonymousBoolean, Camera paramAnonymousCamera)
      {
        AutoFocusManager.this.handler.post(new Runnable()
        {
          public void run()
          {
            AutoFocusManager.access$202(AutoFocusManager.this, false);
            AutoFocusManager.this.autoFocusAgainLater();
          }
        });
      }
    };
    this.handler = new Handler(this.focusHandlerCallback);
    this.camera = paramCamera;
    paramCamera = paramCamera.getParameters().getFocusMode();
    if ((!paramCameraSettings.isAutoFocusEnabled()) || (!FOCUS_MODES_CALLING_AF.contains(paramCamera))) {
      bool = false;
    }
    this.useAutoFocus = bool;
    paramCameraSettings = TAG;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Current focus mode '");
    localStringBuilder.append(paramCamera);
    localStringBuilder.append("'; use auto focus? ");
    localStringBuilder.append(this.useAutoFocus);
    Log.i(paramCameraSettings, localStringBuilder.toString());
    start();
  }
  
  private void autoFocusAgainLater()
  {
    try
    {
      if ((!this.stopped) && (!this.handler.hasMessages(this.MESSAGE_FOCUS))) {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(this.MESSAGE_FOCUS), 2000L);
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void cancelOutstandingTask()
  {
    this.handler.removeMessages(this.MESSAGE_FOCUS);
  }
  
  private void focus()
  {
    if ((this.useAutoFocus) && (!this.stopped) && (!this.focusing)) {
      try
      {
        this.camera.autoFocus(this.autoFocusCallback);
        this.focusing = true;
      }
      catch (RuntimeException localRuntimeException)
      {
        Log.w(TAG, "Unexpected exception while focusing", localRuntimeException);
        autoFocusAgainLater();
      }
    }
  }
  
  public void start()
  {
    this.stopped = false;
    focus();
  }
  
  public void stop()
  {
    this.stopped = true;
    this.focusing = false;
    cancelOutstandingTask();
    if (this.useAutoFocus) {
      try
      {
        this.camera.cancelAutoFocus();
      }
      catch (RuntimeException localRuntimeException)
      {
        Log.w(TAG, "Unexpected exception while cancelling focusing", localRuntimeException);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/AutoFocusManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */