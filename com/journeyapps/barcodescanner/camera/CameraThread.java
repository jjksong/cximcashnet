package com.journeyapps.barcodescanner.camera;

import android.os.Handler;
import android.os.HandlerThread;

class CameraThread
{
  private static final String TAG = "CameraThread";
  private static CameraThread instance;
  private final Object LOCK = new Object();
  private Handler handler;
  private int openCount = 0;
  private HandlerThread thread;
  
  private void checkRunning()
  {
    synchronized (this.LOCK)
    {
      if (this.handler == null)
      {
        Object localObject2;
        if (this.openCount > 0)
        {
          localObject2 = new android/os/HandlerThread;
          ((HandlerThread)localObject2).<init>("CameraThread");
          this.thread = ((HandlerThread)localObject2);
          this.thread.start();
          localObject2 = new android/os/Handler;
          ((Handler)localObject2).<init>(this.thread.getLooper());
          this.handler = ((Handler)localObject2);
        }
        else
        {
          localObject2 = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject2).<init>("CameraThread is not open");
          throw ((Throwable)localObject2);
        }
      }
      return;
    }
  }
  
  public static CameraThread getInstance()
  {
    if (instance == null) {
      instance = new CameraThread();
    }
    return instance;
  }
  
  private void quit()
  {
    synchronized (this.LOCK)
    {
      this.thread.quit();
      this.thread = null;
      this.handler = null;
      return;
    }
  }
  
  protected void decrementInstances()
  {
    synchronized (this.LOCK)
    {
      this.openCount -= 1;
      if (this.openCount == 0) {
        quit();
      }
      return;
    }
  }
  
  protected void enqueue(Runnable paramRunnable)
  {
    synchronized (this.LOCK)
    {
      checkRunning();
      this.handler.post(paramRunnable);
      return;
    }
  }
  
  protected void enqueueDelayed(Runnable paramRunnable, long paramLong)
  {
    synchronized (this.LOCK)
    {
      checkRunning();
      this.handler.postDelayed(paramRunnable, paramLong);
      return;
    }
  }
  
  protected void incrementAndEnqueue(Runnable paramRunnable)
  {
    synchronized (this.LOCK)
    {
      this.openCount += 1;
      enqueue(paramRunnable);
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */