package com.journeyapps.barcodescanner.camera;

import android.graphics.Rect;
import android.util.Log;
import com.journeyapps.barcodescanner.Size;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LegacyPreviewScalingStrategy
  extends PreviewScalingStrategy
{
  private static final String TAG = "LegacyPreviewScalingStrategy";
  
  public static Size scale(Size paramSize1, Size paramSize2)
  {
    Size localSize1 = paramSize1;
    Size localSize2;
    if (!paramSize2.fitsIn(paramSize1))
    {
      do
      {
        localSize2 = paramSize1.scale(3, 2);
        localSize1 = paramSize1.scale(2, 1);
        if (paramSize2.fitsIn(localSize2)) {
          return localSize2;
        }
        paramSize1 = localSize1;
      } while (!paramSize2.fitsIn(localSize1));
      return localSize1;
    }
    for (;;)
    {
      localSize2 = localSize1.scale(2, 3);
      paramSize1 = localSize1.scale(1, 2);
      if (!paramSize2.fitsIn(paramSize1))
      {
        if (paramSize2.fitsIn(localSize2)) {
          return localSize2;
        }
        return localSize1;
      }
      localSize1 = paramSize1;
    }
  }
  
  public Size getBestPreviewSize(List<Size> paramList, final Size paramSize)
  {
    if (paramSize == null) {
      return (Size)paramList.get(0);
    }
    Collections.sort(paramList, new Comparator()
    {
      public int compare(Size paramAnonymousSize1, Size paramAnonymousSize2)
      {
        int i = LegacyPreviewScalingStrategy.scale(paramAnonymousSize1, paramSize).width - paramAnonymousSize1.width;
        int j = LegacyPreviewScalingStrategy.scale(paramAnonymousSize2, paramSize).width - paramAnonymousSize2.width;
        if ((i == 0) && (j == 0)) {
          return paramAnonymousSize1.compareTo(paramAnonymousSize2);
        }
        if (i == 0) {
          return -1;
        }
        if (j == 0) {
          return 1;
        }
        if ((i < 0) && (j < 0)) {
          return paramAnonymousSize1.compareTo(paramAnonymousSize2);
        }
        if ((i > 0) && (j > 0)) {
          return -paramAnonymousSize1.compareTo(paramAnonymousSize2);
        }
        if (i < 0) {
          return -1;
        }
        return 1;
      }
    });
    String str = TAG;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Viewfinder size: ");
    ((StringBuilder)localObject).append(paramSize);
    Log.i(str, ((StringBuilder)localObject).toString());
    localObject = TAG;
    paramSize = new StringBuilder();
    paramSize.append("Preview in order of preference: ");
    paramSize.append(paramList);
    Log.i((String)localObject, paramSize.toString());
    return (Size)paramList.get(0);
  }
  
  public Rect scalePreview(Size paramSize1, Size paramSize2)
  {
    Size localSize = scale(paramSize1, paramSize2);
    String str = TAG;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Preview: ");
    localStringBuilder.append(paramSize1);
    localStringBuilder.append("; Scaled: ");
    localStringBuilder.append(localSize);
    localStringBuilder.append("; Want: ");
    localStringBuilder.append(paramSize2);
    Log.i(str, localStringBuilder.toString());
    int j = (localSize.width - paramSize2.width) / 2;
    int i = (localSize.height - paramSize2.height) / 2;
    return new Rect(-j, -i, localSize.width - j, localSize.height - i);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/LegacyPreviewScalingStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */