package com.journeyapps.barcodescanner.camera;

import android.graphics.Rect;
import com.journeyapps.barcodescanner.Size;

public class FitXYStrategy
  extends PreviewScalingStrategy
{
  private static final String TAG = "FitXYStrategy";
  
  private static float absRatio(float paramFloat)
  {
    if (paramFloat < 1.0F) {
      return 1.0F / paramFloat;
    }
    return paramFloat;
  }
  
  protected float getScore(Size paramSize1, Size paramSize2)
  {
    if ((paramSize1.width > 0) && (paramSize1.height > 0))
    {
      float f1 = absRatio(paramSize1.width * 1.0F / paramSize2.width);
      float f2 = absRatio(paramSize1.height * 1.0F / paramSize2.height);
      f2 = 1.0F / f1 / f2;
      f1 = absRatio(paramSize1.width * 1.0F / paramSize1.height / (paramSize2.width * 1.0F / paramSize2.height));
      return f2 * (1.0F / f1 / f1 / f1);
    }
    return 0.0F;
  }
  
  public Rect scalePreview(Size paramSize1, Size paramSize2)
  {
    return new Rect(0, 0, paramSize2.width, paramSize2.height);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/FitXYStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */