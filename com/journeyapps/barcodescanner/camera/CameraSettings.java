package com.journeyapps.barcodescanner.camera;

public class CameraSettings
{
  private boolean autoFocusEnabled = true;
  private boolean autoTorchEnabled = false;
  private boolean barcodeSceneModeEnabled = false;
  private boolean continuousFocusEnabled = false;
  private boolean exposureEnabled = false;
  private FocusMode focusMode = FocusMode.AUTO;
  private boolean meteringEnabled = false;
  private int requestedCameraId = -1;
  private boolean scanInverted = false;
  
  public FocusMode getFocusMode()
  {
    return this.focusMode;
  }
  
  public int getRequestedCameraId()
  {
    return this.requestedCameraId;
  }
  
  public boolean isAutoFocusEnabled()
  {
    return this.autoFocusEnabled;
  }
  
  public boolean isAutoTorchEnabled()
  {
    return this.autoTorchEnabled;
  }
  
  public boolean isBarcodeSceneModeEnabled()
  {
    return this.barcodeSceneModeEnabled;
  }
  
  public boolean isContinuousFocusEnabled()
  {
    return this.continuousFocusEnabled;
  }
  
  public boolean isExposureEnabled()
  {
    return this.exposureEnabled;
  }
  
  public boolean isMeteringEnabled()
  {
    return this.meteringEnabled;
  }
  
  public boolean isScanInverted()
  {
    return this.scanInverted;
  }
  
  public void setAutoFocusEnabled(boolean paramBoolean)
  {
    this.autoFocusEnabled = paramBoolean;
    if ((paramBoolean) && (this.continuousFocusEnabled)) {
      this.focusMode = FocusMode.CONTINUOUS;
    } else if (paramBoolean) {
      this.focusMode = FocusMode.AUTO;
    } else {
      this.focusMode = null;
    }
  }
  
  public void setAutoTorchEnabled(boolean paramBoolean)
  {
    this.autoTorchEnabled = paramBoolean;
  }
  
  public void setBarcodeSceneModeEnabled(boolean paramBoolean)
  {
    this.barcodeSceneModeEnabled = paramBoolean;
  }
  
  public void setContinuousFocusEnabled(boolean paramBoolean)
  {
    this.continuousFocusEnabled = paramBoolean;
    if (paramBoolean) {
      this.focusMode = FocusMode.CONTINUOUS;
    } else if (this.autoFocusEnabled) {
      this.focusMode = FocusMode.AUTO;
    } else {
      this.focusMode = null;
    }
  }
  
  public void setExposureEnabled(boolean paramBoolean)
  {
    this.exposureEnabled = paramBoolean;
  }
  
  public void setFocusMode(FocusMode paramFocusMode)
  {
    this.focusMode = paramFocusMode;
  }
  
  public void setMeteringEnabled(boolean paramBoolean)
  {
    this.meteringEnabled = paramBoolean;
  }
  
  public void setRequestedCameraId(int paramInt)
  {
    this.requestedCameraId = paramInt;
  }
  
  public void setScanInverted(boolean paramBoolean)
  {
    this.scanInverted = paramBoolean;
  }
  
  public static enum FocusMode
  {
    AUTO,  CONTINUOUS,  INFINITY,  MACRO;
    
    private FocusMode() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/camera/CameraSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */