package com.journeyapps.barcodescanner;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.common.HybridBinarizer;

public class MixedDecoder
  extends Decoder
{
  private boolean isInverted = true;
  
  public MixedDecoder(Reader paramReader)
  {
    super(paramReader);
  }
  
  protected BinaryBitmap toBitmap(LuminanceSource paramLuminanceSource)
  {
    if (this.isInverted)
    {
      this.isInverted = false;
      return new BinaryBitmap(new HybridBinarizer(paramLuminanceSource.invert()));
    }
    this.isInverted = true;
    return new BinaryBitmap(new HybridBinarizer(paramLuminanceSource));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/MixedDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */