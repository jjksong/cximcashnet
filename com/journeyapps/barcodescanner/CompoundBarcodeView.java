package com.journeyapps.barcodescanner;

import android.content.Context;
import android.util.AttributeSet;

public class CompoundBarcodeView
  extends DecoratedBarcodeView
{
  public CompoundBarcodeView(Context paramContext)
  {
    super(paramContext);
  }
  
  public CompoundBarcodeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public CompoundBarcodeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/CompoundBarcodeView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */