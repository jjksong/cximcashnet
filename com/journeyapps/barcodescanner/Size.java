package com.journeyapps.barcodescanner;

import android.support.annotation.NonNull;

public class Size
  implements Comparable<Size>
{
  public final int height;
  public final int width;
  
  public Size(int paramInt1, int paramInt2)
  {
    this.width = paramInt1;
    this.height = paramInt2;
  }
  
  public int compareTo(@NonNull Size paramSize)
  {
    int j = this.height * this.width;
    int i = paramSize.height * paramSize.width;
    if (i < j) {
      return 1;
    }
    if (i > j) {
      return -1;
    }
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (Size)paramObject;
      if ((this.width != ((Size)paramObject).width) || (this.height != ((Size)paramObject).height)) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public boolean fitsIn(Size paramSize)
  {
    boolean bool;
    if ((this.width <= paramSize.width) && (this.height <= paramSize.height)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int hashCode()
  {
    return this.width * 31 + this.height;
  }
  
  public Size rotate()
  {
    return new Size(this.height, this.width);
  }
  
  public Size scale(int paramInt1, int paramInt2)
  {
    return new Size(this.width * paramInt1 / paramInt2, this.height * paramInt1 / paramInt2);
  }
  
  public Size scaleCrop(Size paramSize)
  {
    int j = this.width;
    int i = paramSize.height;
    int k = paramSize.width;
    int m = this.height;
    if (j * i <= k * m) {
      return new Size(k, m * k / j);
    }
    return new Size(j * i / m, i);
  }
  
  public Size scaleFit(Size paramSize)
  {
    int k = this.width;
    int i = paramSize.height;
    int j = paramSize.width;
    int m = this.height;
    if (k * i >= j * m) {
      return new Size(j, m * j / k);
    }
    return new Size(k * i / m, i);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.width);
    localStringBuilder.append("x");
    localStringBuilder.append(this.height);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/Size.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */