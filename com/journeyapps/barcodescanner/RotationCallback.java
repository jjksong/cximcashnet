package com.journeyapps.barcodescanner;

public abstract interface RotationCallback
{
  public abstract void onRotationChanged(int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/RotationCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */