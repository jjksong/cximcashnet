package com.journeyapps.barcodescanner;

import android.os.Looper;

public class Util
{
  public static void validateMainThread()
  {
    if (Looper.getMainLooper() == Looper.myLooper()) {
      return;
    }
    throw new IllegalStateException("Must be called from the main thread.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/Util.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */