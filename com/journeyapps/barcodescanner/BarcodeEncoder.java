package com.journeyapps.barcodescanner;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Map;

public class BarcodeEncoder
{
  private static final int BLACK = -16777216;
  private static final int WHITE = -1;
  
  public Bitmap createBitmap(BitMatrix paramBitMatrix)
  {
    int n = paramBitMatrix.getWidth();
    int m = paramBitMatrix.getHeight();
    int[] arrayOfInt = new int[n * m];
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++)
      {
        int k;
        if (paramBitMatrix.get(j, i)) {
          k = -16777216;
        } else {
          k = -1;
        }
        arrayOfInt[(i * n + j)] = k;
      }
    }
    paramBitMatrix = Bitmap.createBitmap(n, m, Bitmap.Config.ARGB_8888);
    paramBitMatrix.setPixels(arrayOfInt, 0, n, 0, 0, n, m);
    return paramBitMatrix;
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
    throws WriterException
  {
    try
    {
      MultiFormatWriter localMultiFormatWriter = new com/google/zxing/MultiFormatWriter;
      localMultiFormatWriter.<init>();
      paramString = localMultiFormatWriter.encode(paramString, paramBarcodeFormat, paramInt1, paramInt2);
      return paramString;
    }
    catch (Exception paramString)
    {
      throw new WriterException(paramString);
    }
    catch (WriterException paramString)
    {
      throw paramString;
    }
  }
  
  public BitMatrix encode(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    try
    {
      MultiFormatWriter localMultiFormatWriter = new com/google/zxing/MultiFormatWriter;
      localMultiFormatWriter.<init>();
      paramString = localMultiFormatWriter.encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap);
      return paramString;
    }
    catch (Exception paramString)
    {
      throw new WriterException(paramString);
    }
    catch (WriterException paramString)
    {
      throw paramString;
    }
  }
  
  public Bitmap encodeBitmap(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2)
    throws WriterException
  {
    return createBitmap(encode(paramString, paramBarcodeFormat, paramInt1, paramInt2));
  }
  
  public Bitmap encodeBitmap(String paramString, BarcodeFormat paramBarcodeFormat, int paramInt1, int paramInt2, Map<EncodeHintType, ?> paramMap)
    throws WriterException
  {
    return createBitmap(encode(paramString, paramBarcodeFormat, paramInt1, paramInt2, paramMap));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/BarcodeEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */