package com.journeyapps.barcodescanner;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import com.google.zxing.client.android.R.id;
import com.google.zxing.client.android.R.layout;

public class CaptureActivity
  extends Activity
{
  private DecoratedBarcodeView barcodeScannerView;
  private CaptureManager capture;
  
  protected DecoratedBarcodeView initializeContent()
  {
    setContentView(R.layout.zxing_capture);
    return (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.barcodeScannerView = initializeContent();
    this.capture = new CaptureManager(this, this.barcodeScannerView);
    this.capture.initializeFromIntent(getIntent(), paramBundle);
    this.capture.decode();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.capture.onDestroy();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool;
    if ((!this.barcodeScannerView.onKeyDown(paramInt, paramKeyEvent)) && (!super.onKeyDown(paramInt, paramKeyEvent))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  protected void onPause()
  {
    super.onPause();
    this.capture.onPause();
  }
  
  public void onRequestPermissionsResult(int paramInt, @NonNull String[] paramArrayOfString, @NonNull int[] paramArrayOfInt)
  {
    this.capture.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  protected void onResume()
  {
    super.onResume();
    this.capture.onResume();
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    this.capture.onSaveInstanceState(paramBundle);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/CaptureActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */