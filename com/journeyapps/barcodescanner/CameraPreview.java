package com.journeyapps.barcodescanner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.google.zxing.client.android.R.id;
import com.google.zxing.client.android.R.styleable;
import com.journeyapps.barcodescanner.camera.CameraInstance;
import com.journeyapps.barcodescanner.camera.CameraParametersCallback;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import com.journeyapps.barcodescanner.camera.CameraSurface;
import com.journeyapps.barcodescanner.camera.CenterCropStrategy;
import com.journeyapps.barcodescanner.camera.DisplayConfiguration;
import com.journeyapps.barcodescanner.camera.FitCenterStrategy;
import com.journeyapps.barcodescanner.camera.FitXYStrategy;
import com.journeyapps.barcodescanner.camera.PreviewScalingStrategy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CameraPreview
  extends ViewGroup
{
  private static final int ROTATION_LISTENER_DELAY_MS = 250;
  private static final String TAG = "CameraPreview";
  private CameraInstance cameraInstance;
  private CameraSettings cameraSettings = new CameraSettings();
  private Size containerSize;
  private Size currentSurfaceSize;
  private DisplayConfiguration displayConfiguration;
  private final StateListener fireState = new StateListener()
  {
    public void cameraClosed()
    {
      Iterator localIterator = CameraPreview.this.stateListeners.iterator();
      while (localIterator.hasNext()) {
        ((CameraPreview.StateListener)localIterator.next()).cameraClosed();
      }
    }
    
    public void cameraError(Exception paramAnonymousException)
    {
      Iterator localIterator = CameraPreview.this.stateListeners.iterator();
      while (localIterator.hasNext()) {
        ((CameraPreview.StateListener)localIterator.next()).cameraError(paramAnonymousException);
      }
    }
    
    public void previewSized()
    {
      Iterator localIterator = CameraPreview.this.stateListeners.iterator();
      while (localIterator.hasNext()) {
        ((CameraPreview.StateListener)localIterator.next()).previewSized();
      }
    }
    
    public void previewStarted()
    {
      Iterator localIterator = CameraPreview.this.stateListeners.iterator();
      while (localIterator.hasNext()) {
        ((CameraPreview.StateListener)localIterator.next()).previewStarted();
      }
    }
    
    public void previewStopped()
    {
      Iterator localIterator = CameraPreview.this.stateListeners.iterator();
      while (localIterator.hasNext()) {
        ((CameraPreview.StateListener)localIterator.next()).previewStopped();
      }
    }
  };
  private Rect framingRect = null;
  private Size framingRectSize = null;
  private double marginFraction = 0.1D;
  private int openedOrientation = -1;
  private boolean previewActive = false;
  private Rect previewFramingRect = null;
  private PreviewScalingStrategy previewScalingStrategy = null;
  private Size previewSize;
  private RotationCallback rotationCallback = new RotationCallback()
  {
    public void onRotationChanged(int paramAnonymousInt)
    {
      CameraPreview.this.stateHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          CameraPreview.this.rotationChanged();
        }
      }, 250L);
    }
  };
  private RotationListener rotationListener;
  private final Handler.Callback stateCallback = new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == R.id.zxing_prewiew_size_ready)
      {
        CameraPreview.this.previewSized((Size)paramAnonymousMessage.obj);
        return true;
      }
      if (paramAnonymousMessage.what == R.id.zxing_camera_error)
      {
        paramAnonymousMessage = (Exception)paramAnonymousMessage.obj;
        if (CameraPreview.this.isActive())
        {
          CameraPreview.this.pause();
          CameraPreview.this.fireState.cameraError(paramAnonymousMessage);
        }
      }
      else if (paramAnonymousMessage.what == R.id.zxing_camera_closed)
      {
        CameraPreview.this.fireState.cameraClosed();
      }
      return false;
    }
  };
  private Handler stateHandler;
  private List<StateListener> stateListeners = new ArrayList();
  private final SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback()
  {
    public void surfaceChanged(SurfaceHolder paramAnonymousSurfaceHolder, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      if (paramAnonymousSurfaceHolder == null)
      {
        Log.e(CameraPreview.TAG, "*** WARNING *** surfaceChanged() gave us a null surface!");
        return;
      }
      CameraPreview.access$002(CameraPreview.this, new Size(paramAnonymousInt2, paramAnonymousInt3));
      CameraPreview.this.startPreviewIfReady();
    }
    
    public void surfaceCreated(SurfaceHolder paramAnonymousSurfaceHolder) {}
    
    public void surfaceDestroyed(SurfaceHolder paramAnonymousSurfaceHolder)
    {
      CameraPreview.access$002(CameraPreview.this, null);
    }
  };
  private Rect surfaceRect;
  private SurfaceView surfaceView;
  private TextureView textureView;
  private boolean torchOn = false;
  private boolean useTextureView = false;
  private WindowManager windowManager;
  
  public CameraPreview(Context paramContext)
  {
    super(paramContext);
    initialize(paramContext, null, 0, 0);
  }
  
  public CameraPreview(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initialize(paramContext, paramAttributeSet, 0, 0);
  }
  
  public CameraPreview(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initialize(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  private void calculateFrames()
  {
    if (this.containerSize != null)
    {
      Object localObject = this.previewSize;
      if ((localObject != null) && (this.displayConfiguration != null))
      {
        int j = ((Size)localObject).width;
        int m = this.previewSize.height;
        int i = this.containerSize.width;
        int k = this.containerSize.height;
        this.surfaceRect = this.displayConfiguration.scalePreview(this.previewSize);
        this.framingRect = calculateFramingRect(new Rect(0, 0, i, k), this.surfaceRect);
        localObject = new Rect(this.framingRect);
        ((Rect)localObject).offset(-this.surfaceRect.left, -this.surfaceRect.top);
        this.previewFramingRect = new Rect(((Rect)localObject).left * j / this.surfaceRect.width(), ((Rect)localObject).top * m / this.surfaceRect.height(), ((Rect)localObject).right * j / this.surfaceRect.width(), ((Rect)localObject).bottom * m / this.surfaceRect.height());
        if ((this.previewFramingRect.width() > 0) && (this.previewFramingRect.height() > 0))
        {
          this.fireState.previewSized();
        }
        else
        {
          this.previewFramingRect = null;
          this.framingRect = null;
          Log.w(TAG, "Preview frame is too small");
        }
        return;
      }
    }
    this.previewFramingRect = null;
    this.framingRect = null;
    this.surfaceRect = null;
    throw new IllegalStateException("containerSize or previewSize is not set yet");
  }
  
  private void containerSized(Size paramSize)
  {
    this.containerSize = paramSize;
    CameraInstance localCameraInstance = this.cameraInstance;
    if ((localCameraInstance != null) && (localCameraInstance.getDisplayConfiguration() == null))
    {
      this.displayConfiguration = new DisplayConfiguration(getDisplayRotation(), paramSize);
      this.displayConfiguration.setPreviewScalingStrategy(getPreviewScalingStrategy());
      this.cameraInstance.setDisplayConfiguration(this.displayConfiguration);
      this.cameraInstance.configureCamera();
      boolean bool = this.torchOn;
      if (bool) {
        this.cameraInstance.setTorch(bool);
      }
    }
  }
  
  private int getDisplayRotation()
  {
    return this.windowManager.getDefaultDisplay().getRotation();
  }
  
  private void initCamera()
  {
    if (this.cameraInstance != null)
    {
      Log.w(TAG, "initCamera called twice");
      return;
    }
    this.cameraInstance = createCameraInstance();
    this.cameraInstance.setReadyHandler(this.stateHandler);
    this.cameraInstance.open();
    this.openedOrientation = getDisplayRotation();
  }
  
  private void initialize(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    if (getBackground() == null) {
      setBackgroundColor(-16777216);
    }
    initializeAttributes(paramAttributeSet);
    this.windowManager = ((WindowManager)paramContext.getSystemService("window"));
    this.stateHandler = new Handler(this.stateCallback);
    this.rotationListener = new RotationListener();
  }
  
  private void previewSized(Size paramSize)
  {
    this.previewSize = paramSize;
    if (this.containerSize != null)
    {
      calculateFrames();
      requestLayout();
      startPreviewIfReady();
    }
  }
  
  private void rotationChanged()
  {
    if ((isActive()) && (getDisplayRotation() != this.openedOrientation))
    {
      pause();
      resume();
    }
  }
  
  private void setupSurfaceView()
  {
    if (this.useTextureView)
    {
      this.textureView = new TextureView(getContext());
      this.textureView.setSurfaceTextureListener(surfaceTextureListener());
      addView(this.textureView);
    }
    else
    {
      this.surfaceView = new SurfaceView(getContext());
      this.surfaceView.getHolder().addCallback(this.surfaceCallback);
      addView(this.surfaceView);
    }
  }
  
  private void startCameraPreview(CameraSurface paramCameraSurface)
  {
    if ((!this.previewActive) && (this.cameraInstance != null))
    {
      Log.i(TAG, "Starting preview");
      this.cameraInstance.setSurface(paramCameraSurface);
      this.cameraInstance.startPreview();
      this.previewActive = true;
      previewStarted();
      this.fireState.previewStarted();
    }
  }
  
  private void startPreviewIfReady()
  {
    Object localObject = this.currentSurfaceSize;
    if ((localObject != null) && (this.previewSize != null))
    {
      Rect localRect = this.surfaceRect;
      if (localRect != null) {
        if ((this.surfaceView != null) && (((Size)localObject).equals(new Size(localRect.width(), this.surfaceRect.height()))))
        {
          startCameraPreview(new CameraSurface(this.surfaceView.getHolder()));
        }
        else
        {
          localObject = this.textureView;
          if ((localObject != null) && (((TextureView)localObject).getSurfaceTexture() != null))
          {
            if (this.previewSize != null)
            {
              localObject = calculateTextureTransform(new Size(this.textureView.getWidth(), this.textureView.getHeight()), this.previewSize);
              this.textureView.setTransform((Matrix)localObject);
            }
            startCameraPreview(new CameraSurface(this.textureView.getSurfaceTexture()));
          }
        }
      }
    }
  }
  
  @TargetApi(14)
  private TextureView.SurfaceTextureListener surfaceTextureListener()
  {
    new TextureView.SurfaceTextureListener()
    {
      public void onSurfaceTextureAvailable(SurfaceTexture paramAnonymousSurfaceTexture, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        onSurfaceTextureSizeChanged(paramAnonymousSurfaceTexture, paramAnonymousInt1, paramAnonymousInt2);
      }
      
      public boolean onSurfaceTextureDestroyed(SurfaceTexture paramAnonymousSurfaceTexture)
      {
        return false;
      }
      
      public void onSurfaceTextureSizeChanged(SurfaceTexture paramAnonymousSurfaceTexture, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        CameraPreview.access$002(CameraPreview.this, new Size(paramAnonymousInt1, paramAnonymousInt2));
        CameraPreview.this.startPreviewIfReady();
      }
      
      public void onSurfaceTextureUpdated(SurfaceTexture paramAnonymousSurfaceTexture) {}
    };
  }
  
  public void addStateListener(StateListener paramStateListener)
  {
    this.stateListeners.add(paramStateListener);
  }
  
  protected Rect calculateFramingRect(Rect paramRect1, Rect paramRect2)
  {
    paramRect1 = new Rect(paramRect1);
    paramRect1.intersect(paramRect2);
    if (this.framingRectSize != null)
    {
      paramRect1.inset(Math.max(0, (paramRect1.width() - this.framingRectSize.width) / 2), Math.max(0, (paramRect1.height() - this.framingRectSize.height) / 2));
      return paramRect1;
    }
    double d4 = paramRect1.width();
    double d1 = this.marginFraction;
    Double.isNaN(d4);
    double d2 = paramRect1.height();
    double d3 = this.marginFraction;
    Double.isNaN(d2);
    int i = (int)Math.min(d4 * d1, d2 * d3);
    paramRect1.inset(i, i);
    if (paramRect1.height() > paramRect1.width()) {
      paramRect1.inset(0, (paramRect1.height() - paramRect1.width()) / 2);
    }
    return paramRect1;
  }
  
  protected Matrix calculateTextureTransform(Size paramSize1, Size paramSize2)
  {
    float f1 = paramSize1.width / paramSize1.height;
    float f3 = paramSize2.width / paramSize2.height;
    float f2 = 1.0F;
    if (f1 < f3)
    {
      f2 = f3 / f1;
      f1 = 1.0F;
    }
    else
    {
      f1 /= f3;
    }
    paramSize2 = new Matrix();
    paramSize2.setScale(f2, f1);
    f3 = paramSize1.width;
    float f4 = paramSize1.height;
    paramSize2.postTranslate((paramSize1.width - f3 * f2) / 2.0F, (paramSize1.height - f4 * f1) / 2.0F);
    return paramSize2;
  }
  
  public void changeCameraParameters(CameraParametersCallback paramCameraParametersCallback)
  {
    CameraInstance localCameraInstance = this.cameraInstance;
    if (localCameraInstance != null) {
      localCameraInstance.changeCameraParameters(paramCameraParametersCallback);
    }
  }
  
  protected CameraInstance createCameraInstance()
  {
    CameraInstance localCameraInstance = new CameraInstance(getContext());
    localCameraInstance.setCameraSettings(this.cameraSettings);
    return localCameraInstance;
  }
  
  public CameraInstance getCameraInstance()
  {
    return this.cameraInstance;
  }
  
  public CameraSettings getCameraSettings()
  {
    return this.cameraSettings;
  }
  
  public Rect getFramingRect()
  {
    return this.framingRect;
  }
  
  public Size getFramingRectSize()
  {
    return this.framingRectSize;
  }
  
  public double getMarginFraction()
  {
    return this.marginFraction;
  }
  
  public Rect getPreviewFramingRect()
  {
    return this.previewFramingRect;
  }
  
  public PreviewScalingStrategy getPreviewScalingStrategy()
  {
    PreviewScalingStrategy localPreviewScalingStrategy = this.previewScalingStrategy;
    if (localPreviewScalingStrategy != null) {
      return localPreviewScalingStrategy;
    }
    if (this.textureView != null) {
      return new CenterCropStrategy();
    }
    return new FitCenterStrategy();
  }
  
  protected void initializeAttributes(AttributeSet paramAttributeSet)
  {
    paramAttributeSet = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.zxing_camera_preview);
    int i = (int)paramAttributeSet.getDimension(R.styleable.zxing_camera_preview_zxing_framing_rect_width, -1.0F);
    int j = (int)paramAttributeSet.getDimension(R.styleable.zxing_camera_preview_zxing_framing_rect_height, -1.0F);
    if ((i > 0) && (j > 0)) {
      this.framingRectSize = new Size(i, j);
    }
    this.useTextureView = paramAttributeSet.getBoolean(R.styleable.zxing_camera_preview_zxing_use_texture_view, true);
    i = paramAttributeSet.getInteger(R.styleable.zxing_camera_preview_zxing_preview_scaling_strategy, -1);
    if (i == 1) {
      this.previewScalingStrategy = new CenterCropStrategy();
    } else if (i == 2) {
      this.previewScalingStrategy = new FitCenterStrategy();
    } else if (i == 3) {
      this.previewScalingStrategy = new FitXYStrategy();
    }
    paramAttributeSet.recycle();
  }
  
  protected boolean isActive()
  {
    boolean bool;
    if (this.cameraInstance != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isCameraClosed()
  {
    CameraInstance localCameraInstance = this.cameraInstance;
    boolean bool;
    if ((localCameraInstance != null) && (!localCameraInstance.isCameraClosed())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isPreviewActive()
  {
    return this.previewActive;
  }
  
  public boolean isUseTextureView()
  {
    return this.useTextureView;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    setupSurfaceView();
  }
  
  @SuppressLint({"DrawAllocation"})
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    containerSized(new Size(paramInt3 - paramInt1, paramInt4 - paramInt2));
    SurfaceView localSurfaceView = this.surfaceView;
    Object localObject;
    if (localSurfaceView != null)
    {
      localObject = this.surfaceRect;
      if (localObject == null) {
        localSurfaceView.layout(0, 0, getWidth(), getHeight());
      } else {
        localSurfaceView.layout(((Rect)localObject).left, this.surfaceRect.top, this.surfaceRect.right, this.surfaceRect.bottom);
      }
    }
    else
    {
      localObject = this.textureView;
      if (localObject != null) {
        ((TextureView)localObject).layout(0, 0, getWidth(), getHeight());
      }
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof Bundle))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    paramParcelable = (Bundle)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getParcelable("super"));
    setTorch(paramParcelable.getBoolean("torch"));
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Parcelable localParcelable = super.onSaveInstanceState();
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("super", localParcelable);
    localBundle.putBoolean("torch", this.torchOn);
    return localBundle;
  }
  
  public void pause()
  {
    Util.validateMainThread();
    Log.d(TAG, "pause()");
    this.openedOrientation = -1;
    Object localObject = this.cameraInstance;
    if (localObject != null)
    {
      ((CameraInstance)localObject).close();
      this.cameraInstance = null;
      this.previewActive = false;
    }
    else
    {
      this.stateHandler.sendEmptyMessage(R.id.zxing_camera_closed);
    }
    if (this.currentSurfaceSize == null)
    {
      localObject = this.surfaceView;
      if (localObject != null) {
        ((SurfaceView)localObject).getHolder().removeCallback(this.surfaceCallback);
      }
    }
    if (this.currentSurfaceSize == null)
    {
      localObject = this.textureView;
      if (localObject != null) {
        ((TextureView)localObject).setSurfaceTextureListener(null);
      }
    }
    this.containerSize = null;
    this.previewSize = null;
    this.previewFramingRect = null;
    this.rotationListener.stop();
    this.fireState.previewStopped();
  }
  
  public void pauseAndWait()
  {
    CameraInstance localCameraInstance = getCameraInstance();
    pause();
    long l = System.nanoTime();
    for (;;)
    {
      if ((localCameraInstance != null) && (!localCameraInstance.isCameraClosed()) && (System.nanoTime() - l <= 2000000000L)) {}
      try
      {
        Thread.sleep(1L);
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  protected void previewStarted() {}
  
  public void resume()
  {
    Util.validateMainThread();
    Log.d(TAG, "resume()");
    initCamera();
    if (this.currentSurfaceSize != null)
    {
      startPreviewIfReady();
    }
    else
    {
      Object localObject = this.surfaceView;
      if (localObject != null)
      {
        ((SurfaceView)localObject).getHolder().addCallback(this.surfaceCallback);
      }
      else
      {
        localObject = this.textureView;
        if (localObject != null) {
          if (((TextureView)localObject).isAvailable()) {
            surfaceTextureListener().onSurfaceTextureAvailable(this.textureView.getSurfaceTexture(), this.textureView.getWidth(), this.textureView.getHeight());
          } else {
            this.textureView.setSurfaceTextureListener(surfaceTextureListener());
          }
        }
      }
    }
    requestLayout();
    this.rotationListener.listen(getContext(), this.rotationCallback);
  }
  
  public void setCameraSettings(CameraSettings paramCameraSettings)
  {
    this.cameraSettings = paramCameraSettings;
  }
  
  public void setFramingRectSize(Size paramSize)
  {
    this.framingRectSize = paramSize;
  }
  
  public void setMarginFraction(double paramDouble)
  {
    if (paramDouble < 0.5D)
    {
      this.marginFraction = paramDouble;
      return;
    }
    throw new IllegalArgumentException("The margin fraction must be less than 0.5");
  }
  
  public void setPreviewScalingStrategy(PreviewScalingStrategy paramPreviewScalingStrategy)
  {
    this.previewScalingStrategy = paramPreviewScalingStrategy;
  }
  
  public void setTorch(boolean paramBoolean)
  {
    this.torchOn = paramBoolean;
    CameraInstance localCameraInstance = this.cameraInstance;
    if (localCameraInstance != null) {
      localCameraInstance.setTorch(paramBoolean);
    }
  }
  
  public void setUseTextureView(boolean paramBoolean)
  {
    this.useTextureView = paramBoolean;
  }
  
  public static abstract interface StateListener
  {
    public abstract void cameraClosed();
    
    public abstract void cameraError(Exception paramException);
    
    public abstract void previewSized();
    
    public abstract void previewStarted();
    
    public abstract void previewStopped();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/CameraPreview.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */