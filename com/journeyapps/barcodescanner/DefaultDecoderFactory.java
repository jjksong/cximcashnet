package com.journeyapps.barcodescanner;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

public class DefaultDecoderFactory
  implements DecoderFactory
{
  private String characterSet;
  private Collection<BarcodeFormat> decodeFormats;
  private Map<DecodeHintType, ?> hints;
  private int scanType;
  
  public DefaultDecoderFactory() {}
  
  public DefaultDecoderFactory(Collection<BarcodeFormat> paramCollection)
  {
    this.decodeFormats = paramCollection;
  }
  
  public DefaultDecoderFactory(Collection<BarcodeFormat> paramCollection, Map<DecodeHintType, ?> paramMap, String paramString, int paramInt)
  {
    this.decodeFormats = paramCollection;
    this.hints = paramMap;
    this.characterSet = paramString;
    this.scanType = paramInt;
  }
  
  public Decoder createDecoder(Map<DecodeHintType, ?> paramMap)
  {
    EnumMap localEnumMap = new EnumMap(DecodeHintType.class);
    localEnumMap.putAll(paramMap);
    paramMap = this.hints;
    if (paramMap != null) {
      localEnumMap.putAll(paramMap);
    }
    if (this.decodeFormats != null) {
      localEnumMap.put(DecodeHintType.POSSIBLE_FORMATS, this.decodeFormats);
    }
    if (this.characterSet != null) {
      localEnumMap.put(DecodeHintType.CHARACTER_SET, this.characterSet);
    }
    paramMap = new MultiFormatReader();
    paramMap.setHints(localEnumMap);
    switch (this.scanType)
    {
    default: 
      return new Decoder(paramMap);
    case 2: 
      return new MixedDecoder(paramMap);
    case 1: 
      return new InvertedDecoder(paramMap);
    }
    return new Decoder(paramMap);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/DefaultDecoderFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */