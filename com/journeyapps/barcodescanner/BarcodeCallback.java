package com.journeyapps.barcodescanner;

import com.google.zxing.ResultPoint;
import java.util.List;

public abstract interface BarcodeCallback
{
  public abstract void barcodeResult(BarcodeResult paramBarcodeResult);
  
  public abstract void possibleResultPoints(List<ResultPoint> paramList);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/BarcodeCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */