package com.journeyapps.barcodescanner;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.common.HybridBinarizer;

public class InvertedDecoder
  extends Decoder
{
  public InvertedDecoder(Reader paramReader)
  {
    super(paramReader);
  }
  
  protected BinaryBitmap toBitmap(LuminanceSource paramLuminanceSource)
  {
    return new BinaryBitmap(new HybridBinarizer(paramLuminanceSource.invert()));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/InvertedDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */