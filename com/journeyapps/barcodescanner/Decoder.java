package com.journeyapps.barcodescanner;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.HybridBinarizer;
import java.util.ArrayList;
import java.util.List;

public class Decoder
  implements ResultPointCallback
{
  private List<ResultPoint> possibleResultPoints = new ArrayList();
  private Reader reader;
  
  public Decoder(Reader paramReader)
  {
    this.reader = paramReader;
  }
  
  /* Error */
  protected Result decode(BinaryBitmap paramBinaryBitmap)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 21	com/journeyapps/barcodescanner/Decoder:possibleResultPoints	Ljava/util/List;
    //   4: invokeinterface 33 1 0
    //   9: aload_0
    //   10: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   13: instanceof 35
    //   16: ifeq +26 -> 42
    //   19: aload_0
    //   20: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   23: checkcast 35	com/google/zxing/MultiFormatReader
    //   26: aload_1
    //   27: invokevirtual 38	com/google/zxing/MultiFormatReader:decodeWithState	(Lcom/google/zxing/BinaryBitmap;)Lcom/google/zxing/Result;
    //   30: astore_1
    //   31: aload_0
    //   32: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   35: invokeinterface 43 1 0
    //   40: aload_1
    //   41: areturn
    //   42: aload_0
    //   43: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   46: aload_1
    //   47: invokeinterface 45 2 0
    //   52: astore_1
    //   53: aload_0
    //   54: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   57: invokeinterface 43 1 0
    //   62: aload_1
    //   63: areturn
    //   64: astore_1
    //   65: aload_0
    //   66: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   69: invokeinterface 43 1 0
    //   74: aload_1
    //   75: athrow
    //   76: astore_1
    //   77: aload_0
    //   78: getfield 23	com/journeyapps/barcodescanner/Decoder:reader	Lcom/google/zxing/Reader;
    //   81: invokeinterface 43 1 0
    //   86: aconst_null
    //   87: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	88	0	this	Decoder
    //   0	88	1	paramBinaryBitmap	BinaryBitmap
    // Exception table:
    //   from	to	target	type
    //   9	31	64	finally
    //   42	53	64	finally
    //   9	31	76	java/lang/Exception
    //   42	53	76	java/lang/Exception
  }
  
  public Result decode(LuminanceSource paramLuminanceSource)
  {
    return decode(toBitmap(paramLuminanceSource));
  }
  
  public void foundPossibleResultPoint(ResultPoint paramResultPoint)
  {
    this.possibleResultPoints.add(paramResultPoint);
  }
  
  public List<ResultPoint> getPossibleResultPoints()
  {
    return new ArrayList(this.possibleResultPoints);
  }
  
  protected Reader getReader()
  {
    return this.reader;
  }
  
  protected BinaryBitmap toBitmap(LuminanceSource paramLuminanceSource)
  {
    return new BinaryBitmap(new HybridBinarizer(paramLuminanceSource));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/Decoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */