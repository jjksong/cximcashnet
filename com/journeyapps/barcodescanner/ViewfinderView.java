package com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.R.color;
import com.google.zxing.client.android.R.styleable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ViewfinderView
  extends View
{
  protected static final long ANIMATION_DELAY = 80L;
  protected static final int CURRENT_POINT_OPACITY = 160;
  protected static final int MAX_RESULT_POINTS = 20;
  protected static final int POINT_SIZE = 6;
  protected static final int[] SCANNER_ALPHA = { 0, 64, 128, 192, 255, 192, 128, 64 };
  protected static final String TAG = "ViewfinderView";
  protected CameraPreview cameraPreview;
  protected Rect framingRect;
  protected final int laserColor;
  protected List<ResultPoint> lastPossibleResultPoints;
  protected final int maskColor;
  protected final Paint paint = new Paint(1);
  protected List<ResultPoint> possibleResultPoints;
  protected Rect previewFramingRect;
  protected Bitmap resultBitmap;
  protected final int resultColor;
  protected final int resultPointColor;
  protected int scannerAlpha;
  
  public ViewfinderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getResources();
    paramAttributeSet = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.zxing_finder);
    this.maskColor = paramAttributeSet.getColor(R.styleable.zxing_finder_zxing_viewfinder_mask, paramContext.getColor(R.color.zxing_viewfinder_mask));
    this.resultColor = paramAttributeSet.getColor(R.styleable.zxing_finder_zxing_result_view, paramContext.getColor(R.color.zxing_result_view));
    this.laserColor = paramAttributeSet.getColor(R.styleable.zxing_finder_zxing_viewfinder_laser, paramContext.getColor(R.color.zxing_viewfinder_laser));
    this.resultPointColor = paramAttributeSet.getColor(R.styleable.zxing_finder_zxing_possible_result_points, paramContext.getColor(R.color.zxing_possible_result_points));
    paramAttributeSet.recycle();
    this.scannerAlpha = 0;
    this.possibleResultPoints = new ArrayList(20);
    this.lastPossibleResultPoints = new ArrayList(20);
  }
  
  public void addPossibleResultPoint(ResultPoint paramResultPoint)
  {
    if (this.possibleResultPoints.size() < 20) {
      this.possibleResultPoints.add(paramResultPoint);
    }
  }
  
  public void drawResultBitmap(Bitmap paramBitmap)
  {
    this.resultBitmap = paramBitmap;
    invalidate();
  }
  
  public void drawViewfinder()
  {
    Bitmap localBitmap = this.resultBitmap;
    this.resultBitmap = null;
    if (localBitmap != null) {
      localBitmap.recycle();
    }
    invalidate();
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    refreshSizes();
    Rect localRect = this.framingRect;
    if (localRect != null)
    {
      Object localObject2 = this.previewFramingRect;
      if (localObject2 != null)
      {
        int k = paramCanvas.getWidth();
        int j = paramCanvas.getHeight();
        Object localObject1 = this.paint;
        int i;
        if (this.resultBitmap != null) {
          i = this.resultColor;
        } else {
          i = this.maskColor;
        }
        ((Paint)localObject1).setColor(i);
        float f1 = k;
        paramCanvas.drawRect(0.0F, 0.0F, f1, localRect.top, this.paint);
        paramCanvas.drawRect(0.0F, localRect.top, localRect.left, localRect.bottom + 1, this.paint);
        paramCanvas.drawRect(localRect.right + 1, localRect.top, f1, localRect.bottom + 1, this.paint);
        paramCanvas.drawRect(0.0F, localRect.bottom + 1, f1, j, this.paint);
        if (this.resultBitmap != null)
        {
          this.paint.setAlpha(160);
          paramCanvas.drawBitmap(this.resultBitmap, null, localRect, this.paint);
        }
        else
        {
          this.paint.setColor(this.laserColor);
          this.paint.setAlpha(SCANNER_ALPHA[this.scannerAlpha]);
          this.scannerAlpha = ((this.scannerAlpha + 1) % SCANNER_ALPHA.length);
          i = localRect.height() / 2 + localRect.top;
          paramCanvas.drawRect(localRect.left + 2, i - 1, localRect.right - 1, i + 2, this.paint);
          f1 = localRect.width() / ((Rect)localObject2).width();
          float f2 = localRect.height() / ((Rect)localObject2).height();
          j = localRect.left;
          i = localRect.top;
          if (!this.lastPossibleResultPoints.isEmpty())
          {
            this.paint.setAlpha(80);
            this.paint.setColor(this.resultPointColor);
            localObject1 = this.lastPossibleResultPoints.iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (ResultPoint)((Iterator)localObject1).next();
              paramCanvas.drawCircle((int)(((ResultPoint)localObject2).getX() * f1) + j, (int)(((ResultPoint)localObject2).getY() * f2) + i, 3.0F, this.paint);
            }
            this.lastPossibleResultPoints.clear();
          }
          if (!this.possibleResultPoints.isEmpty())
          {
            this.paint.setAlpha(160);
            this.paint.setColor(this.resultPointColor);
            localObject1 = this.possibleResultPoints.iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (ResultPoint)((Iterator)localObject1).next();
              paramCanvas.drawCircle((int)(((ResultPoint)localObject2).getX() * f1) + j, (int)(((ResultPoint)localObject2).getY() * f2) + i, 6.0F, this.paint);
            }
            paramCanvas = this.possibleResultPoints;
            this.possibleResultPoints = this.lastPossibleResultPoints;
            this.lastPossibleResultPoints = paramCanvas;
            this.possibleResultPoints.clear();
          }
          postInvalidateDelayed(80L, localRect.left - 6, localRect.top - 6, localRect.right + 6, localRect.bottom + 6);
        }
        return;
      }
    }
  }
  
  protected void refreshSizes()
  {
    Object localObject = this.cameraPreview;
    if (localObject == null) {
      return;
    }
    localObject = ((CameraPreview)localObject).getFramingRect();
    Rect localRect = this.cameraPreview.getPreviewFramingRect();
    if ((localObject != null) && (localRect != null))
    {
      this.framingRect = ((Rect)localObject);
      this.previewFramingRect = localRect;
    }
  }
  
  public void setCameraPreview(CameraPreview paramCameraPreview)
  {
    this.cameraPreview = paramCameraPreview;
    paramCameraPreview.addStateListener(new CameraPreview.StateListener()
    {
      public void cameraClosed() {}
      
      public void cameraError(Exception paramAnonymousException) {}
      
      public void previewSized()
      {
        ViewfinderView.this.refreshSizes();
        ViewfinderView.this.invalidate();
      }
      
      public void previewStarted() {}
      
      public void previewStopped() {}
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/ViewfinderView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */