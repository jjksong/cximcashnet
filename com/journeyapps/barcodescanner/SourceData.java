package com.journeyapps.barcodescanner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import com.google.zxing.PlanarYUVLuminanceSource;
import java.io.ByteArrayOutputStream;

public class SourceData
{
  private Rect cropRect;
  private byte[] data;
  private int dataHeight;
  private int dataWidth;
  private int imageFormat;
  private int rotation;
  
  public SourceData(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.data = paramArrayOfByte;
    this.dataWidth = paramInt1;
    this.dataHeight = paramInt2;
    this.rotation = paramInt4;
    this.imageFormat = paramInt3;
    if (paramInt1 * paramInt2 <= paramArrayOfByte.length) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Image data does not match the resolution. ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append("x");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(" > ");
    localStringBuilder.append(paramArrayOfByte.length);
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  private Bitmap getBitmap(Rect paramRect, int paramInt)
  {
    Object localObject = paramRect;
    if (isRotated()) {
      localObject = new Rect(paramRect.top, paramRect.left, paramRect.bottom, paramRect.right);
    }
    YuvImage localYuvImage = new YuvImage(this.data, this.imageFormat, this.dataWidth, this.dataHeight, null);
    paramRect = new ByteArrayOutputStream();
    localYuvImage.compressToJpeg((Rect)localObject, 90, paramRect);
    paramRect = paramRect.toByteArray();
    localObject = new BitmapFactory.Options();
    ((BitmapFactory.Options)localObject).inSampleSize = paramInt;
    localObject = BitmapFactory.decodeByteArray(paramRect, 0, paramRect.length, (BitmapFactory.Options)localObject);
    paramRect = (Rect)localObject;
    if (this.rotation != 0)
    {
      paramRect = new Matrix();
      paramRect.postRotate(this.rotation);
      paramRect = Bitmap.createBitmap((Bitmap)localObject, 0, 0, ((Bitmap)localObject).getWidth(), ((Bitmap)localObject).getHeight(), paramRect, false);
    }
    return paramRect;
  }
  
  public static byte[] rotate180(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 * paramInt2;
    byte[] arrayOfByte = new byte[i];
    paramInt2 = i - 1;
    for (paramInt1 = 0; paramInt1 < i; paramInt1++)
    {
      arrayOfByte[paramInt2] = paramArrayOfByte[paramInt1];
      paramInt2--;
    }
    return arrayOfByte;
  }
  
  public static byte[] rotateCCW(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 * paramInt2;
    byte[] arrayOfByte = new byte[i];
    int k = i - 1;
    for (i = 0; i < paramInt1; i++) {
      for (int j = paramInt2 - 1; j >= 0; j--)
      {
        arrayOfByte[k] = paramArrayOfByte[(j * paramInt1 + i)];
        k--;
      }
    }
    return arrayOfByte;
  }
  
  public static byte[] rotateCW(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = new byte[paramInt1 * paramInt2];
    int i = 0;
    int k = 0;
    while (i < paramInt1)
    {
      for (int j = paramInt2 - 1; j >= 0; j--)
      {
        arrayOfByte[k] = paramArrayOfByte[(j * paramInt1 + i)];
        k++;
      }
      i++;
    }
    return arrayOfByte;
  }
  
  public static byte[] rotateCameraPreview(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    if (paramInt1 != 0)
    {
      if (paramInt1 != 90)
      {
        if (paramInt1 != 180)
        {
          if (paramInt1 != 270) {
            return paramArrayOfByte;
          }
          return rotateCCW(paramArrayOfByte, paramInt2, paramInt3);
        }
        return rotate180(paramArrayOfByte, paramInt2, paramInt3);
      }
      return rotateCW(paramArrayOfByte, paramInt2, paramInt3);
    }
    return paramArrayOfByte;
  }
  
  public PlanarYUVLuminanceSource createSource()
  {
    byte[] arrayOfByte = rotateCameraPreview(this.rotation, this.data, this.dataWidth, this.dataHeight);
    if (isRotated()) {
      return new PlanarYUVLuminanceSource(arrayOfByte, this.dataHeight, this.dataWidth, this.cropRect.left, this.cropRect.top, this.cropRect.width(), this.cropRect.height(), false);
    }
    return new PlanarYUVLuminanceSource(arrayOfByte, this.dataWidth, this.dataHeight, this.cropRect.left, this.cropRect.top, this.cropRect.width(), this.cropRect.height(), false);
  }
  
  public Bitmap getBitmap()
  {
    return getBitmap(1);
  }
  
  public Bitmap getBitmap(int paramInt)
  {
    return getBitmap(this.cropRect, paramInt);
  }
  
  public Rect getCropRect()
  {
    return this.cropRect;
  }
  
  public byte[] getData()
  {
    return this.data;
  }
  
  public int getDataHeight()
  {
    return this.dataHeight;
  }
  
  public int getDataWidth()
  {
    return this.dataWidth;
  }
  
  public int getImageFormat()
  {
    return this.imageFormat;
  }
  
  public boolean isRotated()
  {
    boolean bool;
    if (this.rotation % 180 != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void setCropRect(Rect paramRect)
  {
    this.cropRect = paramRect;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/SourceData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */