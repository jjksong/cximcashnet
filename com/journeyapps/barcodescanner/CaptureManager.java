package com.journeyapps.barcodescanner;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.google.zxing.client.android.InactivityTimer;
import com.google.zxing.client.android.R.string;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CaptureManager
{
  private static final String SAVED_ORIENTATION_LOCK = "SAVED_ORIENTATION_LOCK";
  private static final String TAG = "CaptureManager";
  private static int cameraPermissionReqCode = 250;
  private Activity activity;
  private boolean askedPermission = false;
  private DecoratedBarcodeView barcodeView;
  private BeepManager beepManager;
  private BarcodeCallback callback = new BarcodeCallback()
  {
    public void barcodeResult(final BarcodeResult paramAnonymousBarcodeResult)
    {
      CaptureManager.this.barcodeView.pause();
      CaptureManager.this.beepManager.playBeepSoundAndVibrate();
      CaptureManager.this.handler.post(new Runnable()
      {
        public void run()
        {
          CaptureManager.this.returnResult(paramAnonymousBarcodeResult);
        }
      });
    }
    
    public void possibleResultPoints(List<ResultPoint> paramAnonymousList) {}
  };
  private boolean destroyed = false;
  private boolean finishWhenClosed = false;
  private Handler handler;
  private InactivityTimer inactivityTimer;
  private int orientationLock = -1;
  private boolean returnBarcodeImagePath = false;
  private final CameraPreview.StateListener stateListener = new CameraPreview.StateListener()
  {
    public void cameraClosed()
    {
      if (CaptureManager.this.finishWhenClosed)
      {
        Log.d(CaptureManager.TAG, "Camera closed; finishing activity");
        CaptureManager.this.finish();
      }
    }
    
    public void cameraError(Exception paramAnonymousException)
    {
      CaptureManager.this.displayFrameworkBugMessageAndExit();
    }
    
    public void previewSized() {}
    
    public void previewStarted() {}
    
    public void previewStopped() {}
  };
  
  public CaptureManager(Activity paramActivity, DecoratedBarcodeView paramDecoratedBarcodeView)
  {
    this.activity = paramActivity;
    this.barcodeView = paramDecoratedBarcodeView;
    paramDecoratedBarcodeView.getBarcodeView().addStateListener(this.stateListener);
    this.handler = new Handler();
    this.inactivityTimer = new InactivityTimer(paramActivity, new Runnable()
    {
      public void run()
      {
        Log.d(CaptureManager.TAG, "Finishing due to inactivity");
        CaptureManager.this.finish();
      }
    });
    this.beepManager = new BeepManager(paramActivity);
  }
  
  private void finish()
  {
    this.activity.finish();
  }
  
  private String getBarcodeImagePath(BarcodeResult paramBarcodeResult)
  {
    if (this.returnBarcodeImagePath)
    {
      Object localObject1 = paramBarcodeResult.getBitmap();
      try
      {
        paramBarcodeResult = File.createTempFile("barcodeimage", ".jpg", this.activity.getCacheDir());
        localObject2 = new java/io/FileOutputStream;
        ((FileOutputStream)localObject2).<init>(paramBarcodeResult);
        ((Bitmap)localObject1).compress(Bitmap.CompressFormat.JPEG, 100, (OutputStream)localObject2);
        ((FileOutputStream)localObject2).close();
        paramBarcodeResult = paramBarcodeResult.getAbsolutePath();
      }
      catch (IOException paramBarcodeResult)
      {
        localObject1 = TAG;
        Object localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("Unable to create temporary file and store bitmap! ");
        ((StringBuilder)localObject2).append(paramBarcodeResult);
        Log.w((String)localObject1, ((StringBuilder)localObject2).toString());
      }
    }
    else
    {
      paramBarcodeResult = null;
    }
    return paramBarcodeResult;
  }
  
  public static int getCameraPermissionReqCode()
  {
    return cameraPermissionReqCode;
  }
  
  @TargetApi(23)
  private void openCameraWithPermission()
  {
    if (ContextCompat.checkSelfPermission(this.activity, "android.permission.CAMERA") == 0)
    {
      this.barcodeView.resume();
    }
    else if (!this.askedPermission)
    {
      Activity localActivity = this.activity;
      int i = cameraPermissionReqCode;
      ActivityCompat.requestPermissions(localActivity, new String[] { "android.permission.CAMERA" }, i);
      this.askedPermission = true;
    }
  }
  
  public static Intent resultIntent(BarcodeResult paramBarcodeResult, String paramString)
  {
    Intent localIntent = new Intent("com.google.zxing.client.android.SCAN");
    localIntent.addFlags(524288);
    localIntent.putExtra("SCAN_RESULT", paramBarcodeResult.toString());
    localIntent.putExtra("SCAN_RESULT_FORMAT", paramBarcodeResult.getBarcodeFormat().toString());
    Object localObject = paramBarcodeResult.getRawBytes();
    if ((localObject != null) && (localObject.length > 0)) {
      localIntent.putExtra("SCAN_RESULT_BYTES", (byte[])localObject);
    }
    paramBarcodeResult = paramBarcodeResult.getResultMetadata();
    if (paramBarcodeResult != null)
    {
      if (paramBarcodeResult.containsKey(ResultMetadataType.UPC_EAN_EXTENSION)) {
        localIntent.putExtra("SCAN_RESULT_UPC_EAN_EXTENSION", paramBarcodeResult.get(ResultMetadataType.UPC_EAN_EXTENSION).toString());
      }
      localObject = (Number)paramBarcodeResult.get(ResultMetadataType.ORIENTATION);
      if (localObject != null) {
        localIntent.putExtra("SCAN_RESULT_ORIENTATION", ((Number)localObject).intValue());
      }
      localObject = (String)paramBarcodeResult.get(ResultMetadataType.ERROR_CORRECTION_LEVEL);
      if (localObject != null) {
        localIntent.putExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL", (String)localObject);
      }
      paramBarcodeResult = (Iterable)paramBarcodeResult.get(ResultMetadataType.BYTE_SEGMENTS);
      if (paramBarcodeResult != null)
      {
        int i = 0;
        paramBarcodeResult = paramBarcodeResult.iterator();
        while (paramBarcodeResult.hasNext())
        {
          byte[] arrayOfByte = (byte[])paramBarcodeResult.next();
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("SCAN_RESULT_BYTE_SEGMENTS_");
          ((StringBuilder)localObject).append(i);
          localIntent.putExtra(((StringBuilder)localObject).toString(), arrayOfByte);
          i++;
        }
      }
    }
    if (paramString != null) {
      localIntent.putExtra("SCAN_RESULT_IMAGE_PATH", paramString);
    }
    return localIntent;
  }
  
  public static void setCameraPermissionReqCode(int paramInt)
  {
    cameraPermissionReqCode = paramInt;
  }
  
  protected void closeAndFinish()
  {
    if (this.barcodeView.getBarcodeView().isCameraClosed()) {
      finish();
    } else {
      this.finishWhenClosed = true;
    }
    this.barcodeView.pause();
    this.inactivityTimer.cancel();
  }
  
  public void decode()
  {
    this.barcodeView.decodeSingle(this.callback);
  }
  
  protected void displayFrameworkBugMessageAndExit()
  {
    if ((!this.activity.isFinishing()) && (!this.destroyed) && (!this.finishWhenClosed))
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.activity);
      localBuilder.setTitle(this.activity.getString(R.string.zxing_app_name));
      localBuilder.setMessage(this.activity.getString(R.string.zxing_msg_camera_framework_bug));
      localBuilder.setPositiveButton(R.string.zxing_button_ok, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          CaptureManager.this.finish();
        }
      });
      localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          CaptureManager.this.finish();
        }
      });
      localBuilder.show();
      return;
    }
  }
  
  public void initializeFromIntent(Intent paramIntent, Bundle paramBundle)
  {
    this.activity.getWindow().addFlags(128);
    if (paramBundle != null) {
      this.orientationLock = paramBundle.getInt("SAVED_ORIENTATION_LOCK", -1);
    }
    if (paramIntent != null)
    {
      if (paramIntent.getBooleanExtra("SCAN_ORIENTATION_LOCKED", true)) {
        lockOrientation();
      }
      if ("com.google.zxing.client.android.SCAN".equals(paramIntent.getAction())) {
        this.barcodeView.initializeFromIntent(paramIntent);
      }
      if (!paramIntent.getBooleanExtra("BEEP_ENABLED", true)) {
        this.beepManager.setBeepEnabled(false);
      }
      if (paramIntent.hasExtra("TIMEOUT"))
      {
        paramBundle = new Runnable()
        {
          public void run()
          {
            CaptureManager.this.returnResultTimeout();
          }
        };
        this.handler.postDelayed(paramBundle, paramIntent.getLongExtra("TIMEOUT", 0L));
      }
      if (paramIntent.getBooleanExtra("BARCODE_IMAGE_ENABLED", false)) {
        this.returnBarcodeImagePath = true;
      }
    }
  }
  
  protected void lockOrientation()
  {
    if (this.orientationLock == -1)
    {
      int m = this.activity.getWindowManager().getDefaultDisplay().getRotation();
      int k = this.activity.getResources().getConfiguration().orientation;
      int j = 0;
      int i;
      if (k == 2)
      {
        i = j;
        if (m != 0) {
          if (m == 1) {
            i = j;
          } else {
            i = 8;
          }
        }
      }
      else
      {
        i = j;
        if (k == 1) {
          if ((m != 0) && (m != 3)) {
            i = 9;
          } else {
            i = 1;
          }
        }
      }
      this.orientationLock = i;
    }
    this.activity.setRequestedOrientation(this.orientationLock);
  }
  
  public void onDestroy()
  {
    this.destroyed = true;
    this.inactivityTimer.cancel();
    this.handler.removeCallbacksAndMessages(null);
  }
  
  public void onPause()
  {
    this.inactivityTimer.cancel();
    this.barcodeView.pauseAndWait();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    if (paramInt == cameraPermissionReqCode) {
      if ((paramArrayOfInt.length > 0) && (paramArrayOfInt[0] == 0)) {
        this.barcodeView.resume();
      } else {
        displayFrameworkBugMessageAndExit();
      }
    }
  }
  
  public void onResume()
  {
    if (Build.VERSION.SDK_INT >= 23) {
      openCameraWithPermission();
    } else {
      this.barcodeView.resume();
    }
    this.inactivityTimer.start();
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putInt("SAVED_ORIENTATION_LOCK", this.orientationLock);
  }
  
  protected void returnResult(BarcodeResult paramBarcodeResult)
  {
    paramBarcodeResult = resultIntent(paramBarcodeResult, getBarcodeImagePath(paramBarcodeResult));
    this.activity.setResult(-1, paramBarcodeResult);
    closeAndFinish();
  }
  
  protected void returnResultTimeout()
  {
    Intent localIntent = new Intent("com.google.zxing.client.android.SCAN");
    localIntent.putExtra("TIMEOUT", true);
    this.activity.setResult(0, localIntent);
    closeAndFinish();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/CaptureManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */