package com.journeyapps.barcodescanner;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.client.android.R.id;
import com.journeyapps.barcodescanner.camera.CameraInstance;
import com.journeyapps.barcodescanner.camera.PreviewCallback;

public class DecoderThread
{
  private static final String TAG = "DecoderThread";
  private final Object LOCK = new Object();
  private final Handler.Callback callback = new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == R.id.zxing_decode) {
        DecoderThread.this.decode((SourceData)paramAnonymousMessage.obj);
      } else if (paramAnonymousMessage.what == R.id.zxing_preview_failed) {
        DecoderThread.this.requestNextPreview();
      }
      return true;
    }
  };
  private CameraInstance cameraInstance;
  private Rect cropRect;
  private Decoder decoder;
  private Handler handler;
  private final PreviewCallback previewCallback = new PreviewCallback()
  {
    public void onPreview(SourceData paramAnonymousSourceData)
    {
      synchronized (DecoderThread.this.LOCK)
      {
        if (DecoderThread.this.running) {
          DecoderThread.this.handler.obtainMessage(R.id.zxing_decode, paramAnonymousSourceData).sendToTarget();
        }
        return;
      }
    }
    
    public void onPreviewError(Exception arg1)
    {
      synchronized (DecoderThread.this.LOCK)
      {
        if (DecoderThread.this.running) {
          DecoderThread.this.handler.obtainMessage(R.id.zxing_preview_failed).sendToTarget();
        }
        return;
      }
    }
  };
  private Handler resultHandler;
  private boolean running = false;
  private HandlerThread thread;
  
  public DecoderThread(CameraInstance paramCameraInstance, Decoder paramDecoder, Handler paramHandler)
  {
    Util.validateMainThread();
    this.cameraInstance = paramCameraInstance;
    this.decoder = paramDecoder;
    this.resultHandler = paramHandler;
  }
  
  private void decode(SourceData paramSourceData)
  {
    long l1 = System.currentTimeMillis();
    paramSourceData.setCropRect(this.cropRect);
    Object localObject = createSource(paramSourceData);
    if (localObject != null) {
      localObject = this.decoder.decode((LuminanceSource)localObject);
    } else {
      localObject = null;
    }
    if (localObject != null)
    {
      long l2 = System.currentTimeMillis();
      String str = TAG;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Found barcode in ");
      localStringBuilder.append(l2 - l1);
      localStringBuilder.append(" ms");
      Log.d(str, localStringBuilder.toString());
      if (this.resultHandler != null)
      {
        paramSourceData = new BarcodeResult((Result)localObject, paramSourceData);
        paramSourceData = Message.obtain(this.resultHandler, R.id.zxing_decode_succeeded, paramSourceData);
        paramSourceData.setData(new Bundle());
        paramSourceData.sendToTarget();
      }
    }
    else
    {
      paramSourceData = this.resultHandler;
      if (paramSourceData != null) {
        Message.obtain(paramSourceData, R.id.zxing_decode_failed).sendToTarget();
      }
    }
    if (this.resultHandler != null)
    {
      paramSourceData = this.decoder.getPossibleResultPoints();
      Message.obtain(this.resultHandler, R.id.zxing_possible_result_points, paramSourceData).sendToTarget();
    }
    requestNextPreview();
  }
  
  private void requestNextPreview()
  {
    this.cameraInstance.requestPreview(this.previewCallback);
  }
  
  protected LuminanceSource createSource(SourceData paramSourceData)
  {
    if (this.cropRect == null) {
      return null;
    }
    return paramSourceData.createSource();
  }
  
  public Rect getCropRect()
  {
    return this.cropRect;
  }
  
  public Decoder getDecoder()
  {
    return this.decoder;
  }
  
  public void setCropRect(Rect paramRect)
  {
    this.cropRect = paramRect;
  }
  
  public void setDecoder(Decoder paramDecoder)
  {
    this.decoder = paramDecoder;
  }
  
  public void start()
  {
    Util.validateMainThread();
    this.thread = new HandlerThread(TAG);
    this.thread.start();
    this.handler = new Handler(this.thread.getLooper(), this.callback);
    this.running = true;
    requestNextPreview();
  }
  
  public void stop()
  {
    
    synchronized (this.LOCK)
    {
      this.running = false;
      this.handler.removeCallbacksAndMessages(null);
      this.thread.quit();
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/DecoderThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */