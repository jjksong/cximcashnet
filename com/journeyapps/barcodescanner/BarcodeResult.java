package com.journeyapps.barcodescanner;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import java.util.Map;

public class BarcodeResult
{
  private static final float PREVIEW_DOT_WIDTH = 10.0F;
  private static final float PREVIEW_LINE_WIDTH = 4.0F;
  protected Result mResult;
  private final int mScaleFactor = 2;
  protected SourceData sourceData;
  
  public BarcodeResult(Result paramResult, SourceData paramSourceData)
  {
    this.mResult = paramResult;
    this.sourceData = paramSourceData;
  }
  
  private static void drawLine(Canvas paramCanvas, Paint paramPaint, ResultPoint paramResultPoint1, ResultPoint paramResultPoint2, int paramInt)
  {
    if ((paramResultPoint1 != null) && (paramResultPoint2 != null))
    {
      float f2 = paramResultPoint1.getX();
      float f1 = paramInt;
      paramCanvas.drawLine(f2 / f1, paramResultPoint1.getY() / f1, paramResultPoint2.getX() / f1, paramResultPoint2.getY() / f1, paramPaint);
    }
  }
  
  public BarcodeFormat getBarcodeFormat()
  {
    return this.mResult.getBarcodeFormat();
  }
  
  public Bitmap getBitmap()
  {
    return this.sourceData.getBitmap(2);
  }
  
  public int getBitmapScaleFactor()
  {
    return 2;
  }
  
  public Bitmap getBitmapWithResultPoints(int paramInt)
  {
    Bitmap localBitmap2 = getBitmap();
    ResultPoint[] arrayOfResultPoint = this.mResult.getResultPoints();
    Bitmap localBitmap1 = localBitmap2;
    if (arrayOfResultPoint != null)
    {
      localBitmap1 = localBitmap2;
      if (arrayOfResultPoint.length > 0)
      {
        localBitmap1 = localBitmap2;
        if (localBitmap2 != null)
        {
          localBitmap1 = Bitmap.createBitmap(localBitmap2.getWidth(), localBitmap2.getHeight(), Bitmap.Config.ARGB_8888);
          Canvas localCanvas = new Canvas(localBitmap1);
          localCanvas.drawBitmap(localBitmap2, 0.0F, 0.0F, null);
          Paint localPaint = new Paint();
          localPaint.setColor(paramInt);
          int i = arrayOfResultPoint.length;
          paramInt = 0;
          if (i == 2)
          {
            localPaint.setStrokeWidth(4.0F);
            drawLine(localCanvas, localPaint, arrayOfResultPoint[0], arrayOfResultPoint[1], 2);
          }
          else if ((arrayOfResultPoint.length == 4) && ((this.mResult.getBarcodeFormat() == BarcodeFormat.UPC_A) || (this.mResult.getBarcodeFormat() == BarcodeFormat.EAN_13)))
          {
            drawLine(localCanvas, localPaint, arrayOfResultPoint[0], arrayOfResultPoint[1], 2);
            drawLine(localCanvas, localPaint, arrayOfResultPoint[2], arrayOfResultPoint[3], 2);
          }
          else
          {
            localPaint.setStrokeWidth(10.0F);
            i = arrayOfResultPoint.length;
            while (paramInt < i)
            {
              localBitmap2 = arrayOfResultPoint[paramInt];
              if (localBitmap2 != null) {
                localCanvas.drawPoint(localBitmap2.getX() / 2.0F, localBitmap2.getY() / 2.0F, localPaint);
              }
              paramInt++;
            }
          }
        }
      }
    }
    return localBitmap1;
  }
  
  public byte[] getRawBytes()
  {
    return this.mResult.getRawBytes();
  }
  
  public Result getResult()
  {
    return this.mResult;
  }
  
  public Map<ResultMetadataType, Object> getResultMetadata()
  {
    return this.mResult.getResultMetadata();
  }
  
  public ResultPoint[] getResultPoints()
  {
    return this.mResult.getResultPoints();
  }
  
  public String getText()
  {
    return this.mResult.getText();
  }
  
  public long getTimestamp()
  {
    return this.mResult.getTimestamp();
  }
  
  public String toString()
  {
    return this.mResult.getText();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/BarcodeResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */