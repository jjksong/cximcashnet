package com.journeyapps.barcodescanner;

import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;

public class DecoderResultPointCallback
  implements ResultPointCallback
{
  private Decoder decoder;
  
  public DecoderResultPointCallback() {}
  
  public DecoderResultPointCallback(Decoder paramDecoder)
  {
    this.decoder = paramDecoder;
  }
  
  public void foundPossibleResultPoint(ResultPoint paramResultPoint)
  {
    Decoder localDecoder = this.decoder;
    if (localDecoder != null) {
      localDecoder.foundPossibleResultPoint(paramResultPoint);
    }
  }
  
  public Decoder getDecoder()
  {
    return this.decoder;
  }
  
  public void setDecoder(Decoder paramDecoder)
  {
    this.decoder = paramDecoder;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/journeyapps/barcodescanner/DecoderResultPointCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */