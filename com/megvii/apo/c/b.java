package com.megvii.apo.c;

import android.os.Handler;
import android.os.HandlerThread;
import com.megvii.apo.util.e;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class b
{
  CopyOnWriteArrayList<d> a;
  CopyOnWriteArrayList<d> b;
  CopyOnWriteArrayList<d> c;
  public long d;
  public Handler e;
  public Runnable f = new Runnable()
  {
    public final void run()
    {
      b localb = b.this;
      try
      {
        localb.c.clear();
        d locald;
        if (localb.a.size() > 0)
        {
          ??? = localb.a.iterator();
          while (((Iterator)???).hasNext())
          {
            locald = (d)((Iterator)???).next();
            long l1 = System.currentTimeMillis();
            long l2 = locald.a;
            int i;
            if (locald.b * 60000L > l1 - l2) {
              i = 0;
            } else {
              i = 1;
            }
            if (i != 0)
            {
              localb.c.add(locald);
              localb.b.add(locald);
            }
          }
        }
        synchronized (localb.a)
        {
          localb.a.removeAll(localb.c);
          if (!localb.b.isEmpty())
          {
            localb.c.clear();
            ??? = localb.b.iterator();
            while (((Iterator)???).hasNext())
            {
              locald = (d)((Iterator)???).next();
              locald.a = System.currentTimeMillis();
              locald.c.a();
              localb.c.add(locald);
              locald.a = System.currentTimeMillis();
              localb.a.add(locald);
            }
            synchronized (localb.b)
            {
              localb.b.removeAll(localb.c);
            }
          }
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        e.a(localThrowable);
        if (b.this.d > 0L) {
          b.this.e.postDelayed(this, b.this.d * 60000L);
        }
      }
    }
  };
  private HandlerThread g;
  
  private b()
  {
    try
    {
      Object localObject = new java/util/concurrent/CopyOnWriteArrayList;
      ((CopyOnWriteArrayList)localObject).<init>();
      this.a = ((CopyOnWriteArrayList)localObject);
      localObject = new java/util/concurrent/CopyOnWriteArrayList;
      ((CopyOnWriteArrayList)localObject).<init>();
      this.b = ((CopyOnWriteArrayList)localObject);
      localObject = new java/util/concurrent/CopyOnWriteArrayList;
      ((CopyOnWriteArrayList)localObject).<init>();
      this.c = ((CopyOnWriteArrayList)localObject);
      localObject = new android/os/HandlerThread;
      ((HandlerThread)localObject).<init>("di");
      this.g = ((HandlerThread)localObject);
      this.g.start();
      localObject = new android/os/Handler;
      ((Handler)localObject).<init>(this.g.getLooper());
      this.e = ((Handler)localObject);
      return;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
  }
  
  public final b a(d paramd)
  {
    try
    {
      this.b.add(paramd);
    }
    catch (Throwable paramd)
    {
      e.a(paramd);
    }
    return this;
  }
  
  public static final class a
  {
    private static final b a = new b((byte)0);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */