package com.megvii.apo;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.megvii.apo.util.b;
import com.megvii.apo.util.c;
import com.megvii.apo.util.d;
import com.megvii.apo.util.e;
import com.megvii.apo.util.f;
import com.megvii.apo.util.j;
import com.megvii.apo.util.k;
import java.util.Map;

public final class o
  extends n
{
  public o(Context paramContext)
  {
    super(paramContext);
  }
  
  private String b()
  {
    try
    {
      PackageInfo localPackageInfo = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(localPackageInfo.versionCode);
      localObject = ((StringBuilder)localObject).toString();
      return (String)localObject;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return "";
  }
  
  private String c()
  {
    try
    {
      String str = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0).versionName;
      return str;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return "";
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.g != 1) {
      return;
    }
    try
    {
      String str = (String)k.b(this.a, "p_z_d", "");
      Object localObject = str;
      if (TextUtils.isEmpty(str))
      {
        localObject = b.a();
        k.a(this.a, "p_z_d", localObject);
      }
      paramMap.put("101000001", localObject);
      paramMap.put("101000002", Build.BRAND);
      paramMap.put("101000003", Build.MODEL);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Android_");
      ((StringBuilder)localObject).append(Build.VERSION.RELEASE);
      paramMap.put("101000004", ((StringBuilder)localObject).toString());
      localObject = "";
      switch (f.a(this.a))
      {
      default: 
        break;
      case 4: 
        localObject = "4G";
        break;
      case 3: 
        localObject = "3G";
        break;
      case 2: 
        localObject = "2G";
        break;
      case 1: 
        localObject = "WIFI";
      }
      paramMap.put("101000005", localObject);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject = "";
      } else {
        localObject = String.valueOf(a(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getIpAddress()));
      }
      paramMap.put("101000006", localObject);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(c.a("iwYV0WxGEEDoRuNyvd0xYQ=="));
      ((StringBuilder)localObject).append("_1.0");
      paramMap.put("101000007", ((StringBuilder)localObject).toString());
      paramMap.put("101000008", "");
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(System.currentTimeMillis());
      paramMap.put("101000009", ((StringBuilder)localObject).toString());
      paramMap.put("101000010", this.a.getPackageName());
      paramMap.put("101000011", b());
      paramMap.put("101000012", c());
      paramMap.put("101000013", this.a.getPackageName());
      paramMap.put("101000014", "0");
      str = d.b();
      localObject = str;
      if (TextUtils.isEmpty(str))
      {
        localObject = b.a();
        d.a((String)localObject);
      }
      paramMap.put("101000015", localObject);
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */