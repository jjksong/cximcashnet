package com.megvii.apo.b;

import android.content.Context;
import android.text.TextUtils;
import com.megvii.apo.util.e;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

public final class a
{
  byte[] a = new byte[' '];
  private Context b;
  private HttpURLConnection c;
  private String d;
  private String e;
  private int f = 120000;
  private int g = 120000;
  
  public a(Context paramContext)
  {
    this.b = paramContext.getApplicationContext();
  }
  
  public static boolean a(String paramString)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      boolean bool = localJSONObject.getString("code").equals("200");
      if (bool) {
        return true;
      }
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
    }
    return false;
  }
  
  private byte[] a(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    for (;;)
    {
      int i = paramInputStream.read(this.a);
      if (i == -1) {
        break;
      }
      localByteArrayOutputStream.write(this.a, 0, i);
    }
    paramInputStream = localByteArrayOutputStream.toByteArray();
    localByteArrayOutputStream.close();
    return paramInputStream;
  }
  
  public final String a(String paramString1, String paramString2, Map<String, String> paramMap, String paramString3)
  {
    try
    {
      this.d = "POST";
      this.e = paramString1;
      if ((!TextUtils.isEmpty(this.d)) && (!TextUtils.isEmpty(this.e)))
      {
        if ((!this.d.equals("GET")) && (!this.d.equals("POST"))) {
          this.d = "POST";
        }
        paramString1 = new java/net/URL;
        paramString1.<init>(this.e);
        HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramString1.openConnection();
        if (this.e.startsWith("https"))
        {
          paramString1 = (HttpsURLConnection)localHttpURLConnection;
          paramString1.setSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
          paramString1.setHostnameVerifier(HttpsURLConnection.getDefaultHostnameVerifier());
        }
        localHttpURLConnection.setRequestMethod(this.d);
        localHttpURLConnection.setDoInput(true);
        if ("POST".equals(this.d)) {
          localHttpURLConnection.setDoOutput(true);
        }
        localHttpURLConnection.setInstanceFollowRedirects(true);
        localHttpURLConnection.setConnectTimeout(this.f);
        localHttpURLConnection.setReadTimeout(this.g);
        localHttpURLConnection.setRequestProperty("Pragma", "no-cache");
        localHttpURLConnection.setRequestProperty("Accept", "*/*");
        localHttpURLConnection.setRequestProperty("Content-Type", "application/json");
        paramString1 = localHttpURLConnection;
        if (paramMap != null)
        {
          paramString1 = localHttpURLConnection;
          if (!paramMap.isEmpty())
          {
            Iterator localIterator = paramMap.keySet().iterator();
            for (;;)
            {
              paramString1 = localHttpURLConnection;
              if (!localIterator.hasNext()) {
                break;
              }
              paramString1 = (String)localIterator.next();
              localHttpURLConnection.setRequestProperty(paramString1, (String)paramMap.get(paramString1));
            }
          }
        }
      }
      else
      {
        paramString1 = null;
      }
      this.c = paramString1;
      if (this.c == null)
      {
        paramString1 = null;
      }
      else if (paramString2 == null)
      {
        paramString1 = this.c.getInputStream();
      }
      else
      {
        paramString1 = new java/io/BufferedOutputStream;
        paramString1.<init>(this.c.getOutputStream());
        paramString1.write(paramString2.getBytes());
        paramString1.flush();
        paramString1.close();
        int i = this.c.getResponseCode();
        paramString1 = new java/lang/StringBuilder;
        paramString1.<init>("net code : ");
        paramString1.append(i);
        e.a(paramString1.toString());
        paramString1 = this.c.getInputStream();
      }
      if (paramString1 == null)
      {
        if (paramString1 != null) {}
        try
        {
          paramString1.close();
          if (this.c != null)
          {
            this.c.disconnect();
            this.c = null;
          }
        }
        catch (Throwable paramString1)
        {
          e.a(paramString1);
        }
        return null;
      }
      if (paramString1 == null) {
        paramString2 = null;
      }
      try
      {
        paramString2 = a(paramString1);
        if (paramString2 == null) {
          paramString2 = null;
        } else if (!TextUtils.isEmpty(paramString3)) {
          paramString2 = new String(com.megvii.apo.util.a.a(paramString2, paramString3.getBytes()));
        } else {
          paramString2 = new String(paramString2);
        }
        if (paramString1 != null) {}
        try
        {
          paramString1.close();
          if (this.c != null)
          {
            this.c.disconnect();
            this.c = null;
          }
        }
        catch (Throwable paramString1)
        {
          e.a(paramString1);
        }
        return paramString2;
      }
      finally {}
      if (paramString1 == null) {}
    }
    finally
    {
      paramString1 = null;
    }
    try
    {
      paramString1.close();
      if (this.c != null)
      {
        this.c.disconnect();
        this.c = null;
      }
    }
    catch (Throwable paramString1)
    {
      e.a(paramString1);
    }
    throw paramString2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */