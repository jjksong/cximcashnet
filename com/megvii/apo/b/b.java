package com.megvii.apo.b;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public final class b
{
  private static final char[] d = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  public final byte[] a = "Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes();
  public String b = null;
  public ByteArrayOutputStream c = new ByteArrayOutputStream();
  private final String e = "\r\n";
  private final String f = "Content-Type: ";
  private final String g = "Content-Disposition: ";
  private final String h = "text/plain";
  private final String i = "application/octet-stream";
  private final byte[] j = "Content-Transfer-Encoding: binary\r\n\r\n".getBytes();
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
      }
      catch (IOException paramCloseable)
      {
        paramCloseable.printStackTrace();
        return;
      }
    }
  }
  
  public static byte[] a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    StringBuilder localStringBuilder2 = new StringBuilder("Content-Disposition: form-data; name=\"");
    localStringBuilder2.append(paramString1);
    localStringBuilder2.append("\"");
    localStringBuilder1.append(localStringBuilder2.toString());
    if ((paramString2 != null) && (!"".equals(paramString2)))
    {
      paramString1 = new StringBuilder("; filename=\"");
      paramString1.append(paramString2);
      paramString1.append("\"");
      localStringBuilder1.append(paramString1.toString());
    }
    localStringBuilder1.append("\r\n");
    return localStringBuilder1.toString().getBytes();
  }
  
  private static String c()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int k = 0; k < 30; k++)
    {
      char[] arrayOfChar = d;
      localStringBuffer.append(arrayOfChar[localRandom.nextInt(arrayOfChar.length)]);
    }
    return localStringBuffer.toString();
  }
  
  public final void a()
  {
    ByteArrayOutputStream localByteArrayOutputStream = this.c;
    StringBuilder localStringBuilder = new StringBuilder("--");
    localStringBuilder.append(this.b);
    localStringBuilder.append("\r\n");
    localByteArrayOutputStream.write(localStringBuilder.toString().getBytes());
  }
  
  public final byte[] b()
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      localByteArrayOutputStream.write(this.c.toByteArray());
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/b/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */