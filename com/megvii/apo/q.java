package com.megvii.apo;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import com.megvii.apo.util.k;
import java.util.Map;

public final class q
  extends n
{
  private a A;
  String c;
  String d;
  String e;
  String f;
  String g;
  String h;
  String i;
  String j;
  String k;
  String l;
  String m;
  private SensorManager n;
  private Sensor o;
  private Sensor p;
  private Sensor q;
  private Sensor r;
  private Sensor s;
  private Sensor t;
  private Sensor u;
  private Sensor v;
  private Sensor w;
  private Sensor x;
  private Sensor y;
  private int z;
  
  public q(Context paramContext)
  {
    super(paramContext);
    try
    {
      this.n = ((SensorManager)this.a.getSystemService("sensor"));
      paramContext = new com/megvii/apo/q$a;
      paramContext.<init>(this);
      this.A = paramContext;
      this.o = this.n.getDefaultSensor(5);
      if (b("_lt"))
      {
        a("_lt", false);
        this.n.registerListener(this.A, this.o, 3);
      }
      this.p = this.n.getDefaultSensor(8);
      if (b("_px"))
      {
        a("_px", false);
        this.n.registerListener(this.A, this.p, 3);
      }
      this.q = this.n.getDefaultSensor(2);
      if (b("_mgt"))
      {
        a("_mgt", false);
        this.n.registerListener(this.A, this.q, 3);
      }
      this.r = this.n.getDefaultSensor(3);
      if (b("_ori")) {
        a("_ori", false);
      }
      this.n.registerListener(this.A, this.r, 3);
      this.s = this.n.getDefaultSensor(4);
      if (b("_gyro"))
      {
        a("_gyro", false);
        this.n.registerListener(this.A, this.s, 3);
      }
      this.t = this.n.getDefaultSensor(9);
      if (b("_gty"))
      {
        a("_gty", false);
        this.n.registerListener(this.A, this.t, 3);
      }
      this.u = this.n.getDefaultSensor(10);
      if (b("_lacc"))
      {
        a("_lacc", false);
        this.n.registerListener(this.A, this.u, 3);
      }
      this.v = this.n.getDefaultSensor(11);
      if (b("_rota"))
      {
        a("_rota", false);
        this.n.registerListener(this.A, this.v, 3);
      }
      this.w = this.n.getDefaultSensor(20);
      if (b("_geo"))
      {
        a("_geo", false);
        this.n.registerListener(this.A, this.w, 3);
      }
      this.x = this.n.getDefaultSensor(1);
      if (b("_acc"))
      {
        a("_acc", false);
        this.n.registerListener(this.A, this.x, 3);
      }
      this.y = this.n.getDefaultSensor(13);
      if (b("_tem"))
      {
        a("_tem", false);
        this.n.registerListener(this.A, this.y, 3);
      }
      return;
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
    }
  }
  
  private void a(String paramString, boolean paramBoolean)
  {
    k.a(this.a, paramString, Boolean.valueOf(paramBoolean));
  }
  
  private double b()
  {
    try
    {
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>("android.intent.action.BATTERY_CHANGED");
      this.z = (this.a.registerReceiver(null, localIntentFilter).getIntExtra("temperature", 0) / 10);
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return this.z;
  }
  
  private boolean b(String paramString)
  {
    return ((Boolean)k.b(this.a, paramString, Boolean.valueOf(true))).booleanValue();
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.K != 1) {
      return;
    }
    try
    {
      Object localObject;
      if (this.o == null)
      {
        localObject = "";
      }
      else
      {
        a("_lt", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.o.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.c);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090001", localObject);
      if (this.p == null)
      {
        localObject = "";
      }
      else
      {
        a("_px", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.p.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.d);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090002", localObject);
      if (this.q == null)
      {
        localObject = "";
      }
      else
      {
        a("_mgt", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.q.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.e);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090003", localObject);
      if (this.r == null)
      {
        localObject = "";
      }
      else
      {
        a("_ori", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.r.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.f);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090004", localObject);
      if (this.s == null)
      {
        localObject = "";
      }
      else
      {
        a("_gyro", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.s.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.g);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090005", localObject);
      if (this.t == null)
      {
        localObject = "";
      }
      else
      {
        a("_gty", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.t.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.h);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090006", localObject);
      if (this.u == null)
      {
        localObject = "";
      }
      else
      {
        a("_lacc", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.u.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.i);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090007", localObject);
      if (this.v == null)
      {
        localObject = "";
      }
      else
      {
        a("_rota", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.v.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.j);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090008", localObject);
      if (this.w == null)
      {
        localObject = "";
      }
      else
      {
        a("_geo", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.w.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.k);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090009", localObject);
      if (this.x == null)
      {
        localObject = "";
      }
      else
      {
        a("_acc", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.x.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.l);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090011", localObject);
      if (this.y == null)
      {
        localObject = "";
      }
      else
      {
        a("_tem", true);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(this.y.getVendor());
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(this.m);
        localObject = ((StringBuilder)localObject).toString();
      }
      paramMap.put("101090013", localObject);
      paramMap.put("101090014", Double.valueOf(b()));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
  
  final class a
    implements SensorEventListener
  {
    a() {}
    
    public final void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
    
    public final void onSensorChanged(SensorEvent paramSensorEvent)
    {
      try
      {
        int i = paramSensorEvent.sensor.getType();
        Object localObject1;
        Object localObject2;
        if (i != 13)
        {
          if (i != 20)
          {
            switch (i)
            {
            default: 
              switch (i)
              {
              default: 
                break;
              case 11: 
                localObject1 = q.this;
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[0]));
                ((StringBuilder)localObject2).append(",");
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[1]));
                ((StringBuilder)localObject2).append(",");
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
                ((q)localObject1).j = ((StringBuilder)localObject2).toString();
                return;
              case 10: 
                localObject1 = q.this;
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[0]));
                ((StringBuilder)localObject2).append(",");
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[1]));
                ((StringBuilder)localObject2).append(",");
                ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
                ((q)localObject1).i = ((StringBuilder)localObject2).toString();
                return;
              case 9: 
                localObject2 = q.this;
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[0]));
                ((StringBuilder)localObject1).append(",");
                ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[1]));
                ((StringBuilder)localObject1).append(",");
                ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[2]));
                ((q)localObject2).h = ((StringBuilder)localObject1).toString();
                return;
              case 8: 
                q.this.d = String.valueOf(paramSensorEvent.values[0]);
                return;
              }
              break;
            case 5: 
              q.this.c = String.valueOf(paramSensorEvent.values[0]);
              return;
            case 4: 
              localObject1 = q.this;
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[0]));
              ((StringBuilder)localObject2).append(",");
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[1]));
              ((StringBuilder)localObject2).append(",");
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
              ((q)localObject1).g = ((StringBuilder)localObject2).toString();
              return;
            case 3: 
              localObject2 = q.this;
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[0]));
              ((StringBuilder)localObject1).append(",");
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[1]));
              ((StringBuilder)localObject1).append(",");
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[2]));
              ((q)localObject2).f = ((StringBuilder)localObject1).toString();
              return;
            case 2: 
              localObject2 = q.this;
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[0]));
              ((StringBuilder)localObject1).append(",");
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[1]));
              ((StringBuilder)localObject1).append(",");
              ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[2]));
              ((q)localObject2).e = ((StringBuilder)localObject1).toString();
              return;
            case 1: 
              localObject1 = q.this;
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[0]));
              ((StringBuilder)localObject2).append(",");
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[1]));
              ((StringBuilder)localObject2).append(",");
              ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
              ((q)localObject1).l = ((StringBuilder)localObject2).toString();
              return;
            }
          }
          else
          {
            localObject1 = q.this;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[0]));
            ((StringBuilder)localObject2).append(",");
            ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[1]));
            ((StringBuilder)localObject2).append(",");
            ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
            ((q)localObject1).k = ((StringBuilder)localObject2).toString();
          }
        }
        else
        {
          q localq = q.this;
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append(String.valueOf(paramSensorEvent.values[0]));
          ((StringBuilder)localObject1).append(",");
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(paramSensorEvent.values[1]);
          ((StringBuilder)localObject2).append(",");
          ((StringBuilder)localObject2).append(String.valueOf(paramSensorEvent.values[2]));
          ((StringBuilder)localObject1).append(String.valueOf(((StringBuilder)localObject2).toString()));
          localq.m = ((StringBuilder)localObject1).toString();
        }
        return;
      }
      catch (Throwable paramSensorEvent)
      {
        e.a(paramSensorEvent);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */