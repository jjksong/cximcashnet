package com.megvii.apo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.io.File;
import java.io.FileFilter;
import java.util.Map;
import java.util.regex.Pattern;

public final class h
  extends n
{
  public h(Context paramContext)
  {
    super(paramContext);
  }
  
  private String b()
  {
    try
    {
      Object localObject1 = new java/io/File;
      ((File)localObject1).<init>(c.a("2TfER6mCjFXQ6s36dvL3IOW+Mj28jQ19kKkBvmgEHD8="));
      Object localObject2 = new com/megvii/apo/h$a;
      ((a)localObject2).<init>(this);
      localObject2 = ((File)localObject1).listFiles((FileFilter)localObject2);
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(localObject2.length);
      localObject1 = ((StringBuilder)localObject1).toString();
      return (String)localObject1;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return "1";
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.j != 1) {
      return;
    }
    try
    {
      paramMap.put("101010001", Build.MANUFACTURER);
      paramMap.put("101010002", Build.MODEL);
      paramMap.put("101010003", Build.BRAND);
      paramMap.put("101010004", Build.BOARD);
      paramMap.put("101010005", Build.DEVICE);
      paramMap.put("101010006", Build.HARDWARE);
      paramMap.put("101010007", Build.PRODUCT);
      Object localObject1 = this.a.getResources().getDisplayMetrics();
      int i = ((DisplayMetrics)localObject1).widthPixels;
      int j = ((DisplayMetrics)localObject1).heightPixels;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(i);
      ((StringBuilder)localObject1).append("x");
      ((StringBuilder)localObject1).append(j);
      paramMap.put("101020001", ((StringBuilder)localObject1).toString());
      localObject1 = this.a.getResources().getDisplayMetrics();
      i = ((DisplayMetrics)localObject1).widthPixels;
      j = ((DisplayMetrics)localObject1).heightPixels;
      double d1 = i / ((DisplayMetrics)localObject1).xdpi;
      double d2 = j / ((DisplayMetrics)localObject1).ydpi;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(d1);
      ((StringBuilder)localObject1).append("x");
      ((StringBuilder)localObject1).append(d2);
      paramMap.put("101020002", ((StringBuilder)localObject1).toString());
      paramMap.put("101050001", Build.HARDWARE);
      paramMap.put("101050002", Build.CPU_ABI);
      paramMap.put("101050005", b());
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(Runtime.getRuntime().availableProcessors());
      paramMap.put("101050008", ((StringBuilder)localObject1).toString());
      Object localObject2;
      if (Build.VERSION.SDK_INT > 17)
      {
        localObject1 = (WindowManager)this.a.getSystemService("window");
        localObject2 = new android/util/DisplayMetrics;
        ((DisplayMetrics)localObject2).<init>();
        ((WindowManager)localObject1).getDefaultDisplay().getRealMetrics((DisplayMetrics)localObject2);
        j = ((DisplayMetrics)localObject2).widthPixels;
        i = ((DisplayMetrics)localObject2).heightPixels;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(j);
        ((StringBuilder)localObject1).append("x");
        ((StringBuilder)localObject1).append(i);
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      else
      {
        localObject1 = "";
      }
      paramMap.put("101020008", localObject1);
      if (Build.VERSION.SDK_INT > 17)
      {
        localObject2 = (WindowManager)this.a.getSystemService("window");
        localObject1 = new android/graphics/Point;
        ((Point)localObject1).<init>();
        ((WindowManager)localObject2).getDefaultDisplay().getRealSize((Point)localObject1);
        i = ((Point)localObject1).x;
        j = ((Point)localObject1).y;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(i);
        ((StringBuilder)localObject1).append("x");
        ((StringBuilder)localObject1).append(j);
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      else
      {
        localObject1 = "";
      }
      paramMap.put("101020009", localObject1);
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
  
  final class a
    implements FileFilter
  {
    a() {}
    
    public final boolean accept(File paramFile)
    {
      return Pattern.matches("cpu[0-9]", paramFile.getName());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */