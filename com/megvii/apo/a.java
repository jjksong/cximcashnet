package com.megvii.apo;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public final class a
  extends n
{
  public a(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  private static String b()
  {
    // Byte code:
    //   0: ldc 15
    //   2: astore_1
    //   3: new 17	java/io/FileInputStream
    //   6: dup
    //   7: ldc 19
    //   9: invokestatic 25	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   12: invokespecial 28	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   15: astore 4
    //   17: new 30	java/io/BufferedReader
    //   20: dup
    //   21: new 32	java/io/InputStreamReader
    //   24: dup
    //   25: aload 4
    //   27: invokespecial 35	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   30: sipush 8192
    //   33: invokespecial 38	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   36: astore_3
    //   37: ldc 15
    //   39: astore_0
    //   40: aload_0
    //   41: astore_2
    //   42: aload_3
    //   43: invokevirtual 41	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   46: astore 5
    //   48: aload 5
    //   50: ifnull +32 -> 82
    //   53: new 43	java/lang/StringBuilder
    //   56: astore_0
    //   57: aload_0
    //   58: invokespecial 46	java/lang/StringBuilder:<init>	()V
    //   61: aload_0
    //   62: aload_2
    //   63: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_0
    //   68: aload 5
    //   70: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload_0
    //   75: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   78: astore_0
    //   79: goto -39 -> 40
    //   82: aload_3
    //   83: invokevirtual 56	java/io/BufferedReader:close	()V
    //   86: aload 4
    //   88: invokevirtual 59	java/io/InputStream:close	()V
    //   91: goto +29 -> 120
    //   94: astore_0
    //   95: aload_0
    //   96: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   99: goto +21 -> 120
    //   102: astore_0
    //   103: goto +70 -> 173
    //   106: astore_0
    //   107: aload_0
    //   108: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   111: aload_3
    //   112: invokevirtual 56	java/io/BufferedReader:close	()V
    //   115: aload 4
    //   117: invokevirtual 59	java/io/InputStream:close	()V
    //   120: aload_1
    //   121: astore_0
    //   122: aload_2
    //   123: ldc 15
    //   125: if_acmpeq +46 -> 171
    //   128: ldc 66
    //   130: invokestatic 25	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   133: astore_0
    //   134: aload_2
    //   135: aload_2
    //   136: aload_0
    //   137: invokevirtual 72	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   140: aload_0
    //   141: invokevirtual 76	java/lang/String:length	()I
    //   144: iadd
    //   145: invokevirtual 80	java/lang/String:substring	(I)Ljava/lang/String;
    //   148: astore_0
    //   149: aload_0
    //   150: iconst_0
    //   151: aload_0
    //   152: ldc 82
    //   154: invokevirtual 72	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   157: invokevirtual 85	java/lang/String:substring	(II)Ljava/lang/String;
    //   160: astore_0
    //   161: goto +10 -> 171
    //   164: astore_0
    //   165: aload_0
    //   166: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   169: aload_1
    //   170: astore_0
    //   171: aload_0
    //   172: areturn
    //   173: aload_3
    //   174: invokevirtual 56	java/io/BufferedReader:close	()V
    //   177: aload 4
    //   179: invokevirtual 59	java/io/InputStream:close	()V
    //   182: goto +8 -> 190
    //   185: astore_1
    //   186: aload_1
    //   187: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   190: aload_0
    //   191: athrow
    //   192: astore_0
    //   193: aload_0
    //   194: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   197: ldc 15
    //   199: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   39	40	0	localObject1	Object
    //   94	2	0	localThrowable1	Throwable
    //   102	1	0	localObject2	Object
    //   106	2	0	localThrowable2	Throwable
    //   121	40	0	str1	String
    //   164	2	0	localThrowable3	Throwable
    //   170	21	0	str2	String
    //   192	2	0	localThrowable4	Throwable
    //   2	168	1	str3	String
    //   185	2	1	localThrowable5	Throwable
    //   41	95	2	localObject3	Object
    //   36	138	3	localBufferedReader	java.io.BufferedReader
    //   15	163	4	localFileInputStream	java.io.FileInputStream
    //   46	23	5	str4	String
    // Exception table:
    //   from	to	target	type
    //   82	91	94	java/lang/Throwable
    //   111	120	94	java/lang/Throwable
    //   42	48	102	finally
    //   53	79	102	finally
    //   107	111	102	finally
    //   42	48	106	java/lang/Throwable
    //   53	79	106	java/lang/Throwable
    //   128	161	164	java/lang/Throwable
    //   173	182	185	java/lang/Throwable
    //   3	17	192	java/lang/Throwable
  }
  
  private static boolean b(String paramString)
  {
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(paramString);
      boolean bool = localFile.exists();
      if (bool) {
        return true;
      }
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
    }
    return false;
  }
  
  private String c()
  {
    Object localObject = "";
    try
    {
      String str = this.a.getPackageManager().getPackageInfo(c.a("JASFBHiha0VMGUCGLGDV2ywPazWLritWnQyxd5r3zh0="), 0).versionName;
      localObject = str;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return (String)localObject;
  }
  
  /* Error */
  private static String d()
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 68	java/lang/String
    //   4: astore_2
    //   5: aload_2
    //   6: iconst_0
    //   7: ldc 15
    //   9: aastore
    //   10: aconst_null
    //   11: astore 4
    //   13: aconst_null
    //   14: astore_3
    //   15: aload_3
    //   16: astore_0
    //   17: new 30	java/io/BufferedReader
    //   20: astore_1
    //   21: aload_3
    //   22: astore_0
    //   23: new 32	java/io/InputStreamReader
    //   26: astore 5
    //   28: aload_3
    //   29: astore_0
    //   30: new 17	java/io/FileInputStream
    //   33: astore 6
    //   35: aload_3
    //   36: astore_0
    //   37: aload 6
    //   39: ldc 120
    //   41: invokestatic 25	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   44: invokespecial 28	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   47: aload_3
    //   48: astore_0
    //   49: aload 5
    //   51: aload 6
    //   53: invokespecial 35	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   56: aload_3
    //   57: astore_0
    //   58: aload_1
    //   59: aload 5
    //   61: invokespecial 123	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   64: aload_1
    //   65: invokevirtual 41	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   68: ldc 82
    //   70: invokevirtual 127	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   73: astore_3
    //   74: aload_3
    //   75: astore_0
    //   76: aload_1
    //   77: invokevirtual 56	java/io/BufferedReader:close	()V
    //   80: aload_3
    //   81: astore_0
    //   82: goto +53 -> 135
    //   85: astore_1
    //   86: aload_1
    //   87: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   90: goto +45 -> 135
    //   93: astore_0
    //   94: aload_1
    //   95: astore_2
    //   96: aload_0
    //   97: astore_1
    //   98: aload_2
    //   99: astore_0
    //   100: goto +39 -> 139
    //   103: astore_3
    //   104: goto +11 -> 115
    //   107: astore_1
    //   108: goto +31 -> 139
    //   111: astore_3
    //   112: aload 4
    //   114: astore_1
    //   115: aload_1
    //   116: astore_0
    //   117: aload_3
    //   118: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   121: aload_2
    //   122: astore_0
    //   123: aload_1
    //   124: ifnull +11 -> 135
    //   127: aload_2
    //   128: astore_0
    //   129: aload_1
    //   130: invokevirtual 56	java/io/BufferedReader:close	()V
    //   133: aload_2
    //   134: astore_0
    //   135: aload_0
    //   136: iconst_0
    //   137: aaload
    //   138: areturn
    //   139: aload_0
    //   140: ifnull +15 -> 155
    //   143: aload_0
    //   144: invokevirtual 56	java/io/BufferedReader:close	()V
    //   147: goto +8 -> 155
    //   150: astore_0
    //   151: aload_0
    //   152: invokestatic 64	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   155: aload_1
    //   156: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   16	66	0	arrayOfString1	String[]
    //   93	4	0	localObject1	Object
    //   99	45	0	localObject2	Object
    //   150	2	0	localThrowable1	Throwable
    //   20	57	1	localBufferedReader	java.io.BufferedReader
    //   85	10	1	localThrowable2	Throwable
    //   97	1	1	localObject3	Object
    //   107	1	1	localObject4	Object
    //   114	42	1	localObject5	Object
    //   4	130	2	localObject6	Object
    //   14	67	3	arrayOfString2	String[]
    //   103	1	3	localThrowable3	Throwable
    //   111	7	3	localThrowable4	Throwable
    //   11	102	4	localObject7	Object
    //   26	34	5	localInputStreamReader	java.io.InputStreamReader
    //   33	19	6	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   76	80	85	java/lang/Throwable
    //   129	133	85	java/lang/Throwable
    //   64	74	93	finally
    //   64	74	103	java/lang/Throwable
    //   17	21	107	finally
    //   23	28	107	finally
    //   30	35	107	finally
    //   37	47	107	finally
    //   49	56	107	finally
    //   58	64	107	finally
    //   117	121	107	finally
    //   17	21	111	java/lang/Throwable
    //   23	28	111	java/lang/Throwable
    //   30	35	111	java/lang/Throwable
    //   37	47	111	java/lang/Throwable
    //   49	56	111	java/lang/Throwable
    //   58	64	111	java/lang/Throwable
    //   143	147	150	java/lang/Throwable
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    int i = j.s;
    int j = 1;
    if (i != 1) {
      return;
    }
    try
    {
      paramMap.put("101030001", Build.VERSION.RELEASE);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(Build.VERSION.SDK_INT);
      paramMap.put("101030018", ((StringBuilder)localObject).toString());
      i = j;
      if (!b(c.a("ltz8ndzqbXSA+d9ZeRI+AA==")))
      {
        i = j;
        if (!b(c.a("a83SWKl2MIqEA+IDx16fUQ==")))
        {
          i = j;
          if (!b(c.a("uRdnUKd8L7ALcDs7apOIyA==")))
          {
            i = j;
            if (!b(c.a("pLDoOid7Pm0tLC+XeB0aMQ==")))
            {
              i = j;
              if (!b(c.a("WPM2O2jRUj1CnGWZzB+T29vIKsC5rjYyK6zFmxtnerY=")))
              {
                i = j;
                if (!b(c.a("wkfroYRvQLcfA/vOLh6yw18M+yyQpvVsXX6c4eV3mbk=")))
                {
                  i = j;
                  if (!b(c.a("WIKHDxO7DP3AeRArgziBwQ=="))) {
                    if (b(c.a("sjGGh25CKk+x+cnSgavYUEOMjTvTGoXEWTi/8SsOj/I="))) {
                      i = j;
                    } else {
                      i = 0;
                    }
                  }
                }
              }
            }
          }
        }
      }
      if (i != 0) {
        localObject = "1";
      } else {
        localObject = "0";
      }
      paramMap.put("101030020", localObject);
      paramMap.put("101031001", Build.DISPLAY);
      paramMap.put("101030004", Build.ID);
      paramMap.put("101030005", Build.VERSION.INCREMENTAL);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(System.getProperty(c.a("Km1OTSEFtKGMFZCsLygxVzhvJ851wCA5AVwqtyZr8G0=")));
      ((StringBuilder)localObject).append(System.getProperty(c.a("hPGmNX0ruNIPpMSrvmZ3WQLCscMq+E08/RU8rc0msA4=")));
      paramMap.put("101032001", ((StringBuilder)localObject).toString());
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(System.getProperty(c.a("p1sKaUZLovpe0a2tcT0R9A==")));
      ((StringBuilder)localObject).append(System.getProperty(c.a("faLHu+3KOFrJLqgMRkIxfQ==")));
      paramMap.put("101030006", ((StringBuilder)localObject).toString());
      Runtime localRuntime = Runtime.getRuntime();
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(localRuntime.maxMemory() / 1024L / 1024L);
      paramMap.put("101030011", ((StringBuilder)localObject).toString());
      paramMap.put("101033001", System.getProperty(c.a("QuOtNagPOSuNDo2tgJLYHQ==")));
      paramMap.put("101033002", b());
      paramMap.put("101031002", Build.TAGS);
      paramMap.put("101031003", Build.TYPE);
      paramMap.put("101030012", c());
      paramMap.put("101030013", System.getProperty(c.a("r9jRtVsT05V4PpQLvhMox39c25tn23ofdxeWAfUyEEk=")));
      paramMap.put("101030014", System.getProperty(c.a("oa1Kf+CnhK764oCXyonf9ldJV+fCABoLxwNXsarm+Cg=")));
      paramMap.put("101034001", System.getProperty(c.a("kW+kY2ze0AlO7hxmL3I7EHhUazH8dGl4y8b3OCP0TJg=")));
      paramMap.put("101034002", System.getProperty(c.a("eYHNHacCrVAMPF+Up5QPooYak7pkYOyNm77JlwjWObM=")));
      paramMap.put("101034003", System.getProperty(c.a("hUbuQyVF5460ljdl69c/4Pnhhu8MkRDeNdl11+k/rtk=")));
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(Locale.getDefault().getLanguage());
      ((StringBuilder)localObject).append(Locale.getDefault().getCountry());
      paramMap.put("101030015", ((StringBuilder)localObject).toString());
      paramMap.put("101030016", TimeZone.getDefault().getDisplayName());
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(SystemClock.elapsedRealtime());
      paramMap.put("101030017", ((StringBuilder)localObject).toString());
      paramMap.put("101030018", d());
      paramMap.put("101033003", System.getProperty(c.a("2XiYzdzr9CmLuMWAs4w5ew==")));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */