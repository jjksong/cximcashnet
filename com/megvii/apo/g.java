package com.megvii.apo;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Map;

public final class g
  extends n
{
  public g(Context paramContext)
  {
    super(paramContext);
  }
  
  private static String b()
  {
    Object localObject2 = c.a("7SDLt6eE94k4I3MgHA7rsw==");
    String str = "";
    Object localObject1 = str;
    try
    {
      FileReader localFileReader = new java/io/FileReader;
      localObject1 = str;
      localFileReader.<init>((String)localObject2);
      localObject1 = str;
      localObject2 = new java/io/BufferedReader;
      localObject1 = str;
      ((BufferedReader)localObject2).<init>(localFileReader, 8192);
      localObject1 = str;
      str = localObject2.readLine().split("\\s+")[1];
      localObject1 = str;
      ((BufferedReader)localObject2).close();
      localObject1 = str;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    int i;
    if (localObject1 != null) {
      i = (int)Math.ceil(Float.valueOf(Float.valueOf((String)localObject1).floatValue() / 1048576.0F).doubleValue());
    } else {
      i = 0;
    }
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append("GB");
    return ((StringBuilder)localObject1).toString();
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.m != 1) {
      return;
    }
    try
    {
      paramMap.put("101010010", b());
      Object localObject2 = (ActivityManager)this.a.getSystemService("activity");
      Object localObject1 = new android/app/ActivityManager$MemoryInfo;
      ((ActivityManager.MemoryInfo)localObject1).<init>();
      ((ActivityManager)localObject2).getMemoryInfo((ActivityManager.MemoryInfo)localObject1);
      paramMap.put("101010011", Formatter.formatFileSize(this.a, ((ActivityManager.MemoryInfo)localObject1).availMem));
      localObject2 = Environment.getDataDirectory();
      localObject1 = new android/os/StatFs;
      ((StatFs)localObject1).<init>(((File)localObject2).getPath());
      long l1;
      long l2;
      if (Build.VERSION.SDK_INT < 18)
      {
        l1 = ((StatFs)localObject1).getBlockSize();
        l2 = ((StatFs)localObject1).getBlockCount();
      }
      else
      {
        l1 = ((StatFs)localObject1).getBlockSizeLong();
        l2 = ((StatFs)localObject1).getBlockCountLong();
      }
      paramMap.put("101010012", Formatter.formatFileSize(this.a, l2 * l1));
      localObject1 = Environment.getDataDirectory();
      localObject2 = new android/os/StatFs;
      ((StatFs)localObject2).<init>(((File)localObject1).getPath());
      if (Build.VERSION.SDK_INT < 18)
      {
        l2 = ((StatFs)localObject2).getBlockSize();
        l1 = ((StatFs)localObject2).getAvailableBlocks();
      }
      else
      {
        l2 = ((StatFs)localObject2).getBlockSizeLong();
        l1 = ((StatFs)localObject2).getAvailableBlocksLong();
      }
      paramMap.put("101010013", Formatter.formatFileSize(this.a, l1 * l2));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */