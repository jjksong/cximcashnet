package com.megvii.apo;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;
import java.util.regex.Pattern;

public final class d
  extends n
{
  public d(Context paramContext)
  {
    super(paramContext);
  }
  
  @TargetApi(21)
  private static String b()
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    try
    {
      if (Build.VERSION.SDK_INT >= 21)
      {
        localObject = Build.SUPPORTED_ABIS;
        for (int i = 0; i < localObject.length; i++)
        {
          localStringBuilder1.append(localObject[i]);
          if (i != localObject.length - 1) {
            localStringBuilder1.append(",");
          }
        }
      }
      String str = Build.CPU_ABI;
      Object localObject = Build.CPU_ABI2;
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>();
      localStringBuilder2.append(str);
      localStringBuilder2.append(",");
      localStringBuilder2.append((String)localObject);
      localStringBuilder1.append(localStringBuilder2.toString());
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return localStringBuilder1.toString();
  }
  
  private static String b(String paramString)
  {
    Object localObject2 = "";
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(c.a("2TfER6mCjFXQ6s36dvL3IByjDs0ZKIY05Q/18sYbh4U="));
    ((StringBuilder)localObject1).append(paramString);
    ((StringBuilder)localObject1).append(c.a("z34pa4DPQ6ZsbwyrQTIKAsKD1CvHrVGqNbD+XF/v6IM="));
    paramString = ((StringBuilder)localObject1).toString();
    paramString = new ProcessBuilder(new String[] { c.a("mS9Og5cjPiWe+XQgm7W/8Q=="), paramString });
    localObject1 = localObject2;
    try
    {
      InputStream localInputStream = paramString.start().getInputStream();
      localObject1 = localObject2;
      byte[] arrayOfByte = new byte[24];
      for (paramString = (String)localObject2;; paramString = ((StringBuilder)localObject2).toString())
      {
        localObject1 = paramString;
        if (localInputStream.read(arrayOfByte) == -1) {
          break;
        }
        localObject1 = paramString;
        localObject2 = new java/lang/StringBuilder;
        localObject1 = paramString;
        ((StringBuilder)localObject2).<init>();
        localObject1 = paramString;
        ((StringBuilder)localObject2).append(paramString);
        localObject1 = paramString;
        String str = new java/lang/String;
        localObject1 = paramString;
        str.<init>(arrayOfByte);
        localObject1 = paramString;
        ((StringBuilder)localObject2).append(str);
        localObject1 = paramString;
      }
      localObject1 = paramString;
      localInputStream.close();
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
      paramString = (String)localObject1;
    }
    return paramString.trim();
  }
  
  private static String c()
  {
    Object localObject2 = "";
    Object localObject4;
    try
    {
      localObject4 = c.a("mS9Og5cjPiWe+XQgm7W/8Q==");
      Object localObject1 = c.a("2TfER6mCjFXQ6s36dvL3IEWJsulLhJQnMXThgXzBdtyW1bJf3oBfAfVmIp5Y7/coc0RMAvapX+JrjL9Fak+C+g==");
      localObject3 = new java/lang/ProcessBuilder;
      ((ProcessBuilder)localObject3).<init>(new String[] { localObject4, localObject1 });
      localObject1 = ((ProcessBuilder)localObject3).start();
      localObject3 = new java/io/BufferedReader;
      localObject4 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject4).<init>(((Process)localObject1).getInputStream());
      ((BufferedReader)localObject3).<init>((Reader)localObject4);
      localObject1 = ((BufferedReader)localObject3).readLine();
      localObject2 = localObject1;
    }
    catch (Throwable localThrowable1)
    {
      e.a(localThrowable1);
    }
    String str1 = "";
    String str2;
    try
    {
      localObject4 = c.a("mS9Og5cjPiWe+XQgm7W/8Q==");
      localObject3 = c.a("2TfER6mCjFXQ6s36dvL3IEWJsulLhJQnMXThgXzBdtxeBn9Lk8DnzfqlyxDgbLd6l0wqzdREqzmwLQ3n95aA4Q==");
      Object localObject5 = new java/lang/ProcessBuilder;
      ((ProcessBuilder)localObject5).<init>(new String[] { localObject4, localObject3 });
      localObject3 = ((ProcessBuilder)localObject5).start().getInputStream();
      localObject4 = new byte[24];
      while (((InputStream)localObject3).read((byte[])localObject4) != -1)
      {
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        ((StringBuilder)localObject5).append(str1);
        str1 = new java/lang/String;
        str1.<init>((byte[])localObject4);
        ((StringBuilder)localObject5).append(str1);
        str1 = ((StringBuilder)localObject5).toString();
      }
      ((InputStream)localObject3).close();
    }
    catch (Throwable localThrowable2)
    {
      e.a(localThrowable2);
      str2 = "N/A";
    }
    Object localObject3 = new StringBuilder();
    ((StringBuilder)localObject3).append(str2.trim());
    ((StringBuilder)localObject3).append("~~");
    ((StringBuilder)localObject3).append((String)localObject2);
    ((StringBuilder)localObject3).append("KHZ");
    return ((StringBuilder)localObject3).toString();
  }
  
  private String d()
  {
    try
    {
      Object localObject1 = new java/io/File;
      ((File)localObject1).<init>(c.a("2TfER6mCjFXQ6s36dvL3IOW+Mj28jQ19kKkBvmgEHD8="));
      Object localObject2 = new com/megvii/apo/d$a;
      ((a)localObject2).<init>(this);
      localObject2 = ((File)localObject1).listFiles((FileFilter)localObject2);
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(localObject2.length);
      localObject1 = ((StringBuilder)localObject1).toString();
      return (String)localObject1;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return "1";
  }
  
  private static String e()
  {
    float f2 = (float)f();
    float f3 = (float)g();
    try
    {
      Thread.sleep(360L);
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    float f1 = (float)f();
    return String.valueOf(((float)g() - f3) * 100.0F / (f1 - f2));
  }
  
  private static long f()
  {
    Object localObject2;
    try
    {
      Object localObject1 = new java/io/BufferedReader;
      Object localObject3 = new java/io/InputStreamReader;
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(c.a("ngjGDUuZLTBiNu5EbFIvrA=="));
      ((InputStreamReader)localObject3).<init>(localFileInputStream);
      ((BufferedReader)localObject1).<init>((Reader)localObject3, 1000);
      localObject3 = ((BufferedReader)localObject1).readLine();
      ((BufferedReader)localObject1).close();
      localObject1 = ((String)localObject3).split(" ");
    }
    catch (Throwable localThrowable1)
    {
      e.a(localThrowable1);
      localObject2 = null;
    }
    long l2 = 0L;
    long l1 = l2;
    if (localObject2 != null)
    {
      l1 = l2;
      try
      {
        if (localObject2.length > 0)
        {
          long l6 = Long.parseLong(localObject2[2]);
          long l3 = Long.parseLong(localObject2[3]);
          long l8 = Long.parseLong(localObject2[4]);
          long l7 = Long.parseLong(localObject2[6]);
          long l5 = Long.parseLong(localObject2[5]);
          long l4 = Long.parseLong(localObject2[7]);
          l1 = Long.parseLong(localObject2[8]);
          l1 = l6 + l3 + l8 + l7 + l5 + l4 + l1;
        }
      }
      catch (Throwable localThrowable2)
      {
        e.a(localThrowable2);
        l1 = l2;
      }
    }
    return l1;
  }
  
  private static long g()
  {
    Object localObject2;
    try
    {
      int i = android.os.Process.myPid();
      Object localObject1 = new java/io/BufferedReader;
      InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      ((StringBuilder)localObject3).append(c.a("vnW8/s+yOk7vXz4qTaIYHg=="));
      ((StringBuilder)localObject3).append(i);
      ((StringBuilder)localObject3).append(c.a("S/OzKa9C0LXu4bIh2IG+ng=="));
      localFileInputStream.<init>(((StringBuilder)localObject3).toString());
      localInputStreamReader.<init>(localFileInputStream);
      ((BufferedReader)localObject1).<init>(localInputStreamReader, 1000);
      localObject3 = ((BufferedReader)localObject1).readLine();
      ((BufferedReader)localObject1).close();
      localObject1 = ((String)localObject3).split(" ");
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
      localObject2 = null;
    }
    return Long.parseLong(localObject2[13]) + Long.parseLong(localObject2[14]) + Long.parseLong(localObject2[15]) + Long.parseLong(localObject2[16]);
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.y != 1) {
      return;
    }
    try
    {
      paramMap.put("101052001", b());
      paramMap.put("101051001", c());
      int j = Integer.valueOf(d()).intValue();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      for (int i = 0; i < j; i++)
      {
        localStringBuilder.append(b(String.valueOf(i)));
        if (i != j - 1) {
          localStringBuilder.append(",");
        }
      }
      paramMap.put("101051002", localStringBuilder.toString());
      paramMap.put("101050006", e());
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
  
  final class a
    implements FileFilter
  {
    a() {}
    
    public final boolean accept(File paramFile)
    {
      return Pattern.matches("cpu[0-9]", paramFile.getName());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */