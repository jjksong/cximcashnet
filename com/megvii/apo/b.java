package com.megvii.apo;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.megvii.apo.util.e;
import com.megvii.apo.util.g;
import com.megvii.apo.util.j;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b
  extends n
{
  private PackageManager c = this.a.getPackageManager();
  
  public b(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  private static List<String> a(Context paramContext)
  {
    // Byte code:
    //   0: new 30	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 33	java/util/ArrayList:<init>	()V
    //   7: astore 6
    //   9: aconst_null
    //   10: astore 5
    //   12: aconst_null
    //   13: astore 4
    //   15: aconst_null
    //   16: astore_2
    //   17: invokestatic 39	android/os/Process:myPid	()I
    //   20: istore_1
    //   21: new 41	java/lang/StringBuilder
    //   24: astore_3
    //   25: aload_3
    //   26: ldc 43
    //   28: invokespecial 46	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   31: aload_3
    //   32: iload_1
    //   33: invokevirtual 50	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_3
    //   38: ldc 52
    //   40: invokevirtual 55	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload_3
    //   45: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   48: astore 7
    //   50: new 61	java/io/File
    //   53: astore_3
    //   54: aload_3
    //   55: aload 7
    //   57: invokespecial 62	java/io/File:<init>	(Ljava/lang/String;)V
    //   60: aload_3
    //   61: invokevirtual 66	java/io/File:exists	()Z
    //   64: ifeq +155 -> 219
    //   67: new 68	java/io/FileReader
    //   70: astore_2
    //   71: aload_2
    //   72: aload_3
    //   73: invokespecial 71	java/io/FileReader:<init>	(Ljava/io/File;)V
    //   76: new 73	java/io/BufferedReader
    //   79: astore_3
    //   80: aload_3
    //   81: aload_2
    //   82: invokespecial 76	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   85: aload_3
    //   86: invokevirtual 79	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   89: astore 4
    //   91: aload 4
    //   93: ifnull +88 -> 181
    //   96: aload 4
    //   98: ldc 81
    //   100: invokevirtual 87	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   103: ifeq -18 -> 85
    //   106: aload 4
    //   108: ldc 89
    //   110: invokevirtual 93	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   113: astore 4
    //   115: aload 4
    //   117: ifnull -32 -> 85
    //   120: aload 4
    //   122: arraylength
    //   123: ifle -38 -> 85
    //   126: aload 4
    //   128: aload 4
    //   130: arraylength
    //   131: iconst_1
    //   132: isub
    //   133: aaload
    //   134: astore 4
    //   136: aload 4
    //   138: invokestatic 99	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   141: ifne -56 -> 85
    //   144: aload 4
    //   146: aload_0
    //   147: invokevirtual 102	android/content/Context:getPackageName	()Ljava/lang/String;
    //   150: invokevirtual 105	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   153: ifne -68 -> 85
    //   156: aload 6
    //   158: aload 4
    //   160: invokeinterface 110 2 0
    //   165: ifne -80 -> 85
    //   168: aload 6
    //   170: aload 4
    //   172: invokeinterface 113 2 0
    //   177: pop
    //   178: goto -93 -> 85
    //   181: aload_2
    //   182: astore_0
    //   183: goto +40 -> 223
    //   186: astore 4
    //   188: aload_3
    //   189: astore_0
    //   190: aload 4
    //   192: astore_3
    //   193: goto +112 -> 305
    //   196: astore 4
    //   198: aload_3
    //   199: astore_0
    //   200: aload 4
    //   202: astore_3
    //   203: goto +13 -> 216
    //   206: astore_0
    //   207: aload 4
    //   209: astore_3
    //   210: goto +103 -> 313
    //   213: astore_3
    //   214: aconst_null
    //   215: astore_0
    //   216: goto +49 -> 265
    //   219: aconst_null
    //   220: astore_3
    //   221: aload_2
    //   222: astore_0
    //   223: aload_0
    //   224: ifnull +15 -> 239
    //   227: aload_0
    //   228: invokevirtual 116	java/io/FileReader:close	()V
    //   231: goto +8 -> 239
    //   234: astore_0
    //   235: aload_0
    //   236: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   239: aload_3
    //   240: ifnull +61 -> 301
    //   243: aload_3
    //   244: invokevirtual 122	java/io/BufferedReader:close	()V
    //   247: goto +54 -> 301
    //   250: astore_0
    //   251: aconst_null
    //   252: astore_2
    //   253: aload 4
    //   255: astore_3
    //   256: goto +57 -> 313
    //   259: astore_3
    //   260: aconst_null
    //   261: astore_0
    //   262: aload 5
    //   264: astore_2
    //   265: aload_3
    //   266: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   269: aload_2
    //   270: ifnull +15 -> 285
    //   273: aload_2
    //   274: invokevirtual 116	java/io/FileReader:close	()V
    //   277: goto +8 -> 285
    //   280: astore_2
    //   281: aload_2
    //   282: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   285: aload_0
    //   286: ifnull +15 -> 301
    //   289: aload_0
    //   290: invokevirtual 122	java/io/BufferedReader:close	()V
    //   293: goto +8 -> 301
    //   296: astore_0
    //   297: aload_0
    //   298: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   301: aload 6
    //   303: areturn
    //   304: astore_3
    //   305: aload_0
    //   306: astore 4
    //   308: aload_3
    //   309: astore_0
    //   310: aload 4
    //   312: astore_3
    //   313: aload_2
    //   314: ifnull +15 -> 329
    //   317: aload_2
    //   318: invokevirtual 116	java/io/FileReader:close	()V
    //   321: goto +8 -> 329
    //   324: astore_2
    //   325: aload_2
    //   326: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   329: aload_3
    //   330: ifnull +15 -> 345
    //   333: aload_3
    //   334: invokevirtual 122	java/io/BufferedReader:close	()V
    //   337: goto +8 -> 345
    //   340: astore_2
    //   341: aload_2
    //   342: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   345: aload_0
    //   346: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	347	0	paramContext	Context
    //   20	13	1	i	int
    //   16	258	2	localObject1	Object
    //   280	38	2	localThrowable1	Throwable
    //   324	2	2	localThrowable2	Throwable
    //   340	2	2	localThrowable3	Throwable
    //   24	186	3	localObject2	Object
    //   213	1	3	localThrowable4	Throwable
    //   220	36	3	localThrowable5	Throwable
    //   259	7	3	localThrowable6	Throwable
    //   304	5	3	localObject3	Object
    //   312	22	3	localContext1	Context
    //   13	158	4	localObject4	Object
    //   186	5	4	localObject5	Object
    //   196	58	4	localThrowable7	Throwable
    //   306	5	4	localContext2	Context
    //   10	253	5	localObject6	Object
    //   7	295	6	localArrayList	ArrayList
    //   48	8	7	str	String
    // Exception table:
    //   from	to	target	type
    //   85	91	186	finally
    //   96	115	186	finally
    //   120	178	186	finally
    //   85	91	196	java/lang/Throwable
    //   96	115	196	java/lang/Throwable
    //   120	178	196	java/lang/Throwable
    //   76	85	206	finally
    //   76	85	213	java/lang/Throwable
    //   227	231	234	java/lang/Throwable
    //   17	76	250	finally
    //   17	76	259	java/lang/Throwable
    //   273	277	280	java/lang/Throwable
    //   243	247	296	java/lang/Throwable
    //   289	293	296	java/lang/Throwable
    //   265	269	304	finally
    //   317	321	324	java/lang/Throwable
    //   333	337	340	java/lang/Throwable
  }
  
  private static String[] a(List<a> paramList)
  {
    String str = "";
    Object localObject = "";
    Iterator localIterator = paramList.iterator();
    paramList = (List<a>)localObject;
    while (localIterator.hasNext())
    {
      localObject = (a)localIterator.next();
      if ("1".equals(((a)localObject).m))
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(str);
        localStringBuilder.append(((a)localObject).b);
        str = localStringBuilder.toString();
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(str);
        localStringBuilder.append(",");
        str = localStringBuilder.toString();
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramList);
        localStringBuilder.append(((a)localObject).d);
        localObject = localStringBuilder.toString();
        paramList = new StringBuilder();
        paramList.append((String)localObject);
        paramList.append(",");
        paramList = paramList.toString();
      }
    }
    return new String[] { str, paramList, "" };
  }
  
  private boolean b()
  {
    boolean bool2 = false;
    boolean bool1;
    try
    {
      Object localObject = a(this.a);
      if (((List)localObject).size() == 0) {
        return false;
      }
      localObject = ((List)localObject).iterator();
      do
      {
        bool1 = bool2;
        if (!((Iterator)localObject).hasNext()) {
          break;
        }
        bool1 = b((String)((Iterator)localObject).next());
      } while (!bool1);
      bool1 = true;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
      bool1 = bool2;
    }
    return bool1;
  }
  
  /* Error */
  private static boolean b(String paramString)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: iconst_0
    //   5: istore 4
    //   7: new 61	java/io/File
    //   10: astore 6
    //   12: aload 6
    //   14: aload_0
    //   15: invokespecial 62	java/io/File:<init>	(Ljava/lang/String;)V
    //   18: aload 6
    //   20: invokevirtual 66	java/io/File:exists	()Z
    //   23: ifne +5 -> 28
    //   26: iconst_0
    //   27: ireturn
    //   28: new 168	java/util/zip/ZipInputStream
    //   31: astore 5
    //   33: new 170	java/io/FileInputStream
    //   36: astore_0
    //   37: aload_0
    //   38: aload 6
    //   40: invokespecial 171	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   43: aload 5
    //   45: aload_0
    //   46: invokespecial 174	java/util/zip/ZipInputStream:<init>	(Ljava/io/InputStream;)V
    //   49: aload 5
    //   51: astore_0
    //   52: aload 5
    //   54: invokevirtual 178	java/util/zip/ZipInputStream:getNextEntry	()Ljava/util/zip/ZipEntry;
    //   57: astore 6
    //   59: iload 4
    //   61: istore_1
    //   62: aload 6
    //   64: ifnull +77 -> 141
    //   67: aload 5
    //   69: astore_0
    //   70: aload 6
    //   72: invokevirtual 183	java/util/zip/ZipEntry:isDirectory	()Z
    //   75: ifne -26 -> 49
    //   78: aload 5
    //   80: astore_0
    //   81: aload 6
    //   83: invokevirtual 186	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   86: invokestatic 99	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   89: ifne -40 -> 49
    //   92: aload 5
    //   94: astore_0
    //   95: aload 6
    //   97: invokevirtual 186	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   100: ldc -68
    //   102: invokevirtual 105	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   105: ifne -56 -> 49
    //   108: aload 5
    //   110: astore_0
    //   111: aload 6
    //   113: invokevirtual 186	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   116: astore 6
    //   118: aload 5
    //   120: astore_0
    //   121: ldc -66
    //   123: invokestatic 196	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   126: aload 6
    //   128: invokevirtual 200	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   131: invokevirtual 205	java/util/regex/Matcher:matches	()Z
    //   134: istore_1
    //   135: iload_1
    //   136: ifeq -87 -> 49
    //   139: iconst_1
    //   140: istore_1
    //   141: iload_1
    //   142: istore_2
    //   143: aload 5
    //   145: invokevirtual 206	java/util/zip/ZipInputStream:close	()V
    //   148: goto +52 -> 200
    //   151: astore_0
    //   152: aload_0
    //   153: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   156: iload_2
    //   157: istore_1
    //   158: goto +42 -> 200
    //   161: astore 6
    //   163: goto +15 -> 178
    //   166: astore_0
    //   167: aconst_null
    //   168: astore 5
    //   170: goto +40 -> 210
    //   173: astore 6
    //   175: aconst_null
    //   176: astore 5
    //   178: aload 5
    //   180: astore_0
    //   181: aload 6
    //   183: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   186: iload_3
    //   187: istore_1
    //   188: aload 5
    //   190: ifnull +10 -> 200
    //   193: aload 5
    //   195: invokevirtual 206	java/util/zip/ZipInputStream:close	()V
    //   198: iload_3
    //   199: istore_1
    //   200: iload_1
    //   201: ireturn
    //   202: astore 6
    //   204: aload_0
    //   205: astore 5
    //   207: aload 6
    //   209: astore_0
    //   210: aload 5
    //   212: ifnull +18 -> 230
    //   215: aload 5
    //   217: invokevirtual 206	java/util/zip/ZipInputStream:close	()V
    //   220: goto +10 -> 230
    //   223: astore 5
    //   225: aload 5
    //   227: invokestatic 121	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   230: aload_0
    //   231: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	232	0	paramString	String
    //   61	140	1	bool1	boolean
    //   1	156	2	bool2	boolean
    //   3	196	3	bool3	boolean
    //   5	55	4	bool4	boolean
    //   31	185	5	localObject1	Object
    //   223	3	5	localThrowable1	Throwable
    //   10	117	6	localObject2	Object
    //   161	1	6	localThrowable2	Throwable
    //   173	9	6	localThrowable3	Throwable
    //   202	6	6	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   143	148	151	java/lang/Throwable
    //   193	198	151	java/lang/Throwable
    //   52	59	161	java/lang/Throwable
    //   70	78	161	java/lang/Throwable
    //   81	92	161	java/lang/Throwable
    //   95	108	161	java/lang/Throwable
    //   111	118	161	java/lang/Throwable
    //   121	135	161	java/lang/Throwable
    //   7	26	166	finally
    //   28	49	166	finally
    //   7	26	173	java/lang/Throwable
    //   28	49	173	java/lang/Throwable
    //   52	59	202	finally
    //   70	78	202	finally
    //   81	92	202	finally
    //   95	108	202	finally
    //   111	118	202	finally
    //   121	135	202	finally
    //   181	186	202	finally
    //   215	220	223	java/lang/Throwable
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.E != 1) {
      return;
    }
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Object localObject1 = this.c.getInstalledApplications(8192).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (ApplicationInfo)((Iterator)localObject1).next();
        localObject2 = g.a(this.a, (ApplicationInfo)localObject2);
        ((a)localObject2).a = "0";
        localArrayList.add(localObject2);
      }
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>(localArrayList.toString());
      paramMap.put("101071000", localObject1);
      paramMap.put("101074001", a(localArrayList)[0]);
      paramMap.put("101074003", a(localArrayList)[1]);
      paramMap.put("101076005", Boolean.valueOf(b()));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
  
  public static final class a
  {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m = "0";
    public boolean n;
    
    public final String toString()
    {
      JSONObject localJSONObject = new JSONObject();
      try
      {
        localJSONObject.put("101071001", this.a);
        localJSONObject.put("101071002", this.b);
        localJSONObject.put("101071003", this.c);
        localJSONObject.put("101071004", this.d);
        localJSONObject.put("101071005", this.e);
        localJSONObject.put("101071007", this.g);
        localJSONObject.put("101071008", this.h);
        localJSONObject.put("101071009", this.i);
        localJSONObject.put("101071011", this.k);
        localJSONObject.put("101071014", this.l);
        localJSONObject.put("101071015", this.m);
        String str;
        if (this.n) {
          str = "1";
        } else {
          str = "0";
        }
        localJSONObject.put("101071016", str);
        localJSONObject.put("101071010", this.j);
        localJSONObject.put("101071006", this.f);
      }
      catch (Throwable localThrowable)
      {
        e.a(localThrowable);
      }
      return localJSONObject.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */