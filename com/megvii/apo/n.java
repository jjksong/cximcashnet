package com.megvii.apo;

import android.content.Context;
import android.content.pm.PackageManager;
import com.megvii.apo.a.a;
import com.megvii.apo.util.e;
import java.util.Map;

public abstract class n
{
  protected Context a;
  protected a b;
  
  public n(Context paramContext)
  {
    this.a = paramContext.getApplicationContext();
  }
  
  public static String a(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramInt & 0xFF);
    localStringBuilder.append(".");
    localStringBuilder.append(paramInt >> 8 & 0xFF);
    localStringBuilder.append(".");
    localStringBuilder.append(paramInt >> 16 & 0xFF);
    localStringBuilder.append(".");
    localStringBuilder.append(paramInt >> 24 & 0xFF);
    return localStringBuilder.toString();
  }
  
  public final a a()
  {
    return this.b;
  }
  
  public final void a(a parama)
  {
    this.b = parama;
  }
  
  public abstract void a(Map<String, Object> paramMap);
  
  public final boolean a(String paramString)
  {
    try
    {
      int i = this.a.getPackageManager().checkPermission(paramString, this.a.getPackageName());
      return i == 0;
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */