package com.megvii.apo.util;

import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

public final class l
{
  /* Error */
  public static int a(Context paramContext, int paramInt)
  {
    // Byte code:
    //   0: ldc 10
    //   2: invokestatic 15	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   5: invokestatic 21	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   8: astore_3
    //   9: aload_0
    //   10: invokevirtual 27	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   13: astore 4
    //   15: aconst_null
    //   16: astore_0
    //   17: aconst_null
    //   18: astore_2
    //   19: aload 4
    //   21: aload_3
    //   22: iconst_2
    //   23: anewarray 29	java/lang/String
    //   26: dup
    //   27: iconst_0
    //   28: ldc 31
    //   30: aastore
    //   31: dup
    //   32: iconst_1
    //   33: ldc 33
    //   35: aastore
    //   36: ldc 35
    //   38: iconst_1
    //   39: anewarray 29	java/lang/String
    //   42: dup
    //   43: iconst_0
    //   44: iload_1
    //   45: invokestatic 39	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   48: aastore
    //   49: aconst_null
    //   50: invokevirtual 45	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   53: astore_3
    //   54: aload_3
    //   55: ifnull +47 -> 102
    //   58: aload_3
    //   59: astore_2
    //   60: aload_3
    //   61: astore_0
    //   62: aload_3
    //   63: invokeinterface 51 1 0
    //   68: ifeq +34 -> 102
    //   71: aload_3
    //   72: astore_2
    //   73: aload_3
    //   74: astore_0
    //   75: aload_3
    //   76: aload_3
    //   77: ldc 31
    //   79: invokeinterface 55 2 0
    //   84: invokeinterface 59 2 0
    //   89: istore_1
    //   90: aload_3
    //   91: ifnull +9 -> 100
    //   94: aload_3
    //   95: invokeinterface 63 1 0
    //   100: iload_1
    //   101: ireturn
    //   102: aload_3
    //   103: ifnull +29 -> 132
    //   106: aload_3
    //   107: astore_0
    //   108: goto +18 -> 126
    //   111: astore_0
    //   112: goto +22 -> 134
    //   115: astore_3
    //   116: aload_0
    //   117: astore_2
    //   118: aload_3
    //   119: invokestatic 68	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   122: aload_0
    //   123: ifnull +9 -> 132
    //   126: aload_0
    //   127: invokeinterface 63 1 0
    //   132: iconst_m1
    //   133: ireturn
    //   134: aload_2
    //   135: ifnull +9 -> 144
    //   138: aload_2
    //   139: invokeinterface 63 1 0
    //   144: aload_0
    //   145: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	146	0	paramContext	Context
    //   0	146	1	paramInt	int
    //   18	121	2	localObject1	Object
    //   8	99	3	localObject2	Object
    //   115	4	3	localThrowable	Throwable
    //   13	7	4	localContentResolver	android.content.ContentResolver
    // Exception table:
    //   from	to	target	type
    //   19	54	111	finally
    //   62	71	111	finally
    //   75	90	111	finally
    //   118	122	111	finally
    //   19	54	115	java/lang/Throwable
    //   62	71	115	java/lang/Throwable
    //   75	90	115	java/lang/Throwable
  }
  
  public static String a(Context paramContext, int paramInt1, int paramInt2, String paramString, int paramInt3)
  {
    Object localObject = null;
    try
    {
      TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
      paramContext = (Context)localObject;
      if (Build.VERSION.SDK_INT >= paramInt3)
      {
        paramString = localTelephonyManager.getClass().getMethod(paramString, a(paramString, paramInt2));
        if (paramInt2 == 0)
        {
          paramContext = String.valueOf(paramString.invoke(localTelephonyManager, new Object[0]));
        }
        else
        {
          paramContext = (Context)localObject;
          if (paramInt1 >= 0) {
            paramContext = String.valueOf(paramString.invoke(localTelephonyManager, new Object[] { Integer.valueOf(paramInt1) }));
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
      paramContext = (Context)localObject;
    }
    return paramContext;
  }
  
  private static Class[] a(String paramString, int paramInt)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    try
    {
      Method[] arrayOfMethod = TelephonyManager.class.getDeclaredMethods();
      int i = 0;
      Object localObject3;
      for (;;)
      {
        localObject1 = localObject2;
        localObject3 = localObject2;
        if (i >= arrayOfMethod.length) {
          break;
        }
        localObject3 = localObject2;
        localObject1 = localObject2;
        if (paramString.equals(arrayOfMethod[i].getName()))
        {
          localObject1 = localObject2;
          localObject2 = arrayOfMethod[i].getParameterTypes();
          localObject1 = localObject2;
          int j = localObject2.length;
          localObject3 = localObject2;
          if (j >= paramInt) {
            break;
          }
          localObject3 = localObject2;
        }
        i++;
        localObject2 = localObject3;
      }
      return (Class[])localObject3;
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
      localObject3 = localObject1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */