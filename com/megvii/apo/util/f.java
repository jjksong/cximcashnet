package com.megvii.apo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public final class f
{
  public static int a(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    int j = 0;
    int i = j;
    if (localNetworkInfo != null)
    {
      i = j;
      if (localNetworkInfo.isConnected())
      {
        int k = localNetworkInfo.getType();
        if (k == 1)
        {
          i = 1;
        }
        else
        {
          i = j;
          if (k == 0) {
            switch (((TelephonyManager)paramContext.getSystemService("phone")).getNetworkType())
            {
            default: 
              i = j;
              break;
            case 13: 
              i = 4;
              break;
            case 3: 
            case 5: 
            case 6: 
            case 8: 
            case 9: 
            case 10: 
            case 12: 
            case 14: 
            case 15: 
              i = 3;
              break;
            case 1: 
            case 2: 
            case 4: 
            case 7: 
            case 11: 
              i = 2;
            }
          }
        }
      }
    }
    return i;
  }
  
  public static boolean b(Context paramContext)
  {
    int i = a(paramContext);
    return (i == 2) || (i == 3) || (i == 4);
  }
  
  public static boolean c(Context paramContext)
  {
    return a(paramContext) == 1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */