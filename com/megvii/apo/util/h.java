package com.megvii.apo.util;

import android.content.Context;
import com.megvii.apo.b.a;
import java.io.File;

public final class h
{
  private Context a;
  private File b;
  private File c;
  private File d;
  private a e;
  private long f = 0L;
  private long g = 0L;
  
  public h(Context paramContext)
  {
    this.a = paramContext;
    this.b = new File(paramContext.getFilesDir(), "a");
    this.c = new File(paramContext.getFilesDir(), "d");
    this.d = new File(paramContext.getFilesDir(), "p");
    this.e = new a(this.a);
  }
  
  /* Error */
  public final void a(int paramInt)
  {
    // Byte code:
    //   0: iload_1
    //   1: iconst_1
    //   2: if_icmpne +17 -> 19
    //   5: aload_0
    //   6: getfield 40	com/megvii/apo/util/h:b	Ljava/io/File;
    //   9: astore 5
    //   11: goto +39 -> 50
    //   14: astore 5
    //   16: goto +635 -> 651
    //   19: iload_1
    //   20: iconst_2
    //   21: if_icmpne +12 -> 33
    //   24: aload_0
    //   25: getfield 43	com/megvii/apo/util/h:c	Ljava/io/File;
    //   28: astore 5
    //   30: goto +20 -> 50
    //   33: iload_1
    //   34: iconst_3
    //   35: if_icmpne +12 -> 47
    //   38: aload_0
    //   39: getfield 47	com/megvii/apo/util/h:d	Ljava/io/File;
    //   42: astore 5
    //   44: goto +6 -> 50
    //   47: aconst_null
    //   48: astore 5
    //   50: aload 5
    //   52: ifnonnull +4 -> 56
    //   55: return
    //   56: aload 5
    //   58: invokevirtual 63	java/io/File:listFiles	()[Ljava/io/File;
    //   61: astore 9
    //   63: aload 9
    //   65: ifnull +585 -> 650
    //   68: aload 9
    //   70: arraylength
    //   71: ifgt +6 -> 77
    //   74: goto +576 -> 650
    //   77: aload 9
    //   79: arraylength
    //   80: istore_3
    //   81: iconst_0
    //   82: istore_2
    //   83: iload_2
    //   84: iload_3
    //   85: if_icmpge +553 -> 638
    //   88: aload 9
    //   90: iload_2
    //   91: aaload
    //   92: astore 10
    //   94: new 65	com/megvii/apo/b/b
    //   97: astore 11
    //   99: aload 11
    //   101: invokespecial 66	com/megvii/apo/b/b:<init>	()V
    //   104: new 68	java/io/FileInputStream
    //   107: astore 6
    //   109: aload 6
    //   111: aload 10
    //   113: invokespecial 71	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   116: aload 6
    //   118: astore 7
    //   120: aload 11
    //   122: invokevirtual 73	com/megvii/apo/b/b:a	()V
    //   125: aload 6
    //   127: astore 7
    //   129: aload 11
    //   131: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   134: ldc 78
    //   136: aload 10
    //   138: invokevirtual 82	java/io/File:getName	()Ljava/lang/String;
    //   141: invokestatic 85	com/megvii/apo/b/b:a	(Ljava/lang/String;Ljava/lang/String;)[B
    //   144: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   147: aload 6
    //   149: astore 7
    //   151: aload 11
    //   153: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   156: ldc 93
    //   158: invokevirtual 99	java/lang/String:getBytes	()[B
    //   161: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   164: aload 6
    //   166: astore 7
    //   168: aload 11
    //   170: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   173: ldc 101
    //   175: invokevirtual 99	java/lang/String:getBytes	()[B
    //   178: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   181: aload 6
    //   183: astore 7
    //   185: sipush 4096
    //   188: newarray <illegal type>
    //   190: astore 8
    //   192: aload 6
    //   194: astore 7
    //   196: aload 6
    //   198: aload 8
    //   200: invokevirtual 107	java/io/InputStream:read	([B)I
    //   203: istore 4
    //   205: iload 4
    //   207: iconst_m1
    //   208: if_icmpeq +23 -> 231
    //   211: aload 6
    //   213: astore 7
    //   215: aload 11
    //   217: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   220: aload 8
    //   222: iconst_0
    //   223: iload 4
    //   225: invokevirtual 110	java/io/ByteArrayOutputStream:write	([BII)V
    //   228: goto -36 -> 192
    //   231: aload 6
    //   233: astore 7
    //   235: aload 11
    //   237: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   240: ldc 101
    //   242: invokevirtual 99	java/lang/String:getBytes	()[B
    //   245: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   248: aload 6
    //   250: astore 7
    //   252: new 112	java/lang/StringBuilder
    //   255: astore 8
    //   257: aload 6
    //   259: astore 7
    //   261: aload 8
    //   263: ldc 114
    //   265: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   268: aload 6
    //   270: astore 7
    //   272: aload 8
    //   274: aload 11
    //   276: getfield 120	com/megvii/apo/b/b:b	Ljava/lang/String;
    //   279: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   282: pop
    //   283: aload 6
    //   285: astore 7
    //   287: aload 8
    //   289: ldc 126
    //   291: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload 6
    //   297: astore 7
    //   299: aload 8
    //   301: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   304: astore 8
    //   306: aload 6
    //   308: astore 7
    //   310: aload 11
    //   312: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   315: aload 8
    //   317: invokevirtual 99	java/lang/String:getBytes	()[B
    //   320: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   323: aload 6
    //   325: astore 7
    //   327: aload 11
    //   329: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   332: invokevirtual 132	java/io/ByteArrayOutputStream:flush	()V
    //   335: aload 6
    //   337: invokestatic 135	com/megvii/apo/b/b:a	(Ljava/io/Closeable;)V
    //   340: goto +33 -> 373
    //   343: astore 8
    //   345: goto +16 -> 361
    //   348: astore 6
    //   350: aconst_null
    //   351: astore 7
    //   353: goto +259 -> 612
    //   356: astore 8
    //   358: aconst_null
    //   359: astore 6
    //   361: aload 6
    //   363: astore 7
    //   365: aload 8
    //   367: invokevirtual 138	java/io/IOException:printStackTrace	()V
    //   370: goto -35 -> 335
    //   373: ldc -116
    //   375: invokevirtual 99	java/lang/String:getBytes	()[B
    //   378: astore 7
    //   380: aload 11
    //   382: getfield 143	com/megvii/apo/b/b:a	[B
    //   385: astore 6
    //   387: aload 11
    //   389: invokevirtual 73	com/megvii/apo/b/b:a	()V
    //   392: aload 11
    //   394: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   397: astore 12
    //   399: new 112	java/lang/StringBuilder
    //   402: astore 8
    //   404: aload 8
    //   406: ldc -111
    //   408: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   411: aload 8
    //   413: ldc -109
    //   415: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload 8
    //   421: ldc 101
    //   423: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   426: pop
    //   427: aload 12
    //   429: aload 8
    //   431: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   434: invokevirtual 99	java/lang/String:getBytes	()[B
    //   437: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   440: aload 11
    //   442: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   445: ldc -107
    //   447: ldc -105
    //   449: invokestatic 85	com/megvii/apo/b/b:a	(Ljava/lang/String;Ljava/lang/String;)[B
    //   452: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   455: aload 11
    //   457: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   460: aload 6
    //   462: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   465: aload 11
    //   467: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   470: aload 7
    //   472: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   475: aload 11
    //   477: getfield 76	com/megvii/apo/b/b:c	Ljava/io/ByteArrayOutputStream;
    //   480: ldc 101
    //   482: invokevirtual 99	java/lang/String:getBytes	()[B
    //   485: invokevirtual 91	java/io/ByteArrayOutputStream:write	([B)V
    //   488: goto +10 -> 498
    //   491: astore 6
    //   493: aload 6
    //   495: invokevirtual 138	java/io/IOException:printStackTrace	()V
    //   498: aload 11
    //   500: invokevirtual 153	com/megvii/apo/b/b:b	()[B
    //   503: astore 7
    //   505: new 95	java/lang/String
    //   508: astore 6
    //   510: aload 6
    //   512: aload 7
    //   514: invokespecial 155	java/lang/String:<init>	([B)V
    //   517: new 157	java/util/HashMap
    //   520: astore 7
    //   522: aload 7
    //   524: invokespecial 158	java/util/HashMap:<init>	()V
    //   527: new 112	java/lang/StringBuilder
    //   530: astore 8
    //   532: aload 8
    //   534: ldc -96
    //   536: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   539: aload 8
    //   541: aload 11
    //   543: getfield 120	com/megvii/apo/b/b:b	Ljava/lang/String;
    //   546: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload 7
    //   552: ldc -94
    //   554: aload 8
    //   556: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   559: invokeinterface 168 3 0
    //   564: pop
    //   565: aload_0
    //   566: getfield 53	com/megvii/apo/util/h:e	Lcom/megvii/apo/b/a;
    //   569: astore 8
    //   571: aload 8
    //   573: ldc -86
    //   575: aload 6
    //   577: aload 7
    //   579: aconst_null
    //   580: invokevirtual 173	com/megvii/apo/b/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    //   583: astore 6
    //   585: aload 6
    //   587: ifnonnull +6 -> 593
    //   590: goto +42 -> 632
    //   593: aload 6
    //   595: invokestatic 176	com/megvii/apo/b/a:a	(Ljava/lang/String;)Z
    //   598: ifeq +34 -> 632
    //   601: aload 10
    //   603: invokevirtual 180	java/io/File:delete	()Z
    //   606: pop
    //   607: goto +25 -> 632
    //   610: astore 6
    //   612: aload 7
    //   614: invokestatic 135	com/megvii/apo/b/b:a	(Ljava/io/Closeable;)V
    //   617: aload 6
    //   619: athrow
    //   620: astore 6
    //   622: goto +5 -> 627
    //   625: astore 6
    //   627: aload 6
    //   629: invokestatic 185	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   632: iinc 2 1
    //   635: goto -552 -> 83
    //   638: iload_1
    //   639: iconst_3
    //   640: if_icmpge +9 -> 649
    //   643: aload 5
    //   645: invokevirtual 180	java/io/File:delete	()Z
    //   648: pop
    //   649: return
    //   650: return
    //   651: aload 5
    //   653: invokestatic 185	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   656: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	657	0	this	h
    //   0	657	1	paramInt	int
    //   82	551	2	i	int
    //   80	6	3	j	int
    //   203	21	4	k	int
    //   9	1	5	localFile1	File
    //   14	1	5	localThrowable1	Throwable
    //   28	624	5	localFile2	File
    //   107	229	6	localFileInputStream	java.io.FileInputStream
    //   348	1	6	localObject1	Object
    //   359	102	6	arrayOfByte	byte[]
    //   491	3	6	localIOException1	java.io.IOException
    //   508	86	6	str	String
    //   610	8	6	localObject2	Object
    //   620	1	6	localThrowable2	Throwable
    //   625	3	6	localThrowable3	Throwable
    //   118	495	7	localObject3	Object
    //   190	126	8	localObject4	Object
    //   343	1	8	localIOException2	java.io.IOException
    //   356	10	8	localIOException3	java.io.IOException
    //   402	170	8	localObject5	Object
    //   61	28	9	arrayOfFile	File[]
    //   92	510	10	localFile3	File
    //   97	445	11	localb	com.megvii.apo.b.b
    //   397	31	12	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    // Exception table:
    //   from	to	target	type
    //   5	11	14	java/lang/Throwable
    //   24	30	14	java/lang/Throwable
    //   38	44	14	java/lang/Throwable
    //   56	63	14	java/lang/Throwable
    //   68	74	14	java/lang/Throwable
    //   77	81	14	java/lang/Throwable
    //   627	632	14	java/lang/Throwable
    //   643	649	14	java/lang/Throwable
    //   120	125	343	java/io/IOException
    //   129	147	343	java/io/IOException
    //   151	164	343	java/io/IOException
    //   168	181	343	java/io/IOException
    //   185	192	343	java/io/IOException
    //   196	205	343	java/io/IOException
    //   215	228	343	java/io/IOException
    //   235	248	343	java/io/IOException
    //   252	257	343	java/io/IOException
    //   261	268	343	java/io/IOException
    //   272	283	343	java/io/IOException
    //   287	295	343	java/io/IOException
    //   299	306	343	java/io/IOException
    //   310	323	343	java/io/IOException
    //   327	335	343	java/io/IOException
    //   104	116	348	finally
    //   104	116	356	java/io/IOException
    //   387	488	491	java/io/IOException
    //   120	125	610	finally
    //   129	147	610	finally
    //   151	164	610	finally
    //   168	181	610	finally
    //   185	192	610	finally
    //   196	205	610	finally
    //   215	228	610	finally
    //   235	248	610	finally
    //   252	257	610	finally
    //   261	268	610	finally
    //   272	283	610	finally
    //   287	295	610	finally
    //   299	306	610	finally
    //   310	323	610	finally
    //   327	335	610	finally
    //   365	370	610	finally
    //   571	585	620	java/lang/Throwable
    //   593	607	620	java/lang/Throwable
    //   612	620	620	java/lang/Throwable
    //   94	104	625	java/lang/Throwable
    //   335	340	625	java/lang/Throwable
    //   373	387	625	java/lang/Throwable
    //   387	488	625	java/lang/Throwable
    //   493	498	625	java/lang/Throwable
    //   498	571	625	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */