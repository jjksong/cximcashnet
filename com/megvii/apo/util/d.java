package com.megvii.apo.util;

import android.os.Environment;

public final class d
{
  /* Error */
  public static void a(String paramString)
  {
    // Byte code:
    //   0: invokestatic 11	com/megvii/apo/util/d:a	()Z
    //   3: ifeq +98 -> 101
    //   6: new 13	java/io/File
    //   9: astore_3
    //   10: aload_3
    //   11: getstatic 19	com/megvii/apo/util/j:b	Ljava/lang/String;
    //   14: invokespecial 22	java/io/File:<init>	(Ljava/lang/String;)V
    //   17: new 13	java/io/File
    //   20: astore_1
    //   21: new 24	java/lang/StringBuilder
    //   24: astore_2
    //   25: aload_2
    //   26: invokespecial 27	java/lang/StringBuilder:<init>	()V
    //   29: aload_2
    //   30: aload_3
    //   31: invokevirtual 31	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload_2
    //   36: ldc 33
    //   38: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: aload_1
    //   43: aload_2
    //   44: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   47: invokespecial 22	java/io/File:<init>	(Ljava/lang/String;)V
    //   50: aload_3
    //   51: invokevirtual 43	java/io/File:exists	()Z
    //   54: ifne +8 -> 62
    //   57: aload_3
    //   58: invokevirtual 46	java/io/File:mkdirs	()Z
    //   61: pop
    //   62: aload_1
    //   63: invokevirtual 43	java/io/File:exists	()Z
    //   66: ifne +8 -> 74
    //   69: aload_1
    //   70: invokevirtual 49	java/io/File:createNewFile	()Z
    //   73: pop
    //   74: new 51	java/io/OutputStreamWriter
    //   77: astore_2
    //   78: new 53	java/io/FileOutputStream
    //   81: astore_3
    //   82: aload_3
    //   83: aload_1
    //   84: invokespecial 56	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   87: aload_2
    //   88: aload_3
    //   89: invokespecial 59	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
    //   92: aload_2
    //   93: aload_0
    //   94: invokevirtual 62	java/io/OutputStreamWriter:write	(Ljava/lang/String;)V
    //   97: aload_2
    //   98: invokevirtual 65	java/io/OutputStreamWriter:close	()V
    //   101: return
    //   102: astore_0
    //   103: goto +9 -> 112
    //   106: astore_0
    //   107: aload_0
    //   108: invokestatic 70	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   111: return
    //   112: aload_0
    //   113: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	114	0	paramString	String
    //   20	64	1	localFile	java.io.File
    //   24	74	2	localObject1	Object
    //   9	80	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   0	62	102	finally
    //   62	74	102	finally
    //   74	101	102	finally
    //   107	111	102	finally
    //   0	62	106	java/lang/Throwable
    //   62	74	106	java/lang/Throwable
    //   74	101	106	java/lang/Throwable
  }
  
  public static boolean a()
  {
    return Environment.getExternalStorageState().equals("mounted");
  }
  
  /* Error */
  public static String b()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_1
    //   3: astore_0
    //   4: invokestatic 11	com/megvii/apo/util/d:a	()Z
    //   7: ifeq +99 -> 106
    //   10: new 13	java/io/File
    //   13: astore_2
    //   14: new 24	java/lang/StringBuilder
    //   17: astore_0
    //   18: aload_0
    //   19: invokespecial 27	java/lang/StringBuilder:<init>	()V
    //   22: aload_0
    //   23: getstatic 19	com/megvii/apo/util/j:b	Ljava/lang/String;
    //   26: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload_0
    //   31: ldc 33
    //   33: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_2
    //   38: aload_0
    //   39: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   42: invokespecial 22	java/io/File:<init>	(Ljava/lang/String;)V
    //   45: aload_2
    //   46: invokevirtual 43	java/io/File:exists	()Z
    //   49: ifne +8 -> 57
    //   52: ldc 86
    //   54: invokestatic 88	com/megvii/apo/util/e:a	(Ljava/lang/String;)V
    //   57: new 90	java/io/FileInputStream
    //   60: astore_0
    //   61: aload_0
    //   62: aload_2
    //   63: invokespecial 91	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   66: aload_0
    //   67: invokevirtual 95	java/io/FileInputStream:available	()I
    //   70: newarray <illegal type>
    //   72: astore_2
    //   73: aload_0
    //   74: aload_2
    //   75: invokevirtual 99	java/io/FileInputStream:read	([B)I
    //   78: pop
    //   79: aload_0
    //   80: invokevirtual 100	java/io/FileInputStream:close	()V
    //   83: new 80	java/lang/String
    //   86: astore_0
    //   87: aload_0
    //   88: aload_2
    //   89: invokespecial 103	java/lang/String:<init>	([B)V
    //   92: goto +14 -> 106
    //   95: astore_0
    //   96: goto +12 -> 108
    //   99: astore_0
    //   100: aload_0
    //   101: invokestatic 70	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   104: aload_1
    //   105: astore_0
    //   106: aload_0
    //   107: areturn
    //   108: aload_0
    //   109: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   3	85	0	localObject1	Object
    //   95	1	0	localObject2	Object
    //   99	2	0	localThrowable	Throwable
    //   105	4	0	localObject3	Object
    //   1	104	1	localObject4	Object
    //   13	76	2	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   4	57	95	finally
    //   57	92	95	finally
    //   100	104	95	finally
    //   4	57	99	java/lang/Throwable
    //   57	92	99	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */