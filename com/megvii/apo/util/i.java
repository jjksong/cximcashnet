package com.megvii.apo.util;

import android.content.Context;
import com.megvii.apo.m;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class i
{
  public File a;
  private File b;
  private File c;
  private Context d;
  private m e;
  
  public i(Context paramContext, m paramm)
  {
    this.d = paramContext;
    this.e = paramm;
    this.b = new File(paramContext.getFilesDir(), "a");
    this.c = new File(paramContext.getFilesDir(), "d");
    this.a = new File(paramContext.getFilesDir(), "p");
  }
  
  public final String a()
  {
    try
    {
      Object localObject3 = new java/util/Date;
      ((Date)localObject3).<init>(System.currentTimeMillis());
      Object localObject1 = new java/text/SimpleDateFormat;
      ((SimpleDateFormat)localObject1).<init>("yyyyMMddHHmmssSSS", Locale.CHINA);
      localObject1 = ((SimpleDateFormat)localObject1).format((Date)localObject3);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      ((StringBuilder)localObject3).append((String)localObject1);
      ((StringBuilder)localObject3).append(".txt");
      localObject1 = ((StringBuilder)localObject3).toString();
      return (String)localObject1;
    }
    finally
    {
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */