package com.megvii.apo.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import com.megvii.apo.b.a;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class g
{
  private static final String[] a = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
  private static volatile boolean b = false;
  
  public static b.a a(Context paramContext, ApplicationInfo paramApplicationInfo)
  {
    Object localObject = paramContext.getPackageManager();
    locala = new b.a();
    locala.b = paramApplicationInfo.packageName;
    locala.c = paramApplicationInfo.loadLabel((PackageManager)localObject).toString();
    try
    {
      locala.k = a(paramApplicationInfo.loadIcon((PackageManager)localObject));
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    locala.n = a(paramContext, paramApplicationInfo.packageName);
    int i = paramApplicationInfo.flags;
    int j = 0;
    if ((i & 0x80) != 0) {
      i = 1;
    } else if ((paramApplicationInfo.flags & 0x1) == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      locala.e = "1";
    } else {
      locala.e = "0";
    }
    try
    {
      paramApplicationInfo = ((PackageManager)localObject).getPackageInfo(locala.b, 4160);
      locala.l = paramApplicationInfo.versionName;
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append(paramApplicationInfo.versionCode);
      locala.d = paramContext.toString();
      localObject = paramApplicationInfo.signatures;
      CertificateFactory localCertificateFactory = CertificateFactory.getInstance("X.509");
      paramContext = new java/io/ByteArrayInputStream;
      paramContext.<init>(localObject[0].toByteArray());
      paramContext = (X509Certificate)localCertificateFactory.generateCertificate(paramContext);
      locala.g = paramContext.getNotBefore().toString();
      locala.h = paramContext.getNotAfter().toString();
      locala.i = paramContext.getSubjectDN().getName();
      locala.j = paramContext.getPublicKey().toString();
      paramContext = null;
      localObject = paramApplicationInfo.signatures;
      if (localObject != null) {
        paramContext = a(localObject[0].toByteArray());
      }
      locala.f = paramContext;
      localObject = paramApplicationInfo.requestedPermissions;
      if (localObject != null)
      {
        paramContext = "";
        for (i = j; i < localObject.length; i++)
        {
          paramApplicationInfo = new java/lang/StringBuilder;
          paramApplicationInfo.<init>();
          paramApplicationInfo.append(paramContext);
          paramApplicationInfo.append(localObject[i]);
          paramApplicationInfo = paramApplicationInfo.toString();
          if (c.a("AqP20/fWurXwemeh6GnJUB7O/YtQuFtmdkBtxz3rIIMu1MplP5spkT/2PaKdiFSa").equals(localObject[i])) {
            locala.m = "1";
          }
          paramContext = paramApplicationInfo;
          if (i != localObject.length - 1)
          {
            paramContext = new java/lang/StringBuilder;
            paramContext.<init>();
            paramContext.append(paramApplicationInfo);
            paramContext.append(",");
            paramContext = paramContext.toString();
          }
        }
      }
      return locala;
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
    }
  }
  
  /* Error */
  private static String a(android.graphics.drawable.Drawable paramDrawable)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnonnull +9 -> 13
    //   7: ldc 2
    //   9: monitorexit
    //   10: ldc -44
    //   12: areturn
    //   13: new 233	java/lang/StringBuffer
    //   16: astore 5
    //   18: aload 5
    //   20: invokespecial 234	java/lang/StringBuffer:<init>	()V
    //   23: aload_0
    //   24: invokevirtual 240	android/graphics/drawable/Drawable:getIntrinsicWidth	()I
    //   27: istore_1
    //   28: aload_0
    //   29: invokevirtual 243	android/graphics/drawable/Drawable:getIntrinsicHeight	()I
    //   32: istore_2
    //   33: aload_0
    //   34: invokevirtual 246	android/graphics/drawable/Drawable:getOpacity	()I
    //   37: iconst_m1
    //   38: if_icmpeq +11 -> 49
    //   41: getstatic 252	android/graphics/Bitmap$Config:ARGB_8888	Landroid/graphics/Bitmap$Config;
    //   44: astore 4
    //   46: goto +8 -> 54
    //   49: getstatic 255	android/graphics/Bitmap$Config:RGB_565	Landroid/graphics/Bitmap$Config;
    //   52: astore 4
    //   54: iload_1
    //   55: iload_2
    //   56: aload 4
    //   58: invokestatic 261	android/graphics/Bitmap:createBitmap	(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   61: astore 4
    //   63: new 263	android/graphics/Canvas
    //   66: astore 6
    //   68: aload 6
    //   70: aload 4
    //   72: invokespecial 266	android/graphics/Canvas:<init>	(Landroid/graphics/Bitmap;)V
    //   75: aload_0
    //   76: invokevirtual 240	android/graphics/drawable/Drawable:getIntrinsicWidth	()I
    //   79: istore_2
    //   80: aload_0
    //   81: invokevirtual 243	android/graphics/drawable/Drawable:getIntrinsicHeight	()I
    //   84: istore_3
    //   85: iconst_0
    //   86: istore_1
    //   87: aload_0
    //   88: iconst_0
    //   89: iconst_0
    //   90: iload_2
    //   91: iload_3
    //   92: invokevirtual 270	android/graphics/drawable/Drawable:setBounds	(IIII)V
    //   95: aload_0
    //   96: aload 6
    //   98: invokevirtual 274	android/graphics/drawable/Drawable:draw	(Landroid/graphics/Canvas;)V
    //   101: aload 4
    //   103: invokevirtual 277	android/graphics/Bitmap:getWidth	()I
    //   106: istore_3
    //   107: aload 4
    //   109: invokevirtual 280	android/graphics/Bitmap:getHeight	()I
    //   112: istore_2
    //   113: new 282	java/io/ByteArrayOutputStream
    //   116: astore_0
    //   117: aload_0
    //   118: iload_3
    //   119: iload_2
    //   120: imul
    //   121: iconst_4
    //   122: imul
    //   123: invokespecial 285	java/io/ByteArrayOutputStream:<init>	(I)V
    //   126: aload 4
    //   128: getstatic 291	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   131: bipush 100
    //   133: aload_0
    //   134: invokevirtual 295	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   137: pop
    //   138: aload_0
    //   139: invokevirtual 296	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   142: astore_0
    //   143: ldc_w 298
    //   146: invokestatic 303	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   149: aload_0
    //   150: invokevirtual 307	java/security/MessageDigest:digest	([B)[B
    //   153: astore_0
    //   154: iload_1
    //   155: aload_0
    //   156: arraylength
    //   157: if_icmpge +46 -> 203
    //   160: aload_0
    //   161: iload_1
    //   162: baload
    //   163: sipush 255
    //   166: iand
    //   167: istore_2
    //   168: iload_2
    //   169: bipush 16
    //   171: if_icmpge +11 -> 182
    //   174: aload 5
    //   176: ldc 16
    //   178: invokevirtual 310	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   181: pop
    //   182: aload 5
    //   184: iload_2
    //   185: invokestatic 316	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   188: invokevirtual 310	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   191: pop
    //   192: iinc 1 1
    //   195: goto -41 -> 154
    //   198: astore_0
    //   199: aload_0
    //   200: invokestatic 98	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   203: aload 5
    //   205: invokevirtual 317	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   208: astore_0
    //   209: ldc 2
    //   211: monitorexit
    //   212: aload_0
    //   213: areturn
    //   214: astore_0
    //   215: ldc 2
    //   217: monitorexit
    //   218: aload_0
    //   219: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	220	0	paramDrawable	android.graphics.drawable.Drawable
    //   27	166	1	i	int
    //   32	153	2	j	int
    //   84	37	3	k	int
    //   44	83	4	localObject	Object
    //   16	188	5	localStringBuffer	StringBuffer
    //   66	31	6	localCanvas	android.graphics.Canvas
    // Exception table:
    //   from	to	target	type
    //   23	46	198	java/lang/Throwable
    //   49	54	198	java/lang/Throwable
    //   54	85	198	java/lang/Throwable
    //   87	154	198	java/lang/Throwable
    //   154	160	198	java/lang/Throwable
    //   174	182	198	java/lang/Throwable
    //   182	192	198	java/lang/Throwable
    //   13	23	214	finally
    //   23	46	214	finally
    //   49	54	214	finally
    //   54	85	214	finally
    //   87	154	214	finally
    //   154	160	214	finally
    //   174	182	214	finally
    //   182	192	214	finally
    //   199	203	214	finally
    //   203	209	214	finally
  }
  
  public static String a(String paramString)
  {
    Object localObject4 = "";
    String str = "";
    Object localObject6;
    Object localObject2;
    try
    {
      Object localObject1 = new java/io/FileReader;
      ((FileReader)localObject1).<init>(c.a("wPDUJ11+x2IHKDM6+uV7hYBU52Tcfs5Ngf1ldaTFQjg="));
      localObject6 = new java/io/BufferedReader;
      ((BufferedReader)localObject6).<init>((Reader)localObject1);
      localObject1 = localObject4;
      if (Boolean.valueOf(((BufferedReader)localObject6).readLine().toLowerCase().contentEquals("sd")) != null) {
        localObject1 = c.a("wPDUJ11+x2IHKDM6+uV7hZ62WZzYwfFJsfxj7LRF2pQ=");
      }
    }
    catch (Throwable localThrowable1)
    {
      e.a(localThrowable1);
      localObject2 = localObject4;
    }
    Object localObject5;
    try
    {
      localObject4 = new java/io/FileReader;
      ((FileReader)localObject4).<init>(c.a("wPDUJ11+x2IHKDM6+uV7heIg4oil0gbKqBAzlVpVfLg="));
      localObject6 = new java/io/BufferedReader;
      ((BufferedReader)localObject6).<init>((Reader)localObject4);
      localObject4 = localObject2;
      if (Boolean.valueOf(((BufferedReader)localObject6).readLine().toLowerCase().contentEquals("sd")) != null) {
        localObject4 = c.a("wPDUJ11+x2IHKDM6+uV7hQkpy+AG0ns1CnO/3Hk3J5E=");
      }
    }
    catch (Throwable localThrowable2)
    {
      e.a(localThrowable2);
      localObject5 = localObject2;
    }
    Object localObject3;
    try
    {
      localObject2 = new java/io/FileReader;
      ((FileReader)localObject2).<init>(c.a("wPDUJ11+x2IHKDM6+uV7hQqfndsGDU/5ZANnxwwyS0c="));
      localObject6 = new java/io/BufferedReader;
      ((BufferedReader)localObject6).<init>((Reader)localObject2);
      localObject2 = localObject5;
      if (Boolean.valueOf(((BufferedReader)localObject6).readLine().toLowerCase().contentEquals("sd")) != null) {
        localObject2 = c.a("wPDUJ11+x2IHKDM6+uV7hejqGMjNhNNe/sXf/r5uhTo=");
      }
    }
    catch (Exception localException)
    {
      e.a(localException);
      localObject3 = localObject5;
    }
    try
    {
      localObject5 = new java/io/FileReader;
      localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>();
      ((StringBuilder)localObject6).append((String)localObject3);
      ((StringBuilder)localObject6).append(paramString);
      ((FileReader)localObject5).<init>(((StringBuilder)localObject6).toString());
      paramString = new java/io/BufferedReader;
      paramString.<init>((Reader)localObject5);
      paramString = paramString.readLine();
    }
    catch (Exception paramString)
    {
      e.a(paramString);
      paramString = str;
    }
    return paramString;
  }
  
  private static String a(byte[] paramArrayOfByte)
  {
    Object localObject = null;
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 0))
    {
      try
      {
        paramArrayOfByte = b(MessageDigest.getInstance("MD5").digest(paramArrayOfByte));
      }
      catch (Throwable paramArrayOfByte)
      {
        paramArrayOfByte.printStackTrace();
        paramArrayOfByte = (byte[])localObject;
      }
      return paramArrayOfByte;
    }
    return null;
  }
  
  private static boolean a(Context paramContext, String paramString)
  {
    try
    {
      paramContext = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
      int i = android.os.Process.myPid();
      Iterator localIterator = paramContext.iterator();
      while (localIterator.hasNext())
      {
        paramContext = (ActivityManager.RunningAppProcessInfo)localIterator.next();
        if ((paramContext != null) && (paramContext.processName != null) && (paramContext.pid == i) && (paramContext.processName.contains(paramString))) {
          return true;
        }
      }
      boolean bool = b(paramString);
      if (bool) {
        return true;
      }
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
    }
    return false;
  }
  
  private static String b(byte[] paramArrayOfByte)
  {
    localStringBuffer = new StringBuffer();
    int i = 0;
    try
    {
      while (i < paramArrayOfByte.length)
      {
        int k = paramArrayOfByte[i];
        int j = k;
        if (k < 0) {
          j = k + 256;
        }
        k = j / 16;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(a[k]);
        localStringBuilder.append(a[(j % 16)]);
        localStringBuffer.append(localStringBuilder.toString());
        i++;
      }
      return localStringBuffer.toString();
    }
    catch (Throwable paramArrayOfByte)
    {
      e.a(paramArrayOfByte);
    }
  }
  
  private static boolean b(String paramString)
  {
    paramString = new Thread()
    {
      public final void run()
      {
        super.run();
        try
        {
          Object localObject = Runtime.getRuntime().exec("ps\n");
          BufferedReader localBufferedReader = new java/io/BufferedReader;
          InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
          localInputStreamReader.<init>(((Process)localObject).getInputStream());
          localBufferedReader.<init>(localInputStreamReader);
          do
          {
            localObject = localBufferedReader.readLine();
            if (localObject == null) {
              break;
            }
          } while (!((String)localObject).contains(this.a));
          g.a();
          localBufferedReader.close();
          return;
        }
        catch (Throwable localThrowable)
        {
          e.a(localThrowable);
        }
      }
    };
    try
    {
      paramString.start();
      for (int i = 0; (i < 10) && (!b); i++) {
        Thread.sleep(100L);
      }
      paramString.interrupt();
    }
    catch (Throwable paramString)
    {
      e.a(paramString);
    }
    return b;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */