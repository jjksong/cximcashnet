package com.megvii.apo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class k
{
  public static void a(Context paramContext, String paramString, Object paramObject)
  {
    try
    {
      String str = paramObject.getClass().getSimpleName();
      paramContext = paramContext.getSharedPreferences("m_p_f", 0).edit();
      if ("Integer".equals(str)) {
        paramContext.putInt(paramString, ((Integer)paramObject).intValue());
      } else if ("Boolean".equals(str)) {
        paramContext.putBoolean(paramString, ((Boolean)paramObject).booleanValue());
      } else if ("String".equals(str)) {
        paramContext.putString(paramString, (String)paramObject);
      } else if ("Float".equals(str)) {
        paramContext.putFloat(paramString, ((Float)paramObject).floatValue());
      } else if ("Long".equals(str)) {
        paramContext.putLong(paramString, ((Long)paramObject).longValue());
      }
      paramContext.apply();
      return;
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
    }
  }
  
  public static Object b(Context paramContext, String paramString, Object paramObject)
  {
    try
    {
      String str = paramObject.getClass().getSimpleName();
      paramContext = paramContext.getSharedPreferences("m_p_f", 0);
      if ("Integer".equals(str)) {
        return Integer.valueOf(paramContext.getInt(paramString, ((Integer)paramObject).intValue()));
      }
      if ("Boolean".equals(str)) {
        return Boolean.valueOf(paramContext.getBoolean(paramString, Boolean.valueOf(paramObject.toString()).booleanValue()));
      }
      if ("String".equals(str)) {
        return paramContext.getString(paramString, (String)paramObject);
      }
      if ("Float".equals(str)) {
        return Float.valueOf(paramContext.getFloat(paramString, ((Float)paramObject).floatValue()));
      }
      if ("Long".equals(str))
      {
        long l = paramContext.getLong(paramString, ((Long)paramObject).longValue());
        return Long.valueOf(l);
      }
    }
    catch (Throwable paramContext)
    {
      e.a(paramContext);
    }
    return paramObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */