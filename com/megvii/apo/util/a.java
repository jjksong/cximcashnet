package com.megvii.apo.util;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class a
{
  public static IvParameterSpec a;
  public static SecretKeySpec b;
  
  public static String a()
  {
    try
    {
      Object localObject = KeyGenerator.getInstance("AES");
      ((KeyGenerator)localObject).init(64);
      byte[] arrayOfByte = ((KeyGenerator)localObject).generateKey().getEncoded();
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      for (int i = 0; i < arrayOfByte.length; i++)
      {
        localObject = Integer.toHexString(arrayOfByte[i]);
        if (((String)localObject).length() > 3)
        {
          localStringBuffer.append(((String)localObject).substring(6));
        }
        else if (((String)localObject).length() < 2)
        {
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>("0");
          localStringBuilder.append((String)localObject);
          localStringBuffer.append(localStringBuilder.toString());
        }
        else
        {
          localStringBuffer.append((String)localObject);
        }
      }
      localObject = localStringBuffer.toString();
      return (String)localObject;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      localNoSuchAlgorithmException.printStackTrace();
    }
    return "";
  }
  
  public static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    try
    {
      byte[] arrayOfByte = new byte[16];
      for (int i = 0; (i < paramArrayOfByte2.length) && (i < 16); i++) {
        arrayOfByte[i] = paramArrayOfByte2[i];
      }
      Object localObject = new javax/crypto/spec/SecretKeySpec;
      ((SecretKeySpec)localObject).<init>(arrayOfByte, "AES");
      b = (SecretKeySpec)localObject;
      localObject = new javax/crypto/spec/IvParameterSpec;
      ((IvParameterSpec)localObject).<init>(paramArrayOfByte2);
      a = (IvParameterSpec)localObject;
    }
    catch (Throwable paramArrayOfByte2)
    {
      e.a(paramArrayOfByte2);
    }
    try
    {
      paramArrayOfByte2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
      paramArrayOfByte2.init(2, b, a);
      paramArrayOfByte1 = paramArrayOfByte2.doFinal(paramArrayOfByte1);
    }
    catch (Throwable paramArrayOfByte1)
    {
      e.a(paramArrayOfByte1);
      paramArrayOfByte1 = null;
    }
    return paramArrayOfByte1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/util/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */