package com.megvii.apo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.media.AudioManager;
import android.provider.Settings.System;
import android.util.DisplayMetrics;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.util.Map;

public final class i
  extends n
{
  public i(Context paramContext)
  {
    super(paramContext);
  }
  
  private String b()
  {
    ContentResolver localContentResolver = this.a.getContentResolver();
    int i;
    try
    {
      i = Settings.System.getInt(localContentResolver, "screen_brightness");
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
      i = 0;
    }
    return String.valueOf(i);
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.p != 1) {
      return;
    }
    try
    {
      Object localObject = this.a.getResources().getDisplayMetrics();
      int i = ((DisplayMetrics)localObject).widthPixels;
      int j = ((DisplayMetrics)localObject).heightPixels;
      paramMap.put("101020003", String.valueOf(Math.sqrt(Math.pow(i / ((DisplayMetrics)localObject).xdpi, 2.0D) + Math.pow(j / ((DisplayMetrics)localObject).ydpi, 2.0D))));
      DisplayMetrics localDisplayMetrics = this.a.getResources().getDisplayMetrics();
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(localDisplayMetrics.densityDpi);
      paramMap.put("101020004", ((StringBuilder)localObject).toString());
      localDisplayMetrics = this.a.getResources().getDisplayMetrics();
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(localDisplayMetrics.xdpi);
      ((StringBuilder)localObject).append("/");
      ((StringBuilder)localObject).append(localDisplayMetrics.ydpi);
      paramMap.put("101020005", ((StringBuilder)localObject).toString());
      paramMap.put("101020006", b());
      paramMap.put("101020007", String.valueOf(((AudioManager)this.a.getSystemService("audio")).getStreamVolume(1)));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */