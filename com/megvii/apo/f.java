package com.megvii.apo;

import android.content.Context;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.os.Build.VERSION;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class f
  extends n
{
  public f(Context paramContext)
  {
    super(paramContext);
  }
  
  private static String b(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    if (Build.VERSION.SDK_INT >= 21)
    {
      MediaCodecInfo[] arrayOfMediaCodecInfo = new MediaCodecList(1).getCodecInfos();
      for (int i = 0; i < arrayOfMediaCodecInfo.length; i++) {
        localArrayList.add(arrayOfMediaCodecInfo[i].getName());
      }
    }
    if (localArrayList.contains(paramString)) {
      return "1";
    }
    return "0";
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.N != 1) {
      return;
    }
    try
    {
      paramMap.put("101101001", b(c.a("u5hTnYI7soskAg14WypO3IGhKCIVZob4cfCBx37anVs=")));
      paramMap.put("101101002", b(c.a("lOXLieygp4YR1jA5trYx1yOqRDNx1Nked1RLsKF27fQ=")));
      paramMap.put("101101003", b(c.a("ePRJr2mrsZRBUpIJx4BjOv6si9w09+arixbmf2glnFw=")));
      paramMap.put("101101004", b(c.a("P9XzvZY8Z2/05Nt6t5Ng8Yj/cDWPMU/aKCBmVnwiUvM=")));
      paramMap.put("101101005", b(c.a("A+7lKC/2j0a4L22Es3P45+m+i2BU5GkkzQxtwQTPTU8=")));
      paramMap.put("101101006", b(c.a("A+7lKC/2j0a4L22Es3P45yksskOaGE/q4pv/UtRrhoc=")));
      paramMap.put("101101007", b(c.a("9Bh8DIKEumM/+56wE2u5GLj0lssZZVpVKZGxNpTR/40=")));
      paramMap.put("101101008", b(c.a("2OhTUPcGhfsAJ/xVkXdnc8drqNeS6kVh5otvVYnRMiM=")));
      paramMap.put("101101009", b(c.a("WnmNktfJaMdo53DrcpbyM/9SEYqRaEFGPdHp1jAmuMA=")));
      paramMap.put("101111005", b(c.a("64aBUmTnX/xQUcE2+w3AQ/gByLIZnwm9LoGcdhjvnbI=")));
      paramMap.put("101111006", b(c.a("lOXLieygp4YR1jA5trYx1wRdC4IUgfNMi9Dbc5aw0qI=")));
      paramMap.put("101111007", b(c.a("ePRJr2mrsZRBUpIJx4BjOuHQtFCR2rHwzeNUE+u/PCE=")));
      paramMap.put("101111008", b(c.a("lmOMdvJ8ZXmSGG1EnCc8zex/WD6nfHSka5BcbAjGJfo=")));
      paramMap.put("101101010", b(c.a("C2jq9d2zbMdj/hBeMftVqMGJeUFyQwpvQ2hULJJ4Y4w=")));
      paramMap.put("101112001", b(c.a("X3YMA/SuLhLhFFiBEyVww4l3UGqGtGikeHHshFawit8=")));
      paramMap.put("101102001", b(c.a("XIz+N97ySdVghg2dS6hTxQJD7TANSwZjW21aBTo6ydc=")));
      paramMap.put("101102002", b(c.a("XIz+N97ySdVghg2dS6hTxa9FRgaaEpOv2A981kwUU7Y=")));
      paramMap.put("101102003", b(c.a("XIz+N97ySdVghg2dS6hTxdzmeIFKFtU5ssyom+SfeLo=")));
      paramMap.put("101102004", b(c.a("XIz+N97ySdVghg2dS6hTxQcX/REyUkjhrQJerLeg+hY=")));
      paramMap.put("101102005", b(c.a("XIz+N97ySdVghg2dS6hTxXHSt9nOPvSTHtEvztWlb/0=")));
      paramMap.put("101102006", b(c.a("XIz+N97ySdVghg2dS6hTxR0zBgJnmeMP30wuHaiSZ0M=")));
      paramMap.put("101102007", b(c.a("3UmTlQzUa4DRi4W95/MHjqO7YDdcsKDyzMESh7z1ng8=")));
      paramMap.put("101103001", b(c.a("Pi+r/TmNUYKSw4WWAQs5PJa7nZqckueESM+I/eu5pp4=")));
      paramMap.put("101101011", b(c.a("Thkn/2XwLyK3RmH7f65Wvrl584FpfS6VUbg9WnKlbVQ=")));
      paramMap.put("101101012", b(c.a("QorE+dAbomVhigTy4krIIURizQl6STDVo3lMtyTKrQs=")));
      paramMap.put("101101013", b(c.a("gMyMv8jc4ccCQcK9Gl/jyXMLcW9TOCrEgGwR7FedfBI=")));
      paramMap.put("101101014", b(c.a("gg29Vp9SbfFwOAcPX7IeU1NdcznH0/eNSDgfqIzb2zQ=")));
      paramMap.put("101101015", b(c.a("wzxx3gFVepzkoDFx+mnrJ1ja02RlN6T4zPky9OAckmY=")));
      paramMap.put("101101016", b(c.a("P4MomXF9exgVSB9U6hhMmMJTelnLQsvjgpBm+R4A4wE=")));
      paramMap.put("101111001", b(c.a("QorE+dAbomVhigTy4krIIf7asb7PrYecX7GaR53CPYw=")));
      paramMap.put("101111002", b(c.a("gMyMv8jc4ccCQcK9Gl/jyb6jCWYG+PMh9B2exroYzDM=")));
      paramMap.put("101111003", b(c.a("Thkn/2XwLyK3RmH7f65WviGeAMDBHvWMZ/FLteETJRE=")));
      paramMap.put("101111004", b(c.a("GHdlpn7ilgkS40eK9DIHTgLwPO8aIm3qoO2j9H38d2E=")));
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */