package com.megvii.apo;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.megvii.apo.util.DeltaEncode;
import com.megvii.apo.util.b;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.g;
import com.megvii.apo.util.j;
import com.megvii.apo.util.l;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.Map;

public final class k
  extends n
{
  public k(Context paramContext)
  {
    super(paramContext);
  }
  
  private static String a(Reader paramReader)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      char[] arrayOfChar = new char['က'];
      for (int i = paramReader.read(arrayOfChar); i >= 0; i = paramReader.read(arrayOfChar)) {
        localStringBuilder.append(arrayOfChar, 0, i);
      }
      paramReader = localStringBuilder.toString();
      return paramReader;
    }
    catch (Throwable paramReader)
    {
      e.a(paramReader);
    }
    return null;
  }
  
  private String b()
  {
    try
    {
      String str = Settings.System.getString(this.a.getContentResolver(), "x_c_s");
      return str;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return "";
  }
  
  private static String c()
  {
    String str;
    try
    {
      Object localObject = Class.forName(c.a("md885t/s6Jva+pGPvGnEFfBS1vgobP6JTZSRcxRkgz4="));
      localObject = (String)((Class)localObject).getMethod("get", new Class[] { String.class, String.class }).invoke(localObject, new Object[] { c.a("6oMif8usps1JxYvp5yPYQSeC5FBBs2gc4xe5jDAHrgo="), "unknown" });
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
      str = null;
    }
    return str;
  }
  
  private static String d()
  {
    String str;
    try
    {
      Object localObject = Class.forName(c.a("md885t/s6Jva+pGPvGnEFfBS1vgobP6JTZSRcxRkgz4="));
      localObject = (String)((Class)localObject).getMethod("get", new Class[] { String.class, String.class }).invoke(localObject, new Object[] { c.a("4sQVmAtX177jNws5rvuGrvsIYxoVhCl7iylsf9sdGp4="), "unknown" });
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
      str = null;
    }
    return str;
  }
  
  /* Error */
  private static String e()
  {
    // Byte code:
    //   0: ldc 57
    //   2: astore_2
    //   3: new 97	java/io/BufferedReader
    //   6: astore 4
    //   8: new 99	java/io/FileReader
    //   11: astore_1
    //   12: aload_1
    //   13: ldc 101
    //   15: invokestatic 65	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   18: invokespecial 104	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   21: aload 4
    //   23: aload_1
    //   24: invokespecial 107	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   27: aload 4
    //   29: astore_1
    //   30: aload 4
    //   32: invokevirtual 110	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   35: astore_3
    //   36: aload_2
    //   37: astore_1
    //   38: aload_3
    //   39: ifnull +111 -> 150
    //   42: aload 4
    //   44: astore_1
    //   45: aload_3
    //   46: ldc 112
    //   48: invokevirtual 116	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   51: astore 5
    //   53: aload 5
    //   55: ifnull -28 -> 27
    //   58: aload 4
    //   60: astore_1
    //   61: aload 5
    //   63: arraylength
    //   64: iconst_4
    //   65: if_icmplt -38 -> 27
    //   68: aload 5
    //   70: iconst_0
    //   71: aaload
    //   72: astore_3
    //   73: aload 5
    //   75: iconst_3
    //   76: aaload
    //   77: astore 6
    //   79: aload 4
    //   81: astore_1
    //   82: aload 6
    //   84: ldc 118
    //   86: invokestatic 65	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   89: invokevirtual 122	java/lang/String:matches	(Ljava/lang/String;)Z
    //   92: istore_0
    //   93: iload_0
    //   94: ifeq -67 -> 27
    //   97: aload 4
    //   99: astore_1
    //   100: new 124	org/json/JSONObject
    //   103: astore 5
    //   105: aload 4
    //   107: astore_1
    //   108: aload 5
    //   110: invokespecial 125	org/json/JSONObject:<init>	()V
    //   113: aload 4
    //   115: astore_1
    //   116: aload 5
    //   118: aload_3
    //   119: aload 6
    //   121: invokevirtual 129	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   124: pop
    //   125: aload 4
    //   127: astore_1
    //   128: aload 5
    //   130: invokevirtual 130	org/json/JSONObject:toString	()Ljava/lang/String;
    //   133: astore_3
    //   134: aload_3
    //   135: astore_1
    //   136: goto +14 -> 150
    //   139: astore_3
    //   140: aload 4
    //   142: astore_1
    //   143: aload_3
    //   144: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   147: goto -120 -> 27
    //   150: aload_1
    //   151: astore_3
    //   152: aload 4
    //   154: invokevirtual 133	java/io/BufferedReader:close	()V
    //   157: goto +43 -> 200
    //   160: astore_1
    //   161: aload_1
    //   162: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   165: aload_3
    //   166: astore_1
    //   167: goto +33 -> 200
    //   170: astore_3
    //   171: goto +13 -> 184
    //   174: astore_1
    //   175: aconst_null
    //   176: astore_3
    //   177: goto +30 -> 207
    //   180: astore_3
    //   181: aconst_null
    //   182: astore 4
    //   184: aload 4
    //   186: astore_1
    //   187: aload_3
    //   188: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   191: aload_2
    //   192: astore_3
    //   193: aload 4
    //   195: invokevirtual 133	java/io/BufferedReader:close	()V
    //   198: aload_2
    //   199: astore_1
    //   200: aload_1
    //   201: areturn
    //   202: astore_2
    //   203: aload_1
    //   204: astore_3
    //   205: aload_2
    //   206: astore_1
    //   207: aload_3
    //   208: invokevirtual 133	java/io/BufferedReader:close	()V
    //   211: goto +8 -> 219
    //   214: astore_2
    //   215: aload_2
    //   216: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   219: aload_1
    //   220: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   92	2	0	bool	boolean
    //   11	140	1	localObject1	Object
    //   160	2	1	localThrowable1	Throwable
    //   166	1	1	localObject2	Object
    //   174	1	1	localObject3	Object
    //   186	34	1	localObject4	Object
    //   2	197	2	str1	String
    //   202	4	2	localObject5	Object
    //   214	2	2	localThrowable2	Throwable
    //   35	100	3	str2	String
    //   139	5	3	localThrowable3	Throwable
    //   151	15	3	localObject6	Object
    //   170	1	3	localThrowable4	Throwable
    //   176	1	3	localObject7	Object
    //   180	8	3	localThrowable5	Throwable
    //   192	16	3	localObject8	Object
    //   6	188	4	localBufferedReader	java.io.BufferedReader
    //   51	78	5	localObject9	Object
    //   77	43	6	localObject10	Object
    // Exception table:
    //   from	to	target	type
    //   100	105	139	java/lang/Throwable
    //   108	113	139	java/lang/Throwable
    //   116	125	139	java/lang/Throwable
    //   128	134	139	java/lang/Throwable
    //   152	157	160	java/lang/Throwable
    //   193	198	160	java/lang/Throwable
    //   30	36	170	java/lang/Throwable
    //   45	53	170	java/lang/Throwable
    //   61	68	170	java/lang/Throwable
    //   82	93	170	java/lang/Throwable
    //   143	147	170	java/lang/Throwable
    //   3	27	174	finally
    //   3	27	180	java/lang/Throwable
    //   30	36	202	finally
    //   45	53	202	finally
    //   61	68	202	finally
    //   82	93	202	finally
    //   100	105	202	finally
    //   108	113	202	finally
    //   116	125	202	finally
    //   128	134	202	finally
    //   143	147	202	finally
    //   187	191	202	finally
    //   207	211	214	java/lang/Throwable
  }
  
  private static String f()
  {
    Object localObject = "";
    try
    {
      String str = DeltaEncode.ma();
      localObject = str;
    }
    catch (Throwable localThrowable)
    {
      e.a(localThrowable);
    }
    return (String)localObject;
  }
  
  /* Error */
  private static String g()
  {
    // Byte code:
    //   0: ldc 57
    //   2: astore_0
    //   3: ldc 57
    //   5: astore_1
    //   6: aconst_null
    //   7: astore 4
    //   9: aconst_null
    //   10: astore_2
    //   11: invokestatic 148	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   14: ldc -106
    //   16: invokestatic 65	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   19: invokevirtual 154	java/lang/Runtime:exec	(Ljava/lang/String;)Ljava/lang/Process;
    //   22: astore 5
    //   24: new 156	java/io/InputStreamReader
    //   27: astore_3
    //   28: aload_3
    //   29: aload 5
    //   31: invokevirtual 162	java/lang/Process:getInputStream	()Ljava/io/InputStream;
    //   34: invokespecial 165	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   37: new 167	java/io/LineNumberReader
    //   40: astore 4
    //   42: aload 4
    //   44: aload_3
    //   45: invokespecial 168	java/io/LineNumberReader:<init>	(Ljava/io/Reader;)V
    //   48: aload_0
    //   49: astore_2
    //   50: aload_1
    //   51: astore_0
    //   52: aload_2
    //   53: ifnull +37 -> 90
    //   56: aload 4
    //   58: invokevirtual 169	java/io/LineNumberReader:readLine	()Ljava/lang/String;
    //   61: astore_0
    //   62: aload_0
    //   63: astore_2
    //   64: aload_0
    //   65: ifnull -15 -> 50
    //   68: aload_0
    //   69: invokevirtual 172	java/lang/String:trim	()Ljava/lang/String;
    //   72: astore_0
    //   73: goto +17 -> 90
    //   76: astore_0
    //   77: aload 4
    //   79: astore_1
    //   80: goto +45 -> 125
    //   83: astore_2
    //   84: aload 4
    //   86: astore_0
    //   87: goto +46 -> 133
    //   90: aload_3
    //   91: invokevirtual 173	java/io/InputStreamReader:close	()V
    //   94: goto +8 -> 102
    //   97: astore_1
    //   98: aload_1
    //   99: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   102: aload_0
    //   103: astore_2
    //   104: aload 4
    //   106: invokevirtual 174	java/io/LineNumberReader:close	()V
    //   109: aload_0
    //   110: astore_2
    //   111: goto +79 -> 190
    //   114: astore_0
    //   115: aload_0
    //   116: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   119: goto +71 -> 190
    //   122: astore_0
    //   123: aconst_null
    //   124: astore_1
    //   125: aload_3
    //   126: astore_2
    //   127: goto +70 -> 197
    //   130: astore_2
    //   131: aconst_null
    //   132: astore_0
    //   133: aload_3
    //   134: astore 4
    //   136: aload_2
    //   137: astore_3
    //   138: aload 4
    //   140: astore_2
    //   141: goto +15 -> 156
    //   144: astore_0
    //   145: aconst_null
    //   146: astore_1
    //   147: aload 4
    //   149: astore_2
    //   150: goto +47 -> 197
    //   153: astore_3
    //   154: aconst_null
    //   155: astore_0
    //   156: aload_3
    //   157: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   160: aload_2
    //   161: ifnull +15 -> 176
    //   164: aload_2
    //   165: invokevirtual 173	java/io/InputStreamReader:close	()V
    //   168: goto +8 -> 176
    //   171: astore_2
    //   172: aload_2
    //   173: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   176: aload_1
    //   177: astore_2
    //   178: aload_0
    //   179: ifnull +11 -> 190
    //   182: aload_1
    //   183: astore_2
    //   184: aload_0
    //   185: invokevirtual 174	java/io/LineNumberReader:close	()V
    //   188: aload_1
    //   189: astore_2
    //   190: aload_2
    //   191: areturn
    //   192: astore_3
    //   193: aload_0
    //   194: astore_1
    //   195: aload_3
    //   196: astore_0
    //   197: aload_2
    //   198: ifnull +15 -> 213
    //   201: aload_2
    //   202: invokevirtual 173	java/io/InputStreamReader:close	()V
    //   205: goto +8 -> 213
    //   208: astore_2
    //   209: aload_2
    //   210: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   213: aload_1
    //   214: ifnull +15 -> 229
    //   217: aload_1
    //   218: invokevirtual 174	java/io/LineNumberReader:close	()V
    //   221: goto +8 -> 229
    //   224: astore_1
    //   225: aload_1
    //   226: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   229: aload_0
    //   230: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   2	71	0	localObject1	Object
    //   76	1	0	localObject2	Object
    //   86	24	0	localObject3	Object
    //   114	2	0	localIOException1	java.io.IOException
    //   122	1	0	localObject4	Object
    //   132	1	0	localObject5	Object
    //   144	1	0	localObject6	Object
    //   155	75	0	localObject7	Object
    //   5	75	1	localObject8	Object
    //   97	2	1	localThrowable1	Throwable
    //   124	94	1	localObject9	Object
    //   224	2	1	localIOException2	java.io.IOException
    //   10	54	2	localObject10	Object
    //   83	1	2	localThrowable2	Throwable
    //   103	24	2	localObject11	Object
    //   130	7	2	localThrowable3	Throwable
    //   140	25	2	localObject12	Object
    //   171	2	2	localThrowable4	Throwable
    //   177	25	2	localObject13	Object
    //   208	2	2	localThrowable5	Throwable
    //   27	111	3	localObject14	Object
    //   153	4	3	localThrowable6	Throwable
    //   192	4	3	localObject15	Object
    //   7	141	4	localObject16	Object
    //   22	8	5	localProcess	Process
    // Exception table:
    //   from	to	target	type
    //   56	62	76	finally
    //   68	73	76	finally
    //   56	62	83	java/lang/Throwable
    //   68	73	83	java/lang/Throwable
    //   90	94	97	java/lang/Throwable
    //   104	109	114	java/io/IOException
    //   184	188	114	java/io/IOException
    //   37	48	122	finally
    //   37	48	130	java/lang/Throwable
    //   11	37	144	finally
    //   11	37	153	java/lang/Throwable
    //   164	168	171	java/lang/Throwable
    //   156	160	192	finally
    //   201	205	208	java/lang/Throwable
    //   217	221	224	java/io/IOException
  }
  
  /* Error */
  private static String h()
  {
    // Byte code:
    //   0: ldc 57
    //   2: astore_0
    //   3: ldc -79
    //   5: invokestatic 65	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   8: astore 5
    //   10: aconst_null
    //   11: astore 4
    //   13: aconst_null
    //   14: astore_3
    //   15: aload_3
    //   16: astore_1
    //   17: new 99	java/io/FileReader
    //   20: astore_2
    //   21: aload_3
    //   22: astore_1
    //   23: aload_2
    //   24: aload 5
    //   26: invokespecial 104	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   29: aload_2
    //   30: invokestatic 179	com/megvii/apo/k:a	(Ljava/io/Reader;)Ljava/lang/String;
    //   33: astore_1
    //   34: aload_1
    //   35: astore_0
    //   36: aload_1
    //   37: invokevirtual 182	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   40: iconst_0
    //   41: bipush 17
    //   43: invokevirtual 186	java/lang/String:substring	(II)Ljava/lang/String;
    //   46: astore_3
    //   47: aload_3
    //   48: astore_1
    //   49: aload_2
    //   50: invokevirtual 187	java/io/FileReader:close	()V
    //   53: aload_3
    //   54: astore_1
    //   55: goto +49 -> 104
    //   58: astore_0
    //   59: aload_0
    //   60: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   63: goto +41 -> 104
    //   66: astore_0
    //   67: aload_2
    //   68: astore_1
    //   69: goto +37 -> 106
    //   72: astore_3
    //   73: goto +11 -> 84
    //   76: astore_0
    //   77: goto +29 -> 106
    //   80: astore_3
    //   81: aload 4
    //   83: astore_2
    //   84: aload_2
    //   85: astore_1
    //   86: aload_3
    //   87: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   90: aload_0
    //   91: astore_1
    //   92: aload_2
    //   93: ifnull +11 -> 104
    //   96: aload_0
    //   97: astore_1
    //   98: aload_2
    //   99: invokevirtual 187	java/io/FileReader:close	()V
    //   102: aload_0
    //   103: astore_1
    //   104: aload_1
    //   105: areturn
    //   106: aload_1
    //   107: ifnull +15 -> 122
    //   110: aload_1
    //   111: invokevirtual 187	java/io/FileReader:close	()V
    //   114: goto +8 -> 122
    //   117: astore_1
    //   118: aload_1
    //   119: invokestatic 37	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   122: aload_0
    //   123: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   2	34	0	localObject1	Object
    //   58	2	0	localThrowable1	Throwable
    //   66	1	0	localObject2	Object
    //   76	47	0	localObject3	Object
    //   16	95	1	localObject4	Object
    //   117	2	1	localThrowable2	Throwable
    //   20	79	2	localObject5	Object
    //   14	40	3	str1	String
    //   72	1	3	localThrowable3	Throwable
    //   80	7	3	localThrowable4	Throwable
    //   11	71	4	localObject6	Object
    //   8	17	5	str2	String
    // Exception table:
    //   from	to	target	type
    //   49	53	58	java/lang/Throwable
    //   98	102	58	java/lang/Throwable
    //   29	34	66	finally
    //   36	47	66	finally
    //   29	34	72	java/lang/Throwable
    //   36	47	72	java/lang/Throwable
    //   17	21	76	finally
    //   23	29	76	finally
    //   86	90	76	finally
    //   17	21	80	java/lang/Throwable
    //   23	29	80	java/lang/Throwable
    //   110	114	117	java/lang/Throwable
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.g != 1) {
      return;
    }
    try
    {
      paramMap.put("101010008", Build.SERIAL);
      Object localObject1;
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((TelephonyManager)this.a.getSystemService("phone")).getDeviceId();
      }
      paramMap.put("101010014", localObject1);
      Object localObject2;
      if (!a("android.permission.ACCESS_WIFI_STATE"))
      {
        localObject2 = "";
      }
      else
      {
        localObject2 = ((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getMacAddress();
        if (!TextUtils.isEmpty((CharSequence)localObject2))
        {
          localObject1 = localObject2;
          if (!((String)localObject2).equals(c.a("3hiEzTB+U3i95wtCyV5rTXhs8bQQKex0xy37N2nWj5Y="))) {}
        }
        else
        {
          localObject1 = g();
        }
        localObject2 = localObject1;
        if (TextUtils.isEmpty((CharSequence)localObject1)) {
          localObject2 = h();
        }
      }
      paramMap.put("101010015", localObject2);
      paramMap.put("101010016", "");
      paramMap.put("101010017", (String)com.megvii.apo.util.k.b(this.a, "x_c_s", ""));
      paramMap.put("101010018", b());
      paramMap.put("101010019", b.a(this.a));
      paramMap.put("101030002", Settings.Secure.getString(this.a.getContentResolver(), "android_id"));
      paramMap.put("101040001", g.a(c.a("VknC2CcEX52fpPZYEEEB9Q==")));
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((TelephonyManager)this.a.getSystemService("phone")).getSimSerialNumber();
      }
      paramMap.put("101061005", localObject1);
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((TelephonyManager)this.a.getSystemService("phone")).getSubscriberId();
      }
      paramMap.put("101061006", localObject1);
      paramMap.put("101010020", c());
      paramMap.put("101010021", d());
      paramMap.put("101010027", e());
      paramMap.put("101010028", f());
      Object localObject3;
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = l.a(this.a, 0, 1, c.a("NpHXHdKnskMfQbVNbb5BEw=="), 21);
        localObject3 = l.a(this.a, 1, 1, c.a("NpHXHdKnskMfQbVNbb5BEw=="), 21);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append("_");
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject2).toString();
      }
      paramMap.put("101010022", localObject1);
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = l.a(this.a, -1, 0, c.a("ThjHWu5NAYSorpz/8V3hnQ=="), 26);
      }
      paramMap.put("101010023", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = l.a(this.a, 0, 1, c.a("ThjHWu5NAYSorpz/8V3hnQ=="), 26);
        localObject3 = l.a(this.a, 1, 1, c.a("ThjHWu5NAYSorpz/8V3hnQ=="), 26);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append("_");
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject2).toString();
      }
      paramMap.put("101010024", localObject1);
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = l.a(this.a, -1, 0, c.a("p8TjCc5SJzgp6VpT+65KYA=="), 26);
      }
      paramMap.put("101010025", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = l.a(this.a, 0, 1, c.a("p8TjCc5SJzgp6VpT+65KYA=="), 26);
        localObject3 = l.a(this.a, 1, 1, c.a("p8TjCc5SJzgp6VpT+65KYA=="), 26);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append("_");
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject2).toString();
      }
      paramMap.put("101010026", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        int i = l.a(this.a, 0);
        int j = l.a(this.a, 1);
        localObject1 = l.a(this.a, i, 1, c.a("2SkG6Dr4jU6VhALgzd8DrQ=="), 21);
        localObject3 = l.a(this.a, j, 1, c.a("2SkG6Dr4jU6VhALgzd8DrQ=="), 21);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append("_");
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject2).toString();
      }
      paramMap.put("101061012", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject2 = l.a(this.a, 0, 1, c.a("l+YtLvJgUDVTCpAMAyPYV8R9xPjbibmM0PwecHSBQuU="), 21);
        localObject1 = l.a(this.a, 1, 1, c.a("l+YtLvJgUDVTCpAMAyPYV8R9xPjbibmM0PwecHSBQuU="), 21);
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append("_");
        ((StringBuilder)localObject3).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject3).toString();
      }
      paramMap.put("101061011", localObject1);
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */