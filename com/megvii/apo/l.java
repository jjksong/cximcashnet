package com.megvii.apo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.megvii.apo.util.c;
import com.megvii.apo.util.e;
import com.megvii.apo.util.j;
import java.util.Map;

public final class l
  extends n
{
  public l(Context paramContext)
  {
    super(paramContext);
  }
  
  public final void a(Map<String, Object> paramMap)
  {
    if (j.B != 1) {
      return;
    }
    try
    {
      Object localObject2;
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject2 = (TelephonyManager)this.a.getSystemService("phone");
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(((TelephonyManager)localObject2).getPhoneType());
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      paramMap.put("101061001", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject2 = (TelephonyManager)this.a.getSystemService("phone");
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(((TelephonyManager)localObject2).getSimState());
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      paramMap.put("101061002", localObject1);
      if (!a("android.permission.READ_PHONE_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((TelephonyManager)this.a.getSystemService("phone")).getNetworkCountryIso();
      }
      paramMap.put("101061003", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = "1";
        if (TextUtils.isEmpty(((TelephonyManager)this.a.getSystemService("phone")).getSimSerialNumber())) {
          localObject1 = "0";
        }
      }
      paramMap.put("101061004", localObject1);
      if (!a("android.permission.ACCESS_NETWORK_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = ((ConnectivityManager)this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        if ((localObject1 != null) && (((NetworkInfo)localObject1).getType() == 1)) {
          localObject1 = "1";
        } else {
          localObject1 = "0";
        }
      }
      paramMap.put("101063001", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getSSID();
      }
      paramMap.put("101063002", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = ((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getBSSID();
      }
      paramMap.put("101063003", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else if (((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getHiddenSSID()) {
        localObject1 = "1";
      } else {
        localObject1 = "0";
      }
      paramMap.put("101063004", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(a(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getIpAddress()));
      }
      paramMap.put("101063005", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getMacAddress());
      }
      paramMap.put("101063006", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getRssi());
      }
      paramMap.put("101063007", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getLinkSpeed());
      }
      paramMap.put("101063008", localObject1);
      if (a("android.permission.ACCESS_WIFI_STATE"))
      {
        localObject1 = ((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo();
        if (Build.VERSION.SDK_INT >= 21)
        {
          localObject1 = String.valueOf(((WifiInfo)localObject1).getFrequency());
          break label688;
        }
      }
      Object localObject1 = "";
      label688:
      paramMap.put("101063009", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getNetworkId());
      }
      paramMap.put("101063010", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(a(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getDhcpInfo().gateway));
      }
      paramMap.put("101063011", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(a(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getDhcpInfo().netmask));
      }
      paramMap.put("101063012", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE")) {
        localObject1 = "";
      } else {
        localObject1 = String.valueOf(((WifiManager)this.a.getApplicationContext().getSystemService("wifi")).getDhcpInfo().leaseDuration);
      }
      paramMap.put("101063013", localObject1);
      if (!a("android.permission.ACCESS_WIFI_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = (WifiManager)this.a.getApplicationContext().getSystemService("wifi");
        if ((Build.VERSION.SDK_INT >= 21) && (((WifiManager)localObject1).is5GHzBandSupported())) {
          localObject1 = "1";
        } else {
          localObject1 = "0";
        }
      }
      paramMap.put("101063014", localObject1);
      Object localObject3;
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject1 = com.megvii.apo.util.l.a(this.a, 0, 1, c.a("LEm+Y6KR1kZFbAQ/q0Vyg40rlo+po1Lu2Tc9huJfr0A="), 21);
        localObject2 = com.megvii.apo.util.l.a(this.a, 1, 1, c.a("LEm+Y6KR1kZFbAQ/q0Vyg40rlo+po1Lu2Tc9huJfr0A="), 21);
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append("_");
        ((StringBuilder)localObject3).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject3).toString();
      }
      paramMap.put("101061007", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject3 = com.megvii.apo.util.l.a(this.a, 0, 1, c.a("jLTK/Kx/8NVtUHq7vly3zQ=="), 21);
        localObject1 = com.megvii.apo.util.l.a(this.a, 1, 1, c.a("jLTK/Kx/8NVtUHq7vly3zQ=="), 21);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject3);
        ((StringBuilder)localObject2).append("_");
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject2).toString();
      }
      paramMap.put("101061008", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject3 = com.megvii.apo.util.l.a(this.a, 0, 1, c.a("mAupSWyYGH0YjS5kuSAAMhxjtoll2X/p0Gsxs0Ge1dM="), 21);
        localObject2 = com.megvii.apo.util.l.a(this.a, 1, 1, c.a("mAupSWyYGH0YjS5kuSAAMhxjtoll2X/p0Gsxs0Ge1dM="), 21);
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append((String)localObject3);
        ((StringBuilder)localObject1).append("_");
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      paramMap.put("101061009", localObject1);
      if (!a("android.permission.READ_PHONE_STATE"))
      {
        localObject1 = "";
      }
      else
      {
        localObject2 = com.megvii.apo.util.l.a(this.a, 0, 1, c.a("l+YtLvJgUDVTCpAMAyPYV8R9xPjbibmM0PwecHSBQuU="), 21);
        localObject3 = com.megvii.apo.util.l.a(this.a, 1, 1, c.a("l+YtLvJgUDVTCpAMAyPYV8R9xPjbibmM0PwecHSBQuU="), 21);
        localObject1 = localObject2;
        if (TextUtils.isEmpty((CharSequence)localObject2)) {
          localObject1 = "0";
        }
        localObject2 = localObject3;
        if (TextUtils.isEmpty((CharSequence)localObject3)) {
          localObject2 = "0";
        }
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append("_");
        ((StringBuilder)localObject3).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject3).toString();
      }
      paramMap.put("101061010", localObject1);
      return;
    }
    catch (Throwable paramMap)
    {
      e.a(paramMap);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */