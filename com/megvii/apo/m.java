package com.megvii.apo;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.util.Base64;
import com.megvii.apo.util.DeltaEncode;
import com.megvii.apo.util.j;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class m
{
  private static volatile m e;
  public Context a;
  public String b;
  public Handler c;
  volatile boolean d = false;
  private com.megvii.apo.b.a f;
  private com.megvii.apo.util.i g;
  private com.megvii.apo.util.h h;
  private o i;
  private k j;
  private r k;
  private h l;
  private g m;
  private i n;
  private a o;
  private p p;
  private d q;
  private l r;
  private n s;
  private n t;
  private n u;
  private n v;
  private Handler w;
  private volatile Map<String, Object> x;
  private CopyOnWriteArrayList<n> y;
  private CopyOnWriteArrayList<com.megvii.apo.c.a> z;
  
  private m(Context paramContext)
  {
    this.a = paramContext;
    this.f = new com.megvii.apo.b.a(this.a);
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(this.a.getFilesDir(), "a");
      paramContext = new java/io/File;
      paramContext.<init>(this.a.getFilesDir(), "d");
      if (!localFile.exists()) {
        localFile.mkdirs();
      }
      if (!paramContext.exists()) {
        paramContext.mkdirs();
      }
    }
    catch (Throwable paramContext)
    {
      com.megvii.apo.util.e.a(paramContext);
    }
    this.g = new com.megvii.apo.util.i(this.a, this);
    this.h = new com.megvii.apo.util.h(this.a);
    paramContext = new HandlerThread("pf");
    paramContext.start();
    this.w = new Handler(paramContext.getLooper());
    paramContext = new HandlerThread("pf1");
    paramContext.start();
    this.c = new Handler(paramContext.getLooper());
    this.i = new o(this.a);
    this.j = new k(this.a);
    this.k = new r(this.a);
    this.l = new h(this.a);
    this.m = new g(this.a);
    this.n = new i(this.a);
    this.o = new a(this.a);
    this.p = new p(this.a);
    this.q = new d(this.a);
    this.r = new l(this.a);
    this.s = new b(this.a);
    this.t = new q(this.a);
    this.u = new f(this.a);
    this.v = new e(this.a);
    this.y = new CopyOnWriteArrayList();
    this.y.add(this.k);
    this.y.add(this.l);
    this.y.add(this.m);
    this.y.add(this.n);
    this.y.add(this.o);
    this.y.add(this.p);
    this.y.add(this.q);
    this.y.add(this.r);
    this.y.add(this.s);
    this.y.add(this.t);
    this.y.add(this.u);
    this.y.add(this.v);
    c();
  }
  
  public static m a(Context paramContext)
  {
    if (e == null) {
      try
      {
        if (e == null)
        {
          m localm = new com/megvii/apo/m;
          localm.<init>(paramContext);
          e = localm;
        }
        paramContext = e;
        return paramContext;
      }
      finally {}
    }
    return e;
  }
  
  private void a(String paramString)
  {
    try
    {
      Object localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(paramString);
      paramString = ((JSONObject)localObject).getJSONObject(com.megvii.apo.util.c.a("dmea3YF4ZgH1FtwC2bX2Tw=="));
      localObject = paramString.getJSONObject("c000");
      j.c = ((JSONObject)localObject).getInt("21");
      j.d = ((JSONObject)localObject).getInt("93");
      if (((JSONObject)localObject).has("46")) {
        j.e = ((JSONObject)localObject).getInt("46");
      }
      if (((JSONObject)localObject).has("47")) {
        j.f = ((JSONObject)localObject).getInt("47");
      }
      localObject = paramString.getJSONObject("c001");
      j.g = ((JSONObject)localObject).getInt("21");
      j.h = ((JSONObject)localObject).getInt("93");
      j.i = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c002");
      j.j = ((JSONObject)localObject).getInt("21");
      j.k = ((JSONObject)localObject).getInt("93");
      j.l = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c003");
      j.m = ((JSONObject)localObject).getInt("21");
      j.n = ((JSONObject)localObject).getInt("93");
      j.o = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c004");
      j.p = ((JSONObject)localObject).getInt("21");
      j.q = ((JSONObject)localObject).getInt("93");
      j.r = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c005");
      j.s = ((JSONObject)localObject).getInt("21");
      j.t = ((JSONObject)localObject).getInt("93");
      j.u = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c006");
      j.v = ((JSONObject)localObject).getInt("21");
      j.w = ((JSONObject)localObject).getInt("93");
      j.x = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c007");
      j.y = ((JSONObject)localObject).getInt("21");
      j.z = ((JSONObject)localObject).getInt("93");
      j.A = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c008");
      j.B = ((JSONObject)localObject).getInt("21");
      j.C = ((JSONObject)localObject).getInt("93");
      j.D = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c009");
      j.E = ((JSONObject)localObject).getInt("21");
      j.F = ((JSONObject)localObject).getInt("93");
      j.G = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c010");
      j.H = ((JSONObject)localObject).getInt("21");
      j.I = ((JSONObject)localObject).getInt("93");
      j.J = ((JSONObject)localObject).getInt("45");
      localObject = paramString.getJSONObject("c011");
      j.K = ((JSONObject)localObject).getInt("21");
      j.L = ((JSONObject)localObject).getInt("93");
      j.M = ((JSONObject)localObject).getInt("45");
      paramString = paramString.getJSONObject("c012");
      j.N = paramString.getInt("21");
      j.O = paramString.getInt("93");
      j.P = paramString.getInt("45");
      com.megvii.apo.util.k.a(this.a, "i_s", Integer.valueOf(j.e));
      paramString = this.k;
      localObject = new com/megvii/apo/a/a;
      ((com.megvii.apo.a.a)localObject).<init>(j.g, j.h, j.i);
      paramString.a((com.megvii.apo.a.a)localObject);
      localObject = this.l;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.j, j.k, j.l);
      ((h)localObject).a(paramString);
      localObject = this.m;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.m, j.n, j.o);
      ((g)localObject).a(paramString);
      localObject = this.n;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.p, j.q, j.r);
      ((i)localObject).a(paramString);
      localObject = this.o;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.s, j.t, j.u);
      ((a)localObject).a(paramString);
      localObject = this.p;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.v, j.w, j.x);
      ((p)localObject).a(paramString);
      paramString = this.q;
      localObject = new com/megvii/apo/a/a;
      ((com.megvii.apo.a.a)localObject).<init>(j.y, j.z, j.A);
      paramString.a((com.megvii.apo.a.a)localObject);
      localObject = this.r;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.B, j.C, j.D);
      ((l)localObject).a(paramString);
      localObject = this.s;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.E, j.F, j.G);
      ((n)localObject).a(paramString);
      paramString = this.t;
      localObject = new com/megvii/apo/a/a;
      ((com.megvii.apo.a.a)localObject).<init>(j.H, j.I, j.J);
      paramString.a((com.megvii.apo.a.a)localObject);
      localObject = this.u;
      paramString = new com/megvii/apo/a/a;
      paramString.<init>(j.K, j.L, j.M);
      ((n)localObject).a(paramString);
      paramString = this.v;
      localObject = new com/megvii/apo/a/a;
      ((com.megvii.apo.a.a)localObject).<init>(j.N, j.O, j.P);
      paramString.a((com.megvii.apo.a.a)localObject);
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  private boolean a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
    case 1: 
    case 0: 
      try
      {
        if (!com.megvii.apo.util.f.b(this.a)) {
          break;
        }
        return true;
      }
      catch (Throwable localThrowable)
      {
        boolean bool;
        com.megvii.apo.util.e.a(localThrowable);
      }
      if (com.megvii.apo.util.f.c(this.a))
      {
        return true;
        if (!com.megvii.apo.util.f.b(this.a))
        {
          bool = com.megvii.apo.util.f.c(this.a);
          if (!bool) {
            break;
          }
        }
        else
        {
          return true;
        }
      }
      break;
    }
    return false;
  }
  
  private void c()
  {
    this.x = null;
    this.x = new HashMap();
    Object localObject = this.z;
    if ((localObject != null) && (((CopyOnWriteArrayList)localObject).size() > 0)) {
      this.z.clear();
    }
    this.z = new CopyOnWriteArrayList();
    for (int i1 = 0; i1 < this.y.size(); i1++)
    {
      localObject = new com.megvii.apo.c.a();
      this.z.add(localObject);
    }
  }
  
  /* Error */
  private void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 491	java/util/HashMap
    //   5: astore_3
    //   6: aload_3
    //   7: invokespecial 492	java/util/HashMap:<init>	()V
    //   10: aload_0
    //   11: getfield 71	com/megvii/apo/m:a	Landroid/content/Context;
    //   14: ldc_w 546
    //   17: ldc_w 548
    //   20: invokestatic 551	com/megvii/apo/util/k:b	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    //   23: checkcast 553	java/lang/String
    //   26: astore_2
    //   27: aload_2
    //   28: astore_1
    //   29: aload_2
    //   30: invokestatic 559	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   33: ifeq +18 -> 51
    //   36: invokestatic 563	com/megvii/apo/util/b:a	()Ljava/lang/String;
    //   39: astore_1
    //   40: aload_0
    //   41: getfield 71	com/megvii/apo/m:a	Landroid/content/Context;
    //   44: ldc_w 546
    //   47: aload_1
    //   48: invokestatic 400	com/megvii/apo/util/k:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    //   51: aload_3
    //   52: ldc_w 564
    //   55: aload_1
    //   56: invokeinterface 570 3 0
    //   61: pop
    //   62: new 233	org/json/JSONObject
    //   65: astore_1
    //   66: aload_1
    //   67: aload_3
    //   68: invokespecial 572	org/json/JSONObject:<init>	(Ljava/util/Map;)V
    //   71: aload_0
    //   72: getfield 77	com/megvii/apo/m:f	Lcom/megvii/apo/b/a;
    //   75: ldc_w 574
    //   78: aload_1
    //   79: invokevirtual 575	org/json/JSONObject:toString	()Ljava/lang/String;
    //   82: aconst_null
    //   83: aconst_null
    //   84: invokevirtual 578	com/megvii/apo/b/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    //   87: astore_1
    //   88: aload_1
    //   89: ifnonnull +6 -> 95
    //   92: aload_0
    //   93: monitorexit
    //   94: return
    //   95: new 233	org/json/JSONObject
    //   98: astore_2
    //   99: aload_2
    //   100: aload_1
    //   101: invokespecial 234	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   104: aload_2
    //   105: ldc_w 580
    //   108: invokestatic 241	com/megvii/apo/util/c:a	(Ljava/lang/String;)Ljava/lang/String;
    //   111: invokevirtual 583	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   114: astore_1
    //   115: aload_0
    //   116: getfield 71	com/megvii/apo/m:a	Landroid/content/Context;
    //   119: ldc_w 585
    //   122: aload_1
    //   123: invokestatic 400	com/megvii/apo/util/k:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V
    //   126: aload_0
    //   127: getfield 71	com/megvii/apo/m:a	Landroid/content/Context;
    //   130: astore_3
    //   131: new 79	java/io/File
    //   134: astore_2
    //   135: aload_2
    //   136: aload_3
    //   137: ldc_w 587
    //   140: invokevirtual 591	android/content/Context:getExternalFilesDir	(Ljava/lang/String;)Ljava/io/File;
    //   143: ldc_w 587
    //   146: invokespecial 89	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   149: new 593	java/io/FileOutputStream
    //   152: astore_3
    //   153: aload_3
    //   154: aload_2
    //   155: invokespecial 596	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   158: aload_3
    //   159: aload_1
    //   160: invokevirtual 600	java/lang/String:getBytes	()[B
    //   163: invokevirtual 604	java/io/FileOutputStream:write	([B)V
    //   166: aload_3
    //   167: invokevirtual 607	java/io/FileOutputStream:flush	()V
    //   170: aload_3
    //   171: invokevirtual 610	java/io/FileOutputStream:close	()V
    //   174: goto +8 -> 182
    //   177: astore_2
    //   178: aload_2
    //   179: invokestatic 102	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   182: aload_0
    //   183: getfield 71	com/megvii/apo/m:a	Landroid/content/Context;
    //   186: invokevirtual 614	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   189: ldc_w 585
    //   192: aload_1
    //   193: invokestatic 620	android/provider/Settings$System:putString	(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    //   196: pop
    //   197: aload_0
    //   198: monitorexit
    //   199: return
    //   200: astore_1
    //   201: aload_1
    //   202: invokestatic 102	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   205: aload_0
    //   206: monitorexit
    //   207: return
    //   208: astore_1
    //   209: aload_1
    //   210: invokestatic 102	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   213: aload_0
    //   214: monitorexit
    //   215: return
    //   216: astore_1
    //   217: goto +11 -> 228
    //   220: astore_1
    //   221: aload_1
    //   222: invokestatic 102	com/megvii/apo/util/e:a	(Ljava/lang/Throwable;)V
    //   225: aload_0
    //   226: monitorexit
    //   227: return
    //   228: aload_0
    //   229: monitorexit
    //   230: aload_1
    //   231: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	232	0	this	m
    //   28	165	1	localObject1	Object
    //   200	2	1	localThrowable1	Throwable
    //   208	2	1	localThrowable2	Throwable
    //   216	1	1	localObject2	Object
    //   220	11	1	localThrowable3	Throwable
    //   26	129	2	localObject3	Object
    //   177	2	2	localThrowable4	Throwable
    //   5	166	3	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   131	174	177	java/lang/Throwable
    //   182	197	200	java/lang/Throwable
    //   95	131	208	java/lang/Throwable
    //   178	182	208	java/lang/Throwable
    //   201	205	208	java/lang/Throwable
    //   2	27	216	finally
    //   29	51	216	finally
    //   51	88	216	finally
    //   95	131	216	finally
    //   131	174	216	finally
    //   178	182	216	finally
    //   182	197	216	finally
    //   201	205	216	finally
    //   209	213	216	finally
    //   221	225	216	finally
    //   2	27	220	java/lang/Throwable
    //   29	51	220	java/lang/Throwable
    //   51	88	220	java/lang/Throwable
    //   209	213	220	java/lang/Throwable
  }
  
  private static String e()
  {
    Date localDate = new Date(System.currentTimeMillis());
    return new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.CHINA).format(localDate);
  }
  
  public final String a()
  {
    Object localObject = new HashMap();
    ((Map)localObject).put(com.megvii.apo.util.c.a("CyDQwnhz5hJhpiFQYplpLA=="), this.a.getPackageName());
    ((Map)localObject).put(com.megvii.apo.util.c.a("eTvIhLA9xKC3hdE6/Fhj7w=="), Build.BRAND);
    ((Map)localObject).put(com.megvii.apo.util.c.a("3FeMK1fC6PivLlSzaUaWhg=="), Integer.valueOf(Build.VERSION.SDK_INT));
    localObject = new JSONObject((Map)localObject);
    if (TextUtils.isEmpty(com.megvii.apo.util.d.b())) {
      com.megvii.apo.util.d.a(com.megvii.apo.util.b.a());
    }
    com.megvii.apo.util.e.a("ii():  before en");
    localObject = DeltaEncode.a(((JSONObject)localObject).toString());
    com.megvii.apo.util.e.a("ii():  after en");
    return (String)localObject;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    try
    {
      Handler localHandler = this.w;
      Runnable local1 = new com/megvii/apo/m$1;
      local1.<init>(this, paramString2, paramString1);
      localHandler.post(local1);
      return;
    }
    finally
    {
      paramString1 = finally;
      throw paramString1;
    }
  }
  
  public final String b()
  {
    try
    {
      Object localObject1 = this.z.iterator();
      Object localObject2;
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (com.megvii.apo.c.a)((Iterator)localObject1).next();
        if (((com.megvii.apo.c.a)localObject2).b) {
          this.x.putAll(((com.megvii.apo.c.a)localObject2).a);
        }
      }
      if ((this.x != null) && (!this.x.isEmpty()))
      {
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>(this.x);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("数据已拿走     :");
        ((StringBuilder)localObject2).append(e());
        com.megvii.apo.util.e.a(((StringBuilder)localObject2).toString());
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("数据  ：");
        ((StringBuilder)localObject2).append(((JSONObject)localObject1).toString());
        com.megvii.apo.util.e.a(((StringBuilder)localObject2).toString());
        if (j.c == 2) {
          localObject1 = ((JSONObject)localObject1).toString();
        } else {
          localObject1 = "";
        }
        this.d = true;
        return (String)localObject1;
      }
      return "";
    }
    catch (Throwable localThrowable)
    {
      com.megvii.apo.util.e.a(localThrowable);
    }
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/apo/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */