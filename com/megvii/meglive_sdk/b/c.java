package com.megvii.meglive_sdk.b;

import android.os.Build;
import android.os.Build.VERSION;
import com.megvii.meglive_sdk.g.a;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c
{
  private String a;
  private long b;
  private long c;
  private String d;
  private List<String> e;
  
  public c(long paramLong, String paramString, Throwable paramThrowable)
  {
    this.b = paramLong;
    this.a = paramString;
    this.c = System.currentTimeMillis();
    this.e = new ArrayList();
    this.d = paramThrowable.getLocalizedMessage();
    while (paramThrowable != null)
    {
      StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
      if (arrayOfStackTraceElement != null)
      {
        int j = arrayOfStackTraceElement.length;
        for (int i = 0; i < j; i++)
        {
          paramString = arrayOfStackTraceElement[i];
          List localList = this.e;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append(paramString.getClassName());
          localStringBuilder.append(".");
          localStringBuilder.append(paramString.getMethodName());
          localStringBuilder.append(":");
          localStringBuilder.append(paramString.getLineNumber());
          localList.add(localStringBuilder.toString());
        }
      }
      paramThrowable = paramThrowable.getCause();
    }
  }
  
  public final String toString()
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.CHINA);
    JSONObject localJSONObject1 = new JSONObject();
    JSONObject localJSONObject2 = new JSONObject();
    JSONArray localJSONArray = new JSONArray();
    try
    {
      if (this.a == null) {
        localObject = "";
      } else {
        localObject = this.a;
      }
      localJSONObject1.put("bizNumberStr", localObject);
      Object localObject = new java/util/Date;
      ((Date)localObject).<init>(this.b);
      localJSONObject1.put("startTime", localSimpleDateFormat.format((Date)localObject));
      localObject = new java/util/Date;
      ((Date)localObject).<init>(this.c);
      localJSONObject1.put("crashTIme", localSimpleDateFormat.format((Date)localObject));
      if (this.d == null) {
        localObject = "";
      } else {
        localObject = this.d;
      }
      localJSONObject1.put("exceptionName", localObject);
      localObject = this.e.iterator();
      while (((Iterator)localObject).hasNext()) {
        localJSONArray.put((String)((Iterator)localObject).next());
      }
      localJSONObject1.put("exceptionArray", localJSONArray);
      a.a();
      localJSONObject2.put("App_Version", a.b());
      localJSONObject2.put("Device", Build.MANUFACTURER);
      localJSONObject2.put("Device_Model", Build.MODEL);
      localJSONObject2.put("Device_Name", Build.FINGERPRINT);
      localJSONObject2.put("Device_SystemName", "Android");
      localJSONObject2.put("Device_SystemVersion", Build.VERSION.RELEASE);
      localJSONObject1.put("deviceInfo", localJSONObject2);
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return localJSONObject1.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/b/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */