package com.megvii.meglive_sdk.b;

import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public final class a
{
  public static int a = 1;
  public static final String[] b = { "no_fail", "no_face_found", "pitch_too_big", "yaw_too_big", "face_area_too_small", "face_too_dark", "face_too_bright", "face_width_too_small", "face_width_too_big", "face_too_blurry", "face_out_of_rect", "eye_occlusion", "mouth_occlusion", "vertical_detection_failed" };
  public static final String[] c = { "face_none", "other_action", "incontinuous_image", "timeout", "no_face_found", "no_face_sometimes", "face_lost", "action_too_fast", "face_occlusion", "mask", "face_aimless" };
  public static final String[] d = { "FaceIDFlashLiveFailedTypeNone", "FaceIDFlashLiveFailedTypeActionTimeout", "FaceIDFlashLiveFailedTypeFlashTimeout", "FaceIDFlashLiveFailedTypeFlashFailed", "FaceIDFlashLiveFailedTypeActionFailed", "FaceIDFlashLiveFailedTypeSDKInitFailed", "FaceIDFlashLiveFailedTypeUserCanceled" };
  
  public static JSONObject a(String paramString1, int paramInt1, int paramInt2, String paramString2)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("event_id", UUID.randomUUID().toString());
      localJSONObject.put("type", "track");
      localJSONObject.put("project", "FaceIDZFAC");
      localJSONObject.put("time", System.currentTimeMillis());
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString1);
      localStringBuilder.append("_");
      localStringBuilder.append(paramInt2 + 1);
      paramString1 = localStringBuilder.toString();
      if (paramInt1 == 1)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString1);
        localStringBuilder.append("_blink");
        paramString1 = localStringBuilder.toString();
      }
      else if (paramInt1 == 2)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString1);
        localStringBuilder.append("_mouth");
        paramString1 = localStringBuilder.toString();
      }
      else if (paramInt1 == 3)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString1);
        localStringBuilder.append("_shake");
        paramString1 = localStringBuilder.toString();
      }
      else if (paramInt1 == 4)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString1);
        localStringBuilder.append("_nod");
        paramString1 = localStringBuilder.toString();
      }
      else
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString1);
        localStringBuilder.append("_else");
        paramString1 = localStringBuilder.toString();
      }
      localJSONObject.put("event", paramString1);
      paramString1 = new org/json/JSONObject;
      paramString1.<init>();
      paramString1.put("biz_token", paramString2);
      paramString1.put("liveness", 2);
      paramString1.put("try_times", a);
      localJSONObject.put("properties", paramString1);
      return localJSONObject;
    }
    catch (JSONException paramString1)
    {
      paramString1.printStackTrace();
    }
    return null;
  }
  
  public static JSONObject a(String paramString1, String paramString2, int paramInt)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("type", "track");
      localJSONObject.put("project", "FaceIDZFAC");
      localJSONObject.put("event_id", UUID.randomUUID().toString());
      localJSONObject.put("time", System.currentTimeMillis());
      localJSONObject.put("event", paramString1);
      paramString1 = new org/json/JSONObject;
      paramString1.<init>();
      paramString1.put("liveness", paramInt);
      paramString1.put("biz_token", paramString2);
      paramString1.put("try_times", a);
      localJSONObject.put("properties", paramString1);
      return localJSONObject;
    }
    catch (JSONException paramString1)
    {
      paramString1.printStackTrace();
    }
    return null;
  }
  
  public static JSONObject a(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("type", "track");
      localJSONObject.put("project", "FaceIDZFAC");
      localJSONObject.put("event_id", UUID.randomUUID().toString());
      localJSONObject.put("time", System.currentTimeMillis());
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString1);
      localStringBuilder.append(":");
      localStringBuilder.append(b[paramInt2]);
      localJSONObject.put("event", localStringBuilder.toString());
      paramString1 = new org/json/JSONObject;
      paramString1.<init>();
      paramString1.put("biz_token", paramString2);
      paramString1.put("liveness", paramInt1);
      paramString1.put("try_times", a);
      localJSONObject.put("properties", paramString1);
      return localJSONObject;
    }
    catch (JSONException paramString1)
    {
      paramString1.printStackTrace();
    }
    return null;
  }
  
  public static JSONObject a(Map<String, Object> paramMap, String paramString)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramMap);
      paramMap = new org/json/JSONObject;
      paramMap.<init>();
      paramMap.put("properties", localJSONObject);
      paramMap.put("time", System.currentTimeMillis());
      paramMap.put("type", "profile_set");
      paramMap.put("project", "FaceIDZFAC");
      paramMap.put("event_id", UUID.randomUUID().toString());
      paramMap.put("event", "set_header");
      paramMap.put("biz_token", paramString);
      return paramMap;
    }
    catch (JSONException paramMap)
    {
      paramMap.printStackTrace();
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */