package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Toast;

public final class v
  extends Toast
{
  public Toast a;
  
  public v(Context paramContext)
  {
    super(paramContext);
  }
  
  public static Toast a(Context paramContext, int paramInt)
  {
    Toast localToast = new Toast(paramContext);
    localToast.setView(((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(paramInt, null));
    localToast.setGravity(17, 0, 250);
    localToast.setDuration(0);
    return localToast;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */