package com.megvii.meglive_sdk.i;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.res.Resources;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;
import com.megvii.meglive_sdk.R.string;

public final class g
{
  public Activity a;
  
  public g(Activity paramActivity)
  {
    this.a = paramActivity;
  }
  
  public final AlertDialog a(View.OnClickListener paramOnClickListener)
  {
    View localView = View.inflate(this.a, R.layout.megvii_liveness_dialog, null);
    TextView localTextView3 = (TextView)localView.findViewById(R.id.tv_megvii_dialog_title);
    TextView localTextView2 = (TextView)localView.findViewById(R.id.tv_megvii_dialog_left);
    localTextView2.setOnClickListener(paramOnClickListener);
    TextView localTextView1 = (TextView)localView.findViewById(R.id.tv_megvii_dialog_right);
    localTextView1.setOnClickListener(paramOnClickListener);
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_titlePrompt_text);
    int i = q.a(q.b, paramOnClickListener);
    localTextView3.setText(this.a.getResources().getString(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_titlePrompt_size);
    i = q.a(q.f, paramOnClickListener);
    localTextView3.setTextSize(0, this.a.getResources().getDimensionPixelSize(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_titlePrompt_color);
    i = q.a(q.e, paramOnClickListener);
    localTextView3.setTextColor(this.a.getResources().getColor(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_leftPrompt_text);
    i = q.a(q.b, paramOnClickListener);
    localTextView2.setText(this.a.getResources().getString(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_leftPrompt_size);
    i = q.a(q.f, paramOnClickListener);
    localTextView2.setTextSize(0, this.a.getResources().getDimensionPixelSize(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_leftPrompt_color);
    i = q.a(q.e, paramOnClickListener);
    localTextView2.setTextColor(this.a.getResources().getColor(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_rightPrompt_text);
    i = q.a(q.b, paramOnClickListener);
    localTextView1.setText(this.a.getResources().getString(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_rightPrompt_size);
    i = q.a(q.f, paramOnClickListener);
    localTextView1.setTextSize(0, this.a.getResources().getDimensionPixelSize(i));
    q.a(this.a);
    paramOnClickListener = this.a.getResources().getString(R.string.key_liveness_exit_rightPrompt_color);
    i = q.a(q.e, paramOnClickListener);
    localTextView1.setTextColor(this.a.getResources().getColor(i));
    return a(localView);
  }
  
  public final AlertDialog a(View paramView)
  {
    AlertDialog localAlertDialog = new AlertDialog.Builder(this.a).setCancelable(false).create();
    localAlertDialog.show();
    localAlertDialog.getWindow().setContentView(paramView);
    localAlertDialog.getWindow().setBackgroundDrawableResource(17170445);
    paramView = localAlertDialog.getWindow().getAttributes();
    paramView.width = (((WindowManager)this.a.getSystemService("window")).getDefaultDisplay().getWidth() * 5 / 6);
    paramView.height = -2;
    localAlertDialog.getWindow().setAttributes(paramView);
    return localAlertDialog;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */