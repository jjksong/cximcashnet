package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class u
{
  public static void a(Context paramContext, String paramString, Object paramObject)
  {
    paramContext = paramContext.getSharedPreferences("meglive_data", 0).edit();
    if ((paramObject instanceof Boolean)) {
      paramContext.putBoolean(paramString, ((Boolean)paramObject).booleanValue());
    } else if ((paramObject instanceof Float)) {
      paramContext.putFloat(paramString, ((Float)paramObject).floatValue());
    } else if ((paramObject instanceof Integer)) {
      paramContext.putInt(paramString, ((Integer)paramObject).intValue());
    } else if ((paramObject instanceof Long)) {
      paramContext.putLong(paramString, ((Long)paramObject).longValue());
    } else {
      paramContext.putString(paramString, (String)paramObject);
    }
    paramContext.commit();
  }
  
  public static Object b(Context paramContext, String paramString, Object paramObject)
  {
    paramContext = paramContext.getSharedPreferences("meglive_data", 0);
    if ((paramObject instanceof Boolean)) {
      return Boolean.valueOf(paramContext.getBoolean(paramString, ((Boolean)paramObject).booleanValue()));
    }
    if ((paramObject instanceof Float)) {
      return Float.valueOf(paramContext.getFloat(paramString, ((Float)paramObject).floatValue()));
    }
    if ((paramObject instanceof Integer)) {
      return Integer.valueOf(paramContext.getInt(paramString, ((Integer)paramObject).intValue()));
    }
    if ((paramObject instanceof Long)) {
      return Long.valueOf(paramContext.getLong(paramString, ((Long)paramObject).longValue()));
    }
    if ((paramObject instanceof String)) {
      return paramContext.getString(paramString, (String)paramObject);
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */