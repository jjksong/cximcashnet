package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.megvii.meglive_sdk.b.d;
import com.megvii.meglive_sdk.g.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class f
{
  public static String a(int paramInt1, int paramInt2)
  {
    try
    {
      Object localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      ((JSONObject)localObject).put("biz_token", e.a(a.a().a));
      k.b("delta biztoken", e.a(a.a().a));
      ((JSONObject)localObject).put("log", "");
      ((JSONObject)localObject).put("bid", a.a().a.getPackageName());
      ((JSONObject)localObject).put("liveness_type", e.b(a.a().a).a);
      ((JSONObject)localObject).put("liveness_config", (String)u.b(a.a().a, "livenessConfig", ""));
      if (paramInt1 == 0) {
        paramInt1 = 2000;
      } else {
        paramInt1 = 2001;
      }
      ((JSONObject)localObject).put("liveness_result", paramInt1);
      ((JSONObject)localObject).put("liveness_failure_reason", paramInt2);
      ((JSONObject)localObject).put("sdk_version", "FaceIDZFAC 1.3.0A");
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("systemName", "Android");
      localJSONObject.put("systemVersion", Build.VERSION.RELEASE);
      localJSONObject.put("deviceName", Build.FINGERPRINT);
      localJSONObject.put("deviceModel", Build.MODEL);
      ((JSONObject)localObject).put("user_info", localJSONObject);
      localObject = ((JSONObject)localObject).toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */