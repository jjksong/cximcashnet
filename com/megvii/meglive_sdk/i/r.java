package com.megvii.meglive_sdk.i;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public final class r
{
  public static float a = 0.15F;
  public static int b;
  public static int c;
  public static int d;
  public static int e;
  public static int f;
  public static float g;
  public static float h;
  public static float i;
  public static float j;
  public static float k;
  public static float l;
  public static float m;
  public static float n;
  public static float o;
  
  public static int a(Context paramContext, float paramFloat)
  {
    return (int)(paramFloat * paramContext.getResources().getDisplayMetrics().density + 0.5F);
  }
  
  public static void a(Context paramContext)
  {
    if ((i == 0.0F) || (j == 0.0F) || (e == 0) || (f == 0) || (h == 0.0F))
    {
      DisplayMetrics localDisplayMetrics1 = paramContext.getResources().getDisplayMetrics();
      DisplayMetrics localDisplayMetrics2 = new DisplayMetrics();
      ((Activity)paramContext).getWindowManager().getDefaultDisplay().getRealMetrics(localDisplayMetrics2);
      e = localDisplayMetrics2.widthPixels;
      f = localDisplayMetrics2.heightPixels;
      h = localDisplayMetrics1.density;
      b = (int)(h * 35.0F);
      new StringBuilder("mNotificationBarHeight =").append(b);
      new StringBuilder("mWidth =").append(e);
      new StringBuilder("mHeight =").append(f);
      c = localDisplayMetrics1.widthPixels;
      d = localDisplayMetrics1.heightPixels;
      new StringBuilder("mScreenWidth =").append(c);
      new StringBuilder("mScreenHeight =").append(d);
      g = localDisplayMetrics1.densityDpi;
      float f1 = h;
      k = f1 * 30.0F;
      l = 30.0F * f1;
      m = 50.0F * f1;
      n = f1 * 40.0F;
      i = e - k - l;
      j = f - m - n;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */