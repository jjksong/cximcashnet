package com.megvii.meglive_sdk.i;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public final class j
{
  public Camera a;
  public int b;
  public int c;
  public int d = 1;
  public int e;
  
  private Camera.Size a(Camera.Parameters paramParameters)
  {
    Object localObject = paramParameters.getSupportedPreviewSizes();
    paramParameters = new ArrayList();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      Camera.Size localSize = (Camera.Size)((Iterator)localObject).next();
      if (localSize.width > localSize.height) {
        paramParameters.add(localSize);
      }
    }
    Collections.sort(paramParameters, new Comparator() {});
    return (Camera.Size)paramParameters.get(0);
  }
  
  public static boolean b()
  {
    if (Build.VERSION.SDK_INT >= 9)
    {
      int j = Camera.getNumberOfCameras();
      Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
      for (int i = 0; i < j; i++)
      {
        Camera.getCameraInfo(i, localCameraInfo);
        if (1 == localCameraInfo.facing) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final Camera a(Activity paramActivity, int paramInt)
  {
    try
    {
      this.d = paramInt;
      this.a = Camera.open(paramInt);
      Object localObject1 = new android/hardware/Camera$CameraInfo;
      ((Camera.CameraInfo)localObject1).<init>();
      Camera.getCameraInfo(paramInt, (Camera.CameraInfo)localObject1);
      localObject1 = this.a.getParameters();
      Object localObject2 = a(this.a.getParameters());
      this.b = ((Camera.Size)localObject2).width;
      this.c = ((Camera.Size)localObject2).height;
      ((Camera.Parameters)localObject1).setPreviewSize(this.b, this.c);
      if (((Camera.Parameters)localObject1).getSupportedFocusModes().contains("continuous-video")) {
        ((Camera.Parameters)localObject1).setFocusMode("continuous-video");
      }
      localObject2 = new android/hardware/Camera$CameraInfo;
      ((Camera.CameraInfo)localObject2).<init>();
      Camera.getCameraInfo(this.d, (Camera.CameraInfo)localObject2);
      int j = paramActivity.getWindowManager().getDefaultDisplay().getRotation();
      int i = 0;
      paramInt = i;
      switch (j)
      {
      default: 
        paramInt = i;
        break;
      case 3: 
        paramInt = 270;
        break;
      case 2: 
        paramInt = 180;
        break;
      case 1: 
        paramInt = 90;
      }
      if (((Camera.CameraInfo)localObject2).facing == 1) {
        paramInt = (360 - (((Camera.CameraInfo)localObject2).orientation + paramInt) % 360) % 360;
      } else {
        paramInt = (((Camera.CameraInfo)localObject2).orientation - paramInt + 360) % 360;
      }
      this.e = paramInt;
      this.a.setDisplayOrientation(this.e);
      this.a.setParameters((Camera.Parameters)localObject1);
      paramActivity = this.a;
      return paramActivity;
    }
    catch (Exception paramActivity)
    {
      paramActivity.printStackTrace();
    }
    return null;
  }
  
  public final void a()
  {
    Camera localCamera = this.a;
    if (localCamera != null) {
      try
      {
        localCamera.stopPreview();
        this.a.setPreviewCallback(null);
        this.a.release();
        this.a = null;
        return;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public final void a(SurfaceTexture paramSurfaceTexture)
  {
    Camera localCamera = this.a;
    if (localCamera != null) {
      try
      {
        localCamera.setPreviewTexture(paramSurfaceTexture);
        this.a.startPreview();
        return;
      }
      catch (Exception paramSurfaceTexture)
      {
        paramSurfaceTexture.printStackTrace();
      }
    }
  }
  
  public final void a(Camera.PreviewCallback paramPreviewCallback)
  {
    try
    {
      if (this.a != null)
      {
        k.a("test", "Camera.setPreviewCallback");
        this.a.setPreviewCallback(paramPreviewCallback);
      }
      return;
    }
    catch (Exception paramPreviewCallback)
    {
      paramPreviewCallback.printStackTrace();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */