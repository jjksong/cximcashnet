package com.megvii.meglive_sdk.i;

import android.content.Context;
import com.megvii.meglive_sdk.volley.e;
import com.megvii.meglive_sdk.volley.n;

public final class x
{
  private static x a;
  private static Context c;
  private n b;
  
  private x(Context paramContext)
  {
    c = paramContext.getApplicationContext();
    this.b = a();
  }
  
  public static x a(Context paramContext)
  {
    try
    {
      if (a == null)
      {
        x localx = new com/megvii/meglive_sdk/i/x;
        localx.<init>(paramContext);
        a = localx;
      }
      paramContext = a;
      return paramContext;
    }
    finally {}
  }
  
  private n a()
  {
    if (this.b == null)
    {
      Context localContext = c;
      if (localContext == null) {
        return null;
      }
      this.b = com.megvii.meglive_sdk.volley.toolbox.m.a(localContext.getApplicationContext(), null);
    }
    return this.b;
  }
  
  public final <T> boolean a(com.megvii.meglive_sdk.volley.m<T> paramm)
  {
    if (a() == null) {
      return false;
    }
    paramm.n = new e(10000, 0, 1.0F);
    a().a(paramm);
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/x.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */