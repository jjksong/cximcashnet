package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public final class t
  implements SensorEventListener
{
  public float a;
  private SensorManager b;
  private Sensor c;
  
  public t(Context paramContext)
  {
    this.b = ((SensorManager)paramContext.getSystemService("sensor"));
    this.c = this.b.getDefaultSensor(1);
    paramContext = this.c;
    if (paramContext != null) {
      this.b.registerListener(this, paramContext, 3);
    }
  }
  
  public final void a()
  {
    if (this.c != null)
    {
      SensorManager localSensorManager = this.b;
      if (localSensorManager != null) {
        localSensorManager.unregisterListener(this);
      }
    }
  }
  
  public final boolean b()
  {
    return this.a >= 8.0F;
  }
  
  public final void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public final void onSensorChanged(SensorEvent paramSensorEvent)
  {
    if ((paramSensorEvent != null) && (paramSensorEvent.values != null) && (paramSensorEvent.values.length > 1))
    {
      this.a = paramSensorEvent.values[1];
      return;
    }
    this.a = 0.0F;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */