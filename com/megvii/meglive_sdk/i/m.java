package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.os.Environment;
import android.text.format.Formatter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class m
{
  private static Context a;
  private static m b;
  private static File c;
  private static SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private static String e;
  private static boolean f = false;
  
  private static long a(File paramFile)
  {
    long l;
    if (paramFile.exists()) {
      try
      {
        FileInputStream localFileInputStream = new java/io/FileInputStream;
        localFileInputStream.<init>(paramFile);
        int i = localFileInputStream.available();
        l = i;
      }
      catch (Exception paramFile)
      {
        paramFile.toString();
      }
    } else {
      l = 0L;
    }
    return l;
  }
  
  private static File a()
  {
    if (Environment.getExternalStorageState().equals("mounted"))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(a.getExternalFilesDir("Log").getPath());
      ((StringBuilder)localObject).append("/");
      localObject = new File(((StringBuilder)localObject).toString());
    }
    else
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(a.getFilesDir().getPath());
      ((StringBuilder)localObject).append("/Log/");
      localObject = new File(((StringBuilder)localObject).toString());
    }
    if (!((File)localObject).exists()) {
      ((File)localObject).mkdir();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(((File)localObject).getPath());
    localStringBuilder.append("/logs.txt");
    Object localObject = new File(localStringBuilder.toString());
    if (!((File)localObject).exists()) {
      try
      {
        ((File)localObject).createNewFile();
      }
      catch (Exception localException)
      {
        new StringBuilder("Create log file failure !!! ").append(localException.toString());
      }
    }
    return (File)localObject;
  }
  
  public static void a(Context paramContext)
  {
    if (!f) {
      return;
    }
    if ((a != null) && (b != null))
    {
      File localFile = c;
      if ((localFile != null) && (localFile.exists())) {}
    }
    else
    {
      a = paramContext;
      b = new m();
      c = a();
      new StringBuilder("LogFilePath is: ").append(c.getPath());
      long l = a(c);
      new StringBuilder("Log max size is: ").append(Formatter.formatFileSize(paramContext, 10485760L));
      new StringBuilder("log now size is: ").append(Formatter.formatFileSize(paramContext, l));
      if (10485760L < l)
      {
        paramContext = new StringBuilder();
        paramContext.append(c.getParent());
        paramContext.append("/lastLog.txt");
        paramContext = new File(paramContext.toString());
        if (paramContext.exists()) {
          paramContext.delete();
        }
        c.renameTo(paramContext);
        try
        {
          c.createNewFile();
          return;
        }
        catch (Exception paramContext)
        {
          new StringBuilder("Create log file failure !!! ").append(paramContext.toString());
        }
      }
    }
  }
  
  public static void a(Object paramObject)
  {
    if (!f) {
      return;
    }
    if ((a != null) && (b != null))
    {
      Object localObject = c;
      if ((localObject != null) && (((File)localObject).exists()))
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(b());
        ((StringBuilder)localObject).append(" - ");
        ((StringBuilder)localObject).append(paramObject.toString());
        paramObject = ((StringBuilder)localObject).toString();
        try
        {
          BufferedWriter localBufferedWriter = new java/io/BufferedWriter;
          localObject = new java/io/FileWriter;
          ((FileWriter)localObject).<init>(c, true);
          localBufferedWriter.<init>((Writer)localObject);
          localBufferedWriter.write((String)paramObject);
          localBufferedWriter.write("\r\n");
          localBufferedWriter.flush();
          return;
        }
        catch (Exception paramObject)
        {
          new StringBuilder("Write failure !!! ").append(((Exception)paramObject).toString());
          return;
        }
      }
    }
  }
  
  private static String b()
  {
    Object localObject2 = Thread.currentThread().getStackTrace();
    if (localObject2 == null) {
      return null;
    }
    int j = localObject2.length;
    for (int i = 0; i < j; i++)
    {
      Object localObject1 = localObject2[i];
      if ((!((StackTraceElement)localObject1).isNativeMethod()) && (!((StackTraceElement)localObject1).getClassName().equals(Thread.class.getName())) && (!((StackTraceElement)localObject1).getClassName().equals(b.getClass().getName())))
      {
        e = ((StackTraceElement)localObject1).getFileName();
        localObject2 = new StringBuilder("[");
        ((StringBuilder)localObject2).append(d.format(new Date()));
        ((StringBuilder)localObject2).append(" ThreadId:");
        ((StringBuilder)localObject2).append(Thread.currentThread().getId());
        ((StringBuilder)localObject2).append(" ");
        ((StringBuilder)localObject2).append(((StackTraceElement)localObject1).getClassName());
        ((StringBuilder)localObject2).append(" ");
        ((StringBuilder)localObject2).append(((StackTraceElement)localObject1).getMethodName());
        ((StringBuilder)localObject2).append(" Line:");
        ((StringBuilder)localObject2).append(((StackTraceElement)localObject1).getLineNumber());
        ((StringBuilder)localObject2).append("]");
        return ((StringBuilder)localObject2).toString();
      }
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */