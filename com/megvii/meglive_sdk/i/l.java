package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;

public final class l
{
  public MediaPlayer a;
  private Context b;
  
  public l(Context paramContext)
  {
    this.b = paramContext;
    this.a = new MediaPlayer();
  }
  
  public final void a()
  {
    this.b = null;
    MediaPlayer localMediaPlayer = this.a;
    if (localMediaPlayer != null)
    {
      localMediaPlayer.reset();
      this.a.release();
      this.a = null;
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject = this.a;
    if ((localObject != null) && (paramInt > 0))
    {
      ((MediaPlayer)localObject).reset();
      try
      {
        localObject = this.b.getResources().openRawResourceFd(paramInt);
        this.a.setDataSource(((AssetFileDescriptor)localObject).getFileDescriptor(), ((AssetFileDescriptor)localObject).getStartOffset(), ((AssetFileDescriptor)localObject).getLength());
        ((AssetFileDescriptor)localObject).close();
        MediaPlayer localMediaPlayer = this.a;
        localObject = new com/megvii/meglive_sdk/i/l$1;
        ((1)localObject).<init>(this);
        localMediaPlayer.setOnPreparedListener((MediaPlayer.OnPreparedListener)localObject);
        this.a.prepareAsync();
        return;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        return;
      }
    }
  }
  
  public final void b()
  {
    MediaPlayer localMediaPlayer = this.a;
    if (localMediaPlayer != null) {
      localMediaPlayer.reset();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */