package com.megvii.meglive_sdk.i;

import android.app.Activity;
import android.os.Build.VERSION;
import android.view.View;
import android.view.Window;

public final class d
{
  public static void a(Activity paramActivity)
  {
    int i = paramActivity.getWindow().getDecorView().getSystemUiVisibility();
    int j = i;
    if (Build.VERSION.SDK_INT >= 14) {
      j = i ^ 0x2;
    }
    i = j;
    if (Build.VERSION.SDK_INT >= 16) {
      i = j ^ 0x4;
    }
    j = i;
    if (Build.VERSION.SDK_INT >= 19) {
      j = i ^ 0x1000;
    }
    paramActivity.getWindow().getDecorView().setSystemUiVisibility(j);
  }
  
  public static void b(Activity paramActivity)
  {
    if ((Build.VERSION.SDK_INT > 11) && (Build.VERSION.SDK_INT < 19))
    {
      paramActivity.getWindow().getDecorView().setSystemUiVisibility(8);
      return;
    }
    if (Build.VERSION.SDK_INT >= 19) {
      paramActivity.getWindow().getDecorView().setSystemUiVisibility(4102);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */