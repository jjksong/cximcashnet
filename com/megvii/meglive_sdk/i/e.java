package com.megvii.meglive_sdk.i;

import android.content.Context;
import com.megvii.meglive_sdk.b.d;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class e
{
  public static String a(Context paramContext)
  {
    return (String)u.b(paramContext, "bizToken", "");
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    u.a(paramContext, "option_code", Integer.valueOf(paramInt));
  }
  
  public static d b(Context paramContext)
  {
    Object localObject2 = (String)u.b(paramContext, "livenessConfig", "");
    paramContext = new d();
    if (!"".equals(localObject2)) {
      try
      {
        Object localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>((String)localObject2);
        paramContext.a = ((JSONObject)localObject1).optInt("liveness_type");
        paramContext.b = ((JSONObject)localObject1).getInt("liveness_action_count");
        paramContext.c = ((JSONObject)localObject1).getInt("liveness_timeout");
        localObject1 = ((JSONObject)localObject1).getJSONArray("liveness_action_queue");
        localObject2 = new int[((JSONArray)localObject1).length()];
        for (int i = 0; i < ((JSONArray)localObject1).length(); i++) {
          localObject2[i] = ((JSONArray)localObject1).getInt(i);
        }
        paramContext.d = ((int[])localObject2);
      }
      catch (JSONException localJSONException)
      {
        localJSONException.printStackTrace();
      }
    }
    return paramContext;
  }
  
  public static int c(Context paramContext)
  {
    return ((Integer)u.b(paramContext, "option_code", Integer.valueOf(0))).intValue();
  }
  
  public static String d(Context paramContext)
  {
    String str = (String)u.b(paramContext, "language", "");
    paramContext = str;
    if (!str.equals("en")) {
      paramContext = "zh";
    }
    return paramContext;
  }
  
  public static String e(Context paramContext)
  {
    String str = (String)u.b(paramContext, "host", "");
    paramContext = str;
    if (str.equals("")) {
      paramContext = "https://openapi.faceid.com";
    }
    return paramContext;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */