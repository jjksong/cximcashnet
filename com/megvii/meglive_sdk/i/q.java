package com.megvii.meglive_sdk.i;

import android.content.Context;
import java.lang.reflect.Field;

public final class q
{
  public static Class<?> a;
  public static Class<?> b;
  public static Class<?> c;
  public static Class<?> d;
  public static Class<?> e;
  public static Class<?> f;
  private static q g;
  private static String h;
  private static Class<?> i;
  private static Class<?> j;
  private static Class<?> k;
  
  private q(String paramString)
  {
    try
    {
      StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
      localStringBuilder1.<init>();
      localStringBuilder1.append(paramString);
      localStringBuilder1.append(".R$layout");
      i = Class.forName(localStringBuilder1.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException1)
    {
      localClassNotFoundException1.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>();
      localStringBuilder2.append(paramString);
      localStringBuilder2.append(".R$drawable");
      a = Class.forName(localStringBuilder2.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException2)
    {
      localClassNotFoundException2.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder3 = new java/lang/StringBuilder;
      localStringBuilder3.<init>();
      localStringBuilder3.append(paramString);
      localStringBuilder3.append(".R$id");
      j = Class.forName(localStringBuilder3.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException3)
    {
      localClassNotFoundException3.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder4 = new java/lang/StringBuilder;
      localStringBuilder4.<init>();
      localStringBuilder4.append(paramString);
      localStringBuilder4.append(".R$string");
      b = Class.forName(localStringBuilder4.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException4)
    {
      localClassNotFoundException4.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder5 = new java/lang/StringBuilder;
      localStringBuilder5.<init>();
      localStringBuilder5.append(paramString);
      localStringBuilder5.append(".R$attr");
      k = Class.forName(localStringBuilder5.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException5)
    {
      localClassNotFoundException5.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder6 = new java/lang/StringBuilder;
      localStringBuilder6.<init>();
      localStringBuilder6.append(paramString);
      localStringBuilder6.append(".R$raw");
      c = Class.forName(localStringBuilder6.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException6)
    {
      localClassNotFoundException6.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder7 = new java/lang/StringBuilder;
      localStringBuilder7.<init>();
      localStringBuilder7.append(paramString);
      localStringBuilder7.append(".R$mipmap");
      d = Class.forName(localStringBuilder7.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException7)
    {
      localClassNotFoundException7.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder8 = new java/lang/StringBuilder;
      localStringBuilder8.<init>();
      localStringBuilder8.append(paramString);
      localStringBuilder8.append(".R$color");
      e = Class.forName(localStringBuilder8.toString());
    }
    catch (ClassNotFoundException localClassNotFoundException8)
    {
      localClassNotFoundException8.printStackTrace();
    }
    try
    {
      StringBuilder localStringBuilder9 = new java/lang/StringBuilder;
      localStringBuilder9.<init>();
      localStringBuilder9.append(paramString);
      localStringBuilder9.append(".R$dimen");
      f = Class.forName(localStringBuilder9.toString());
      return;
    }
    catch (ClassNotFoundException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  public static int a(Class<?> paramClass, String paramString)
  {
    if (paramClass != null) {
      try
      {
        int m = paramClass.getField(paramString).getInt(paramString);
        return m;
      }
      catch (Exception paramClass)
      {
        paramClass.printStackTrace();
        return -1;
      }
    }
    paramClass = new StringBuilder("ResClass is not initialized. Please make sure you have added neccessary resources. Also make sure you have ");
    paramClass.append(h);
    paramClass.append(".R$* configured in obfuscation. field=");
    paramClass.append(paramString);
    throw new IllegalArgumentException(paramClass.toString());
  }
  
  public static q a(Context paramContext)
  {
    if (g == null)
    {
      String str2 = h;
      String str1 = str2;
      if (str2 == null) {
        str1 = (String)u.b(paramContext, "manifest_package", paramContext.getPackageName());
      }
      h = str1;
      g = new q(h);
    }
    return g;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */