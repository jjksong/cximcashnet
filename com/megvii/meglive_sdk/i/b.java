package com.megvii.meglive_sdk.i;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.megvii.meglive_sdk.a.d;
import com.megvii.meglive_sdk.a.e;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  private static String a = "faceid-midas-prod";
  private static String b = "sdk-liveness";
  private static com.megvii.meglive_sdk.c.c c;
  
  public static void a(String paramString)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("time", System.currentTimeMillis());
      localJSONObject.put("type", "track");
      localJSONObject.put("event", "app_crash");
      localJSONObject.put("project", "liveness-sdk");
      localJSONObject.put("properties", paramString);
      a(localJSONObject.toString(), a, b);
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  private static void a(final String paramString1, String paramString2, final String paramString3)
  {
    if (c == null) {
      c = new com.megvii.meglive_sdk.c.c(3, 3);
    }
    c.a(new Runnable()
    {
      public final void run()
      {
        try
        {
          Object localObject1 = new com/megvii/meglive_sdk/a/c;
          ((com.megvii.meglive_sdk.a.c)localObject1).<init>("cn-beijing.log.aliyuncs.com", "LTAI97c2nIqQ6dOs", "gxJhx45FpVmOEHHEK44D3RVJeMnU5S", this.a);
          Object localObject2 = new com/megvii/meglive_sdk/a/e;
          int i = ((WifiManager)com.megvii.meglive_sdk.g.a.a().a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getIpAddress();
          Object localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          ((StringBuilder)localObject3).append(i & 0xFF);
          ((StringBuilder)localObject3).append(".");
          ((StringBuilder)localObject3).append(i >> 8 & 0xFF);
          ((StringBuilder)localObject3).append(".");
          ((StringBuilder)localObject3).append(i >> 16 & 0xFF);
          ((StringBuilder)localObject3).append(".");
          ((StringBuilder)localObject3).append(i >> 24 & 0xFF);
          ((e)localObject2).<init>("", String.valueOf(((StringBuilder)localObject3).toString()));
          Object localObject4 = new com/megvii/meglive_sdk/a/a;
          ((com.megvii.meglive_sdk.a.a)localObject4).<init>();
          localObject3 = com.megvii.meglive_sdk.a.b.a(paramString1);
          if (!"data".isEmpty()) {
            ((com.megvii.meglive_sdk.a.a)localObject4).a.put("data", localObject3);
          }
          ((e)localObject2).a((com.megvii.meglive_sdk.a.a)localObject4);
          localObject3 = paramString3;
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          ((StringBuilder)localObject4).append(paramString3);
          ((StringBuilder)localObject4).append(paramString1);
          ((StringBuilder)localObject4).append(".");
          ((StringBuilder)localObject4).append(((com.megvii.meglive_sdk.a.c)localObject1).a);
          ((StringBuilder)localObject4).append("/logstores/");
          ((StringBuilder)localObject4).append((String)localObject3);
          ((StringBuilder)localObject4).append("/shards/lb");
          localObject4 = ((StringBuilder)localObject4).toString();
          try
          {
            byte[] arrayOfByte = ((e)localObject2).a().getBytes("UTF-8");
            localObject2 = com.megvii.meglive_sdk.a.c.a(arrayOfByte);
            com.megvii.meglive_sdk.a.c.a((String)localObject4, ((com.megvii.meglive_sdk.a.c)localObject1).a((String)localObject3, arrayOfByte, (byte[])localObject2), (byte[])localObject2);
            return;
          }
          catch (UnsupportedEncodingException localUnsupportedEncodingException)
          {
            localObject1 = new com/megvii/meglive_sdk/a/d;
            ((d)localObject1).<init>("LogClientError", "Failed to pass log to utf-8 bytes", localUnsupportedEncodingException, "");
            throw ((Throwable)localObject1);
          }
          return;
        }
        catch (d locald)
        {
          locald.printStackTrace();
          k.a("LogException: ", "");
        }
      }
    });
  }
  
  public static void a(JSONObject paramJSONObject)
  {
    k.a("buriedPoint: log", paramJSONObject.toString());
    k.a("projectName", a);
    a(paramJSONObject.toString(), a, b);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/i/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */