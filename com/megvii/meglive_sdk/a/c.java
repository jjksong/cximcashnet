package com.megvii.meglive_sdk.a;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class c
{
  public String a;
  public String b;
  public String c = "http://";
  private String d;
  private String e;
  private String f;
  
  public c(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    if (paramString1 != "")
    {
      this.a = paramString1;
      if (this.a.startsWith("http://"))
      {
        this.a = this.a.substring(7);
      }
      else if (this.a.startsWith("https://"))
      {
        this.a = this.a.substring(8);
        this.c = "https://";
      }
      while (this.a.endsWith("/"))
      {
        paramString1 = this.a;
        this.a = paramString1.substring(0, paramString1.length() - 1);
      }
      if (paramString2 != "")
      {
        this.d = paramString2;
        if (paramString3 != "")
        {
          this.e = paramString3;
          if (paramString4 != "")
          {
            this.b = paramString4;
            this.f = "";
            return;
          }
          throw new NullPointerException("projectName is null");
        }
        throw new NullPointerException("accessKeySecret is null");
      }
      throw new NullPointerException("accessKeyID is null");
    }
    throw new NullPointerException("endpoint is null");
  }
  
  /* Error */
  public static void a(String paramString, Map<String, String> paramMap, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: new 81	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 82	java/net/URL:<init>	(Ljava/lang/String;)V
    //   8: astore 4
    //   10: aload 4
    //   12: invokevirtual 86	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   15: checkcast 88	java/net/HttpURLConnection
    //   18: astore 4
    //   20: aload 4
    //   22: ldc 90
    //   24: invokevirtual 93	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   27: aload 4
    //   29: iconst_1
    //   30: invokevirtual 97	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   33: aload_1
    //   34: invokeinterface 103 1 0
    //   39: invokeinterface 109 1 0
    //   44: astore 5
    //   46: aload 5
    //   48: invokeinterface 115 1 0
    //   53: ifeq +40 -> 93
    //   56: aload 5
    //   58: invokeinterface 119 1 0
    //   63: checkcast 121	java/util/Map$Entry
    //   66: astore_1
    //   67: aload 4
    //   69: aload_1
    //   70: invokeinterface 124 1 0
    //   75: checkcast 26	java/lang/String
    //   78: aload_1
    //   79: invokeinterface 127 1 0
    //   84: checkcast 26	java/lang/String
    //   87: invokevirtual 131	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   90: goto -44 -> 46
    //   93: new 133	java/io/DataOutputStream
    //   96: astore_1
    //   97: aload_1
    //   98: aload 4
    //   100: invokevirtual 137	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   103: invokespecial 140	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   106: aload_1
    //   107: aload_2
    //   108: invokevirtual 144	java/io/DataOutputStream:write	([B)V
    //   111: aload_1
    //   112: invokevirtual 147	java/io/DataOutputStream:flush	()V
    //   115: aload_1
    //   116: invokevirtual 150	java/io/DataOutputStream:close	()V
    //   119: aload 4
    //   121: invokevirtual 153	java/net/HttpURLConnection:getResponseCode	()I
    //   124: istore_3
    //   125: aload 4
    //   127: ldc -101
    //   129: invokevirtual 159	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   132: astore_1
    //   133: aload_1
    //   134: astore_0
    //   135: aload_1
    //   136: ifnonnull +6 -> 142
    //   139: ldc 22
    //   141: astore_0
    //   142: iload_3
    //   143: sipush 200
    //   146: if_icmpeq +225 -> 371
    //   149: aload 4
    //   151: invokevirtual 163	java/net/HttpURLConnection:getErrorStream	()Ljava/io/InputStream;
    //   154: astore 4
    //   156: aload 4
    //   158: ifnull +170 -> 328
    //   161: new 165	java/io/BufferedReader
    //   164: astore_2
    //   165: new 167	java/io/InputStreamReader
    //   168: astore_1
    //   169: aload_1
    //   170: aload 4
    //   172: invokespecial 170	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   175: aload_2
    //   176: aload_1
    //   177: invokespecial 173	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   180: new 175	java/lang/StringBuffer
    //   183: astore_1
    //   184: aload_1
    //   185: invokespecial 176	java/lang/StringBuffer:<init>	()V
    //   188: aload_2
    //   189: invokevirtual 180	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   192: astore 4
    //   194: aload 4
    //   196: ifnull +13 -> 209
    //   199: aload_1
    //   200: aload 4
    //   202: invokevirtual 184	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   205: pop
    //   206: goto -18 -> 188
    //   209: aload_2
    //   210: invokevirtual 185	java/io/BufferedReader:close	()V
    //   213: aload_1
    //   214: invokevirtual 188	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   217: astore 4
    //   219: new 190	org/json/JSONObject
    //   222: astore_2
    //   223: aload_2
    //   224: aload 4
    //   226: invokespecial 191	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   229: aload_2
    //   230: ldc -63
    //   232: invokevirtual 196	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   235: ifeq +38 -> 273
    //   238: aload_2
    //   239: ldc -58
    //   241: invokevirtual 196	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   244: ifeq +29 -> 273
    //   247: new 200	com/megvii/meglive_sdk/a/d
    //   250: astore 4
    //   252: aload 4
    //   254: aload_2
    //   255: ldc -63
    //   257: invokevirtual 203	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   260: aload_2
    //   261: ldc -58
    //   263: invokevirtual 203	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   266: aload_0
    //   267: invokespecial 206	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   270: aload 4
    //   272: athrow
    //   273: new 200	com/megvii/meglive_sdk/a/d
    //   276: astore 4
    //   278: new 208	java/lang/StringBuilder
    //   281: astore_2
    //   282: aload_2
    //   283: ldc -46
    //   285: invokespecial 211	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   288: aload_2
    //   289: iload_3
    //   290: invokestatic 214	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   293: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   296: pop
    //   297: aload_2
    //   298: ldc -37
    //   300: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: aload_2
    //   305: aload_1
    //   306: invokevirtual 188	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   309: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload 4
    //   315: ldc -35
    //   317: aload_2
    //   318: invokevirtual 222	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   321: aload_0
    //   322: invokespecial 206	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   325: aload 4
    //   327: athrow
    //   328: new 200	com/megvii/meglive_sdk/a/d
    //   331: astore_1
    //   332: new 208	java/lang/StringBuilder
    //   335: astore_2
    //   336: aload_2
    //   337: ldc -46
    //   339: invokespecial 211	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   342: aload_2
    //   343: iload_3
    //   344: invokestatic 214	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   347: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   350: pop
    //   351: aload_2
    //   352: ldc -32
    //   354: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   357: pop
    //   358: aload_1
    //   359: ldc -35
    //   361: aload_2
    //   362: invokevirtual 222	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   365: aload_0
    //   366: invokespecial 206	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   369: aload_1
    //   370: athrow
    //   371: return
    //   372: astore_0
    //   373: new 200	com/megvii/meglive_sdk/a/d
    //   376: dup
    //   377: ldc -35
    //   379: ldc -30
    //   381: ldc 22
    //   383: invokespecial 206	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   386: athrow
    //   387: astore_1
    //   388: ldc -28
    //   390: aload_1
    //   391: invokevirtual 229	java/io/IOException:toString	()Ljava/lang/String;
    //   394: invokestatic 233	com/megvii/meglive_sdk/i/k:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   397: new 208	java/lang/StringBuilder
    //   400: dup
    //   401: ldc -21
    //   403: invokespecial 211	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   406: astore_2
    //   407: aload_2
    //   408: aload_0
    //   409: invokevirtual 217	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: pop
    //   413: new 200	com/megvii/meglive_sdk/a/d
    //   416: dup
    //   417: ldc -19
    //   419: aload_2
    //   420: invokevirtual 222	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   423: aload_1
    //   424: ldc 22
    //   426: invokespecial 240	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    //   429: athrow
    //   430: astore_0
    //   431: ldc -28
    //   433: aload_0
    //   434: invokevirtual 241	java/net/ProtocolException:toString	()Ljava/lang/String;
    //   437: invokestatic 233	com/megvii/meglive_sdk/i/k:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   440: new 200	com/megvii/meglive_sdk/a/d
    //   443: dup
    //   444: ldc -19
    //   446: ldc -13
    //   448: aload_0
    //   449: ldc 22
    //   451: invokespecial 240	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    //   454: athrow
    //   455: astore_0
    //   456: ldc -28
    //   458: aload_0
    //   459: invokevirtual 229	java/io/IOException:toString	()Ljava/lang/String;
    //   462: invokestatic 233	com/megvii/meglive_sdk/i/k:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   465: new 200	com/megvii/meglive_sdk/a/d
    //   468: dup
    //   469: ldc -19
    //   471: ldc -11
    //   473: aload_0
    //   474: ldc 22
    //   476: invokespecial 240	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    //   479: athrow
    //   480: astore_0
    //   481: ldc -28
    //   483: aload_0
    //   484: invokevirtual 246	java/net/MalformedURLException:toString	()Ljava/lang/String;
    //   487: invokestatic 233	com/megvii/meglive_sdk/i/k:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   490: new 200	com/megvii/meglive_sdk/a/d
    //   493: dup
    //   494: ldc -19
    //   496: ldc -8
    //   498: aload_0
    //   499: ldc 22
    //   501: invokespecial 240	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    //   504: athrow
    //   505: astore_2
    //   506: goto -233 -> 273
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	509	0	paramString	String
    //   0	509	1	paramMap	Map<String, String>
    //   0	509	2	paramArrayOfByte	byte[]
    //   124	220	3	i	int
    //   8	318	4	localObject	Object
    //   44	13	5	localIterator	java.util.Iterator
    // Exception table:
    //   from	to	target	type
    //   119	133	372	java/io/IOException
    //   149	156	372	java/io/IOException
    //   161	188	372	java/io/IOException
    //   188	194	372	java/io/IOException
    //   199	206	372	java/io/IOException
    //   209	219	372	java/io/IOException
    //   219	273	372	java/io/IOException
    //   273	328	372	java/io/IOException
    //   328	371	372	java/io/IOException
    //   93	119	387	java/io/IOException
    //   20	27	430	java/net/ProtocolException
    //   10	20	455	java/io/IOException
    //   0	10	480	java/net/MalformedURLException
    //   219	273	505	org/json/JSONException
  }
  
  /* Error */
  public static byte[] a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: new 255	java/util/zip/Deflater
    //   3: dup
    //   4: invokespecial 256	java/util/zip/Deflater:<init>	()V
    //   7: astore 5
    //   9: aconst_null
    //   10: astore_3
    //   11: aconst_null
    //   12: astore 4
    //   14: aload 4
    //   16: astore_1
    //   17: new 258	java/io/ByteArrayOutputStream
    //   20: astore_2
    //   21: aload 4
    //   23: astore_1
    //   24: aload_2
    //   25: aload_0
    //   26: arraylength
    //   27: invokespecial 261	java/io/ByteArrayOutputStream:<init>	(I)V
    //   30: aload 5
    //   32: aload_0
    //   33: invokevirtual 264	java/util/zip/Deflater:setInput	([B)V
    //   36: aload 5
    //   38: invokevirtual 267	java/util/zip/Deflater:finish	()V
    //   41: sipush 10240
    //   44: newarray <illegal type>
    //   46: astore_0
    //   47: aload 5
    //   49: invokevirtual 270	java/util/zip/Deflater:finished	()Z
    //   52: ifne +18 -> 70
    //   55: aload_2
    //   56: aload_0
    //   57: iconst_0
    //   58: aload 5
    //   60: aload_0
    //   61: invokevirtual 274	java/util/zip/Deflater:deflate	([B)I
    //   64: invokevirtual 277	java/io/ByteArrayOutputStream:write	([BII)V
    //   67: goto -20 -> 47
    //   70: aload_2
    //   71: invokevirtual 281	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   74: astore_0
    //   75: aload 5
    //   77: invokevirtual 284	java/util/zip/Deflater:end	()V
    //   80: aload_2
    //   81: invokevirtual 287	java/io/ByteArrayOutputStream:size	()I
    //   84: ifeq +7 -> 91
    //   87: aload_2
    //   88: invokevirtual 288	java/io/ByteArrayOutputStream:close	()V
    //   91: aload_0
    //   92: areturn
    //   93: astore_0
    //   94: aload_2
    //   95: astore_1
    //   96: goto +36 -> 132
    //   99: astore_0
    //   100: aload_2
    //   101: astore_0
    //   102: goto +7 -> 109
    //   105: astore_0
    //   106: goto +26 -> 132
    //   109: aload_0
    //   110: astore_1
    //   111: new 200	com/megvii/meglive_sdk/a/d
    //   114: astore_2
    //   115: aload_0
    //   116: astore_1
    //   117: aload_2
    //   118: ldc -19
    //   120: ldc_w 290
    //   123: ldc 22
    //   125: invokespecial 206	com/megvii/meglive_sdk/a/d:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   128: aload_0
    //   129: astore_1
    //   130: aload_2
    //   131: athrow
    //   132: aload 5
    //   134: invokevirtual 284	java/util/zip/Deflater:end	()V
    //   137: aload_1
    //   138: invokevirtual 287	java/io/ByteArrayOutputStream:size	()I
    //   141: ifeq +7 -> 148
    //   144: aload_1
    //   145: invokevirtual 288	java/io/ByteArrayOutputStream:close	()V
    //   148: aload_0
    //   149: athrow
    //   150: astore_0
    //   151: aload_3
    //   152: astore_0
    //   153: goto -44 -> 109
    //   156: astore_1
    //   157: goto -66 -> 91
    //   160: astore_1
    //   161: goto -13 -> 148
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	164	0	paramArrayOfByte	byte[]
    //   16	129	1	localObject1	Object
    //   156	1	1	localIOException1	java.io.IOException
    //   160	1	1	localIOException2	java.io.IOException
    //   20	111	2	localObject2	Object
    //   10	142	3	localObject3	Object
    //   12	10	4	localObject4	Object
    //   7	126	5	localDeflater	java.util.zip.Deflater
    // Exception table:
    //   from	to	target	type
    //   30	47	93	finally
    //   47	67	93	finally
    //   70	75	93	finally
    //   30	47	99	java/lang/Exception
    //   47	67	99	java/lang/Exception
    //   70	75	99	java/lang/Exception
    //   17	21	105	finally
    //   24	30	105	finally
    //   111	115	105	finally
    //   117	128	105	finally
    //   130	132	105	finally
    //   17	21	150	java/lang/Exception
    //   24	30	150	java/lang/Exception
    //   80	91	156	java/io/IOException
    //   137	148	160	java/io/IOException
  }
  
  private static String b(byte[] paramArrayOfByte)
  {
    try
    {
      Object localObject2 = MessageDigest.getInstance("MD5");
      Object localObject1 = new java/math/BigInteger;
      ((BigInteger)localObject1).<init>(1, ((MessageDigest)localObject2).digest(paramArrayOfByte));
      paramArrayOfByte = ((BigInteger)localObject1).toString(16).toUpperCase();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      for (int i = 0; paramArrayOfByte.length() + i < 32; i++) {
        ((StringBuilder)localObject1).append("0");
      }
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(((StringBuilder)localObject1).toString());
      ((StringBuilder)localObject2).append(paramArrayOfByte);
      paramArrayOfByte = ((StringBuilder)localObject2).toString();
      return paramArrayOfByte;
    }
    catch (NoSuchAlgorithmException paramArrayOfByte)
    {
      throw new d("LogClientError", "Not Supported signature method MD5", paramArrayOfByte, "");
    }
  }
  
  public final Map<String, String> a(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("x-log-apiversion", "0.6.0");
    localHashMap.put("x-log-signaturemethod", "hmac-sha1");
    localHashMap.put("Content-Type", "application/json");
    Calendar localCalendar = Calendar.getInstance();
    Object localObject = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
    ((SimpleDateFormat)localObject).setTimeZone(TimeZone.getTimeZone("GMT"));
    localHashMap.put("Date", ((SimpleDateFormat)localObject).format(localCalendar.getTime()));
    localHashMap.put("Content-MD5", b(paramArrayOfByte2));
    localHashMap.put("Content-Length", String.valueOf(paramArrayOfByte2.length));
    localHashMap.put("x-log-bodyrawsize", String.valueOf(paramArrayOfByte1.length));
    localHashMap.put("x-log-compresstype", "deflate");
    paramArrayOfByte1 = new StringBuilder();
    paramArrayOfByte1.append(this.b);
    paramArrayOfByte1.append(".");
    paramArrayOfByte1.append(this.a);
    localHashMap.put("Host", paramArrayOfByte1.toString());
    paramArrayOfByte1 = new StringBuilder("POST\n");
    paramArrayOfByte2 = new StringBuilder();
    paramArrayOfByte2.append((String)localHashMap.get("Content-MD5"));
    paramArrayOfByte2.append("\n");
    paramArrayOfByte1.append(paramArrayOfByte2.toString());
    paramArrayOfByte2 = new StringBuilder();
    paramArrayOfByte2.append((String)localHashMap.get("Content-Type"));
    paramArrayOfByte2.append("\n");
    paramArrayOfByte1.append(paramArrayOfByte2.toString());
    paramArrayOfByte2 = new StringBuilder();
    paramArrayOfByte2.append((String)localHashMap.get("Date"));
    paramArrayOfByte2.append("\n");
    paramArrayOfByte1.append(paramArrayOfByte2.toString());
    paramArrayOfByte2 = this.f;
    if ((paramArrayOfByte2 != null) && (paramArrayOfByte2 != ""))
    {
      localHashMap.put("x-acs-security-token", paramArrayOfByte2);
      paramArrayOfByte2 = new StringBuilder("x-acs-security-token:");
      paramArrayOfByte2.append((String)localHashMap.get("x-acs-security-token"));
      paramArrayOfByte2.append("\n");
      paramArrayOfByte1.append(paramArrayOfByte2.toString());
    }
    paramArrayOfByte1.append("x-log-apiversion:0.6.0\n");
    paramArrayOfByte2 = new StringBuilder("x-log-bodyrawsize:");
    paramArrayOfByte2.append((String)localHashMap.get("x-log-bodyrawsize"));
    paramArrayOfByte2.append("\n");
    paramArrayOfByte1.append(paramArrayOfByte2.toString());
    paramArrayOfByte1.append("x-log-compresstype:deflate\nx-log-signaturemethod:hmac-sha1\n");
    paramArrayOfByte2 = new StringBuilder("/logstores/");
    paramArrayOfByte2.append(paramString);
    paramArrayOfByte2.append("/shards/lb");
    paramArrayOfByte1.append(paramArrayOfByte2.toString());
    paramArrayOfByte1 = paramArrayOfByte1.toString();
    try
    {
      paramString = this.e.getBytes("UTF-8");
      paramArrayOfByte1 = paramArrayOfByte1.getBytes("UTF-8");
      paramArrayOfByte2 = Mac.getInstance("HmacSHA1");
      localObject = new javax/crypto/spec/SecretKeySpec;
      ((SecretKeySpec)localObject).<init>(paramString, "HmacSHA1");
      paramArrayOfByte2.init((Key)localObject);
      paramString = new java/lang/String;
      paramString.<init>(b.a(paramArrayOfByte2.doFinal(paramArrayOfByte1)));
      paramArrayOfByte1 = new java/lang/StringBuilder;
      paramArrayOfByte1.<init>("LOG ");
      paramArrayOfByte1.append(this.d);
      paramArrayOfByte1.append(":");
      paramArrayOfByte1.append(paramString);
      localHashMap.put("Authorization", paramArrayOfByte1.toString());
      return localHashMap;
    }
    catch (Exception paramString)
    {
      throw new d("LogClientError", "fail to get encode signature", paramString, "");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */