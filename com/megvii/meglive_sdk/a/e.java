package com.megvii.meglive_sdk.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class e
{
  protected List<a> a = new ArrayList();
  private String b = "";
  private String c = "";
  
  public e() {}
  
  public e(String paramString1, String paramString2)
  {
    this.b = paramString1;
    this.c = paramString2;
  }
  
  public final String a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("__source__", this.c);
      localJSONObject.put("__topic__", this.b);
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>();
      Iterator localIterator = this.a.iterator();
      while (localIterator.hasNext())
      {
        Map localMap = ((a)localIterator.next()).a;
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>(localMap);
        localJSONArray.put(localObject);
      }
      localJSONObject.put("__logs__", localJSONArray);
      Object localObject = localJSONObject.toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return null;
  }
  
  public final void a(a parama)
  {
    this.a.add(parama);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */