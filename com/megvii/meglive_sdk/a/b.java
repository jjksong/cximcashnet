package com.megvii.meglive_sdk.a;

public final class b
{
  private static char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();
  private static byte[] b = new byte['Ā'];
  
  static
  {
    for (int i = 0; i < 256; i++) {
      b[i] = -1;
    }
    for (i = 65; i <= 90; i++) {
      b[i] = ((byte)(i - 65));
    }
    for (i = 97; i <= 122; i++) {
      b[i] = ((byte)(i + 26 - 97));
    }
    for (i = 48; i <= 57; i++) {
      b[i] = ((byte)(i + 52 - 48));
    }
    byte[] arrayOfByte = b;
    arrayOfByte[43] = 62;
    arrayOfByte[47] = 63;
  }
  
  public static String a(String paramString)
  {
    return new String(a(paramString.getBytes()));
  }
  
  public static char[] a(byte[] paramArrayOfByte)
  {
    char[] arrayOfChar1 = new char[(paramArrayOfByte.length + 2) / 3 * 4];
    int j = 0;
    for (int i = 0; j < paramArrayOfByte.length; i += 4)
    {
      int m = (paramArrayOfByte[j] & 0xFF) << 8;
      int k = j + 1;
      int i1 = paramArrayOfByte.length;
      int n = 1;
      if (k < i1)
      {
        m |= paramArrayOfByte[k] & 0xFF;
        k = 1;
      }
      else
      {
        k = 0;
      }
      m <<= 8;
      i1 = j + 2;
      if (i1 < paramArrayOfByte.length) {
        m |= paramArrayOfByte[i1] & 0xFF;
      } else {
        n = 0;
      }
      char[] arrayOfChar2 = a;
      i1 = 64;
      if (n != 0) {
        n = m & 0x3F;
      } else {
        n = 64;
      }
      arrayOfChar1[(i + 3)] = arrayOfChar2[n];
      n = m >> 6;
      arrayOfChar2 = a;
      m = i1;
      if (k != 0) {
        m = n & 0x3F;
      }
      arrayOfChar1[(i + 2)] = arrayOfChar2[m];
      k = n >> 6;
      arrayOfChar2 = a;
      arrayOfChar1[(i + 1)] = arrayOfChar2[(k & 0x3F)];
      arrayOfChar1[(i + 0)] = arrayOfChar2[(k >> 6 & 0x3F)];
      j += 3;
    }
    return arrayOfChar1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */