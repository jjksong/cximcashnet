package com.megvii.meglive_sdk.g;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.megvii.action.fmp.liveness.lib.jni.MegAuth;
import com.megvii.action.fmp.liveness.lib.jni.MegDelta;
import com.megvii.apo.m;
import com.megvii.meglive_sdk.f.d;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.u;
import com.megvii.meglive_sdk.i.w;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  private static b f;
  public Context a;
  public String b;
  public com.megvii.meglive_sdk.d.b c;
  public com.megvii.meglive_sdk.d.c d;
  public String e;
  
  private b(Context paramContext)
  {
    this.a = paramContext.getApplicationContext();
  }
  
  public static b a(Context paramContext)
  {
    try
    {
      if (f == null)
      {
        b localb = new com/megvii/meglive_sdk/g/b;
        localb.<init>(paramContext);
        f = localb;
      }
      paramContext = f;
      return paramContext;
    }
    finally {}
  }
  
  public static void a(h paramh)
  {
    int i = paramh.q;
    a.a().a(paramh);
  }
  
  private static void b(h paramh, com.megvii.meglive_sdk.f.e parame, boolean paramBoolean)
  {
    if (parame != null)
    {
      if (paramBoolean)
      {
        parame.a();
        return;
      }
      parame.b();
      return;
    }
    a(paramh);
  }
  
  public final void a(String paramString1, String paramString2, final com.megvii.meglive_sdk.f.e parame)
  {
    if (!com.megvii.meglive_sdk.c.a.a(this.a))
    {
      b(h.h, parame, false);
      return;
    }
    final Object localObject1 = m.a(a.a().a);
    ((m)localObject1).b = com.megvii.apo.util.a.a();
    localObject1 = ((m)localObject1).b;
    Object localObject2 = m.a(a.a().a).a();
    String str = com.megvii.meglive_sdk.i.e.d(this.a);
    Object localObject3 = new StringBuilder("language = ");
    ((StringBuilder)localObject3).append(str);
    k.a("getLivenessConfig", ((StringBuilder)localObject3).toString());
    localObject3 = new JSONObject();
    try
    {
      ((JSONObject)localObject3).put("biz_token", this.b);
      ((JSONObject)localObject3).put("os", "android");
      ((JSONObject)localObject3).put("key", this.e);
      ((JSONObject)localObject3).put("language", str);
      if (!TextUtils.isEmpty(paramString2)) {
        ((JSONObject)localObject3).put("advanced_option", paramString2);
      }
      paramString2 = new org/json/JSONObject;
      paramString2.<init>();
      paramString2.put("3932", "android");
      paramString2.put("6823", localObject2);
      paramString2.put("7833", localObject1);
      ((JSONObject)localObject3).put("finger", paramString2);
      paramString2 = new StringBuilder("json = ");
      paramString2.append(((JSONObject)localObject3).toString());
      k.b("getLivenessConfig  ", paramString2.toString());
      localObject3 = com.megvii.action.fmp.liveness.lib.b.c.a((JSONObject)localObject3);
      paramString2 = com.megvii.meglive_sdk.c.b.a();
      localObject2 = this.a;
      str = this.b;
      parame = new d()
      {
        public final void a(int paramAnonymousInt, byte[] paramAnonymousArrayOfByte)
        {
          StringBuilder localStringBuilder = new StringBuilder("code= ");
          localStringBuilder.append(paramAnonymousInt);
          localStringBuilder.append("  responseBody = ");
          localStringBuilder.append(paramAnonymousArrayOfByte);
          k.b("getLivenessConfig failure", localStringBuilder.toString());
          if (parame == null)
          {
            b.a(paramAnonymousArrayOfByte, 101);
            return;
          }
          b.a(h.b, parame, false);
        }
        
        public final void a(String paramAnonymousString)
        {
          Object localObject1 = null;
          try
          {
            Object localObject2 = new org/json/JSONObject;
            ((JSONObject)localObject2).<init>(paramAnonymousString);
            localObject2 = ((JSONObject)localObject2).optString("result");
            if (localObject2 != null)
            {
              localObject1 = Base64.decode((String)localObject2, 0);
              localObject2 = MegDelta.decodeJsonStr(b.a(b.this), (byte[])localObject1);
              localObject1 = new java/lang/String;
              ((String)localObject1).<init>((byte[])localObject2);
            }
            int i;
            return;
          }
          catch (JSONException paramAnonymousString)
          {
            try
            {
              localObject2 = new org/json/JSONObject;
              ((JSONObject)localObject2).<init>((String)localObject1);
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>("responseBody = ");
              ((StringBuilder)localObject1).append(paramAnonymousString);
              k.b("getLivenessConfig  success", ((StringBuilder)localObject1).toString());
              paramAnonymousString = new java/lang/StringBuilder;
              paramAnonymousString.<init>("dataStr = ");
              paramAnonymousString.append(((JSONObject)localObject2).toString());
              k.b("getLivenessConfig  success", paramAnonymousString.toString());
              if (((JSONObject)localObject2).has("option_code"))
              {
                i = ((JSONObject)localObject2).optInt("option_code", 0);
                com.megvii.meglive_sdk.i.e.a(b.b(b.this), i);
              }
              else
              {
                com.megvii.meglive_sdk.i.e.a(b.b(b.this), 0);
              }
              if (((JSONObject)localObject2).has("liveness_config"))
              {
                paramAnonymousString = ((JSONObject)localObject2).getJSONObject("liveness_config");
                u.a(b.b(b.this), "livenessConfig", paramAnonymousString.toString());
                paramAnonymousString = ((JSONObject)localObject2).optString("sdk_agreement_url");
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>("agreeUrl = ");
                ((StringBuilder)localObject1).append(paramAnonymousString);
                k.a("getLivenessConfig", ((StringBuilder)localObject1).toString());
                u.a(b.b(b.this), "agreeUrl", paramAnonymousString);
                paramAnonymousString = ((JSONObject)localObject2).optString("finger_config");
                b.a(b.this, paramAnonymousString, localObject1);
                b.a(h.a, parame, true);
                return;
              }
              b.a(h.b, parame, false);
              return;
            }
            catch (JSONException paramAnonymousString)
            {
              paramAnonymousString.printStackTrace();
              b.a(h.e, parame, false);
            }
            paramAnonymousString = paramAnonymousString;
            paramAnonymousString.printStackTrace();
            b.a(h.b, parame, false);
            return;
          }
        }
      };
      localObject1 = new StringBuilder("bizToken = ");
      ((StringBuilder)localObject1).append(str);
      k.a("getLivenessConfig", ((StringBuilder)localObject1).toString());
      localObject1 = new StringBuilder("data = ");
      ((StringBuilder)localObject1).append((String)localObject3);
      k.a("getLivenessConfig", ((StringBuilder)localObject1).toString());
      localObject1 = new StringBuilder("URL = ");
      ((StringBuilder)localObject1).append(w.b(paramString1));
      k.a("getLivenessConfig", ((StringBuilder)localObject1).toString());
      localObject1 = new HashMap();
      ((HashMap)localObject1).put("biz_token", str);
      ((HashMap)localObject1).put("data", localObject3);
      localObject3 = new HashMap();
      paramString2.a((Context)localObject2, w.b(paramString1), (Map)localObject1, (Map)localObject3, parame);
      return;
    }
    catch (JSONException paramString1)
    {
      paramString1.printStackTrace();
      b(h.e, parame, false);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/g/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */