package com.megvii.meglive_sdk.g;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Base64;
import com.megvii.apo.m;
import com.megvii.apo.m.3;
import com.megvii.apo.util.k;
import com.megvii.meglive_sdk.i.b;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.p;
import com.megvii.meglive_sdk.i.s;
import com.megvii.meglive_sdk.i.u;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class a
{
  private static a i = new a();
  public Context a;
  public String b;
  public String c;
  public int d = 0;
  public com.megvii.meglive_sdk.f.c e;
  public String f = "";
  public String g = "";
  public com.megvii.meglive_sdk.e.a.c h;
  private long j;
  private Thread.UncaughtExceptionHandler k;
  private com.megvii.meglive_sdk.f.a l;
  private Thread.UncaughtExceptionHandler m = new Thread.UncaughtExceptionHandler()
  {
    public final void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
    {
      try
      {
        com.megvii.meglive_sdk.b.c localc = new com/megvii/meglive_sdk/b/c;
        localc.<init>(a.a(a.this), a.b(a.this), paramAnonymousThrowable);
        b.a(localc.toString());
        if (a.c(a.this) != null)
        {
          a.c(a.this).uncaughtException(paramAnonymousThread, paramAnonymousThrowable);
          return;
        }
        paramAnonymousThread = new java/lang/RuntimeException;
        paramAnonymousThread.<init>(paramAnonymousThrowable);
        throw paramAnonymousThread;
      }
      catch (Exception paramAnonymousThread)
      {
        paramAnonymousThread.printStackTrace();
      }
    }
  };
  
  private static int a(int paramInt)
  {
    int n = paramInt;
    if (paramInt < 50000) {
      n = paramInt + 50000;
    }
    return n;
  }
  
  public static a a()
  {
    return i;
  }
  
  public static String b()
  {
    return "FaceIDZFAC 1.3.0A";
  }
  
  private void b(h paramh)
  {
    a(paramh.q, paramh.r);
  }
  
  public static String c()
  {
    return "87f86b699fc22f06efe1680291cf0aff94e9a200,35,20190121175917";
  }
  
  public final void a(int paramInt, String paramString)
  {
    this.c = "";
    this.f = "";
    this.g = "";
    com.megvii.meglive_sdk.f.a locala = this.l;
    if (locala != null)
    {
      locala.a(a(paramInt), paramString);
      this.l = null;
    }
    this.b = "";
    s.b();
    u.a(this.a, "bizToken", "");
    com.megvii.meglive_sdk.h.a.a = null;
    this.h = null;
  }
  
  public final void a(h paramh)
  {
    if (Thread.getDefaultUncaughtExceptionHandler() == this.m) {
      Thread.setDefaultUncaughtExceptionHandler(this.k);
    }
    com.megvii.meglive_sdk.f.c localc = this.e;
    if (localc != null)
    {
      localc.c(a(paramh.q), paramh.r);
      this.e = null;
    }
  }
  
  public final boolean a(com.megvii.meglive_sdk.f.a parama)
  {
    this.l = parama;
    parama = this.a;
    int n = Build.VERSION.SDK_INT;
    int i1 = 0;
    if (n >= 23)
    {
      if (parama.checkSelfPermission("android.permission.CAMERA") == 0) {
        n = 1;
      } else {
        n = 0;
      }
    }
    else if (parama.getPackageManager().checkPermission("android.permission.CAMERA", parama.getPackageName()) == 0) {
      n = 1;
    } else {
      n = 0;
    }
    if (n == 0)
    {
      b(h.k);
      return false;
    }
    parama = this.a;
    if (Build.VERSION.SDK_INT >= 23)
    {
      if (parama.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
        n = 1;
      } else {
        n = 0;
      }
    }
    else if (parama.getPackageManager().checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", parama.getPackageName()) == 0) {
      n = 1;
    } else {
      n = 0;
    }
    if (n == 0)
    {
      b(h.n);
      return false;
    }
    this.j = System.currentTimeMillis();
    this.k = Thread.getDefaultUncaughtExceptionHandler();
    Thread.setDefaultUncaughtExceptionHandler(this.m);
    parama = m.a(this.a);
    try
    {
      localObject1 = k.b(parama.a, "i_s", Integer.valueOf(0));
      if ((localObject1 != null) && (((Integer)localObject1).intValue() != 0))
      {
        localObject2 = parama.c;
        localObject1 = new com/megvii/apo/m$3;
        ((m.3)localObject1).<init>(parama);
        ((Handler)localObject2).post((Runnable)localObject1);
      }
    }
    catch (Throwable parama)
    {
      com.megvii.apo.util.e.a(parama);
    }
    p localp = new p(this.a);
    Object localObject2 = new HashMap();
    if (localp.a.getPackageManager().checkPermission("android.permission.RECORD_AUDIO", "packageName") == 0) {
      n = 1;
    } else {
      n = 0;
    }
    if (n != 0)
    {
      parama = new StringBuilder();
      parama.append(((TelephonyManager)localp.a.getSystemService("phone")).getDeviceId());
      parama.append(Settings.Secure.getString(localp.a.getContentResolver(), "android_id"));
      parama = p.a(parama.toString());
    }
    else
    {
      localObject3 = localp.a;
      localObject1 = (String)u.b((Context)localObject3, "uuid", "");
      parama = (com.megvii.meglive_sdk.f.a)localObject1;
      if ("".equals(localObject1))
      {
        parama = Base64.encodeToString(UUID.randomUUID().toString().getBytes(), 0);
        u.a((Context)localObject3, "uuid", parama);
      }
      parama = parama.replaceAll("\r|\n| ", "");
    }
    ((Map)localObject2).put("zid", parama);
    ((Map)localObject2).put("user_brand", Build.BRAND);
    ((Map)localObject2).put("user_model", Build.MODEL);
    parama = new StringBuilder("Android_");
    parama.append(Build.VERSION.RELEASE);
    ((Map)localObject2).put("user_os", parama.toString());
    parama = "";
    Object localObject1 = localp.a;
    Object localObject3 = ((ConnectivityManager)((Context)localObject1).getSystemService("connectivity")).getActiveNetworkInfo();
    n = i1;
    if (localObject3 != null)
    {
      n = i1;
      if (((NetworkInfo)localObject3).isConnected())
      {
        int i2 = ((NetworkInfo)localObject3).getType();
        if (i2 == 1)
        {
          n = 1;
        }
        else
        {
          n = i1;
          if (i2 == 0) {
            switch (((TelephonyManager)((Context)localObject1).getSystemService("phone")).getNetworkType())
            {
            default: 
              n = i1;
              break;
            case 13: 
              n = 4;
              break;
            case 3: 
            case 5: 
            case 6: 
            case 8: 
            case 9: 
            case 10: 
            case 12: 
            case 14: 
            case 15: 
              n = 3;
              break;
            case 1: 
            case 2: 
            case 4: 
            case 7: 
            case 11: 
              n = 2;
            }
          }
        }
      }
    }
    switch (n)
    {
    default: 
      break;
    case 4: 
      parama = "4G";
      break;
    case 3: 
      parama = "3G";
      break;
    case 2: 
      parama = "2G";
      break;
    case 1: 
      parama = "WIFI";
    }
    ((Map)localObject2).put("net_status", parama);
    n = ((WifiManager)localp.a.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getIpAddress();
    parama = new StringBuilder();
    parama.append(n & 0xFF);
    parama.append(".");
    parama.append(n >> 8 & 0xFF);
    parama.append(".");
    parama.append(n >> 16 & 0xFF);
    parama.append(".");
    parama.append(n >> 24 & 0xFF);
    ((Map)localObject2).put("user_ip", String.valueOf(parama.toString()));
    ((Map)localObject2).put("sdk_version", "FaceIDZFAC 1.3.0A");
    ((Map)localObject2).put("sdk_name", "FaceIDZFAC 1.3.0A");
    ((Map)localObject2).put("sdk_type", "FaceIDZFAC 1.3.0A");
    ((Map)localObject2).put("log_id", Integer.valueOf(1));
    ((Map)localObject2).put("host_app", localp.a());
    ((Map)localObject2).put("host_app_version", localp.b());
    ((Map)localObject2).put("sdk_language", com.megvii.meglive_sdk.i.e.d(localp.a));
    s.a(com.megvii.meglive_sdk.b.a.a((Map)localObject2, this.b));
    return true;
  }
  
  public final void b(int paramInt, String paramString)
  {
    this.c = "";
    this.f = "";
    this.g = "";
    com.megvii.meglive_sdk.f.a locala = this.l;
    if (locala != null)
    {
      locala.b(a(paramInt), paramString);
      this.l = null;
    }
    this.b = "";
    s.b();
    u.a(this.a, "bizToken", "");
    com.megvii.meglive_sdk.h.a.a = null;
    this.h = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/g/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */