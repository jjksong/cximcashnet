package com.megvii.meglive_sdk.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.widget.ImageView.ScaleType;
import com.megvii.meglive_sdk.volley.e;
import com.megvii.meglive_sdk.volley.j;
import com.megvii.meglive_sdk.volley.l;
import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.m.a;
import com.megvii.meglive_sdk.volley.o;
import com.megvii.meglive_sdk.volley.o.a;
import com.megvii.meglive_sdk.volley.o.b;
import com.megvii.meglive_sdk.volley.t;
import com.megvii.meglive_sdk.volley.u;

public final class h
  extends m<Bitmap>
{
  private static final Object r = new Object();
  private final o.b<Bitmap> a;
  private final Bitmap.Config b;
  private final int c;
  private final int p;
  private final ImageView.ScaleType q;
  
  public h(String paramString, o.b<Bitmap> paramb, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType, Bitmap.Config paramConfig, o.a parama)
  {
    super(0, paramString, parama);
    this.n = new e(1000, 2, 2.0F);
    this.a = paramb;
    this.b = paramConfig;
    this.c = paramInt1;
    this.p = paramInt2;
    this.q = paramScaleType;
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    double d2 = paramInt1;
    double d1 = paramInt3;
    Double.isNaN(d2);
    Double.isNaN(d1);
    d1 = d2 / d1;
    d2 = paramInt2;
    double d3 = paramInt4;
    Double.isNaN(d2);
    Double.isNaN(d3);
    d1 = Math.min(d1, d2 / d3);
    float f2;
    for (float f1 = 1.0F;; f1 = f2)
    {
      f2 = 2.0F * f1;
      if (f2 > d1) {
        break;
      }
    }
    return (int)f1;
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ImageView.ScaleType paramScaleType)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0)) {
      return paramInt3;
    }
    if (paramScaleType == ImageView.ScaleType.FIT_XY)
    {
      if (paramInt1 == 0) {
        return paramInt3;
      }
      return paramInt1;
    }
    if (paramInt1 == 0)
    {
      d1 = paramInt2;
      d2 = paramInt4;
      Double.isNaN(d1);
      Double.isNaN(d2);
      d2 = d1 / d2;
      d1 = paramInt3;
      Double.isNaN(d1);
      return (int)(d1 * d2);
    }
    if (paramInt2 == 0) {
      return paramInt1;
    }
    double d1 = paramInt4;
    double d2 = paramInt3;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    if (paramScaleType == ImageView.ScaleType.CENTER_CROP)
    {
      d3 = paramInt1;
      Double.isNaN(d3);
      d2 = paramInt2;
      if (d3 * d1 < d2)
      {
        Double.isNaN(d2);
        paramInt1 = (int)(d2 / d1);
      }
      return paramInt1;
    }
    d2 = paramInt1;
    Double.isNaN(d2);
    double d3 = paramInt2;
    if (d2 * d1 > d3)
    {
      Double.isNaN(d3);
      paramInt1 = (int)(d3 / d1);
    }
    return paramInt1;
  }
  
  protected final o<Bitmap> a(j paramj)
  {
    try
    {
      synchronized (r)
      {
        Object localObject2 = paramj.b;
        Object localObject1 = new android/graphics/BitmapFactory$Options;
        ((BitmapFactory.Options)localObject1).<init>();
        if ((this.c == 0) && (this.p == 0))
        {
          ((BitmapFactory.Options)localObject1).inPreferredConfig = this.b;
          localObject1 = BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
        }
        else
        {
          ((BitmapFactory.Options)localObject1).inJustDecodeBounds = true;
          BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
          int i = ((BitmapFactory.Options)localObject1).outWidth;
          int k = ((BitmapFactory.Options)localObject1).outHeight;
          int m = a(this.c, this.p, i, k, this.q);
          int j = a(this.p, this.c, k, i, this.q);
          ((BitmapFactory.Options)localObject1).inJustDecodeBounds = false;
          ((BitmapFactory.Options)localObject1).inSampleSize = a(i, k, m, j);
          localObject2 = BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
          localObject1 = localObject2;
          if (localObject2 != null) {
            if (((Bitmap)localObject2).getWidth() <= m)
            {
              localObject1 = localObject2;
              if (((Bitmap)localObject2).getHeight() <= j) {}
            }
            else
            {
              localObject1 = Bitmap.createScaledBitmap((Bitmap)localObject2, m, j, true);
              ((Bitmap)localObject2).recycle();
            }
          }
        }
        if (localObject1 == null)
        {
          localObject1 = new com/megvii/meglive_sdk/volley/l;
          ((l)localObject1).<init>(paramj);
          localObject1 = o.a((t)localObject1);
          paramj = (j)localObject1;
        }
        else
        {
          localObject1 = o.a(localObject1, d.a(paramj));
          paramj = (j)localObject1;
        }
        return paramj;
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      u.c("Caught OOM for %d byte liveness_image_center, url=%s", new Object[] { Integer.valueOf(paramj.b.length), this.e });
      paramj = new com/megvii/meglive_sdk/volley/l;
      paramj.<init>(localOutOfMemoryError);
      paramj = o.a(paramj);
      return paramj;
    }
  }
  
  public final m.a g()
  {
    return m.a.a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */