package com.megvii.meglive_sdk.volley.toolbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public final class b
{
  protected static final Comparator<byte[]> a = new Comparator() {};
  private final List<byte[]> b = new LinkedList();
  private final List<byte[]> c = new ArrayList(64);
  private int d = 0;
  private final int e = 4096;
  
  private void a()
  {
    try
    {
      while (this.d > this.e)
      {
        byte[] arrayOfByte = (byte[])this.b.remove(0);
        this.c.remove(arrayOfByte);
        this.d -= arrayOfByte.length;
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void a(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null) {
      try
      {
        if (paramArrayOfByte.length <= this.e)
        {
          this.b.add(paramArrayOfByte);
          int j = Collections.binarySearch(this.c, paramArrayOfByte, a);
          int i = j;
          if (j < 0) {
            i = -j - 1;
          }
          this.c.add(i, paramArrayOfByte);
          this.d += paramArrayOfByte.length;
          a();
          return;
        }
      }
      finally {}
    }
  }
  
  public final byte[] a(int paramInt)
  {
    int i = 0;
    try
    {
      while (i < this.c.size())
      {
        arrayOfByte = (byte[])this.c.get(i);
        if (arrayOfByte.length >= paramInt)
        {
          this.d -= arrayOfByte.length;
          this.c.remove(i);
          this.b.remove(arrayOfByte);
          return arrayOfByte;
        }
        i++;
      }
      byte[] arrayOfByte = new byte[paramInt];
      return arrayOfByte;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */