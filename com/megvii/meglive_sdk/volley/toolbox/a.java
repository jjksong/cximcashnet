package com.megvii.meglive_sdk.volley.toolbox;

import android.os.SystemClock;
import com.megvii.meglive_sdk.volley.a.c;
import com.megvii.meglive_sdk.volley.b.a;
import com.megvii.meglive_sdk.volley.d;
import com.megvii.meglive_sdk.volley.g;
import com.megvii.meglive_sdk.volley.j;
import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.q;
import com.megvii.meglive_sdk.volley.r;
import com.megvii.meglive_sdk.volley.s;
import com.megvii.meglive_sdk.volley.t;
import com.megvii.meglive_sdk.volley.u;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public final class a
  implements g
{
  protected static final boolean a = u.b;
  protected final e b;
  protected final b c;
  
  public a(e parame)
  {
    this(parame, new b());
  }
  
  private a(e parame, b paramb)
  {
    this.b = parame;
    this.c = paramb;
  }
  
  private static Map<String, String> a(com.megvii.meglive_sdk.volley.a.b[] paramArrayOfb)
  {
    TreeMap localTreeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
    for (int i = 0; i < paramArrayOfb.length; i++) {
      localTreeMap.put(paramArrayOfb[i].b(), paramArrayOfb[i].c());
    }
    return localTreeMap;
  }
  
  private static void a(String paramString, m<?> paramm, t paramt)
  {
    q localq = paramm.n;
    int i = paramm.h();
    try
    {
      localq.a(paramt);
      paramm.a(String.format("%s-retry [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      return;
    }
    catch (t paramt)
    {
      paramm.a(String.format("%s-timeout-giveup [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      throw paramt;
    }
  }
  
  private byte[] a(c paramc)
  {
    k localk = new k(this.c, (int)paramc.a());
    byte[] arrayOfByte = null;
    Object localObject = arrayOfByte;
    try
    {
      InputStream localInputStream = paramc.b();
      if (localInputStream != null)
      {
        localObject = arrayOfByte;
        paramc = this.c.a(1024);
        for (;;)
        {
          localObject = paramc;
          int i = localInputStream.read(paramc);
          if (i == -1) {
            break;
          }
          localObject = paramc;
          localk.write(paramc, 0, i);
        }
        localObject = paramc;
        arrayOfByte = localk.toByteArray();
        this.c.a(paramc);
        localk.close();
        return arrayOfByte;
      }
      localObject = arrayOfByte;
      paramc = new com/megvii/meglive_sdk/volley/r;
      localObject = arrayOfByte;
      paramc.<init>();
      localObject = arrayOfByte;
      throw paramc;
    }
    finally
    {
      this.c.a((byte[])localObject);
      localk.close();
    }
  }
  
  public final j a(m<?> paramm)
  {
    long l2 = SystemClock.elapsedRealtime();
    for (;;)
    {
      Object localObject3 = Collections.emptyMap();
      Object localObject5 = null;
      try
      {
        Object localObject4;
        try
        {
          Object localObject1 = new java/util/HashMap;
          ((HashMap)localObject1).<init>();
          Object localObject6 = paramm.o;
          if (localObject6 != null)
          {
            if (((b.a)localObject6).b != null) {
              ((Map)localObject1).put("If-None-Match", ((b.a)localObject6).b);
            }
            if (((b.a)localObject6).d > 0L)
            {
              localObject2 = new java/util/Date;
              ((Date)localObject2).<init>(((b.a)localObject6).d);
              ((Map)localObject1).put("If-Modified-Since", com.megvii.meglive_sdk.volley.a.a.a.a.a((Date)localObject2));
            }
          }
          localObject6 = this.b.a(paramm, (Map)localObject1);
          localObject2 = localObject3;
          localObject1 = localObject5;
          try
          {
            com.megvii.meglive_sdk.volley.a.i locali = ((com.megvii.meglive_sdk.volley.a.e)localObject6).b();
            localObject2 = localObject3;
            localObject1 = localObject5;
            i = locali.b();
            localObject2 = localObject3;
            localObject1 = localObject5;
            Map localMap = a(((com.megvii.meglive_sdk.volley.a.e)localObject6).a());
            if (i == 304)
            {
              localObject2 = localMap;
              localObject1 = localObject5;
              localObject3 = paramm.o;
              if (localObject3 == null)
              {
                localObject2 = localMap;
                localObject1 = localObject5;
                return new j(304, null, localMap, true, SystemClock.elapsedRealtime() - l2);
              }
              localObject2 = localMap;
              localObject1 = localObject5;
              ((b.a)localObject3).g.putAll(localMap);
              localObject2 = localMap;
              localObject1 = localObject5;
              return new j(304, ((b.a)localObject3).a, ((b.a)localObject3).g, true, SystemClock.elapsedRealtime() - l2);
            }
            localObject2 = localMap;
            localObject1 = localObject5;
            if (((com.megvii.meglive_sdk.volley.a.e)localObject6).c() != null)
            {
              localObject2 = localMap;
              localObject1 = localObject5;
              localObject3 = a(((com.megvii.meglive_sdk.volley.a.e)localObject6).c());
            }
            else
            {
              localObject2 = localMap;
              localObject1 = localObject5;
              localObject3 = new byte[0];
            }
            localObject2 = localMap;
            localObject1 = localObject3;
            long l1 = SystemClock.elapsedRealtime() - l2;
            localObject2 = localMap;
            localObject1 = localObject3;
            if ((a) || (l1 > 3000L))
            {
              if (localObject3 != null)
              {
                localObject2 = localMap;
                localObject1 = localObject3;
                localObject5 = Integer.valueOf(localObject3.length);
              }
              else
              {
                localObject5 = "null";
              }
              localObject2 = localMap;
              localObject1 = localObject3;
              u.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", new Object[] { paramm, Long.valueOf(l1), localObject5, Integer.valueOf(locali.b()), Integer.valueOf(paramm.n.b()) });
            }
            if ((i >= 200) && (i <= 299))
            {
              localObject2 = localMap;
              localObject1 = localObject3;
              return new j(i, (byte[])localObject3, localMap, false, SystemClock.elapsedRealtime() - l2);
            }
            localObject2 = localMap;
            localObject1 = localObject3;
            localObject5 = new java/io/IOException;
            localObject2 = localMap;
            localObject1 = localObject3;
            ((IOException)localObject5).<init>();
            localObject2 = localMap;
            localObject1 = localObject3;
            throw ((Throwable)localObject5);
          }
          catch (IOException localIOException2)
          {
            localObject5 = localObject1;
            localObject1 = localIOException2;
            localObject4 = localObject6;
          }
          if (localObject4 == null) {
            break label772;
          }
        }
        catch (IOException localIOException1)
        {
          localObject2 = localObject4;
          localObject4 = null;
          localObject5 = localObject4;
        }
        int i = ((com.megvii.meglive_sdk.volley.a.e)localObject4).b().b();
        u.c("Unexpected response code %d for %s", new Object[] { Integer.valueOf(i), paramm.e });
        j localj;
        if (localObject5 != null)
        {
          localj = new j(i, (byte[])localObject5, (Map)localObject2, false, SystemClock.elapsedRealtime() - l2);
          if ((i != 401) && (i != 403))
          {
            if ((i >= 400) && (i <= 499)) {
              throw new d(localj);
            }
            if ((i >= 500) && (i <= 599))
            {
              if (paramm.m) {
                a("server", paramm, new r(localj));
              } else {
                throw new r(localj);
              }
            }
            else {
              throw new r(localj);
            }
          }
          else
          {
            a("auth", paramm, new com.megvii.meglive_sdk.volley.a(localj));
          }
        }
        else
        {
          a("network", paramm, new com.megvii.meglive_sdk.volley.i());
          continue;
          label772:
          throw new com.megvii.meglive_sdk.volley.k(localj);
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        Object localObject2 = new StringBuilder("Bad URL ");
        ((StringBuilder)localObject2).append(paramm.e);
        throw new RuntimeException(((StringBuilder)localObject2).toString(), localMalformedURLException);
      }
      catch (com.megvii.meglive_sdk.volley.a.b.a locala)
      {
        a("connection", paramm, new s());
      }
      catch (SocketTimeoutException localSocketTimeoutException)
      {
        a("socket", paramm, new s());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */