package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.a.a.a.a;
import com.megvii.meglive_sdk.volley.b.a;
import com.megvii.meglive_sdk.volley.j;
import java.util.Date;
import java.util.Map;

public final class d
{
  private static long a(String paramString)
  {
    try
    {
      long l = a.a(paramString).getTime();
      return l;
    }
    catch (Exception paramString) {}
    return 0L;
  }
  
  public static b.a a(j paramj)
  {
    long l6 = System.currentTimeMillis();
    Map localMap = paramj.c;
    Object localObject1 = (String)localMap.get("Date");
    long l3;
    if (localObject1 != null) {
      l3 = a((String)localObject1);
    } else {
      l3 = 0L;
    }
    localObject1 = (String)localMap.get("Cache-Control");
    int i = 0;
    int j = 0;
    if (localObject1 != null)
    {
      localObject1 = ((String)localObject1).split(",");
      l2 = 0L;
      i = 0;
      l1 = 0L;
      while (j < localObject1.length)
      {
        localObject2 = localObject1[j].trim();
        if ((((String)localObject2).equals("no-cache")) || (((String)localObject2).equals("no-store")) || (((String)localObject2).startsWith("max-age="))) {}
        try
        {
          l4 = Long.parseLong(((String)localObject2).substring(8));
          l5 = l1;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            long l4 = l2;
            long l5 = l1;
          }
        }
        if (((String)localObject2).startsWith("stale-while-revalidate="))
        {
          l5 = Long.parseLong(((String)localObject2).substring(23));
          l4 = l2;
        }
        else if (!((String)localObject2).equals("must-revalidate"))
        {
          l4 = l2;
          l5 = l1;
          if (!((String)localObject2).equals("proxy-revalidate")) {}
        }
        else
        {
          i = 1;
          l5 = l1;
          l4 = l2;
        }
        j++;
        l2 = l4;
        l1 = l5;
        continue;
        return null;
      }
      j = 1;
    }
    else
    {
      l2 = 0L;
      l1 = 0L;
      j = 0;
    }
    localObject1 = (String)localMap.get("Expires");
    if (localObject1 != null) {
      l5 = a((String)localObject1);
    } else {
      l5 = 0L;
    }
    localObject1 = (String)localMap.get("Last-Modified");
    if (localObject1 != null) {
      l4 = a((String)localObject1);
    } else {
      l4 = 0L;
    }
    localObject1 = (String)localMap.get("ETag");
    if (j != 0)
    {
      l2 = l6 + l2 * 1000L;
      if (i != 0)
      {
        l1 = l2;
      }
      else
      {
        Long.signum(l1);
        l1 = l1 * 1000L + l2;
      }
    }
    else if ((l3 > 0L) && (l5 >= l3))
    {
      l1 = l5 - l3 + l6;
      l2 = l1;
    }
    else
    {
      l2 = 0L;
      l1 = l2;
    }
    Object localObject2 = new b.a();
    ((b.a)localObject2).a = paramj.b;
    ((b.a)localObject2).b = ((String)localObject1);
    ((b.a)localObject2).f = l2;
    ((b.a)localObject2).e = l1;
    ((b.a)localObject2).c = l3;
    ((b.a)localObject2).d = l4;
    ((b.a)localObject2).g = localMap;
    return (b.a)localObject2;
  }
  
  public static String a(Map<String, String> paramMap)
  {
    paramMap = (String)paramMap.get("Content-Type");
    if (paramMap != null)
    {
      String[] arrayOfString = paramMap.split(";");
      for (int i = 1; i < arrayOfString.length; i++)
      {
        paramMap = arrayOfString[i].trim().split("=");
        if ((paramMap.length == 2) && (paramMap[0].equals("charset"))) {
          return paramMap[1];
        }
      }
    }
    return "ISO-8859-1";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */