package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.o;
import com.megvii.meglive_sdk.volley.o.a;
import com.megvii.meglive_sdk.volley.o.b;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class j
  extends m<String>
{
  private Map<String, String> a = new HashMap();
  private o.b<String> b;
  public i c;
  
  public j(String paramString, o.b<String> paramb, o.a parama)
  {
    super(1, paramString, parama);
    this.b = paramb;
  }
  
  protected final o<String> a(com.megvii.meglive_sdk.volley.j paramj)
  {
    String str2;
    try
    {
      String str1 = new java/lang/String;
      str1.<init>(paramj.b, d.a(paramj.c));
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str2 = new String(paramj.b);
    }
    return o.a(str2, d.a(paramj));
  }
  
  public Map<String, String> b()
  {
    return this.a;
  }
  
  public final void c()
  {
    super.c();
    this.b = null;
  }
  
  public final String e()
  {
    i locali = this.c;
    StringBuilder localStringBuilder = new StringBuilder("multipart/form-data; boundary=");
    localStringBuilder.append(locali.c);
    return new com.megvii.meglive_sdk.volley.a.d.b("Content-Type", localStringBuilder.toString()).c();
  }
  
  public final byte[] f()
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      i locali = this.c;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("--");
      ((StringBuilder)localObject).append(locali.c);
      ((StringBuilder)localObject).append("--\r\n");
      localObject = ((StringBuilder)localObject).toString();
      locali.d.write(((String)localObject).getBytes());
      localByteArrayOutputStream.write(locali.d.toByteArray());
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */