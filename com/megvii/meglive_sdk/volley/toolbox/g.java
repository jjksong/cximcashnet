package com.megvii.meglive_sdk.volley.toolbox;

import android.graphics.Bitmap;
import android.os.Handler;
import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.n;
import com.megvii.meglive_sdk.volley.o.a;
import com.megvii.meglive_sdk.volley.o.b;
import com.megvii.meglive_sdk.volley.t;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public final class g
{
  final n a;
  final b b;
  final HashMap<String, a> c;
  final HashMap<String, a> d;
  Runnable e;
  private int f;
  private final Handler g;
  
  final void a(String paramString, a parama)
  {
    this.d.put(paramString, parama);
    if (this.e == null)
    {
      this.e = new Runnable()
      {
        public final void run()
        {
          Iterator localIterator1 = g.this.d.values().iterator();
          while (localIterator1.hasNext())
          {
            g.a locala = (g.a)localIterator1.next();
            Iterator localIterator2 = locala.c.iterator();
            while (localIterator2.hasNext())
            {
              g.c localc = (g.c)localIterator2.next();
              if (localc.b != null) {
                if (locala.b == null)
                {
                  localc.a = locala.a;
                  localc.b.a(localc, false);
                }
                else
                {
                  localc.b.a(locala.b);
                }
              }
            }
          }
          g.this.d.clear();
          g.this.e = null;
        }
      };
      this.g.postDelayed(this.e, this.f);
    }
  }
  
  private final class a
  {
    Bitmap a;
    t b;
    final LinkedList<g.c> c = new LinkedList();
    private final m<?> e;
    
    public a(g.c paramc)
    {
      this.e = paramc;
      Object localObject;
      this.c.add(localObject);
    }
    
    public final boolean a(g.c paramc)
    {
      this.c.remove(paramc);
      if (this.c.size() == 0)
      {
        this.e.k = true;
        return true;
      }
      return false;
    }
  }
  
  public static abstract interface b
  {
    public abstract Bitmap a();
  }
  
  public final class c
  {
    Bitmap a;
    final g.d b;
    final String c;
    private final String e;
    
    public c(Bitmap paramBitmap, String paramString1, String paramString2, g.d paramd)
    {
      this.a = paramBitmap;
      this.c = paramString1;
      this.e = paramString2;
      this.b = paramd;
    }
    
    public final void a()
    {
      if (this.b == null) {
        return;
      }
      g.a locala = (g.a)g.this.c.get(this.e);
      if (locala != null)
      {
        if (locala.a(this)) {
          g.this.c.remove(this.e);
        }
        return;
      }
      locala = (g.a)g.this.d.get(this.e);
      if (locala != null)
      {
        locala.a(this);
        if (locala.c.size() == 0) {
          g.this.d.remove(this.e);
        }
      }
    }
  }
  
  public static abstract interface d
    extends o.a
  {
    public abstract void a(g.c paramc, boolean paramBoolean);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */