package com.megvii.meglive_sdk.volley.toolbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.n;
import com.megvii.meglive_sdk.volley.t;
import java.util.HashMap;
import java.util.LinkedList;

public class NetworkImageView
  extends ImageView
{
  private String a;
  private int b;
  private int c;
  private g d;
  private g.c e;
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void a()
  {
    int i = this.b;
    if (i != 0)
    {
      setImageResource(i);
      return;
    }
    setImageBitmap(null);
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    invalidate();
  }
  
  protected void onDetachedFromWindow()
  {
    g.c localc = this.e;
    if (localc != null)
    {
      localc.a();
      setImageBitmap(null);
      this.e = null;
    }
    super.onDetachedFromWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    int i = getWidth();
    paramInt4 = getHeight();
    Object localObject2 = getScaleType();
    if (getLayoutParams() != null)
    {
      if (getLayoutParams().width == -2) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      }
      if (getLayoutParams().height == -2)
      {
        paramInt3 = 1;
        paramInt2 = paramInt1;
        paramInt1 = paramInt3;
      }
      else
      {
        paramInt3 = 0;
        paramInt2 = paramInt1;
        paramInt1 = paramInt3;
      }
    }
    else
    {
      paramInt2 = 0;
      paramInt1 = 0;
    }
    if ((paramInt2 != 0) && (paramInt1 != 0)) {
      paramInt3 = 1;
    } else {
      paramInt3 = 0;
    }
    if ((i != 0) || (paramInt4 != 0) || (paramInt3 != 0))
    {
      if (TextUtils.isEmpty(this.a))
      {
        localObject1 = this.e;
        if (localObject1 != null)
        {
          ((g.c)localObject1).a();
          this.e = null;
        }
        a();
        return;
      }
      Object localObject1 = this.e;
      if ((localObject1 != null) && (((g.c)localObject1).c != null))
      {
        if (!this.e.c.equals(this.a))
        {
          this.e.a();
          a();
        }
      }
      else
      {
        paramInt3 = i;
        if (paramInt2 != 0) {
          paramInt3 = 0;
        }
        if (paramInt1 != 0) {
          paramInt1 = 0;
        } else {
          paramInt1 = paramInt4;
        }
        g localg = this.d;
        String str1 = this.a;
        Object localObject3 = new g.d()
        {
          public final void a(t paramAnonymoust)
          {
            if (NetworkImageView.a(NetworkImageView.this) != 0)
            {
              paramAnonymoust = NetworkImageView.this;
              paramAnonymoust.setImageResource(NetworkImageView.a(paramAnonymoust));
            }
          }
          
          public final void a(final g.c paramAnonymousc, boolean paramAnonymousBoolean)
          {
            if ((paramAnonymousBoolean) && (this.a))
            {
              NetworkImageView.this.post(new Runnable()
              {
                public final void run()
                {
                  NetworkImageView.1.this.a(paramAnonymousc, false);
                }
              });
              return;
            }
            if (paramAnonymousc.a != null)
            {
              NetworkImageView.this.setImageBitmap(paramAnonymousc.a);
              return;
            }
            if (NetworkImageView.b(NetworkImageView.this) != 0)
            {
              paramAnonymousc = NetworkImageView.this;
              paramAnonymousc.setImageResource(NetworkImageView.b(paramAnonymousc));
            }
          }
        };
        if (Looper.myLooper() != Looper.getMainLooper()) {
          break label548;
        }
        localObject1 = new StringBuilder(str1.length() + 12);
        ((StringBuilder)localObject1).append("#W");
        ((StringBuilder)localObject1).append(paramInt3);
        ((StringBuilder)localObject1).append("#H");
        ((StringBuilder)localObject1).append(paramInt1);
        ((StringBuilder)localObject1).append("#S");
        ((StringBuilder)localObject1).append(((ImageView.ScaleType)localObject2).ordinal());
        ((StringBuilder)localObject1).append(str1);
        String str2 = ((StringBuilder)localObject1).toString();
        localObject1 = localg.b.a();
        if (localObject1 != null)
        {
          localObject1 = new g.c(localg, (Bitmap)localObject1, str1, null, null);
          ((g.d)localObject3).a((g.c)localObject1, true);
        }
        else
        {
          localObject1 = new g.c(localg, null, str1, str2, (g.d)localObject3);
          ((g.d)localObject3).a((g.c)localObject1, true);
          localObject3 = (g.a)localg.c.get(str2);
          if (localObject3 != null)
          {
            ((g.a)localObject3).c.add(localObject1);
          }
          else
          {
            g.1 local1 = new g.1(localg, str2);
            localObject3 = Bitmap.Config.RGB_565;
            g.2 local2 = new g.2(localg, str2);
            localObject2 = new h(str1, local1, paramInt3, paramInt1, (ImageView.ScaleType)localObject2, (Bitmap.Config)localObject3, local2);
            localg.a.a((m)localObject2);
            localg.c.put(str2, new g.a(localg, (m)localObject2, (g.c)localObject1));
          }
        }
        this.e = ((g.c)localObject1);
      }
    }
    return;
    label548:
    throw new IllegalStateException("ImageLoader must be invoked from the main thread.");
  }
  
  public void setDefaultImageResId(int paramInt)
  {
    this.b = paramInt;
  }
  
  public void setErrorImageResId(int paramInt)
  {
    this.c = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/NetworkImageView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */