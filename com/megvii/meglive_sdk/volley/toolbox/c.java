package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.b;
import com.megvii.meglive_sdk.volley.b.a;
import com.megvii.meglive_sdk.volley.u;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class c
  implements b
{
  private final Map<String, a> a = new LinkedHashMap(16, 0.75F, true);
  private long b = 0L;
  private final File c;
  private final int d;
  
  private c(File paramFile)
  {
    this.c = paramFile;
    this.d = 5242880;
  }
  
  public c(File paramFile, byte paramByte)
  {
    this(paramFile);
  }
  
  static int a(InputStream paramInputStream)
  {
    int j = c(paramInputStream);
    int i = c(paramInputStream);
    int k = c(paramInputStream);
    return c(paramInputStream) << 24 | j << 0 | 0x0 | i << 8 | k << 16;
  }
  
  private static InputStream a(File paramFile)
  {
    return new FileInputStream(paramFile);
  }
  
  static String a(b paramb)
  {
    return new String(a(paramb, b(paramb)), "UTF-8");
  }
  
  static void a(OutputStream paramOutputStream, int paramInt)
  {
    paramOutputStream.write(paramInt >> 0 & 0xFF);
    paramOutputStream.write(paramInt >> 8 & 0xFF);
    paramOutputStream.write(paramInt >> 16 & 0xFF);
    paramOutputStream.write(paramInt >> 24 & 0xFF);
  }
  
  static void a(OutputStream paramOutputStream, long paramLong)
  {
    paramOutputStream.write((byte)(int)(paramLong >>> 0));
    paramOutputStream.write((byte)(int)(paramLong >>> 8));
    paramOutputStream.write((byte)(int)(paramLong >>> 16));
    paramOutputStream.write((byte)(int)(paramLong >>> 24));
    paramOutputStream.write((byte)(int)(paramLong >>> 32));
    paramOutputStream.write((byte)(int)(paramLong >>> 40));
    paramOutputStream.write((byte)(int)(paramLong >>> 48));
    paramOutputStream.write((byte)(int)(paramLong >>> 56));
  }
  
  static void a(OutputStream paramOutputStream, String paramString)
  {
    paramString = paramString.getBytes("UTF-8");
    a(paramOutputStream, paramString.length);
    paramOutputStream.write(paramString, 0, paramString.length);
  }
  
  private void a(String paramString, a parama)
  {
    if (!this.a.containsKey(paramString))
    {
      this.b += parama.a;
    }
    else
    {
      a locala = (a)this.a.get(paramString);
      this.b += parama.a - locala.a;
    }
    this.a.put(paramString, parama);
  }
  
  private static byte[] a(b paramb, long paramLong)
  {
    long l = paramb.a();
    if ((paramLong >= 0L) && (paramLong <= l))
    {
      int i = (int)paramLong;
      if (i == paramLong)
      {
        byte[] arrayOfByte = new byte[i];
        new DataInputStream(paramb).readFully(arrayOfByte);
        return arrayOfByte;
      }
    }
    paramb = new StringBuilder("streamToBytes length=");
    paramb.append(paramLong);
    paramb.append(", maxLength=");
    paramb.append(l);
    throw new IOException(paramb.toString());
  }
  
  static long b(InputStream paramInputStream)
  {
    return (c(paramInputStream) & 0xFF) << 0 | 0L | (c(paramInputStream) & 0xFF) << 8 | (c(paramInputStream) & 0xFF) << 16 | (c(paramInputStream) & 0xFF) << 24 | (c(paramInputStream) & 0xFF) << 32 | (c(paramInputStream) & 0xFF) << 40 | (c(paramInputStream) & 0xFF) << 48 | (0xFF & c(paramInputStream)) << 56;
  }
  
  static Map<String, String> b(b paramb)
  {
    int j = a(paramb);
    Object localObject;
    if (j == 0) {
      localObject = Collections.emptyMap();
    } else {
      localObject = new HashMap(j);
    }
    for (int i = 0; i < j; i++) {
      ((Map)localObject).put(a(paramb).intern(), a(paramb).intern());
    }
    return (Map<String, String>)localObject;
  }
  
  private void b(String paramString)
  {
    try
    {
      boolean bool = d(paramString).delete();
      e(paramString);
      if (!bool) {
        u.b("Could not delete cache entry for key=%s, filename=%s", new Object[] { paramString, c(paramString) });
      }
      return;
    }
    finally {}
  }
  
  private static int c(InputStream paramInputStream)
  {
    int i = paramInputStream.read();
    if (i != -1) {
      return i;
    }
    throw new EOFException();
  }
  
  private static String c(String paramString)
  {
    int i = paramString.length() / 2;
    int j = paramString.substring(0, i).hashCode();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(String.valueOf(j));
    localStringBuilder.append(String.valueOf(paramString.substring(i).hashCode()));
    return localStringBuilder.toString();
  }
  
  private File d(String paramString)
  {
    return new File(this.c, c(paramString));
  }
  
  private void e(String paramString)
  {
    paramString = (a)this.a.remove(paramString);
    if (paramString != null) {
      this.b -= paramString.a;
    }
  }
  
  /* Error */
  public final b.a a(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 32	com/megvii/meglive_sdk/volley/toolbox/c:a	Ljava/util/Map;
    //   6: aload_1
    //   7: invokeinterface 95 2 0
    //   12: checkcast 8	com/megvii/meglive_sdk/volley/toolbox/c$a
    //   15: astore 4
    //   17: aload 4
    //   19: ifnonnull +7 -> 26
    //   22: aload_0
    //   23: monitorexit
    //   24: aconst_null
    //   25: areturn
    //   26: aload_0
    //   27: aload_1
    //   28: invokespecial 159	com/megvii/meglive_sdk/volley/toolbox/c:d	(Ljava/lang/String;)Ljava/io/File;
    //   31: astore_2
    //   32: new 11	com/megvii/meglive_sdk/volley/toolbox/c$b
    //   35: astore_3
    //   36: new 213	java/io/BufferedInputStream
    //   39: astore 5
    //   41: aload 5
    //   43: aload_2
    //   44: invokestatic 215	com/megvii/meglive_sdk/volley/toolbox/c:a	(Ljava/io/File;)Ljava/io/InputStream;
    //   47: invokespecial 216	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   50: aload_3
    //   51: aload 5
    //   53: aload_2
    //   54: invokevirtual 218	java/io/File:length	()J
    //   57: invokespecial 221	com/megvii/meglive_sdk/volley/toolbox/c$b:<init>	(Ljava/io/InputStream;J)V
    //   60: aload_3
    //   61: invokestatic 224	com/megvii/meglive_sdk/volley/toolbox/c$a:a	(Lcom/megvii/meglive_sdk/volley/toolbox/c$b;)Lcom/megvii/meglive_sdk/volley/toolbox/c$a;
    //   64: astore 5
    //   66: aload_1
    //   67: aload 5
    //   69: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   72: invokestatic 233	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   75: ifne +44 -> 119
    //   78: ldc -21
    //   80: iconst_3
    //   81: anewarray 4	java/lang/Object
    //   84: dup
    //   85: iconst_0
    //   86: aload_2
    //   87: invokevirtual 238	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   90: aastore
    //   91: dup
    //   92: iconst_1
    //   93: aload_1
    //   94: aastore
    //   95: dup
    //   96: iconst_2
    //   97: aload 5
    //   99: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   102: aastore
    //   103: invokestatic 178	com/megvii/meglive_sdk/volley/u:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   106: aload_0
    //   107: aload_1
    //   108: invokespecial 168	com/megvii/meglive_sdk/volley/toolbox/c:e	(Ljava/lang/String;)V
    //   111: aload_3
    //   112: invokevirtual 241	com/megvii/meglive_sdk/volley/toolbox/c$b:close	()V
    //   115: aload_0
    //   116: monitorexit
    //   117: aconst_null
    //   118: areturn
    //   119: aload_3
    //   120: aload_3
    //   121: invokevirtual 102	com/megvii/meglive_sdk/volley/toolbox/c$b:a	()J
    //   124: invokestatic 59	com/megvii/meglive_sdk/volley/toolbox/c:a	(Lcom/megvii/meglive_sdk/volley/toolbox/c$b;J)[B
    //   127: astore 5
    //   129: new 243	com/megvii/meglive_sdk/volley/b$a
    //   132: astore 6
    //   134: aload 6
    //   136: invokespecial 244	com/megvii/meglive_sdk/volley/b$a:<init>	()V
    //   139: aload 6
    //   141: aload 5
    //   143: putfield 247	com/megvii/meglive_sdk/volley/b$a:a	[B
    //   146: aload 6
    //   148: aload 4
    //   150: getfield 249	com/megvii/meglive_sdk/volley/toolbox/c$a:c	Ljava/lang/String;
    //   153: putfield 250	com/megvii/meglive_sdk/volley/b$a:b	Ljava/lang/String;
    //   156: aload 6
    //   158: aload 4
    //   160: getfield 252	com/megvii/meglive_sdk/volley/toolbox/c$a:d	J
    //   163: putfield 254	com/megvii/meglive_sdk/volley/b$a:c	J
    //   166: aload 6
    //   168: aload 4
    //   170: getfield 256	com/megvii/meglive_sdk/volley/toolbox/c$a:e	J
    //   173: putfield 257	com/megvii/meglive_sdk/volley/b$a:d	J
    //   176: aload 6
    //   178: aload 4
    //   180: getfield 260	com/megvii/meglive_sdk/volley/toolbox/c$a:f	J
    //   183: putfield 261	com/megvii/meglive_sdk/volley/b$a:e	J
    //   186: aload 6
    //   188: aload 4
    //   190: getfield 264	com/megvii/meglive_sdk/volley/toolbox/c$a:g	J
    //   193: putfield 265	com/megvii/meglive_sdk/volley/b$a:f	J
    //   196: aload 6
    //   198: aload 4
    //   200: getfield 268	com/megvii/meglive_sdk/volley/toolbox/c$a:h	Ljava/util/Map;
    //   203: putfield 270	com/megvii/meglive_sdk/volley/b$a:g	Ljava/util/Map;
    //   206: aload_3
    //   207: invokevirtual 241	com/megvii/meglive_sdk/volley/toolbox/c$b:close	()V
    //   210: aload_0
    //   211: monitorexit
    //   212: aload 6
    //   214: areturn
    //   215: astore 4
    //   217: aload_3
    //   218: invokevirtual 241	com/megvii/meglive_sdk/volley/toolbox/c$b:close	()V
    //   221: aload 4
    //   223: athrow
    //   224: astore_3
    //   225: ldc_w 272
    //   228: iconst_2
    //   229: anewarray 4	java/lang/Object
    //   232: dup
    //   233: iconst_0
    //   234: aload_2
    //   235: invokevirtual 238	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   238: aastore
    //   239: dup
    //   240: iconst_1
    //   241: aload_3
    //   242: invokevirtual 273	java/io/IOException:toString	()Ljava/lang/String;
    //   245: aastore
    //   246: invokestatic 178	com/megvii/meglive_sdk/volley/u:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   249: aload_0
    //   250: aload_1
    //   251: invokespecial 275	com/megvii/meglive_sdk/volley/toolbox/c:b	(Ljava/lang/String;)V
    //   254: aload_0
    //   255: monitorexit
    //   256: aconst_null
    //   257: areturn
    //   258: astore_1
    //   259: aload_0
    //   260: monitorexit
    //   261: aload_1
    //   262: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	263	0	this	c
    //   0	263	1	paramString	String
    //   31	204	2	localFile	File
    //   35	183	3	localb	b
    //   224	18	3	localIOException	IOException
    //   15	184	4	locala	a
    //   215	7	4	localObject1	Object
    //   39	103	5	localObject2	Object
    //   132	81	6	locala1	b.a
    // Exception table:
    //   from	to	target	type
    //   60	111	215	finally
    //   119	206	215	finally
    //   32	60	224	java/io/IOException
    //   111	115	224	java/io/IOException
    //   206	210	224	java/io/IOException
    //   217	224	224	java/io/IOException
    //   2	17	258	finally
    //   26	32	258	finally
    //   32	60	258	finally
    //   111	115	258	finally
    //   206	210	258	finally
    //   217	224	258	finally
    //   225	254	258	finally
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 36	com/megvii/meglive_sdk/volley/toolbox/c:c	Ljava/io/File;
    //   6: invokevirtual 278	java/io/File:exists	()Z
    //   9: istore_3
    //   10: iconst_0
    //   11: istore_1
    //   12: iload_3
    //   13: ifne +36 -> 49
    //   16: aload_0
    //   17: getfield 36	com/megvii/meglive_sdk/volley/toolbox/c:c	Ljava/io/File;
    //   20: invokevirtual 281	java/io/File:mkdirs	()Z
    //   23: ifne +23 -> 46
    //   26: ldc_w 283
    //   29: iconst_1
    //   30: anewarray 4	java/lang/Object
    //   33: dup
    //   34: iconst_0
    //   35: aload_0
    //   36: getfield 36	com/megvii/meglive_sdk/volley/toolbox/c:c	Ljava/io/File;
    //   39: invokevirtual 238	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   42: aastore
    //   43: invokestatic 285	com/megvii/meglive_sdk/volley/u:c	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: aload_0
    //   50: getfield 36	com/megvii/meglive_sdk/volley/toolbox/c:c	Ljava/io/File;
    //   53: invokevirtual 289	java/io/File:listFiles	()[Ljava/io/File;
    //   56: astore 7
    //   58: aload 7
    //   60: ifnonnull +6 -> 66
    //   63: aload_0
    //   64: monitorexit
    //   65: return
    //   66: aload 7
    //   68: arraylength
    //   69: istore_2
    //   70: iload_1
    //   71: iload_2
    //   72: if_icmpge +102 -> 174
    //   75: aload 7
    //   77: iload_1
    //   78: aaload
    //   79: astore 6
    //   81: aload 6
    //   83: invokevirtual 218	java/io/File:length	()J
    //   86: lstore 4
    //   88: new 11	com/megvii/meglive_sdk/volley/toolbox/c$b
    //   91: astore 8
    //   93: new 213	java/io/BufferedInputStream
    //   96: astore 9
    //   98: aload 9
    //   100: aload 6
    //   102: invokestatic 215	com/megvii/meglive_sdk/volley/toolbox/c:a	(Ljava/io/File;)Ljava/io/InputStream;
    //   105: invokespecial 216	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   108: aload 8
    //   110: aload 9
    //   112: lload 4
    //   114: invokespecial 221	com/megvii/meglive_sdk/volley/toolbox/c$b:<init>	(Ljava/io/InputStream;J)V
    //   117: aload 8
    //   119: invokestatic 224	com/megvii/meglive_sdk/volley/toolbox/c$a:a	(Lcom/megvii/meglive_sdk/volley/toolbox/c$b;)Lcom/megvii/meglive_sdk/volley/toolbox/c$a;
    //   122: astore 9
    //   124: aload 9
    //   126: lload 4
    //   128: putfield 91	com/megvii/meglive_sdk/volley/toolbox/c$a:a	J
    //   131: aload_0
    //   132: aload 9
    //   134: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   137: aload 9
    //   139: invokespecial 291	com/megvii/meglive_sdk/volley/toolbox/c:a	(Ljava/lang/String;Lcom/megvii/meglive_sdk/volley/toolbox/c$a;)V
    //   142: aload 8
    //   144: invokevirtual 241	com/megvii/meglive_sdk/volley/toolbox/c$b:close	()V
    //   147: goto +21 -> 168
    //   150: astore 9
    //   152: aload 8
    //   154: invokevirtual 241	com/megvii/meglive_sdk/volley/toolbox/c$b:close	()V
    //   157: aload 9
    //   159: athrow
    //   160: astore 8
    //   162: aload 6
    //   164: invokevirtual 165	java/io/File:delete	()Z
    //   167: pop
    //   168: iinc 1 1
    //   171: goto -101 -> 70
    //   174: aload_0
    //   175: monitorexit
    //   176: return
    //   177: astore 6
    //   179: aload_0
    //   180: monitorexit
    //   181: aload 6
    //   183: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	184	0	this	c
    //   11	158	1	i	int
    //   69	4	2	j	int
    //   9	4	3	bool	boolean
    //   86	41	4	l	long
    //   79	84	6	localFile	File
    //   177	5	6	localObject1	Object
    //   56	20	7	arrayOfFile	File[]
    //   91	62	8	localb	b
    //   160	1	8	localIOException	IOException
    //   96	42	9	localObject2	Object
    //   150	8	9	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   117	142	150	finally
    //   81	117	160	java/io/IOException
    //   142	147	160	java/io/IOException
    //   152	160	160	java/io/IOException
    //   2	10	177	finally
    //   16	46	177	finally
    //   49	58	177	finally
    //   66	70	177	finally
    //   81	117	177	finally
    //   142	147	177	finally
    //   152	160	177	finally
    //   162	168	177	finally
  }
  
  /* Error */
  public final void a(String paramString, b.a parama)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_2
    //   3: getfield 247	com/megvii/meglive_sdk/volley/b$a:a	[B
    //   6: arraylength
    //   7: istore_3
    //   8: aload_0
    //   9: getfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   12: lstore 4
    //   14: iload_3
    //   15: i2l
    //   16: lstore 6
    //   18: lload 4
    //   20: lload 6
    //   22: ladd
    //   23: aload_0
    //   24: getfield 39	com/megvii/meglive_sdk/volley/toolbox/c:d	I
    //   27: i2l
    //   28: lcmp
    //   29: iflt +223 -> 252
    //   32: getstatic 295	com/megvii/meglive_sdk/volley/u:b	Z
    //   35: ifeq +13 -> 48
    //   38: ldc_w 297
    //   41: iconst_0
    //   42: anewarray 4	java/lang/Object
    //   45: invokestatic 299	com/megvii/meglive_sdk/volley/u:a	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   48: aload_0
    //   49: getfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   52: lstore 4
    //   54: invokestatic 304	android/os/SystemClock:elapsedRealtime	()J
    //   57: lstore 8
    //   59: aload_0
    //   60: getfield 32	com/megvii/meglive_sdk/volley/toolbox/c:a	Ljava/util/Map;
    //   63: invokeinterface 308 1 0
    //   68: invokeinterface 314 1 0
    //   73: astore 10
    //   75: iconst_0
    //   76: istore_3
    //   77: aload 10
    //   79: invokeinterface 319 1 0
    //   84: ifeq +120 -> 204
    //   87: aload 10
    //   89: invokeinterface 323 1 0
    //   94: checkcast 325	java/util/Map$Entry
    //   97: invokeinterface 328 1 0
    //   102: checkcast 8	com/megvii/meglive_sdk/volley/toolbox/c$a
    //   105: astore 11
    //   107: aload_0
    //   108: aload 11
    //   110: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   113: invokespecial 159	com/megvii/meglive_sdk/volley/toolbox/c:d	(Ljava/lang/String;)Ljava/io/File;
    //   116: invokevirtual 165	java/io/File:delete	()Z
    //   119: ifeq +20 -> 139
    //   122: aload_0
    //   123: aload_0
    //   124: getfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   127: aload 11
    //   129: getfield 91	com/megvii/meglive_sdk/volley/toolbox/c$a:a	J
    //   132: lsub
    //   133: putfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   136: goto +31 -> 167
    //   139: ldc -86
    //   141: iconst_2
    //   142: anewarray 4	java/lang/Object
    //   145: dup
    //   146: iconst_0
    //   147: aload 11
    //   149: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   152: aastore
    //   153: dup
    //   154: iconst_1
    //   155: aload 11
    //   157: getfield 227	com/megvii/meglive_sdk/volley/toolbox/c$a:b	Ljava/lang/String;
    //   160: invokestatic 173	com/megvii/meglive_sdk/volley/toolbox/c:c	(Ljava/lang/String;)Ljava/lang/String;
    //   163: aastore
    //   164: invokestatic 178	com/megvii/meglive_sdk/volley/u:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   167: aload 10
    //   169: invokeinterface 330 1 0
    //   174: iinc 3 1
    //   177: aload_0
    //   178: getfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   181: lload 6
    //   183: ladd
    //   184: l2f
    //   185: aload_0
    //   186: getfield 39	com/megvii/meglive_sdk/volley/toolbox/c:d	I
    //   189: i2f
    //   190: ldc_w 331
    //   193: fmul
    //   194: fcmpg
    //   195: ifge +6 -> 201
    //   198: goto +6 -> 204
    //   201: goto -124 -> 77
    //   204: getstatic 295	com/megvii/meglive_sdk/volley/u:b	Z
    //   207: ifeq +45 -> 252
    //   210: ldc_w 333
    //   213: iconst_3
    //   214: anewarray 4	java/lang/Object
    //   217: dup
    //   218: iconst_0
    //   219: iload_3
    //   220: invokestatic 338	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   223: aastore
    //   224: dup
    //   225: iconst_1
    //   226: aload_0
    //   227: getfield 34	com/megvii/meglive_sdk/volley/toolbox/c:b	J
    //   230: lload 4
    //   232: lsub
    //   233: invokestatic 343	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   236: aastore
    //   237: dup
    //   238: iconst_2
    //   239: invokestatic 304	android/os/SystemClock:elapsedRealtime	()J
    //   242: lload 8
    //   244: lsub
    //   245: invokestatic 343	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   248: aastore
    //   249: invokestatic 299	com/megvii/meglive_sdk/volley/u:a	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   252: aload_0
    //   253: aload_1
    //   254: invokespecial 159	com/megvii/meglive_sdk/volley/toolbox/c:d	(Ljava/lang/String;)Ljava/io/File;
    //   257: astore 10
    //   259: new 345	java/io/BufferedOutputStream
    //   262: astore 11
    //   264: new 347	java/io/FileOutputStream
    //   267: astore 12
    //   269: aload 12
    //   271: aload 10
    //   273: invokespecial 348	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   276: aload 11
    //   278: aload 12
    //   280: invokespecial 351	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   283: new 8	com/megvii/meglive_sdk/volley/toolbox/c$a
    //   286: astore 12
    //   288: aload 12
    //   290: aload_1
    //   291: aload_2
    //   292: invokespecial 353	com/megvii/meglive_sdk/volley/toolbox/c$a:<init>	(Ljava/lang/String;Lcom/megvii/meglive_sdk/volley/b$a;)V
    //   295: aload 12
    //   297: aload 11
    //   299: invokevirtual 356	com/megvii/meglive_sdk/volley/toolbox/c$a:a	(Ljava/io/OutputStream;)Z
    //   302: ifeq +27 -> 329
    //   305: aload 11
    //   307: aload_2
    //   308: getfield 247	com/megvii/meglive_sdk/volley/b$a:a	[B
    //   311: invokevirtual 358	java/io/BufferedOutputStream:write	([B)V
    //   314: aload 11
    //   316: invokevirtual 359	java/io/BufferedOutputStream:close	()V
    //   319: aload_0
    //   320: aload_1
    //   321: aload 12
    //   323: invokespecial 291	com/megvii/meglive_sdk/volley/toolbox/c:a	(Ljava/lang/String;Lcom/megvii/meglive_sdk/volley/toolbox/c$a;)V
    //   326: aload_0
    //   327: monitorexit
    //   328: return
    //   329: aload 11
    //   331: invokevirtual 359	java/io/BufferedOutputStream:close	()V
    //   334: ldc_w 361
    //   337: iconst_1
    //   338: anewarray 4	java/lang/Object
    //   341: dup
    //   342: iconst_0
    //   343: aload 10
    //   345: invokevirtual 238	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   348: aastore
    //   349: invokestatic 178	com/megvii/meglive_sdk/volley/u:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   352: new 129	java/io/IOException
    //   355: astore_1
    //   356: aload_1
    //   357: invokespecial 362	java/io/IOException:<init>	()V
    //   360: aload_1
    //   361: athrow
    //   362: astore_1
    //   363: aload 10
    //   365: invokevirtual 165	java/io/File:delete	()Z
    //   368: ifne +21 -> 389
    //   371: ldc_w 364
    //   374: iconst_1
    //   375: anewarray 4	java/lang/Object
    //   378: dup
    //   379: iconst_0
    //   380: aload 10
    //   382: invokevirtual 238	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   385: aastore
    //   386: invokestatic 178	com/megvii/meglive_sdk/volley/u:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   389: aload_0
    //   390: monitorexit
    //   391: return
    //   392: astore_1
    //   393: aload_0
    //   394: monitorexit
    //   395: aload_1
    //   396: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	397	0	this	c
    //   0	397	1	paramString	String
    //   0	397	2	parama	b.a
    //   7	213	3	i	int
    //   12	219	4	l1	long
    //   16	166	6	l2	long
    //   57	186	8	l3	long
    //   73	308	10	localObject1	Object
    //   105	225	11	localObject2	Object
    //   267	55	12	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   259	326	362	java/io/IOException
    //   329	362	362	java/io/IOException
    //   2	14	392	finally
    //   18	48	392	finally
    //   48	75	392	finally
    //   77	136	392	finally
    //   139	167	392	finally
    //   167	174	392	finally
    //   177	198	392	finally
    //   204	252	392	finally
    //   252	259	392	finally
    //   259	326	392	finally
    //   329	362	392	finally
    //   363	389	392	finally
  }
  
  static final class a
  {
    long a;
    final String b;
    final String c;
    final long d;
    final long e;
    final long f;
    final long g;
    final Map<String, String> h;
    
    a(String paramString, b.a parama)
    {
      this(paramString, parama.b, parama.c, parama.d, parama.e, parama.f, parama.g);
      this.a = parama.a.length;
    }
    
    private a(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, Map<String, String> paramMap)
    {
      this.b = paramString1;
      paramString1 = paramString2;
      if ("".equals(paramString2)) {
        paramString1 = null;
      }
      this.c = paramString1;
      this.d = paramLong1;
      this.e = paramLong2;
      this.f = paramLong3;
      this.g = paramLong4;
      this.h = paramMap;
    }
    
    static a a(c.b paramb)
    {
      if (c.a(paramb) == 538247942) {
        return new a(c.a(paramb), c.a(paramb), c.b(paramb), c.b(paramb), c.b(paramb), c.b(paramb), c.b(paramb));
      }
      throw new IOException();
    }
    
    final boolean a(OutputStream paramOutputStream)
    {
      try
      {
        c.a(paramOutputStream, 538247942);
        c.a(paramOutputStream, this.b);
        if (this.c == null) {
          localObject = "";
        } else {
          localObject = this.c;
        }
        c.a(paramOutputStream, (String)localObject);
        c.a(paramOutputStream, this.d);
        c.a(paramOutputStream, this.e);
        c.a(paramOutputStream, this.f);
        c.a(paramOutputStream, this.g);
        Object localObject = this.h;
        if (localObject != null)
        {
          c.a(paramOutputStream, ((Map)localObject).size());
          Iterator localIterator = ((Map)localObject).entrySet().iterator();
          while (localIterator.hasNext())
          {
            localObject = (Map.Entry)localIterator.next();
            c.a(paramOutputStream, (String)((Map.Entry)localObject).getKey());
            c.a(paramOutputStream, (String)((Map.Entry)localObject).getValue());
          }
        }
        c.a(paramOutputStream, 0);
        paramOutputStream.flush();
        return true;
      }
      catch (IOException paramOutputStream)
      {
        u.b("%s", new Object[] { paramOutputStream.toString() });
      }
      return false;
    }
  }
  
  static final class b
    extends FilterInputStream
  {
    private final long a;
    private long b;
    
    b(InputStream paramInputStream, long paramLong)
    {
      super();
      this.a = paramLong;
    }
    
    final long a()
    {
      return this.a - this.b;
    }
    
    public final int read()
    {
      int i = super.read();
      if (i != -1) {
        this.b += 1L;
      }
      return i;
    }
    
    public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      paramInt1 = super.read(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt1 != -1) {
        this.b += paramInt1;
      }
      return paramInt1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */