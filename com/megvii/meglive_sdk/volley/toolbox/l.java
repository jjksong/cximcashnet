package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.j;
import com.megvii.meglive_sdk.volley.m;
import com.megvii.meglive_sdk.volley.o;
import com.megvii.meglive_sdk.volley.o.a;
import com.megvii.meglive_sdk.volley.o.b;
import java.io.UnsupportedEncodingException;

public class l
  extends m<String>
{
  private final o.b<String> a;
  
  public l(String paramString, o.b<String> paramb, o.a parama)
  {
    super(1, paramString, parama);
    this.a = paramb;
  }
  
  protected final o<String> a(j paramj)
  {
    String str2;
    try
    {
      String str1 = new java/lang/String;
      str1.<init>(paramj.b, "UTF-8");
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str2 = new String(paramj.b);
    }
    return o.a(str2, d.a(paramj));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */