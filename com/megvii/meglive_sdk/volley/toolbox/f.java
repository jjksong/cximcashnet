package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.a.g;
import com.megvii.meglive_sdk.volley.a.i;
import com.megvii.meglive_sdk.volley.m;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public final class f
  implements e
{
  private final a a = null;
  private final SSLSocketFactory b = null;
  
  public f()
  {
    this((byte)0);
  }
  
  private f(byte paramByte)
  {
    this('\000');
  }
  
  private f(char paramChar) {}
  
  private static com.megvii.meglive_sdk.volley.a.c a(HttpURLConnection paramHttpURLConnection)
  {
    com.megvii.meglive_sdk.volley.a.c.b localb = new com.megvii.meglive_sdk.volley.a.c.b();
    InputStream localInputStream2;
    try
    {
      InputStream localInputStream1 = paramHttpURLConnection.getInputStream();
    }
    catch (IOException localIOException)
    {
      localInputStream2 = paramHttpURLConnection.getErrorStream();
    }
    localb.d = localInputStream2;
    localb.e = paramHttpURLConnection.getContentLength();
    localb.b(paramHttpURLConnection.getContentEncoding());
    localb.a(paramHttpURLConnection.getContentType());
    return localb;
  }
  
  private static void a(HttpURLConnection paramHttpURLConnection, m<?> paramm)
  {
    byte[] arrayOfByte = paramm.f();
    if (arrayOfByte != null) {
      a(paramHttpURLConnection, paramm, arrayOfByte);
    }
  }
  
  private static void a(HttpURLConnection paramHttpURLConnection, m<?> paramm, byte[] paramArrayOfByte)
  {
    paramHttpURLConnection.setDoOutput(true);
    paramHttpURLConnection.addRequestProperty("Content-Type", paramm.e());
    paramHttpURLConnection = new DataOutputStream(paramHttpURLConnection.getOutputStream());
    paramHttpURLConnection.write(paramArrayOfByte);
    paramHttpURLConnection.close();
  }
  
  public final com.megvii.meglive_sdk.volley.a.e a(m<?> paramm, Map<String, String> paramMap)
  {
    Object localObject1 = paramm.e;
    Object localObject2 = new HashMap();
    ((HashMap)localObject2).putAll(paramm.b());
    ((HashMap)localObject2).putAll(paramMap);
    paramMap = this.a;
    if (paramMap != null)
    {
      paramMap = paramMap.a();
      if (paramMap == null)
      {
        paramm = new StringBuilder("URL blocked by rewriter: ");
        paramm.append((String)localObject1);
        throw new IOException(paramm.toString());
      }
    }
    else
    {
      paramMap = (Map<String, String>)localObject1;
    }
    localObject1 = new URL(paramMap);
    paramMap = (HttpURLConnection)((URL)localObject1).openConnection();
    paramMap.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
    int i = paramm.h();
    paramMap.setConnectTimeout(i);
    paramMap.setReadTimeout(i);
    paramMap.setUseCaches(false);
    i = 1;
    paramMap.setDoInput(true);
    if ("https".equals(((URL)localObject1).getProtocol()))
    {
      localObject1 = this.b;
      if (localObject1 != null) {
        ((HttpsURLConnection)paramMap).setSSLSocketFactory((SSLSocketFactory)localObject1);
      }
    }
    localObject1 = ((HashMap)localObject2).keySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      String str = (String)((Iterator)localObject1).next();
      paramMap.addRequestProperty(str, (String)((HashMap)localObject2).get(str));
    }
    switch (paramm.d)
    {
    default: 
      throw new IllegalStateException("Unknown method type.");
    case 7: 
      paramMap.setRequestMethod("PATCH");
      a(paramMap, paramm);
      break;
    case 6: 
      paramMap.setRequestMethod("TRACE");
      break;
    case 5: 
      paramMap.setRequestMethod("OPTIONS");
      break;
    case 4: 
      paramMap.setRequestMethod("HEAD");
      break;
    case 3: 
      paramMap.setRequestMethod("DELETE");
      break;
    case 2: 
      paramMap.setRequestMethod("PUT");
      a(paramMap, paramm);
      break;
    case 1: 
      paramMap.setRequestMethod("POST");
      a(paramMap, paramm);
      break;
    case 0: 
      paramMap.setRequestMethod("GET");
      break;
    case -1: 
      localObject1 = paramm.d();
      if (localObject1 != null)
      {
        paramMap.setRequestMethod("POST");
        a(paramMap, paramm, (byte[])localObject1);
      }
      break;
    }
    localObject1 = new g("HTTP", 1, 1);
    if (paramMap.getResponseCode() != -1)
    {
      localObject2 = new com.megvii.meglive_sdk.volley.a.d.e((g)localObject1, paramMap.getResponseCode(), paramMap.getResponseMessage());
      localObject1 = new com.megvii.meglive_sdk.volley.a.d.c((i)localObject2);
      int j = paramm.d;
      int k = ((i)localObject2).b();
      if ((j == 4) || ((100 <= k) && (k < 200)) || (k == 204) || (k == 304)) {
        i = 0;
      }
      if (i != 0) {
        ((com.megvii.meglive_sdk.volley.a.d.c)localObject1).c = a(paramMap);
      }
      paramMap = paramMap.getHeaderFields().entrySet().iterator();
      while (paramMap.hasNext())
      {
        paramm = (Map.Entry)paramMap.next();
        if (paramm.getKey() != null) {
          ((com.megvii.meglive_sdk.volley.a.d.c)localObject1).a(new com.megvii.meglive_sdk.volley.a.d.b((String)paramm.getKey(), (String)((List)paramm.getValue()).get(0)));
        }
      }
      return (com.megvii.meglive_sdk.volley.a.e)localObject1;
    }
    throw new IOException("Could not retrieve response code from HttpUrlConnection.");
  }
  
  public static abstract interface a
  {
    public abstract String a();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */