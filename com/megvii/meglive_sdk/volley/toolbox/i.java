package com.megvii.meglive_sdk.volley.toolbox;

import com.megvii.meglive_sdk.volley.a.c;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public final class i
  implements c
{
  private static final char[] e = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  public final byte[] a = "Content-Transfer-Encoding: binary\r\n\r\n".getBytes();
  public final byte[] b = "Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes();
  String c = null;
  ByteArrayOutputStream d = new ByteArrayOutputStream();
  private final String f = "\r\n";
  private final String g = "Content-Type: ";
  private final String h = "Content-Disposition: ";
  private final String i = "text/plain; charset=UTF-8";
  private final String j = "application/octet-stream";
  
  private static String c()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int k = 0; k < 30; k++)
    {
      char[] arrayOfChar = e;
      localStringBuffer.append(arrayOfChar[localRandom.nextInt(arrayOfChar.length)]);
    }
    return localStringBuffer.toString();
  }
  
  public final long a()
  {
    return this.d.toByteArray().length;
  }
  
  public final void a(String paramString1, byte[] paramArrayOfByte1, String paramString2, byte[] paramArrayOfByte2, String paramString3)
  {
    try
    {
      Object localObject2 = this.d;
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("--");
      ((StringBuilder)localObject1).append(this.c);
      ((StringBuilder)localObject1).append("\r\n");
      ((ByteArrayOutputStream)localObject2).write(((StringBuilder)localObject1).toString().getBytes());
      localObject2 = this.d;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Content-Type: ");
      ((StringBuilder)localObject1).append(paramString2);
      ((StringBuilder)localObject1).append("\r\n");
      ((ByteArrayOutputStream)localObject2).write(((StringBuilder)localObject1).toString().getBytes());
      localObject1 = this.d;
      paramString2 = new java/lang/StringBuilder;
      paramString2.<init>();
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Content-Disposition: form-data; name=\"");
      ((StringBuilder)localObject2).append(paramString1);
      ((StringBuilder)localObject2).append("\"");
      paramString2.append(((StringBuilder)localObject2).toString());
      if (!"".equals(paramString3))
      {
        paramString1 = new java/lang/StringBuilder;
        paramString1.<init>("; filename=\"");
        paramString1.append(paramString3);
        paramString1.append("\"");
        paramString2.append(paramString1.toString());
      }
      paramString2.append("\r\n");
      ((ByteArrayOutputStream)localObject1).write(paramString2.toString().getBytes());
      this.d.write(paramArrayOfByte2);
      this.d.write(paramArrayOfByte1);
      this.d.write("\r\n".getBytes());
      return;
    }
    catch (IOException paramString1)
    {
      paramString1.printStackTrace();
    }
  }
  
  public final InputStream b()
  {
    return new ByteArrayInputStream(this.d.toByteArray());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */