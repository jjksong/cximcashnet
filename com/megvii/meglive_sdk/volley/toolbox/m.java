package com.megvii.meglive_sdk.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import com.megvii.meglive_sdk.volley.n;
import java.io.File;

public final class m
{
  public static n a(Context paramContext, e parame)
  {
    File localFile = new File(paramContext.getCacheDir(), "volley");
    try
    {
      String str = paramContext.getPackageName();
      paramContext = paramContext.getPackageManager().getPackageInfo(str, 0);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(str);
      localStringBuilder.append("/");
      localStringBuilder.append(paramContext.versionCode);
      if (Build.VERSION.SDK_INT >= 9) {
        parame = new f();
      }
      paramContext = new a(parame);
      paramContext = new n(new c(localFile, (byte)0), paramContext, (byte)0);
      paramContext.a();
      return paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/toolbox/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */