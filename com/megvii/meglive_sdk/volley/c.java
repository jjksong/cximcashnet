package com.megvii.meglive_sdk.volley;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public final class c
  extends Thread
{
  private static final boolean b = u.b;
  volatile boolean a = false;
  private final BlockingQueue<m<?>> c;
  private final BlockingQueue<m<?>> d;
  private final b e;
  private final p f;
  
  public c(BlockingQueue<m<?>> paramBlockingQueue1, BlockingQueue<m<?>> paramBlockingQueue2, b paramb, p paramp)
  {
    this.c = paramBlockingQueue1;
    this.d = paramBlockingQueue2;
    this.e = paramb;
    this.f = paramp;
  }
  
  public final void run()
  {
    if (b) {
      u.a("start new dispatcher", new Object[0]);
    }
    Process.setThreadPriority(10);
    this.e.a();
    try
    {
      do
      {
        for (;;)
        {
          m localm = (m)this.c.take();
          localm.a("cache-queue-take");
          if (localm.k)
          {
            localm.b("cache-discard-canceled");
          }
          else
          {
            Object localObject2 = this.e.a(localm.e);
            if (localObject2 == null)
            {
              localm.a("cache-miss");
              this.d.put(localm);
            }
            else
            {
              int i;
              if (((b.a)localObject2).e < System.currentTimeMillis()) {
                i = 1;
              } else {
                i = 0;
              }
              if (i != 0)
              {
                localm.a("cache-hit-expired");
                localm.o = ((b.a)localObject2);
                this.d.put(localm);
              }
              else
              {
                localm.a("cache-hit");
                Object localObject1 = new com/megvii/meglive_sdk/volley/j;
                ((j)localObject1).<init>(((b.a)localObject2).a, ((b.a)localObject2).g);
                localObject1 = localm.a((j)localObject1);
                localm.a("cache-hit-parsed");
                if (((b.a)localObject2).f < System.currentTimeMillis()) {
                  i = 1;
                } else {
                  i = 0;
                }
                if (i == 0)
                {
                  this.f.a(localm, (o)localObject1);
                }
                else
                {
                  localm.a("cache-hit-refresh-needed");
                  localm.o = ((b.a)localObject2);
                  ((o)localObject1).d = true;
                  p localp = this.f;
                  localObject2 = new com/megvii/meglive_sdk/volley/c$1;
                  ((1)localObject2).<init>(this, localm);
                  localp.a(localm, (o)localObject1, (Runnable)localObject2);
                }
              }
            }
          }
        }
      } while (!this.a);
    }
    catch (InterruptedException localInterruptedException) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */