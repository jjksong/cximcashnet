package com.megvii.meglive_sdk.volley;

import android.net.TrafficStats;
import android.os.Build.VERSION;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public final class h
  extends Thread
{
  volatile boolean a = false;
  private final BlockingQueue<m<?>> b;
  private final g c;
  private final b d;
  private final p e;
  
  public h(BlockingQueue<m<?>> paramBlockingQueue, g paramg, b paramb, p paramp)
  {
    this.b = paramBlockingQueue;
    this.c = paramg;
    this.d = paramb;
    this.e = paramp;
  }
  
  public final void run()
  {
    Process.setThreadPriority(10);
    do
    {
      for (;;)
      {
        long l = SystemClock.elapsedRealtime();
        try
        {
          m localm = (m)this.b.take();
          try
          {
            localm.a("network-queue-take");
            if (localm.k)
            {
              localm.b("network-discard-cancelled");
            }
            else
            {
              if (Build.VERSION.SDK_INT >= 14) {
                TrafficStats.setThreadStatsTag(localm.f);
              }
              Object localObject = this.c.a(localm);
              localm.a("network-http-complete");
              if ((((j)localObject).d) && (localm.l))
              {
                localm.b("not-modified");
              }
              else
              {
                localObject = localm.a((j)localObject);
                localm.a("network-parse-complete");
                if ((localm.j) && (((o)localObject).b != null))
                {
                  this.d.a(localm.e, ((o)localObject).b);
                  localm.a("network-cache-written");
                }
                localm.l = true;
                this.e.a(localm, (o)localObject);
              }
            }
          }
          catch (Exception localException)
          {
            u.d("Unhandled exception %s", new Object[] { localException.toString() });
            t localt1 = new t(localException);
            localt1.b = (SystemClock.elapsedRealtime() - l);
            this.e.a(localm, localt1);
          }
          catch (t localt2)
          {
            localt2.b = (SystemClock.elapsedRealtime() - l);
            t localt3 = m.a(localt2);
            this.e.a(localm, localt3);
          }
        }
        catch (InterruptedException localInterruptedException) {}
      }
    } while (!this.a);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */