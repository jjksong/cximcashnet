package com.megvii.meglive_sdk.volley;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class u
{
  public static String a = "Volley";
  public static boolean b = Log.isLoggable("Volley", 2);
  
  public static void a(String paramString, Object... paramVarArgs)
  {
    if (b) {
      e(paramString, paramVarArgs);
    }
  }
  
  public static void b(String paramString, Object... paramVarArgs)
  {
    e(paramString, paramVarArgs);
  }
  
  public static void c(String paramString, Object... paramVarArgs)
  {
    e(paramString, paramVarArgs);
  }
  
  public static void d(String paramString, Object... paramVarArgs)
  {
    e(paramString, paramVarArgs);
  }
  
  private static String e(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs != null) {
      paramString = String.format(Locale.US, paramString, paramVarArgs);
    }
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().fillInStackTrace().getStackTrace();
    String str = "<unknown>";
    for (int i = 2;; i++)
    {
      paramVarArgs = str;
      if (i >= arrayOfStackTraceElement.length) {
        break;
      }
      if (!arrayOfStackTraceElement[i].getClass().equals(u.class))
      {
        paramVarArgs = arrayOfStackTraceElement[i].getClassName();
        paramVarArgs = paramVarArgs.substring(paramVarArgs.lastIndexOf('.') + 1);
        str = paramVarArgs.substring(paramVarArgs.lastIndexOf('$') + 1);
        paramVarArgs = new StringBuilder();
        paramVarArgs.append(str);
        paramVarArgs.append(".");
        paramVarArgs.append(arrayOfStackTraceElement[i].getMethodName());
        paramVarArgs = paramVarArgs.toString();
        break;
      }
    }
    return String.format(Locale.US, "[%d] %s: %s", new Object[] { Long.valueOf(Thread.currentThread().getId()), paramVarArgs, paramString });
  }
  
  static final class a
  {
    public static final boolean a = u.b;
    private final List<a> b = new ArrayList();
    private boolean c = false;
    
    public final void a(String paramString)
    {
      try
      {
        this.c = true;
        if (this.b.size() == 0)
        {
          l1 = 0L;
        }
        else
        {
          l2 = ((a)this.b.get(0)).c;
          l1 = ((a)this.b.get(this.b.size() - 1)).c;
          l1 -= l2;
        }
        if (l1 <= 0L) {
          return;
        }
        long l2 = ((a)this.b.get(0)).c;
        u.b("(%-4d ms) %s", new Object[] { Long.valueOf(l1), paramString });
        Iterator localIterator = this.b.iterator();
        for (long l1 = l2; localIterator.hasNext(); l1 = l2)
        {
          paramString = (a)localIterator.next();
          l2 = paramString.c;
          u.b("(+%-4d) [%2d] %s", new Object[] { Long.valueOf(l2 - l1), Long.valueOf(paramString.b), paramString.a });
        }
        return;
      }
      finally {}
    }
    
    public final void a(String paramString, long paramLong)
    {
      try
      {
        if (!this.c)
        {
          List localList = this.b;
          a locala = new com/megvii/meglive_sdk/volley/u$a$a;
          locala.<init>(paramString, paramLong, SystemClock.elapsedRealtime());
          localList.add(locala);
          return;
        }
        paramString = new java/lang/IllegalStateException;
        paramString.<init>("Marker added to finished log");
        throw paramString;
      }
      finally {}
    }
    
    protected final void finalize()
    {
      if (!this.c)
      {
        a("Request on the loose");
        u.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
      }
    }
    
    private static final class a
    {
      public final String a;
      public final long b;
      public final long c;
      
      public a(String paramString, long paramLong1, long paramLong2)
      {
        this.a = paramString;
        this.b = paramLong1;
        this.c = paramLong2;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */