package com.megvii.meglive_sdk.volley;

public final class e
  implements q
{
  private int a;
  private int b;
  private final int c;
  private final float d;
  
  public e()
  {
    this(2500, 1, 1.0F);
  }
  
  public e(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a = paramInt1;
    this.c = paramInt2;
    this.d = paramFloat;
  }
  
  public final int a()
  {
    return this.a;
  }
  
  public final void a(t paramt)
  {
    int j = this.b;
    int i = 1;
    this.b = (j + 1);
    j = this.a;
    this.a = ((int)(j + j * this.d));
    if (this.b > this.c) {
      i = 0;
    }
    if (i != 0) {
      return;
    }
    throw paramt;
  }
  
  public final int b()
  {
    return this.b;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */