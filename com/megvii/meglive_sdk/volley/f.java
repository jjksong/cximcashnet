package com.megvii.meglive_sdk.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

public final class f
  implements p
{
  private final Executor a;
  
  public f(final Handler paramHandler)
  {
    this.a = new Executor()
    {
      public final void execute(Runnable paramAnonymousRunnable)
      {
        paramHandler.post(paramAnonymousRunnable);
      }
    };
  }
  
  public final void a(m<?> paramm, o<?> paramo)
  {
    a(paramm, paramo, null);
  }
  
  public final void a(m<?> paramm, o<?> paramo, Runnable paramRunnable)
  {
    paramm.l = true;
    paramm.a("post-response");
    this.a.execute(new a(paramm, paramo, paramRunnable));
  }
  
  public final void a(m<?> paramm, t paramt)
  {
    paramm.a("post-error");
    paramt = o.a(paramt);
    this.a.execute(new a(paramm, paramt, null));
  }
  
  private final class a
    implements Runnable
  {
    private final m b;
    private final o c;
    private final Runnable d;
    
    public a(m paramm, o paramo, Runnable paramRunnable)
    {
      this.b = paramm;
      this.c = paramo;
      this.d = paramRunnable;
    }
    
    public final void run()
    {
      if (this.b.k)
      {
        this.b.b("canceled-at-delivery");
        return;
      }
      int i;
      if (this.c.c == null) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0)
      {
        this.b.a(this.c.a);
      }
      else
      {
        m localm = this.b;
        localObject = this.c.c;
        if (localm.g != null) {
          localm.g.a((t)localObject);
        }
      }
      if (this.c.d) {
        this.b.a("intermediate-response");
      } else {
        this.b.b("done");
      }
      Object localObject = this.d;
      if (localObject != null) {
        ((Runnable)localObject).run();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */