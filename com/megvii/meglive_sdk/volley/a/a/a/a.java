package com.megvii.meglive_sdk.volley.a.a.a;

import java.lang.ref.SoftReference;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public final class a
{
  public static final TimeZone a;
  private static final String[] b = { "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy" };
  private static final Date c;
  
  static
  {
    a = TimeZone.getTimeZone("GMT");
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTimeZone(a);
    localCalendar.set(2000, 0, 1, 0, 0, 0);
    localCalendar.set(14, 0);
    c = localCalendar.getTime();
  }
  
  public static String a(Date paramDate)
  {
    com.megvii.meglive_sdk.volley.a.f.a.a(paramDate, "Date");
    com.megvii.meglive_sdk.volley.a.f.a.a("EEE, dd MMM yyyy HH:mm:ss zzz", "Pattern");
    return a.a("EEE, dd MMM yyyy HH:mm:ss zzz").format(paramDate);
  }
  
  public static Date a(String paramString)
  {
    com.megvii.meglive_sdk.volley.a.f.a.a(paramString, "Date value");
    String[] arrayOfString = b;
    Date localDate = c;
    String str = paramString;
    if (paramString.length() > 1)
    {
      str = paramString;
      if (paramString.startsWith("'"))
      {
        str = paramString;
        if (paramString.endsWith("'")) {
          str = paramString.substring(1, paramString.length() - 1);
        }
      }
    }
    int j = arrayOfString.length;
    for (int i = 0; i < j; i++)
    {
      Object localObject = a.a(arrayOfString[i]);
      ((SimpleDateFormat)localObject).set2DigitYearStart(localDate);
      paramString = new ParsePosition(0);
      localObject = ((SimpleDateFormat)localObject).parse(str, paramString);
      if (paramString.getIndex() != 0) {
        return (Date)localObject;
      }
    }
    return null;
  }
  
  static final class a
  {
    private static final ThreadLocal<SoftReference<Map<String, SimpleDateFormat>>> a = new ThreadLocal() {};
    
    public static SimpleDateFormat a(String paramString)
    {
      Object localObject2 = (Map)((SoftReference)a.get()).get();
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new HashMap();
        a.set(new SoftReference(localObject1));
      }
      SimpleDateFormat localSimpleDateFormat = (SimpleDateFormat)((Map)localObject1).get(paramString);
      localObject2 = localSimpleDateFormat;
      if (localSimpleDateFormat == null)
      {
        localObject2 = new SimpleDateFormat(paramString, Locale.US);
        ((SimpleDateFormat)localObject2).setTimeZone(TimeZone.getTimeZone("GMT"));
        ((Map)localObject1).put(paramString, localObject2);
      }
      return (SimpleDateFormat)localObject2;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */