package com.megvii.meglive_sdk.volley.a.c;

import java.io.InputStream;

public final class b
  extends a
{
  public InputStream d;
  public long e = -1L;
  
  public final long a()
  {
    return this.e;
  }
  
  public final InputStream b()
  {
    int i;
    if (this.d != null) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return this.d;
    }
    throw new IllegalStateException("Content has not been provided");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */