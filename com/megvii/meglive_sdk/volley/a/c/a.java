package com.megvii.meglive_sdk.volley.a.c;

import com.megvii.meglive_sdk.volley.a.c;

public abstract class a
  implements c
{
  protected com.megvii.meglive_sdk.volley.a.b a;
  protected com.megvii.meglive_sdk.volley.a.b b;
  protected boolean c;
  
  public final void a(String paramString)
  {
    if (paramString != null) {
      paramString = new com.megvii.meglive_sdk.volley.a.d.b("Content-Type", paramString);
    } else {
      paramString = null;
    }
    this.a = paramString;
  }
  
  public final void b(String paramString)
  {
    if (paramString != null) {
      paramString = new com.megvii.meglive_sdk.volley.a.d.b("Content-Encoding", paramString);
    } else {
      paramString = null;
    }
    this.b = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append('[');
    if (this.a != null)
    {
      localStringBuilder.append("Content-Type: ");
      localStringBuilder.append(this.a.c());
      localStringBuilder.append(',');
    }
    if (this.b != null)
    {
      localStringBuilder.append("Content-Encoding: ");
      localStringBuilder.append(this.b.c());
      localStringBuilder.append(',');
    }
    long l = a();
    if (l >= 0L)
    {
      localStringBuilder.append("Content-Length: ");
      localStringBuilder.append(l);
      localStringBuilder.append(',');
    }
    localStringBuilder.append("Chunked: ");
    localStringBuilder.append(this.c);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */