package com.megvii.meglive_sdk.volley.a.f;

import java.io.Serializable;

public final class b
  implements Serializable
{
  public int a;
  private char[] b;
  
  public b()
  {
    a.a(64, "Buffer capacity");
    this.b = new char[64];
  }
  
  private void b(int paramInt)
  {
    char[] arrayOfChar = new char[Math.max(this.b.length << 1, paramInt)];
    System.arraycopy(this.b, 0, arrayOfChar, 0, this.a);
    this.b = arrayOfChar;
  }
  
  public final void a(char paramChar)
  {
    int i = this.a + 1;
    if (i > this.b.length) {
      b(i);
    }
    this.b[this.a] = paramChar;
    this.a = i;
  }
  
  public final void a(int paramInt)
  {
    if (paramInt <= 0) {
      return;
    }
    int j = this.b.length;
    int i = this.a;
    if (paramInt > j - i) {
      b(i + paramInt);
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString == null) {
      paramString = "null";
    }
    int i = paramString.length();
    int j = this.a + i;
    if (j > this.b.length) {
      b(j);
    }
    paramString.getChars(0, i, this.b, this.a);
    this.a = j;
  }
  
  public final String toString()
  {
    return new String(this.b, 0, this.a);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/f/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */