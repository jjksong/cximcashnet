package com.megvii.meglive_sdk.volley.a.f;

public final class a
{
  public static int a(int paramInt, String paramString)
  {
    if (paramInt >= 0) {
      return paramInt;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" may not be negative");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public static <T> T a(T paramT, String paramString)
  {
    if (paramT != null) {
      return paramT;
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" may not be null");
    throw new IllegalArgumentException(paramT.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/f/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */