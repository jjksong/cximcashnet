package com.megvii.meglive_sdk.volley.a;

import com.megvii.meglive_sdk.volley.a.f.a;
import java.io.Serializable;

public class g
  implements Serializable, Cloneable
{
  protected final String d;
  protected final int e;
  protected final int f;
  
  public g(String paramString, int paramInt1, int paramInt2)
  {
    this.d = ((String)a.a(paramString, "Protocol name"));
    this.e = a.a(paramInt1, "Protocol minor version");
    this.f = a.a(paramInt2, "Protocol minor version");
  }
  
  public final String a()
  {
    return this.d;
  }
  
  public final int b()
  {
    return this.e;
  }
  
  public final int c()
  {
    return this.f;
  }
  
  public Object clone()
  {
    return super.clone();
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof g)) {
      return false;
    }
    paramObject = (g)paramObject;
    return (this.d.equals(((g)paramObject).d)) && (this.e == ((g)paramObject).e) && (this.f == ((g)paramObject).f);
  }
  
  public final int hashCode()
  {
    return this.d.hashCode() ^ this.e * 100000 ^ this.f;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.d);
    localStringBuilder.append('/');
    localStringBuilder.append(Integer.toString(this.e));
    localStringBuilder.append('.');
    localStringBuilder.append(Integer.toString(this.f));
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */