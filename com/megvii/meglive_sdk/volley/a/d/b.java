package com.megvii.meglive_sdk.volley.a.d;

import com.megvii.meglive_sdk.volley.a.f.a;
import java.io.Serializable;

public final class b
  implements com.megvii.meglive_sdk.volley.a.b, Serializable, Cloneable
{
  private final String a;
  private final String b;
  
  public b(String paramString1, String paramString2)
  {
    this.a = ((String)a.a(paramString1, "Name"));
    this.b = paramString2;
  }
  
  public final String b()
  {
    return this.a;
  }
  
  public final String c()
  {
    return this.b;
  }
  
  public final Object clone()
  {
    return super.clone();
  }
  
  public final String toString()
  {
    d locald = d.b;
    return d.a(this).toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/d/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */