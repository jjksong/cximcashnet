package com.megvii.meglive_sdk.volley.a.d;

import com.megvii.meglive_sdk.volley.a.f;
import com.megvii.meglive_sdk.volley.a.g;
import com.megvii.meglive_sdk.volley.a.h;
import com.megvii.meglive_sdk.volley.a.i;
import java.util.Locale;

public final class c
  extends a
  implements com.megvii.meglive_sdk.volley.a.e
{
  public com.megvii.meglive_sdk.volley.a.c c;
  private i d;
  private g e;
  private int f;
  private String g;
  private final h h;
  private Locale i;
  
  public c(i parami)
  {
    super((byte)0);
    this.d = ((i)com.megvii.meglive_sdk.volley.a.f.a.a(parami, "Status line"));
    this.e = parami.a();
    this.f = parami.b();
    this.g = parami.c();
    this.h = null;
    this.i = null;
  }
  
  public final i b()
  {
    if (this.d == null)
    {
      Object localObject2 = this.e;
      if (localObject2 == null) {
        localObject2 = f.c;
      }
      int j = this.f;
      Object localObject1 = this.g;
      if (localObject1 == null)
      {
        localObject1 = this.h;
        if (localObject1 != null)
        {
          if (this.i == null) {
            Locale.getDefault();
          }
          localObject1 = ((h)localObject1).a();
        }
        else
        {
          localObject1 = null;
        }
      }
      this.d = new e((g)localObject2, j, (String)localObject1);
    }
    return this.d;
  }
  
  public final com.megvii.meglive_sdk.volley.a.c c()
  {
    return this.c;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b());
    localStringBuilder.append(' ');
    localStringBuilder.append(this.a);
    if (this.c != null)
    {
      localStringBuilder.append(' ');
      localStringBuilder.append(this.c);
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/d/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */