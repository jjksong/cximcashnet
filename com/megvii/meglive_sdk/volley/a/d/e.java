package com.megvii.meglive_sdk.volley.a.d;

import com.megvii.meglive_sdk.volley.a.f.a;
import com.megvii.meglive_sdk.volley.a.f.b;
import com.megvii.meglive_sdk.volley.a.g;
import com.megvii.meglive_sdk.volley.a.i;
import java.io.Serializable;

public final class e
  implements i, Serializable, Cloneable
{
  private final g a;
  private final int b;
  private final String c;
  
  public e(g paramg, int paramInt, String paramString)
  {
    this.a = ((g)a.a(paramg, "Version"));
    this.b = a.a(paramInt, "Status code");
    this.c = paramString;
  }
  
  public final g a()
  {
    return this.a;
  }
  
  public final int b()
  {
    return this.b;
  }
  
  public final String c()
  {
    return this.c;
  }
  
  public final Object clone()
  {
    return super.clone();
  }
  
  public final String toString()
  {
    Object localObject = d.b;
    a.a(this, "Status line");
    b localb = d.a();
    int j = d.a(a()) + 1 + 3 + 1;
    String str = c();
    int i = j;
    if (str != null) {
      i = j + str.length();
    }
    localb.a(i);
    localObject = a();
    a.a(localObject, "Protocol version");
    localb.a(d.a((g)localObject));
    localb.a(((g)localObject).a());
    localb.a('/');
    localb.a(Integer.toString(((g)localObject).b()));
    localb.a('.');
    localb.a(Integer.toString(((g)localObject).c()));
    localb.a(' ');
    localb.a(Integer.toString(b()));
    localb.a(' ');
    if (str != null) {
      localb.a(str);
    }
    return localb.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/d/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */