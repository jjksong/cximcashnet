package com.megvii.meglive_sdk.volley.a.d;

import com.megvii.meglive_sdk.volley.a.g;

public final class d
{
  @Deprecated
  public static final d a = new d();
  public static final d b = new d();
  
  static int a(g paramg)
  {
    return paramg.a().length() + 4;
  }
  
  static com.megvii.meglive_sdk.volley.a.f.b a()
  {
    return new com.megvii.meglive_sdk.volley.a.f.b();
  }
  
  public static com.megvii.meglive_sdk.volley.a.f.b a(com.megvii.meglive_sdk.volley.a.b paramb)
  {
    com.megvii.meglive_sdk.volley.a.f.a.a(paramb, "Header");
    if ((paramb instanceof com.megvii.meglive_sdk.volley.a.a))
    {
      paramb = ((com.megvii.meglive_sdk.volley.a.a)paramb).a();
    }
    else
    {
      com.megvii.meglive_sdk.volley.a.f.b localb = new com.megvii.meglive_sdk.volley.a.f.b();
      String str = paramb.b();
      paramb = paramb.c();
      int j = str.length() + 2;
      int i = j;
      if (paramb != null) {
        i = j + paramb.length();
      }
      localb.a(i);
      localb.a(str);
      localb.a(": ");
      if (paramb != null) {
        localb.a(paramb);
      }
      paramb = localb;
    }
    return paramb;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/a/d/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */