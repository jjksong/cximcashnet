package com.megvii.meglive_sdk.volley;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

public abstract class m<T>
  implements Comparable<m<T>>
{
  private final u.a a;
  public final int d;
  public final String e;
  final int f;
  o.a g;
  Integer h;
  n i;
  boolean j;
  public boolean k;
  boolean l;
  public boolean m;
  public q n;
  public b.a o;
  
  public m(int paramInt, String paramString, o.a parama)
  {
    u.a locala;
    if (u.a.a) {
      locala = new u.a();
    } else {
      locala = null;
    }
    this.a = locala;
    this.j = true;
    int i1 = 0;
    this.k = false;
    this.l = false;
    this.m = false;
    this.o = null;
    this.d = paramInt;
    this.e = paramString;
    this.g = parama;
    this.n = new e();
    paramInt = i1;
    if (!TextUtils.isEmpty(paramString))
    {
      paramString = Uri.parse(paramString);
      paramInt = i1;
      if (paramString != null)
      {
        paramString = paramString.getHost();
        paramInt = i1;
        if (paramString != null) {
          paramInt = paramString.hashCode();
        }
      }
    }
    this.f = paramInt;
  }
  
  protected static t a(t paramt)
  {
    return paramt;
  }
  
  private static byte[] a(Map<String, String> paramMap, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        localStringBuilder.append(URLEncoder.encode((String)localEntry.getKey(), paramString));
        localStringBuilder.append('=');
        localStringBuilder.append(URLEncoder.encode((String)localEntry.getValue(), paramString));
        localStringBuilder.append('&');
      }
      paramMap = localStringBuilder.toString().getBytes(paramString);
      return paramMap;
    }
    catch (UnsupportedEncodingException paramMap)
    {
      localStringBuilder = new StringBuilder("Encoding not supported: ");
      localStringBuilder.append(paramString);
      throw new RuntimeException(localStringBuilder.toString(), paramMap);
    }
  }
  
  public abstract o<T> a(j paramj);
  
  protected Map<String, String> a()
  {
    return null;
  }
  
  public abstract void a(T paramT);
  
  public final void a(String paramString)
  {
    if (u.a.a) {
      this.a.a(paramString, Thread.currentThread().getId());
    }
  }
  
  public Map<String, String> b()
  {
    return Collections.emptyMap();
  }
  
  final void b(final String paramString)
  {
    n localn = this.i;
    if (localn != null) {
      synchronized (localn.b)
      {
        c();
        localn.b.remove(this);
        synchronized (localn.d)
        {
          Object localObject2 = localn.d.iterator();
          while (((Iterator)localObject2).hasNext()) {
            ((Iterator)localObject2).next();
          }
          if (this.j) {
            synchronized (localn.a)
            {
              localObject2 = this.e;
              Queue localQueue = (Queue)localn.a.remove(localObject2);
              if (localQueue != null)
              {
                if (u.b) {
                  u.a("Releasing %d waiting requests for cacheKey=%s.", new Object[] { Integer.valueOf(localQueue.size()), localObject2 });
                }
                localn.c.addAll(localQueue);
              }
            }
          }
        }
      }
    }
    if (u.a.a)
    {
      final long l1 = Thread.currentThread().getId();
      if (Looper.myLooper() != Looper.getMainLooper())
      {
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {
          public final void run()
          {
            m.a(m.this).a(paramString, l1);
            m.a(m.this).a(toString());
          }
        });
        return;
      }
      this.a.a(paramString, l1);
      this.a.a(toString());
    }
  }
  
  public void c()
  {
    this.g = null;
  }
  
  @Deprecated
  public final byte[] d()
  {
    Map localMap = a();
    if ((localMap != null) && (localMap.size() > 0)) {
      return a(localMap, "UTF-8");
    }
    return null;
  }
  
  public String e()
  {
    return "application/x-www-form-urlencoded; charset=UTF-8";
  }
  
  public byte[] f()
  {
    Map localMap = a();
    if ((localMap != null) && (localMap.size() > 0)) {
      return a(localMap, "UTF-8");
    }
    return null;
  }
  
  public a g()
  {
    return a.b;
  }
  
  public final int h()
  {
    return this.n.a();
  }
  
  public String toString()
  {
    Object localObject = new StringBuilder("0x");
    ((StringBuilder)localObject).append(Integer.toHexString(this.f));
    String str = ((StringBuilder)localObject).toString();
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.k) {
      localObject = "[X] ";
    } else {
      localObject = "[ ] ";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(this.e);
    localStringBuilder.append(" ");
    localStringBuilder.append(str);
    localStringBuilder.append(" ");
    localStringBuilder.append(g());
    localStringBuilder.append(" ");
    localStringBuilder.append(this.h);
    return localStringBuilder.toString();
  }
  
  public static enum a
  {
    private a() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */