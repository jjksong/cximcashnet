package com.megvii.meglive_sdk.volley;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class n
{
  final Map<String, Queue<m<?>>> a = new HashMap();
  final Set<m<?>> b = new HashSet();
  final PriorityBlockingQueue<m<?>> c = new PriorityBlockingQueue();
  final List<Object> d = new ArrayList();
  private final AtomicInteger e = new AtomicInteger();
  private final PriorityBlockingQueue<m<?>> f = new PriorityBlockingQueue();
  private final b g;
  private final g h;
  private final p i;
  private final h[] j;
  private c k;
  
  private n(b paramb, g paramg)
  {
    this(paramb, paramg, new f(new Handler(Looper.getMainLooper())));
  }
  
  public n(b paramb, g paramg, byte paramByte)
  {
    this(paramb, paramg);
  }
  
  private n(b paramb, g paramg, p paramp)
  {
    this.g = paramb;
    this.h = paramg;
    this.j = new h[4];
    this.i = paramp;
  }
  
  public final <T> m<T> a(m<T> paramm)
  {
    paramm.i = this;
    synchronized (this.b)
    {
      this.b.add(paramm);
      paramm.h = Integer.valueOf(this.e.incrementAndGet());
      paramm.a("add-to-queue");
      if (!paramm.j)
      {
        this.f.add(paramm);
        return paramm;
      }
      synchronized (this.a)
      {
        String str = paramm.e;
        if (this.a.containsKey(str))
        {
          Queue localQueue = (Queue)this.a.get(str);
          ??? = localQueue;
          if (localQueue == null)
          {
            ??? = new java/util/LinkedList;
            ((LinkedList)???).<init>();
          }
          ((Queue)???).add(paramm);
          this.a.put(str, ???);
          if (u.b) {
            u.a("Request for cacheKey=%s is in flight, putting on hold.", new Object[] { str });
          }
        }
        else
        {
          this.a.put(str, null);
          this.c.add(paramm);
        }
        return paramm;
      }
    }
  }
  
  public final void a()
  {
    Object localObject1 = this.k;
    if (localObject1 != null)
    {
      ((c)localObject1).a = true;
      ((c)localObject1).interrupt();
    }
    localObject1 = this.j;
    int i1 = localObject1.length;
    int n = 0;
    for (int m = 0; m < i1; m++)
    {
      Object localObject2 = localObject1[m];
      if (localObject2 != null)
      {
        ((h)localObject2).a = true;
        ((h)localObject2).interrupt();
      }
    }
    this.k = new c(this.c, this.f, this.g, this.i);
    this.k.start();
    for (m = n; m < this.j.length; m++)
    {
      localObject1 = new h(this.f, this.h, this.g, this.i);
      this.j[m] = localObject1;
      ((h)localObject1).start();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/volley/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */