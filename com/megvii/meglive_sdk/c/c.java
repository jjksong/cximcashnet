package com.megvii.meglive_sdk.c;

import java.util.ArrayList;

public final class c
{
  int a;
  private final ArrayList<a> b;
  private final ArrayList<Runnable> c;
  private int d = 0;
  private int e;
  private int f;
  private int g = 5;
  private int h;
  
  public c()
  {
    this(2, 2);
  }
  
  public c(int paramInt1, int paramInt2)
  {
    this.e = paramInt1;
    this.f = paramInt2;
    this.g = 5;
    this.b = new ArrayList();
    this.c = new ArrayList();
  }
  
  private void a()
  {
    try
    {
      if ((this.b.size() > 0) && (this.c.size() > 0))
      {
        a locala = (a)this.b.get(0);
        this.b.remove(locala);
        Runnable localRunnable = (Runnable)this.c.get(0);
        this.c.remove(localRunnable);
        a.a(locala, localRunnable);
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void a(Runnable paramRunnable)
  {
    Object localObject2 = null;
    try
    {
      Object localObject1;
      if (!this.b.isEmpty())
      {
        localObject1 = (a)this.b.get(0);
        this.b.remove(localObject1);
      }
      else if (this.a >= this.f)
      {
        localObject1 = localObject2;
        if (!this.c.contains(paramRunnable))
        {
          this.c.add(paramRunnable);
          localObject1 = localObject2;
        }
      }
      else
      {
        localObject1 = new com/megvii/meglive_sdk/c/c$a;
        this.h += 1;
        ((a)localObject1).<init>(this);
        ((a)localObject1).setPriority(this.g);
        ((a)localObject1).start();
        this.a += 1;
      }
      if (localObject1 != null) {
        a.a((a)localObject1, paramRunnable);
      }
      return;
    }
    finally {}
  }
  
  final boolean a(a parama)
  {
    try
    {
      Runnable localRunnable = a.a(parama);
      if (localRunnable != null) {
        return false;
      }
      if (this.b.size() > this.d)
      {
        if (this.c.size() == 0)
        {
          this.b.remove(parama);
          return true;
        }
        localRunnable = (Runnable)this.c.get(0);
        this.c.remove(localRunnable);
        a.b(parama, localRunnable);
      }
      return false;
    }
    finally {}
  }
  
  final boolean b(a parama)
  {
    try
    {
      this.b.remove(parama);
      if (this.b.size() < this.e)
      {
        this.b.add(parama);
        a();
        return false;
      }
      return true;
    }
    finally
    {
      parama = finally;
      throw parama;
    }
  }
  
  private final class a
    extends Thread
  {
    private Runnable b;
    
    public a() {}
    
    private void a(Runnable paramRunnable)
    {
      try
      {
        this.b = paramRunnable;
        notify();
        return;
      }
      finally
      {
        paramRunnable = finally;
        throw paramRunnable;
      }
    }
    
    /* Error */
    public final void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   6: astore_2
      //   7: aload_2
      //   8: ifnonnull +18 -> 26
      //   11: aload_0
      //   12: ldc2_w 37
      //   15: invokevirtual 42	java/lang/Object:wait	(J)V
      //   18: goto +8 -> 26
      //   21: astore_2
      //   22: aload_2
      //   23: invokevirtual 45	java/lang/InterruptedException:printStackTrace	()V
      //   26: aload_0
      //   27: monitorexit
      //   28: aload_0
      //   29: getfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   32: ifnonnull +64 -> 96
      //   35: aload_0
      //   36: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   39: aload_0
      //   40: invokevirtual 48	com/megvii/meglive_sdk/c/c:a	(Lcom/megvii/meglive_sdk/c/c$a;)Z
      //   43: ifeq +46 -> 89
      //   46: aload_0
      //   47: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   50: invokestatic 51	com/megvii/meglive_sdk/c/c:a	(Lcom/megvii/meglive_sdk/c/c;)I
      //   53: pop
      //   54: getstatic 57	java/lang/System:out	Ljava/io/PrintStream;
      //   57: astore_2
      //   58: new 59	java/lang/StringBuilder
      //   61: dup
      //   62: ldc 61
      //   64: invokespecial 64	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   67: astore_3
      //   68: aload_3
      //   69: aload_0
      //   70: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   73: getfield 67	com/megvii/meglive_sdk/c/c:a	I
      //   76: invokevirtual 71	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   79: pop
      //   80: aload_2
      //   81: aload_3
      //   82: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   85: invokevirtual 80	java/io/PrintStream:println	(Ljava/lang/String;)V
      //   88: return
      //   89: aload_0
      //   90: getfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   93: ifnull -93 -> 0
      //   96: aload_0
      //   97: getfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   100: invokeinterface 84 1 0
      //   105: aload_0
      //   106: aconst_null
      //   107: putfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   110: aload_0
      //   111: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   114: aload_0
      //   115: invokevirtual 86	com/megvii/meglive_sdk/c/c:b	(Lcom/megvii/meglive_sdk/c/c$a;)Z
      //   118: ifeq -118 -> 0
      //   121: aload_0
      //   122: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   125: invokestatic 51	com/megvii/meglive_sdk/c/c:a	(Lcom/megvii/meglive_sdk/c/c;)I
      //   128: pop
      //   129: getstatic 57	java/lang/System:out	Ljava/io/PrintStream;
      //   132: astore_2
      //   133: new 59	java/lang/StringBuilder
      //   136: dup
      //   137: ldc 88
      //   139: invokespecial 64	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   142: astore_3
      //   143: aload_0
      //   144: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   147: getfield 67	com/megvii/meglive_sdk/c/c:a	I
      //   150: istore_1
      //   151: aload_3
      //   152: iload_1
      //   153: invokevirtual 71	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   156: pop
      //   157: aload_2
      //   158: aload_3
      //   159: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   162: invokevirtual 80	java/io/PrintStream:println	(Ljava/lang/String;)V
      //   165: return
      //   166: astore_2
      //   167: goto +57 -> 224
      //   170: astore_2
      //   171: aload_2
      //   172: invokevirtual 89	java/lang/Throwable:printStackTrace	()V
      //   175: aload_0
      //   176: aconst_null
      //   177: putfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   180: aload_0
      //   181: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   184: aload_0
      //   185: invokevirtual 86	com/megvii/meglive_sdk/c/c:b	(Lcom/megvii/meglive_sdk/c/c$a;)Z
      //   188: ifeq -188 -> 0
      //   191: aload_0
      //   192: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   195: invokestatic 51	com/megvii/meglive_sdk/c/c:a	(Lcom/megvii/meglive_sdk/c/c;)I
      //   198: pop
      //   199: getstatic 57	java/lang/System:out	Ljava/io/PrintStream;
      //   202: astore_2
      //   203: new 59	java/lang/StringBuilder
      //   206: dup
      //   207: ldc 88
      //   209: invokespecial 64	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   212: astore_3
      //   213: aload_0
      //   214: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   217: getfield 67	com/megvii/meglive_sdk/c/c:a	I
      //   220: istore_1
      //   221: goto -70 -> 151
      //   224: aload_0
      //   225: aconst_null
      //   226: putfield 21	com/megvii/meglive_sdk/c/c$a:b	Ljava/lang/Runnable;
      //   229: aload_0
      //   230: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   233: aload_0
      //   234: invokevirtual 86	com/megvii/meglive_sdk/c/c:b	(Lcom/megvii/meglive_sdk/c/c$a;)Z
      //   237: ifeq +36 -> 273
      //   240: aload_0
      //   241: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   244: invokestatic 51	com/megvii/meglive_sdk/c/c:a	(Lcom/megvii/meglive_sdk/c/c;)I
      //   247: pop
      //   248: getstatic 57	java/lang/System:out	Ljava/io/PrintStream;
      //   251: astore_2
      //   252: new 59	java/lang/StringBuilder
      //   255: dup
      //   256: ldc 88
      //   258: invokespecial 64	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   261: astore_3
      //   262: aload_0
      //   263: getfield 14	com/megvii/meglive_sdk/c/c$a:a	Lcom/megvii/meglive_sdk/c/c;
      //   266: getfield 67	com/megvii/meglive_sdk/c/c:a	I
      //   269: istore_1
      //   270: goto -119 -> 151
      //   273: aload_2
      //   274: athrow
      //   275: astore_2
      //   276: aload_0
      //   277: monitorexit
      //   278: aload_2
      //   279: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	280	0	this	a
      //   150	120	1	i	int
      //   6	2	2	localRunnable	Runnable
      //   21	2	2	localInterruptedException	InterruptedException
      //   57	101	2	localPrintStream1	java.io.PrintStream
      //   166	1	2	localObject1	Object
      //   170	2	2	localThrowable	Throwable
      //   202	72	2	localPrintStream2	java.io.PrintStream
      //   275	4	2	localObject2	Object
      //   67	195	3	localStringBuilder	StringBuilder
      // Exception table:
      //   from	to	target	type
      //   11	18	21	java/lang/InterruptedException
      //   96	105	166	finally
      //   171	175	166	finally
      //   96	105	170	java/lang/Throwable
      //   2	7	275	finally
      //   11	18	275	finally
      //   22	26	275	finally
      //   26	28	275	finally
      //   276	278	275	finally
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/c/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */