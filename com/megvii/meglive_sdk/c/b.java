package com.megvii.meglive_sdk.c;

import android.content.Context;
import com.megvii.meglive_sdk.f.d;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.w;
import com.megvii.meglive_sdk.i.x;
import com.megvii.meglive_sdk.volley.o.a;
import com.megvii.meglive_sdk.volley.o.b;
import com.megvii.meglive_sdk.volley.t;
import com.megvii.meglive_sdk.volley.toolbox.i;
import com.megvii.meglive_sdk.volley.toolbox.l;
import java.util.HashMap;
import java.util.Map;

public final class b
{
  private static b a;
  
  public static b a()
  {
    if (a == null) {
      a = new b();
    }
    return a;
  }
  
  public final void a(Context paramContext, String paramString1, final String paramString2, byte[] paramArrayOfByte, final d paramd)
  {
    Object localObject = new StringBuilder("bizToken = ");
    ((StringBuilder)localObject).append(paramString2);
    k.a("verify", ((StringBuilder)localObject).toString());
    localObject = new StringBuilder("data = ");
    ((StringBuilder)localObject).append(paramArrayOfByte);
    k.a("verify", ((StringBuilder)localObject).toString());
    localObject = new StringBuilder("URL = ");
    ((StringBuilder)localObject).append(w.c(paramString1));
    k.a("verify", ((StringBuilder)localObject).toString());
    localObject = new i();
    ((i)localObject).a("biz_token", paramString2.getBytes(), "text/plain; charset=UTF-8", ((i)localObject).b, "");
    ((i)localObject).a("meglive_data", paramArrayOfByte, "application/octet-stream", ((i)localObject).a, "meglive_data");
    paramString1 = w.c(paramString1);
    paramString2 = new HashMap();
    paramString1 = new com.megvii.meglive_sdk.volley.toolbox.j(paramString1, new o.b()new o.a {}, new o.a()
    {
      public final void a(t paramAnonymoust)
      {
        if (paramAnonymoust == null)
        {
          k.a("volleyError", "in null");
          paramAnonymoust = paramd;
          if (paramAnonymoust != null) {
            paramAnonymoust.a(-1, "timeout exception".getBytes());
          }
        }
        else if (paramAnonymoust.a == null)
        {
          k.a("volleyError", "networkResponse in null");
          paramAnonymoust = paramd;
          if (paramAnonymoust != null) {
            paramAnonymoust.a(-1, "timeout exception".getBytes());
          }
        }
        else
        {
          Object localObject = paramd;
          if (localObject != null) {
            ((d)localObject).a(paramAnonymoust.a.a, paramAnonymoust.a.b);
          }
          localObject = new StringBuilder("code: ");
          ((StringBuilder)localObject).append(paramAnonymoust.a.a);
          ((StringBuilder)localObject).append(" data: ");
          ((StringBuilder)localObject).append(new String(paramAnonymoust.a.b));
          k.a("response Fail", ((StringBuilder)localObject).toString());
        }
      }
    })
    {
      public final Map<String, String> b()
      {
        return paramString2;
      }
    };
    paramString1.c = ((i)localObject);
    x.a(paramContext).a(paramString1);
  }
  
  public final void a(Context paramContext, String paramString, final Map<String, String> paramMap1, final Map<String, String> paramMap2, final d paramd)
  {
    paramString = new l(paramString, new o.b()new o.a {}, new o.a()
    {
      public final void a(t paramAnonymoust)
      {
        if (paramAnonymoust == null)
        {
          k.a("volleyError", "in null");
          paramAnonymoust = paramd;
          if (paramAnonymoust != null) {
            paramAnonymoust.a(-1, "timeout exception".getBytes());
          }
        }
        else if (paramAnonymoust.a == null)
        {
          k.a("volleyError", "networkResponse in null");
          paramAnonymoust = paramd;
          if (paramAnonymoust != null) {
            paramAnonymoust.a(-1, "timeout exception".getBytes());
          }
        }
        else
        {
          Object localObject = paramd;
          if (localObject != null) {
            ((d)localObject).a(paramAnonymoust.a.a, paramAnonymoust.a.b);
          }
          localObject = new StringBuilder("code: ");
          ((StringBuilder)localObject).append(paramAnonymoust.a.a);
          ((StringBuilder)localObject).append(" data: ");
          ((StringBuilder)localObject).append(new String(paramAnonymoust.a.b));
          k.a("response Fail", ((StringBuilder)localObject).toString());
        }
      }
    })
    {
      protected final Map<String, String> a()
      {
        return paramMap1;
      }
      
      public final Map<String, String> b()
      {
        return paramMap2;
      }
    };
    x.a(paramContext).a(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */