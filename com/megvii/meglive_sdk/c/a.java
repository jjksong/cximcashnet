package com.megvii.meglive_sdk.c;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.provider.Settings.System;

public final class a
{
  public static boolean a(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    String str = Settings.System.getString(paramContext.getContentResolver(), "airplane_mode_on");
    int i;
    if ((str != null) && (str.equalsIgnoreCase("1"))) {
      i = -1;
    } else if ((localNetworkInfo != null) && (localNetworkInfo.isConnected())) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 1)
    {
      paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if (paramContext != null)
      {
        i = paramContext.getType();
        paramContext = paramContext.getState();
        if ((i == 1) && (paramContext == NetworkInfo.State.CONNECTED))
        {
          i = 1;
          break label140;
        }
        if ((i == 0) && (paramContext == NetworkInfo.State.CONNECTED))
        {
          i = 2;
          break label140;
        }
      }
      i = 0;
    }
    else if (i != 0)
    {
      i = 3;
    }
    else
    {
      i = 0;
    }
    label140:
    return i != 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */