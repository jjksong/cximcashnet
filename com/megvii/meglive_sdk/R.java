package com.megvii.meglive_sdk;

public final class R
{
  public static final class anim
  {
    public static final int mg_liveness_leftout = 2130771990;
    public static final int mg_liveness_rightin = 2130771991;
    public static final int mg_slide_in_left = 2130771992;
    public static final int mg_slide_in_right = 2130771993;
    public static final int mg_slide_out_left = 2130771994;
    public static final int mg_slide_out_right = 2130771995;
    public static final int progress_circle_shape = 2130771997;
  }
  
  public static final class attr
  {
    public static final int progress_width = 2130837855;
  }
  
  public static final class color
  {
    public static final int agree_toast_bg_color = 2130968603;
    public static final int black = 2130968613;
    public static final int blue = 2130968615;
    public static final int button_bg = 2130968623;
    public static final int button_disable = 2130968624;
    public static final int button_normal = 2130968627;
    public static final int button_pressed = 2130968628;
    public static final int dialog_check_btn_color = 2130968641;
    public static final int flash_bg_color = 2130968672;
    public static final int gray = 2130968675;
    public static final int gray1 = 2130968676;
    public static final int image_desc_textcolor = 2130968681;
    public static final int image_desc_textcolor1 = 2130968682;
    public static final int load_bg = 2130968709;
    public static final int progress = 2130968738;
    public static final int text_title_loading_page = 2130968763;
    public static final int toast_bg_color = 2130968766;
    public static final int white = 2130968771;
  }
  
  public static final class dimen
  {
    public static final int agree_toast_height = 2131034195;
    public static final int agree_toast_text_size = 2131034196;
    public static final int agree_toast_width = 2131034197;
    public static final int bottom_bar_height = 2131034202;
    public static final int bottom_bar_textsize = 2131034203;
    public static final int center_img_size = 2131034210;
    public static final int check_box_size = 2131034211;
    public static final int detect_tips_text_size = 2131034219;
    public static final int dialog_content_margin_top = 2131034220;
    public static final int dialog_item_height = 2131034221;
    public static final int dialog_line_margin_top = 2131034222;
    public static final int dialog_text_size = 2131034223;
    public static final int face_bg_height = 2131034226;
    public static final int face_bg_margin = 2131034227;
    public static final int face_bg_width = 2131034228;
    public static final int go_back_bt_height = 2131034235;
    public static final int go_back_bt_width = 2131034236;
    public static final int image_desc_text_size = 2131034244;
    public static final int image_desc_text_size_middle = 2131034245;
    public static final int image_desc_text_size_small = 2131034246;
    public static final int liveness_progress_maxsize = 2131034257;
    public static final int liveness_progress_minsize = 2131034258;
    public static final int load_img_height = 2131034259;
    public static final int load_img_width = 2131034260;
    public static final int progress_width = 2131034278;
    public static final int start_bt_height = 2131034279;
    public static final int start_bt_margin_bottom = 2131034280;
    public static final int start_bt_width = 2131034281;
    public static final int text_loading_page_title_size = 2131034282;
    public static final int text_margin_image = 2131034283;
    public static final int text_margin_text = 2131034284;
    public static final int tips_text_size = 2131034285;
    public static final int title_bar_height = 2131034286;
    public static final int title_bar_textsize = 2131034287;
    public static final int title_margin_top = 2131034289;
    public static final int user_agree_margin_bottom = 2131034298;
    public static final int user_agree_text_margin_left = 2131034299;
    public static final int user_agree_text_size = 2131034300;
  }
  
  public static final class drawable
  {
    public static final int left_shadow = 2131099854;
    public static final int selector_checkbox = 2131099932;
    public static final int selector_start_button = 2131099933;
    public static final int shape_agreement_toast_bg = 2131099934;
    public static final int shape_dialog_bg = 2131099935;
    public static final int shape_start_button_disable = 2131099936;
    public static final int shape_start_button_enable = 2131099937;
    public static final int shape_start_button_pressed = 2131099938;
    public static final int shape_toast_bg = 2131099939;
    public static final int start_button_bg = 2131099943;
    public static final int toast_bg = 2131099949;
  }
  
  public static final class id
  {
    public static final int begin_detect = 2131165262;
    public static final int bottombar = 2131165301;
    public static final int cb_user_agreement = 2131165320;
    public static final int image = 2131165480;
    public static final int image_animation = 2131165481;
    public static final int img_bar_left = 2131165486;
    public static final int iv_liveness_homepage_close = 2131165516;
    public static final int iv_megvii_powerby = 2131165517;
    public static final int iv_power = 2131165519;
    public static final int line = 2131165531;
    public static final int linearlayout_agreement = 2131165536;
    public static final int linearlayout_checkbox_hot_area = 2131165537;
    public static final int liveness_layout_cameraView = 2131165542;
    public static final int liveness_layout_textureview = 2131165543;
    public static final int livess_layout_coverview = 2131165544;
    public static final int ll_action_close = 2131165545;
    public static final int ll_bar_left = 2131165546;
    public static final int ll_detect_close = 2131165547;
    public static final int ll_progress_bar = 2131165548;
    public static final int main = 2131165562;
    public static final int pb_megvii_load = 2131165648;
    public static final int rl_mask = 2131165721;
    public static final int rl_phone = 2131165722;
    public static final int rl_title_bar = 2131165724;
    public static final int text0 = 2131165813;
    public static final int text2 = 2131165814;
    public static final int title_bar = 2131165824;
    public static final int toast_tv = 2131165833;
    public static final int tv_agreement_toast = 2131165854;
    public static final int tv_bar_title = 2131165855;
    public static final int tv_exit_confirm = 2131165859;
    public static final int tv_megvii_dialog_left = 2131165860;
    public static final int tv_megvii_dialog_right = 2131165861;
    public static final int tv_megvii_dialog_title = 2131165862;
    public static final int tv_megvii_retry_dialog_left = 2131165863;
    public static final int tv_megvii_retry_dialog_right = 2131165864;
    public static final int tv_megvii_retry_dialog_title = 2131165865;
    public static final int tv_tips_text = 2131165868;
    public static final int tv_user_agreement = 2131165870;
    public static final int tv_verify_title = 2131165871;
    public static final int web_agreement = 2131165902;
  }
  
  public static final class layout
  {
    public static final int action_liveness_activity = 2131296288;
    public static final int agreement_toast = 2131296414;
    public static final int bar_bottom = 2131296416;
    public static final int bar_title = 2131296417;
    public static final int dialog_exit = 2131296425;
    public static final int fmp_activity = 2131296430;
    public static final int grant_activity_new = 2131296440;
    public static final int idcard_toast = 2131296441;
    public static final int megvii_bar_bottom = 2131296454;
    public static final int megvii_liveness_dialog = 2131296455;
    public static final int megvii_liveness_retry_dialog = 2131296456;
    public static final int toast_agreement = 2131296490;
    public static final int user_agreement = 2131296491;
  }
  
  public static final class mipmap
  {
    public static final int checked = 2131361792;
    public static final int ic_close = 2131361793;
    public static final int ic_return = 2131361796;
    public static final int iv_megvii_powerby = 2131361809;
    public static final int unchecked = 2131361812;
  }
  
  public static final class raw
  {
    public static final int meg_action = 2131492870;
    public static final int meg_facelandmark = 2131492871;
    public static final int meg_facerect = 2131492872;
  }
  
  public static final class string
  {
    public static final int agree_link_text = 2131558439;
    public static final int agree_toast_text = 2131558440;
    public static final int agreement_title = 2131558441;
    public static final int back = 2131558444;
    public static final int face_screen_tips = 2131558465;
    public static final int grant_title = 2131558466;
    public static final int key_agreement_image_center = 2131558467;
    public static final int key_eye_close = 2131558468;
    public static final int key_eye_open = 2131558469;
    public static final int key_livenessHomePromptVerticalText = 2131558470;
    public static final int key_liveness_detect_button_highlight_bg_color = 2131558471;
    public static final int key_liveness_detect_button_normal_bg_color = 2131558472;
    public static final int key_liveness_detect_button_selected_bg_color = 2131558473;
    public static final int key_liveness_detect_button_text_color = 2131558474;
    public static final int key_liveness_exit_leftPrompt_color = 2131558475;
    public static final int key_liveness_exit_leftPrompt_size = 2131558476;
    public static final int key_liveness_exit_leftPrompt_text = 2131558477;
    public static final int key_liveness_exit_rightPrompt_color = 2131558478;
    public static final int key_liveness_exit_rightPrompt_size = 2131558479;
    public static final int key_liveness_exit_rightPrompt_text = 2131558480;
    public static final int key_liveness_exit_titlePrompt_color = 2131558481;
    public static final int key_liveness_exit_titlePrompt_size = 2131558482;
    public static final int key_liveness_exit_titlePrompt_text = 2131558483;
    public static final int key_liveness_guide_read_color = 2131558484;
    public static final int key_liveness_guide_remindtext_color = 2131558485;
    public static final int key_liveness_home_background_color = 2131558486;
    public static final int key_liveness_home_brand_material = 2131558487;
    public static final int key_liveness_home_closeIcon_material = 2131558488;
    public static final int key_liveness_home_loadingIcon_material = 2131558489;
    public static final int key_liveness_home_processBar_color = 2131558490;
    public static final int key_liveness_home_promptBlink_text = 2131558491;
    public static final int key_liveness_home_promptBrighter_text = 2131558492;
    public static final int key_liveness_home_promptCloser_text = 2131558493;
    public static final int key_liveness_home_promptDarker_text = 2131558494;
    public static final int key_liveness_home_promptFaceErea_text = 2131558495;
    public static final int key_liveness_home_promptFrontalFaceInBoundingBox_text = 2131558496;
    public static final int key_liveness_home_promptFrontalFace_text = 2131558497;
    public static final int key_liveness_home_promptFurther_text = 2131558498;
    public static final int key_liveness_home_promptNoBacklighting_text = 2131558499;
    public static final int key_liveness_home_promptNoEyesOcclusion_text = 2131558500;
    public static final int key_liveness_home_promptNoMouthOcclusion_text = 2131558501;
    public static final int key_liveness_home_promptNod_text = 2131558502;
    public static final int key_liveness_home_promptOpenMouth_text = 2131558503;
    public static final int key_liveness_home_promptShakeHead_text = 2131558504;
    public static final int key_liveness_home_promptStayStill_text = 2131558505;
    public static final int key_liveness_home_promptWait_text = 2131558506;
    public static final int key_liveness_home_prompt_color = 2131558507;
    public static final int key_liveness_home_prompt_size = 2131558508;
    public static final int key_liveness_home_ring_color = 2131558509;
    public static final int key_liveness_home_validationFailProcessBar_color = 2131558510;
    public static final int key_liveness_retry_leftPrompt_color = 2131558511;
    public static final int key_liveness_retry_leftPrompt_size = 2131558512;
    public static final int key_liveness_retry_leftPrompt_text = 2131558513;
    public static final int key_liveness_retry_rightPrompt_color = 2131558514;
    public static final int key_liveness_retry_rightPrompt_size = 2131558515;
    public static final int key_liveness_retry_rightPrompt_text = 2131558516;
    public static final int key_liveness_retry_titlePrompt_color = 2131558517;
    public static final int key_liveness_retry_titlePrompt_size = 2131558518;
    public static final int key_meglive_eye_blink_m4a = 2131558519;
    public static final int key_meglive_mouth_open_m4a = 2131558520;
    public static final int key_meglive_pitch_down_m4a = 2131558521;
    public static final int key_meglive_well_done_m4a = 2131558522;
    public static final int key_meglive_yaw_m4a = 2131558523;
    public static final int key_mouth_close = 2131558524;
    public static final int key_mouth_open = 2131558525;
    public static final int key_nod_down = 2131558526;
    public static final int key_nod_up = 2131558527;
    public static final int key_shakehead_left = 2131558528;
    public static final int key_shakehead_right = 2131558529;
    public static final int start_detect = 2131558554;
    public static final int title_source = 2131558556;
    public static final int verification_failed = 2131558557;
    public static final int verification_timeout = 2131558558;
  }
  
  public static final class style
  {
    public static final int DarkActionBar_Slide = 2131624109;
    public static final int DarkActionBar_Slide_Animation = 2131624110;
    public static final int mProgress_circle = 2131624313;
    public static final int sdkTheme = 2131624314;
  }
  
  public static final class styleable
  {
    public static final int[] CoverView = { 2130837855 };
    public static final int CoverView_progress_width = 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */