package com.megvii.meglive_sdk.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.megvii.action.fmp.liveness.lib.jni.MegActionLiveDetector;
import com.megvii.meglive_sdk.R.anim;
import com.megvii.meglive_sdk.R.dimen;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;
import com.megvii.meglive_sdk.R.raw;
import com.megvii.meglive_sdk.R.string;
import com.megvii.meglive_sdk.e.a.b;
import com.megvii.meglive_sdk.e.a.b.a;
import com.megvii.meglive_sdk.i.g;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.j;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.l;
import com.megvii.meglive_sdk.i.n;
import com.megvii.meglive_sdk.i.o;
import com.megvii.meglive_sdk.i.q;
import com.megvii.meglive_sdk.i.r;
import com.megvii.meglive_sdk.i.s;
import com.megvii.meglive_sdk.i.t;
import com.megvii.meglive_sdk.sdk.activity.GrantActivity;
import com.megvii.meglive_sdk.view.CameraGLView;
import com.megvii.meglive_sdk.view.CameraGLView.c;
import com.megvii.meglive_sdk.view.CoverView;
import java.util.Locale;
import org.json.JSONObject;

public class ActionLivenessActivity
  extends Activity
  implements Camera.PreviewCallback, TextureView.SurfaceTextureListener, View.OnClickListener, CameraGLView.c
{
  private com.megvii.meglive_sdk.i.c A;
  private ImageView B;
  private String C = "";
  private int D;
  private int E;
  private int F;
  private boolean G = false;
  private AnimationDrawable H = new AnimationDrawable();
  private AnimationDrawable I = new AnimationDrawable();
  private AnimationDrawable J = new AnimationDrawable();
  private AnimationDrawable K = new AnimationDrawable();
  private AnimationDrawable L;
  private int M = -1;
  private Runnable N = new Runnable()
  {
    public final void run()
    {
      ActionLivenessActivity.f(ActionLivenessActivity.this);
    }
  };
  private int O = -1;
  private int P = -1;
  private int Q = -1;
  private int R = -1;
  private int S = 0;
  private boolean T = false;
  private boolean U = false;
  private int V = 8;
  private boolean W = false;
  private int X = 0;
  private boolean Y = false;
  private boolean Z = false;
  com.megvii.meglive_sdk.g.a a;
  private com.megvii.action.fmp.liveness.lib.c.a aa = null;
  private com.megvii.meglive_sdk.e.a.c ab;
  private boolean ac = true;
  private boolean ad = false;
  private String ae = "";
  private final b.a af = new b.a()
  {
    public final void a(b paramAnonymousb)
    {
      if ((paramAnonymousb instanceof com.megvii.meglive_sdk.e.a.d)) {
        ActionLivenessActivity.m(ActionLivenessActivity.this).setVideoEncoder((com.megvii.meglive_sdk.e.a.d)paramAnonymousb);
      }
    }
    
    public final void b(b paramAnonymousb)
    {
      if ((paramAnonymousb instanceof com.megvii.meglive_sdk.e.a.d)) {
        ActionLivenessActivity.m(ActionLivenessActivity.this).setVideoEncoder(null);
      }
    }
    
    public final void c(b paramAnonymousb) {}
  };
  private AlertDialog ag;
  Handler b;
  boolean c = false;
  private com.megvii.meglive_sdk.d.a d;
  private TextureView e;
  private CameraGLView f;
  private CoverView g;
  private ProgressBar h;
  private LinearLayout i;
  private j j;
  private LinearLayout k;
  private ImageView l;
  private ImageView m;
  private t n;
  private boolean o = false;
  private byte[] p;
  private com.megvii.meglive_sdk.b.d q;
  private int r = 2;
  private int s;
  private int t;
  private l u;
  private String v;
  private Handler w = null;
  private g x;
  private String y;
  private int[] z;
  
  private void a(int paramInt)
  {
    Window localWindow = getWindow();
    WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
    if (paramInt == -1)
    {
      localLayoutParams.screenBrightness = -1.0F;
    }
    else
    {
      int i1 = paramInt;
      if (paramInt <= 0) {
        i1 = 1;
      }
      localLayoutParams.screenBrightness = (i1 / 255.0F);
    }
    localWindow.setAttributes(localLayoutParams);
  }
  
  private void a(final String paramString)
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        ActionLivenessActivity.a(ActionLivenessActivity.this).setTips(paramString);
      }
    });
  }
  
  private void a(final boolean paramBoolean, final float paramFloat, final int paramInt)
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        if (!paramBoolean)
        {
          ActionLivenessActivity.a(ActionLivenessActivity.this).setMode(1);
          ActionLivenessActivity.a(ActionLivenessActivity.this).a(paramFloat, paramInt, this.d);
          return;
        }
        ActionLivenessActivity.a(ActionLivenessActivity.this).setMode(1);
        ActionLivenessActivity.a(ActionLivenessActivity.this).a(paramFloat, paramInt, this.d);
        ActionLivenessActivity.a(ActionLivenessActivity.this).setMode(0);
      }
    });
  }
  
  static boolean a()
  {
    return Build.VERSION.SDK_INT >= 18;
  }
  
  private void c()
  {
    if (!this.c)
    {
      this.c = true;
      a(h.j, null);
      finish();
    }
  }
  
  private void d()
  {
    if ((o.a()) || (o.b())) {
      this.b.postDelayed(new Runnable()
      {
        public final void run()
        {
          if (ActionLivenessActivity.e(ActionLivenessActivity.this) == null) {
            ActionLivenessActivity.this.a(h.l, null);
          }
        }
      }, 1000L);
    }
  }
  
  public final void a(final int paramInt, final String paramString1, final String paramString2)
  {
    a();
    this.c = true;
    this.b.postDelayed(new Runnable()
    {
      public final void run()
      {
        com.megvii.meglive_sdk.g.a.a().a(paramInt, paramString1);
        ActionLivenessActivity.this.finish();
      }
    }, 500L);
  }
  
  public final void a(h paramh, String paramString)
  {
    a(paramh.q, paramh.r, paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      d();
      return;
    }
    a(h.l, null);
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == R.id.ll_action_close)
    {
      this.ag = this.x.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_quit_icon", this.y, this.r));
      return;
    }
    if (paramView.getId() == R.id.tv_megvii_dialog_left)
    {
      paramView = this.ag;
      if (paramView != null) {
        paramView.dismiss();
      }
      com.megvii.meglive_sdk.i.d.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_cancel_quit", this.y, this.r));
      return;
    }
    if (paramView.getId() == R.id.tv_megvii_dialog_right)
    {
      paramView = this.ag;
      if (paramView != null) {
        paramView.dismiss();
      }
      s.a(com.megvii.meglive_sdk.b.a.a("click_confirm_quit", this.y, this.r));
      c();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRequestedOrientation(1);
    Object localObject3 = getIntent().getStringExtra("language");
    if (!TextUtils.isEmpty((CharSequence)localObject3))
    {
      localObject1 = getResources();
      paramBundle = ((Resources)localObject1).getDisplayMetrics();
      localObject2 = ((Resources)localObject1).getConfiguration();
      localObject3 = new Locale((String)localObject3);
      if (Build.VERSION.SDK_INT >= 17) {
        ((Configuration)localObject2).setLocale((Locale)localObject3);
      } else {
        ((Configuration)localObject2).locale = ((Locale)localObject3);
      }
      ((Resources)localObject1).updateConfiguration((Configuration)localObject2, paramBundle);
    }
    setContentView(R.layout.action_liveness_activity);
    com.megvii.meglive_sdk.i.d.a(this);
    com.megvii.meglive_sdk.i.d.b(this);
    this.F = getIntent().getIntExtra("verticalCheckType", 0);
    this.n = new t(this);
    a(255);
    this.x = new g(this);
    this.u = new l(this);
    this.a = com.megvii.meglive_sdk.g.a.a();
    this.d = new com.megvii.meglive_sdk.d.a();
    this.y = com.megvii.meglive_sdk.i.e.a(this.a.a);
    this.q = com.megvii.meglive_sdk.i.e.b(this.a.a);
    this.r = this.q.a;
    this.s = this.q.b;
    this.t = this.q.c;
    this.z = this.q.d;
    paramBundle = this.y;
    int i2 = this.t;
    int i1 = this.s;
    int[] arrayOfInt = this.z;
    Object localObject1 = n.a(this.a.a, R.raw.meg_facerect);
    byte[] arrayOfByte = n.a(this.a.a, R.raw.meg_facelandmark);
    localObject3 = n.a(this.a.a, R.raw.meg_action);
    Object localObject2 = com.megvii.action.fmp.liveness.lib.b.a.a().a;
    if (((com.megvii.action.fmp.liveness.lib.a.a)localObject2).b == 0L)
    {
      if (i1 == arrayOfInt.length) {
        bool = false;
      } else {
        bool = true;
      }
      ((com.megvii.action.fmp.liveness.lib.a.a)localObject2).b = ((com.megvii.action.fmp.liveness.lib.a.a)localObject2).a.nativeCreateActionHandle(bool, i2, i1, paramBundle, arrayOfInt);
      if (((com.megvii.action.fmp.liveness.lib.a.a)localObject2).b != 0L)
      {
        bool = ((com.megvii.action.fmp.liveness.lib.a.a)localObject2).a.nativeLoadActionModel(((com.megvii.action.fmp.liveness.lib.a.a)localObject2).b, (byte[])localObject1, arrayOfByte, (byte[])localObject3);
        break label413;
      }
    }
    boolean bool = false;
    label413:
    this.b = new Handler();
    if (!bool)
    {
      a(h.m, null);
    }
    else
    {
      r.a(this);
      this.i = ((LinearLayout)findViewById(R.id.ll_progress_bar));
      this.l = ((ImageView)findViewById(R.id.iv_liveness_homepage_close));
      this.m = ((ImageView)findViewById(R.id.iv_megvii_powerby));
      this.h = ((ProgressBar)findViewById(R.id.pb_megvii_load));
      this.k = ((LinearLayout)findViewById(R.id.ll_action_close));
      this.k.setOnClickListener(this);
      this.f = ((CameraGLView)findViewById(R.id.liveness_layout_cameraView));
      this.e = ((TextureView)findViewById(R.id.liveness_layout_textureview));
      a();
      k.b("ActionLivenessActivity", "is not VideoRecord");
      this.e.setVisibility(0);
      this.e.setSurfaceTextureListener(this);
      this.g = ((CoverView)findViewById(R.id.livess_layout_coverview));
      this.j = new j();
      paramBundle = new HandlerThread("worker");
      paramBundle.start();
      this.w = new Handler(paramBundle.getLooper());
      this.B = ((ImageView)findViewById(R.id.image_animation));
      this.A = new com.megvii.meglive_sdk.i.c(this, this.B);
    }
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_processBar_color);
    i1 = q.a(q.e, paramBundle);
    this.D = getResources().getColor(i1);
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_validationFailProcessBar_color);
    i1 = q.a(q.e, paramBundle);
    this.E = getResources().getColor(i1);
    paramBundle = this.H;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_mouth_close);
    paramBundle.addFrame(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)), 500);
    paramBundle = this.H;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_mouth_open);
    paramBundle.addFrame(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)), 500);
    localObject1 = this.I;
    localObject2 = getResources();
    q.a(this);
    paramBundle = getString(R.string.key_eye_open);
    ((AnimationDrawable)localObject1).addFrame(((Resources)localObject2).getDrawable(q.a(q.a, paramBundle)), 500);
    localObject1 = this.I;
    paramBundle = getResources();
    q.a(this);
    localObject2 = getString(R.string.key_eye_close);
    ((AnimationDrawable)localObject1).addFrame(paramBundle.getDrawable(q.a(q.a, (String)localObject2)), 500);
    localObject2 = this.J;
    localObject1 = getResources();
    q.a(this);
    paramBundle = getString(R.string.key_nod_up);
    ((AnimationDrawable)localObject2).addFrame(((Resources)localObject1).getDrawable(q.a(q.a, paramBundle)), 500);
    paramBundle = this.J;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_nod_down);
    paramBundle.addFrame(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)), 500);
    paramBundle = this.K;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_shakehead_left);
    paramBundle.addFrame(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)), 500);
    paramBundle = this.K;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_shakehead_right);
    paramBundle.addFrame(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)), 500);
    localObject2 = (RotateAnimation)AnimationUtils.loadAnimation(this, R.anim.progress_circle_shape);
    ((RotateAnimation)localObject2).setDuration(1000L);
    ((RotateAnimation)localObject2).setRepeatCount(-1);
    ((RotateAnimation)localObject2).setInterpolator(new LinearInterpolator());
    paramBundle = this.h;
    localObject1 = getResources();
    q.a(this);
    localObject3 = getString(R.string.key_liveness_home_loadingIcon_material);
    paramBundle.setIndeterminateDrawable(((Resources)localObject1).getDrawable(q.a(q.a, (String)localObject3)));
    this.h.startAnimation((Animation)localObject2);
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_closeIcon_material);
    i1 = q.a(q.a, paramBundle);
    this.l.setImageBitmap(BitmapFactory.decodeResource(getResources(), i1));
    i1 = com.megvii.meglive_sdk.i.e.c(this);
    if (i1 == 1)
    {
      this.m.setVisibility(8);
      return;
    }
    if (i1 == 2)
    {
      paramBundle = getIntent().getStringExtra("logoFileName");
      if (!"".equals(paramBundle))
      {
        q.a(this);
        i1 = q.a(q.d, paramBundle);
        if (i1 != -1)
        {
          paramBundle = getResources().getDrawable(i1);
          this.m.setImageDrawable(paramBundle);
        }
      }
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.b = null;
    Object localObject = this.j;
    if (localObject != null) {
      ((j)localObject).a();
    }
    a(-1);
    localObject = com.megvii.action.fmp.liveness.lib.b.a.a().a;
    if (((com.megvii.action.fmp.liveness.lib.a.a)localObject).b != 0L)
    {
      ((com.megvii.action.fmp.liveness.lib.a.a)localObject).a.nativeActionRelease(((com.megvii.action.fmp.liveness.lib.a.a)localObject).b);
      ((com.megvii.action.fmp.liveness.lib.a.a)localObject).b = 0L;
    }
    localObject = this.n;
    if (localObject != null) {
      ((t)localObject).a();
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      this.ag = this.x.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_quit_icon", this.y, this.r));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  protected void onPause()
  {
    super.onPause();
    a();
    Object localObject = this.j;
    if (localObject != null) {
      ((j)localObject).a();
    }
    localObject = this.w;
    if (localObject != null) {
      ((Handler)localObject).removeCallbacksAndMessages(null);
    }
    localObject = this.u;
    if (localObject != null) {
      ((l)localObject).a();
    }
    c();
  }
  
  public void onPreviewFrame(final byte[] paramArrayOfByte, final Camera paramCamera)
  {
    if (this.p == null) {
      this.p = paramArrayOfByte;
    }
    int i1;
    if (this.F == 2) {
      i1 = 1;
    } else if ((!this.G) && (!this.n.b())) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if ((i1 == 0) && (this.R == 0))
    {
      paramArrayOfByte = getResources();
      q.a(this);
      paramCamera = getString(R.string.key_livenessHomePromptVerticalText);
      a(paramArrayOfByte.getString(q.a(q.b, paramCamera)));
      if (this.M != 13)
      {
        this.M = 13;
        s.a(com.megvii.meglive_sdk.b.a.a("fail_mirror", this.y, this.r, com.megvii.meglive_sdk.b.a.b.length - 1));
      }
      return;
    }
    if ((!this.T) && (!this.U))
    {
      paramCamera = paramCamera.getParameters().getPreviewSize();
      this.w.post(new Runnable()
      {
        public final void run()
        {
          ActionLivenessActivity.i(ActionLivenessActivity.this);
          if ((ActionLivenessActivity.b()) && (!ActionLivenessActivity.j(ActionLivenessActivity.this)))
          {
            ActionLivenessActivity.k(ActionLivenessActivity.this);
            ActionLivenessActivity.l(ActionLivenessActivity.this);
          }
          ActionLivenessActivity.a(ActionLivenessActivity.this, paramArrayOfByte, paramCamera.width, paramCamera.height);
        }
      });
      return;
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    a();
    this.g.postDelayed(new Runnable()
    {
      public final void run()
      {
        ActionLivenessActivity.a(ActionLivenessActivity.this).getMCenterX();
        float f = ActionLivenessActivity.a(ActionLivenessActivity.this).getMCenterY();
        int i = (int)ActionLivenessActivity.this.getResources().getDimension(R.dimen.liveness_progress_maxsize);
        RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        localLayoutParams.addRule(14);
        localLayoutParams.setMargins(0, (int)(f - i / 2), 0, 0);
        ActionLivenessActivity.b(ActionLivenessActivity.this).setLayoutParams(localLayoutParams);
      }
    }, 200L);
  }
  
  public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    this.o = false;
    return false;
  }
  
  public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {}
  
  public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/activity/ActionLivenessActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */