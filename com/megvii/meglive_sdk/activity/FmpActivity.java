package com.megvii.meglive_sdk.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.MediaCodec;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import com.megvii.action.fmp.liveness.lib.jni.MegFMPLiveDetector;
import com.megvii.meglive_sdk.R.anim;
import com.megvii.meglive_sdk.R.dimen;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;
import com.megvii.meglive_sdk.R.raw;
import com.megvii.meglive_sdk.R.string;
import com.megvii.meglive_sdk.e.a.b.a;
import com.megvii.meglive_sdk.e.c.a.a;
import com.megvii.meglive_sdk.e.c.a.b;
import com.megvii.meglive_sdk.i.e;
import com.megvii.meglive_sdk.i.f;
import com.megvii.meglive_sdk.i.g;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.i;
import com.megvii.meglive_sdk.i.j;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.l;
import com.megvii.meglive_sdk.i.n;
import com.megvii.meglive_sdk.i.o;
import com.megvii.meglive_sdk.i.q;
import com.megvii.meglive_sdk.i.r;
import com.megvii.meglive_sdk.i.s;
import com.megvii.meglive_sdk.i.t;
import com.megvii.meglive_sdk.view.CameraGLView;
import com.megvii.meglive_sdk.view.CameraGLView.c;
import com.megvii.meglive_sdk.view.CoverView;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import org.json.JSONObject;

public class FmpActivity
  extends Activity
  implements Camera.PreviewCallback, TextureView.SurfaceTextureListener, View.OnClickListener, CameraGLView.c
{
  private String A;
  private com.megvii.meglive_sdk.e.c.a B;
  private int C;
  private boolean D = false;
  private float E = 0.0F;
  private float F = 0.0F;
  private AlertDialog G;
  private int H = 1;
  private int I;
  private int J;
  private Runnable K = new Runnable()
  {
    public final void run()
    {
      FmpActivity.c(FmpActivity.this);
    }
  };
  private long L = 0L;
  private long M = 0L;
  private int N = -1;
  private int O = -1;
  private int P = 0;
  private boolean Q = false;
  private boolean R = false;
  private int S = 8;
  private boolean T = false;
  private boolean U = false;
  private int V = 0;
  private int W = -1;
  private float X = 0.0F;
  private com.megvii.action.fmp.liveness.lib.c.b Y;
  private long Z;
  Handler a;
  private ValueAnimator aa;
  private int ab = -1;
  private final long ac = 500L;
  private com.megvii.meglive_sdk.e.a.c ad;
  private boolean ae = true;
  private boolean af = false;
  private String ag = "";
  private final b.a ah = new b.a()
  {
    public final void a(com.megvii.meglive_sdk.e.a.b paramAnonymousb)
    {
      if ((paramAnonymousb instanceof com.megvii.meglive_sdk.e.a.d)) {
        FmpActivity.s(FmpActivity.this).setVideoEncoder((com.megvii.meglive_sdk.e.a.d)paramAnonymousb);
      }
    }
    
    public final void b(com.megvii.meglive_sdk.e.a.b paramAnonymousb)
    {
      if ((paramAnonymousb instanceof com.megvii.meglive_sdk.e.a.d)) {
        FmpActivity.s(FmpActivity.this).setVideoEncoder(null);
      }
    }
    
    public final void c(com.megvii.meglive_sdk.e.a.b paramAnonymousb)
    {
      if (((paramAnonymousb instanceof com.megvii.meglive_sdk.e.a.d)) && (FmpActivity.m(FmpActivity.this) == 2))
      {
        paramAnonymousb = new StringBuilder("onReleased: time = ");
        paramAnonymousb.append(System.currentTimeMillis() - FmpActivity.t(FmpActivity.this));
        k.a(paramAnonymousb.toString());
        paramAnonymousb = new StringBuilder("onReleased: failedType=");
        paramAnonymousb.append(FmpActivity.u(FmpActivity.this));
        paramAnonymousb.append(", liveness_failure_reason=");
        paramAnonymousb.append(FmpActivity.k(FmpActivity.this));
        paramAnonymousb.append(",curStep=");
        paramAnonymousb.append(FmpActivity.m(FmpActivity.this));
        paramAnonymousb.append(",lastStep=");
        paramAnonymousb.append(FmpActivity.v(FmpActivity.this));
        k.a(paramAnonymousb.toString());
        com.megvii.meglive_sdk.i.m.a("MediaEncoder onReleased...");
        FmpActivity.w(FmpActivity.this);
      }
    }
  };
  boolean b = false;
  long c = 0L;
  private com.megvii.meglive_sdk.g.a d;
  private TextureView e;
  private CameraGLView f;
  private ImageView g;
  private ImageView h;
  private CoverView i;
  private ProgressBar j;
  private LinearLayout k;
  private j l;
  private boolean m = false;
  private byte[] n;
  private LinearLayout o;
  private com.megvii.meglive_sdk.d.d p;
  private com.megvii.meglive_sdk.b.d q;
  private int r;
  private int s;
  private int t;
  private l u;
  private t v;
  private String w;
  private Handler x = null;
  private Handler y = null;
  private g z;
  
  private void a(final float paramFloat1, final float paramFloat2)
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { paramFloat1, paramFloat2 });
        localValueAnimator.setDuration(200L);
        localValueAnimator.setRepeatCount(0);
        localValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
          public final void onAnimationUpdate(ValueAnimator paramAnonymous2ValueAnimator)
          {
            FmpActivity.a(FmpActivity.this).a(((Float)paramAnonymous2ValueAnimator.getAnimatedValue()).floatValue(), FmpActivity.l(FmpActivity.this));
          }
        });
        localValueAnimator.addListener(new AnimatorListenerAdapter()
        {
          public final void onAnimationEnd(Animator paramAnonymous2Animator)
          {
            super.onAnimationEnd(paramAnonymous2Animator);
            if (FmpActivity.m(FmpActivity.this) == 1) {
              FmpActivity.n(FmpActivity.this);
            }
          }
        });
        localValueAnimator.start();
      }
    });
  }
  
  private void a(final float paramFloat, final int paramInt)
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        FmpActivity.a(FmpActivity.this).a(paramFloat, paramInt);
      }
    });
  }
  
  private void a(int paramInt)
  {
    Window localWindow = getWindow();
    WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
    if (paramInt == -1)
    {
      localLayoutParams.screenBrightness = -1.0F;
    }
    else
    {
      int i1 = paramInt;
      if (paramInt <= 0) {
        i1 = 1;
      }
      localLayoutParams.screenBrightness = (i1 / 255.0F);
    }
    localWindow.setAttributes(localLayoutParams);
  }
  
  private void a(final String paramString)
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        FmpActivity.a(FmpActivity.this).setTips(paramString);
      }
    });
  }
  
  private void b(int paramInt)
  {
    Object localObject = new StringBuilder("handleResult exec...,type =");
    ((StringBuilder)localObject).append(paramInt);
    com.megvii.meglive_sdk.i.m.a(((StringBuilder)localObject).toString());
    this.R = true;
    s.a(com.megvii.meglive_sdk.b.a.a("still_stop_video", this.A, this.H));
    Resources localResources = getResources();
    q.a(this);
    localObject = getString(R.string.key_liveness_home_promptWait_text);
    a(localResources.getString(q.a(q.b, (String)localObject)));
    if (paramInt == 0)
    {
      this.V = 0;
      a(360.0F, this.I);
      s.a(com.megvii.meglive_sdk.b.a.a("pass_stableliveness", this.A, this.H));
    }
    else
    {
      a(360.0F, this.J);
      this.V = 3003;
      if (paramInt == 1) {
        this.V = 3002;
      }
      s.a(com.megvii.meglive_sdk.b.a.a("fail_stableliveness:time_out", this.A, this.H));
    }
    d();
    f();
    localObject = com.megvii.action.fmp.liveness.lib.b.d.a().a;
    if (((com.megvii.action.fmp.liveness.lib.a.c)localObject).b != 0L) {
      ((com.megvii.action.fmp.liveness.lib.a.c)localObject).a.nativeStopSilentLiveDetect(((com.megvii.action.fmp.liveness.lib.a.c)localObject).b);
    }
  }
  
  private void b(boolean paramBoolean)
  {
    com.megvii.meglive_sdk.i.m.a("verify exec...");
    Object localObject3 = "";
    Object localObject2 = localObject3;
    try
    {
      Object localObject1 = new com/megvii/meglive_sdk/activity/FmpActivity$16;
      localObject2 = localObject3;
      ((16)localObject1).<init>(this);
      localObject2 = localObject3;
      runOnUiThread((Runnable)localObject1);
      localObject2 = localObject3;
      long l1 = System.currentTimeMillis();
      localObject2 = localObject3;
      localObject1 = new java/lang/StringBuilder;
      localObject2 = localObject3;
      ((StringBuilder)localObject1).<init>("videoOutputPath:");
      localObject2 = localObject3;
      ((StringBuilder)localObject1).append(this.ag);
      localObject2 = localObject3;
      k.a("md5", ((StringBuilder)localObject1).toString());
      localObject2 = localObject3;
      Object localObject4 = new java/io/File;
      localObject2 = localObject3;
      ((File)localObject4).<init>(this.ag);
      if (paramBoolean)
      {
        localObject2 = localObject3;
        com.megvii.meglive_sdk.i.m.a("isNeedCheck is true...");
        localObject2 = localObject3;
        if (!e())
        {
          localObject2 = localObject3;
          this.V = 3007;
        }
      }
      localObject2 = localObject3;
      localObject1 = new java/lang/StringBuilder;
      localObject2 = localObject3;
      ((StringBuilder)localObject1).<init>("verify: failedType=");
      localObject2 = localObject3;
      ((StringBuilder)localObject1).append(this.W);
      localObject2 = localObject3;
      ((StringBuilder)localObject1).append(", liveness_failure_reason=");
      localObject2 = localObject3;
      ((StringBuilder)localObject1).append(this.V);
      localObject2 = localObject3;
      k.a(((StringBuilder)localObject1).toString());
      localObject2 = localObject3;
      String str = f.a(this.W, this.V);
      localObject2 = localObject3;
      long l2 = ((File)localObject4).length();
      localObject2 = localObject3;
      k.a("video file size", String.valueOf(l2));
      if (l2 > 0L)
      {
        localObject2 = localObject3;
        localObject1 = i.a((File)localObject4);
      }
      else
      {
        localObject2 = localObject3;
        localObject1 = "".getBytes();
      }
      localObject2 = localObject3;
      Object localObject5 = com.megvii.apo.m.a(this).b();
      localObject2 = localObject3;
      System.currentTimeMillis();
      localObject2 = localObject3;
      Object localObject6 = new java/lang/StringBuilder;
      localObject2 = localObject3;
      ((StringBuilder)localObject6).<init>();
      localObject2 = localObject3;
      ((StringBuilder)localObject6).append(System.currentTimeMillis() - l1);
      localObject2 = localObject3;
      k.a("time const check", ((StringBuilder)localObject6).toString());
      localObject2 = localObject3;
      localObject6 = new java/lang/StringBuilder;
      localObject2 = localObject3;
      ((StringBuilder)localObject6).<init>();
      localObject2 = localObject3;
      ((StringBuilder)localObject6).append(((String)localObject5).getBytes().length);
      localObject2 = localObject3;
      k.a("fingerData size", ((StringBuilder)localObject6).toString());
      localObject2 = localObject3;
      k.a("fingerData data", (String)localObject5);
      localObject2 = localObject3;
      k.a("getSdkLog", s.a());
      localObject2 = localObject3;
      l1 = System.currentTimeMillis();
      localObject2 = localObject3;
      com.megvii.meglive_sdk.i.m.a("getDelta begin...");
      localObject2 = localObject3;
      if (this.W == 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }
      localObject2 = localObject3;
      localObject6 = s.a();
      localObject2 = localObject3;
      Object localObject7 = com.megvii.action.fmp.liveness.lib.b.d.a().a;
      localObject2 = localObject3;
      if (((com.megvii.action.fmp.liveness.lib.a.c)localObject7).b == 0L)
      {
        localObject1 = "";
      }
      else
      {
        localObject2 = localObject3;
        localObject1 = ((com.megvii.action.fmp.liveness.lib.a.c)localObject7).a.getSilentDeltaInfo(((com.megvii.action.fmp.liveness.lib.a.c)localObject7).b, str, paramBoolean, (String)localObject6, (String)localObject5, (byte[])localObject1);
      }
      localObject2 = localObject1;
      com.megvii.meglive_sdk.i.m.a("getDelta end...");
      localObject2 = localObject1;
      if (((File)localObject4).exists())
      {
        localObject2 = localObject1;
        ((File)localObject4).delete();
      }
      localObject2 = localObject1;
      localObject3 = new java/lang/StringBuilder;
      localObject2 = localObject1;
      ((StringBuilder)localObject3).<init>();
      localObject2 = localObject1;
      ((StringBuilder)localObject3).append(System.currentTimeMillis() - l1);
      localObject2 = localObject1;
      k.a("time const delta", ((StringBuilder)localObject3).toString());
      localObject2 = localObject1;
      localObject3 = new java/lang/StringBuilder;
      localObject2 = localObject1;
      ((StringBuilder)localObject3).<init>();
      localObject2 = localObject1;
      ((StringBuilder)localObject3).append(((String)localObject1).getBytes().length);
      localObject2 = localObject1;
      k.a("delta size", ((StringBuilder)localObject3).toString());
      localObject3 = localObject1;
      if (localObject1 == null) {
        localObject3 = "";
      }
      localObject2 = localObject3;
      localObject4 = com.megvii.meglive_sdk.c.b.a();
      localObject2 = localObject3;
      localObject5 = this.d.a;
      localObject2 = localObject3;
      str = e.e(this.d.a);
      localObject2 = localObject3;
      localObject7 = this.A;
      localObject2 = localObject3;
      localObject6 = ((String)localObject3).getBytes();
      localObject2 = localObject3;
      localObject1 = new com/megvii/meglive_sdk/activity/FmpActivity$17;
      localObject2 = localObject3;
      ((17)localObject1).<init>(this);
      localObject2 = localObject3;
      ((com.megvii.meglive_sdk.c.b)localObject4).a((Context)localObject5, str, (String)localObject7, (byte[])localObject6, (com.megvii.meglive_sdk.f.d)localObject1);
      return;
    }
    catch (Exception localException)
    {
      a(h.o, (String)localObject2);
      localException.printStackTrace();
      com.megvii.meglive_sdk.i.m.a("verify Exception...");
    }
  }
  
  private static boolean b()
  {
    return Build.VERSION.SDK_INT >= 18;
  }
  
  private void c()
  {
    if (!this.b)
    {
      this.b = true;
      a(h.j, null);
      finish();
    }
  }
  
  private void d()
  {
    com.megvii.meglive_sdk.i.m.a("doStopRecordVideo exec...");
    this.U = false;
    this.f.setCanVideoRecord(false);
    this.y.post(new Runnable()
    {
      public final void run()
      {
        if (!FmpActivity.a())
        {
          if (FmpActivity.f(FmpActivity.this) != null)
          {
            Object localObject = new StringBuilder("video const time: ");
            ((StringBuilder)localObject).append(System.currentTimeMillis() - FmpActivity.i(FmpActivity.this));
            k.a("test", ((StringBuilder)localObject).toString());
            com.megvii.meglive_sdk.e.c.a locala = FmpActivity.f(FmpActivity.this);
            if (locala.a != null)
            {
              localObject = locala.a;
              ((a.b)localObject).g = false;
              ((a.b)localObject).a.flush();
              ((a.b)localObject).a.stop();
              ((a.b)localObject).a.release();
            }
            if (locala.b != null)
            {
              localObject = locala.b;
              ((a.a)localObject).b = false;
              ((a.a)localObject).a = false;
              ((a.a)localObject).c.flush();
              ((a.a)localObject).c.stop();
              ((a.a)localObject).c.release();
            }
            localObject = FmpActivity.this;
            FmpActivity.a((FmpActivity)localObject, FmpActivity.f((FmpActivity)localObject).c);
          }
        }
        else {
          FmpActivity.j(FmpActivity.this);
        }
      }
    });
  }
  
  private boolean e()
  {
    com.megvii.meglive_sdk.i.m.a("checkVideo exec...");
    int i1 = 0;
    for (int i8 = 40;; i8--)
    {
      int i3 = 0;
      if (i8 <= 0) {
        break;
      }
      i1++;
      int i2 = i1;
      try
      {
        Object localObject1 = this.ag;
        i2 = i1;
        Object localObject2 = new java/io/File;
        i2 = i1;
        ((File)localObject2).<init>((String)localObject1);
        i2 = i1;
        byte[] arrayOfByte;
        long l1;
        long l2;
        long l3;
        int i5;
        int i9;
        int i4;
        if (!((File)localObject2).exists())
        {
          i2 = i3;
        }
        else
        {
          i2 = i1;
          localObject2 = new java/io/RandomAccessFile;
          i2 = i1;
          ((RandomAccessFile)localObject2).<init>((String)localObject1, "r");
          i2 = i1;
          arrayOfByte = new byte[4];
          i2 = i1;
          localObject1 = new byte[4];
          l1 = 0L;
          i2 = i1;
          l2 = ((RandomAccessFile)localObject2).length();
          i2 = i1;
          l3 = System.currentTimeMillis();
          i5 = 0;
          i9 = 0;
          i4 = 0;
        }
        for (;;)
        {
          int i7;
          int i6;
          long l4;
          if (l1 < l2)
          {
            i2 = i1;
            ((RandomAccessFile)localObject2).seek(l1);
            i2 = i1;
            ((RandomAccessFile)localObject2).read(arrayOfByte);
            i2 = i1;
            ((RandomAccessFile)localObject2).read((byte[])localObject1);
            i2 = arrayOfByte[0];
            i7 = arrayOfByte[1];
            i6 = arrayOfByte[2];
            i3 = arrayOfByte[3];
            i2 = (i7 & 0xFF) << 16 | (i2 & 0xFF) << 24 | (i6 & 0xFF) << 8 | (i3 & 0xFF) << 0;
            if (i2 > 0)
            {
              i3 = i1;
              l4 = i2;
              if (l4 <= l2 - l1) {
                i2 = i3;
              }
            }
          }
          try
          {
            String str = new java/lang/String;
            i2 = i3;
            str.<init>((byte[])localObject1);
            i6 = i5;
            i7 = i4;
            i2 = i3;
            if (!"".equals(str))
            {
              i2 = i3;
              if (str.equals("ftyp"))
              {
                if (i5 != 0) {
                  break label445;
                }
                i6 = 1;
                i7 = i4;
              }
              else
              {
                i2 = i3;
                if (str.equals("moov"))
                {
                  if (i9 != 0) {
                    break label445;
                  }
                  l1 += 8L;
                  i9 = 1;
                  break label381;
                }
                i6 = i5;
                i7 = i4;
                i2 = i3;
                if (str.equals("trak"))
                {
                  i7 = 1;
                  i6 = i5;
                }
              }
            }
            l1 += l4;
            i4 = i7;
            i5 = i6;
            label381:
            if ((i5 != 0) && (i9 != 0) && (i4 != 0))
            {
              i2 = i3;
              ((RandomAccessFile)localObject2).close();
              i2 = 1;
              i1 = i3;
            }
            else
            {
              i2 = i3;
              if (System.currentTimeMillis() - l3 <= 1000L)
              {
                i1 = i3;
                continue;
                i2 = i1;
                ((RandomAccessFile)localObject2).close();
                i2 = 0;
              }
              else
              {
                label445:
                i2 = i1;
                ((RandomAccessFile)localObject2).close();
                i2 = 0;
              }
            }
            if (i2 != 0)
            {
              bool = true;
              break label506;
            }
            i2 = i1;
            Thread.sleep(50L);
          }
          catch (Exception localException1)
          {
            i1 = i2;
          }
        }
        localException2.printStackTrace();
      }
      catch (Exception localException2)
      {
        i1 = i2;
      }
      com.megvii.meglive_sdk.i.m.a("checkVideo  Exception...");
    }
    boolean bool = false;
    label506:
    StringBuilder localStringBuilder = new StringBuilder("checkVideo  finish...,result= ");
    localStringBuilder.append(bool);
    com.megvii.meglive_sdk.i.m.a(localStringBuilder.toString());
    localStringBuilder = new StringBuilder("检查完毕，result = ");
    localStringBuilder.append(bool);
    localStringBuilder.append(",count=");
    localStringBuilder.append(i1);
    k.a("check", localStringBuilder.toString());
    return bool;
  }
  
  private void f()
  {
    runOnUiThread(new Runnable()
    {
      public final void run()
      {
        if (FmpActivity.o(FmpActivity.this) != null) {
          FmpActivity.o(FmpActivity.this).cancel();
        }
      }
    });
  }
  
  private void g()
  {
    s.a(com.megvii.meglive_sdk.b.a.a("enter_stableliveness", this.A, this.H));
    com.megvii.action.fmp.liveness.lib.a.c localc = com.megvii.action.fmp.liveness.lib.b.d.a().a;
    if (localc.b != 0L) {
      localc.a.nativeStartSilentLiveDetect(localc.b);
    }
  }
  
  private void h()
  {
    if ((o.a()) || (o.b())) {
      this.a.postDelayed(new Runnable()
      {
        public final void run()
        {
          if (FmpActivity.r(FmpActivity.this) == null) {
            FmpActivity.this.a(h.l, null);
          }
        }
      }, 1000L);
    }
  }
  
  private boolean i()
  {
    try
    {
      k.a("recording", "start recording");
      com.megvii.meglive_sdk.e.a.c localc = new com/megvii/meglive_sdk/e/a/c;
      localc.<init>(this);
      this.ad = localc;
      this.d.h = this.ad;
      if (this.ae) {
        new com.megvii.meglive_sdk.e.a.d(this.ad, this.ah, this.f.c, this.f.d);
      }
      if (this.af) {
        new com.megvii.meglive_sdk.e.a.a(this.ad, this.ah);
      }
      this.ad.a();
      this.ad.b();
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final void a(final int paramInt, final String paramString1, final String paramString2)
  {
    com.megvii.meglive_sdk.i.m.a("onFailed exec...");
    this.b = true;
    long l1 = System.currentTimeMillis() - this.c;
    if (l1 >= 500L)
    {
      com.megvii.meglive_sdk.g.a.a().a(paramInt, paramString1);
      finish();
      com.megvii.meglive_sdk.i.m.a("activity finish...");
      return;
    }
    this.a.postDelayed(new Runnable()
    {
      public final void run()
      {
        com.megvii.meglive_sdk.g.a.a().a(paramInt, paramString1);
        FmpActivity.this.finish();
        com.megvii.meglive_sdk.i.m.a("activity finish...");
      }
    }, 500L - l1);
  }
  
  public final void a(h paramh, String paramString)
  {
    a(paramh.q, paramh.r, paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      h();
      g();
      return;
    }
    a(h.l, null);
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == R.id.ll_detect_close)
    {
      this.G = this.z.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_quit_icon", this.A, this.H));
      return;
    }
    if (paramView.getId() == R.id.tv_megvii_dialog_left)
    {
      paramView = this.G;
      if (paramView != null) {
        paramView.dismiss();
      }
      com.megvii.meglive_sdk.i.d.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_cancel_quit", this.A, this.H));
      return;
    }
    if (paramView.getId() == R.id.tv_megvii_dialog_right)
    {
      paramView = this.G;
      if (paramView != null) {
        paramView.dismiss();
      }
      s.a(com.megvii.meglive_sdk.b.a.a("click_confirm_quit", this.A, this.H));
      c();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    r.a(this);
    setRequestedOrientation(1);
    Object localObject3 = getIntent().getStringExtra("language");
    if (localObject3 == null) {
      i1 = 1;
    } else if (((CharSequence)localObject3).length() == 0) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if (i1 == 0)
    {
      localObject1 = getResources();
      paramBundle = ((Resources)localObject1).getDisplayMetrics();
      localObject2 = ((Resources)localObject1).getConfiguration();
      localObject3 = new Locale((String)localObject3);
      if (Build.VERSION.SDK_INT >= 17) {
        ((Configuration)localObject2).setLocale((Locale)localObject3);
      } else {
        ((Configuration)localObject2).locale = ((Locale)localObject3);
      }
      ((Resources)localObject1).updateConfiguration((Configuration)localObject2, paramBundle);
    }
    setContentView(R.layout.fmp_activity);
    com.megvii.meglive_sdk.i.d.a(this);
    com.megvii.meglive_sdk.i.d.b(this);
    com.megvii.meglive_sdk.i.m.a(this);
    com.megvii.meglive_sdk.i.m.a("-------------------------------------------");
    this.v = new t(this);
    a(255);
    this.z = new g(this);
    this.u = new l(this);
    this.d = com.megvii.meglive_sdk.g.a.a();
    this.o = ((LinearLayout)findViewById(R.id.ll_detect_close));
    this.o.setOnClickListener(this);
    this.C = getIntent().getIntExtra("verticalCheckType", 0);
    this.A = e.a(this.d.a);
    this.q = e.b(this.d.a);
    this.r = this.q.c;
    this.s = this.q.e;
    this.t = this.q.f;
    paramBundle = new StringBuilder("recordTime = ");
    paramBundle.append(this.s);
    paramBundle.append("  recordFps = ");
    paramBundle.append(this.t);
    k.b("init", paramBundle.toString());
    this.a = new Handler();
    this.p = new com.megvii.meglive_sdk.d.d();
    Object localObject1 = this.A;
    int i1 = this.r;
    long l1 = this.s * 1000;
    byte[] arrayOfByte = n.a(this, R.raw.meg_facerect);
    Object localObject2 = n.a(this, R.raw.meg_facelandmark);
    localObject3 = n.a(this, R.raw.meg_action);
    paramBundle = com.megvii.action.fmp.liveness.lib.b.d.a().a;
    if (paramBundle.b == 0L)
    {
      paramBundle.b = paramBundle.a.nativeCreateSilentHandle((String)localObject1, i1, l1);
      if (paramBundle.b != 0L)
      {
        bool = paramBundle.a.nativeLoadSilentModel(paramBundle.b, arrayOfByte, (byte[])localObject2, (byte[])localObject3);
        break label475;
      }
    }
    boolean bool = false;
    label475:
    if (bool)
    {
      k.a("test", "模型加载成功");
      i1 = 1;
    }
    else
    {
      k.a("test", "模型加载失败");
      i1 = 0;
    }
    if (i1 == 0)
    {
      a(h.m, null);
    }
    else
    {
      this.g = ((ImageView)findViewById(R.id.iv_liveness_homepage_close));
      this.h = ((ImageView)findViewById(R.id.iv_megvii_powerby));
      this.k = ((LinearLayout)findViewById(R.id.ll_progress_bar));
      this.f = ((CameraGLView)findViewById(R.id.liveness_layout_cameraView));
      this.e = ((TextureView)findViewById(R.id.liveness_layout_textureview));
      if (b())
      {
        this.f.setVisibility(0);
        this.f.setVideoFps(this.t);
        this.f.setPreviewCallback(this);
        this.f.setICameraOpenCallBack(this);
      }
      else
      {
        this.e.setVisibility(0);
        this.e.setSurfaceTextureListener(this);
      }
      this.i = ((CoverView)findViewById(R.id.livess_layout_coverview));
      this.j = ((ProgressBar)findViewById(R.id.pb_megvii_load));
      this.l = new j();
      paramBundle = new HandlerThread("worker");
      paramBundle.start();
      this.x = new Handler(paramBundle.getLooper());
      paramBundle = new HandlerThread("videoEncoder");
      paramBundle.start();
      this.y = new Handler(paramBundle.getLooper());
    }
    this.E = 198.0F;
    this.F = 270.0F;
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_processBar_color);
    i1 = q.a(q.e, paramBundle);
    this.I = getResources().getColor(i1);
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_validationFailProcessBar_color);
    i1 = q.a(q.e, paramBundle);
    this.J = getResources().getColor(i1);
    localObject3 = (RotateAnimation)AnimationUtils.loadAnimation(this, R.anim.progress_circle_shape);
    ((RotateAnimation)localObject3).setDuration(1000L);
    ((RotateAnimation)localObject3).setRepeatCount(-1);
    ((RotateAnimation)localObject3).setInterpolator(new LinearInterpolator());
    paramBundle = this.j;
    localObject2 = getResources();
    q.a(this);
    localObject1 = getString(R.string.key_liveness_home_loadingIcon_material);
    paramBundle.setIndeterminateDrawable(((Resources)localObject2).getDrawable(q.a(q.a, (String)localObject1)));
    this.j.startAnimation((Animation)localObject3);
    q.a(this);
    paramBundle = getString(R.string.key_liveness_home_closeIcon_material);
    i1 = q.a(q.a, paramBundle);
    this.g.setImageBitmap(BitmapFactory.decodeResource(getResources(), i1));
    i1 = e.c(this);
    if (i1 == 1)
    {
      this.h.setVisibility(8);
      return;
    }
    if (i1 == 2)
    {
      paramBundle = getIntent().getStringExtra("logoFileName");
      if (!"".equals(paramBundle))
      {
        q.a(this);
        i1 = q.a(q.d, paramBundle);
        if (i1 != -1)
        {
          paramBundle = getResources().getDrawable(i1);
          this.h.setImageDrawable(paramBundle);
        }
      }
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.a = null;
    a(-1);
    if (this.p != null)
    {
      localObject = com.megvii.action.fmp.liveness.lib.b.d.a().a;
      if (((com.megvii.action.fmp.liveness.lib.a.c)localObject).b != 0L)
      {
        ((com.megvii.action.fmp.liveness.lib.a.c)localObject).a.nativeSilentRelease(((com.megvii.action.fmp.liveness.lib.a.c)localObject).b);
        ((com.megvii.action.fmp.liveness.lib.a.c)localObject).b = 0L;
      }
    }
    Object localObject = this.v;
    if (localObject != null) {
      ((t)localObject).a();
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      this.G = this.z.a(this);
      s.a(com.megvii.meglive_sdk.b.a.a("click_quit_icon", this.A, this.H));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  protected void onPause()
  {
    super.onPause();
    if (b())
    {
      localObject = this.f;
      if (localObject != null)
      {
        ((CameraGLView)localObject).setICameraOpenCallBack(null);
        this.f.onPause();
        break label47;
      }
    }
    Object localObject = this.l;
    if (localObject != null) {
      ((j)localObject).a();
    }
    label47:
    this.e = null;
    localObject = this.x;
    if (localObject != null) {
      ((Handler)localObject).removeCallbacksAndMessages(null);
    }
    localObject = this.u;
    if (localObject != null) {
      ((l)localObject).a();
    }
    c();
  }
  
  public void onPreviewFrame(final byte[] paramArrayOfByte, final Camera paramCamera)
  {
    if (this.n == null) {
      this.n = paramArrayOfByte;
    }
    if ((!this.Q) && (!this.R))
    {
      if (this.N == -1) {
        this.L = System.currentTimeMillis();
      }
      int i1 = this.C;
      int i2 = 0;
      if ((i1 == 1) && (System.currentTimeMillis() - this.L >= this.r * 1000)) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 != 0)
      {
        b(1);
        return;
      }
      if (this.C == 2)
      {
        i1 = 1;
      }
      else if (!this.D)
      {
        i1 = i2;
        if (!this.v.b()) {}
      }
      else
      {
        i1 = 1;
      }
      if ((i1 == 0) && (this.N == 0))
      {
        paramArrayOfByte = getResources();
        q.a(this);
        paramCamera = getString(R.string.key_livenessHomePromptVerticalText);
        a(paramArrayOfByte.getString(q.a(q.b, paramCamera)));
        if (this.ab != 13)
        {
          this.ab = 13;
          s.a(com.megvii.meglive_sdk.b.a.a("fail_mirror", this.A, this.H, com.megvii.meglive_sdk.b.a.b.length - 1));
        }
        return;
      }
      paramCamera = paramCamera.getParameters().getPreviewSize();
      this.f.setCanVideoRecord(true);
      this.y.post(new Runnable()
      {
        public final void run()
        {
          if (FmpActivity.d(FmpActivity.this))
          {
            Object localObject1;
            if (!FmpActivity.a())
            {
              if (!FmpActivity.e(FmpActivity.this))
              {
                k.a("test", "mIMediaMuxer init...");
                localObject1 = FmpActivity.this;
                FmpActivity.a((FmpActivity)localObject1, new com.megvii.meglive_sdk.e.c.a((Context)localObject1));
                localObject1 = FmpActivity.f(FmpActivity.this);
                if ((com.megvii.meglive_sdk.e.c.a.a()) && (((com.megvii.meglive_sdk.e.c.a)localObject1).a == null)) {
                  ((com.megvii.meglive_sdk.e.c.a)localObject1).a = new a.b((com.megvii.meglive_sdk.e.c.a)localObject1);
                }
                FmpActivity.a(FmpActivity.this, System.currentTimeMillis());
              }
              com.megvii.meglive_sdk.e.c.a locala = FmpActivity.f(FmpActivity.this);
              Object localObject2 = paramArrayOfByte;
              int i2 = FmpActivity.g(FmpActivity.this).b;
              int i3 = FmpActivity.g(FmpActivity.this).c;
              int j = 360 - FmpActivity.g(FmpActivity.this).e;
              localObject1 = localObject2;
              if (j != 0)
              {
                int i = 0;
                int k;
                int m;
                int n;
                if (j != 90)
                {
                  if (j != 180)
                  {
                    if (j != 270)
                    {
                      localObject1 = localObject2;
                    }
                    else
                    {
                      j = i2 * i3;
                      localObject1 = new byte[j * 3 / 2];
                      k = i2 - 1;
                      m = k;
                      i = 0;
                      while (m >= 0)
                      {
                        int i1 = 0;
                        n = 0;
                        while (i1 < i3)
                        {
                          localObject1[i] = localObject2[(n + m)];
                          i++;
                          n += i2;
                          i1++;
                        }
                        m--;
                      }
                      i = j;
                      while (k > 0)
                      {
                        m = j;
                        for (n = 0; n < i3 / 2; n++)
                        {
                          localObject1[i] = localObject2[(k - 1 + m)];
                          i++;
                          localObject1[i] = localObject2[(m + k)];
                          i++;
                          m += i2;
                        }
                        k -= 2;
                      }
                    }
                  }
                  else
                  {
                    k = i2 * i3;
                    m = k * 3 / 2;
                    localObject1 = new byte[m];
                    for (j = k - 1; j >= 0; j--)
                    {
                      localObject1[i] = localObject2[j];
                      i++;
                    }
                    for (j = m - 1; j >= k; j -= 2)
                    {
                      m = i + 1;
                      localObject1[i] = localObject2[(j - 1)];
                      i = m + 1;
                      localObject1[m] = localObject2[j];
                    }
                  }
                }
                else
                {
                  m = i2 * i3;
                  n = m * 3 / 2;
                  localObject1 = new byte[n];
                  i = 0;
                  j = 0;
                  while (i < i2)
                  {
                    for (k = i3 - 1; k >= 0; k--)
                    {
                      localObject1[j] = localObject2[(k * i2 + i)];
                      j++;
                    }
                    i++;
                  }
                  i = n - 1;
                  for (j = i2 - 1; j > 0; j -= 2) {
                    for (k = 0; k < i3 / 2; k++)
                    {
                      n = k * i2 + m;
                      localObject1[i] = localObject2[(n + j)];
                      i--;
                      localObject1[i] = localObject2[(n + (j - 1))];
                      i--;
                    }
                  }
                }
              }
              if (locala.a != null)
              {
                localObject2 = locala.a;
                ((a.b)localObject2).g = true;
                if (((a.b)localObject2).f.size() >= ((a.b)localObject2).f.size()) {
                  ((a.b)localObject2).f.poll();
                }
                ((a.b)localObject2).f.add(localObject1);
              }
              if (locala.b != null)
              {
                localObject1 = locala.b;
                ((a.a)localObject1).a = true;
                ((a.a)localObject1).b = true;
              }
              FmpActivity.a(FmpActivity.this, true);
              return;
            }
            if (!FmpActivity.e(FmpActivity.this))
            {
              localObject1 = FmpActivity.this;
              FmpActivity.a((FmpActivity)localObject1, FmpActivity.h((FmpActivity)localObject1));
            }
          }
        }
      });
      this.x.post(new Runnable()
      {
        public final void run()
        {
          FmpActivity.a(FmpActivity.this, paramArrayOfByte, paramCamera.width, paramCamera.height);
        }
      });
      return;
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    if ((b()) && (this.f != null))
    {
      CameraGLView.a = 1;
      if (!j.b()) {
        CameraGLView.a = 0;
      }
      this.f.onResume();
    }
    this.i.postDelayed(new Runnable()
    {
      public final void run()
      {
        FmpActivity.a(FmpActivity.this).getMCenterX();
        float f = FmpActivity.a(FmpActivity.this).getMCenterY();
        int i = (int)FmpActivity.this.getResources().getDimension(R.dimen.liveness_progress_maxsize);
        RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        localLayoutParams.addRule(14);
        localLayoutParams.setMargins(0, (int)(f - i / 2), 0, 0);
        FmpActivity.b(FmpActivity.this).setLayoutParams(localLayoutParams);
      }
    }, 200L);
  }
  
  public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    this.m = false;
    return false;
  }
  
  public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {}
  
  public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/activity/FmpActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */