package com.megvii.meglive_sdk.e.b;

import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.text.TextUtils;

public final class e
  implements Runnable
{
  public final Object a = new Object();
  public EGLContext b;
  public boolean c;
  public Object d;
  public int e = -1;
  public float[] f = new float[32];
  public boolean g;
  public boolean h;
  private int i;
  private c j;
  private c.a k;
  private d l;
  
  public static final e a(String paramString)
  {
    e locale = new e();
    synchronized (locale.a)
    {
      Thread localThread = new java/lang/Thread;
      if (TextUtils.isEmpty(paramString)) {
        paramString = "RenderHandler";
      }
      localThread.<init>(locale, paramString);
      localThread.start();
    }
    try
    {
      locale.a.wait();
      return locale;
      paramString = finally;
      throw paramString;
    }
    catch (InterruptedException paramString)
    {
      for (;;) {}
    }
  }
  
  private final void a()
  {
    Object localObject1 = this.k;
    Object localObject2;
    if (localObject1 != null)
    {
      ((c.a)localObject1).a.a();
      localObject2 = ((c.a)localObject1).a;
      EGLSurface localEGLSurface = ((c.a)localObject1).b;
      if (localEGLSurface != EGL14.EGL_NO_SURFACE)
      {
        EGL14.eglMakeCurrent(((c)localObject2).b, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
        EGL14.eglDestroySurface(((c)localObject2).b, localEGLSurface);
      }
      localObject2 = EGL14.EGL_NO_SURFACE;
      ((c.a)localObject1).b = EGL14.EGL_NO_SURFACE;
      this.k = null;
    }
    localObject1 = this.l;
    if (localObject1 != null)
    {
      if (((d)localObject1).a >= 0) {
        GLES20.glDeleteProgram(((d)localObject1).a);
      }
      ((d)localObject1).a = -1;
      this.l = null;
    }
    localObject1 = this.j;
    if (localObject1 != null)
    {
      if (((c)localObject1).b != EGL14.EGL_NO_DISPLAY)
      {
        if (!EGL14.eglDestroyContext(((c)localObject1).b, ((c)localObject1).a))
        {
          localObject2 = new StringBuilder("display:");
          ((StringBuilder)localObject2).append(((c)localObject1).b);
          ((StringBuilder)localObject2).append(" context: ");
          ((StringBuilder)localObject2).append(((c)localObject1).a);
          new StringBuilder("eglDestroyContex:").append(EGL14.eglGetError());
        }
        ((c)localObject1).a = EGL14.EGL_NO_CONTEXT;
        if (((c)localObject1).c != EGL14.EGL_NO_CONTEXT)
        {
          if (!EGL14.eglDestroyContext(((c)localObject1).b, ((c)localObject1).c))
          {
            localObject2 = new StringBuilder("display:");
            ((StringBuilder)localObject2).append(((c)localObject1).b);
            ((StringBuilder)localObject2).append(" context: ");
            ((StringBuilder)localObject2).append(((c)localObject1).c);
            new StringBuilder("eglDestroyContex:").append(EGL14.eglGetError());
          }
          ((c)localObject1).c = EGL14.EGL_NO_CONTEXT;
        }
        EGL14.eglTerminate(((c)localObject1).b);
        EGL14.eglReleaseThread();
      }
      ((c)localObject1).b = EGL14.EGL_NO_DISPLAY;
      ((c)localObject1).a = EGL14.EGL_NO_CONTEXT;
      this.j = null;
    }
  }
  
  public final void a(int paramInt, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    synchronized (this.a)
    {
      if (this.h) {
        return;
      }
      this.e = paramInt;
      if ((paramArrayOfFloat1 != null) && (paramArrayOfFloat1.length >= 16)) {
        System.arraycopy(paramArrayOfFloat1, 0, this.f, 0, 16);
      } else {
        Matrix.setIdentityM(this.f, 0);
      }
      if ((paramArrayOfFloat2 != null) && (paramArrayOfFloat2.length >= 16)) {
        System.arraycopy(paramArrayOfFloat2, 0, this.f, 16, 16);
      } else {
        Matrix.setIdentityM(this.f, 16);
      }
      this.i += 1;
      this.a.notifyAll();
      return;
    }
  }
  
  public final void run()
  {
    synchronized (this.a)
    {
      this.h = false;
      this.g = false;
      this.i = 0;
      this.a.notifyAll();
      synchronized (this.a)
      {
        while (!this.h)
        {
          if (this.g)
          {
            this.g = false;
            a();
            ??? = new com/megvii/meglive_sdk/e/b/c;
            ((c)???).<init>(this.b, this.c);
            this.j = ((c)???);
            ??? = this.j;
            Object localObject7 = this.d;
            c.a locala = new com/megvii/meglive_sdk/e/b/c$a;
            locala.<init>((c)???, localObject7);
            locala.a();
            this.k = locala;
            this.k.a();
            ??? = new com/megvii/meglive_sdk/e/b/d;
            ((d)???).<init>();
            this.l = ((d)???);
            this.d = null;
            this.a.notifyAll();
          }
          int m;
          if (this.i > 0) {
            m = 1;
          } else {
            m = 0;
          }
          if (m != 0) {
            this.i -= 1;
          }
          if (m != 0)
          {
            if ((this.j != null) && (this.e >= 0))
            {
              this.k.a();
              GLES20.glClearColor(1.0F, 1.0F, 0.0F, 1.0F);
              GLES20.glClear(16384);
              ??? = this.l;
              ??? = this.f;
              if ((??? != null) && (???.length >= 32)) {
                System.arraycopy(???, 16, ((d)???).f, 0, 16);
              } else {
                Matrix.setIdentityM(((d)???).f, 0);
              }
              ??? = this.l;
              m = this.e;
              ??? = this.f;
              GLES20.glUseProgram(((d)???).a);
              if (??? != null) {
                GLES20.glUniformMatrix4fv(((d)???).e, 1, false, (float[])???, 0);
              }
              GLES20.glUniformMatrix4fv(((d)???).d, 1, false, ((d)???).f, 0);
              GLES20.glActiveTexture(33984);
              GLES20.glBindTexture(36197, m);
              GLES20.glDrawArrays(5, 0, 4);
              GLES20.glBindTexture(36197, 0);
              GLES20.glUseProgram(0);
              ??? = this.k;
              ??? = ((c.a)???).a;
              ??? = ((c.a)???).b;
              if (!EGL14.eglSwapBuffers(((c)???).b, (EGLSurface)???)) {
                EGL14.eglGetError();
              }
            }
          }
          else {
            try
            {
              synchronized (this.a)
              {
                this.a.wait();
              }
              synchronized (this.a)
              {
                this.h = true;
                a();
                this.a.notifyAll();
                return;
              }
            }
            catch (InterruptedException localInterruptedException) {}
          }
        }
        throw ((Throwable)localObject5);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/b/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */