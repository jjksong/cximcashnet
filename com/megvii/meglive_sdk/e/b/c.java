package com.megvii.meglive_sdk.e.b;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.os.Build.VERSION;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@TargetApi(18)
public final class c
{
  EGLContext a = EGL14.EGL_NO_CONTEXT;
  EGLDisplay b = EGL14.EGL_NO_DISPLAY;
  EGLContext c = EGL14.EGL_NO_CONTEXT;
  private EGLConfig d = null;
  
  public c(EGLContext paramEGLContext, boolean paramBoolean)
  {
    if (this.b == EGL14.EGL_NO_DISPLAY)
    {
      this.b = EGL14.eglGetDisplay(0);
      if (this.b != EGL14.EGL_NO_DISPLAY)
      {
        Object localObject = new int[2];
        if (EGL14.eglInitialize(this.b, (int[])localObject, 0, (int[])localObject, 1))
        {
          if (paramEGLContext == null) {
            paramEGLContext = EGL14.EGL_NO_CONTEXT;
          }
          if (this.a == EGL14.EGL_NO_CONTEXT)
          {
            this.d = a(paramBoolean);
            localObject = this.d;
            if (localObject != null)
            {
              paramEGLContext = EGL14.eglCreateContext(this.b, (EGLConfig)localObject, paramEGLContext, new int[] { 12440, 2, 12344 }, 0);
              int i = EGL14.eglGetError();
              if (i == 12288)
              {
                this.a = paramEGLContext;
              }
              else
              {
                paramEGLContext = new StringBuilder();
                paramEGLContext.append("eglCreateContext");
                paramEGLContext.append(": EGL error: 0x");
                paramEGLContext.append(Integer.toHexString(i));
                throw new RuntimeException(paramEGLContext.toString());
              }
            }
            else
            {
              throw new RuntimeException("chooseConfig failed");
            }
          }
          paramEGLContext = new int[1];
          EGL14.eglQueryContext(this.b, this.a, 12440, paramEGLContext, 0);
          a();
          return;
        }
        this.b = null;
        throw new RuntimeException("eglInitialize failed");
      }
      throw new RuntimeException("eglGetDisplay failed");
    }
    throw new RuntimeException("EGL already set up");
  }
  
  private EGLConfig a(boolean paramBoolean)
  {
    int[] arrayOfInt2 = new int[17];
    int[] tmp8_6 = arrayOfInt2;
    tmp8_6[0] = '぀';
    int[] tmp14_8 = tmp8_6;
    tmp14_8[1] = 4;
    int[] tmp18_14 = tmp14_8;
    tmp18_14[2] = '〤';
    int[] tmp24_18 = tmp18_14;
    tmp24_18[3] = 8;
    int[] tmp29_24 = tmp24_18;
    tmp29_24[4] = '〣';
    int[] tmp35_29 = tmp29_24;
    tmp35_29[5] = 8;
    int[] tmp40_35 = tmp35_29;
    tmp40_35[6] = '〢';
    int[] tmp47_40 = tmp40_35;
    tmp47_40[7] = 8;
    int[] tmp53_47 = tmp47_40;
    tmp53_47[8] = '〡';
    int[] tmp60_53 = tmp53_47;
    tmp60_53[9] = 8;
    int[] tmp66_60 = tmp60_53;
    tmp66_60[10] = '〸';
    int[] tmp73_66 = tmp66_60;
    tmp73_66[11] = '〸';
    int[] tmp80_73 = tmp73_66;
    tmp80_73[12] = '〸';
    int[] tmp87_80 = tmp80_73;
    tmp87_80[13] = '〸';
    int[] tmp94_87 = tmp87_80;
    tmp94_87[14] = '〸';
    int[] tmp101_94 = tmp94_87;
    tmp101_94[15] = '〸';
    int[] tmp108_101 = tmp101_94;
    tmp108_101[16] = '〸';
    tmp108_101;
    int j = 10;
    int i = j;
    if (paramBoolean)
    {
      i = j;
      if (Build.VERSION.SDK_INT >= 18)
      {
        arrayOfInt2[10] = 12610;
        i = 12;
        arrayOfInt2[11] = 1;
      }
    }
    for (j = 16; j >= i; j--) {
      arrayOfInt2[j] = 12344;
    }
    EGLConfig[] arrayOfEGLConfig = new EGLConfig[1];
    int[] arrayOfInt1 = new int[1];
    if (!EGL14.eglChooseConfig(this.b, arrayOfInt2, 0, arrayOfEGLConfig, 0, 1, arrayOfInt1, 0)) {
      return null;
    }
    return arrayOfEGLConfig[0];
  }
  
  public final int a(EGLSurface paramEGLSurface, int paramInt)
  {
    int[] arrayOfInt = new int[1];
    EGL14.eglQuerySurface(this.b, paramEGLSurface, paramInt, arrayOfInt, 0);
    return arrayOfInt[0];
  }
  
  final EGLSurface a(Object paramObject)
  {
    try
    {
      paramObject = EGL14.eglCreateWindowSurface(this.b, this.d, paramObject, new int[] { 12344 }, 0);
    }
    catch (IllegalArgumentException paramObject)
    {
      paramObject = null;
    }
    return (EGLSurface)paramObject;
  }
  
  final void a()
  {
    if (!EGL14.eglMakeCurrent(this.b, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT)) {
      new StringBuilder("makeDefault").append(EGL14.eglGetError());
    }
  }
  
  public static final class a
  {
    final c a;
    EGLSurface b = EGL14.EGL_NO_SURFACE;
    private final int c;
    private final int d;
    
    a(c paramc, Object paramObject)
    {
      if ((!(paramObject instanceof SurfaceView)) && (!(paramObject instanceof Surface)) && (!(paramObject instanceof SurfaceHolder)) && (!(paramObject instanceof SurfaceTexture))) {
        throw new IllegalArgumentException("unsupported surface");
      }
      this.a = paramc;
      this.b = this.a.a(paramObject);
      this.c = this.a.a(this.b, 12375);
      this.d = this.a.a(this.b, 12374);
    }
    
    public final void a()
    {
      c localc = this.a;
      EGLSurface localEGLSurface = this.b;
      if ((localEGLSurface != null) && (localEGLSurface != EGL14.EGL_NO_SURFACE))
      {
        if (!EGL14.eglMakeCurrent(localc.b, localEGLSurface, localEGLSurface, localc.a)) {
          new StringBuilder("eglMakeCurrent:").append(EGL14.eglGetError());
        }
        return;
      }
      EGL14.eglGetError();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/b/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */