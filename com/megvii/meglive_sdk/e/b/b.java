package com.megvii.meglive_sdk.e.b;

import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;
import com.megvii.meglive_sdk.e.a.d;
import com.megvii.meglive_sdk.view.CameraGLView;
import java.lang.ref.WeakReference;
import java.nio.FloatBuffer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public final class b
  implements SurfaceTexture.OnFrameAvailableListener, GLSurfaceView.Renderer
{
  public SurfaceTexture a;
  public int b;
  public d c;
  public int d;
  public boolean e = false;
  public boolean f = false;
  private final WeakReference<CameraGLView> g;
  private final float[] h = new float[16];
  private final float[] i = new float[16];
  private long j;
  private CameraGLView k;
  private a l;
  private volatile boolean m = false;
  private boolean n = true;
  private int o = 0;
  
  public b(CameraGLView paramCameraGLView)
  {
    this.g = new WeakReference(paramCameraGLView);
    Matrix.setIdentityM(this.i, 0);
    Matrix.rotateM(this.i, 0, 270.0F, 0.0F, 0.0F, 1.0F);
  }
  
  public final void onDrawFrame(GL10 paramGL10)
  {
    GLES20.glClear(16640);
    this.a.getTransformMatrix(this.h);
    a locala = this.l;
    float[] arrayOfFloat3 = this.h;
    GLES20.glUseProgram(locala.d);
    GLES20.glActiveTexture(33984);
    GLES20.glBindTexture(36197, locala.h);
    int i3 = GLES20.glGetAttribLocation(locala.d, "vPosition");
    GLES20.glEnableVertexAttribArray(i3);
    GLES20.glVertexAttribPointer(i3, 2, 5126, false, 8, locala.a);
    int i2 = GLES20.glGetAttribLocation(locala.d, "inputTextureCoordinate");
    GLES20.glEnableVertexAttribArray(i2);
    locala.b.clear();
    FloatBuffer localFloatBuffer = locala.b;
    float[] arrayOfFloat1 = a.g;
    float[] arrayOfFloat2 = new float[arrayOfFloat1.length];
    paramGL10 = new float[4];
    for (int i1 = 0; i1 < arrayOfFloat1.length; i1 += 2)
    {
      float f1 = arrayOfFloat1[i1];
      int i4 = i1 + 1;
      Matrix.multiplyMV(paramGL10, 0, arrayOfFloat3, 0, new float[] { f1, arrayOfFloat1[i4], 0.0F, 1.0F }, 0);
      arrayOfFloat2[i1] = paramGL10[0];
      arrayOfFloat2[i4] = arrayOfFloat1[i4];
    }
    localFloatBuffer.put(arrayOfFloat2);
    locala.b.position(0);
    GLES20.glVertexAttribPointer(i2, 2, 5126, false, 8, locala.b);
    GLES20.glDrawElements(4, locala.e.length, 5123, locala.c);
    GLES20.glDisableVertexAttribArray(i3);
    GLES20.glDisableVertexAttribArray(i2);
    this.a.updateTexImage();
    this.n ^= true;
    if (this.n) {
      try
      {
        if ((this.c != null) && (System.currentTimeMillis() - this.j >= 1000 / this.d) && (this.e) && (this.f))
        {
          this.e = false;
          this.j = System.currentTimeMillis();
          this.c.a(this.h, this.i);
        }
        return;
      }
      finally {}
    }
  }
  
  public final void onFrameAvailable(SurfaceTexture paramSurfaceTexture)
  {
    paramSurfaceTexture = this.k;
    if (paramSurfaceTexture != null) {
      paramSurfaceTexture.requestRender();
    }
  }
  
  public final void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
  {
    GLES20.glViewport(0, 0, paramInt1, paramInt2);
    paramGL10 = (CameraGLView)this.g.get();
    if (paramGL10 != null) {
      paramGL10.a();
    }
  }
  
  public final void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
  {
    if (GLES20.glGetString(7939).contains("OES_EGL_image_external"))
    {
      paramGL10 = new int[1];
      GLES20.glGenTextures(1, paramGL10, 0);
      GLES20.glBindTexture(36197, paramGL10[0]);
      GLES20.glTexParameterf(36197, 10241, 9729.0F);
      GLES20.glTexParameterf(36197, 10240, 9729.0F);
      GLES20.glTexParameteri(36197, 10242, 33071);
      GLES20.glTexParameteri(36197, 10243, 33071);
      this.b = paramGL10[0];
      this.a = new SurfaceTexture(this.b);
      this.a.setOnFrameAvailableListener(this);
      GLES20.glClearColor(1.0F, 1.0F, 0.0F, 1.0F);
      this.k = ((CameraGLView)this.g.get());
      paramGL10 = this.k;
      if (paramGL10 != null) {
        paramGL10.b = true;
      }
      this.l = new a(this.b);
      this.j = System.currentTimeMillis();
      return;
    }
    throw new RuntimeException("This system does not support OES_EGL_image_external.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/b/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */