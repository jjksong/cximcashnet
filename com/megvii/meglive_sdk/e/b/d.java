package com.megvii.meglive_sdk.e.b;

import android.opengl.GLES20;
import android.opengl.Matrix;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public final class d
{
  private static final float[] g = { 1.0F, 1.0F, -1.0F, 1.0F, 1.0F, -1.0F, -1.0F, -1.0F };
  private static final float[] h = { 1.0F, 1.0F, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F };
  int a;
  int b;
  int c;
  int d;
  int e;
  final float[] f = new float[16];
  private final FloatBuffer i = ByteBuffer.allocateDirect(32).order(ByteOrder.nativeOrder()).asFloatBuffer();
  private final FloatBuffer j;
  
  public d()
  {
    this.i.put(g);
    this.i.flip();
    this.j = ByteBuffer.allocateDirect(32).order(ByteOrder.nativeOrder()).asFloatBuffer();
    this.j.put(h);
    this.j.flip();
    int m = GLES20.glCreateShader(35633);
    GLES20.glShaderSource(m, "uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute highp vec4 aPosition;\nattribute highp vec4 aTextureCoord;\nvarying highp vec2 vTextureCoord;\n\nvoid main() {\n\tgl_Position = uMVPMatrix * aPosition;\n\tvTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n");
    GLES20.glCompileShader(m);
    int[] arrayOfInt = new int[1];
    GLES20.glGetShaderiv(m, 35713, arrayOfInt, 0);
    int k = m;
    if (arrayOfInt[0] == 0)
    {
      GLES20.glDeleteShader(m);
      k = 0;
    }
    int n = GLES20.glCreateShader(35632);
    GLES20.glShaderSource(n, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES sTexture;\nvarying highp vec2 vTextureCoord;\nvoid main() {\n  gl_FragColor = texture2D(sTexture, vTextureCoord);\n}");
    GLES20.glCompileShader(n);
    GLES20.glGetShaderiv(n, 35713, arrayOfInt, 0);
    m = n;
    if (arrayOfInt[0] == 0)
    {
      GLES20.glDeleteShader(n);
      m = 0;
    }
    n = GLES20.glCreateProgram();
    GLES20.glAttachShader(n, k);
    GLES20.glAttachShader(n, m);
    GLES20.glLinkProgram(n);
    this.a = n;
    GLES20.glUseProgram(this.a);
    this.b = GLES20.glGetAttribLocation(this.a, "aPosition");
    this.c = GLES20.glGetAttribLocation(this.a, "aTextureCoord");
    this.d = GLES20.glGetUniformLocation(this.a, "uMVPMatrix");
    this.e = GLES20.glGetUniformLocation(this.a, "uTexMatrix");
    Matrix.setIdentityM(this.f, 0);
    GLES20.glUniformMatrix4fv(this.d, 1, false, this.f, 0);
    GLES20.glUniformMatrix4fv(this.e, 1, false, this.f, 0);
    GLES20.glVertexAttribPointer(this.b, 2, 5126, false, 8, this.i);
    GLES20.glVertexAttribPointer(this.c, 2, 5126, false, 8, this.j);
    GLES20.glEnableVertexAttribArray(this.b);
    GLES20.glEnableVertexAttribArray(this.c);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/b/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */