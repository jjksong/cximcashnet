package com.megvii.meglive_sdk.e.a;

import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Process;
import java.nio.ByteBuffer;

public final class a
  extends b
{
  private static final int[] k = { 1, 0, 5, 7, 6 };
  private a j = null;
  
  public a(c paramc, b.a parama)
  {
    super(paramc, parama);
  }
  
  protected final void a()
  {
    this.f = -1;
    this.d = false;
    this.e = false;
    int n = MediaCodecList.getCodecCount();
    for (int i = 0; i < n; i++)
    {
      localObject = MediaCodecList.getCodecInfoAt(i);
      if (((MediaCodecInfo)localObject).isEncoder())
      {
        String[] arrayOfString = ((MediaCodecInfo)localObject).getSupportedTypes();
        for (int m = 0; m < arrayOfString.length; m++) {
          if (arrayOfString[m].equalsIgnoreCase("audio/mp4a-latm")) {
            break label86;
          }
        }
      }
    }
    Object localObject = null;
    label86:
    if (localObject == null) {
      return;
    }
    localObject = MediaFormat.createAudioFormat("audio/mp4a-latm", 44100, 1);
    ((MediaFormat)localObject).setInteger("aac-profile", 2);
    ((MediaFormat)localObject).setInteger("channel-mask", 16);
    ((MediaFormat)localObject).setInteger("bitrate", 64000);
    ((MediaFormat)localObject).setInteger("channel-count", 1);
    this.g = MediaCodec.createEncoderByType("audio/mp4a-latm");
    this.g.configure((MediaFormat)localObject, null, null, 1);
    this.g.start();
    if (this.i != null) {}
    try
    {
      this.i.a(this);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected final void b()
  {
    super.b();
    if (this.j == null)
    {
      this.j = new a((byte)0);
      this.j.start();
    }
  }
  
  protected final void c()
  {
    this.j = null;
    super.c();
  }
  
  private final class a
    extends Thread
  {
    private a() {}
    
    public final void run()
    {
      Process.setThreadPriority(-19);
      try
      {
        int j = AudioRecord.getMinBufferSize(44100, 16, 2);
        int i = 25600;
        if (25600 < j) {
          i = (j / 1024 + 1) * 1024 * 2;
        }
        int[] arrayOfInt = a.d();
        int k = arrayOfInt.length;
        AudioRecord localAudioRecord = null;
        Object localObject3;
        ByteBuffer localByteBuffer;
        for (j = 0;; j++)
        {
          localObject3 = localAudioRecord;
          if (j >= k) {
            break;
          }
          int m = arrayOfInt[j];
          try
          {
            localAudioRecord = new android/media/AudioRecord;
            localAudioRecord.<init>(m, 44100, 16, 2, i);
            m = localAudioRecord.getState();
            if (m != 1) {
              localAudioRecord = null;
            }
          }
          catch (Exception localException1)
          {
            localByteBuffer = null;
          }
          localObject3 = localByteBuffer;
          if (localByteBuffer != null) {
            break;
          }
        }
        if (localObject3 != null) {
          try
          {
            if (a.this.b)
            {
              localByteBuffer = ByteBuffer.allocateDirect(1024);
              ((AudioRecord)localObject3).startRecording();
              try
              {
                while ((a.this.b) && (!a.this.c) && (!a.this.d))
                {
                  localByteBuffer.clear();
                  i = ((AudioRecord)localObject3).read(localByteBuffer, 1024);
                  if (i > 0)
                  {
                    localByteBuffer.position(i);
                    localByteBuffer.flip();
                    a.this.a(localByteBuffer, i, a.this.h());
                    a.this.e();
                  }
                }
                a.this.e();
              }
              finally {}
            }
            return;
          }
          finally
          {
            ((AudioRecord)localObject3).release();
          }
        }
        return;
      }
      catch (Exception localException2) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */