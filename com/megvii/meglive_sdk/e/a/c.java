package com.megvii.meglive_sdk.e.a;

import android.content.Context;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Environment;
import java.io.File;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Locale;

public final class c
{
  private static final SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);
  public String a;
  int b;
  b c;
  b d;
  private final MediaMuxer f;
  private int g;
  private boolean h;
  
  public c(Context paramContext)
  {
    try
    {
      Object localObject = Environment.DIRECTORY_MOVIES;
      localObject = paramContext.getExternalFilesDir("megviiVideo");
      if (((File)localObject).exists()) {
        ((File)localObject).delete();
      }
      ((File)localObject).mkdirs();
      if (((File)localObject).canWrite())
      {
        paramContext = new java/io/File;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("meglive_fmp_vedio");
        localStringBuilder.append(".mp4");
        paramContext.<init>((File)localObject, localStringBuilder.toString());
      }
      else
      {
        paramContext = null;
      }
      this.a = paramContext.toString();
      this.f = new MediaMuxer(this.a, 0);
      this.g = 0;
      this.b = 0;
      this.h = false;
      return;
    }
    catch (NullPointerException paramContext)
    {
      throw new RuntimeException("This app has no permission of writing external storage");
    }
  }
  
  final int a(MediaFormat paramMediaFormat)
  {
    try
    {
      if (!this.h)
      {
        int i = this.f.addTrack(paramMediaFormat);
        return i;
      }
      paramMediaFormat = new java/lang/IllegalStateException;
      paramMediaFormat.<init>("muxer already started");
      throw paramMediaFormat;
    }
    finally {}
  }
  
  public final void a()
  {
    b localb = this.c;
    if (localb != null) {
      localb.a();
    }
    localb = this.d;
    if (localb != null) {
      localb.a();
    }
  }
  
  final void a(int paramInt, ByteBuffer paramByteBuffer, MediaCodec.BufferInfo paramBufferInfo)
  {
    try
    {
      if (this.g > 0) {
        this.f.writeSampleData(paramInt, paramByteBuffer, paramBufferInfo);
      }
      return;
    }
    finally
    {
      paramByteBuffer = finally;
      throw paramByteBuffer;
    }
  }
  
  public final void b()
  {
    b localb = this.c;
    if (localb != null) {
      localb.b();
    }
    localb = this.d;
    if (localb != null) {
      localb.b();
    }
  }
  
  public final void c()
  {
    b localb = this.c;
    if (localb != null) {
      localb.f();
    }
    this.c = null;
    localb = this.d;
    if (localb != null) {
      localb.f();
    }
    this.d = null;
  }
  
  public final boolean d()
  {
    try
    {
      boolean bool = this.h;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final boolean e()
  {
    try
    {
      this.g += 1;
      if ((this.b > 0) && (this.g == this.b))
      {
        this.f.start();
        this.h = true;
        notifyAll();
      }
      boolean bool = this.h;
      return bool;
    }
    finally {}
  }
  
  /* Error */
  final void f()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield 102	com/megvii/meglive_sdk/e/a/c:g	I
    //   7: iconst_1
    //   8: isub
    //   9: putfield 102	com/megvii/meglive_sdk/e/a/c:g	I
    //   12: aload_0
    //   13: getfield 104	com/megvii/meglive_sdk/e/a/c:b	I
    //   16: ifle +29 -> 45
    //   19: aload_0
    //   20: getfield 102	com/megvii/meglive_sdk/e/a/c:g	I
    //   23: ifgt +22 -> 45
    //   26: aload_0
    //   27: getfield 100	com/megvii/meglive_sdk/e/a/c:f	Landroid/media/MediaMuxer;
    //   30: invokevirtual 147	android/media/MediaMuxer:stop	()V
    //   33: aload_0
    //   34: getfield 100	com/megvii/meglive_sdk/e/a/c:f	Landroid/media/MediaMuxer;
    //   37: invokevirtual 150	android/media/MediaMuxer:release	()V
    //   40: aload_0
    //   41: iconst_0
    //   42: putfield 106	com/megvii/meglive_sdk/e/a/c:h	Z
    //   45: aload_0
    //   46: monitorexit
    //   47: return
    //   48: astore_1
    //   49: goto +11 -> 60
    //   52: astore_1
    //   53: aload_1
    //   54: invokevirtual 153	java/lang/Exception:printStackTrace	()V
    //   57: aload_0
    //   58: monitorexit
    //   59: return
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	c
    //   48	1	1	localObject	Object
    //   52	11	1	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   2	45	48	finally
    //   53	57	48	finally
    //   2	45	52	java/lang/Exception
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */