package com.megvii.meglive_sdk.e.a;

import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

public abstract class b
  implements Runnable
{
  protected final Object a = new Object();
  protected volatile boolean b;
  protected volatile boolean c;
  protected boolean d;
  protected boolean e;
  protected int f;
  protected MediaCodec g;
  protected final WeakReference<c> h;
  protected final a i;
  private int j;
  private MediaCodec.BufferInfo k;
  private long l = 0L;
  
  public b(c arg1, a parama)
  {
    if (parama != null) {
      if (??? != null)
      {
        this.h = new WeakReference(???);
        if ((this instanceof d))
        {
          if (???.c == null) {
            ???.c = this;
          } else {
            throw new IllegalArgumentException("Video encoder already added.");
          }
        }
        else
        {
          if (!(this instanceof a)) {
            break label203;
          }
          if (???.d != null) {
            break label193;
          }
          ???.d = this;
        }
        b localb = ???.c;
        int n = 1;
        int m;
        if (localb != null) {
          m = 1;
        } else {
          m = 0;
        }
        if (???.d == null) {
          n = 0;
        }
        ???.b = (m + n);
        this.i = parama;
        synchronized (this.a)
        {
          parama = new android/media/MediaCodec$BufferInfo;
          parama.<init>();
          this.k = parama;
          parama = new java/lang/Thread;
          parama.<init>(this, getClass().getSimpleName());
          parama.start();
        }
      }
    }
    try
    {
      this.a.wait();
      return;
      parama = finally;
      throw parama;
      label193:
      throw new IllegalArgumentException("Video encoder already added.");
      label203:
      throw new IllegalArgumentException("unsupported encoder");
      throw new NullPointerException("MediaMuxerWrapper is null");
      throw new NullPointerException("MediaEncoderListener is null");
    }
    catch (InterruptedException parama)
    {
      for (;;) {}
    }
  }
  
  private void d()
  {
    Object localObject1 = this.g;
    if (localObject1 == null) {
      return;
    }
    localObject1 = ((MediaCodec)localObject1).getOutputBuffers();
    c localc = (c)this.h.get();
    if (localc == null) {
      return;
    }
    int m = 0;
    while (this.b)
    {
      int n = this.g.dequeueOutputBuffer(this.k, 10000L);
      if (n == -1)
      {
        if (!this.d)
        {
          n = m + 1;
          m = n;
          if (n <= 5) {
            break;
          }
        }
      }
      else if (n == -3)
      {
        localObject1 = this.g.getOutputBuffers();
      }
      else if (n == -2)
      {
        if (!this.e)
        {
          this.f = localc.a(this.g.getOutputFormat());
          this.e = true;
          if (!localc.e()) {
            try
            {
              for (;;)
              {
                boolean bool = localc.d();
                if (!bool) {
                  try
                  {
                    localc.wait(100L);
                  }
                  catch (InterruptedException localInterruptedException)
                  {
                    return;
                  }
                }
              }
              continue;
            }
            finally {}
          }
        }
        else
        {
          throw new RuntimeException("format changed twice");
        }
      }
      else if (n >= 0)
      {
        ByteBuffer localByteBuffer = localObject2[n];
        if (localByteBuffer != null)
        {
          if ((this.k.flags & 0x2) != 0) {
            this.k.size = 0;
          }
          if (this.k.size != 0) {
            if (this.e)
            {
              this.k.presentationTimeUs = h();
              localc.a(this.f, localByteBuffer, this.k);
              this.l = this.k.presentationTimeUs;
              m = 0;
            }
            else
            {
              throw new RuntimeException("drain:muxer hasn't started");
            }
          }
          this.g.releaseOutputBuffer(n, false);
          if ((this.k.flags & 0x4) != 0) {
            this.b = false;
          }
        }
        else
        {
          StringBuilder localStringBuilder = new StringBuilder("encoderOutputBuffer ");
          localStringBuilder.append(n);
          localStringBuilder.append(" was null");
          throw new RuntimeException(localStringBuilder.toString());
        }
      }
    }
  }
  
  abstract void a();
  
  protected final void a(ByteBuffer paramByteBuffer, int paramInt, long paramLong)
  {
    if (!this.b) {
      return;
    }
    Object localObject = this.g.getInputBuffers();
    while (this.b)
    {
      int m = this.g.dequeueInputBuffer(10000L);
      if (m >= 0)
      {
        localObject = localObject[m];
        ((ByteBuffer)localObject).clear();
        if (paramByteBuffer != null) {
          ((ByteBuffer)localObject).put(paramByteBuffer);
        }
        if (paramInt <= 0)
        {
          this.d = true;
          this.g.queueInputBuffer(m, 0, 0, paramLong, 4);
          return;
        }
        this.g.queueInputBuffer(m, 0, paramInt, paramLong, 0);
        return;
      }
    }
  }
  
  void b()
  {
    synchronized (this.a)
    {
      this.b = true;
      this.c = false;
      this.a.notifyAll();
      return;
    }
  }
  
  protected void c()
  {
    try
    {
      this.i.b(this);
      this.b = false;
      MediaCodec localMediaCodec = this.g;
      if (localMediaCodec != null) {
        try
        {
          localMediaCodec.stop();
          this.g.release();
          this.g = null;
        }
        catch (Exception localException1) {}
      }
      if (this.e)
      {
        localObject = this.h;
        if (localObject != null) {
          localObject = (c)((WeakReference)localObject).get();
        } else {
          localObject = null;
        }
        if (localObject == null) {}
      }
    }
    catch (Exception localException2)
    {
      try
      {
        Object localObject;
        ((c)localObject).f();
        this.k = null;
        this.i.c(this);
        return;
        localException2 = localException2;
      }
      catch (Exception localException3)
      {
        for (;;) {}
      }
    }
  }
  
  public boolean e()
  {
    synchronized (this.a)
    {
      if ((this.b) && (!this.c))
      {
        this.j += 1;
        this.a.notifyAll();
        return true;
      }
      return false;
    }
  }
  
  final void f()
  {
    synchronized (this.a)
    {
      if ((this.b) && (!this.c))
      {
        this.c = true;
        this.a.notifyAll();
        return;
      }
      return;
    }
  }
  
  protected void g()
  {
    a(null, 0, h());
  }
  
  protected final long h()
  {
    long l2 = System.nanoTime() / 1000L;
    long l3 = this.l;
    long l1 = l2;
    if (l2 < l3) {
      l1 = l2 + (l3 - l2);
    }
    return l1;
  }
  
  public void run()
  {
    synchronized (this.a)
    {
      this.c = false;
      this.j = 0;
      this.a.notify();
      synchronized (this.a)
      {
        for (;;)
        {
          boolean bool = this.c;
          int m;
          if (this.j > 0) {
            m = 1;
          } else {
            m = 0;
          }
          if (m != 0) {
            this.j -= 1;
          }
          if (bool)
          {
            d();
            g();
            d();
            c();
            break label130;
          }
          if (m == 0) {
            break;
          }
          d();
        }
        try
        {
          synchronized (this.a)
          {
            this.a.wait();
          }
          synchronized (this.a)
          {
            this.c = true;
            this.b = false;
            return;
          }
        }
        catch (InterruptedException localInterruptedException) {}
        label130:
        throw ((Throwable)localObject3);
      }
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(b paramb);
    
    public abstract void b(b paramb);
    
    public abstract void c(b paramb);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */