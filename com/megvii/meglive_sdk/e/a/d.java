package com.megvii.meglive_sdk.e.a;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.view.Surface;
import com.megvii.meglive_sdk.e.b.e;

public final class d
  extends b
{
  protected static int[] l = { 2130708361 };
  public e j;
  public Surface k;
  private final int m;
  private final int n;
  
  public d(c paramc, b.a parama, int paramInt1, int paramInt2)
  {
    super(paramc, parama);
    this.m = paramInt1;
    this.n = paramInt2;
    this.j = e.a("MediaVideoEncoder");
  }
  
  private static int a(MediaCodecInfo paramMediaCodecInfo, String paramString)
  {
    try
    {
      Thread.currentThread().setPriority(10);
      paramString = paramMediaCodecInfo.getCapabilitiesForType(paramString);
      Thread.currentThread().setPriority(5);
      int i3 = 0;
      int i1;
      for (int i = 0;; i++)
      {
        i1 = i3;
        if (i >= paramString.colorFormats.length) {
          break;
        }
        int i4 = paramString.colorFormats[i];
        paramMediaCodecInfo = l;
        if (paramMediaCodecInfo != null) {
          i1 = paramMediaCodecInfo.length;
        } else {
          i1 = 0;
        }
        for (int i2 = 0; i2 < i1; i2++) {
          if (l[i2] == i4)
          {
            i1 = 1;
            break label95;
          }
        }
        i1 = 0;
        label95:
        if (i1 != 0)
        {
          i1 = i4;
          break;
        }
      }
      return i1;
    }
    finally
    {
      Thread.currentThread().setPriority(5);
    }
  }
  
  protected final void a()
  {
    this.f = -1;
    this.d = false;
    this.e = false;
    int i2 = MediaCodecList.getCodecCount();
    for (int i = 0; i < i2; i++)
    {
      localObject = MediaCodecList.getCodecInfoAt(i);
      if (((MediaCodecInfo)localObject).isEncoder())
      {
        String[] arrayOfString = ((MediaCodecInfo)localObject).getSupportedTypes();
        for (int i1 = 0; i1 < arrayOfString.length; i1++) {
          if ((arrayOfString[i1].equalsIgnoreCase("video/avc")) && (a((MediaCodecInfo)localObject, "video/avc") > 0)) {
            break label96;
          }
        }
      }
    }
    Object localObject = null;
    label96:
    if (localObject == null) {
      return;
    }
    localObject = MediaFormat.createVideoFormat("video/avc", this.m, this.n);
    ((MediaFormat)localObject).setInteger("color-format", 2130708361);
    ((MediaFormat)localObject).setInteger("bitrate", (int)(this.m * 13.333334F * this.n) / 2);
    ((MediaFormat)localObject).setInteger("frame-rate", 25);
    ((MediaFormat)localObject).setInteger("i-frame-interval", 10);
    this.g = MediaCodec.createEncoderByType("video/avc");
    this.g.configure((MediaFormat)localObject, null, null, 1);
    this.k = this.g.createInputSurface();
    this.g.start();
    if (this.i != null) {}
    try
    {
      this.i.a(this);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public final boolean a(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    boolean bool = super.e();
    if (bool)
    {
      e locale = this.j;
      locale.a(locale.e, paramArrayOfFloat1, paramArrayOfFloat2);
    }
    return bool;
  }
  
  protected final void c()
  {
    ??? = this.k;
    if (??? != null)
    {
      ((Surface)???).release();
      this.k = null;
    }
    e locale = this.j;
    if (locale != null) {
      synchronized (locale.a)
      {
        if (!locale.h)
        {
          locale.h = true;
          locale.a.notifyAll();
        }
      }
    }
    try
    {
      locale.a.wait();
      this.j = null;
      break label80;
      localObject2 = finally;
      throw ((Throwable)localObject2);
      label80:
      super.c();
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  public final boolean e()
  {
    boolean bool = super.e();
    if (bool)
    {
      e locale = this.j;
      locale.a(locale.e, null, null);
    }
    return bool;
  }
  
  protected final void g()
  {
    this.g.signalEndOfInputStream();
    this.d = true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */