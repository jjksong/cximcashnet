package com.megvii.meglive_sdk.e.c;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build.VERSION;
import android.os.Environment;
import java.io.File;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;

public final class a
{
  private static final SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);
  public b a;
  public a b;
  public String c;
  private int d = 12000;
  private File f;
  private Context g;
  private MediaMuxer h;
  private int i = -1;
  private int j = -1;
  
  public a(Context paramContext)
  {
    try
    {
      this.g = paramContext;
      try
      {
        Object localObject = Environment.DIRECTORY_MOVIES;
        this.f = paramContext.getExternalFilesDir("megviiVideo");
        if (this.f.exists()) {
          this.f.delete();
        }
        this.f.mkdirs();
        if (this.f.canWrite())
        {
          paramContext = new java/io/File;
          File localFile = this.f;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("meglive_fmp_vedio");
          ((StringBuilder)localObject).append(".mp4");
          paramContext.<init>(localFile, ((StringBuilder)localObject).toString());
        }
        else
        {
          paramContext = null;
        }
        this.c = paramContext.toString();
        paramContext = new android/media/MediaMuxer;
        paramContext.<init>(this.c, 0);
        this.h = paramContext;
        return;
      }
      catch (NullPointerException paramContext)
      {
        paramContext = new java/lang/RuntimeException;
        paramContext.<init>("This app has no permission of writing external storage");
        throw paramContext;
      }
      return;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  private void a(MediaFormat paramMediaFormat)
  {
    try
    {
      this.i = this.h.addTrack(paramMediaFormat);
      if ((this.a != null) && (this.b != null)) {
        if (this.i != -1)
        {
          int k = this.j;
          if (k != -1) {}
        }
        else
        {
          return;
        }
      }
      this.h.start();
      return;
    }
    finally {}
  }
  
  private void a(ByteBuffer paramByteBuffer, MediaCodec.BufferInfo paramBufferInfo)
  {
    try
    {
      int k = this.i;
      this.h.writeSampleData(k, paramByteBuffer, paramBufferInfo);
      return;
    }
    finally
    {
      paramByteBuffer = finally;
      throw paramByteBuffer;
    }
  }
  
  public static boolean a()
  {
    if (Build.VERSION.SDK_INT >= 18) {
      for (int k = MediaCodecList.getCodecCount() - 1; k >= 0; k--)
      {
        String[] arrayOfString = MediaCodecList.getCodecInfoAt(k).getSupportedTypes();
        for (int m = 0; m < arrayOfString.length; m++) {
          if (arrayOfString[m].equalsIgnoreCase("video/avc")) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public final class a
  {
    public boolean a;
    public boolean b;
    public MediaCodec c;
  }
  
  public final class b
  {
    public MediaCodec a;
    int b = 480;
    int c = 640;
    int d = 12;
    long e = 0L;
    public ArrayBlockingQueue<byte[]> f = new ArrayBlockingQueue(10);
    public boolean g = true;
    
    public b()
    {
      this$1 = MediaFormat.createVideoFormat("video/avc", 480, 640);
      a.this.setInteger("color-format", 21);
      double d1 = this.d;
      Double.isNaN(d1);
      double d2 = this.b;
      Double.isNaN(d2);
      double d3 = this.c;
      Double.isNaN(d3);
      a.this.setInteger("bitrate", (int)(d1 * 0.25D * d2 * d3));
      a.this.setInteger("frame-rate", this.d);
      a.this.setInteger("i-frame-interval", 10);
      try
      {
        this.a = MediaCodec.createEncoderByType("video/avc");
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
      this.a.configure(a.this, null, null, 1);
      this.a.start();
      this.g = true;
      this.e = 0L;
      new Thread(new a()).start();
    }
    
    final class a
      implements Runnable
    {
      a() {}
      
      public final void run()
      {
        for (;;)
        {
          if (a.b.this.g) {
            if (a.b.this.f.size() > 0)
            {
              Object localObject2 = (byte[])a.b.this.f.poll();
              Object localObject1 = new byte[a.b.this.b * a.b.this.c * 3 / 2];
              int j = a.b.this.b;
              int i = a.b.this.c;
              if (localObject2 != null)
              {
                j *= i;
                System.arraycopy(localObject2, 0, localObject1, 0, j);
                for (i = j; i < j / 2 + j; i += 2)
                {
                  int k = i + 1;
                  localObject1[i] = localObject2[k];
                  localObject1[k] = localObject2[i];
                }
              }
              try
              {
                Object localObject3 = a.b.this.a.getInputBuffers();
                i = a.b.this.a.dequeueInputBuffer(-1L);
                if (i >= 0)
                {
                  localObject2 = a.b.this;
                  long l = a.b.this.e * 1000000L / ((a.b)localObject2).d;
                  localObject2 = localObject3[i];
                  ((ByteBuffer)localObject2).clear();
                  ((ByteBuffer)localObject2).put((byte[])localObject1);
                  a.b.this.a.queueInputBuffer(i, 0, localObject1.length, l + 132L, 0);
                  a.b.this.e += 1L;
                }
                localObject3 = new android/media/MediaCodec$BufferInfo;
                ((MediaCodec.BufferInfo)localObject3).<init>();
                i = a.b.this.a.dequeueOutputBuffer((MediaCodec.BufferInfo)localObject3, a.b(a.this));
                if (i == -2)
                {
                  localObject1 = a.b.this.a.getOutputFormat();
                  a.a(a.this, (MediaFormat)localObject1);
                }
                else
                {
                  localObject1 = a.b.this.a.getOutputBuffers();
                  while (i >= 0)
                  {
                    localObject2 = localObject1[i];
                    if ((((MediaCodec.BufferInfo)localObject3).flags & 0x2) != 0) {
                      ((MediaCodec.BufferInfo)localObject3).size = 0;
                    }
                    if (((MediaCodec.BufferInfo)localObject3).size != 0)
                    {
                      ((ByteBuffer)localObject2).position(((MediaCodec.BufferInfo)localObject3).offset);
                      ((ByteBuffer)localObject2).limit(((MediaCodec.BufferInfo)localObject3).offset + ((MediaCodec.BufferInfo)localObject3).size);
                      a.a(a.this, (ByteBuffer)localObject2, (MediaCodec.BufferInfo)localObject3);
                    }
                    a.b.this.a.releaseOutputBuffer(i, false);
                    i = a.b.this.a.dequeueOutputBuffer((MediaCodec.BufferInfo)localObject3, a.b(a.this));
                  }
                }
              }
              catch (Throwable localThrowable)
              {
                localThrowable.printStackTrace();
              }
            }
          }
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/e/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */