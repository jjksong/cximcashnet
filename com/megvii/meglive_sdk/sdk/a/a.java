package com.megvii.meglive_sdk.sdk.a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import java.util.Locale;

public final class a
{
  public static void a(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (paramString != null))
    {
      Resources localResources = paramContext.getResources();
      DisplayMetrics localDisplayMetrics = localResources.getDisplayMetrics();
      paramContext = localResources.getConfiguration();
      paramString = new Locale(paramString);
      if (Build.VERSION.SDK_INT >= 17) {
        paramContext.setLocale(paramString);
      } else {
        paramContext.locale = paramString;
      }
      localResources.updateConfiguration(paramContext, localDisplayMetrics);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */