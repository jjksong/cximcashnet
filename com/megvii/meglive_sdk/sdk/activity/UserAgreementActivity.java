package com.megvii.meglive_sdk.sdk.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;
import com.megvii.meglive_sdk.R.string;
import com.megvii.meglive_sdk.b.d;
import com.megvii.meglive_sdk.i.e;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.s;
import com.megvii.meglive_sdk.i.u;
import com.megvii.meglive_sdk.sdk.view.SlidingLayout;

public class UserAgreementActivity
  extends Activity
{
  private TextView a;
  private WebView b;
  private LinearLayout c;
  private TextView d;
  private int e;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    new WebView(this).destroy();
    com.megvii.meglive_sdk.sdk.a.a.a(this, e.d(this));
    requestWindowFeature(1);
    setContentView(R.layout.user_agreement);
    SlidingLayout localSlidingLayout = new SlidingLayout(this);
    localSlidingLayout.a = this;
    paramBundle = (ViewGroup)localSlidingLayout.a.getWindow().getDecorView();
    Object localObject = paramBundle.getChildAt(0);
    paramBundle.removeView((View)localObject);
    localSlidingLayout.addView((View)localObject);
    paramBundle.addView(localSlidingLayout);
    this.a = ((TextView)findViewById(R.id.tv_verify_title));
    this.a.setText(R.string.agreement_title);
    this.c = ((LinearLayout)findViewById(R.id.ll_bar_left));
    this.c.setOnClickListener(new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        UserAgreementActivity.this.finish();
      }
    });
    this.d = ((TextView)findViewById(R.id.tv_bar_title));
    this.d.setOnClickListener(new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        UserAgreementActivity.this.finish();
      }
    });
    this.b = ((WebView)findViewById(R.id.web_agreement));
    paramBundle = (String)u.b(this, "agreeUrl", "");
    this.e = e.b(getApplicationContext()).a;
    localObject = new StringBuilder("agreeUrl = ");
    ((StringBuilder)localObject).append(paramBundle);
    k.b("UserAgreementActivity", ((StringBuilder)localObject).toString());
    if (paramBundle != null)
    {
      this.b.loadUrl(paramBundle);
      s.a(com.megvii.meglive_sdk.b.a.a("enter_agreement", e.a(this), this.e));
      return;
    }
    this.b.loadUrl("http://meglivesdk.oss-cn-hangzhou.aliyuncs.com/Agreement/MegLiveV3/Release/agreement.html");
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/activity/UserAgreementActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */