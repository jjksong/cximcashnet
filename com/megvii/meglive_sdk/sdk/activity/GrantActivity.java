package com.megvii.meglive_sdk.sdk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.megvii.meglive_sdk.R.color;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;
import com.megvii.meglive_sdk.R.string;
import com.megvii.meglive_sdk.activity.ActionLivenessActivity;
import com.megvii.meglive_sdk.activity.FmpActivity;
import com.megvii.meglive_sdk.i.e;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.q;
import com.megvii.meglive_sdk.i.r;
import com.megvii.meglive_sdk.i.s;
import com.megvii.meglive_sdk.i.v;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GrantActivity
  extends Activity
  implements View.OnClickListener
{
  private Button a;
  private CheckBox b;
  private LinearLayout c;
  private TextView d;
  private LinearLayout e;
  private TextView f;
  private LinearLayout g;
  private TextView h;
  private TextView i;
  private RelativeLayout j;
  private RelativeLayout k;
  private ImageView l;
  private String m = "";
  private GLSurfaceView n;
  private v o;
  private String p;
  private boolean q = false;
  private int r;
  private int s;
  private String t;
  private String u;
  
  private Drawable a(int paramInt)
  {
    int i1 = r.a(this, 40.0F);
    Object localObject1 = new float[8];
    float f1 = i1;
    localObject1[0] = f1;
    localObject1[1] = f1;
    localObject1[2] = f1;
    localObject1[3] = f1;
    localObject1[4] = f1;
    localObject1[5] = f1;
    localObject1[6] = f1;
    localObject1[7] = f1;
    Object localObject3 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject5 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject7 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject9 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject12 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject11 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject10 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject8 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject6 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject4 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject2 = new RoundRectShape((float[])localObject1, null, null);
    RoundRectShape localRoundRectShape = new RoundRectShape((float[])localObject1, null, null);
    localObject1 = new ShapeDrawable();
    ((ShapeDrawable)localObject1).setShape((Shape)localObject3);
    ((ShapeDrawable)localObject1).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject1).getPaint().setColor(Color.parseColor("#05E0E3E5"));
    localObject3 = new ShapeDrawable();
    ((ShapeDrawable)localObject3).setShape((Shape)localObject5);
    ((ShapeDrawable)localObject3).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject3).getPaint().setColor(Color.parseColor("#1FE0E3E5"));
    localObject5 = new ShapeDrawable();
    ((ShapeDrawable)localObject5).setShape((Shape)localObject7);
    ((ShapeDrawable)localObject5).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject5).getPaint().setColor(Color.parseColor("#42E0E3E5"));
    localObject7 = new ShapeDrawable();
    ((ShapeDrawable)localObject7).setShape((Shape)localObject9);
    ((ShapeDrawable)localObject7).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject7).getPaint().setColor(Color.parseColor("#61E0E3E5"));
    localObject9 = new ShapeDrawable();
    ((ShapeDrawable)localObject9).setShape((Shape)localObject12);
    ((ShapeDrawable)localObject9).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject9).getPaint().setColor(Color.parseColor("#7AE0E3E5"));
    localObject12 = new ShapeDrawable();
    ((ShapeDrawable)localObject12).setShape((Shape)localObject11);
    ((ShapeDrawable)localObject12).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject12).getPaint().setColor(Color.parseColor("#8AE0E3E5"));
    localObject11 = new ShapeDrawable();
    ((ShapeDrawable)localObject11).setShape((Shape)localObject10);
    ((ShapeDrawable)localObject11).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject11).getPaint().setColor(Color.parseColor("#A3E0E3E5"));
    localObject10 = new ShapeDrawable();
    ((ShapeDrawable)localObject10).setShape((Shape)localObject8);
    ((ShapeDrawable)localObject10).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject10).getPaint().setColor(Color.parseColor("#B8E0E3E5"));
    localObject8 = new ShapeDrawable();
    ((ShapeDrawable)localObject8).setShape((Shape)localObject6);
    ((ShapeDrawable)localObject8).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject8).getPaint().setColor(Color.parseColor("#CCE0E3E5"));
    localObject6 = new ShapeDrawable();
    ((ShapeDrawable)localObject6).setShape((Shape)localObject4);
    ((ShapeDrawable)localObject6).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject6).getPaint().setColor(Color.parseColor("#E0E0E3E5"));
    localObject4 = new ShapeDrawable();
    ((ShapeDrawable)localObject4).setShape((Shape)localObject2);
    ((ShapeDrawable)localObject4).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject4).getPaint().setColor(Color.parseColor("#F5E0E3E5"));
    localObject2 = new ShapeDrawable();
    ((ShapeDrawable)localObject2).setShape(localRoundRectShape);
    ((ShapeDrawable)localObject2).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject2).getPaint().setColor(paramInt);
    localObject1 = new LayerDrawable(new Drawable[] { localObject1, localObject3, localObject5, localObject7, localObject9, localObject12, localObject11, localObject10, localObject8, localObject6, localObject4, localObject2 });
    ((LayerDrawable)localObject1).setLayerInset(0, 0, 0, 0, 0);
    ((LayerDrawable)localObject1).setLayerInset(1, 0, 0, 0, r.a(this, 1.0F));
    ((LayerDrawable)localObject1).setLayerInset(2, 0, 0, 0, r.a(this, 2.0F));
    ((LayerDrawable)localObject1).setLayerInset(3, 0, 0, 0, r.a(this, 3.0F));
    ((LayerDrawable)localObject1).setLayerInset(4, 0, 0, 0, r.a(this, 4.0F));
    ((LayerDrawable)localObject1).setLayerInset(5, 0, 0, 0, r.a(this, 5.0F));
    ((LayerDrawable)localObject1).setLayerInset(6, 0, 0, 0, r.a(this, 6.0F));
    ((LayerDrawable)localObject1).setLayerInset(7, 0, 0, 0, r.a(this, 7.0F));
    ((LayerDrawable)localObject1).setLayerInset(8, 0, 0, 0, r.a(this, 8.0F));
    ((LayerDrawable)localObject1).setLayerInset(9, 0, 0, 0, r.a(this, 9.0F));
    ((LayerDrawable)localObject1).setLayerInset(10, 0, 0, 0, r.a(this, 10.0F));
    ((LayerDrawable)localObject1).setLayerInset(11, 0, 0, 0, r.a(this, 11.0F));
    return (Drawable)localObject1;
  }
  
  private void a()
  {
    h localh = h.j;
    com.megvii.meglive_sdk.g.a.a().a(localh.q, localh.r);
    finish();
    finish();
  }
  
  public static void a(Context paramContext, boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2)
  {
    Intent localIntent = new Intent(paramContext, GrantActivity.class);
    localIntent.addFlags(268435456);
    localIntent.putExtra("protocol_status", paramBoolean);
    localIntent.putExtra("liveness_type", paramInt1);
    localIntent.putExtra("verticalDetection", paramInt2);
    localIntent.putExtra("logoFileName", paramString1);
    localIntent.putExtra("language", paramString2);
    paramContext.startActivity(localIntent);
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    if (i1 == R.id.begin_detect)
    {
      if (this.a.isActivated())
      {
        i1 = this.r;
        if (i1 == 1)
        {
          paramView = new Intent(this, FmpActivity.class);
          paramView.putExtra("verticalCheckType", this.s);
          paramView.putExtra("logoFileName", this.t);
          paramView.putExtra("language", this.u);
          paramView.addFlags(268435456);
          startActivity(paramView);
        }
        else if (i1 == 2)
        {
          paramView = new Intent(this, ActionLivenessActivity.class);
          paramView.putExtra("verticalCheckType", this.s);
          paramView.putExtra("logoFileName", this.t);
          paramView.putExtra("language", this.u);
          paramView.addFlags(268435456);
          startActivity(paramView);
        }
        else
        {
          com.megvii.meglive_sdk.g.a.a().a(h.b);
        }
        finish();
        return;
      }
      paramView = this.o;
      i1 = R.layout.agreement_toast;
      if (paramView.a == null) {
        paramView.a = v.a(this, i1);
      }
      paramView.a.show();
      return;
    }
    if (i1 == R.id.linearlayout_checkbox_hot_area)
    {
      if ((this.b.isChecked() ^ true))
      {
        this.a.setActivated(true);
        this.b.setChecked(true);
        return;
      }
      this.a.setActivated(false);
      this.b.setChecked(false);
      return;
    }
    if ((i1 == R.id.ll_bar_left) || (i1 == R.id.tv_bar_title)) {
      a();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.megvii.meglive_sdk.sdk.a.a.a(this, e.d(this));
    setContentView(R.layout.grant_activity_new);
    this.q = getIntent().getBooleanExtra("protocol_status", false);
    this.r = getIntent().getIntExtra("liveness_type", 2);
    this.s = getIntent().getIntExtra("verticalDetection", 0);
    this.t = getIntent().getStringExtra("logoFileName");
    this.u = getIntent().getStringExtra("language");
    this.j = ((RelativeLayout)findViewById(R.id.main));
    this.b = ((CheckBox)findViewById(R.id.cb_user_agreement));
    this.a = ((Button)findViewById(R.id.begin_detect));
    this.a.setOnClickListener(this);
    paramBundle = new StateListDrawable();
    q.a(this);
    Object localObject1 = getResources().getString(R.string.key_liveness_detect_button_normal_bg_color);
    int i1 = q.a(q.e, (String)localObject1);
    int i3 = getResources().getColor(i1);
    int i2 = r.a(this, 40.0F);
    i1 = 8;
    localObject1 = new float[8];
    float f1 = i2;
    localObject1[0] = f1;
    localObject1[1] = f1;
    localObject1[2] = f1;
    localObject1[3] = f1;
    localObject1[4] = f1;
    localObject1[5] = f1;
    localObject1[6] = f1;
    localObject1[7] = f1;
    final Object localObject3 = new RoundRectShape((float[])localObject1, null, null);
    Object localObject2 = new RoundRectShape((float[])localObject1, null, null);
    localObject1 = new ShapeDrawable();
    ((ShapeDrawable)localObject1).setShape((Shape)localObject3);
    ((ShapeDrawable)localObject1).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject1).getPaint().setColor(i3);
    localObject3 = new ShapeDrawable();
    ((ShapeDrawable)localObject3).setShape((Shape)localObject2);
    ((ShapeDrawable)localObject3).getPaint().setStyle(Paint.Style.FILL);
    ((ShapeDrawable)localObject3).getPaint().setColor(Color.parseColor("#05E0E3E5"));
    localObject1 = new LayerDrawable(new Drawable[] { localObject3, localObject1 });
    ((LayerDrawable)localObject1).setLayerInset(0, 0, 0, 0, 0);
    ((LayerDrawable)localObject1).setLayerInset(1, 0, 0, 0, r.a(this, 11.0F));
    paramBundle.addState(new int[] { -16843518 }, (Drawable)localObject1);
    q.a(this);
    localObject1 = getResources().getString(R.string.key_liveness_detect_button_highlight_bg_color);
    i2 = q.a(q.e, (String)localObject1);
    localObject1 = a(getResources().getColor(i2));
    paramBundle.addState(new int[] { 16842919 }, (Drawable)localObject1);
    q.a(this);
    localObject1 = getResources().getString(R.string.key_liveness_detect_button_selected_bg_color);
    i2 = q.a(q.e, (String)localObject1);
    localObject1 = a(getResources().getColor(i2));
    paramBundle.addState(new int[0], (Drawable)localObject1);
    this.a.setBackground(paramBundle);
    localObject2 = this.a;
    localObject1 = getResources();
    q.a(this);
    paramBundle = getResources().getString(R.string.key_liveness_detect_button_text_color);
    ((Button)localObject2).setTextColor(((Resources)localObject1).getColor(q.a(q.e, paramBundle)));
    this.c = ((LinearLayout)findViewById(R.id.linearlayout_checkbox_hot_area));
    this.c.setOnClickListener(this);
    this.d = ((TextView)findViewById(R.id.tv_user_agreement));
    this.d.setHighlightColor(getResources().getColor(17170445));
    localObject1 = this.d;
    localObject2 = getResources();
    q.a(this);
    paramBundle = getResources().getString(R.string.key_liveness_guide_read_color);
    ((TextView)localObject1).setTextColor(((Resources)localObject2).getColor(q.a(q.e, paramBundle)));
    paramBundle = this.d;
    localObject1 = paramBundle.getText().toString();
    if (e.d(com.megvii.meglive_sdk.g.a.a().a).equals("en")) {
      i1 = 9;
    }
    localObject2 = new SpannableString((CharSequence)localObject1);
    localObject3 = new Intent();
    ((Intent)localObject3).setClass(this, UserAgreementActivity.class);
    ((SpannableString)localObject2).setSpan(new a(new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        GrantActivity.this.startActivity(localObject3);
      }
    }), i1, ((String)localObject1).length(), 33);
    paramBundle.setText((CharSequence)localObject2);
    paramBundle.setMovementMethod(LinkMovementMethod.getInstance());
    this.e = ((LinearLayout)findViewById(R.id.ll_bar_left));
    this.e.setOnClickListener(this);
    this.f = ((TextView)findViewById(R.id.tv_bar_title));
    this.f.setOnClickListener(this);
    this.h = ((TextView)findViewById(R.id.text2));
    localObject2 = this.h;
    paramBundle = getResources();
    q.a(this);
    localObject1 = getResources().getString(R.string.key_liveness_guide_remindtext_color);
    ((TextView)localObject2).setTextColor(paramBundle.getColor(q.a(q.e, (String)localObject1)));
    this.i = ((TextView)findViewById(R.id.tv_verify_title));
    this.i.setText(getResources().getString(R.string.grant_title));
    this.k = ((RelativeLayout)findViewById(R.id.rl_phone));
    this.l = ((ImageView)findViewById(R.id.image));
    localObject1 = this.l;
    paramBundle = getResources();
    q.a(this);
    localObject2 = getResources().getString(R.string.key_agreement_image_center);
    ((ImageView)localObject1).setImageDrawable(paramBundle.getDrawable(q.a(q.a, (String)localObject2)));
    this.a.setActivated(this.q);
    this.b.setChecked(this.q);
    this.g = ((LinearLayout)findViewById(R.id.linearlayout_agreement));
    this.o = new v(this);
    this.p = e.a(this);
    if (Build.VERSION.SDK_INT >= 18)
    {
      this.n = new GLSurfaceView(this);
      this.n.setRenderer(new b());
      ((RelativeLayout)findViewById(R.id.main)).addView(this.n);
      f1 = TypedValue.applyDimension(5, 0.1F, getResources().getDisplayMetrics());
      paramBundle = this.n.getLayoutParams();
      i1 = (int)f1;
      paramBundle.width = i1;
      paramBundle.height = i1;
      this.n.setLayoutParams(paramBundle);
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.o = null;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      a();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  protected void onResume()
  {
    super.onResume();
    this.a.post(new Runnable()
    {
      public final void run()
      {
        int i = GrantActivity.a(GrantActivity.this).getBottom();
        if (GrantActivity.b(GrantActivity.this).getTop() < i)
        {
          RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)GrantActivity.c(GrantActivity.this).getLayoutParams();
          localLayoutParams.addRule(3, R.id.text0);
          localLayoutParams.topMargin = r.a(GrantActivity.this, 10.0F);
          GrantActivity.c(GrantActivity.this).setLayoutParams(localLayoutParams);
          localLayoutParams = (RelativeLayout.LayoutParams)GrantActivity.d(GrantActivity.this).getLayoutParams();
          localLayoutParams.width = ((int)(localLayoutParams.width * 0.9F));
          localLayoutParams.height = ((int)(localLayoutParams.height * 0.9F));
          GrantActivity.d(GrantActivity.this).setLayoutParams(localLayoutParams);
          GrantActivity.e(GrantActivity.this).requestLayout();
        }
      }
    });
    com.megvii.meglive_sdk.sdk.a.a.a(this, e.d(this));
    s.a(com.megvii.meglive_sdk.b.a.a("enter_first_page", this.p, this.r));
  }
  
  public final class a
    extends ClickableSpan
  {
    private final View.OnClickListener b;
    
    public a(View.OnClickListener paramOnClickListener)
    {
      this.b = paramOnClickListener;
    }
    
    public final void onClick(View paramView)
    {
      this.b.onClick(paramView);
    }
    
    public final void updateDrawState(TextPaint paramTextPaint)
    {
      paramTextPaint.setColor(GrantActivity.this.getResources().getColor(R.color.blue));
      paramTextPaint.setUnderlineText(false);
    }
  }
  
  final class b
    implements GLSurfaceView.Renderer
  {
    b() {}
    
    public final void onDrawFrame(GL10 paramGL10)
    {
      paramGL10.glClear(16640);
    }
    
    public final void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
    {
      paramGL10.glViewport(0, 0, paramInt1, paramInt2);
    }
    
    public final void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
    {
      GrantActivity.a(GrantActivity.this, GLES20.glGetString(7939));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/activity/GrantActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */