package com.megvii.meglive_sdk.sdk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.megvii.meglive_sdk.R.id;
import com.megvii.meglive_sdk.R.layout;

public class TestActivity
  extends Activity
{
  private LinearLayout a;
  private Button b;
  private ImageView c;
  
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.grant_activity_new);
    this.a = ((LinearLayout)findViewById(R.id.linearlayout_agreement));
    this.b = ((Button)findViewById(R.id.begin_detect));
    this.c = ((ImageView)findViewById(R.id.image));
    this.b.setActivated(true);
  }
  
  protected void onResume()
  {
    super.onResume();
    final int i = (int)(getResources().getDisplayMetrics().density * 20.0F + 0.5F);
    this.b.post(new Runnable()
    {
      public final void run()
      {
        int i = TestActivity.a(TestActivity.this).getBottom();
        if (TestActivity.b(TestActivity.this).getTop() - i < i)
        {
          RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)TestActivity.c(TestActivity.this).getLayoutParams();
          localLayoutParams.width = ((int)(localLayoutParams.width * 0.75F));
          localLayoutParams.height = ((int)(localLayoutParams.height * 0.75F));
          TestActivity.c(TestActivity.this).setLayoutParams(localLayoutParams);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/activity/TestActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */