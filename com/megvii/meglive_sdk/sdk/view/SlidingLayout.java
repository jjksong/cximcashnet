package com.megvii.meglive_sdk.sdk.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.Scroller;
import com.megvii.meglive_sdk.R.drawable;

public class SlidingLayout
  extends FrameLayout
{
  public Activity a;
  private Scroller b;
  private Drawable c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  private int i;
  private int j;
  private boolean k = false;
  
  public SlidingLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SlidingLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SlidingLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.b = new Scroller(paramContext);
    this.c = getResources().getDrawable(R.drawable.left_shadow);
    this.d = ((int)getResources().getDisplayMetrics().density * 16);
  }
  
  public void computeScroll()
  {
    if (this.b.computeScrollOffset())
    {
      scrollTo(this.b.getCurrX(), 0);
      postInvalidate();
      return;
    }
    if (-getScrollX() >= getWidth()) {
      this.a.finish();
    }
  }
  
  protected void dispatchDraw(Canvas paramCanvas)
  {
    super.dispatchDraw(paramCanvas);
    this.c.setBounds(0, 0, this.d, getHeight());
    paramCanvas.save();
    paramCanvas.translate(-this.d, 0.0F);
    this.c.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int m = (int)paramMotionEvent.getX();
    int n = (int)paramMotionEvent.getY();
    int i1 = paramMotionEvent.getAction();
    boolean bool1 = false;
    boolean bool2 = false;
    switch (i1)
    {
    default: 
      break;
    case 2: 
      i1 = this.f;
      int i2 = this.g;
      bool1 = bool2;
      if (this.e < getWidth() / 10)
      {
        bool1 = bool2;
        if (Math.abs(m - i1) > Math.abs(n - i2)) {
          bool1 = true;
        }
      }
      this.f = m;
      this.g = n;
      break;
    case 1: 
      this.g = 0;
      this.f = 0;
      this.e = 0;
      break;
    case 0: 
      this.e = m;
      this.f = m;
      this.g = n;
    }
    return bool1;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int n = (int)paramMotionEvent.getX();
    int m = (int)paramMotionEvent.getY();
    int i1;
    switch (paramMotionEvent.getAction())
    {
    default: 
      break;
    case 2: 
      i1 = this.i;
      int i2 = this.j;
      if ((!this.k) && (this.h < getWidth() / 10) && (Math.abs(n - i1) > Math.abs(m - i2))) {
        this.k = true;
      }
      if (this.k)
      {
        i1 = this.i - (int)paramMotionEvent.getX();
        if (getScrollX() + i1 >= 0) {
          scrollTo(0, 0);
        } else {
          scrollBy(i1, 0);
        }
      }
      this.i = n;
      this.j = m;
      break;
    case 1: 
      this.k = false;
      this.j = 0;
      this.i = 0;
      this.h = 0;
      if (-getScrollX() < getWidth() / 2)
      {
        n = getScrollX();
        m = -getScrollX();
        this.b.startScroll(n, 0, m, 0, 300);
        invalidate();
      }
      else
      {
        i1 = getScrollX();
        n = -getScrollX();
        m = getWidth();
        this.b.startScroll(i1, 0, n - m, 0, 300);
        invalidate();
      }
      break;
    case 0: 
      this.h = n;
      this.i = n;
      this.j = m;
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/view/SlidingLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */