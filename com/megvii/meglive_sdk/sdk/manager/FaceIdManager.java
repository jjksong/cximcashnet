package com.megvii.meglive_sdk.sdk.manager;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.megvii.action.fmp.liveness.lib.jni.MegAuth;
import com.megvii.meglive_sdk.g.b.1;
import com.megvii.meglive_sdk.i.e;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.u;
import com.megvii.meglive_sdk.i.w;
import com.megvii.meglive_sdk.sdk.activity.GrantActivity;
import com.megvii.meglive_sdk.sdk.listener.FaceIdDetectListener;
import com.megvii.meglive_sdk.sdk.listener.FaceIdInitListener;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public final class FaceIdManager
{
  public static final int DETECT_VERITICAL_DISABLE = 2;
  public static final int DETECT_VERITICAL_FRONT = 0;
  public static final int DETECT_VERITICAL_KEEP = 1;
  private static FaceIdManager a;
  private Context b;
  private a c;
  
  private FaceIdManager(Context paramContext)
  {
    if (paramContext != null)
    {
      this.b = paramContext.getApplicationContext();
      this.c = new a(this.b);
    }
  }
  
  private a a()
  {
    try
    {
      if (this.c == null)
      {
        locala = new com/megvii/meglive_sdk/sdk/manager/a;
        locala.<init>(this.b);
        this.c = locala;
      }
      a locala = this.c;
      return locala;
    }
    finally {}
  }
  
  public static FaceIdManager getInstance(Context paramContext)
  {
    try
    {
      if (a == null)
      {
        FaceIdManager localFaceIdManager = new com/megvii/meglive_sdk/sdk/manager/FaceIdManager;
        localFaceIdManager.<init>(paramContext);
        a = localFaceIdManager;
      }
      paramContext = a;
      return paramContext;
    }
    finally {}
  }
  
  public final String getSdkBuildInfo()
  {
    a();
    com.megvii.meglive_sdk.g.a.a();
    return com.megvii.meglive_sdk.g.a.c();
  }
  
  public final String getSdkVersion()
  {
    a();
    com.megvii.meglive_sdk.g.a.a();
    return com.megvii.meglive_sdk.g.a.b();
  }
  
  public final void init(String paramString)
  {
    Object localObject3 = a();
    String str2 = e.d(((a)localObject3).a);
    String str1 = e.e(((a)localObject3).a);
    Object localObject1 = com.megvii.meglive_sdk.g.a.a();
    Object localObject2 = ((a)localObject3).a;
    ((com.megvii.meglive_sdk.g.a)localObject1).e = ((com.megvii.meglive_sdk.f.c)localObject3);
    ((com.megvii.meglive_sdk.g.a)localObject1).b = paramString;
    ((com.megvii.meglive_sdk.g.a)localObject1).c = str2;
    int j = 0;
    int i;
    if ((localObject2 != null) && (paramString != null) && (!paramString.equals(""))) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0)
    {
      ((com.megvii.meglive_sdk.g.a)localObject1).a(h.b);
      return;
    }
    ((com.megvii.meglive_sdk.g.a)localObject1).a = ((Context)localObject2).getApplicationContext();
    localObject2 = ((com.megvii.meglive_sdk.g.a)localObject1).a.getPackageManager();
    if ((!((PackageManager)localObject2).hasSystemFeature("android.hardware.camera")) && (!((PackageManager)localObject2).hasSystemFeature("android.hardware.camera.front"))) {
      i = j;
    } else if (Build.VERSION.SDK_INT < 18) {
      i = j;
    } else {
      i = 1;
    }
    if (i == 0)
    {
      ((com.megvii.meglive_sdk.g.a)localObject1).a(h.f);
      return;
    }
    if (TextUtils.isEmpty(str1))
    {
      ((com.megvii.meglive_sdk.g.a)localObject1).a(h.b);
      return;
    }
    u.a(((com.megvii.meglive_sdk.g.a)localObject1).a, "bizToken", paramString);
    localObject3 = com.megvii.meglive_sdk.g.b.a(((com.megvii.meglive_sdk.g.a)localObject1).a);
    Object localObject4 = ((com.megvii.meglive_sdk.g.a)localObject1).f;
    ((com.megvii.meglive_sdk.g.b)localObject3).e = com.megvii.meglive_sdk.i.a.a();
    ((com.megvii.meglive_sdk.g.b)localObject3).c = new com.megvii.meglive_sdk.d.b();
    ((com.megvii.meglive_sdk.g.b)localObject3).d = new com.megvii.meglive_sdk.d.c();
    ((com.megvii.meglive_sdk.g.b)localObject3).b = e.a(((com.megvii.meglive_sdk.g.b)localObject3).a);
    if (!com.megvii.meglive_sdk.c.a.a(((com.megvii.meglive_sdk.g.b)localObject3).a))
    {
      com.megvii.meglive_sdk.g.b.a(h.h);
      return;
    }
    com.megvii.action.fmp.liveness.lib.b.b.a();
    if (!com.megvii.action.fmp.liveness.lib.a.b.a())
    {
      com.megvii.meglive_sdk.g.b.a(h.f);
      return;
    }
    paramString = UUID.randomUUID().toString();
    com.megvii.action.fmp.liveness.lib.b.b.a();
    localObject1 = MegAuth.nativeGetContext(paramString, "ZZPlatform 1.3.0A");
    paramString = new JSONObject();
    try
    {
      paramString.put("biz_token", ((com.megvii.meglive_sdk.g.b)localObject3).b);
      paramString.put("auth_msg", localObject1);
      paramString.put("version", "FaceIDZFAC 1.3.0A");
      paramString.put("bundle_id", ((com.megvii.meglive_sdk.g.b)localObject3).a.getPackageName());
      paramString.put("key", ((com.megvii.meglive_sdk.g.b)localObject3).e);
      localObject1 = new StringBuilder("delta = ");
      ((StringBuilder)localObject1).append(paramString.toString());
      k.b("access", ((StringBuilder)localObject1).toString());
      str2 = com.megvii.action.fmp.liveness.lib.b.c.a(paramString);
      paramString = com.megvii.meglive_sdk.c.b.a();
      localObject1 = ((com.megvii.meglive_sdk.g.b)localObject3).a;
      localObject2 = ((com.megvii.meglive_sdk.g.b)localObject3).b;
      localObject3 = new b.1((com.megvii.meglive_sdk.g.b)localObject3, str1, (String)localObject4);
      localObject4 = new StringBuilder("bizToken = ");
      ((StringBuilder)localObject4).append((String)localObject2);
      k.a("grantAccess", ((StringBuilder)localObject4).toString());
      localObject4 = new StringBuilder("data = ");
      ((StringBuilder)localObject4).append(str2);
      k.a("grantAccess", ((StringBuilder)localObject4).toString());
      localObject4 = new StringBuilder("URL = ");
      ((StringBuilder)localObject4).append(w.a(str1));
      k.a("grantAccess", ((StringBuilder)localObject4).toString());
      localObject4 = new HashMap();
      ((HashMap)localObject4).put("biz_token", localObject2);
      ((HashMap)localObject4).put("data", str2);
      localObject2 = new HashMap();
      paramString.a((Context)localObject1, w.a(str1), (Map)localObject4, (Map)localObject2, (com.megvii.meglive_sdk.f.d)localObject3);
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
      com.megvii.meglive_sdk.g.b.a(h.e);
    }
  }
  
  public final void setFaceIdDetectListener(FaceIdDetectListener paramFaceIdDetectListener)
  {
    a().c = paramFaceIdDetectListener;
  }
  
  public final void setFaceIdInitListener(FaceIdInitListener paramFaceIdInitListener)
  {
    a().b = paramFaceIdInitListener;
  }
  
  public final void setHost(Context paramContext, String paramString)
  {
    a locala = a();
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)))
    {
      locala.a = paramContext.getApplicationContext();
      u.a(locala.a, "host", paramString);
    }
  }
  
  public final void setLanguage(Activity paramActivity, String paramString)
  {
    a locala = a();
    if ((paramActivity != null) && (!TextUtils.isEmpty(paramString)))
    {
      locala.a = paramActivity.getApplicationContext();
      u.a(locala.a, "language", paramString);
    }
  }
  
  public final void setManifestPack(Context paramContext, String paramString)
  {
    a();
    com.megvii.meglive_sdk.g.a locala = com.megvii.meglive_sdk.g.a.a();
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)))
    {
      locala.a = paramContext.getApplicationContext();
      u.a(paramContext, "manifest_package", paramString);
    }
  }
  
  public final void setVerticalDetectionType(int paramInt)
  {
    a();
    com.megvii.meglive_sdk.g.a locala = com.megvii.meglive_sdk.g.a.a();
    if ((paramInt != 0) && (paramInt != 1) && (paramInt != 2))
    {
      locala.d = 0;
      return;
    }
    locala.d = paramInt;
  }
  
  public final void startDetect()
  {
    a locala = a();
    if (com.megvii.meglive_sdk.g.a.a().a(locala))
    {
      String str1 = com.megvii.meglive_sdk.g.a.a().f;
      int i = com.megvii.meglive_sdk.g.a.a().d;
      str1 = com.megvii.meglive_sdk.g.a.a().g;
      String str2 = com.megvii.meglive_sdk.g.a.a().c;
      com.megvii.meglive_sdk.b.d locald = e.b(locala.a);
      com.megvii.meglive_sdk.b.a.a = 1;
      GrantActivity.a(locala.a, false, locald.a, i, str1, str2);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/manager/FaceIdManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */