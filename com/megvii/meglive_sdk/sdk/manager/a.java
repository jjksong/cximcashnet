package com.megvii.meglive_sdk.sdk.manager;

import android.content.Context;
import com.megvii.meglive_sdk.f.c;
import com.megvii.meglive_sdk.i.h;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.sdk.listener.FaceIdDetectListener;
import com.megvii.meglive_sdk.sdk.listener.FaceIdInitListener;

public final class a
  implements com.megvii.meglive_sdk.f.a, c
{
  Context a;
  FaceIdInitListener b;
  FaceIdDetectListener c;
  
  public a(Context paramContext)
  {
    this.a = paramContext;
  }
  
  public final void a(int paramInt, String paramString)
  {
    Object localObject = new StringBuilder("onDetectFinish -- errorCode=");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(", errorMessage=");
    ((StringBuilder)localObject).append(paramString);
    k.a(((StringBuilder)localObject).toString());
    localObject = this.c;
    if (localObject != null) {
      ((FaceIdDetectListener)localObject).onFailed(paramInt, paramString);
    }
  }
  
  public final void b(int paramInt, String paramString)
  {
    Object localObject = new StringBuilder("onDetectSuccess -- errorCode=");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(", errorMessage=");
    ((StringBuilder)localObject).append(paramString);
    k.a(((StringBuilder)localObject).toString());
    localObject = this.c;
    if (localObject != null) {
      ((FaceIdDetectListener)localObject).onSuccess(paramInt, paramString);
    }
  }
  
  public final void c(int paramInt, String paramString)
  {
    if (paramInt == h.a.q)
    {
      paramString = this.b;
      if (paramString != null) {
        paramString.onSuccess();
      }
    }
    else
    {
      FaceIdInitListener localFaceIdInitListener = this.b;
      if (localFaceIdInitListener != null) {
        localFaceIdInitListener.onFailed(paramInt, paramString);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/manager/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */