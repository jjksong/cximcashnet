package com.megvii.meglive_sdk.sdk.listener;

public abstract interface FaceIdDetectListener
{
  public abstract void onFailed(int paramInt, String paramString);
  
  public abstract void onSuccess(int paramInt, String paramString);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/listener/FaceIdDetectListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */