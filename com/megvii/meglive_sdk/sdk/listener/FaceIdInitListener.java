package com.megvii.meglive_sdk.sdk.listener;

public abstract interface FaceIdInitListener
{
  public abstract void onFailed(int paramInt, String paramString);
  
  public abstract void onSuccess();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/sdk/listener/FaceIdInitListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */