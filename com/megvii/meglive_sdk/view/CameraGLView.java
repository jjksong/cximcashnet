package com.megvii.meglive_sdk.view;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout.LayoutParams;
import com.megvii.meglive_sdk.e.a.d;
import com.megvii.meglive_sdk.e.b.b;
import com.megvii.meglive_sdk.e.b.e;
import com.megvii.meglive_sdk.i.r;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class CameraGLView
  extends GLSurfaceView
{
  public static int a = 1;
  public boolean b;
  public int c;
  public int d;
  public int e;
  int f;
  int g;
  public c h;
  private b i = new b(this);
  private a j = null;
  private Camera.PreviewCallback k = null;
  
  public CameraGLView(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public CameraGLView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CameraGLView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet);
    r.a(paramContext);
    this.f = r.e;
    this.g = r.f;
    setEGLContextClientVersion(2);
    setRenderer(this.i);
    setRenderMode(0);
  }
  
  public final void a()
  {
    try
    {
      if (this.j == null)
      {
        localObject1 = new com/megvii/meglive_sdk/view/CameraGLView$b;
        ((b)localObject1).<init>(this);
        ((b)localObject1).start();
        this.j = ((b)localObject1).a();
      }
      Object localObject1 = this.j;
      ((a)localObject1).sendMessage(((a)localObject1).obtainMessage(1, 640, 480));
      return;
    }
    finally {}
  }
  
  public final SurfaceTexture getSurfaceTexture()
  {
    b localb = this.i;
    if (localb != null) {
      return localb.a;
    }
    return null;
  }
  
  public final void onPause()
  {
    super.onPause();
    a locala = this.j;
    if (locala != null) {
      locala.a(false);
    }
    getHolder().getSurface().release();
  }
  
  public final void onResume()
  {
    super.onResume();
    if ((this.b) && (this.j == null))
    {
      getWidth();
      getHeight();
      a();
    }
  }
  
  public final void setCanVideoRecord(boolean paramBoolean)
  {
    this.i.f = paramBoolean;
  }
  
  public final void setHasFace(boolean paramBoolean)
  {
    this.i.e = paramBoolean;
  }
  
  public final void setICameraOpenCallBack(c paramc)
  {
    this.h = paramc;
  }
  
  public final void setPreviewCallback(Camera.PreviewCallback paramPreviewCallback)
  {
    this.k = paramPreviewCallback;
  }
  
  public final void setVideoEncoder(final d paramd)
  {
    queueEvent(new Runnable()
    {
      public final void run()
      {
        Object localObject5;
        synchronized (CameraGLView.a(CameraGLView.this))
        {
          if (paramd != null)
          {
            Object localObject1 = paramd;
            EGLContext localEGLContext = EGL14.eglGetCurrentContext();
            int i = CameraGLView.a(CameraGLView.this).b;
            localObject5 = ((d)localObject1).j;
            localObject1 = ((d)localObject1).k;
            if ((!(localObject1 instanceof Surface)) && (!(localObject1 instanceof SurfaceTexture)) && (!(localObject1 instanceof SurfaceHolder)))
            {
              localObject5 = new java/lang/RuntimeException;
              ??? = new java/lang/StringBuilder;
              ((StringBuilder)???).<init>("unsupported window type:");
              ((StringBuilder)???).append(localObject1);
              ((RuntimeException)localObject5).<init>(((StringBuilder)???).toString());
              throw ((Throwable)localObject5);
            }
            synchronized (((e)localObject5).a)
            {
              if (!((e)localObject5).h)
              {
                ((e)localObject5).b = localEGLContext;
                ((e)localObject5).e = i;
                ((e)localObject5).d = localObject1;
                ((e)localObject5).c = true;
                ((e)localObject5).g = true;
                Matrix.setIdentityM(((e)localObject5).f, 0);
                Matrix.setIdentityM(((e)localObject5).f, 16);
                ((e)localObject5).a.notifyAll();
              }
            }
          }
        }
        try
        {
          ((e)localObject5).a.wait();
          break label212;
          localObject2 = finally;
          throw ((Throwable)localObject2);
          label212:
          CameraGLView.a(CameraGLView.this).c = paramd;
          return;
          localObject3 = finally;
          throw ((Throwable)localObject3);
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public final void setVideoFps(int paramInt)
  {
    this.i.d = paramInt;
  }
  
  public final void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    Object localObject = this.j;
    if (localObject != null) {
      ((a)localObject).a(true);
    }
    this.j = null;
    this.b = false;
    localObject = this.i;
    if (((b)localObject).a != null)
    {
      ((b)localObject).a.release();
      ((b)localObject).a = null;
    }
    super.surfaceDestroyed(paramSurfaceHolder);
  }
  
  private static final class a
    extends Handler
  {
    private CameraGLView.b a;
    
    public a(CameraGLView.b paramb)
    {
      this.a = paramb;
    }
    
    /* Error */
    public final void a(boolean paramBoolean)
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_2
      //   4: invokevirtual 23	com/megvii/meglive_sdk/view/CameraGLView$a:sendEmptyMessage	(I)Z
      //   7: pop
      //   8: iload_1
      //   9: ifeq +19 -> 28
      //   12: aload_0
      //   13: getfield 15	com/megvii/meglive_sdk/view/CameraGLView$a:a	Lcom/megvii/meglive_sdk/view/CameraGLView$b;
      //   16: invokestatic 28	com/megvii/meglive_sdk/view/CameraGLView$b:a	(Lcom/megvii/meglive_sdk/view/CameraGLView$b;)Z
      //   19: istore_1
      //   20: iload_1
      //   21: ifeq +7 -> 28
      //   24: aload_0
      //   25: invokevirtual 33	java/lang/Object:wait	()V
      //   28: aload_0
      //   29: monitorexit
      //   30: return
      //   31: astore_2
      //   32: aload_0
      //   33: monitorexit
      //   34: aload_2
      //   35: athrow
      //   36: astore_2
      //   37: goto -9 -> 28
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	40	0	this	a
      //   0	40	1	paramBoolean	boolean
      //   31	4	2	localObject	Object
      //   36	1	2	localInterruptedException	InterruptedException
      // Exception table:
      //   from	to	target	type
      //   2	8	31	finally
      //   12	20	31	finally
      //   24	28	31	finally
      //   28	30	31	finally
      //   32	34	31	finally
      //   24	28	36	java/lang/InterruptedException
    }
    
    public final void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        StringBuilder localStringBuilder = new StringBuilder("unknown message:what=");
        localStringBuilder.append(paramMessage.what);
        throw new RuntimeException(localStringBuilder.toString());
      case 2: 
        CameraGLView.b.b(this.a);
        try
        {
          notifyAll();
          Looper.myLooper().quit();
          this.a = null;
          return;
        }
        finally {}
      }
      CameraGLView.b.a(this.a, paramMessage.arg1, paramMessage.arg2);
    }
  }
  
  private static final class b
    extends Thread
  {
    private final Object a = new Object();
    private final WeakReference<CameraGLView> b;
    private CameraGLView.a c;
    private volatile boolean d = false;
    private Camera e;
    private boolean f;
    
    public b(CameraGLView paramCameraGLView)
    {
      super();
      this.b = new WeakReference(paramCameraGLView);
    }
    
    private static Camera.Size a(List<Camera.Size> paramList, int paramInt1, final int paramInt2)
    {
      (Camera.Size)Collections.min(paramList, new Comparator()
      {
        private int a(Camera.Size paramAnonymousSize)
        {
          return Math.abs(this.a - paramAnonymousSize.width) + Math.abs(paramInt2 - paramAnonymousSize.height);
        }
      });
    }
    
    public final CameraGLView.a a()
    {
      try
      {
        synchronized (this.a)
        {
          this.a.wait();
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
      return this.c;
      throw ((Throwable)localObject2);
    }
    
    public final void run()
    {
      
      synchronized (this.a)
      {
        ??? = new com/megvii/meglive_sdk/view/CameraGLView$a;
        ((CameraGLView.a)???).<init>(this);
        this.c = ((CameraGLView.a)???);
        this.d = true;
        this.a.notify();
        Looper.loop();
        synchronized (this.a)
        {
          this.c = null;
          this.d = false;
          return;
        }
      }
    }
  }
  
  public static abstract interface c
  {
    public abstract void a(boolean paramBoolean);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/view/CameraGLView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */