package com.megvii.meglive_sdk.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import com.megvii.meglive_sdk.R.string;
import com.megvii.meglive_sdk.R.styleable;
import com.megvii.meglive_sdk.i.k;
import com.megvii.meglive_sdk.i.q;
import com.megvii.meglive_sdk.i.r;
import java.util.ArrayList;
import java.util.List;

public class CoverView
  extends View
{
  private Matrix A;
  private int[] B;
  private LinearGradient C;
  private int D = 0;
  private int E = 0;
  private float F = 0.0F;
  private float G = 0.0F;
  private float H = 2.0F;
  private float I = 0.0F;
  private float J = 0.0F;
  private float K = this.e;
  private float L = 0.0F;
  private float M = 0.0F;
  private float N = 0.0F;
  private float O = 0.0F;
  private float P = 0.0F;
  private float Q = 0.0F;
  private float R = 0.0F;
  private float S = 0.0F;
  private float T = 0.0F;
  private float U = 0.0F;
  private float V = 0.0F;
  private int W = -1;
  public float a = 0.0F;
  private float aa = 0.0F;
  private float ab = 0.0F;
  private float ac = 0.0F;
  private float ad = 0.0F;
  private float ae = 0.75F;
  private PorterDuffXfermode af;
  private int ag = -1;
  private float ah = 3.6F;
  private float ai = 18.0F;
  private long aj = 30L;
  private float ak = 0.5F;
  private int al = 1;
  private float am = 0.0F;
  private Runnable an = new Runnable()
  {
    public final void run()
    {
      CoverView localCoverView = CoverView.this;
      CoverView.a(localCoverView, CoverView.a(localCoverView) + CoverView.b(CoverView.this) * CoverView.c(CoverView.this));
      if (CoverView.a(CoverView.this) >= CoverView.d(CoverView.this))
      {
        localCoverView = CoverView.this;
        CoverView.a(localCoverView, CoverView.d(localCoverView));
        localCoverView = CoverView.this;
        CoverView.a(localCoverView, -CoverView.c(localCoverView));
      }
      else if (CoverView.a(CoverView.this) <= CoverView.e(CoverView.this))
      {
        localCoverView = CoverView.this;
        CoverView.a(localCoverView, CoverView.e(localCoverView));
        localCoverView = CoverView.this;
        CoverView.a(localCoverView, -CoverView.c(localCoverView));
      }
      CoverView.this.invalidate();
      localCoverView = CoverView.this;
      localCoverView.postDelayed(CoverView.f(localCoverView), CoverView.g(CoverView.this));
    }
  };
  public float b = 0.0F;
  private Context c;
  private int d = 1;
  private int e = 20;
  private int[] f = { 255, 255, 255, 255 };
  private List<Integer> g;
  private String h = "";
  private Bitmap i;
  private Canvas j;
  private Paint k;
  private Paint l;
  private Paint m;
  private TextPaint n;
  private float o = 0.0F;
  private int p;
  private RectF q;
  private Rect r;
  private RectF s;
  private RectF t;
  private RectF u;
  private float v;
  private int w;
  private int x;
  private int y;
  private int z;
  
  public CoverView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CoverView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CoverView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CoverView);
    this.c = paramContext;
    this.A = new Matrix();
    this.A.setRotate(0.0F);
    this.g = new ArrayList();
    this.p = Color.parseColor("#FF38B0E8");
    q.a(paramContext);
    paramAttributeSet = getResources().getString(R.string.key_liveness_home_ring_color);
    this.w = q.a(q.e, paramAttributeSet);
    q.a(paramContext);
    paramAttributeSet = getResources().getString(R.string.key_liveness_home_background_color);
    this.x = q.a(q.e, paramAttributeSet);
    q.a(paramContext);
    paramAttributeSet = getResources().getString(R.string.key_liveness_home_prompt_color);
    this.y = q.a(q.e, paramAttributeSet);
    q.a(paramContext);
    paramAttributeSet = getResources().getString(R.string.key_liveness_home_prompt_size);
    this.z = q.a(q.f, paramAttributeSet);
    this.q = new RectF();
    this.r = new Rect();
    this.s = new RectF();
    this.t = new RectF();
    this.u = new RectF();
    this.k = new Paint();
    this.k.setAntiAlias(true);
    this.l = new Paint();
    this.l.setAntiAlias(true);
    this.m = new Paint();
    this.m.setAntiAlias(true);
    this.n = new TextPaint(1);
    paramAttributeSet = new StringBuilder("mBorderWid_progress=");
    paramAttributeSet.append(this.e);
    k.b("coverView", paramAttributeSet.toString());
    r.a(paramContext);
    new BitmapFactory.Options().inScaled = false;
  }
  
  private void a(Canvas paramCanvas, float paramFloat)
  {
    this.m.setColor(this.p);
    this.m.setStyle(Paint.Style.STROKE);
    this.m.setStrokeWidth(this.e);
    this.m.setStrokeCap(Paint.Cap.ROUND);
    this.s.set(this.L, this.b, this.M, this.N);
    paramCanvas.drawArc(this.s, 270.0F, paramFloat, false, this.m);
  }
  
  public final void a()
  {
    this.e = r.a(this.c, 6.0F);
    this.K = this.e;
    if (this.D == 0) {
      this.D = getWidth();
    }
    if (this.E == 0) {
      this.E = getHeight();
    }
    if (this.F == 0.0F)
    {
      this.F = (this.D * this.ae);
      this.G = (this.F / 2.0F);
    }
    if (this.I == 0.0F) {
      this.I = (this.D / 2);
    }
    if (this.J == 0.0F) {
      this.J = (this.E * 0.37F);
    }
    if (this.a == 0.0F) {
      this.a = this.F;
    }
    float f2;
    float f1;
    if (this.L == 0.0F)
    {
      f2 = this.D;
      f1 = this.a;
      this.L = ((f2 - f1) / 2.0F);
      this.b = (this.J - this.G);
      this.M = (this.L + f1);
      this.N = (f1 + this.b);
    }
    if (this.O == 0.0F)
    {
      f2 = this.D;
      f1 = this.F;
      this.O = ((f2 - f1) / 2.0F);
      this.P = (this.J - this.G);
      this.Q = (this.O + f1);
      this.R = (f1 + this.P);
    }
    float f3;
    if (this.T == 0.0F)
    {
      f1 = this.G;
      double d2 = 0.5F * f1;
      Double.isNaN(d2);
      Double.isNaN(d2);
      double d1 = f1 * f1;
      Double.isNaN(d1);
      d1 = Math.sqrt(d2 * d2 + d1);
      f2 = this.G;
      double d3 = f2;
      Double.isNaN(d3);
      Double.isNaN(d2);
      f1 = this.J;
      this.T = (f1 - f2);
      double d4 = this.T;
      Double.isNaN(d4);
      this.V = ((float)(d4 + (d3 - d2)));
      f3 = this.I;
      d2 = f3;
      Double.isNaN(d2);
      this.S = ((float)(d2 - d1));
      d2 = f3;
      Double.isNaN(d2);
      this.U = ((float)(d2 + d1));
      this.T = (f1 + f2 + r.a(this.c, 16.0F));
      this.V = (this.T + r.a(this.c, 28.0F));
    }
    if (this.aa == 0.0F)
    {
      f3 = this.I;
      f1 = this.G;
      float f4 = this.K;
      this.aa = (f3 - f1 - f4);
      f2 = this.J;
      this.ab = (f2 - f1 - f4);
      this.ac = (f3 + f1 + f4);
      this.ad = (f2 + f1 + f4);
    }
    this.v = (this.V + r.a(this.c, 32.0F));
  }
  
  public final void a(float paramFloat, int paramInt)
  {
    this.o = paramFloat;
    this.p = paramInt;
    invalidate();
  }
  
  public final void a(float paramFloat1, int paramInt, float paramFloat2)
  {
    this.o = paramFloat1;
    this.p = paramInt;
    if (paramFloat2 != -1.0F)
    {
      int i1 = getWidth() / 5;
      this.g.clear();
      for (paramInt = 0; paramInt < i1; paramInt += 5)
      {
        paramFloat1 = paramInt / (i1 * 0.5F);
        int[] arrayOfInt = this.f;
        arrayOfInt[0] = ((int)((0.5F + paramFloat2) * 255.0F));
        arrayOfInt[1] = 192;
        double d1 = (paramFloat1 - 1.0F) * paramFloat2;
        Double.isNaN(d1);
        arrayOfInt[2] = ((int)((d1 + 0.5D) * 255.0D));
        arrayOfInt[3] = 255;
        this.g.add(Integer.valueOf(Color.argb(arrayOfInt[3], arrayOfInt[0], arrayOfInt[1], arrayOfInt[2])));
      }
    }
    invalidate();
  }
  
  public float getImageY()
  {
    return this.v;
  }
  
  public float getMCenterX()
  {
    return this.I;
  }
  
  public float getMCenterY()
  {
    return this.J;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    System.currentTimeMillis();
    a();
    this.k.setStyle(Paint.Style.FILL);
    Object localObject = this.g;
    if ((localObject != null) && (!((List)localObject).isEmpty()))
    {
      if (this.B == null) {
        this.B = new int[this.g.size()];
      }
      for (int i1 = 0; i1 < this.g.size(); i1++) {
        this.B[i1] = ((Integer)this.g.get(i1)).intValue();
      }
      this.C = new LinearGradient(0.0F, 0.0F, this.D / this.H, 0.0F, this.B, null, Shader.TileMode.CLAMP);
      this.k.setShader(this.C);
    }
    else
    {
      this.k.setColor(getResources().getColor(this.x));
    }
    if (this.i == null)
    {
      f2 = this.D;
      f1 = this.H;
      this.i = Bitmap.createBitmap((int)(f2 / f1), (int)(this.E / f1), Bitmap.Config.ARGB_8888);
    }
    if (this.j == null) {
      this.j = new Canvas(this.i);
    }
    localObject = this.r;
    float f2 = this.D;
    float f1 = this.H;
    ((Rect)localObject).set(0, 0, (int)(f2 / f1), (int)(this.E / f1));
    this.j.drawRect(this.r, this.k);
    if (this.af == null) {
      this.af = new PorterDuffXfermode(PorterDuff.Mode.XOR);
    }
    this.l.setXfermode(this.af);
    localObject = this.j;
    f2 = this.I;
    f1 = this.H;
    ((Canvas)localObject).drawCircle(f2 / f1, this.J / f1, this.G / f1, this.l);
    this.l.setXfermode(null);
    this.q.set(0.0F, 0.0F, this.D, this.E);
    paramCanvas.drawBitmap(this.i, this.r, this.q, this.k);
    this.m.setColor(Color.rgb(221, 221, 221));
    this.m.setStyle(Paint.Style.STROKE);
    this.m.setStrokeWidth(this.d);
    paramCanvas.drawCircle(this.I, this.J, this.G, this.m);
    this.m.setColor(getResources().getColor(this.w));
    this.m.setStyle(Paint.Style.STROKE);
    this.m.setStrokeWidth(this.e);
    this.s.set(this.L, this.b, this.M, this.N);
    paramCanvas.drawArc(this.s, 90.0F, 360.0F, false, this.m);
    if (this.ag == 0)
    {
      a(paramCanvas, this.o + this.am);
    }
    else
    {
      f1 = this.o;
      if (f1 != 0.0F) {
        a(paramCanvas, f1);
      }
    }
    this.n.setARGB(0, 0, 0, 0);
    this.u.set(this.S, this.T, this.U, this.V);
    paramCanvas.drawRect(this.u, this.n);
    this.n.setColor(getResources().getColor(this.y));
    localObject = this.n.getFontMetricsInt();
    this.n.setTextSize(this.c.getResources().getDimensionPixelSize(this.z));
    f1 = (this.u.bottom + this.u.top - ((Paint.FontMetricsInt)localObject).bottom - ((Paint.FontMetricsInt)localObject).top) / 2.0F;
    this.n.setTextAlign(Paint.Align.CENTER);
    localObject = new StaticLayout(this.h, this.n, (int)this.F, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, false);
    paramCanvas.translate(this.u.centerX(), this.u.top);
    ((StaticLayout)localObject).draw(paramCanvas);
    paramCanvas.translate(-this.u.centerX(), -f1);
  }
  
  public void setMode(int paramInt)
  {
    if (this.ag == paramInt) {
      return;
    }
    this.ag = paramInt;
    if (paramInt == 0)
    {
      this.al = 1;
      this.am = 0.0F;
      postDelayed(this.an, this.aj);
      return;
    }
    removeCallbacks(this.an);
  }
  
  public void setTips(String paramString)
  {
    this.h = paramString;
    invalidate();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/meglive_sdk/view/CoverView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */