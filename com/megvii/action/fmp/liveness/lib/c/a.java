package com.megvii.action.fmp.liveness.lib.c;

public final class a
{
  public int a = -1;
  public int b;
  public int c;
  public int d;
  public long e;
  public int f;
  public long g;
  public int h;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("ActionLivenessDetectResult{currentSetp=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", qualityResult=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", currentActionIndex=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", seletedAction=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", actionTimeout=");
    localStringBuilder.append(this.e);
    localStringBuilder.append(", actionCount=");
    localStringBuilder.append(this.f);
    localStringBuilder.append(", detectTime=");
    localStringBuilder.append(this.g);
    localStringBuilder.append(", detectResult=");
    localStringBuilder.append(this.h);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/action/fmp/liveness/lib/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */