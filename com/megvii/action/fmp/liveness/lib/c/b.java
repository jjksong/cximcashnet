package com.megvii.action.fmp.liveness.lib.c;

public final class b
{
  public int a = -1;
  public int b;
  public int c;
  public float d;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("FMPLivenessDetectResult{currentStep=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", qualityResult=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", detectResult=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", progress=");
    localStringBuilder.append(this.d);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/action/fmp/liveness/lib/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */