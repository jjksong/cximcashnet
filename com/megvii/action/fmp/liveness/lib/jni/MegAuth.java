package com.megvii.action.fmp.liveness.lib.jni;

public class MegAuth
{
  public static native String nativeGetContext(String paramString1, String paramString2);
  
  public static native long nativeGetExpireTime(String paramString);
  
  public static native boolean nativeSetLicence(String paramString);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/action/fmp/liveness/lib/jni/MegAuth.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */