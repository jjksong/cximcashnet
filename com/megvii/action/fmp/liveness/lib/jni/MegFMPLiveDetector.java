package com.megvii.action.fmp.liveness.lib.jni;

public class MegFMPLiveDetector
{
  public native float getProgress(long paramLong);
  
  public native int getSilentCurrentStep(long paramLong);
  
  public native String getSilentDeltaInfo(long paramLong, String paramString1, boolean paramBoolean, String paramString2, String paramString3, byte[] paramArrayOfByte);
  
  public native int getSilentDetectFailedType(long paramLong);
  
  public native int getSilentQualityErrorType(long paramLong);
  
  public native long nativeCreateSilentHandle(String paramString, int paramInt, long paramLong);
  
  public native boolean nativeLoadSilentModel(long paramLong, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3);
  
  public native void nativeSilentLiveDetect(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);
  
  public native void nativeSilentRelease(long paramLong);
  
  public native void nativeStartSilentLiveDetect(long paramLong);
  
  public native void nativeStopSilentLiveDetect(long paramLong);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/action/fmp/liveness/lib/jni/MegFMPLiveDetector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */