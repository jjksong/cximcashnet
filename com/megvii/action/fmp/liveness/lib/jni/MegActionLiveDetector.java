package com.megvii.action.fmp.liveness.lib.jni;

public class MegActionLiveDetector
{
  public native int getActionCount(long paramLong);
  
  public native int getActionCurrentStep(long paramLong);
  
  public native String getActionDeltaInfo(long paramLong, String paramString1, boolean paramBoolean, String paramString2, String paramString3);
  
  public native int getActionDetectFailedType(long paramLong);
  
  public native int getActionQualityErrorType(long paramLong);
  
  public native long getActionTimeout(long paramLong);
  
  public native int getCurrentActionIndex(long paramLong);
  
  public native long getDetectTime(long paramLong);
  
  public native int getSelectedAction(long paramLong);
  
  public native void nativeActionLiveDetect(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);
  
  public native void nativeActionRelease(long paramLong);
  
  public native long nativeCreateActionHandle(boolean paramBoolean, int paramInt1, int paramInt2, String paramString, int[] paramArrayOfInt);
  
  public native boolean nativeLoadActionModel(long paramLong, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3);
  
  public native void nativeStartActionLiveDetect(long paramLong);
  
  public native void nativeStopActionLiveDetect(long paramLong);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/megvii/action/fmp/liveness/lib/jni/MegActionLiveDetector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */