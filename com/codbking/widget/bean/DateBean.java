package com.codbking.widget.bean;

public class DateBean
{
  private int day;
  private int hour;
  private int minute;
  private int moth;
  private int year;
  
  private String makeZero(int paramInt)
  {
    if (paramInt > 9)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("");
      localStringBuilder.append(paramInt);
      return localStringBuilder.toString();
    }
    if (paramInt > 0)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("0");
      localStringBuilder.append(paramInt);
      return localStringBuilder.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("");
    localStringBuilder.append(paramInt);
    return localStringBuilder.toString();
  }
  
  public int getDay()
  {
    return this.day;
  }
  
  public String getDisplayDate(DateType paramDateType)
  {
    String str = "";
    switch (paramDateType)
    {
    default: 
      paramDateType = str;
      break;
    case ???: 
      paramDateType = new StringBuilder();
      paramDateType.append(makeZero(this.hour));
      paramDateType.append(":");
      paramDateType.append(makeZero(this.minute));
      paramDateType = paramDateType.toString();
      break;
    case ???: 
      paramDateType = new StringBuilder();
      paramDateType.append(this.year);
      paramDateType.append("-");
      paramDateType.append(makeZero(this.moth));
      paramDateType.append("-");
      paramDateType.append(makeZero(this.day));
      paramDateType = paramDateType.toString();
      break;
    case ???: 
      paramDateType = new StringBuilder();
      paramDateType.append(this.year);
      paramDateType.append("-");
      paramDateType.append(makeZero(this.moth));
      paramDateType.append("-");
      paramDateType.append(makeZero(this.day));
      paramDateType.append(" ");
      paramDateType.append(makeZero(this.hour));
      paramDateType = paramDateType.toString();
      break;
    case ???: 
      paramDateType = new StringBuilder();
      paramDateType.append(this.year);
      paramDateType.append("-");
      paramDateType.append(makeZero(this.moth));
      paramDateType.append("-");
      paramDateType.append(makeZero(this.day));
      paramDateType.append(" ");
      paramDateType.append(makeZero(this.hour));
      paramDateType.append(":");
      paramDateType.append(makeZero(this.minute));
      paramDateType = paramDateType.toString();
      break;
    case ???: 
      paramDateType = new StringBuilder();
      paramDateType.append(this.year);
      paramDateType.append("-");
      paramDateType.append(makeZero(this.moth));
      paramDateType.append("-");
      paramDateType.append(makeZero(this.day));
      paramDateType.append(" ");
      paramDateType.append(makeZero(this.hour));
      paramDateType.append(":");
      paramDateType.append(makeZero(this.minute));
      paramDateType = paramDateType.toString();
    }
    return paramDateType;
  }
  
  public int getHour()
  {
    return this.hour;
  }
  
  public int getMinute()
  {
    return this.minute;
  }
  
  public int getMoth()
  {
    return this.moth;
  }
  
  public int getYear()
  {
    return this.year;
  }
  
  public void setDay(int paramInt)
  {
    this.day = paramInt;
  }
  
  public void setHour(int paramInt)
  {
    this.hour = paramInt;
  }
  
  public void setMinute(int paramInt)
  {
    this.minute = paramInt;
  }
  
  public void setMoth(int paramInt)
  {
    this.moth = paramInt;
  }
  
  public void setYear(int paramInt)
  {
    this.year = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/bean/DateBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */