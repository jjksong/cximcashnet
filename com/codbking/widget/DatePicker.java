package com.codbking.widget;

import android.content.Context;
import android.widget.TextView;
import com.codbking.widget.bean.DateType;
import com.codbking.widget.genview.WheelGeneralAdapter;
import com.codbking.widget.view.WheelView;
import java.util.Date;

class DatePicker
  extends BaseWheelPick
{
  private static final String TAG = "WheelPicker";
  private DatePickerHelper datePicker;
  private Integer[] dayArr;
  private WheelView dayView;
  private Integer[] hourArr;
  private WheelView hourView;
  private Integer[] minutArr;
  private WheelView minuteView;
  private WheelView monthView;
  private Integer[] mothArr;
  private OnChangeLisener onChangeLisener;
  private int selectDay;
  private Date startDate = new Date();
  public DateType type = DateType.TYPE_ALL;
  private TextView weekView;
  private Integer[] yearArr;
  private int yearLimt = 5;
  private WheelView yearView;
  
  public DatePicker(Context paramContext, DateType paramDateType)
  {
    super(paramContext);
    if (this.type != null) {
      this.type = paramDateType;
    }
  }
  
  private void setChangeDaySelect(int paramInt1, int paramInt2)
  {
    this.dayArr = this.datePicker.genDay(paramInt1, paramInt2);
    ((WheelGeneralAdapter)this.dayView.getViewAdapter()).setData(convertData(this.dayView, this.dayArr));
    paramInt1 = this.datePicker.findIndextByValue(this.selectDay, this.dayArr);
    if (paramInt1 == -1) {
      this.dayView.setCurrentItem(0);
    } else {
      this.dayView.setCurrentItem(paramInt1);
    }
  }
  
  protected String[] convertData(WheelView paramWheelView, Integer[] paramArrayOfInteger)
  {
    if (paramWheelView == this.yearView) {
      return this.datePicker.getDisplayValue(paramArrayOfInteger, "年");
    }
    if (paramWheelView == this.monthView) {
      return this.datePicker.getDisplayValue(paramArrayOfInteger, "月");
    }
    if (paramWheelView == this.dayView) {
      return this.datePicker.getDisplayValue(paramArrayOfInteger, "日");
    }
    if (paramWheelView == this.hourView) {
      return this.datePicker.getDisplayValue(paramArrayOfInteger, "");
    }
    if (paramWheelView == this.minuteView) {
      return this.datePicker.getDisplayValue(paramArrayOfInteger, "");
    }
    return new String[0];
  }
  
  protected int getItemHeight()
  {
    return this.dayView.getItemHeight();
  }
  
  protected int getLayout()
  {
    return R.layout.cbk_wheel_picker;
  }
  
  public Date getSelectDate()
  {
    return DateUtils.getDate(this.yearArr[this.yearView.getCurrentItem()].intValue(), this.mothArr[this.monthView.getCurrentItem()].intValue(), this.dayArr[this.dayView.getCurrentItem()].intValue(), this.hourArr[this.hourView.getCurrentItem()].intValue(), this.minutArr[this.minuteView.getCurrentItem()].intValue());
  }
  
  public void init()
  {
    this.minuteView = ((WheelView)findViewById(R.id.minute));
    this.hourView = ((WheelView)findViewById(R.id.hour));
    this.weekView = ((TextView)findViewById(R.id.week));
    this.dayView = ((WheelView)findViewById(R.id.day));
    this.monthView = ((WheelView)findViewById(R.id.month));
    this.yearView = ((WheelView)findViewById(R.id.year));
    switch (this.type)
    {
    default: 
      break;
    case ???: 
      this.minuteView.setVisibility(0);
      this.hourView.setVisibility(0);
      this.weekView.setVisibility(8);
      this.dayView.setVisibility(8);
      this.monthView.setVisibility(8);
      this.yearView.setVisibility(8);
      break;
    case ???: 
      this.minuteView.setVisibility(8);
      this.hourView.setVisibility(8);
      this.weekView.setVisibility(8);
      this.dayView.setVisibility(0);
      this.monthView.setVisibility(0);
      this.yearView.setVisibility(0);
      break;
    case ???: 
      this.minuteView.setVisibility(8);
      this.hourView.setVisibility(0);
      this.weekView.setVisibility(8);
      this.dayView.setVisibility(0);
      this.monthView.setVisibility(0);
      this.yearView.setVisibility(0);
      break;
    case ???: 
      this.minuteView.setVisibility(0);
      this.hourView.setVisibility(0);
      this.weekView.setVisibility(8);
      this.dayView.setVisibility(0);
      this.monthView.setVisibility(0);
      this.yearView.setVisibility(0);
      break;
    case ???: 
      this.minuteView.setVisibility(0);
      this.hourView.setVisibility(0);
      this.weekView.setVisibility(0);
      this.dayView.setVisibility(0);
      this.monthView.setVisibility(0);
      this.yearView.setVisibility(0);
    }
    this.datePicker = new DatePickerHelper();
    this.datePicker.setStartDate(this.startDate, this.yearLimt);
    this.dayArr = this.datePicker.genDay();
    this.yearArr = this.datePicker.genYear();
    this.mothArr = this.datePicker.genMonth();
    this.hourArr = this.datePicker.genHour();
    this.minutArr = this.datePicker.genMinut();
    this.weekView.setText(this.datePicker.getDisplayStartWeek());
    setWheelListener(this.yearView, this.yearArr, false);
    setWheelListener(this.monthView, this.mothArr, true);
    setWheelListener(this.dayView, this.dayArr, true);
    setWheelListener(this.hourView, this.hourArr, true);
    setWheelListener(this.minuteView, this.minutArr, true);
    Object localObject1 = this.yearView;
    Object localObject2 = this.datePicker;
    ((WheelView)localObject1).setCurrentItem(((DatePickerHelper)localObject2).findIndextByValue(((DatePickerHelper)localObject2).getToady(DatePickerHelper.Type.YEAR), this.yearArr));
    localObject2 = this.monthView;
    localObject1 = this.datePicker;
    ((WheelView)localObject2).setCurrentItem(((DatePickerHelper)localObject1).findIndextByValue(((DatePickerHelper)localObject1).getToady(DatePickerHelper.Type.MOTH), this.mothArr));
    localObject1 = this.dayView;
    localObject2 = this.datePicker;
    ((WheelView)localObject1).setCurrentItem(((DatePickerHelper)localObject2).findIndextByValue(((DatePickerHelper)localObject2).getToady(DatePickerHelper.Type.DAY), this.dayArr));
    localObject1 = this.hourView;
    localObject2 = this.datePicker;
    ((WheelView)localObject1).setCurrentItem(((DatePickerHelper)localObject2).findIndextByValue(((DatePickerHelper)localObject2).getToady(DatePickerHelper.Type.HOUR), this.hourArr));
    localObject1 = this.minuteView;
    localObject2 = this.datePicker;
    ((WheelView)localObject1).setCurrentItem(((DatePickerHelper)localObject2).findIndextByValue(((DatePickerHelper)localObject2).getToady(DatePickerHelper.Type.MINUTE), this.minutArr));
  }
  
  public void onChanged(WheelView paramWheelView, int paramInt1, int paramInt2)
  {
    paramInt1 = this.yearArr[this.yearView.getCurrentItem()].intValue();
    paramInt2 = this.mothArr[this.monthView.getCurrentItem()].intValue();
    int k = this.dayArr[this.dayView.getCurrentItem()].intValue();
    int j = this.hourArr[this.hourView.getCurrentItem()].intValue();
    int i = this.minutArr[this.minuteView.getCurrentItem()].intValue();
    if ((paramWheelView != this.yearView) && (paramWheelView != this.monthView)) {
      this.selectDay = k;
    } else {
      setChangeDaySelect(paramInt1, paramInt2);
    }
    if ((paramWheelView == this.yearView) || (paramWheelView == this.monthView) || (paramWheelView == this.dayView)) {
      this.weekView.setText(this.datePicker.getDisplayWeek(paramInt1, paramInt2, k));
    }
    paramWheelView = this.onChangeLisener;
    if (paramWheelView != null) {
      paramWheelView.onChanged(DateUtils.getDate(paramInt1, paramInt2, k, j, i));
    }
  }
  
  public void onScrollingFinished(WheelView paramWheelView) {}
  
  public void onScrollingStarted(WheelView paramWheelView) {}
  
  protected void setData(Object[] paramArrayOfObject) {}
  
  public void setOnChangeLisener(OnChangeLisener paramOnChangeLisener)
  {
    this.onChangeLisener = paramOnChangeLisener;
  }
  
  public void setStartDate(Date paramDate)
  {
    this.startDate = paramDate;
  }
  
  public void setYearLimt(int paramInt)
  {
    this.yearLimt = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/DatePicker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */