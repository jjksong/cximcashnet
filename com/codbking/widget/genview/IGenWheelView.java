package com.codbking.widget.genview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public abstract interface IGenWheelView
{
  public abstract View setup(Context paramContext, int paramInt, View paramView, ViewGroup paramViewGroup, Object paramObject);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/genview/IGenWheelView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */