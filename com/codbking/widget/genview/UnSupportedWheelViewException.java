package com.codbking.widget.genview;

public class UnSupportedWheelViewException
  extends Exception
{
  private static final long serialVersionUID = 1894662683963152958L;
  String mistake;
  
  public UnSupportedWheelViewException()
  {
    this.mistake = "Only support List, Map,String Array,Cursor,SparseArray,SparseBooleanArray,SparseIntArray,Vector, and basic data type";
  }
  
  public UnSupportedWheelViewException(String paramString)
  {
    super(paramString);
    this.mistake = paramString;
  }
  
  public String getError()
  {
    return this.mistake;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/genview/UnSupportedWheelViewException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */