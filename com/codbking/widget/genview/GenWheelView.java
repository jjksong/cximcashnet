package com.codbking.widget.genview;

import android.content.Context;
import android.database.Cursor;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

public abstract class GenWheelView
  implements IGenWheelView
{
  protected abstract View genBody(Context paramContext, View paramView, Object paramObject, int paramInt);
  
  public View setup(Context paramContext, int paramInt, View paramView, ViewGroup paramViewGroup, Object paramObject)
  {
    if ((paramObject instanceof Object[])) {
      return genBody(paramContext, paramView, ((Object[])paramObject)[paramInt], paramInt);
    }
    if ((paramObject instanceof ArrayList)) {
      return genBody(paramContext, paramView, ((ArrayList)paramObject).get(paramInt), paramInt);
    }
    if ((paramObject instanceof LinkedHashMap))
    {
      paramViewGroup = ((LinkedHashMap)paramObject).entrySet().iterator();
      int i = paramInt;
      while (paramViewGroup.hasNext())
      {
        paramObject = (Map.Entry)paramViewGroup.next();
        if ((((Map.Entry)paramObject).getValue() instanceof List))
        {
          if (i <= ((List)((Map.Entry)paramObject).getValue()).size()) {
            return genBody(paramContext, paramView, ((List)((Map.Entry)paramObject).getValue()).get(i - 1), paramInt);
          }
          i = i - ((List)((Map.Entry)paramObject).getValue()).size() - 1;
        }
      }
      return null;
    }
    if ((paramObject instanceof Cursor))
    {
      ((Cursor)paramObject).moveToPosition(paramInt);
      return genBody(paramContext, paramView, paramObject, paramInt);
    }
    if ((paramObject instanceof SparseArray)) {
      return genBody(paramContext, paramView, ((SparseArray)paramObject).valueAt(paramInt), paramInt);
    }
    if ((paramObject instanceof SparseBooleanArray))
    {
      paramViewGroup = (SparseBooleanArray)paramObject;
      return genBody(paramContext, paramView, Boolean.valueOf(paramViewGroup.get(paramViewGroup.keyAt(paramInt))), paramInt);
    }
    if ((paramObject instanceof SparseIntArray)) {
      return genBody(paramContext, paramView, Integer.valueOf(((SparseIntArray)paramObject).valueAt(paramInt)), paramInt);
    }
    if ((paramObject instanceof Vector)) {
      return genBody(paramContext, paramView, ((Vector)paramObject).get(paramInt), paramInt);
    }
    if ((paramObject instanceof LinkedList)) {
      return genBody(paramContext, paramView, ((LinkedList)paramObject).get(paramInt), paramInt);
    }
    return paramView;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/genview/GenWheelView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */