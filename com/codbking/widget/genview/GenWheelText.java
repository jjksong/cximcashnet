package com.codbking.widget.genview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.codbking.widget.R.id;
import com.codbking.widget.R.layout;

public class GenWheelText
  extends GenWheelView
{
  private int line = 1;
  private int textColor;
  private int textSize = 24;
  
  public GenWheelText()
  {
    this(1, 24, 4473924);
  }
  
  public GenWheelText(int paramInt)
  {
    this(1, 24, paramInt);
  }
  
  public GenWheelText(int paramInt1, int paramInt2, int paramInt3)
  {
    this.line = paramInt1;
    this.textSize = paramInt2;
    this.textColor = paramInt3;
  }
  
  protected View genBody(Context paramContext, View paramView, Object paramObject, int paramInt)
  {
    if (paramView != null)
    {
      paramContext = (ViewHolder)paramView.getTag();
    }
    else
    {
      paramView = LayoutInflater.from(paramContext).inflate(R.layout.cbk_wheel_default_inner_text, null);
      paramContext = new ViewHolder(null);
      paramContext.text = ((TextView)paramView.findViewById(R.id.text));
      paramView.setTag(paramContext);
    }
    paramContext.text.setTextSize(this.textSize);
    paramContext.text.setMaxLines(this.line);
    paramContext.text.setText(paramObject.toString());
    paramContext.text.setTextColor(this.textColor);
    return paramView;
  }
  
  private class ViewHolder
  {
    public TextView text;
    
    private ViewHolder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/genview/GenWheelText.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */