package com.codbking.widget.listener;

import com.codbking.widget.bean.DateBean;
import com.codbking.widget.bean.DateType;

public abstract interface WheelPickerListener
{
  public abstract void onSelect(DateType paramDateType, DateBean paramDateBean);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/listener/WheelPickerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */