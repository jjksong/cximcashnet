package com.codbking.widget.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.codbking.widget.R.id;
import com.codbking.widget.adapters.WheelViewAdapter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class WheelView
  extends View
{
  private static final int DEF_VISIBLE_ITEMS = 5;
  private static final int ITEM_OFFSET_PERCENT = 10;
  private static final int PADDING = 10;
  private static final int[] SHADOWS_COLORS = { -15658735, 11184810, 11184810 };
  private static final String TAG = "WheelView";
  private GradientDrawable bottomShadow;
  private Drawable centerDrawable;
  private List<OnWheelChangedListener> changingListeners = new LinkedList();
  private List<OnWheelClickedListener> clickingListeners = new LinkedList();
  private int currentItem = 0;
  private DataSetObserver dataObserver = new DataSetObserver()
  {
    public void onChanged()
    {
      WheelView.this.invalidateWheel(false);
    }
    
    public void onInvalidated()
    {
      WheelView.this.invalidateWheel(true);
    }
  };
  private int firstItem;
  boolean isCyclic = false;
  private boolean isScrollingPerformed;
  private int itemHeight = 0;
  private LinearLayout itemsLayout;
  private WheelRecycle recycle = new WheelRecycle(this);
  private WheelScroller scroller;
  WheelScroller.ScrollingListener scrollingListener = new WheelScroller.ScrollingListener()
  {
    public void onFinished()
    {
      if (WheelView.this.isScrollingPerformed)
      {
        WheelView.this.notifyScrollingListenersAboutEnd();
        WheelView.access$002(WheelView.this, false);
      }
      WheelView.access$202(WheelView.this, 0);
      WheelView.this.invalidate();
    }
    
    public void onJustify()
    {
      if (Math.abs(WheelView.this.scrollingOffset) > 1) {
        WheelView.this.scroller.scroll(WheelView.this.scrollingOffset, 0);
      }
    }
    
    public void onScroll(int paramAnonymousInt)
    {
      WheelView.this.doScroll(paramAnonymousInt);
      int i = WheelView.this.getHeight();
      if (WheelView.this.scrollingOffset > i)
      {
        WheelView.access$202(WheelView.this, i);
        WheelView.this.scroller.stopScrolling();
      }
      else
      {
        paramAnonymousInt = WheelView.this.scrollingOffset;
        i = -i;
        if (paramAnonymousInt < i)
        {
          WheelView.access$202(WheelView.this, i);
          WheelView.this.scroller.stopScrolling();
        }
      }
    }
    
    public void onStarted()
    {
      WheelView.access$002(WheelView.this, true);
      WheelView.this.notifyScrollingListenersAboutStart();
    }
  };
  private List<OnWheelScrollListener> scrollingListeners = new LinkedList();
  private int scrollingOffset;
  int selectTextColor;
  int textColor;
  private GradientDrawable topShadow;
  private WheelViewAdapter viewAdapter;
  private int visibleItems = 5;
  
  public WheelView(Context paramContext)
  {
    super(paramContext);
    initData(paramContext);
  }
  
  public WheelView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initData(paramContext);
  }
  
  public WheelView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initData(paramContext);
  }
  
  private boolean addViewItem(int paramInt, boolean paramBoolean)
  {
    View localView = getItemView(paramInt);
    refreshTextStatus(localView, paramInt);
    if (localView != null)
    {
      if (paramBoolean) {
        this.itemsLayout.addView(localView, 0);
      } else {
        this.itemsLayout.addView(localView);
      }
      return true;
    }
    return false;
  }
  
  private void buildViewForMeasuring()
  {
    LinearLayout localLinearLayout = this.itemsLayout;
    if (localLinearLayout != null) {
      this.recycle.recycleItems(localLinearLayout, this.firstItem, new ItemsRange());
    } else {
      createItemsLayout();
    }
    int j = this.visibleItems / 2;
    for (int i = this.currentItem + j; i >= this.currentItem - j; i--) {
      if (addViewItem(i, true)) {
        this.firstItem = i;
      }
    }
  }
  
  private int calculateLayoutWidth(int paramInt1, int paramInt2)
  {
    initResourcesIfNecessary();
    this.itemsLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    this.itemsLayout.measure(View.MeasureSpec.makeMeasureSpec(paramInt1, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    int i = this.itemsLayout.getMeasuredWidth();
    if (paramInt2 != 1073741824)
    {
      i = Math.max(i + 20, getSuggestedMinimumWidth());
      if ((paramInt2 != Integer.MIN_VALUE) || (paramInt1 >= i)) {
        paramInt1 = i;
      }
    }
    this.itemsLayout.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 - 20, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
    return paramInt1;
  }
  
  private void createItemsLayout()
  {
    if (this.itemsLayout == null)
    {
      this.itemsLayout = new LinearLayout(getContext());
      this.itemsLayout.setOrientation(1);
    }
  }
  
  private void doScroll(int paramInt)
  {
    this.scrollingOffset += paramInt;
    int n = getItemHeight();
    int j = this.scrollingOffset / n;
    int k = this.currentItem - j;
    int i1 = this.viewAdapter.getItemsCount();
    paramInt = this.scrollingOffset % n;
    int m = paramInt;
    if (Math.abs(paramInt) <= n / 2) {
      m = 0;
    }
    int i;
    if ((this.isCyclic) && (i1 > 0))
    {
      if (m > 0)
      {
        i = k - 1;
        paramInt = j + 1;
      }
      else
      {
        paramInt = j;
        i = k;
        if (m < 0)
        {
          i = k + 1;
          paramInt = j - 1;
        }
      }
      while (i < 0) {
        i += i1;
      }
      i %= i1;
    }
    else if (k < 0)
    {
      paramInt = this.currentItem;
      i = 0;
    }
    else if (k >= i1)
    {
      paramInt = this.currentItem - i1 + 1;
      i = i1 - 1;
    }
    else if ((k > 0) && (m > 0))
    {
      i = k - 1;
      paramInt = j + 1;
    }
    else
    {
      paramInt = j;
      i = k;
      if (k < i1 - 1)
      {
        paramInt = j;
        i = k;
        if (m < 0)
        {
          i = k + 1;
          paramInt = j - 1;
        }
      }
    }
    j = this.scrollingOffset;
    if (i != this.currentItem) {
      setCurrentItem(i, false);
    } else {
      invalidate();
    }
    this.scrollingOffset = (j - paramInt * n);
    if (this.scrollingOffset > getHeight()) {
      this.scrollingOffset = (this.scrollingOffset % getHeight() + getHeight());
    }
  }
  
  private void drawCenterRect(Canvas paramCanvas)
  {
    int i = getHeight() / 2;
    i = getItemHeight() / 2;
  }
  
  private void drawItems(Canvas paramCanvas)
  {
    paramCanvas.save();
    paramCanvas.translate(10.0F, -((this.currentItem - this.firstItem) * getItemHeight() + (getItemHeight() - getHeight()) / 2) + this.scrollingOffset);
    this.itemsLayout.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  private void drawShadows(Canvas paramCanvas)
  {
    double d = getItemHeight();
    Double.isNaN(d);
    int i = (int)(d * 1.5D);
    this.topShadow.setBounds(0, 0, getWidth(), i);
    this.topShadow.draw(paramCanvas);
    this.bottomShadow.setBounds(0, getHeight() - i, getWidth(), getHeight());
    this.bottomShadow.draw(paramCanvas);
  }
  
  private int getDesiredHeight(LinearLayout paramLinearLayout)
  {
    if ((paramLinearLayout != null) && (paramLinearLayout.getChildAt(0) != null)) {
      this.itemHeight = paramLinearLayout.getChildAt(0).getMeasuredHeight();
    }
    int i = this.itemHeight;
    return Math.max(this.visibleItems * i - i * 10 / 50, getSuggestedMinimumHeight());
  }
  
  private View getItemView(int paramInt)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() != 0))
    {
      int j = this.viewAdapter.getItemsCount();
      int i = paramInt;
      if (!isValidItemIndex(paramInt)) {
        return this.viewAdapter.getEmptyItem(this.recycle.getEmptyItem(), this.itemsLayout);
      }
      while (i < 0) {
        i += j;
      }
      return this.viewAdapter.getItem(i % j, this.recycle.getItem(), this.itemsLayout);
    }
    return null;
  }
  
  private ItemsRange getItemsRange()
  {
    if (getItemHeight() == 0) {
      return null;
    }
    int i = this.currentItem;
    for (int j = 1; getItemHeight() * j < getHeight(); j += 2) {
      i--;
    }
    int n = this.scrollingOffset;
    int m = i;
    int k = j;
    if (n != 0)
    {
      k = i;
      if (n > 0) {
        k = i - 1;
      }
      i = this.scrollingOffset / getItemHeight();
      m = k - i;
      double d2 = j + 1;
      double d1 = Math.asin(i);
      Double.isNaN(d2);
      k = (int)(d2 + d1);
    }
    return new ItemsRange(m, k);
  }
  
  private void initData(Context paramContext)
  {
    this.scroller = new WheelScroller(getContext(), this.scrollingListener);
  }
  
  private void initResourcesIfNecessary()
  {
    if (this.topShadow == null) {
      this.topShadow = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, SHADOWS_COLORS);
    }
    if (this.bottomShadow == null) {
      this.bottomShadow = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, SHADOWS_COLORS);
    }
  }
  
  private boolean isValidItemIndex(int paramInt)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    boolean bool;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() > 0) && ((this.isCyclic) || ((paramInt >= 0) && (paramInt < this.viewAdapter.getItemsCount())))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void layout(int paramInt1, int paramInt2)
  {
    this.itemsLayout.layout(0, 0, paramInt1 - 20, paramInt2);
  }
  
  private boolean rebuildItems()
  {
    ItemsRange localItemsRange = getItemsRange();
    LinearLayout localLinearLayout = this.itemsLayout;
    boolean bool2;
    if (localLinearLayout != null)
    {
      i = this.recycle.recycleItems(localLinearLayout, this.firstItem, localItemsRange);
      if (this.firstItem != i) {
        bool2 = true;
      } else {
        bool2 = false;
      }
      this.firstItem = i;
    }
    else
    {
      createItemsLayout();
      bool2 = true;
    }
    boolean bool1 = bool2;
    if (!bool2) {
      if ((this.firstItem == localItemsRange.getFirst()) && (this.itemsLayout.getChildCount() == localItemsRange.getCount())) {
        bool1 = false;
      } else {
        bool1 = true;
      }
    }
    if ((this.firstItem > localItemsRange.getFirst()) && (this.firstItem <= localItemsRange.getLast())) {
      i = this.firstItem - 1;
    }
    while ((i >= localItemsRange.getFirst()) && (addViewItem(i, true)))
    {
      this.firstItem = i;
      i--;
      continue;
      this.firstItem = localItemsRange.getFirst();
    }
    int k = this.firstItem;
    int i = this.itemsLayout.getChildCount();
    while (i < localItemsRange.getCount())
    {
      int j = k;
      if (!addViewItem(this.firstItem + i, false))
      {
        j = k;
        if (this.itemsLayout.getChildCount() == 0) {
          j = k + 1;
        }
      }
      i++;
      k = j;
    }
    this.firstItem = k;
    return bool1;
  }
  
  private void refreshTextStatus(View paramView, int paramInt)
  {
    if (paramView == null) {
      return;
    }
    paramView = (TextView)paramView.findViewById(R.id.text);
    if (paramInt == this.currentItem) {
      paramView.setTextColor(this.selectTextColor);
    } else {
      paramView.setTextColor(this.textColor);
    }
  }
  
  private void updateView()
  {
    if (rebuildItems())
    {
      calculateLayoutWidth(getWidth(), 1073741824);
      layout(getWidth(), getHeight());
    }
  }
  
  public void addChangingListener(OnWheelChangedListener paramOnWheelChangedListener)
  {
    this.changingListeners.add(paramOnWheelChangedListener);
  }
  
  public void addClickingListener(OnWheelClickedListener paramOnWheelClickedListener)
  {
    this.clickingListeners.add(paramOnWheelClickedListener);
  }
  
  public void addScrollingListener(OnWheelScrollListener paramOnWheelScrollListener)
  {
    this.scrollingListeners.add(paramOnWheelScrollListener);
  }
  
  public int getCurrentItem()
  {
    return this.currentItem;
  }
  
  public int getItemHeight()
  {
    int i = this.itemHeight;
    if (i != 0) {
      return i;
    }
    LinearLayout localLinearLayout = this.itemsLayout;
    if ((localLinearLayout != null) && (localLinearLayout.getChildAt(0) != null))
    {
      this.itemHeight = this.itemsLayout.getChildAt(0).getHeight();
      return this.itemHeight;
    }
    return getHeight() / this.visibleItems;
  }
  
  public WheelViewAdapter getViewAdapter()
  {
    return this.viewAdapter;
  }
  
  public int getVisibleItems()
  {
    return this.visibleItems;
  }
  
  public void invalidateWheel(boolean paramBoolean)
  {
    LinearLayout localLinearLayout;
    if (paramBoolean)
    {
      this.recycle.clearAll();
      localLinearLayout = this.itemsLayout;
      if (localLinearLayout != null) {
        localLinearLayout.removeAllViews();
      }
      this.scrollingOffset = 0;
    }
    else
    {
      localLinearLayout = this.itemsLayout;
      if (localLinearLayout != null) {
        this.recycle.recycleItems(localLinearLayout, this.firstItem, new ItemsRange());
      }
    }
    invalidate();
  }
  
  public boolean isCyclic()
  {
    return this.isCyclic;
  }
  
  public boolean isScrollingPerformed()
  {
    return this.isScrollingPerformed;
  }
  
  protected void notifyChangingListeners(int paramInt1, int paramInt2)
  {
    Object localObject = this.changingListeners.iterator();
    while (((Iterator)localObject).hasNext()) {
      ((OnWheelChangedListener)((Iterator)localObject).next()).onChanged(this, paramInt1, paramInt2);
    }
    if ((paramInt1 >= 0) && (paramInt2 >= 0))
    {
      localObject = this.itemsLayout;
      if (localObject != null)
      {
        localObject = ((LinearLayout)localObject).getChildAt(paramInt1 - this.firstItem);
        View localView = this.itemsLayout.getChildAt(paramInt2 - this.firstItem);
        refreshTextStatus((View)localObject, paramInt1);
        refreshTextStatus(localView, paramInt2);
        return;
      }
    }
  }
  
  protected void notifyClickListenersAboutClick(int paramInt)
  {
    Iterator localIterator = this.clickingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelClickedListener)localIterator.next()).onItemClicked(this, paramInt);
    }
  }
  
  protected void notifyScrollingListenersAboutEnd()
  {
    Iterator localIterator = this.scrollingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelScrollListener)localIterator.next()).onScrollingFinished(this);
    }
  }
  
  protected void notifyScrollingListenersAboutStart()
  {
    Iterator localIterator = this.scrollingListeners.iterator();
    while (localIterator.hasNext()) {
      ((OnWheelScrollListener)localIterator.next()).onScrollingStarted(this);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() > 0))
    {
      updateView();
      drawItems(paramCanvas);
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    layout(paramInt3 - paramInt1, paramInt4 - paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.getMode(paramInt1);
    int i = View.MeasureSpec.getMode(paramInt2);
    int k = View.MeasureSpec.getSize(paramInt1);
    paramInt1 = View.MeasureSpec.getSize(paramInt2);
    buildViewForMeasuring();
    j = calculateLayoutWidth(k, j);
    if (i != 1073741824)
    {
      paramInt2 = getDesiredHeight(this.itemsLayout);
      if (i == Integer.MIN_VALUE) {
        paramInt1 = Math.min(paramInt2, paramInt1);
      } else {
        paramInt1 = paramInt2;
      }
    }
    setMeasuredDimension(j, paramInt1);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((isEnabled()) && (getViewAdapter() != null))
    {
      switch (paramMotionEvent.getAction())
      {
      default: 
        break;
      case 2: 
        if (getParent() != null) {
          getParent().requestDisallowInterceptTouchEvent(true);
        }
        break;
      case 1: 
        if (!this.isScrollingPerformed)
        {
          int i = (int)paramMotionEvent.getY() - getHeight() / 2;
          if (i > 0) {
            i += getItemHeight() / 2;
          } else {
            i -= getItemHeight() / 2;
          }
          i /= getItemHeight();
          if ((i != 0) && (isValidItemIndex(this.currentItem + i))) {
            notifyClickListenersAboutClick(this.currentItem + i);
          }
        }
        break;
      }
      return this.scroller.onTouchEvent(paramMotionEvent);
    }
    return true;
  }
  
  public void removeChangingListener(OnWheelChangedListener paramOnWheelChangedListener)
  {
    this.changingListeners.remove(paramOnWheelChangedListener);
  }
  
  public void removeClickingListener(OnWheelClickedListener paramOnWheelClickedListener)
  {
    this.clickingListeners.remove(paramOnWheelClickedListener);
  }
  
  public void removeScrollingListener(OnWheelScrollListener paramOnWheelScrollListener)
  {
    this.scrollingListeners.remove(paramOnWheelScrollListener);
  }
  
  public void scroll(int paramInt1, int paramInt2)
  {
    int i = getItemHeight();
    int j = this.scrollingOffset;
    this.scroller.scroll(paramInt1 * i - j, paramInt2);
  }
  
  public void setCurrentItem(int paramInt)
  {
    setCurrentItem(paramInt, false);
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if ((localWheelViewAdapter != null) && (localWheelViewAdapter.getItemsCount() != 0))
    {
      int k = this.viewAdapter.getItemsCount();
      int i;
      if (paramInt >= 0)
      {
        i = paramInt;
        if (paramInt < k) {}
      }
      else
      {
        if (!this.isCyclic) {
          break label181;
        }
        while (paramInt < 0) {
          paramInt += k;
        }
        i = paramInt % k;
      }
      int m = this.currentItem;
      if (i != m) {
        if (paramBoolean)
        {
          int j = i - m;
          paramInt = j;
          if (this.isCyclic)
          {
            i = k + Math.min(i, m) - Math.max(i, this.currentItem);
            paramInt = j;
            if (i < Math.abs(j)) {
              if (j < 0) {
                paramInt = i;
              } else {
                paramInt = -i;
              }
            }
          }
          scroll(paramInt, 0);
        }
        else
        {
          this.scrollingOffset = 0;
          this.currentItem = i;
          notifyChangingListeners(m, this.currentItem);
          invalidate();
        }
      }
      return;
      label181:
      return;
    }
  }
  
  public void setCyclic(boolean paramBoolean)
  {
    this.isCyclic = paramBoolean;
    invalidateWheel(false);
  }
  
  public void setInterpolator(Interpolator paramInterpolator)
  {
    this.scroller.setInterpolator(paramInterpolator);
  }
  
  public void setSelectTextColor(int paramInt1, int paramInt2)
  {
    this.selectTextColor = paramInt2;
    this.textColor = paramInt1;
  }
  
  public void setViewAdapter(WheelViewAdapter paramWheelViewAdapter)
  {
    WheelViewAdapter localWheelViewAdapter = this.viewAdapter;
    if (localWheelViewAdapter != null) {
      localWheelViewAdapter.unregisterDataSetObserver(this.dataObserver);
    }
    this.viewAdapter = paramWheelViewAdapter;
    paramWheelViewAdapter = this.viewAdapter;
    if (paramWheelViewAdapter != null) {
      paramWheelViewAdapter.registerDataSetObserver(this.dataObserver);
    }
    invalidateWheel(true);
  }
  
  public void setVisibleItems(int paramInt)
  {
    this.visibleItems = paramInt;
  }
  
  public void stopScrolling()
  {
    this.scroller.stopScrolling();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/view/WheelView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */