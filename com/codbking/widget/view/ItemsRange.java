package com.codbking.widget.view;

public class ItemsRange
{
  private int count;
  private int first;
  
  public ItemsRange()
  {
    this(0, 0);
  }
  
  public ItemsRange(int paramInt1, int paramInt2)
  {
    this.first = paramInt1;
    this.count = paramInt2;
  }
  
  public boolean contains(int paramInt)
  {
    boolean bool;
    if ((paramInt >= getFirst()) && (paramInt <= getLast())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int getCount()
  {
    return this.count;
  }
  
  public int getFirst()
  {
    return this.first;
  }
  
  public int getLast()
  {
    return getFirst() + getCount() - 1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/view/ItemsRange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */