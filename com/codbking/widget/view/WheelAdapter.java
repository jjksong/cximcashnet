package com.codbking.widget.view;

public abstract interface WheelAdapter
{
  public abstract String getItem(int paramInt);
  
  public abstract int getItemsCount();
  
  public abstract int getMaximumLength();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/view/WheelAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */