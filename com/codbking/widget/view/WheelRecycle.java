package com.codbking.widget.view;

import android.view.View;
import android.widget.LinearLayout;
import com.codbking.widget.adapters.WheelViewAdapter;
import java.util.LinkedList;
import java.util.List;

public class WheelRecycle
{
  private List<View> emptyItems;
  private List<View> items;
  private WheelView wheel;
  
  public WheelRecycle(WheelView paramWheelView)
  {
    this.wheel = paramWheelView;
  }
  
  private List<View> addView(View paramView, List<View> paramList)
  {
    Object localObject = paramList;
    if (paramList == null) {
      localObject = new LinkedList();
    }
    ((List)localObject).add(paramView);
    return (List<View>)localObject;
  }
  
  private View getCachedView(List<View> paramList)
  {
    if ((paramList != null) && (paramList.size() > 0))
    {
      View localView = (View)paramList.get(0);
      paramList.remove(0);
      return localView;
    }
    return null;
  }
  
  private void recycleView(View paramView, int paramInt)
  {
    int j = this.wheel.getViewAdapter().getItemsCount();
    int i;
    if (paramInt >= 0)
    {
      i = paramInt;
      if (paramInt < j) {}
    }
    else
    {
      i = paramInt;
      if (!this.wheel.isCyclic())
      {
        this.emptyItems = addView(paramView, this.emptyItems);
        return;
      }
    }
    while (i < 0) {
      i += j;
    }
    this.items = addView(paramView, this.items);
  }
  
  public void clearAll()
  {
    List localList = this.items;
    if (localList != null) {
      localList.clear();
    }
    localList = this.emptyItems;
    if (localList != null) {
      localList.clear();
    }
  }
  
  public View getEmptyItem()
  {
    return getCachedView(this.emptyItems);
  }
  
  public View getItem()
  {
    return getCachedView(this.items);
  }
  
  public int recycleItems(LinearLayout paramLinearLayout, int paramInt, ItemsRange paramItemsRange)
  {
    int k = 0;
    int j = paramInt;
    int i = paramInt;
    paramInt = j;
    while (k < paramLinearLayout.getChildCount())
    {
      int m;
      if (!paramItemsRange.contains(paramInt))
      {
        recycleView(paramLinearLayout.getChildAt(k), paramInt);
        paramLinearLayout.removeViewAt(k);
        m = k;
        j = i;
        if (k == 0)
        {
          j = i + 1;
          m = k;
        }
      }
      else
      {
        m = k + 1;
        j = i;
      }
      paramInt++;
      k = m;
      i = j;
    }
    return i;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/view/WheelRecycle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */