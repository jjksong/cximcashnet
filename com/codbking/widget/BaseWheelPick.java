package com.codbking.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.codbking.widget.genview.GenWheelText;
import com.codbking.widget.genview.WheelGeneralAdapter;
import com.codbking.widget.view.OnWheelChangedListener;
import com.codbking.widget.view.OnWheelScrollListener;
import com.codbking.widget.view.WheelView;

abstract class BaseWheelPick
  extends LinearLayout
  implements OnWheelChangedListener, OnWheelScrollListener
{
  protected Context ctx;
  private GenWheelText genView;
  protected int selectColor = -12303292;
  protected int split = -2236963;
  protected int splitHeight = 1;
  protected int textColor = -2236963;
  
  public BaseWheelPick(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public BaseWheelPick(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DatePicker);
    this.textColor = paramAttributeSet.getColor(R.styleable.DatePicker_picker_text_color, -2236963);
    this.selectColor = paramAttributeSet.getColor(R.styleable.DatePicker_picker_select_textColor, -12303292);
    this.split = paramAttributeSet.getColor(R.styleable.DatePicker_picker_split, -2236963);
    this.splitHeight = ((int)paramAttributeSet.getDimension(R.styleable.DatePicker_picker_split_height, 0.5F));
    paramAttributeSet.recycle();
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.genView = new GenWheelText(this.textColor);
    this.ctx = paramContext;
    LayoutInflater.from(paramContext).inflate(getLayout(), this);
  }
  
  protected String[] convertData(WheelView paramWheelView, Integer[] paramArrayOfInteger)
  {
    return new String[0];
  }
  
  protected abstract int getItemHeight();
  
  protected abstract int getLayout();
  
  public void onChanged(WheelView paramWheelView, int paramInt1, int paramInt2) {}
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    Paint localPaint = new Paint();
    localPaint.setStrokeWidth(this.splitHeight);
    localPaint.setColor(this.split);
    localPaint.setAntiAlias(true);
    localPaint.setStyle(Paint.Style.FILL);
    int j = getItemHeight();
    int i = 0;
    while (i < 5)
    {
      i++;
      float f = i * j;
      paramCanvas.drawLine(0.0F, f, getWidth(), f, localPaint);
    }
  }
  
  protected abstract void setData(Object[] paramArrayOfObject);
  
  protected void setWheelListener(WheelView paramWheelView, Object[] paramArrayOfObject, boolean paramBoolean)
  {
    WheelGeneralAdapter localWheelGeneralAdapter = new WheelGeneralAdapter(this.ctx, this.genView);
    if ((paramArrayOfObject[0] instanceof Integer)) {
      localWheelGeneralAdapter.setData(convertData(paramWheelView, (Integer[])paramArrayOfObject));
    } else {
      localWheelGeneralAdapter.setData(paramArrayOfObject);
    }
    paramWheelView.setSelectTextColor(this.textColor, this.selectColor);
    paramWheelView.setCyclic(paramBoolean);
    paramWheelView.setViewAdapter(localWheelGeneralAdapter);
    paramWheelView.addChangingListener(this);
    paramWheelView.addScrollingListener(this);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/BaseWheelPick.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */