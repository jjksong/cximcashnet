package com.codbking.widget.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Locale;

public class UIAdjuster
{
  public static void addViewIfNotNull(ViewGroup paramViewGroup, View paramView)
  {
    if (paramView != null) {
      paramViewGroup.addView(paramView);
    }
  }
  
  public static void closeKeyBoard(Context paramContext)
  {
    if ((paramContext != null) && ((paramContext instanceof Activity))) {
      closeKeyBoardInner((Activity)paramContext);
    }
  }
  
  private static void closeKeyBoardInner(Activity paramActivity)
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)paramActivity.getSystemService("input_method");
    if ((paramActivity.getCurrentFocus() != null) && (paramActivity.getCurrentFocus().getWindowToken() != null)) {
      localInputMethodManager.hideSoftInputFromWindow(paramActivity.getCurrentFocus().getWindowToken(), 2);
    }
  }
  
  public static float computeDIPtoPixel(Context paramContext, float paramFloat)
  {
    if (paramContext == null) {
      paramContext = Resources.getSystem();
    } else {
      paramContext = paramContext.getResources();
    }
    return paramContext.getDisplayMetrics().density * paramFloat;
  }
  
  public static float computeDynamicWidth(TextView paramTextView, int paramInt)
  {
    Rect localRect = new Rect();
    return computeDynamicWidth(paramTextView, paramInt, new Paint(), localRect);
  }
  
  public static float computeDynamicWidth(TextView paramTextView, int paramInt, Paint paramPaint, Rect paramRect)
  {
    String str = (String)paramTextView.getText();
    for (float f = paramTextView.getTextSize();; f -= 1.0F)
    {
      paramPaint.setTextSize(f);
      paramPaint.getTextBounds(str, 0, str.length(), paramRect);
      if (paramRect.width() <= paramInt) {
        break;
      }
    }
    return f;
  }
  
  public static int computeStringWidth(String paramString, float paramFloat)
  {
    Rect localRect = new Rect();
    Paint localPaint = new Paint();
    localPaint.setTextSize(paramFloat);
    localPaint.getTextBounds(paramString, 0, paramString.length(), localRect);
    return localRect.width();
  }
  
  public static boolean getLanguage(Context paramContext)
  {
    paramContext = paramContext.getResources().getConfiguration();
    return (paramContext.locale.equals(Locale.TAIWAN)) || (paramContext.locale.equals(Locale.TRADITIONAL_CHINESE));
  }
  
  public static String getLocale(Context paramContext)
  {
    paramContext = paramContext.getResources().getConfiguration();
    if ((!paramContext.locale.equals(Locale.TAIWAN)) && (!paramContext.locale.equals(Locale.TRADITIONAL_CHINESE))) {
      return "en_US";
    }
    return "zh_TW";
  }
  
  public static int setListViewHeightBasedOnChildren(ListView paramListView)
  {
    ListAdapter localListAdapter = paramListView.getAdapter();
    if (localListAdapter == null) {
      return 0;
    }
    int j = 0;
    int i = 0;
    while (j < localListAdapter.getCount())
    {
      localObject = localListAdapter.getView(j, null, paramListView);
      ((View)localObject).measure(0, 0);
      i += ((View)localObject).getMeasuredHeight();
      j++;
    }
    Object localObject = paramListView.getLayoutParams();
    ((ViewGroup.LayoutParams)localObject).height = (i + paramListView.getDividerHeight() * (localListAdapter.getCount() - 1) + paramListView.getPaddingTop() + paramListView.getPaddingBottom());
    i = ((ViewGroup.LayoutParams)localObject).height;
    paramListView.setLayoutParams((ViewGroup.LayoutParams)localObject);
    paramListView.requestLayout();
    return i;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/utils/UIAdjuster.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */