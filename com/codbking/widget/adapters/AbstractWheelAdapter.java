package com.codbking.widget.adapters;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractWheelAdapter
  implements WheelViewAdapter
{
  private List<DataSetObserver> datasetObservers;
  
  public View getEmptyItem(View paramView, ViewGroup paramViewGroup)
  {
    return null;
  }
  
  protected void notifyDataChangedEvent()
  {
    Object localObject = this.datasetObservers;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        ((DataSetObserver)((Iterator)localObject).next()).onChanged();
      }
    }
  }
  
  protected void notifyDataInvalidatedEvent()
  {
    Object localObject = this.datasetObservers;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        ((DataSetObserver)((Iterator)localObject).next()).onInvalidated();
      }
    }
  }
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    if (this.datasetObservers == null) {
      this.datasetObservers = new LinkedList();
    }
    this.datasetObservers.add(paramDataSetObserver);
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    List localList = this.datasetObservers;
    if (localList != null) {
      localList.remove(paramDataSetObserver);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/adapters/AbstractWheelAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */