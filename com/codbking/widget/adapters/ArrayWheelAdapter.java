package com.codbking.widget.adapters;

import android.content.Context;

public class ArrayWheelAdapter<T>
  extends AbstractWheelTextAdapter
{
  private T[] items;
  
  public ArrayWheelAdapter(Context paramContext, T[] paramArrayOfT)
  {
    super(paramContext);
    this.items = paramArrayOfT;
  }
  
  public CharSequence getItemText(int paramInt)
  {
    if (paramInt >= 0)
    {
      Object localObject = this.items;
      if (paramInt < localObject.length)
      {
        localObject = localObject[paramInt];
        if ((localObject instanceof CharSequence)) {
          return (CharSequence)localObject;
        }
        return localObject.toString();
      }
    }
    return null;
  }
  
  public int getItemsCount()
  {
    return this.items.length;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/adapters/ArrayWheelAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */