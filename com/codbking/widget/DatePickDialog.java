package com.codbking.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.codbking.widget.bean.DateType;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatePickDialog
  extends Dialog
  implements OnChangeLisener
{
  private TextView cancel;
  private String format;
  private DatePicker mDatePicker;
  private TextView messgeTv;
  private OnChangeLisener onChangeLisener;
  private OnSureLisener onSureLisener;
  private Date startDate = new Date();
  private TextView sure;
  private String title;
  private TextView titleTv;
  private DateType type = DateType.TYPE_ALL;
  private FrameLayout wheelLayout;
  private int yearLimt = 5;
  
  public DatePickDialog(Context paramContext)
  {
    super(paramContext, R.style.dialog_style);
  }
  
  private DatePicker getDatePicker()
  {
    DatePicker localDatePicker = new DatePicker(getContext(), this.type);
    localDatePicker.setStartDate(this.startDate);
    localDatePicker.setYearLimt(this.yearLimt);
    localDatePicker.setOnChangeLisener(this);
    localDatePicker.init();
    return localDatePicker;
  }
  
  private void initParas()
  {
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.gravity = 80;
    localLayoutParams.width = DateUtils.getScreenWidth(getContext());
    getWindow().setAttributes(localLayoutParams);
  }
  
  private void initView()
  {
    this.sure = ((TextView)findViewById(R.id.sure));
    this.cancel = ((TextView)findViewById(R.id.cancel));
    this.wheelLayout = ((FrameLayout)findViewById(R.id.wheelLayout));
    this.titleTv = ((TextView)findViewById(R.id.title));
    this.messgeTv = ((TextView)findViewById(R.id.message));
    this.mDatePicker = getDatePicker();
    this.wheelLayout.addView(this.mDatePicker);
    this.titleTv.setText(this.title);
    this.cancel.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DatePickDialog.this.dismiss();
      }
    });
    this.sure.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        DatePickDialog.this.dismiss();
        if (DatePickDialog.this.onSureLisener != null) {
          DatePickDialog.this.onSureLisener.onSure(DatePickDialog.this.mDatePicker.getSelectDate());
        }
      }
    });
  }
  
  public void onChanged(Date paramDate)
  {
    Object localObject = this.onChangeLisener;
    if (localObject != null) {
      ((OnChangeLisener)localObject).onChanged(paramDate);
    }
    if (!TextUtils.isEmpty(this.format))
    {
      localObject = "";
      try
      {
        SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
        localSimpleDateFormat.<init>(this.format);
        paramDate = localSimpleDateFormat.format(paramDate);
      }
      catch (Exception paramDate)
      {
        paramDate.printStackTrace();
        paramDate = (Date)localObject;
      }
      this.messgeTv.setText(paramDate);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.cbk_dialog_pick_time);
    initView();
    initParas();
  }
  
  public void setMessageFormat(String paramString)
  {
    this.format = paramString;
  }
  
  public void setOnChangeLisener(OnChangeLisener paramOnChangeLisener)
  {
    this.onChangeLisener = paramOnChangeLisener;
  }
  
  public void setOnSureLisener(OnSureLisener paramOnSureLisener)
  {
    this.onSureLisener = paramOnSureLisener;
  }
  
  public void setStartDate(Date paramDate)
  {
    this.startDate = paramDate;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(DateType paramDateType)
  {
    this.type = paramDateType;
  }
  
  public void setYearLimt(int paramInt)
  {
    this.yearLimt = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/codbking/widget/DatePickDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */