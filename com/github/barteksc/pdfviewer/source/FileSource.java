package com.github.barteksc.pdfviewer.source;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import java.io.File;
import java.io.IOException;

public class FileSource
  implements DocumentSource
{
  private File file;
  
  public FileSource(File paramFile)
  {
    this.file = paramFile;
  }
  
  public PdfDocument createDocument(Context paramContext, PdfiumCore paramPdfiumCore, String paramString)
    throws IOException
  {
    return paramPdfiumCore.newDocument(ParcelFileDescriptor.open(this.file, 268435456), paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/source/FileSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */