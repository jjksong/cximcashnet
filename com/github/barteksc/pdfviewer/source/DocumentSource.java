package com.github.barteksc.pdfviewer.source;

import android.content.Context;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import java.io.IOException;

public abstract interface DocumentSource
{
  public abstract PdfDocument createDocument(Context paramContext, PdfiumCore paramPdfiumCore, String paramString)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/source/DocumentSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */