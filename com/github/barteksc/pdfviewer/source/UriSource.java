package com.github.barteksc.pdfviewer.source;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import java.io.IOException;

public class UriSource
  implements DocumentSource
{
  private Uri uri;
  
  public UriSource(Uri paramUri)
  {
    this.uri = paramUri;
  }
  
  public PdfDocument createDocument(Context paramContext, PdfiumCore paramPdfiumCore, String paramString)
    throws IOException
  {
    return paramPdfiumCore.newDocument(paramContext.getContentResolver().openFileDescriptor(this.uri, "r"), paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/source/UriSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */