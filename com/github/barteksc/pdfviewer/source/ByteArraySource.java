package com.github.barteksc.pdfviewer.source;

import android.content.Context;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import java.io.IOException;

public class ByteArraySource
  implements DocumentSource
{
  private byte[] data;
  
  public ByteArraySource(byte[] paramArrayOfByte)
  {
    this.data = paramArrayOfByte;
  }
  
  public PdfDocument createDocument(Context paramContext, PdfiumCore paramPdfiumCore, String paramString)
    throws IOException
  {
    return paramPdfiumCore.newDocument(this.data, paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/source/ByteArraySource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */