package com.github.barteksc.pdfviewer.listener;

public abstract interface OnErrorListener
{
  public abstract void onError(Throwable paramThrowable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/listener/OnErrorListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */