package com.github.barteksc.pdfviewer.listener;

import android.view.MotionEvent;
import com.github.barteksc.pdfviewer.link.LinkHandler;
import com.github.barteksc.pdfviewer.model.LinkTapEvent;

public class Callbacks
{
  private LinkHandler linkHandler;
  private OnDrawListener onDrawAllListener;
  private OnDrawListener onDrawListener;
  private OnErrorListener onErrorListener;
  private OnLoadCompleteListener onLoadCompleteListener;
  private OnLongPressListener onLongPressListener;
  private OnPageChangeListener onPageChangeListener;
  private OnPageErrorListener onPageErrorListener;
  private OnPageScrollListener onPageScrollListener;
  private OnRenderListener onRenderListener;
  private OnTapListener onTapListener;
  
  public void callLinkHandler(LinkTapEvent paramLinkTapEvent)
  {
    LinkHandler localLinkHandler = this.linkHandler;
    if (localLinkHandler != null) {
      localLinkHandler.handleLinkEvent(paramLinkTapEvent);
    }
  }
  
  public void callOnLoadComplete(int paramInt)
  {
    OnLoadCompleteListener localOnLoadCompleteListener = this.onLoadCompleteListener;
    if (localOnLoadCompleteListener != null) {
      localOnLoadCompleteListener.loadComplete(paramInt);
    }
  }
  
  public void callOnLongPress(MotionEvent paramMotionEvent)
  {
    OnLongPressListener localOnLongPressListener = this.onLongPressListener;
    if (localOnLongPressListener != null) {
      localOnLongPressListener.onLongPress(paramMotionEvent);
    }
  }
  
  public void callOnPageChange(int paramInt1, int paramInt2)
  {
    OnPageChangeListener localOnPageChangeListener = this.onPageChangeListener;
    if (localOnPageChangeListener != null) {
      localOnPageChangeListener.onPageChanged(paramInt1, paramInt2);
    }
  }
  
  public boolean callOnPageError(int paramInt, Throwable paramThrowable)
  {
    OnPageErrorListener localOnPageErrorListener = this.onPageErrorListener;
    if (localOnPageErrorListener != null)
    {
      localOnPageErrorListener.onPageError(paramInt, paramThrowable);
      return true;
    }
    return false;
  }
  
  public void callOnPageScroll(int paramInt, float paramFloat)
  {
    OnPageScrollListener localOnPageScrollListener = this.onPageScrollListener;
    if (localOnPageScrollListener != null) {
      localOnPageScrollListener.onPageScrolled(paramInt, paramFloat);
    }
  }
  
  public void callOnRender(int paramInt)
  {
    OnRenderListener localOnRenderListener = this.onRenderListener;
    if (localOnRenderListener != null) {
      localOnRenderListener.onInitiallyRendered(paramInt);
    }
  }
  
  public boolean callOnTap(MotionEvent paramMotionEvent)
  {
    OnTapListener localOnTapListener = this.onTapListener;
    boolean bool;
    if ((localOnTapListener != null) && (localOnTapListener.onTap(paramMotionEvent))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public OnDrawListener getOnDraw()
  {
    return this.onDrawListener;
  }
  
  public OnDrawListener getOnDrawAll()
  {
    return this.onDrawAllListener;
  }
  
  public OnErrorListener getOnError()
  {
    return this.onErrorListener;
  }
  
  public void setLinkHandler(LinkHandler paramLinkHandler)
  {
    this.linkHandler = paramLinkHandler;
  }
  
  public void setOnDraw(OnDrawListener paramOnDrawListener)
  {
    this.onDrawListener = paramOnDrawListener;
  }
  
  public void setOnDrawAll(OnDrawListener paramOnDrawListener)
  {
    this.onDrawAllListener = paramOnDrawListener;
  }
  
  public void setOnError(OnErrorListener paramOnErrorListener)
  {
    this.onErrorListener = paramOnErrorListener;
  }
  
  public void setOnLoadComplete(OnLoadCompleteListener paramOnLoadCompleteListener)
  {
    this.onLoadCompleteListener = paramOnLoadCompleteListener;
  }
  
  public void setOnLongPress(OnLongPressListener paramOnLongPressListener)
  {
    this.onLongPressListener = paramOnLongPressListener;
  }
  
  public void setOnPageChange(OnPageChangeListener paramOnPageChangeListener)
  {
    this.onPageChangeListener = paramOnPageChangeListener;
  }
  
  public void setOnPageError(OnPageErrorListener paramOnPageErrorListener)
  {
    this.onPageErrorListener = paramOnPageErrorListener;
  }
  
  public void setOnPageScroll(OnPageScrollListener paramOnPageScrollListener)
  {
    this.onPageScrollListener = paramOnPageScrollListener;
  }
  
  public void setOnRender(OnRenderListener paramOnRenderListener)
  {
    this.onRenderListener = paramOnRenderListener;
  }
  
  public void setOnTap(OnTapListener paramOnTapListener)
  {
    this.onTapListener = paramOnTapListener;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/listener/Callbacks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */