package com.github.barteksc.pdfviewer.listener;

import android.view.MotionEvent;

public abstract interface OnTapListener
{
  public abstract boolean onTap(MotionEvent paramMotionEvent);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/listener/OnTapListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */