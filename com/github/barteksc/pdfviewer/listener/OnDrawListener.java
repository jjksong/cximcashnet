package com.github.barteksc.pdfviewer.listener;

import android.graphics.Canvas;

public abstract interface OnDrawListener
{
  public abstract void onLayerDrawn(Canvas paramCanvas, float paramFloat1, float paramFloat2, int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/listener/OnDrawListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */