package com.github.barteksc.pdfviewer.listener;

public abstract interface OnPageScrollListener
{
  public abstract void onPageScrolled(int paramInt, float paramFloat);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/listener/OnPageScrollListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */