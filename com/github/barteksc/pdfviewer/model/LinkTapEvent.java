package com.github.barteksc.pdfviewer.model;

import android.graphics.RectF;
import com.shockwave.pdfium.PdfDocument.Link;

public class LinkTapEvent
{
  private float documentX;
  private float documentY;
  private PdfDocument.Link link;
  private RectF mappedLinkRect;
  private float originalX;
  private float originalY;
  
  public LinkTapEvent(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, RectF paramRectF, PdfDocument.Link paramLink)
  {
    this.originalX = paramFloat1;
    this.originalY = paramFloat2;
    this.documentX = paramFloat3;
    this.documentY = paramFloat4;
    this.mappedLinkRect = paramRectF;
    this.link = paramLink;
  }
  
  public float getDocumentX()
  {
    return this.documentX;
  }
  
  public float getDocumentY()
  {
    return this.documentY;
  }
  
  public PdfDocument.Link getLink()
  {
    return this.link;
  }
  
  public RectF getMappedLinkRect()
  {
    return this.mappedLinkRect;
  }
  
  public float getOriginalX()
  {
    return this.originalX;
  }
  
  public float getOriginalY()
  {
    return this.originalY;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/model/LinkTapEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */