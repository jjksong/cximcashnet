package com.github.barteksc.pdfviewer.model;

import android.graphics.Bitmap;
import android.graphics.RectF;

public class PagePart
{
  private int cacheOrder;
  private int page;
  private RectF pageRelativeBounds;
  private Bitmap renderedBitmap;
  private boolean thumbnail;
  
  public PagePart(int paramInt1, Bitmap paramBitmap, RectF paramRectF, boolean paramBoolean, int paramInt2)
  {
    this.page = paramInt1;
    this.renderedBitmap = paramBitmap;
    this.pageRelativeBounds = paramRectF;
    this.thumbnail = paramBoolean;
    this.cacheOrder = paramInt2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof PagePart;
    boolean bool2 = false;
    if (!bool1) {
      return false;
    }
    paramObject = (PagePart)paramObject;
    bool1 = bool2;
    if (((PagePart)paramObject).getPage() == this.page)
    {
      bool1 = bool2;
      if (((PagePart)paramObject).getPageRelativeBounds().left == this.pageRelativeBounds.left)
      {
        bool1 = bool2;
        if (((PagePart)paramObject).getPageRelativeBounds().right == this.pageRelativeBounds.right)
        {
          bool1 = bool2;
          if (((PagePart)paramObject).getPageRelativeBounds().top == this.pageRelativeBounds.top)
          {
            bool1 = bool2;
            if (((PagePart)paramObject).getPageRelativeBounds().bottom == this.pageRelativeBounds.bottom) {
              bool1 = true;
            }
          }
        }
      }
    }
    return bool1;
  }
  
  public int getCacheOrder()
  {
    return this.cacheOrder;
  }
  
  public int getPage()
  {
    return this.page;
  }
  
  public RectF getPageRelativeBounds()
  {
    return this.pageRelativeBounds;
  }
  
  public Bitmap getRenderedBitmap()
  {
    return this.renderedBitmap;
  }
  
  public boolean isThumbnail()
  {
    return this.thumbnail;
  }
  
  public void setCacheOrder(int paramInt)
  {
    this.cacheOrder = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/model/PagePart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */