package com.github.barteksc.pdfviewer;

import android.os.AsyncTask;
import com.github.barteksc.pdfviewer.source.DocumentSource;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.util.Size;

class DecodingAsyncTask
  extends AsyncTask<Void, Void, Throwable>
{
  private boolean cancelled;
  private DocumentSource docSource;
  private String password;
  private PdfFile pdfFile;
  private PDFView pdfView;
  private PdfiumCore pdfiumCore;
  private int[] userPages;
  
  DecodingAsyncTask(DocumentSource paramDocumentSource, String paramString, int[] paramArrayOfInt, PDFView paramPDFView, PdfiumCore paramPdfiumCore)
  {
    this.docSource = paramDocumentSource;
    this.userPages = paramArrayOfInt;
    this.cancelled = false;
    this.pdfView = paramPDFView;
    this.password = paramString;
    this.pdfiumCore = paramPdfiumCore;
  }
  
  private Size getViewSize()
  {
    return new Size(this.pdfView.getWidth(), this.pdfView.getHeight());
  }
  
  protected Throwable doInBackground(Void... paramVarArgs)
  {
    try
    {
      PdfDocument localPdfDocument = this.docSource.createDocument(this.pdfView.getContext(), this.pdfiumCore, this.password);
      paramVarArgs = new com/github/barteksc/pdfviewer/PdfFile;
      paramVarArgs.<init>(this.pdfiumCore, localPdfDocument, this.pdfView.getPageFitPolicy(), getViewSize(), this.userPages, this.pdfView.isSwipeVertical(), this.pdfView.getSpacingPx(), this.pdfView.doAutoSpacing());
      this.pdfFile = paramVarArgs;
      return null;
    }
    catch (Throwable paramVarArgs) {}
    return paramVarArgs;
  }
  
  protected void onCancelled()
  {
    this.cancelled = true;
  }
  
  protected void onPostExecute(Throwable paramThrowable)
  {
    if (paramThrowable != null)
    {
      this.pdfView.loadError(paramThrowable);
      return;
    }
    if (!this.cancelled) {
      this.pdfView.loadComplete(this.pdfFile);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/DecodingAsyncTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */