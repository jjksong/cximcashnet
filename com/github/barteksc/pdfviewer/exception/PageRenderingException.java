package com.github.barteksc.pdfviewer.exception;

public class PageRenderingException
  extends Exception
{
  private final int page;
  
  public PageRenderingException(int paramInt, Throwable paramThrowable)
  {
    super(paramThrowable);
    this.page = paramInt;
  }
  
  public int getPage()
  {
    return this.page;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/exception/PageRenderingException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */