package com.github.barteksc.pdfviewer.exception;

@Deprecated
public class FileNotFoundException
  extends RuntimeException
{
  public FileNotFoundException(String paramString)
  {
    super(paramString);
  }
  
  public FileNotFoundException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/exception/FileNotFoundException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */