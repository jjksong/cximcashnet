package com.github.barteksc.pdfviewer;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import com.github.barteksc.pdfviewer.model.PagePart;
import com.github.barteksc.pdfviewer.util.Constants.Cache;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

class CacheManager
{
  private final PriorityQueue<PagePart> activeCache = new PriorityQueue(Constants.Cache.CACHE_SIZE, this.orderComparator);
  private final PagePartComparator orderComparator = new PagePartComparator();
  private final Object passiveActiveLock = new Object();
  private final PriorityQueue<PagePart> passiveCache = new PriorityQueue(Constants.Cache.CACHE_SIZE, this.orderComparator);
  private final List<PagePart> thumbnails = new ArrayList();
  
  private void addWithoutDuplicates(Collection<PagePart> paramCollection, PagePart paramPagePart)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext()) {
      if (((PagePart)localIterator.next()).equals(paramPagePart))
      {
        paramPagePart.getRenderedBitmap().recycle();
        return;
      }
    }
    paramCollection.add(paramPagePart);
  }
  
  @Nullable
  private static PagePart find(PriorityQueue<PagePart> paramPriorityQueue, PagePart paramPagePart)
  {
    Iterator localIterator = paramPriorityQueue.iterator();
    while (localIterator.hasNext())
    {
      paramPriorityQueue = (PagePart)localIterator.next();
      if (paramPriorityQueue.equals(paramPagePart)) {
        return paramPriorityQueue;
      }
    }
    return null;
  }
  
  private void makeAFreeSpace()
  {
    synchronized (this.passiveActiveLock)
    {
      while ((this.activeCache.size() + this.passiveCache.size() >= Constants.Cache.CACHE_SIZE) && (!this.passiveCache.isEmpty())) {
        ((PagePart)this.passiveCache.poll()).getRenderedBitmap().recycle();
      }
      while ((this.activeCache.size() + this.passiveCache.size() >= Constants.Cache.CACHE_SIZE) && (!this.activeCache.isEmpty())) {
        ((PagePart)this.activeCache.poll()).getRenderedBitmap().recycle();
      }
      return;
    }
  }
  
  public void cachePart(PagePart paramPagePart)
  {
    synchronized (this.passiveActiveLock)
    {
      makeAFreeSpace();
      this.activeCache.offer(paramPagePart);
      return;
    }
  }
  
  public void cacheThumbnail(PagePart paramPagePart)
  {
    synchronized (this.thumbnails)
    {
      while (this.thumbnails.size() >= Constants.Cache.THUMBNAILS_CACHE_SIZE) {
        ((PagePart)this.thumbnails.remove(0)).getRenderedBitmap().recycle();
      }
      addWithoutDuplicates(this.thumbnails, paramPagePart);
      return;
    }
  }
  
  public boolean containsThumbnail(int paramInt, RectF arg2)
  {
    PagePart localPagePart = new PagePart(paramInt, null, ???, true, 0);
    synchronized (this.thumbnails)
    {
      Iterator localIterator = this.thumbnails.iterator();
      while (localIterator.hasNext()) {
        if (((PagePart)localIterator.next()).equals(localPagePart)) {
          return true;
        }
      }
      return false;
    }
  }
  
  public List<PagePart> getPageParts()
  {
    synchronized (this.passiveActiveLock)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>(this.passiveCache);
      localArrayList.addAll(this.activeCache);
      return localArrayList;
    }
  }
  
  public List<PagePart> getThumbnails()
  {
    synchronized (this.thumbnails)
    {
      List localList2 = this.thumbnails;
      return localList2;
    }
  }
  
  public void makeANewSet()
  {
    synchronized (this.passiveActiveLock)
    {
      this.passiveCache.addAll(this.activeCache);
      this.activeCache.clear();
      return;
    }
  }
  
  public void recycle()
  {
    synchronized (this.passiveActiveLock)
    {
      Iterator localIterator = this.passiveCache.iterator();
      while (localIterator.hasNext()) {
        ((PagePart)localIterator.next()).getRenderedBitmap().recycle();
      }
      this.passiveCache.clear();
      localIterator = this.activeCache.iterator();
      while (localIterator.hasNext()) {
        ((PagePart)localIterator.next()).getRenderedBitmap().recycle();
      }
      this.activeCache.clear();
      synchronized (this.thumbnails)
      {
        localIterator = this.thumbnails.iterator();
        while (localIterator.hasNext()) {
          ((PagePart)localIterator.next()).getRenderedBitmap().recycle();
        }
        this.thumbnails.clear();
        return;
      }
    }
  }
  
  public boolean upPartIfContained(int paramInt1, RectF arg2, int paramInt2)
  {
    PagePart localPagePart1 = new PagePart(paramInt1, null, ???, false, 0);
    synchronized (this.passiveActiveLock)
    {
      PagePart localPagePart2 = find(this.passiveCache, localPagePart1);
      boolean bool = true;
      if (localPagePart2 != null)
      {
        this.passiveCache.remove(localPagePart2);
        localPagePart2.setCacheOrder(paramInt2);
        this.activeCache.offer(localPagePart2);
        return true;
      }
      if (find(this.activeCache, localPagePart1) == null) {
        bool = false;
      }
      return bool;
    }
  }
  
  class PagePartComparator
    implements Comparator<PagePart>
  {
    PagePartComparator() {}
    
    public int compare(PagePart paramPagePart1, PagePart paramPagePart2)
    {
      if (paramPagePart1.getCacheOrder() == paramPagePart2.getCacheOrder()) {
        return 0;
      }
      int i;
      if (paramPagePart1.getCacheOrder() > paramPagePart2.getCacheOrder()) {
        i = 1;
      } else {
        i = -1;
      }
      return i;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/CacheManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */