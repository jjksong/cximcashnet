package com.github.barteksc.pdfviewer;

import android.graphics.RectF;
import com.github.barteksc.pdfviewer.util.Constants;
import com.github.barteksc.pdfviewer.util.Constants.Cache;
import com.github.barteksc.pdfviewer.util.MathUtils;
import com.github.barteksc.pdfviewer.util.Util;
import com.shockwave.pdfium.util.SizeF;

class PagesLoader
{
  private int cacheOrder;
  private final GridSize firstGrid = new GridSize(null);
  private final Holder firstHolder = new Holder(null);
  private final GridSize lastGrid = new GridSize(null);
  private final Holder lastHolder = new Holder(null);
  private final GridSize middleGrid = new GridSize(null);
  private float pageRelativePartHeight;
  private float pageRelativePartWidth;
  private float partRenderHeight;
  private float partRenderWidth;
  private PDFView pdfView;
  private final int preloadOffset;
  private final RectF thumbnailRect = new RectF(0.0F, 0.0F, 1.0F, 1.0F);
  private float xOffset;
  private float yOffset;
  
  PagesLoader(PDFView paramPDFView)
  {
    this.pdfView = paramPDFView;
    this.preloadOffset = Util.getDP(paramPDFView.getContext(), Constants.PRELOAD_OFFSET);
  }
  
  private void calculatePartSize(GridSize paramGridSize)
  {
    this.pageRelativePartWidth = (1.0F / paramGridSize.cols);
    this.pageRelativePartHeight = (1.0F / paramGridSize.rows);
    this.partRenderWidth = (Constants.PART_SIZE / this.pageRelativePartWidth);
    this.partRenderHeight = (Constants.PART_SIZE / this.pageRelativePartHeight);
  }
  
  private Holder getPageAndCoordsByOffset(Holder paramHolder, GridSize paramGridSize, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    float f1 = -MathUtils.max(paramFloat1, 0.0F);
    paramFloat2 = -MathUtils.max(paramFloat2, 0.0F);
    if (this.pdfView.isSwipeVertical()) {
      paramFloat1 = paramFloat2;
    } else {
      paramFloat1 = f1;
    }
    paramHolder.page = this.pdfView.pdfFile.getPageAtOffset(paramFloat1, this.pdfView.getZoom());
    getPageColsRows(paramGridSize, paramHolder.page);
    SizeF localSizeF = this.pdfView.pdfFile.getScaledPageSize(paramHolder.page, this.pdfView.getZoom());
    float f3 = localSizeF.getHeight() / paramGridSize.rows;
    paramFloat1 = localSizeF.getWidth() / paramGridSize.cols;
    float f2 = this.pdfView.pdfFile.getSecondaryPageOffset(paramHolder.page, this.pdfView.getZoom());
    if (this.pdfView.isSwipeVertical())
    {
      paramFloat2 = Math.abs(paramFloat2 - this.pdfView.pdfFile.getPageOffset(paramHolder.page, this.pdfView.getZoom())) / f3;
      paramFloat1 = MathUtils.min(f1 - f2, 0.0F) / paramFloat1;
    }
    else
    {
      paramFloat1 = Math.abs(f1 - this.pdfView.pdfFile.getPageOffset(paramHolder.page, this.pdfView.getZoom())) / paramFloat1;
      paramFloat2 = MathUtils.min(paramFloat2 - f2, 0.0F) / f3;
    }
    if (paramBoolean)
    {
      paramHolder.row = MathUtils.ceil(paramFloat2);
      paramHolder.col = MathUtils.ceil(paramFloat1);
    }
    else
    {
      paramHolder.row = MathUtils.floor(paramFloat2);
      paramHolder.col = MathUtils.floor(paramFloat1);
    }
    return paramHolder;
  }
  
  private void getPageColsRows(GridSize paramGridSize, int paramInt)
  {
    SizeF localSizeF = this.pdfView.pdfFile.getPageSize(paramInt);
    float f2 = 1.0F / localSizeF.getWidth();
    float f1 = 1.0F / localSizeF.getHeight();
    f1 = Constants.PART_SIZE * f1 / this.pdfView.getZoom();
    f2 = Constants.PART_SIZE * f2 / this.pdfView.getZoom();
    paramGridSize.rows = MathUtils.ceil(1.0F / f1);
    paramGridSize.cols = MathUtils.ceil(1.0F / f2);
  }
  
  private boolean loadCell(int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2)
  {
    float f1 = paramInt3 * paramFloat1;
    float f2 = paramInt2 * paramFloat2;
    float f4 = this.partRenderWidth;
    float f3 = this.partRenderHeight;
    if (f1 + paramFloat1 > 1.0F) {
      paramFloat1 = 1.0F - f1;
    }
    if (f2 + paramFloat2 > 1.0F) {
      paramFloat2 = 1.0F - f2;
    }
    f4 *= paramFloat1;
    f3 *= paramFloat2;
    RectF localRectF = new RectF(f1, f2, paramFloat1 + f1, paramFloat2 + f2);
    if ((f4 > 0.0F) && (f3 > 0.0F))
    {
      if (!this.pdfView.cacheManager.upPartIfContained(paramInt1, localRectF, this.cacheOrder)) {
        this.pdfView.renderingHandler.addRenderingTask(paramInt1, f4, f3, localRectF, false, this.cacheOrder, this.pdfView.isBestQuality(), this.pdfView.isAnnotationRendering());
      }
      this.cacheOrder += 1;
      return true;
    }
    return false;
  }
  
  private int loadPage(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    int j = 0;
    int i = paramInt2;
    paramInt2 = j;
    while (i <= paramInt3)
    {
      int k = paramInt4;
      while (k <= paramInt5)
      {
        j = paramInt2;
        if (loadCell(paramInt1, i, k, this.pageRelativePartWidth, this.pageRelativePartHeight)) {
          j = paramInt2 + 1;
        }
        if (j >= paramInt6) {
          return j;
        }
        k++;
        paramInt2 = j;
      }
      i++;
    }
    return paramInt2;
  }
  
  private int loadPageCenter(Holder paramHolder1, Holder paramHolder2, GridSize paramGridSize, int paramInt)
  {
    calculatePartSize(paramGridSize);
    return loadPage(paramHolder1.page, paramHolder1.row, paramHolder2.row, paramHolder1.col, paramHolder2.col, paramInt);
  }
  
  private int loadPageEnd(Holder paramHolder, GridSize paramGridSize, int paramInt)
  {
    calculatePartSize(paramGridSize);
    if (this.pdfView.isSwipeVertical())
    {
      i = paramHolder.row;
      return loadPage(paramHolder.page, i, paramGridSize.rows - 1, 0, paramGridSize.cols - 1, paramInt);
    }
    int i = paramHolder.col;
    return loadPage(paramHolder.page, 0, paramGridSize.rows - 1, i, paramGridSize.cols - 1, paramInt);
  }
  
  private int loadPageStart(Holder paramHolder, GridSize paramGridSize, int paramInt)
  {
    calculatePartSize(paramGridSize);
    if (this.pdfView.isSwipeVertical())
    {
      i = paramHolder.row;
      return loadPage(paramHolder.page, 0, i, 0, paramGridSize.cols - 1, paramInt);
    }
    int i = paramHolder.col;
    return loadPage(paramHolder.page, 0, paramGridSize.rows - 1, 0, i, paramInt);
  }
  
  private void loadThumbnail(int paramInt)
  {
    SizeF localSizeF = this.pdfView.pdfFile.getPageSize(paramInt);
    float f1 = localSizeF.getWidth();
    float f3 = Constants.THUMBNAIL_RATIO;
    float f4 = localSizeF.getHeight();
    float f2 = Constants.THUMBNAIL_RATIO;
    if (!this.pdfView.cacheManager.containsThumbnail(paramInt, this.thumbnailRect)) {
      this.pdfView.renderingHandler.addRenderingTask(paramInt, f1 * f3, f4 * f2, this.thumbnailRect, true, 0, this.pdfView.isBestQuality(), this.pdfView.isAnnotationRendering());
    }
  }
  
  private void loadVisible()
  {
    float f2 = this.preloadOffset * this.pdfView.getZoom();
    float f3 = this.xOffset;
    float f1 = -f3;
    f3 = -f3;
    float f5 = this.pdfView.getWidth();
    float f6 = this.yOffset;
    float f4 = -f6;
    float f7 = -f6;
    f6 = this.pdfView.getHeight();
    getPageAndCoordsByOffset(this.firstHolder, this.firstGrid, f1 + f2, f4 + f2, false);
    getPageAndCoordsByOffset(this.lastHolder, this.lastGrid, f3 - f5 - f2, f7 - f6 - f2, true);
    for (int i = this.firstHolder.page; i <= this.lastHolder.page; i++) {
      loadThumbnail(i);
    }
    int k = this.lastHolder.page - this.firstHolder.page + 1;
    int j = this.firstHolder.page;
    i = 0;
    while ((j <= this.lastHolder.page) && (i < Constants.Cache.CACHE_SIZE))
    {
      if ((j == this.firstHolder.page) && (k > 1))
      {
        i += loadPageEnd(this.firstHolder, this.firstGrid, Constants.Cache.CACHE_SIZE - i);
      }
      else if ((j == this.lastHolder.page) && (k > 1))
      {
        i += loadPageStart(this.lastHolder, this.lastGrid, Constants.Cache.CACHE_SIZE - i);
      }
      else if (k == 1)
      {
        i += loadPageCenter(this.firstHolder, this.lastHolder, this.firstGrid, Constants.Cache.CACHE_SIZE - i);
      }
      else
      {
        getPageColsRows(this.middleGrid, j);
        i += loadWholePage(j, this.middleGrid, Constants.Cache.CACHE_SIZE - i);
      }
      j++;
    }
  }
  
  private int loadWholePage(int paramInt1, GridSize paramGridSize, int paramInt2)
  {
    calculatePartSize(paramGridSize);
    return loadPage(paramInt1, 0, paramGridSize.rows - 1, 0, paramGridSize.cols - 1, paramInt2);
  }
  
  void loadPages()
  {
    this.cacheOrder = 1;
    this.xOffset = (-MathUtils.max(this.pdfView.getCurrentXOffset(), 0.0F));
    this.yOffset = (-MathUtils.max(this.pdfView.getCurrentYOffset(), 0.0F));
    loadVisible();
  }
  
  private class GridSize
  {
    int cols;
    int rows;
    
    private GridSize() {}
  }
  
  private class Holder
  {
    int col;
    int page;
    int row;
    
    private Holder() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/PagesLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */