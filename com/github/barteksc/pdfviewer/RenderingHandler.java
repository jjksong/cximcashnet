package com.github.barteksc.pdfviewer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.github.barteksc.pdfviewer.exception.PageRenderingException;
import com.github.barteksc.pdfviewer.model.PagePart;

class RenderingHandler
  extends Handler
{
  static final int MSG_RENDER_TASK = 1;
  private static final String TAG = "com.github.barteksc.pdfviewer.RenderingHandler";
  private PDFView pdfView;
  private RectF renderBounds = new RectF();
  private Matrix renderMatrix = new Matrix();
  private Rect roundedRenderBounds = new Rect();
  private boolean running = false;
  
  RenderingHandler(Looper paramLooper, PDFView paramPDFView)
  {
    super(paramLooper);
    this.pdfView = paramPDFView;
  }
  
  private void calculateBounds(int paramInt1, int paramInt2, RectF paramRectF)
  {
    this.renderMatrix.reset();
    Matrix localMatrix = this.renderMatrix;
    float f4 = -paramRectF.left;
    float f2 = paramInt1;
    float f3 = -paramRectF.top;
    float f1 = paramInt2;
    localMatrix.postTranslate(f4 * f2, f3 * f1);
    this.renderMatrix.postScale(1.0F / paramRectF.width(), 1.0F / paramRectF.height());
    this.renderBounds.set(0.0F, 0.0F, f2, f1);
    this.renderMatrix.mapRect(this.renderBounds);
    this.renderBounds.round(this.roundedRenderBounds);
  }
  
  private PagePart proceed(RenderingTask paramRenderingTask)
    throws PageRenderingException
  {
    PdfFile localPdfFile = this.pdfView.pdfFile;
    localPdfFile.openPage(paramRenderingTask.page);
    int i = Math.round(paramRenderingTask.width);
    int j = Math.round(paramRenderingTask.height);
    if ((i != 0) && (j != 0) && (!localPdfFile.pageHasError(paramRenderingTask.page))) {
      try
      {
        if (paramRenderingTask.bestQuality) {
          localObject = Bitmap.Config.ARGB_8888;
        } else {
          localObject = Bitmap.Config.RGB_565;
        }
        Object localObject = Bitmap.createBitmap(i, j, (Bitmap.Config)localObject);
        calculateBounds(i, j, paramRenderingTask.bounds);
        localPdfFile.renderPageBitmap((Bitmap)localObject, paramRenderingTask.page, this.roundedRenderBounds, paramRenderingTask.annotationRendering);
        return new PagePart(paramRenderingTask.page, (Bitmap)localObject, paramRenderingTask.bounds, paramRenderingTask.thumbnail, paramRenderingTask.cacheOrder);
      }
      catch (IllegalArgumentException paramRenderingTask)
      {
        Log.e(TAG, "Cannot create bitmap", paramRenderingTask);
        return null;
      }
    }
    return null;
  }
  
  void addRenderingTask(int paramInt1, float paramFloat1, float paramFloat2, RectF paramRectF, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, boolean paramBoolean3)
  {
    sendMessage(obtainMessage(1, new RenderingTask(paramFloat1, paramFloat2, paramRectF, paramInt1, paramBoolean1, paramInt2, paramBoolean2, paramBoolean3)));
  }
  
  public void handleMessage(final Message paramMessage)
  {
    paramMessage = (RenderingTask)paramMessage.obj;
    try
    {
      paramMessage = proceed(paramMessage);
      if (paramMessage != null) {
        if (this.running)
        {
          PDFView localPDFView = this.pdfView;
          Runnable local1 = new com/github/barteksc/pdfviewer/RenderingHandler$1;
          local1.<init>(this, paramMessage);
          localPDFView.post(local1);
        }
        else
        {
          paramMessage.getRenderedBitmap().recycle();
        }
      }
    }
    catch (PageRenderingException paramMessage)
    {
      this.pdfView.post(new Runnable()
      {
        public void run()
        {
          RenderingHandler.this.pdfView.onPageError(paramMessage);
        }
      });
    }
  }
  
  void start()
  {
    this.running = true;
  }
  
  void stop()
  {
    this.running = false;
  }
  
  private class RenderingTask
  {
    boolean annotationRendering;
    boolean bestQuality;
    RectF bounds;
    int cacheOrder;
    float height;
    int page;
    boolean thumbnail;
    float width;
    
    RenderingTask(float paramFloat1, float paramFloat2, RectF paramRectF, int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, boolean paramBoolean3)
    {
      this.page = paramInt1;
      this.width = paramFloat1;
      this.height = paramFloat2;
      this.bounds = paramRectF;
      this.thumbnail = paramBoolean1;
      this.cacheOrder = paramInt2;
      this.bestQuality = paramBoolean2;
      this.annotationRendering = paramBoolean3;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/RenderingHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */