package com.github.barteksc.pdfviewer;

import android.graphics.PointF;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import com.github.barteksc.pdfviewer.listener.Callbacks;
import com.github.barteksc.pdfviewer.model.LinkTapEvent;
import com.github.barteksc.pdfviewer.scroll.ScrollHandle;
import com.github.barteksc.pdfviewer.util.Constants.Pinch;
import com.shockwave.pdfium.PdfDocument.Link;
import com.shockwave.pdfium.util.SizeF;
import java.util.Iterator;
import java.util.List;

class DragPinchManager
  implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener, View.OnTouchListener
{
  private AnimationManager animationManager;
  private boolean enabled = false;
  private GestureDetector gestureDetector;
  private PDFView pdfView;
  private ScaleGestureDetector scaleGestureDetector;
  private boolean scaling = false;
  private boolean scrolling = false;
  
  DragPinchManager(PDFView paramPDFView, AnimationManager paramAnimationManager)
  {
    this.pdfView = paramPDFView;
    this.animationManager = paramAnimationManager;
    this.gestureDetector = new GestureDetector(paramPDFView.getContext(), this);
    this.scaleGestureDetector = new ScaleGestureDetector(paramPDFView.getContext(), this);
    paramPDFView.setOnTouchListener(this);
  }
  
  private boolean checkDoPageFling(float paramFloat1, float paramFloat2)
  {
    paramFloat1 = Math.abs(paramFloat1);
    paramFloat2 = Math.abs(paramFloat2);
    boolean bool2 = this.pdfView.isSwipeVertical();
    boolean bool1 = true;
    if (bool2 ? paramFloat2 <= paramFloat1 : paramFloat1 <= paramFloat2) {
      bool1 = false;
    }
    return bool1;
  }
  
  private boolean checkLinkTapped(float paramFloat1, float paramFloat2)
  {
    PdfFile localPdfFile = this.pdfView.pdfFile;
    float f3 = -this.pdfView.getCurrentXOffset() + paramFloat1;
    float f2 = -this.pdfView.getCurrentYOffset() + paramFloat2;
    float f1;
    if (this.pdfView.isSwipeVertical()) {
      f1 = f2;
    } else {
      f1 = f3;
    }
    int m = localPdfFile.getPageAtOffset(f1, this.pdfView.getZoom());
    SizeF localSizeF = localPdfFile.getScaledPageSize(m, this.pdfView.getZoom());
    int i;
    int j;
    if (this.pdfView.isSwipeVertical())
    {
      i = (int)localPdfFile.getSecondaryPageOffset(m, this.pdfView.getZoom());
      j = (int)localPdfFile.getPageOffset(m, this.pdfView.getZoom());
    }
    else
    {
      i = (int)localPdfFile.getSecondaryPageOffset(m, this.pdfView.getZoom());
      int k = (int)localPdfFile.getPageOffset(m, this.pdfView.getZoom());
      j = i;
      i = k;
    }
    Iterator localIterator = localPdfFile.getPageLinks(m).iterator();
    while (localIterator.hasNext())
    {
      PdfDocument.Link localLink = (PdfDocument.Link)localIterator.next();
      RectF localRectF = localPdfFile.mapRectToDevice(m, i, j, (int)localSizeF.getWidth(), (int)localSizeF.getHeight(), localLink.getBounds());
      localRectF.sort();
      if (localRectF.contains(f3, f2))
      {
        this.pdfView.callbacks.callLinkHandler(new LinkTapEvent(paramFloat1, paramFloat2, f3, f2, localRectF, localLink));
        return true;
      }
    }
    return false;
  }
  
  private void hideHandle()
  {
    ScrollHandle localScrollHandle = this.pdfView.getScrollHandle();
    if ((localScrollHandle != null) && (localScrollHandle.shown())) {
      localScrollHandle.hideDelayed();
    }
  }
  
  private void onBoundedFling(float paramFloat1, float paramFloat2)
  {
    int j = (int)this.pdfView.getCurrentXOffset();
    int i = (int)this.pdfView.getCurrentYOffset();
    PdfFile localPdfFile = this.pdfView.pdfFile;
    float f1 = -localPdfFile.getPageOffset(this.pdfView.getCurrentPage(), this.pdfView.getZoom());
    float f5 = f1 - localPdfFile.getPageLength(this.pdfView.getCurrentPage(), this.pdfView.getZoom());
    boolean bool = this.pdfView.isSwipeVertical();
    float f3 = 0.0F;
    float f2;
    float f4;
    if (bool)
    {
      f2 = -(this.pdfView.toCurrentScale(localPdfFile.getMaxPageWidth()) - this.pdfView.getWidth());
      f4 = f5 + this.pdfView.getHeight();
      f3 = f1;
      f1 = 0.0F;
    }
    else
    {
      f2 = this.pdfView.getWidth();
      f4 = -(this.pdfView.toCurrentScale(localPdfFile.getMaxPageHeight()) - this.pdfView.getHeight());
      f2 = f5 + f2;
    }
    this.animationManager.startFlingAnimation(j, i, (int)paramFloat1, (int)paramFloat2, (int)f2, (int)f1, (int)f4, (int)f3);
  }
  
  private void onScrollEnd(MotionEvent paramMotionEvent)
  {
    this.pdfView.loadPages();
    hideHandle();
    if (!this.animationManager.isFlinging()) {
      this.pdfView.performPageSnap();
    }
  }
  
  private void startPageFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    if (!checkDoPageFling(paramFloat1, paramFloat2)) {
      return;
    }
    boolean bool = this.pdfView.isSwipeVertical();
    int i = -1;
    if (bool)
    {
      if (paramFloat2 <= 0.0F) {
        i = 1;
      }
    }
    else if (paramFloat1 <= 0.0F) {
      i = 1;
    }
    if (this.pdfView.isSwipeVertical())
    {
      paramFloat1 = paramMotionEvent2.getY();
      paramFloat2 = paramMotionEvent1.getY();
    }
    else
    {
      paramFloat1 = paramMotionEvent2.getX();
      paramFloat2 = paramMotionEvent1.getX();
    }
    paramFloat2 = paramFloat1 - paramFloat2;
    float f1 = this.pdfView.getCurrentXOffset();
    float f2 = this.pdfView.getZoom();
    float f3 = this.pdfView.getCurrentYOffset();
    paramFloat1 = this.pdfView.getZoom();
    int j = this.pdfView.findFocusPage(f1 - f2 * paramFloat2, f3 - paramFloat2 * paramFloat1);
    i = Math.max(0, Math.min(this.pdfView.getPageCount() - 1, j + i));
    paramMotionEvent1 = this.pdfView.findSnapEdge(i);
    paramFloat1 = this.pdfView.snapOffsetForPage(i, paramMotionEvent1);
    this.animationManager.startPageFlingAnimation(-paramFloat1);
  }
  
  void disable()
  {
    this.enabled = false;
  }
  
  void enable()
  {
    this.enabled = true;
  }
  
  public boolean onDoubleTap(MotionEvent paramMotionEvent)
  {
    if (!this.pdfView.isDoubletapEnabled()) {
      return false;
    }
    if (this.pdfView.getZoom() < this.pdfView.getMidZoom()) {
      this.pdfView.zoomWithAnimation(paramMotionEvent.getX(), paramMotionEvent.getY(), this.pdfView.getMidZoom());
    } else if (this.pdfView.getZoom() < this.pdfView.getMaxZoom()) {
      this.pdfView.zoomWithAnimation(paramMotionEvent.getX(), paramMotionEvent.getY(), this.pdfView.getMaxZoom());
    } else {
      this.pdfView.resetZoomWithAnimation();
    }
    return true;
  }
  
  public boolean onDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }
  
  public boolean onDown(MotionEvent paramMotionEvent)
  {
    this.animationManager.stopFling();
    return true;
  }
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    if (!this.pdfView.isSwipeEnabled()) {
      return false;
    }
    if (this.pdfView.doPageFling())
    {
      if (this.pdfView.pageFillsScreen()) {
        onBoundedFling(paramFloat1, paramFloat2);
      } else {
        startPageFling(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
      }
      return true;
    }
    int i = (int)this.pdfView.getCurrentXOffset();
    int j = (int)this.pdfView.getCurrentYOffset();
    paramMotionEvent1 = this.pdfView.pdfFile;
    float f2;
    float f1;
    if (this.pdfView.isSwipeVertical())
    {
      f2 = -(this.pdfView.toCurrentScale(paramMotionEvent1.getMaxPageWidth()) - this.pdfView.getWidth());
      f1 = -(paramMotionEvent1.getDocLen(this.pdfView.getZoom()) - this.pdfView.getHeight());
    }
    else
    {
      f2 = -(paramMotionEvent1.getDocLen(this.pdfView.getZoom()) - this.pdfView.getWidth());
      f1 = -(this.pdfView.toCurrentScale(paramMotionEvent1.getMaxPageHeight()) - this.pdfView.getHeight());
    }
    this.animationManager.startFlingAnimation(i, j, (int)paramFloat1, (int)paramFloat2, (int)f2, 0, (int)f1, 0);
    return true;
  }
  
  public void onLongPress(MotionEvent paramMotionEvent)
  {
    this.pdfView.callbacks.callOnLongPress(paramMotionEvent);
  }
  
  public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
  {
    float f1 = paramScaleGestureDetector.getScaleFactor();
    float f2 = this.pdfView.getZoom() * f1;
    if (f2 < Constants.Pinch.MINIMUM_ZOOM) {
      f1 = Constants.Pinch.MINIMUM_ZOOM / this.pdfView.getZoom();
    } else if (f2 > Constants.Pinch.MAXIMUM_ZOOM) {
      f1 = Constants.Pinch.MAXIMUM_ZOOM / this.pdfView.getZoom();
    }
    this.pdfView.zoomCenteredRelativeTo(f1, new PointF(paramScaleGestureDetector.getFocusX(), paramScaleGestureDetector.getFocusY()));
    return true;
  }
  
  public boolean onScaleBegin(ScaleGestureDetector paramScaleGestureDetector)
  {
    this.scaling = true;
    return true;
  }
  
  public void onScaleEnd(ScaleGestureDetector paramScaleGestureDetector)
  {
    this.pdfView.loadPages();
    hideHandle();
    this.scaling = false;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    this.scrolling = true;
    if ((this.pdfView.isZooming()) || (this.pdfView.isSwipeEnabled())) {
      this.pdfView.moveRelativeTo(-paramFloat1, -paramFloat2);
    }
    if ((!this.scaling) || (this.pdfView.doRenderDuringScale())) {
      this.pdfView.loadPageByOffset();
    }
    return true;
  }
  
  public void onShowPress(MotionEvent paramMotionEvent) {}
  
  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    boolean bool1 = this.pdfView.callbacks.callOnTap(paramMotionEvent);
    boolean bool2 = checkLinkTapped(paramMotionEvent.getX(), paramMotionEvent.getY());
    if ((!bool1) && (!bool2))
    {
      paramMotionEvent = this.pdfView.getScrollHandle();
      if ((paramMotionEvent != null) && (!this.pdfView.documentFitsView())) {
        if (!paramMotionEvent.shown()) {
          paramMotionEvent.show();
        } else {
          paramMotionEvent.hide();
        }
      }
    }
    this.pdfView.performClick();
    return true;
  }
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    return false;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (!this.enabled) {
      return false;
    }
    boolean bool = this.scaleGestureDetector.onTouchEvent(paramMotionEvent);
    if ((!this.gestureDetector.onTouchEvent(paramMotionEvent)) && (!bool)) {
      bool = false;
    } else {
      bool = true;
    }
    if ((paramMotionEvent.getAction() == 1) && (this.scrolling))
    {
      this.scrolling = false;
      onScrollEnd(paramMotionEvent);
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/DragPinchManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */