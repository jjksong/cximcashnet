package com.github.barteksc.pdfviewer.util;

import java.util.ArrayList;
import java.util.List;

public class ArrayUtils
{
  public static String arrayToString(int[] paramArrayOfInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("[");
    for (int i = 0; i < paramArrayOfInt.length; i++)
    {
      localStringBuilder.append(paramArrayOfInt[i]);
      if (i != paramArrayOfInt.length - 1) {
        localStringBuilder.append(",");
      }
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public static int[] calculateIndexesInDuplicateArray(int[] paramArrayOfInt)
  {
    int[] arrayOfInt = new int[paramArrayOfInt.length];
    if (paramArrayOfInt.length == 0) {
      return arrayOfInt;
    }
    int k = 0;
    arrayOfInt[0] = 0;
    int i = 1;
    while (i < paramArrayOfInt.length)
    {
      int j = k;
      if (paramArrayOfInt[i] != paramArrayOfInt[(i - 1)]) {
        j = k + 1;
      }
      arrayOfInt[i] = j;
      i++;
      k = j;
    }
    return arrayOfInt;
  }
  
  public static int[] deleteDuplicatedPages(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    int m = paramArrayOfInt.length;
    int k = 0;
    int j = 0;
    int i = -1;
    while (j < m)
    {
      Integer localInteger = Integer.valueOf(paramArrayOfInt[j]);
      if (i != localInteger.intValue()) {
        localArrayList.add(localInteger);
      }
      i = localInteger.intValue();
      j++;
    }
    paramArrayOfInt = new int[localArrayList.size()];
    for (i = k; i < localArrayList.size(); i++) {
      paramArrayOfInt[i] = ((Integer)localArrayList.get(i)).intValue();
    }
    return paramArrayOfInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/util/ArrayUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */