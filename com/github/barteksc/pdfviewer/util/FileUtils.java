package com.github.barteksc.pdfviewer.util;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.File;
import java.io.IOException;

public class FileUtils
{
  /* Error */
  public static void copy(java.io.InputStream paramInputStream, File paramFile)
    throws IOException
  {
    // Byte code:
    //   0: new 15	java/io/FileOutputStream
    //   3: astore 4
    //   5: aload 4
    //   7: aload_1
    //   8: invokespecial 18	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   11: sipush 1024
    //   14: newarray <illegal type>
    //   16: astore_1
    //   17: aload_0
    //   18: aload_1
    //   19: invokevirtual 24	java/io/InputStream:read	([B)I
    //   22: istore_2
    //   23: iload_2
    //   24: iconst_m1
    //   25: if_icmpeq +14 -> 39
    //   28: aload 4
    //   30: aload_1
    //   31: iconst_0
    //   32: iload_2
    //   33: invokevirtual 30	java/io/OutputStream:write	([BII)V
    //   36: goto -19 -> 17
    //   39: aload_0
    //   40: ifnull +18 -> 58
    //   43: aload_0
    //   44: invokevirtual 33	java/io/InputStream:close	()V
    //   47: goto +11 -> 58
    //   50: astore_0
    //   51: aload 4
    //   53: invokevirtual 34	java/io/OutputStream:close	()V
    //   56: aload_0
    //   57: athrow
    //   58: aload 4
    //   60: invokevirtual 34	java/io/OutputStream:close	()V
    //   63: return
    //   64: astore_3
    //   65: aload 4
    //   67: astore_1
    //   68: goto +6 -> 74
    //   71: astore_3
    //   72: aconst_null
    //   73: astore_1
    //   74: aload_0
    //   75: ifnull +21 -> 96
    //   78: aload_0
    //   79: invokevirtual 33	java/io/InputStream:close	()V
    //   82: goto +14 -> 96
    //   85: astore_0
    //   86: aload_1
    //   87: ifnull +7 -> 94
    //   90: aload_1
    //   91: invokevirtual 34	java/io/OutputStream:close	()V
    //   94: aload_0
    //   95: athrow
    //   96: aload_1
    //   97: ifnull +7 -> 104
    //   100: aload_1
    //   101: invokevirtual 34	java/io/OutputStream:close	()V
    //   104: aload_3
    //   105: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	paramInputStream	java.io.InputStream
    //   0	106	1	paramFile	File
    //   22	11	2	i	int
    //   64	1	3	localObject1	Object
    //   71	34	3	localObject2	Object
    //   3	63	4	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   43	47	50	finally
    //   11	17	64	finally
    //   17	23	64	finally
    //   28	36	64	finally
    //   0	11	71	finally
    //   78	82	85	finally
  }
  
  public static File fileFromAsset(Context paramContext, String paramString)
    throws IOException
  {
    File localFile = paramContext.getCacheDir();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("-pdfview.pdf");
    localObject = new File(localFile, ((StringBuilder)localObject).toString());
    if (paramString.contains("/")) {
      ((File)localObject).getParentFile().mkdirs();
    }
    copy(paramContext.getAssets().open(paramString), (File)localObject);
    return (File)localObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/util/FileUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */