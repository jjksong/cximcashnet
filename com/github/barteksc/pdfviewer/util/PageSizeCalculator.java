package com.github.barteksc.pdfviewer.util;

import com.shockwave.pdfium.util.Size;
import com.shockwave.pdfium.util.SizeF;

public class PageSizeCalculator
{
  private FitPolicy fitPolicy;
  private float heightRatio;
  private SizeF optimalMaxHeightPageSize;
  private SizeF optimalMaxWidthPageSize;
  private final Size originalMaxHeightPageSize;
  private final Size originalMaxWidthPageSize;
  private final Size viewSize;
  private float widthRatio;
  
  public PageSizeCalculator(FitPolicy paramFitPolicy, Size paramSize1, Size paramSize2, Size paramSize3)
  {
    this.fitPolicy = paramFitPolicy;
    this.originalMaxWidthPageSize = paramSize1;
    this.originalMaxHeightPageSize = paramSize2;
    this.viewSize = paramSize3;
    calculateMaxPages();
  }
  
  private void calculateMaxPages()
  {
    Size localSize;
    switch (this.fitPolicy)
    {
    default: 
      this.optimalMaxWidthPageSize = fitWidth(this.originalMaxWidthPageSize, this.viewSize.getWidth());
      this.widthRatio = (this.optimalMaxWidthPageSize.getWidth() / this.originalMaxWidthPageSize.getWidth());
      localSize = this.originalMaxHeightPageSize;
      this.optimalMaxHeightPageSize = fitWidth(localSize, localSize.getWidth() * this.widthRatio);
      break;
    case ???: 
      float f = fitBoth(this.originalMaxWidthPageSize, this.viewSize.getWidth(), this.viewSize.getHeight()).getWidth() / this.originalMaxWidthPageSize.getWidth();
      localSize = this.originalMaxHeightPageSize;
      this.optimalMaxHeightPageSize = fitBoth(localSize, localSize.getWidth() * f, this.viewSize.getHeight());
      this.heightRatio = (this.optimalMaxHeightPageSize.getHeight() / this.originalMaxHeightPageSize.getHeight());
      this.optimalMaxWidthPageSize = fitBoth(this.originalMaxWidthPageSize, this.viewSize.getWidth(), this.originalMaxWidthPageSize.getHeight() * this.heightRatio);
      this.widthRatio = (this.optimalMaxWidthPageSize.getWidth() / this.originalMaxWidthPageSize.getWidth());
      break;
    case ???: 
      this.optimalMaxHeightPageSize = fitHeight(this.originalMaxHeightPageSize, this.viewSize.getHeight());
      this.heightRatio = (this.optimalMaxHeightPageSize.getHeight() / this.originalMaxHeightPageSize.getHeight());
      localSize = this.originalMaxWidthPageSize;
      this.optimalMaxWidthPageSize = fitHeight(localSize, localSize.getHeight() * this.heightRatio);
    }
  }
  
  private SizeF fitBoth(Size paramSize, float paramFloat1, float paramFloat2)
  {
    float f3 = paramSize.getWidth() / paramSize.getHeight();
    float f2 = (float)Math.floor(paramFloat1 / f3);
    float f1 = f2;
    if (f2 > paramFloat2)
    {
      paramFloat1 = (float)Math.floor(f3 * paramFloat2);
      f1 = paramFloat2;
    }
    return new SizeF(paramFloat1, f1);
  }
  
  private SizeF fitHeight(Size paramSize, float paramFloat)
  {
    float f = paramSize.getWidth();
    return new SizeF((float)Math.floor(paramFloat / (paramSize.getHeight() / f)), paramFloat);
  }
  
  private SizeF fitWidth(Size paramSize, float paramFloat)
  {
    return new SizeF(paramFloat, (float)Math.floor(paramFloat / (paramSize.getWidth() / paramSize.getHeight())));
  }
  
  public SizeF calculate(Size paramSize)
  {
    if ((paramSize.getWidth() > 0) && (paramSize.getHeight() > 0))
    {
      switch (this.fitPolicy)
      {
      default: 
        return fitWidth(paramSize, paramSize.getWidth() * this.widthRatio);
      case ???: 
        return fitBoth(paramSize, paramSize.getWidth() * this.widthRatio, paramSize.getHeight() * this.heightRatio);
      }
      return fitHeight(paramSize, paramSize.getHeight() * this.heightRatio);
    }
    return new SizeF(0.0F, 0.0F);
  }
  
  public SizeF getOptimalMaxHeightPageSize()
  {
    return this.optimalMaxHeightPageSize;
  }
  
  public SizeF getOptimalMaxWidthPageSize()
  {
    return this.optimalMaxWidthPageSize;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/util/PageSizeCalculator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */