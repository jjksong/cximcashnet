package com.github.barteksc.pdfviewer;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.SparseBooleanArray;
import com.github.barteksc.pdfviewer.exception.PageRenderingException;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.github.barteksc.pdfviewer.util.PageSizeCalculator;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfDocument.Bookmark;
import com.shockwave.pdfium.PdfDocument.Link;
import com.shockwave.pdfium.PdfDocument.Meta;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.util.Size;
import com.shockwave.pdfium.util.SizeF;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class PdfFile
{
  private static final Object lock = new Object();
  private boolean autoSpacing;
  private float documentLength = 0.0F;
  private boolean isVertical = true;
  private SizeF maxHeightPageSize = new SizeF(0.0F, 0.0F);
  private SizeF maxWidthPageSize = new SizeF(0.0F, 0.0F);
  private SparseBooleanArray openedPages = new SparseBooleanArray();
  private Size originalMaxHeightPageSize = new Size(0, 0);
  private Size originalMaxWidthPageSize = new Size(0, 0);
  private List<Size> originalPageSizes = new ArrayList();
  private int[] originalUserPages;
  private final FitPolicy pageFitPolicy;
  private List<Float> pageOffsets = new ArrayList();
  private List<SizeF> pageSizes = new ArrayList();
  private List<Float> pageSpacing = new ArrayList();
  private int pagesCount = 0;
  private PdfDocument pdfDocument;
  private PdfiumCore pdfiumCore;
  private int spacingPx = 0;
  
  PdfFile(PdfiumCore paramPdfiumCore, PdfDocument paramPdfDocument, FitPolicy paramFitPolicy, Size paramSize, int[] paramArrayOfInt, boolean paramBoolean1, int paramInt, boolean paramBoolean2)
  {
    this.pdfiumCore = paramPdfiumCore;
    this.pdfDocument = paramPdfDocument;
    this.pageFitPolicy = paramFitPolicy;
    this.originalUserPages = paramArrayOfInt;
    this.isVertical = paramBoolean1;
    this.spacingPx = paramInt;
    this.autoSpacing = paramBoolean2;
    setup(paramSize);
  }
  
  private void prepareAutoSpacing(Size paramSize)
  {
    this.pageSpacing.clear();
    for (int i = 0; i < getPagesCount(); i++)
    {
      SizeF localSizeF = (SizeF)this.pageSizes.get(i);
      if (this.isVertical) {
        f1 = paramSize.getHeight() - localSizeF.getHeight();
      } else {
        f1 = paramSize.getWidth() - localSizeF.getWidth();
      }
      float f2 = Math.max(0.0F, f1);
      float f1 = f2;
      if (i < getPagesCount() - 1) {
        f1 = f2 + this.spacingPx;
      }
      this.pageSpacing.add(Float.valueOf(f1));
    }
  }
  
  private void prepareDocLen()
  {
    float f1 = 0.0F;
    for (int i = 0; i < getPagesCount(); i++)
    {
      SizeF localSizeF = (SizeF)this.pageSizes.get(i);
      if (this.isVertical) {
        f2 = localSizeF.getHeight();
      } else {
        f2 = localSizeF.getWidth();
      }
      float f2 = f1 + f2;
      if (this.autoSpacing)
      {
        f1 = f2 + ((Float)this.pageSpacing.get(i)).floatValue();
      }
      else
      {
        f1 = f2;
        if (i < getPagesCount() - 1) {
          f1 = f2 + this.spacingPx;
        }
      }
    }
    this.documentLength = f1;
  }
  
  private void preparePagesOffset()
  {
    this.pageOffsets.clear();
    float f1 = 0.0F;
    for (int i = 0; i < getPagesCount(); i++)
    {
      SizeF localSizeF = (SizeF)this.pageSizes.get(i);
      float f2;
      if (this.isVertical) {
        f2 = localSizeF.getHeight();
      } else {
        f2 = localSizeF.getWidth();
      }
      if (this.autoSpacing)
      {
        float f3 = f1 + ((Float)this.pageSpacing.get(i)).floatValue() / 2.0F;
        if (i == 0)
        {
          f1 = f3 - this.spacingPx / 2.0F;
        }
        else
        {
          f1 = f3;
          if (i == getPagesCount() - 1) {
            f1 = f3 + this.spacingPx / 2.0F;
          }
        }
        this.pageOffsets.add(Float.valueOf(f1));
        f1 += f2 + ((Float)this.pageSpacing.get(i)).floatValue() / 2.0F;
      }
      else
      {
        this.pageOffsets.add(Float.valueOf(f1));
        f1 += f2 + this.spacingPx;
      }
    }
  }
  
  private void setup(Size paramSize)
  {
    Object localObject = this.originalUserPages;
    if (localObject != null) {
      this.pagesCount = localObject.length;
    } else {
      this.pagesCount = this.pdfiumCore.getPageCount(this.pdfDocument);
    }
    for (int i = 0; i < this.pagesCount; i++)
    {
      localObject = this.pdfiumCore.getPageSize(this.pdfDocument, documentPage(i));
      if (((Size)localObject).getWidth() > this.originalMaxWidthPageSize.getWidth()) {
        this.originalMaxWidthPageSize = ((Size)localObject);
      }
      if (((Size)localObject).getHeight() > this.originalMaxHeightPageSize.getHeight()) {
        this.originalMaxHeightPageSize = ((Size)localObject);
      }
      this.originalPageSizes.add(localObject);
    }
    recalculatePageSizes(paramSize);
  }
  
  public int determineValidPageNumberFrom(int paramInt)
  {
    if (paramInt <= 0) {
      return 0;
    }
    int[] arrayOfInt = this.originalUserPages;
    if (arrayOfInt != null)
    {
      if (paramInt >= arrayOfInt.length) {
        return arrayOfInt.length - 1;
      }
    }
    else if (paramInt >= getPagesCount()) {
      return getPagesCount() - 1;
    }
    return paramInt;
  }
  
  public void dispose()
  {
    PdfiumCore localPdfiumCore = this.pdfiumCore;
    if (localPdfiumCore != null)
    {
      PdfDocument localPdfDocument = this.pdfDocument;
      if (localPdfDocument != null) {
        localPdfiumCore.closeDocument(localPdfDocument);
      }
    }
    this.pdfDocument = null;
    this.originalUserPages = null;
  }
  
  public int documentPage(int paramInt)
  {
    int[] arrayOfInt = this.originalUserPages;
    int i;
    if (arrayOfInt != null)
    {
      if ((paramInt >= 0) && (paramInt < arrayOfInt.length)) {
        i = arrayOfInt[paramInt];
      } else {
        return -1;
      }
    }
    else {
      i = paramInt;
    }
    if ((i >= 0) && (paramInt < getPagesCount())) {
      return i;
    }
    return -1;
  }
  
  public List<PdfDocument.Bookmark> getBookmarks()
  {
    PdfDocument localPdfDocument = this.pdfDocument;
    if (localPdfDocument == null) {
      return new ArrayList();
    }
    return this.pdfiumCore.getTableOfContents(localPdfDocument);
  }
  
  public float getDocLen(float paramFloat)
  {
    return this.documentLength * paramFloat;
  }
  
  public float getMaxPageHeight()
  {
    return getMaxPageSize().getHeight();
  }
  
  public SizeF getMaxPageSize()
  {
    SizeF localSizeF;
    if (this.isVertical) {
      localSizeF = this.maxWidthPageSize;
    } else {
      localSizeF = this.maxHeightPageSize;
    }
    return localSizeF;
  }
  
  public float getMaxPageWidth()
  {
    return getMaxPageSize().getWidth();
  }
  
  public PdfDocument.Meta getMetaData()
  {
    PdfDocument localPdfDocument = this.pdfDocument;
    if (localPdfDocument == null) {
      return null;
    }
    return this.pdfiumCore.getDocumentMeta(localPdfDocument);
  }
  
  public int getPageAtOffset(float paramFloat1, float paramFloat2)
  {
    int i = 0;
    int j = 0;
    while ((i < getPagesCount()) && (((Float)this.pageOffsets.get(i)).floatValue() * paramFloat2 - getPageSpacing(i, paramFloat2) / 2.0F < paramFloat1))
    {
      j++;
      i++;
    }
    i = j - 1;
    if (i < 0) {
      i = 0;
    }
    return i;
  }
  
  public float getPageLength(int paramInt, float paramFloat)
  {
    SizeF localSizeF = getPageSize(paramInt);
    float f;
    if (this.isVertical) {
      f = localSizeF.getHeight();
    } else {
      f = localSizeF.getWidth();
    }
    return f * paramFloat;
  }
  
  public List<PdfDocument.Link> getPageLinks(int paramInt)
  {
    paramInt = documentPage(paramInt);
    return this.pdfiumCore.getPageLinks(this.pdfDocument, paramInt);
  }
  
  public float getPageOffset(int paramInt, float paramFloat)
  {
    if (documentPage(paramInt) < 0) {
      return 0.0F;
    }
    return ((Float)this.pageOffsets.get(paramInt)).floatValue() * paramFloat;
  }
  
  public SizeF getPageSize(int paramInt)
  {
    if (documentPage(paramInt) < 0) {
      return new SizeF(0.0F, 0.0F);
    }
    return (SizeF)this.pageSizes.get(paramInt);
  }
  
  public float getPageSpacing(int paramInt, float paramFloat)
  {
    float f;
    if (this.autoSpacing) {
      f = ((Float)this.pageSpacing.get(paramInt)).floatValue();
    } else {
      f = this.spacingPx;
    }
    return f * paramFloat;
  }
  
  public int getPagesCount()
  {
    return this.pagesCount;
  }
  
  public SizeF getScaledPageSize(int paramInt, float paramFloat)
  {
    SizeF localSizeF = getPageSize(paramInt);
    return new SizeF(localSizeF.getWidth() * paramFloat, localSizeF.getHeight() * paramFloat);
  }
  
  public float getSecondaryPageOffset(int paramInt, float paramFloat)
  {
    SizeF localSizeF = getPageSize(paramInt);
    if (this.isVertical) {
      return paramFloat * (getMaxPageWidth() - localSizeF.getWidth()) / 2.0F;
    }
    return paramFloat * (getMaxPageHeight() - localSizeF.getHeight()) / 2.0F;
  }
  
  public RectF mapRectToDevice(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, RectF paramRectF)
  {
    paramInt1 = documentPage(paramInt1);
    return this.pdfiumCore.mapRectToDevice(this.pdfDocument, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0, paramRectF);
  }
  
  public boolean openPage(int paramInt)
    throws PageRenderingException
  {
    int j = documentPage(paramInt);
    if (j < 0) {
      return false;
    }
    synchronized (lock)
    {
      int i = this.openedPages.indexOfKey(j);
      if (i < 0) {
        try
        {
          this.pdfiumCore.openPage(this.pdfDocument, j);
          this.openedPages.put(j, true);
          return true;
        }
        catch (Exception localException)
        {
          this.openedPages.put(j, false);
          PageRenderingException localPageRenderingException = new com/github/barteksc/pdfviewer/exception/PageRenderingException;
          localPageRenderingException.<init>(paramInt, localException);
          throw localPageRenderingException;
        }
      }
      return false;
    }
  }
  
  public boolean pageHasError(int paramInt)
  {
    paramInt = documentPage(paramInt);
    return this.openedPages.get(paramInt, false) ^ true;
  }
  
  public void recalculatePageSizes(Size paramSize)
  {
    this.pageSizes.clear();
    PageSizeCalculator localPageSizeCalculator = new PageSizeCalculator(this.pageFitPolicy, this.originalMaxWidthPageSize, this.originalMaxHeightPageSize, paramSize);
    this.maxWidthPageSize = localPageSizeCalculator.getOptimalMaxWidthPageSize();
    this.maxHeightPageSize = localPageSizeCalculator.getOptimalMaxHeightPageSize();
    Iterator localIterator = this.originalPageSizes.iterator();
    while (localIterator.hasNext())
    {
      Size localSize = (Size)localIterator.next();
      this.pageSizes.add(localPageSizeCalculator.calculate(localSize));
    }
    if (this.autoSpacing) {
      prepareAutoSpacing(paramSize);
    }
    prepareDocLen();
    preparePagesOffset();
  }
  
  public void renderPageBitmap(Bitmap paramBitmap, int paramInt, Rect paramRect, boolean paramBoolean)
  {
    paramInt = documentPage(paramInt);
    this.pdfiumCore.renderPageBitmap(this.pdfDocument, paramBitmap, paramInt, paramRect.left, paramRect.top, paramRect.width(), paramRect.height(), paramBoolean);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/PdfFile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */