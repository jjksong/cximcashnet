package com.github.barteksc.pdfviewer.link;

import com.github.barteksc.pdfviewer.model.LinkTapEvent;

public abstract interface LinkHandler
{
  public abstract void handleLinkEvent(LinkTapEvent paramLinkTapEvent);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/link/LinkHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */