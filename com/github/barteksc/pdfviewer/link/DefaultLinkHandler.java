package com.github.barteksc.pdfviewer.link;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.model.LinkTapEvent;
import com.shockwave.pdfium.PdfDocument.Link;

public class DefaultLinkHandler
  implements LinkHandler
{
  private static final String TAG = "DefaultLinkHandler";
  private PDFView pdfView;
  
  public DefaultLinkHandler(PDFView paramPDFView)
  {
    this.pdfView = paramPDFView;
  }
  
  private void handlePage(int paramInt)
  {
    this.pdfView.jumpTo(paramInt);
  }
  
  private void handleUri(String paramString)
  {
    Object localObject1 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    Object localObject2 = this.pdfView.getContext();
    if (((Intent)localObject1).resolveActivity(((Context)localObject2).getPackageManager()) != null)
    {
      ((Context)localObject2).startActivity((Intent)localObject1);
    }
    else
    {
      localObject2 = TAG;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("No activity found for URI: ");
      ((StringBuilder)localObject1).append(paramString);
      Log.w((String)localObject2, ((StringBuilder)localObject1).toString());
    }
  }
  
  public void handleLinkEvent(LinkTapEvent paramLinkTapEvent)
  {
    String str = paramLinkTapEvent.getLink().getUri();
    paramLinkTapEvent = paramLinkTapEvent.getLink().getDestPageIdx();
    if ((str != null) && (!str.isEmpty())) {
      handleUri(str);
    } else if (paramLinkTapEvent != null) {
      handlePage(paramLinkTapEvent.intValue());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/barteksc/pdfviewer/link/DefaultLinkHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */