package com.github.mikephil.charting.interfaces.dataprovider;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.LineData;

public abstract interface LineDataProvider
  extends BarLineScatterCandleBubbleDataProvider
{
  public abstract YAxis getAxis(YAxis.AxisDependency paramAxisDependency);
  
  public abstract LineData getLineData();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/interfaces/dataprovider/LineDataProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */