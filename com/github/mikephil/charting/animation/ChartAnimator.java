package com.github.mikephil.charting.animation;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.os.Build.VERSION;
import androidx.annotation.RequiresApi;

public class ChartAnimator
{
  private ValueAnimator.AnimatorUpdateListener mListener;
  protected float mPhaseX = 1.0F;
  protected float mPhaseY = 1.0F;
  
  public ChartAnimator() {}
  
  @RequiresApi(11)
  public ChartAnimator(ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener)
  {
    this.mListener = paramAnimatorUpdateListener;
  }
  
  @RequiresApi(11)
  private ObjectAnimator xAnimator(int paramInt, Easing.EasingFunction paramEasingFunction)
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "phaseX", new float[] { 0.0F, 1.0F });
    localObjectAnimator.setInterpolator(paramEasingFunction);
    localObjectAnimator.setDuration(paramInt);
    return localObjectAnimator;
  }
  
  @RequiresApi(11)
  private ObjectAnimator yAnimator(int paramInt, Easing.EasingFunction paramEasingFunction)
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "phaseY", new float[] { 0.0F, 1.0F });
    localObjectAnimator.setInterpolator(paramEasingFunction);
    localObjectAnimator.setDuration(paramInt);
    return localObjectAnimator;
  }
  
  @RequiresApi(11)
  public void animateX(int paramInt)
  {
    animateX(paramInt, Easing.Linear);
  }
  
  @RequiresApi(11)
  public void animateX(int paramInt, Easing.EasingFunction paramEasingFunction)
  {
    paramEasingFunction = xAnimator(paramInt, paramEasingFunction);
    paramEasingFunction.addUpdateListener(this.mListener);
    paramEasingFunction.start();
  }
  
  @Deprecated
  public void animateX(int paramInt, Easing.EasingOption paramEasingOption)
  {
    if (Build.VERSION.SDK_INT < 11) {
      return;
    }
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "phaseX", new float[] { 0.0F, 1.0F });
    localObjectAnimator.setInterpolator(Easing.getEasingFunctionFromOption(paramEasingOption));
    localObjectAnimator.setDuration(paramInt);
    localObjectAnimator.addUpdateListener(this.mListener);
    localObjectAnimator.start();
  }
  
  @RequiresApi(11)
  public void animateXY(int paramInt1, int paramInt2)
  {
    animateXY(paramInt1, paramInt2, Easing.Linear, Easing.Linear);
  }
  
  @RequiresApi(11)
  public void animateXY(int paramInt1, int paramInt2, Easing.EasingFunction paramEasingFunction)
  {
    ObjectAnimator localObjectAnimator = xAnimator(paramInt1, paramEasingFunction);
    paramEasingFunction = yAnimator(paramInt2, paramEasingFunction);
    if (paramInt1 > paramInt2) {
      localObjectAnimator.addUpdateListener(this.mListener);
    } else {
      paramEasingFunction.addUpdateListener(this.mListener);
    }
    localObjectAnimator.start();
    paramEasingFunction.start();
  }
  
  @RequiresApi(11)
  public void animateXY(int paramInt1, int paramInt2, Easing.EasingFunction paramEasingFunction1, Easing.EasingFunction paramEasingFunction2)
  {
    paramEasingFunction1 = xAnimator(paramInt1, paramEasingFunction1);
    paramEasingFunction2 = yAnimator(paramInt2, paramEasingFunction2);
    if (paramInt1 > paramInt2) {
      paramEasingFunction1.addUpdateListener(this.mListener);
    } else {
      paramEasingFunction2.addUpdateListener(this.mListener);
    }
    paramEasingFunction1.start();
    paramEasingFunction2.start();
  }
  
  @Deprecated
  public void animateXY(int paramInt1, int paramInt2, Easing.EasingOption paramEasingOption1, Easing.EasingOption paramEasingOption2)
  {
    if (Build.VERSION.SDK_INT < 11) {
      return;
    }
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "phaseY", new float[] { 0.0F, 1.0F });
    localObjectAnimator.setInterpolator(Easing.getEasingFunctionFromOption(paramEasingOption2));
    localObjectAnimator.setDuration(paramInt2);
    paramEasingOption2 = ObjectAnimator.ofFloat(this, "phaseX", new float[] { 0.0F, 1.0F });
    paramEasingOption2.setInterpolator(Easing.getEasingFunctionFromOption(paramEasingOption1));
    paramEasingOption2.setDuration(paramInt1);
    if (paramInt1 > paramInt2) {
      paramEasingOption2.addUpdateListener(this.mListener);
    } else {
      localObjectAnimator.addUpdateListener(this.mListener);
    }
    paramEasingOption2.start();
    localObjectAnimator.start();
  }
  
  @RequiresApi(11)
  public void animateY(int paramInt)
  {
    animateY(paramInt, Easing.Linear);
  }
  
  @RequiresApi(11)
  public void animateY(int paramInt, Easing.EasingFunction paramEasingFunction)
  {
    paramEasingFunction = yAnimator(paramInt, paramEasingFunction);
    paramEasingFunction.addUpdateListener(this.mListener);
    paramEasingFunction.start();
  }
  
  @Deprecated
  public void animateY(int paramInt, Easing.EasingOption paramEasingOption)
  {
    if (Build.VERSION.SDK_INT < 11) {
      return;
    }
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "phaseY", new float[] { 0.0F, 1.0F });
    localObjectAnimator.setInterpolator(Easing.getEasingFunctionFromOption(paramEasingOption));
    localObjectAnimator.setDuration(paramInt);
    localObjectAnimator.addUpdateListener(this.mListener);
    localObjectAnimator.start();
  }
  
  public float getPhaseX()
  {
    return this.mPhaseX;
  }
  
  public float getPhaseY()
  {
    return this.mPhaseY;
  }
  
  public void setPhaseX(float paramFloat)
  {
    float f;
    if (paramFloat > 1.0F)
    {
      f = 1.0F;
    }
    else
    {
      f = paramFloat;
      if (paramFloat < 0.0F) {
        f = 0.0F;
      }
    }
    this.mPhaseX = f;
  }
  
  public void setPhaseY(float paramFloat)
  {
    float f;
    if (paramFloat > 1.0F)
    {
      f = 1.0F;
    }
    else
    {
      f = paramFloat;
      if (paramFloat < 0.0F) {
        f = 0.0F;
      }
    }
    this.mPhaseY = f;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/animation/ChartAnimator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */