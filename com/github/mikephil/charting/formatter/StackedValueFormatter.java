package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.text.DecimalFormat;

public class StackedValueFormatter
  implements IValueFormatter
{
  private String mAppendix;
  private boolean mDrawWholeStack;
  private DecimalFormat mFormat;
  
  public StackedValueFormatter(boolean paramBoolean, String paramString, int paramInt)
  {
    this.mDrawWholeStack = paramBoolean;
    this.mAppendix = paramString;
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; i < paramInt; i++)
    {
      if (i == 0) {
        localStringBuffer.append(".");
      }
      localStringBuffer.append("0");
    }
    paramString = new StringBuilder();
    paramString.append("###,###,###,##0");
    paramString.append(localStringBuffer.toString());
    this.mFormat = new DecimalFormat(paramString.toString());
  }
  
  public String getFormattedValue(float paramFloat, Entry paramEntry, int paramInt, ViewPortHandler paramViewPortHandler)
  {
    if ((!this.mDrawWholeStack) && ((paramEntry instanceof BarEntry)))
    {
      paramEntry = (BarEntry)paramEntry;
      paramViewPortHandler = paramEntry.getYVals();
      if (paramViewPortHandler != null)
      {
        if (paramViewPortHandler[(paramViewPortHandler.length - 1)] == paramFloat)
        {
          paramViewPortHandler = new StringBuilder();
          paramViewPortHandler.append(this.mFormat.format(paramEntry.getY()));
          paramViewPortHandler.append(this.mAppendix);
          return paramViewPortHandler.toString();
        }
        return "";
      }
    }
    paramEntry = new StringBuilder();
    paramEntry.append(this.mFormat.format(paramFloat));
    paramEntry.append(this.mAppendix);
    return paramEntry.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/StackedValueFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */