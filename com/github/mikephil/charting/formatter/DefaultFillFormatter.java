package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class DefaultFillFormatter
  implements IFillFormatter
{
  public float getFillLinePosition(ILineDataSet paramILineDataSet, LineDataProvider paramLineDataProvider)
  {
    float f1 = paramLineDataProvider.getYChartMax();
    float f2 = paramLineDataProvider.getYChartMin();
    paramLineDataProvider = paramLineDataProvider.getLineData();
    float f4 = paramILineDataSet.getYMax();
    float f3 = 0.0F;
    if ((f4 > 0.0F) && (paramILineDataSet.getYMin() < 0.0F))
    {
      f1 = f3;
    }
    else
    {
      if (paramLineDataProvider.getYMax() > 0.0F) {
        f1 = 0.0F;
      }
      if (paramLineDataProvider.getYMin() < 0.0F) {
        f2 = 0.0F;
      }
      if (paramILineDataSet.getYMin() >= 0.0F) {
        f1 = f2;
      }
    }
    return f1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/DefaultFillFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */