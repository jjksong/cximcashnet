package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;
import java.text.DecimalFormat;

public class DefaultAxisValueFormatter
  implements IAxisValueFormatter
{
  protected int digits;
  protected DecimalFormat mFormat;
  
  public DefaultAxisValueFormatter(int paramInt)
  {
    int i = 0;
    this.digits = 0;
    this.digits = paramInt;
    StringBuffer localStringBuffer = new StringBuffer();
    while (i < paramInt)
    {
      if (i == 0) {
        localStringBuffer.append(".");
      }
      localStringBuffer.append("0");
      i++;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("###,###,###,##0");
    localStringBuilder.append(localStringBuffer.toString());
    this.mFormat = new DecimalFormat(localStringBuilder.toString());
  }
  
  public int getDecimalDigits()
  {
    return this.digits;
  }
  
  public String getFormattedValue(float paramFloat, AxisBase paramAxisBase)
  {
    return this.mFormat.format(paramFloat);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/DefaultAxisValueFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */