package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public abstract interface IFillFormatter
{
  public abstract float getFillLinePosition(ILineDataSet paramILineDataSet, LineDataProvider paramLineDataProvider);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/IFillFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */