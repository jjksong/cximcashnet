package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;

public abstract interface IAxisValueFormatter
{
  public abstract String getFormattedValue(float paramFloat, AxisBase paramAxisBase);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/IAxisValueFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */