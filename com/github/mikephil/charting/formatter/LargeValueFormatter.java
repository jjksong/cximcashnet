package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.text.DecimalFormat;

public class LargeValueFormatter
  implements IValueFormatter, IAxisValueFormatter
{
  private DecimalFormat mFormat = new DecimalFormat("###E00");
  private int mMaxLength = 5;
  private String[] mSuffix = { "", "k", "m", "b", "t" };
  private String mText = "";
  
  public LargeValueFormatter() {}
  
  public LargeValueFormatter(String paramString)
  {
    this();
    this.mText = paramString;
  }
  
  private String makePretty(double paramDouble)
  {
    String str = this.mFormat.format(paramDouble);
    int j = Character.getNumericValue(str.charAt(str.length() - 1));
    int i = Character.getNumericValue(str.charAt(str.length() - 2));
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(i);
    localStringBuilder.append("");
    localStringBuilder.append(j);
    i = Integer.valueOf(localStringBuilder.toString()).intValue();
    for (str = str.replaceAll("E[0-9][0-9]", this.mSuffix[(i / 3)]);; str = localStringBuilder.toString())
    {
      if ((str.length() <= this.mMaxLength) && (!str.matches("[0-9]+\\.[a-z]"))) {
        return str;
      }
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(str.substring(0, str.length() - 2));
      localStringBuilder.append(str.substring(str.length() - 1));
    }
  }
  
  public int getDecimalDigits()
  {
    return 0;
  }
  
  public String getFormattedValue(float paramFloat, AxisBase paramAxisBase)
  {
    paramAxisBase = new StringBuilder();
    paramAxisBase.append(makePretty(paramFloat));
    paramAxisBase.append(this.mText);
    return paramAxisBase.toString();
  }
  
  public String getFormattedValue(float paramFloat, Entry paramEntry, int paramInt, ViewPortHandler paramViewPortHandler)
  {
    paramEntry = new StringBuilder();
    paramEntry.append(makePretty(paramFloat));
    paramEntry.append(this.mText);
    return paramEntry.toString();
  }
  
  public void setAppendix(String paramString)
  {
    this.mText = paramString;
  }
  
  public void setMaxLength(int paramInt)
  {
    this.mMaxLength = paramInt;
  }
  
  public void setSuffix(String[] paramArrayOfString)
  {
    this.mSuffix = paramArrayOfString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/formatter/LargeValueFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */