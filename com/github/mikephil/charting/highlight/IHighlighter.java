package com.github.mikephil.charting.highlight;

public abstract interface IHighlighter
{
  public abstract Highlight getHighlight(float paramFloat1, float paramFloat2);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/highlight/IHighlighter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */