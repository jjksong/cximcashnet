package com.github.mikephil.charting.highlight;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.DataSet.Rounding;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.dataprovider.CombinedDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import java.util.Iterator;
import java.util.List;

public class CombinedHighlighter
  extends ChartHighlighter<CombinedDataProvider>
  implements IHighlighter
{
  protected BarHighlighter barHighlighter;
  
  public CombinedHighlighter(CombinedDataProvider paramCombinedDataProvider, BarDataProvider paramBarDataProvider)
  {
    super(paramCombinedDataProvider);
    if (paramBarDataProvider.getBarData() == null) {
      paramCombinedDataProvider = null;
    } else {
      paramCombinedDataProvider = new BarHighlighter(paramBarDataProvider);
    }
    this.barHighlighter = paramCombinedDataProvider;
  }
  
  protected List<Highlight> getHighlightsAtXValue(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.mHighlightBuffer.clear();
    List localList = ((CombinedDataProvider)this.mChart).getCombinedData().getAllData();
    for (int i = 0; i < localList.size(); i++)
    {
      Object localObject2 = (ChartData)localList.get(i);
      Object localObject1 = this.barHighlighter;
      if ((localObject1 != null) && ((localObject2 instanceof BarData)))
      {
        localObject1 = ((BarHighlighter)localObject1).getHighlight(paramFloat2, paramFloat3);
        if (localObject1 != null)
        {
          ((Highlight)localObject1).setDataIndex(i);
          this.mHighlightBuffer.add(localObject1);
        }
      }
      else
      {
        int k = ((ChartData)localObject2).getDataSetCount();
        for (int j = 0; j < k; j++)
        {
          localObject1 = ((BarLineScatterCandleBubbleData)localList.get(i)).getDataSetByIndex(j);
          if (((IDataSet)localObject1).isHighlightEnabled())
          {
            localObject1 = buildHighlights((IDataSet)localObject1, j, paramFloat1, DataSet.Rounding.CLOSEST).iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (Highlight)((Iterator)localObject1).next();
              ((Highlight)localObject2).setDataIndex(i);
              this.mHighlightBuffer.add(localObject2);
            }
          }
        }
      }
    }
    return this.mHighlightBuffer;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/highlight/CombinedHighlighter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */