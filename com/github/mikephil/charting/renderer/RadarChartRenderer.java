package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.Iterator;
import java.util.List;

public class RadarChartRenderer
  extends LineRadarRenderer
{
  protected RadarChart mChart;
  protected Path mDrawDataSetSurfacePathBuffer = new Path();
  protected Path mDrawHighlightCirclePathBuffer = new Path();
  protected Paint mHighlightCirclePaint;
  protected Paint mWebPaint;
  
  public RadarChartRenderer(RadarChart paramRadarChart, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramRadarChart;
    this.mHighlightPaint = new Paint(1);
    this.mHighlightPaint.setStyle(Paint.Style.STROKE);
    this.mHighlightPaint.setStrokeWidth(2.0F);
    this.mHighlightPaint.setColor(Color.rgb(255, 187, 115));
    this.mWebPaint = new Paint(1);
    this.mWebPaint.setStyle(Paint.Style.STROKE);
    this.mHighlightCirclePaint = new Paint(1);
  }
  
  public void drawData(Canvas paramCanvas)
  {
    Object localObject = (RadarData)this.mChart.getData();
    int i = ((IRadarDataSet)((RadarData)localObject).getMaxEntryCountSet()).getEntryCount();
    Iterator localIterator = ((RadarData)localObject).getDataSets().iterator();
    while (localIterator.hasNext())
    {
      localObject = (IRadarDataSet)localIterator.next();
      if (((IRadarDataSet)localObject).isVisible()) {
        drawDataSet(paramCanvas, (IRadarDataSet)localObject, i);
      }
    }
  }
  
  protected void drawDataSet(Canvas paramCanvas, IRadarDataSet paramIRadarDataSet, int paramInt)
  {
    float f4 = this.mAnimator.getPhaseX();
    float f3 = this.mAnimator.getPhaseY();
    float f2 = this.mChart.getSliceAngle();
    float f1 = this.mChart.getFactor();
    MPPointF localMPPointF1 = this.mChart.getCenterOffsets();
    MPPointF localMPPointF2 = MPPointF.getInstance(0.0F, 0.0F);
    Path localPath = this.mDrawDataSetSurfacePathBuffer;
    localPath.reset();
    int i = 0;
    int j = 0;
    while (i < paramIRadarDataSet.getEntryCount())
    {
      this.mRenderPaint.setColor(paramIRadarDataSet.getColor(i));
      Utils.getPosition(localMPPointF1, (((RadarEntry)paramIRadarDataSet.getEntryForIndex(i)).getY() - this.mChart.getYChartMin()) * f1 * f3, i * f2 * f4 + this.mChart.getRotationAngle(), localMPPointF2);
      if (!Float.isNaN(localMPPointF2.x)) {
        if (j == 0)
        {
          localPath.moveTo(localMPPointF2.x, localMPPointF2.y);
          j = 1;
        }
        else
        {
          localPath.lineTo(localMPPointF2.x, localMPPointF2.y);
        }
      }
      i++;
    }
    if (paramIRadarDataSet.getEntryCount() > paramInt) {
      localPath.lineTo(localMPPointF1.x, localMPPointF1.y);
    }
    localPath.close();
    if (paramIRadarDataSet.isDrawFilledEnabled())
    {
      Drawable localDrawable = paramIRadarDataSet.getFillDrawable();
      if (localDrawable != null) {
        drawFilledPath(paramCanvas, localPath, localDrawable);
      } else {
        drawFilledPath(paramCanvas, localPath, paramIRadarDataSet.getFillColor(), paramIRadarDataSet.getFillAlpha());
      }
    }
    this.mRenderPaint.setStrokeWidth(paramIRadarDataSet.getLineWidth());
    this.mRenderPaint.setStyle(Paint.Style.STROKE);
    if ((!paramIRadarDataSet.isDrawFilledEnabled()) || (paramIRadarDataSet.getFillAlpha() < 255)) {
      paramCanvas.drawPath(localPath, this.mRenderPaint);
    }
    MPPointF.recycleInstance(localMPPointF1);
    MPPointF.recycleInstance(localMPPointF2);
  }
  
  public void drawExtras(Canvas paramCanvas)
  {
    drawWeb(paramCanvas);
  }
  
  public void drawHighlightCircle(Canvas paramCanvas, MPPointF paramMPPointF, float paramFloat1, float paramFloat2, int paramInt1, int paramInt2, float paramFloat3)
  {
    paramCanvas.save();
    paramFloat2 = Utils.convertDpToPixel(paramFloat2);
    paramFloat1 = Utils.convertDpToPixel(paramFloat1);
    if (paramInt1 != 1122867)
    {
      Path localPath = this.mDrawHighlightCirclePathBuffer;
      localPath.reset();
      localPath.addCircle(paramMPPointF.x, paramMPPointF.y, paramFloat2, Path.Direction.CW);
      if (paramFloat1 > 0.0F) {
        localPath.addCircle(paramMPPointF.x, paramMPPointF.y, paramFloat1, Path.Direction.CCW);
      }
      this.mHighlightCirclePaint.setColor(paramInt1);
      this.mHighlightCirclePaint.setStyle(Paint.Style.FILL);
      paramCanvas.drawPath(localPath, this.mHighlightCirclePaint);
    }
    if (paramInt2 != 1122867)
    {
      this.mHighlightCirclePaint.setColor(paramInt2);
      this.mHighlightCirclePaint.setStyle(Paint.Style.STROKE);
      this.mHighlightCirclePaint.setStrokeWidth(Utils.convertDpToPixel(paramFloat3));
      paramCanvas.drawCircle(paramMPPointF.x, paramMPPointF.y, paramFloat2, this.mHighlightCirclePaint);
    }
    paramCanvas.restore();
  }
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    float f1 = this.mChart.getSliceAngle();
    float f2 = this.mChart.getFactor();
    MPPointF localMPPointF2 = this.mChart.getCenterOffsets();
    MPPointF localMPPointF1 = MPPointF.getInstance(0.0F, 0.0F);
    RadarData localRadarData = (RadarData)this.mChart.getData();
    int m = paramArrayOfHighlight.length;
    for (int j = 0; j < m; j++)
    {
      Highlight localHighlight = paramArrayOfHighlight[j];
      IRadarDataSet localIRadarDataSet = (IRadarDataSet)localRadarData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localIRadarDataSet != null) && (localIRadarDataSet.isHighlightEnabled()))
      {
        RadarEntry localRadarEntry = (RadarEntry)localIRadarDataSet.getEntryForIndex((int)localHighlight.getX());
        if (isInBoundsX(localRadarEntry, localIRadarDataSet))
        {
          Utils.getPosition(localMPPointF2, (localRadarEntry.getY() - this.mChart.getYChartMin()) * f2 * this.mAnimator.getPhaseY(), localHighlight.getX() * f1 * this.mAnimator.getPhaseX() + this.mChart.getRotationAngle(), localMPPointF1);
          localHighlight.setDraw(localMPPointF1.x, localMPPointF1.y);
          drawHighlightLines(paramCanvas, localMPPointF1.x, localMPPointF1.y, localIRadarDataSet);
          if (localIRadarDataSet.isDrawHighlightCircleEnabled())
          {
            if ((!Float.isNaN(localMPPointF1.x)) && (!Float.isNaN(localMPPointF1.y)))
            {
              int k = localIRadarDataSet.getHighlightCircleStrokeColor();
              int i = k;
              if (k == 1122867) {
                i = localIRadarDataSet.getColor(0);
              }
              if (localIRadarDataSet.getHighlightCircleStrokeAlpha() < 255) {
                i = ColorTemplate.colorWithAlpha(i, localIRadarDataSet.getHighlightCircleStrokeAlpha());
              }
              drawHighlightCircle(paramCanvas, localMPPointF1, localIRadarDataSet.getHighlightCircleInnerRadius(), localIRadarDataSet.getHighlightCircleOuterRadius(), localIRadarDataSet.getHighlightCircleFillColor(), i, localIRadarDataSet.getHighlightCircleStrokeWidth());
            }
          }
          else {}
        }
      }
    }
    MPPointF.recycleInstance(localMPPointF2);
    MPPointF.recycleInstance(localMPPointF1);
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    float f1 = this.mAnimator.getPhaseX();
    float f5 = this.mAnimator.getPhaseY();
    float f2 = this.mChart.getSliceAngle();
    float f4 = this.mChart.getFactor();
    MPPointF localMPPointF3 = this.mChart.getCenterOffsets();
    MPPointF localMPPointF4 = MPPointF.getInstance(0.0F, 0.0F);
    MPPointF localMPPointF1 = MPPointF.getInstance(0.0F, 0.0F);
    float f3 = Utils.convertDpToPixel(5.0F);
    for (int i = 0; i < ((RadarData)this.mChart.getData()).getDataSetCount(); i++)
    {
      IRadarDataSet localIRadarDataSet = (IRadarDataSet)((RadarData)this.mChart.getData()).getDataSetByIndex(i);
      if (shouldDrawValues(localIRadarDataSet))
      {
        applyValueTextStyle(localIRadarDataSet);
        MPPointF localMPPointF2 = MPPointF.getInstance(localIRadarDataSet.getIconsOffset());
        localMPPointF2.x = Utils.convertDpToPixel(localMPPointF2.x);
        localMPPointF2.y = Utils.convertDpToPixel(localMPPointF2.y);
        for (int j = 0; j < localIRadarDataSet.getEntryCount(); j++)
        {
          RadarEntry localRadarEntry = (RadarEntry)localIRadarDataSet.getEntryForIndex(j);
          float f8 = localRadarEntry.getY();
          float f7 = this.mChart.getYChartMin();
          float f6 = j * f2 * f1;
          Utils.getPosition(localMPPointF3, (f8 - f7) * f4 * f5, f6 + this.mChart.getRotationAngle(), localMPPointF4);
          if (localIRadarDataSet.isDrawValuesEnabled()) {
            drawValue(paramCanvas, localIRadarDataSet.getValueFormatter(), localRadarEntry.getY(), localRadarEntry, i, localMPPointF4.x, localMPPointF4.y - f3, localIRadarDataSet.getValueTextColor(j));
          }
          if ((localRadarEntry.getIcon() != null) && (localIRadarDataSet.isDrawIconsEnabled()))
          {
            Drawable localDrawable = localRadarEntry.getIcon();
            Utils.getPosition(localMPPointF3, localRadarEntry.getY() * f4 * f5 + localMPPointF2.y, f6 + this.mChart.getRotationAngle(), localMPPointF1);
            localMPPointF1.y += localMPPointF2.x;
            Utils.drawImage(paramCanvas, localDrawable, (int)localMPPointF1.x, (int)localMPPointF1.y, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
          }
        }
        MPPointF.recycleInstance(localMPPointF2);
      }
    }
    MPPointF.recycleInstance(localMPPointF3);
    MPPointF.recycleInstance(localMPPointF4);
    MPPointF.recycleInstance(localMPPointF1);
  }
  
  protected void drawWeb(Canvas paramCanvas)
  {
    float f2 = this.mChart.getSliceAngle();
    float f4 = this.mChart.getFactor();
    float f3 = this.mChart.getRotationAngle();
    MPPointF localMPPointF1 = this.mChart.getCenterOffsets();
    this.mWebPaint.setStrokeWidth(this.mChart.getWebLineWidth());
    this.mWebPaint.setColor(this.mChart.getWebColor());
    this.mWebPaint.setAlpha(this.mChart.getWebAlpha());
    int k = this.mChart.getSkipWebLineCount();
    int j = ((IRadarDataSet)((RadarData)this.mChart.getData()).getMaxEntryCountSet()).getEntryCount();
    MPPointF localMPPointF2 = MPPointF.getInstance(0.0F, 0.0F);
    int i = 0;
    while (i < j)
    {
      Utils.getPosition(localMPPointF1, this.mChart.getYRange() * f4, i * f2 + f3, localMPPointF2);
      paramCanvas.drawLine(localMPPointF1.x, localMPPointF1.y, localMPPointF2.x, localMPPointF2.y, this.mWebPaint);
      i += k + 1;
    }
    MPPointF.recycleInstance(localMPPointF2);
    this.mWebPaint.setStrokeWidth(this.mChart.getWebLineWidthInner());
    this.mWebPaint.setColor(this.mChart.getWebColorInner());
    this.mWebPaint.setAlpha(this.mChart.getWebAlpha());
    k = this.mChart.getYAxis().mEntryCount;
    localMPPointF2 = MPPointF.getInstance(0.0F, 0.0F);
    MPPointF localMPPointF3 = MPPointF.getInstance(0.0F, 0.0F);
    for (i = 0; i < k; i++)
    {
      j = 0;
      while (j < ((RadarData)this.mChart.getData()).getEntryCount())
      {
        float f1 = (this.mChart.getYAxis().mEntries[i] - this.mChart.getYChartMin()) * f4;
        Utils.getPosition(localMPPointF1, f1, j * f2 + f3, localMPPointF2);
        j++;
        Utils.getPosition(localMPPointF1, f1, j * f2 + f3, localMPPointF3);
        paramCanvas.drawLine(localMPPointF2.x, localMPPointF2.y, localMPPointF3.x, localMPPointF3.y, this.mWebPaint);
      }
    }
    MPPointF.recycleInstance(localMPPointF2);
    MPPointF.recycleInstance(localMPPointF3);
  }
  
  public Paint getWebPaint()
  {
    return this.mWebPaint;
  }
  
  public void initBuffers() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/RadarChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */