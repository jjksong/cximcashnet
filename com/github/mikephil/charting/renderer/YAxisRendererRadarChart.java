package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class YAxisRendererRadarChart
  extends YAxisRenderer
{
  private RadarChart mChart;
  private Path mRenderLimitLinesPathBuffer = new Path();
  
  public YAxisRendererRadarChart(ViewPortHandler paramViewPortHandler, YAxis paramYAxis, RadarChart paramRadarChart)
  {
    super(paramViewPortHandler, paramYAxis, null);
    this.mChart = paramRadarChart;
  }
  
  protected void computeAxisValues(float paramFloat1, float paramFloat2)
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public void renderAxisLabels(Canvas paramCanvas)
  {
    if ((this.mYAxis.isEnabled()) && (this.mYAxis.isDrawLabelsEnabled()))
    {
      this.mAxisLabelPaint.setTypeface(this.mYAxis.getTypeface());
      this.mAxisLabelPaint.setTextSize(this.mYAxis.getTextSize());
      this.mAxisLabelPaint.setColor(this.mYAxis.getTextColor());
      MPPointF localMPPointF2 = this.mChart.getCenterOffsets();
      MPPointF localMPPointF1 = MPPointF.getInstance(0.0F, 0.0F);
      float f = this.mChart.getFactor();
      int j = this.mYAxis.isDrawBottomYLabelEntryEnabled() ^ true;
      int i;
      if (this.mYAxis.isDrawTopYLabelEntryEnabled()) {
        i = this.mYAxis.mEntryCount;
      } else {
        i = this.mYAxis.mEntryCount - 1;
      }
      while (j < i)
      {
        Utils.getPosition(localMPPointF2, (this.mYAxis.mEntries[j] - this.mYAxis.mAxisMinimum) * f, this.mChart.getRotationAngle(), localMPPointF1);
        paramCanvas.drawText(this.mYAxis.getFormattedLabel(j), localMPPointF1.x + 10.0F, localMPPointF1.y, this.mAxisLabelPaint);
        j++;
      }
      MPPointF.recycleInstance(localMPPointF2);
      MPPointF.recycleInstance(localMPPointF1);
      return;
    }
  }
  
  public void renderLimitLines(Canvas paramCanvas)
  {
    List localList = this.mYAxis.getLimitLines();
    if (localList == null) {
      return;
    }
    float f3 = this.mChart.getSliceAngle();
    float f2 = this.mChart.getFactor();
    MPPointF localMPPointF1 = this.mChart.getCenterOffsets();
    MPPointF localMPPointF2 = MPPointF.getInstance(0.0F, 0.0F);
    for (int i = 0; i < localList.size(); i++)
    {
      Object localObject = (LimitLine)localList.get(i);
      if (((LimitLine)localObject).isEnabled())
      {
        this.mLimitLinePaint.setColor(((LimitLine)localObject).getLineColor());
        this.mLimitLinePaint.setPathEffect(((LimitLine)localObject).getDashPathEffect());
        this.mLimitLinePaint.setStrokeWidth(((LimitLine)localObject).getLineWidth());
        float f1 = ((LimitLine)localObject).getLimit();
        float f4 = this.mChart.getYChartMin();
        localObject = this.mRenderLimitLinesPathBuffer;
        ((Path)localObject).reset();
        for (int j = 0; j < ((IRadarDataSet)((RadarData)this.mChart.getData()).getMaxEntryCountSet()).getEntryCount(); j++)
        {
          Utils.getPosition(localMPPointF1, (f1 - f4) * f2, j * f3 + this.mChart.getRotationAngle(), localMPPointF2);
          if (j == 0) {
            ((Path)localObject).moveTo(localMPPointF2.x, localMPPointF2.y);
          } else {
            ((Path)localObject).lineTo(localMPPointF2.x, localMPPointF2.y);
          }
        }
        ((Path)localObject).close();
        paramCanvas.drawPath((Path)localObject, this.mLimitLinePaint);
      }
    }
    MPPointF.recycleInstance(localMPPointF1);
    MPPointF.recycleInstance(localMPPointF2);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/YAxisRendererRadarChart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */