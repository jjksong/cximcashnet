package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.buffer.HorizontalBarBuffer;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class HorizontalBarChartRenderer
  extends BarChartRenderer
{
  private RectF mBarShadowRectBuffer = new RectF();
  
  public HorizontalBarChartRenderer(BarDataProvider paramBarDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramBarDataProvider, paramChartAnimator, paramViewPortHandler);
    this.mValuePaint.setTextAlign(Paint.Align.LEFT);
  }
  
  protected void drawDataSet(Canvas paramCanvas, IBarDataSet paramIBarDataSet, int paramInt)
  {
    Object localObject2 = this.mChart.getTransformer(paramIBarDataSet.getAxisDependency());
    this.mBarBorderPaint.setColor(paramIBarDataSet.getBarBorderColor());
    this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(paramIBarDataSet.getBarBorderWidth()));
    float f1 = paramIBarDataSet.getBarBorderWidth();
    int k = 0;
    int m = 1;
    int i;
    if (f1 > 0.0F) {
      i = 1;
    } else {
      i = 0;
    }
    float f2 = this.mAnimator.getPhaseX();
    float f4 = this.mAnimator.getPhaseY();
    int n;
    if (this.mChart.isDrawBarShadowEnabled())
    {
      this.mShadowPaint.setColor(paramIBarDataSet.getBarShadowColor());
      f1 = this.mChart.getBarData().getBarWidth() / 2.0F;
      n = Math.min((int)Math.ceil(paramIBarDataSet.getEntryCount() * f2), paramIBarDataSet.getEntryCount());
      for (j = 0; j < n; j++)
      {
        float f3 = ((BarEntry)paramIBarDataSet.getEntryForIndex(j)).getX();
        localObject1 = this.mBarShadowRectBuffer;
        ((RectF)localObject1).top = (f3 - f1);
        ((RectF)localObject1).bottom = (f3 + f1);
        ((Transformer)localObject2).rectValueToPixel((RectF)localObject1);
        if (this.mViewPortHandler.isInBoundsTop(this.mBarShadowRectBuffer.bottom))
        {
          if (!this.mViewPortHandler.isInBoundsBottom(this.mBarShadowRectBuffer.top)) {
            break;
          }
          this.mBarShadowRectBuffer.left = this.mViewPortHandler.contentLeft();
          this.mBarShadowRectBuffer.right = this.mViewPortHandler.contentRight();
          paramCanvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
        }
      }
    }
    Object localObject1 = this.mBarBuffers[paramInt];
    ((BarBuffer)localObject1).setPhases(f2, f4);
    ((BarBuffer)localObject1).setDataSet(paramInt);
    ((BarBuffer)localObject1).setInverted(this.mChart.isInverted(paramIBarDataSet.getAxisDependency()));
    ((BarBuffer)localObject1).setBarWidth(this.mChart.getBarData().getBarWidth());
    ((BarBuffer)localObject1).feed(paramIBarDataSet);
    ((Transformer)localObject2).pointValuesToPixel(((BarBuffer)localObject1).buffer);
    if (paramIBarDataSet.getColors().size() == 1) {
      paramInt = m;
    } else {
      paramInt = 0;
    }
    int j = k;
    if (paramInt != 0) {
      this.mRenderPaint.setColor(paramIBarDataSet.getColor());
    }
    for (j = k; j < ((BarBuffer)localObject1).size(); j += 4)
    {
      localObject2 = this.mViewPortHandler;
      float[] arrayOfFloat = ((BarBuffer)localObject1).buffer;
      m = j + 3;
      if (!((ViewPortHandler)localObject2).isInBoundsTop(arrayOfFloat[m])) {
        break;
      }
      localObject2 = this.mViewPortHandler;
      arrayOfFloat = ((BarBuffer)localObject1).buffer;
      k = j + 1;
      if (((ViewPortHandler)localObject2).isInBoundsBottom(arrayOfFloat[k]))
      {
        if (paramInt == 0) {
          this.mRenderPaint.setColor(paramIBarDataSet.getColor(j / 4));
        }
        f2 = localObject1.buffer[j];
        f1 = localObject1.buffer[k];
        localObject2 = ((BarBuffer)localObject1).buffer;
        n = j + 2;
        paramCanvas.drawRect(f2, f1, localObject2[n], localObject1.buffer[m], this.mRenderPaint);
        if (i != 0) {
          paramCanvas.drawRect(localObject1.buffer[j], localObject1.buffer[k], localObject1.buffer[n], localObject1.buffer[m], this.mBarBorderPaint);
        }
      }
    }
  }
  
  protected void drawValue(Canvas paramCanvas, String paramString, float paramFloat1, float paramFloat2, int paramInt)
  {
    this.mValuePaint.setColor(paramInt);
    paramCanvas.drawText(paramString, paramFloat1, paramFloat2, this.mValuePaint);
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    if (isDrawingValuesAllowed(this.mChart))
    {
      Object localObject1 = this.mChart.getBarData().getDataSets();
      float f1 = Utils.convertDpToPixel(5.0F);
      boolean bool1 = this.mChart.isDrawValueAboveBarEnabled();
      for (int j = 0; j < this.mChart.getBarData().getDataSetCount(); j++)
      {
        IBarDataSet localIBarDataSet = (IBarDataSet)((List)localObject1).get(j);
        if (shouldDrawValues(localIBarDataSet))
        {
          boolean bool2 = this.mChart.isInverted(localIBarDataSet.getAxisDependency());
          applyValueTextStyle(localIBarDataSet);
          float f8 = Utils.calcTextHeight(this.mValuePaint, "10") / 2.0F;
          Object localObject6 = localIBarDataSet.getValueFormatter();
          Object localObject5 = this.mBarBuffers[j];
          float f9 = this.mAnimator.getPhaseY();
          MPPointF localMPPointF = MPPointF.getInstance(localIBarDataSet.getIconsOffset());
          localMPPointF.x = Utils.convertDpToPixel(localMPPointF.x);
          localMPPointF.y = Utils.convertDpToPixel(localMPPointF.y);
          int i;
          float f4;
          Object localObject2;
          Object localObject3;
          Object localObject4;
          int k;
          float f6;
          float f7;
          float f5;
          float f3;
          float f2;
          if (!localIBarDataSet.isStacked())
          {
            i = 0;
            f4 = f8;
            localObject2 = localObject1;
            localObject1 = localObject6;
            localObject3 = localObject5;
            while (i < ((BarBuffer)localObject3).buffer.length * this.mAnimator.getPhaseX())
            {
              localObject4 = ((BarBuffer)localObject3).buffer;
              k = i + 1;
              f6 = (localObject4[k] + localObject3.buffer[(i + 3)]) / 2.0F;
              if (!this.mViewPortHandler.isInBoundsTop(localObject3.buffer[k])) {
                break;
              }
              if ((this.mViewPortHandler.isInBoundsX(localObject3.buffer[i])) && (this.mViewPortHandler.isInBoundsBottom(localObject3.buffer[k])))
              {
                localObject6 = (BarEntry)localIBarDataSet.getEntryForIndex(i / 4);
                f7 = ((BarEntry)localObject6).getY();
                localObject4 = ((IValueFormatter)localObject1).getFormattedValue(f7, (Entry)localObject6, j, this.mViewPortHandler);
                f5 = Utils.calcTextWidth(this.mValuePaint, (String)localObject4);
                if (bool1) {
                  f3 = f1;
                } else {
                  f3 = -(f5 + f1);
                }
                if (bool1) {
                  f2 = -(f5 + f1);
                } else {
                  f2 = f1;
                }
                if (bool2)
                {
                  f8 = -f3;
                  f3 = -f2;
                  f2 = f8 - f5;
                  f3 -= f5;
                }
                else
                {
                  f5 = f2;
                  f2 = f3;
                  f3 = f5;
                }
                if (localIBarDataSet.isDrawValuesEnabled())
                {
                  f8 = localObject3.buffer[(i + 2)];
                  if (f7 >= 0.0F) {
                    f5 = f2;
                  } else {
                    f5 = f3;
                  }
                  drawValue(paramCanvas, (String)localObject4, f5 + f8, f6 + f4, localIBarDataSet.getValueTextColor(i / 2));
                }
                localObject4 = localObject1;
                localObject5 = localMPPointF;
                localObject1 = localObject4;
                if (((BarEntry)localObject6).getIcon() != null)
                {
                  localObject1 = localObject4;
                  if (localIBarDataSet.isDrawIconsEnabled())
                  {
                    localObject1 = ((BarEntry)localObject6).getIcon();
                    f5 = localObject3.buffer[(i + 2)];
                    if (f7 >= 0.0F) {
                      f3 = f2;
                    }
                    f7 = ((MPPointF)localObject5).x;
                    f2 = ((MPPointF)localObject5).y;
                    Utils.drawImage(paramCanvas, (Drawable)localObject1, (int)(f5 + f3 + f7), (int)(f6 + f2), ((Drawable)localObject1).getIntrinsicWidth(), ((Drawable)localObject1).getIntrinsicHeight());
                    localObject1 = localObject4;
                  }
                }
              }
              i += 4;
            }
            localObject1 = localObject2;
          }
          else
          {
            Transformer localTransformer = this.mChart.getTransformer(localIBarDataSet.getAxisDependency());
            k = 0;
            i = 0;
            while (k < localIBarDataSet.getEntryCount() * this.mAnimator.getPhaseX())
            {
              localObject3 = (BarEntry)localIBarDataSet.getEntryForIndex(k);
              int i1 = localIBarDataSet.getValueTextColor(k);
              localObject2 = ((BarEntry)localObject3).getYVals();
              Object localObject7;
              int m;
              if (localObject2 == null)
              {
                localObject7 = this.mViewPortHandler;
                localObject4 = ((BarBuffer)localObject5).buffer;
                m = i + 1;
                if (!((ViewPortHandler)localObject7).isInBoundsTop(localObject4[m])) {
                  break;
                }
                if ((!this.mViewPortHandler.isInBoundsX(localObject5.buffer[i])) || (!this.mViewPortHandler.isInBoundsBottom(localObject5.buffer[m]))) {
                  continue;
                }
                localObject4 = ((IValueFormatter)localObject6).getFormattedValue(((BarEntry)localObject3).getY(), (Entry)localObject3, j, this.mViewPortHandler);
                f4 = Utils.calcTextWidth(this.mValuePaint, (String)localObject4);
                if (bool1) {
                  f3 = f1;
                } else {
                  f3 = -(f4 + f1);
                }
                if (bool1) {
                  f2 = -(f4 + f1);
                } else {
                  f2 = f1;
                }
                if (bool2)
                {
                  f5 = -f3;
                  f3 = -f2;
                  f2 = f5 - f4;
                  f3 -= f4;
                }
                else
                {
                  f4 = f3;
                  f3 = f2;
                  f2 = f4;
                }
                if (localIBarDataSet.isDrawValuesEnabled())
                {
                  f5 = localObject5.buffer[(i + 2)];
                  if (((BarEntry)localObject3).getY() >= 0.0F) {
                    f4 = f2;
                  } else {
                    f4 = f3;
                  }
                  drawValue(paramCanvas, (String)localObject4, f5 + f4, localObject5.buffer[m] + f8, i1);
                }
                if ((((BarEntry)localObject3).getIcon() != null) && (localIBarDataSet.isDrawIconsEnabled()))
                {
                  localObject4 = ((BarEntry)localObject3).getIcon();
                  f4 = localObject5.buffer[(i + 2)];
                  if (((BarEntry)localObject3).getY() >= 0.0F) {
                    f3 = f2;
                  }
                  f5 = localObject5.buffer[m];
                  f2 = localMPPointF.x;
                  f6 = localMPPointF.y;
                  Utils.drawImage(paramCanvas, (Drawable)localObject4, (int)(f4 + f3 + f2), (int)(f5 + f6), ((Drawable)localObject4).getIntrinsicWidth(), ((Drawable)localObject4).getIntrinsicHeight());
                }
              }
              else
              {
                localObject7 = localObject2;
                Object localObject8 = new float[localObject7.length * 2];
                f2 = -((BarEntry)localObject3).getNegativeSum();
                int n = 0;
                m = 0;
                f7 = 0.0F;
                while (n < localObject8.length)
                {
                  f6 = localObject7[m];
                  if (f6 == 0.0F)
                  {
                    f3 = f6;
                    f5 = f7;
                    f4 = f2;
                    if (f7 == 0.0F) {
                      break label1231;
                    }
                    if (f2 == 0.0F)
                    {
                      f3 = f6;
                      f5 = f7;
                      f4 = f2;
                      break label1231;
                    }
                  }
                  if (f6 >= 0.0F)
                  {
                    f3 = f7 + f6;
                    f5 = f3;
                    f4 = f2;
                  }
                  else
                  {
                    f4 = f2 - f6;
                    f5 = f7;
                    f3 = f2;
                  }
                  label1231:
                  localObject8[n] = (f3 * f9);
                  n += 2;
                  m++;
                  f7 = f5;
                  f2 = f4;
                }
                localTransformer.pointValuesToPixel((float[])localObject8);
                m = 0;
                localObject4 = localObject3;
                localObject3 = localObject8;
                while (m < localObject3.length)
                {
                  float f10 = localObject7[(m / 2)];
                  localObject8 = ((IValueFormatter)localObject6).getFormattedValue(f10, (Entry)localObject4, j, this.mViewPortHandler);
                  float f11 = Utils.calcTextWidth(this.mValuePaint, (String)localObject8);
                  if (bool1) {
                    f4 = f1;
                  } else {
                    f4 = -(f11 + f1);
                  }
                  if (bool1) {
                    f5 = -(f11 + f1);
                  } else {
                    f5 = f1;
                  }
                  f6 = f4;
                  f3 = f5;
                  if (bool2)
                  {
                    f6 = -f4 - f11;
                    f3 = -f5 - f11;
                  }
                  if (((f10 == 0.0F) && (f2 == 0.0F) && (f7 > 0.0F)) || (f10 < 0.0F)) {
                    n = 1;
                  } else {
                    n = 0;
                  }
                  f4 = localObject3[m];
                  if (n != 0) {
                    f6 = f3;
                  }
                  f4 += f6;
                  f3 = (localObject5.buffer[(i + 1)] + localObject5.buffer[(i + 3)]) / 2.0F;
                  if (!this.mViewPortHandler.isInBoundsTop(f3)) {
                    break;
                  }
                  if ((this.mViewPortHandler.isInBoundsX(f4)) && (this.mViewPortHandler.isInBoundsBottom(f3)))
                  {
                    if (localIBarDataSet.isDrawValuesEnabled()) {
                      drawValue(paramCanvas, (String)localObject8, f4, f3 + f8, i1);
                    }
                    if ((((BarEntry)localObject4).getIcon() != null) && (localIBarDataSet.isDrawIconsEnabled()))
                    {
                      localObject8 = ((BarEntry)localObject4).getIcon();
                      Utils.drawImage(paramCanvas, (Drawable)localObject8, (int)(f4 + localMPPointF.x), (int)(f3 + localMPPointF.y), ((Drawable)localObject8).getIntrinsicWidth(), ((Drawable)localObject8).getIntrinsicHeight());
                    }
                  }
                  m += 2;
                }
              }
              if (localObject2 == null) {
                i += 4;
              } else {
                i += localObject2.length * 4;
              }
              k++;
            }
          }
          MPPointF.recycleInstance(localMPPointF);
        }
      }
    }
  }
  
  public void initBuffers()
  {
    BarData localBarData = this.mChart.getBarData();
    this.mBarBuffers = new HorizontalBarBuffer[localBarData.getDataSetCount()];
    for (int i = 0; i < this.mBarBuffers.length; i++)
    {
      IBarDataSet localIBarDataSet = (IBarDataSet)localBarData.getDataSetByIndex(i);
      BarBuffer[] arrayOfBarBuffer = this.mBarBuffers;
      int k = localIBarDataSet.getEntryCount();
      int j;
      if (localIBarDataSet.isStacked()) {
        j = localIBarDataSet.getStackSize();
      } else {
        j = 1;
      }
      arrayOfBarBuffer[i] = new HorizontalBarBuffer(k * 4 * j, localBarData.getDataSetCount(), localIBarDataSet.isStacked());
    }
  }
  
  protected boolean isDrawingValuesAllowed(ChartInterface paramChartInterface)
  {
    boolean bool;
    if (paramChartInterface.getData().getEntryCount() < paramChartInterface.getMaxVisibleCount() * this.mViewPortHandler.getScaleY()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected void prepareBarHighlight(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Transformer paramTransformer)
  {
    this.mBarRect.set(paramFloat2, paramFloat1 - paramFloat4, paramFloat3, paramFloat1 + paramFloat4);
    paramTransformer.rectToPixelPhaseHorizontal(this.mBarRect, this.mAnimator.getPhaseY());
  }
  
  protected void setHighlightDrawPos(Highlight paramHighlight, RectF paramRectF)
  {
    paramHighlight.setDraw(paramRectF.centerY(), paramRectF.right);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/HorizontalBarChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */