package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class YAxisRenderer
  extends AxisRenderer
{
  protected Path mDrawZeroLinePath = new Path();
  protected float[] mGetTransformedPositionsBuffer = new float[2];
  protected RectF mGridClippingRect = new RectF();
  protected RectF mLimitLineClippingRect = new RectF();
  protected Path mRenderGridLinesPath = new Path();
  protected Path mRenderLimitLines = new Path();
  protected float[] mRenderLimitLinesBuffer = new float[2];
  protected YAxis mYAxis;
  protected RectF mZeroLineClippingRect = new RectF();
  protected Paint mZeroLinePaint;
  
  public YAxisRenderer(ViewPortHandler paramViewPortHandler, YAxis paramYAxis, Transformer paramTransformer)
  {
    super(paramViewPortHandler, paramTransformer, paramYAxis);
    this.mYAxis = paramYAxis;
    if (this.mViewPortHandler != null)
    {
      this.mAxisLabelPaint.setColor(-16777216);
      this.mAxisLabelPaint.setTextSize(Utils.convertDpToPixel(10.0F));
      this.mZeroLinePaint = new Paint(1);
      this.mZeroLinePaint.setColor(-7829368);
      this.mZeroLinePaint.setStrokeWidth(1.0F);
      this.mZeroLinePaint.setStyle(Paint.Style.STROKE);
    }
  }
  
  protected void drawYLabels(Canvas paramCanvas, float paramFloat1, float[] paramArrayOfFloat, float paramFloat2)
  {
    int j = this.mYAxis.isDrawBottomYLabelEntryEnabled() ^ true;
    int i;
    if (this.mYAxis.isDrawTopYLabelEntryEnabled()) {
      i = this.mYAxis.mEntryCount;
    } else {
      i = this.mYAxis.mEntryCount - 1;
    }
    while (j < i)
    {
      paramCanvas.drawText(this.mYAxis.getFormattedLabel(j), paramFloat1, paramArrayOfFloat[(j * 2 + 1)] + paramFloat2, this.mAxisLabelPaint);
      j++;
    }
  }
  
  protected void drawZeroLine(Canvas paramCanvas)
  {
    int i = paramCanvas.save();
    this.mZeroLineClippingRect.set(this.mViewPortHandler.getContentRect());
    this.mZeroLineClippingRect.inset(0.0F, -this.mYAxis.getZeroLineWidth());
    paramCanvas.clipRect(this.mZeroLineClippingRect);
    MPPointD localMPPointD = this.mTrans.getPixelForValues(0.0F, 0.0F);
    this.mZeroLinePaint.setColor(this.mYAxis.getZeroLineColor());
    this.mZeroLinePaint.setStrokeWidth(this.mYAxis.getZeroLineWidth());
    Path localPath = this.mDrawZeroLinePath;
    localPath.reset();
    localPath.moveTo(this.mViewPortHandler.contentLeft(), (float)localMPPointD.y);
    localPath.lineTo(this.mViewPortHandler.contentRight(), (float)localMPPointD.y);
    paramCanvas.drawPath(localPath, this.mZeroLinePaint);
    paramCanvas.restoreToCount(i);
  }
  
  public RectF getGridClippingRect()
  {
    this.mGridClippingRect.set(this.mViewPortHandler.getContentRect());
    this.mGridClippingRect.inset(0.0F, -this.mAxis.getGridLineWidth());
    return this.mGridClippingRect;
  }
  
  protected float[] getTransformedPositions()
  {
    if (this.mGetTransformedPositionsBuffer.length != this.mYAxis.mEntryCount * 2) {
      this.mGetTransformedPositionsBuffer = new float[this.mYAxis.mEntryCount * 2];
    }
    float[] arrayOfFloat = this.mGetTransformedPositionsBuffer;
    for (int i = 0; i < arrayOfFloat.length; i += 2) {
      arrayOfFloat[(i + 1)] = this.mYAxis.mEntries[(i / 2)];
    }
    this.mTrans.pointValuesToPixel(arrayOfFloat);
    return arrayOfFloat;
  }
  
  protected Path linePath(Path paramPath, int paramInt, float[] paramArrayOfFloat)
  {
    float f = this.mViewPortHandler.offsetLeft();
    paramInt++;
    paramPath.moveTo(f, paramArrayOfFloat[paramInt]);
    paramPath.lineTo(this.mViewPortHandler.contentRight(), paramArrayOfFloat[paramInt]);
    return paramPath;
  }
  
  public void renderAxisLabels(Canvas paramCanvas)
  {
    if ((this.mYAxis.isEnabled()) && (this.mYAxis.isDrawLabelsEnabled()))
    {
      float[] arrayOfFloat = getTransformedPositions();
      this.mAxisLabelPaint.setTypeface(this.mYAxis.getTypeface());
      this.mAxisLabelPaint.setTextSize(this.mYAxis.getTextSize());
      this.mAxisLabelPaint.setColor(this.mYAxis.getTextColor());
      float f1 = this.mYAxis.getXOffset();
      float f2 = Utils.calcTextHeight(this.mAxisLabelPaint, "A") / 2.5F;
      float f3 = this.mYAxis.getYOffset();
      YAxis.AxisDependency localAxisDependency = this.mYAxis.getAxisDependency();
      YAxis.YAxisLabelPosition localYAxisLabelPosition = this.mYAxis.getLabelPosition();
      if (localAxisDependency == YAxis.AxisDependency.LEFT)
      {
        if (localYAxisLabelPosition == YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        {
          this.mAxisLabelPaint.setTextAlign(Paint.Align.RIGHT);
          f1 = this.mViewPortHandler.offsetLeft() - f1;
        }
        else
        {
          this.mAxisLabelPaint.setTextAlign(Paint.Align.LEFT);
          f1 = this.mViewPortHandler.offsetLeft() + f1;
        }
      }
      else if (localYAxisLabelPosition == YAxis.YAxisLabelPosition.OUTSIDE_CHART)
      {
        this.mAxisLabelPaint.setTextAlign(Paint.Align.LEFT);
        f1 = this.mViewPortHandler.contentRight() + f1;
      }
      else
      {
        this.mAxisLabelPaint.setTextAlign(Paint.Align.RIGHT);
        f1 = this.mViewPortHandler.contentRight() - f1;
      }
      drawYLabels(paramCanvas, f1, arrayOfFloat, f2 + f3);
      return;
    }
  }
  
  public void renderAxisLine(Canvas paramCanvas)
  {
    if ((this.mYAxis.isEnabled()) && (this.mYAxis.isDrawAxisLineEnabled()))
    {
      this.mAxisLinePaint.setColor(this.mYAxis.getAxisLineColor());
      this.mAxisLinePaint.setStrokeWidth(this.mYAxis.getAxisLineWidth());
      if (this.mYAxis.getAxisDependency() == YAxis.AxisDependency.LEFT) {
        paramCanvas.drawLine(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop(), this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentBottom(), this.mAxisLinePaint);
      } else {
        paramCanvas.drawLine(this.mViewPortHandler.contentRight(), this.mViewPortHandler.contentTop(), this.mViewPortHandler.contentRight(), this.mViewPortHandler.contentBottom(), this.mAxisLinePaint);
      }
      return;
    }
  }
  
  public void renderGridLines(Canvas paramCanvas)
  {
    if (!this.mYAxis.isEnabled()) {
      return;
    }
    if (this.mYAxis.isDrawGridLinesEnabled())
    {
      int j = paramCanvas.save();
      paramCanvas.clipRect(getGridClippingRect());
      float[] arrayOfFloat = getTransformedPositions();
      this.mGridPaint.setColor(this.mYAxis.getGridColor());
      this.mGridPaint.setStrokeWidth(this.mYAxis.getGridLineWidth());
      this.mGridPaint.setPathEffect(this.mYAxis.getGridDashPathEffect());
      Path localPath = this.mRenderGridLinesPath;
      localPath.reset();
      for (int i = 0; i < arrayOfFloat.length; i += 2)
      {
        paramCanvas.drawPath(linePath(localPath, i, arrayOfFloat), this.mGridPaint);
        localPath.reset();
      }
      paramCanvas.restoreToCount(j);
    }
    if (this.mYAxis.isDrawZeroLineEnabled()) {
      drawZeroLine(paramCanvas);
    }
  }
  
  public void renderLimitLines(Canvas paramCanvas)
  {
    List localList = this.mYAxis.getLimitLines();
    if ((localList != null) && (localList.size() > 0))
    {
      float[] arrayOfFloat = this.mRenderLimitLinesBuffer;
      int i = 0;
      arrayOfFloat[0] = 0.0F;
      arrayOfFloat[1] = 0.0F;
      Path localPath = this.mRenderLimitLines;
      localPath.reset();
      while (i < localList.size())
      {
        Object localObject = (LimitLine)localList.get(i);
        if (((LimitLine)localObject).isEnabled())
        {
          int j = paramCanvas.save();
          this.mLimitLineClippingRect.set(this.mViewPortHandler.getContentRect());
          this.mLimitLineClippingRect.inset(0.0F, -((LimitLine)localObject).getLineWidth());
          paramCanvas.clipRect(this.mLimitLineClippingRect);
          this.mLimitLinePaint.setStyle(Paint.Style.STROKE);
          this.mLimitLinePaint.setColor(((LimitLine)localObject).getLineColor());
          this.mLimitLinePaint.setStrokeWidth(((LimitLine)localObject).getLineWidth());
          this.mLimitLinePaint.setPathEffect(((LimitLine)localObject).getDashPathEffect());
          arrayOfFloat[1] = ((LimitLine)localObject).getLimit();
          this.mTrans.pointValuesToPixel(arrayOfFloat);
          localPath.moveTo(this.mViewPortHandler.contentLeft(), arrayOfFloat[1]);
          localPath.lineTo(this.mViewPortHandler.contentRight(), arrayOfFloat[1]);
          paramCanvas.drawPath(localPath, this.mLimitLinePaint);
          localPath.reset();
          String str = ((LimitLine)localObject).getLabel();
          if ((str != null) && (!str.equals("")))
          {
            this.mLimitLinePaint.setStyle(((LimitLine)localObject).getTextStyle());
            this.mLimitLinePaint.setPathEffect(null);
            this.mLimitLinePaint.setColor(((LimitLine)localObject).getTextColor());
            this.mLimitLinePaint.setTypeface(((LimitLine)localObject).getTypeface());
            this.mLimitLinePaint.setStrokeWidth(0.5F);
            this.mLimitLinePaint.setTextSize(((LimitLine)localObject).getTextSize());
            float f2 = Utils.calcTextHeight(this.mLimitLinePaint, str);
            float f3 = Utils.convertDpToPixel(4.0F) + ((LimitLine)localObject).getXOffset();
            float f1 = ((LimitLine)localObject).getLineWidth() + f2 + ((LimitLine)localObject).getYOffset();
            localObject = ((LimitLine)localObject).getLabelPosition();
            if (localObject == LimitLine.LimitLabelPosition.RIGHT_TOP)
            {
              this.mLimitLinePaint.setTextAlign(Paint.Align.RIGHT);
              paramCanvas.drawText(str, this.mViewPortHandler.contentRight() - f3, arrayOfFloat[1] - f1 + f2, this.mLimitLinePaint);
            }
            else if (localObject == LimitLine.LimitLabelPosition.RIGHT_BOTTOM)
            {
              this.mLimitLinePaint.setTextAlign(Paint.Align.RIGHT);
              paramCanvas.drawText(str, this.mViewPortHandler.contentRight() - f3, arrayOfFloat[1] + f1, this.mLimitLinePaint);
            }
            else if (localObject == LimitLine.LimitLabelPosition.LEFT_TOP)
            {
              this.mLimitLinePaint.setTextAlign(Paint.Align.LEFT);
              paramCanvas.drawText(str, this.mViewPortHandler.contentLeft() + f3, arrayOfFloat[1] - f1 + f2, this.mLimitLinePaint);
            }
            else
            {
              this.mLimitLinePaint.setTextAlign(Paint.Align.LEFT);
              paramCanvas.drawText(str, this.mViewPortHandler.offsetLeft() + f3, arrayOfFloat[1] + f1, this.mLimitLinePaint);
            }
          }
          paramCanvas.restoreToCount(j);
        }
        i++;
      }
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/YAxisRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */