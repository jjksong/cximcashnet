package com.github.mikephil.charting.renderer;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.DataSet.Rounding;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.dataprovider.BarLineScatterCandleBubbleDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

public abstract class BarLineScatterCandleBubbleRenderer
  extends DataRenderer
{
  protected XBounds mXBounds = new XBounds();
  
  public BarLineScatterCandleBubbleRenderer(ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
  }
  
  protected boolean isInBoundsX(Entry paramEntry, IBarLineScatterCandleBubbleDataSet paramIBarLineScatterCandleBubbleDataSet)
  {
    if (paramEntry == null) {
      return false;
    }
    float f = paramIBarLineScatterCandleBubbleDataSet.getEntryIndex(paramEntry);
    return (paramEntry != null) && (f < paramIBarLineScatterCandleBubbleDataSet.getEntryCount() * this.mAnimator.getPhaseX());
  }
  
  protected boolean shouldDrawValues(IDataSet paramIDataSet)
  {
    boolean bool;
    if ((paramIDataSet.isVisible()) && ((paramIDataSet.isDrawValuesEnabled()) || (paramIDataSet.isDrawIconsEnabled()))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected class XBounds
  {
    public int max;
    public int min;
    public int range;
    
    protected XBounds() {}
    
    public void set(BarLineScatterCandleBubbleDataProvider paramBarLineScatterCandleBubbleDataProvider, IBarLineScatterCandleBubbleDataSet paramIBarLineScatterCandleBubbleDataSet)
    {
      float f3 = Math.max(0.0F, Math.min(1.0F, BarLineScatterCandleBubbleRenderer.this.mAnimator.getPhaseX()));
      float f2 = paramBarLineScatterCandleBubbleDataProvider.getLowestVisibleX();
      float f1 = paramBarLineScatterCandleBubbleDataProvider.getHighestVisibleX();
      Entry localEntry = paramIBarLineScatterCandleBubbleDataSet.getEntryForXValue(f2, NaN.0F, DataSet.Rounding.DOWN);
      paramBarLineScatterCandleBubbleDataProvider = paramIBarLineScatterCandleBubbleDataSet.getEntryForXValue(f1, NaN.0F, DataSet.Rounding.UP);
      int j = 0;
      int i;
      if (localEntry == null) {
        i = 0;
      } else {
        i = paramIBarLineScatterCandleBubbleDataSet.getEntryIndex(localEntry);
      }
      this.min = i;
      if (paramBarLineScatterCandleBubbleDataProvider == null) {
        i = j;
      } else {
        i = paramIBarLineScatterCandleBubbleDataSet.getEntryIndex(paramBarLineScatterCandleBubbleDataProvider);
      }
      this.max = i;
      this.range = ((int)((this.max - this.min) * f3));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/BarLineScatterCandleBubbleRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */