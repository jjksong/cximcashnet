package com.github.mikephil.charting.renderer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet.Mode;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class LineChartRenderer
  extends LineRadarRenderer
{
  protected Path cubicFillPath = new Path();
  protected Path cubicPath = new Path();
  protected Canvas mBitmapCanvas;
  protected Bitmap.Config mBitmapConfig = Bitmap.Config.ARGB_8888;
  protected LineDataProvider mChart;
  protected Paint mCirclePaintInner;
  private float[] mCirclesBuffer = new float[2];
  protected WeakReference<Bitmap> mDrawBitmap;
  protected Path mGenerateFilledPathBuffer = new Path();
  private HashMap<IDataSet, DataSetImageCache> mImageCaches = new HashMap();
  private float[] mLineBuffer = new float[4];
  
  public LineChartRenderer(LineDataProvider paramLineDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramLineDataProvider;
    this.mCirclePaintInner = new Paint(1);
    this.mCirclePaintInner.setStyle(Paint.Style.FILL);
    this.mCirclePaintInner.setColor(-1);
  }
  
  private void generateFilledPath(ILineDataSet paramILineDataSet, int paramInt1, int paramInt2, Path paramPath)
  {
    float f1 = paramILineDataSet.getFillFormatter().getFillLinePosition(paramILineDataSet, this.mChart);
    float f2 = this.mAnimator.getPhaseY();
    int i;
    if (paramILineDataSet.getMode() == LineDataSet.Mode.STEPPED) {
      i = 1;
    } else {
      i = 0;
    }
    paramPath.reset();
    Object localObject1 = paramILineDataSet.getEntryForIndex(paramInt1);
    paramPath.moveTo(((Entry)localObject1).getX(), f1);
    paramPath.lineTo(((Entry)localObject1).getX(), ((Entry)localObject1).getY() * f2);
    Object localObject2 = null;
    paramInt1++;
    while (paramInt1 <= paramInt2)
    {
      localObject2 = paramILineDataSet.getEntryForIndex(paramInt1);
      if (i != 0) {
        paramPath.lineTo(((Entry)localObject2).getX(), ((Entry)localObject1).getY() * f2);
      }
      paramPath.lineTo(((Entry)localObject2).getX(), ((Entry)localObject2).getY() * f2);
      paramInt1++;
      localObject1 = localObject2;
      localObject2 = localObject1;
    }
    if (localObject2 != null) {
      paramPath.lineTo(((Entry)localObject2).getX(), f1);
    }
    paramPath.close();
  }
  
  protected void drawCircles(Canvas paramCanvas)
  {
    this.mRenderPaint.setStyle(Paint.Style.FILL);
    float f1 = this.mAnimator.getPhaseY();
    Object localObject1 = this.mCirclesBuffer;
    localObject1[0] = 0.0F;
    localObject1[1] = 0.0F;
    List localList = this.mChart.getLineData().getDataSets();
    for (int i = 0; i < localList.size(); i++)
    {
      ILineDataSet localILineDataSet = (ILineDataSet)localList.get(i);
      if ((localILineDataSet.isVisible()) && (localILineDataSet.isDrawCirclesEnabled()) && (localILineDataSet.getEntryCount() != 0))
      {
        this.mCirclePaintInner.setColor(localILineDataSet.getCircleHoleColor());
        Transformer localTransformer = this.mChart.getTransformer(localILineDataSet.getAxisDependency());
        this.mXBounds.set(this.mChart, localILineDataSet);
        float f2 = localILineDataSet.getCircleRadius();
        float f3 = localILineDataSet.getCircleHoleRadius();
        boolean bool1;
        if ((localILineDataSet.isDrawCircleHoleEnabled()) && (f3 < f2) && (f3 > 0.0F)) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        boolean bool2;
        if ((bool1) && (localILineDataSet.getCircleHoleColor() == 1122867)) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (this.mImageCaches.containsKey(localILineDataSet))
        {
          localObject1 = (DataSetImageCache)this.mImageCaches.get(localILineDataSet);
        }
        else
        {
          localObject1 = new DataSetImageCache(null);
          this.mImageCaches.put(localILineDataSet, localObject1);
        }
        if (((DataSetImageCache)localObject1).init(localILineDataSet)) {
          ((DataSetImageCache)localObject1).fill(localILineDataSet, bool1, bool2);
        }
        int m = this.mXBounds.range;
        int k = this.mXBounds.min;
        for (int j = this.mXBounds.min; j <= m + k; j++)
        {
          Object localObject2 = localILineDataSet.getEntryForIndex(j);
          if (localObject2 == null) {
            break;
          }
          this.mCirclesBuffer[0] = ((Entry)localObject2).getX();
          this.mCirclesBuffer[1] = (((Entry)localObject2).getY() * f1);
          localTransformer.pointValuesToPixel(this.mCirclesBuffer);
          if (!this.mViewPortHandler.isInBoundsRight(this.mCirclesBuffer[0])) {
            break;
          }
          if ((this.mViewPortHandler.isInBoundsLeft(this.mCirclesBuffer[0])) && (this.mViewPortHandler.isInBoundsY(this.mCirclesBuffer[1])))
          {
            Bitmap localBitmap = ((DataSetImageCache)localObject1).getBitmap(j);
            if (localBitmap != null)
            {
              localObject2 = this.mCirclesBuffer;
              paramCanvas.drawBitmap(localBitmap, localObject2[0] - f2, localObject2[1] - f2, null);
            }
            else {}
          }
        }
      }
    }
  }
  
  protected void drawCubicBezier(ILineDataSet paramILineDataSet)
  {
    float f3 = this.mAnimator.getPhaseY();
    Transformer localTransformer = this.mChart.getTransformer(paramILineDataSet.getAxisDependency());
    this.mXBounds.set(this.mChart, paramILineDataSet);
    float f6 = paramILineDataSet.getCubicIntensity();
    this.cubicPath.reset();
    if (this.mXBounds.range >= 1)
    {
      int i = this.mXBounds.min + 1;
      int j = this.mXBounds.min;
      j = this.mXBounds.range;
      Object localObject3 = paramILineDataSet.getEntryForIndex(Math.max(i - 2, 0));
      Object localObject1 = paramILineDataSet.getEntryForIndex(Math.max(i - 1, 0));
      int k = -1;
      if (localObject1 == null) {
        return;
      }
      this.cubicPath.moveTo(((Entry)localObject1).getX(), ((Entry)localObject1).getY() * f3);
      i = this.mXBounds.min + 1;
      Object localObject2 = localObject1;
      while (i <= this.mXBounds.range + this.mXBounds.min)
      {
        if (k != i) {
          localObject2 = paramILineDataSet.getEntryForIndex(i);
        }
        j = i + 1;
        if (j < paramILineDataSet.getEntryCount()) {
          i = j;
        }
        Entry localEntry = paramILineDataSet.getEntryForIndex(i);
        float f9 = ((Entry)localObject2).getX();
        float f10 = ((Entry)localObject3).getX();
        float f1 = ((Entry)localObject2).getY();
        float f5 = ((Entry)localObject3).getY();
        float f2 = localEntry.getX();
        float f7 = ((Entry)localObject1).getX();
        float f8 = localEntry.getY();
        float f4 = ((Entry)localObject1).getY();
        this.cubicPath.cubicTo(((Entry)localObject1).getX() + (f9 - f10) * f6, (((Entry)localObject1).getY() + (f1 - f5) * f6) * f3, ((Entry)localObject2).getX() - (f2 - f7) * f6, (((Entry)localObject2).getY() - (f8 - f4) * f6) * f3, ((Entry)localObject2).getX(), ((Entry)localObject2).getY() * f3);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localEntry;
        k = i;
        i = j;
      }
    }
    if (paramILineDataSet.isDrawFilledEnabled())
    {
      this.cubicFillPath.reset();
      this.cubicFillPath.addPath(this.cubicPath);
      drawCubicFill(this.mBitmapCanvas, paramILineDataSet, this.cubicFillPath, localTransformer, this.mXBounds);
    }
    this.mRenderPaint.setColor(paramILineDataSet.getColor());
    this.mRenderPaint.setStyle(Paint.Style.STROKE);
    localTransformer.pathValueToPixel(this.cubicPath);
    this.mBitmapCanvas.drawPath(this.cubicPath, this.mRenderPaint);
    this.mRenderPaint.setPathEffect(null);
  }
  
  protected void drawCubicFill(Canvas paramCanvas, ILineDataSet paramILineDataSet, Path paramPath, Transformer paramTransformer, BarLineScatterCandleBubbleRenderer.XBounds paramXBounds)
  {
    float f = paramILineDataSet.getFillFormatter().getFillLinePosition(paramILineDataSet, this.mChart);
    paramPath.lineTo(paramILineDataSet.getEntryForIndex(paramXBounds.min + paramXBounds.range).getX(), f);
    paramPath.lineTo(paramILineDataSet.getEntryForIndex(paramXBounds.min).getX(), f);
    paramPath.close();
    paramTransformer.pathValueToPixel(paramPath);
    paramTransformer = paramILineDataSet.getFillDrawable();
    if (paramTransformer != null) {
      drawFilledPath(paramCanvas, paramPath, paramTransformer);
    } else {
      drawFilledPath(paramCanvas, paramPath, paramILineDataSet.getFillColor(), paramILineDataSet.getFillAlpha());
    }
  }
  
  public void drawData(Canvas paramCanvas)
  {
    int j = (int)this.mViewPortHandler.getChartWidth();
    int i = (int)this.mViewPortHandler.getChartHeight();
    Object localObject1 = this.mDrawBitmap;
    if (localObject1 == null) {
      localObject1 = null;
    } else {
      localObject1 = (Bitmap)((WeakReference)localObject1).get();
    }
    Object localObject2;
    if ((localObject1 != null) && (((Bitmap)localObject1).getWidth() == j))
    {
      localObject2 = localObject1;
      if (((Bitmap)localObject1).getHeight() == i) {}
    }
    else
    {
      if ((j <= 0) || (i <= 0)) {
        return;
      }
      localObject2 = Bitmap.createBitmap(j, i, this.mBitmapConfig);
      this.mDrawBitmap = new WeakReference(localObject2);
      this.mBitmapCanvas = new Canvas((Bitmap)localObject2);
    }
    ((Bitmap)localObject2).eraseColor(0);
    Iterator localIterator = this.mChart.getLineData().getDataSets().iterator();
    while (localIterator.hasNext())
    {
      localObject1 = (ILineDataSet)localIterator.next();
      if (((ILineDataSet)localObject1).isVisible()) {
        drawDataSet(paramCanvas, (ILineDataSet)localObject1);
      }
    }
    paramCanvas.drawBitmap((Bitmap)localObject2, 0.0F, 0.0F, this.mRenderPaint);
    return;
  }
  
  protected void drawDataSet(Canvas paramCanvas, ILineDataSet paramILineDataSet)
  {
    if (paramILineDataSet.getEntryCount() < 1) {
      return;
    }
    this.mRenderPaint.setStrokeWidth(paramILineDataSet.getLineWidth());
    this.mRenderPaint.setPathEffect(paramILineDataSet.getDashPathEffect());
    switch (paramILineDataSet.getMode())
    {
    default: 
      drawLinear(paramCanvas, paramILineDataSet);
      break;
    case ???: 
      drawHorizontalBezier(paramILineDataSet);
      break;
    case ???: 
      drawCubicBezier(paramILineDataSet);
    }
    this.mRenderPaint.setPathEffect(null);
  }
  
  public void drawExtras(Canvas paramCanvas)
  {
    drawCircles(paramCanvas);
  }
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    LineData localLineData = this.mChart.getLineData();
    int j = paramArrayOfHighlight.length;
    for (int i = 0; i < j; i++)
    {
      Highlight localHighlight = paramArrayOfHighlight[i];
      ILineDataSet localILineDataSet = (ILineDataSet)localLineData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localILineDataSet != null) && (localILineDataSet.isHighlightEnabled()))
      {
        Object localObject = localILineDataSet.getEntryForXValue(localHighlight.getX(), localHighlight.getY());
        if (isInBoundsX((Entry)localObject, localILineDataSet))
        {
          localObject = this.mChart.getTransformer(localILineDataSet.getAxisDependency()).getPixelForValues(((Entry)localObject).getX(), ((Entry)localObject).getY() * this.mAnimator.getPhaseY());
          localHighlight.setDraw((float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y);
          drawHighlightLines(paramCanvas, (float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y, localILineDataSet);
        }
      }
    }
  }
  
  protected void drawHorizontalBezier(ILineDataSet paramILineDataSet)
  {
    float f2 = this.mAnimator.getPhaseY();
    Transformer localTransformer = this.mChart.getTransformer(paramILineDataSet.getAxisDependency());
    this.mXBounds.set(this.mChart, paramILineDataSet);
    this.cubicPath.reset();
    if (this.mXBounds.range >= 1)
    {
      Object localObject = paramILineDataSet.getEntryForIndex(this.mXBounds.min);
      this.cubicPath.moveTo(((Entry)localObject).getX(), ((Entry)localObject).getY() * f2);
      int i = this.mXBounds.min + 1;
      while (i <= this.mXBounds.range + this.mXBounds.min)
      {
        Entry localEntry = paramILineDataSet.getEntryForIndex(i);
        float f1 = ((Entry)localObject).getX() + (localEntry.getX() - ((Entry)localObject).getX()) / 2.0F;
        this.cubicPath.cubicTo(f1, ((Entry)localObject).getY() * f2, f1, localEntry.getY() * f2, localEntry.getX(), localEntry.getY() * f2);
        i++;
        localObject = localEntry;
      }
    }
    if (paramILineDataSet.isDrawFilledEnabled())
    {
      this.cubicFillPath.reset();
      this.cubicFillPath.addPath(this.cubicPath);
      drawCubicFill(this.mBitmapCanvas, paramILineDataSet, this.cubicFillPath, localTransformer, this.mXBounds);
    }
    this.mRenderPaint.setColor(paramILineDataSet.getColor());
    this.mRenderPaint.setStyle(Paint.Style.STROKE);
    localTransformer.pathValueToPixel(this.cubicPath);
    this.mBitmapCanvas.drawPath(this.cubicPath, this.mRenderPaint);
    this.mRenderPaint.setPathEffect(null);
  }
  
  protected void drawLinear(Canvas paramCanvas, ILineDataSet paramILineDataSet)
  {
    int j = paramILineDataSet.getEntryCount();
    boolean bool = paramILineDataSet.isDrawSteppedEnabled();
    int i;
    if (bool) {
      i = 4;
    } else {
      i = 2;
    }
    Transformer localTransformer = this.mChart.getTransformer(paramILineDataSet.getAxisDependency());
    float f = this.mAnimator.getPhaseY();
    this.mRenderPaint.setStyle(Paint.Style.STROKE);
    Canvas localCanvas;
    if (paramILineDataSet.isDashedLineEnabled()) {
      localCanvas = this.mBitmapCanvas;
    } else {
      localCanvas = paramCanvas;
    }
    this.mXBounds.set(this.mChart, paramILineDataSet);
    if ((paramILineDataSet.isDrawFilledEnabled()) && (j > 0)) {
      drawLinearFill(paramCanvas, paramILineDataSet, localTransformer, this.mXBounds);
    }
    Object localObject;
    if (paramILineDataSet.getColors().size() > 1)
    {
      k = this.mLineBuffer.length;
      j = i * 2;
      if (k <= j) {
        this.mLineBuffer = new float[i * 4];
      }
      for (i = this.mXBounds.min; i <= this.mXBounds.range + this.mXBounds.min; i++)
      {
        paramCanvas = paramILineDataSet.getEntryForIndex(i);
        if (paramCanvas != null)
        {
          this.mLineBuffer[0] = paramCanvas.getX();
          this.mLineBuffer[1] = (paramCanvas.getY() * f);
          if (i < this.mXBounds.max)
          {
            localObject = paramILineDataSet.getEntryForIndex(i + 1);
            if (localObject == null) {
              break;
            }
            if (bool)
            {
              this.mLineBuffer[2] = ((Entry)localObject).getX();
              paramCanvas = this.mLineBuffer;
              paramCanvas[3] = paramCanvas[1];
              paramCanvas[4] = paramCanvas[2];
              paramCanvas[5] = paramCanvas[3];
              paramCanvas[6] = ((Entry)localObject).getX();
              this.mLineBuffer[7] = (((Entry)localObject).getY() * f);
            }
            else
            {
              this.mLineBuffer[2] = ((Entry)localObject).getX();
              this.mLineBuffer[3] = (((Entry)localObject).getY() * f);
            }
          }
          else
          {
            paramCanvas = this.mLineBuffer;
            paramCanvas[2] = paramCanvas[0];
            paramCanvas[3] = paramCanvas[1];
          }
          localTransformer.pointValuesToPixel(this.mLineBuffer);
          if (!this.mViewPortHandler.isInBoundsRight(this.mLineBuffer[0])) {
            break;
          }
          if ((this.mViewPortHandler.isInBoundsLeft(this.mLineBuffer[2])) && ((this.mViewPortHandler.isInBoundsTop(this.mLineBuffer[1])) || (this.mViewPortHandler.isInBoundsBottom(this.mLineBuffer[3]))))
          {
            this.mRenderPaint.setColor(paramILineDataSet.getColor(i));
            localCanvas.drawLines(this.mLineBuffer, 0, j, this.mRenderPaint);
          }
        }
      }
    }
    int k = this.mLineBuffer.length;
    j *= i;
    if (k < Math.max(j, i) * 2) {
      this.mLineBuffer = new float[Math.max(j, i) * 4];
    }
    if (paramILineDataSet.getEntryForIndex(this.mXBounds.min) != null)
    {
      j = this.mXBounds.min;
      int m;
      for (k = 0; j <= this.mXBounds.range + this.mXBounds.min; k = m)
      {
        if (j == 0) {
          m = 0;
        } else {
          m = j - 1;
        }
        localObject = paramILineDataSet.getEntryForIndex(m);
        paramCanvas = paramILineDataSet.getEntryForIndex(j);
        m = k;
        if (localObject != null) {
          if (paramCanvas == null)
          {
            m = k;
          }
          else
          {
            float[] arrayOfFloat = this.mLineBuffer;
            int n = k + 1;
            arrayOfFloat[k] = ((Entry)localObject).getX();
            arrayOfFloat = this.mLineBuffer;
            m = n + 1;
            arrayOfFloat[n] = (((Entry)localObject).getY() * f);
            k = m;
            if (bool)
            {
              arrayOfFloat = this.mLineBuffer;
              k = m + 1;
              arrayOfFloat[m] = paramCanvas.getX();
              arrayOfFloat = this.mLineBuffer;
              n = k + 1;
              arrayOfFloat[k] = (((Entry)localObject).getY() * f);
              arrayOfFloat = this.mLineBuffer;
              m = n + 1;
              arrayOfFloat[n] = paramCanvas.getX();
              arrayOfFloat = this.mLineBuffer;
              k = m + 1;
              arrayOfFloat[m] = (((Entry)localObject).getY() * f);
            }
            localObject = this.mLineBuffer;
            m = k + 1;
            localObject[k] = paramCanvas.getX();
            this.mLineBuffer[m] = (paramCanvas.getY() * f);
            m++;
          }
        }
        j++;
      }
      if (k > 0)
      {
        localTransformer.pointValuesToPixel(this.mLineBuffer);
        i = Math.max((this.mXBounds.range + 1) * i, i);
        this.mRenderPaint.setColor(paramILineDataSet.getColor());
        localCanvas.drawLines(this.mLineBuffer, 0, i * 2, this.mRenderPaint);
      }
    }
    this.mRenderPaint.setPathEffect(null);
  }
  
  protected void drawLinearFill(Canvas paramCanvas, ILineDataSet paramILineDataSet, Transformer paramTransformer, BarLineScatterCandleBubbleRenderer.XBounds paramXBounds)
  {
    Path localPath = this.mGenerateFilledPathBuffer;
    int n = paramXBounds.min;
    int k = paramXBounds.range + paramXBounds.min;
    int i = 0;
    int i1;
    int j;
    do
    {
      i1 = i * 128 + n;
      int m = i1 + 128;
      j = m;
      if (m > k) {
        j = k;
      }
      if (i1 <= j)
      {
        generateFilledPath(paramILineDataSet, i1, j, localPath);
        paramTransformer.pathValueToPixel(localPath);
        paramXBounds = paramILineDataSet.getFillDrawable();
        if (paramXBounds != null) {
          drawFilledPath(paramCanvas, localPath, paramXBounds);
        } else {
          drawFilledPath(paramCanvas, localPath, paramILineDataSet.getFillColor(), paramILineDataSet.getFillAlpha());
        }
      }
      i++;
    } while (i1 <= j);
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    if (isDrawingValuesAllowed(this.mChart))
    {
      List localList = this.mChart.getLineData().getDataSets();
      for (int i = 0; i < localList.size(); i++)
      {
        ILineDataSet localILineDataSet = (ILineDataSet)localList.get(i);
        if ((shouldDrawValues(localILineDataSet)) && (localILineDataSet.getEntryCount() >= 1))
        {
          applyValueTextStyle(localILineDataSet);
          Object localObject1 = this.mChart.getTransformer(localILineDataSet.getAxisDependency());
          int j = (int)(localILineDataSet.getCircleRadius() * 1.75F);
          if (!localILineDataSet.isDrawCirclesEnabled()) {
            j /= 2;
          }
          this.mXBounds.set(this.mChart, localILineDataSet);
          float[] arrayOfFloat = ((Transformer)localObject1).generateTransformedValuesLine(localILineDataSet, this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);
          localObject1 = MPPointF.getInstance(localILineDataSet.getIconsOffset());
          ((MPPointF)localObject1).x = Utils.convertDpToPixel(((MPPointF)localObject1).x);
          ((MPPointF)localObject1).y = Utils.convertDpToPixel(((MPPointF)localObject1).y);
          for (int k = 0; k < arrayOfFloat.length; k += 2)
          {
            float f2 = arrayOfFloat[k];
            float f1 = arrayOfFloat[(k + 1)];
            if (!this.mViewPortHandler.isInBoundsRight(f2)) {
              break;
            }
            if ((this.mViewPortHandler.isInBoundsLeft(f2)) && (this.mViewPortHandler.isInBoundsY(f1)))
            {
              int m = k / 2;
              Object localObject3 = localILineDataSet.getEntryForIndex(this.mXBounds.min + m);
              if (localILineDataSet.isDrawValuesEnabled()) {
                drawValue(paramCanvas, localILineDataSet.getValueFormatter(), ((Entry)localObject3).getY(), (Entry)localObject3, i, f2, f1 - j, localILineDataSet.getValueTextColor(m));
              }
              Object localObject2 = localObject1;
              if ((((Entry)localObject3).getIcon() != null) && (localILineDataSet.isDrawIconsEnabled()))
              {
                localObject3 = ((Entry)localObject3).getIcon();
                Utils.drawImage(paramCanvas, (Drawable)localObject3, (int)(f2 + ((MPPointF)localObject2).x), (int)(f1 + ((MPPointF)localObject2).y), ((Drawable)localObject3).getIntrinsicWidth(), ((Drawable)localObject3).getIntrinsicHeight());
              }
            }
          }
          MPPointF.recycleInstance((MPPointF)localObject1);
        }
      }
    }
  }
  
  public Bitmap.Config getBitmapConfig()
  {
    return this.mBitmapConfig;
  }
  
  public void initBuffers() {}
  
  public void releaseBitmap()
  {
    Object localObject = this.mBitmapCanvas;
    if (localObject != null)
    {
      ((Canvas)localObject).setBitmap(null);
      this.mBitmapCanvas = null;
    }
    localObject = this.mDrawBitmap;
    if (localObject != null)
    {
      localObject = (Bitmap)((WeakReference)localObject).get();
      if (localObject != null) {
        ((Bitmap)localObject).recycle();
      }
      this.mDrawBitmap.clear();
      this.mDrawBitmap = null;
    }
  }
  
  public void setBitmapConfig(Bitmap.Config paramConfig)
  {
    this.mBitmapConfig = paramConfig;
    releaseBitmap();
  }
  
  private class DataSetImageCache
  {
    private Bitmap[] circleBitmaps;
    private Path mCirclePathBuffer = new Path();
    
    private DataSetImageCache() {}
    
    protected void fill(ILineDataSet paramILineDataSet, boolean paramBoolean1, boolean paramBoolean2)
    {
      int j = paramILineDataSet.getCircleColorCount();
      float f1 = paramILineDataSet.getCircleRadius();
      float f2 = paramILineDataSet.getCircleHoleRadius();
      for (int i = 0; i < j; i++)
      {
        Object localObject = Bitmap.Config.ARGB_4444;
        double d = f1;
        Double.isNaN(d);
        int k = (int)(d * 2.1D);
        localObject = Bitmap.createBitmap(k, k, (Bitmap.Config)localObject);
        Canvas localCanvas = new Canvas((Bitmap)localObject);
        this.circleBitmaps[i] = localObject;
        LineChartRenderer.this.mRenderPaint.setColor(paramILineDataSet.getCircleColor(i));
        if (paramBoolean2)
        {
          this.mCirclePathBuffer.reset();
          this.mCirclePathBuffer.addCircle(f1, f1, f1, Path.Direction.CW);
          this.mCirclePathBuffer.addCircle(f1, f1, f2, Path.Direction.CCW);
          localCanvas.drawPath(this.mCirclePathBuffer, LineChartRenderer.this.mRenderPaint);
        }
        else
        {
          localCanvas.drawCircle(f1, f1, f1, LineChartRenderer.this.mRenderPaint);
          if (paramBoolean1) {
            localCanvas.drawCircle(f1, f1, f2, LineChartRenderer.this.mCirclePaintInner);
          }
        }
      }
    }
    
    protected Bitmap getBitmap(int paramInt)
    {
      Bitmap[] arrayOfBitmap = this.circleBitmaps;
      return arrayOfBitmap[(paramInt % arrayOfBitmap.length)];
    }
    
    protected boolean init(ILineDataSet paramILineDataSet)
    {
      int i = paramILineDataSet.getCircleColorCount();
      paramILineDataSet = this.circleBitmaps;
      boolean bool = true;
      if (paramILineDataSet == null) {
        this.circleBitmaps = new Bitmap[i];
      } else if (paramILineDataSet.length != i) {
        this.circleBitmaps = new Bitmap[i];
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/LineChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */