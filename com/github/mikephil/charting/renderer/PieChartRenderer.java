package com.github.mikephil.charting.renderer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet.ValuePosition;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

public class PieChartRenderer
  extends DataRenderer
{
  protected Canvas mBitmapCanvas;
  private RectF mCenterTextLastBounds = new RectF();
  private CharSequence mCenterTextLastValue;
  private StaticLayout mCenterTextLayout;
  private TextPaint mCenterTextPaint;
  protected PieChart mChart;
  protected WeakReference<Bitmap> mDrawBitmap;
  protected Path mDrawCenterTextPathBuffer = new Path();
  protected RectF mDrawHighlightedRectF = new RectF();
  private Paint mEntryLabelsPaint;
  private Path mHoleCirclePath = new Path();
  protected Paint mHolePaint;
  private RectF mInnerRectBuffer = new RectF();
  private Path mPathBuffer = new Path();
  private RectF[] mRectBuffer = { new RectF(), new RectF(), new RectF() };
  protected Paint mTransparentCirclePaint;
  protected Paint mValueLinePaint;
  
  public PieChartRenderer(PieChart paramPieChart, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramPieChart;
    this.mHolePaint = new Paint(1);
    this.mHolePaint.setColor(-1);
    this.mHolePaint.setStyle(Paint.Style.FILL);
    this.mTransparentCirclePaint = new Paint(1);
    this.mTransparentCirclePaint.setColor(-1);
    this.mTransparentCirclePaint.setStyle(Paint.Style.FILL);
    this.mTransparentCirclePaint.setAlpha(105);
    this.mCenterTextPaint = new TextPaint(1);
    this.mCenterTextPaint.setColor(-16777216);
    this.mCenterTextPaint.setTextSize(Utils.convertDpToPixel(12.0F));
    this.mValuePaint.setTextSize(Utils.convertDpToPixel(13.0F));
    this.mValuePaint.setColor(-1);
    this.mValuePaint.setTextAlign(Paint.Align.CENTER);
    this.mEntryLabelsPaint = new Paint(1);
    this.mEntryLabelsPaint.setColor(-1);
    this.mEntryLabelsPaint.setTextAlign(Paint.Align.CENTER);
    this.mEntryLabelsPaint.setTextSize(Utils.convertDpToPixel(13.0F));
    this.mValueLinePaint = new Paint(1);
    this.mValueLinePaint.setStyle(Paint.Style.STROKE);
  }
  
  protected float calculateMinimumRadiusForSpacedSlice(MPPointF paramMPPointF, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    float f3 = paramFloat6 / 2.0F;
    float f1 = paramMPPointF.x;
    double d1 = (paramFloat5 + paramFloat6) * 0.017453292F;
    f1 += (float)Math.cos(d1) * paramFloat1;
    float f2 = paramMPPointF.y + (float)Math.sin(d1) * paramFloat1;
    paramFloat6 = paramMPPointF.x;
    d1 = (paramFloat5 + f3) * 0.017453292F;
    f3 = (float)Math.cos(d1);
    float f4 = paramMPPointF.y;
    paramFloat5 = (float)Math.sin(d1);
    d1 = Math.sqrt(Math.pow(f1 - paramFloat3, 2.0D) + Math.pow(f2 - paramFloat4, 2.0D)) / 2.0D;
    double d2 = paramFloat2;
    Double.isNaN(d2);
    d2 = paramFloat1 - (float)(d1 * Math.tan((180.0D - d2) / 2.0D * 0.017453292519943295D));
    d1 = Math.sqrt(Math.pow(paramFloat6 + f3 * paramFloat1 - (f1 + paramFloat3) / 2.0F, 2.0D) + Math.pow(f4 + paramFloat5 * paramFloat1 - (f2 + paramFloat4) / 2.0F, 2.0D));
    Double.isNaN(d2);
    return (float)(d2 - d1);
  }
  
  protected void drawCenterText(Canvas paramCanvas)
  {
    Object localObject1 = this.mChart.getCenterText();
    if ((this.mChart.isDrawCenterTextEnabled()) && (localObject1 != null))
    {
      MPPointF localMPPointF2 = this.mChart.getCenterCircleBox();
      MPPointF localMPPointF1 = this.mChart.getCenterTextOffset();
      float f3 = localMPPointF2.x + localMPPointF1.x;
      float f2 = localMPPointF2.y + localMPPointF1.y;
      if ((this.mChart.isDrawHoleEnabled()) && (!this.mChart.isDrawSlicesUnderHoleEnabled())) {
        f1 = this.mChart.getRadius() * (this.mChart.getHoleRadius() / 100.0F);
      } else {
        f1 = this.mChart.getRadius();
      }
      Object localObject2 = this.mRectBuffer;
      RectF localRectF = localObject2[0];
      localRectF.left = (f3 - f1);
      localRectF.top = (f2 - f1);
      localRectF.right = (f3 + f1);
      localRectF.bottom = (f2 + f1);
      localObject2 = localObject2[1];
      ((RectF)localObject2).set(localRectF);
      float f1 = this.mChart.getCenterTextRadiusPercent() / 100.0F;
      if (f1 > 0.0D) {
        ((RectF)localObject2).inset((((RectF)localObject2).width() - ((RectF)localObject2).width() * f1) / 2.0F, (((RectF)localObject2).height() - ((RectF)localObject2).height() * f1) / 2.0F);
      }
      if ((localObject1.equals(this.mCenterTextLastValue)) && (((RectF)localObject2).equals(this.mCenterTextLastBounds))) {
        break label323;
      }
      this.mCenterTextLastBounds.set((RectF)localObject2);
      this.mCenterTextLastValue = ((CharSequence)localObject1);
      f1 = this.mCenterTextLastBounds.width();
      this.mCenterTextLayout = new StaticLayout((CharSequence)localObject1, 0, ((CharSequence)localObject1).length(), this.mCenterTextPaint, (int)Math.max(Math.ceil(f1), 1.0D), Layout.Alignment.ALIGN_CENTER, 1.0F, 0.0F, false);
      label323:
      f1 = this.mCenterTextLayout.getHeight();
      paramCanvas.save();
      if (Build.VERSION.SDK_INT >= 18)
      {
        localObject1 = this.mDrawCenterTextPathBuffer;
        ((Path)localObject1).reset();
        ((Path)localObject1).addOval(localRectF, Path.Direction.CW);
        paramCanvas.clipPath((Path)localObject1);
      }
      paramCanvas.translate(((RectF)localObject2).left, ((RectF)localObject2).top + (((RectF)localObject2).height() - f1) / 2.0F);
      this.mCenterTextLayout.draw(paramCanvas);
      paramCanvas.restore();
      MPPointF.recycleInstance(localMPPointF2);
      MPPointF.recycleInstance(localMPPointF1);
    }
  }
  
  public void drawData(Canvas paramCanvas)
  {
    int j = (int)this.mViewPortHandler.getChartWidth();
    int i = (int)this.mViewPortHandler.getChartHeight();
    Object localObject1 = this.mDrawBitmap;
    if (localObject1 == null) {
      localObject1 = null;
    } else {
      localObject1 = (Bitmap)((WeakReference)localObject1).get();
    }
    Object localObject2;
    if ((localObject1 != null) && (((Bitmap)localObject1).getWidth() == j))
    {
      localObject2 = localObject1;
      if (((Bitmap)localObject1).getHeight() == i) {}
    }
    else
    {
      if ((j <= 0) || (i <= 0)) {
        return;
      }
      localObject2 = Bitmap.createBitmap(j, i, Bitmap.Config.ARGB_4444);
      this.mDrawBitmap = new WeakReference(localObject2);
      this.mBitmapCanvas = new Canvas((Bitmap)localObject2);
    }
    ((Bitmap)localObject2).eraseColor(0);
    localObject1 = ((PieData)this.mChart.getData()).getDataSets().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (IPieDataSet)((Iterator)localObject1).next();
      if ((((IPieDataSet)localObject2).isVisible()) && (((IPieDataSet)localObject2).getEntryCount() > 0)) {
        drawDataSet(paramCanvas, (IPieDataSet)localObject2);
      }
    }
    return;
  }
  
  protected void drawDataSet(Canvas paramCanvas, IPieDataSet paramIPieDataSet)
  {
    Object localObject1 = this;
    Object localObject2 = paramIPieDataSet;
    float f6 = ((PieChartRenderer)localObject1).mChart.getRotationAngle();
    float f3 = ((PieChartRenderer)localObject1).mAnimator.getPhaseX();
    float f11 = ((PieChartRenderer)localObject1).mAnimator.getPhaseY();
    RectF localRectF = ((PieChartRenderer)localObject1).mChart.getCircleBox();
    int n = paramIPieDataSet.getEntryCount();
    float[] arrayOfFloat = ((PieChartRenderer)localObject1).mChart.getDrawAngles();
    paramCanvas = ((PieChartRenderer)localObject1).mChart.getCenterCircleBox();
    float f7 = ((PieChartRenderer)localObject1).mChart.getRadius();
    int j;
    if ((((PieChartRenderer)localObject1).mChart.isDrawHoleEnabled()) && (!((PieChartRenderer)localObject1).mChart.isDrawSlicesUnderHoleEnabled())) {
      j = 1;
    } else {
      j = 0;
    }
    float f2;
    if (j != 0) {
      f2 = ((PieChartRenderer)localObject1).mChart.getHoleRadius() / 100.0F * f7;
    } else {
      f2 = 0.0F;
    }
    int m = 0;
    for (int i = 0; m < n; i = k)
    {
      k = i;
      if (Math.abs(((PieEntry)((IPieDataSet)localObject2).getEntryForIndex(m)).getY()) > Utils.FLOAT_EPSILON) {
        k = i + 1;
      }
      m++;
    }
    float f5;
    if (i <= 1) {
      f5 = 0.0F;
    } else {
      f5 = ((PieChartRenderer)localObject1).getSliceSpace((IPieDataSet)localObject2);
    }
    m = 0;
    float f8 = 0.0F;
    int k = n;
    for (;;)
    {
      localObject2 = paramIPieDataSet;
      if (m >= k) {
        break;
      }
      float f12 = arrayOfFloat[m];
      if (Math.abs(((IPieDataSet)localObject2).getEntryForIndex(m).getY()) > Utils.FLOAT_EPSILON) {
        if (!((PieChartRenderer)localObject1).mChart.needsHighlight(m))
        {
          if ((f5 > 0.0F) && (f12 <= 180.0F)) {
            n = 1;
          } else {
            n = 0;
          }
          ((PieChartRenderer)localObject1).mRenderPaint.setColor(((IPieDataSet)localObject2).getColor(m));
          if (i == 1) {
            f1 = 0.0F;
          } else {
            f1 = f5 / (f7 * 0.017453292F);
          }
          float f9 = f6 + (f8 + f1 / 2.0F) * f11;
          float f4 = (f12 - f1) * f11;
          float f1 = f4;
          if (f4 < 0.0F) {
            f1 = 0.0F;
          }
          ((PieChartRenderer)localObject1).mPathBuffer.reset();
          f4 = paramCanvas.x;
          double d = f9 * 0.017453292F;
          float f10 = f4 + (float)Math.cos(d) * f7;
          float f13 = paramCanvas.y + (float)Math.sin(d) * f7;
          if ((f1 >= 360.0F) && (f1 % 360.0F <= Utils.FLOAT_EPSILON))
          {
            ((PieChartRenderer)localObject1).mPathBuffer.addCircle(paramCanvas.x, paramCanvas.y, f7, Path.Direction.CW);
          }
          else
          {
            ((PieChartRenderer)localObject1).mPathBuffer.moveTo(f10, f13);
            ((PieChartRenderer)localObject1).mPathBuffer.arcTo(localRectF, f9, f1);
          }
          ((PieChartRenderer)localObject1).mInnerRectBuffer.set(paramCanvas.x - f2, paramCanvas.y - f2, paramCanvas.x + f2, paramCanvas.y + f2);
          if (j != 0)
          {
            if ((f2 <= 0.0F) && (n == 0)) {
              break label827;
            }
            if (n != 0)
            {
              f9 = calculateMinimumRadiusForSpacedSlice(paramCanvas, f7, f12 * f11, f10, f13, f9, f1);
              f4 = f9;
              if (f9 < 0.0F) {
                f4 = -f9;
              }
              f4 = Math.max(f2, f4);
            }
            else
            {
              f4 = f2;
            }
            f9 = f1;
            if ((i != 1) && (f4 != 0.0F)) {
              f1 = f5 / (f4 * 0.017453292F);
            } else {
              f1 = 0.0F;
            }
            f13 = f1 / 2.0F;
            f10 = (f12 - f1) * f11;
            f1 = f10;
            if (f10 < 0.0F) {
              f1 = 0.0F;
            }
            f10 = (f8 + f13) * f11 + f6 + f1;
            if ((f9 >= 360.0F) && (f9 % 360.0F <= Utils.FLOAT_EPSILON))
            {
              this.mPathBuffer.addCircle(paramCanvas.x, paramCanvas.y, f4, Path.Direction.CCW);
            }
            else
            {
              localObject1 = this;
              localObject2 = ((PieChartRenderer)localObject1).mPathBuffer;
              f9 = paramCanvas.x;
              d = f10 * 0.017453292F;
              ((Path)localObject2).lineTo(f9 + (float)Math.cos(d) * f4, paramCanvas.y + f4 * (float)Math.sin(d));
              ((PieChartRenderer)localObject1).mPathBuffer.arcTo(((PieChartRenderer)localObject1).mInnerRectBuffer, f10, -f1);
            }
            localObject2 = this;
            break label981;
          }
          label827:
          Canvas localCanvas = paramCanvas;
          localObject2 = localObject1;
          paramCanvas = localCanvas;
          if (f1 % 360.0F > Utils.FLOAT_EPSILON) {
            if (n != 0)
            {
              f4 = f1 / 2.0F;
              f10 = calculateMinimumRadiusForSpacedSlice(localCanvas, f7, f12 * f11, f10, f13, f9, f1);
              f1 = localCanvas.x;
              d = (f9 + f4) * 0.017453292F;
              f13 = (float)Math.cos(d);
              f9 = localCanvas.y;
              f4 = (float)Math.sin(d);
              ((PieChartRenderer)localObject1).mPathBuffer.lineTo(f1 + f13 * f10, f9 + f10 * f4);
              localObject2 = localObject1;
              paramCanvas = localCanvas;
            }
            else
            {
              ((PieChartRenderer)localObject1).mPathBuffer.lineTo(localCanvas.x, localCanvas.y);
              paramCanvas = localCanvas;
              localObject2 = localObject1;
            }
          }
          label981:
          ((PieChartRenderer)localObject2).mPathBuffer.close();
          ((PieChartRenderer)localObject2).mBitmapCanvas.drawPath(((PieChartRenderer)localObject2).mPathBuffer, ((PieChartRenderer)localObject2).mRenderPaint);
          localObject1 = localObject2;
        }
        else {}
      }
      f8 += f12 * f3;
      m++;
    }
    MPPointF.recycleInstance(paramCanvas);
  }
  
  protected void drawEntryLabel(Canvas paramCanvas, String paramString, float paramFloat1, float paramFloat2)
  {
    paramCanvas.drawText(paramString, paramFloat1, paramFloat2, this.mEntryLabelsPaint);
  }
  
  public void drawExtras(Canvas paramCanvas)
  {
    drawHole(paramCanvas);
    paramCanvas.drawBitmap((Bitmap)this.mDrawBitmap.get(), 0.0F, 0.0F, null);
    drawCenterText(paramCanvas);
  }
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    float f3 = this.mAnimator.getPhaseX();
    float f10 = this.mAnimator.getPhaseY();
    float f9 = this.mChart.getRotationAngle();
    float[] arrayOfFloat = this.mChart.getDrawAngles();
    paramCanvas = this.mChart.getAbsoluteAngles();
    MPPointF localMPPointF = this.mChart.getCenterCircleBox();
    float f8 = this.mChart.getRadius();
    int i;
    if ((this.mChart.isDrawHoleEnabled()) && (!this.mChart.isDrawSlicesUnderHoleEnabled())) {
      i = 1;
    } else {
      i = 0;
    }
    float f1;
    if (i != 0) {
      f1 = this.mChart.getHoleRadius() / 100.0F * f8;
    } else {
      f1 = 0.0F;
    }
    RectF localRectF = this.mDrawHighlightedRectF;
    localRectF.set(0.0F, 0.0F, 0.0F, 0.0F);
    for (int j = 0;; j++)
    {
      Object localObject = paramArrayOfHighlight;
      if (j >= localObject.length) {
        break;
      }
      int i1 = (int)localObject[j].getX();
      if (i1 < arrayOfFloat.length)
      {
        localObject = ((PieData)this.mChart.getData()).getDataSetByIndex(localObject[j].getDataSetIndex());
        if ((localObject != null) && (((IPieDataSet)localObject).isHighlightEnabled()))
        {
          int i2 = ((IPieDataSet)localObject).getEntryCount();
          int n = 0;
          int m;
          for (int k = 0; n < i2; k = m)
          {
            m = k;
            if (Math.abs(((PieEntry)((IPieDataSet)localObject).getEntryForIndex(n)).getY()) > Utils.FLOAT_EPSILON) {
              m = k + 1;
            }
            n++;
          }
          float f4;
          if (i1 == 0) {
            f4 = 0.0F;
          } else {
            f4 = paramCanvas[(i1 - 1)] * f3;
          }
          float f5;
          if (k <= 1) {
            f5 = 0.0F;
          } else {
            f5 = ((IPieDataSet)localObject).getSliceSpace();
          }
          float f11 = arrayOfFloat[i1];
          float f2 = ((IPieDataSet)localObject).getSelectionShift();
          float f13 = f8 + f2;
          localRectF.set(this.mChart.getCircleBox());
          f2 = -f2;
          localRectF.inset(f2, f2);
          if ((f5 > 0.0F) && (f11 <= 180.0F)) {
            m = 1;
          } else {
            m = 0;
          }
          this.mRenderPaint.setColor(((IPieDataSet)localObject).getColor(i1));
          if (k == 1) {
            f6 = 0.0F;
          } else {
            f6 = f5 / (f8 * 0.017453292F);
          }
          if (k == 1) {
            f2 = 0.0F;
          } else {
            f2 = f5 / (f13 * 0.017453292F);
          }
          float f12 = f9 + (f4 + f6 / 2.0F) * f10;
          float f6 = (f11 - f6) * f10;
          if (f6 < 0.0F) {
            f6 = 0.0F;
          }
          float f14 = (f4 + f2 / 2.0F) * f10 + f9;
          float f7 = (f11 - f2) * f10;
          f2 = f7;
          if (f7 < 0.0F) {
            f2 = 0.0F;
          }
          this.mPathBuffer.reset();
          double d;
          if ((f6 >= 360.0F) && (f6 % 360.0F <= Utils.FLOAT_EPSILON))
          {
            this.mPathBuffer.addCircle(localMPPointF.x, localMPPointF.y, f13, Path.Direction.CW);
          }
          else
          {
            localObject = this.mPathBuffer;
            f7 = localMPPointF.x;
            d = f14 * 0.017453292F;
            ((Path)localObject).moveTo(f7 + (float)Math.cos(d) * f13, localMPPointF.y + f13 * (float)Math.sin(d));
            this.mPathBuffer.arcTo(localRectF, f14, f2);
          }
          if (m != 0)
          {
            f2 = localMPPointF.x;
            d = f12 * 0.017453292F;
            f2 = calculateMinimumRadiusForSpacedSlice(localMPPointF, f8, f11 * f10, (float)Math.cos(d) * f8 + f2, localMPPointF.y + (float)Math.sin(d) * f8, f12, f6);
          }
          else
          {
            f2 = 0.0F;
          }
          this.mInnerRectBuffer.set(localMPPointF.x - f1, localMPPointF.y - f1, localMPPointF.x + f1, localMPPointF.y + f1);
          if ((i != 0) && ((f1 > 0.0F) || (m != 0)))
          {
            if (m != 0)
            {
              f7 = f2;
              if (f2 < 0.0F) {
                f7 = -f2;
              }
              f2 = Math.max(f1, f7);
            }
            else
            {
              f2 = f1;
            }
            if ((k != 1) && (f2 != 0.0F)) {
              f5 /= f2 * 0.017453292F;
            } else {
              f5 = 0.0F;
            }
            f12 = f5 / 2.0F;
            f7 = (f11 - f5) * f10;
            f5 = f7;
            if (f7 < 0.0F) {
              f5 = 0.0F;
            }
            f4 = f9 + (f4 + f12) * f10 + f5;
            if ((f6 >= 360.0F) && (f6 % 360.0F <= Utils.FLOAT_EPSILON))
            {
              this.mPathBuffer.addCircle(localMPPointF.x, localMPPointF.y, f2, Path.Direction.CCW);
            }
            else
            {
              localObject = this.mPathBuffer;
              f6 = localMPPointF.x;
              d = f4 * 0.017453292F;
              ((Path)localObject).lineTo(f6 + (float)Math.cos(d) * f2, localMPPointF.y + f2 * (float)Math.sin(d));
              this.mPathBuffer.arcTo(this.mInnerRectBuffer, f4, -f5);
            }
          }
          else if (f6 % 360.0F > Utils.FLOAT_EPSILON)
          {
            if (m != 0)
            {
              f5 = f6 / 2.0F;
              f4 = localMPPointF.x;
              d = (f12 + f5) * 0.017453292F;
              f6 = (float)Math.cos(d);
              f5 = localMPPointF.y;
              f7 = (float)Math.sin(d);
              this.mPathBuffer.lineTo(f4 + f6 * f2, f5 + f2 * f7);
            }
            else
            {
              this.mPathBuffer.lineTo(localMPPointF.x, localMPPointF.y);
            }
          }
          this.mPathBuffer.close();
          this.mBitmapCanvas.drawPath(this.mPathBuffer, this.mRenderPaint);
        }
      }
    }
    MPPointF.recycleInstance(localMPPointF);
  }
  
  protected void drawHole(Canvas paramCanvas)
  {
    if ((this.mChart.isDrawHoleEnabled()) && (this.mBitmapCanvas != null))
    {
      float f2 = this.mChart.getRadius();
      float f3 = this.mChart.getHoleRadius() / 100.0F * f2;
      paramCanvas = this.mChart.getCenterCircleBox();
      if (Color.alpha(this.mHolePaint.getColor()) > 0) {
        this.mBitmapCanvas.drawCircle(paramCanvas.x, paramCanvas.y, f3, this.mHolePaint);
      }
      if ((Color.alpha(this.mTransparentCirclePaint.getColor()) > 0) && (this.mChart.getTransparentCircleRadius() > this.mChart.getHoleRadius()))
      {
        int i = this.mTransparentCirclePaint.getAlpha();
        float f1 = this.mChart.getTransparentCircleRadius() / 100.0F;
        this.mTransparentCirclePaint.setAlpha((int)(i * this.mAnimator.getPhaseX() * this.mAnimator.getPhaseY()));
        this.mHoleCirclePath.reset();
        this.mHoleCirclePath.addCircle(paramCanvas.x, paramCanvas.y, f2 * f1, Path.Direction.CW);
        this.mHoleCirclePath.addCircle(paramCanvas.x, paramCanvas.y, f3, Path.Direction.CCW);
        this.mBitmapCanvas.drawPath(this.mHoleCirclePath, this.mTransparentCirclePaint);
        this.mTransparentCirclePaint.setAlpha(i);
      }
      MPPointF.recycleInstance(paramCanvas);
    }
  }
  
  protected void drawRoundedSlices(Canvas paramCanvas)
  {
    if (!this.mChart.isDrawRoundedSlicesEnabled()) {
      return;
    }
    IPieDataSet localIPieDataSet = ((PieData)this.mChart.getData()).getDataSet();
    if (!localIPieDataSet.isVisible()) {
      return;
    }
    float f5 = this.mAnimator.getPhaseX();
    float f2 = this.mAnimator.getPhaseY();
    MPPointF localMPPointF = this.mChart.getCenterCircleBox();
    float f4 = this.mChart.getRadius();
    float f3 = (f4 - this.mChart.getHoleRadius() * f4 / 100.0F) / 2.0F;
    paramCanvas = this.mChart.getDrawAngles();
    float f1 = this.mChart.getRotationAngle();
    for (int i = 0; i < localIPieDataSet.getEntryCount(); i++)
    {
      float f6 = paramCanvas[i];
      if (Math.abs(localIPieDataSet.getEntryForIndex(i).getY()) > Utils.FLOAT_EPSILON)
      {
        double d1 = f4 - f3;
        double d4 = (f1 + f6) * f2;
        double d2 = Math.cos(Math.toRadians(d4));
        Double.isNaN(d1);
        double d3 = localMPPointF.x;
        Double.isNaN(d3);
        float f8 = (float)(d3 + d2 * d1);
        d3 = Math.sin(Math.toRadians(d4));
        Double.isNaN(d1);
        d2 = localMPPointF.y;
        Double.isNaN(d2);
        float f7 = (float)(d1 * d3 + d2);
        this.mRenderPaint.setColor(localIPieDataSet.getColor(i));
        this.mBitmapCanvas.drawCircle(f8, f7, f3, this.mRenderPaint);
      }
      f1 += f6 * f5;
    }
    MPPointF.recycleInstance(localMPPointF);
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    Canvas localCanvas = paramCanvas;
    MPPointF localMPPointF = this.mChart.getCenterCircleBox();
    float f3 = this.mChart.getRadius();
    float f2 = this.mChart.getRotationAngle();
    float[] arrayOfFloat = this.mChart.getDrawAngles();
    Object localObject2 = this.mChart.getAbsoluteAngles();
    float f9 = this.mAnimator.getPhaseX();
    float f8 = this.mAnimator.getPhaseY();
    float f10 = this.mChart.getHoleRadius() / 100.0F;
    float f1 = f3 / 10.0F * 3.6F;
    if (this.mChart.isDrawHoleEnabled()) {
      f1 = (f3 - f3 * f10) / 2.0F;
    }
    float f13 = f3 - f1;
    PieData localPieData = (PieData)this.mChart.getData();
    Object localObject1 = localPieData.getDataSets();
    float f12 = localPieData.getYValueSum();
    boolean bool2 = this.mChart.isDrawEntryLabelsEnabled();
    paramCanvas.save();
    float f11 = Utils.convertDpToPixel(5.0F);
    int j = 0;
    int i = 0;
    f1 = f3;
    while (i < ((List)localObject1).size())
    {
      Object localObject4 = (IPieDataSet)((List)localObject1).get(i);
      boolean bool1 = ((IPieDataSet)localObject4).isDrawValuesEnabled();
      if ((!bool1) && (!bool2))
      {
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
      else
      {
        PieDataSet.ValuePosition localValuePosition1 = ((IPieDataSet)localObject4).getXValuePosition();
        PieDataSet.ValuePosition localValuePosition2 = ((IPieDataSet)localObject4).getYValuePosition();
        applyValueTextStyle((IDataSet)localObject4);
        float f14 = Utils.calcTextHeight(this.mValuePaint, "Q") + Utils.convertDpToPixel(4.0F);
        IValueFormatter localIValueFormatter = ((IPieDataSet)localObject4).getValueFormatter();
        int m = ((IPieDataSet)localObject4).getEntryCount();
        this.mValueLinePaint.setColor(((IPieDataSet)localObject4).getValueLineColor());
        this.mValueLinePaint.setStrokeWidth(Utils.convertDpToPixel(((IPieDataSet)localObject4).getValueLineWidth()));
        float f15 = getSliceSpace((IPieDataSet)localObject4);
        localObject3 = MPPointF.getInstance(((IPieDataSet)localObject4).getIconsOffset());
        ((MPPointF)localObject3).x = Utils.convertDpToPixel(((MPPointF)localObject3).x);
        ((MPPointF)localObject3).y = Utils.convertDpToPixel(((MPPointF)localObject3).y);
        int n = 0;
        int k = j;
        j = i;
        for (i = n; i < m; i++)
        {
          Object localObject5 = (PieEntry)((IPieDataSet)localObject4).getEntryForIndex(i);
          if (k == 0) {
            f3 = 0.0F;
          } else {
            f3 = localObject2[(k - 1)] * f9;
          }
          float f6 = (f3 + (arrayOfFloat[k] - f15 / (f13 * 0.017453292F) / 2.0F) / 2.0F) * f8 + f2;
          float f4;
          if (this.mChart.isUsePercentValuesEnabled()) {
            f4 = ((PieEntry)localObject5).getY() / f12 * 100.0F;
          } else {
            f4 = ((PieEntry)localObject5).getY();
          }
          double d = f6 * 0.017453292F;
          float f7 = (float)Math.cos(d);
          float f16 = (float)Math.sin(d);
          if ((bool2) && (localValuePosition1 == PieDataSet.ValuePosition.OUTSIDE_SLICE)) {
            n = 1;
          } else {
            n = 0;
          }
          int i1;
          if ((bool1) && (localValuePosition2 == PieDataSet.ValuePosition.OUTSIDE_SLICE)) {
            i1 = 1;
          } else {
            i1 = 0;
          }
          int i2;
          if ((bool2) && (localValuePosition1 == PieDataSet.ValuePosition.INSIDE_SLICE)) {
            i2 = 1;
          } else {
            i2 = 0;
          }
          int i3;
          if ((bool1) && (localValuePosition2 == PieDataSet.ValuePosition.INSIDE_SLICE)) {
            i3 = 1;
          } else {
            i3 = 0;
          }
          if ((n == 0) && (i1 == 0)) {
            break label1147;
          }
          float f19 = ((IPieDataSet)localObject4).getValueLinePart1Length();
          f3 = ((IPieDataSet)localObject4).getValueLinePart2Length();
          float f5 = ((IPieDataSet)localObject4).getValueLinePart1OffsetPercentage() / 100.0F;
          if (this.mChart.isDrawHoleEnabled())
          {
            f17 = f1 * f10;
            f5 = (f1 - f17) * f5 + f17;
          }
          else
          {
            f5 = f1 * f5;
          }
          if (((IPieDataSet)localObject4).isValueLineVariableLength()) {
            f3 = f3 * f13 * (float)Math.abs(Math.sin(d));
          } else {
            f3 *= f13;
          }
          float f18 = localMPPointF.x;
          float f17 = localMPPointF.y;
          f19 = (f19 + 1.0F) * f13;
          float f20 = localMPPointF.x + f19 * f7;
          f19 = f19 * f16 + localMPPointF.y;
          d = f6;
          Double.isNaN(d);
          d %= 360.0D;
          if ((d >= 90.0D) && (d <= 270.0D))
          {
            f3 = f20 - f3;
            this.mValuePaint.setTextAlign(Paint.Align.RIGHT);
            if (n != 0) {
              this.mEntryLabelsPaint.setTextAlign(Paint.Align.RIGHT);
            }
            f6 = f3;
            f3 -= f11;
          }
          else
          {
            f6 = f20 + f3;
            this.mValuePaint.setTextAlign(Paint.Align.LEFT);
            if (n != 0) {
              this.mEntryLabelsPaint.setTextAlign(Paint.Align.LEFT);
            }
            f3 = f6 + f11;
          }
          if (((IPieDataSet)localObject4).getValueLineColor() != 1122867)
          {
            if (((IPieDataSet)localObject4).isUsingSliceColorAsValueLineColor()) {
              this.mValueLinePaint.setColor(((IPieDataSet)localObject4).getColor(i));
            }
            paramCanvas.drawLine(f5 * f7 + f18, f5 * f16 + f17, f20, f19, this.mValueLinePaint);
            paramCanvas.drawLine(f20, f19, f6, f19, this.mValueLinePaint);
          }
          int i4 = i;
          if ((n != 0) && (i1 != 0))
          {
            drawValue(paramCanvas, localIValueFormatter, f4, (Entry)localObject5, 0, f3, f19, ((IPieDataSet)localObject4).getValueTextColor(i4));
            if ((i4 < localPieData.getEntryCount()) && (((PieEntry)localObject5).getLabel() != null)) {
              drawEntryLabel(localCanvas, ((PieEntry)localObject5).getLabel(), f3, f19 + f14);
            }
          }
          else if (n != 0)
          {
            if ((i4 < localPieData.getEntryCount()) && (((PieEntry)localObject5).getLabel() != null)) {
              drawEntryLabel(localCanvas, ((PieEntry)localObject5).getLabel(), f3, f19 + f14 / 2.0F);
            }
          }
          else if (i1 != 0)
          {
            drawValue(paramCanvas, localIValueFormatter, f4, (Entry)localObject5, 0, f3, f19 + f14 / 2.0F, ((IPieDataSet)localObject4).getValueTextColor(i4));
          }
          label1147:
          if ((i2 == 0) && (i3 == 0)) {
            break label1361;
          }
          f3 = f13 * f7 + localMPPointF.x;
          f5 = f13 * f16 + localMPPointF.y;
          this.mValuePaint.setTextAlign(Paint.Align.CENTER);
          if ((i2 != 0) && (i3 != 0))
          {
            drawValue(paramCanvas, localIValueFormatter, f4, (Entry)localObject5, 0, f3, f5, ((IPieDataSet)localObject4).getValueTextColor(i));
            if ((i < localPieData.getEntryCount()) && (((PieEntry)localObject5).getLabel() != null)) {
              drawEntryLabel(paramCanvas, ((PieEntry)localObject5).getLabel(), f3, f5 + f14);
            }
          }
          else if (i2 != 0)
          {
            if ((i < localPieData.getEntryCount()) && (((PieEntry)localObject5).getLabel() != null)) {
              drawEntryLabel(paramCanvas, ((PieEntry)localObject5).getLabel(), f3, f5 + f14 / 2.0F);
            }
          }
          else if (i3 != 0)
          {
            drawValue(paramCanvas, localIValueFormatter, f4, (Entry)localObject5, 0, f3, f5 + f14 / 2.0F, ((IPieDataSet)localObject4).getValueTextColor(i));
          }
          label1361:
          localCanvas = paramCanvas;
          if ((((PieEntry)localObject5).getIcon() != null) && (((IPieDataSet)localObject4).isDrawIconsEnabled()))
          {
            Drawable localDrawable = ((PieEntry)localObject5).getIcon();
            localObject5 = localObject3;
            f5 = ((MPPointF)localObject5).y;
            f4 = localMPPointF.x;
            f17 = ((MPPointF)localObject5).y;
            f3 = localMPPointF.y;
            f6 = ((MPPointF)localObject5).x;
            Utils.drawImage(paramCanvas, localDrawable, (int)((f13 + f5) * f7 + f4), (int)((f13 + f17) * f16 + f3 + f6), localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
          }
          k++;
        }
        i = j;
        localObject4 = localObject1;
        localObject1 = localObject2;
        MPPointF.recycleInstance((MPPointF)localObject3);
        j = k;
        localObject2 = localObject4;
      }
      i++;
      Object localObject3 = localObject2;
      localObject2 = localObject1;
      localObject1 = localObject3;
    }
    MPPointF.recycleInstance(localMPPointF);
    paramCanvas.restore();
  }
  
  public TextPaint getPaintCenterText()
  {
    return this.mCenterTextPaint;
  }
  
  public Paint getPaintEntryLabels()
  {
    return this.mEntryLabelsPaint;
  }
  
  public Paint getPaintHole()
  {
    return this.mHolePaint;
  }
  
  public Paint getPaintTransparentCircle()
  {
    return this.mTransparentCirclePaint;
  }
  
  protected float getSliceSpace(IPieDataSet paramIPieDataSet)
  {
    if (!paramIPieDataSet.isAutomaticallyDisableSliceSpacingEnabled()) {
      return paramIPieDataSet.getSliceSpace();
    }
    float f;
    if (paramIPieDataSet.getSliceSpace() / this.mViewPortHandler.getSmallestContentExtension() > paramIPieDataSet.getYMin() / ((PieData)this.mChart.getData()).getYValueSum() * 2.0F) {
      f = 0.0F;
    } else {
      f = paramIPieDataSet.getSliceSpace();
    }
    return f;
  }
  
  public void initBuffers() {}
  
  public void releaseBitmap()
  {
    Object localObject = this.mBitmapCanvas;
    if (localObject != null)
    {
      ((Canvas)localObject).setBitmap(null);
      this.mBitmapCanvas = null;
    }
    localObject = this.mDrawBitmap;
    if (localObject != null)
    {
      localObject = (Bitmap)((WeakReference)localObject).get();
      if (localObject != null) {
        ((Bitmap)localObject).recycle();
      }
      this.mDrawBitmap.clear();
      this.mDrawBitmap = null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/PieChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */