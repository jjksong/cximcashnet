package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.ScatterDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.renderer.scatter.IShapeRenderer;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.Iterator;
import java.util.List;

public class ScatterChartRenderer
  extends LineScatterCandleRadarRenderer
{
  protected ScatterDataProvider mChart;
  float[] mPixelBuffer = new float[2];
  
  public ScatterChartRenderer(ScatterDataProvider paramScatterDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramScatterDataProvider;
  }
  
  public void drawData(Canvas paramCanvas)
  {
    Iterator localIterator = this.mChart.getScatterData().getDataSets().iterator();
    while (localIterator.hasNext())
    {
      IScatterDataSet localIScatterDataSet = (IScatterDataSet)localIterator.next();
      if (localIScatterDataSet.isVisible()) {
        drawDataSet(paramCanvas, localIScatterDataSet);
      }
    }
  }
  
  protected void drawDataSet(Canvas paramCanvas, IScatterDataSet paramIScatterDataSet)
  {
    if (paramIScatterDataSet.getEntryCount() < 1) {
      return;
    }
    ViewPortHandler localViewPortHandler1 = this.mViewPortHandler;
    Transformer localTransformer = this.mChart.getTransformer(paramIScatterDataSet.getAxisDependency());
    float f = this.mAnimator.getPhaseY();
    IShapeRenderer localIShapeRenderer = paramIScatterDataSet.getShapeRenderer();
    if (localIShapeRenderer == null)
    {
      Log.i("MISSING", "There's no IShapeRenderer specified for ScatterDataSet");
      return;
    }
    int j = (int)Math.min(Math.ceil(paramIScatterDataSet.getEntryCount() * this.mAnimator.getPhaseX()), paramIScatterDataSet.getEntryCount());
    for (int i = 0; i < j; i++)
    {
      Object localObject = paramIScatterDataSet.getEntryForIndex(i);
      this.mPixelBuffer[0] = ((Entry)localObject).getX();
      this.mPixelBuffer[1] = (((Entry)localObject).getY() * f);
      localTransformer.pointValuesToPixel(this.mPixelBuffer);
      if (!localViewPortHandler1.isInBoundsRight(this.mPixelBuffer[0])) {
        break;
      }
      if ((localViewPortHandler1.isInBoundsLeft(this.mPixelBuffer[0])) && (localViewPortHandler1.isInBoundsY(this.mPixelBuffer[1])))
      {
        this.mRenderPaint.setColor(paramIScatterDataSet.getColor(i / 2));
        ViewPortHandler localViewPortHandler2 = this.mViewPortHandler;
        localObject = this.mPixelBuffer;
        localIShapeRenderer.renderShape(paramCanvas, paramIScatterDataSet, localViewPortHandler2, localObject[0], localObject[1], this.mRenderPaint);
      }
    }
  }
  
  public void drawExtras(Canvas paramCanvas) {}
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    ScatterData localScatterData = this.mChart.getScatterData();
    int j = paramArrayOfHighlight.length;
    for (int i = 0; i < j; i++)
    {
      Highlight localHighlight = paramArrayOfHighlight[i];
      IScatterDataSet localIScatterDataSet = (IScatterDataSet)localScatterData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localIScatterDataSet != null) && (localIScatterDataSet.isHighlightEnabled()))
      {
        Object localObject = localIScatterDataSet.getEntryForXValue(localHighlight.getX(), localHighlight.getY());
        if (isInBoundsX((Entry)localObject, localIScatterDataSet))
        {
          localObject = this.mChart.getTransformer(localIScatterDataSet.getAxisDependency()).getPixelForValues(((Entry)localObject).getX(), ((Entry)localObject).getY() * this.mAnimator.getPhaseY());
          localHighlight.setDraw((float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y);
          drawHighlightLines(paramCanvas, (float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y, localIScatterDataSet);
        }
      }
    }
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    if (isDrawingValuesAllowed(this.mChart))
    {
      List localList = this.mChart.getScatterData().getDataSets();
      for (int i = 0; i < this.mChart.getScatterData().getDataSetCount(); i++)
      {
        IScatterDataSet localIScatterDataSet = (IScatterDataSet)localList.get(i);
        if ((shouldDrawValues(localIScatterDataSet)) && (localIScatterDataSet.getEntryCount() >= 1))
        {
          applyValueTextStyle(localIScatterDataSet);
          this.mXBounds.set(this.mChart, localIScatterDataSet);
          float[] arrayOfFloat = this.mChart.getTransformer(localIScatterDataSet.getAxisDependency()).generateTransformedValuesScatter(localIScatterDataSet, this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);
          float f = Utils.convertDpToPixel(localIScatterDataSet.getScatterShapeSize());
          MPPointF localMPPointF = MPPointF.getInstance(localIScatterDataSet.getIconsOffset());
          localMPPointF.x = Utils.convertDpToPixel(localMPPointF.x);
          localMPPointF.y = Utils.convertDpToPixel(localMPPointF.y);
          for (int j = 0; (j < arrayOfFloat.length) && (this.mViewPortHandler.isInBoundsRight(arrayOfFloat[j])); j += 2) {
            if (this.mViewPortHandler.isInBoundsLeft(arrayOfFloat[j]))
            {
              Object localObject1 = this.mViewPortHandler;
              int m = j + 1;
              if (((ViewPortHandler)localObject1).isInBoundsY(arrayOfFloat[m]))
              {
                int k = j / 2;
                Object localObject2 = localIScatterDataSet.getEntryForIndex(this.mXBounds.min + k);
                if (localIScatterDataSet.isDrawValuesEnabled()) {
                  drawValue(paramCanvas, localIScatterDataSet.getValueFormatter(), ((Entry)localObject2).getY(), (Entry)localObject2, i, arrayOfFloat[j], arrayOfFloat[m] - f, localIScatterDataSet.getValueTextColor(k + this.mXBounds.min));
                }
                localObject1 = localMPPointF;
                if ((((Entry)localObject2).getIcon() != null) && (localIScatterDataSet.isDrawIconsEnabled()))
                {
                  localObject2 = ((Entry)localObject2).getIcon();
                  Utils.drawImage(paramCanvas, (Drawable)localObject2, (int)(arrayOfFloat[j] + ((MPPointF)localObject1).x), (int)(arrayOfFloat[m] + ((MPPointF)localObject1).y), ((Drawable)localObject2).getIntrinsicWidth(), ((Drawable)localObject2).getIntrinsicHeight());
                }
              }
            }
          }
          MPPointF.recycleInstance(localMPPointF);
        }
      }
    }
  }
  
  public void initBuffers() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/ScatterChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */