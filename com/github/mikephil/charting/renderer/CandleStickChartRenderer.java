package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.CandleDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.Iterator;
import java.util.List;

public class CandleStickChartRenderer
  extends LineScatterCandleRadarRenderer
{
  private float[] mBodyBuffers = new float[4];
  protected CandleDataProvider mChart;
  private float[] mCloseBuffers = new float[4];
  private float[] mOpenBuffers = new float[4];
  private float[] mRangeBuffers = new float[4];
  private float[] mShadowBuffers = new float[8];
  
  public CandleStickChartRenderer(CandleDataProvider paramCandleDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramCandleDataProvider;
  }
  
  public void drawData(Canvas paramCanvas)
  {
    Iterator localIterator = this.mChart.getCandleData().getDataSets().iterator();
    while (localIterator.hasNext())
    {
      ICandleDataSet localICandleDataSet = (ICandleDataSet)localIterator.next();
      if (localICandleDataSet.isVisible()) {
        drawDataSet(paramCanvas, localICandleDataSet);
      }
    }
  }
  
  protected void drawDataSet(Canvas paramCanvas, ICandleDataSet paramICandleDataSet)
  {
    Transformer localTransformer = this.mChart.getTransformer(paramICandleDataSet.getAxisDependency());
    float f5 = this.mAnimator.getPhaseY();
    float f1 = paramICandleDataSet.getBarSpace();
    boolean bool = paramICandleDataSet.getShowCandleBar();
    this.mXBounds.set(this.mChart, paramICandleDataSet);
    this.mRenderPaint.setStrokeWidth(paramICandleDataSet.getShadowWidth());
    for (int j = this.mXBounds.min; j <= this.mXBounds.range + this.mXBounds.min; j++)
    {
      Object localObject = (CandleEntry)paramICandleDataSet.getEntryForIndex(j);
      if (localObject != null)
      {
        float f3 = ((CandleEntry)localObject).getX();
        float f4 = ((CandleEntry)localObject).getOpen();
        float f2 = ((CandleEntry)localObject).getClose();
        float f6 = ((CandleEntry)localObject).getHigh();
        float f7 = ((CandleEntry)localObject).getLow();
        int i;
        if (bool)
        {
          localObject = this.mShadowBuffers;
          localObject[0] = f3;
          localObject[2] = f3;
          localObject[4] = f3;
          localObject[6] = f3;
          if (f4 > f2)
          {
            localObject[1] = (f6 * f5);
            localObject[3] = (f4 * f5);
            localObject[5] = (f7 * f5);
            localObject[7] = (f2 * f5);
          }
          else if (f4 < f2)
          {
            localObject[1] = (f6 * f5);
            localObject[3] = (f2 * f5);
            localObject[5] = (f7 * f5);
            localObject[7] = (f4 * f5);
          }
          else
          {
            localObject[1] = (f6 * f5);
            localObject[3] = (f4 * f5);
            localObject[5] = (f7 * f5);
            localObject[7] = localObject[3];
          }
          localTransformer.pointValuesToPixel(this.mShadowBuffers);
          if (paramICandleDataSet.getShadowColorSameAsCandle())
          {
            if (f4 > f2)
            {
              localObject = this.mRenderPaint;
              if (paramICandleDataSet.getDecreasingColor() == 1122867) {
                i = paramICandleDataSet.getColor(j);
              } else {
                i = paramICandleDataSet.getDecreasingColor();
              }
              ((Paint)localObject).setColor(i);
            }
            else if (f4 < f2)
            {
              localObject = this.mRenderPaint;
              if (paramICandleDataSet.getIncreasingColor() == 1122867) {
                i = paramICandleDataSet.getColor(j);
              } else {
                i = paramICandleDataSet.getIncreasingColor();
              }
              ((Paint)localObject).setColor(i);
            }
            else
            {
              localObject = this.mRenderPaint;
              if (paramICandleDataSet.getNeutralColor() == 1122867) {
                i = paramICandleDataSet.getColor(j);
              } else {
                i = paramICandleDataSet.getNeutralColor();
              }
              ((Paint)localObject).setColor(i);
            }
          }
          else
          {
            localObject = this.mRenderPaint;
            if (paramICandleDataSet.getShadowColor() == 1122867) {
              i = paramICandleDataSet.getColor(j);
            } else {
              i = paramICandleDataSet.getShadowColor();
            }
            ((Paint)localObject).setColor(i);
          }
          this.mRenderPaint.setStyle(Paint.Style.STROKE);
          paramCanvas.drawLines(this.mShadowBuffers, this.mRenderPaint);
          localObject = this.mBodyBuffers;
          localObject[0] = (f3 - 0.5F + f1);
          localObject[1] = (f2 * f5);
          localObject[2] = (f3 + 0.5F - f1);
          localObject[3] = (f4 * f5);
          localTransformer.pointValuesToPixel((float[])localObject);
          if (f4 > f2)
          {
            if (paramICandleDataSet.getDecreasingColor() == 1122867) {
              this.mRenderPaint.setColor(paramICandleDataSet.getColor(j));
            } else {
              this.mRenderPaint.setColor(paramICandleDataSet.getDecreasingColor());
            }
            this.mRenderPaint.setStyle(paramICandleDataSet.getDecreasingPaintStyle());
            localObject = this.mBodyBuffers;
            paramCanvas.drawRect(localObject[0], localObject[3], localObject[2], localObject[1], this.mRenderPaint);
          }
          else if (f4 < f2)
          {
            if (paramICandleDataSet.getIncreasingColor() == 1122867) {
              this.mRenderPaint.setColor(paramICandleDataSet.getColor(j));
            } else {
              this.mRenderPaint.setColor(paramICandleDataSet.getIncreasingColor());
            }
            this.mRenderPaint.setStyle(paramICandleDataSet.getIncreasingPaintStyle());
            localObject = this.mBodyBuffers;
            paramCanvas.drawRect(localObject[0], localObject[1], localObject[2], localObject[3], this.mRenderPaint);
          }
          else
          {
            if (paramICandleDataSet.getNeutralColor() == 1122867) {
              this.mRenderPaint.setColor(paramICandleDataSet.getColor(j));
            } else {
              this.mRenderPaint.setColor(paramICandleDataSet.getNeutralColor());
            }
            localObject = this.mBodyBuffers;
            paramCanvas.drawLine(localObject[0], localObject[1], localObject[2], localObject[3], this.mRenderPaint);
          }
        }
        else
        {
          localObject = this.mRangeBuffers;
          localObject[0] = f3;
          localObject[1] = (f6 * f5);
          localObject[2] = f3;
          localObject[3] = (f7 * f5);
          float[] arrayOfFloat = this.mOpenBuffers;
          arrayOfFloat[0] = (f3 - 0.5F + f1);
          f6 = f4 * f5;
          arrayOfFloat[1] = f6;
          arrayOfFloat[2] = f3;
          arrayOfFloat[3] = f6;
          arrayOfFloat = this.mCloseBuffers;
          arrayOfFloat[0] = (0.5F + f3 - f1);
          f6 = f2 * f5;
          arrayOfFloat[1] = f6;
          arrayOfFloat[2] = f3;
          arrayOfFloat[3] = f6;
          localTransformer.pointValuesToPixel((float[])localObject);
          localTransformer.pointValuesToPixel(this.mOpenBuffers);
          localTransformer.pointValuesToPixel(this.mCloseBuffers);
          if (f4 > f2)
          {
            if (paramICandleDataSet.getDecreasingColor() == 1122867) {
              i = paramICandleDataSet.getColor(j);
            } else {
              i = paramICandleDataSet.getDecreasingColor();
            }
          }
          else if (f4 < f2)
          {
            if (paramICandleDataSet.getIncreasingColor() == 1122867) {
              i = paramICandleDataSet.getColor(j);
            } else {
              i = paramICandleDataSet.getIncreasingColor();
            }
          }
          else if (paramICandleDataSet.getNeutralColor() == 1122867) {
            i = paramICandleDataSet.getColor(j);
          } else {
            i = paramICandleDataSet.getNeutralColor();
          }
          this.mRenderPaint.setColor(i);
          localObject = this.mRangeBuffers;
          paramCanvas.drawLine(localObject[0], localObject[1], localObject[2], localObject[3], this.mRenderPaint);
          localObject = this.mOpenBuffers;
          paramCanvas.drawLine(localObject[0], localObject[1], localObject[2], localObject[3], this.mRenderPaint);
          localObject = this.mCloseBuffers;
          paramCanvas.drawLine(localObject[0], localObject[1], localObject[2], localObject[3], this.mRenderPaint);
        }
      }
    }
  }
  
  public void drawExtras(Canvas paramCanvas) {}
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    CandleData localCandleData = this.mChart.getCandleData();
    int j = paramArrayOfHighlight.length;
    for (int i = 0; i < j; i++)
    {
      Highlight localHighlight = paramArrayOfHighlight[i];
      ICandleDataSet localICandleDataSet = (ICandleDataSet)localCandleData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localICandleDataSet != null) && (localICandleDataSet.isHighlightEnabled()))
      {
        Object localObject = (CandleEntry)localICandleDataSet.getEntryForXValue(localHighlight.getX(), localHighlight.getY());
        if (isInBoundsX((Entry)localObject, localICandleDataSet))
        {
          float f = (((CandleEntry)localObject).getLow() * this.mAnimator.getPhaseY() + ((CandleEntry)localObject).getHigh() * this.mAnimator.getPhaseY()) / 2.0F;
          localObject = this.mChart.getTransformer(localICandleDataSet.getAxisDependency()).getPixelForValues(((CandleEntry)localObject).getX(), f);
          localHighlight.setDraw((float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y);
          drawHighlightLines(paramCanvas, (float)((MPPointD)localObject).x, (float)((MPPointD)localObject).y, localICandleDataSet);
        }
      }
    }
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    if (isDrawingValuesAllowed(this.mChart))
    {
      List localList = this.mChart.getCandleData().getDataSets();
      for (int i = 0; i < localList.size(); i++)
      {
        ICandleDataSet localICandleDataSet = (ICandleDataSet)localList.get(i);
        if ((shouldDrawValues(localICandleDataSet)) && (localICandleDataSet.getEntryCount() >= 1))
        {
          applyValueTextStyle(localICandleDataSet);
          Object localObject1 = this.mChart.getTransformer(localICandleDataSet.getAxisDependency());
          this.mXBounds.set(this.mChart, localICandleDataSet);
          float[] arrayOfFloat = ((Transformer)localObject1).generateTransformedValuesCandle(localICandleDataSet, this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);
          float f3 = Utils.convertDpToPixel(5.0F);
          localObject1 = MPPointF.getInstance(localICandleDataSet.getIconsOffset());
          ((MPPointF)localObject1).x = Utils.convertDpToPixel(((MPPointF)localObject1).x);
          ((MPPointF)localObject1).y = Utils.convertDpToPixel(((MPPointF)localObject1).y);
          for (int j = 0; j < arrayOfFloat.length; j += 2)
          {
            float f2 = arrayOfFloat[j];
            float f1 = arrayOfFloat[(j + 1)];
            if (!this.mViewPortHandler.isInBoundsRight(f2)) {
              break;
            }
            if ((this.mViewPortHandler.isInBoundsLeft(f2)) && (this.mViewPortHandler.isInBoundsY(f1)))
            {
              int k = j / 2;
              Object localObject3 = (CandleEntry)localICandleDataSet.getEntryForIndex(this.mXBounds.min + k);
              if (localICandleDataSet.isDrawValuesEnabled()) {
                drawValue(paramCanvas, localICandleDataSet.getValueFormatter(), ((CandleEntry)localObject3).getHigh(), (Entry)localObject3, i, f2, f1 - f3, localICandleDataSet.getValueTextColor(k));
              }
              Object localObject2 = localObject1;
              if ((((CandleEntry)localObject3).getIcon() != null) && (localICandleDataSet.isDrawIconsEnabled()))
              {
                localObject3 = ((CandleEntry)localObject3).getIcon();
                Utils.drawImage(paramCanvas, (Drawable)localObject3, (int)(f2 + ((MPPointF)localObject2).x), (int)(f1 + ((MPPointF)localObject2).y), ((Drawable)localObject3).getIntrinsicWidth(), ((Drawable)localObject3).getIntrinsicHeight());
              }
            }
          }
          MPPointF.recycleInstance((MPPointF)localObject1);
        }
      }
    }
  }
  
  public void initBuffers() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/CandleStickChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */