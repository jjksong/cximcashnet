package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

public abstract class DataRenderer
  extends Renderer
{
  protected ChartAnimator mAnimator;
  protected Paint mDrawPaint;
  protected Paint mHighlightPaint;
  protected Paint mRenderPaint;
  protected Paint mValuePaint;
  
  public DataRenderer(ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramViewPortHandler);
    this.mAnimator = paramChartAnimator;
    this.mRenderPaint = new Paint(1);
    this.mRenderPaint.setStyle(Paint.Style.FILL);
    this.mDrawPaint = new Paint(4);
    this.mValuePaint = new Paint(1);
    this.mValuePaint.setColor(Color.rgb(63, 63, 63));
    this.mValuePaint.setTextAlign(Paint.Align.CENTER);
    this.mValuePaint.setTextSize(Utils.convertDpToPixel(9.0F));
    this.mHighlightPaint = new Paint(1);
    this.mHighlightPaint.setStyle(Paint.Style.STROKE);
    this.mHighlightPaint.setStrokeWidth(2.0F);
    this.mHighlightPaint.setColor(Color.rgb(255, 187, 115));
  }
  
  protected void applyValueTextStyle(IDataSet paramIDataSet)
  {
    this.mValuePaint.setTypeface(paramIDataSet.getValueTypeface());
    this.mValuePaint.setTextSize(paramIDataSet.getValueTextSize());
  }
  
  public abstract void drawData(Canvas paramCanvas);
  
  public abstract void drawExtras(Canvas paramCanvas);
  
  public abstract void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight);
  
  public void drawValue(Canvas paramCanvas, IValueFormatter paramIValueFormatter, float paramFloat1, Entry paramEntry, int paramInt1, float paramFloat2, float paramFloat3, int paramInt2)
  {
    this.mValuePaint.setColor(paramInt2);
    paramCanvas.drawText(paramIValueFormatter.getFormattedValue(paramFloat1, paramEntry, paramInt1, this.mViewPortHandler), paramFloat2, paramFloat3, this.mValuePaint);
  }
  
  public abstract void drawValues(Canvas paramCanvas);
  
  public Paint getPaintHighlight()
  {
    return this.mHighlightPaint;
  }
  
  public Paint getPaintRender()
  {
    return this.mRenderPaint;
  }
  
  public Paint getPaintValues()
  {
    return this.mValuePaint;
  }
  
  public abstract void initBuffers();
  
  protected boolean isDrawingValuesAllowed(ChartInterface paramChartInterface)
  {
    boolean bool;
    if (paramChartInterface.getData().getEntryCount() < paramChartInterface.getMaxVisibleCount() * this.mViewPortHandler.getScaleX()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/DataRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */