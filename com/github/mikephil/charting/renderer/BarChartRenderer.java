package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.Range;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class BarChartRenderer
  extends BarLineScatterCandleBubbleRenderer
{
  protected Paint mBarBorderPaint;
  protected BarBuffer[] mBarBuffers;
  protected RectF mBarRect = new RectF();
  private RectF mBarShadowRectBuffer = new RectF();
  protected BarDataProvider mChart;
  protected Paint mShadowPaint;
  
  public BarChartRenderer(BarDataProvider paramBarDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramBarDataProvider;
    this.mHighlightPaint = new Paint(1);
    this.mHighlightPaint.setStyle(Paint.Style.FILL);
    this.mHighlightPaint.setColor(Color.rgb(0, 0, 0));
    this.mHighlightPaint.setAlpha(120);
    this.mShadowPaint = new Paint(1);
    this.mShadowPaint.setStyle(Paint.Style.FILL);
    this.mBarBorderPaint = new Paint(1);
    this.mBarBorderPaint.setStyle(Paint.Style.STROKE);
  }
  
  public void drawData(Canvas paramCanvas)
  {
    BarData localBarData = this.mChart.getBarData();
    for (int i = 0; i < localBarData.getDataSetCount(); i++)
    {
      IBarDataSet localIBarDataSet = (IBarDataSet)localBarData.getDataSetByIndex(i);
      if (localIBarDataSet.isVisible()) {
        drawDataSet(paramCanvas, localIBarDataSet, i);
      }
    }
  }
  
  protected void drawDataSet(Canvas paramCanvas, IBarDataSet paramIBarDataSet, int paramInt)
  {
    Object localObject2 = this.mChart.getTransformer(paramIBarDataSet.getAxisDependency());
    this.mBarBorderPaint.setColor(paramIBarDataSet.getBarBorderColor());
    this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(paramIBarDataSet.getBarBorderWidth()));
    float f1 = paramIBarDataSet.getBarBorderWidth();
    int k = 0;
    int m = 1;
    int i;
    if (f1 > 0.0F) {
      i = 1;
    } else {
      i = 0;
    }
    float f3 = this.mAnimator.getPhaseX();
    float f4 = this.mAnimator.getPhaseY();
    float f2;
    int n;
    if (this.mChart.isDrawBarShadowEnabled())
    {
      this.mShadowPaint.setColor(paramIBarDataSet.getBarShadowColor());
      f2 = this.mChart.getBarData().getBarWidth() / 2.0F;
      n = Math.min((int)Math.ceil(paramIBarDataSet.getEntryCount() * f3), paramIBarDataSet.getEntryCount());
      for (j = 0; j < n; j++)
      {
        f1 = ((BarEntry)paramIBarDataSet.getEntryForIndex(j)).getX();
        localObject1 = this.mBarShadowRectBuffer;
        ((RectF)localObject1).left = (f1 - f2);
        ((RectF)localObject1).right = (f1 + f2);
        ((Transformer)localObject2).rectValueToPixel((RectF)localObject1);
        if (this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right))
        {
          if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left)) {
            break;
          }
          this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
          this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();
          paramCanvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
        }
      }
    }
    Object localObject1 = this.mBarBuffers[paramInt];
    ((BarBuffer)localObject1).setPhases(f3, f4);
    ((BarBuffer)localObject1).setDataSet(paramInt);
    ((BarBuffer)localObject1).setInverted(this.mChart.isInverted(paramIBarDataSet.getAxisDependency()));
    ((BarBuffer)localObject1).setBarWidth(this.mChart.getBarData().getBarWidth());
    ((BarBuffer)localObject1).feed(paramIBarDataSet);
    ((Transformer)localObject2).pointValuesToPixel(((BarBuffer)localObject1).buffer);
    if (paramIBarDataSet.getColors().size() == 1) {
      paramInt = m;
    } else {
      paramInt = 0;
    }
    int j = k;
    if (paramInt != 0) {
      this.mRenderPaint.setColor(paramIBarDataSet.getColor());
    }
    for (j = k; j < ((BarBuffer)localObject1).size(); j += 4)
    {
      ViewPortHandler localViewPortHandler = this.mViewPortHandler;
      localObject2 = ((BarBuffer)localObject1).buffer;
      k = j + 2;
      if (localViewPortHandler.isInBoundsLeft(localObject2[k]))
      {
        if (!this.mViewPortHandler.isInBoundsRight(localObject1.buffer[j])) {
          break;
        }
        if (paramInt == 0) {
          this.mRenderPaint.setColor(paramIBarDataSet.getColor(j / 4));
        }
        if (paramIBarDataSet.getGradientColor() != null)
        {
          localObject2 = paramIBarDataSet.getGradientColor();
          this.mRenderPaint.setShader(new LinearGradient(localObject1.buffer[j], localObject1.buffer[(j + 3)], localObject1.buffer[j], localObject1.buffer[(j + 1)], ((GradientColor)localObject2).getStartColor(), ((GradientColor)localObject2).getEndColor(), Shader.TileMode.MIRROR));
        }
        if (paramIBarDataSet.getGradientColors() != null)
        {
          localObject2 = this.mRenderPaint;
          f1 = localObject1.buffer[j];
          f2 = localObject1.buffer[(j + 3)];
          f3 = localObject1.buffer[j];
          f4 = localObject1.buffer[(j + 1)];
          m = j / 4;
          ((Paint)localObject2).setShader(new LinearGradient(f1, f2, f3, f4, paramIBarDataSet.getGradientColor(m).getStartColor(), paramIBarDataSet.getGradientColor(m).getEndColor(), Shader.TileMode.MIRROR));
        }
        f2 = localObject1.buffer[j];
        localObject2 = ((BarBuffer)localObject1).buffer;
        m = j + 1;
        f1 = localObject2[m];
        f3 = localObject1.buffer[k];
        localObject2 = ((BarBuffer)localObject1).buffer;
        n = j + 3;
        paramCanvas.drawRect(f2, f1, f3, localObject2[n], this.mRenderPaint);
        if (i != 0) {
          paramCanvas.drawRect(localObject1.buffer[j], localObject1.buffer[m], localObject1.buffer[k], localObject1.buffer[n], this.mBarBorderPaint);
        }
      }
    }
  }
  
  public void drawExtras(Canvas paramCanvas) {}
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    BarData localBarData = this.mChart.getBarData();
    int k = paramArrayOfHighlight.length;
    for (int i = 0; i < k; i++)
    {
      Highlight localHighlight = paramArrayOfHighlight[i];
      Object localObject = (IBarDataSet)localBarData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localObject != null) && (((IBarDataSet)localObject).isHighlightEnabled()))
      {
        BarEntry localBarEntry = (BarEntry)((IBarDataSet)localObject).getEntryForXValue(localHighlight.getX(), localHighlight.getY());
        if (isInBoundsX(localBarEntry, (IBarLineScatterCandleBubbleDataSet)localObject))
        {
          Transformer localTransformer = this.mChart.getTransformer(((IBarDataSet)localObject).getAxisDependency());
          this.mHighlightPaint.setColor(((IBarDataSet)localObject).getHighLightColor());
          this.mHighlightPaint.setAlpha(((IBarDataSet)localObject).getHighLightAlpha());
          int j;
          if ((localHighlight.getStackIndex() >= 0) && (localBarEntry.isStacked())) {
            j = 1;
          } else {
            j = 0;
          }
          float f1;
          float f2;
          if (j != 0)
          {
            if (this.mChart.isHighlightFullBarEnabled())
            {
              f1 = localBarEntry.getPositiveSum();
              f2 = -localBarEntry.getNegativeSum();
            }
            else
            {
              localObject = localBarEntry.getRanges()[localHighlight.getStackIndex()];
              f1 = ((Range)localObject).from;
              f2 = ((Range)localObject).to;
            }
          }
          else
          {
            f1 = localBarEntry.getY();
            f2 = 0.0F;
          }
          prepareBarHighlight(localBarEntry.getX(), f1, f2, localBarData.getBarWidth() / 2.0F, localTransformer);
          setHighlightDrawPos(localHighlight, this.mBarRect);
          paramCanvas.drawRect(this.mBarRect, this.mHighlightPaint);
        }
      }
    }
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    if (isDrawingValuesAllowed(this.mChart))
    {
      Object localObject1 = this.mChart.getBarData().getDataSets();
      float f3 = Utils.convertDpToPixel(4.5F);
      boolean bool1 = this.mChart.isDrawValueAboveBarEnabled();
      int k = 0;
      while (k < this.mChart.getBarData().getDataSetCount())
      {
        IBarDataSet localIBarDataSet = (IBarDataSet)((List)localObject1).get(k);
        float f1;
        if (!shouldDrawValues(localIBarDataSet))
        {
          f1 = f3;
        }
        else
        {
          applyValueTextStyle(localIBarDataSet);
          boolean bool2 = this.mChart.isInverted(localIBarDataSet.getAxisDependency());
          float f4 = Utils.calcTextHeight(this.mValuePaint, "8");
          if (bool1) {
            f1 = -f3;
          } else {
            f1 = f4 + f3;
          }
          float f2;
          if (bool1) {
            f2 = f4 + f3;
          } else {
            f2 = -f3;
          }
          if (bool2)
          {
            f1 = -f1;
            f2 = -f2;
            f1 -= f4;
            f2 -= f4;
          }
          Object localObject6 = this.mBarBuffers[k];
          float f11 = this.mAnimator.getPhaseY();
          Object localObject4 = MPPointF.getInstance(localIBarDataSet.getIconsOffset());
          ((MPPointF)localObject4).x = Utils.convertDpToPixel(((MPPointF)localObject4).x);
          ((MPPointF)localObject4).y = Utils.convertDpToPixel(((MPPointF)localObject4).y);
          int i;
          Object localObject2;
          Object localObject3;
          float f5;
          Object localObject5;
          int j;
          int m;
          float f6;
          float f7;
          if (!localIBarDataSet.isStacked())
          {
            i = 0;
            localObject2 = localObject1;
            localObject3 = localObject6;
            localObject1 = localObject4;
            while (i < ((BarBuffer)localObject3).buffer.length * this.mAnimator.getPhaseX())
            {
              f5 = (localObject3.buffer[i] + localObject3.buffer[(i + 2)]) / 2.0F;
              if (!this.mViewPortHandler.isInBoundsRight(f5)) {
                break;
              }
              localObject5 = this.mViewPortHandler;
              localObject4 = ((BarBuffer)localObject3).buffer;
              j = i + 1;
              if ((((ViewPortHandler)localObject5).isInBoundsY(localObject4[j])) && (this.mViewPortHandler.isInBoundsLeft(f5)))
              {
                m = i / 4;
                localObject6 = (BarEntry)localIBarDataSet.getEntryForIndex(m);
                f6 = ((BarEntry)localObject6).getY();
                if (localIBarDataSet.isDrawValuesEnabled())
                {
                  localObject4 = localIBarDataSet.getValueFormatter();
                  if (f6 >= 0.0F) {
                    f4 = localObject3.buffer[j] + f1;
                  } else {
                    f4 = localObject3.buffer[(i + 3)] + f2;
                  }
                  drawValue(paramCanvas, (IValueFormatter)localObject4, f6, (Entry)localObject6, k, f5, f4, localIBarDataSet.getValueTextColor(m));
                }
                localObject4 = localObject1;
                localObject5 = localObject3;
                if ((((BarEntry)localObject6).getIcon() != null) && (localIBarDataSet.isDrawIconsEnabled()))
                {
                  localObject6 = ((BarEntry)localObject6).getIcon();
                  if (f6 >= 0.0F) {
                    f4 = localObject5.buffer[j] + f1;
                  } else {
                    f4 = localObject5.buffer[(i + 3)] + f2;
                  }
                  f6 = ((MPPointF)localObject4).x;
                  f7 = ((MPPointF)localObject4).y;
                  Utils.drawImage(paramCanvas, (Drawable)localObject6, (int)(f5 + f6), (int)(f4 + f7), ((Drawable)localObject6).getIntrinsicWidth(), ((Drawable)localObject6).getIntrinsicHeight());
                }
              }
              i += 4;
            }
            f1 = f3;
            localObject4 = localObject1;
            localObject1 = localObject2;
          }
          else
          {
            localObject2 = localObject4;
            localObject3 = localObject1;
            localObject1 = this.mChart.getTransformer(localIBarDataSet.getAxisDependency());
            i = 0;
            j = 0;
            while (i < localIBarDataSet.getEntryCount() * this.mAnimator.getPhaseX())
            {
              BarEntry localBarEntry = (BarEntry)localIBarDataSet.getEntryForIndex(i);
              localObject4 = localBarEntry.getYVals();
              f7 = (localObject6.buffer[j] + localObject6.buffer[(j + 2)]) / 2.0F;
              int i1 = localIBarDataSet.getValueTextColor(i);
              Object localObject7;
              float f8;
              if (localObject4 == null)
              {
                if (!this.mViewPortHandler.isInBoundsRight(f7))
                {
                  f1 = f3;
                  localObject4 = localObject2;
                  localObject1 = localObject3;
                  break label1444;
                }
                localObject5 = this.mViewPortHandler;
                localObject7 = ((BarBuffer)localObject6).buffer;
                m = j + 1;
                if ((((ViewPortHandler)localObject5).isInBoundsY(localObject7[m])) && (this.mViewPortHandler.isInBoundsLeft(f7)))
                {
                  if (localIBarDataSet.isDrawValuesEnabled())
                  {
                    localObject5 = localIBarDataSet.getValueFormatter();
                    f6 = localBarEntry.getY();
                    f5 = localObject6.buffer[m];
                    if (localBarEntry.getY() >= 0.0F) {
                      f4 = f1;
                    } else {
                      f4 = f2;
                    }
                    drawValue(paramCanvas, (IValueFormatter)localObject5, f6, localBarEntry, k, f7, f5 + f4, i1);
                  }
                  if ((localBarEntry.getIcon() == null) || (!localIBarDataSet.isDrawIconsEnabled())) {
                    break label1406;
                  }
                  localObject5 = localBarEntry.getIcon();
                  f5 = localObject6.buffer[m];
                  if (localBarEntry.getY() >= 0.0F) {
                    f4 = f1;
                  } else {
                    f4 = f2;
                  }
                  f8 = ((MPPointF)localObject2).x;
                  f6 = ((MPPointF)localObject2).y;
                  Utils.drawImage(paramCanvas, (Drawable)localObject5, (int)(f7 + f8), (int)(f5 + f4 + f6), ((Drawable)localObject5).getIntrinsicWidth(), ((Drawable)localObject5).getIntrinsicHeight());
                }
              }
              else
              {
                localObject7 = localObject4;
                localObject5 = new float[localObject7.length * 2];
                f4 = -localBarEntry.getNegativeSum();
                int n = 0;
                m = 0;
                f6 = 0.0F;
                while (n < localObject5.length)
                {
                  float f10 = localObject7[m];
                  float f9;
                  if (f10 == 0.0F)
                  {
                    f5 = f10;
                    f8 = f6;
                    f9 = f4;
                    if (f6 == 0.0F) {
                      break label1129;
                    }
                    if (f4 == 0.0F)
                    {
                      f5 = f10;
                      f8 = f6;
                      f9 = f4;
                      break label1129;
                    }
                  }
                  if (f10 >= 0.0F)
                  {
                    f5 = f6 + f10;
                    f8 = f5;
                    f9 = f4;
                  }
                  else
                  {
                    f9 = f4 - f10;
                    f8 = f6;
                    f5 = f4;
                  }
                  label1129:
                  localObject5[(n + 1)] = (f5 * f11);
                  n += 2;
                  m++;
                  f6 = f8;
                  f4 = f9;
                }
                ((Transformer)localObject1).pointValuesToPixel((float[])localObject5);
                for (m = 0; m < localObject5.length; m += 2)
                {
                  int i2 = m / 2;
                  f5 = localObject7[i2];
                  if (((f5 == 0.0F) && (f4 == 0.0F) && (f6 > 0.0F)) || (f5 < 0.0F)) {
                    n = 1;
                  } else {
                    n = 0;
                  }
                  f8 = localObject5[(m + 1)];
                  if (n != 0) {
                    f5 = f2;
                  } else {
                    f5 = f1;
                  }
                  f5 = f8 + f5;
                  if (!this.mViewPortHandler.isInBoundsRight(f7)) {
                    break;
                  }
                  if ((this.mViewPortHandler.isInBoundsY(f5)) && (this.mViewPortHandler.isInBoundsLeft(f7)))
                  {
                    if (localIBarDataSet.isDrawValuesEnabled()) {
                      drawValue(paramCanvas, localIBarDataSet.getValueFormatter(), localObject7[i2], localBarEntry, k, f7, f5, i1);
                    }
                    if ((localBarEntry.getIcon() != null) && (localIBarDataSet.isDrawIconsEnabled()))
                    {
                      Drawable localDrawable = localBarEntry.getIcon();
                      Utils.drawImage(paramCanvas, localDrawable, (int)(f7 + ((MPPointF)localObject2).x), (int)(f5 + ((MPPointF)localObject2).y), localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
                    }
                  }
                }
              }
              label1406:
              if (localObject4 == null) {
                j += 4;
              } else {
                j += localObject4.length * 4;
              }
              i++;
            }
            f1 = f3;
            localObject1 = localObject3;
            localObject4 = localObject2;
          }
          label1444:
          MPPointF.recycleInstance((MPPointF)localObject4);
        }
        k++;
        f3 = f1;
      }
    }
  }
  
  public void initBuffers()
  {
    BarData localBarData = this.mChart.getBarData();
    this.mBarBuffers = new BarBuffer[localBarData.getDataSetCount()];
    for (int i = 0; i < this.mBarBuffers.length; i++)
    {
      IBarDataSet localIBarDataSet = (IBarDataSet)localBarData.getDataSetByIndex(i);
      BarBuffer[] arrayOfBarBuffer = this.mBarBuffers;
      int k = localIBarDataSet.getEntryCount();
      int j;
      if (localIBarDataSet.isStacked()) {
        j = localIBarDataSet.getStackSize();
      } else {
        j = 1;
      }
      arrayOfBarBuffer[i] = new BarBuffer(k * 4 * j, localBarData.getDataSetCount(), localIBarDataSet.isStacked());
    }
  }
  
  protected void prepareBarHighlight(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Transformer paramTransformer)
  {
    this.mBarRect.set(paramFloat1 - paramFloat4, paramFloat2, paramFloat1 + paramFloat4, paramFloat3);
    paramTransformer.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
  }
  
  protected void setHighlightDrawPos(Highlight paramHighlight, RectF paramRectF)
  {
    paramHighlight.setDraw(paramRectF.centerX(), paramRectF.top);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/BarChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */