package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.BubbleData;
import com.github.mikephil.charting.data.BubbleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.BubbleDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBubbleDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.Iterator;
import java.util.List;

public class BubbleChartRenderer
  extends BarLineScatterCandleBubbleRenderer
{
  private float[] _hsvBuffer = new float[3];
  protected BubbleDataProvider mChart;
  private float[] pointBuffer = new float[2];
  private float[] sizeBuffer = new float[4];
  
  public BubbleChartRenderer(BubbleDataProvider paramBubbleDataProvider, ChartAnimator paramChartAnimator, ViewPortHandler paramViewPortHandler)
  {
    super(paramChartAnimator, paramViewPortHandler);
    this.mChart = paramBubbleDataProvider;
    this.mRenderPaint.setStyle(Paint.Style.FILL);
    this.mHighlightPaint.setStyle(Paint.Style.STROKE);
    this.mHighlightPaint.setStrokeWidth(Utils.convertDpToPixel(1.5F));
  }
  
  public void drawData(Canvas paramCanvas)
  {
    Iterator localIterator = this.mChart.getBubbleData().getDataSets().iterator();
    while (localIterator.hasNext())
    {
      IBubbleDataSet localIBubbleDataSet = (IBubbleDataSet)localIterator.next();
      if (localIBubbleDataSet.isVisible()) {
        drawDataSet(paramCanvas, localIBubbleDataSet);
      }
    }
  }
  
  protected void drawDataSet(Canvas paramCanvas, IBubbleDataSet paramIBubbleDataSet)
  {
    if (paramIBubbleDataSet.getEntryCount() < 1) {
      return;
    }
    Transformer localTransformer = this.mChart.getTransformer(paramIBubbleDataSet.getAxisDependency());
    float f1 = this.mAnimator.getPhaseY();
    this.mXBounds.set(this.mChart, paramIBubbleDataSet);
    Object localObject = this.sizeBuffer;
    localObject[0] = 0.0F;
    localObject[2] = 1.0F;
    localTransformer.pointValuesToPixel((float[])localObject);
    boolean bool = paramIBubbleDataSet.isNormalizeSizeEnabled();
    localObject = this.sizeBuffer;
    float f2 = Math.abs(localObject[2] - localObject[0]);
    float f3 = Math.min(Math.abs(this.mViewPortHandler.contentBottom() - this.mViewPortHandler.contentTop()), f2);
    for (int i = this.mXBounds.min; i <= this.mXBounds.range + this.mXBounds.min; i++)
    {
      localObject = (BubbleEntry)paramIBubbleDataSet.getEntryForIndex(i);
      this.pointBuffer[0] = ((BubbleEntry)localObject).getX();
      this.pointBuffer[1] = (((BubbleEntry)localObject).getY() * f1);
      localTransformer.pointValuesToPixel(this.pointBuffer);
      f2 = getShapeSize(((BubbleEntry)localObject).getSize(), paramIBubbleDataSet.getMaxSize(), f3, bool) / 2.0F;
      if ((this.mViewPortHandler.isInBoundsTop(this.pointBuffer[1] + f2)) && (this.mViewPortHandler.isInBoundsBottom(this.pointBuffer[1] - f2)) && (this.mViewPortHandler.isInBoundsLeft(this.pointBuffer[0] + f2)))
      {
        if (!this.mViewPortHandler.isInBoundsRight(this.pointBuffer[0] - f2)) {
          break;
        }
        int j = paramIBubbleDataSet.getColor((int)((BubbleEntry)localObject).getX());
        this.mRenderPaint.setColor(j);
        localObject = this.pointBuffer;
        paramCanvas.drawCircle(localObject[0], localObject[1], f2, this.mRenderPaint);
      }
    }
  }
  
  public void drawExtras(Canvas paramCanvas) {}
  
  public void drawHighlighted(Canvas paramCanvas, Highlight[] paramArrayOfHighlight)
  {
    BubbleData localBubbleData = this.mChart.getBubbleData();
    float f1 = this.mAnimator.getPhaseY();
    int j = paramArrayOfHighlight.length;
    for (int i = 0; i < j; i++)
    {
      Highlight localHighlight = paramArrayOfHighlight[i];
      Object localObject1 = (IBubbleDataSet)localBubbleData.getDataSetByIndex(localHighlight.getDataSetIndex());
      if ((localObject1 != null) && (((IBubbleDataSet)localObject1).isHighlightEnabled()))
      {
        Object localObject2 = (BubbleEntry)((IBubbleDataSet)localObject1).getEntryForXValue(localHighlight.getX(), localHighlight.getY());
        if ((((BubbleEntry)localObject2).getY() == localHighlight.getY()) && (isInBoundsX((Entry)localObject2, (IBarLineScatterCandleBubbleDataSet)localObject1)))
        {
          Object localObject3 = this.mChart.getTransformer(((IBubbleDataSet)localObject1).getAxisDependency());
          float[] arrayOfFloat = this.sizeBuffer;
          arrayOfFloat[0] = 0.0F;
          arrayOfFloat[2] = 1.0F;
          ((Transformer)localObject3).pointValuesToPixel(arrayOfFloat);
          boolean bool = ((IBubbleDataSet)localObject1).isNormalizeSizeEnabled();
          arrayOfFloat = this.sizeBuffer;
          float f2 = Math.abs(arrayOfFloat[2] - arrayOfFloat[0]);
          f2 = Math.min(Math.abs(this.mViewPortHandler.contentBottom() - this.mViewPortHandler.contentTop()), f2);
          this.pointBuffer[0] = ((BubbleEntry)localObject2).getX();
          this.pointBuffer[1] = (((BubbleEntry)localObject2).getY() * f1);
          ((Transformer)localObject3).pointValuesToPixel(this.pointBuffer);
          localObject3 = this.pointBuffer;
          localHighlight.setDraw(localObject3[0], localObject3[1]);
          f2 = getShapeSize(((BubbleEntry)localObject2).getSize(), ((IBubbleDataSet)localObject1).getMaxSize(), f2, bool) / 2.0F;
          if (this.mViewPortHandler.isInBoundsTop(this.pointBuffer[1] + f2))
          {
            if ((this.mViewPortHandler.isInBoundsBottom(this.pointBuffer[1] - f2)) && (this.mViewPortHandler.isInBoundsLeft(this.pointBuffer[0] + f2)))
            {
              if (!this.mViewPortHandler.isInBoundsRight(this.pointBuffer[0] - f2)) {
                break;
              }
              int k = ((IBubbleDataSet)localObject1).getColor((int)((BubbleEntry)localObject2).getX());
              Color.RGBToHSV(Color.red(k), Color.green(k), Color.blue(k), this._hsvBuffer);
              localObject2 = this._hsvBuffer;
              localObject2[2] *= 0.5F;
              k = Color.HSVToColor(Color.alpha(k), this._hsvBuffer);
              this.mHighlightPaint.setColor(k);
              this.mHighlightPaint.setStrokeWidth(((IBubbleDataSet)localObject1).getHighlightCircleWidth());
              localObject1 = this.pointBuffer;
              paramCanvas.drawCircle(localObject1[0], localObject1[1], f2, this.mHighlightPaint);
            }
          }
          else {}
        }
      }
    }
  }
  
  public void drawValues(Canvas paramCanvas)
  {
    Object localObject1 = this.mChart.getBubbleData();
    if (localObject1 == null) {
      return;
    }
    if (isDrawingValuesAllowed(this.mChart))
    {
      List localList = ((BubbleData)localObject1).getDataSets();
      float f3 = Utils.calcTextHeight(this.mValuePaint, "1");
      for (int i = 0; i < localList.size(); i++)
      {
        IBubbleDataSet localIBubbleDataSet = (IBubbleDataSet)localList.get(i);
        if ((shouldDrawValues(localIBubbleDataSet)) && (localIBubbleDataSet.getEntryCount() >= 1))
        {
          applyValueTextStyle(localIBubbleDataSet);
          float f2 = Math.max(0.0F, Math.min(1.0F, this.mAnimator.getPhaseX()));
          float f1 = this.mAnimator.getPhaseY();
          this.mXBounds.set(this.mChart, localIBubbleDataSet);
          float[] arrayOfFloat = this.mChart.getTransformer(localIBubbleDataSet.getAxisDependency()).generateTransformedValuesBubble(localIBubbleDataSet, f1, this.mXBounds.min, this.mXBounds.max);
          if (f2 != 1.0F) {
            f1 = f2;
          }
          localObject1 = MPPointF.getInstance(localIBubbleDataSet.getIconsOffset());
          ((MPPointF)localObject1).x = Utils.convertDpToPixel(((MPPointF)localObject1).x);
          ((MPPointF)localObject1).y = Utils.convertDpToPixel(((MPPointF)localObject1).y);
          for (int j = 0; j < arrayOfFloat.length; j += 2)
          {
            int k = j / 2;
            int m = localIBubbleDataSet.getValueTextColor(this.mXBounds.min + k);
            m = Color.argb(Math.round(255.0F * f1), Color.red(m), Color.green(m), Color.blue(m));
            float f4 = arrayOfFloat[j];
            f2 = arrayOfFloat[(j + 1)];
            if (!this.mViewPortHandler.isInBoundsRight(f4)) {
              break;
            }
            if ((this.mViewPortHandler.isInBoundsLeft(f4)) && (this.mViewPortHandler.isInBoundsY(f2)))
            {
              Object localObject3 = (BubbleEntry)localIBubbleDataSet.getEntryForIndex(k + this.mXBounds.min);
              if (localIBubbleDataSet.isDrawValuesEnabled()) {
                drawValue(paramCanvas, localIBubbleDataSet.getValueFormatter(), ((BubbleEntry)localObject3).getSize(), (Entry)localObject3, i, f4, f2 + 0.5F * f3, m);
              }
              Object localObject2 = localObject1;
              if ((((BubbleEntry)localObject3).getIcon() != null) && (localIBubbleDataSet.isDrawIconsEnabled()))
              {
                localObject3 = ((BubbleEntry)localObject3).getIcon();
                Utils.drawImage(paramCanvas, (Drawable)localObject3, (int)(f4 + ((MPPointF)localObject2).x), (int)(f2 + ((MPPointF)localObject2).y), ((Drawable)localObject3).getIntrinsicWidth(), ((Drawable)localObject3).getIntrinsicHeight());
              }
            }
          }
          MPPointF.recycleInstance((MPPointF)localObject1);
        }
      }
    }
  }
  
  protected float getShapeSize(float paramFloat1, float paramFloat2, float paramFloat3, boolean paramBoolean)
  {
    float f = paramFloat1;
    if (paramBoolean) {
      if (paramFloat2 == 0.0F) {
        f = 1.0F;
      } else {
        f = (float)Math.sqrt(paramFloat1 / paramFloat2);
      }
    }
    return paramFloat3 * f;
  }
  
  public void initBuffers() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/BubbleChartRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */