package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendDirection;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment;
import com.github.mikephil.charting.components.Legend.LegendOrientation;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.FSize;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LegendRenderer
  extends Renderer
{
  protected List<LegendEntry> computedEntries = new ArrayList(16);
  protected Paint.FontMetrics legendFontMetrics = new Paint.FontMetrics();
  protected Legend mLegend;
  protected Paint mLegendFormPaint;
  protected Paint mLegendLabelPaint;
  private Path mLineFormPath = new Path();
  
  public LegendRenderer(ViewPortHandler paramViewPortHandler, Legend paramLegend)
  {
    super(paramViewPortHandler);
    this.mLegend = paramLegend;
    this.mLegendLabelPaint = new Paint(1);
    this.mLegendLabelPaint.setTextSize(Utils.convertDpToPixel(9.0F));
    this.mLegendLabelPaint.setTextAlign(Paint.Align.LEFT);
    this.mLegendFormPaint = new Paint(1);
    this.mLegendFormPaint.setStyle(Paint.Style.FILL);
  }
  
  public void computeLegend(ChartData<?> paramChartData)
  {
    Object localObject = paramChartData;
    if (!this.mLegend.isLegendCustom())
    {
      this.computedEntries.clear();
      for (int i = 0; i < paramChartData.getDataSetCount(); i++)
      {
        IDataSet localIDataSet = ((ChartData)localObject).getDataSetByIndex(i);
        List localList = localIDataSet.getColors();
        int k = localIDataSet.getEntryCount();
        int j;
        if ((localIDataSet instanceof IBarDataSet))
        {
          IBarDataSet localIBarDataSet = (IBarDataSet)localIDataSet;
          if (localIBarDataSet.isStacked())
          {
            String[] arrayOfString = localIBarDataSet.getStackLabels();
            for (j = 0; (j < localList.size()) && (j < localIBarDataSet.getStackSize()); j++) {
              this.computedEntries.add(new LegendEntry(arrayOfString[(j % arrayOfString.length)], localIDataSet.getForm(), localIDataSet.getFormSize(), localIDataSet.getFormLineWidth(), localIDataSet.getFormLineDashEffect(), ((Integer)localList.get(j)).intValue()));
            }
            if (localIBarDataSet.getLabel() != null) {
              this.computedEntries.add(new LegendEntry(localIDataSet.getLabel(), Legend.LegendForm.NONE, NaN.0F, NaN.0F, null, 1122867));
            }
            continue;
          }
        }
        if ((localIDataSet instanceof IPieDataSet))
        {
          localObject = (IPieDataSet)localIDataSet;
          for (j = 0; (j < localList.size()) && (j < k); j++) {
            this.computedEntries.add(new LegendEntry(((PieEntry)((IPieDataSet)localObject).getEntryForIndex(j)).getLabel(), localIDataSet.getForm(), localIDataSet.getFormSize(), localIDataSet.getFormLineWidth(), localIDataSet.getFormLineDashEffect(), ((Integer)localList.get(j)).intValue()));
          }
          if (((IPieDataSet)localObject).getLabel() != null) {
            this.computedEntries.add(new LegendEntry(localIDataSet.getLabel(), Legend.LegendForm.NONE, NaN.0F, NaN.0F, null, 1122867));
          }
          localObject = paramChartData;
        }
        else
        {
          if ((localIDataSet instanceof ICandleDataSet))
          {
            localObject = (ICandleDataSet)localIDataSet;
            if (((ICandleDataSet)localObject).getDecreasingColor() != 1122867)
            {
              k = ((ICandleDataSet)localObject).getDecreasingColor();
              j = ((ICandleDataSet)localObject).getIncreasingColor();
              this.computedEntries.add(new LegendEntry(null, localIDataSet.getForm(), localIDataSet.getFormSize(), localIDataSet.getFormLineWidth(), localIDataSet.getFormLineDashEffect(), k));
              this.computedEntries.add(new LegendEntry(localIDataSet.getLabel(), localIDataSet.getForm(), localIDataSet.getFormSize(), localIDataSet.getFormLineWidth(), localIDataSet.getFormLineDashEffect(), j));
              localObject = paramChartData;
              continue;
            }
          }
          for (j = 0; (j < localList.size()) && (j < k); j++)
          {
            if ((j < localList.size() - 1) && (j < k - 1)) {
              localObject = null;
            } else {
              localObject = paramChartData.getDataSetByIndex(i).getLabel();
            }
            this.computedEntries.add(new LegendEntry((String)localObject, localIDataSet.getForm(), localIDataSet.getFormSize(), localIDataSet.getFormLineWidth(), localIDataSet.getFormLineDashEffect(), ((Integer)localList.get(j)).intValue()));
          }
          localObject = paramChartData;
        }
      }
      if (this.mLegend.getExtraEntries() != null) {
        Collections.addAll(this.computedEntries, this.mLegend.getExtraEntries());
      }
      this.mLegend.setEntries(this.computedEntries);
    }
    paramChartData = this.mLegend.getTypeface();
    if (paramChartData != null) {
      this.mLegendLabelPaint.setTypeface(paramChartData);
    }
    this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
    this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
    this.mLegend.calculateDimensions(this.mLegendLabelPaint, this.mViewPortHandler);
  }
  
  protected void drawForm(Canvas paramCanvas, float paramFloat1, float paramFloat2, LegendEntry paramLegendEntry, Legend paramLegend)
  {
    if ((paramLegendEntry.formColor != 1122868) && (paramLegendEntry.formColor != 1122867) && (paramLegendEntry.formColor != 0))
    {
      int i = paramCanvas.save();
      Legend.LegendForm localLegendForm2 = paramLegendEntry.form;
      Legend.LegendForm localLegendForm1 = localLegendForm2;
      if (localLegendForm2 == Legend.LegendForm.DEFAULT) {
        localLegendForm1 = paramLegend.getForm();
      }
      this.mLegendFormPaint.setColor(paramLegendEntry.formColor);
      if (Float.isNaN(paramLegendEntry.formSize)) {
        f1 = paramLegend.getFormSize();
      } else {
        f1 = paramLegendEntry.formSize;
      }
      float f2 = Utils.convertDpToPixel(f1);
      float f1 = f2 / 2.0F;
      switch (localLegendForm1)
      {
      default: 
        break;
      case ???: 
        if (Float.isNaN(paramLegendEntry.formLineWidth)) {
          f1 = paramLegend.getFormLineWidth();
        } else {
          f1 = paramLegendEntry.formLineWidth;
        }
        f1 = Utils.convertDpToPixel(f1);
        if (paramLegendEntry.formLineDashEffect == null) {
          paramLegendEntry = paramLegend.getFormLineDashEffect();
        } else {
          paramLegendEntry = paramLegendEntry.formLineDashEffect;
        }
        this.mLegendFormPaint.setStyle(Paint.Style.STROKE);
        this.mLegendFormPaint.setStrokeWidth(f1);
        this.mLegendFormPaint.setPathEffect(paramLegendEntry);
        this.mLineFormPath.reset();
        this.mLineFormPath.moveTo(paramFloat1, paramFloat2);
        this.mLineFormPath.lineTo(paramFloat1 + f2, paramFloat2);
        paramCanvas.drawPath(this.mLineFormPath, this.mLegendFormPaint);
        break;
      case ???: 
        this.mLegendFormPaint.setStyle(Paint.Style.FILL);
        paramCanvas.drawRect(paramFloat1, paramFloat2 - f1, paramFloat1 + f2, paramFloat2 + f1, this.mLegendFormPaint);
        break;
      case ???: 
      case ???: 
        this.mLegendFormPaint.setStyle(Paint.Style.FILL);
        paramCanvas.drawCircle(paramFloat1 + f1, paramFloat2, f1, this.mLegendFormPaint);
      }
      paramCanvas.restoreToCount(i);
      return;
    }
  }
  
  protected void drawLabel(Canvas paramCanvas, float paramFloat1, float paramFloat2, String paramString)
  {
    paramCanvas.drawText(paramString, paramFloat1, paramFloat2, this.mLegendLabelPaint);
  }
  
  public Paint getFormPaint()
  {
    return this.mLegendFormPaint;
  }
  
  public Paint getLabelPaint()
  {
    return this.mLegendLabelPaint;
  }
  
  public void renderLegend(Canvas paramCanvas)
  {
    if (!this.mLegend.isEnabled()) {
      return;
    }
    Object localObject1 = this.mLegend.getTypeface();
    if (localObject1 != null) {
      this.mLegendLabelPaint.setTypeface((Typeface)localObject1);
    }
    this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
    this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
    float f10 = Utils.getLineHeight(this.mLegendLabelPaint, this.legendFontMetrics);
    float f11 = Utils.getLineSpacing(this.mLegendLabelPaint, this.legendFontMetrics) + Utils.convertDpToPixel(this.mLegend.getYEntrySpace());
    float f9 = f10 - Utils.calcTextHeight(this.mLegendLabelPaint, "ABC") / 2.0F;
    LegendEntry[] arrayOfLegendEntry = this.mLegend.getEntries();
    float f6 = Utils.convertDpToPixel(this.mLegend.getFormToTextSpace());
    float f12 = Utils.convertDpToPixel(this.mLegend.getXEntrySpace());
    localObject1 = this.mLegend.getOrientation();
    Legend.LegendHorizontalAlignment localLegendHorizontalAlignment = this.mLegend.getHorizontalAlignment();
    Object localObject3 = this.mLegend.getVerticalAlignment();
    Legend.LegendDirection localLegendDirection = this.mLegend.getDirection();
    float f7 = Utils.convertDpToPixel(this.mLegend.getFormSize());
    float f5 = Utils.convertDpToPixel(this.mLegend.getStackSpace());
    float f4 = this.mLegend.getYOffset();
    float f1 = this.mLegend.getXOffset();
    float f2;
    switch (1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendHorizontalAlignment[localLegendHorizontalAlignment.ordinal()])
    {
    default: 
      f1 = 0.0F;
      break;
    case 3: 
      if (localObject1 == Legend.LegendOrientation.VERTICAL) {
        f2 = this.mViewPortHandler.getChartWidth() / 2.0F;
      } else {
        f2 = this.mViewPortHandler.contentLeft() + this.mViewPortHandler.contentWidth() / 2.0F;
      }
      if (localLegendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
        f3 = f1;
      } else {
        f3 = -f1;
      }
      f2 = f3 + f2;
      if (localObject1 == Legend.LegendOrientation.VERTICAL)
      {
        double d2 = f2;
        double d1;
        double d3;
        if (localLegendDirection == Legend.LegendDirection.LEFT_TO_RIGHT)
        {
          d1 = -this.mLegend.mNeededWidth;
          Double.isNaN(d1);
          d1 /= 2.0D;
          d3 = f1;
          Double.isNaN(d3);
          d1 += d3;
        }
        else
        {
          d1 = this.mLegend.mNeededWidth;
          Double.isNaN(d1);
          d1 /= 2.0D;
          d3 = f1;
          Double.isNaN(d3);
          d1 -= d3;
        }
        Double.isNaN(d2);
        f1 = (float)(d2 + d1);
      }
      else
      {
        f1 = f2;
      }
      break;
    case 2: 
      if (localObject1 == Legend.LegendOrientation.VERTICAL) {
        f1 = this.mViewPortHandler.getChartWidth() - f1;
      } else {
        f1 = this.mViewPortHandler.contentRight() - f1;
      }
      if (localLegendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
        f1 -= this.mLegend.mNeededWidth;
      }
      break;
    case 1: 
      if (localObject1 != Legend.LegendOrientation.VERTICAL) {
        f1 += this.mViewPortHandler.contentLeft();
      }
      if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
        f1 += this.mLegend.mNeededWidth;
      }
      break;
    }
    float f3 = f5;
    int i;
    int j;
    switch (1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendOrientation[localObject1.ordinal()])
    {
    default: 
      break;
    case 2: 
      switch (1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[localObject3.ordinal()])
      {
      default: 
        f2 = 0.0F;
        break;
      case 3: 
        f2 = this.mViewPortHandler.getChartHeight() / 2.0F - this.mLegend.mNeededHeight / 2.0F + this.mLegend.getYOffset();
        break;
      case 2: 
        if (localLegendHorizontalAlignment == Legend.LegendHorizontalAlignment.CENTER) {
          f2 = this.mViewPortHandler.getChartHeight();
        } else {
          f2 = this.mViewPortHandler.contentBottom();
        }
        f2 -= this.mLegend.mNeededHeight + f4;
        break;
      case 1: 
        if (localLegendHorizontalAlignment == Legend.LegendHorizontalAlignment.CENTER) {
          f2 = 0.0F;
        } else {
          f2 = this.mViewPortHandler.contentTop();
        }
        f2 += f4;
      }
      i = 0;
      f5 = 0.0F;
      j = 0;
      f4 = f3;
      f3 = f2;
      localObject1 = localLegendDirection;
    case 1: 
      while (i < arrayOfLegendEntry.length)
      {
        Object localObject2 = arrayOfLegendEntry[i];
        if (((LegendEntry)localObject2).form != Legend.LegendForm.NONE) {
          k = 1;
        } else {
          k = 0;
        }
        if (Float.isNaN(((LegendEntry)localObject2).formSize)) {
          f8 = f7;
        } else {
          f8 = Utils.convertDpToPixel(((LegendEntry)localObject2).formSize);
        }
        if (k != 0)
        {
          if (localObject1 == Legend.LegendDirection.LEFT_TO_RIGHT) {
            f2 = f1 + f5;
          } else {
            f2 = f1 - (f8 - f5);
          }
          drawForm(paramCanvas, f2, f3 + f9, (LegendEntry)localObject2, this.mLegend);
          if (localObject1 == Legend.LegendDirection.LEFT_TO_RIGHT) {
            f2 += f8;
          }
        }
        else
        {
          f2 = f1;
        }
        if (((LegendEntry)localObject2).label != null)
        {
          if ((k != 0) && (j == 0))
          {
            if (localObject1 == Legend.LegendDirection.LEFT_TO_RIGHT) {
              f5 = f6;
            } else {
              f5 = -f6;
            }
            f2 += f5;
          }
          else if (j != 0)
          {
            f2 = f1;
          }
          f5 = f2;
          if (localObject1 == Legend.LegendDirection.RIGHT_TO_LEFT) {
            f5 = f2 - Utils.calcTextWidth(this.mLegendLabelPaint, ((LegendEntry)localObject2).label);
          }
          if (j == 0)
          {
            drawLabel(paramCanvas, f5, f3 + f10, ((LegendEntry)localObject2).label);
          }
          else
          {
            f3 += f10 + f11;
            drawLabel(paramCanvas, f5, f3 + f10, ((LegendEntry)localObject2).label);
          }
          f3 += f10 + f11;
          f2 = 0.0F;
        }
        else
        {
          f2 = f5 + (f8 + f4);
          j = 1;
        }
        i++;
        f5 = f2;
        continue;
        localObject2 = this.mLegend.getCalculatedLineSizes();
        localObject1 = this.mLegend.getCalculatedLabelSizes();
        List localList = this.mLegend.getCalculatedLabelBreakPoints();
        f2 = f4;
        switch (1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[localObject3.ordinal()])
        {
        default: 
          f2 = 0.0F;
          break;
        case 3: 
          f2 = f4 + (this.mViewPortHandler.getChartHeight() - this.mLegend.mNeededHeight) / 2.0F;
          break;
        case 2: 
          f2 = this.mViewPortHandler.getChartHeight() - f4 - this.mLegend.mNeededHeight;
        }
        int k = arrayOfLegendEntry.length;
        f4 = f2;
        f5 = f1;
        i = 0;
        j = 0;
        float f8 = f9;
        f2 = f1;
        for (f1 = f4; i < k; f1 = f4)
        {
          localObject3 = arrayOfLegendEntry[i];
          int m;
          if (((LegendEntry)localObject3).form != Legend.LegendForm.NONE) {
            m = 1;
          } else {
            m = 0;
          }
          if (Float.isNaN(((LegendEntry)localObject3).formSize)) {
            f9 = f7;
          } else {
            f9 = Utils.convertDpToPixel(((LegendEntry)localObject3).formSize);
          }
          if ((i < localList.size()) && (((Boolean)localList.get(i)).booleanValue()))
          {
            f4 = f1 + (f10 + f11);
            f1 = f2;
          }
          else
          {
            f4 = f1;
            f1 = f5;
          }
          if ((f1 == f2) && (localLegendHorizontalAlignment == Legend.LegendHorizontalAlignment.CENTER)) {
            if (j < ((List)localObject2).size())
            {
              if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
                f5 = ((FSize)((List)localObject2).get(j)).width;
              } else {
                f5 = -((FSize)((List)localObject2).get(j)).width;
              }
              f1 += f5 / 2.0F;
              j++;
            }
            else {}
          }
          int n;
          if (((LegendEntry)localObject3).label == null) {
            n = 1;
          } else {
            n = 0;
          }
          if (m != 0)
          {
            f5 = f1;
            if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
              f5 = f1 - f9;
            }
            drawForm(paramCanvas, f5, f4 + f8, (LegendEntry)localObject3, this.mLegend);
            f1 = f5;
            if (localLegendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
              f1 = f5 + f9;
            }
          }
          if (n == 0)
          {
            f5 = f1;
            if (m != 0)
            {
              if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
                f5 = -f6;
              } else {
                f5 = f6;
              }
              f5 = f1 + f5;
            }
            if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
              f1 = f5 - ((FSize)((List)localObject1).get(i)).width;
            } else {
              f1 = f5;
            }
            drawLabel(paramCanvas, f1, f4 + f10, ((LegendEntry)localObject3).label);
            f5 = f1;
            if (localLegendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
              f5 = f1 + ((FSize)((List)localObject1).get(i)).width;
            }
            if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
              f1 = -f12;
            } else {
              f1 = f12;
            }
            f5 += f1;
          }
          else
          {
            if (localLegendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
              f5 = -f3;
            } else {
              f5 = f3;
            }
            f5 = f1 + f5;
          }
          i++;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/renderer/LegendRenderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */