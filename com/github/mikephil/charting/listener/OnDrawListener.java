package com.github.mikephil.charting.listener;

import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;

public abstract interface OnDrawListener
{
  public abstract void onDrawFinished(DataSet<?> paramDataSet);
  
  public abstract void onEntryAdded(Entry paramEntry);
  
  public abstract void onEntryMoved(Entry paramEntry);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/listener/OnDrawListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */