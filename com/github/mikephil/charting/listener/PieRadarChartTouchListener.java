package com.github.mikephil.charting.listener;

import android.annotation.SuppressLint;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.github.mikephil.charting.charts.PieRadarChartBase;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

public class PieRadarChartTouchListener
  extends ChartTouchListener<PieRadarChartBase<?>>
{
  private ArrayList<AngularVelocitySample> _velocitySamples = new ArrayList();
  private float mDecelerationAngularVelocity = 0.0F;
  private long mDecelerationLastTime = 0L;
  private float mStartAngle = 0.0F;
  private MPPointF mTouchStartPoint = MPPointF.getInstance(0.0F, 0.0F);
  
  public PieRadarChartTouchListener(PieRadarChartBase<?> paramPieRadarChartBase)
  {
    super(paramPieRadarChartBase);
  }
  
  private float calculateVelocity()
  {
    if (this._velocitySamples.isEmpty()) {
      return 0.0F;
    }
    Object localObject = this._velocitySamples;
    int j = 0;
    AngularVelocitySample localAngularVelocitySample1 = (AngularVelocitySample)((ArrayList)localObject).get(0);
    localObject = this._velocitySamples;
    AngularVelocitySample localAngularVelocitySample2 = (AngularVelocitySample)((ArrayList)localObject).get(((ArrayList)localObject).size() - 1);
    int i = this._velocitySamples.size() - 1;
    localObject = localAngularVelocitySample1;
    while (i >= 0)
    {
      localObject = (AngularVelocitySample)this._velocitySamples.get(i);
      if (((AngularVelocitySample)localObject).angle != localAngularVelocitySample2.angle) {
        break;
      }
      i--;
    }
    float f2 = (float)(localAngularVelocitySample2.time - localAngularVelocitySample1.time) / 1000.0F;
    float f1 = f2;
    if (f2 == 0.0F) {
      f1 = 0.1F;
    }
    i = j;
    if (localAngularVelocitySample2.angle >= ((AngularVelocitySample)localObject).angle) {
      i = 1;
    }
    j = i;
    if (Math.abs(localAngularVelocitySample2.angle - ((AngularVelocitySample)localObject).angle) > 270.0D) {
      j = i ^ 0x1;
    }
    double d;
    if (localAngularVelocitySample2.angle - localAngularVelocitySample1.angle > 180.0D)
    {
      d = localAngularVelocitySample1.angle;
      Double.isNaN(d);
      localAngularVelocitySample1.angle = ((float)(d + 360.0D));
    }
    else if (localAngularVelocitySample1.angle - localAngularVelocitySample2.angle > 180.0D)
    {
      d = localAngularVelocitySample2.angle;
      Double.isNaN(d);
      localAngularVelocitySample2.angle = ((float)(d + 360.0D));
    }
    f2 = Math.abs((localAngularVelocitySample2.angle - localAngularVelocitySample1.angle) / f1);
    f1 = f2;
    if (j == 0) {
      f1 = -f2;
    }
    return f1;
  }
  
  private void resetVelocity()
  {
    this._velocitySamples.clear();
  }
  
  private void sampleVelocity(float paramFloat1, float paramFloat2)
  {
    long l = AnimationUtils.currentAnimationTimeMillis();
    this._velocitySamples.add(new AngularVelocitySample(l, ((PieRadarChartBase)this.mChart).getAngleForPoint(paramFloat1, paramFloat2)));
    for (int i = this._velocitySamples.size(); (i - 2 > 0) && (l - ((AngularVelocitySample)this._velocitySamples.get(0)).time > 1000L); i--) {
      this._velocitySamples.remove(0);
    }
  }
  
  public void computeScroll()
  {
    if (this.mDecelerationAngularVelocity == 0.0F) {
      return;
    }
    long l = AnimationUtils.currentAnimationTimeMillis();
    this.mDecelerationAngularVelocity *= ((PieRadarChartBase)this.mChart).getDragDecelerationFrictionCoef();
    float f = (float)(l - this.mDecelerationLastTime) / 1000.0F;
    ((PieRadarChartBase)this.mChart).setRotationAngle(((PieRadarChartBase)this.mChart).getRotationAngle() + this.mDecelerationAngularVelocity * f);
    this.mDecelerationLastTime = l;
    if (Math.abs(this.mDecelerationAngularVelocity) >= 0.001D) {
      Utils.postInvalidateOnAnimation(this.mChart);
    } else {
      stopDeceleration();
    }
  }
  
  public void onLongPress(MotionEvent paramMotionEvent)
  {
    this.mLastGesture = ChartTouchListener.ChartGesture.LONG_PRESS;
    OnChartGestureListener localOnChartGestureListener = ((PieRadarChartBase)this.mChart).getOnChartGestureListener();
    if (localOnChartGestureListener != null) {
      localOnChartGestureListener.onChartLongPressed(paramMotionEvent);
    }
  }
  
  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    return true;
  }
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    this.mLastGesture = ChartTouchListener.ChartGesture.SINGLE_TAP;
    OnChartGestureListener localOnChartGestureListener = ((PieRadarChartBase)this.mChart).getOnChartGestureListener();
    if (localOnChartGestureListener != null) {
      localOnChartGestureListener.onChartSingleTapped(paramMotionEvent);
    }
    if (!((PieRadarChartBase)this.mChart).isHighlightPerTapEnabled()) {
      return false;
    }
    performHighlight(((PieRadarChartBase)this.mChart).getHighlightByTouchPoint(paramMotionEvent.getX(), paramMotionEvent.getY()), paramMotionEvent);
    return true;
  }
  
  @SuppressLint({"ClickableViewAccessibility"})
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (this.mGestureDetector.onTouchEvent(paramMotionEvent)) {
      return true;
    }
    if (((PieRadarChartBase)this.mChart).isRotationEnabled())
    {
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      switch (paramMotionEvent.getAction())
      {
      default: 
        break;
      case 2: 
        if (((PieRadarChartBase)this.mChart).isDragDecelerationEnabled()) {
          sampleVelocity(f1, f2);
        }
        if ((this.mTouchMode == 0) && (distance(f1, this.mTouchStartPoint.x, f2, this.mTouchStartPoint.y) > Utils.convertDpToPixel(8.0F)))
        {
          this.mLastGesture = ChartTouchListener.ChartGesture.ROTATE;
          this.mTouchMode = 6;
          ((PieRadarChartBase)this.mChart).disableScroll();
        }
        else if (this.mTouchMode == 6)
        {
          updateGestureRotation(f1, f2);
          ((PieRadarChartBase)this.mChart).invalidate();
        }
        endAction(paramMotionEvent);
        break;
      case 1: 
        if (((PieRadarChartBase)this.mChart).isDragDecelerationEnabled())
        {
          stopDeceleration();
          sampleVelocity(f1, f2);
          this.mDecelerationAngularVelocity = calculateVelocity();
          if (this.mDecelerationAngularVelocity != 0.0F)
          {
            this.mDecelerationLastTime = AnimationUtils.currentAnimationTimeMillis();
            Utils.postInvalidateOnAnimation(this.mChart);
          }
        }
        ((PieRadarChartBase)this.mChart).enableScroll();
        this.mTouchMode = 0;
        endAction(paramMotionEvent);
        break;
      case 0: 
        startAction(paramMotionEvent);
        stopDeceleration();
        resetVelocity();
        if (((PieRadarChartBase)this.mChart).isDragDecelerationEnabled()) {
          sampleVelocity(f1, f2);
        }
        setGestureStartAngle(f1, f2);
        paramView = this.mTouchStartPoint;
        paramView.x = f1;
        paramView.y = f2;
      }
    }
    return true;
  }
  
  public void setGestureStartAngle(float paramFloat1, float paramFloat2)
  {
    this.mStartAngle = (((PieRadarChartBase)this.mChart).getAngleForPoint(paramFloat1, paramFloat2) - ((PieRadarChartBase)this.mChart).getRawRotationAngle());
  }
  
  public void stopDeceleration()
  {
    this.mDecelerationAngularVelocity = 0.0F;
  }
  
  public void updateGestureRotation(float paramFloat1, float paramFloat2)
  {
    ((PieRadarChartBase)this.mChart).setRotationAngle(((PieRadarChartBase)this.mChart).getAngleForPoint(paramFloat1, paramFloat2) - this.mStartAngle);
  }
  
  private class AngularVelocitySample
  {
    public float angle;
    public long time;
    
    public AngularVelocitySample(long paramLong, float paramFloat)
    {
      this.time = paramLong;
      this.angle = paramFloat;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/listener/PieRadarChartTouchListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */