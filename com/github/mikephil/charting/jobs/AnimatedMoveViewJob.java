package com.github.mikephil.charting.jobs;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.view.View;
import com.github.mikephil.charting.utils.ObjectPool;
import com.github.mikephil.charting.utils.ObjectPool.Poolable;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;

@SuppressLint({"NewApi"})
public class AnimatedMoveViewJob
  extends AnimatedViewPortJob
{
  private static ObjectPool<AnimatedMoveViewJob> pool = ObjectPool.create(4, new AnimatedMoveViewJob(null, 0.0F, 0.0F, null, null, 0.0F, 0.0F, 0L));
  
  static
  {
    pool.setReplenishPercentage(0.5F);
  }
  
  public AnimatedMoveViewJob(ViewPortHandler paramViewPortHandler, float paramFloat1, float paramFloat2, Transformer paramTransformer, View paramView, float paramFloat3, float paramFloat4, long paramLong)
  {
    super(paramViewPortHandler, paramFloat1, paramFloat2, paramTransformer, paramView, paramFloat3, paramFloat4, paramLong);
  }
  
  public static AnimatedMoveViewJob getInstance(ViewPortHandler paramViewPortHandler, float paramFloat1, float paramFloat2, Transformer paramTransformer, View paramView, float paramFloat3, float paramFloat4, long paramLong)
  {
    AnimatedMoveViewJob localAnimatedMoveViewJob = (AnimatedMoveViewJob)pool.get();
    localAnimatedMoveViewJob.mViewPortHandler = paramViewPortHandler;
    localAnimatedMoveViewJob.xValue = paramFloat1;
    localAnimatedMoveViewJob.yValue = paramFloat2;
    localAnimatedMoveViewJob.mTrans = paramTransformer;
    localAnimatedMoveViewJob.view = paramView;
    localAnimatedMoveViewJob.xOrigin = paramFloat3;
    localAnimatedMoveViewJob.yOrigin = paramFloat4;
    localAnimatedMoveViewJob.animator.setDuration(paramLong);
    return localAnimatedMoveViewJob;
  }
  
  public static void recycleInstance(AnimatedMoveViewJob paramAnimatedMoveViewJob)
  {
    pool.recycle(paramAnimatedMoveViewJob);
  }
  
  protected ObjectPool.Poolable instantiate()
  {
    return new AnimatedMoveViewJob(null, 0.0F, 0.0F, null, null, 0.0F, 0.0F, 0L);
  }
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    this.pts[0] = (this.xOrigin + (this.xValue - this.xOrigin) * this.phase);
    this.pts[1] = (this.yOrigin + (this.yValue - this.yOrigin) * this.phase);
    this.mTrans.pointValuesToPixel(this.pts);
    this.mViewPortHandler.centerViewPort(this.pts, this.view);
  }
  
  public void recycleSelf()
  {
    recycleInstance(this);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/jobs/AnimatedMoveViewJob.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */