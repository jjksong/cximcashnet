package com.github.mikephil.charting.jobs;

import android.view.View;
import com.github.mikephil.charting.utils.ObjectPool;
import com.github.mikephil.charting.utils.ObjectPool.Poolable;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class MoveViewJob
  extends ViewPortJob
{
  private static ObjectPool<MoveViewJob> pool = ObjectPool.create(2, new MoveViewJob(null, 0.0F, 0.0F, null, null));
  
  static
  {
    pool.setReplenishPercentage(0.5F);
  }
  
  public MoveViewJob(ViewPortHandler paramViewPortHandler, float paramFloat1, float paramFloat2, Transformer paramTransformer, View paramView)
  {
    super(paramViewPortHandler, paramFloat1, paramFloat2, paramTransformer, paramView);
  }
  
  public static MoveViewJob getInstance(ViewPortHandler paramViewPortHandler, float paramFloat1, float paramFloat2, Transformer paramTransformer, View paramView)
  {
    MoveViewJob localMoveViewJob = (MoveViewJob)pool.get();
    localMoveViewJob.mViewPortHandler = paramViewPortHandler;
    localMoveViewJob.xValue = paramFloat1;
    localMoveViewJob.yValue = paramFloat2;
    localMoveViewJob.mTrans = paramTransformer;
    localMoveViewJob.view = paramView;
    return localMoveViewJob;
  }
  
  public static void recycleInstance(MoveViewJob paramMoveViewJob)
  {
    pool.recycle(paramMoveViewJob);
  }
  
  protected ObjectPool.Poolable instantiate()
  {
    return new MoveViewJob(this.mViewPortHandler, this.xValue, this.yValue, this.mTrans, this.view);
  }
  
  public void run()
  {
    this.pts[0] = this.xValue;
    this.pts[1] = this.yValue;
    this.mTrans.pointValuesToPixel(this.pts);
    this.mViewPortHandler.centerViewPort(this.pts, this.view);
    recycleInstance(this);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/jobs/MoveViewJob.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */