package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class HorizontalBarBuffer
  extends BarBuffer
{
  public HorizontalBarBuffer(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(paramInt1, paramInt2, paramBoolean);
  }
  
  public void feed(IBarDataSet paramIBarDataSet)
  {
    float f7 = paramIBarDataSet.getEntryCount();
    float f6 = this.phaseX;
    float f8 = this.mBarWidth / 2.0F;
    for (int i = 0; i < f7 * f6; i++)
    {
      BarEntry localBarEntry = (BarEntry)paramIBarDataSet.getEntryForIndex(i);
      if (localBarEntry != null)
      {
        float f9 = localBarEntry.getX();
        float f1 = localBarEntry.getY();
        float[] arrayOfFloat = localBarEntry.getYVals();
        int j;
        float f4;
        if ((this.mContainsStacks) && (arrayOfFloat != null))
        {
          f1 = -localBarEntry.getNegativeSum();
          j = 0;
          f4 = 0.0F;
        }
        while (j < arrayOfFloat.length)
        {
          float f3 = arrayOfFloat[j];
          float f5;
          if (f3 >= 0.0F)
          {
            f2 = f3 + f4;
            f3 = f1;
            f1 = f4;
            f5 = f2;
          }
          else
          {
            f2 = Math.abs(f3);
            f3 = Math.abs(f3) + f1;
            f2 += f1;
            f5 = f4;
          }
          if (this.mInverted)
          {
            if (f1 >= f2) {
              f4 = f1;
            } else {
              f4 = f2;
            }
            if (f1 > f2) {
              f1 = f2;
            }
            f2 = f4;
            f4 = f1;
            f1 = f2;
          }
          else
          {
            if (f1 >= f2) {
              f4 = f1;
            } else {
              f4 = f2;
            }
            if (f1 > f2) {
              f1 = f2;
            }
          }
          float f2 = this.phaseY;
          addBar(f1 * this.phaseY, f9 + f8, f4 * f2, f9 - f8);
          j++;
          f1 = f3;
          f4 = f5;
          continue;
          if (this.mInverted)
          {
            if (f1 >= 0.0F) {
              f2 = f1;
            } else {
              f2 = 0.0F;
            }
            if (f1 > 0.0F) {
              f1 = 0.0F;
            }
            f3 = f2;
            f2 = f1;
            f1 = f3;
          }
          else
          {
            if (f1 >= 0.0F) {
              f2 = f1;
            } else {
              f2 = 0.0F;
            }
            if (f1 > 0.0F) {
              f1 = 0.0F;
            }
          }
          if (f2 > 0.0F) {
            f2 *= this.phaseY;
          } else {
            f1 *= this.phaseY;
          }
          addBar(f1, f9 + f8, f2, f9 - f8);
        }
      }
    }
    reset();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/buffer/HorizontalBarBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */