package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class BarBuffer
  extends AbstractBuffer<IBarDataSet>
{
  protected float mBarWidth = 1.0F;
  protected boolean mContainsStacks = false;
  protected int mDataSetCount = 1;
  protected int mDataSetIndex = 0;
  protected boolean mInverted = false;
  
  public BarBuffer(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(paramInt1);
    this.mDataSetCount = paramInt2;
    this.mContainsStacks = paramBoolean;
  }
  
  protected void addBar(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    float[] arrayOfFloat = this.buffer;
    int i = this.index;
    this.index = (i + 1);
    arrayOfFloat[i] = paramFloat1;
    arrayOfFloat = this.buffer;
    i = this.index;
    this.index = (i + 1);
    arrayOfFloat[i] = paramFloat2;
    arrayOfFloat = this.buffer;
    i = this.index;
    this.index = (i + 1);
    arrayOfFloat[i] = paramFloat3;
    arrayOfFloat = this.buffer;
    i = this.index;
    this.index = (i + 1);
    arrayOfFloat[i] = paramFloat4;
  }
  
  public void feed(IBarDataSet paramIBarDataSet)
  {
    float f7 = paramIBarDataSet.getEntryCount();
    float f8 = this.phaseX;
    float f6 = this.mBarWidth / 2.0F;
    for (int i = 0; i < f7 * f8; i++)
    {
      BarEntry localBarEntry = (BarEntry)paramIBarDataSet.getEntryForIndex(i);
      if (localBarEntry != null)
      {
        float f9 = localBarEntry.getX();
        float f1 = localBarEntry.getY();
        float[] arrayOfFloat = localBarEntry.getYVals();
        int j;
        float f2;
        if ((this.mContainsStacks) && (arrayOfFloat != null))
        {
          f1 = -localBarEntry.getNegativeSum();
          j = 0;
          f2 = 0.0F;
        }
        while (j < arrayOfFloat.length)
        {
          float f4 = arrayOfFloat[j];
          float f5;
          float f3;
          if ((f4 == 0.0F) && ((f2 == 0.0F) || (f1 == 0.0F)))
          {
            f5 = f1;
            f1 = f4;
            f3 = f2;
            f2 = f4;
            f4 = f5;
          }
          else if (f4 >= 0.0F)
          {
            f4 += f2;
            f5 = f1;
            f3 = f4;
            f1 = f2;
            f2 = f4;
            f4 = f5;
          }
          else
          {
            float f10 = Math.abs(f4);
            f4 = Math.abs(f4);
            f3 = f2;
            f5 = f1;
            f4 += f1;
            f2 = f10 + f1;
            f1 = f5;
          }
          if (this.mInverted)
          {
            if (f1 >= f2) {
              f5 = f1;
            } else {
              f5 = f2;
            }
            if (f1 > f2) {
              f1 = f2;
            }
            f2 = f5;
            f5 = f1;
            f1 = f2;
          }
          else
          {
            if (f1 >= f2) {
              f5 = f1;
            } else {
              f5 = f2;
            }
            if (f1 > f2) {
              f1 = f2;
            }
          }
          addBar(f9 - f6, f5 * this.phaseY, f9 + f6, f1 * this.phaseY);
          j++;
          f2 = f3;
          f1 = f4;
          continue;
          if (this.mInverted)
          {
            if (f1 >= 0.0F) {
              f2 = f1;
            } else {
              f2 = 0.0F;
            }
            if (f1 > 0.0F) {
              f1 = 0.0F;
            }
            f3 = f2;
            f2 = f1;
            f1 = f3;
          }
          else
          {
            if (f1 >= 0.0F) {
              f2 = f1;
            } else {
              f2 = 0.0F;
            }
            if (f1 > 0.0F) {
              f1 = 0.0F;
            }
          }
          if (f2 > 0.0F) {
            f2 *= this.phaseY;
          } else {
            f1 *= this.phaseY;
          }
          addBar(f9 - f6, f2, f9 + f6, f1);
        }
      }
    }
    reset();
  }
  
  public void setBarWidth(float paramFloat)
  {
    this.mBarWidth = paramFloat;
  }
  
  public void setDataSet(int paramInt)
  {
    this.mDataSetIndex = paramInt;
  }
  
  public void setInverted(boolean paramBoolean)
  {
    this.mInverted = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/buffer/BarBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */