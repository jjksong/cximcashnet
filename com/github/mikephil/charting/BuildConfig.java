package com.github.mikephil.charting;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.github.mikephil.charting";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "3.0.3";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */