package com.github.mikephil.charting.components;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.FSize;
import com.github.mikephil.charting.utils.MPPointF;
import java.lang.ref.WeakReference;

public class MarkerImage
  implements IMarker
{
  private Context mContext;
  private Drawable mDrawable;
  private Rect mDrawableBoundsCache = new Rect();
  private MPPointF mOffset = new MPPointF();
  private MPPointF mOffset2 = new MPPointF();
  private FSize mSize = new FSize();
  private WeakReference<Chart> mWeakChart;
  
  public MarkerImage(Context paramContext, int paramInt)
  {
    this.mContext = paramContext;
    if (Build.VERSION.SDK_INT >= 21) {
      this.mDrawable = this.mContext.getResources().getDrawable(paramInt, null);
    } else {
      this.mDrawable = this.mContext.getResources().getDrawable(paramInt);
    }
  }
  
  public void draw(Canvas paramCanvas, float paramFloat1, float paramFloat2)
  {
    if (this.mDrawable == null) {
      return;
    }
    MPPointF localMPPointF = getOffsetForDrawingAtPoint(paramFloat1, paramFloat2);
    float f2 = this.mSize.width;
    float f3 = this.mSize.height;
    float f1 = f2;
    if (f2 == 0.0F) {
      f1 = this.mDrawable.getIntrinsicWidth();
    }
    f2 = f3;
    if (f3 == 0.0F) {
      f2 = this.mDrawable.getIntrinsicHeight();
    }
    this.mDrawable.copyBounds(this.mDrawableBoundsCache);
    this.mDrawable.setBounds(this.mDrawableBoundsCache.left, this.mDrawableBoundsCache.top, this.mDrawableBoundsCache.left + (int)f1, this.mDrawableBoundsCache.top + (int)f2);
    int i = paramCanvas.save();
    paramCanvas.translate(paramFloat1 + localMPPointF.x, paramFloat2 + localMPPointF.y);
    this.mDrawable.draw(paramCanvas);
    paramCanvas.restoreToCount(i);
    this.mDrawable.setBounds(this.mDrawableBoundsCache);
  }
  
  public Chart getChartView()
  {
    Object localObject = this.mWeakChart;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = (Chart)((WeakReference)localObject).get();
    }
    return (Chart)localObject;
  }
  
  public MPPointF getOffset()
  {
    return this.mOffset;
  }
  
  public MPPointF getOffsetForDrawingAtPoint(float paramFloat1, float paramFloat2)
  {
    Object localObject = getOffset();
    this.mOffset2.x = ((MPPointF)localObject).x;
    this.mOffset2.y = ((MPPointF)localObject).y;
    localObject = getChartView();
    float f2 = this.mSize.width;
    float f3 = this.mSize.height;
    float f1 = f2;
    Drawable localDrawable;
    if (f2 == 0.0F)
    {
      localDrawable = this.mDrawable;
      f1 = f2;
      if (localDrawable != null) {
        f1 = localDrawable.getIntrinsicWidth();
      }
    }
    f2 = f3;
    if (f3 == 0.0F)
    {
      localDrawable = this.mDrawable;
      f2 = f3;
      if (localDrawable != null) {
        f2 = localDrawable.getIntrinsicHeight();
      }
    }
    if (this.mOffset2.x + paramFloat1 < 0.0F) {
      this.mOffset2.x = (-paramFloat1);
    } else if ((localObject != null) && (paramFloat1 + f1 + this.mOffset2.x > ((Chart)localObject).getWidth())) {
      this.mOffset2.x = (((Chart)localObject).getWidth() - paramFloat1 - f1);
    }
    if (this.mOffset2.y + paramFloat2 < 0.0F) {
      this.mOffset2.y = (-paramFloat2);
    } else if ((localObject != null) && (paramFloat2 + f2 + this.mOffset2.y > ((Chart)localObject).getHeight())) {
      this.mOffset2.y = (((Chart)localObject).getHeight() - paramFloat2 - f2);
    }
    return this.mOffset2;
  }
  
  public FSize getSize()
  {
    return this.mSize;
  }
  
  public void refreshContent(Entry paramEntry, Highlight paramHighlight) {}
  
  public void setChartView(Chart paramChart)
  {
    this.mWeakChart = new WeakReference(paramChart);
  }
  
  public void setOffset(float paramFloat1, float paramFloat2)
  {
    MPPointF localMPPointF = this.mOffset;
    localMPPointF.x = paramFloat1;
    localMPPointF.y = paramFloat2;
  }
  
  public void setOffset(MPPointF paramMPPointF)
  {
    this.mOffset = paramMPPointF;
    if (this.mOffset == null) {
      this.mOffset = new MPPointF();
    }
  }
  
  public void setSize(FSize paramFSize)
  {
    this.mSize = paramFSize;
    if (this.mSize == null) {
      this.mSize = new FSize();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/components/MarkerImage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */