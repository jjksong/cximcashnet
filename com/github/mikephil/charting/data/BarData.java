package com.github.mikephil.charting.data;

import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import java.util.Iterator;
import java.util.List;

public class BarData
  extends BarLineScatterCandleBubbleData<IBarDataSet>
{
  private float mBarWidth = 0.85F;
  
  public BarData() {}
  
  public BarData(List<IBarDataSet> paramList)
  {
    super(paramList);
  }
  
  public BarData(IBarDataSet... paramVarArgs)
  {
    super(paramVarArgs);
  }
  
  public float getBarWidth()
  {
    return this.mBarWidth;
  }
  
  public float getGroupWidth(float paramFloat1, float paramFloat2)
  {
    return this.mDataSets.size() * (this.mBarWidth + paramFloat2) + paramFloat1;
  }
  
  public void groupBars(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    if (this.mDataSets.size() > 1)
    {
      int j = ((IBarDataSet)getMaxEntryCountSet()).getEntryCount();
      float f2 = paramFloat2 / 2.0F;
      float f1 = paramFloat3 / 2.0F;
      float f3 = this.mBarWidth / 2.0F;
      paramFloat3 = getGroupWidth(paramFloat2, paramFloat3);
      for (int i = 0; i < j; i++)
      {
        paramFloat2 = paramFloat1 + f2;
        Iterator localIterator = this.mDataSets.iterator();
        while (localIterator.hasNext())
        {
          Object localObject = (IBarDataSet)localIterator.next();
          paramFloat2 = paramFloat2 + f1 + f3;
          if (i < ((IBarDataSet)localObject).getEntryCount())
          {
            localObject = (BarEntry)((IBarDataSet)localObject).getEntryForIndex(i);
            if (localObject != null) {
              ((BarEntry)localObject).setX(paramFloat2);
            }
          }
          paramFloat2 = paramFloat2 + f3 + f1;
        }
        paramFloat2 += f2;
        paramFloat1 = paramFloat3 - (paramFloat2 - paramFloat1);
        if ((paramFloat1 <= 0.0F) && (paramFloat1 >= 0.0F)) {
          paramFloat1 = paramFloat2;
        } else {
          paramFloat1 = paramFloat2 + paramFloat1;
        }
      }
      notifyDataChanged();
      return;
    }
    throw new RuntimeException("BarData needs to hold at least 2 BarDataSets to allow grouping.");
  }
  
  public void setBarWidth(float paramFloat)
  {
    this.mBarWidth = paramFloat;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/data/BarData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */