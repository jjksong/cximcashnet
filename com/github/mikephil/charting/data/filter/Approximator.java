package com.github.mikephil.charting.data.filter;

import android.annotation.TargetApi;
import java.util.Arrays;

public class Approximator
{
  float[] concat(float[]... paramVarArgs)
  {
    int k = paramVarArgs.length;
    int i = 0;
    int j = 0;
    while (i < k)
    {
      j += paramVarArgs[i].length;
      i++;
    }
    float[] arrayOfFloat2 = new float[j];
    int m = paramVarArgs.length;
    j = 0;
    i = 0;
    while (j < m)
    {
      float[] arrayOfFloat1 = paramVarArgs[j];
      int n = arrayOfFloat1.length;
      for (k = 0; k < n; k++)
      {
        arrayOfFloat2[i] = arrayOfFloat1[k];
        i++;
      }
      j++;
    }
    return arrayOfFloat2;
  }
  
  @TargetApi(9)
  public float[] reduceWithDouglasPeucker(float[] paramArrayOfFloat, float paramFloat)
  {
    Object localObject = new Line(paramArrayOfFloat[0], paramArrayOfFloat[1], paramArrayOfFloat[(paramArrayOfFloat.length - 2)], paramArrayOfFloat[(paramArrayOfFloat.length - 1)]);
    int i = 2;
    float f1 = 0.0F;
    int j = 0;
    while (i < paramArrayOfFloat.length - 2)
    {
      float f3 = ((Line)localObject).distance(paramArrayOfFloat[i], paramArrayOfFloat[(i + 1)]);
      float f2 = f1;
      if (f3 > f1)
      {
        j = i;
        f2 = f3;
      }
      i += 2;
      f1 = f2;
    }
    if (f1 > paramFloat)
    {
      localObject = reduceWithDouglasPeucker(Arrays.copyOfRange(paramArrayOfFloat, 0, j + 2), paramFloat);
      paramArrayOfFloat = reduceWithDouglasPeucker(Arrays.copyOfRange(paramArrayOfFloat, j, paramArrayOfFloat.length), paramFloat);
      return concat(new float[][] { localObject, Arrays.copyOfRange(paramArrayOfFloat, 2, paramArrayOfFloat.length) });
    }
    return ((Line)localObject).getPoints();
  }
  
  private class Line
  {
    private float dx;
    private float dy;
    private float exsy;
    private float length;
    private float[] points;
    private float sxey;
    
    public Line(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      this.dx = (paramFloat1 - paramFloat3);
      this.dy = (paramFloat2 - paramFloat4);
      this.sxey = (paramFloat1 * paramFloat4);
      this.exsy = (paramFloat3 * paramFloat2);
      float f1 = this.dx;
      float f2 = this.dy;
      this.length = ((float)Math.sqrt(f1 * f1 + f2 * f2));
      this.points = new float[] { paramFloat1, paramFloat2, paramFloat3, paramFloat4 };
    }
    
    public float distance(float paramFloat1, float paramFloat2)
    {
      return Math.abs(this.dy * paramFloat1 - this.dx * paramFloat2 + this.sxey - this.exsy) / this.length;
    }
    
    public float[] getPoints()
    {
      return this.points;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/data/filter/Approximator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */