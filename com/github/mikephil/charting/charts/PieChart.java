package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.PieHighlighter;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.renderer.DataRenderer;
import com.github.mikephil.charting.renderer.LegendRenderer;
import com.github.mikephil.charting.renderer.PieChartRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import java.util.List;

public class PieChart
  extends PieRadarChartBase<PieData>
{
  private float[] mAbsoluteAngles = new float[1];
  private CharSequence mCenterText = "";
  private MPPointF mCenterTextOffset = MPPointF.getInstance(0.0F, 0.0F);
  private float mCenterTextRadiusPercent = 100.0F;
  private RectF mCircleBox = new RectF();
  private float[] mDrawAngles = new float[1];
  private boolean mDrawCenterText = true;
  private boolean mDrawEntryLabels = true;
  private boolean mDrawHole = true;
  private boolean mDrawRoundedSlices = false;
  private boolean mDrawSlicesUnderHole = false;
  private float mHoleRadiusPercent = 50.0F;
  protected float mMaxAngle = 360.0F;
  protected float mTransparentCircleRadiusPercent = 55.0F;
  private boolean mUsePercentValues = false;
  
  public PieChart(Context paramContext)
  {
    super(paramContext);
  }
  
  public PieChart(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public PieChart(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private float calcAngle(float paramFloat)
  {
    return calcAngle(paramFloat, ((PieData)this.mData).getYValueSum());
  }
  
  private float calcAngle(float paramFloat1, float paramFloat2)
  {
    return paramFloat1 / paramFloat2 * this.mMaxAngle;
  }
  
  private void calcAngles()
  {
    int j = ((PieData)this.mData).getEntryCount();
    if (this.mDrawAngles.length != j) {
      this.mDrawAngles = new float[j];
    } else {
      for (i = 0; i < j; i++) {
        this.mDrawAngles[i] = 0.0F;
      }
    }
    if (this.mAbsoluteAngles.length != j) {
      this.mAbsoluteAngles = new float[j];
    } else {
      for (i = 0; i < j; i++) {
        this.mAbsoluteAngles[i] = 0.0F;
      }
    }
    float f = ((PieData)this.mData).getYValueSum();
    List localList = ((PieData)this.mData).getDataSets();
    j = 0;
    int i = 0;
    while (j < ((PieData)this.mData).getDataSetCount())
    {
      IPieDataSet localIPieDataSet = (IPieDataSet)localList.get(j);
      for (int k = 0; k < localIPieDataSet.getEntryCount(); k++)
      {
        this.mDrawAngles[i] = calcAngle(Math.abs(((PieEntry)localIPieDataSet.getEntryForIndex(k)).getY()), f);
        if (i == 0)
        {
          this.mAbsoluteAngles[i] = this.mDrawAngles[i];
        }
        else
        {
          float[] arrayOfFloat = this.mAbsoluteAngles;
          arrayOfFloat[i] = (arrayOfFloat[(i - 1)] + this.mDrawAngles[i]);
        }
        i++;
      }
      j++;
    }
  }
  
  protected void calcMinMax()
  {
    calcAngles();
  }
  
  public void calculateOffsets()
  {
    super.calculateOffsets();
    if (this.mData == null) {
      return;
    }
    float f2 = getDiameter() / 2.0F;
    MPPointF localMPPointF = getCenterOffsets();
    float f1 = ((PieData)this.mData).getDataSet().getSelectionShift();
    this.mCircleBox.set(localMPPointF.x - f2 + f1, localMPPointF.y - f2 + f1, localMPPointF.x + f2 - f1, localMPPointF.y + f2 - f1);
    MPPointF.recycleInstance(localMPPointF);
  }
  
  public float[] getAbsoluteAngles()
  {
    return this.mAbsoluteAngles;
  }
  
  public MPPointF getCenterCircleBox()
  {
    return MPPointF.getInstance(this.mCircleBox.centerX(), this.mCircleBox.centerY());
  }
  
  public CharSequence getCenterText()
  {
    return this.mCenterText;
  }
  
  public MPPointF getCenterTextOffset()
  {
    return MPPointF.getInstance(this.mCenterTextOffset.x, this.mCenterTextOffset.y);
  }
  
  public float getCenterTextRadiusPercent()
  {
    return this.mCenterTextRadiusPercent;
  }
  
  public RectF getCircleBox()
  {
    return this.mCircleBox;
  }
  
  public int getDataSetIndexForIndex(int paramInt)
  {
    List localList = ((PieData)this.mData).getDataSets();
    for (int i = 0; i < localList.size(); i++) {
      if (((IPieDataSet)localList.get(i)).getEntryForXValue(paramInt, NaN.0F) != null) {
        return i;
      }
    }
    return -1;
  }
  
  public float[] getDrawAngles()
  {
    return this.mDrawAngles;
  }
  
  public float getHoleRadius()
  {
    return this.mHoleRadiusPercent;
  }
  
  public int getIndexForAngle(float paramFloat)
  {
    paramFloat = Utils.getNormalizedAngle(paramFloat - getRotationAngle());
    for (int i = 0;; i++)
    {
      float[] arrayOfFloat = this.mAbsoluteAngles;
      if (i >= arrayOfFloat.length) {
        break;
      }
      if (arrayOfFloat[i] > paramFloat) {
        return i;
      }
    }
    return -1;
  }
  
  protected float[] getMarkerPosition(Highlight paramHighlight)
  {
    MPPointF localMPPointF = getCenterCircleBox();
    float f2 = getRadius();
    float f1 = f2 / 10.0F * 3.6F;
    if (isDrawHoleEnabled()) {
      f1 = (f2 - f2 / 100.0F * getHoleRadius()) / 2.0F;
    }
    float f3 = getRotationAngle();
    int i = (int)paramHighlight.getX();
    float f4 = this.mDrawAngles[i] / 2.0F;
    double d1 = f2 - f1;
    double d2 = Math.cos(Math.toRadians((this.mAbsoluteAngles[i] + f3 - f4) * this.mAnimator.getPhaseY()));
    Double.isNaN(d1);
    double d3 = localMPPointF.x;
    Double.isNaN(d3);
    f1 = (float)(d2 * d1 + d3);
    d3 = Math.sin(Math.toRadians((f3 + this.mAbsoluteAngles[i] - f4) * this.mAnimator.getPhaseY()));
    Double.isNaN(d1);
    d2 = localMPPointF.y;
    Double.isNaN(d2);
    f2 = (float)(d1 * d3 + d2);
    MPPointF.recycleInstance(localMPPointF);
    return new float[] { f1, f2 };
  }
  
  public float getMaxAngle()
  {
    return this.mMaxAngle;
  }
  
  public float getRadius()
  {
    RectF localRectF = this.mCircleBox;
    if (localRectF == null) {
      return 0.0F;
    }
    return Math.min(localRectF.width() / 2.0F, this.mCircleBox.height() / 2.0F);
  }
  
  protected float getRequiredBaseOffset()
  {
    return 0.0F;
  }
  
  protected float getRequiredLegendOffset()
  {
    return this.mLegendRenderer.getLabelPaint().getTextSize() * 2.0F;
  }
  
  public float getTransparentCircleRadius()
  {
    return this.mTransparentCircleRadiusPercent;
  }
  
  @Deprecated
  public XAxis getXAxis()
  {
    throw new RuntimeException("PieChart has no XAxis");
  }
  
  protected void init()
  {
    super.init();
    this.mRenderer = new PieChartRenderer(this, this.mAnimator, this.mViewPortHandler);
    this.mXAxis = null;
    this.mHighlighter = new PieHighlighter(this);
  }
  
  public boolean isDrawCenterTextEnabled()
  {
    return this.mDrawCenterText;
  }
  
  public boolean isDrawEntryLabelsEnabled()
  {
    return this.mDrawEntryLabels;
  }
  
  public boolean isDrawHoleEnabled()
  {
    return this.mDrawHole;
  }
  
  public boolean isDrawRoundedSlicesEnabled()
  {
    return this.mDrawRoundedSlices;
  }
  
  public boolean isDrawSlicesUnderHoleEnabled()
  {
    return this.mDrawSlicesUnderHole;
  }
  
  public boolean isUsePercentValuesEnabled()
  {
    return this.mUsePercentValues;
  }
  
  public boolean needsHighlight(int paramInt)
  {
    if (!valuesToHighlight()) {
      return false;
    }
    for (int i = 0; i < this.mIndicesToHighlight.length; i++) {
      if ((int)this.mIndicesToHighlight[i].getX() == paramInt) {
        return true;
      }
    }
    return false;
  }
  
  protected void onDetachedFromWindow()
  {
    if ((this.mRenderer != null) && ((this.mRenderer instanceof PieChartRenderer))) {
      ((PieChartRenderer)this.mRenderer).releaseBitmap();
    }
    super.onDetachedFromWindow();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mData == null) {
      return;
    }
    this.mRenderer.drawData(paramCanvas);
    if (valuesToHighlight()) {
      this.mRenderer.drawHighlighted(paramCanvas, this.mIndicesToHighlight);
    }
    this.mRenderer.drawExtras(paramCanvas);
    this.mRenderer.drawValues(paramCanvas);
    this.mLegendRenderer.renderLegend(paramCanvas);
    drawDescription(paramCanvas);
    drawMarkers(paramCanvas);
  }
  
  public void setCenterText(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      this.mCenterText = "";
    } else {
      this.mCenterText = paramCharSequence;
    }
  }
  
  public void setCenterTextColor(int paramInt)
  {
    ((PieChartRenderer)this.mRenderer).getPaintCenterText().setColor(paramInt);
  }
  
  public void setCenterTextOffset(float paramFloat1, float paramFloat2)
  {
    this.mCenterTextOffset.x = Utils.convertDpToPixel(paramFloat1);
    this.mCenterTextOffset.y = Utils.convertDpToPixel(paramFloat2);
  }
  
  public void setCenterTextRadiusPercent(float paramFloat)
  {
    this.mCenterTextRadiusPercent = paramFloat;
  }
  
  public void setCenterTextSize(float paramFloat)
  {
    ((PieChartRenderer)this.mRenderer).getPaintCenterText().setTextSize(Utils.convertDpToPixel(paramFloat));
  }
  
  public void setCenterTextSizePixels(float paramFloat)
  {
    ((PieChartRenderer)this.mRenderer).getPaintCenterText().setTextSize(paramFloat);
  }
  
  public void setCenterTextTypeface(Typeface paramTypeface)
  {
    ((PieChartRenderer)this.mRenderer).getPaintCenterText().setTypeface(paramTypeface);
  }
  
  public void setDrawCenterText(boolean paramBoolean)
  {
    this.mDrawCenterText = paramBoolean;
  }
  
  public void setDrawEntryLabels(boolean paramBoolean)
  {
    this.mDrawEntryLabels = paramBoolean;
  }
  
  public void setDrawHoleEnabled(boolean paramBoolean)
  {
    this.mDrawHole = paramBoolean;
  }
  
  @Deprecated
  public void setDrawSliceText(boolean paramBoolean)
  {
    this.mDrawEntryLabels = paramBoolean;
  }
  
  public void setDrawSlicesUnderHole(boolean paramBoolean)
  {
    this.mDrawSlicesUnderHole = paramBoolean;
  }
  
  public void setEntryLabelColor(int paramInt)
  {
    ((PieChartRenderer)this.mRenderer).getPaintEntryLabels().setColor(paramInt);
  }
  
  public void setEntryLabelTextSize(float paramFloat)
  {
    ((PieChartRenderer)this.mRenderer).getPaintEntryLabels().setTextSize(Utils.convertDpToPixel(paramFloat));
  }
  
  public void setEntryLabelTypeface(Typeface paramTypeface)
  {
    ((PieChartRenderer)this.mRenderer).getPaintEntryLabels().setTypeface(paramTypeface);
  }
  
  public void setHoleColor(int paramInt)
  {
    ((PieChartRenderer)this.mRenderer).getPaintHole().setColor(paramInt);
  }
  
  public void setHoleRadius(float paramFloat)
  {
    this.mHoleRadiusPercent = paramFloat;
  }
  
  public void setMaxAngle(float paramFloat)
  {
    float f = paramFloat;
    if (paramFloat > 360.0F) {
      f = 360.0F;
    }
    paramFloat = f;
    if (f < 90.0F) {
      paramFloat = 90.0F;
    }
    this.mMaxAngle = paramFloat;
  }
  
  public void setTransparentCircleAlpha(int paramInt)
  {
    ((PieChartRenderer)this.mRenderer).getPaintTransparentCircle().setAlpha(paramInt);
  }
  
  public void setTransparentCircleColor(int paramInt)
  {
    Paint localPaint = ((PieChartRenderer)this.mRenderer).getPaintTransparentCircle();
    int i = localPaint.getAlpha();
    localPaint.setColor(paramInt);
    localPaint.setAlpha(i);
  }
  
  public void setTransparentCircleRadius(float paramFloat)
  {
    this.mTransparentCircleRadiusPercent = paramFloat;
  }
  
  public void setUsePercentValues(boolean paramBoolean)
  {
    this.mUsePercentValues = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/charts/PieChart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */