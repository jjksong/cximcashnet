package com.github.mikephil.charting.charts;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.HorizontalBarHighlighter;
import com.github.mikephil.charting.highlight.IHighlighter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.renderer.HorizontalBarChartRenderer;
import com.github.mikephil.charting.renderer.XAxisRendererHorizontalBarChart;
import com.github.mikephil.charting.renderer.YAxisRenderer;
import com.github.mikephil.charting.renderer.YAxisRendererHorizontalBarChart;
import com.github.mikephil.charting.utils.HorizontalViewPortHandler;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.TransformerHorizontalBarChart;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class HorizontalBarChart
  extends BarChart
{
  protected float[] mGetPositionBuffer = new float[2];
  private RectF mOffsetsBuffer = new RectF();
  
  public HorizontalBarChart(Context paramContext)
  {
    super(paramContext);
  }
  
  public HorizontalBarChart(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public HorizontalBarChart(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public void calculateOffsets()
  {
    calculateLegendOffsets(this.mOffsetsBuffer);
    float f6 = this.mOffsetsBuffer.left + 0.0F;
    float f2 = this.mOffsetsBuffer.top + 0.0F;
    float f5 = this.mOffsetsBuffer.right + 0.0F;
    float f1 = this.mOffsetsBuffer.bottom + 0.0F;
    float f3 = f2;
    if (this.mAxisLeft.needsOffset()) {
      f3 = f2 + this.mAxisLeft.getRequiredHeightSpace(this.mAxisRendererLeft.getPaintAxisLabels());
    }
    float f4 = f1;
    if (this.mAxisRight.needsOffset()) {
      f4 = f1 + this.mAxisRight.getRequiredHeightSpace(this.mAxisRendererRight.getPaintAxisLabels());
    }
    float f7 = this.mXAxis.mLabelRotatedWidth;
    f1 = f6;
    f2 = f5;
    if (this.mXAxis.isEnabled()) {
      if (this.mXAxis.getPosition() == XAxis.XAxisPosition.BOTTOM)
      {
        f1 = f6 + f7;
        f2 = f5;
      }
      else if (this.mXAxis.getPosition() == XAxis.XAxisPosition.TOP)
      {
        f2 = f5 + f7;
        f1 = f6;
      }
      else
      {
        f1 = f6;
        f2 = f5;
        if (this.mXAxis.getPosition() == XAxis.XAxisPosition.BOTH_SIDED)
        {
          f1 = f6 + f7;
          f2 = f5 + f7;
        }
      }
    }
    f3 += getExtraTopOffset();
    f2 += getExtraRightOffset();
    f4 += getExtraBottomOffset();
    f5 = f1 + getExtraLeftOffset();
    f1 = Utils.convertDpToPixel(this.mMinOffset);
    this.mViewPortHandler.restrainViewPort(Math.max(f1, f5), Math.max(f1, f3), Math.max(f1, f2), Math.max(f1, f4));
    if (this.mLogEnabled)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("offsetLeft: ");
      localStringBuilder.append(f5);
      localStringBuilder.append(", offsetTop: ");
      localStringBuilder.append(f3);
      localStringBuilder.append(", offsetRight: ");
      localStringBuilder.append(f2);
      localStringBuilder.append(", offsetBottom: ");
      localStringBuilder.append(f4);
      Log.i("MPAndroidChart", localStringBuilder.toString());
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("Content: ");
      localStringBuilder.append(this.mViewPortHandler.getContentRect().toString());
      Log.i("MPAndroidChart", localStringBuilder.toString());
    }
    prepareOffsetMatrix();
    prepareValuePxMatrix();
  }
  
  public void getBarBounds(BarEntry paramBarEntry, RectF paramRectF)
  {
    IBarDataSet localIBarDataSet = (IBarDataSet)((BarData)this.mData).getDataSetForEntry(paramBarEntry);
    if (localIBarDataSet == null)
    {
      paramRectF.set(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
      return;
    }
    float f2 = paramBarEntry.getY();
    float f3 = paramBarEntry.getX();
    float f4 = ((BarData)this.mData).getBarWidth() / 2.0F;
    float f1;
    if (f2 >= 0.0F) {
      f1 = f2;
    } else {
      f1 = 0.0F;
    }
    if (f2 > 0.0F) {
      f2 = 0.0F;
    }
    paramRectF.set(f1, f3 - f4, f2, f3 + f4);
    getTransformer(localIBarDataSet.getAxisDependency()).rectValueToPixel(paramRectF);
  }
  
  public float getHighestVisibleX()
  {
    getTransformer(YAxis.AxisDependency.LEFT).getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop(), this.posForGetHighestVisibleX);
    return (float)Math.min(this.mXAxis.mAxisMaximum, this.posForGetHighestVisibleX.y);
  }
  
  public Highlight getHighlightByTouchPoint(float paramFloat1, float paramFloat2)
  {
    if (this.mData == null)
    {
      if (this.mLogEnabled) {
        Log.e("MPAndroidChart", "Can't select by touch. No data set.");
      }
      return null;
    }
    return getHighlighter().getHighlight(paramFloat2, paramFloat1);
  }
  
  public float getLowestVisibleX()
  {
    getTransformer(YAxis.AxisDependency.LEFT).getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentBottom(), this.posForGetLowestVisibleX);
    return (float)Math.max(this.mXAxis.mAxisMinimum, this.posForGetLowestVisibleX.y);
  }
  
  protected float[] getMarkerPosition(Highlight paramHighlight)
  {
    return new float[] { paramHighlight.getDrawY(), paramHighlight.getDrawX() };
  }
  
  public MPPointF getPosition(Entry paramEntry, YAxis.AxisDependency paramAxisDependency)
  {
    if (paramEntry == null) {
      return null;
    }
    float[] arrayOfFloat = this.mGetPositionBuffer;
    arrayOfFloat[0] = paramEntry.getY();
    arrayOfFloat[1] = paramEntry.getX();
    getTransformer(paramAxisDependency).pointValuesToPixel(arrayOfFloat);
    return MPPointF.getInstance(arrayOfFloat[0], arrayOfFloat[1]);
  }
  
  protected void init()
  {
    this.mViewPortHandler = new HorizontalViewPortHandler();
    super.init();
    this.mLeftAxisTransformer = new TransformerHorizontalBarChart(this.mViewPortHandler);
    this.mRightAxisTransformer = new TransformerHorizontalBarChart(this.mViewPortHandler);
    this.mRenderer = new HorizontalBarChartRenderer(this, this.mAnimator, this.mViewPortHandler);
    setHighlighter(new HorizontalBarHighlighter(this));
    this.mAxisRendererLeft = new YAxisRendererHorizontalBarChart(this.mViewPortHandler, this.mAxisLeft, this.mLeftAxisTransformer);
    this.mAxisRendererRight = new YAxisRendererHorizontalBarChart(this.mViewPortHandler, this.mAxisRight, this.mRightAxisTransformer);
    this.mXAxisRenderer = new XAxisRendererHorizontalBarChart(this.mViewPortHandler, this.mXAxis, this.mLeftAxisTransformer, this);
  }
  
  protected void prepareValuePxMatrix()
  {
    this.mRightAxisTransformer.prepareMatrixValuePx(this.mAxisRight.mAxisMinimum, this.mAxisRight.mAxisRange, this.mXAxis.mAxisRange, this.mXAxis.mAxisMinimum);
    this.mLeftAxisTransformer.prepareMatrixValuePx(this.mAxisLeft.mAxisMinimum, this.mAxisLeft.mAxisRange, this.mXAxis.mAxisRange, this.mXAxis.mAxisMinimum);
  }
  
  public void setVisibleXRange(float paramFloat1, float paramFloat2)
  {
    paramFloat1 = this.mXAxis.mAxisRange / paramFloat1;
    paramFloat2 = this.mXAxis.mAxisRange / paramFloat2;
    this.mViewPortHandler.setMinMaxScaleY(paramFloat1, paramFloat2);
  }
  
  public void setVisibleXRangeMaximum(float paramFloat)
  {
    paramFloat = this.mXAxis.mAxisRange / paramFloat;
    this.mViewPortHandler.setMinimumScaleY(paramFloat);
  }
  
  public void setVisibleXRangeMinimum(float paramFloat)
  {
    paramFloat = this.mXAxis.mAxisRange / paramFloat;
    this.mViewPortHandler.setMaximumScaleY(paramFloat);
  }
  
  public void setVisibleYRange(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat1 = getAxisRange(paramAxisDependency) / paramFloat1;
    paramFloat2 = getAxisRange(paramAxisDependency) / paramFloat2;
    this.mViewPortHandler.setMinMaxScaleX(paramFloat1, paramFloat2);
  }
  
  public void setVisibleYRangeMaximum(float paramFloat, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat = getAxisRange(paramAxisDependency) / paramFloat;
    this.mViewPortHandler.setMinimumScaleX(paramFloat);
  }
  
  public void setVisibleYRangeMinimum(float paramFloat, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat = getAxisRange(paramAxisDependency) / paramFloat;
    this.mViewPortHandler.setMaximumScaleX(paramFloat);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/charts/HorizontalBarChart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */