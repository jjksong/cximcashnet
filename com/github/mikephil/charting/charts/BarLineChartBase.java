package com.github.mikephil.charting.charts;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.ChartHighlighter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.BarLineScatterCandleBubbleDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.jobs.AnimatedMoveViewJob;
import com.github.mikephil.charting.jobs.AnimatedZoomJob;
import com.github.mikephil.charting.jobs.MoveViewJob;
import com.github.mikephil.charting.jobs.ZoomJob;
import com.github.mikephil.charting.listener.BarLineChartTouchListener;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnDrawListener;
import com.github.mikephil.charting.renderer.DataRenderer;
import com.github.mikephil.charting.renderer.LegendRenderer;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.renderer.YAxisRenderer;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

@SuppressLint({"RtlHardcoded"})
public abstract class BarLineChartBase<T extends BarLineScatterCandleBubbleData<? extends IBarLineScatterCandleBubbleDataSet<? extends Entry>>>
  extends Chart<T>
  implements BarLineScatterCandleBubbleDataProvider
{
  private long drawCycles = 0L;
  protected boolean mAutoScaleMinMaxEnabled = false;
  protected YAxis mAxisLeft;
  protected YAxisRenderer mAxisRendererLeft;
  protected YAxisRenderer mAxisRendererRight;
  protected YAxis mAxisRight;
  protected Paint mBorderPaint;
  protected boolean mClipValuesToContent = false;
  private boolean mCustomViewPortEnabled = false;
  protected boolean mDoubleTapToZoomEnabled = true;
  private boolean mDragXEnabled = true;
  private boolean mDragYEnabled = true;
  protected boolean mDrawBorders = false;
  protected boolean mDrawGridBackground = false;
  protected OnDrawListener mDrawListener;
  protected Matrix mFitScreenMatrixBuffer = new Matrix();
  protected float[] mGetPositionBuffer = new float[2];
  protected Paint mGridBackgroundPaint;
  protected boolean mHighlightPerDragEnabled = true;
  protected boolean mKeepPositionOnRotation = false;
  protected Transformer mLeftAxisTransformer;
  protected int mMaxVisibleCount = 100;
  protected float mMinOffset = 15.0F;
  private RectF mOffsetsBuffer = new RectF();
  protected float[] mOnSizeChangedBuffer = new float[2];
  protected boolean mPinchZoomEnabled = false;
  protected Transformer mRightAxisTransformer;
  private boolean mScaleXEnabled = true;
  private boolean mScaleYEnabled = true;
  protected XAxisRenderer mXAxisRenderer;
  protected Matrix mZoomMatrixBuffer = new Matrix();
  protected MPPointD posForGetHighestVisibleX = MPPointD.getInstance(0.0D, 0.0D);
  protected MPPointD posForGetLowestVisibleX = MPPointD.getInstance(0.0D, 0.0D);
  private long totalTime = 0L;
  
  public BarLineChartBase(Context paramContext)
  {
    super(paramContext);
  }
  
  public BarLineChartBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public BarLineChartBase(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void autoScale()
  {
    float f1 = getLowestVisibleX();
    float f2 = getHighestVisibleX();
    ((BarLineScatterCandleBubbleData)this.mData).calcMinMaxY(f1, f2);
    this.mXAxis.calculate(((BarLineScatterCandleBubbleData)this.mData).getXMin(), ((BarLineScatterCandleBubbleData)this.mData).getXMax());
    if (this.mAxisLeft.isEnabled()) {
      this.mAxisLeft.calculate(((BarLineScatterCandleBubbleData)this.mData).getYMin(YAxis.AxisDependency.LEFT), ((BarLineScatterCandleBubbleData)this.mData).getYMax(YAxis.AxisDependency.LEFT));
    }
    if (this.mAxisRight.isEnabled()) {
      this.mAxisRight.calculate(((BarLineScatterCandleBubbleData)this.mData).getYMin(YAxis.AxisDependency.RIGHT), ((BarLineScatterCandleBubbleData)this.mData).getYMax(YAxis.AxisDependency.RIGHT));
    }
    calculateOffsets();
  }
  
  protected void calcMinMax()
  {
    this.mXAxis.calculate(((BarLineScatterCandleBubbleData)this.mData).getXMin(), ((BarLineScatterCandleBubbleData)this.mData).getXMax());
    this.mAxisLeft.calculate(((BarLineScatterCandleBubbleData)this.mData).getYMin(YAxis.AxisDependency.LEFT), ((BarLineScatterCandleBubbleData)this.mData).getYMax(YAxis.AxisDependency.LEFT));
    this.mAxisRight.calculate(((BarLineScatterCandleBubbleData)this.mData).getYMin(YAxis.AxisDependency.RIGHT), ((BarLineScatterCandleBubbleData)this.mData).getYMax(YAxis.AxisDependency.RIGHT));
  }
  
  protected void calculateLegendOffsets(RectF paramRectF)
  {
    paramRectF.left = 0.0F;
    paramRectF.right = 0.0F;
    paramRectF.top = 0.0F;
    paramRectF.bottom = 0.0F;
    if ((this.mLegend != null) && (this.mLegend.isEnabled()) && (!this.mLegend.isDrawInsideEnabled())) {
      switch (this.mLegend.getOrientation())
      {
      default: 
        break;
      case ???: 
        switch (2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[this.mLegend.getVerticalAlignment().ordinal()])
        {
        default: 
          break;
        case 2: 
          paramRectF.bottom += Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent()) + this.mLegend.getYOffset();
          break;
        case 1: 
          paramRectF.top += Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent()) + this.mLegend.getYOffset();
        }
        break;
      case ???: 
        switch (2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendHorizontalAlignment[this.mLegend.getHorizontalAlignment().ordinal()])
        {
        default: 
          break;
        case 3: 
          switch (2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[this.mLegend.getVerticalAlignment().ordinal()])
          {
          default: 
            break;
          case 2: 
            paramRectF.bottom += Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent()) + this.mLegend.getYOffset();
            break;
          case 1: 
            paramRectF.top += Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent()) + this.mLegend.getYOffset();
          }
          break;
        case 2: 
          paramRectF.right += Math.min(this.mLegend.mNeededWidth, this.mViewPortHandler.getChartWidth() * this.mLegend.getMaxSizePercent()) + this.mLegend.getXOffset();
          break;
        case 1: 
          paramRectF.left += Math.min(this.mLegend.mNeededWidth, this.mViewPortHandler.getChartWidth() * this.mLegend.getMaxSizePercent()) + this.mLegend.getXOffset();
        }
        break;
      }
    }
  }
  
  public void calculateOffsets()
  {
    if (!this.mCustomViewPortEnabled)
    {
      calculateLegendOffsets(this.mOffsetsBuffer);
      float f2 = this.mOffsetsBuffer.left + 0.0F;
      float f5 = this.mOffsetsBuffer.top + 0.0F;
      float f1 = this.mOffsetsBuffer.right + 0.0F;
      float f6 = this.mOffsetsBuffer.bottom + 0.0F;
      float f3 = f2;
      if (this.mAxisLeft.needsOffset()) {
        f3 = f2 + this.mAxisLeft.getRequiredWidthSpace(this.mAxisRendererLeft.getPaintAxisLabels());
      }
      float f4 = f1;
      if (this.mAxisRight.needsOffset()) {
        f4 = f1 + this.mAxisRight.getRequiredWidthSpace(this.mAxisRendererRight.getPaintAxisLabels());
      }
      f2 = f5;
      f1 = f6;
      if (this.mXAxis.isEnabled())
      {
        f2 = f5;
        f1 = f6;
        if (this.mXAxis.isDrawLabelsEnabled())
        {
          float f7 = this.mXAxis.mLabelRotatedHeight + this.mXAxis.getYOffset();
          if (this.mXAxis.getPosition() == XAxis.XAxisPosition.BOTTOM)
          {
            f1 = f6 + f7;
            f2 = f5;
          }
          else if (this.mXAxis.getPosition() == XAxis.XAxisPosition.TOP)
          {
            f2 = f5 + f7;
            f1 = f6;
          }
          else
          {
            f2 = f5;
            f1 = f6;
            if (this.mXAxis.getPosition() == XAxis.XAxisPosition.BOTH_SIDED)
            {
              f1 = f6 + f7;
              f2 = f5 + f7;
            }
          }
        }
      }
      f2 += getExtraTopOffset();
      f4 += getExtraRightOffset();
      f1 += getExtraBottomOffset();
      f5 = f3 + getExtraLeftOffset();
      f3 = Utils.convertDpToPixel(this.mMinOffset);
      this.mViewPortHandler.restrainViewPort(Math.max(f3, f5), Math.max(f3, f2), Math.max(f3, f4), Math.max(f3, f1));
      if (this.mLogEnabled)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("offsetLeft: ");
        localStringBuilder.append(f5);
        localStringBuilder.append(", offsetTop: ");
        localStringBuilder.append(f2);
        localStringBuilder.append(", offsetRight: ");
        localStringBuilder.append(f4);
        localStringBuilder.append(", offsetBottom: ");
        localStringBuilder.append(f1);
        Log.i("MPAndroidChart", localStringBuilder.toString());
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("Content: ");
        localStringBuilder.append(this.mViewPortHandler.getContentRect().toString());
        Log.i("MPAndroidChart", localStringBuilder.toString());
      }
    }
    prepareOffsetMatrix();
    prepareValuePxMatrix();
  }
  
  public void centerViewTo(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    float f2 = getAxisRange(paramAxisDependency) / this.mViewPortHandler.getScaleY();
    float f1 = getXAxis().mAxisRange / this.mViewPortHandler.getScaleX();
    addViewportJob(MoveViewJob.getInstance(this.mViewPortHandler, paramFloat1 - f1 / 2.0F, paramFloat2 + f2 / 2.0F, getTransformer(paramAxisDependency), this));
  }
  
  @TargetApi(11)
  public void centerViewToAnimated(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency, long paramLong)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      MPPointD localMPPointD = getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop(), paramAxisDependency);
      float f2 = getAxisRange(paramAxisDependency) / this.mViewPortHandler.getScaleY();
      float f1 = getXAxis().mAxisRange / this.mViewPortHandler.getScaleX();
      addViewportJob(AnimatedMoveViewJob.getInstance(this.mViewPortHandler, paramFloat1 - f1 / 2.0F, paramFloat2 + f2 / 2.0F, getTransformer(paramAxisDependency), this, (float)localMPPointD.x, (float)localMPPointD.y, paramLong));
      MPPointD.recycleInstance(localMPPointD);
    }
    else
    {
      Log.e("MPAndroidChart", "Unable to execute centerViewToAnimated(...) on API level < 11");
    }
  }
  
  public void centerViewToY(float paramFloat, YAxis.AxisDependency paramAxisDependency)
  {
    float f = getAxisRange(paramAxisDependency) / this.mViewPortHandler.getScaleY();
    addViewportJob(MoveViewJob.getInstance(this.mViewPortHandler, 0.0F, paramFloat + f / 2.0F, getTransformer(paramAxisDependency), this));
  }
  
  public void computeScroll()
  {
    if ((this.mChartTouchListener instanceof BarLineChartTouchListener)) {
      ((BarLineChartTouchListener)this.mChartTouchListener).computeScroll();
    }
  }
  
  protected void drawGridBackground(Canvas paramCanvas)
  {
    if (this.mDrawGridBackground) {
      paramCanvas.drawRect(this.mViewPortHandler.getContentRect(), this.mGridBackgroundPaint);
    }
    if (this.mDrawBorders) {
      paramCanvas.drawRect(this.mViewPortHandler.getContentRect(), this.mBorderPaint);
    }
  }
  
  public void fitScreen()
  {
    Matrix localMatrix = this.mFitScreenMatrixBuffer;
    this.mViewPortHandler.fitScreen(localMatrix);
    this.mViewPortHandler.refresh(localMatrix, this, false);
    calculateOffsets();
    postInvalidate();
  }
  
  public YAxis getAxis(YAxis.AxisDependency paramAxisDependency)
  {
    if (paramAxisDependency == YAxis.AxisDependency.LEFT) {
      return this.mAxisLeft;
    }
    return this.mAxisRight;
  }
  
  public YAxis getAxisLeft()
  {
    return this.mAxisLeft;
  }
  
  protected float getAxisRange(YAxis.AxisDependency paramAxisDependency)
  {
    if (paramAxisDependency == YAxis.AxisDependency.LEFT) {
      return this.mAxisLeft.mAxisRange;
    }
    return this.mAxisRight.mAxisRange;
  }
  
  public YAxis getAxisRight()
  {
    return this.mAxisRight;
  }
  
  public IBarLineScatterCandleBubbleDataSet getDataSetByTouchPoint(float paramFloat1, float paramFloat2)
  {
    Highlight localHighlight = getHighlightByTouchPoint(paramFloat1, paramFloat2);
    if (localHighlight != null) {
      return (IBarLineScatterCandleBubbleDataSet)((BarLineScatterCandleBubbleData)this.mData).getDataSetByIndex(localHighlight.getDataSetIndex());
    }
    return null;
  }
  
  public OnDrawListener getDrawListener()
  {
    return this.mDrawListener;
  }
  
  public Entry getEntryByTouchPoint(float paramFloat1, float paramFloat2)
  {
    Highlight localHighlight = getHighlightByTouchPoint(paramFloat1, paramFloat2);
    if (localHighlight != null) {
      return ((BarLineScatterCandleBubbleData)this.mData).getEntryForHighlight(localHighlight);
    }
    return null;
  }
  
  public float getHighestVisibleX()
  {
    getTransformer(YAxis.AxisDependency.LEFT).getValuesByTouchPoint(this.mViewPortHandler.contentRight(), this.mViewPortHandler.contentBottom(), this.posForGetHighestVisibleX);
    return (float)Math.min(this.mXAxis.mAxisMaximum, this.posForGetHighestVisibleX.x);
  }
  
  public float getLowestVisibleX()
  {
    getTransformer(YAxis.AxisDependency.LEFT).getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentBottom(), this.posForGetLowestVisibleX);
    return (float)Math.max(this.mXAxis.mAxisMinimum, this.posForGetLowestVisibleX.x);
  }
  
  public int getMaxVisibleCount()
  {
    return this.mMaxVisibleCount;
  }
  
  public float getMinOffset()
  {
    return this.mMinOffset;
  }
  
  public Paint getPaint(int paramInt)
  {
    Paint localPaint = super.getPaint(paramInt);
    if (localPaint != null) {
      return localPaint;
    }
    if (paramInt != 4) {
      return null;
    }
    return this.mGridBackgroundPaint;
  }
  
  public MPPointD getPixelForValues(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    return getTransformer(paramAxisDependency).getPixelForValues(paramFloat1, paramFloat2);
  }
  
  public MPPointF getPosition(Entry paramEntry, YAxis.AxisDependency paramAxisDependency)
  {
    if (paramEntry == null) {
      return null;
    }
    this.mGetPositionBuffer[0] = paramEntry.getX();
    this.mGetPositionBuffer[1] = paramEntry.getY();
    getTransformer(paramAxisDependency).pointValuesToPixel(this.mGetPositionBuffer);
    paramEntry = this.mGetPositionBuffer;
    return MPPointF.getInstance(paramEntry[0], paramEntry[1]);
  }
  
  public YAxisRenderer getRendererLeftYAxis()
  {
    return this.mAxisRendererLeft;
  }
  
  public YAxisRenderer getRendererRightYAxis()
  {
    return this.mAxisRendererRight;
  }
  
  public XAxisRenderer getRendererXAxis()
  {
    return this.mXAxisRenderer;
  }
  
  public float getScaleX()
  {
    if (this.mViewPortHandler == null) {
      return 1.0F;
    }
    return this.mViewPortHandler.getScaleX();
  }
  
  public float getScaleY()
  {
    if (this.mViewPortHandler == null) {
      return 1.0F;
    }
    return this.mViewPortHandler.getScaleY();
  }
  
  public Transformer getTransformer(YAxis.AxisDependency paramAxisDependency)
  {
    if (paramAxisDependency == YAxis.AxisDependency.LEFT) {
      return this.mLeftAxisTransformer;
    }
    return this.mRightAxisTransformer;
  }
  
  public MPPointD getValuesByTouchPoint(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    MPPointD localMPPointD = MPPointD.getInstance(0.0D, 0.0D);
    getValuesByTouchPoint(paramFloat1, paramFloat2, paramAxisDependency, localMPPointD);
    return localMPPointD;
  }
  
  public void getValuesByTouchPoint(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency, MPPointD paramMPPointD)
  {
    getTransformer(paramAxisDependency).getValuesByTouchPoint(paramFloat1, paramFloat2, paramMPPointD);
  }
  
  public float getVisibleXRange()
  {
    return Math.abs(getHighestVisibleX() - getLowestVisibleX());
  }
  
  public float getYChartMax()
  {
    return Math.max(this.mAxisLeft.mAxisMaximum, this.mAxisRight.mAxisMaximum);
  }
  
  public float getYChartMin()
  {
    return Math.min(this.mAxisLeft.mAxisMinimum, this.mAxisRight.mAxisMinimum);
  }
  
  public boolean hasNoDragOffset()
  {
    return this.mViewPortHandler.hasNoDragOffset();
  }
  
  protected void init()
  {
    super.init();
    this.mAxisLeft = new YAxis(YAxis.AxisDependency.LEFT);
    this.mAxisRight = new YAxis(YAxis.AxisDependency.RIGHT);
    this.mLeftAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mRightAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mAxisRendererLeft = new YAxisRenderer(this.mViewPortHandler, this.mAxisLeft, this.mLeftAxisTransformer);
    this.mAxisRendererRight = new YAxisRenderer(this.mViewPortHandler, this.mAxisRight, this.mRightAxisTransformer);
    this.mXAxisRenderer = new XAxisRenderer(this.mViewPortHandler, this.mXAxis, this.mLeftAxisTransformer);
    setHighlighter(new ChartHighlighter(this));
    this.mChartTouchListener = new BarLineChartTouchListener(this, this.mViewPortHandler.getMatrixTouch(), 3.0F);
    this.mGridBackgroundPaint = new Paint();
    this.mGridBackgroundPaint.setStyle(Paint.Style.FILL);
    this.mGridBackgroundPaint.setColor(Color.rgb(240, 240, 240));
    this.mBorderPaint = new Paint();
    this.mBorderPaint.setStyle(Paint.Style.STROKE);
    this.mBorderPaint.setColor(-16777216);
    this.mBorderPaint.setStrokeWidth(Utils.convertDpToPixel(1.0F));
  }
  
  public boolean isAnyAxisInverted()
  {
    if (this.mAxisLeft.isInverted()) {
      return true;
    }
    return this.mAxisRight.isInverted();
  }
  
  public boolean isAutoScaleMinMaxEnabled()
  {
    return this.mAutoScaleMinMaxEnabled;
  }
  
  public boolean isClipValuesToContentEnabled()
  {
    return this.mClipValuesToContent;
  }
  
  public boolean isDoubleTapToZoomEnabled()
  {
    return this.mDoubleTapToZoomEnabled;
  }
  
  public boolean isDragEnabled()
  {
    boolean bool;
    if ((!this.mDragXEnabled) && (!this.mDragYEnabled)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isDragXEnabled()
  {
    return this.mDragXEnabled;
  }
  
  public boolean isDragYEnabled()
  {
    return this.mDragYEnabled;
  }
  
  public boolean isDrawBordersEnabled()
  {
    return this.mDrawBorders;
  }
  
  public boolean isFullyZoomedOut()
  {
    return this.mViewPortHandler.isFullyZoomedOut();
  }
  
  public boolean isHighlightPerDragEnabled()
  {
    return this.mHighlightPerDragEnabled;
  }
  
  public boolean isInverted(YAxis.AxisDependency paramAxisDependency)
  {
    return getAxis(paramAxisDependency).isInverted();
  }
  
  public boolean isKeepPositionOnRotation()
  {
    return this.mKeepPositionOnRotation;
  }
  
  public boolean isPinchZoomEnabled()
  {
    return this.mPinchZoomEnabled;
  }
  
  public boolean isScaleXEnabled()
  {
    return this.mScaleXEnabled;
  }
  
  public boolean isScaleYEnabled()
  {
    return this.mScaleYEnabled;
  }
  
  public void moveViewTo(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    float f = getAxisRange(paramAxisDependency) / this.mViewPortHandler.getScaleY();
    addViewportJob(MoveViewJob.getInstance(this.mViewPortHandler, paramFloat1, paramFloat2 + f / 2.0F, getTransformer(paramAxisDependency), this));
  }
  
  @TargetApi(11)
  public void moveViewToAnimated(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency, long paramLong)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      MPPointD localMPPointD = getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop(), paramAxisDependency);
      float f = getAxisRange(paramAxisDependency) / this.mViewPortHandler.getScaleY();
      addViewportJob(AnimatedMoveViewJob.getInstance(this.mViewPortHandler, paramFloat1, paramFloat2 + f / 2.0F, getTransformer(paramAxisDependency), this, (float)localMPPointD.x, (float)localMPPointD.y, paramLong));
      MPPointD.recycleInstance(localMPPointD);
    }
    else
    {
      Log.e("MPAndroidChart", "Unable to execute moveViewToAnimated(...) on API level < 11");
    }
  }
  
  public void moveViewToX(float paramFloat)
  {
    addViewportJob(MoveViewJob.getInstance(this.mViewPortHandler, paramFloat, 0.0F, getTransformer(YAxis.AxisDependency.LEFT), this));
  }
  
  public void notifyDataSetChanged()
  {
    if (this.mData == null)
    {
      if (this.mLogEnabled) {
        Log.i("MPAndroidChart", "Preparing... DATA NOT SET.");
      }
      return;
    }
    if (this.mLogEnabled) {
      Log.i("MPAndroidChart", "Preparing...");
    }
    if (this.mRenderer != null) {
      this.mRenderer.initBuffers();
    }
    calcMinMax();
    this.mAxisRendererLeft.computeAxis(this.mAxisLeft.mAxisMinimum, this.mAxisLeft.mAxisMaximum, this.mAxisLeft.isInverted());
    this.mAxisRendererRight.computeAxis(this.mAxisRight.mAxisMinimum, this.mAxisRight.mAxisMaximum, this.mAxisRight.isInverted());
    this.mXAxisRenderer.computeAxis(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisMaximum, false);
    if (this.mLegend != null) {
      this.mLegendRenderer.computeLegend(this.mData);
    }
    calculateOffsets();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mData == null) {
      return;
    }
    long l1 = System.currentTimeMillis();
    drawGridBackground(paramCanvas);
    if (this.mAutoScaleMinMaxEnabled) {
      autoScale();
    }
    if (this.mAxisLeft.isEnabled()) {
      this.mAxisRendererLeft.computeAxis(this.mAxisLeft.mAxisMinimum, this.mAxisLeft.mAxisMaximum, this.mAxisLeft.isInverted());
    }
    if (this.mAxisRight.isEnabled()) {
      this.mAxisRendererRight.computeAxis(this.mAxisRight.mAxisMinimum, this.mAxisRight.mAxisMaximum, this.mAxisRight.isInverted());
    }
    if (this.mXAxis.isEnabled()) {
      this.mXAxisRenderer.computeAxis(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisMaximum, false);
    }
    this.mXAxisRenderer.renderAxisLine(paramCanvas);
    this.mAxisRendererLeft.renderAxisLine(paramCanvas);
    this.mAxisRendererRight.renderAxisLine(paramCanvas);
    if (this.mXAxis.isDrawGridLinesBehindDataEnabled()) {
      this.mXAxisRenderer.renderGridLines(paramCanvas);
    }
    if (this.mAxisLeft.isDrawGridLinesBehindDataEnabled()) {
      this.mAxisRendererLeft.renderGridLines(paramCanvas);
    }
    if (this.mAxisRight.isDrawGridLinesBehindDataEnabled()) {
      this.mAxisRendererRight.renderGridLines(paramCanvas);
    }
    if ((this.mXAxis.isEnabled()) && (this.mXAxis.isDrawLimitLinesBehindDataEnabled())) {
      this.mXAxisRenderer.renderLimitLines(paramCanvas);
    }
    if ((this.mAxisLeft.isEnabled()) && (this.mAxisLeft.isDrawLimitLinesBehindDataEnabled())) {
      this.mAxisRendererLeft.renderLimitLines(paramCanvas);
    }
    if ((this.mAxisRight.isEnabled()) && (this.mAxisRight.isDrawLimitLinesBehindDataEnabled())) {
      this.mAxisRendererRight.renderLimitLines(paramCanvas);
    }
    int i = paramCanvas.save();
    paramCanvas.clipRect(this.mViewPortHandler.getContentRect());
    this.mRenderer.drawData(paramCanvas);
    if (!this.mXAxis.isDrawGridLinesBehindDataEnabled()) {
      this.mXAxisRenderer.renderGridLines(paramCanvas);
    }
    if (!this.mAxisLeft.isDrawGridLinesBehindDataEnabled()) {
      this.mAxisRendererLeft.renderGridLines(paramCanvas);
    }
    if (!this.mAxisRight.isDrawGridLinesBehindDataEnabled()) {
      this.mAxisRendererRight.renderGridLines(paramCanvas);
    }
    if (valuesToHighlight()) {
      this.mRenderer.drawHighlighted(paramCanvas, this.mIndicesToHighlight);
    }
    paramCanvas.restoreToCount(i);
    this.mRenderer.drawExtras(paramCanvas);
    if ((this.mXAxis.isEnabled()) && (!this.mXAxis.isDrawLimitLinesBehindDataEnabled())) {
      this.mXAxisRenderer.renderLimitLines(paramCanvas);
    }
    if ((this.mAxisLeft.isEnabled()) && (!this.mAxisLeft.isDrawLimitLinesBehindDataEnabled())) {
      this.mAxisRendererLeft.renderLimitLines(paramCanvas);
    }
    if ((this.mAxisRight.isEnabled()) && (!this.mAxisRight.isDrawLimitLinesBehindDataEnabled())) {
      this.mAxisRendererRight.renderLimitLines(paramCanvas);
    }
    this.mXAxisRenderer.renderAxisLabels(paramCanvas);
    this.mAxisRendererLeft.renderAxisLabels(paramCanvas);
    this.mAxisRendererRight.renderAxisLabels(paramCanvas);
    if (isClipValuesToContentEnabled())
    {
      i = paramCanvas.save();
      paramCanvas.clipRect(this.mViewPortHandler.getContentRect());
      this.mRenderer.drawValues(paramCanvas);
      paramCanvas.restoreToCount(i);
    }
    else
    {
      this.mRenderer.drawValues(paramCanvas);
    }
    this.mLegendRenderer.renderLegend(paramCanvas);
    drawDescription(paramCanvas);
    drawMarkers(paramCanvas);
    if (this.mLogEnabled)
    {
      l1 = System.currentTimeMillis() - l1;
      this.totalTime += l1;
      this.drawCycles += 1L;
      long l2 = this.totalTime / this.drawCycles;
      paramCanvas = new StringBuilder();
      paramCanvas.append("Drawtime: ");
      paramCanvas.append(l1);
      paramCanvas.append(" ms, average: ");
      paramCanvas.append(l2);
      paramCanvas.append(" ms, cycles: ");
      paramCanvas.append(this.drawCycles);
      Log.i("MPAndroidChart", paramCanvas.toString());
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    float[] arrayOfFloat = this.mOnSizeChangedBuffer;
    arrayOfFloat[1] = 0.0F;
    arrayOfFloat[0] = 0.0F;
    if (this.mKeepPositionOnRotation)
    {
      arrayOfFloat[0] = this.mViewPortHandler.contentLeft();
      this.mOnSizeChangedBuffer[1] = this.mViewPortHandler.contentTop();
      getTransformer(YAxis.AxisDependency.LEFT).pixelsToValue(this.mOnSizeChangedBuffer);
    }
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.mKeepPositionOnRotation)
    {
      getTransformer(YAxis.AxisDependency.LEFT).pointValuesToPixel(this.mOnSizeChangedBuffer);
      this.mViewPortHandler.centerViewPort(this.mOnSizeChangedBuffer, this);
    }
    else
    {
      this.mViewPortHandler.refresh(this.mViewPortHandler.getMatrixTouch(), this, true);
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    super.onTouchEvent(paramMotionEvent);
    if ((this.mChartTouchListener != null) && (this.mData != null))
    {
      if (!this.mTouchEnabled) {
        return false;
      }
      return this.mChartTouchListener.onTouch(this, paramMotionEvent);
    }
    return false;
  }
  
  protected void prepareOffsetMatrix()
  {
    this.mRightAxisTransformer.prepareMatrixOffset(this.mAxisRight.isInverted());
    this.mLeftAxisTransformer.prepareMatrixOffset(this.mAxisLeft.isInverted());
  }
  
  protected void prepareValuePxMatrix()
  {
    if (this.mLogEnabled)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Preparing Value-Px Matrix, xmin: ");
      localStringBuilder.append(this.mXAxis.mAxisMinimum);
      localStringBuilder.append(", xmax: ");
      localStringBuilder.append(this.mXAxis.mAxisMaximum);
      localStringBuilder.append(", xdelta: ");
      localStringBuilder.append(this.mXAxis.mAxisRange);
      Log.i("MPAndroidChart", localStringBuilder.toString());
    }
    this.mRightAxisTransformer.prepareMatrixValuePx(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisRange, this.mAxisRight.mAxisRange, this.mAxisRight.mAxisMinimum);
    this.mLeftAxisTransformer.prepareMatrixValuePx(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisRange, this.mAxisLeft.mAxisRange, this.mAxisLeft.mAxisMinimum);
  }
  
  public void resetTracking()
  {
    this.totalTime = 0L;
    this.drawCycles = 0L;
  }
  
  public void resetViewPortOffsets()
  {
    this.mCustomViewPortEnabled = false;
    calculateOffsets();
  }
  
  public void resetZoom()
  {
    this.mViewPortHandler.resetZoom(this.mZoomMatrixBuffer);
    this.mViewPortHandler.refresh(this.mZoomMatrixBuffer, this, false);
    calculateOffsets();
    postInvalidate();
  }
  
  public void setAutoScaleMinMaxEnabled(boolean paramBoolean)
  {
    this.mAutoScaleMinMaxEnabled = paramBoolean;
  }
  
  public void setBorderColor(int paramInt)
  {
    this.mBorderPaint.setColor(paramInt);
  }
  
  public void setBorderWidth(float paramFloat)
  {
    this.mBorderPaint.setStrokeWidth(Utils.convertDpToPixel(paramFloat));
  }
  
  public void setClipValuesToContent(boolean paramBoolean)
  {
    this.mClipValuesToContent = paramBoolean;
  }
  
  public void setDoubleTapToZoomEnabled(boolean paramBoolean)
  {
    this.mDoubleTapToZoomEnabled = paramBoolean;
  }
  
  public void setDragEnabled(boolean paramBoolean)
  {
    this.mDragXEnabled = paramBoolean;
    this.mDragYEnabled = paramBoolean;
  }
  
  public void setDragOffsetX(float paramFloat)
  {
    this.mViewPortHandler.setDragOffsetX(paramFloat);
  }
  
  public void setDragOffsetY(float paramFloat)
  {
    this.mViewPortHandler.setDragOffsetY(paramFloat);
  }
  
  public void setDragXEnabled(boolean paramBoolean)
  {
    this.mDragXEnabled = paramBoolean;
  }
  
  public void setDragYEnabled(boolean paramBoolean)
  {
    this.mDragYEnabled = paramBoolean;
  }
  
  public void setDrawBorders(boolean paramBoolean)
  {
    this.mDrawBorders = paramBoolean;
  }
  
  public void setDrawGridBackground(boolean paramBoolean)
  {
    this.mDrawGridBackground = paramBoolean;
  }
  
  public void setGridBackgroundColor(int paramInt)
  {
    this.mGridBackgroundPaint.setColor(paramInt);
  }
  
  public void setHighlightPerDragEnabled(boolean paramBoolean)
  {
    this.mHighlightPerDragEnabled = paramBoolean;
  }
  
  public void setKeepPositionOnRotation(boolean paramBoolean)
  {
    this.mKeepPositionOnRotation = paramBoolean;
  }
  
  public void setMaxVisibleValueCount(int paramInt)
  {
    this.mMaxVisibleCount = paramInt;
  }
  
  public void setMinOffset(float paramFloat)
  {
    this.mMinOffset = paramFloat;
  }
  
  public void setOnDrawListener(OnDrawListener paramOnDrawListener)
  {
    this.mDrawListener = paramOnDrawListener;
  }
  
  public void setPaint(Paint paramPaint, int paramInt)
  {
    super.setPaint(paramPaint, paramInt);
    if (paramInt == 4) {
      this.mGridBackgroundPaint = paramPaint;
    }
  }
  
  public void setPinchZoom(boolean paramBoolean)
  {
    this.mPinchZoomEnabled = paramBoolean;
  }
  
  public void setRendererLeftYAxis(YAxisRenderer paramYAxisRenderer)
  {
    this.mAxisRendererLeft = paramYAxisRenderer;
  }
  
  public void setRendererRightYAxis(YAxisRenderer paramYAxisRenderer)
  {
    this.mAxisRendererRight = paramYAxisRenderer;
  }
  
  public void setScaleEnabled(boolean paramBoolean)
  {
    this.mScaleXEnabled = paramBoolean;
    this.mScaleYEnabled = paramBoolean;
  }
  
  public void setScaleMinima(float paramFloat1, float paramFloat2)
  {
    this.mViewPortHandler.setMinimumScaleX(paramFloat1);
    this.mViewPortHandler.setMinimumScaleY(paramFloat2);
  }
  
  public void setScaleXEnabled(boolean paramBoolean)
  {
    this.mScaleXEnabled = paramBoolean;
  }
  
  public void setScaleYEnabled(boolean paramBoolean)
  {
    this.mScaleYEnabled = paramBoolean;
  }
  
  public void setViewPortOffsets(final float paramFloat1, final float paramFloat2, final float paramFloat3, final float paramFloat4)
  {
    this.mCustomViewPortEnabled = true;
    post(new Runnable()
    {
      public void run()
      {
        BarLineChartBase.this.mViewPortHandler.restrainViewPort(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
        BarLineChartBase.this.prepareOffsetMatrix();
        BarLineChartBase.this.prepareValuePxMatrix();
      }
    });
  }
  
  public void setVisibleXRange(float paramFloat1, float paramFloat2)
  {
    paramFloat1 = this.mXAxis.mAxisRange / paramFloat1;
    paramFloat2 = this.mXAxis.mAxisRange / paramFloat2;
    this.mViewPortHandler.setMinMaxScaleX(paramFloat1, paramFloat2);
  }
  
  public void setVisibleXRangeMaximum(float paramFloat)
  {
    paramFloat = this.mXAxis.mAxisRange / paramFloat;
    this.mViewPortHandler.setMinimumScaleX(paramFloat);
  }
  
  public void setVisibleXRangeMinimum(float paramFloat)
  {
    paramFloat = this.mXAxis.mAxisRange / paramFloat;
    this.mViewPortHandler.setMaximumScaleX(paramFloat);
  }
  
  public void setVisibleYRange(float paramFloat1, float paramFloat2, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat1 = getAxisRange(paramAxisDependency) / paramFloat1;
    paramFloat2 = getAxisRange(paramAxisDependency) / paramFloat2;
    this.mViewPortHandler.setMinMaxScaleY(paramFloat1, paramFloat2);
  }
  
  public void setVisibleYRangeMaximum(float paramFloat, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat = getAxisRange(paramAxisDependency) / paramFloat;
    this.mViewPortHandler.setMinimumScaleY(paramFloat);
  }
  
  public void setVisibleYRangeMinimum(float paramFloat, YAxis.AxisDependency paramAxisDependency)
  {
    paramFloat = getAxisRange(paramAxisDependency) / paramFloat;
    this.mViewPortHandler.setMaximumScaleY(paramFloat);
  }
  
  public void setXAxisRenderer(XAxisRenderer paramXAxisRenderer)
  {
    this.mXAxisRenderer = paramXAxisRenderer;
  }
  
  public void zoom(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.mViewPortHandler.zoom(paramFloat1, paramFloat2, paramFloat3, -paramFloat4, this.mZoomMatrixBuffer);
    this.mViewPortHandler.refresh(this.mZoomMatrixBuffer, this, false);
    calculateOffsets();
    postInvalidate();
  }
  
  public void zoom(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, YAxis.AxisDependency paramAxisDependency)
  {
    addViewportJob(ZoomJob.getInstance(this.mViewPortHandler, paramFloat1, paramFloat2, paramFloat3, paramFloat4, getTransformer(paramAxisDependency), paramAxisDependency, this));
  }
  
  @TargetApi(11)
  public void zoomAndCenterAnimated(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, YAxis.AxisDependency paramAxisDependency, long paramLong)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      MPPointD localMPPointD = getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop(), paramAxisDependency);
      addViewportJob(AnimatedZoomJob.getInstance(this.mViewPortHandler, this, getTransformer(paramAxisDependency), getAxis(paramAxisDependency), this.mXAxis.mAxisRange, paramFloat1, paramFloat2, this.mViewPortHandler.getScaleX(), this.mViewPortHandler.getScaleY(), paramFloat3, paramFloat4, (float)localMPPointD.x, (float)localMPPointD.y, paramLong));
      MPPointD.recycleInstance(localMPPointD);
    }
    else
    {
      Log.e("MPAndroidChart", "Unable to execute zoomAndCenterAnimated(...) on API level < 11");
    }
  }
  
  public void zoomIn()
  {
    MPPointF localMPPointF = this.mViewPortHandler.getContentCenter();
    this.mViewPortHandler.zoomIn(localMPPointF.x, -localMPPointF.y, this.mZoomMatrixBuffer);
    this.mViewPortHandler.refresh(this.mZoomMatrixBuffer, this, false);
    MPPointF.recycleInstance(localMPPointF);
    calculateOffsets();
    postInvalidate();
  }
  
  public void zoomOut()
  {
    MPPointF localMPPointF = this.mViewPortHandler.getContentCenter();
    this.mViewPortHandler.zoomOut(localMPPointF.x, -localMPPointF.y, this.mZoomMatrixBuffer);
    this.mViewPortHandler.refresh(this.mZoomMatrixBuffer, this, false);
    MPPointF.recycleInstance(localMPPointF);
    calculateOffsets();
    postInvalidate();
  }
  
  public void zoomToCenter(float paramFloat1, float paramFloat2)
  {
    MPPointF localMPPointF = getCenterOffsets();
    Matrix localMatrix = this.mZoomMatrixBuffer;
    this.mViewPortHandler.zoom(paramFloat1, paramFloat2, localMPPointF.x, -localMPPointF.y, localMatrix);
    this.mViewPortHandler.refresh(localMatrix, this, false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/charts/BarLineChartBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */