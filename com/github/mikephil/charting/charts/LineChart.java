package com.github.mikephil.charting.charts;

import android.content.Context;
import android.util.AttributeSet;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.renderer.LineChartRenderer;

public class LineChart
  extends BarLineChartBase<LineData>
  implements LineDataProvider
{
  public LineChart(Context paramContext)
  {
    super(paramContext);
  }
  
  public LineChart(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public LineChart(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public LineData getLineData()
  {
    return (LineData)this.mData;
  }
  
  protected void init()
  {
    super.init();
    this.mRenderer = new LineChartRenderer(this, this.mAnimator, this.mViewPortHandler);
  }
  
  protected void onDetachedFromWindow()
  {
    if ((this.mRenderer != null) && ((this.mRenderer instanceof LineChartRenderer))) {
      ((LineChartRenderer)this.mRenderer).releaseBitmap();
    }
    super.onDetachedFromWindow();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/charts/LineChart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */