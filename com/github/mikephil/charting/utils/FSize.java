package com.github.mikephil.charting.utils;

import java.util.List;

public final class FSize
  extends ObjectPool.Poolable
{
  private static ObjectPool<FSize> pool = ObjectPool.create(256, new FSize(0.0F, 0.0F));
  public float height;
  public float width;
  
  static
  {
    pool.setReplenishPercentage(0.5F);
  }
  
  public FSize() {}
  
  public FSize(float paramFloat1, float paramFloat2)
  {
    this.width = paramFloat1;
    this.height = paramFloat2;
  }
  
  public static FSize getInstance(float paramFloat1, float paramFloat2)
  {
    FSize localFSize = (FSize)pool.get();
    localFSize.width = paramFloat1;
    localFSize.height = paramFloat2;
    return localFSize;
  }
  
  public static void recycleInstance(FSize paramFSize)
  {
    pool.recycle(paramFSize);
  }
  
  public static void recycleInstances(List<FSize> paramList)
  {
    pool.recycle(paramList);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    if (paramObject == null) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof FSize))
    {
      paramObject = (FSize)paramObject;
      boolean bool1 = bool2;
      if (this.width == ((FSize)paramObject).width)
      {
        bool1 = bool2;
        if (this.height == ((FSize)paramObject).height) {
          bool1 = true;
        }
      }
      return bool1;
    }
    return false;
  }
  
  public int hashCode()
  {
    return Float.floatToIntBits(this.width) ^ Float.floatToIntBits(this.height);
  }
  
  protected ObjectPool.Poolable instantiate()
  {
    return new FSize(0.0F, 0.0F);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.width);
    localStringBuilder.append("x");
    localStringBuilder.append(this.height);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/utils/FSize.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */