package com.github.mikephil.charting.utils;

import android.os.Environment;
import android.util.Log;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileUtils
{
  private static final String LOG = "MPChart-FileUtils";
  
  /* Error */
  public static List<BarEntry> loadBarEntriesFromAssets(android.content.res.AssetManager paramAssetManager, String paramString)
  {
    // Byte code:
    //   0: new 19	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 20	java/util/ArrayList:<init>	()V
    //   7: astore 6
    //   9: aconst_null
    //   10: astore 4
    //   12: aconst_null
    //   13: astore 5
    //   15: aload 5
    //   17: astore_2
    //   18: new 22	java/io/BufferedReader
    //   21: astore_3
    //   22: aload 5
    //   24: astore_2
    //   25: new 24	java/io/InputStreamReader
    //   28: astore 7
    //   30: aload 5
    //   32: astore_2
    //   33: aload 7
    //   35: aload_0
    //   36: aload_1
    //   37: invokevirtual 30	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   40: ldc 32
    //   42: invokespecial 35	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   45: aload 5
    //   47: astore_2
    //   48: aload_3
    //   49: aload 7
    //   51: invokespecial 38	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   54: aload_3
    //   55: invokevirtual 42	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   58: astore_0
    //   59: aload_0
    //   60: ifnull +47 -> 107
    //   63: aload_0
    //   64: ldc 44
    //   66: invokevirtual 50	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   69: astore_1
    //   70: new 52	com/github/mikephil/charting/data/BarEntry
    //   73: astore_0
    //   74: aload_0
    //   75: aload_1
    //   76: iconst_1
    //   77: aaload
    //   78: invokestatic 58	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   81: aload_1
    //   82: iconst_0
    //   83: aaload
    //   84: invokestatic 58	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   87: invokespecial 61	com/github/mikephil/charting/data/BarEntry:<init>	(FF)V
    //   90: aload 6
    //   92: aload_0
    //   93: invokeinterface 67 2 0
    //   98: pop
    //   99: aload_3
    //   100: invokevirtual 42	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   103: astore_0
    //   104: goto -45 -> 59
    //   107: aload_3
    //   108: invokevirtual 70	java/io/BufferedReader:close	()V
    //   111: goto +57 -> 168
    //   114: astore_0
    //   115: goto +56 -> 171
    //   118: astore_1
    //   119: aload_3
    //   120: astore_0
    //   121: goto +13 -> 134
    //   124: astore_0
    //   125: aload_2
    //   126: astore_3
    //   127: goto +44 -> 171
    //   130: astore_1
    //   131: aload 4
    //   133: astore_0
    //   134: aload_0
    //   135: astore_2
    //   136: ldc 8
    //   138: aload_1
    //   139: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   142: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   145: pop
    //   146: aload_0
    //   147: ifnull +21 -> 168
    //   150: aload_0
    //   151: invokevirtual 70	java/io/BufferedReader:close	()V
    //   154: goto +14 -> 168
    //   157: astore_0
    //   158: ldc 8
    //   160: aload_0
    //   161: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   164: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   167: pop
    //   168: aload 6
    //   170: areturn
    //   171: aload_3
    //   172: ifnull +21 -> 193
    //   175: aload_3
    //   176: invokevirtual 70	java/io/BufferedReader:close	()V
    //   179: goto +14 -> 193
    //   182: astore_1
    //   183: ldc 8
    //   185: aload_1
    //   186: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   189: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   192: pop
    //   193: aload_0
    //   194: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	195	0	paramAssetManager	android.content.res.AssetManager
    //   0	195	1	paramString	String
    //   17	119	2	localObject1	Object
    //   21	155	3	localObject2	Object
    //   10	122	4	localObject3	Object
    //   13	33	5	localObject4	Object
    //   7	162	6	localArrayList	ArrayList
    //   28	22	7	localInputStreamReader	java.io.InputStreamReader
    // Exception table:
    //   from	to	target	type
    //   54	59	114	finally
    //   63	104	114	finally
    //   54	59	118	java/io/IOException
    //   63	104	118	java/io/IOException
    //   18	22	124	finally
    //   25	30	124	finally
    //   33	45	124	finally
    //   48	54	124	finally
    //   136	146	124	finally
    //   18	22	130	java/io/IOException
    //   25	30	130	java/io/IOException
    //   33	45	130	java/io/IOException
    //   48	54	130	java/io/IOException
    //   107	111	157	java/io/IOException
    //   150	154	157	java/io/IOException
    //   175	179	182	java/io/IOException
  }
  
  /* Error */
  public static List<Entry> loadEntriesFromAssets(android.content.res.AssetManager paramAssetManager, String paramString)
  {
    // Byte code:
    //   0: new 19	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 20	java/util/ArrayList:<init>	()V
    //   7: astore 8
    //   9: aconst_null
    //   10: astore 6
    //   12: aconst_null
    //   13: astore 7
    //   15: aload 7
    //   17: astore 4
    //   19: new 22	java/io/BufferedReader
    //   22: astore 5
    //   24: aload 7
    //   26: astore 4
    //   28: new 24	java/io/InputStreamReader
    //   31: astore 9
    //   33: aload 7
    //   35: astore 4
    //   37: aload 9
    //   39: aload_0
    //   40: aload_1
    //   41: invokevirtual 30	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   44: ldc 32
    //   46: invokespecial 35	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   49: aload 7
    //   51: astore 4
    //   53: aload 5
    //   55: aload 9
    //   57: invokespecial 38	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   60: aload 5
    //   62: invokevirtual 42	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   65: astore_0
    //   66: aload_0
    //   67: ifnull +121 -> 188
    //   70: aload_0
    //   71: ldc 44
    //   73: invokevirtual 50	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   76: astore_0
    //   77: aload_0
    //   78: arraylength
    //   79: istore_3
    //   80: iconst_0
    //   81: istore_2
    //   82: iload_3
    //   83: iconst_2
    //   84: if_icmpgt +35 -> 119
    //   87: new 84	com/github/mikephil/charting/data/Entry
    //   90: astore_1
    //   91: aload_1
    //   92: aload_0
    //   93: iconst_1
    //   94: aaload
    //   95: invokestatic 58	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   98: aload_0
    //   99: iconst_0
    //   100: aaload
    //   101: invokestatic 58	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   104: invokespecial 85	com/github/mikephil/charting/data/Entry:<init>	(FF)V
    //   107: aload 8
    //   109: aload_1
    //   110: invokeinterface 67 2 0
    //   115: pop
    //   116: goto +63 -> 179
    //   119: aload_0
    //   120: arraylength
    //   121: iconst_1
    //   122: isub
    //   123: newarray <illegal type>
    //   125: astore 4
    //   127: iload_2
    //   128: aload 4
    //   130: arraylength
    //   131: if_icmpge +19 -> 150
    //   134: aload 4
    //   136: iload_2
    //   137: aload_0
    //   138: iload_2
    //   139: aaload
    //   140: invokestatic 58	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   143: fastore
    //   144: iinc 2 1
    //   147: goto -20 -> 127
    //   150: new 52	com/github/mikephil/charting/data/BarEntry
    //   153: astore_1
    //   154: aload_1
    //   155: aload_0
    //   156: aload_0
    //   157: arraylength
    //   158: iconst_1
    //   159: isub
    //   160: aaload
    //   161: invokestatic 91	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   164: i2f
    //   165: aload 4
    //   167: invokespecial 94	com/github/mikephil/charting/data/BarEntry:<init>	(F[F)V
    //   170: aload 8
    //   172: aload_1
    //   173: invokeinterface 67 2 0
    //   178: pop
    //   179: aload 5
    //   181: invokevirtual 42	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   184: astore_0
    //   185: goto -119 -> 66
    //   188: aload 5
    //   190: invokevirtual 70	java/io/BufferedReader:close	()V
    //   193: goto +61 -> 254
    //   196: astore_0
    //   197: goto +60 -> 257
    //   200: astore_1
    //   201: aload 5
    //   203: astore_0
    //   204: goto +15 -> 219
    //   207: astore_0
    //   208: aload 4
    //   210: astore 5
    //   212: goto +45 -> 257
    //   215: astore_1
    //   216: aload 6
    //   218: astore_0
    //   219: aload_0
    //   220: astore 4
    //   222: ldc 8
    //   224: aload_1
    //   225: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   228: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   231: pop
    //   232: aload_0
    //   233: ifnull +21 -> 254
    //   236: aload_0
    //   237: invokevirtual 70	java/io/BufferedReader:close	()V
    //   240: goto +14 -> 254
    //   243: astore_0
    //   244: ldc 8
    //   246: aload_0
    //   247: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   250: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   253: pop
    //   254: aload 8
    //   256: areturn
    //   257: aload 5
    //   259: ifnull +22 -> 281
    //   262: aload 5
    //   264: invokevirtual 70	java/io/BufferedReader:close	()V
    //   267: goto +14 -> 281
    //   270: astore_1
    //   271: ldc 8
    //   273: aload_1
    //   274: invokevirtual 73	java/io/IOException:toString	()Ljava/lang/String;
    //   277: invokestatic 79	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   280: pop
    //   281: aload_0
    //   282: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	283	0	paramAssetManager	android.content.res.AssetManager
    //   0	283	1	paramString	String
    //   81	64	2	i	int
    //   79	6	3	j	int
    //   17	204	4	localObject1	Object
    //   22	241	5	localObject2	Object
    //   10	207	6	localObject3	Object
    //   13	37	7	localObject4	Object
    //   7	248	8	localArrayList	ArrayList
    //   31	25	9	localInputStreamReader	java.io.InputStreamReader
    // Exception table:
    //   from	to	target	type
    //   60	66	196	finally
    //   70	80	196	finally
    //   87	116	196	finally
    //   119	127	196	finally
    //   127	144	196	finally
    //   150	179	196	finally
    //   179	185	196	finally
    //   60	66	200	java/io/IOException
    //   70	80	200	java/io/IOException
    //   87	116	200	java/io/IOException
    //   119	127	200	java/io/IOException
    //   127	144	200	java/io/IOException
    //   150	179	200	java/io/IOException
    //   179	185	200	java/io/IOException
    //   19	24	207	finally
    //   28	33	207	finally
    //   37	49	207	finally
    //   53	60	207	finally
    //   222	232	207	finally
    //   19	24	215	java/io/IOException
    //   28	33	215	java/io/IOException
    //   37	49	215	java/io/IOException
    //   53	60	215	java/io/IOException
    //   188	193	243	java/io/IOException
    //   236	240	243	java/io/IOException
    //   262	267	270	java/io/IOException
  }
  
  public static List<Entry> loadEntriesFromFile(String paramString)
  {
    Object localObject1 = new File(Environment.getExternalStorageDirectory(), paramString);
    paramString = new ArrayList();
    try
    {
      BufferedReader localBufferedReader = new java/io/BufferedReader;
      Object localObject2 = new java/io/FileReader;
      ((FileReader)localObject2).<init>((File)localObject1);
      localBufferedReader.<init>((Reader)localObject2);
      for (;;)
      {
        localObject1 = localBufferedReader.readLine();
        if (localObject1 == null) {
          break;
        }
        localObject1 = ((String)localObject1).split("#");
        int j = localObject1.length;
        int i = 0;
        if (j <= 2)
        {
          localObject2 = new com/github/mikephil/charting/data/Entry;
          ((Entry)localObject2).<init>(Float.parseFloat(localObject1[0]), Integer.parseInt(localObject1[1]));
          paramString.add(localObject2);
        }
        else
        {
          localObject2 = new float[localObject1.length - 1];
          while (i < localObject2.length)
          {
            localObject2[i] = Float.parseFloat(localObject1[i]);
            i++;
          }
          BarEntry localBarEntry = new com/github/mikephil/charting/data/BarEntry;
          localBarEntry.<init>(Integer.parseInt(localObject1[(localObject1.length - 1)]), (float[])localObject2);
          paramString.add(localBarEntry);
        }
      }
      return paramString;
    }
    catch (IOException localIOException)
    {
      Log.e("MPChart-FileUtils", localIOException.toString());
    }
  }
  
  public static void saveToSdCard(List<Entry> paramList, String paramString)
  {
    Object localObject1 = new File(Environment.getExternalStorageDirectory(), paramString);
    if (!((File)localObject1).exists()) {
      try
      {
        ((File)localObject1).createNewFile();
      }
      catch (IOException paramString)
      {
        Log.e("MPChart-FileUtils", paramString.toString());
      }
    }
    try
    {
      paramString = new java/io/BufferedWriter;
      Object localObject2 = new java/io/FileWriter;
      ((FileWriter)localObject2).<init>((File)localObject1, true);
      paramString.<init>((Writer)localObject2);
      localObject2 = paramList.iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject1 = (Entry)((Iterator)localObject2).next();
        paramList = new java/lang/StringBuilder;
        paramList.<init>();
        paramList.append(((Entry)localObject1).getY());
        paramList.append("#");
        paramList.append(((Entry)localObject1).getX());
        paramString.append(paramList.toString());
        paramString.newLine();
      }
      paramString.close();
    }
    catch (IOException paramList)
    {
      Log.e("MPChart-FileUtils", paramList.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/utils/FileUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */