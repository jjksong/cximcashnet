package com.github.mikephil.charting.utils;

import java.util.List;

public class ObjectPool<T extends Poolable>
{
  private static int ids;
  private int desiredCapacity;
  private T modelObject;
  private Object[] objects;
  private int objectsPointer;
  private int poolId;
  private float replenishPercentage;
  
  private ObjectPool(int paramInt, T paramT)
  {
    if (paramInt > 0)
    {
      this.desiredCapacity = paramInt;
      this.objects = new Object[this.desiredCapacity];
      this.objectsPointer = 0;
      this.modelObject = paramT;
      this.replenishPercentage = 1.0F;
      refillPool();
      return;
    }
    throw new IllegalArgumentException("Object Pool must be instantiated with a capacity greater than 0!");
  }
  
  public static ObjectPool create(int paramInt, Poolable paramPoolable)
  {
    try
    {
      ObjectPool localObjectPool = new com/github/mikephil/charting/utils/ObjectPool;
      localObjectPool.<init>(paramInt, paramPoolable);
      localObjectPool.poolId = ids;
      ids += 1;
      return localObjectPool;
    }
    finally
    {
      paramPoolable = finally;
      throw paramPoolable;
    }
  }
  
  private void refillPool()
  {
    refillPool(this.replenishPercentage);
  }
  
  private void refillPool(float paramFloat)
  {
    int k = this.desiredCapacity;
    int j = (int)(k * paramFloat);
    int i;
    if (j < 1)
    {
      i = 1;
    }
    else
    {
      i = j;
      if (j > k) {
        i = k;
      }
    }
    for (j = 0; j < i; j++) {
      this.objects[j] = this.modelObject.instantiate();
    }
    this.objectsPointer = (i - 1);
  }
  
  private void resizePool()
  {
    int j = this.desiredCapacity;
    this.desiredCapacity = (j * 2);
    Object[] arrayOfObject = new Object[this.desiredCapacity];
    for (int i = 0; i < j; i++) {
      arrayOfObject[i] = this.objects[i];
    }
    this.objects = arrayOfObject;
  }
  
  public T get()
  {
    try
    {
      if ((this.objectsPointer == -1) && (this.replenishPercentage > 0.0F)) {
        refillPool();
      }
      Poolable localPoolable = (Poolable)this.objects[this.objectsPointer];
      localPoolable.currentOwnerId = Poolable.NO_OWNER;
      this.objectsPointer -= 1;
      return localPoolable;
    }
    finally {}
  }
  
  public int getPoolCapacity()
  {
    return this.objects.length;
  }
  
  public int getPoolCount()
  {
    return this.objectsPointer + 1;
  }
  
  public int getPoolId()
  {
    return this.poolId;
  }
  
  public float getReplenishPercentage()
  {
    return this.replenishPercentage;
  }
  
  public void recycle(T paramT)
  {
    try
    {
      if (paramT.currentOwnerId != Poolable.NO_OWNER)
      {
        if (paramT.currentOwnerId == this.poolId)
        {
          paramT = new java/lang/IllegalArgumentException;
          paramT.<init>("The object passed is already stored in this pool!");
          throw paramT;
        }
        IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("The object to recycle already belongs to poolId ");
        localStringBuilder.append(paramT.currentOwnerId);
        localStringBuilder.append(".  Object cannot belong to two different pool instances simultaneously!");
        localIllegalArgumentException.<init>(localStringBuilder.toString());
        throw localIllegalArgumentException;
      }
      this.objectsPointer += 1;
      if (this.objectsPointer >= this.objects.length) {
        resizePool();
      }
      paramT.currentOwnerId = this.poolId;
      this.objects[this.objectsPointer] = paramT;
      return;
    }
    finally {}
  }
  
  public void recycle(List<T> paramList)
  {
    try
    {
      while (paramList.size() + this.objectsPointer + 1 > this.desiredCapacity) {
        resizePool();
      }
      int j = paramList.size();
      for (int i = 0; i < j; i++)
      {
        Poolable localPoolable = (Poolable)paramList.get(i);
        if (localPoolable.currentOwnerId != Poolable.NO_OWNER)
        {
          if (localPoolable.currentOwnerId == this.poolId)
          {
            paramList = new java/lang/IllegalArgumentException;
            paramList.<init>("The object passed is already stored in this pool!");
            throw paramList;
          }
          paramList = new java/lang/IllegalArgumentException;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("The object to recycle already belongs to poolId ");
          localStringBuilder.append(localPoolable.currentOwnerId);
          localStringBuilder.append(".  Object cannot belong to two different pool instances simultaneously!");
          paramList.<init>(localStringBuilder.toString());
          throw paramList;
        }
        localPoolable.currentOwnerId = this.poolId;
        this.objects[(this.objectsPointer + 1 + i)] = localPoolable;
      }
      this.objectsPointer += j;
      return;
    }
    finally {}
  }
  
  public void setReplenishPercentage(float paramFloat)
  {
    float f;
    if (paramFloat > 1.0F)
    {
      f = 1.0F;
    }
    else
    {
      f = paramFloat;
      if (paramFloat < 0.0F) {
        f = 0.0F;
      }
    }
    this.replenishPercentage = f;
  }
  
  public static abstract class Poolable
  {
    public static int NO_OWNER = -1;
    int currentOwnerId = NO_OWNER;
    
    protected abstract Poolable instantiate();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/github/mikephil/charting/utils/ObjectPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */