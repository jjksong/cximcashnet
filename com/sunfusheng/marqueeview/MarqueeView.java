package com.sunfusheng.marqueeview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.annotation.AnimRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.util.ArrayList;
import java.util.List;

public class MarqueeView
  extends ViewFlipper
{
  private static final int DIRECTION_BOTTOM_TO_TOP = 0;
  private static final int DIRECTION_LEFT_TO_RIGHT = 3;
  private static final int DIRECTION_RIGHT_TO_LEFT = 2;
  private static final int DIRECTION_TOP_TO_BOTTOM = 1;
  private static final int GRAVITY_CENTER = 1;
  private static final int GRAVITY_LEFT = 0;
  private static final int GRAVITY_RIGHT = 2;
  private int animDuration = 1000;
  private int direction = 0;
  private int gravity = 19;
  private boolean hasSetAnimDuration = false;
  private boolean hasSetDirection = false;
  @AnimRes
  private int inAnimResId = R.anim.anim_bottom_in;
  private int interval = 3000;
  private List<? extends CharSequence> notices = new ArrayList();
  private OnItemClickListener onItemClickListener;
  @AnimRes
  private int outAnimResId = R.anim.anim_top_out;
  private int position;
  private boolean singleLine = false;
  private int textColor = -1;
  private int textSize = 14;
  
  public MarqueeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet, 0);
  }
  
  private TextView createTextView(CharSequence paramCharSequence)
  {
    TextView localTextView2 = (TextView)getChildAt((getDisplayedChild() + 1) % 3);
    TextView localTextView1 = localTextView2;
    if (localTextView2 == null)
    {
      localTextView1 = new TextView(getContext());
      localTextView1.setGravity(this.gravity);
      localTextView1.setTextColor(this.textColor);
      localTextView1.setTextSize(this.textSize);
      localTextView1.setSingleLine(this.singleLine);
    }
    localTextView1.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (MarqueeView.this.onItemClickListener != null) {
          MarqueeView.this.onItemClickListener.onItemClick(MarqueeView.this.getPosition(), (TextView)paramAnonymousView);
        }
      }
    });
    localTextView1.setText(paramCharSequence);
    localTextView1.setTag(Integer.valueOf(this.position));
    return localTextView1;
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MarqueeViewStyle, paramInt, 0);
    this.interval = paramAttributeSet.getInteger(R.styleable.MarqueeViewStyle_mvInterval, this.interval);
    this.hasSetAnimDuration = paramAttributeSet.hasValue(R.styleable.MarqueeViewStyle_mvAnimDuration);
    this.animDuration = paramAttributeSet.getInteger(R.styleable.MarqueeViewStyle_mvAnimDuration, this.animDuration);
    this.singleLine = paramAttributeSet.getBoolean(R.styleable.MarqueeViewStyle_mvSingleLine, false);
    if (paramAttributeSet.hasValue(R.styleable.MarqueeViewStyle_mvTextSize))
    {
      this.textSize = ((int)paramAttributeSet.getDimension(R.styleable.MarqueeViewStyle_mvTextSize, this.textSize));
      this.textSize = Utils.px2sp(paramContext, this.textSize);
    }
    this.textColor = paramAttributeSet.getColor(R.styleable.MarqueeViewStyle_mvTextColor, this.textColor);
    switch (paramAttributeSet.getInt(R.styleable.MarqueeViewStyle_mvGravity, 0))
    {
    default: 
      break;
    case 2: 
      this.gravity = 21;
      break;
    case 1: 
      this.gravity = 17;
      break;
    case 0: 
      this.gravity = 19;
    }
    this.hasSetDirection = paramAttributeSet.hasValue(R.styleable.MarqueeViewStyle_mvDirection);
    this.direction = paramAttributeSet.getInt(R.styleable.MarqueeViewStyle_mvDirection, this.direction);
    if (this.hasSetDirection)
    {
      switch (this.direction)
      {
      default: 
        break;
      case 3: 
        this.inAnimResId = R.anim.anim_left_in;
        this.outAnimResId = R.anim.anim_right_out;
        break;
      case 2: 
        this.inAnimResId = R.anim.anim_right_in;
        this.outAnimResId = R.anim.anim_left_out;
        break;
      case 1: 
        this.inAnimResId = R.anim.anim_top_in;
        this.outAnimResId = R.anim.anim_bottom_out;
        break;
      case 0: 
        this.inAnimResId = R.anim.anim_bottom_in;
        this.outAnimResId = R.anim.anim_top_out;
        break;
      }
    }
    else
    {
      this.inAnimResId = R.anim.anim_bottom_in;
      this.outAnimResId = R.anim.anim_top_out;
    }
    paramAttributeSet.recycle();
    setFlipInterval(this.interval);
  }
  
  private void postStart(@AnimRes final int paramInt1, @AnimRes final int paramInt2)
  {
    post(new Runnable()
    {
      public void run()
      {
        MarqueeView.this.start(paramInt1, paramInt2);
      }
    });
  }
  
  private void setInAndOutAnimation(@AnimRes int paramInt1, @AnimRes int paramInt2)
  {
    Animation localAnimation = AnimationUtils.loadAnimation(getContext(), paramInt1);
    if (this.hasSetAnimDuration) {
      localAnimation.setDuration(this.animDuration);
    }
    setInAnimation(localAnimation);
    localAnimation = AnimationUtils.loadAnimation(getContext(), paramInt2);
    if (this.hasSetAnimDuration) {
      localAnimation.setDuration(this.animDuration);
    }
    setOutAnimation(localAnimation);
  }
  
  private void start(@AnimRes int paramInt1, @AnimRes int paramInt2)
  {
    removeAllViews();
    clearAnimation();
    this.position = 0;
    addView(createTextView((CharSequence)this.notices.get(this.position)));
    if (this.notices.size() > 1)
    {
      setInAndOutAnimation(paramInt1, paramInt2);
      startFlipping();
    }
    if (getInAnimation() != null) {
      getInAnimation().setAnimationListener(new Animation.AnimationListener()
      {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
          MarqueeView.access$208(MarqueeView.this);
          if (MarqueeView.this.position >= MarqueeView.this.notices.size()) {
            MarqueeView.access$202(MarqueeView.this, 0);
          }
          paramAnonymousAnimation = MarqueeView.this;
          paramAnonymousAnimation = paramAnonymousAnimation.createTextView((CharSequence)paramAnonymousAnimation.notices.get(MarqueeView.this.position));
          if (paramAnonymousAnimation.getParent() == null) {
            MarqueeView.this.addView(paramAnonymousAnimation);
          }
        }
        
        public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
        
        public void onAnimationStart(Animation paramAnonymousAnimation) {}
      });
    }
  }
  
  private void startWithFixedWidth(String paramString, @AnimRes int paramInt1, @AnimRes int paramInt2)
  {
    int k = paramString.length();
    int i = Utils.px2dip(getContext(), getWidth());
    if (i != 0)
    {
      int i3 = i / this.textSize;
      ArrayList localArrayList = new ArrayList();
      if (k <= i3)
      {
        localArrayList.add(paramString);
      }
      else
      {
        int i2 = k / i3;
        i = 0;
        int j;
        if (k % i3 != 0) {
          j = 1;
        } else {
          j = 0;
        }
        for (;;)
        {
          int n = i;
          if (n >= i2 + j) {
            break;
          }
          int m = n + 1;
          int i1 = m * i3;
          i = i1;
          if (i1 >= k) {
            i = k;
          }
          localArrayList.add(paramString.substring(n * i3, i));
          i = m;
        }
      }
      if (this.notices == null) {
        this.notices = new ArrayList();
      }
      this.notices.clear();
      this.notices.addAll(localArrayList);
      postStart(paramInt1, paramInt2);
      return;
    }
    throw new RuntimeException("Please set the width of MarqueeView !");
  }
  
  public List<? extends CharSequence> getNotices()
  {
    return this.notices;
  }
  
  public int getPosition()
  {
    return ((Integer)getCurrentView().getTag()).intValue();
  }
  
  public void setNotices(List<? extends CharSequence> paramList)
  {
    this.notices = paramList;
  }
  
  public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener)
  {
    this.onItemClickListener = paramOnItemClickListener;
  }
  
  public void startWithList(List<? extends CharSequence> paramList)
  {
    startWithList(paramList, this.inAnimResId, this.outAnimResId);
  }
  
  public void startWithList(List<? extends CharSequence> paramList, @AnimRes int paramInt1, @AnimRes int paramInt2)
  {
    if (Utils.isEmpty(paramList)) {
      return;
    }
    setNotices(paramList);
    postStart(paramInt1, paramInt2);
  }
  
  public void startWithText(String paramString)
  {
    startWithText(paramString, this.inAnimResId, this.outAnimResId);
  }
  
  public void startWithText(final String paramString, @AnimRes final int paramInt1, @AnimRes final int paramInt2)
  {
    if (TextUtils.isEmpty(paramString)) {
      return;
    }
    getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
    {
      public void onGlobalLayout()
      {
        if (Build.VERSION.SDK_INT >= 16) {
          MarqueeView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } else {
          MarqueeView.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        MarqueeView.this.startWithFixedWidth(paramString, paramInt1, paramInt2);
      }
    });
  }
  
  public static abstract interface OnItemClickListener
  {
    public abstract void onItemClick(int paramInt, TextView paramTextView);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/sunfusheng/marqueeview/MarqueeView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */