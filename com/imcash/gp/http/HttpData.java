package com.imcash.gp.http;

import org.json.JSONObject;

public class HttpData
{
  public static String BASE_URL_HTTP = "https://www.imcash.ltd";
  public static final HttpTypeEnum[] DIS_HTTP_TYPE_ENUM = { HttpTypeEnum.Bfc_Back_Join };
  public static final String ERRO_CACHE_USELESS = "-105";
  public static final String ERRO_TOKEN_USELESS = "000002";
  public static String Face_URL_HTTP = "https://openapi.faceid.com/";
  public static final String[] HTTPS_REQUEST_ARRAY = { "" };
  public static final int MainServer = 1;
  public static final String RT_RULE_URL = "";
  public static String SEARCH_URL_HTTP = "";
  public static final String STATAND_CODE = "success";
  public static final int SearchServer = 2;
  public static final int TIME_OUT = 6000;
  
  public static String getCode(HttpInfo paramHttpInfo)
  {
    return paramHttpInfo.mJsonObj.optString("status", "");
  }
  
  public static String getCodeMessage(HttpInfo paramHttpInfo)
  {
    return paramHttpInfo.mJsonObj.optString("msg", "");
  }
  
  public static String getErroMsg(HttpInfo paramHttpInfo)
  {
    return paramHttpInfo.mJsonObj.optString("msg", "");
  }
  
  public static boolean isDisHttpType(HttpTypeEnum paramHttpTypeEnum)
  {
    boolean bool2 = false;
    boolean bool1;
    for (int i = 0;; i++)
    {
      HttpTypeEnum[] arrayOfHttpTypeEnum = DIS_HTTP_TYPE_ENUM;
      bool1 = bool2;
      if (i >= arrayOfHttpTypeEnum.length) {
        break;
      }
      if (arrayOfHttpTypeEnum[i] == paramHttpTypeEnum)
      {
        bool1 = true;
        break;
      }
    }
    return bool1;
  }
  
  public static String requestResult(HttpInfo paramHttpInfo)
  {
    return paramHttpInfo.mJsonObj.optString("status", "");
  }
  
  public static void setSearchUrl(String paramString)
  {
    SEARCH_URL_HTTP = paramString;
  }
  
  public static void setSearchUrl(JSONObject paramJSONObject)
  {
    SEARCH_URL_HTTP = paramJSONObject.optJSONObject("data").optString("api_url", "");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */