package com.imcash.gp.http;

import android.content.Context;
import com.imcash.gp.tools.GetNetworkInfo;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpRequest
{
  private static HttpRequest mHttpRequest;
  
  public static HttpRequest getInstance()
  {
    if (mHttpRequest == null) {
      mHttpRequest = new HttpRequest();
    }
    return mHttpRequest;
  }
  
  public void getJsonHTTP(final HttpCallBack paramHttpCallBack, final HttpTypeEnum paramHttpTypeEnum, RequestParams paramRequestParams)
  {
    GetNetworkInfo.getNetWorkType(ImcashApplication.getInstance());
    if (GetNetworkInfo.mNetWorkType < 1)
    {
      try
      {
        paramRequestParams = HttpTypeEnum.No_Net_Type;
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        paramHttpCallBack.httpSuccess(paramRequestParams, localJSONObject.put("code", 400).put("type", paramHttpTypeEnum));
      }
      catch (JSONException paramHttpCallBack)
      {
        paramHttpCallBack.printStackTrace();
      }
      return;
    }
    HttpClient.get(paramHttpTypeEnum.getApi(), paramRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable)
      {
        paramHttpCallBack.httpFailure(paramHttpTypeEnum, paramAnonymousThrowable);
      }
      
      public void onFinish()
      {
        paramHttpCallBack.httpFinish();
      }
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt) {
          paramHttpCallBack.httpSuccess(paramHttpTypeEnum, paramAnonymousJSONObject);
        }
      }
    });
  }
  
  public void getJsonHTTP(final HttpCallBack paramHttpCallBack, final HttpTypeEnum paramHttpTypeEnum, RequestParams paramRequestParams, int paramInt)
  {
    GetNetworkInfo.getNetWorkType(ImcashApplication.getInstance());
    if (GetNetworkInfo.mNetWorkType < 1)
    {
      try
      {
        HttpTypeEnum localHttpTypeEnum = HttpTypeEnum.No_Net_Type;
        paramRequestParams = new org/json/JSONObject;
        paramRequestParams.<init>();
        paramHttpCallBack.httpSuccess(localHttpTypeEnum, paramRequestParams.put("code", 400).put("type", paramHttpTypeEnum));
      }
      catch (JSONException paramHttpCallBack)
      {
        paramHttpCallBack.printStackTrace();
      }
      return;
    }
    HttpClient.get(paramHttpTypeEnum.getApi(), paramRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable)
      {
        paramHttpCallBack.httpFailure(paramHttpTypeEnum, paramAnonymousThrowable);
      }
      
      public void onFinish()
      {
        paramHttpCallBack.httpFinish();
      }
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt) {
          paramHttpCallBack.httpSuccess(paramHttpTypeEnum, paramAnonymousJSONObject);
        }
      }
    });
  }
  
  public void postJsonHTTP(Context paramContext, final HttpCallBack paramHttpCallBack, final HttpTypeEnum paramHttpTypeEnum, final String paramString)
  {
    GetNetworkInfo.getNetWorkType(ImcashApplication.getInstance());
    if (GetNetworkInfo.mNetWorkType < 1)
    {
      try
      {
        paramContext = HttpTypeEnum.No_Net_Type;
        paramString = new org/json/JSONObject;
        paramString.<init>();
        paramHttpCallBack.httpSuccess(paramContext, paramString.put("code", 400).put("type", paramHttpTypeEnum));
      }
      catch (JSONException paramContext)
      {
        paramContext.printStackTrace();
      }
      return;
    }
    HttpClient.post(paramContext, paramHttpTypeEnum, paramString, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable)
      {
        paramHttpCallBack.httpFailure(paramHttpTypeEnum, paramAnonymousThrowable);
      }
      
      public void onFinish()
      {
        paramHttpCallBack.httpFinish();
      }
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt) {
          paramHttpCallBack.httpSuccess(paramHttpTypeEnum, paramAnonymousJSONObject);
        }
      }
    });
  }
  
  public void postJsonHTTP(final HttpCallBack paramHttpCallBack, final HttpTypeEnum paramHttpTypeEnum, final RequestParams paramRequestParams)
  {
    GetNetworkInfo.getNetWorkType(ImcashApplication.getInstance());
    if (GetNetworkInfo.mNetWorkType < 1)
    {
      try
      {
        paramRequestParams = HttpTypeEnum.No_Net_Type;
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        paramHttpCallBack.httpSuccess(paramRequestParams, localJSONObject.put("code", 400).put("type", paramHttpTypeEnum));
      }
      catch (JSONException paramHttpCallBack)
      {
        paramHttpCallBack.printStackTrace();
      }
      return;
    }
    HttpClient.post(paramHttpTypeEnum, paramRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable)
      {
        paramHttpCallBack.httpFailure(paramHttpTypeEnum, paramAnonymousThrowable);
      }
      
      public void onFinish()
      {
        paramHttpCallBack.httpFinish();
      }
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt) {
          paramHttpCallBack.httpSuccess(paramHttpTypeEnum, paramAnonymousJSONObject);
        }
      }
    });
  }
  
  public void postJsonHTTP(final HttpCallBack paramHttpCallBack, final HttpTypeEnum paramHttpTypeEnum, final RequestParams paramRequestParams, int paramInt)
  {
    GetNetworkInfo.getNetWorkType(ImcashApplication.getInstance());
    if (GetNetworkInfo.mNetWorkType < 1)
    {
      try
      {
        HttpTypeEnum localHttpTypeEnum = HttpTypeEnum.No_Net_Type;
        paramRequestParams = new org/json/JSONObject;
        paramRequestParams.<init>();
        paramHttpCallBack.httpSuccess(localHttpTypeEnum, paramRequestParams.put("code", 400).put("type", paramHttpTypeEnum));
      }
      catch (JSONException paramHttpCallBack)
      {
        paramHttpCallBack.printStackTrace();
      }
      return;
    }
    HttpClient.post(paramHttpTypeEnum.getApi(), paramRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable)
      {
        paramHttpCallBack.httpFailure(paramHttpTypeEnum, paramAnonymousThrowable);
      }
      
      public void onFinish()
      {
        paramHttpCallBack.httpFinish();
      }
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt) {
          paramHttpCallBack.httpSuccess(paramHttpTypeEnum, paramAnonymousJSONObject);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */