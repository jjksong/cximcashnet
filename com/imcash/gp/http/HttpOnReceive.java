package com.imcash.gp.http;

import android.os.Message;

public abstract interface HttpOnReceive
{
  public abstract void failMsg(Message paramMessage);
  
  public abstract void finishMsg();
  
  public abstract void httpDataCallBack(HttpInfo paramHttpInfo);
  
  public abstract void onNoNet();
  
  public abstract void onTokenInvalid();
  
  public abstract void receiveErro(HttpInfo paramHttpInfo);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpOnReceive.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */