package com.imcash.gp.http;

import org.json.JSONObject;

public abstract interface HttpCallBack
{
  public abstract void httpFailure(HttpTypeEnum paramHttpTypeEnum, Throwable paramThrowable);
  
  public abstract void httpFailure(HttpTypeEnum paramHttpTypeEnum, Throwable paramThrowable, int paramInt);
  
  public abstract void httpFinish();
  
  public abstract void httpSuccess(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject);
  
  public abstract void httpSuccess(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject, int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpCallBack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */