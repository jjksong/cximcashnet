package com.imcash.gp.http;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.entity.StringEntity;

public class HttpClient
{
  private static AsyncHttpClient client = new AsyncHttpClient();
  
  static
  {
    AsyncHttpClient localAsyncHttpClient = client;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Android ");
    localStringBuilder.append(Build.VERSION.RELEASE);
    localAsyncHttpClient.setUserAgent(localStringBuilder.toString());
    client.setTimeout(6000);
  }
  
  public static void delete(String paramString, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    client.delete(getAbsoluteUrl(paramString), paramAsyncHttpResponseHandler);
  }
  
  public static void delete(String paramString, JsonHttpResponseHandler paramJsonHttpResponseHandler)
  {
    client.delete(getAbsoluteUrl(paramString), paramJsonHttpResponseHandler);
  }
  
  public static void get(String paramString, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    client.get(getAbsoluteUrl(paramString), paramAsyncHttpResponseHandler);
  }
  
  public static void get(String paramString, BinaryHttpResponseHandler paramBinaryHttpResponseHandler)
  {
    client.get(paramString, paramBinaryHttpResponseHandler);
  }
  
  public static void get(String paramString, JsonHttpResponseHandler paramJsonHttpResponseHandler)
  {
    client.get(getAbsoluteUrl(paramString), paramJsonHttpResponseHandler);
  }
  
  public static void get(String paramString, RequestParams paramRequestParams, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    client.get(getAbsoluteUrl(paramString), paramRequestParams, paramAsyncHttpResponseHandler);
  }
  
  public static void get(String paramString, RequestParams paramRequestParams, JsonHttpResponseHandler paramJsonHttpResponseHandler)
  {
    client.get(getAbsoluteUrl(paramString), paramRequestParams, paramJsonHttpResponseHandler);
  }
  
  private static String getAbsoluteUrl(HttpTypeEnum paramHttpTypeEnum)
  {
    Object localObject = "";
    if (1 == paramHttpTypeEnum.getServerType())
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(getMainServerUrl());
      ((StringBuilder)localObject).append(paramHttpTypeEnum.getApi());
      localObject = ((StringBuilder)localObject).toString();
    }
    else if (2 == paramHttpTypeEnum.getServerType())
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(getSearchServer());
      ((StringBuilder)localObject).append(paramHttpTypeEnum.getApi());
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  private static String getAbsoluteUrl(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getRequestBaseUrl(paramString));
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    Log.v("url is:", paramString);
    return paramString;
  }
  
  public static AsyncHttpClient getClient()
  {
    return client;
  }
  
  protected static String getMainServerUrl()
  {
    return HttpData.BASE_URL_HTTP;
  }
  
  protected static String getRequestBaseUrl(String paramString)
  {
    return HttpData.BASE_URL_HTTP;
  }
  
  protected static String getSearchServer()
  {
    return HttpData.Face_URL_HTTP;
  }
  
  protected static String getSearchServerUrl()
  {
    return HttpData.SEARCH_URL_HTTP;
  }
  
  public static void post(Context paramContext, HttpTypeEnum paramHttpTypeEnum, String paramString, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    paramString = new StringEntity(paramString, "UTF-8");
    paramString.setContentType("application/json");
    client.post(paramContext, getAbsoluteUrl(paramHttpTypeEnum), paramString, "application/json", paramAsyncHttpResponseHandler);
  }
  
  public static void post(HttpTypeEnum paramHttpTypeEnum, RequestParams paramRequestParams, JsonHttpResponseHandler paramJsonHttpResponseHandler)
  {
    client.post(getAbsoluteUrl(paramHttpTypeEnum), paramRequestParams, paramJsonHttpResponseHandler);
  }
  
  public static void post(String paramString, RequestParams paramRequestParams, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    client.post(getAbsoluteUrl(paramString), paramRequestParams, paramAsyncHttpResponseHandler);
  }
  
  public static void post(String paramString, RequestParams paramRequestParams, JsonHttpResponseHandler paramJsonHttpResponseHandler)
  {
    client.post(getAbsoluteUrl(paramString), paramRequestParams, paramJsonHttpResponseHandler);
  }
  
  protected static void setTimeOut()
  {
    client.setTimeout(6000);
  }
  
  protected static void setUserAgent()
  {
    AsyncHttpClient localAsyncHttpClient = client;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Android ");
    localStringBuilder.append(Build.VERSION.RELEASE);
    localAsyncHttpClient.setUserAgent(localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */