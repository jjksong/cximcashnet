package com.imcash.gp.http;

import org.json.JSONObject;

public class HttpInfo
{
  public int mCallBackNum = -1;
  public HttpTypeEnum mHttpType;
  public JSONObject mJsonObj;
  
  public HttpInfo(JSONObject paramJSONObject, HttpTypeEnum paramHttpTypeEnum)
  {
    this.mJsonObj = paramJSONObject;
    this.mHttpType = paramHttpTypeEnum;
  }
  
  public HttpInfo(JSONObject paramJSONObject, HttpTypeEnum paramHttpTypeEnum, int paramInt)
  {
    this.mJsonObj = paramJSONObject;
    this.mHttpType = paramHttpTypeEnum;
    this.mCallBackNum = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/http/HttpInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */