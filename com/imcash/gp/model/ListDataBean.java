package com.imcash.gp.model;

public class ListDataBean
{
  private String add_time;
  private String cateid;
  private String content;
  private String currency;
  private String currencylist;
  private String description;
  private String id;
  private String image;
  private String income_no;
  private String income_yes;
  private String istuijian;
  private String keyword;
  private String max;
  private String min;
  private String name;
  private String showtime;
  private String sortid;
  private String status;
  private String timelist;
  private String touzinum;
  
  public String getAdd_time()
  {
    return this.add_time;
  }
  
  public String getCateid()
  {
    return this.cateid;
  }
  
  public String getContent()
  {
    return this.content;
  }
  
  public String getCurrency()
  {
    return this.currency;
  }
  
  public String getCurrencylist()
  {
    return this.currencylist;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getImage()
  {
    return this.image;
  }
  
  public String getIncome_no()
  {
    return this.income_no;
  }
  
  public String getIncome_yes()
  {
    return this.income_yes;
  }
  
  public String getIstuijian()
  {
    return this.istuijian;
  }
  
  public String getKeyword()
  {
    return this.keyword;
  }
  
  public String getMax()
  {
    return this.max;
  }
  
  public String getMin()
  {
    return this.min;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getShowtime()
  {
    return this.showtime;
  }
  
  public String getSortid()
  {
    return this.sortid;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getTimelist()
  {
    return this.timelist;
  }
  
  public String getTouzinum()
  {
    return this.touzinum;
  }
  
  public void setAdd_time(String paramString)
  {
    this.add_time = paramString;
  }
  
  public void setCateid(String paramString)
  {
    this.cateid = paramString;
  }
  
  public void setContent(String paramString)
  {
    this.content = paramString;
  }
  
  public void setCurrency(String paramString)
  {
    this.currency = paramString;
  }
  
  public void setCurrencylist(String paramString)
  {
    this.currencylist = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.description = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setImage(String paramString)
  {
    this.image = paramString;
  }
  
  public void setIncome_no(String paramString)
  {
    this.income_no = paramString;
  }
  
  public void setIncome_yes(String paramString)
  {
    this.income_yes = paramString;
  }
  
  public void setIstuijian(String paramString)
  {
    this.istuijian = paramString;
  }
  
  public void setKeyword(String paramString)
  {
    this.keyword = paramString;
  }
  
  public void setMax(String paramString)
  {
    this.max = paramString;
  }
  
  public void setMin(String paramString)
  {
    this.min = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setShowtime(String paramString)
  {
    this.showtime = paramString;
  }
  
  public void setSortid(String paramString)
  {
    this.sortid = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
  
  public void setTimelist(String paramString)
  {
    this.timelist = paramString;
  }
  
  public void setTouzinum(String paramString)
  {
    this.touzinum = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/ListDataBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */