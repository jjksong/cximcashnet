package com.imcash.gp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseModel
{
  public JSONObject mDataJson;
  public JSONArray mDataListJson;
  public JSONObject mJSON;
  public String mJsonData;
  
  public BaseModel() {}
  
  public BaseModel(JSONObject paramJSONObject)
  {
    this.mJSON = paramJSONObject;
    this.mJsonData = paramJSONObject.toString();
    this.mDataJson = paramJSONObject.optJSONObject("data");
    Object localObject = this.mDataJson;
    if ((localObject == null) || (((JSONObject)localObject).length() == 0)) {
      this.mDataJson = paramJSONObject;
    }
    paramJSONObject = this.mDataJson;
    if (paramJSONObject != null)
    {
      this.mDataListJson = paramJSONObject.optJSONArray("list");
      if (this.mDataListJson == null)
      {
        localObject = this.mDataJson.optString("list", "");
        if (((String)localObject).length() == 0) {}
      }
    }
    try
    {
      paramJSONObject = new org/json/JSONArray;
      paramJSONObject.<init>((String)localObject);
      this.mDataListJson = paramJSONObject;
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  public static JSONObject getDataJson(JSONObject paramJSONObject)
  {
    return paramJSONObject.optJSONObject("data");
  }
  
  public static JSONArray getListJson(JSONObject paramJSONObject)
  {
    paramJSONObject = getDataJson(paramJSONObject);
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0)) {
      return paramJSONObject.optJSONArray("list");
    }
    return null;
  }
  
  public static int getListJsonCount(JSONObject paramJSONObject)
  {
    if (getListJson(paramJSONObject) == null) {
      return 0;
    }
    return getListJson(paramJSONObject).length();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/BaseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */