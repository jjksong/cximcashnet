package com.imcash.gp.model;

public class IntegralDetail
{
  private String amount;
  private String create_time;
  private String msg;
  private String symbol;
  private String type;
  private String uid;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getMsg()
  {
    return this.msg;
  }
  
  public String getSymbol()
  {
    return this.symbol;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public String getUid()
  {
    return this.uid;
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setMsg(String paramString)
  {
    this.msg = paramString;
  }
  
  public void setSymbol(String paramString)
  {
    this.symbol = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setUid(String paramString)
  {
    this.uid = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/IntegralDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */