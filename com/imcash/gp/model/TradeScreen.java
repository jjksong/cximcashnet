package com.imcash.gp.model;

public class TradeScreen
{
  private String mBfcType;
  private String mCoinType;
  private String mMoneyState;
  private String mRateState;
  private String mTimeState;
  
  public TradeScreen(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.mTimeState = paramString1;
    this.mMoneyState = paramString2;
    this.mRateState = paramString3;
    this.mCoinType = paramString4;
    this.mBfcType = paramString5;
  }
  
  public String getmBfcType()
  {
    return this.mBfcType;
  }
  
  public String getmCoinType()
  {
    return this.mCoinType;
  }
  
  public String getmMoneyState()
  {
    return this.mMoneyState;
  }
  
  public String getmRateState()
  {
    return this.mRateState;
  }
  
  public String getmTimeState()
  {
    return this.mTimeState;
  }
  
  public void setmBfcType(String paramString)
  {
    this.mBfcType = paramString;
  }
  
  public void setmCoinType(String paramString)
  {
    this.mCoinType = paramString;
  }
  
  public void setmMoneyState(String paramString)
  {
    this.mMoneyState = paramString;
  }
  
  public void setmRateState(String paramString)
  {
    this.mRateState = paramString;
  }
  
  public void setmTimeState(String paramString)
  {
    this.mTimeState = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/TradeScreen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */