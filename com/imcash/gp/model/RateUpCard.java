package com.imcash.gp.model;

public class RateUpCard
{
  private String add_bili;
  private String coupon_remark;
  private String coupon_title;
  private String end_time;
  private String id;
  public boolean mIsSelected = false;
  
  public String getAdd_bili()
  {
    return this.add_bili;
  }
  
  public String getCoupon_remark()
  {
    return this.coupon_remark;
  }
  
  public String getCoupon_title()
  {
    return this.coupon_title;
  }
  
  public String getEnd_time()
  {
    return this.end_time;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public void setAdd_bili(String paramString)
  {
    this.add_bili = paramString;
  }
  
  public void setCoupon_remark(String paramString)
  {
    this.coupon_remark = paramString;
  }
  
  public void setCoupon_title(String paramString)
  {
    this.coupon_title = paramString;
  }
  
  public void setEnd_time(String paramString)
  {
    this.end_time = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RateUpCard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */