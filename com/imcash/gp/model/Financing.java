package com.imcash.gp.model;

public class Financing
{
  private String id;
  private String income_no;
  private String keyword;
  private String max;
  private String min;
  private String name;
  private String timelist;
  
  public String getId()
  {
    return this.id;
  }
  
  public String getIncome_no()
  {
    return this.income_no;
  }
  
  public String getKeyword()
  {
    return this.keyword;
  }
  
  public String getMax()
  {
    return this.max;
  }
  
  public String getMin()
  {
    return this.min;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getTimelist()
  {
    return this.timelist;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setIncome_no(String paramString)
  {
    this.income_no = paramString;
  }
  
  public void setKeyword(String paramString)
  {
    this.keyword = paramString;
  }
  
  public void setMax(String paramString)
  {
    this.max = paramString;
  }
  
  public void setMin(String paramString)
  {
    this.min = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setTimelist(String paramString)
  {
    this.timelist = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/Financing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */