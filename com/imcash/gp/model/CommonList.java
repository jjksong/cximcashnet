package com.imcash.gp.model;

import java.util.ArrayList;
import org.json.JSONObject;

public abstract class CommonList<T>
  extends BaseModel
{
  public ArrayList<T> mDataArray;
  
  public CommonList(JSONObject paramJSONObject)
  {
    super(paramJSONObject);
    initData();
  }
  
  public abstract void addData();
  
  public void initData()
  {
    if (this.mDataArray == null) {
      this.mDataArray = new ArrayList();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/CommonList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */