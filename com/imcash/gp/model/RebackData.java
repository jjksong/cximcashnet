package com.imcash.gp.model;

public class RebackData
{
  private String buy_back;
  private String buy_token;
  private String create_time;
  private String from_phone;
  private String quota;
  private String total_money;
  
  public String getBuy_back()
  {
    return this.buy_back;
  }
  
  public String getBuy_token()
  {
    return this.buy_token;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getFrom_phone()
  {
    return this.from_phone;
  }
  
  public String getQuota()
  {
    return this.quota;
  }
  
  public String getTotal_money()
  {
    return this.total_money;
  }
  
  public void setBuy_back(String paramString)
  {
    this.buy_back = paramString;
  }
  
  public void setBuy_token(String paramString)
  {
    this.buy_token = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setFrom_phone(String paramString)
  {
    this.from_phone = paramString;
  }
  
  public void setQuota(String paramString)
  {
    this.quota = paramString;
  }
  
  public void setTotal_money(String paramString)
  {
    this.total_money = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RebackData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */