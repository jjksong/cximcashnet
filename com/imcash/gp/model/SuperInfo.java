package com.imcash.gp.model;

public class SuperInfo
{
  private String days;
  private String grade;
  private String name;
  private String num;
  private String phone;
  private String profit;
  private String starttime;
  private String userid;
  
  public String getDays()
  {
    return this.days;
  }
  
  public String getGrade()
  {
    return this.grade;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getProfit()
  {
    return this.profit;
  }
  
  public String getStarttime()
  {
    return this.starttime;
  }
  
  public String getUserid()
  {
    return this.userid;
  }
  
  public void setDays(String paramString)
  {
    this.days = paramString;
  }
  
  public void setGrade(String paramString)
  {
    this.grade = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setProfit(String paramString)
  {
    this.profit = paramString;
  }
  
  public void setStarttime(String paramString)
  {
    this.starttime = paramString;
  }
  
  public void setUserid(String paramString)
  {
    this.userid = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/SuperInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */