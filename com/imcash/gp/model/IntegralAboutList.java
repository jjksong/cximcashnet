package com.imcash.gp.model;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class IntegralAboutList
  extends CommonList<IntegralAbout>
{
  public IntegralAboutList(JSONObject paramJSONObject)
  {
    super(paramJSONObject);
    addData();
  }
  
  public void addData()
  {
    if ((this.mDataListJson != null) && (this.mDataListJson.length() != 0))
    {
      for (int i = 0; i < this.mDataListJson.length(); i++) {
        this.mDataArray.add(new IntegralAbout(this.mDataListJson.optJSONObject(i)));
      }
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/IntegralAboutList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */