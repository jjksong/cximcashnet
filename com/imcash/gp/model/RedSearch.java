package com.imcash.gp.model;

public class RedSearch
{
  private String bag_id;
  private String phone;
  private String remark;
  
  public String getBag_id()
  {
    return this.bag_id;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getRemark()
  {
    return this.remark;
  }
  
  public void setBag_id(String paramString)
  {
    this.bag_id = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setRemark(String paramString)
  {
    this.remark = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RedSearch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */