package com.imcash.gp.model;

public class MerchantInfo
{
  private String bank_card;
  private String bank_name;
  private String card;
  private int is_bind;
  private String is_realname;
  private String username;
  
  public String getBank_card()
  {
    return this.bank_card;
  }
  
  public String getBank_name()
  {
    return this.bank_name;
  }
  
  public String getCard()
  {
    return this.card;
  }
  
  public int getIs_bind()
  {
    return this.is_bind;
  }
  
  public String getIs_realname()
  {
    return this.is_realname;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public boolean isBindCard()
  {
    return this.is_bind != 0;
  }
  
  public boolean isRealName()
  {
    return "1".equals(this.is_realname);
  }
  
  public String lastFourCardNum()
  {
    return getBank_card().substring(getBank_card().length() - 4);
  }
  
  public void setBank_card(String paramString)
  {
    this.bank_card = paramString;
  }
  
  public void setBank_name(String paramString)
  {
    this.bank_name = paramString;
  }
  
  public void setCard(String paramString)
  {
    this.card = paramString;
  }
  
  public void setIs_bind(int paramInt)
  {
    this.is_bind = paramInt;
  }
  
  public void setIs_realname(String paramString)
  {
    this.is_realname = paramString;
  }
  
  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/MerchantInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */