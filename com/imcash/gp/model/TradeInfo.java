package com.imcash.gp.model;

public class TradeInfo
{
  private String amount;
  private String create_time;
  private String currency_name;
  private String due_date;
  private String end_time;
  private String fee;
  private String finish_time;
  private String fund_id;
  public boolean mIsSelected = false;
  private String name;
  private String num;
  private String price;
  private String rate;
  private int status;
  private String total_amount;
  private String trade_id;
  private String trade_no;
  private String username;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getCurrency_name()
  {
    return this.currency_name;
  }
  
  public String getDue_date()
  {
    return this.due_date;
  }
  
  public String getEnd_time()
  {
    return this.end_time;
  }
  
  public String getFee()
  {
    return this.fee;
  }
  
  public String getFinish_time()
  {
    return this.finish_time;
  }
  
  public String getFund_id()
  {
    return this.fund_id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public String getPrice()
  {
    return this.price;
  }
  
  public String getRate()
  {
    return this.rate;
  }
  
  public int getStatus()
  {
    return this.status;
  }
  
  public String getStatusStr()
  {
    String str = "";
    switch (this.status)
    {
    default: 
      break;
    case 1: 
      str = "已成交";
      break;
    case 0: 
      str = "挂单中";
    }
    return str;
  }
  
  public String getTotal_amount()
  {
    return this.total_amount;
  }
  
  public String getTrade_id()
  {
    return this.trade_id;
  }
  
  public String getTrade_no()
  {
    return this.trade_no;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public boolean isSelling()
  {
    boolean bool;
    if (this.status == 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setCurrency_name(String paramString)
  {
    this.currency_name = paramString;
  }
  
  public void setDue_date(String paramString)
  {
    this.due_date = paramString;
  }
  
  public void setEnd_time(String paramString)
  {
    this.end_time = paramString;
  }
  
  public void setFee(String paramString)
  {
    this.fee = paramString;
  }
  
  public void setFinish_time(String paramString)
  {
    this.finish_time = paramString;
  }
  
  public void setFund_id(String paramString)
  {
    this.fund_id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
  
  public void setRate(String paramString)
  {
    this.rate = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.status = paramInt;
  }
  
  public void setTotal_amount(String paramString)
  {
    this.total_amount = paramString;
  }
  
  public void setTrade_id(String paramString)
  {
    this.trade_id = paramString;
  }
  
  public void setTrade_no(String paramString)
  {
    this.trade_no = paramString;
  }
  
  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/TradeInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */