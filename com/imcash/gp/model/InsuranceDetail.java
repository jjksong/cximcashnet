package com.imcash.gp.model;

public class InsuranceDetail
{
  private String days;
  private String price;
  
  public String getDays()
  {
    return this.days;
  }
  
  public String getPrice()
  {
    return this.price;
  }
  
  public void setDays(String paramString)
  {
    this.days = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/InsuranceDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */