package com.imcash.gp.model;

import android.graphics.Color;

public class OtcRecord
{
  private String amount;
  private String create_time;
  private String record_id;
  private String state;
  private int status;
  private String trade_no;
  private String type;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getRecord_id()
  {
    return this.record_id;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public int getStateColor()
  {
    int i = this.status;
    if (i == 0) {
      i = Color.rgb(99, 93, 251);
    } else if (1 == i) {
      i = Color.rgb(150, 150, 150);
    } else {
      i = Color.rgb(216, 109, 99);
    }
    return i;
  }
  
  public int getStateRes()
  {
    int i = this.status;
    if (i == 0) {
      return 2131099902;
    }
    if (1 == i) {
      return 2131099900;
    }
    return 2131099901;
  }
  
  public int getStatus()
  {
    return this.status;
  }
  
  public String getTrade_no()
  {
    return this.trade_no;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isCanContinueOut()
  {
    return 4 == this.status;
  }
  
  public boolean isOutCanCancel()
  {
    int i = this.status;
    return (3 == i) || (4 == i);
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setRecord_id(String paramString)
  {
    this.record_id = paramString;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.status = paramInt;
  }
  
  public void setTrade_no(String paramString)
  {
    this.trade_no = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public boolean showDetailBtc()
  {
    return this.status == 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/OtcRecord.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */