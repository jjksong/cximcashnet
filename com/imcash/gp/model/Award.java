package com.imcash.gp.model;

public class Award
{
  private String id;
  private String name;
  private String sum;
  private String surplus;
  
  public String getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getSum()
  {
    return this.sum;
  }
  
  public String getSurplus()
  {
    return this.surplus;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setSum(String paramString)
  {
    this.sum = paramString;
  }
  
  public void setSurplus(String paramString)
  {
    this.surplus = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/Award.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */