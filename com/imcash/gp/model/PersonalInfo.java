package com.imcash.gp.model;

import org.json.JSONObject;

public class PersonalInfo
  extends BaseModel
{
  public static final int Leavel_Company = 1;
  public static final int Leavel_Super = 2;
  public static final int Level_Normal = 0;
  public static final int Man_Key = 1;
  public static final int Secret_Key = 0;
  public static final int Woman_Key = 2;
  public static final String[] mSexArray = { "保密", "男士", "女士" };
  public String address;
  public String birthday;
  public String email;
  public int grade;
  public int integral;
  public int is_realname;
  public String nickname;
  public String phone;
  public int sex;
  
  public PersonalInfo(JSONObject paramJSONObject)
  {
    super(paramJSONObject);
    init();
  }
  
  public String getHidePhone()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.phone.substring(0, 3));
    localStringBuilder.append("****");
    String str = this.phone;
    localStringBuilder.append(str.substring(7, str.length()));
    return localStringBuilder.toString();
  }
  
  public String getSexStr()
  {
    return mSexArray[this.sex];
  }
  
  protected void init()
  {
    this.phone = this.mDataJson.optString("phone", "");
    this.email = this.mDataJson.optString("email", "");
    this.nickname = this.mDataJson.optString("nickname", "");
    this.address = this.mDataJson.optString("address", "");
    this.birthday = this.mDataJson.optString("birthday", "");
    this.is_realname = this.mDataJson.optInt("is_realname", 0);
    this.grade = this.mDataJson.optInt("grade", 0);
    this.sex = this.mDataJson.optInt("sex", 0);
    int i = this.sex;
    if ((i > 2) || (i < 0)) {
      this.sex = 0;
    }
    this.integral = this.mDataJson.optInt("integral", 0);
  }
  
  public boolean isRealName()
  {
    return this.is_realname != 0;
  }
  
  public boolean isSuper()
  {
    boolean bool;
    if (2 == this.grade) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/PersonalInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */