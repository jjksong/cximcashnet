package com.imcash.gp.model;

public class ReplyInfo
{
  private String content;
  private String create_time;
  private String reply;
  
  public String getContent()
  {
    return this.content;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getReply()
  {
    return this.reply;
  }
  
  public void setContent(String paramString)
  {
    this.content = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setReply(String paramString)
  {
    this.reply = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/ReplyInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */