package com.imcash.gp.model;

public class Mission
{
  private String content;
  private String mark;
  private int status;
  
  public String getContent()
  {
    return this.content;
  }
  
  public String getMark()
  {
    return this.mark;
  }
  
  public int getStatus()
  {
    return this.status;
  }
  
  public boolean isComplete()
  {
    return getStatus() != 0;
  }
  
  public void setContent(String paramString)
  {
    this.content = paramString;
  }
  
  public void setMark(String paramString)
  {
    this.mark = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.status = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/Mission.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */