package com.imcash.gp.model;

public class BfcRate
{
  private String bfc_price;
  private String integral;
  private String price;
  private String rule;
  private String usdt;
  
  public String getBfc_price()
  {
    return this.bfc_price;
  }
  
  public String getIntegral()
  {
    return this.integral;
  }
  
  public String getPrice()
  {
    return this.price;
  }
  
  public String getRule()
  {
    return this.rule;
  }
  
  public String getUsdt()
  {
    return this.usdt;
  }
  
  public void setBfc_price(String paramString)
  {
    this.bfc_price = paramString;
  }
  
  public void setIntegral(String paramString)
  {
    this.integral = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
  
  public void setRule(String paramString)
  {
    this.rule = paramString;
  }
  
  public void setUsdt(String paramString)
  {
    this.usdt = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/BfcRate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */