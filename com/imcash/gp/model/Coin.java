package com.imcash.gp.model;

public class Coin
{
  private String id;
  public boolean mIsSelected = false;
  private String name;
  
  public String getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/Coin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */