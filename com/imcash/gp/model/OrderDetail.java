package com.imcash.gp.model;

public class OrderDetail
{
  private String amount;
  private String create_time;
  private String message;
  private String name;
  private String symbol;
  private String type;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getSymbol()
  {
    return this.symbol;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setSymbol(String paramString)
  {
    this.symbol = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/OrderDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */