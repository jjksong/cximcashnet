package com.imcash.gp.model;

public class CoinLimit
{
  private double balance;
  private double coinoutmax;
  private double coinoutmin;
  private String content;
  private double feenum;
  
  public double getBalance()
  {
    return this.balance;
  }
  
  public double getCoinoutmax()
  {
    return this.coinoutmax;
  }
  
  public double getCoinoutmin()
  {
    return this.coinoutmin;
  }
  
  public String getContent()
  {
    return this.content;
  }
  
  public double getFeenum()
  {
    return this.feenum;
  }
  
  public void setBalance(double paramDouble)
  {
    this.balance = paramDouble;
  }
  
  public void setCoinoutmax(double paramDouble)
  {
    this.coinoutmax = paramDouble;
  }
  
  public void setCoinoutmin(double paramDouble)
  {
    this.coinoutmin = paramDouble;
  }
  
  public void setContent(String paramString)
  {
    this.content = paramString;
  }
  
  public void setFeenum(double paramDouble)
  {
    this.feenum = paramDouble;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/CoinLimit.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */