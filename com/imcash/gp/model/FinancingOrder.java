package com.imcash.gp.model;

public class FinancingOrder
{
  private String coin;
  private String days;
  private String end_time;
  private String id;
  private String name;
  private String num;
  private String rate;
  private String start_time;
  private String touzi_id;
  private String trade_no;
  
  public String getCoin()
  {
    return this.coin;
  }
  
  public String getDays()
  {
    return this.days;
  }
  
  public String getEnd_time()
  {
    return this.end_time;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public String getRate()
  {
    return this.rate;
  }
  
  public String getShowTime()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getStart_time());
    localStringBuilder.append("至");
    localStringBuilder.append(getEnd_time());
    return localStringBuilder.toString();
  }
  
  public String getStart_time()
  {
    return this.start_time;
  }
  
  public String getTouzi_id()
  {
    return this.touzi_id;
  }
  
  public String getTrade_no()
  {
    return this.trade_no;
  }
  
  public void setCoin(String paramString)
  {
    this.coin = paramString;
  }
  
  public void setDays(String paramString)
  {
    this.days = paramString;
  }
  
  public void setEnd_time(String paramString)
  {
    this.end_time = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
  
  public void setRate(String paramString)
  {
    this.rate = paramString;
  }
  
  public void setStart_time(String paramString)
  {
    this.start_time = paramString;
  }
  
  public void setTouzi_id(String paramString)
  {
    this.touzi_id = paramString;
  }
  
  public void setTrade_no(String paramString)
  {
    this.trade_no = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/FinancingOrder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */