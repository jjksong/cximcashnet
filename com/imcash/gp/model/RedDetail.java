package com.imcash.gp.model;

public class RedDetail
{
  private String amount;
  private String coin_name;
  private String create_time;
  private String phone;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCoin_name()
  {
    return this.coin_name;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getPhoneHide()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.phone.substring(0, 3));
    localStringBuilder.append("****");
    String str = this.phone;
    localStringBuilder.append(str.substring(7, str.length()));
    return localStringBuilder.toString();
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCoin_name(String paramString)
  {
    this.coin_name = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RedDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */