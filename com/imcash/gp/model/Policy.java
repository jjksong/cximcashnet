package com.imcash.gp.model;

public class Policy
{
  public final String[] States = { "正在生效", "续费购买", "重新购买" };
  private String amount;
  private String create_time;
  private String days;
  private String end_time;
  private String id;
  private String phone;
  private int state;
  private String trade_no;
  
  public boolean canBuy()
  {
    int i = getState();
    boolean bool2 = false;
    boolean bool1 = bool2;
    switch (i)
    {
    default: 
      bool1 = bool2;
      break;
    case 1: 
    case 2: 
      bool1 = true;
    }
    return bool1;
  }
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getDays()
  {
    return this.days;
  }
  
  public String getEnd_time()
  {
    return this.end_time;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getShowTime()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getCreate_time());
    localStringBuilder.append("至");
    localStringBuilder.append(getEnd_time());
    return localStringBuilder.toString();
  }
  
  public int getState()
  {
    return this.state;
  }
  
  public int getStateBackColor()
  {
    int j = getState();
    int i = 2131099785;
    if (j == 0) {
      i = 2131099780;
    } else if ((1 != getState()) && (2 != getState())) {
      i = 2131099780;
    }
    return i;
  }
  
  public String getStateStr()
  {
    int i = getState();
    String[] arrayOfString = this.States;
    if (i >= arrayOfString.length) {
      return "";
    }
    return arrayOfString[getState()];
  }
  
  public String getTrade_no()
  {
    return this.trade_no;
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setDays(String paramString)
  {
    this.days = paramString;
  }
  
  public void setEnd_time(String paramString)
  {
    this.end_time = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setState(int paramInt)
  {
    this.state = paramInt;
  }
  
  public void setTrade_no(String paramString)
  {
    this.trade_no = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/Policy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */