package com.imcash.gp.model;

import android.graphics.Color;

public class WalletRecord
{
  private String actual;
  private String add_time;
  private String message;
  private int type;
  
  public String getActual()
  {
    return this.actual;
  }
  
  public int getActualColor()
  {
    int i = this.type;
    if (1 == i) {
      i = Color.rgb(248, 84, 83);
    } else if (2 == i) {
      i = Color.rgb(61, 193, 139);
    } else {
      i = 0;
    }
    return i;
  }
  
  public String getAdd_time()
  {
    return this.add_time;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getShowActual()
  {
    String str = getActual();
    int i = this.type;
    Object localObject;
    if (1 == i)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("+");
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = str;
      if (2 == i)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("-");
        ((StringBuilder)localObject).append(str);
        localObject = ((StringBuilder)localObject).toString();
      }
    }
    return (String)localObject;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public void setActual(String paramString)
  {
    this.actual = paramString;
  }
  
  public void setAdd_time(String paramString)
  {
    this.add_time = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setType(int paramInt)
  {
    this.type = paramInt;
  }
  
  public String stateStr()
  {
    return this.message;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/WalletRecord.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */