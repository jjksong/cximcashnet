package com.imcash.gp.model;

public class BfcLock
{
  private String amount;
  private String create_time;
  private String message;
  private String symbol;
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getSymbol()
  {
    return this.symbol;
  }
  
  public void setAmount(String paramString)
  {
    this.amount = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setSymbol(String paramString)
  {
    this.symbol = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/BfcLock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */