package com.imcash.gp.model;

public class InviteInfo
{
  private String add_time;
  private String bfc_balance;
  private String id;
  private String nickname;
  private String num;
  
  public String getAdd_time()
  {
    return this.add_time;
  }
  
  public String getBfc_balance()
  {
    return this.bfc_balance;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getNickname()
  {
    return this.nickname;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public void setAdd_time(String paramString)
  {
    this.add_time = paramString;
  }
  
  public void setBfc_balance(String paramString)
  {
    this.bfc_balance = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setNickname(String paramString)
  {
    this.nickname = paramString;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/InviteInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */