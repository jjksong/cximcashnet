package com.imcash.gp.model;

public class RedRecord
{
  private String bag_id;
  private String coin_name;
  private String create_time;
  private String id;
  private String keyword;
  private int mold;
  private String num;
  private int status;
  private String total_amount;
  private String type;
  
  public String getBag_id()
  {
    return this.bag_id;
  }
  
  public String getCoinStr()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getTotal_amount());
    localStringBuilder.append(getCoin_name());
    return localStringBuilder.toString();
  }
  
  public String getCoin_name()
  {
    return this.coin_name;
  }
  
  public String getCreate_time()
  {
    return this.create_time;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getKeyword()
  {
    return this.keyword;
  }
  
  public int getMold()
  {
    return this.mold;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public String getPress()
  {
    String str = "";
    if (1 == this.status) {
      str = "已完成 ";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append(getNum());
    return localStringBuilder.toString();
  }
  
  public int getStatus()
  {
    return this.status;
  }
  
  public String getTotal_amount()
  {
    return this.total_amount;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isRedSharePacket()
  {
    return getMold() != 0;
  }
  
  public void setBag_id(String paramString)
  {
    this.bag_id = paramString;
  }
  
  public void setCoin_name(String paramString)
  {
    this.coin_name = paramString;
  }
  
  public void setCreate_time(String paramString)
  {
    this.create_time = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setKeyword(String paramString)
  {
    this.keyword = paramString;
  }
  
  public void setMold(int paramInt)
  {
    this.mold = paramInt;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.status = paramInt;
  }
  
  public void setTotal_amount(String paramString)
  {
    this.total_amount = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RedRecord.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */