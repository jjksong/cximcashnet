package com.imcash.gp.model;

import android.graphics.Color;
import java.util.List;

public class CoinInfo
{
  private String balance;
  private String cny;
  private String coin;
  private String color;
  private String id;
  private List<String> price;
  private String usd;
  
  public String getBalance()
  {
    return this.balance;
  }
  
  public String getCny()
  {
    return this.cny;
  }
  
  public String getCoin()
  {
    return this.coin;
  }
  
  public String getColor()
  {
    return this.color;
  }
  
  public double[] getDoubleArray()
  {
    double[] arrayOfDouble = new double[getPrice().size()];
    for (int i = 0; i < getPrice().size(); i++) {
      arrayOfDouble[i] = Double.valueOf((String)getPrice().get(i)).doubleValue();
    }
    return arrayOfDouble;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public int getLineColor()
  {
    return Color.rgb(89, 97, 250);
  }
  
  public List<String> getPrice()
  {
    return this.price;
  }
  
  public String getUsd()
  {
    return this.usd;
  }
  
  public void setBalance(String paramString)
  {
    this.balance = paramString;
  }
  
  public void setCny(String paramString)
  {
    this.cny = paramString;
  }
  
  public void setCoin(String paramString)
  {
    this.coin = paramString;
  }
  
  public void setColor(String paramString)
  {
    this.color = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setPrice(List<String> paramList)
  {
    this.price = paramList;
  }
  
  public void setUsd(String paramString)
  {
    this.usd = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/CoinInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */