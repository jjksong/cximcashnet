package com.imcash.gp.model;

import android.graphics.Color;
import com.imcash.gp.tools.ArithUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FourHour
{
  public static final int YCount = 12;
  private List<FourHourPriceBean> four_hour_price;
  private String id;
  private String name;
  private String price;
  
  public List<FourHourPriceBean> getFour_hour_price()
  {
    return this.four_hour_price;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public List<Double> getLineShow()
  {
    ArrayList localArrayList = new ArrayList();
    double d2 = (getMaxPrice() - getMinPrice()) / 1.0D;
    for (int i = 0; i < getFour_hour_price().size(); i++)
    {
      double d1;
      try
      {
        d1 = ArithUtil.div(Double.valueOf(((FourHourPriceBean)getFour_hour_price().get(i)).getPrice()).doubleValue() - getMinPrice(), d2);
        d1 += 0.1D;
      }
      catch (Exception localException)
      {
        d1 = 0.6D;
      }
      localArrayList.add(Double.valueOf(d1));
    }
    return localArrayList;
  }
  
  public int getLineaColor()
  {
    Color.rgb(18, 184, 134);
    double d1 = Double.valueOf(((FourHourPriceBean)getFour_hour_price().get(getFour_hour_price().size() - 1)).getPrice()).doubleValue();
    double d2 = Double.valueOf(((FourHourPriceBean)getFour_hour_price().get(getFour_hour_price().size() - 2)).getPrice()).doubleValue();
    int i;
    if (Double.doubleToRawLongBits(d1) >= Double.doubleToRawLongBits(d2)) {
      i = Color.rgb(18, 184, 134);
    } else {
      i = Color.rgb(250, 82, 82);
    }
    return i;
  }
  
  public double getMaxPrice()
  {
    double d = Double.valueOf(((FourHourPriceBean)getFour_hour_price().get(0)).getPrice()).doubleValue();
    Iterator localIterator = getFour_hour_price().iterator();
    while (localIterator.hasNext())
    {
      FourHourPriceBean localFourHourPriceBean = (FourHourPriceBean)localIterator.next();
      if (Double.doubleToRawLongBits(d) < Double.doubleToRawLongBits(Double.valueOf(localFourHourPriceBean.getPrice()).doubleValue())) {
        d = Double.valueOf(localFourHourPriceBean.getPrice()).doubleValue();
      }
    }
    return d;
  }
  
  public double getMinPrice()
  {
    double d = Double.valueOf(((FourHourPriceBean)getFour_hour_price().get(0)).getPrice()).doubleValue();
    Iterator localIterator = getFour_hour_price().iterator();
    while (localIterator.hasNext())
    {
      FourHourPriceBean localFourHourPriceBean = (FourHourPriceBean)localIterator.next();
      if (Double.doubleToRawLongBits(d) > Double.doubleToRawLongBits(Double.valueOf(localFourHourPriceBean.getPrice()).doubleValue())) {
        d = Double.valueOf(localFourHourPriceBean.getPrice()).doubleValue();
      }
    }
    return d;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getPrice()
  {
    return this.price;
  }
  
  public double getShowPriceByIndex(int paramInt)
  {
    double d1 = getMinPrice();
    double d3 = getMaxPrice() - d1;
    double d2 = getFour_hour_price().size();
    Double.isNaN(d2);
    d2 = d3 / d2;
    if (Double.doubleToRawLongBits(d3) == Double.doubleToRawLongBits(0.0D))
    {
      d2 = paramInt;
      Double.isNaN(d2);
      d1 = d1 - 1.2000000000000002D + d2 * 0.2D;
    }
    else
    {
      d3 = paramInt;
      Double.isNaN(d3);
      d1 += d2 * d3;
    }
    return d1;
  }
  
  public void setFour_hour_price(List<FourHourPriceBean> paramList)
  {
    this.four_hour_price = paramList;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
  
  public static class FourHourPriceBean
  {
    private String price;
    private String time;
    
    public String getPrice()
    {
      return this.price;
    }
    
    public String getTime()
    {
      return this.time;
    }
    
    public double getValue()
    {
      return Double.valueOf(this.price).doubleValue();
    }
    
    public void setPrice(String paramString)
    {
      this.price = paramString;
    }
    
    public void setTime(String paramString)
    {
      this.time = paramString;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/FourHour.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */