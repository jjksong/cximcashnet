package com.imcash.gp.model;

public class QuotaInfo
{
  private String balance;
  private int countdown;
  private String quota;
  private int sendBack;
  private String surplus;
  
  public String getBalance()
  {
    return this.balance;
  }
  
  public int getCountdown()
  {
    return this.countdown;
  }
  
  public String getQuota()
  {
    return this.quota;
  }
  
  public int getSendBack()
  {
    return this.sendBack;
  }
  
  public String getSurplus()
  {
    return this.surplus;
  }
  
  public void setBalance(String paramString)
  {
    this.balance = paramString;
  }
  
  public void setCountdown(int paramInt)
  {
    this.countdown = paramInt;
  }
  
  public void setQuota(String paramString)
  {
    this.quota = paramString;
  }
  
  public void setSendBack(int paramInt)
  {
    this.sendBack = paramInt;
  }
  
  public void setSurplus(String paramString)
  {
    this.surplus = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/QuotaInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */