package com.imcash.gp.model;

import java.util.Iterator;
import java.util.List;

public class BBExchange
{
  private List<CoinBean> coin;
  private String rate;
  
  public List<CoinBean> getCoin()
  {
    return this.coin;
  }
  
  public CoinBean getCoinBeanByName(String paramString)
  {
    Iterator localIterator = getCoin().iterator();
    Object localObject = null;
    while (localIterator.hasNext())
    {
      CoinBean localCoinBean = (CoinBean)localIterator.next();
      if (paramString.equals(localCoinBean.getName())) {
        localObject = localCoinBean;
      }
    }
    return (CoinBean)localObject;
  }
  
  public String getIdByName(String paramString)
  {
    String str = "";
    Iterator localIterator = getCoin().iterator();
    do
    {
      localObject = str;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject = (CoinBean)localIterator.next();
    } while (!paramString.equals(((CoinBean)localObject).getName()));
    Object localObject = ((CoinBean)localObject).getId();
    return (String)localObject;
  }
  
  public String getRate()
  {
    return this.rate;
  }
  
  public void setCoin(List<CoinBean> paramList)
  {
    this.coin = paramList;
  }
  
  public void setRate(String paramString)
  {
    this.rate = paramString;
  }
  
  public static class CoinBean
  {
    private String id;
    private String name;
    private String num;
    private String price;
    
    public String getId()
    {
      return this.id;
    }
    
    public String getName()
    {
      return this.name;
    }
    
    public String getNum()
    {
      return this.num;
    }
    
    public String getPrice()
    {
      return this.price;
    }
    
    public void setId(String paramString)
    {
      this.id = paramString;
    }
    
    public void setName(String paramString)
    {
      this.name = paramString;
    }
    
    public void setNum(String paramString)
    {
      this.num = paramString;
    }
    
    public void setPrice(String paramString)
    {
      this.price = paramString;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/BBExchange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */