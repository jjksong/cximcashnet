package com.imcash.gp.model;

public class OutSide
{
  private String alipay_qrcode;
  private String balance;
  private String bank_card;
  private String bond;
  private String coin;
  private String id;
  private String max;
  private String merchant;
  private String min;
  private String minute;
  private String phone;
  private String wechat;
  private String wechat_qrcode;
  private String weight;
  
  public String getAlipay_qrcode()
  {
    return this.alipay_qrcode;
  }
  
  public String getBalance()
  {
    return this.balance;
  }
  
  public String getBank_card()
  {
    return this.bank_card;
  }
  
  public String getBond()
  {
    return this.bond;
  }
  
  public String getCoin()
  {
    return this.coin;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getMax()
  {
    return this.max;
  }
  
  public String getMerchant()
  {
    return this.merchant;
  }
  
  public String getMin()
  {
    return this.min;
  }
  
  public String getMinute()
  {
    return this.minute;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getWechat()
  {
    return this.wechat;
  }
  
  public String getWechat_qrcode()
  {
    return this.wechat_qrcode;
  }
  
  public String getWeight()
  {
    return this.weight;
  }
  
  public void setAlipay_qrcode(String paramString)
  {
    this.alipay_qrcode = paramString;
  }
  
  public void setBalance(String paramString)
  {
    this.balance = paramString;
  }
  
  public void setBank_card(String paramString)
  {
    this.bank_card = paramString;
  }
  
  public void setBond(String paramString)
  {
    this.bond = paramString;
  }
  
  public void setCoin(String paramString)
  {
    this.coin = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setMax(String paramString)
  {
    this.max = paramString;
  }
  
  public void setMerchant(String paramString)
  {
    this.merchant = paramString;
  }
  
  public void setMin(String paramString)
  {
    this.min = paramString;
  }
  
  public void setMinute(String paramString)
  {
    this.minute = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setWechat(String paramString)
  {
    this.wechat = paramString;
  }
  
  public void setWechat_qrcode(String paramString)
  {
    this.wechat_qrcode = paramString;
  }
  
  public void setWeight(String paramString)
  {
    this.weight = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/OutSide.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */