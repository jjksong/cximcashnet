package com.imcash.gp.model;

public class InviteDetail
{
  private String currencyid;
  private String name;
  private String num;
  private String sendtime;
  
  public String getCurrencyid()
  {
    return this.currencyid;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNum()
  {
    return this.num;
  }
  
  public String getSendtime()
  {
    return this.sendtime;
  }
  
  public void setCurrencyid(String paramString)
  {
    this.currencyid = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNum(String paramString)
  {
    this.num = paramString;
  }
  
  public void setSendtime(String paramString)
  {
    this.sendtime = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/InviteDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */