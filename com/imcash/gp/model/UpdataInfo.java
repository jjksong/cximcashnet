package com.imcash.gp.model;

public class UpdataInfo
{
  private String force;
  private String system;
  private String url;
  private String version_code;
  private String version_content;
  private String version_num;
  
  public String getForce()
  {
    return this.force;
  }
  
  public String getSystem()
  {
    return this.system;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public String getVersion_code()
  {
    return this.version_code;
  }
  
  public String getVersion_content()
  {
    return this.version_content;
  }
  
  public String getVersion_num()
  {
    return this.version_num;
  }
  
  public void setForce(String paramString)
  {
    this.force = paramString;
  }
  
  public void setSystem(String paramString)
  {
    this.system = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
  
  public void setVersion_code(String paramString)
  {
    this.version_code = paramString;
  }
  
  public void setVersion_content(String paramString)
  {
    this.version_content = paramString;
  }
  
  public void setVersion_num(String paramString)
  {
    this.version_num = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/UpdataInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */