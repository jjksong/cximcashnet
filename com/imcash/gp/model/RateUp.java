package com.imcash.gp.model;

public class RateUp
{
  private String add_bili;
  private String content;
  private String exchange_num;
  private String id;
  private String remark;
  private String title;
  private String validity;
  
  public String getAdd_bili()
  {
    return this.add_bili;
  }
  
  public String getContent()
  {
    return this.content;
  }
  
  public String getExchange_num()
  {
    return this.exchange_num;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getRemark()
  {
    return this.remark;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getValidity()
  {
    return this.validity;
  }
  
  public void setAdd_bili(String paramString)
  {
    this.add_bili = paramString;
  }
  
  public void setContent(String paramString)
  {
    this.content = paramString;
  }
  
  public void setExchange_num(String paramString)
  {
    this.exchange_num = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setRemark(String paramString)
  {
    this.remark = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setValidity(String paramString)
  {
    this.validity = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/RateUp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */