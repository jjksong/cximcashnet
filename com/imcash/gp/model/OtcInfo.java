package com.imcash.gp.model;

public class OtcInfo
{
  private int appkey;
  private String async_url;
  private String coin_amount;
  private String coin_sign;
  private String company_order_num;
  private String kyc;
  private String order_time;
  private String pay_card_bank;
  private String pay_card_num;
  private String phone;
  private String sign;
  private String sync_url;
  private String username;
  
  public int getAppkey()
  {
    return this.appkey;
  }
  
  public String getAsync_url()
  {
    return this.async_url;
  }
  
  public String getCoin_amount()
  {
    return this.coin_amount;
  }
  
  public String getCoin_sign()
  {
    return this.coin_sign;
  }
  
  public String getCompany_order_num()
  {
    return this.company_order_num;
  }
  
  public String getKyc()
  {
    return this.kyc;
  }
  
  public String getOrder_time()
  {
    return this.order_time;
  }
  
  public String getPay_card_bank()
  {
    return this.pay_card_bank;
  }
  
  public String getPay_card_num()
  {
    return this.pay_card_num;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getSign()
  {
    return this.sign;
  }
  
  public String getSync_url()
  {
    return this.sync_url;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public void setAppkey(int paramInt)
  {
    this.appkey = paramInt;
  }
  
  public void setAsync_url(String paramString)
  {
    this.async_url = paramString;
  }
  
  public void setCoin_amount(String paramString)
  {
    this.coin_amount = paramString;
  }
  
  public void setCoin_sign(String paramString)
  {
    this.coin_sign = paramString;
  }
  
  public void setCompany_order_num(String paramString)
  {
    this.company_order_num = paramString;
  }
  
  public void setKyc(String paramString)
  {
    this.kyc = paramString;
  }
  
  public void setOrder_time(String paramString)
  {
    this.order_time = paramString;
  }
  
  public void setPay_card_bank(String paramString)
  {
    this.pay_card_bank = paramString;
  }
  
  public void setPay_card_num(String paramString)
  {
    this.pay_card_num = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setSign(String paramString)
  {
    this.sign = paramString;
  }
  
  public void setSync_url(String paramString)
  {
    this.sync_url = paramString;
  }
  
  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/OtcInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */