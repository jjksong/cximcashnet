package com.imcash.gp.model;

import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.tools.RandomUntil;
import org.json.JSONObject;

public class User
  extends BaseModel
{
  public static final String KEY = "user_info";
  public String angel_num;
  public String id;
  public String mKeyNumber;
  public String num;
  public String username;
  
  public User(JSONObject paramJSONObject)
  {
    super(paramJSONObject);
    init();
  }
  
  protected String createKeyNumber()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    String str1 = RandomUntil.getLargeLetter(22);
    String str2 = GlobalFunction.getTenTime();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(str1);
    ((StringBuilder)localObject).append(str2);
    str1 = ((StringBuilder)localObject).toString();
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(this.num);
    ((StringBuilder)localObject).append(str1);
    localObject = MD5.encode(((StringBuilder)localObject).toString());
    for (int i = 0; i < ((String)localObject).length(); i++)
    {
      localStringBuffer.append(str1.charAt(i));
      localStringBuffer.append(((String)localObject).charAt(i));
    }
    return localStringBuffer.toString();
  }
  
  public String getHidePhone()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.username.substring(0, 3));
    localStringBuilder.append("****");
    String str = this.username;
    localStringBuilder.append(str.substring(7, str.length()));
    return localStringBuilder.toString();
  }
  
  public String getKeyNumber()
  {
    String str = this.mKeyNumber;
    if ((str == null) || (str.length() == 0)) {
      createKeyNumber();
    }
    return this.mKeyNumber;
  }
  
  protected void init()
  {
    this.username = this.mDataJson.optString("username", "");
    this.id = this.mDataJson.optString("id", "");
    this.num = this.mDataJson.optString("num", "");
    this.angel_num = this.mDataJson.optString("angel_num", "");
    this.mKeyNumber = createKeyNumber();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/model/User.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */