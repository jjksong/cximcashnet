package com.imcash.gp.GlobalFunction;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.imcash.gp.tools.ArithUtil;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GlobalFunction
{
  public static ArrayList<Double> changeSevenData(double[] paramArrayOfDouble)
  {
    ArrayList localArrayList = new ArrayList();
    int j = 0;
    double d2 = paramArrayOfDouble[0];
    double d5 = 0.0D;
    double d1 = 0.0D;
    int i = 0;
    while (i < paramArrayOfDouble.length)
    {
      double d3 = d5;
      if (Double.doubleToLongBits(d5) < Double.doubleToLongBits(paramArrayOfDouble[i])) {
        d3 = paramArrayOfDouble[i];
      }
      double d4 = d2;
      if (Double.doubleToLongBits(d2) > Double.doubleToLongBits(paramArrayOfDouble[i])) {
        d4 = paramArrayOfDouble[i];
      }
      d1 = ArithUtil.add(d1, paramArrayOfDouble[i]);
      i++;
      d5 = d3;
      d2 = d4;
    }
    d1 = (d5 - d2) / 8.0D;
    double[] arrayOfDouble = new double[7];
    double[] tmp120_118 = arrayOfDouble;
    tmp120_118[0] = 0.0D;
    double[] tmp124_120 = tmp120_118;
    tmp124_120[1] = 0.0D;
    double[] tmp128_124 = tmp124_120;
    tmp128_124[2] = 0.0D;
    double[] tmp132_128 = tmp128_124;
    tmp132_128[3] = 0.0D;
    double[] tmp136_132 = tmp132_128;
    tmp136_132[4] = 0.0D;
    double[] tmp140_136 = tmp136_132;
    tmp140_136[5] = 0.0D;
    double[] tmp144_140 = tmp140_136;
    tmp144_140[6] = 0.0D;
    tmp144_140;
    for (tmp124_120 = tmp128_124; tmp124_120 < paramArrayOfDouble.length; tmp124_120++)
    {
      tmp136_132[tmp124_120] = ArithUtil.div(paramArrayOfDouble[tmp124_120] - d2, d1);
      tmp132_128.add(Double.valueOf(tmp136_132[tmp124_120]));
    }
    return tmp132_128;
  }
  
  public static String changeTime(String paramString)
  {
    return paramString.replace("年", "-").replace("月", "-").replace("日", "");
  }
  
  public static String dateToString(Date paramDate, String paramString)
  {
    return new SimpleDateFormat(paramString).format(paramDate);
  }
  
  public static int dip2px(Context paramContext, float paramFloat)
  {
    return (int)(paramFloat * paramContext.getResources().getDisplayMetrics().density + 0.5F);
  }
  
  public static String doubleToString(double paramDouble)
  {
    return new DecimalFormat("0.00").format(paramDouble);
  }
  
  public static String doubleToStringEight(double paramDouble)
  {
    return new DecimalFormat("0.00000000").format(paramDouble);
  }
  
  public static Bitmap drawableToBitmap(Drawable paramDrawable)
  {
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    if (paramDrawable.getOpacity() != -1) {
      localObject = Bitmap.Config.ARGB_8888;
    } else {
      localObject = Bitmap.Config.RGB_565;
    }
    Object localObject = Bitmap.createBitmap(i, j, (Bitmap.Config)localObject);
    Canvas localCanvas = new Canvas((Bitmap)localObject);
    paramDrawable.setBounds(0, 0, i, j);
    paramDrawable.draw(localCanvas);
    return (Bitmap)localObject;
  }
  
  public static int getCoinImgByName(String paramString)
  {
    "BTC".equals(paramString);
    int i = 2131099794;
    if ("ETH".equals(paramString)) {
      i = 2131099795;
    }
    if ("LTC".equals(paramString)) {
      i = 2131099796;
    }
    if ("BCH".equals(paramString)) {
      i = 2131099793;
    }
    if ("USDT".equals(paramString)) {
      i = 2131099797;
    }
    return i;
  }
  
  public static int getCoinIndexByName(String paramString)
  {
    int k = 0;
    int j;
    for (int i = 0;; i++)
    {
      j = k;
      if (i >= Constants.CoinArray.length) {
        break;
      }
      if (paramString.equals(Constants.CoinArray[i]))
      {
        j = i;
        break;
      }
    }
    return j;
  }
  
  public static String getCurrentDate()
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
    return String.valueOf(localCalendar.get(5));
  }
  
  public static int getCurrentIndex()
  {
    return indexWeek(stringWeek());
  }
  
  public static String getCurrentSystemAllTime()
  {
    return new SimpleDateFormat("yyyy年MM月dd日 HH:mm", Locale.getDefault()).format(new Date());
  }
  
  public static String getCurrentSystemTime()
  {
    return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
  }
  
  public static String getDate(String paramString, int paramInt)
  {
    String str = "";
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    try
    {
      long l = localSimpleDateFormat.parse(paramString).getTime();
      paramString = new java/util/Date;
      paramString.<init>(l + paramInt * 24 * 60 * 60 * 1000);
      paramString = localSimpleDateFormat.format(paramString);
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      paramString = str;
    }
    return paramString;
  }
  
  public static String getDistanceTime(long paramLong1, long paramLong2)
  {
    if (paramLong1 < paramLong2) {
      paramLong1 = paramLong2 - paramLong1;
    } else {
      paramLong1 -= paramLong2;
    }
    paramLong2 = paramLong1 / 86400000L;
    long l1 = paramLong1 / 3600000L;
    long l2 = 24L * paramLong2;
    l1 -= l2;
    l2 = paramLong1 / 60000L - l2 * 60L - 60L * l1;
    paramLong1 /= 1000L;
    Object localObject2 = "";
    if (paramLong2 != 0L)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("");
      ((StringBuilder)localObject1).append(paramLong2);
      ((StringBuilder)localObject1).append("天");
      ((StringBuilder)localObject1).append("");
      localObject2 = ((StringBuilder)localObject1).toString();
    }
    Object localObject1 = localObject2;
    if (l1 != 0L)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append(l1);
      ((StringBuilder)localObject1).append("小时");
      ((StringBuilder)localObject1).append("");
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    localObject2 = localObject1;
    if (l2 != 0L)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(l2);
      ((StringBuilder)localObject2).append("分钟");
      ((StringBuilder)localObject2).append("");
      localObject2 = ((StringBuilder)localObject2).toString();
    }
    localObject1 = localObject2;
    if (((String)localObject2).length() == 0) {
      localObject1 = "刚刚";
    }
    return (String)localObject1;
  }
  
  public static long getFullTimeStampNumber(String paramString)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
    long l;
    try
    {
      l = localSimpleDateFormat.parse(paramString).getTime();
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      l = 0L;
    }
    return l;
  }
  
  public static long getInputTimeStamp(String paramString)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
    long l;
    try
    {
      l = localSimpleDateFormat.parse(paramString).getTime();
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      l = 0L;
    }
    return l;
  }
  
  public static int getLineaColor(String paramString)
  {
    "BTC".equals(paramString);
    int i = 2130968622;
    if ("ETH".equals(paramString)) {
      i = 2130968671;
    }
    if ("LTC".equals(paramString)) {
      i = 2130968711;
    }
    if ("BCH".equals(paramString)) {
      i = 2130968612;
    }
    if ("USDT".equals(paramString)) {
      i = 2130968770;
    }
    return i;
  }
  
  public static String getRandomStr()
  {
    String str = getTime();
    Random localRandom = new Random();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append("w");
    localStringBuilder.append(localRandom.nextInt(1000000));
    return localStringBuilder.toString();
  }
  
  public static String getStandDate(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramLong);
    localStringBuilder.append("");
    return getStandDate(localStringBuilder.toString());
  }
  
  public static String getStandDate(String paramString)
  {
    long l = Long.parseLong(paramString);
    l = (System.currentTimeMillis() - l) / 1000L;
    if (l < 60L)
    {
      paramString = "刚刚";
    }
    else
    {
      l /= 60L;
      if (l < 60L)
      {
        paramString = new StringBuilder();
        paramString.append(l);
        paramString.append("分钟前");
        paramString = paramString.toString();
      }
      else
      {
        l /= 60L;
        if (l < 24L)
        {
          paramString = new StringBuilder();
          paramString.append(l);
          paramString.append("小时前");
          paramString = paramString.toString();
        }
        else
        {
          l /= 24L;
          if (l < 30L)
          {
            paramString = new StringBuilder();
            paramString.append(l);
            paramString.append("天前");
            paramString = paramString.toString();
          }
          else
          {
            l /= 30L;
            if (l < 12L)
            {
              paramString = new StringBuilder();
              paramString.append(l);
              paramString.append("月前");
              paramString = paramString.toString();
            }
            else
            {
              l /= 12L;
              paramString = new StringBuilder();
              paramString.append(l);
              paramString.append("年前");
              paramString = paramString.toString();
            }
          }
        }
      }
    }
    return paramString;
  }
  
  public static int getStatusBarHeight(Context paramContext)
  {
    int i;
    try
    {
      Class localClass = Class.forName("com.android.internal.R$dimen");
      Object localObject = localClass.newInstance();
      i = Integer.parseInt(localClass.getField("status_bar_height").get(localObject).toString());
      i = paramContext.getResources().getDimensionPixelSize(i);
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
      i = 0;
    }
    return i;
  }
  
  public static String getTenTime()
  {
    return String.valueOf(System.currentTimeMillis() / 1000L);
  }
  
  public static String getTime()
  {
    return String.valueOf(System.currentTimeMillis());
  }
  
  public static long getTimeDifference(String paramString1, String paramString2)
  {
    return getInputTimeStamp(paramString1) - getInputTimeStamp(paramString2);
  }
  
  public static String getTodayDate()
  {
    return new SimpleDateFormat("MM-dd", Locale.getDefault()).format(new Date());
  }
  
  public static String getWeek(String paramString)
  {
    Object localObject = "";
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Calendar localCalendar = Calendar.getInstance();
    try
    {
      localCalendar.setTime(localSimpleDateFormat.parse(paramString));
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
    }
    if (localCalendar.get(7) == 1)
    {
      paramString = new StringBuilder();
      paramString.append("");
      paramString.append("日");
      localObject = paramString.toString();
    }
    paramString = (String)localObject;
    if (localCalendar.get(7) == 2)
    {
      paramString = new StringBuilder();
      paramString.append((String)localObject);
      paramString.append("一");
      paramString = paramString.toString();
    }
    localObject = paramString;
    if (localCalendar.get(7) == 3)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append("二");
      localObject = ((StringBuilder)localObject).toString();
    }
    paramString = (String)localObject;
    if (localCalendar.get(7) == 4)
    {
      paramString = new StringBuilder();
      paramString.append((String)localObject);
      paramString.append("三");
      paramString = paramString.toString();
    }
    localObject = paramString;
    if (localCalendar.get(7) == 5)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append("四");
      localObject = ((StringBuilder)localObject).toString();
    }
    paramString = (String)localObject;
    if (localCalendar.get(7) == 6)
    {
      paramString = new StringBuilder();
      paramString.append((String)localObject);
      paramString.append("五");
      paramString = paramString.toString();
    }
    localObject = paramString;
    if (localCalendar.get(7) == 7)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append("六");
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  public static int indexWeek(String paramString)
  {
    boolean bool = "一".equals(paramString);
    int i = 0;
    if (!bool) {
      if ("二".equals(paramString)) {
        i = 1;
      } else if ("三".equals(paramString)) {
        i = 2;
      } else if ("四".equals(paramString)) {
        i = 3;
      } else if ("五".equals(paramString)) {
        i = 4;
      } else if ("六".equals(paramString)) {
        i = 5;
      } else if ("日".equals(paramString)) {
        i = 6;
      }
    }
    return i;
  }
  
  public static boolean isEmail(String paramString)
  {
    if ((paramString != null) && (!"".equals(paramString))) {
      return Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*").matcher(paramString).matches();
    }
    return false;
  }
  
  public static int px2dip(Context paramContext, float paramFloat)
  {
    return (int)(paramFloat / paramContext.getResources().getDisplayMetrics().density + 0.5F);
  }
  
  public static int px2sp(Context paramContext, float paramFloat)
  {
    return (int)(paramFloat / paramContext.getResources().getDisplayMetrics().scaledDensity + 0.5F);
  }
  
  public static String stringWeek()
  {
    Object localObject = Calendar.getInstance();
    ((Calendar)localObject).setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
    String str = String.valueOf(((Calendar)localObject).get(7));
    if ("1".equals(str))
    {
      localObject = "日";
    }
    else if ("2".equals(str))
    {
      localObject = "一";
    }
    else if ("3".equals(str))
    {
      localObject = "二";
    }
    else if ("4".equals(str))
    {
      localObject = "三";
    }
    else if ("5".equals(str))
    {
      localObject = "四";
    }
    else if ("6".equals(str))
    {
      localObject = "五";
    }
    else
    {
      localObject = str;
      if ("7".equals(str)) {
        localObject = "六";
      }
    }
    return (String)localObject;
  }
  
  public static String timeToStandData(String paramString)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    long l;
    try
    {
      l = localSimpleDateFormat.parse(paramString).getTime();
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      l = 0L;
    }
    return getStandDate(l);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/GlobalFunction/GlobalFunction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */