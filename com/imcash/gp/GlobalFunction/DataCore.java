package com.imcash.gp.GlobalFunction;

import android.app.Application;
import android.os.Bundle;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.SPUtils;
import com.imcash.gp.ui.model.WindowInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class DataCore
{
  public static final String EyesStateKey = "EyesStateKey";
  public static final int EyesState_Close = 0;
  public static final int EyesState_Open = 1;
  public boolean gFinishRt = false;
  public Application mApp;
  public String mCurFragTag = "";
  public int mOutLogin = 0;
  public String mTargetTag = "";
  protected User mUserInfo;
  public WindowInfo mWindowInfo;
  
  public DataCore(Application paramApplication)
  {
    this.mApp = paramApplication;
  }
  
  public void cleanCacheData()
  {
    onDestroyCurrentPageTag();
    SPUtils.put(this.mApp, "user_info", "");
    this.mUserInfo = null;
  }
  
  public boolean getEyesIsOpen()
  {
    return getEyesState() != 0;
  }
  
  public int getEyesState()
  {
    return ((Integer)SPUtils.get(this.mApp, "EyesStateKey", Integer.valueOf(0))).intValue();
  }
  
  public String getToken()
  {
    if (getUserInfo() == null) {
      return "";
    }
    return getUserInfo().getKeyNumber();
  }
  
  public String getUserId()
  {
    if (!isLogin()) {
      return "";
    }
    return getUserInfo().id;
  }
  
  public User getUserInfo()
  {
    Object localObject;
    if (this.mUserInfo == null)
    {
      localObject = (String)SPUtils.get(this.mApp, "user_info", "");
      if ((localObject == null) || (((String)localObject).length() == 0)) {}
    }
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>((String)localObject);
      localObject = new com/imcash/gp/model/User;
      ((User)localObject).<init>(localJSONObject);
      this.mUserInfo = ((User)localObject);
      return this.mUserInfo;
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
  }
  
  public boolean isLogin()
  {
    return (getToken() != null) && (getToken().length() != 0);
  }
  
  public void onDestroyCurrentPageTag()
  {
    this.mCurFragTag = "";
    this.mTargetTag = "";
  }
  
  public void resetData(Bundle paramBundle) {}
  
  public void saveData(Bundle paramBundle) {}
  
  public void setEyesState(int paramInt)
  {
    SPUtils.put(this.mApp, "EyesStateKey", Integer.valueOf(paramInt));
  }
  
  public void setUserInfo(JSONObject paramJSONObject)
  {
    SPUtils.put(this.mApp, "user_info", paramJSONObject.toString());
    this.mUserInfo = new User(paramJSONObject);
    setEyesState(paramJSONObject.optJSONObject("data").optInt("eye", 1));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/GlobalFunction/DataCore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */