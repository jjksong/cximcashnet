package com.imcash.gp.GlobalFunction;

public class Constants
{
  public static final int BASE_UPDATA_LIST = 15;
  public static final String[] CoinArray = { "", "", "", "BTC", "BCH", "LTC", "ETH", "USDT" };
  public static final String[] CoinType = { "BTC", "ETH", "LTC", "BCH", "USDT" };
  public static final boolean Debug = false;
  public static final String Device_Type = "android";
  public static final int EyeClose = 1;
  public static final int EyeOpen = 0;
  public static final String GuidImgKey = "GuidImgKey";
  public static final int GuidImgVersion = 1;
  public static final String LOAD_LOADING = "加载中...";
  public static final String LOAD_TITLE = "提示";
  public static final boolean TestServer = false;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/GlobalFunction/Constants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */