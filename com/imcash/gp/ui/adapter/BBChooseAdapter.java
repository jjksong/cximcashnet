package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.BBExchange.CoinBean;
import com.imcash.gp.ui.activity.BBChooseCoinAct;
import java.util.List;

public class BBChooseAdapter
  extends RecyclerView.Adapter<MyHolder>
{
  protected Context mContext;
  protected List<BBExchange.CoinBean> mDatas;
  
  public BBChooseAdapter(Context paramContext, List<BBExchange.CoinBean> paramList)
  {
    this.mDatas = paramList;
    this.mContext = paramContext;
  }
  
  public int getItemCount()
  {
    List localList = this.mDatas;
    int i;
    if (localList == null) {
      i = 0;
    } else {
      i = localList.size();
    }
    return i;
  }
  
  public void onBindViewHolder(MyHolder paramMyHolder, final int paramInt)
  {
    BBExchange.CoinBean localCoinBean = (BBExchange.CoinBean)this.mDatas.get(paramInt);
    paramMyHolder.source_coin_img.setImageResource(GlobalFunction.getCoinImgByName(localCoinBean.getName()));
    paramMyHolder.source_coin_name.setText(localCoinBean.getName());
    TextView localTextView = paramMyHolder.cout_num;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("余额：");
    localStringBuilder.append(localCoinBean.getNum());
    localTextView.setText(localStringBuilder.toString());
    paramMyHolder.coin_layout.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ((BBChooseCoinAct)BBChooseAdapter.this.mContext).coinClicked(paramInt);
      }
    });
  }
  
  public MyHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new MyHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2131296286, paramViewGroup, false));
  }
  
  class MyHolder
    extends RecyclerView.ViewHolder
  {
    public LinearLayout coin_layout;
    public TextView cout_num;
    public ImageView source_coin_img;
    public TextView source_coin_name;
    
    public MyHolder(View paramView)
    {
      super();
      this.source_coin_img = ((ImageView)paramView.findViewById(2131165777));
      this.source_coin_name = ((TextView)paramView.findViewById(2131165778));
      this.cout_num = ((TextView)paramView.findViewById(2131165387));
      this.coin_layout = ((LinearLayout)paramView.findViewById(2131165353));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/BBChooseAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */