package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.SuperInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class SuperHistroyAdapter
  extends BaseCommonAdapter<SuperInfo>
{
  public SuperHistroyAdapter(Context paramContext, ArrayList<SuperInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296388, paramViewGroup, false);
      paramView = new SuperItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((SuperItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(SuperItem paramSuperItem, int paramInt)
  {
    SuperInfo localSuperInfo = (SuperInfo)getItem(paramInt);
    paramSuperItem.name.setText(localSuperInfo.getPhone());
    paramSuperItem.info.setText(localSuperInfo.getGrade());
    Object localObject2 = paramSuperItem.num;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(localSuperInfo.getNum());
    ((StringBuilder)localObject1).append(localSuperInfo.getName());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = paramSuperItem.lable;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(localSuperInfo.getDays());
    ((StringBuilder)localObject2).append("天");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject2 = paramSuperItem.profit;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("结算额：");
    ((StringBuilder)localObject1).append(localSuperInfo.getProfit());
    ((StringBuilder)localObject1).append("USDT");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    paramSuperItem.time_tv.setText(localSuperInfo.getStarttime());
  }
  
  private class SuperItem
  {
    @ViewInject(2131165497)
    public TextView info;
    @ViewInject(2131165524)
    public TextView lable;
    @ViewInject(2131165598)
    public TextView name;
    @ViewInject(2131165615)
    public TextView num;
    @ViewInject(2131165663)
    public TextView profit;
    @ViewInject(2131165820)
    public TextView time_tv;
    
    private SuperItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/SuperHistroyAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */