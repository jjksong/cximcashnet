package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.model.RateUpCard;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class FinancingCardAdapter
  extends BaseCommonAdapter<RateUpCard>
{
  protected boolean mCanSelecte = true;
  
  public FinancingCardAdapter(Context paramContext, ArrayList<RateUpCard> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296333, paramViewGroup, false);
      paramView = new CardItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((CardItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(CardItem paramCardItem, int paramInt)
  {
    RateUpCard localRateUpCard = (RateUpCard)getItem(paramInt);
    paramCardItem.name.setText(localRateUpCard.getCoupon_title());
    paramCardItem.time.setText(localRateUpCard.getEnd_time());
    TextView localTextView = paramCardItem.rate;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("增加利率");
    localStringBuilder.append(localRateUpCard.getAdd_bili());
    localTextView.setText(localStringBuilder.toString());
    paramCardItem.rule.setText(localRateUpCard.getCoupon_remark());
    if (localRateUpCard.mIsSelected) {
      paramCardItem.choose_img.setImageResource(2131099739);
    } else {
      paramCardItem.choose_img.setImageResource(2131099738);
    }
    if (!this.mCanSelecte) {
      paramCardItem.choose_img.setVisibility(8);
    }
  }
  
  public void setCanSelected(boolean paramBoolean)
  {
    this.mCanSelecte = paramBoolean;
  }
  
  private class CardItem
  {
    @ViewInject(2131165335)
    public ImageView choose_img;
    @ViewInject(2131165598)
    public TextView name;
    @ViewInject(2131165690)
    public TextView rate;
    @ViewInject(2131165726)
    public TextView rule;
    @ViewInject(2131165817)
    public TextView time;
    
    private CardItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/FinancingCardAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */