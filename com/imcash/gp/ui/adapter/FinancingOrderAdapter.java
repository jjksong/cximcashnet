package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.FinancingOrder;
import com.imcash.gp.ui.activity.FinancingOrderAct;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class FinancingOrderAdapter
  extends BaseCommonAdapter<FinancingOrder>
{
  public FinancingOrderAdapter(Context paramContext, ArrayList<FinancingOrder> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296336, paramViewGroup, false);
      paramView = new OrderItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((OrderItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(OrderItem paramOrderItem, final int paramInt)
  {
    FinancingOrder localFinancingOrder = (FinancingOrder)getItem(paramInt);
    paramOrderItem.coin_img.setImageResource(GlobalFunction.getCoinImgByName(localFinancingOrder.getCoin()));
    paramOrderItem.name.setText(localFinancingOrder.getName());
    Object localObject2 = paramOrderItem.percent;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("收益率");
    ((StringBuilder)localObject1).append(localFinancingOrder.getRate());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = paramOrderItem.pro_id;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("ID:");
    ((StringBuilder)localObject2).append(localFinancingOrder.getTrade_no());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramOrderItem.time.setText(localFinancingOrder.getShowTime());
    localObject1 = paramOrderItem.in_money;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("共投入");
    ((StringBuilder)localObject2).append(localFinancingOrder.getNum());
    ((StringBuilder)localObject2).append(localFinancingOrder.getCoin());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramOrderItem.add_card.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        FinancingOrderAdapter.this.toAddCard(paramInt);
      }
    });
    localObject1 = paramOrderItem.count_time;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("持仓期限");
    ((StringBuilder)localObject2).append(localFinancingOrder.getDays());
    ((StringBuilder)localObject2).append("天");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    if (!localFinancingOrder.getName().contains("体验")) {
      paramOrderItem.add_card.setVisibility(0);
    } else {
      paramOrderItem.add_card.setVisibility(4);
    }
  }
  
  protected void toAddCard(int paramInt)
  {
    ((FinancingOrderAct)this.mContext).toAddCard(paramInt);
  }
  
  private class OrderItem
  {
    @ViewInject(2131165223)
    public TextView add_card;
    @ViewInject(2131165352)
    public ImageView coin_img;
    @ViewInject(2131165383)
    public TextView count_time;
    @ViewInject(2131165489)
    public TextView in_money;
    @ViewInject(2131165598)
    public TextView name;
    @ViewInject(2131165650)
    public TextView percent;
    @ViewInject(2131165660)
    public TextView pro_id;
    @ViewInject(2131165817)
    public TextView time;
    
    private OrderItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/FinancingOrderAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */