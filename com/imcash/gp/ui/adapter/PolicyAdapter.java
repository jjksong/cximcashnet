package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.Policy;
import com.imcash.gp.ui.activity.PolicyAct;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class PolicyAdapter
  extends BaseCommonAdapter<Policy>
{
  public PolicyAdapter(Context paramContext, ArrayList<Policy> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public void buyPolicy(int paramInt)
  {
    ((PolicyAct)this.mContext).buyClicked(paramInt);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296372, paramViewGroup, false);
      paramView = new PolicyItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((PolicyItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(PolicyItem paramPolicyItem, final int paramInt)
  {
    final Policy localPolicy = (Policy)this.mDatas.get(paramInt);
    TextView localTextView = paramPolicyItem.id_tv;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ID:");
    localStringBuilder.append(localPolicy.getId());
    localTextView.setText(localStringBuilder.toString());
    paramPolicyItem.account.setText(localPolicy.getPhone());
    localTextView = paramPolicyItem.money_tv;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(localPolicy.getAmount());
    localStringBuilder.append("USDT");
    localTextView.setText(localStringBuilder.toString());
    paramPolicyItem.time_tv.setText(localPolicy.getShowTime());
    paramPolicyItem.state_btn.setBackgroundResource(localPolicy.getStateBackColor());
    paramPolicyItem.state_btn.setText(localPolicy.getStateStr());
    paramPolicyItem.state_btn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (!localPolicy.canBuy()) {
          return;
        }
        PolicyAdapter.this.buyPolicy(paramInt);
      }
    });
  }
  
  private class PolicyItem
  {
    @ViewInject(2131165199)
    public TextView account;
    @ViewInject(2131165478)
    public TextView id_tv;
    @ViewInject(2131165590)
    public TextView money_tv;
    @ViewInject(2131165792)
    public TextView state_btn;
    @ViewInject(2131165820)
    public TextView time_tv;
    
    private PolicyItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/PolicyAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */