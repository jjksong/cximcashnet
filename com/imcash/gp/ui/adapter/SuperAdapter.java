package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.SuperInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class SuperAdapter
  extends BaseCommonAdapter<SuperInfo>
{
  public SuperAdapter(Context paramContext, ArrayList<SuperInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296389, paramViewGroup, false);
      paramView = new SuperItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((SuperItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(SuperItem paramSuperItem, int paramInt)
  {
    SuperInfo localSuperInfo = (SuperInfo)getItem(paramInt);
    paramSuperItem.name.setText(localSuperInfo.getPhone());
    paramSuperItem.info.setText(localSuperInfo.getGrade());
    Object localObject = paramSuperItem.num;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localSuperInfo.getNum());
    localStringBuilder.append(localSuperInfo.getName());
    ((TextView)localObject).setText(localStringBuilder.toString());
    paramSuperItem = paramSuperItem.lable;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(localSuperInfo.getDays());
    ((StringBuilder)localObject).append("天");
    paramSuperItem.setText(((StringBuilder)localObject).toString());
  }
  
  private class SuperItem
  {
    @ViewInject(2131165497)
    public TextView info;
    @ViewInject(2131165524)
    public TextView lable;
    @ViewInject(2131165598)
    public TextView name;
    @ViewInject(2131165615)
    public TextView num;
    
    private SuperItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/SuperAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */