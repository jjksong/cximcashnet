package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.TradeInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class TradeAdapter
  extends BaseCommonAdapter<TradeInfo>
{
  public TradeAdapter(Context paramContext, ArrayList<TradeInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296396, paramViewGroup, false);
      paramView = new TradeItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((TradeItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(TradeItem paramTradeItem, int paramInt)
  {
    TradeInfo localTradeInfo = (TradeInfo)getItem(paramInt);
    paramTradeItem.trade_name.setText(localTradeInfo.getName());
    Object localObject1 = paramTradeItem.coin_type;
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("购入币种: ");
    ((StringBuilder)localObject2).append(localTradeInfo.getCurrency_name());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject1 = paramTradeItem.source_money;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(localTradeInfo.getNum());
    ((StringBuilder)localObject2).append(localTradeInfo.getCurrency_name());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject2 = paramTradeItem.endtime;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(localTradeInfo.getEnd_time());
    ((StringBuilder)localObject1).append("天");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = paramTradeItem.rate_tv;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(localTradeInfo.getRate());
    ((StringBuilder)localObject2).append("");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramTradeItem = paramTradeItem.amount_tv;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(localTradeInfo.getAmount());
    ((StringBuilder)localObject1).append(localTradeInfo.getCurrency_name());
    paramTradeItem.setText(((StringBuilder)localObject1).toString());
  }
  
  private class TradeItem
  {
    @ViewInject(2131165242)
    public TextView amount_tv;
    @ViewInject(2131165357)
    public TextView coin_type;
    @ViewInject(2131165423)
    public TextView endtime;
    @ViewInject(2131165694)
    public TextView rate_tv;
    @ViewInject(2131165780)
    public TextView source_money;
    @ViewInject(2131165848)
    public TextView trade_name;
    
    private TradeItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/TradeAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */