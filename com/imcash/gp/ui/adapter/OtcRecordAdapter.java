package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.OtcRecord;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class OtcRecordAdapter
  extends BaseCommonAdapter<OtcRecord>
{
  public OtcRecordAdapter(Context paramContext, ArrayList<OtcRecord> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296366, paramViewGroup, false);
      paramView = new RecordItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((RecordItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(RecordItem paramRecordItem, int paramInt)
  {
    OtcRecord localOtcRecord = (OtcRecord)this.mDatas.get(paramInt);
    paramRecordItem.title_tv.setText(localOtcRecord.getType());
    paramRecordItem.type_tv.setText(localOtcRecord.getState());
    TextView localTextView = paramRecordItem.usdt_count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localOtcRecord.getAmount());
    localStringBuilder.append("USDT");
    localTextView.setText(localStringBuilder.toString());
    paramRecordItem.time_tv.setText(localOtcRecord.getCreate_time());
    paramRecordItem.type_tv.setBackgroundResource(localOtcRecord.getStateRes());
    paramRecordItem.type_tv.setTextColor(localOtcRecord.getStateColor());
  }
  
  private class RecordItem
  {
    @ViewInject(2131165820)
    protected TextView time_tv;
    @ViewInject(2131165829)
    protected TextView title_tv;
    @ViewInject(2131165874)
    protected TextView type_tv;
    @ViewInject(2131165881)
    protected TextView usdt_count;
    
    private RecordItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/OtcRecordAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */