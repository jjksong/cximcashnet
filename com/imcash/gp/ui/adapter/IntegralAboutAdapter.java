package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.imcash.gp.model.IntegralAbout;
import java.util.ArrayList;

public class IntegralAboutAdapter
  extends BaseCommonAdapter<IntegralAbout>
{
  public IntegralAboutAdapter(Context paramContext, ArrayList<IntegralAbout> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296290, paramViewGroup, false);
      paramView = new IntegralAboutItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((IntegralAboutItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(IntegralAboutItem paramIntegralAboutItem, int paramInt) {}
  
  protected void initView(View paramView, IntegralAboutItem paramIntegralAboutItem) {}
  
  private class IntegralAboutItem
  {
    private IntegralAboutItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/IntegralAboutAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */