package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.TradeInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class TradeMineAdapter
  extends BaseCommonAdapter<TradeInfo>
{
  public TradeMineAdapter(Context paramContext, ArrayList<TradeInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296399, paramViewGroup, false);
      paramView = new MineItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((MineItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(MineItem paramMineItem, int paramInt)
  {
    TradeInfo localTradeInfo = (TradeInfo)getItem(paramInt);
    paramMineItem.trade_name.setText(localTradeInfo.getName());
    paramMineItem.time_tv.setText(localTradeInfo.getCreate_time());
    paramMineItem.status_tv.setText(localTradeInfo.getStatusStr());
    TextView localTextView = paramMineItem.money_count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localTradeInfo.getAmount());
    localStringBuilder.append(" ");
    localStringBuilder.append(localTradeInfo.getCurrency_name());
    localTextView.setText(localStringBuilder.toString());
    localTextView = paramMineItem.order_id;
    paramMineItem = new StringBuilder();
    paramMineItem.append("挂单号：");
    paramMineItem.append(localTradeInfo.getTrade_no());
    localTextView.setText(paramMineItem.toString());
  }
  
  private class MineItem
  {
    @ViewInject(2131165583)
    public TextView money_count;
    @ViewInject(2131165624)
    protected TextView order_id;
    @ViewInject(2131165795)
    public TextView status_tv;
    @ViewInject(2131165820)
    public TextView time_tv;
    @ViewInject(2131165848)
    public TextView trade_name;
    
    private MineItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/TradeMineAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */