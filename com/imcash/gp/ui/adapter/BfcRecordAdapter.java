package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.BfcTokenRecord;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class BfcRecordAdapter
  extends BaseCommonAdapter<BfcTokenRecord>
{
  public BfcRecordAdapter(Context paramContext, ArrayList<BfcTokenRecord> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296434, paramViewGroup, false);
      paramView = new BfcRecordItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((BfcRecordItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(BfcRecordItem paramBfcRecordItem, int paramInt)
  {
    BfcTokenRecord localBfcTokenRecord = (BfcTokenRecord)getItem(paramInt);
    paramBfcRecordItem.top_view.setVisibility(0);
    paramBfcRecordItem.top_line.setVisibility(8);
    paramBfcRecordItem.left_view.setVisibility(0);
    paramBfcRecordItem.right_view.setVisibility(0);
    paramBfcRecordItem.time_tv.setText(localBfcTokenRecord.getMessage());
    paramBfcRecordItem.source_tv.setText(localBfcTokenRecord.getCreate_time());
    TextView localTextView = paramBfcRecordItem.count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localBfcTokenRecord.getSymbolSign());
    localStringBuilder.append(localBfcTokenRecord.getAmount());
    localStringBuilder.append("BFC");
    localTextView.setText(localStringBuilder.toString());
    if (1 == localBfcTokenRecord.getSymbol()) {
      paramBfcRecordItem.count.setTextColor(Color.rgb(76, 197, 161));
    } else {
      paramBfcRecordItem.count.setTextColor(Color.rgb(238, 65, 55));
    }
  }
  
  private class BfcRecordItem
  {
    @ViewInject(2131165379)
    protected TextView count;
    @ViewInject(2131165530)
    protected View left_view;
    @ViewInject(2131165719)
    protected View right_view;
    @ViewInject(2131165782)
    protected TextView source_tv;
    @ViewInject(2131165820)
    protected TextView time_tv;
    @ViewInject(2131165840)
    protected View top_line;
    @ViewInject(2131165845)
    protected View top_view;
    
    private BfcRecordItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/BfcRecordAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */