package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.InviteInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class InviteAdapter
  extends BaseCommonAdapter<InviteInfo>
{
  public InviteAdapter(Context paramContext, ArrayList<InviteInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296344, paramViewGroup, false);
      paramView = new InviteItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((InviteItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(InviteItem paramInviteItem, int paramInt)
  {
    InviteInfo localInviteInfo = (InviteInfo)this.mDatas.get(paramInt);
    paramInviteItem.title.setText(localInviteInfo.getNickname());
    paramInviteItem.num.setText(localInviteInfo.getNum());
    paramInviteItem.time.setText(localInviteInfo.getAdd_time());
    paramInviteItem = paramInviteItem.bfc_count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("BFC:");
    localStringBuilder.append(localInviteInfo.getBfc_balance());
    paramInviteItem.setText(localStringBuilder.toString());
  }
  
  private class InviteItem
  {
    @ViewInject(2131165272)
    protected TextView bfc_count;
    @ViewInject(2131165615)
    protected TextView num;
    @ViewInject(2131165817)
    protected TextView time;
    @ViewInject(2131165822)
    protected TextView title;
    
    private InviteItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/InviteAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */