package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.model.ListDataBean;
import com.imcash.gp.ui.activity.FinancingDetailAct;
import java.util.ArrayList;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class MainAdapter
  extends BaseCommonAdapter<ListDataBean>
{
  protected int mCutIndex = 0;
  
  public MainAdapter(Context paramContext, ArrayList<ListDataBean> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public int getCount()
  {
    int i;
    if (this.mDatas == null) {
      i = 0;
    } else {
      i = this.mDatas.size();
    }
    return i;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296436, paramViewGroup, false);
      paramView = new MainItem(null);
      x.view().inject(paramView, localView);
      localView.setTag(paramView);
    }
    initData((MainItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(MainItem paramMainItem, final int paramInt)
  {
    ListDataBean localListDataBean = (ListDataBean)this.mDatas.get(paramInt);
    if (paramInt == this.mCutIndex) {
      paramMainItem.top_view.setVisibility(0);
    } else {
      paramMainItem.top_view.setVisibility(8);
    }
    if ((paramInt != 0) && (this.mCutIndex != paramInt))
    {
      paramMainItem.top_layout.setVisibility(8);
      paramMainItem.top_line.setVisibility(8);
    }
    else
    {
      paramMainItem.top_layout.setVisibility(0);
      paramMainItem.top_line.setVisibility(8);
    }
    if (paramInt == 0) {
      paramMainItem.top_msg.setText("智能推荐");
    }
    if (this.mCutIndex == paramInt) {
      paramMainItem.top_msg.setText("精选推荐");
    }
    paramMainItem.project_name.setText(localListDataBean.getName());
    TextView localTextView = paramMainItem.percent_tv;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("+");
    localStringBuilder.append(localListDataBean.getIncome_no());
    localTextView.setText(localStringBuilder.toString());
    localTextView = paramMainItem.time_tv;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("预计收益时间：");
    localStringBuilder.append(localListDataBean.getTimelist());
    localStringBuilder.append("天");
    localTextView.setText(localStringBuilder.toString());
    paramMainItem.click_purchase.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = (ListDataBean)MainAdapter.this.mDatas.get(paramInt);
        Intent localIntent = new Intent();
        localIntent.putExtra("FinancingDetailKey", new Gson().toJson(paramAnonymousView));
        localIntent.setClass(MainAdapter.this.mContext, FinancingDetailAct.class);
        MainAdapter.this.mContext.startActivity(localIntent);
      }
    });
  }
  
  public void setCutIndex(int paramInt)
  {
    this.mCutIndex = paramInt;
  }
  
  private class MainItem
  {
    @ViewInject(2131165290)
    protected View bottom_line;
    @ViewInject(2131165344)
    public TextView click_purchase;
    @ViewInject(2131165651)
    public TextView percent_tv;
    @ViewInject(2131165666)
    public TextView project_name;
    @ViewInject(2131165820)
    public TextView time_tv;
    @ViewInject(2131165839)
    public LinearLayout top_layout;
    @ViewInject(2131165840)
    protected View top_line;
    @ViewInject(2131165842)
    public TextView top_msg;
    @ViewInject(2131165845)
    public View top_view;
    
    private MainItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/MainAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */