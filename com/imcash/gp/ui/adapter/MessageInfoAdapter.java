package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.ReplyInfo;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class MessageInfoAdapter
  extends BaseCommonAdapter<ReplyInfo>
{
  public MessageInfoAdapter(Context paramContext, ArrayList<ReplyInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296354, paramViewGroup, false);
      paramView = new MessageItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((MessageItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(MessageItem paramMessageItem, int paramInt)
  {
    ReplyInfo localReplyInfo = (ReplyInfo)getItem(paramInt);
    if (localReplyInfo.getReply().length() == 0) {
      paramMessageItem.reply_tv.setText("未回复");
    } else {
      paramMessageItem.reply_tv.setText(localReplyInfo.getReply());
    }
    paramMessageItem.time_tv.setText(localReplyInfo.getCreate_time());
    paramMessageItem.ques_tv.setText(localReplyInfo.getContent());
  }
  
  private class MessageItem
  {
    @ViewInject(2131165684)
    protected TextView ques_tv;
    @ViewInject(2131165708)
    protected TextView reply_tv;
    @ViewInject(2131165820)
    protected TextView time_tv;
    
    private MessageItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/MessageInfoAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */