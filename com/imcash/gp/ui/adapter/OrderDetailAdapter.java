package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.imcash.gp.model.OrderDetail;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class OrderDetailAdapter
  extends BaseCommonAdapter<OrderDetail>
{
  public OrderDetailAdapter(Context paramContext, ArrayList<OrderDetail> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296363, paramViewGroup, false);
      paramView = new OrderDetailItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((OrderDetailItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(OrderDetailItem paramOrderDetailItem, int paramInt)
  {
    OrderDetail localOrderDetail = (OrderDetail)getItem(paramInt);
    if (paramInt == 0) {
      paramOrderDetailItem.title_layout.setVisibility(0);
    } else {
      paramOrderDetailItem.title_layout.setVisibility(8);
    }
    if (getCount() - 1 == paramInt)
    {
      paramOrderDetailItem.bottom_long_line.setVisibility(0);
      paramOrderDetailItem.bottom_short_line.setVisibility(8);
    }
    else
    {
      paramOrderDetailItem.bottom_long_line.setVisibility(8);
      paramOrderDetailItem.bottom_short_line.setVisibility(0);
    }
    TextView localTextView = paramOrderDetailItem.msg;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localOrderDetail.getName());
    localStringBuilder.append(" - ");
    localStringBuilder.append(localOrderDetail.getMessage());
    localTextView.setText(localStringBuilder.toString());
    paramOrderDetailItem.time.setText(localOrderDetail.getCreate_time());
    if ("1".equals(localOrderDetail.getSymbol()))
    {
      paramOrderDetailItem = paramOrderDetailItem.amount;
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("+");
      localStringBuilder.append(localOrderDetail.getAmount());
      paramOrderDetailItem.setText(localStringBuilder.toString());
    }
    else
    {
      paramOrderDetailItem = paramOrderDetailItem.amount;
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("-");
      localStringBuilder.append(localOrderDetail.getAmount());
      paramOrderDetailItem.setText(localStringBuilder.toString());
    }
  }
  
  private class OrderDetailItem
  {
    @ViewInject(2131165241)
    protected TextView amount;
    @ViewInject(2131165291)
    protected View bottom_long_line;
    @ViewInject(2131165292)
    protected View bottom_short_line;
    @ViewInject(2131165593)
    protected TextView msg;
    @ViewInject(2131165817)
    protected TextView time;
    @ViewInject(2131165825)
    protected LinearLayout title_layout;
    
    private OrderDetailItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/OrderDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */