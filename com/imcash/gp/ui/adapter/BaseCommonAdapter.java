package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import org.xutils.ViewInjector;
import org.xutils.x;

public abstract class BaseCommonAdapter<T>
  extends BaseAdapter
{
  protected Context mContext;
  protected ArrayList<T> mDatas;
  protected LayoutInflater mInflater;
  
  public BaseCommonAdapter(Context paramContext)
  {
    this.mContext = paramContext;
    this.mInflater = LayoutInflater.from(this.mContext);
  }
  
  public BaseCommonAdapter(Context paramContext, ArrayList<T> paramArrayList)
  {
    this.mContext = paramContext;
    this.mInflater = LayoutInflater.from(this.mContext);
    this.mDatas = paramArrayList;
  }
  
  public int getCount()
  {
    ArrayList localArrayList = this.mDatas;
    int i;
    if (localArrayList == null) {
      i = 0;
    } else {
      i = localArrayList.size();
    }
    return i;
  }
  
  public T getItem(int paramInt)
  {
    ArrayList localArrayList = this.mDatas;
    if (localArrayList == null) {
      return null;
    }
    return (T)localArrayList.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public abstract View getView(int paramInt, View paramView, ViewGroup paramViewGroup);
  
  protected void initView(View paramView, Object paramObject)
  {
    x.view().inject(paramObject, paramView);
  }
  
  public void setData(ArrayList<T> paramArrayList)
  {
    this.mDatas = paramArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/BaseCommonAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */