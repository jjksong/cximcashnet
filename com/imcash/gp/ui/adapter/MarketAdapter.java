package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.CoinInfo;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.LineView;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class MarketAdapter
  extends BaseCommonAdapter<CoinInfo>
{
  public MarketAdapter(Context paramContext, ArrayList<CoinInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296450, paramViewGroup, false);
      paramView = new MarketItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((MarketItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(MarketItem paramMarketItem, int paramInt)
  {
    CoinInfo localCoinInfo = (CoinInfo)getItem(paramInt);
    paramMarketItem.coin_img.setImageResource(GlobalFunction.getCoinImgByName(localCoinInfo.getCoin()));
    paramMarketItem.coin_name.setText(localCoinInfo.getCoin());
    TextView localTextView;
    Object localObject;
    if (!"USDT".equals(localCoinInfo.getCoin()))
    {
      localTextView = paramMarketItem.money_cny;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("≈");
      ((StringBuilder)localObject).append(localCoinInfo.getUsd());
      ((StringBuilder)localObject).append(" USDT");
      localTextView.setText(((StringBuilder)localObject).toString());
    }
    else
    {
      localTextView = paramMarketItem.money_cny;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("≈");
      ((StringBuilder)localObject).append(localCoinInfo.getCny());
      ((StringBuilder)localObject).append(" CNY");
      localTextView.setText(((StringBuilder)localObject).toString());
    }
    paramMarketItem.u_money.setText(localCoinInfo.getBalance());
    if (!ImcashApplication.getInstance().getDataCore().getEyesIsOpen()) {
      paramMarketItem.u_money.setText("****");
    }
    if (!"USDT".equals(localCoinInfo.getCoin()))
    {
      localObject = localCoinInfo.getDoubleArray();
      paramMarketItem.line_view.setData(GlobalFunction.changeSevenData((double[])localObject), 8, 2, localCoinInfo.getLineColor());
    }
  }
  
  private class MarketItem
  {
    @ViewInject(2131165352)
    public ImageView coin_img;
    @ViewInject(2131165356)
    public TextView coin_name;
    @ViewInject(2131165534)
    public LineView line_view;
    @ViewInject(2131165582)
    public TextView money_cny;
    @ViewInject(2131165875)
    public TextView u_money;
    
    private MarketItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/MarketAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */