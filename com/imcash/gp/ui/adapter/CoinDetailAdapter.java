package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.imcash.gp.model.CoinDetail;
import java.util.ArrayList;

public class CoinDetailAdapter
  extends BaseCommonAdapter<CoinDetail>
{
  public CoinDetailAdapter(Context paramContext, ArrayList<CoinDetail> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296327, paramViewGroup, false);
      paramView = new CoinDetailItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((CoinDetailItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(CoinDetailItem paramCoinDetailItem, int paramInt) {}
  
  protected void initView(View paramView, CoinDetailItem paramCoinDetailItem) {}
  
  private class CoinDetailItem
  {
    private CoinDetailItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/CoinDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */