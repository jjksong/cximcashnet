package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.Mission;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class MissionAdapter
  extends BaseCommonAdapter<Mission>
{
  public MissionAdapter(Context paramContext, ArrayList<Mission> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296356, paramViewGroup, false);
      paramView = new MissionItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((MissionItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(MissionItem paramMissionItem, int paramInt)
  {
    Mission localMission = (Mission)this.mDatas.get(paramInt);
    if (paramInt == 0) {
      paramMissionItem.title_tv.setVisibility(0);
    } else {
      paramMissionItem.title_tv.setVisibility(8);
    }
    paramMissionItem.mission_name.setText(localMission.getContent());
    if (localMission.isComplete())
    {
      paramMissionItem.can_do.setVisibility(8);
      paramMissionItem.complete_state.setVisibility(0);
    }
    else
    {
      paramMissionItem.can_do.setVisibility(0);
      paramMissionItem.complete_state.setVisibility(8);
    }
    paramMissionItem.mark.setText(localMission.getMark());
  }
  
  private class MissionItem
  {
    @ViewInject(2131165311)
    public TextView can_do;
    @ViewInject(2131165363)
    public TextView complete_state;
    @ViewInject(2131165565)
    public TextView mark;
    @ViewInject(2131165578)
    public TextView mission_name;
    @ViewInject(2131165829)
    public TextView title_tv;
    
    private MissionItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/MissionAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */