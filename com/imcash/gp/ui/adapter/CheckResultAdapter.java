package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.ListDataBean;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class CheckResultAdapter
  extends BaseCommonAdapter<ListDataBean>
{
  public CheckResultAdapter(Context paramContext, ArrayList<ListDataBean> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296322, paramViewGroup, false);
      paramView = new CheckResultItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((CheckResultItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(CheckResultItem paramCheckResultItem, int paramInt)
  {
    ListDataBean localListDataBean = (ListDataBean)getItem(paramInt);
    paramCheckResultItem.name.setText(localListDataBean.getName());
    paramCheckResultItem.num.setText(localListDataBean.getIncome_no());
    TextView localTextView = paramCheckResultItem.info;
    paramCheckResultItem = new StringBuilder();
    paramCheckResultItem.append("持仓期限");
    paramCheckResultItem.append(localListDataBean.getTimelist());
    paramCheckResultItem.append("天,预计收益");
    paramCheckResultItem.append(localListDataBean.getIncome_no());
    localTextView.setText(paramCheckResultItem.toString());
  }
  
  private class CheckResultItem
  {
    @ViewInject(2131165497)
    public TextView info;
    @ViewInject(2131165598)
    public TextView name;
    @ViewInject(2131165615)
    public TextView num;
    
    private CheckResultItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/CheckResultAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */