package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.Policy;
import com.imcash.gp.ui.activity.PolicyAct;
import java.util.ArrayList;

public class PolicyRecyclerAdapter
  extends RecyclerView.Adapter<MyHolder>
{
  protected Context mContext;
  protected ArrayList<Policy> mDatas;
  
  public PolicyRecyclerAdapter(Context paramContext, ArrayList<Policy> paramArrayList)
  {
    this.mDatas = paramArrayList;
    this.mContext = paramContext;
  }
  
  public void buyPolicy(int paramInt)
  {
    ((PolicyAct)this.mContext).buyClicked(paramInt);
  }
  
  public int getItemCount()
  {
    ArrayList localArrayList = this.mDatas;
    if (localArrayList == null) {
      return 0;
    }
    return localArrayList.size();
  }
  
  public void onBindViewHolder(MyHolder paramMyHolder, final int paramInt)
  {
    final Policy localPolicy = (Policy)this.mDatas.get(paramInt);
    Object localObject1 = paramMyHolder.id_tv;
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("ID:");
    ((StringBuilder)localObject2).append(localPolicy.getTrade_no());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramMyHolder.account.setText(localPolicy.getPhone());
    localObject2 = paramMyHolder.money_tv;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(localPolicy.getAmount());
    ((StringBuilder)localObject1).append("USDT");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    paramMyHolder.time_tv.setText(localPolicy.getShowTime());
    paramMyHolder.state_btn.setBackgroundResource(localPolicy.getStateBackColor());
    paramMyHolder.state_btn.setText(localPolicy.getStateStr());
    paramMyHolder.state_btn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (!localPolicy.canBuy()) {
          return;
        }
        PolicyRecyclerAdapter.this.buyPolicy(paramInt);
      }
    });
    paramMyHolder.rule_tv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ((PolicyAct)PolicyRecyclerAdapter.this.mContext).toPdfPage("ImCash保险条款", "IMCASH保险条款.pdf");
      }
    });
  }
  
  public MyHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new MyHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2131296372, paramViewGroup, false));
  }
  
  class MyHolder
    extends RecyclerView.ViewHolder
  {
    public TextView account;
    public TextView id_tv;
    public TextView money_tv;
    public TextView rule_tv;
    public TextView state_btn;
    public TextView time_tv;
    
    public MyHolder(View paramView)
    {
      super();
      this.id_tv = ((TextView)paramView.findViewById(2131165478));
      this.account = ((TextView)paramView.findViewById(2131165199));
      this.money_tv = ((TextView)paramView.findViewById(2131165590));
      this.state_btn = ((TextView)paramView.findViewById(2131165792));
      this.time_tv = ((TextView)paramView.findViewById(2131165820));
      this.rule_tv = ((TextView)paramView.findViewById(2131165729));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/PolicyRecyclerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */