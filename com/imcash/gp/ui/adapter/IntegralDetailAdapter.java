package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.IntegralDetail;
import java.util.ArrayList;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class IntegralDetailAdapter
  extends BaseCommonAdapter<IntegralDetail>
{
  public IntegralDetailAdapter(Context paramContext, ArrayList<IntegralDetail> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public int getCount()
  {
    int i;
    if (this.mDatas == null) {
      i = 0;
    } else {
      i = this.mDatas.size();
    }
    return i;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296341, paramViewGroup, false);
      paramView = new IntegralDetailItem(null);
      x.view().inject(paramView, localView);
      localView.setTag(paramView);
    }
    initData((IntegralDetailItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(IntegralDetailItem paramIntegralDetailItem, int paramInt)
  {
    IntegralDetail localIntegralDetail = (IntegralDetail)this.mDatas.get(paramInt);
    paramIntegralDetailItem.title_msg.setText(localIntegralDetail.getMsg());
    paramIntegralDetailItem.time_tv.setText(localIntegralDetail.getCreate_time());
    StringBuilder localStringBuilder;
    if ("1".equals(localIntegralDetail.getSymbol()))
    {
      paramIntegralDetailItem = paramIntegralDetailItem.count_tv;
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("+");
      localStringBuilder.append(localIntegralDetail.getAmount());
      paramIntegralDetailItem.setText(localStringBuilder.toString());
    }
    else
    {
      paramIntegralDetailItem = paramIntegralDetailItem.count_tv;
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("-");
      localStringBuilder.append(localIntegralDetail.getAmount());
      paramIntegralDetailItem.setText(localStringBuilder.toString());
    }
  }
  
  private class IntegralDetailItem
  {
    @ViewInject(2131165384)
    public TextView count_tv;
    @ViewInject(2131165820)
    public TextView time_tv;
    @ViewInject(2131165826)
    public TextView title_msg;
    
    private IntegralDetailItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/IntegralDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */