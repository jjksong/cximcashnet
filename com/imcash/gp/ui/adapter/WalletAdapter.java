package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.ui.base.ImcashApplication;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class WalletAdapter
  extends BaseCommonAdapter<Wallet>
{
  public WalletAdapter(Context paramContext, ArrayList<Wallet> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296406, paramViewGroup, false);
      paramView = new WalletItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((WalletItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(WalletItem paramWalletItem, int paramInt)
  {
    Wallet localWallet = (Wallet)this.mDatas.get(paramInt);
    paramWalletItem.image.setImageResource(GlobalFunction.getCoinImgByName(localWallet.getName()));
    paramWalletItem.wallet_name.setText(localWallet.getName());
    TextView localTextView = paramWalletItem.select_but;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前可用余额：");
    localStringBuilder.append(localWallet.getNum());
    localTextView.setText(localStringBuilder.toString());
    if (!ImcashApplication.getInstance().getDataCore().getEyesIsOpen()) {
      paramWalletItem.select_but.setText("当前可用余额：****");
    }
    if (paramInt == this.mDatas.size() - 1) {
      paramWalletItem.bottom_view.setVisibility(0);
    } else {
      paramWalletItem.bottom_view.setVisibility(8);
    }
  }
  
  private class WalletItem
  {
    @ViewInject(2131165300)
    public View bottom_view;
    @ViewInject(2131165480)
    public ImageView image;
    @ViewInject(2131165752)
    public TextView select_but;
    @ViewInject(2131165900)
    public TextView wallet_name;
    
    private WalletItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/WalletAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */