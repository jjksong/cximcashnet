package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.RateUp;
import java.util.ArrayList;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class RateUpAdapter
  extends BaseCommonAdapter<RateUp>
{
  public RateUpAdapter(Context paramContext, ArrayList<RateUp> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public int getCount()
  {
    int i;
    if (this.mDatas == null) {
      i = 0;
    } else {
      i = this.mDatas.size();
    }
    return i;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296375, paramViewGroup, false);
      paramView = new RateUpItem(null);
      x.view().inject(paramView, localView);
      localView.setTag(paramView);
    }
    initData((RateUpItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(RateUpItem paramRateUpItem, int paramInt)
  {
    RateUp localRateUp = (RateUp)this.mDatas.get(paramInt);
    paramRateUpItem.title_name.setText(localRateUp.getTitle());
    paramRateUpItem.cont_account.setText(localRateUp.getExchange_num());
    paramRateUpItem.remark.setText(localRateUp.getRemark());
  }
  
  private class RateUpItem
  {
    @ViewInject(2131165368)
    public TextView cont_account;
    @ViewInject(2131165707)
    public TextView remark;
    @ViewInject(2131165827)
    public TextView title_name;
    
    private RateUpItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/RateUpAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */