package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.InviteDetail;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class InviteDetailAdapter
  extends BaseCommonAdapter<InviteDetail>
{
  public InviteDetailAdapter(Context paramContext, ArrayList<InviteDetail> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296347, paramViewGroup, false);
      paramView = new InviteDetailItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((InviteDetailItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(InviteDetailItem paramInviteDetailItem, int paramInt)
  {
    InviteDetail localInviteDetail = (InviteDetail)this.mDatas.get(paramInt);
    Object localObject2 = paramInviteDetailItem.time;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("购入时间：");
    ((StringBuilder)localObject1).append(localInviteDetail.getSendtime());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    paramInviteDetailItem.coin_name.setText(localInviteDetail.getName());
    localObject1 = paramInviteDetailItem.money_tv;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("总收益：");
    ((StringBuilder)localObject2).append(localInviteDetail.getNum());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramInviteDetailItem.coin_img.setImageResource(GlobalFunction.getCoinImgByName(localInviteDetail.getName()));
  }
  
  private class InviteDetailItem
  {
    @ViewInject(2131165352)
    protected ImageView coin_img;
    @ViewInject(2131165356)
    protected TextView coin_name;
    @ViewInject(2131165590)
    protected TextView money_tv;
    @ViewInject(2131165817)
    protected TextView time;
    
    private InviteDetailItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/InviteDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */