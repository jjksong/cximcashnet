package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.BFCReleaseRecord;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class BfcReleaseAdapter
  extends BaseCommonAdapter<BFCReleaseRecord>
{
  public BfcReleaseAdapter(Context paramContext, ArrayList<BFCReleaseRecord> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296315, paramViewGroup, false);
      paramView = new BfcReleaseItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((BfcReleaseItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(BfcReleaseItem paramBfcReleaseItem, int paramInt)
  {
    BFCReleaseRecord localBFCReleaseRecord = (BFCReleaseRecord)getItem(paramInt);
    paramBfcReleaseItem.msg_tv.setText(localBFCReleaseRecord.getMessage());
    paramBfcReleaseItem.time_tv.setText(localBFCReleaseRecord.getCreate_time());
    TextView localTextView = paramBfcReleaseItem.money;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("+");
    localStringBuilder.append(localBFCReleaseRecord.getAmount());
    localStringBuilder.append(localBFCReleaseRecord.getName());
    localTextView.setText(localStringBuilder.toString());
    if (1 == localBFCReleaseRecord.getSymbol()) {
      paramBfcReleaseItem.money.setTextColor(Color.rgb(76, 197, 161));
    } else {
      paramBfcReleaseItem.money.setTextColor(Color.rgb(238, 65, 55));
    }
  }
  
  private class BfcReleaseItem
  {
    @ViewInject(2131165581)
    protected TextView money;
    @ViewInject(2131165594)
    protected TextView msg_tv;
    @ViewInject(2131165820)
    protected TextView time_tv;
    
    private BfcReleaseItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/BfcReleaseAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */