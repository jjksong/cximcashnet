package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.model.TradeInfo;
import java.text.DecimalFormat;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class TradeAllowAdapter
  extends BaseCommonAdapter<TradeInfo>
{
  public TradeAllowAdapter(Context paramContext, ArrayList<TradeInfo> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296392, paramViewGroup, false);
      paramView = new AllowItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((AllowItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(AllowItem paramAllowItem, int paramInt)
  {
    TradeInfo localTradeInfo = (TradeInfo)getItem(paramInt);
    if (localTradeInfo.mIsSelected) {
      paramAllowItem.selected_img.setImageResource(2131099739);
    } else {
      paramAllowItem.selected_img.setImageResource(2131099738);
    }
    paramAllowItem.trade_name.setText(localTradeInfo.getName());
    Object localObject2 = paramAllowItem.coin_type;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("购入币种: ");
    ((StringBuilder)localObject1).append(localTradeInfo.getCurrency_name());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    double d = Double.valueOf(localTradeInfo.getNum()).doubleValue();
    localObject1 = new DecimalFormat("0.0000").format(d);
    TextView localTextView = paramAllowItem.source_money;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append(localTradeInfo.getCurrency_name());
    localTextView.setText(((StringBuilder)localObject2).toString());
    localObject1 = paramAllowItem.endtime;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(localTradeInfo.getEnd_time());
    ((StringBuilder)localObject2).append("天");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    paramAllowItem.rate_tv.setText(localTradeInfo.getRate());
  }
  
  private class AllowItem
  {
    @ViewInject(2131165357)
    public TextView coin_type;
    @ViewInject(2131165423)
    public TextView endtime;
    @ViewInject(2131165694)
    public TextView rate_tv;
    @ViewInject(2131165755)
    public ImageView selected_img;
    @ViewInject(2131165780)
    public TextView source_money;
    @ViewInject(2131165848)
    public TextView trade_name;
    
    private AllowItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/TradeAllowAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */