package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.WalletRecord;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class WalletDetailAdapter
  extends BaseCommonAdapter<WalletRecord>
{
  public WalletDetailAdapter(Context paramContext, ArrayList<WalletRecord> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296327, paramViewGroup, false);
      paramView = new WalletRecordItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((WalletRecordItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(WalletRecordItem paramWalletRecordItem, int paramInt)
  {
    WalletRecord localWalletRecord = (WalletRecord)this.mDatas.get(paramInt);
    paramWalletRecordItem.title.setText(localWalletRecord.stateStr());
    paramWalletRecordItem.time.setText(localWalletRecord.getAdd_time());
    paramWalletRecordItem.num.setText(localWalletRecord.getShowActual());
    paramWalletRecordItem.num.setTextColor(localWalletRecord.getActualColor());
  }
  
  private class WalletRecordItem
  {
    @ViewInject(2131165615)
    public TextView num;
    @ViewInject(2131165817)
    public TextView time;
    @ViewInject(2131165822)
    public TextView title;
    
    private WalletRecordItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/WalletDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */