package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.RebackData;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class RebackMainAdapter
  extends BaseCommonAdapter<RebackData>
{
  public RebackMainAdapter(Context paramContext, ArrayList<RebackData> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296307, paramViewGroup, false);
      paramView = new Item(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((Item)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(Item paramItem, int paramInt)
  {
    RebackData localRebackData = (RebackData)getItem(paramInt);
    paramItem.time_tv.setText(localRebackData.getCreate_time());
    paramItem.quota.setText(localRebackData.getQuota());
    paramItem.number.setText(localRebackData.getFrom_phone());
  }
  
  private class Item
  {
    @ViewInject(2131165616)
    protected TextView number;
    @ViewInject(2131165686)
    protected TextView quota;
    @ViewInject(2131165820)
    protected TextView time_tv;
    
    private Item() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/RebackMainAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */