package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.RedDetail;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class RedRecordDetailAdapter
  extends BaseCommonAdapter<RedDetail>
{
  public RedRecordDetailAdapter(Context paramContext, ArrayList<RedDetail> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296380, paramViewGroup, false);
      paramView = new RedItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((RedItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(RedItem paramRedItem, int paramInt)
  {
    RedDetail localRedDetail = (RedDetail)getItem(paramInt);
    paramRedItem.word_type.setText(localRedDetail.getPhoneHide());
    TextView localTextView = paramRedItem.coin_type;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(localRedDetail.getAmount());
    localStringBuilder.append(localRedDetail.getCoin_name());
    localTextView.setText(localStringBuilder.toString());
    paramRedItem.time_tv.setText(localRedDetail.getCreate_time());
  }
  
  private class RedItem
  {
    @ViewInject(2131165357)
    protected TextView coin_type;
    @ViewInject(2131165662)
    protected TextView procress_tv;
    @ViewInject(2131165820)
    protected TextView time_tv;
    @ViewInject(2131165911)
    protected TextView word_type;
    
    private RedItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/RedRecordDetailAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */