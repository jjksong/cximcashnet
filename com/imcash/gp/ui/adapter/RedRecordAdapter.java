package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.model.RedRecord;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class RedRecordAdapter
  extends BaseCommonAdapter<RedRecord>
{
  public RedRecordAdapter(Context paramContext, ArrayList<RedRecord> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296381, paramViewGroup, false);
      paramView = new RedItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((RedItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(RedItem paramRedItem, int paramInt)
  {
    RedRecord localRedRecord = (RedRecord)getItem(paramInt);
    TextView localTextView = paramRedItem.word_type;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("口令：");
    localStringBuilder.append(localRedRecord.getKeyword());
    localTextView.setText(localStringBuilder.toString());
    paramRedItem.coin_type.setText(localRedRecord.getCoinStr());
    paramRedItem.time_tv.setText(localRedRecord.getCreate_time());
    paramRedItem.procress_tv.setText(localRedRecord.getPress());
    if (localRedRecord.isRedSharePacket()) {
      paramRedItem.share_bag_img.setVisibility(0);
    } else {
      paramRedItem.share_bag_img.setVisibility(8);
    }
  }
  
  private class RedItem
  {
    @ViewInject(2131165357)
    protected TextView coin_type;
    @ViewInject(2131165662)
    protected TextView procress_tv;
    @ViewInject(2131165767)
    protected ImageView share_bag_img;
    @ViewInject(2131165820)
    protected TextView time_tv;
    @ViewInject(2131165911)
    protected TextView word_type;
    
    private RedItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/RedRecordAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */