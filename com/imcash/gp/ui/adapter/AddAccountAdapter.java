package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.model.Coin;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class AddAccountAdapter
  extends BaseCommonAdapter<Coin>
{
  public AddAccountAdapter(Context paramContext, ArrayList<Coin> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296292, paramViewGroup, false);
      paramView = new CoinItem(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((CoinItem)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(CoinItem paramCoinItem, int paramInt)
  {
    Coin localCoin = (Coin)getItem(paramInt);
    paramCoinItem.coin_img.setImageResource(GlobalFunction.getCoinImgByName(localCoin.getName()));
    paramCoinItem.coin_name.setText(localCoin.getName());
    if (localCoin.mIsSelected) {
      paramCoinItem.state_img.setImageResource(2131099739);
    } else {
      paramCoinItem.state_img.setImageResource(2131099738);
    }
  }
  
  private class CoinItem
  {
    @ViewInject(2131165352)
    public ImageView coin_img;
    @ViewInject(2131165356)
    public TextView coin_name;
    @ViewInject(2131165793)
    public ImageView state_img;
    
    private CoinItem() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/AddAccountAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */