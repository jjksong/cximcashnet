package com.imcash.gp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.model.BfcLock;
import java.util.ArrayList;
import org.xutils.view.annotation.ViewInject;

public class BfcLockAdapter
  extends BaseCommonAdapter<BfcLock>
{
  public BfcLockAdapter(Context paramContext, ArrayList<BfcLock> paramArrayList)
  {
    super(paramContext, paramArrayList);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (paramView == null)
    {
      this.mInflater = LayoutInflater.from(this.mContext);
      localView = this.mInflater.inflate(2131296298, paramViewGroup, false);
      paramView = new Item(null);
      initView(localView, paramView);
      localView.setTag(paramView);
    }
    initData((Item)localView.getTag(), paramInt);
    return localView;
  }
  
  protected void initData(Item paramItem, int paramInt)
  {
    BfcLock localBfcLock = (BfcLock)getItem(paramInt);
    paramItem.time_tv.setText(localBfcLock.getMessage());
    paramItem.source_tv.setText(localBfcLock.getCreate_time());
    paramItem = paramItem.count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("+");
    localStringBuilder.append(localBfcLock.getAmount());
    localStringBuilder.append("BFC");
    paramItem.setText(localStringBuilder.toString());
  }
  
  private class Item
  {
    @ViewInject(2131165379)
    protected TextView count;
    @ViewInject(2131165782)
    protected TextView source_tv;
    @ViewInject(2131165820)
    protected TextView time_tv;
    
    private Item() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/adapter/BfcLockAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */