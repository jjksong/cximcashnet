package com.imcash.gp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.PersonalInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.activity.BBExchangeAct;
import com.imcash.gp.ui.activity.BankCardAct;
import com.imcash.gp.ui.activity.CheckAct;
import com.imcash.gp.ui.activity.ConnectAct;
import com.imcash.gp.ui.activity.FinancingOrderAct;
import com.imcash.gp.ui.activity.InviteAct;
import com.imcash.gp.ui.activity.MyIntegralAct;
import com.imcash.gp.ui.activity.NewOtcAct;
import com.imcash.gp.ui.activity.OrderDetailAct;
import com.imcash.gp.ui.activity.PersonalAct;
import com.imcash.gp.ui.activity.PolicyAct;
import com.imcash.gp.ui.activity.TradeMineAct;
import com.imcash.gp.ui.activity.UserQrAct;
import com.imcash.gp.ui.activity.WalletAct;
import com.imcash.gp.ui.base.BaseFragment;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;

public class CenterFragment
  extends BaseFragment
{
  public static final String TAG = "center_fragment";
  protected TextView help_service;
  protected PersonalInfo mPersonalInfo;
  protected TextView user_id;
  protected TextView user_phone;
  
  @SuppressLint({"ValidFragment"})
  public CenterFragment() {}
  
  @SuppressLint({"ValidFragment"})
  public CenterFragment(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  protected void checkLogic()
  {
    PersonalInfo localPersonalInfo = this.mPersonalInfo;
    if (localPersonalInfo == null) {
      return;
    }
    if (1 == localPersonalInfo.is_realname) {
      toPage(CheckAct.class);
    } else {
      showMessageBox(this.mActivity, "请先完成实名认证", "");
    }
  }
  
  public void findViewById()
  {
    this.help_service = ((TextView)this.mView.findViewById(2131165465));
    this.help_service.setText("关于我们&在线客服");
    this.user_phone = ((TextView)this.mView.findViewById(2131165887));
    this.user_id = ((TextView)this.mView.findViewById(2131165885));
    this.mView.findViewById(2131165653).setOnClickListener(this);
    this.mView.findViewById(2131165899).setOnClickListener(this);
    this.mView.findViewById(2131165501).setOnClickListener(this);
    this.mView.findViewById(2131165508).setOnClickListener(this);
    this.mView.findViewById(2131165331).setOnClickListener(this);
    this.mView.findViewById(2131165658).setOnClickListener(this);
    this.mView.findViewById(2131165625).setOnClickListener(this);
    this.mView.findViewById(2131165623).setOnClickListener(this);
    this.mView.findViewById(2131165636).setOnClickListener(this);
    this.mView.findViewById(2131165366).setOnClickListener(this);
    this.mView.findViewById(2131165631).setOnClickListener(this);
    this.mView.findViewById(2131165441).setOnClickListener(this);
    this.mView.findViewById(2131165888).setOnClickListener(this);
    this.mView.findViewById(2131165256).setOnClickListener(this);
  }
  
  protected void getPsersonalInfo(JSONObject paramJSONObject)
  {
    this.mPersonalInfo = new PersonalInfo(paramJSONObject);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getPsersonalInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  public void initData()
  {
    this.user_phone.setText(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    TextView localTextView = this.user_id;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ID:");
    localStringBuilder.append(this.mApplication.getDataCore().getUserInfo().angel_num);
    localTextView.setText(localStringBuilder.toString());
    postPersonalInfo();
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default: 
      break;
    case 2131165899: 
      toPage(WalletAct.class);
      break;
    case 2131165888: 
      toPage(UserQrAct.class);
      break;
    case 2131165658: 
      toPage(PolicyAct.class);
      break;
    case 2131165653: 
      toPage(PersonalAct.class);
      break;
    case 2131165636: 
      toPage(BBExchangeAct.class);
      break;
    case 2131165631: 
      toPage(NewOtcAct.class);
      break;
    case 2131165625: 
      toPage(FinancingOrderAct.class);
      break;
    case 2131165623: 
      toPage(OrderDetailAct.class);
      break;
    case 2131165508: 
      toPage(InviteAct.class);
      break;
    case 2131165501: 
      toPage(MyIntegralAct.class);
      break;
    case 2131165441: 
      toPage(TradeMineAct.class);
      break;
    case 2131165366: 
      toPage(ConnectAct.class);
      break;
    case 2131165331: 
      checkLogic();
      break;
    case 2131165256: 
      toPage(BankCardAct.class);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mView = paramLayoutInflater.inflate(2131296422, null);
    findViewById();
    initData();
    return this.mView;
  }
  
  public void onResume()
  {
    super.onResume();
    if ("center_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
      postPersonalInfo();
    }
  }
  
  protected void postPersonalInfo()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    postNet(HttpTypeEnum.Personal_Type, localRequestParams);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/fragment/CenterFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */