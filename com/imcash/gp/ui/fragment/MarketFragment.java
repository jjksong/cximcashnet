package com.imcash.gp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.CoinInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.activity.AddAccountAct;
import com.imcash.gp.ui.activity.CoinDetailAct;
import com.imcash.gp.ui.activity.RedMainAct;
import com.imcash.gp.ui.activity.WalletAct;
import com.imcash.gp.ui.adapter.MarketAdapter;
import com.imcash.gp.ui.base.BaseFragment;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout.onUpwardListener;
import com.imcash.gp.ui.view.swiperefresh.SwipeRefreshLayout.OnRefreshListener;
import com.loopj.android.http.RequestParams;
import com.sunfusheng.marqueeview.MarqueeView;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class MarketFragment
  extends BaseFragment
{
  public static final String TAG = "market_fragment";
  protected ImageView eye_iv;
  protected MarketAdapter mAdapter;
  protected View mBottomView;
  protected ArrayList<CoinInfo> mDatas;
  protected int mEyeState = 0;
  protected View mHeadView;
  protected ListView mListView;
  protected ArrayList<String> mMarqueeDatas;
  protected SwipeLayout mSwipeLayout;
  protected MarqueeView marqueeView;
  protected String topMoneyStr = "0.0000";
  protected TextView top_money;
  
  @SuppressLint({"ValidFragment"})
  public MarketFragment() {}
  
  @SuppressLint({"ValidFragment"})
  public MarketFragment(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  protected void changeEyeState()
  {
    int i = this.mEyeState;
    if (i == 0) {
      this.mEyeState = 1;
    } else if (i == 1) {
      this.mEyeState = 0;
    }
    updataEyeState();
  }
  
  public void findViewById()
  {
    this.mListView = ((ListView)this.mView.findViewById(2131165540));
    this.mHeadView = LayoutInflater.from(this.mActivity).inflate(2131296451, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.mHeadView.findViewById(2131165701).setOnClickListener(this);
    this.eye_iv = ((ImageView)this.mHeadView.findViewById(2131165429));
    this.mHeadView.findViewById(2131165429).setOnClickListener(this);
    this.mDatas = new ArrayList();
    this.mAdapter = new MarketAdapter(this.mActivity, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.top_money = ((TextView)this.mHeadView.findViewById(2131165841));
    this.marqueeView = ((MarqueeView)this.mHeadView.findViewById(2131165566));
    this.mHeadView.findViewById(2131165898).setOnClickListener(this);
    this.mHeadView.findViewById(2131165198).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MarketFragment.this.showToastInCenter("累计收益：基金收益+平台奖励金+邀请奖励");
      }
    });
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        if (paramAnonymousInt == 0) {
          return;
        }
        paramAnonymousAdapterView = new Intent();
        paramAnonymousAdapterView.putExtra("CoinDetailKey", new Gson().toJson(MarketFragment.this.mDatas.get(paramAnonymousInt - 1)));
        MarketFragment.this.toPage(paramAnonymousAdapterView, CoinDetailAct.class);
      }
    });
    initListView();
  }
  
  protected void getCoinInfo(JSONObject paramJSONObject)
  {
    Object localObject = this.mSwipeLayout;
    if (localObject != null) {
      ((SwipeLayout)localObject).setRefreshing(false);
    }
    localObject = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)((Gson)localObject).fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    if (5 != this.mDatas.size())
    {
      if (this.mBottomView == null)
      {
        this.mBottomView = LayoutInflater.from(this.mActivity).inflate(2131296449, null);
        this.mListView.addFooterView(this.mBottomView);
      }
      this.mBottomView.findViewById(2131165222).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          MarketFragment.this.toPage(AddAccountAct.class);
        }
      });
      this.mBottomView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          Log.v("asd", "321");
        }
      });
    }
    else
    {
      paramJSONObject = this.mBottomView;
      if (paramJSONObject != null) {
        this.mListView.removeFooterView(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected void getProfitColumn(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONArray("data");
    this.mMarqueeDatas.clear();
    for (int i = 0; i < paramJSONObject.length(); i++) {
      this.mMarqueeDatas.add(paramJSONObject.optString(i, ""));
    }
    this.marqueeView.startWithList(this.mMarqueeDatas, 2130771980, 2130771988);
  }
  
  protected void getWealth(JSONObject paramJSONObject)
  {
    this.topMoneyStr = paramJSONObject.optJSONObject("data").optString("Assets", "0.00");
    updataEyeState();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getProfitColumn(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getCoinInfo(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getWealth(paramHttpInfo.mJsonObj);
    }
  }
  
  public void initData()
  {
    this.mMarqueeDatas = new ArrayList();
  }
  
  protected void initListView()
  {
    this.mListView.setVerticalScrollBarEnabled(false);
    this.mSwipeLayout = ((SwipeLayout)this.mView.findViewById(2131165802));
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(238);
    this.mSwipeLayout.setColorAllResources(2130968751, 2130968752, 2130968753, 2130968754);
    this.mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
    {
      public void onRefresh()
      {
        MarketFragment.this.postWealth();
        MarketFragment.this.postCoinInfo();
        MarketFragment.this.postProfitColumn();
      }
    });
    this.mSwipeLayout.setOnUpwardListener(new SwipeLayout.onUpwardListener()
    {
      public void onUpward() {}
    });
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (i != 2131165429)
    {
      if (i != 2131165701)
      {
        if (i == 2131165898) {
          toPage(WalletAct.class);
        }
      }
      else {
        toPage(RedMainAct.class);
      }
    }
    else {
      changeEyeState();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mView = paramLayoutInflater.inflate(2131296448, null);
    findViewById();
    initData();
    return this.mView;
  }
  
  public void onResume()
  {
    super.onResume();
    if ("market_fragment".equals(this.mApplication.getDataCore().mCurFragTag))
    {
      postWealth();
      postCoinInfo();
      postProfitColumn();
    }
  }
  
  public void onStart()
  {
    super.onStart();
    this.marqueeView.startFlipping();
  }
  
  public void onStop()
  {
    super.onStop();
    this.marqueeView.stopFlipping();
  }
  
  protected void postCoinInfo()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.Wallet_CurrentShow, localRequestParams);
  }
  
  protected void postProfitColumn()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    postNet(HttpTypeEnum.ProfitColumn, localRequestParams);
  }
  
  protected void postWealth()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.UserWealth, localRequestParams);
  }
  
  protected void updataEyeState()
  {
    switch (this.mEyeState)
    {
    default: 
      break;
    case 1: 
      this.eye_iv.setImageResource(2131099806);
      this.top_money.setText("****");
      break;
    case 0: 
      this.eye_iv.setImageResource(2131099807);
      this.top_money.setText(this.topMoneyStr);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/fragment/MarketFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */