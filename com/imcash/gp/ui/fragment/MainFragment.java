package com.imcash.gp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cn.jpush.android.api.JPushInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.ListDataBean;
import com.imcash.gp.model.UpdataInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.activity.TradeAct;
import com.imcash.gp.ui.adapter.MainAdapter;
import com.imcash.gp.ui.base.BaseFragment;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.MessageDialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONObject;

public class MainFragment
  extends BaseFragment
{
  public static final String TAG = "main_fragment";
  protected TextView current_release;
  protected ImageView eye_iv;
  protected ListView listView1;
  protected MainAdapter mAdapter;
  private ArrayList<ListDataBean> mData;
  protected int mEyeState = 0;
  protected View mHeadView;
  protected String topMoneyStr = "0.0000";
  protected TextView top_money;
  
  @SuppressLint({"ValidFragment"})
  public MainFragment() {}
  
  @SuppressLint({"ValidFragment"})
  public MainFragment(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  protected void changeEyeState()
  {
    int i = this.mEyeState;
    if (i == 0) {
      this.mEyeState = 1;
    } else if (i == 1) {
      this.mEyeState = 0;
    }
    updataEyeState();
  }
  
  public void findViewById()
  {
    this.listView1 = ((ListView)this.mView.findViewById(2131165540));
    this.mHeadView = LayoutInflater.from(this.mActivity).inflate(2131296437, null);
    this.listView1.addHeaderView(this.mHeadView);
    this.eye_iv = ((ImageView)this.mHeadView.findViewById(2131165429));
    this.mHeadView.findViewById(2131165429).setOnClickListener(this);
    this.mHeadView.findViewById(2131165847).setOnClickListener(this);
    this.top_money = ((TextView)this.mHeadView.findViewById(2131165841));
    this.current_release = ((TextView)this.mHeadView.findViewById(2131165394));
  }
  
  protected void getCurrentRelease(JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = paramJSONObject.optJSONObject("data");
    TextView localTextView = this.current_release;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("UNCG ");
    paramJSONObject.append(localJSONObject.optString("rate"));
    localTextView.setText(paramJSONObject.toString());
  }
  
  protected void getMainProject(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    try
    {
      Object localObject = paramJSONObject.optJSONObject("data");
      paramJSONObject = ((JSONObject)localObject).getString("tui_data");
      String str = ((JSONObject)localObject).getString("list_data");
      this.mData.clear();
      if ((str != null) && (str.length() != 0))
      {
        localObject = new com/imcash/gp/ui/fragment/MainFragment$1;
        ((1)localObject).<init>(this);
        localObject = (ArrayList)localGson.fromJson(str, ((1)localObject).getType());
        if ((localObject != null) && (((ArrayList)localObject).size() > 0))
        {
          this.mData.addAll((Collection)localObject);
          this.mAdapter.setCutIndex(this.mData.size());
        }
      }
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
      {
        localObject = new com/imcash/gp/ui/fragment/MainFragment$2;
        ((2)localObject).<init>(this);
        paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, ((2)localObject).getType());
        if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
          this.mData.addAll(paramJSONObject);
        }
      }
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
    this.mAdapter.notifyDataSetChanged();
  }
  
  protected void getWealth(JSONObject paramJSONObject)
  {
    this.topMoneyStr = paramJSONObject.optJSONObject("data").optString("balance", "0.0000");
    updataEyeState();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      versionUpdata(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      if (1 == paramHttpInfo.mJsonObj.optJSONObject("data").optInt("state", 0)) {
        showMessageBox(this.mActivity, paramHttpInfo.mJsonObj.optJSONObject("data").optString("content", ""), "");
      }
      break;
    case ???: 
      getCurrentRelease(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getWealth(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getMainProject(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      showSign(paramHttpInfo.mJsonObj);
    }
  }
  
  public void initData()
  {
    this.mData = new ArrayList();
    this.mAdapter = new MainAdapter(this.mActivity, this.mData);
    this.listView1.setAdapter(this.mAdapter);
    postSign();
    postMainData();
    postShowMsg();
    updataStatus();
    postRegistPush();
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (i != 2131165429)
    {
      if (i == 2131165847) {
        toPage(TradeAct.class);
      }
    }
    else {
      changeEyeState();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mView = paramLayoutInflater.inflate(2131296435, null);
    findViewById();
    initData();
    return this.mView;
  }
  
  public void onResume()
  {
    super.onResume();
    if ("main_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
      postWealth();
    }
  }
  
  protected void postCurrentRelease()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.CurrentRelease, localRequestParams);
  }
  
  protected void postMainData()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.MainProject_Type, localRequestParams);
  }
  
  protected void postRegistPush()
  {
    String str = JPushInterface.getRegistrationID(this.mActivity);
    if (str.isEmpty()) {
      return;
    }
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    localRequestParams.put("device", "android");
    localRequestParams.put("device_id", str);
    postNet(HttpTypeEnum.PostRegistPush, localRequestParams);
  }
  
  protected void postShowMsg()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.Main_PopWindows, localRequestParams);
  }
  
  protected void postSign()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    postNet(HttpTypeEnum.Sign_Type, localRequestParams);
  }
  
  protected void postWealth()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.UserWealth, localRequestParams);
  }
  
  protected void showSign(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONObject("data").optString("content", "");
    if (paramJSONObject.length() != 0) {
      showToastInCenter(paramJSONObject);
    }
  }
  
  protected void updataEyeState()
  {
    switch (this.mEyeState)
    {
    default: 
      break;
    case 1: 
      this.eye_iv.setImageResource(2131099806);
      this.top_money.setText("****");
      break;
    case 0: 
      this.eye_iv.setImageResource(2131099807);
      this.top_money.setText(this.topMoneyStr);
    }
  }
  
  protected void updataStatus()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    String str = getResources().getString(2131558559);
    localRequestParams.put("system", "android");
    localRequestParams.put("version_code", str);
    postNet(HttpTypeEnum.Version_Updata, localRequestParams);
  }
  
  protected void versionUpdata(final JSONObject paramJSONObject)
  {
    if (!((UpdataInfo)new Gson().fromJson(paramJSONObject.optString("data"), UpdataInfo.class)).getForce().equals("0")) {
      showMessageBox(this.mActivity, "版本更新", paramJSONObject.optJSONObject("data").optString("version_content", ""), new MessageDialog.OnBtnListener()
      {
        public void onClick()
        {
          Intent localIntent = new Intent();
          localIntent.setAction("android.intent.action.VIEW");
          localIntent.setData(Uri.parse(paramJSONObject.optJSONObject("data").optString("url", "https://wkzx.store/HplA")));
          localIntent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
          MainFragment.this.startActivity(localIntent);
        }
      });
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/fragment/MainFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */