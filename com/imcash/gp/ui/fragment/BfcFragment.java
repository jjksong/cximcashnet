package com.imcash.gp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BFCReleaseRecord;
import com.imcash.gp.model.BfcTokenRecord;
import com.imcash.gp.ui.activity.BfcLockAct;
import com.imcash.gp.ui.activity.BfcOutAct;
import com.imcash.gp.ui.activity.BfcQrImg;
import com.imcash.gp.ui.activity.BfcRecordAct;
import com.imcash.gp.ui.activity.BfcReleaseAct;
import com.imcash.gp.ui.activity.BfcTransferAct;
import com.imcash.gp.ui.activity.BfcUsdtAct;
import com.imcash.gp.ui.activity.ReBackFirstAct;
import com.imcash.gp.ui.activity.RebackMainAct;
import com.imcash.gp.ui.adapter.BfcReleaseAdapter;
import com.imcash.gp.ui.base.BaseFragment;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout.onUpwardListener;
import com.imcash.gp.ui.view.swiperefresh.SwipeRefreshLayout.OnRefreshListener;
import com.loopj.android.http.RequestParams;
import com.sunfusheng.marqueeview.MarqueeView;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class BfcFragment
  extends BaseFragment
{
  public static final int Pai_Type = 1;
  public static final int Record_Type = 0;
  public static final String TAG = "bfc_fragment";
  protected LinearLayout bfc_record_layout;
  protected BfcReleaseAdapter mAdapter;
  protected String mBfcCount;
  protected ArrayList<BfcTokenRecord> mBfcRecordDatas;
  protected String mChangeInfo;
  protected ArrayList<BFCReleaseRecord> mDatas;
  protected View mHeadView;
  protected int mIntegral = 0;
  protected int mJoinStatus;
  protected ListView mListView;
  protected ArrayList<String> mMarqueeDatas;
  protected ArrayList<BFCReleaseRecord> mReleaseRecords;
  protected int mRule = 0;
  protected int mShowState = 0;
  protected SwipeLayout mSwipeLayout;
  protected MarqueeView marqueeView;
  protected LinearLayout money_release_layout;
  
  @SuppressLint({"ValidFragment"})
  public BfcFragment() {}
  
  @SuppressLint({"ValidFragment"})
  public BfcFragment(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  protected void bfcReBackBtnLogic()
  {
    if (this.mBfcCount == null) {
      return;
    }
    int i = this.mJoinStatus;
    if (i == 0)
    {
      Intent localIntent = new Intent();
      localIntent.putExtra("BFC_Balance_Key", this.mBfcCount);
      toPage(localIntent, ReBackFirstAct.class);
    }
    else if (1 == i)
    {
      toPage(RebackMainAct.class);
    }
  }
  
  public void findViewById()
  {
    this.mListView = ((ListView)this.mView.findViewById(2131165540));
    this.mHeadView = LayoutInflater.from(this.mActivity).inflate(2131296433, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.bfc_record_layout = ((LinearLayout)this.mHeadView.findViewById(2131165274));
    this.money_release_layout = ((LinearLayout)this.mHeadView.findViewById(2131165588));
    this.marqueeView = ((MarqueeView)this.mHeadView.findViewById(2131165566));
    this.mHeadView.findViewById(2131165849).setOnClickListener(this);
    this.mSwipeLayout = ((SwipeLayout)this.mView.findViewById(2131165802));
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(238);
    this.mSwipeLayout.setColorAllResources(2130968751, 2130968752, 2130968753, 2130968754);
    this.mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
    {
      public void onRefresh()
      {
        BfcFragment.this.postIndex();
      }
    });
    this.mSwipeLayout.setOnUpwardListener(new SwipeLayout.onUpwardListener()
    {
      public void onUpward() {}
    });
  }
  
  protected void getIndex(JSONObject paramJSONObject)
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    if (localSwipeLayout != null) {
      localSwipeLayout.setRefreshing(false);
    }
    paramJSONObject = paramJSONObject.optJSONObject("data");
    updataTopUi(paramJSONObject);
    getProfitColumn(paramJSONObject);
    getTokenRecord(paramJSONObject);
    getReleaseRecord(paramJSONObject);
    getJoinStatus(paramJSONObject);
    updataState();
  }
  
  protected void getJoinStatus(JSONObject paramJSONObject)
  {
    this.mJoinStatus = paramJSONObject.optInt("is_join", 0);
  }
  
  protected void getProfitColumn(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONArray("banner");
    this.mMarqueeDatas.clear();
    for (int i = 0; i < paramJSONObject.length(); i++)
    {
      if (i == 0) {
        this.mChangeInfo = paramJSONObject.optString(i, "");
      }
      this.mMarqueeDatas.add(paramJSONObject.optString(i, ""));
    }
    this.marqueeView.startWithList(this.mMarqueeDatas, 2130771980, 2130771988);
  }
  
  protected void getReleaseRecord(JSONObject paramJSONObject)
  {
    this.mReleaseRecords.clear();
    Object localObject = new Gson();
    paramJSONObject = paramJSONObject.optString("release_list", "");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)((Gson)localObject).fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mReleaseRecords.addAll(paramJSONObject);
      }
    }
    this.money_release_layout.removeAllViews();
    if (this.mReleaseRecords.size() == 0)
    {
      this.money_release_layout.setVisibility(8);
    }
    else
    {
      this.money_release_layout.setVisibility(0);
      Iterator localIterator = this.mReleaseRecords.iterator();
      while (localIterator.hasNext())
      {
        paramJSONObject = (BFCReleaseRecord)localIterator.next();
        localObject = LayoutInflater.from(this.mActivity).inflate(2131296432, null);
        ((TextView)((View)localObject).findViewById(2131165594)).setText(paramJSONObject.getMessage());
        TextView localTextView = (TextView)((View)localObject).findViewById(2131165581);
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("+");
        localStringBuilder.append(paramJSONObject.getAmount());
        localStringBuilder.append("USDT");
        localTextView.setText(localStringBuilder.toString());
        ((TextView)((View)localObject).findViewById(2131165820)).setText(paramJSONObject.getCreate_time());
        if (1 == paramJSONObject.getSymbol()) {
          ((TextView)((View)localObject).findViewById(2131165581)).setTextColor(Color.rgb(76, 197, 161));
        } else {
          ((TextView)((View)localObject).findViewById(2131165581)).setTextColor(Color.rgb(238, 65, 55));
        }
        this.money_release_layout.addView((View)localObject);
        this.money_release_layout.invalidate();
      }
    }
  }
  
  protected void getTokenRecord(JSONObject paramJSONObject)
  {
    this.mBfcRecordDatas.clear();
    Object localObject = new Gson();
    paramJSONObject = paramJSONObject.optString("token_list", "");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)((Gson)localObject).fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mBfcRecordDatas.addAll(paramJSONObject);
      }
    }
    this.bfc_record_layout.removeAllViews();
    if (this.mBfcRecordDatas.size() == 0)
    {
      this.bfc_record_layout.setVisibility(8);
    }
    else
    {
      this.bfc_record_layout.setVisibility(0);
      Iterator localIterator = this.mBfcRecordDatas.iterator();
      while (localIterator.hasNext())
      {
        paramJSONObject = (BfcTokenRecord)localIterator.next();
        localObject = LayoutInflater.from(this.mActivity).inflate(2131296434, null);
        ((TextView)((View)localObject).findViewById(2131165820)).setText(paramJSONObject.getMessage());
        ((TextView)((View)localObject).findViewById(2131165782)).setText(paramJSONObject.getCreate_time());
        TextView localTextView = (TextView)((View)localObject).findViewById(2131165379);
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramJSONObject.getSymbolSign());
        localStringBuilder.append(paramJSONObject.getAmount());
        localStringBuilder.append("BFC");
        localTextView.setText(localStringBuilder.toString());
        if (1 == paramJSONObject.getSymbol()) {
          ((TextView)((View)localObject).findViewById(2131165379)).setTextColor(Color.rgb(76, 197, 161));
        } else {
          ((TextView)((View)localObject).findViewById(2131165379)).setTextColor(Color.rgb(238, 65, 55));
        }
        this.bfc_record_layout.addView((View)localObject);
        this.bfc_record_layout.invalidate();
      }
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showMessageBox(this.mActivity, "提示", paramHttpInfo.mJsonObj.optString("msg", ""));
      break;
    case ???: 
      getIndex(paramHttpInfo.mJsonObj);
    }
  }
  
  public void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new BfcReleaseAdapter(this.mActivity, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.mMarqueeDatas = new ArrayList();
    this.mBfcRecordDatas = new ArrayList();
    this.mReleaseRecords = new ArrayList();
    this.mView.findViewById(2131165718).setOnClickListener(this);
    this.mHeadView.findViewById(2131165834).setOnClickListener(this);
    this.mHeadView.findViewById(2131165634).setOnClickListener(this);
    this.mHeadView.findViewById(2131165698).setOnClickListener(this);
    this.mHeadView.findViewById(2131165680).setOnClickListener(this);
    this.mHeadView.findViewById(2131165657).setOnClickListener(this);
    this.mHeadView.findViewById(2131165657).setVisibility(8);
    this.mHeadView.findViewById(2131165706).setOnClickListener(this);
    this.mHeadView.findViewById(2131165237).setOnClickListener(this);
    this.mHeadView.findViewById(2131165699).setOnClickListener(this);
    this.mHeadView.findViewById(2131165557).setVisibility(8);
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default: 
      break;
    case 2131165849: 
      toPage(BfcTransferAct.class);
      break;
    case 2131165834: 
      if (this.mShowState != 0)
      {
        this.mShowState = 0;
        updataState();
      }
      break;
    case 2131165718: 
      toWebPage("BFC通证介绍", "http://imcash.art/bfc.html");
      break;
    case 2131165706: 
      if (1 != this.mShowState)
      {
        this.mShowState = 1;
        updataState();
      }
      break;
    case 2131165699: 
      toPage(BfcUsdtAct.class);
      break;
    case 2131165698: 
      toPage(BfcQrImg.class);
      break;
    case 2131165680: 
      toWebPage("BFC通证介绍", "http://imcash.art/bfc.html");
      break;
    case 2131165657: 
      toPage(BfcLockAct.class);
      break;
    case 2131165634: 
      toPage(BfcOutAct.class);
      break;
    case 2131165237: 
      int i = this.mShowState;
      if (i == 0)
      {
        toPage(BfcRecordAct.class);
      }
      else if ((1 == i) && (this.mBfcCount != null))
      {
        paramView = new Intent();
        paramView.putExtra("Bfc_Count_Key", this.mBfcCount);
        toPage(paramView, BfcReleaseAct.class);
      }
      break;
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mView = paramLayoutInflater.inflate(2131296431, null);
    findViewById();
    initData();
    return this.mView;
  }
  
  public void onResume()
  {
    super.onResume();
    if ("bfc_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
      postIndex();
    }
  }
  
  public void onStart()
  {
    super.onStart();
    this.marqueeView.startFlipping();
  }
  
  public void onStop()
  {
    super.onStop();
    this.marqueeView.stopFlipping();
  }
  
  protected void postIndex()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getToken());
    postNet(HttpTypeEnum.BFC_Index, localRequestParams);
  }
  
  protected void updataState()
  {
    int i = this.mShowState;
    if (i == 0)
    {
      this.mHeadView.findViewById(2131165835).setVisibility(0);
      this.mHeadView.findViewById(2131165642).setVisibility(4);
      this.bfc_record_layout.setVisibility(0);
      this.money_release_layout.setVisibility(8);
    }
    else if (1 == i)
    {
      this.mHeadView.findViewById(2131165835).setVisibility(4);
      this.mHeadView.findViewById(2131165642).setVisibility(0);
      this.bfc_record_layout.setVisibility(8);
      this.money_release_layout.setVisibility(0);
    }
  }
  
  protected void updataTopUi(JSONObject paramJSONObject)
  {
    if (paramJSONObject.optInt("lockBfc", 0) == 0)
    {
      this.mHeadView.findViewById(2131165657).setVisibility(8);
      this.mHeadView.findViewById(2131165557).setVisibility(8);
    }
    else
    {
      this.mHeadView.findViewById(2131165657).setVisibility(0);
      this.mHeadView.findViewById(2131165557).setVisibility(0);
    }
    String str = paramJSONObject.optString("locking", "0.00");
    ((TextView)this.mHeadView.findViewById(2131165556)).setText(str);
    this.mBfcCount = paramJSONObject.optString("balance", "0.00");
    this.mRule = paramJSONObject.optInt("rule", 0);
    this.mIntegral = paramJSONObject.optInt("integral", 0);
    str = paramJSONObject.optString("balance", "0.00");
    ((TextView)this.mHeadView.findViewById(2131165250)).setText(str);
    ((TextView)this.mHeadView.findViewById(2131165707)).setText(paramJSONObject.optString("remark", ""));
    ((TextView)this.mHeadView.findViewById(2131165650)).setText(paramJSONObject.optString("percentage", "0.00%"));
    if (1 == paramJSONObject.optInt("grow", 1)) {
      ((ImageView)this.mHeadView.findViewById(2131165879)).setImageResource(2131099755);
    } else {
      ((ImageView)this.mHeadView.findViewById(2131165879)).setImageResource(2131099748);
    }
    updataState();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/fragment/BfcFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */