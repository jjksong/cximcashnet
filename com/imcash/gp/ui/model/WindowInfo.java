package com.imcash.gp.ui.model;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class WindowInfo
{
  public float aspectRatio = 0.0F;
  public float density = 0.0F;
  public float densityDpi = 0.0F;
  public int height = 0;
  public int width = 0;
  
  public static int getBannerHeight(Activity paramActivity)
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    double d = localDisplayMetrics.widthPixels;
    Double.isNaN(d);
    return (int)(d * 2.7D / 6.7D);
  }
  
  public String getPixelInfo()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.width);
    localStringBuilder.append("x");
    localStringBuilder.append(this.height);
    localStringBuilder.append("/");
    localStringBuilder.append(this.densityDpi);
    return localStringBuilder.toString();
  }
  
  public void refresh()
  {
    this.aspectRatio = (this.width / this.height);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/model/WindowInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */