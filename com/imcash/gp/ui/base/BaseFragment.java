package com.imcash.gp.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpData;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpOnReceive;
import com.imcash.gp.http.HttpRequest;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.GetNetworkInfo;
import com.imcash.gp.ui.activity.LoginAct;
import com.imcash.gp.ui.activity.WebAct;
import com.imcash.gp.ui.view.Btn2Dialog;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.imcash.gp.ui.view.LoadingDialog;
import com.imcash.gp.ui.view.MessageDialog;
import com.imcash.gp.ui.view.MessageDialog.OnBtnListener;
import com.loopj.android.http.RequestParams;

public abstract class BaseFragment
  extends Fragment
  implements View.OnClickListener, HttpOnReceive
{
  protected Activity mActivity;
  protected ImcashApplication mApplication;
  protected BaseHttp mBaseHttp;
  private Toast mCenterToast;
  protected LoadingDialog mSelfProgressDialog;
  protected View mView;
  private MessageDialog noNetDialog;
  
  public BaseFragment()
  {
    this.mBaseHttp = new BaseHttp(this);
  }
  
  public BaseFragment(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    this.mApplication = ((ImcashApplication)this.mActivity.getApplication());
    this.mBaseHttp = new BaseHttp(this);
  }
  
  public static void showBtnBox(Context paramContext, String paramString1, String paramString2, Btn2Dialog.OnBtnListener paramOnBtnListener)
  {
    new Btn2Dialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener);
  }
  
  public static void showBtnBox(Context paramContext, String paramString1, String paramString2, Btn2Dialog.OnBtnListener paramOnBtnListener1, Btn2Dialog.OnBtnListener paramOnBtnListener2, String paramString3)
  {
    new Btn2Dialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener1, paramOnBtnListener2, paramString3);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2, MessageDialog.OnBtnListener paramOnBtnListener)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2, MessageDialog.OnBtnListener paramOnBtnListener, String paramString3)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener, paramString3);
  }
  
  public void failMsg(Message paramMessage)
  {
    stopLoad();
  }
  
  public void finishMsg() {}
  
  public void onNoNet()
  {
    if (this.noNetDialog == null) {
      this.noNetDialog = new MessageDialog(this.mActivity);
    }
    if (!this.noNetDialog.isShowing()) {
      this.noNetDialog.showDialog("提示", "请检查您的网络连接状态！");
    }
    stopLoad();
    GetNetworkInfo.getNetWorkType(this.mActivity);
  }
  
  public void onTokenInvalid()
  {
    stopLoad();
    Toast.makeText(this.mActivity, "账号在其他设备进行了登陆，请重新登录", 1).show();
    this.mApplication.getDataCore().cleanCacheData();
  }
  
  public void postNet(HttpTypeEnum paramHttpTypeEnum, RequestParams paramRequestParams)
  {
    if (this.mApplication.getDataCore().isLogin())
    {
      paramRequestParams.put("id", this.mApplication.getDataCore().getUserId());
      paramRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    }
    HttpRequest.getInstance().postJsonHTTP(this.mBaseHttp, paramHttpTypeEnum, paramRequestParams);
  }
  
  public void postNet(HttpTypeEnum paramHttpTypeEnum, String paramString)
  {
    HttpRequest.getInstance().postJsonHTTP(this.mActivity, this.mBaseHttp, paramHttpTypeEnum, paramString);
  }
  
  public void receiveErro(HttpInfo paramHttpInfo)
  {
    if (HttpData.getErroMsg(paramHttpInfo).contains("重新登录"))
    {
      stopLoad();
      if (this.mApplication.getDataCore().mOutLogin == 0)
      {
        this.mApplication.getDataCore().mOutLogin = 1;
        this.mApplication.getDataCore().cleanCacheData();
        paramHttpInfo = new Intent(this.mActivity, LoginAct.class);
        paramHttpInfo.setFlags(268468224);
        startActivity(paramHttpInfo);
        return;
      }
      return;
    }
    showMessageBox(this.mActivity, HttpData.getErroMsg(paramHttpInfo), "");
    stopLoad();
  }
  
  protected void showToastInCenter(String paramString)
  {
    Toast localToast = this.mCenterToast;
    if (localToast == null) {
      this.mCenterToast = Toast.makeText(this.mActivity, paramString, 1);
    } else {
      localToast.setText(paramString);
    }
    this.mCenterToast.setGravity(17, 0, 0);
    this.mCenterToast.show();
  }
  
  protected void startLoad()
  {
    this.mSelfProgressDialog = new LoadingDialog(this.mActivity);
    this.mSelfProgressDialog.show();
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        if ((!BaseFragment.this.mActivity.isFinishing()) && (BaseFragment.this.mSelfProgressDialog != null) && (BaseFragment.this.mSelfProgressDialog.isShowing()))
        {
          Toast.makeText(BaseFragment.this.mActivity, "请求超时", 1).show();
          BaseFragment.this.mSelfProgressDialog.dismiss();
        }
      }
    }, 10000L);
    this.mSelfProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener()
    {
      public boolean onKey(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
      {
        if ((paramAnonymousInt == 4) && (BaseFragment.this.mSelfProgressDialog.isShowing())) {
          BaseFragment.this.mSelfProgressDialog.dismiss();
        }
        return false;
      }
    });
  }
  
  protected void stopLoad()
  {
    LoadingDialog localLoadingDialog = this.mSelfProgressDialog;
    if (localLoadingDialog != null) {
      localLoadingDialog.dismiss();
    }
  }
  
  public void toPage(Intent paramIntent, Class<?> paramClass)
  {
    paramIntent.setClass(this.mActivity, paramClass);
    this.mActivity.startActivity(paramIntent);
  }
  
  public void toPage(Class<?> paramClass)
  {
    toPage(new Intent(), paramClass);
  }
  
  protected void toWebPage(String paramString1, String paramString2)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this.mActivity, WebAct.class);
    Bundle localBundle = new Bundle();
    localBundle.putString("title_key", paramString1);
    localBundle.putString("web_key", paramString2);
    localIntent.putExtras(localBundle);
    startActivity(localIntent);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/base/BaseFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */