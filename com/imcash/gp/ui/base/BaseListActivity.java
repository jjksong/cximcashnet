package com.imcash.gp.ui.base;

import android.os.Bundle;
import android.os.Handler;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout;
import com.imcash.gp.ui.view.swiperefresh.SwipeLayout.onUpwardListener;
import com.imcash.gp.ui.view.swiperefresh.SwipeRefreshLayout.OnRefreshListener;

public abstract class BaseListActivity
  extends BaseActivity
{
  protected boolean mIsOpenUp = false;
  protected boolean mIsRefreshing = false;
  protected ListView mListView;
  protected SwipeLayout mSwipeLayout;
  
  public void changeRefreshState(int paramInt1, int paramInt2)
  {
    if ((paramInt1 != 0) && (paramInt1 % paramInt2 == 0)) {
      startRefreshAll();
    } else {
      closeUpState();
    }
  }
  
  public void closeRefreshAll()
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(255);
    this.mIsOpenUp = false;
  }
  
  public void closeUpState()
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(238);
    this.mIsOpenUp = false;
  }
  
  protected void initListView()
  {
    this.mListView = ((ListView)findViewById(2131165540));
    this.mListView.setVerticalScrollBarEnabled(false);
    this.mSwipeLayout = ((SwipeLayout)findViewById(2131165802));
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(238);
    this.mSwipeLayout.setColorAllResources(2130968751, 2130968752, 2130968753, 2130968754);
    this.mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
    {
      public void onRefresh()
      {
        BaseListActivity.this.onRefreshDown();
      }
    });
    this.mSwipeLayout.setOnUpwardListener(new SwipeLayout.onUpwardListener()
    {
      public void onUpward()
      {
        BaseListActivity.this.onRefreshUp();
      }
    });
    this.mListView.setOnScrollListener(new AbsListView.OnScrollListener()
    {
      public void onScroll(AbsListView paramAnonymousAbsListView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onScrollStateChanged(AbsListView paramAnonymousAbsListView, int paramAnonymousInt)
      {
        if ((paramAnonymousInt == 0) && (paramAnonymousAbsListView.getLastVisiblePosition() == paramAnonymousAbsListView.getCount() - 1) && (!BaseListActivity.this.mIsRefreshing))
        {
          if (!BaseListActivity.this.mIsOpenUp) {
            return;
          }
          BaseListActivity.this.onRefreshUp();
          new Handler().postDelayed(new Runnable()
          {
            public void run()
            {
              if ((!BaseListActivity.this.isFinishing()) && (BaseListActivity.this.mIsRefreshing)) {
                BaseListActivity.this.mIsRefreshing = false;
              }
            }
          }, 10000L);
        }
      }
    });
    showNoListState();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  protected abstract void onRefreshDown();
  
  protected abstract void onRefreshUp();
  
  protected void showNoListState() {}
  
  public void startRefreshAll()
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    localSwipeLayout.getClass();
    localSwipeLayout.setModule(238);
    this.mIsOpenUp = true;
  }
  
  protected void stopDownRefresh()
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    if (localSwipeLayout != null) {
      localSwipeLayout.setRefreshing(false);
    }
  }
  
  protected void stopUpRefresh()
  {
    SwipeLayout localSwipeLayout = this.mSwipeLayout;
    if (localSwipeLayout != null) {
      localSwipeLayout.setRefreshingBottom(false);
    }
    this.mIsRefreshing = false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/base/BaseListActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */