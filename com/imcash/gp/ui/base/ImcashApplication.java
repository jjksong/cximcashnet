package com.imcash.gp.ui.base;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.support.multidex.MultiDexApplication;
import cn.jpush.android.api.JPushInterface;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.tools.GetNetworkInfo;
import java.util.Iterator;
import java.util.List;
import org.xutils.x.Ext;

public class ImcashApplication
  extends MultiDexApplication
{
  protected static ImcashApplication mInstance;
  protected DataCore mDataCore;
  protected int maxMemory = 0;
  private Thread.UncaughtExceptionHandler restartHandler = new Thread.UncaughtExceptionHandler()
  {
    public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable) {}
  };
  
  public static ImcashApplication getInstance()
  {
    return mInstance;
  }
  
  public DataCore getDataCore()
  {
    if (this.mDataCore == null) {
      this.mDataCore = new DataCore(this);
    }
    return this.mDataCore;
  }
  
  public String getToken()
  {
    return this.mDataCore.getToken();
  }
  
  protected void init()
  {
    if (this.mDataCore == null) {
      this.mDataCore = new DataCore(this);
    }
    GetNetworkInfo.getNetWorkType(this);
    JPushInterface.setDebugMode(false);
    JPushInterface.init(this);
  }
  
  public boolean isAppOnForeground()
  {
    Object localObject = (ActivityManager)getApplicationContext().getSystemService("activity");
    String str = getApplicationContext().getPackageName();
    localObject = ((ActivityManager)localObject).getRunningAppProcesses();
    if (localObject == null) {
      return false;
    }
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject).next();
      if ((localRunningAppProcessInfo.processName.equals(str)) && (localRunningAppProcessInfo.importance == 100)) {
        return true;
      }
    }
    return false;
  }
  
  public void onCreate()
  {
    super.onCreate();
    mInstance = this;
    this.maxMemory = ((int)(Runtime.getRuntime().maxMemory() / 1024L));
    Thread.setDefaultUncaughtExceptionHandler(this.restartHandler);
    x.Ext.init(this);
    x.Ext.setDebug(false);
    init();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/base/ImcashApplication.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */