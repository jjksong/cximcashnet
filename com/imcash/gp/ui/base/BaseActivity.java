package com.imcash.gp.ui.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Images.Media;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpData;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpOnReceive;
import com.imcash.gp.http.HttpRequest;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.BitmapTools;
import com.imcash.gp.tools.FileUtil;
import com.imcash.gp.tools.GetNetworkInfo;
import com.imcash.gp.ui.activity.LoginAct;
import com.imcash.gp.ui.activity.PdfAct;
import com.imcash.gp.ui.activity.QRCodeActivity;
import com.imcash.gp.ui.activity.WebAct;
import com.imcash.gp.ui.model.WindowInfo;
import com.imcash.gp.ui.view.Btn2Dialog;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.imcash.gp.ui.view.LoadingDialog;
import com.imcash.gp.ui.view.MessageDialog;
import com.imcash.gp.ui.view.MessageDialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import com.zhihu.matisse.Matisse;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import top.zibin.luban.Luban;
import top.zibin.luban.Luban.Builder;
import top.zibin.luban.OnCompressListener;

public abstract class BaseActivity
  extends Activity
  implements View.OnClickListener, HttpOnReceive
{
  protected static final int Aavtar_CHOOSE_PICTURE = 3;
  protected static final int Avatar_New_Choose = 25;
  protected static final int Avatar_TAKE_PICTURE = 2;
  protected static final int Bitmap = 221;
  protected static final int CHOOSE_PICTURE = 1;
  protected static final int PICTURE_LIMIT_SIZE = 1500;
  protected static final int PICTURE_SCALE_SIZE = 2;
  public static final int QR_CODE = 26;
  protected static final int REQUEST_CODE_CHOOSE = 23;
  protected static final int REQUEST_LOGO_CODE_CHOOSE = 24;
  protected static final int TAKE_PICTURE = 0;
  protected InputMethodManager imm;
  public ImcashApplication mApplication;
  protected BaseHttp mBaseHttp;
  protected File mBitmapFile;
  protected ImageView mCenterIv;
  private Toast mCenterToast;
  protected int mFirstId;
  protected int mSecondId;
  protected LoadingDialog mSelfProgressDialog;
  private Toast mToast;
  private MessageDialog noNetDialog;
  protected File tempFile;
  
  public static void showBtnBox(Context paramContext, String paramString1, String paramString2, Btn2Dialog.OnBtnListener paramOnBtnListener)
  {
    new Btn2Dialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener);
  }
  
  public static void showBtnBox(Context paramContext, String paramString1, String paramString2, Btn2Dialog.OnBtnListener paramOnBtnListener1, Btn2Dialog.OnBtnListener paramOnBtnListener2, String paramString3)
  {
    new Btn2Dialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener1, paramOnBtnListener2, paramString3);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2, MessageDialog.OnBtnListener paramOnBtnListener)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener);
  }
  
  public static void showMessageBox(Context paramContext, String paramString1, String paramString2, MessageDialog.OnBtnListener paramOnBtnListener, String paramString3)
  {
    new MessageDialog(paramContext).showDialog(paramString1, paramString2, paramOnBtnListener, paramString3);
  }
  
  public Object GetIntentData(String paramString)
  {
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey(paramString))) {
      return localBundle.get(paramString);
    }
    return null;
  }
  
  protected void LuBanFile(File paramFile)
  {
    long l = paramFile.length() / 1024L / 1024L;
    Luban.with(this).load(paramFile).ignoreBy(100).setTargetDir(FileUtil.MAINFILE.getPath()).setCompressListener(new OnCompressListener()
    {
      public void onError(Throwable paramAnonymousThrowable) {}
      
      public void onStart() {}
      
      public void onSuccess(File paramAnonymousFile)
      {
        BaseActivity.this.lubanCompressSuccess(paramAnonymousFile);
      }
    }).launch();
  }
  
  protected void StartAlbumPic()
  {
    Intent localIntent;
    if (Build.VERSION.SDK_INT >= 19) {
      localIntent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    } else {
      localIntent = new Intent("android.intent.action.GET_CONTENT");
    }
    localIntent.setType("image/jpg,image/png");
    startActivityForResult(localIntent, 1);
  }
  
  protected void StartCameraPic()
  {
    if (!FileUtil.checkSDCardAvailable())
    {
      showToast("无SD卡将不能使用拍照功能!");
      return;
    }
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    File localFile = FileUtil.MAINFILE;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(DateFormat.format("'IMG'_yyyyMMdd_hhmmss", Calendar.getInstance(Locale.CHINA)));
    localStringBuilder.append(".jpg");
    this.tempFile = new File(localFile, localStringBuilder.toString());
    localIntent.putExtra("output", Uri.fromFile(this.tempFile));
    startActivityForResult(localIntent, 0);
  }
  
  protected void avatarCamera()
  {
    if (!FileUtil.checkSDCardAvailable())
    {
      showToast("无SD卡将不能使用拍照功能!");
      return;
    }
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    if (FileUtil.checkSDCardAvailable())
    {
      this.mBitmapFile = FileUtil.MAINFILE;
      FileUtil.mkdir(this.mBitmapFile);
      File localFile = this.mBitmapFile;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(DateFormat.format("'IMG'_yyyyMMdd_hhmmss", Calendar.getInstance(Locale.CHINA)));
      localStringBuilder.append(".jpg");
      this.mBitmapFile = new File(localFile, localStringBuilder.toString());
      localIntent.putExtra("output", Uri.fromFile(this.mBitmapFile));
    }
    startActivityForResult(localIntent, 2);
  }
  
  protected void avatarPhoto()
  {
    Intent localIntent;
    if (Build.VERSION.SDK_INT >= 19) {
      localIntent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    } else {
      localIntent = new Intent("android.intent.action.GET_CONTENT");
    }
    localIntent.setType("image/*");
    startActivityForResult(localIntent, 3);
  }
  
  public void back(View paramView)
  {
    finish();
  }
  
  protected Bitmap bitmapLogic(Bitmap paramBitmap)
  {
    if ((paramBitmap.getWidth() <= 1500) && (paramBitmap.getHeight() <= 1500)) {
      paramBitmap = ThumbnailUtils.extractThumbnail(paramBitmap, paramBitmap.getWidth() - 1, paramBitmap.getHeight() - 1, 2);
    } else {
      paramBitmap = ThumbnailUtils.extractThumbnail(paramBitmap, paramBitmap.getWidth() / 2, paramBitmap.getHeight() / 2, 2);
    }
    return paramBitmap;
  }
  
  protected void call(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("tel:");
    localStringBuilder.append(paramString);
    paramString = new Intent("android.intent.action.DIAL", Uri.parse(localStringBuilder.toString()));
    paramString.setFlags(268435456);
    startActivity(paramString);
  }
  
  public void failMsg(Message paramMessage)
  {
    stopLoad();
  }
  
  public void finishMsg() {}
  
  public RequestParams getIdAndKeyNumParams()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    return localRequestParams;
  }
  
  protected void getNewAvatar(List<Uri> paramList)
  {
    startPhotoZoom((Uri)paramList.get(0), 255);
  }
  
  protected void getNewLogo(List<Uri> paramList)
  {
    startLogoZoom((Uri)paramList.get(0), 255);
  }
  
  protected void getQrCodePermission()
  {
    XXPermissions.with(this).constantRequest().permission(new String[] { "android.permission.CAMERA" }).request(new OnPermission()
    {
      public void hasPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        new Intent();
        paramAnonymousList = new Intent(BaseActivity.this, QRCodeActivity.class);
        BaseActivity.this.startActivityForResult(paramAnonymousList, 26);
      }
      
      public void noPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        BaseActivity.showBtnBox(BaseActivity.this, "权限获取失败，该功能暂时无法使用，是否要开启权限？", null, new Btn2Dialog.OnBtnListener()
        {
          public void onClick()
          {
            XXPermissions.gotoPermissionSettings(BaseActivity.this);
          }
        });
      }
    });
  }
  
  protected void getQrStr(String paramString) {}
  
  protected void getTestPhone(List<Uri> paramList, List<String> paramList1) {}
  
  protected String getTitleStr()
  {
    return ((TextView)findViewById(2131165822)).getText().toString();
  }
  
  public void hideKeyBoard()
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)getSystemService("input_method");
    try
    {
      localInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      for (;;) {}
    }
  }
  
  protected abstract void initData();
  
  protected abstract void initView();
  
  protected void initWindowInfo()
  {
    this.mApplication.mDataCore.mWindowInfo = new WindowInfo();
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.mApplication.mDataCore.mWindowInfo.width = localDisplayMetrics.widthPixels;
    this.mApplication.mDataCore.mWindowInfo.height = localDisplayMetrics.heightPixels;
    this.mApplication.mDataCore.mWindowInfo.density = localDisplayMetrics.density;
    this.mApplication.mDataCore.mWindowInfo.densityDpi = localDisplayMetrics.densityDpi;
    this.mApplication.mDataCore.mWindowInfo.refresh();
  }
  
  protected void lubanCompressSuccess(File paramFile) {}
  
  public void newAvatarChoose() {}
  
  public void newLogoChoose() {}
  
  public void newPhotoChoose(int paramInt)
  {
    XXPermissions.with(this).constantRequest().permission(new String[] { "android.permission.CAMERA" }).request(new OnPermission()
    {
      public void hasPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean) {}
      
      public void noPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        BaseActivity.showBtnBox(BaseActivity.this, "权限获取失败，该功能暂时无法使用，是否要开启权限？", null, new Btn2Dialog.OnBtnListener()
        {
          public void onClick()
          {
            XXPermissions.gotoPermissionSettings(BaseActivity.this);
          }
        });
      }
    });
  }
  
  @SuppressLint({"NewApi"})
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject2;
    Cursor localCursor;
    Object localObject1;
    if (paramInt2 == -1) {
      if (paramInt1 != 221)
      {
        localObject2 = null;
        localCursor = null;
        localObject1 = null;
        switch (paramInt1)
        {
        default: 
          switch (paramInt1)
          {
          default: 
            break;
          case 26: 
            if (paramIntent == null) {
              break;
            }
          }
          break;
        }
      }
    }
    try
    {
      if (paramIntent.getExtras() != null) {
        break label377;
      }
      return;
    }
    catch (Exception paramIntent)
    {
      for (;;) {}
    }
    getNewAvatar(Matisse.obtainResult(paramIntent));
    break label377;
    getNewLogo(Matisse.obtainResult(paramIntent));
    break label377;
    getTestPhone(Matisse.obtainResult(paramIntent), Matisse.obtainPathResult(paramIntent));
    break label377;
    if (paramIntent != null)
    {
      startPhotoZoom(paramIntent.getData(), 255);
      break label377;
      startPhotoZoom(Uri.fromFile(this.mBitmapFile), 255);
      break label377;
      paramIntent = paramIntent.getData();
      localCursor = getContentResolver().query(paramIntent, new String[] { "_data" }, null, null, null);
      if (localCursor == null)
      {
        paramIntent = paramIntent.toString();
        System.out.println(paramIntent);
        if (paramIntent.contains("file:///"))
        {
          paramIntent = paramIntent.substring(7);
          try
          {
            paramIntent = BitmapTools.revitionImageSize(paramIntent, 100);
          }
          catch (IOException paramIntent)
          {
            paramIntent.printStackTrace();
            paramIntent = (Intent)localObject1;
          }
          if (paramIntent != null) {
            postImage(paramIntent);
          }
          return;
        }
      }
      paramInt1 = localCursor.getColumnIndexOrThrow("_data");
      localCursor.moveToFirst();
      paramIntent = localCursor.getString(paramInt1);
      try
      {
        paramIntent = BitmapTools.revitionImageSize(paramIntent, 100);
      }
      catch (IOException paramIntent)
      {
        paramIntent.printStackTrace();
        paramIntent = (Intent)localObject2;
      }
      if (paramIntent != null) {
        postImage(paramIntent);
      }
      localCursor.close();
      break label377;
      try
      {
        paramIntent = BitmapTools.revitionImageSize(this.tempFile.getPath());
      }
      catch (IOException paramIntent)
      {
        paramIntent.printStackTrace();
        paramIntent = localCursor;
      }
      if (paramIntent != null)
      {
        postImage(paramIntent);
        break label377;
        if (paramIntent != null) {
          setPicToView(paramIntent);
        }
      }
    }
    label377:
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mApplication = ((ImcashApplication)getApplication());
    this.mBaseHttp = new BaseHttp(this, this);
    this.imm = ((InputMethodManager)getSystemService("input_method"));
    if (paramBundle != null) {
      this.mApplication.getDataCore().resetData(paramBundle);
    }
    if (this.mApplication.mDataCore.mWindowInfo == null) {
      initWindowInfo();
    }
  }
  
  public void onNoNet()
  {
    if (this.noNetDialog == null) {
      this.noNetDialog = new MessageDialog(this);
    }
    if (!this.noNetDialog.isShowing()) {
      this.noNetDialog.showDialog("提示", "请检查您的网络连接状态！");
    }
    stopLoad();
    GetNetworkInfo.getNetWorkType(this);
  }
  
  protected void onRestoreInstanceState(Bundle paramBundle) {}
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.mApplication.getDataCore().saveData(paramBundle);
  }
  
  public void onTokenInvalid()
  {
    stopLoad();
    showToast("账号在其他设备进行了登陆，请重新登录");
    this.mApplication.getDataCore().cleanCacheData();
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((paramMotionEvent.getAction() == 0) && (getCurrentFocus() != null) && (getCurrentFocus().getWindowToken() != null)) {
      this.imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  protected void photoChoose() {}
  
  protected void postImage(Bitmap paramBitmap) {}
  
  public void postNet(HttpTypeEnum paramHttpTypeEnum, RequestParams paramRequestParams)
  {
    if (this.mApplication.getDataCore().isLogin())
    {
      paramRequestParams.put("id", this.mApplication.getDataCore().getUserId());
      paramRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    }
    HttpRequest.getInstance().postJsonHTTP(this.mBaseHttp, paramHttpTypeEnum, paramRequestParams);
  }
  
  public void postNet(HttpTypeEnum paramHttpTypeEnum, String paramString)
  {
    HttpRequest.getInstance().postJsonHTTP(this, this.mBaseHttp, paramHttpTypeEnum, paramString);
  }
  
  public void receiveErro(HttpInfo paramHttpInfo)
  {
    if (HttpData.getErroMsg(paramHttpInfo).contains("重新登录"))
    {
      stopLoad();
      if (this.mApplication.getDataCore().mOutLogin == 0)
      {
        this.mApplication.getDataCore().mOutLogin = 1;
        this.mApplication.getDataCore().cleanCacheData();
        paramHttpInfo = new Intent(this, LoginAct.class);
        paramHttpInfo.setFlags(268468224);
        startActivity(paramHttpInfo);
        return;
      }
      return;
    }
    showMessageBox(this, HttpData.getErroMsg(paramHttpInfo), "");
    stopLoad();
  }
  
  protected void setPicToView(Intent paramIntent) {}
  
  protected void setTitleStr(String paramString)
  {
    TextView localTextView = (TextView)findViewById(2131165822);
    if (localTextView != null) {
      localTextView.setText(paramString);
    }
    localTextView.setOnClickListener(this);
  }
  
  protected void showToast(CharSequence paramCharSequence)
  {
    Toast localToast = this.mToast;
    if (localToast == null) {
      this.mToast = Toast.makeText(this, paramCharSequence, 0);
    } else {
      localToast.setText(paramCharSequence);
    }
    this.mToast.show();
  }
  
  protected void showToastInCenter(String paramString)
  {
    Toast localToast = this.mCenterToast;
    if (localToast == null) {
      this.mCenterToast = Toast.makeText(this, paramString, 1);
    } else {
      localToast.setText(paramString);
    }
    this.mCenterToast.setGravity(17, 0, 0);
    this.mCenterToast.show();
  }
  
  protected void startLoad()
  {
    this.mSelfProgressDialog = new LoadingDialog(this);
    this.mSelfProgressDialog.show();
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        if ((!BaseActivity.this.isFinishing()) && (BaseActivity.this.mSelfProgressDialog != null) && (BaseActivity.this.mSelfProgressDialog.isShowing()))
        {
          Toast.makeText(BaseActivity.this, "请求超时", 1).show();
          BaseActivity.this.mSelfProgressDialog.dismiss();
        }
      }
    }, 10000L);
    this.mSelfProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener()
    {
      public boolean onKey(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
      {
        if ((paramAnonymousInt == 4) && (BaseActivity.this.mSelfProgressDialog.isShowing())) {
          BaseActivity.this.mSelfProgressDialog.dismiss();
        }
        return false;
      }
    });
  }
  
  protected void startLogoZoom(Uri paramUri, int paramInt)
  {
    Intent localIntent = new Intent("com.android.camera.action.CROP");
    localIntent.setDataAndType(paramUri, "image/*");
    localIntent.addFlags(1);
    localIntent.addFlags(2);
    localIntent.putExtra("crop", "true");
    localIntent.putExtra("aspectX", 16);
    localIntent.putExtra("aspectY", 9);
    localIntent.putExtra("outputX", 480);
    localIntent.putExtra("outputY", 270);
    localIntent.putExtra("return-data", true);
    startActivityForResult(localIntent, 221);
  }
  
  protected void startPhotoZoom(Uri paramUri, int paramInt)
  {
    Intent localIntent = new Intent("com.android.camera.action.CROP");
    localIntent.setDataAndType(paramUri, "image/*");
    localIntent.addFlags(1);
    localIntent.addFlags(2);
    localIntent.putExtra("crop", "true");
    localIntent.putExtra("aspectX", 1);
    localIntent.putExtra("aspectY", 1);
    localIntent.putExtra("outputX", paramInt);
    localIntent.putExtra("outputY", paramInt);
    localIntent.putExtra("return-data", true);
    startActivityForResult(localIntent, 221);
  }
  
  protected void stopLoad()
  {
    LoadingDialog localLoadingDialog = this.mSelfProgressDialog;
    if (localLoadingDialog != null) {
      localLoadingDialog.dismiss();
    }
  }
  
  public void toPage(Intent paramIntent, Class<?> paramClass)
  {
    paramIntent.setClass(this, paramClass);
    startActivity(paramIntent);
  }
  
  public void toPage(Class<?> paramClass)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this, paramClass);
    startActivity(localIntent);
  }
  
  public void toPdfPage(String paramString1, String paramString2)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this, PdfAct.class);
    localIntent.putExtra("PdfTitleKey", paramString1);
    localIntent.putExtra("PdfNameKey", paramString2);
    startActivity(localIntent);
  }
  
  protected void toWebPage(String paramString1, String paramString2)
  {
    Intent localIntent = new Intent();
    localIntent.setClass(this, WebAct.class);
    Bundle localBundle = new Bundle();
    localBundle.putString("title_key", paramString1);
    localBundle.putString("web_key", paramString2);
    localIntent.putExtras(localBundle);
    startActivity(localIntent);
  }
  
  protected abstract void updatePage();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/base/BaseActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */