package com.imcash.gp.ui.base;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.imcash.gp.http.HttpCallBack;
import com.imcash.gp.http.HttpData;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpOnReceive;
import com.imcash.gp.http.HttpTypeEnum;
import org.json.JSONObject;

@SuppressLint({"HandlerLeak"})
public class BaseHttp
  implements HttpCallBack
{
  protected BaseActivity mActivity;
  private Handler mErrorHandler = null;
  private Handler mFinishHandler = null;
  @SuppressLint({"HandlerLeak"})
  private Handler mHandler = null;
  protected HttpOnReceive mOnReceive;
  
  public BaseHttp(HttpOnReceive paramHttpOnReceive)
  {
    this.mOnReceive = paramHttpOnReceive;
    initHandler();
  }
  
  public BaseHttp(HttpOnReceive paramHttpOnReceive, BaseActivity paramBaseActivity)
  {
    this.mOnReceive = paramHttpOnReceive;
    this.mActivity = paramBaseActivity;
    initHandler();
  }
  
  public void httpFailure(HttpTypeEnum paramHttpTypeEnum, Throwable paramThrowable)
  {
    paramHttpTypeEnum = this.mErrorHandler.obtainMessage();
    paramHttpTypeEnum.obj = paramThrowable;
    this.mErrorHandler.sendMessage(paramHttpTypeEnum);
  }
  
  public void httpFailure(HttpTypeEnum paramHttpTypeEnum, Throwable paramThrowable, int paramInt)
  {
    paramHttpTypeEnum = this.mErrorHandler.obtainMessage();
    paramHttpTypeEnum.obj = paramThrowable;
    this.mErrorHandler.sendMessage(paramHttpTypeEnum);
  }
  
  public void httpFinish()
  {
    Message localMessage = this.mFinishHandler.obtainMessage();
    this.mFinishHandler.sendMessage(localMessage);
  }
  
  public void httpSuccess(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    Message localMessage = this.mHandler.obtainMessage();
    localMessage.obj = new HttpInfo(paramJSONObject, paramHttpTypeEnum);
    this.mHandler.sendMessage(localMessage);
  }
  
  public void httpSuccess(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject, int paramInt)
  {
    Message localMessage = this.mHandler.obtainMessage();
    localMessage.obj = new HttpInfo(paramJSONObject, paramHttpTypeEnum, paramInt);
    this.mHandler.sendMessage(localMessage);
  }
  
  protected void initHandler()
  {
    try
    {
      Object localObject = new com/imcash/gp/ui/base/BaseHttp$1;
      ((1)localObject).<init>(this);
      this.mHandler = ((Handler)localObject);
      localObject = new com/imcash/gp/ui/base/BaseHttp$2;
      ((2)localObject).<init>(this);
      this.mErrorHandler = ((Handler)localObject);
      localObject = new com/imcash/gp/ui/base/BaseHttp$3;
      ((3)localObject).<init>(this);
      this.mFinishHandler = ((Handler)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/base/BaseHttp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */