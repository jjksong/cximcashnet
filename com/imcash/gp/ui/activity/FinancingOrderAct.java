package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.FinancingOrder;
import com.imcash.gp.ui.adapter.FinancingOrderAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296335)
public class FinancingOrderAct
  extends BaseListActivity
{
  @ViewInject(2131165419)
  protected LinearLayout empty_view;
  protected FinancingOrderAdapter mAdapter;
  protected ArrayList<FinancingOrder> mDatas;
  public int mPageIndex = 1;
  
  @Event({2131165831})
  private void postToMain(View paramView)
  {
    this.mApplication.getDataCore().mTargetTag = "main_fragment";
    toPage(MainActivity.class);
    finish();
  }
  
  protected void getOrderList(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      this.mPageIndex += 1;
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if (HttpTypeEnum.ConductList_Down == paramHttpTypeEnum)
      {
        this.mDatas.clear();
        stopDownRefresh();
      }
      if (HttpTypeEnum.ConductList_Up == paramHttpTypeEnum) {
        stopUpRefresh();
      }
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
      if (this.mDatas.size() == 0) {
        this.empty_view.setVisibility(0);
      } else {
        this.empty_view.setVisibility(8);
      }
      changeRefreshState(paramJSONObject.size(), 15);
      this.mAdapter.notifyDataSetChanged();
      this.mListView.invalidate();
      return;
    }
    closeUpState();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getOrderList(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("持仓明细");
    this.mDatas = new ArrayList();
    this.mAdapter = new FinancingOrderAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
  }
  
  protected void initView()
  {
    initListView();
    startRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPageIndex = 1;
    postOrderList(HttpTypeEnum.ConductList_Down);
  }
  
  protected void onRefreshUp()
  {
    postOrderList(HttpTypeEnum.ConductList_Up);
  }
  
  public void onResume()
  {
    super.onResume();
    onRefreshDown();
  }
  
  protected void postOrderList(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPageIndex);
    localRequestParams.put("page_num", 15);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  public void toAddCard(int paramInt)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("ProjectIdKey", ((FinancingOrder)this.mDatas.get(paramInt)).getId());
    localIntent.putExtra("TouziIdKey", ((FinancingOrder)this.mDatas.get(paramInt)).getTouzi_id());
    toPage(localIntent, FinancingCardAct.class);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/FinancingOrderAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */