package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296303)
public class RebackBuyResult
  extends BaseActivity
{
  @ViewInject(2131165709)
  protected ImageView result_img;
  @ViewInject(2131165710)
  protected TextView result_title;
  @ViewInject(2131165711)
  protected TextView resutl_msg;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("回购成功");
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RebackBuyResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */