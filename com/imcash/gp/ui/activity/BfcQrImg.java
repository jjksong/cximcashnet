package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.tools.ClipboardTools;
import com.imcash.gp.tools.ZXingCodeUtils;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296301)
public class BfcQrImg
  extends BaseActivity
{
  @ViewInject(2131165225)
  protected TextView address;
  protected Bitmap mQrBitmap;
  @ViewInject(2131165682)
  protected ImageView qr_code;
  
  @Event({2131165377})
  private void copyClick(View paramView)
  {
    ClipboardTools.copy(this.address.getText().toString(), this);
    showToast("复制成功");
  }
  
  protected void getQrAddress(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject).optString("ethAddress", "");
    this.address.setText(paramJSONObject);
    this.mQrBitmap = ZXingCodeUtils.getInstance().createQRCode(paramJSONObject, this.qr_code.getWidth(), this.qr_code.getHeight());
    this.qr_code.setImageBitmap(this.mQrBitmap);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getQrAddress(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("充值");
    postNet(HttpTypeEnum.BFC_QrAddress, new RequestParams());
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Bitmap localBitmap = this.mQrBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled()))
    {
      this.mQrBitmap.recycle();
      this.mQrBitmap = null;
      System.gc();
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcQrImg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */