package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.model.TradeScreen;
import com.imcash.gp.ui.base.BaseActivity;
import org.greenrobot.eventbus.EventBus;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296400)
public class TradeScreenAct
  extends BaseActivity
{
  @ViewInject(2131165273)
  protected RadioGroup bfc_group;
  @ViewInject(2131165351)
  protected RadioGroup coin_group;
  private TradeScreen mTradeState;
  @ViewInject(2131165585)
  protected RadioGroup money_group;
  @ViewInject(2131165692)
  protected RadioGroup rate_group;
  @ViewInject(2131165819)
  protected RadioGroup time_group;
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    EventBus.getDefault().post(this.mTradeState);
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("筛选列表");
    this.mTradeState = new TradeScreen("", "", "", "", "");
    this.time_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        paramAnonymousInt = TradeScreenAct.this.time_group.getCheckedRadioButtonId();
        if (paramAnonymousInt != 2131165818)
        {
          if (paramAnonymousInt == 2131165821) {
            TradeScreenAct.this.mTradeState.setmTimeState("up");
          }
        }
        else {
          TradeScreenAct.this.mTradeState.setmTimeState("down");
        }
      }
    });
    this.money_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        paramAnonymousInt = TradeScreenAct.this.money_group.getCheckedRadioButtonId();
        if (paramAnonymousInt != 2131165584)
        {
          if (paramAnonymousInt == 2131165591) {
            TradeScreenAct.this.mTradeState.setmMoneyState("up");
          }
        }
        else {
          TradeScreenAct.this.mTradeState.setmMoneyState("down");
        }
      }
    });
    this.rate_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        paramAnonymousInt = TradeScreenAct.this.rate_group.getCheckedRadioButtonId();
        if (paramAnonymousInt != 2131165691)
        {
          if (paramAnonymousInt == 2131165695) {
            TradeScreenAct.this.mTradeState.setmRateState("up");
          }
        }
        else {
          TradeScreenAct.this.mTradeState.setmRateState("down");
        }
      }
    });
    this.coin_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        Object localObject;
        switch (TradeScreenAct.this.coin_group.getCheckedRadioButtonId())
        {
        default: 
          break;
        case 2131165880: 
          localObject = TradeScreenAct.this.mTradeState;
          paramAnonymousRadioGroup = new StringBuilder();
          paramAnonymousRadioGroup.append(GlobalFunction.getCoinIndexByName(com.imcash.gp.GlobalFunction.Constants.CoinType[4]));
          paramAnonymousRadioGroup.append("");
          ((TradeScreen)localObject).setmCoinType(paramAnonymousRadioGroup.toString());
          break;
        case 2131165559: 
          paramAnonymousRadioGroup = TradeScreenAct.this.mTradeState;
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append(GlobalFunction.getCoinIndexByName(com.imcash.gp.GlobalFunction.Constants.CoinType[2]));
          ((StringBuilder)localObject).append("");
          paramAnonymousRadioGroup.setmCoinType(((StringBuilder)localObject).toString());
          break;
        case 2131165424: 
          localObject = TradeScreenAct.this.mTradeState;
          paramAnonymousRadioGroup = new StringBuilder();
          paramAnonymousRadioGroup.append(GlobalFunction.getCoinIndexByName(com.imcash.gp.GlobalFunction.Constants.CoinType[1]));
          paramAnonymousRadioGroup.append("");
          ((TradeScreen)localObject).setmCoinType(paramAnonymousRadioGroup.toString());
          break;
        case 2131165302: 
          localObject = TradeScreenAct.this.mTradeState;
          paramAnonymousRadioGroup = new StringBuilder();
          paramAnonymousRadioGroup.append(GlobalFunction.getCoinIndexByName(com.imcash.gp.GlobalFunction.Constants.CoinType[0]));
          paramAnonymousRadioGroup.append("");
          ((TradeScreen)localObject).setmCoinType(paramAnonymousRadioGroup.toString());
          break;
        case 2131165261: 
          paramAnonymousRadioGroup = TradeScreenAct.this.mTradeState;
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append(GlobalFunction.getCoinIndexByName(com.imcash.gp.GlobalFunction.Constants.CoinType[3]));
          ((StringBuilder)localObject).append("");
          paramAnonymousRadioGroup.setmCoinType(((StringBuilder)localObject).toString());
        }
      }
    });
    this.bfc_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        switch (TradeScreenAct.this.bfc_group.getCheckedRadioButtonId())
        {
        default: 
          break;
        case 2131165268: 
          TradeScreenAct.this.mTradeState.setmBfcType("90");
          break;
        case 2131165267: 
          TradeScreenAct.this.mTradeState.setmBfcType("60");
          break;
        case 2131165266: 
          TradeScreenAct.this.mTradeState.setmBfcType("30");
          break;
        case 2131165265: 
          TradeScreenAct.this.mTradeState.setmBfcType("150");
          break;
        case 2131165264: 
          TradeScreenAct.this.mTradeState.setmBfcType("120");
        }
      }
    });
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeScreenAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */