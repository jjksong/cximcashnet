package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.Coin;
import com.imcash.gp.ui.adapter.AddAccountAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296291)
public class AddAccountAct
  extends BaseListActivity
{
  protected AddAccountAdapter mAdapter;
  protected ArrayList<Coin> mDatas;
  @ViewInject(2131165718)
  protected TextView right_tv;
  
  private void rightClicked()
  {
    Object localObject = this.mDatas;
    if ((localObject != null) && (((ArrayList)localObject).size() != 0) && (isHaveNeedAdd()))
    {
      localObject = "";
      Iterator localIterator = this.mDatas.iterator();
      while (localIterator.hasNext())
      {
        Coin localCoin = (Coin)localIterator.next();
        if (localCoin.mIsSelected)
        {
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append((String)localObject);
          localStringBuilder.append(localCoin.getId());
          localStringBuilder.append(",");
          localObject = localStringBuilder.toString();
        }
      }
      postAdd(((String)localObject).substring(0, ((String)localObject).length() - 1));
      return;
    }
  }
  
  protected void getWalletNotShow(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("添加成功");
      finish();
      break;
    case ???: 
      getWalletNotShow(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new AddAccountAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        ((Coin)AddAccountAct.this.mDatas.get(paramAnonymousInt)).mIsSelected ^= true;
        AddAccountAct.this.mAdapter.notifyDataSetChanged();
        AddAccountAct.this.mListView.invalidate();
      }
    });
    postWalletNotShow();
  }
  
  protected void initView()
  {
    setTitleStr("添加资产");
    this.right_tv.setText("添加");
    this.right_tv.setOnClickListener(this);
    initListView();
    closeRefreshAll();
  }
  
  protected boolean isHaveNeedAdd()
  {
    Iterator localIterator = this.mDatas.iterator();
    while (localIterator.hasNext()) {
      if (((Coin)localIterator.next()).mIsSelected) {
        return true;
      }
    }
    boolean bool = false;
    return bool;
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165718) {
      rightClicked();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postAdd(String paramString)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_list", paramString);
    postNet(HttpTypeEnum.Wallet_Choice_Show, localRequestParams);
  }
  
  protected void postWalletNotShow()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Wallet_Not_Show, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/AddAccountAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */