package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.ListDataBean;
import com.imcash.gp.ui.adapter.CheckResultAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296321)
public class CheckResultAct
  extends BaseListActivity
{
  public static final String CheckResultKey = "CheckResultKey";
  protected TextView connections;
  protected TextView count_tv;
  protected TextView credit;
  protected TextView hobby;
  protected TextView info;
  protected CheckResultAdapter mAdapter;
  protected ArrayList<ListDataBean> mDatas;
  protected View mHeadView;
  protected TextView name;
  protected TextView perform;
  protected LinearLayout to_baoxian;
  
  protected void getSafeData(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    JSONObject localJSONObject = paramJSONObject.optJSONObject("data");
    paramJSONObject = localJSONObject.optString("tui_data");
    localJSONObject = localJSONObject.optJSONObject("safe_detail");
    this.name.setText(localJSONObject.optString("title", ""));
    this.info.setText(localJSONObject.optString("content", ""));
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (4.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getSafeData(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new CheckResultAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    try
    {
      Object localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>((String)GetIntentData("CheckResultKey"));
      localObject = ((JSONObject)localObject).optJSONObject("data");
      this.perform.setText(((JSONObject)localObject).optString("perform", "0"));
      this.connections.setText(((JSONObject)localObject).optString("connections", "0"));
      this.hobby.setText(((JSONObject)localObject).optString("hobby", "0"));
      this.credit.setText(((JSONObject)localObject).optString("credit", "0"));
      int j = Integer.valueOf(this.perform.getText().toString()).intValue();
      int m = Integer.valueOf(this.connections.getText().toString()).intValue();
      int i = Integer.valueOf(this.hobby.getText().toString()).intValue();
      int k = Integer.valueOf(this.credit.getText().toString()).intValue();
      localObject = this.count_tv;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(j + m + i + k);
      localStringBuilder.append("");
      ((TextView)localObject).setText(localStringBuilder.toString());
      this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          if (paramAnonymousInt == 0) {
            return;
          }
          paramAnonymousAdapterView = (ListDataBean)CheckResultAct.this.mDatas.get(paramAnonymousInt - 1);
          paramAnonymousView = new Intent();
          paramAnonymousView.putExtra("FinancingDetailKey", new Gson().toJson(paramAnonymousAdapterView));
          CheckResultAct.this.toPage(paramAnonymousView, FinancingDetailAct.class);
        }
      });
      postSafeRecommend();
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void initView()
  {
    setTitleStr("ImCash指数");
    initListView();
    closeRefreshAll();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296323, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.count_tv = ((TextView)this.mHeadView.findViewById(2131165384));
    this.hobby = ((TextView)this.mHeadView.findViewById(2131165468));
    this.perform = ((TextView)this.mHeadView.findViewById(2131165652));
    this.connections = ((TextView)this.mHeadView.findViewById(2131165367));
    this.credit = ((TextView)this.mHeadView.findViewById(2131165388));
    this.name = ((TextView)this.mHeadView.findViewById(2131165598));
    this.info = ((TextView)this.mHeadView.findViewById(2131165497));
    this.to_baoxian = ((LinearLayout)this.mHeadView.findViewById(2131165830));
    this.to_baoxian.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CheckResultAct.this.toPage(PolicyAct.class);
      }
    });
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postSafeRecommend()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Safe_Recommed, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/CheckResultAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */