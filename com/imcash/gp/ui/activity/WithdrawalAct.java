package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.CoinLimit;
import com.imcash.gp.model.User;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.PayCheckView;
import com.imcash.gp.ui.view.PayCheckViewCall;
import com.loopj.android.http.RequestParams;
import java.text.DecimalFormat;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296413)
public class WithdrawalAct
  extends BaseActivity
{
  public static final String WalletDataKey = "WalletDataKey";
  @ViewInject(2131165312)
  protected TextView can_use;
  @ViewInject(2131165327)
  protected TextView charge_type;
  @ViewInject(2131165354)
  protected EditText coin_location;
  @ViewInject(2131165381)
  protected EditText count_et;
  @ViewInject(2131165385)
  protected TextView count_type;
  protected CoinLimit mCoinLimit;
  private double mOutCount = 0.0D;
  protected Wallet mWallet;
  @ViewInject(2131165610)
  protected TextView notice;
  @ViewInject(2131165635)
  protected TextView out_count;
  protected PayCheckView payCheckView;
  @ViewInject(2131165763)
  protected TextView service_count;
  
  @Event({2131165619})
  private void okClicked(View paramView)
  {
    if (this.coin_location.getText().toString().length() == 0)
    {
      showToast("请输入提币地址");
      return;
    }
    if (Double.doubleToLongBits(0.0D) >= Double.doubleToLongBits(this.mOutCount))
    {
      showToast("提币数量不足");
      return;
    }
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString1.length() == 0)
          {
            WithdrawalAct.this.showToast("请输入正确的短信验证码");
            return;
          }
          if (paramAnonymousString2.length() == 0)
          {
            WithdrawalAct.this.showToast("请输入正确的资金密码");
            return;
          }
          WithdrawalAct.this.postOutMsg(paramAnonymousString1, paramAnonymousString2);
          WithdrawalAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked()
        {
          WithdrawalAct.this.postSms();
        }
      });
      this.payCheckView.setFocusable(true);
      this.payCheckView.setPhone(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    }
    this.payCheckView.show();
  }
  
  @Event({2131165681})
  private void qrClicked(View paramView)
  {
    getQrCodePermission();
  }
  
  @Event({2131165772})
  private void showHand(View paramView)
  {
    this.count_et.setText(this.mWallet.getNum());
  }
  
  protected void getCoinLimit(JSONObject paramJSONObject)
  {
    this.mCoinLimit = ((CoinLimit)new Gson().fromJson(paramJSONObject.optString("data"), CoinLimit.class));
    updataLimitMsg();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getQrMsg(String paramString)
  {
    this.coin_location.setText(paramString);
    paramString = this.coin_location;
    paramString.setSelection(paramString.length());
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("提取成功");
      finish();
      break;
    case ???: 
      getCoinLimit(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("WalletDataKey") != null) {
      this.mWallet = ((Wallet)new Gson().fromJson((String)GetIntentData("WalletDataKey"), Wallet.class));
    }
    this.count_type.setText(this.mWallet.getName());
    this.charge_type.setText(this.mWallet.getName());
    TextView localTextView = this.out_count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mOutCount);
    localStringBuilder.append(this.mWallet.getName());
    localTextView.setText(localStringBuilder.toString());
    localTextView = this.can_use;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("可用");
    localStringBuilder.append(this.mWallet.getNum());
    localStringBuilder.append(" ");
    localStringBuilder.append(this.mWallet.getName());
    localTextView.setText(localStringBuilder.toString());
    this.count_et.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        WithdrawalAct.this.updataOut();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
    postCoinLimit();
  }
  
  protected void initView()
  {
    setTitleStr("提币");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    PayCheckView localPayCheckView = this.payCheckView;
    if (localPayCheckView != null) {
      localPayCheckView.cancelTimeCount();
    }
    EventBus.getDefault().unregister(this);
  }
  
  protected void postCoinLimit()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mWallet.getCurrency_id());
    postNet(HttpTypeEnum.CoinLimit_Type, localRequestParams);
  }
  
  protected void postOutMsg(String paramString1, String paramString2)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currencyId", this.mWallet.getCurrency_id());
    localRequestParams.put("addr", this.coin_location.getText());
    localRequestParams.put("amount", this.count_et.getText().toString());
    localRequestParams.put("fundpass", MD5.encode(paramString2));
    localRequestParams.put("code", paramString1);
    postNet(HttpTypeEnum.Setcoinout_Type, localRequestParams);
  }
  
  protected void postSms()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void updataLimitMsg()
  {
    Object localObject1 = this.count_et;
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("最小提币数量");
    ((StringBuilder)localObject2).append(this.mCoinLimit.getCoinoutmin());
    ((EditText)localObject1).setHint(((StringBuilder)localObject2).toString());
    localObject2 = this.service_count;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(this.mCoinLimit.getFeenum());
    ((StringBuilder)localObject1).append("");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    this.notice.setText(this.mCoinLimit.getContent());
    updataOut();
  }
  
  protected void updataOut()
  {
    if ((this.mCoinLimit != null) && (this.count_et.getText().toString().length() != 0))
    {
      try
      {
        double d2 = Double.valueOf(this.count_et.getText().toString()).doubleValue();
        double d1 = d2;
        if (d2 > Double.valueOf(this.mWallet.getNum()).doubleValue())
        {
          this.count_et.setText(this.mWallet.getNum());
          d1 = Double.valueOf(this.mWallet.getNum()).doubleValue();
        }
        this.mOutCount = (d1 - this.mCoinLimit.getFeenum());
        if (0.0D > this.mOutCount) {
          this.mOutCount = 0.0D;
        }
      }
      catch (Exception localException)
      {
        this.mOutCount = 0.0D;
      }
      DecimalFormat localDecimalFormat = new DecimalFormat("0.00000000");
      TextView localTextView = this.out_count;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(localDecimalFormat.format(this.mOutCount));
      localStringBuilder.append(this.mWallet.getName());
      localTextView.setText(localStringBuilder.toString());
      return;
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WithdrawalAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */