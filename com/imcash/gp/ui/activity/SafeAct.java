package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.PersonalInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296385)
public class SafeAct
  extends BaseActivity
{
  public static final String ModifyPassWord = "ModifyPassWord";
  public static final String UserInfoKey = "UserInfoKey";
  @ViewInject(2131165279)
  protected TextView bind_phone;
  protected PersonalInfo mPersonalInfo;
  @ViewInject(2131165561)
  protected TextView mail_state;
  
  @Event({2131165277})
  private void bindMailClicked(View paramView)
  {
    toPage(BindEMailAct.class);
  }
  
  @Event({2131165579})
  private void modifyPass(View paramView)
  {
    toPage(ModifyPassAct.class);
  }
  
  @Event({2131165580})
  private void modifyTracePass(View paramView)
  {
    toPage(ModifyTraceAct.class);
  }
  
  protected void getPsersonalInfo(JSONObject paramJSONObject)
  {
    this.mPersonalInfo = new PersonalInfo(paramJSONObject);
    updataMail();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getPsersonalInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("安全中心");
    Object localObject2 = this.bind_phone;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("已绑定");
    ((StringBuilder)localObject1).append(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    try
    {
      localObject2 = (String)GetIntentData("UserInfoKey");
      localObject1 = new com/imcash/gp/model/PersonalInfo;
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>((String)localObject2);
      ((PersonalInfo)localObject1).<init>(localJSONObject);
      this.mPersonalInfo = ((PersonalInfo)localObject1);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onResume()
  {
    super.onResume();
    postPersonalInfo();
  }
  
  protected void postPersonalInfo()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Personal_Type, localRequestParams);
  }
  
  protected void updataMail()
  {
    Object localObject = this.mPersonalInfo;
    if (localObject == null) {
      return;
    }
    if ((((PersonalInfo)localObject).email != null) && (!"null".equals(this.mPersonalInfo.email)) && (this.mPersonalInfo.email.length() != 0))
    {
      TextView localTextView = this.mail_state;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("已绑定");
      ((StringBuilder)localObject).append(this.mPersonalInfo.email);
      localTextView.setText(((StringBuilder)localObject).toString());
    }
    else
    {
      this.mail_state.setText("未绑定");
    }
  }
  
  protected void updatePage()
  {
    updataMail();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/SafeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */