package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.InsuranceDetail;
import com.imcash.gp.model.Policy;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.adapter.PolicyRecyclerAdapter;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.BuySafeView;
import com.imcash.gp.ui.view.BuySafeViewCall;
import com.imcash.gp.ui.view.PayCheckView;
import com.imcash.gp.ui.view.PayCheckViewCall;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296371)
public class PolicyAct
  extends BaseActivity
{
  protected BuySafeView buySafeView;
  @ViewInject(2131165419)
  protected LinearLayout empty_view;
  protected PolicyRecyclerAdapter mAdapter;
  protected ArrayList<Policy> mDatas;
  protected ArrayList<InsuranceDetail> mInsuranceDetail;
  private LinearLayoutManager mLinearLayoutManager;
  @ViewInject(2131165540)
  protected RecyclerView mListView;
  protected PayCheckView payCheckView;
  @ViewInject(2131165718)
  protected TextView right_tv;
  
  @Event({2131165831})
  private void showBuy(View paramView)
  {
    if (this.mInsuranceDetail.size() == 0)
    {
      showToast("获取数据中");
      postSingleBuy();
      return;
    }
    singleBuy(null);
  }
  
  @Event({2131165718})
  private void singleBuy(View paramView)
  {
    paramView = this.mInsuranceDetail;
    if ((paramView != null) && (paramView.size() != 0))
    {
      this.buySafeView = new BuySafeView(this, this.mInsuranceDetail, new BuySafeViewCall()
      {
        public void okClicked(int paramAnonymousInt)
        {
          if (-1 == paramAnonymousInt)
          {
            PolicyAct.this.showToast("请选择保险内容");
          }
          else
          {
            PolicyAct.this.showPayPass(paramAnonymousInt);
            PolicyAct.this.buySafeView.dismiss();
          }
        }
        
        public void ruleClicked()
        {
          PolicyAct.this.toPdfPage("ImCash保险条款", "IMCASH保险条款.pdf");
        }
      });
      this.buySafeView.showDialog();
      return;
    }
  }
  
  public void buyClicked(int paramInt)
  {
    final Policy localPolicy = (Policy)this.mDatas.get(paramInt);
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString1.length() == 0)
          {
            PolicyAct.this.showToast("请输入正确的短信验证码");
            return;
          }
          if (paramAnonymousString2.length() == 0)
          {
            PolicyAct.this.showToast("请输入正确的资金密码");
            return;
          }
          PolicyAct.this.postBuy(localPolicy, paramAnonymousString1, paramAnonymousString2);
          PolicyAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked()
        {
          PolicyAct.this.postSms();
        }
      });
      this.payCheckView.setFocusable(true);
      this.payCheckView.setPhone(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    }
    this.payCheckView.show();
  }
  
  protected void getPolicyList(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    this.mDatas.clear();
    paramJSONObject = paramJSONObject.optString("data");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    if (this.mDatas.size() == 0)
    {
      this.empty_view.setVisibility(0);
    }
    else
    {
      this.right_tv.setVisibility(0);
      this.empty_view.setVisibility(8);
    }
    if (this.mAdapter == null)
    {
      this.mAdapter = new PolicyRecyclerAdapter(this, this.mDatas);
      this.mListView.setLayoutManager(new LinearLayoutManager(this));
      this.mListView.setAdapter(this.mAdapter);
    }
  }
  
  protected void getSingleBuy(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    this.mDatas.clear();
    paramJSONObject = paramJSONObject.optString("data");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mInsuranceDetail.addAll(paramJSONObject);
      }
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      showToast("购买成功");
      postPolicyList();
      break;
    case ???: 
      getPolicyList(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      stopLoad();
      getSingleBuy(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("保险");
    this.right_tv.setText("购买");
    this.right_tv.setVisibility(8);
    this.mDatas = new ArrayList();
    this.mInsuranceDetail = new ArrayList();
    postPolicyList();
    postSingleBuy();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    PayCheckView localPayCheckView = this.payCheckView;
    if (localPayCheckView != null) {
      localPayCheckView.cancelTimeCount();
    }
  }
  
  protected void postBuy(int paramInt, String paramString)
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("days", ((InsuranceDetail)this.mInsuranceDetail.get(paramInt)).getDays());
    localRequestParams.put("fundpass", MD5.encode(paramString));
    postNet(HttpTypeEnum.BuyInsurance_Type, localRequestParams);
  }
  
  protected void postBuy(Policy paramPolicy, String paramString1, String paramString2)
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("days", paramPolicy.getDays());
    localRequestParams.put("fundpass", MD5.encode(paramString2));
    if (paramPolicy != null) {
      localRequestParams.put("ensure_id", paramPolicy.getId());
    }
    localRequestParams.put("code", paramString1);
    postNet(HttpTypeEnum.BuyInsurance_Type, localRequestParams);
  }
  
  protected void postPolicyList()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Policy_List, localRequestParams);
  }
  
  protected void postSingleBuy()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.InsuranceDetail, localRequestParams);
  }
  
  protected void postSms()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void showPayPass(final int paramInt)
  {
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString2.length() == 0)
          {
            PolicyAct.this.showToast("请输入正确的资金密码");
            return;
          }
          PolicyAct.this.postBuy(paramInt, paramAnonymousString2);
          PolicyAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked()
        {
          PolicyAct.this.postSms();
        }
      });
      this.payCheckView.setFocusable(true);
      this.payCheckView.setPhone(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    }
    this.payCheckView.closeSmsLayout();
    this.payCheckView.show();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/PolicyAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */