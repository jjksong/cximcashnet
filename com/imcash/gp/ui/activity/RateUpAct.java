package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.RateUp;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.adapter.RateUpAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296374)
public class RateUpAct
  extends BaseListActivity
{
  protected RateUpAdapter mAdapter;
  protected ArrayList<RateUp> mDatas;
  
  protected void exchangeCoupon(int paramInt)
  {
    final RateUp localRateUp = (RateUp)this.mDatas.get(paramInt);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("是否要兑换");
    localStringBuilder.append(localRateUp.getTitle());
    showBtnBox(this, localStringBuilder.toString(), null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        RateUpAct.this.postCoupon(localRateUp.getId());
      }
    });
  }
  
  protected void getRateMarket(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    try
    {
      paramJSONObject = paramJSONObject.optString("data");
      this.mDatas.clear();
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
      {
        TypeToken local3 = new com/imcash/gp/ui/activity/RateUpAct$3;
        local3.<init>(this);
        paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, local3.getType());
        this.mDatas.addAll(paramJSONObject);
      }
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      showToast("兑换成功");
      break;
    case ???: 
      getRateMarket(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("积分商城");
    this.mDatas = new ArrayList();
    this.mAdapter = new RateUpAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    postRateMarket();
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        RateUpAct.this.exchangeCoupon(paramAnonymousInt);
      }
    });
  }
  
  protected void initView()
  {
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postCoupon(String paramString)
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("coupon_id", paramString);
    postNet(HttpTypeEnum.GetCoupon_Type, localRequestParams);
  }
  
  protected void postRateMarket()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.RateUp_Market, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RateUpAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */