package com.imcash.gp.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296349)
public class LoginAct
  extends BaseActivity
{
  private View.OnClickListener click = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LoginAct.this.toPdfPage("用户协议", "用户协议.pdf");
    }
  };
  private View.OnClickListener clickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LoginAct.this.toPdfPage("隐私条款", "隐私条款.pdf");
    }
  };
  @ViewInject(2131165415)
  protected TextView document_tv;
  protected boolean mIsRule = false;
  @ViewInject(2131165654)
  protected EditText phone;
  @ViewInject(2131165678)
  protected EditText pwd;
  @ViewInject(2131165752)
  protected ImageView select_but;
  
  @Event({2131165439})
  private void forgetClicked(View paramView)
  {
    toPage(ForgetPwAct.class);
  }
  
  @Event({2131165558})
  private void loginClicked(View paramView)
  {
    if (11 != this.phone.getText().toString().length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    if ((6 <= this.pwd.getText().toString().length()) && (18 >= this.pwd.getText().toString().length()))
    {
      if (!this.mIsRule)
      {
        showToast("需要同意《平台隐私条款》及《用户服务协议》");
        return;
      }
      postLogin();
      return;
    }
    showToast("请输入正确的密码");
  }
  
  private void postLogin()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("username", this.phone.getText().toString());
    localRequestParams.put("password", this.pwd.getText().toString());
    postNet(HttpTypeEnum.Login_Type, localRequestParams);
  }
  
  @Event({2131165704})
  private void registClicked(View paramView)
  {
    toPage(RegistAct.class);
  }
  
  @Event({2131165752})
  private void ruleClicked(View paramView)
  {
    this.mIsRule ^= true;
    updataRuleState();
  }
  
  protected void getLogin(JSONObject paramJSONObject)
  {
    this.mApplication.getDataCore().mOutLogin = 0;
    stopLoad();
    this.mApplication.getDataCore().setUserInfo(paramJSONObject);
    toPage(MainActivity.class);
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getLogin(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if (1 == this.mApplication.getDataCore().mOutLogin)
    {
      showToast("您的账号已在其他设备登录,请重新登录!");
      this.mApplication.getDataCore().mOutLogin = 0;
    }
    SpannableString localSpannableString = new SpannableString("注册/登录即代表同意《平台隐私条款》及《用户服务协议》");
    localSpannableString.setSpan(new Clickable(this.clickListener), 11, 17, 33);
    localSpannableString.setSpan(new Clickable(this.click), 20, 26, 33);
    this.document_tv.setText(localSpannableString);
    this.document_tv.setMovementMethod(LinkMovementMethod.getInstance());
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updataRuleState()
  {
    if (!this.mIsRule) {
      this.select_but.setImageResource(2131099925);
    } else {
      this.select_but.setImageResource(2131099924);
    }
  }
  
  protected void updatePage()
  {
    updataRuleState();
  }
  
  class Clickable
    extends ClickableSpan
  {
    private final View.OnClickListener mListener;
    
    public Clickable(View.OnClickListener paramOnClickListener)
    {
      this.mListener = paramOnClickListener;
    }
    
    public void onClick(View paramView)
    {
      this.mListener.onClick(paramView);
    }
    
    public void updateDrawState(TextPaint paramTextPaint)
    {
      super.updateDrawState(paramTextPaint);
      paramTextPaint.setUnderlineText(false);
      paramTextPaint.setColor(Color.rgb(0, 0, 214));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/LoginAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */