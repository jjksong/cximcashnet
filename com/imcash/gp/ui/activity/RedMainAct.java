package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.RedSearch;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.red.CustomDialog;
import com.imcash.gp.ui.view.red.OnRedPacketDialogClickListener;
import com.imcash.gp.ui.view.red.RedPacketEntity;
import com.imcash.gp.ui.view.red.RedPacketViewHolder;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296377)
public class RedMainAct
  extends BaseActivity
{
  @ViewInject(2131165500)
  protected EditText input_word;
  private CustomDialog mRedPacketDialog;
  private View mRedPacketDialogView;
  private RedPacketViewHolder mRedPacketViewHolder;
  protected RedSearch mRedSearch;
  
  @Event({2131165601})
  private void newUser(View paramView)
  {
    toWebPage("新手红包规则", "http://imcash.art/red_bag.html");
  }
  
  @Event({2131165703})
  private void redPacket(View paramView)
  {
    toPage(RedPacketSendAct.class);
  }
  
  @Event({2131165718})
  private void redRecord(View paramView)
  {
    toPage(RedRecordListAct.class);
  }
  
  @Event({2131165760})
  private void sendNewReg(View paramView)
  {
    postNewRegistRed();
  }
  
  @Event({2131165800})
  private void sureClicked(View paramView)
  {
    if (this.input_word.getText().toString().length() == 0)
    {
      showToast("请输入正确的口令");
      return;
    }
    postGetRed();
  }
  
  @Event({2131165910})
  private void wordRule(View paramView)
  {
    toWebPage("口令红包规则", "http://imcash.art/bag_rule.html");
  }
  
  protected void getMoney(JSONObject paramJSONObject)
  {
    Object localObject = this.mRedPacketDialog;
    if ((localObject != null) && (((CustomDialog)localObject).isShowing())) {
      this.mRedPacketDialog.dismiss();
    }
    localObject = new Intent();
    ((Intent)localObject).putExtra("GetMoney_Key", BaseModel.getDataJson(paramJSONObject).optString("list", ""));
    toPage((Intent)localObject, RedGetMoneyAct.class);
  }
  
  protected void getNetRegistRed(JSONObject paramJSONObject)
  {
    stopLoad();
    JSONObject localJSONObject = BaseModel.getDataJson(paramJSONObject);
    paramJSONObject = new Intent();
    paramJSONObject.putExtra("Key_Wrod", localJSONObject.optString("keyword", ""));
    paramJSONObject.putExtra("Total_Str", localJSONObject.optString("total_amount", ""));
    paramJSONObject.putExtra("Qr_URL", localJSONObject.optString("url", ""));
    paramJSONObject.putExtra("Bottom_Msg", localJSONObject.optString("content", ""));
    paramJSONObject.putExtra("IsNewUser", "imcash");
    toPage(paramJSONObject, RedShareAct.class);
  }
  
  protected void getRed(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mRedSearch = ((RedSearch)new Gson().fromJson(paramJSONObject.optString("data"), RedSearch.class));
    showRedPacketDialog(new RedPacketEntity(this.mRedSearch.getPhone(), "", this.mRedSearch.getRemark()));
  }
  
  protected void getRedMoney(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject.optInt("is_send", 0);
      return;
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getNetRegistRed(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRedMoney(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getMoney(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRed(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData() {}
  
  protected void initView()
  {
    setTitleStr("领取红包");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onResume()
  {
    super.onResume();
  }
  
  protected void postGetRed()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("keyword", this.input_word.getText().toString());
    postNet(HttpTypeEnum.Red_Search, localRequestParams);
  }
  
  protected void postNewRegistRed()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_CreatePacket, localRequestParams);
  }
  
  protected void postRedGetMoney()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_NewIsCreate, localRequestParams);
  }
  
  public void showRedPacketDialog(RedPacketEntity paramRedPacketEntity)
  {
    if (this.mRedPacketDialogView == null)
    {
      this.mRedPacketDialogView = View.inflate(this, 2131296428, null);
      this.mRedPacketViewHolder = new RedPacketViewHolder(this, this.mRedPacketDialogView);
      this.mRedPacketDialog = new CustomDialog(this, this.mRedPacketDialogView, 2131624311);
      this.mRedPacketDialog.setCancelable(false);
    }
    this.mRedPacketViewHolder.setData(paramRedPacketEntity);
    this.mRedPacketViewHolder.setOnRedPacketDialogClickListener(new OnRedPacketDialogClickListener()
    {
      public void onCloseClick()
      {
        RedMainAct.this.mRedPacketDialog.dismiss();
      }
      
      public void onOpenClick()
      {
        RedMainAct.this.waitToContinue();
      }
    });
    this.mRedPacketDialog.show();
  }
  
  protected void updatePage() {}
  
  protected void waitToContinue()
  {
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        RequestParams localRequestParams = RedMainAct.this.getIdAndKeyNumParams();
        localRequestParams.put("bag_id", RedMainAct.this.mRedSearch.getBag_id());
        RedMainAct.this.postNet(HttpTypeEnum.Red_GetMoney, localRequestParams);
      }
    }, 2500L);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedMainAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */