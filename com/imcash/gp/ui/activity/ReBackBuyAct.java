package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.QuotaInfo;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.math.BigDecimal;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296302)
public class ReBackBuyAct
  extends BaseActivity
{
  public static final String QuotaInfo_Key = "QuotaInfo_Key";
  protected String allMin;
  @ViewInject(2131165247)
  protected EditText back_count;
  @ViewInject(2131165251)
  protected TextView balance_tv;
  @ViewInject(2131165271)
  protected TextView bfc_balance;
  @ViewInject(2131165472)
  protected TextView hour_tv;
  protected QuotaInfo mQuotaInfo;
  protected TimeCount mTimeCount;
  @ViewInject(2131165576)
  protected TextView minute_tv;
  @ViewInject(2131165679)
  protected EditText pwd_et;
  @ViewInject(2131165687)
  protected TextView quota_tv;
  @ViewInject(2131165715)
  protected ImageView right_iv;
  @ViewInject(2131165749)
  protected TextView second_tv;
  
  @Event({2131165233})
  private void allInClicked(View paramView)
  {
    EditText localEditText = this.back_count;
    paramView = new StringBuilder();
    paramView.append(this.allMin);
    paramView.append("");
    localEditText.setText(paramView.toString());
  }
  
  @Event({2131165310})
  private void buyClicked(View paramView)
  {
    if (this.back_count.getText().toString().length() == 0)
    {
      showToast("请输入需要回购的额度");
      return;
    }
    if (this.pwd_et.getText().toString().length() == 0)
    {
      showToast("请输入交易密码");
      return;
    }
    showBtnBox(this, "确定要回购吗？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        ReBackBuyAct.this.postBuy();
      }
    });
  }
  
  @Event({2131165715})
  private void rightClicked(View paramView)
  {
    toPage(RebackRecordAct.class);
  }
  
  protected double getMin(double paramDouble1, double paramDouble2)
  {
    double d = paramDouble1;
    if (new BigDecimal(paramDouble1).compareTo(new BigDecimal(paramDouble2)) > 0) {
      d = paramDouble2;
    }
    return d;
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      toPage(RebackBuyResult.class);
      finish();
    }
  }
  
  protected void initData()
  {
    setTitleStr("回购");
    this.right_iv.setImageResource(2131099917);
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>((String)GetIntentData("QuotaInfo_Key"));
      Gson localGson = new com/google/gson/Gson;
      localGson.<init>();
      this.mQuotaInfo = ((QuotaInfo)localGson.fromJson(localJSONObject.optString("data"), QuotaInfo.class));
      updataUi();
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postBuy()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("amount", this.back_count.getText().toString());
    localRequestParams.put("trade_pass", MD5.encode(this.pwd_et.getText().toString()));
    postNet(HttpTypeEnum.Bfc_BackBuy, localRequestParams);
  }
  
  protected void updataUi()
  {
    if (this.mQuotaInfo == null) {
      return;
    }
    Object localObject1 = this.bfc_balance;
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("当日平台剩余回购额度：");
    ((StringBuilder)localObject2).append(this.mQuotaInfo.getSurplus());
    ((StringBuilder)localObject2).append("BFC");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject1 = this.balance_tv;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("当前持仓：");
    ((StringBuilder)localObject2).append(this.mQuotaInfo.getBalance());
    ((StringBuilder)localObject2).append("BFC");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject2 = this.quota_tv;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("待回购额度：");
    ((StringBuilder)localObject1).append(this.mQuotaInfo.getQuota());
    ((StringBuilder)localObject1).append("BFC");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    double d = getMin(getMin(Double.valueOf(this.mQuotaInfo.getSurplus()).doubleValue(), Double.valueOf(this.mQuotaInfo.getBalance()).doubleValue()), Double.valueOf(this.mQuotaInfo.getQuota()).doubleValue());
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(d);
    ((StringBuilder)localObject1).append("");
    this.allMin = ((StringBuilder)localObject1).toString();
    this.mTimeCount = new TimeCount(this.mQuotaInfo.getCountdown() * 1000, 1000L);
    this.mTimeCount.start();
  }
  
  protected void updatePage() {}
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      ReBackBuyAct.this.toPage(RebackOutTimeAct.class);
      ReBackBuyAct.this.finish();
    }
    
    public void onTick(long paramLong)
    {
      int i = (int)paramLong / 1000;
      int j = i / 3600;
      Object localObject1;
      if (j >= 10)
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(j);
        ((StringBuilder)localObject1).append("");
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      else
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("0");
        ((StringBuilder)localObject1).append("");
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      j = i % 3600 / 60;
      Object localObject2;
      if (j >= 10)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append(j);
        ((StringBuilder)localObject2).append("");
        localObject2 = ((StringBuilder)localObject2).toString();
      }
      else
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("0");
        ((StringBuilder)localObject2).append(j);
        localObject2 = ((StringBuilder)localObject2).toString();
      }
      i %= 60;
      Object localObject3;
      if (i >= 10)
      {
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("");
        ((StringBuilder)localObject3).append(i);
        localObject3 = ((StringBuilder)localObject3).toString();
      }
      else
      {
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("0");
        ((StringBuilder)localObject3).append(i);
        localObject3 = ((StringBuilder)localObject3).toString();
      }
      ReBackBuyAct.this.hour_tv.setText((CharSequence)localObject1);
      ReBackBuyAct.this.minute_tv.setText((CharSequence)localObject2);
      ReBackBuyAct.this.second_tv.setText((CharSequence)localObject3);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ReBackBuyAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */