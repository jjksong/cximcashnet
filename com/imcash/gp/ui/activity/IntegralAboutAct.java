package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.model.IntegralAboutList;
import com.imcash.gp.ui.adapter.IntegralAboutAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296289)
public class IntegralAboutAct
  extends BaseListActivity
{
  protected IntegralAboutAdapter mAdapter;
  protected IntegralAboutList mDatas;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    this.mAdapter = new IntegralAboutAdapter(this, null);
    this.mListView.setAdapter(this.mAdapter);
  }
  
  protected void initView()
  {
    setTitleStr("积分说明");
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/IntegralAboutAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */