package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.PDFView.Configurator;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296369)
public class PdfAct
  extends BaseActivity
{
  public static final String PdfNameKey = "PdfNameKey";
  public static final String PdfTitleKey = "PdfTitleKey";
  @ViewInject(2131165649)
  protected PDFView pdfView;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr((String)GetIntentData("PdfTitleKey"));
    if ((String)GetIntentData("PdfNameKey") != null) {
      this.pdfView.fromAsset((String)GetIntentData("PdfNameKey")).enableSwipe(true).swipeHorizontal(false).enableDoubletap(true).defaultPage(0).autoSpacing(false).load();
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/PdfAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */