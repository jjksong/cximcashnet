package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BBExchange;
import com.imcash.gp.model.BBExchange.CoinBean;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296294)
public class BBExchangeAct
  extends BaseActivity
{
  @ViewInject(2131165393)
  protected TextView current_rate;
  @ViewInject(2131165499)
  protected EditText input_count;
  protected BBExchange mDatas;
  protected int mIndex = 1;
  protected String mSourceCoin = "BTC";
  protected String mTargetCoin = "USDT";
  @ViewInject(2131165777)
  protected ImageView source_coin_img;
  @ViewInject(2131165778)
  protected TextView source_coin_name;
  @ViewInject(2131165780)
  protected TextView source_money;
  @ViewInject(2131165781)
  protected TextView source_num;
  @ViewInject(2131165782)
  protected TextView source_tv;
  @ViewInject(2131165807)
  protected ImageView target_img;
  @ViewInject(2131165809)
  protected TextView target_name;
  @ViewInject(2131165810)
  protected TextView target_tv;
  
  @Event({2131165233})
  private void allIn(View paramView)
  {
    paramView = this.mDatas;
    if (paramView == null) {
      return;
    }
    paramView = paramView.getCoinBeanByName(this.mSourceCoin);
    if (Double.doubleToLongBits(0.0D) >= Double.doubleToLongBits(Double.valueOf(paramView.getNum()).doubleValue())) {
      this.input_count.setText("");
    } else {
      this.input_count.setText(paramView.getNum());
    }
  }
  
  @Event({2131165617})
  private void okClicked(View paramView)
  {
    if (this.mDatas == null)
    {
      showToast("加载数据");
      return;
    }
    if (this.input_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的金额");
      return;
    }
    postChange();
  }
  
  @Event({2131165779})
  private void sourceClicked(View paramView)
  {
    if (this.mDatas == null) {
      return;
    }
    this.mIndex = 1;
    paramView = new Intent();
    paramView.putExtra("BBCoinKey", new Gson().toJson(this.mDatas));
    toPage(paramView, BBChooseCoinAct.class);
  }
  
  @Event({2131165808})
  private void targetClicked(View paramView)
  {
    if (this.mDatas == null) {
      return;
    }
    this.mIndex = 2;
    paramView = new Intent();
    paramView.putExtra("BBCoinKey", new Gson().toJson(this.mDatas));
    toPage(paramView, BBChooseCoinAct.class);
  }
  
  protected void getExchange(JSONObject paramJSONObject)
  {
    this.mDatas = ((BBExchange)new Gson().fromJson(paramJSONObject.optString("data"), BBExchange.class));
    updataCoin();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      postExchange();
      showToast("兑换成功");
      break;
    case ???: 
      getExchange(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    ImageView localImageView = (ImageView)findViewById(2131165715);
    localImageView.setImageResource(2131099945);
    localImageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BBExchangeAct.this.toPage(NewOtcAct.class);
      }
    });
    postExchange();
    this.input_count.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        BBExchangeAct.this.updataOut();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView()
  {
    setTitleStr("币币兑换");
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void modifyCoin(String paramString)
  {
    int i = this.mIndex;
    if (1 == i)
    {
      this.mSourceCoin = paramString;
      if (!"USDT".equals(this.mSourceCoin)) {
        this.mTargetCoin = "USDT";
      }
    }
    else if (2 == i)
    {
      this.mTargetCoin = paramString;
      if (!"USDT".equals(this.mTargetCoin)) {
        this.mSourceCoin = "USDT";
      }
    }
    this.input_count.setText("");
    updataCoin();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  protected void postChange()
  {
    if (this.mDatas == null) {
      return;
    }
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("from_id", this.mDatas.getIdByName(this.mSourceCoin));
    localRequestParams.put("to_id", this.mDatas.getIdByName(this.mTargetCoin));
    localRequestParams.put("amount", this.input_count.getText().toString());
    postNet(HttpTypeEnum.Clicked_Trade, localRequestParams);
  }
  
  protected void postExchange()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.BBExchange, localRequestParams);
  }
  
  protected void updataCoin()
  {
    if (this.mDatas == null) {
      return;
    }
    Object localObject2 = this.current_rate;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("当前汇率:1:");
    ((StringBuilder)localObject1).append(this.mDatas.getRate());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = this.mDatas.getCoinBeanByName(this.mSourceCoin);
    this.source_coin_img.setImageResource(GlobalFunction.getCoinImgByName(((BBExchange.CoinBean)localObject1).getName()));
    this.source_coin_name.setText(((BBExchange.CoinBean)localObject1).getName());
    localObject2 = this.source_num;
    Object localObject3 = new StringBuilder();
    ((StringBuilder)localObject3).append("可用余额:");
    ((StringBuilder)localObject3).append(((BBExchange.CoinBean)localObject1).getNum());
    ((StringBuilder)localObject3).append(((BBExchange.CoinBean)localObject1).getName());
    ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
    localObject3 = this.source_money;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(((BBExchange.CoinBean)localObject1).getName());
    ((StringBuilder)localObject2).append("当前汇率:");
    ((StringBuilder)localObject2).append(((BBExchange.CoinBean)localObject1).getPrice());
    ((StringBuilder)localObject2).append("USDT");
    ((TextView)localObject3).setText(((StringBuilder)localObject2).toString());
    this.target_img.setImageResource(GlobalFunction.getCoinImgByName(this.mTargetCoin));
    this.target_name.setText(this.mTargetCoin);
    if (this.mTargetCoin.equals("USDT"))
    {
      localObject2 = this.source_tv;
      localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append("1");
      ((StringBuilder)localObject3).append(this.mSourceCoin);
      ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      localObject3 = this.target_tv;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(((BBExchange.CoinBean)localObject1).getPrice());
      ((StringBuilder)localObject2).append("USDT");
      ((TextView)localObject3).setText(((StringBuilder)localObject2).toString());
    }
    if (this.mSourceCoin.equals("USDT"))
    {
      localObject1 = this.mDatas.getCoinBeanByName(this.mTargetCoin);
      localObject2 = this.source_tv;
      localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append(((BBExchange.CoinBean)localObject1).getPrice());
      ((StringBuilder)localObject3).append("USDT");
      ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      localObject2 = this.target_tv;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("1");
      ((StringBuilder)localObject1).append(this.mTargetCoin);
      ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    }
  }
  
  protected void updataOut()
  {
    if ((this.mDatas != null) && (this.input_count.getText().toString().length() != 0))
    {
      try
      {
        BBExchange.CoinBean localCoinBean = this.mDatas.getCoinBeanByName(this.mSourceCoin);
        if (Double.valueOf(this.input_count.getText().toString()).doubleValue() > Double.valueOf(localCoinBean.getNum()).doubleValue()) {
          this.input_count.setText(localCoinBean.getNum());
        }
        if (0.0D > Double.valueOf(localCoinBean.getNum()).doubleValue()) {
          this.input_count.setText("");
        }
      }
      catch (Exception localException)
      {
        this.input_count.setText("");
      }
      return;
    }
  }
  
  protected void updatePage()
  {
    updataCoin();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BBExchangeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */