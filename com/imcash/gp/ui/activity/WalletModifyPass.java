package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296407)
public class WalletModifyPass
  extends BaseActivity
{
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("修改密码");
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WalletModifyPass.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */