package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.QuotaInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296309)
public class RebackOutTimeAct
  extends BaseActivity
{
  public static final String Result_Msg_Key = "Result_Msg_Key";
  @ViewInject(2131165711)
  protected TextView resutl_msg;
  @ViewInject(2131165715)
  protected ImageView right_iv;
  
  @Event({2131165246})
  private void backBtn(View paramView)
  {
    finish();
  }
  
  @Event({2131165330})
  private void check_countClicked(View paramView)
  {
    postBuyInfo();
  }
  
  @Event({2131165715})
  private void rightClicked(View paramView)
  {
    toPage(RebackRecordAct.class);
  }
  
  protected void getBuyInfo(JSONObject paramJSONObject)
  {
    paramJSONObject = (QuotaInfo)new Gson().fromJson(paramJSONObject.optString("data"), QuotaInfo.class);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前可回购额度为：");
    localStringBuilder.append(paramJSONObject.getQuota());
    localStringBuilder.append("BFC");
    showMessageBox(this, localStringBuilder.toString(), null);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getBuyInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("回购");
    this.right_iv.setImageResource(2131099917);
    if (GetIntentData("Result_Msg_Key") != null) {
      this.resutl_msg.setText((String)GetIntentData("Result_Msg_Key"));
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postBuyInfo()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_BuyBackQuotaBySystem, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RebackOutTimeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */