package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.RedDetail;
import com.imcash.gp.model.RedRecord;
import com.imcash.gp.ui.adapter.RedRecordDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296379)
public class RedRecordDetailAct
  extends BaseListActivity
{
  public static final String RedRecord_Key = "RedRecord_Key";
  @ViewInject(2131165358)
  protected TextView coint_type;
  protected RedRecordDetailAdapter mAdapter;
  protected ArrayList<RedDetail> mDatas;
  protected RedRecord mRedRecord;
  @ViewInject(2131165661)
  protected TextView process_tv;
  
  protected void getDetail(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
  }
  
  protected void getReShare(JSONObject paramJSONObject)
  {
    stopLoad();
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    Intent localIntent = new Intent();
    localIntent.putExtra("Key_Wrod", paramJSONObject.optString("keyword", ""));
    localIntent.putExtra("Total_Str", paramJSONObject.optString("total_amount", ""));
    localIntent.putExtra("Qr_URL", paramJSONObject.optString("url", ""));
    localIntent.putExtra("Bottom_Msg", paramJSONObject.optString("content", ""));
    if (1 == paramJSONObject.optInt("mold", 0)) {
      localIntent.putExtra("IsNewUser", "imcash");
    }
    toPage(localIntent, RedShareAct.class);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getReShare(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getDetail(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    postDetail();
  }
  
  protected void initView()
  {
    setTitleStr("红包详情");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("分享");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        RedRecordDetailAct.this.postReShare();
      }
    });
    initListView();
    closeRefreshAll();
    this.mDatas = new ArrayList();
    this.mAdapter = new RedRecordDetailAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    if ((String)GetIntentData("RedRecord_Key") != null)
    {
      this.mRedRecord = ((RedRecord)new Gson().fromJson((String)GetIntentData("RedRecord_Key"), RedRecord.class));
      this.coint_type.setText(this.mRedRecord.getCoinStr());
      localTextView = this.process_tv;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("已领取");
      localStringBuilder.append(this.mRedRecord.getNum());
      localTextView.setText(localStringBuilder.toString());
    }
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postDetail()
  {
    if (this.mRedRecord == null) {
      return;
    }
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("bag_id", this.mRedRecord.getBag_id());
    postNet(HttpTypeEnum.Red_Detail, localRequestParams);
  }
  
  protected void postReShare()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("bag_id", this.mRedRecord.getBag_id());
    postNet(HttpTypeEnum.Red_Re_Share, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedRecordDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */