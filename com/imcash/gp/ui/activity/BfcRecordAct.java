package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BfcTokenRecord;
import com.imcash.gp.ui.adapter.BfcRecordAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296313)
public class BfcRecordAct
  extends BaseListActivity
{
  protected BfcRecordAdapter mAdapter;
  protected ArrayList<BfcTokenRecord> mDatas;
  protected int mPageIndex = 1;
  
  protected void getRecord(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    if (HttpTypeEnum.BFC_Record_Down == paramHttpTypeEnum)
    {
      stopDownRefresh();
      this.mDatas.clear();
    }
    else
    {
      stopUpRefresh();
    }
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramHttpTypeEnum = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramHttpTypeEnum != null) && (paramHttpTypeEnum.size() > 0))
      {
        this.mDatas.addAll(paramHttpTypeEnum);
        changeRefreshState(paramHttpTypeEnum.size(), 15);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getRecord(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new BfcRecordAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    setTitleStr("通证记录");
    initListView();
    closeUpState();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPageIndex = 1;
    postRecord(HttpTypeEnum.BFC_Record_Down);
  }
  
  protected void onRefreshUp()
  {
    this.mPageIndex += 1;
    postRecord(HttpTypeEnum.BFC_Record_Up);
  }
  
  protected void postRecord(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPageIndex);
    localRequestParams.put("page_num", 15);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcRecordAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */