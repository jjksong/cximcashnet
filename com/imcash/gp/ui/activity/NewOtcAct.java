package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.OtcInfo;
import com.imcash.gp.tools.ArithUtil;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296360)
public class NewOtcAct
  extends BaseActivity
{
  @ViewInject(2131165382)
  protected EditText count_input;
  @ViewInject(2131165426)
  protected TextView exchange_ucount;
  @ViewInject(2131165523)
  protected TextView label_tv;
  protected OtcInfo mOtcInfo;
  protected double mRate;
  protected double showCount;
  @ViewInject(2131165883)
  protected TextView usdt_percent;
  
  @Event({2131165632})
  private void otcTeach(View paramView)
  {
    toWebPage("一分钟学会出入金", "http://imcash.art/otc.html");
  }
  
  @Event({2131165637})
  private void outMoneyClicked(View paramView)
  {
    toPage(NewOtcOutAct.class);
  }
  
  @Event({2131165832})
  private void toPayClicked(View paramView)
  {
    if (this.count_input.getText().toString().length() == 0)
    {
      showToast("请输入兑换金额");
      return;
    }
    postInputMsg();
  }
  
  protected void exchangeCountLogic()
  {
    if (this.count_input.getText().toString().length() == 0)
    {
      this.exchange_ucount.setText("0USDT");
      return;
    }
    if (0.0D == this.mRate) {
      return;
    }
    this.showCount = ArithUtil.div(Double.valueOf(this.count_input.getText().toString()).doubleValue(), this.mRate);
    TextView localTextView = this.exchange_ucount;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(GlobalFunction.doubleToStringEight(this.showCount));
    localStringBuilder.append("USDT");
    localTextView.setText(localStringBuilder.toString());
  }
  
  protected void getInputMsg(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mOtcInfo = ((OtcInfo)new Gson().fromJson(paramJSONObject.optString("data", ""), OtcInfo.class));
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("appkey", this.mOtcInfo.getAppkey());
    localRequestParams.put("async_url", this.mOtcInfo.getAsync_url());
    localRequestParams.put("coin_amount", this.mOtcInfo.getCoin_amount());
    localRequestParams.put("coin_sign", this.mOtcInfo.getCoin_sign());
    localRequestParams.put("company_order_num", this.mOtcInfo.getCompany_order_num());
    localRequestParams.put("kyc", this.mOtcInfo.getKyc());
    localRequestParams.put("order_time", this.mOtcInfo.getOrder_time());
    localRequestParams.put("phone", this.mOtcInfo.getPhone());
    localRequestParams.put("sync_url", this.mOtcInfo.getSync_url());
    localRequestParams.put("username", this.mOtcInfo.getUsername());
    localRequestParams.put("sign", this.mOtcInfo.getSign());
    paramJSONObject = new Intent();
    paramJSONObject.putExtra("Post_Type", 0);
    paramJSONObject.putExtra("title_key", "支付订单");
    paramJSONObject.putExtra("Post_Data", localRequestParams.toString());
    toPage(paramJSONObject, OtcWebAct.class);
  }
  
  protected void getLableInfo(JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = BaseModel.getDataJson(paramJSONObject);
    this.mRate = localJSONObject.optDouble("buyPrice", 0.0D);
    paramJSONObject = this.usdt_percent;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("1USDT≈￥");
    localStringBuilder.append(this.mRate);
    paramJSONObject.setText(localStringBuilder.toString());
    this.label_tv.setText(localJSONObject.optString("enter_label", ""));
    stopLoad();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getInputMsg(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getLableInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("OTC入金");
    postLableInfo();
    this.count_input.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        NewOtcAct.this.exchangeCountLogic();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView()
  {
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("OTC订单");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        NewOtcAct.this.toPage(OtcRecordAct.class);
      }
    });
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postInputMsg()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("amount", this.count_input.getText().toString());
    postNet(HttpTypeEnum.Merchant_Enter, localRequestParams);
  }
  
  protected void postLableInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Merchant_Label, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/NewOtcAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */