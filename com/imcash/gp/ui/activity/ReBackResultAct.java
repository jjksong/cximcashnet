package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296305)
public class ReBackResultAct
  extends BaseActivity
{
  public static final String Result_Status_Key = "Result_Status_Key";
  @ViewInject(2131165709)
  protected ImageView result_img;
  @ViewInject(2131165710)
  protected TextView result_title;
  @ViewInject(2131165711)
  protected TextView resutl_msg;
  
  @Event({2131165246})
  private void backClicked(View paramView)
  {
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    if ("success".equals((String)GetIntentData("Result_Status_Key")))
    {
      setTitleStr("报名成功");
      this.result_img.setImageResource(2131099849);
    }
    else
    {
      setTitleStr("报名失败");
      this.result_title.setText("锁仓失败");
      this.resutl_msg.setText("请确认持仓后再次提交申请");
      this.result_img.setImageResource(2131099848);
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ReBackResultAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */