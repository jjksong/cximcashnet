package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.TradeInfo;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.PayCheckView;
import com.imcash.gp.ui.view.PayCheckViewCall;
import com.loopj.android.http.RequestParams;
import org.greenrobot.eventbus.EventBus;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296393)
public class TradeBuyAct
  extends BaseActivity
{
  public static final String TradeBuyKey = "TradeBuyKey";
  @ViewInject(2131165357)
  protected TextView coin_type;
  @ViewInject(2131165431)
  protected TextView fee_money;
  @ViewInject(2131165442)
  protected TextView fund_money;
  @ViewInject(2131165443)
  protected TextView fund_name;
  @ViewInject(2131165526)
  protected TextView last_time;
  protected String mTracePass = "";
  protected TradeInfo mTradeInfo;
  @ViewInject(2131165626)
  protected TextView order_money;
  @ViewInject(2131165627)
  protected TextView order_no;
  @ViewInject(2131165628)
  protected TextView order_time;
  protected PayCheckView payCheckView;
  @ViewInject(2131165693)
  protected TextView rate_num;
  @ViewInject(2131165846)
  protected TextView total_money;
  @ViewInject(2131165886)
  protected TextView user_name;
  
  @Event({2131165455})
  private void gotoPay(View paramView)
  {
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString2.length() == 0)
          {
            TradeBuyAct.this.showToast("请输入正确的资金密码");
            return;
          }
          paramAnonymousString1 = TradeBuyAct.this;
          paramAnonymousString1.mTracePass = paramAnonymousString2;
          paramAnonymousString1.postOk();
          TradeBuyAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked() {}
      });
      this.payCheckView.setFocusable(true);
    }
    this.payCheckView.closeSmsLayout();
    this.payCheckView.show();
  }
  
  protected void getOkState()
  {
    stopLoad();
    EventBus.getDefault().post("购买成功");
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getOkState();
    }
  }
  
  protected void initData()
  {
    setTitleStr("等待付款");
    try
    {
      String str = (String)GetIntentData("TradeBuyKey");
      Gson localGson = new com/google/gson/Gson;
      localGson.<init>();
      this.mTradeInfo = ((TradeInfo)localGson.fromJson(str, TradeInfo.class));
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postOk()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("trade_pass", MD5.encode(this.mTracePass));
    localRequestParams.put("trade_id", this.mTradeInfo.getTrade_id());
    postNet(HttpTypeEnum.TradeBuy, localRequestParams);
  }
  
  protected void updatePage()
  {
    Object localObject1 = this.mTradeInfo;
    if (localObject1 == null) {
      return;
    }
    this.user_name.setText(((TradeInfo)localObject1).getUsername());
    Object localObject2 = this.order_money;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(this.mTradeInfo.getAmount());
    ((StringBuilder)localObject1).append(this.mTradeInfo.getCurrency_name());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    this.order_no.setText(this.mTradeInfo.getTrade_no());
    this.order_time.setText(this.mTradeInfo.getCreate_time());
    localObject2 = this.fee_money;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(this.mTradeInfo.getFee());
    ((StringBuilder)localObject1).append(this.mTradeInfo.getCurrency_name());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = this.total_money;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(this.mTradeInfo.getTotal_amount());
    ((StringBuilder)localObject2).append(this.mTradeInfo.getCurrency_name());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    this.fund_name.setText(this.mTradeInfo.getName());
    this.coin_type.setText(this.mTradeInfo.getCurrency_name());
    localObject2 = this.fund_money;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(this.mTradeInfo.getNum());
    ((StringBuilder)localObject1).append(this.mTradeInfo.getCurrency_name());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    this.rate_num.setText(this.mTradeInfo.getRate());
    this.last_time.setText(this.mTradeInfo.getDue_date());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeBuyAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */