package com.imcash.gp.ui.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;
import com.imcash.gp.http.HttpClient;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.third.faceid.GenerateSign;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.megvii.meglive_sdk.sdk.listener.FaceIdDetectListener;
import com.megvii.meglive_sdk.sdk.listener.FaceIdInitListener;
import com.megvii.meglive_sdk.sdk.manager.FaceIdManager;
import cz.msebera.android.httpclient.Header;
import java.util.List;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296357)
public class ModifyTraceAct
  extends BaseActivity
{
  private String bizToken;
  private String card_id;
  private int comparisonType;
  private Handler handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      ModifyTraceAct.this.requestCameraPerm();
    }
  };
  private String livenessType;
  private FaceIdDetectListener mDetectListener = new FaceIdDetectListener()
  {
    public void onFailed(int paramAnonymousInt, String paramAnonymousString) {}
    
    public void onSuccess(int paramAnonymousInt, String paramAnonymousString)
    {
      if (51000 == paramAnonymousInt) {
        ModifyTraceAct.this.postModifyInfo();
      } else {
        ModifyTraceAct.this.showToast("认证失败");
      }
    }
  };
  private FaceIdInitListener mInitListener = new FaceIdInitListener()
  {
    public void onFailed(int paramAnonymousInt, String paramAnonymousString)
    {
      Log.v("aaa", "");
    }
    
    public void onSuccess()
    {
      ModifyTraceAct.this.stopLoad();
      FaceIdManager.getInstance(ModifyTraceAct.this).startDetect();
    }
  };
  private String mName;
  private String mNum;
  private String mSign;
  private String mSignVersion;
  @ViewInject(2131165600)
  protected EditText new_pass;
  private String user_name;
  
  private void enterNextPage()
  {
    FaceIdManager.getInstance(this).setLanguage(this, "zh");
    FaceIdManager.getInstance(this).setHost(this, "https://openapi.faceid.com");
    FaceIdManager.getInstance(this).init(this.bizToken);
  }
  
  @Event({2131165275})
  private void modifyClicked(View paramView)
  {
    if (this.new_pass.getText().toString().length() == 0)
    {
      showToast("请输入新资金密码");
      return;
    }
    postCheck();
  }
  
  private void requestCameraPerm()
  {
    requestExternal();
  }
  
  private void requestExternal()
  {
    startLoad();
    XXPermissions.with(this).constantRequest().permission(new String[] { "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" }).request(new OnPermission()
    {
      public void hasPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        ModifyTraceAct.this.enterNextPage();
      }
      
      public void noPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        BaseActivity.showBtnBox(ModifyTraceAct.this, "权限获取失败，该功能暂时无法使用，是否要开启权限？", null, new Btn2Dialog.OnBtnListener()
        {
          public void onClick()
          {
            XXPermissions.gotoPermissionSettings(ModifyTraceAct.this);
          }
        });
      }
    });
  }
  
  protected void getBizToken()
  {
    final RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("sign", this.mSign);
    localRequestParams.put("sign_version", this.mSignVersion);
    localRequestParams.put("liveness_type", this.livenessType);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("");
    localStringBuilder.append(this.comparisonType);
    localRequestParams.put("comparison_type", localStringBuilder.toString());
    localRequestParams.put("idcard_name", this.mName);
    localRequestParams.put("idcard_number", this.mNum);
    localRequestParams.put("verbose", "1");
    HttpClient.post(HttpTypeEnum.FaceBizToken, localRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable) {}
      
      public void onFinish() {}
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt)
        {
          ModifyTraceAct.access$302(ModifyTraceAct.this, paramAnonymousJSONObject.optString("biz_token"));
          paramAnonymousArrayOfHeader = new Message();
          ModifyTraceAct.this.handler.sendMessage(paramAnonymousArrayOfHeader);
        }
      }
    });
  }
  
  protected void getCheck(JSONObject paramJSONObject)
  {
    stopLoad();
    paramJSONObject = paramJSONObject.optJSONObject("data");
    this.user_name = paramJSONObject.optString("username", "");
    this.card_id = paramJSONObject.optString("card", "");
    verifyBegin();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("修改成功");
      finish();
      break;
    case ???: 
      getCheck(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData() {}
  
  protected void initView()
  {
    setTitleStr("修改资金密码");
    FaceIdManager.getInstance(this).setFaceIdInitListener(this.mInitListener);
    FaceIdManager.getInstance(this).setFaceIdDetectListener(this.mDetectListener);
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onResume()
  {
    super.onResume();
    stopLoad();
  }
  
  protected void postCheck()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.CanModifyTrace, localRequestParams);
  }
  
  protected void postModifyInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("password", MD5.encode(this.new_pass.getText().toString()));
    localRequestParams.put("sign", this.mSign);
    localRequestParams.put("biz_token", this.bizToken);
    postNet(HttpTypeEnum.ModifyTrace, localRequestParams);
  }
  
  protected void updatePage() {}
  
  protected void verifyBegin()
  {
    startLoad();
    long l2 = System.currentTimeMillis() / 1000L;
    long l1 = (System.currentTimeMillis() + 360000L) / 1000L;
    this.mSign = GenerateSign.appSign(getResources().getString(2131558463), getResources().getString(2131558464), l2, l1).replaceAll("[\\s*\t\n\r]", "");
    this.mSignVersion = "hmac_sha1";
    this.mName = this.user_name;
    this.mNum = this.card_id;
    this.livenessType = "meglive";
    this.comparisonType = 1;
    getBizToken();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ModifyTraceAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */