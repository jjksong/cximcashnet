package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.RateUpCard;
import com.imcash.gp.ui.adapter.FinancingCardAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296332)
public class FinancingCardAct
  extends BaseListActivity
{
  public static final String ProjectIdKey = "ProjectIdKey";
  public static final String TouziIdKey = "TouziIdKey";
  protected FinancingCardAdapter mAdapter;
  protected ArrayList<RateUpCard> mDatas;
  @ViewInject(2131165419)
  protected LinearLayout mEmptyView;
  protected String mProjectId;
  protected String mTouziId;
  
  @Event({2131165831})
  private void toMainPage(View paramView)
  {
    this.mApplication.getDataCore().mTargetTag = "main_fragment";
    toPage(MainActivity.class);
    finish();
  }
  
  protected void getCardList(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    if (this.mDatas.size() == 0) {
      this.mEmptyView.setVisibility(0);
    } else {
      this.mEmptyView.setVisibility(8);
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected boolean haveSelectRate()
  {
    Object localObject = this.mDatas;
    boolean bool = false;
    if (localObject == null) {
      return false;
    }
    localObject = ((ArrayList)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      if (((RateUpCard)((Iterator)localObject).next()).mIsSelected) {
        bool = true;
      }
    }
    return bool;
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("加息成功");
      finish();
      break;
    case ???: 
      getCardList(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mProjectId = ((String)GetIntentData("ProjectIdKey"));
    this.mTouziId = ((String)GetIntentData("TouziIdKey"));
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("提交");
    localTextView.setOnClickListener(this);
    if (this.mProjectId == null)
    {
      setTitleStr("我的加息券");
      localTextView.setVisibility(8);
    }
    else
    {
      setTitleStr("可用加息券");
    }
    this.mDatas = new ArrayList();
    this.mAdapter = new FinancingCardAdapter(this, this.mDatas);
    if (this.mProjectId == null) {
      this.mAdapter.setCanSelected(false);
    }
    this.mListView.setAdapter(this.mAdapter);
    if (this.mProjectId != null) {
      this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          ((RateUpCard)FinancingCardAct.this.mDatas.get(paramAnonymousInt)).mIsSelected ^= true;
          FinancingCardAct.this.mAdapter.notifyDataSetChanged();
          FinancingCardAct.this.mListView.invalidate();
        }
      });
    }
    postCardList();
  }
  
  protected void initView()
  {
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165718) {
      useRate();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postCardList()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", 1);
    localRequestParams.put("page_num", 1000);
    String str = this.mProjectId;
    if (str != null) {
      localRequestParams.put("project_id", str);
    }
    postNet(HttpTypeEnum.CouponList_Type, localRequestParams);
  }
  
  protected void postUseRate(String paramString)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("touzi_id", this.mTouziId);
    localRequestParams.put("coupon_id", paramString);
    postNet(HttpTypeEnum.AddInterest_Type, localRequestParams);
  }
  
  protected void updatePage() {}
  
  public void useRate()
  {
    if (!haveSelectRate())
    {
      showToast("请先选择加息券");
      return;
    }
    String str = "";
    Iterator localIterator = this.mDatas.iterator();
    while (localIterator.hasNext())
    {
      RateUpCard localRateUpCard = (RateUpCard)localIterator.next();
      if (localRateUpCard.mIsSelected)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(str);
        localStringBuilder.append(localRateUpCard.getId());
        localStringBuilder.append(",");
        str = localStringBuilder.toString();
      }
    }
    postUseRate(str.substring(0, str.length() - 1));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/FinancingCardAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */