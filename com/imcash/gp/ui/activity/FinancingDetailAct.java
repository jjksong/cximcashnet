package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.ListDataBean;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.PayCheckView;
import com.imcash.gp.ui.view.PayCheckViewCall;
import com.loopj.android.http.RequestParams;
import org.greenrobot.eventbus.EventBus;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296334)
public class FinancingDetailAct
  extends BaseActivity
{
  public static final String FinancingDetailKey = "FinancingDetailKey";
  @ViewInject(2131165350)
  protected TextView coin_count;
  @ViewInject(2131165351)
  protected RadioGroup coin_group;
  @ViewInject(2131165357)
  protected TextView coin_type;
  @ViewInject(2131165495)
  protected TextView income_no;
  @ViewInject(2131165499)
  protected EditText input_count;
  @ViewInject(2131165522)
  protected TextView key_word;
  protected ListDataBean mData;
  protected String mTracePass = "";
  @ViewInject(2131165598)
  protected TextView name;
  protected PayCheckView payCheckView;
  @ViewInject(2131165774)
  protected TextView show_time;
  @ViewInject(2131165826)
  protected TextView title_msg;
  
  @Event({2131165607})
  private void noUsClicked(View paramView)
  {
    toPdfPage("免责条款", "免责条款.pdf");
  }
  
  @Event({2131165610})
  private void noticeClicked(View paramView)
  {
    toPdfPage("投资须知", "投资须知.pdf");
  }
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (this.coin_type.getText().toString().length() == 0)
    {
      showToast("请选择投资币种");
      return;
    }
    if (this.input_count.getText().toString().length() == 0)
    {
      showToast("请输入投资数量");
      return;
    }
    showPayView();
  }
  
  @Event({2131165726})
  private void ruleClicked(View paramView)
  {
    toPdfPage("投保协议", "投保协议.pdf");
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (5.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      stopLoad();
      showToast("投资成功");
      finish();
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("FinancingDetailKey") != null)
    {
      String str = (String)GetIntentData("FinancingDetailKey");
      this.mData = ((ListDataBean)new Gson().fromJson(str, ListDataBean.class));
    }
    this.coin_count.setText("0.00");
    this.coin_type.setText("");
    this.coin_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        switch (FinancingDetailAct.this.coin_group.getCheckedRadioButtonId())
        {
        default: 
          break;
        case 2131165880: 
          FinancingDetailAct.this.coin_type.setText(com.imcash.gp.GlobalFunction.Constants.CoinType[4]);
          break;
        case 2131165559: 
          FinancingDetailAct.this.coin_type.setText(com.imcash.gp.GlobalFunction.Constants.CoinType[2]);
          break;
        case 2131165424: 
          FinancingDetailAct.this.coin_type.setText(com.imcash.gp.GlobalFunction.Constants.CoinType[1]);
          break;
        case 2131165302: 
          FinancingDetailAct.this.coin_type.setText(com.imcash.gp.GlobalFunction.Constants.CoinType[0]);
          break;
        case 2131165261: 
          FinancingDetailAct.this.coin_type.setText(com.imcash.gp.GlobalFunction.Constants.CoinType[3]);
        }
      }
    });
    this.input_count.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        if (FinancingDetailAct.this.input_count.getText().toString().length() == 0) {
          FinancingDetailAct.this.coin_count.setText("0.00");
        } else {
          FinancingDetailAct.this.coin_count.setText(FinancingDetailAct.this.input_count.getText().toString());
        }
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView()
  {
    setTitleStr("量子基金");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("挂单大厅");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        FinancingDetailAct.this.toPage(TradeAct.class);
      }
    });
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    PayCheckView localPayCheckView = this.payCheckView;
    if (localPayCheckView != null) {
      localPayCheckView.cancelTimeCount();
    }
    EventBus.getDefault().unregister(this);
  }
  
  protected void postOk()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currencyid", GlobalFunction.getCoinIndexByName(this.coin_type.getText().toString()));
    localRequestParams.put("projectid", this.mData.getId());
    localRequestParams.put("tradepass", MD5.encode(this.mTracePass));
    localRequestParams.put("num", this.input_count.getText().toString());
    postNet(HttpTypeEnum.BuyTransactions, localRequestParams);
  }
  
  protected void showPayView()
  {
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString2.length() == 0)
          {
            FinancingDetailAct.this.showToast("请输入正确的资金密码");
            return;
          }
          paramAnonymousString1 = FinancingDetailAct.this;
          paramAnonymousString1.mTracePass = paramAnonymousString2;
          paramAnonymousString1.postOk();
          FinancingDetailAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked() {}
      });
      this.payCheckView.setFocusable(true);
    }
    this.payCheckView.closeSmsLayout();
    this.payCheckView.show();
  }
  
  protected void updatePage()
  {
    Object localObject = this.mData;
    if (localObject == null) {
      return;
    }
    this.name.setText(((ListDataBean)localObject).getName());
    TextView localTextView = this.title_msg;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("持仓期限");
    ((StringBuilder)localObject).append(this.mData.getTimelist());
    ((StringBuilder)localObject).append("天，预估收益达");
    ((StringBuilder)localObject).append(this.mData.getIncome_no());
    localTextView.setText(((StringBuilder)localObject).toString());
    this.key_word.setText(this.mData.getKeyword());
    this.income_no.setText(this.mData.getIncome_no());
    localTextView = this.show_time;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(this.mData.getTimelist());
    ((StringBuilder)localObject).append("天");
    localTextView.setText(((StringBuilder)localObject).toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/FinancingDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */