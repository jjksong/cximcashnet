package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.tools.ClipboardTools;
import com.imcash.gp.tools.ImgUtils;
import com.imcash.gp.tools.ZXingCodeUtils;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.util.List;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296376)
public class RechargeAct
  extends BaseActivity
{
  public static final String WalletDataKey = "WalletDataKey";
  @ViewInject(2131165260)
  protected TextView bch_attention;
  @ViewInject(2131165326)
  protected TextView change_location;
  @ViewInject(2131165402)
  protected TextView des;
  @ViewInject(2131165553)
  protected TextView location;
  protected String mNewLocation;
  protected String mOldLocation;
  protected Bitmap mQrBitmap;
  protected Wallet mWallet;
  @ViewInject(2131165620)
  protected TextView only_omni;
  @ViewInject(2131165683)
  protected ImageView qr_img;
  
  @Event({2131165326})
  private void changeLocation(View paramView)
  {
    paramView = "";
    if (this.location.getText().equals(this.mOldLocation)) {
      paramView = this.mNewLocation;
    }
    if (this.location.getText().equals(this.mNewLocation)) {
      paramView = this.mOldLocation;
    }
    updataLocationData(paramView);
  }
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    finish();
  }
  
  @Event({2131165732})
  private void saveLocationStrClicked(View paramView)
  {
    ClipboardTools.copy(this.location.getText().toString(), this);
    showToast("复制成功");
  }
  
  @Event({2131165733})
  private void saveQrImgClicked(View paramView)
  {
    if (this.mQrBitmap == null) {
      return;
    }
    XXPermissions.with(this).constantRequest().permission(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }).request(new OnPermission()
    {
      public void hasPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        paramAnonymousList = RechargeAct.this;
        ImgUtils.saveImageToGallery(paramAnonymousList, paramAnonymousList.mQrBitmap);
        RechargeAct.this.showToast("二维码保存成功");
      }
      
      public void noPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        BaseActivity.showBtnBox(RechargeAct.this, "权限获取失败，该功能暂时无法使用，是否要开启权限？", null, new Btn2Dialog.OnBtnListener()
        {
          public void onClick()
          {
            XXPermissions.gotoPermissionSettings(RechargeAct.this);
          }
        });
      }
    });
  }
  
  protected void getLocation(JSONObject paramJSONObject)
  {
    if ((paramJSONObject.optJSONObject("data") != null) && (paramJSONObject.optString("data").length() != 0))
    {
      String str = paramJSONObject.optJSONObject("data").optString("coin_address", "");
      this.des.setText(str);
      if ("BCH".equals(this.mWallet.getName()))
      {
        this.mNewLocation = paramJSONObject.optJSONObject("data").optString("coin_in_url", "");
        this.mOldLocation = paramJSONObject.optJSONObject("data").optString("coin_url", "");
      }
      updataLocationData(paramJSONObject.optJSONObject("data").optString("coin_in_url", ""));
      return;
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getLocation(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("WalletDataKey") != null) {
      this.mWallet = ((Wallet)new Gson().fromJson((String)GetIntentData("WalletDataKey"), Wallet.class));
    }
    setTitleStr(this.mWallet.getName());
    if (this.mWallet.getName().equals("USDT")) {
      this.only_omni.setVisibility(0);
    } else {
      this.only_omni.setVisibility(8);
    }
    if (this.mWallet.getName().equals("BCH"))
    {
      this.change_location.setVisibility(0);
      this.bch_attention.setVisibility(0);
    }
    else
    {
      this.change_location.setVisibility(8);
      this.bch_attention.setVisibility(8);
    }
    postLocation();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Bitmap localBitmap = this.mQrBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled()))
    {
      this.mQrBitmap.recycle();
      this.mQrBitmap = null;
      System.gc();
    }
  }
  
  protected void postLocation()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mWallet.getCurrency_id());
    postNet(HttpTypeEnum.Currency_Address, localRequestParams);
  }
  
  protected void updataLocationData(String paramString)
  {
    if (paramString.length() != 0)
    {
      this.location.setText(paramString);
      this.mQrBitmap = ZXingCodeUtils.getInstance().createQRCode(paramString, this.qr_img.getWidth(), this.qr_img.getHeight());
      this.qr_img.setImageBitmap(this.mQrBitmap);
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RechargeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */