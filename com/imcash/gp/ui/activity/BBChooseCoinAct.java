package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.model.BBExchange;
import com.imcash.gp.model.BBExchange.CoinBean;
import com.imcash.gp.ui.adapter.BBChooseAdapter;
import com.imcash.gp.ui.base.BaseActivity;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296295)
public class BBChooseCoinAct
  extends BaseActivity
{
  public static final String BBCoinKey = "BBCoinKey";
  @ViewInject(2131165540)
  protected RecyclerView listView;
  protected BBChooseAdapter mAdapter;
  protected BBExchange mDatas;
  private LinearLayoutManager mLinearLayoutManager;
  
  public void coinClicked(int paramInt)
  {
    EventBus.getDefault().post(((BBExchange.CoinBean)this.mDatas.getCoin().get(paramInt)).getName());
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    if ((String)GetIntentData("BBCoinKey") != null)
    {
      this.mDatas = ((BBExchange)new Gson().fromJson((String)GetIntentData("BBCoinKey"), BBExchange.class));
      this.mAdapter = new BBChooseAdapter(this, this.mDatas.getCoin());
      this.mLinearLayoutManager = new LinearLayoutManager(this, 1, false);
      this.listView.setLayoutManager(this.mLinearLayoutManager);
      this.listView.setAdapter(this.mAdapter);
    }
  }
  
  protected void initView()
  {
    setTitleStr("选择币种");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BBChooseCoinAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */