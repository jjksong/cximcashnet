package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.MerchantInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296293)
public class BankCardAct
  extends BaseActivity
{
  @ViewInject(2131165253)
  protected TextView bank_name;
  @ViewInject(2131165257)
  protected EditText bankname_input;
  @ViewInject(2131165276)
  protected LinearLayout bind_layout;
  @ViewInject(2131165278)
  protected TextView bind_name;
  @ViewInject(2131165317)
  protected EditText card_input;
  @ViewInject(2131165318)
  protected TextView card_number;
  protected MerchantInfo mMerchantInfo;
  @ViewInject(2131165773)
  protected LinearLayout show_layout;
  @ViewInject(2131165886)
  protected TextView user_name;
  
  @Event({2131165275})
  private void bindClicked(View paramView)
  {
    if (this.card_input.getText().toString().length() == 0)
    {
      showToast("请输入正确的银行卡号");
      return;
    }
    if (this.bankname_input.getText().toString().length() == 0)
    {
      showToast("请输入正确的银行名称");
      return;
    }
    postBindInfo();
  }
  
  @Event({2131165705})
  private void releaseClicked(View paramView)
  {
    showBtnBox(this, "是否要解除绑定银行卡？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        BankCardAct.this.startLoad();
        RequestParams localRequestParams = new RequestParams();
        BankCardAct.this.postNet(HttpTypeEnum.Merchant_ReleaseCard, localRequestParams);
      }
    });
  }
  
  protected void getBankState(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mMerchantInfo = ((MerchantInfo)new Gson().fromJson(paramJSONObject.optString("data"), MerchantInfo.class));
    updataState();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      postBankState();
      break;
    case ???: 
      stopLoad();
      postBankState();
      break;
    case ???: 
      getBankState(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("银行卡");
    postBankState();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postBankState()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Merchant_Info, localRequestParams);
  }
  
  protected void postBindInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("bank_card", this.card_input.getText().toString());
    localRequestParams.put("bank_name", this.bankname_input.getText().toString());
    postNet(HttpTypeEnum.Merchant_BindCard, localRequestParams);
  }
  
  protected void updataState()
  {
    MerchantInfo localMerchantInfo = this.mMerchantInfo;
    if (localMerchantInfo == null)
    {
      this.bind_layout.setVisibility(8);
      this.show_layout.setVisibility(8);
      return;
    }
    if (!localMerchantInfo.isBindCard())
    {
      setTitleStr("绑定银行卡");
      this.bind_layout.setVisibility(0);
      this.show_layout.setVisibility(8);
      this.user_name.setText(this.mMerchantInfo.getUsername());
    }
    else
    {
      setTitleStr("银行卡信息");
      this.bind_layout.setVisibility(8);
      this.show_layout.setVisibility(0);
      this.bind_name.setText(this.mMerchantInfo.getUsername());
      this.bank_name.setText(this.mMerchantInfo.getBank_name());
      this.card_number.setText(this.mMerchantInfo.getBank_card());
    }
  }
  
  protected void updatePage()
  {
    updataState();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BankCardAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */