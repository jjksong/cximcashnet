package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.model.WalletRecord;
import com.imcash.gp.ui.adapter.WalletDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.x;

@ContentView(2131296403)
public class WalletDetailAct
  extends BaseListActivity
{
  public static final String WalletDataKey = "WalletDataKey";
  protected WalletDetailAdapter mAdapter;
  protected TextView mBal;
  protected ImageView mCoinImg;
  protected TextView mCoinName;
  protected ArrayList<WalletRecord> mDatas;
  protected View mHeadView;
  protected Wallet mWallet;
  
  @Event({2131165488})
  private void inClicked(View paramView)
  {
    paramView = new Intent();
    paramView.putExtra("WalletDataKey", new Gson().toJson(this.mWallet));
    toPage(paramView, RechargeAct.class);
  }
  
  @Event({2131165633})
  private void outClicked(View paramView)
  {
    paramView = new Intent();
    paramView.putExtra("WalletDataKey", new Gson().toJson(this.mWallet));
    toPage(paramView, WithdrawalAct.class);
  }
  
  protected void getOneKindCoin(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONObject("data");
    this.mWallet.setCurrency_id(paramJSONObject.optString("currency_id", "0"));
    this.mWallet.setNum(paramJSONObject.optString("num", "0.00"));
    updataTop();
  }
  
  protected void getRecord(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getOneKindCoin(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRecord(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("WalletDataKey") != null)
    {
      String str = (String)GetIntentData("WalletDataKey");
      this.mWallet = ((Wallet)new Gson().fromJson(str, Wallet.class));
    }
    this.mDatas = new ArrayList();
    this.mAdapter = new WalletDetailAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
  }
  
  protected void initView()
  {
    setTitleStr("我的钱包");
    initListView();
    closeRefreshAll();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296404, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.mCoinImg = ((ImageView)this.mHeadView.findViewById(2131165480));
    this.mCoinName = ((TextView)this.mHeadView.findViewById(2131165900));
    this.mBal = ((TextView)this.mHeadView.findViewById(2131165752));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  public void onResume()
  {
    super.onResume();
    postRecord();
    postOneKindCoin();
  }
  
  protected void postOneKindCoin()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mWallet.getCurrency_id());
    postNet(HttpTypeEnum.OneKindCoin, localRequestParams);
  }
  
  protected void postRecord()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mWallet.getCurrency_id());
    postNet(HttpTypeEnum.WalletRecord_Type, localRequestParams);
  }
  
  protected void updataTop()
  {
    this.mCoinImg.setImageResource(GlobalFunction.getCoinImgByName(this.mWallet.getName()));
    this.mCoinName.setText(this.mWallet.getName());
    TextView localTextView = this.mBal;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前可用余额：");
    localStringBuilder.append(this.mWallet.getNum());
    localTextView.setText(localStringBuilder.toString());
  }
  
  protected void updatePage()
  {
    updataTop();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WalletDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */