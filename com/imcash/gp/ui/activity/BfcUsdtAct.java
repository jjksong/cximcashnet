package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.BfcRate;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296318)
public class BfcUsdtAct
  extends BaseActivity
{
  @ViewInject(2131165499)
  protected EditText input_count;
  protected BfcRate mBfcRate;
  protected String mIntegralCount;
  protected double mRule;
  @ViewInject(2131165593)
  protected TextView msg;
  @ViewInject(2131165780)
  protected TextView source_money;
  @ViewInject(2131165781)
  protected TextView source_num;
  
  @Event({2131165233})
  private void allIn(View paramView)
  {
    EditText localEditText = this.input_count;
    paramView = new StringBuilder();
    paramView.append(this.mIntegralCount);
    paramView.append("");
    localEditText.setText(paramView.toString());
  }
  
  @Event({2131165617})
  private void okClicked(View paramView)
  {
    if (this.mBfcRate == null) {
      return;
    }
    if (this.input_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的兑换数量");
      return;
    }
    double d1 = 0.0D;
    try
    {
      double d3 = Double.valueOf(this.input_count.getText().toString()).doubleValue();
      double d2 = this.mRule;
      d1 = d2 * d3;
    }
    catch (Exception paramView)
    {
      for (;;) {}
    }
    paramView = new StringBuilder();
    paramView.append("是否要兑换");
    paramView.append(GlobalFunction.doubleToString(d1));
    paramView.append("BFC");
    showBtnBox(this, paramView.toString(), null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        BfcUsdtAct.this.startLoad();
        RequestParams localRequestParams = BfcUsdtAct.this.getIdAndKeyNumParams();
        localRequestParams.put("num", BfcUsdtAct.this.input_count.getText().toString());
        BfcUsdtAct.this.postNet(HttpTypeEnum.Bfc_Usdt_Exchange, localRequestParams);
      }
    });
  }
  
  @Event({2131165638})
  private void outRechargeClicked(View paramView)
  {
    paramView = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Bfc_RechargeMsg, paramView);
  }
  
  protected void getBfcRate(JSONObject paramJSONObject)
  {
    stopLoad();
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    this.mBfcRate = ((BfcRate)new Gson().fromJson(paramJSONObject.toString(), BfcRate.class));
    this.mIntegralCount = this.mBfcRate.getUsdt();
    this.mRule = Double.valueOf(this.mBfcRate.getPrice()).doubleValue();
    paramJSONObject = this.source_num;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("可用USDT：");
    ((StringBuilder)localObject).append(this.mBfcRate.getUsdt());
    paramJSONObject.setText(((StringBuilder)localObject).toString());
    localObject = this.source_money;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("购买比例：1USDT:");
    paramJSONObject.append(this.mBfcRate.getPrice());
    paramJSONObject.append("BFC");
    ((TextView)localObject).setText(paramJSONObject.toString());
    localObject = this.msg;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("1BFC≈");
    paramJSONObject.append(this.mBfcRate.getBfc_price());
    paramJSONObject.append("USDT");
    ((TextView)localObject).setText(paramJSONObject.toString());
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showMessageBox(this, "提示", paramHttpInfo.mJsonObj.optString("msg", ""));
      break;
    case ???: 
      stopLoad();
      showToast("兑换成功");
      finish();
      break;
    case ???: 
      getBfcRate(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("购买");
    postBfcRate();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postBfcRate()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Bfc_Rate, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcUsdtAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */