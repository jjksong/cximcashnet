package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.TradeInfo;
import com.imcash.gp.ui.adapter.TradeMineAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296398)
public class TradeMineAct
  extends BaseListActivity
{
  public static final int All_Model = -1;
  public static final int Complete_Model = 1;
  public static final int Selling_Model = 0;
  @ViewInject(2131165235)
  protected View all_line;
  @ViewInject(2131165239)
  protected TextView all_tv;
  @ViewInject(2131165362)
  protected View complete_line;
  @ViewInject(2131165364)
  protected TextView complete_tv;
  protected TradeMineAdapter mAdapter;
  protected int mCurrentModel = -1;
  protected ArrayList<TradeInfo> mDatas;
  protected int mTextDefaultColor = Color.rgb(0, 0, 0);
  protected int mTextSelectedColor = Color.rgb(96, 93, 249);
  @ViewInject(2131165758)
  protected View selling_line;
  @ViewInject(2131165759)
  protected TextView selling_tv;
  
  @Event({2131165234})
  private void allClicked(View paramView)
  {
    if (-1 == this.mCurrentModel) {
      return;
    }
    this.mCurrentModel = -1;
    updataTab();
  }
  
  @Event({2131165361})
  private void completeClicked(View paramView)
  {
    if (1 == this.mCurrentModel) {
      return;
    }
    this.mCurrentModel = 1;
    updataTab();
  }
  
  @Event({2131165757})
  private void sellingClicked(View paramView)
  {
    if (this.mCurrentModel == 0) {
      return;
    }
    this.mCurrentModel = 0;
    updataTab();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getCancelMsg(String paramString)
  {
    onRefreshDown();
  }
  
  protected void getMineList(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    stopDownRefresh();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (4.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getMineList(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("我的挂单");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("发布挂单");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TradeMineAct.this.toPage(TradeAllowAct.class);
      }
    });
    this.mDatas = new ArrayList();
    this.mAdapter = new TradeMineAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = (TradeInfo)TradeMineAct.this.mDatas.get(paramAnonymousInt);
        paramAnonymousView = new Intent();
        paramAnonymousView.putExtra("TradeCancelKey", new Gson().toJson(paramAnonymousAdapterView));
        TradeMineAct.this.toPage(paramAnonymousView, TradeCancelAct.class);
      }
    });
  }
  
  protected void initView()
  {
    initListView();
    stopUpRefresh();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  protected void onRefreshDown()
  {
    postMineList();
  }
  
  protected void onRefreshUp() {}
  
  protected void postMineList()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    int i = this.mCurrentModel;
    if (-1 != i) {
      localRequestParams.put("type", i);
    }
    postNet(HttpTypeEnum.TradeMine, localRequestParams);
  }
  
  protected void updataTab()
  {
    switch (this.mCurrentModel)
    {
    default: 
      break;
    case 1: 
      this.all_line.setVisibility(4);
      this.selling_line.setVisibility(4);
      this.complete_line.setVisibility(0);
      this.all_tv.setTextColor(this.mTextDefaultColor);
      this.selling_tv.setTextColor(this.mTextDefaultColor);
      this.complete_tv.setTextColor(this.mTextSelectedColor);
      break;
    case 0: 
      this.all_line.setVisibility(4);
      this.selling_line.setVisibility(0);
      this.complete_line.setVisibility(4);
      this.all_tv.setTextColor(this.mTextDefaultColor);
      this.selling_tv.setTextColor(this.mTextSelectedColor);
      this.complete_tv.setTextColor(this.mTextDefaultColor);
      break;
    case -1: 
      this.all_line.setVisibility(0);
      this.selling_line.setVisibility(4);
      this.complete_line.setVisibility(4);
      this.all_tv.setTextColor(this.mTextSelectedColor);
      this.selling_tv.setTextColor(this.mTextDefaultColor);
      this.complete_tv.setTextColor(this.mTextDefaultColor);
    }
    onRefreshDown();
  }
  
  protected void updatePage()
  {
    updataTab();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeMineAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */