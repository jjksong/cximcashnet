package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.Award;
import com.imcash.gp.model.InviteInfo;
import com.imcash.gp.ui.adapter.InviteAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.x;

@ContentView(2131296342)
public class InviteAct
  extends BaseListActivity
{
  protected TextView income;
  protected TextView income_bch;
  protected TextView income_btc;
  protected TextView income_eth;
  protected TextView income_ltc;
  protected TextView income_usdt;
  protected InviteAdapter mAdapter;
  protected ArrayList<InviteInfo> mDatas;
  protected int mPages = 1;
  protected ArrayList<Award> mPullAwards;
  protected ArrayList<Award> mReleaseAwards;
  protected View mTopView;
  
  @Event({2131165505})
  private void invitationClicked(View paramView)
  {
    toPage(InviteCodeAct.class);
  }
  
  @Event({2131165715})
  private void rightIvClicked(View paramView)
  {
    toPage(InviteGiftAct.class);
  }
  
  protected void addAward(ArrayList<Award> paramArrayList, String paramString)
  {
    Gson localGson = new Gson();
    paramArrayList.clear();
    if ((paramString != null) && (paramString.length() != 0))
    {
      paramString = (ArrayList)localGson.fromJson(paramString, new TypeToken() {}.getType());
      if ((paramString != null) && (paramString.size() > 0)) {
        paramArrayList.addAll(paramString);
      }
    }
  }
  
  protected void getInviteRecord(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONObject("data");
    Object localObject2 = this.income;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("≈");
    ((StringBuilder)localObject1).append(paramJSONObject.optString("all_total", "0.00"));
    ((StringBuilder)localObject1).append(" USDT");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = new Gson();
    localObject1 = paramJSONObject.optString("list");
    if (HttpTypeEnum.InviteRecord_Type_Down == paramHttpTypeEnum)
    {
      stopDownRefresh();
      this.mDatas.clear();
    }
    else
    {
      stopUpRefresh();
    }
    if ((localObject1 != null) && (((String)localObject1).length() != 0))
    {
      paramHttpTypeEnum = (ArrayList)((Gson)localObject2).fromJson((String)localObject1, new TypeToken() {}.getType());
      if ((paramHttpTypeEnum != null) && (paramHttpTypeEnum.size() > 0))
      {
        this.mDatas.addAll(paramHttpTypeEnum);
        changeRefreshState(paramHttpTypeEnum.size(), 15);
      }
    }
    addAward(this.mPullAwards, paramJSONObject.optString("currency_list"));
    addAward(this.mReleaseAwards, paramJSONObject.optString("release"));
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getInviteRecord(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mPullAwards = new ArrayList();
    this.mReleaseAwards = new ArrayList();
    this.mAdapter = new InviteAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        if (paramAnonymousInt == 0) {
          return;
        }
        paramAnonymousView = (InviteInfo)InviteAct.this.mDatas.get(paramAnonymousInt - 1);
        paramAnonymousAdapterView = new Intent();
        paramAnonymousAdapterView.putExtra("UserIdKey", paramAnonymousView.getId());
        InviteAct.this.toPage(paramAnonymousAdapterView, InviteDetailAct.class);
      }
    });
    onRefreshDown();
  }
  
  protected void initView()
  {
    setTitleStr("邀请");
    ImageView localImageView = (ImageView)findViewById(2131165715);
    localImageView.setImageResource(2131099813);
    localImageView.setVisibility(8);
    initListView();
    closeUpState();
    this.mTopView = LayoutInflater.from(this).inflate(2131296345, null);
    this.income = ((TextView)this.mTopView.findViewById(2131165490));
    this.income_btc = ((TextView)this.mTopView.findViewById(2131165492));
    this.income_bch = ((TextView)this.mTopView.findViewById(2131165491));
    this.income_ltc = ((TextView)this.mTopView.findViewById(2131165494));
    this.income_eth = ((TextView)this.mTopView.findViewById(2131165493));
    this.income_usdt = ((TextView)this.mTopView.findViewById(2131165496));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPages = 1;
    postInviteRecord(HttpTypeEnum.InviteRecord_Type_Down);
  }
  
  protected void onRefreshUp()
  {
    this.mPages += 1;
    postInviteRecord(HttpTypeEnum.InviteRecord_Type_Up);
  }
  
  protected void postInviteRecord(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPages);
    localRequestParams.put("page_num", 15);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updataCoin()
  {
    Iterator localIterator = this.mPullAwards.iterator();
    while (localIterator.hasNext())
    {
      Award localAward = (Award)localIterator.next();
      Object localObject1;
      Object localObject2;
      if (localAward.getName().equals("BTC"))
      {
        localObject1 = this.income_btc;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("BTC邀请收益：");
        ((StringBuilder)localObject2).append(localAward.getSum());
        ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      }
      if (localAward.getName().equals("BCH"))
      {
        localObject2 = this.income_bch;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("BCH邀请收益：");
        ((StringBuilder)localObject1).append(localAward.getSum());
        ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
      }
      if (localAward.getName().equals("LTC"))
      {
        localObject1 = this.income_ltc;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("LTC邀请收益：");
        ((StringBuilder)localObject2).append(localAward.getSum());
        ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      }
      if (localAward.getName().equals("ETH"))
      {
        localObject2 = this.income_eth;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("ETH邀请收益：");
        ((StringBuilder)localObject1).append(localAward.getSum());
        ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
      }
      if (localAward.getName().equals("USDT"))
      {
        localObject1 = this.income_usdt;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("USDT邀请收益：");
        ((StringBuilder)localObject2).append(localAward.getSum());
        ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      }
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/InviteAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */