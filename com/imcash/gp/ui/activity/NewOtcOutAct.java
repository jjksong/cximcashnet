package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.MerchantInfo;
import com.imcash.gp.model.OtcInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296287)
public class NewOtcOutAct
  extends BaseActivity
{
  @ViewInject(2131165253)
  protected TextView bank_name;
  @ViewInject(2131165254)
  protected TextView bank_no_toast;
  @ViewInject(2131165382)
  protected EditText count_input;
  @ViewInject(2131165523)
  protected TextView label_tv;
  protected double mMaxOut = 0.0D;
  protected MerchantInfo mMerchantInfo;
  protected OtcInfo mOtcInfo;
  protected double mRate;
  @ViewInject(2131165697)
  protected TextView real_name_no_toast;
  @ViewInject(2131165883)
  protected TextView usdt_percent;
  @ViewInject(2131165889)
  protected TextView user_real_name;
  
  @Event({2131165236})
  private void allOutClicked(View paramView)
  {
    paramView = this.count_input;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mMaxOut);
    localStringBuilder.append("");
    paramView.setText(localStringBuilder.toString());
  }
  
  @Event({2131165252})
  private void bankNameClicked(View paramView)
  {
    paramView = this.mMerchantInfo;
    if (paramView == null) {
      return;
    }
    if (paramView.isBindCard()) {
      return;
    }
    toPage(BankCardAct.class);
  }
  
  @Event({2131165696})
  private void realNameClicked(View paramView)
  {
    paramView = this.mMerchantInfo;
    if (paramView == null) {
      return;
    }
    if (paramView.isRealName()) {
      return;
    }
    toPage(VerifyUserAct.class);
  }
  
  @Event({2131165832})
  private void toPayClicked(View paramView)
  {
    if (this.count_input.getText().toString().length() == 0)
    {
      showToast("请输入正确的USDT数量");
      return;
    }
    if (!this.mMerchantInfo.isRealName())
    {
      showToast("请先完成实名认证");
      return;
    }
    if (!this.mMerchantInfo.isBindCard())
    {
      showToast("请绑定提现银行卡");
      return;
    }
    postOutMsg();
  }
  
  protected void getBankState(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mMerchantInfo = ((MerchantInfo)new Gson().fromJson(paramJSONObject.optString("data"), MerchantInfo.class));
    updataRealUi();
  }
  
  protected void getLableInfo(JSONObject paramJSONObject)
  {
    Object localObject = BaseModel.getDataJson(paramJSONObject);
    this.mRate = ((JSONObject)localObject).optDouble("sellPrice", 0.0D);
    paramJSONObject = this.usdt_percent;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("1USDT≈￥");
    localStringBuilder.append(this.mRate);
    paramJSONObject.setText(localStringBuilder.toString());
    this.label_tv.setText(((JSONObject)localObject).optString("out_label", ""));
    this.mMaxOut = ((JSONObject)localObject).optDouble("balance", 0.0D);
    localObject = this.count_input;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("最大可出金数量");
    paramJSONObject.append(this.mMaxOut);
    paramJSONObject.append("USDT");
    ((EditText)localObject).setHint(paramJSONObject.toString());
    postBankState();
  }
  
  protected void getOutMsg(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mOtcInfo = ((OtcInfo)new Gson().fromJson(paramJSONObject.optString("data", ""), OtcInfo.class));
    paramJSONObject = new RequestParams();
    paramJSONObject.put("appkey", this.mOtcInfo.getAppkey());
    paramJSONObject.put("async_url", this.mOtcInfo.getAsync_url());
    paramJSONObject.put("coin_amount", this.mOtcInfo.getCoin_amount());
    paramJSONObject.put("coin_sign", this.mOtcInfo.getCoin_sign());
    paramJSONObject.put("company_order_num", this.mOtcInfo.getCompany_order_num());
    paramJSONObject.put("kyc", this.mOtcInfo.getKyc());
    paramJSONObject.put("order_time", this.mOtcInfo.getOrder_time());
    paramJSONObject.put("pay_card_bank", this.mOtcInfo.getPay_card_bank());
    paramJSONObject.put("pay_card_num", this.mOtcInfo.getPay_card_num());
    paramJSONObject.put("phone", this.mOtcInfo.getPhone());
    paramJSONObject.put("sync_url", this.mOtcInfo.getSync_url());
    paramJSONObject.put("username", this.mOtcInfo.getUsername());
    paramJSONObject.put("sign", this.mOtcInfo.getSign());
    Intent localIntent = new Intent();
    localIntent.putExtra("Post_Type", 1);
    localIntent.putExtra("title_key", "出金订单");
    localIntent.putExtra("Post_Data", paramJSONObject.toString());
    toPage(localIntent, OtcWebAct.class);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      toPage(OtcRecordAct.class);
      break;
    case ???: 
      getBankState(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getLableInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("出金");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("OTC订单");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        NewOtcOutAct.this.toPage(OtcRecordAct.class);
      }
    });
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onResume()
  {
    super.onResume();
    postLableInfo();
  }
  
  protected void postBankState()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Merchant_Info, localRequestParams);
  }
  
  protected void postLableInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Merchant_Label, localRequestParams);
  }
  
  protected void postOutMsg()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("amount", this.count_input.getText().toString());
    postNet(HttpTypeEnum.Merchant_New_Out, localRequestParams);
  }
  
  protected void updataRealUi()
  {
    Object localObject = this.mMerchantInfo;
    if (localObject == null)
    {
      this.real_name_no_toast.setVisibility(8);
      this.bank_no_toast.setVisibility(8);
      return;
    }
    if (((MerchantInfo)localObject).isBindCard())
    {
      this.bank_no_toast.setVisibility(8);
      this.bank_name.setVisibility(0);
      TextView localTextView = this.bank_name;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(this.mMerchantInfo.getBank_name());
      ((StringBuilder)localObject).append("(");
      ((StringBuilder)localObject).append(this.mMerchantInfo.lastFourCardNum());
      ((StringBuilder)localObject).append(")");
      localTextView.setText(((StringBuilder)localObject).toString());
    }
    else
    {
      this.bank_no_toast.setVisibility(0);
      this.bank_name.setVisibility(8);
    }
    if (this.mMerchantInfo.isRealName())
    {
      this.real_name_no_toast.setVisibility(8);
      this.user_real_name.setVisibility(0);
      this.user_real_name.setText(this.mMerchantInfo.getUsername());
    }
    else
    {
      this.real_name_no_toast.setVisibility(0);
      this.user_real_name.setVisibility(8);
    }
  }
  
  protected void updatePage()
  {
    updataRealUi();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/NewOtcOutAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */