package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.model.Award;
import com.imcash.gp.ui.base.BaseActivity;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296348)
public class InviteGiftAct
  extends BaseActivity
{
  @ViewInject(2131165394)
  protected TextView current_release;
  @ViewInject(2131165490)
  protected TextView income;
  @ViewInject(2131165491)
  protected TextView income_bch;
  @ViewInject(2131165492)
  protected TextView income_btc;
  @ViewInject(2131165493)
  protected TextView income_eth;
  @ViewInject(2131165494)
  protected TextView income_ltc;
  @ViewInject(2131165496)
  protected TextView income_usdt;
  protected ArrayList<Award> mReleaseAwards;
  
  protected void getInviteRecord(JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = paramJSONObject.optJSONObject("data");
    Object localObject2 = new Gson();
    Object localObject1 = localJSONObject.optString("release");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)((Gson)localObject2).fromJson((String)localObject1, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mReleaseAwards.addAll(paramJSONObject);
      }
    }
    localObject1 = this.mReleaseAwards.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      paramJSONObject = (Award)((Iterator)localObject1).next();
      Object localObject3;
      if (paramJSONObject.getName().equals("BTC"))
      {
        localObject2 = this.income_btc;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("");
        ((StringBuilder)localObject3).append(paramJSONObject.getSurplus());
        ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      }
      if (paramJSONObject.getName().equals("BCH"))
      {
        localObject2 = this.income_bch;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("");
        ((StringBuilder)localObject3).append(paramJSONObject.getSurplus());
        ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      }
      if (paramJSONObject.getName().equals("LTC"))
      {
        localObject3 = this.income_ltc;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("");
        ((StringBuilder)localObject2).append(paramJSONObject.getSurplus());
        ((TextView)localObject3).setText(((StringBuilder)localObject2).toString());
      }
      if (paramJSONObject.getName().equals("ETH"))
      {
        localObject2 = this.income_eth;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("");
        ((StringBuilder)localObject3).append(paramJSONObject.getSurplus());
        ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      }
      if (paramJSONObject.getName().equals("USDT"))
      {
        localObject2 = this.income_usdt;
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("");
        ((StringBuilder)localObject3).append(paramJSONObject.getSurplus());
        ((TextView)localObject2).setText(((StringBuilder)localObject3).toString());
      }
    }
    localObject1 = localJSONObject.optString("release_total", "0.00");
    localObject2 = this.income;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("≈");
    paramJSONObject.append((String)localObject1);
    paramJSONObject.append(" USDT");
    ((TextView)localObject2).setText(paramJSONObject.toString());
    paramJSONObject = this.current_release;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("UNCG ");
    ((StringBuilder)localObject1).append(localJSONObject.optString("rate", "0.005"));
    paramJSONObject.setText(((StringBuilder)localObject1).toString());
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("平台奖励金");
    this.mReleaseAwards = new ArrayList();
    postInviteRecord();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postInviteRecord()
  {
    getIdAndKeyNumParams();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/InviteGiftAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */