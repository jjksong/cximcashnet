package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.CoinLimit;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.PayCheckView;
import com.imcash.gp.ui.view.PayCheckViewCall;
import com.loopj.android.http.RequestParams;
import java.text.DecimalFormat;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296300)
public class BfcOutAct
  extends BaseActivity
{
  @ViewInject(2131165312)
  protected TextView can_use;
  @ViewInject(2131165354)
  protected EditText coin_location;
  @ViewInject(2131165381)
  protected EditText count_et;
  @ViewInject(2131165385)
  protected TextView count_type;
  protected CoinLimit mCoinLimit;
  private double mOutCount = 0.0D;
  @ViewInject(2131165610)
  protected TextView notice;
  @ViewInject(2131165635)
  protected TextView out_count;
  protected PayCheckView payCheckView;
  @ViewInject(2131165763)
  protected TextView service_count;
  
  @Event({2131165619})
  private void okClicked(View paramView)
  {
    if (this.coin_location.getText().toString().length() == 0)
    {
      showToast("请输入提币地址");
      return;
    }
    if (Double.doubleToLongBits(0.0D) >= Double.doubleToLongBits(this.mOutCount))
    {
      showToast("提币数量不足");
      return;
    }
    if (this.payCheckView == null)
    {
      this.payCheckView = new PayCheckView(this, new PayCheckViewCall()
      {
        public void okCallBack(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString2.length() == 0)
          {
            BfcOutAct.this.showToast("请输入正确的资金密码");
            return;
          }
          BfcOutAct.this.postOutMsg(paramAnonymousString2);
          BfcOutAct.this.payCheckView.dismiss();
        }
        
        public void postSmsClicked()
        {
          BfcOutAct.this.postSms();
        }
      });
      this.payCheckView.setFocusable(true);
      this.payCheckView.setPhone(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    }
    this.payCheckView.closeSmsLayout();
    this.payCheckView.show();
  }
  
  @Event({2131165681})
  private void qrClicked(View paramView)
  {
    getQrCodePermission();
  }
  
  @Event({2131165772})
  private void showHand(View paramView)
  {
    if (this.mCoinLimit == null) {
      return;
    }
    EditText localEditText = this.count_et;
    paramView = new StringBuilder();
    paramView.append(this.mCoinLimit.getBalance());
    paramView.append("");
    localEditText.setText(paramView.toString());
  }
  
  protected void getCoinLimit(JSONObject paramJSONObject)
  {
    this.mCoinLimit = ((CoinLimit)new Gson().fromJson(paramJSONObject.optString("data"), CoinLimit.class));
    updataLimitMsg();
    this.count_type.setText("BFC");
    TextView localTextView = this.out_count;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append(this.mOutCount);
    paramJSONObject.append("BFC");
    localTextView.setText(paramJSONObject.toString());
    localTextView = this.can_use;
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("可用");
    paramJSONObject.append(this.mCoinLimit.getBalance());
    paramJSONObject.append(" BFC");
    localTextView.setText(paramJSONObject.toString());
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getQrMsg(String paramString)
  {
    this.coin_location.setText(paramString);
    paramString = this.coin_location;
    paramString.setSelection(paramString.length());
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      showToast(paramHttpInfo.mJsonObj.optString("msg", "提交成功"));
      finish();
      break;
    case ???: 
      getCoinLimit(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("提现");
    this.count_et.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        BfcOutAct.this.updataOut();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
    postInfo();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    PayCheckView localPayCheckView = this.payCheckView;
    if (localPayCheckView != null) {
      localPayCheckView.cancelTimeCount();
    }
    EventBus.getDefault().unregister(this);
  }
  
  protected void postInfo()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Bfc_Out_Info, localRequestParams);
  }
  
  protected void postOutMsg(String paramString)
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("address", this.coin_location.getText());
    localRequestParams.put("amount", this.count_et.getText().toString());
    localRequestParams.put("trade_pass", MD5.encode(paramString));
    postNet(HttpTypeEnum.Bfc_Out, localRequestParams);
  }
  
  protected void postSms()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void updataLimitMsg()
  {
    Object localObject = this.count_et;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("最小提币数量");
    localStringBuilder.append(this.mCoinLimit.getCoinoutmin());
    ((EditText)localObject).setHint(localStringBuilder.toString());
    localObject = this.service_count;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("矿工费");
    localStringBuilder.append(this.mCoinLimit.getFeenum());
    localStringBuilder.append("BFC");
    ((TextView)localObject).setText(localStringBuilder.toString());
    this.notice.setText(this.mCoinLimit.getContent());
    updataOut();
  }
  
  protected void updataOut()
  {
    if ((this.mCoinLimit != null) && (this.count_et.getText().toString().length() != 0))
    {
      try
      {
        double d2 = Double.valueOf(this.count_et.getText().toString()).doubleValue();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(this.mCoinLimit.getBalance());
        localStringBuilder.append("");
        double d1 = d2;
        if (d2 > Double.valueOf(localStringBuilder.toString()).doubleValue())
        {
          localObject = this.count_et;
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append(this.mCoinLimit.getBalance());
          localStringBuilder.append("");
          ((EditText)localObject).setText(localStringBuilder.toString());
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append(this.mCoinLimit.getBalance());
          localStringBuilder.append("");
          d1 = Double.valueOf(localStringBuilder.toString()).doubleValue();
        }
        this.mOutCount = (d1 - this.mCoinLimit.getFeenum());
        if (0.0D > this.mOutCount) {
          this.mOutCount = 0.0D;
        }
      }
      catch (Exception localException)
      {
        this.mOutCount = 0.0D;
      }
      DecimalFormat localDecimalFormat = new DecimalFormat("0.00");
      TextView localTextView = this.out_count;
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append(localDecimalFormat.format(this.mOutCount));
      ((StringBuilder)localObject).append("BFC");
      localTextView.setText(((StringBuilder)localObject).toString());
      return;
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcOutAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */