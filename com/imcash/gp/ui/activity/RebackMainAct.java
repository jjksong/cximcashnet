package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.QuotaInfo;
import com.imcash.gp.model.RebackData;
import com.imcash.gp.ui.adapter.RebackMainAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296306)
public class RebackMainAct
  extends BaseListActivity
{
  protected TextView bfc_balance;
  protected RebackMainAdapter mAdapter;
  protected ArrayList<RebackData> mDatas;
  protected View mHeadView;
  protected TextView my_count;
  @ViewInject(2131165715)
  protected ImageView right_iv;
  protected TextView wait_count;
  
  @Event({2131165505})
  private void invitationClicked(View paramView)
  {
    toPage(InviteCodeAct.class);
  }
  
  @Event({2131165715})
  private void rightIvClicked(View paramView)
  {
    postBuyInfo();
  }
  
  protected void getBuyInfo(JSONObject paramJSONObject)
  {
    Object localObject = (QuotaInfo)new Gson().fromJson(paramJSONObject.optString("data"), QuotaInfo.class);
    if ((((QuotaInfo)localObject).getSurplus() != null) && (!"0.00".equals(((QuotaInfo)localObject).getSurplus())) && (1 != ((QuotaInfo)localObject).getSendBack()) && (!"0.00".equals(((QuotaInfo)localObject).getBalance())))
    {
      localObject = new Intent();
      ((Intent)localObject).putExtra("QuotaInfo_Key", paramJSONObject.toString());
      toPage((Intent)localObject, ReBackBuyAct.class);
    }
    else
    {
      paramJSONObject = new Intent();
      if (1 == ((QuotaInfo)localObject).getSendBack()) {
        paramJSONObject.putExtra("Result_Msg_Key", "您今日的回购申请已提交，如需继续回购请明日再来");
      } else if ("0.00".equals(((QuotaInfo)localObject).getBalance())) {
        paramJSONObject.putExtra("Result_Msg_Key", "您的当前BFC持仓不足，请补充持仓后重试");
      }
      toPage(RebackOutTimeAct.class);
    }
  }
  
  protected void getRebackIndex(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    updataTopUi(paramJSONObject);
    updataListUi(paramJSONObject.optString("list", ""));
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getBuyInfo(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRebackIndex(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("推广节点");
    this.right_iv.setImageResource(2131099916);
    this.mDatas = new ArrayList();
    this.mAdapter = new RebackMainAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296308, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.mHeadView.findViewById(2131165728).setOnClickListener(this);
    this.bfc_balance = ((TextView)this.mHeadView.findViewById(2131165271));
    this.wait_count = ((TextView)this.mHeadView.findViewById(2131165897));
    this.my_count = ((TextView)this.mHeadView.findViewById(2131165596));
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165728) {
      toWebPage("规则详解", "http://imcash.art/node_rule.html");
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    postRebackIndex();
  }
  
  protected void onRefreshUp() {}
  
  protected void postBuyInfo()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_BuyBackQuotaBySystem, localRequestParams);
  }
  
  protected void postRebackIndex()
  {
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_Reback_Index, localRequestParams);
  }
  
  protected void updataListUi(String paramString)
  {
    Gson localGson = new Gson();
    stopDownRefresh();
    this.mDatas.clear();
    if ((paramString != null) && (paramString.length() != 0))
    {
      paramString = (ArrayList)localGson.fromJson(paramString, new TypeToken() {}.getType());
      if ((paramString != null) && (paramString.size() > 0)) {
        this.mDatas.addAll(paramString);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected void updataTopUi(JSONObject paramJSONObject)
  {
    TextView localTextView = this.bfc_balance;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前BFC持仓：");
    localStringBuilder.append(paramJSONObject.optString("balance", "0.00"));
    localTextView.setText(localStringBuilder.toString());
    localTextView = this.wait_count;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("待回购额度：");
    localStringBuilder.append(paramJSONObject.optString("hold", "0.00"));
    localTextView.setText(localStringBuilder.toString());
    localTextView = this.my_count;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前持仓均价：");
    localStringBuilder.append(paramJSONObject.optString("average_price", "0.00"));
    localTextView.setText(localStringBuilder.toString());
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RebackMainAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */