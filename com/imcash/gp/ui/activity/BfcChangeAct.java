package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296296)
public class BfcChangeAct
  extends BaseActivity
{
  public static final String Integral_Key = "Integral_Key";
  public static final String Money_Key = "Money_Key";
  public static final String Rule_Key = "rule";
  @ViewInject(2131165499)
  protected EditText input_count;
  protected int mIntegralCount = 0;
  protected int mRule = 0;
  @ViewInject(2131165593)
  protected TextView msg;
  @ViewInject(2131165780)
  protected TextView source_money;
  @ViewInject(2131165781)
  protected TextView source_num;
  
  @Event({2131165233})
  private void allIn(View paramView)
  {
    EditText localEditText = this.input_count;
    paramView = new StringBuilder();
    paramView.append(this.mIntegralCount);
    paramView.append("");
    localEditText.setText(paramView.toString());
  }
  
  @Event({2131165617})
  private void okClicked(View paramView)
  {
    double d1;
    if ((this.input_count.getText().toString().length() != 0) && (Integer.valueOf(this.input_count.getText().toString()).intValue() != 0)) {
      d1 = 0.0D;
    }
    try
    {
      double d2 = Integer.valueOf(this.input_count.getText().toString()).intValue();
      int i = this.mRule;
      d1 = i;
      Double.isNaN(d2);
      Double.isNaN(d1);
      d1 = d2 / d1;
    }
    catch (Exception paramView)
    {
      for (;;) {}
    }
    paramView = new StringBuilder();
    paramView.append("是否要兑换");
    paramView.append(GlobalFunction.doubleToString(d1));
    paramView.append("BFC");
    showBtnBox(this, paramView.toString(), null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        BfcChangeAct.this.startLoad();
        RequestParams localRequestParams = BfcChangeAct.this.getIdAndKeyNumParams();
        localRequestParams.put("num", BfcChangeAct.this.input_count.getText().toString());
        BfcChangeAct.this.postNet(HttpTypeEnum.Bfc_Change, localRequestParams);
      }
    });
    return;
    showToast("请输入正确的兑换数量");
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      stopLoad();
      showToast("兑换成功");
      finish();
    }
  }
  
  protected void initData()
  {
    this.mIntegralCount = ((Integer)GetIntentData("Integral_Key")).intValue();
    this.mRule = ((Integer)GetIntentData("rule")).intValue();
    TextView localTextView = this.source_num;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("可用积分：");
    localStringBuilder.append(this.mIntegralCount);
    localTextView.setText(localStringBuilder.toString());
    localTextView = this.source_money;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("兑换比例：");
    localStringBuilder.append(this.mRule);
    localStringBuilder.append("积分:1BFC通证");
    localTextView.setText(localStringBuilder.toString());
    this.msg.setText((String)GetIntentData("Money_Key"));
  }
  
  protected void initView()
  {
    setTitleStr("通证兑换");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcChangeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */