package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.RebackData;
import com.imcash.gp.ui.adapter.RebackRecordAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.x;

@ContentView(2131296310)
public class RebackRecordAct
  extends BaseListActivity
{
  protected TextView complete_count;
  protected TextView last_count;
  protected RebackRecordAdapter mAdapter;
  protected ArrayList<RebackData> mDatas;
  protected View mHeadView;
  
  @Event({2131165505})
  private void invitationClicked(View paramView)
  {
    toPage(InviteCodeAct.class);
  }
  
  protected void getRecord(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    updataTopUi(paramJSONObject);
    updataListUi(paramJSONObject.optString("list", ""));
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getRecord(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("回购明细");
    this.mDatas = new ArrayList();
    this.mAdapter = new RebackRecordAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296312, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.complete_count = ((TextView)this.mHeadView.findViewById(2131165360));
    this.last_count = ((TextView)this.mHeadView.findViewById(2131165525));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    postRecord();
  }
  
  protected void onRefreshUp() {}
  
  protected void postRecord()
  {
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_BackRecord, localRequestParams);
  }
  
  protected void updataListUi(String paramString)
  {
    Gson localGson = new Gson();
    stopDownRefresh();
    this.mDatas.clear();
    if ((paramString != null) && (paramString.length() != 0))
    {
      paramString = (ArrayList)localGson.fromJson(paramString, new TypeToken() {}.getType());
      if ((paramString != null) && (paramString.size() > 0)) {
        this.mDatas.addAll(paramString);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected void updataTopUi(JSONObject paramJSONObject)
  {
    TextView localTextView = this.complete_count;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramJSONObject.optString("sum", "0.00"));
    localStringBuilder.append("BFC");
    localTextView.setText(localStringBuilder.toString());
    localTextView = this.last_count;
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramJSONObject.optString("quota", "0.00"));
    localStringBuilder.append("BFC");
    localTextView.setText(localStringBuilder.toString());
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RebackRecordAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */