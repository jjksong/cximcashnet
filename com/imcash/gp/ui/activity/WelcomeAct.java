package com.imcash.gp.ui.activity;

import android.app.ActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.model.WindowInfo;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296412)
public class WelcomeAct
  extends BaseActivity
{
  protected boolean isBack = false;
  protected Handler mNormalHandler = null;
  protected Runnable runnable = new Runnable()
  {
    public void run()
    {
      if (WelcomeAct.this.isFinishing()) {
        return;
      }
      Object localObject = (ActivityManager)WelcomeAct.this.getSystemService("activity");
      if (WelcomeAct.this.isBack) {
        return;
      }
      if (!WelcomeAct.this.mApplication.getDataCore().isLogin()) {
        localObject = new Intent(WelcomeAct.this, LoginAct.class);
      } else {
        localObject = new Intent(WelcomeAct.this, MainActivity.class);
      }
      WelcomeAct.this.startActivity((Intent)localObject);
      WelcomeAct.this.finish();
    }
  };
  protected WindowInfo windowInfo;
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getKeyCode() == 4) {
      this.isBack = true;
    }
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    initWindowInfo();
    normalLogin();
  }
  
  protected void initView() {}
  
  protected void initWindowInfo()
  {
    this.windowInfo = new WindowInfo();
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.windowInfo.width = localDisplayMetrics.widthPixels;
    this.windowInfo.height = localDisplayMetrics.heightPixels;
    this.windowInfo.density = localDisplayMetrics.density;
    this.windowInfo.densityDpi = localDisplayMetrics.densityDpi;
    this.windowInfo.refresh();
    saveWindowInfo();
  }
  
  protected void normalLogin()
  {
    if (this.mNormalHandler == null) {
      this.mNormalHandler = new Handler();
    }
    this.mNormalHandler.postDelayed(this.runnable, 2000L);
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void saveWindowInfo()
  {
    PreferenceManager.getDefaultSharedPreferences(this).edit().putInt("Window_width", this.windowInfo.width).putInt("Window_height", this.windowInfo.height).putFloat("Window_density", this.windowInfo.density).putFloat("Window_DPI", this.windowInfo.densityDpi).commit();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WelcomeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */