package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.TradeInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296394)
public class TradeCancelAct
  extends BaseActivity
{
  public static final String TradeCancelKey = "TradeCancelKey";
  @ViewInject(2131165357)
  protected TextView coin_type;
  @ViewInject(2131165361)
  protected LinearLayout complete_layout;
  @ViewInject(2131165435)
  protected TextView finish_time;
  @ViewInject(2131165442)
  protected TextView fund_money;
  @ViewInject(2131165443)
  protected TextView fund_name;
  @ViewInject(2131165446)
  protected TextView get_money;
  @ViewInject(2131165454)
  protected Button goto_cancel;
  @ViewInject(2131165526)
  protected TextView last_time;
  protected TradeInfo mTradeInfo;
  @ViewInject(2131165624)
  protected TextView order_id;
  @ViewInject(2131165626)
  protected TextView order_money;
  @ViewInject(2131165628)
  protected TextView order_time;
  @ViewInject(2131165693)
  protected TextView rate_num;
  @ViewInject(2131165764)
  protected TextView service_money;
  
  @Event({2131165454})
  private void cancelClicked(View paramView)
  {
    if (!this.mTradeInfo.isSelling())
    {
      finish();
      return;
    }
    showBtnBox(this, "是否要取消此次挂单？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        TradeCancelAct.this.postCancel();
      }
    });
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      showToast(paramHttpInfo.mJsonObj.optString("msg", "取消成功"));
      EventBus.getDefault().post("");
      finish();
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("TradeCancelKey") != null)
    {
      this.mTradeInfo = ((TradeInfo)new Gson().fromJson((String)GetIntentData("TradeCancelKey"), TradeInfo.class));
      if (this.mTradeInfo.isSelling()) {
        setTitleStr("取消挂单");
      } else {
        setTitleStr("已成交");
      }
      this.order_id.setText(this.mTradeInfo.getTrade_no());
      this.order_time.setText(this.mTradeInfo.getCreate_time());
      Object localObject1 = this.order_money;
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(this.mTradeInfo.getAmount());
      ((StringBuilder)localObject2).append(this.mTradeInfo.getCurrency_name());
      ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      this.fund_name.setText(this.mTradeInfo.getName());
      this.coin_type.setText(this.mTradeInfo.getCurrency_name());
      localObject1 = this.fund_money;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(this.mTradeInfo.getNum());
      ((StringBuilder)localObject2).append(this.mTradeInfo.getCurrency_name());
      ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      this.rate_num.setText(this.mTradeInfo.getRate());
      this.last_time.setText(this.mTradeInfo.getDue_date());
      localObject2 = this.service_money;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(this.mTradeInfo.getFee());
      ((StringBuilder)localObject1).append(this.mTradeInfo.getCurrency_name());
      ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
      localObject1 = this.get_money;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(this.mTradeInfo.getTotal_amount());
      ((StringBuilder)localObject2).append(this.mTradeInfo.getCurrency_name());
      ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
      if (this.mTradeInfo.isSelling())
      {
        this.goto_cancel.setVisibility(0);
        this.goto_cancel.setText("取消挂单");
        this.complete_layout.setVisibility(8);
      }
      else
      {
        this.goto_cancel.setText("确定");
        this.finish_time.setText(this.mTradeInfo.getFinish_time());
        this.complete_layout.setVisibility(0);
      }
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postCancel()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("trade_id", this.mTradeInfo.getTrade_id());
    postNet(HttpTypeEnum.TradeCancel, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeCancelAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */