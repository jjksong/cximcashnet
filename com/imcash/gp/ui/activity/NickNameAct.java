package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296361)
public class NickNameAct
  extends BaseActivity
{
  public static final String NickNameKey = "NickNameKey";
  @ViewInject(2131165401)
  protected ImageView delete_btn;
  @ViewInject(2131165605)
  protected EditText nickname;
  
  @Event({2131165401})
  private void deleteClicked(View paramView)
  {
    this.nickname.setText("");
  }
  
  @Event({2131165718})
  private void saveClicked(View paramView)
  {
    postSave();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      stopLoad();
      showToast("修改成功");
      finish();
    }
  }
  
  protected void initData()
  {
    setTitleStr("昵称修改");
    ((TextView)findViewById(2131165718)).setText("保存");
    if ((String)GetIntentData("NickNameKey") != null)
    {
      this.nickname.setText((String)GetIntentData("NickNameKey"));
      EditText localEditText = this.nickname;
      localEditText.setSelection(localEditText.getText().toString().length());
    }
    this.nickname.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        NickNameAct.this.updateDeleteState();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postSave()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("nickname", this.nickname.getText().toString());
    postNet(HttpTypeEnum.ModifyPersonal_Type, localRequestParams);
  }
  
  protected void updateDeleteState()
  {
    if (this.nickname.getText().toString().length() == 0) {
      this.delete_btn.setVisibility(8);
    } else {
      this.delete_btn.setVisibility(0);
    }
  }
  
  protected void updatePage()
  {
    updateDeleteState();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/NickNameAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */