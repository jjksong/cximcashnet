package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.RedRecord;
import com.imcash.gp.ui.adapter.RedRecordAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296378)
public class RedRecordListAct
  extends BaseListActivity
{
  protected RedRecordAdapter mAdapter;
  protected ArrayList<RedRecord> mDatas;
  
  protected void getRecordRed(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getRecordRed(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new RedRecordAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    postRecordRed();
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousView = (RedRecord)RedRecordListAct.this.mDatas.get(paramAnonymousInt);
        paramAnonymousAdapterView = new Intent();
        paramAnonymousAdapterView.putExtra("RedRecord_Key", new Gson().toJson(paramAnonymousView));
        RedRecordListAct.this.toPage(paramAnonymousAdapterView, RedRecordDetailAct.class);
      }
    });
  }
  
  protected void initView()
  {
    setTitleStr("红包记录");
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postRecordRed()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_Record, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedRecordListAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */