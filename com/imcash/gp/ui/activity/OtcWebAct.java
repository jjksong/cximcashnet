package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296367)
public class OtcWebAct
  extends BaseActivity
{
  public static String OTC_URL = "https://open.otc365.com/v1";
  public static final String Post_Data = "Post_Data";
  public static final String Post_Type = "Post_Type";
  public static final String TITLE_KEY = "title_key";
  public static final int Type_Buy = 0;
  public static final int Type_Out = 1;
  protected String mPostDatas = "";
  protected int mWebType = 0;
  protected WebView webView;
  
  protected String getFullUrl()
  {
    String str = OTC_URL;
    int i = this.mWebType;
    Object localObject;
    if (i == 0)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("/third/merchant_buy");
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = str;
      if (1 == i)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append("/third/merchant_sell_view");
        localObject = ((StringBuilder)localObject).toString();
      }
    }
    return (String)localObject;
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr(getIntent().getExtras().getString("title_key"));
    this.mWebType = getIntent().getExtras().getInt("Post_Type");
    this.mPostDatas = getIntent().getExtras().getString("Post_Data");
    this.webView = ((WebView)findViewById(2131165901));
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    getWindow().setFormat(-3);
    getWindow().setSoftInputMode(18);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage()
  {
    this.webView.setWebViewClient(new WebViewClient());
    WebSettings localWebSettings = this.webView.getSettings();
    localWebSettings.setJavaScriptEnabled(true);
    localWebSettings.setSupportMultipleWindows(false);
    localWebSettings.setDisplayZoomControls(false);
    localWebSettings.setBuiltInZoomControls(true);
    localWebSettings.setSupportZoom(true);
    localWebSettings.setDomStorageEnabled(true);
    localWebSettings.setUseWideViewPort(true);
    localWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
    localWebSettings.setPluginState(WebSettings.PluginState.ON);
    localWebSettings.setAllowFileAccess(true);
    localWebSettings.setLoadWithOverviewMode(true);
    this.webView.postUrl(getFullUrl(), this.mPostDatas.getBytes());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/OtcWebAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */