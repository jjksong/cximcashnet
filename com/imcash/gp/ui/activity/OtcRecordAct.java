package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.OtcRecord;
import com.imcash.gp.ui.adapter.OtcRecordAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296365)
public class OtcRecordAct
  extends BaseListActivity
{
  protected OtcRecordAdapter mAdapter;
  protected ArrayList<OtcRecord> mDatas;
  
  protected void getRecordInfo(JSONObject paramJSONObject)
  {
    stopDownRefresh();
    Gson localGson = new Gson();
    try
    {
      String str = paramJSONObject.optString("data");
      this.mDatas.clear();
      if ((str != null) && (str.length() != 0))
      {
        paramJSONObject = new com/imcash/gp/ui/activity/OtcRecordAct$2;
        paramJSONObject.<init>(this);
        paramJSONObject = (ArrayList)localGson.fromJson(str, paramJSONObject.getType());
        if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
          this.mDatas.addAll(paramJSONObject);
        }
      }
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getRecordInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("OTC订单");
    this.mDatas = new ArrayList();
    this.mAdapter = new OtcRecordAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = new Intent();
        paramAnonymousAdapterView.putExtra("Otc_Info_Key", new Gson().toJson(OtcRecordAct.this.mDatas.get(paramAnonymousInt)));
        OtcRecordAct.this.toPage(paramAnonymousAdapterView, OtcRecordDetailAct.class);
      }
    });
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    postRecordInfo();
  }
  
  protected void onRefreshUp() {}
  
  public void onResume()
  {
    super.onResume();
    onRefreshDown();
  }
  
  protected void postRecordInfo()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Merchant_Record, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/OtcRecordAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */