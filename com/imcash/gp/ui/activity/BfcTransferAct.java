package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.math.BigDecimal;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296317)
public class BfcTransferAct
  extends BaseActivity
{
  @ViewInject(2131165350)
  protected EditText coin_count;
  @ViewInject(2131165354)
  protected EditText coin_location;
  protected double mPersent = 0.002D;
  @ViewInject(2131165645)
  protected EditText pass_code;
  @ViewInject(2131165851)
  protected TextView transfer_tv;
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (11 != this.coin_location.getText().toString().length())
    {
      showToast("请输入正确的账号");
      return;
    }
    if (this.coin_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的转账数量");
      return;
    }
    if (this.pass_code.getText().toString().length() == 0)
    {
      showToast("请输入正确的交易密码");
      return;
    }
    showBtnBox(this, "是否要进行此次BFC转账？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        BfcTransferAct.this.postTransfer();
      }
    });
  }
  
  @Event({2131165681})
  private void qrClicked(View paramView)
  {
    getQrCodePermission();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getQrMsg(String paramString)
  {
    this.coin_location.setText(paramString);
    paramString = this.coin_location;
    paramString.setSelection(paramString.length());
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      showToast(paramHttpInfo.mJsonObj.optString("msg", "转账成功"));
      stopLoad();
      finish();
    }
  }
  
  protected void initData()
  {
    setTitleStr("BFC转账");
    this.coin_count.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        BfcTransferAct.this.updataTransfer();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  protected void postTransfer()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("num", this.coin_count.getText().toString());
    localRequestParams.put("trade_pass", MD5.encode(this.pass_code.getText().toString()));
    localRequestParams.put("phone", this.coin_location.getText().toString());
    postNet(HttpTypeEnum.Bfc_Transfer, localRequestParams);
  }
  
  protected void updataTransfer()
  {
    if (this.coin_count.getText().toString().length() == 0) {
      this.transfer_tv.setText("0.00");
    }
    try
    {
      double d = Double.valueOf(this.coin_count.getText().toString()).doubleValue();
      TextView localTextView = this.transfer_tv;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("");
      localStringBuilder.append(BigDecimal.valueOf(d * this.mPersent));
      localTextView.setText(localStringBuilder.toString());
    }
    catch (Exception localException)
    {
      this.transfer_tv.setText("0.00");
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcTransferAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */