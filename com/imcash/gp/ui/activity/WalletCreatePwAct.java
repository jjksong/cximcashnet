package com.imcash.gp.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;
import lsp.com.lib.PasswordInputEdt;
import lsp.com.lib.PasswordInputEdt.onInputOverListener;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296409)
public class WalletCreatePwAct
  extends BaseActivity
{
  @ViewInject(2131165417)
  protected PasswordInputEdt edt;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("创建钱包");
    this.edt.setRectNormalColor(Color.rgb(237, 239, 245));
    this.edt.setRectChooseColor(Color.rgb(237, 239, 245));
    this.edt.setOnInputOverListener(new PasswordInputEdt.onInputOverListener()
    {
      public void onInputOver(String paramAnonymousString)
      {
        WalletCreatePwAct.this.showToast(paramAnonymousString);
        WalletCreatePwAct.this.toPage(WalletCreateSurePwAct.class);
      }
    });
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WalletCreatePwAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */