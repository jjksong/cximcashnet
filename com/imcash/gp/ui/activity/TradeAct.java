package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.TradeInfo;
import com.imcash.gp.model.TradeScreen;
import com.imcash.gp.ui.adapter.TradeAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296395)
public class TradeAct
  extends BaseListActivity
{
  protected TradeAdapter mAdapter;
  protected ArrayList<TradeInfo> mDatas;
  protected View mHeadView;
  protected TradeScreen mTradeScreen;
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void buySuccess(String paramString)
  {
    showToast(paramString);
    postTradeData();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void eventForScreen(TradeScreen paramTradeScreen)
  {
    this.mTradeScreen = paramTradeScreen;
    postTradeData();
  }
  
  protected void getTradeData(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    stopDownRefresh();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (4.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getTradeData(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("交易大厅");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("筛选");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TradeAct.this.toPage(TradeScreenAct.class);
      }
    });
    this.mDatas = new ArrayList();
    this.mAdapter = new TradeAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        try
        {
          paramAnonymousAdapterView = (TradeInfo)TradeAct.this.mDatas.get(paramAnonymousInt);
          paramAnonymousView = new android/content/Intent;
          paramAnonymousView.<init>();
          Gson localGson = new com/google/gson/Gson;
          localGson.<init>();
          paramAnonymousView.putExtra("TradeBuyKey", localGson.toJson(paramAnonymousAdapterView));
          TradeAct.this.toPage(paramAnonymousView, TradeBuyAct.class);
          return;
        }
        catch (Exception paramAnonymousAdapterView)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  protected void onRefreshDown()
  {
    postTradeData();
  }
  
  protected void onRefreshUp() {}
  
  protected void postTradeData()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    TradeScreen localTradeScreen = this.mTradeScreen;
    if (localTradeScreen != null)
    {
      if (localTradeScreen.getmTimeState().length() != 0) {
        localRequestParams.put("time_order", this.mTradeScreen.getmTimeState());
      }
      if (this.mTradeScreen.getmMoneyState().length() != 0) {
        localRequestParams.put("price_order", this.mTradeScreen.getmMoneyState());
      }
      if (this.mTradeScreen.getmRateState().length() != 0) {
        localRequestParams.put("rate_order", this.mTradeScreen.getmRateState());
      }
      if (this.mTradeScreen.getmCoinType().length() != 0) {
        localRequestParams.put("currency_id", this.mTradeScreen.getmCoinType());
      }
      if (this.mTradeScreen.getmBfcType().length() != 0) {
        localRequestParams.put("bfc_type", this.mTradeScreen.getmBfcType());
      }
    }
    postNet(HttpTypeEnum.TradeHallList, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */