package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.ui.adapter.WalletAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296405)
public class WalletAct
  extends BaseListActivity
{
  protected WalletAdapter mAdapter;
  protected View mBottomView;
  protected ArrayList<Wallet> mDatas;
  
  protected void getWallet(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    stopDownRefresh();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getWallet(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("钱包管理");
    this.mDatas = new ArrayList();
    this.mAdapter = new WalletAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousView = (Wallet)WalletAct.this.mDatas.get(paramAnonymousInt);
        paramAnonymousAdapterView = new Intent();
        paramAnonymousAdapterView.putExtra("WalletDataKey", new Gson().toJson(paramAnonymousView));
        WalletAct.this.toPage(paramAnonymousAdapterView, WalletDetailAct.class);
      }
    });
    onRefreshDown();
    closeUpState();
  }
  
  protected void initView()
  {
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    postWallet();
  }
  
  protected void onRefreshUp() {}
  
  protected void postWallet()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.WalletBalance_Type, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WalletAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */