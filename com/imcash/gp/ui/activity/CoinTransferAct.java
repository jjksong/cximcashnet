package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.CoinInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import java.math.BigDecimal;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296329)
public class CoinTransferAct
  extends BaseActivity
{
  public static final String CoinInfoKey = "CoinInfoKey";
  @ViewInject(2131165350)
  protected EditText coin_count;
  @ViewInject(2131165354)
  protected EditText coin_location;
  @ViewInject(2131165356)
  protected TextView coin_name;
  protected CoinInfo mCoinInfo;
  protected TimeCount mTimeCount;
  protected double mTransferPercent = 0.0D;
  @ViewInject(2131165645)
  protected EditText pass_code;
  @ViewInject(2131165762)
  protected TextView send_sms;
  @ViewInject(2131165851)
  protected TextView transfer_tv;
  @ViewInject(2131165892)
  protected EditText verify_code;
  
  @Event({2131165618})
  private void okclicked(View paramView)
  {
    if (11 != this.coin_location.length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    if (this.coin_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的划转数量");
      return;
    }
    if (this.verify_code.getText().toString().length() == 0)
    {
      showToast("请输入正确的短信验证码");
      return;
    }
    if (this.pass_code.getText().toString().length() == 0)
    {
      showToast("请输入正确的资金密码");
      return;
    }
    postTrans();
  }
  
  @Event({2131165681})
  private void qrClicked(View paramView)
  {
    getQrCodePermission();
  }
  
  @Event({2131165762})
  private void sendSmsClicked(View paramView)
  {
    if (this.coin_location.getText().toString().length() == 0)
    {
      showToast("请输入对方账号");
      return;
    }
    if (this.coin_count.getText().toString().length() == 0)
    {
      showToast("请输入金额");
      return;
    }
    startTimeCount();
    postVerify();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getQrMsg(String paramString)
  {
    this.coin_location.setText(paramString);
    paramString = this.coin_location;
    paramString.setSelection(paramString.length());
  }
  
  protected void getTransferNeed(JSONObject paramJSONObject)
  {
    this.mTransferPercent = paramJSONObject.optJSONObject("data").optDouble("fee");
    updataTransfer();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("划转成功");
      stopLoad();
      finish();
      break;
    case ???: 
      getTransferNeed(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("CoinInfoKey") != null) {
      this.mCoinInfo = ((CoinInfo)new Gson().fromJson((String)GetIntentData("CoinInfoKey"), CoinInfo.class));
    }
    CoinInfo localCoinInfo = this.mCoinInfo;
    if (localCoinInfo != null) {
      this.coin_name.setText(localCoinInfo.getCoin());
    }
    postTransferNeed();
    this.mTimeCount = new TimeCount(60000L, 1000L);
    this.coin_count.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        CoinTransferAct.this.updataTransfer();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView()
  {
    setTitleStr("Imcash划转");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  protected void postTrans()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currencyid", this.mCoinInfo.getId());
    localRequestParams.put("addr", this.coin_location.getText().toString());
    localRequestParams.put("amount", this.coin_count.getText().toString());
    localRequestParams.put("fundpass", MD5.encode(this.pass_code.getText().toString()));
    localRequestParams.put("code", this.verify_code.getText().toString());
    postNet(HttpTypeEnum.CoinTrans, localRequestParams);
  }
  
  protected void postTransferNeed()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Wallet_Poundage, localRequestParams);
  }
  
  protected void postVerify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void startTimeCount()
  {
    this.send_sms.setClickable(false);
    this.mTimeCount.start();
  }
  
  protected void updataTransfer()
  {
    if (this.coin_count.getText().toString().length() == 0) {
      this.transfer_tv.setText("0.00");
    }
    try
    {
      double d = Double.valueOf(this.coin_count.getText().toString()).doubleValue();
      TextView localTextView = this.transfer_tv;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("");
      localStringBuilder.append(BigDecimal.valueOf(d * this.mTransferPercent));
      localTextView.setText(localStringBuilder.toString());
    }
    catch (Exception localException)
    {
      this.transfer_tv.setText("0.00");
    }
  }
  
  protected void updatePage() {}
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      CoinTransferAct.this.send_sms.setClickable(true);
      CoinTransferAct.this.send_sms.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = CoinTransferAct.this.send_sms;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/CoinTransferAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */