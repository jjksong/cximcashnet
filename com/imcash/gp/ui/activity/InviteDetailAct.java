package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.InviteDetail;
import com.imcash.gp.ui.adapter.InviteDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296346)
public class InviteDetailAct
  extends BaseListActivity
{
  public static final String UserIdKey = "UserIdKey";
  protected InviteDetailAdapter mAdapter;
  protected ArrayList<InviteDetail> mDatas;
  protected String mUserId;
  
  protected void getDetailInfo(JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getDetailInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    if ((String)GetIntentData("UserIdKey") != null) {
      this.mUserId = ((String)GetIntentData("UserIdKey"));
    }
    this.mDatas = new ArrayList();
    this.mAdapter = new InviteDetailAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    postDetailInfo();
  }
  
  protected void initView()
  {
    setTitleStr("收益详情");
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postDetailInfo()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("record_id", this.mUserId);
    postNet(HttpTypeEnum.InviteDetail_Type, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/InviteDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */