package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296358)
public class ModifyPassAct
  extends BaseActivity
{
  @ViewInject(2131165445)
  protected TextView get_mail_verify;
  protected TimeCount mTimeCount;
  @ViewInject(2131165600)
  protected EditText new_pass;
  @ViewInject(2131165654)
  protected TextView phone;
  @ViewInject(2131165891)
  protected EditText verify;
  
  @Event({2131165275})
  private void bindClicked(View paramView)
  {
    if ((6 <= this.new_pass.getText().toString().length()) && (18 >= this.new_pass.getText().toString().length()))
    {
      if (this.verify.getText().toString().length() == 0)
      {
        showToast("请输入正确的验证码");
        return;
      }
      postModify();
      return;
    }
    showToast("请输入正确的密码");
  }
  
  @Event({2131165445})
  private void getMailVerify(View paramView)
  {
    startTimeCount();
    postVerify();
  }
  
  protected void bindSuccess()
  {
    showToast("修改成功");
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      bindSuccess();
    }
  }
  
  protected void initData()
  {
    setTitleStr("修改密码");
    this.phone.setText(this.mApplication.getDataCore().getUserInfo().getHidePhone());
    this.mTimeCount = new TimeCount(60000L, 1000L);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postModify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    localRequestParams.put("password", this.new_pass.getText().toString());
    localRequestParams.put("code", this.verify.getText().toString());
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.ModifyPassWord_Type, localRequestParams);
  }
  
  protected void postVerify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.mApplication.getDataCore().getUserInfo().username);
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void startTimeCount()
  {
    this.get_mail_verify.setClickable(false);
    this.mTimeCount.start();
  }
  
  protected void updatePage() {}
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      ModifyPassAct.this.get_mail_verify.setClickable(true);
      ModifyPassAct.this.get_mail_verify.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = ModifyPassAct.this.get_mail_verify;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ModifyPassAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */