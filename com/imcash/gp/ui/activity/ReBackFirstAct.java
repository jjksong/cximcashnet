package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296304)
public class ReBackFirstAct
  extends BaseActivity
{
  public static final String BFC_Balance_Key = "BFC_Balance_Key";
  @ViewInject(2131165271)
  protected TextView bfc_balance;
  @ViewInject(2131165727)
  protected TextView rule_3_tv;
  
  @Event({2131165405})
  private void detailRule(View paramView)
  {
    toWebPage("规则详解", "http://imcash.art/node_rule.html");
  }
  
  @Event({2131165521})
  private void joinBtnClicked(View paramView)
  {
    showBtnBox(this, "确定想要参加活动吗？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        ReBackFirstAct.this.postSure();
      }
    });
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      Intent localIntent = new Intent();
      if (paramHttpInfo.mJsonObj.optString("status").equals("success")) {
        localIntent.putExtra("Result_Status_Key", "success");
      } else {
        localIntent.putExtra("Result_Status_Key", "error");
      }
      toPage(localIntent, ReBackResultAct.class);
      finish();
    }
  }
  
  protected void initData()
  {
    setTitleStr("推广节点");
    TextView localTextView = this.bfc_balance;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("当前BFC持仓：");
    localStringBuilder.append((String)GetIntentData("BFC_Balance_Key"));
    localStringBuilder.append("BFC");
    localTextView.setText(localStringBuilder.toString());
    this.rule_3_tv.setText("每天中午12:00，平台以你持仓BFC的平均购入价溢价50%回购你的BFC，回购量最大不超过你的持仓&你当前待回购额度&平台当日剩余回购USDT总量。");
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postSure()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_Back_Join, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ReBackFirstAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */