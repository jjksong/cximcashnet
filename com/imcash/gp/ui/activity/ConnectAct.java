package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.tools.ClipboardTools;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296330)
public class ConnectAct
  extends BaseActivity
{
  @ViewInject(2131165369)
  protected EditText contact_et;
  
  @Event({2131165430})
  private void faceClicked(View paramView)
  {
    ClipboardTools.copy("IMCASH WALLET", this);
    showToast("复制成功");
  }
  
  @Event({2131165520})
  private void jianYaClicked(View paramView)
  {
    ClipboardTools.copy("JiangYa@imcash.email", this);
    showToast("复制成功");
  }
  
  @Event({2131165560})
  private void mailClicked(View paramView)
  {
    ClipboardTools.copy("service@imcash.email", this);
    showToast("复制成功");
  }
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (this.contact_et.getText().toString().length() == 0) {
      return;
    }
    paramView = getIdAndKeyNumParams();
    paramView.put("content", this.contact_et.getText());
    postNet(HttpTypeEnum.Contack_We, paramView);
  }
  
  @Event({2131165811})
  private void telegramClicked(View paramView)
  {
    ClipboardTools.copy("https://t.me/imcash_oversea", this);
    showToast("复制成功");
  }
  
  @Event({2131165872})
  private void twitterClicked(View paramView)
  {
    ClipboardTools.copy("IMCASH", this);
    showToast("复制成功");
  }
  
  @Event({2131165903})
  private void wechatClicked(View paramView)
  {
    ClipboardTools.copy("lMCASH", this);
    showToast("复制成功");
  }
  
  @Event({2131165906})
  private void weiboClicked(View paramView)
  {
    ClipboardTools.copy("Imcash", this);
    showToast("复制成功");
  }
  
  @Event({2131165907})
  private void weopenClicked(View paramView)
  {
    ClipboardTools.copy("Imcash", this);
    showToast("复制成功");
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1)
    {
      showToast(paramHttpInfo.mJsonObj.optString("msg", "提交成功"));
      finish();
    }
  }
  
  protected void initData()
  {
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("消息");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ConnectAct.this.toPage(MessageAct.class);
      }
    });
  }
  
  protected void initView()
  {
    setTitleStr("关于我们&在线客服");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ConnectAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */