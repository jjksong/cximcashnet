package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.IntegralDetail;
import com.imcash.gp.ui.adapter.IntegralDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296340)
public class IntegralDetailAct
  extends BaseListActivity
{
  protected int CountLimit = 20;
  protected IntegralDetailAdapter mAdapter;
  protected int mCurrentPage = 1;
  protected ArrayList<IntegralDetail> mDatas;
  
  protected void getDetail(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    paramJSONObject = (ArrayList)new Gson().fromJson(paramJSONObject.optJSONObject("data").optString("list"), new TypeToken() {}.getType());
    this.mCurrentPage += 1;
    if (paramJSONObject.size() < this.CountLimit) {
      closeUpState();
    } else {
      startRefreshAll();
    }
    if (HttpTypeEnum.Integral_Detail_Down == paramHttpTypeEnum)
    {
      this.mDatas.clear();
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
      stopDownRefresh();
    }
    else
    {
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
      stopUpRefresh();
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getDetail(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new IntegralDetailAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    setTitleStr("积分明细");
    initListView();
    startRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mCurrentPage = 1;
    postDetail(HttpTypeEnum.Integral_Detail_Down);
  }
  
  protected void onRefreshUp()
  {
    postDetail(HttpTypeEnum.Integral_Detail_Up);
  }
  
  protected void postDetail(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("pages", this.mCurrentPage);
    localRequestParams.put("page_num", this.CountLimit);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/IntegralDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */