package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.tools.MD5;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296337)
public class ForgetPwAct
  extends BaseActivity
{
  @ViewInject(2131165448)
  protected TextView get_verify;
  protected TimeCount mTimeCount;
  @ViewInject(2131165600)
  protected EditText new_pass;
  @ViewInject(2131165654)
  protected EditText phone;
  @ViewInject(2131165891)
  protected EditText verify;
  
  @Event({2131165275})
  private void bindClicked(View paramView)
  {
    if (11 != this.phone.getText().toString().length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    if (this.verify.getText().toString().length() == 0)
    {
      showToast("请输入正确的短信验证码");
      return;
    }
    if ((6 <= this.new_pass.getText().toString().length()) && (18 >= this.new_pass.getText().toString().length()))
    {
      postModify();
      return;
    }
    showToast("请输入正确的密码");
  }
  
  @Event({2131165448})
  private void verifyClicked(View paramView)
  {
    if (11 != this.phone.getText().toString().length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    startTimeCount();
    postVerify();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast("修改成功");
      finish();
      break;
    case ???: 
      showToast("短信发送成功");
    }
  }
  
  protected void initData()
  {
    setTitleStr("忘记密码");
    this.mTimeCount = new TimeCount(60000L, 1000L);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postModify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("password", MD5.encode(this.new_pass.getText().toString()));
    localRequestParams.put("phone", this.phone.getText().toString());
    localRequestParams.put("smscode", this.verify.getText().toString());
    postNet(HttpTypeEnum.ForgetPass, localRequestParams);
  }
  
  protected void postVerify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.phone.getText().toString());
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void startTimeCount()
  {
    this.get_verify.setClickable(false);
    this.mTimeCount.start();
  }
  
  protected void updatePage() {}
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      ForgetPwAct.this.get_verify.setClickable(true);
      ForgetPwAct.this.get_verify.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = ForgetPwAct.this.get_verify;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/ForgetPwAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */