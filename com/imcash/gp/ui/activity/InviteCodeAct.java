package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.ClipboardTools;
import com.imcash.gp.tools.ZXingCodeUtils;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296343)
public class InviteCodeAct
  extends BaseActivity
{
  @ViewInject(2131165507)
  protected TextView invite_id;
  protected Bitmap mQrBitmap;
  @ViewInject(2131165605)
  protected TextView nickname;
  @ViewInject(2131165682)
  protected ImageView qr_code;
  
  @Event({2131165378})
  private void copyInviteId(View paramView)
  {
    ClipboardTools.copy(this.mApplication.getDataCore().getUserInfo().angel_num, this);
    showToast("复制成功");
  }
  
  protected void getInvite(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONObject("data").optString("url", "");
    this.mQrBitmap = ZXingCodeUtils.getInstance().createQRCode(paramJSONObject, this.qr_code.getWidth(), this.qr_code.getHeight());
    this.qr_code.setImageBitmap(this.mQrBitmap);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getInvite(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.invite_id.setText(this.mApplication.getDataCore().getUserInfo().angel_num);
    postInvite();
  }
  
  protected void initView()
  {
    setTitleStr("邀请");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Bitmap localBitmap = this.mQrBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled()))
    {
      this.mQrBitmap.recycle();
      this.mQrBitmap = null;
      System.gc();
    }
  }
  
  protected void postInvite()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Invite_Url, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/InviteCodeAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */