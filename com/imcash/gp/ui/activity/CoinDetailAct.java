package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment;
import com.github.mikephil.charting.components.Legend.LegendOrientation;
import com.github.mikephil.charting.components.Legend.LegendVerticalAlignment;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.LineDataSet.Mode;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.CoinInfo;
import com.imcash.gp.model.FourHour;
import com.imcash.gp.model.FourHour.FourHourPriceBean;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.model.WalletRecord;
import com.imcash.gp.ui.adapter.WalletDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.x;

@ContentView(2131296326)
public class CoinDetailAct
  extends BaseListActivity
{
  public static final String CoinDetailKey = "CoinDetailKey";
  protected LinearLayout chat_main_layout;
  protected TextView cny_money;
  protected ImageView coin_img;
  protected TextView coin_money;
  protected TextView coin_type;
  protected TextView currency_name;
  private YAxis leftYAxis;
  private Legend legend;
  private LimitLine limitLine;
  private LineChart lineChart;
  protected WalletDetailAdapter mAdapter;
  protected String mClickedMsg = null;
  protected CoinInfo mCoinInfo;
  protected ArrayList<WalletRecord> mDatas;
  protected FourHour mFourHourData;
  protected View mHeadView;
  private YAxis rightYaxis;
  protected TextView u_money;
  private XAxis xAxis;
  
  private void initChart(LineChart paramLineChart)
  {
    paramLineChart.setDrawGridBackground(false);
    paramLineChart.setDrawBorders(true);
    paramLineChart.setDragEnabled(false);
    paramLineChart.setTouchEnabled(false);
    this.xAxis = paramLineChart.getXAxis();
    this.leftYAxis = paramLineChart.getAxisLeft();
    this.rightYaxis = paramLineChart.getAxisRight();
    this.xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    this.xAxis.setAxisMinimum(0.0F);
    this.xAxis.setGranularity(1.0F);
    this.leftYAxis.setAxisMinimum(0.0F);
    this.rightYaxis.setAxisMinimum(0.0F);
    this.legend = paramLineChart.getLegend();
    this.legend.setForm(Legend.LegendForm.LINE);
    this.legend.setTextSize(12.0F);
    this.legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
    this.legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
    this.legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
    this.legend.setDrawInside(false);
    this.xAxis.setDrawGridLines(false);
    this.rightYaxis.setDrawGridLines(false);
    this.leftYAxis.setDrawGridLines(true);
    this.rightYaxis.setEnabled(false);
    this.legend.setEnabled(false);
    Description localDescription = new Description();
    localDescription.setEnabled(false);
    paramLineChart.setDescription(localDescription);
    this.xAxis.setTextColor(Color.rgb(144, 144, 144));
    this.leftYAxis.setTextColor(Color.rgb(144, 144, 144));
    this.xAxis.setValueFormatter(new IAxisValueFormatter()
    {
      public String getFormattedValue(float paramAnonymousFloat, AxisBase paramAnonymousAxisBase)
      {
        return ((FourHour.FourHourPriceBean)CoinDetailAct.this.mFourHourData.getFour_hour_price().get((int)paramAnonymousFloat % CoinDetailAct.this.mFourHourData.getFour_hour_price().size())).getTime();
      }
    });
    this.leftYAxis.setValueFormatter(new IAxisValueFormatter()
    {
      public String getFormattedValue(float paramAnonymousFloat, AxisBase paramAnonymousAxisBase)
      {
        int i = Float.valueOf(paramAnonymousFloat / 0.1F).intValue();
        paramAnonymousAxisBase = new StringBuilder();
        paramAnonymousAxisBase.append("$");
        paramAnonymousAxisBase.append(GlobalFunction.doubleToString(CoinDetailAct.this.mFourHourData.getShowPriceByIndex(i)));
        return paramAnonymousAxisBase.toString();
      }
    });
    this.leftYAxis.setLabelCount(8);
  }
  
  private void initLineDataSet(LineDataSet paramLineDataSet, int paramInt, LineDataSet.Mode paramMode)
  {
    paramLineDataSet.setColor(paramInt);
    paramLineDataSet.setCircleColor(paramInt);
    paramLineDataSet.setLineWidth(3.0F);
    paramLineDataSet.setCircleRadius(3.0F);
    paramLineDataSet.setDrawCircleHole(false);
    paramLineDataSet.setValueTextSize(10.0F);
    paramLineDataSet.setDrawFilled(false);
    paramLineDataSet.setFillColor(Color.rgb(215, 227, 255));
    paramLineDataSet.setFormLineWidth(1.0F);
    paramLineDataSet.setFormSize(15.0F);
    paramLineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
    this.lineChart.setBackgroundColor(-1);
    this.lineChart.setDrawBorders(false);
    paramLineDataSet.setDrawCircles(false);
    paramLineDataSet.setDrawValues(false);
  }
  
  @Event({2131165586})
  private void moneyIn(View paramView)
  {
    paramView = this.mClickedMsg;
    if (paramView == null)
    {
      showToast("数据加载中");
      return;
    }
    if (paramView.length() != 0)
    {
      showToast(this.mClickedMsg);
      return;
    }
    Wallet localWallet = new Wallet();
    localWallet.setCurrency_id(this.mCoinInfo.getId());
    localWallet.setName(this.mCoinInfo.getCoin());
    localWallet.setNum(this.mCoinInfo.getBalance());
    paramView = new Intent();
    paramView.putExtra("WalletDataKey", new Gson().toJson(localWallet));
    toPage(paramView, RechargeAct.class);
  }
  
  @Event({2131165587})
  private void moneyOut(View paramView)
  {
    if (this.mClickedMsg == null)
    {
      showToast("数据加载中");
      return;
    }
    Wallet localWallet = new Wallet();
    localWallet.setCurrency_id(this.mCoinInfo.getId());
    localWallet.setName(this.mCoinInfo.getCoin());
    localWallet.setNum(this.mCoinInfo.getBalance());
    paramView = new Intent();
    paramView.putExtra("WalletDataKey", new Gson().toJson(localWallet));
    toPage(paramView, WithdrawalAct.class);
  }
  
  @Event({2131165850})
  private void transferClicked(View paramView)
  {
    paramView = new Intent();
    paramView.putExtra("CoinInfoKey", new Gson().toJson(this.mCoinInfo));
    toPage(paramView, CoinTransferAct.class);
  }
  
  protected void fourHour()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mCoinInfo.getId());
    postNet(HttpTypeEnum.Wallet_FourHour, localRequestParams);
  }
  
  protected void getFourHour(JSONObject paramJSONObject)
  {
    try
    {
      Object localObject = new com/google/gson/Gson;
      ((Gson)localObject).<init>();
      if (!"USDT".equals(this.mCoinInfo.getCoin()))
      {
        this.mFourHourData = ((FourHour)((Gson)localObject).fromJson(paramJSONObject.optString("data", ""), FourHour.class));
        paramJSONObject = this.coin_money;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("当前币价≈");
        ((StringBuilder)localObject).append(this.mFourHourData.getPrice());
        ((StringBuilder)localObject).append("USDT");
        paramJSONObject.setText(((StringBuilder)localObject).toString());
        showLineChart(this.mFourHourData.getLineShow(), "我的收益", this.mFourHourData.getLineaColor());
      }
      return;
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  protected void getOneKindCoin(JSONObject paramJSONObject)
  {
    try
    {
      paramJSONObject = paramJSONObject.optJSONObject("data");
      this.mClickedMsg = paramJSONObject.optString("click", "");
      this.mCoinInfo.setId(paramJSONObject.optString("currency_id", "0"));
      this.mCoinInfo.setCny(paramJSONObject.optString("cny", "0.00"));
      this.mCoinInfo.setUsd(paramJSONObject.optString("usd", "0.00"));
      updataTop();
      return;
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  protected void getRecord(JSONObject paramJSONObject)
  {
    try
    {
      Gson localGson = new com/google/gson/Gson;
      localGson.<init>();
      paramJSONObject = paramJSONObject.optString("data");
      this.mDatas.clear();
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
      {
        TypeToken local3 = new com/imcash/gp/ui/activity/CoinDetailAct$3;
        local3.<init>(this);
        paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, local3.getType());
        if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
          this.mDatas.addAll(paramJSONObject);
        }
      }
      stopDownRefresh();
      this.mAdapter.notifyDataSetChanged();
      this.mListView.invalidate();
      return;
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getFourHour(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getOneKindCoin(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRecord(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    try
    {
      String str = (String)GetIntentData("CoinDetailKey");
      Gson localGson = new com/google/gson/Gson;
      localGson.<init>();
      this.mCoinInfo = ((CoinInfo)localGson.fromJson(str, CoinInfo.class));
      this.mDatas = new ArrayList();
      this.mAdapter = new WalletDetailAdapter(this, this.mDatas);
      this.mListView.setAdapter(this.mAdapter);
      if (!"USDT".equals(this.mCoinInfo.getCoin()))
      {
        initChart(this.lineChart);
        this.chat_main_layout.setVisibility(0);
      }
      else
      {
        this.chat_main_layout.setVisibility(8);
      }
      this.coin_img.setImageResource(GlobalFunction.getCoinImgByName(this.mCoinInfo.getCoin()));
      onRefreshDown();
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296328, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.chat_main_layout = ((LinearLayout)this.mHeadView.findViewById(2131165329));
    this.coin_img = ((ImageView)this.mHeadView.findViewById(2131165352));
    this.currency_name = ((TextView)this.mHeadView.findViewById(2131165389));
    this.coin_type = ((TextView)this.mHeadView.findViewById(2131165357));
    this.u_money = ((TextView)this.mHeadView.findViewById(2131165875));
    this.cny_money = ((TextView)this.mHeadView.findViewById(2131165348));
    this.coin_money = ((TextView)this.mHeadView.findViewById(2131165355));
    this.lineChart = ((LineChart)this.mHeadView.findViewById(2131165535));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    postRecord();
    postOneKindCoin();
    fourHour();
  }
  
  protected void onRefreshUp() {}
  
  public void onResume()
  {
    super.onResume();
  }
  
  protected void postOneKindCoin()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mCoinInfo.getId());
    postNet(HttpTypeEnum.OneKindCoin, localRequestParams);
  }
  
  protected void postRecord()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", this.mCoinInfo.getId());
    postNet(HttpTypeEnum.WalletRecord_Type, localRequestParams);
  }
  
  public void showLineChart(List<Double> paramList, String paramString, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramList.size(); i++)
    {
      float f = i;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramList.get(i));
      localStringBuilder.append("");
      localArrayList.add(new Entry(f, Float.valueOf(localStringBuilder.toString()).floatValue()));
    }
    paramList = new LineDataSet(localArrayList, paramString);
    initLineDataSet(paramList, paramInt, LineDataSet.Mode.LINEAR);
    paramList = new LineData(new ILineDataSet[] { paramList });
    this.lineChart.setData(paramList);
  }
  
  protected void updataTop()
  {
    Object localObject = this.mCoinInfo;
    if (localObject == null) {
      return;
    }
    this.coin_img.setImageResource(GlobalFunction.getCoinImgByName(((CoinInfo)localObject).getCoin()));
    this.currency_name.setText(this.mCoinInfo.getCoin());
    this.coin_type.setText(this.mCoinInfo.getCoin());
    this.u_money.setText(this.mCoinInfo.getBalance());
    localObject = this.cny_money;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("≈");
    localStringBuilder.append(this.mCoinInfo.getCny());
    localStringBuilder.append("CNY");
    ((TextView)localObject).setText(localStringBuilder.toString());
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/CoinDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */