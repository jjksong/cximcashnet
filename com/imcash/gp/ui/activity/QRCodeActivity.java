package com.imcash.gp.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.BarcodeView;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;
import java.util.Arrays;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296373)
public class QRCodeActivity
  extends Activity
{
  private DecoratedBarcodeView barcodeView;
  private BarcodeCallback callback = new BarcodeCallback()
  {
    public void barcodeResult(BarcodeResult paramAnonymousBarcodeResult)
    {
      if (paramAnonymousBarcodeResult.getText() == null) {
        return;
      }
      QRCodeActivity.this.barcodeView.setStatusText(paramAnonymousBarcodeResult.getText());
      QRCodeActivity.this.parseQRCode(paramAnonymousBarcodeResult.getText(), new Bundle());
    }
    
    public void possibleResultPoints(List<ResultPoint> paramAnonymousList) {}
  };
  
  private void parseQRCode(String paramString, Bundle paramBundle)
  {
    Log.v("result", paramString);
    EventBus.getDefault().post(paramString);
    finish();
  }
  
  protected void initData() {}
  
  protected void initView()
  {
    this.barcodeView = ((DecoratedBarcodeView)findViewById(2131165258));
    List localList = Arrays.asList(new BarcodeFormat[] { BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39 });
    this.barcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(localList));
    this.barcodeView.decodeContinuous(this.callback);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onPause()
  {
    super.onPause();
    this.barcodeView.pause();
  }
  
  protected void onResume()
  {
    super.onResume();
    this.barcodeView.resume();
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/QRCodeActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */