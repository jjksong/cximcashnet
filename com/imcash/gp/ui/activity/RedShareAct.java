package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.tools.ZXingCodeUtils;
import com.imcash.gp.ui.base.BaseActivity;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296382)
public class RedShareAct
  extends BaseActivity
{
  public static final String Bottom_Msg = "Bottom_Msg";
  public static final String IsNewUser = "IsNewUser";
  public static final String Key_Wrod = "Key_Wrod";
  public static final String Qr_URL = "Qr_URL";
  public static final String Total_Str = "Total_Str";
  @ViewInject(2131165295)
  protected TextView bottom_tv;
  protected int heightPixels = 0;
  protected int imgHeight = 1000;
  protected int imgWidth = 563;
  protected Bitmap mQrBitmap;
  @ViewInject(2131165602)
  protected ImageView new_user_img;
  @ViewInject(2131165621)
  protected TextView open_tv;
  @ViewInject(2131165683)
  protected ImageView qr_img;
  @ViewInject(2131165702)
  protected RelativeLayout red_number_layout;
  @ViewInject(2131165846)
  protected TextView total_money;
  protected int widthPixels = 0;
  @ViewInject(2131165911)
  protected TextView word_type;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    Object localObject = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics((DisplayMetrics)localObject);
    this.widthPixels = ((DisplayMetrics)localObject).widthPixels;
    this.heightPixels = ((DisplayMetrics)localObject).heightPixels;
    if (GetIntentData("IsNewUser") == null) {
      this.new_user_img.setVisibility(8);
    } else {
      this.new_user_img.setVisibility(0);
    }
    localObject = new LinearLayout.LayoutParams(-2, -2);
    double d = this.heightPixels;
    Double.isNaN(d);
    ((LinearLayout.LayoutParams)localObject).setMargins(0, (int)(d * 0.339D), 0, 0);
    ((LinearLayout.LayoutParams)localObject).gravity = 17;
    this.red_number_layout.setLayoutParams((ViewGroup.LayoutParams)localObject);
    this.open_tv.setText("打开\"ImCash-资产-红包\"，输入上方口令抢红包");
    localObject = new LinearLayout.LayoutParams(-2, -2);
    d = this.heightPixels;
    Double.isNaN(d);
    ((LinearLayout.LayoutParams)localObject).setMargins(0, (int)(d * 0.176D), 0, 0);
    ((LinearLayout.LayoutParams)localObject).gravity = 17;
    this.total_money.setLayoutParams((ViewGroup.LayoutParams)localObject);
    localObject = new LinearLayout.LayoutParams(-2, -2);
    d = this.heightPixels;
    Double.isNaN(d);
    ((LinearLayout.LayoutParams)localObject).setMargins(0, (int)(d * 0.046D), 0, 0);
    ((LinearLayout.LayoutParams)localObject).gravity = 17;
    this.qr_img.setLayoutParams((ViewGroup.LayoutParams)localObject);
    this.mQrBitmap = ZXingCodeUtils.getInstance().createQRCode((String)GetIntentData("Qr_URL"), 200, 200);
    this.qr_img.setImageBitmap(this.mQrBitmap);
    this.word_type.setText((String)GetIntentData("Key_Wrod"));
    this.total_money.setText((String)GetIntentData("Total_Str"));
    this.bottom_tv.setText((String)GetIntentData("Bottom_Msg"));
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Bitmap localBitmap = this.mQrBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled()))
    {
      this.mQrBitmap.recycle();
      this.mQrBitmap = null;
      System.gc();
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedShareAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */