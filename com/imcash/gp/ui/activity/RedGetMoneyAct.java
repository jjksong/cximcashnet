package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.RedDetail;
import com.imcash.gp.ui.adapter.RedRecordDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296338)
public class RedGetMoneyAct
  extends BaseListActivity
{
  public static final String GetMoney_Key = "GetMoney_Key";
  @ViewInject(2131165358)
  protected TextView coint_type;
  protected RedRecordDetailAdapter mAdapter;
  protected ArrayList<RedDetail> mDatas;
  @ViewInject(2131165603)
  protected LinearLayout new_user_layout;
  
  @Event({2131165760})
  private void sendNewReg(View paramView)
  {
    postNewRegistRed();
  }
  
  protected void getNetRegistRed(JSONObject paramJSONObject)
  {
    stopLoad();
    JSONObject localJSONObject = BaseModel.getDataJson(paramJSONObject);
    paramJSONObject = new Intent();
    paramJSONObject.putExtra("Key_Wrod", localJSONObject.optString("keyword", ""));
    paramJSONObject.putExtra("Total_Str", localJSONObject.optString("total_amount", ""));
    paramJSONObject.putExtra("Qr_URL", localJSONObject.optString("url", ""));
    paramJSONObject.putExtra("Bottom_Msg", localJSONObject.optString("content", ""));
    toPage(paramJSONObject, RedShareAct.class);
    finish();
  }
  
  protected void getRedMoney(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      if (paramJSONObject.optInt("is_send", 0) == 0) {
        this.new_user_layout.setVisibility(8);
      } else {
        this.new_user_layout.setVisibility(0);
      }
      return;
    }
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getNetRegistRed(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getRedMoney(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    if ((String)GetIntentData("GetMoney_Key") != null)
    {
      Object localObject1 = (String)GetIntentData("GetMoney_Key");
      Object localObject2 = new Gson();
      if ((localObject1 != null) && (((String)localObject1).length() != 0))
      {
        localObject1 = (ArrayList)((Gson)localObject2).fromJson((String)localObject1, new TypeToken() {}.getType());
        if ((localObject1 != null) && (((ArrayList)localObject1).size() > 0)) {
          this.mDatas.addAll((Collection)localObject1);
        }
      }
      if (this.mDatas.size() != 0)
      {
        localObject1 = (RedDetail)this.mDatas.get(0);
        TextView localTextView = this.coint_type;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append(((RedDetail)localObject1).getAmount());
        ((StringBuilder)localObject2).append(((RedDetail)localObject1).getCoin_name());
        localTextView.setText(((StringBuilder)localObject2).toString());
      }
      this.mAdapter = new RedRecordDetailAdapter(this, this.mDatas);
      this.mListView.setAdapter(this.mAdapter);
      this.mAdapter.notifyDataSetChanged();
    }
    this.new_user_layout.setVisibility(8);
  }
  
  protected void initView()
  {
    setTitleStr("抢到红包");
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postNewRegistRed()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_CreatePacket, localRequestParams);
  }
  
  protected void postRedGetMoney()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_NewIsCreate, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedGetMoneyAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */