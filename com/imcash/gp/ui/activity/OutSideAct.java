package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.OutSide;
import com.imcash.gp.tools.ClipboardTools;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296368)
public class OutSideAct
  extends BaseActivity
{
  @ViewInject(2131165250)
  protected TextView balance;
  @ViewInject(2131165255)
  protected TextView bank_number;
  @ViewInject(2131165283)
  protected TextView bond;
  @ViewInject(2131165328)
  protected TextView chat_id;
  @ViewInject(2131165356)
  protected TextView coin_name;
  @ViewInject(2131165425)
  protected TextView ex_count;
  protected OutSide mOutSide;
  protected Bitmap mQrBitmap;
  @ViewInject(2131165622)
  protected TextView order_count;
  @ViewInject(2131165639)
  protected TextView out_time;
  @ViewInject(2131165647)
  protected ImageView pay_qr_img;
  @ViewInject(2131165886)
  protected TextView user_name;
  
  @Event({2131165231})
  private void aliQr(View paramView)
  {
    if (this.mOutSide == null) {
      return;
    }
    ((RequestBuilder)((RequestBuilder)Glide.with(this).load(this.mOutSide.getAlipay_qrcode()).placeholder(2131099884)).diskCacheStrategy(DiskCacheStrategy.ALL)).into(this.pay_qr_img);
  }
  
  @Event({2131165246})
  private void backClicked(View paramView)
  {
    finish();
  }
  
  @Event({2131165731})
  private void bankCopy(View paramView)
  {
    if (this.mOutSide == null) {
      return;
    }
    paramView = new StringBuilder();
    paramView.append("银行卡：");
    paramView.append(this.mOutSide.getBank_card());
    ClipboardTools.copy(paramView.toString(), this);
    showToast("复制成功");
  }
  
  @Event({2131165734})
  private void saveWeChatid(View paramView)
  {
    paramView = this.mOutSide;
    if (paramView == null) {
      return;
    }
    ClipboardTools.copy(paramView.getWechat(), this);
    showToast("复制成功");
  }
  
  @Event({2131165904})
  private void wechatQr(View paramView)
  {
    if (this.mOutSide == null) {
      return;
    }
    ((RequestBuilder)((RequestBuilder)Glide.with(this).load(this.mOutSide.getWechat_qrcode()).placeholder(2131099884)).diskCacheStrategy(DiskCacheStrategy.ALL)).into(this.pay_qr_img);
  }
  
  protected void getOutSide(JSONObject paramJSONObject)
  {
    this.mOutSide = ((OutSide)new Gson().fromJson(paramJSONObject.optString("data"), OutSide.class));
    updatePage();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (2.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getOutSide(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("OTC交易");
    ImageView localImageView = (ImageView)findViewById(2131165715);
    localImageView.setImageResource(2131099944);
    localImageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        OutSideAct.this.finish();
      }
    });
    postOutSide();
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postOutSide()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.OutSide, localRequestParams);
  }
  
  protected void updatePage()
  {
    Object localObject1 = this.mOutSide;
    if (localObject1 == null) {
      return;
    }
    this.user_name.setText(((OutSide)localObject1).getMerchant());
    this.chat_id.setText(this.mOutSide.getWechat());
    Object localObject2 = this.order_count;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("成交量:");
    ((StringBuilder)localObject1).append(this.mOutSide.getWeight());
    ((StringBuilder)localObject1).append("单");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = this.coin_name;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("支持币种:");
    ((StringBuilder)localObject1).append(this.mOutSide.getCoin());
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = this.bond;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("已缴纳保证金：");
    ((StringBuilder)localObject1).append(this.mOutSide.getBond());
    ((StringBuilder)localObject1).append("USDT");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = this.ex_count;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("单笔承兑数量：");
    ((StringBuilder)localObject1).append(this.mOutSide.getMin());
    ((StringBuilder)localObject1).append("USDT---");
    ((StringBuilder)localObject1).append(this.mOutSide.getMax());
    ((StringBuilder)localObject1).append("USDT");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = this.out_time;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("承兑放行时间：");
    ((StringBuilder)localObject1).append(this.mOutSide.getMinute());
    ((StringBuilder)localObject1).append("分钟内到账");
    ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = this.bank_number;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("银行卡：");
    ((StringBuilder)localObject2).append(this.mOutSide.getBank_card());
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject1 = this.balance;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("账户余额：");
    ((StringBuilder)localObject2).append(this.mOutSide.getBalance());
    ((StringBuilder)localObject2).append("USDT");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    ((RequestBuilder)((RequestBuilder)Glide.with(this).load(this.mOutSide.getAlipay_qrcode()).placeholder(2131099884)).diskCacheStrategy(DiskCacheStrategy.ALL)).into(this.pay_qr_img);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/OutSideAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */