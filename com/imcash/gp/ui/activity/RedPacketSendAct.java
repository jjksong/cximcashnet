package com.imcash.gp.ui.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.Wallet;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296383)
public class RedPacketSendAct
  extends BaseActivity
{
  @ViewInject(2131165349)
  protected TextView coin_all;
  @ViewInject(2131165357)
  protected TextView coin_type;
  @ViewInject(2131165379)
  protected EditText count;
  @ViewInject(2131165447)
  protected EditText get_number;
  protected ArrayList<Wallet> mDatas;
  @ViewInject(2131165583)
  protected EditText money_count;
  
  @Event({2131165761})
  private void sendClicked(View paramView)
  {
    if (this.money_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的总额");
      return;
    }
    if (this.count.getText().toString().length() == 0)
    {
      showToast("请输入正确的数量");
      return;
    }
    postSendMsg();
  }
  
  @Event({2131165873})
  private void typeClicked(View paramView)
  {
    paramView = this.mDatas;
    if ((paramView != null) && (paramView.size() != 0))
    {
      paramView = new String[this.mDatas.size()];
      for (int i = 0; i < this.mDatas.size(); i++) {
        paramView[i] = ((Wallet)this.mDatas.get(i)).getName();
      }
      new AlertDialog.Builder(this).setTitle("选择币种").setItems(paramView, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          RedPacketSendAct.this.coin_type.setText(((Wallet)RedPacketSendAct.this.mDatas.get(paramAnonymousInt)).getName());
          RedPacketSendAct.this.updataAllMoney();
        }
      }).create().show();
      return;
    }
  }
  
  protected void getCoinInfo(JSONObject paramJSONObject)
  {
    stopLoad();
    Gson localGson = new Gson();
    try
    {
      paramJSONObject = paramJSONObject.optString("data");
      this.mDatas.clear();
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
      {
        TypeToken local3 = new com/imcash/gp/ui/activity/RedPacketSendAct$3;
        local3.<init>(this);
        paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, local3.getType());
        if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
          this.mDatas.addAll(paramJSONObject);
        }
      }
      return;
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  protected String getCurrentId()
  {
    String str = "";
    Iterator localIterator = this.mDatas.iterator();
    do
    {
      localObject = str;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject = (Wallet)localIterator.next();
    } while (!((Wallet)localObject).getName().equals(this.coin_type.getText().toString()));
    Object localObject = ((Wallet)localObject).getCurrency_id();
    return (String)localObject;
  }
  
  protected void getRedSend(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    Intent localIntent = new Intent();
    localIntent.putExtra("Key_Wrod", paramJSONObject.optString("keyword", ""));
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.money_count.getText().toString());
    localStringBuilder.append(this.coin_type.getText().toString());
    localIntent.putExtra("Total_Str", localStringBuilder.toString());
    localIntent.putExtra("Qr_URL", paramJSONObject.optString("url", ""));
    localIntent.putExtra("Bottom_Msg", paramJSONObject.optString("content", ""));
    toPage(localIntent, RedShareAct.class);
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getRedSend(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      getCoinInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    postCoinInfo();
    this.mDatas = new ArrayList();
    this.coin_type.setText("BFC");
    this.money_count.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        RedPacketSendAct.this.updataAllMoney();
      }
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    });
  }
  
  protected void initView()
  {
    setTitleStr("口令红包");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postCoinInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Red_CoinInfo, localRequestParams);
  }
  
  protected void postSendMsg()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", getCurrentId());
    localRequestParams.put("keyword", this.get_number.getText().toString());
    localRequestParams.put("bag_amount", this.money_count.getText().toString());
    localRequestParams.put("number", this.count.getText().toString());
    postNet(HttpTypeEnum.Red_Send, localRequestParams);
  }
  
  protected void updataAllMoney()
  {
    Object localObject2;
    Object localObject1;
    if (this.money_count.getText().toString().length() == 0)
    {
      localObject2 = this.coin_all;
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("0.00");
      ((StringBuilder)localObject1).append(this.coin_type.getText().toString());
      ((TextView)localObject2).setText(((StringBuilder)localObject1).toString());
    }
    else
    {
      localObject1 = this.coin_all;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(this.money_count.getText().toString());
      ((StringBuilder)localObject2).append(this.coin_type.getText().toString());
      ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RedPacketSendAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */