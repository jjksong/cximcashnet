package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.PersonalInfo;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.imcash.gp.ui.view.MessageDialog.OnBtnListener;
import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.citywheel.CityConfig.Builder;
import com.lljjcoder.citywheel.CityConfig.WheelType;
import com.lljjcoder.style.citypickerview.CityPickerView;
import com.loopj.android.http.RequestParams;
import java.util.Date;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296370)
public class PersonalAct
  extends BaseActivity
{
  @ViewInject(2131165199)
  protected TextView account;
  @ViewInject(2131165280)
  protected TextView birthday;
  @ViewInject(2131165509)
  protected TextView is_realname;
  @ViewInject(2131165553)
  protected TextView location;
  protected String mCity = "北京";
  protected PersonalInfo mPersonalInfo;
  CityPickerView mPicker = new CityPickerView();
  protected String mProvince = "直辖市";
  @ViewInject(2131165605)
  protected TextView nickname;
  @ViewInject(2131165765)
  protected TextView sex;
  @ViewInject(2131165798)
  protected LinearLayout super_layout;
  
  @Event({2131165281})
  private void birthdayClicked(View paramView) {}
  
  @Event({2131165554})
  private void locationClicket(View paramView)
  {
    paramView = new CityConfig.Builder().title("选择地区").titleBackgroundColor("#E9E9E9").titleTextSize(18).titleTextColor("#585858").confirTextColor("#101D37").cancelTextColor("#b1b1b1").visibleItemsCount(8).provinceCyclic(false).cityCyclic(false).showBackground(true).districtCyclic(false).setCityWheelType(CityConfig.WheelType.PRO_CITY).build();
    this.mPicker.setConfig(paramView);
    this.mPicker.setOnCityItemClickListener(new OnCityItemClickListener()
    {
      public void onCancel() {}
      
      public void onSelected(ProvinceBean paramAnonymousProvinceBean, CityBean paramAnonymousCityBean, DistrictBean paramAnonymousDistrictBean)
      {
        PersonalAct.this.mProvince = paramAnonymousProvinceBean.getName();
        PersonalAct.this.mCity = paramAnonymousCityBean.getName();
        PersonalAct.this.changeLocation();
      }
    });
    this.mPicker.showCityPicker();
  }
  
  @Event({2131165606})
  private void nickNameClicked(View paramView) {}
  
  @Event({2131165685})
  private void quitClicked(View paramView)
  {
    showBtnBox(this, "是否要退出账号？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        PersonalAct.this.postQuitMsg();
      }
    });
  }
  
  @Event({2131165730})
  private void safeClicked(View paramView)
  {
    if (this.mPersonalInfo == null) {
      return;
    }
    paramView = new Intent();
    paramView.putExtra("UserInfoKey", this.mPersonalInfo.mJsonData);
    toPage(paramView, SafeAct.class);
  }
  
  @Event({2131165766})
  private void sexClicked(View paramView)
  {
    showToast("暂不支持修改");
  }
  
  @Event({2131165798})
  private void superClicked(View paramView)
  {
    toPage(SuperAct.class);
  }
  
  @Event({2131165893})
  private void verifyClicked(View paramView)
  {
    paramView = this.mPersonalInfo;
    if ((paramView != null) && (!paramView.isRealName()))
    {
      toPage(VerifyUserAct.class);
      return;
    }
  }
  
  protected void changeLocation()
  {
    TextView localTextView = this.location;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mProvince);
    localStringBuilder.append(this.mCity);
    localTextView.setText(localStringBuilder.toString());
    postLocation();
  }
  
  protected void getBirthday(Date paramDate)
  {
    paramDate = GlobalFunction.dateToString(paramDate, "yyyy-MM-dd");
    this.birthday.setText(paramDate);
    postBirthday(paramDate);
  }
  
  protected void getPsersonalInfo(JSONObject paramJSONObject)
  {
    this.mPersonalInfo = new PersonalInfo(paramJSONObject);
    updataUserUi();
  }
  
  protected void getQuitMsg()
  {
    stopLoad();
    this.mApplication.getDataCore().cleanCacheData();
    Intent localIntent = new Intent(this, LoginAct.class);
    localIntent.setFlags(268468224);
    startActivity(localIntent);
  }
  
  protected void getSex()
  {
    stopLoad();
    postPersonalInfo();
  }
  
  @Subscribe(threadMode=ThreadMode.MAIN)
  public void getVerifyId(String paramString)
  {
    showMessageBox(this, "恭喜您获得BFC量子基金1日持仓体验，请至持仓明细中查看。", "", new MessageDialog.OnBtnListener()
    {
      public void onClick() {}
    }, "立即体验");
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getQuitMsg();
      break;
    case ???: 
      getSex();
      break;
    case ???: 
      getPsersonalInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("个人中心");
    this.mPicker.init(this);
    this.super_layout.setVisibility(8);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    EventBus.getDefault().register(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }
  
  public void onResume()
  {
    super.onResume();
    postPersonalInfo();
  }
  
  protected void postBirthday(String paramString)
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("birthday", paramString);
    postNet(HttpTypeEnum.ModifyPersonal_Type, localRequestParams);
  }
  
  protected void postLocation()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mProvince);
    localStringBuilder.append(this.mCity);
    localRequestParams.put("address", localStringBuilder.toString());
    postNet(HttpTypeEnum.ModifyPersonal_Type, localRequestParams);
  }
  
  protected void postPersonalInfo()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Personal_Type, localRequestParams);
  }
  
  protected void postQuitMsg()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.PostQuitMsg, localRequestParams);
  }
  
  protected void postSex(int paramInt)
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("sex", paramInt);
    postNet(HttpTypeEnum.ModifyPersonal_Type, localRequestParams);
  }
  
  protected void updataUserUi()
  {
    PersonalInfo localPersonalInfo = this.mPersonalInfo;
    if (localPersonalInfo == null) {
      return;
    }
    this.account.setText(localPersonalInfo.getHidePhone());
    this.nickname.setText(this.mPersonalInfo.nickname);
    this.sex.setText(this.mPersonalInfo.getSexStr());
    this.birthday.setText(this.mPersonalInfo.birthday);
    this.location.setText(this.mPersonalInfo.address);
    if (this.mPersonalInfo.isRealName()) {
      this.is_realname.setText("已实名");
    } else {
      this.is_realname.setText("未实名");
    }
    if (this.mPersonalInfo.isSuper()) {
      this.super_layout.setVisibility(0);
    } else {
      this.super_layout.setVisibility(8);
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/PersonalAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */