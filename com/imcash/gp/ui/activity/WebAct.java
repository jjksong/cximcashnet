package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.ui.base.BaseActivity;

public class WebAct
  extends BaseActivity
  implements View.OnClickListener
{
  public static final String TITLE_KEY = "title_key";
  public static final String WEBPAGE_KEY = "web_key";
  protected WebView webView;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData() {}
  
  protected void initView()
  {
    ((ImageView)findViewById(2131165248)).setOnClickListener(this);
    ((TextView)findViewById(2131165822)).setText(getIntent().getExtras().getString("title_key"));
    getIntent().getExtras().getString("web_key");
    this.webView = ((WebView)findViewById(2131165901));
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165248) {
      finish();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296492);
    getWindow().setFormat(-3);
    getWindow().setSoftInputMode(18);
    initView();
    initData();
    updatePage();
  }
  
  protected void updatePage()
  {
    String str = getIntent().getExtras().getString("web_key");
    WebSettings localWebSettings = this.webView.getSettings();
    localWebSettings.setJavaScriptEnabled(true);
    localWebSettings.setSupportMultipleWindows(false);
    localWebSettings.setDisplayZoomControls(false);
    localWebSettings.setBuiltInZoomControls(true);
    localWebSettings.setSupportZoom(true);
    localWebSettings.setDomStorageEnabled(true);
    localWebSettings.setUseWideViewPort(true);
    localWebSettings.setDefaultFontSize(40);
    localWebSettings.setPluginState(WebSettings.PluginState.ON);
    localWebSettings.setAllowFileAccess(true);
    localWebSettings.setLoadWithOverviewMode(true);
    this.webView.loadUrl(str);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/WebAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */