package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.gson.Gson;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.OtcInfo;
import com.imcash.gp.model.OtcRecord;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296364)
public class OtcRecordDetailAct
  extends BaseActivity
{
  public static final String Otc_Info_Key = "Otc_Info_Key";
  @ViewInject(2131165403)
  protected Button detail_btn;
  protected OtcRecord mDatas;
  protected OtcInfo mOtcInfo;
  @ViewInject(2131165589)
  protected TextView money_show;
  @ViewInject(2131165624)
  protected TextView order_id;
  @ViewInject(2131165718)
  protected TextView right_tv;
  @ViewInject(2131165795)
  protected TextView status_tv;
  @ViewInject(2131165801)
  protected Button sure_out;
  @ViewInject(2131165820)
  protected TextView time_tv;
  @ViewInject(2131165874)
  protected TextView type_tv;
  
  @Event({2131165718})
  private void cancelClicked(View paramView)
  {
    showBtnBox(this, "是否要取消该比出金？", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        OtcRecordDetailAct.this.postCancel();
      }
    });
  }
  
  @Event({2131165403})
  private void detailClicked(View paramView)
  {
    if (this.mOtcInfo == null) {
      return;
    }
    paramView = new RequestParams();
    paramView.put("appkey", this.mOtcInfo.getAppkey());
    paramView.put("async_url", this.mOtcInfo.getAsync_url());
    paramView.put("coin_amount", this.mOtcInfo.getCoin_amount());
    paramView.put("coin_sign", this.mOtcInfo.getCoin_sign());
    paramView.put("company_order_num", this.mOtcInfo.getCompany_order_num());
    paramView.put("kyc", this.mOtcInfo.getKyc());
    paramView.put("order_time", this.mOtcInfo.getOrder_time());
    paramView.put("phone", this.mOtcInfo.getPhone());
    paramView.put("sync_url", this.mOtcInfo.getSync_url());
    paramView.put("username", this.mOtcInfo.getUsername());
    paramView.put("sign", this.mOtcInfo.getSign());
    Intent localIntent = new Intent();
    localIntent.putExtra("Post_Type", 0);
    localIntent.putExtra("title_key", "支付订单");
    localIntent.putExtra("Post_Data", paramView.toString());
    toPage(localIntent, OtcWebAct.class);
  }
  
  @Event({2131165801})
  private void suerOut(View paramView)
  {
    postContinueOut();
  }
  
  protected void getContinueOut(JSONObject paramJSONObject)
  {
    stopLoad();
    this.mOtcInfo = ((OtcInfo)new Gson().fromJson(paramJSONObject.optString("data", ""), OtcInfo.class));
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("appkey", this.mOtcInfo.getAppkey());
    localRequestParams.put("async_url", this.mOtcInfo.getAsync_url());
    localRequestParams.put("coin_amount", this.mOtcInfo.getCoin_amount());
    localRequestParams.put("coin_sign", this.mOtcInfo.getCoin_sign());
    localRequestParams.put("company_order_num", this.mOtcInfo.getCompany_order_num());
    localRequestParams.put("kyc", this.mOtcInfo.getKyc());
    localRequestParams.put("order_time", this.mOtcInfo.getOrder_time());
    localRequestParams.put("pay_card_bank", this.mOtcInfo.getPay_card_bank());
    localRequestParams.put("pay_card_num", this.mOtcInfo.getPay_card_num());
    localRequestParams.put("phone", this.mOtcInfo.getPhone());
    localRequestParams.put("sync_url", this.mOtcInfo.getSync_url());
    localRequestParams.put("username", this.mOtcInfo.getUsername());
    localRequestParams.put("sign", this.mOtcInfo.getSign());
    paramJSONObject = new Intent();
    paramJSONObject.putExtra("Post_Type", 1);
    paramJSONObject.putExtra("title_key", "出金订单");
    paramJSONObject.putExtra("Post_Data", localRequestParams.toString());
    toPage(paramJSONObject, OtcWebAct.class);
    finish();
  }
  
  protected void getDetailInfo(JSONObject paramJSONObject)
  {
    this.mOtcInfo = ((OtcInfo)new Gson().fromJson(BaseModel.getDataJson(paramJSONObject).optString("order", ""), OtcInfo.class));
    stopLoad();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getContinueOut(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      showToast("取消成功");
      finish();
      break;
    case ???: 
      getDetailInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("详细信息");
    this.right_tv.setText("取消出金");
    this.right_tv.setVisibility(8);
    this.sure_out.setVisibility(8);
    if (GetIntentData("Otc_Info_Key") != null) {
      this.mDatas = ((OtcRecord)new Gson().fromJson((String)GetIntentData("Otc_Info_Key"), OtcRecord.class));
    }
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void postCancel()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("record_id", this.mDatas.getRecord_id());
    postNet(HttpTypeEnum.Merchant_Cancel_Out, localRequestParams);
  }
  
  protected void postContinueOut()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("record_id", this.mDatas.getRecord_id());
    postNet(HttpTypeEnum.Merchant_Continue_Out, localRequestParams);
  }
  
  protected void postDetailInfo()
  {
    if (this.mDatas == null) {
      return;
    }
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("record_id", this.mDatas.getRecord_id());
    postNet(HttpTypeEnum.Merchant_Detail, localRequestParams);
  }
  
  protected void updatePage()
  {
    if (this.mDatas != null)
    {
      TextView localTextView = this.money_show;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(this.mDatas.getAmount());
      localStringBuilder.append(" USDT");
      localTextView.setText(localStringBuilder.toString());
      this.type_tv.setText(this.mDatas.getType());
      this.status_tv.setText(this.mDatas.getState());
      this.time_tv.setText(this.mDatas.getCreate_time());
      if (this.mDatas.showDetailBtc())
      {
        this.detail_btn.setVisibility(0);
        postDetailInfo();
      }
      else
      {
        this.detail_btn.setVisibility(8);
      }
      if ("出金".contains(this.mDatas.getType()))
      {
        this.detail_btn.setVisibility(8);
        if (this.mDatas.isOutCanCancel()) {
          this.right_tv.setVisibility(0);
        } else {
          this.right_tv.setVisibility(8);
        }
        if (this.mDatas.isCanContinueOut()) {
          this.sure_out.setVisibility(0);
        } else {
          this.sure_out.setVisibility(8);
        }
      }
      this.order_id.setText(this.mDatas.getTrade_no());
    }
    this.right_tv.setVisibility(8);
    this.sure_out.setVisibility(8);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/OtcRecordDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */