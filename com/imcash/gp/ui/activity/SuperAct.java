package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.SuperInfo;
import com.imcash.gp.ui.adapter.SuperAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296386)
public class SuperAct
  extends BaseListActivity
{
  protected SuperAdapter mAdapter;
  protected ArrayList<SuperInfo> mDatas;
  protected View mHeadView;
  protected TextView money_tv;
  protected TextView time_tv;
  
  protected void getNodeInfo(JSONObject paramJSONObject)
  {
    stopLoad();
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    TextView localTextView = this.money_tv;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramJSONObject.optString("wait_release", "0.00"));
    ((StringBuilder)localObject).append("USDT");
    localTextView.setText(((StringBuilder)localObject).toString());
    this.time_tv.setText(paramJSONObject.optString("content", "每日24点结算"));
    localObject = new Gson();
    this.mDatas.clear();
    paramJSONObject = paramJSONObject.optString("list", "");
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)((Gson)localObject).fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (3.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getNodeInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("待结算");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("往期收益");
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SuperAct.this.toPage(SuperHistroyAct.class);
      }
    });
    this.mHeadView = LayoutInflater.from(this).inflate(2131296390, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.time_tv = ((TextView)this.mHeadView.findViewById(2131165820));
    this.money_tv = ((TextView)this.mHeadView.findViewById(2131165590));
    this.mDatas = new ArrayList();
    this.mAdapter = new SuperAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    postNodeInfo();
  }
  
  protected void initView()
  {
    initListView();
    closeRefreshAll();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postNodeInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.NodeInfo, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/SuperAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */