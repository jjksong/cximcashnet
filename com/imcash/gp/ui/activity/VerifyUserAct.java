package com.imcash.gp.ui.activity;

import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;
import com.imcash.gp.http.HttpClient;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.third.faceid.GenerateSign;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.megvii.meglive_sdk.sdk.listener.FaceIdDetectListener;
import com.megvii.meglive_sdk.sdk.listener.FaceIdInitListener;
import com.megvii.meglive_sdk.sdk.manager.FaceIdManager;
import cz.msebera.android.httpclient.Header;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296402)
public class VerifyUserAct
  extends BaseActivity
{
  private static final int CAMERA_PERMISSION_REQUEST_CODE = 100;
  private static final int EXTERNAL_STORAGE_REQ_WRITE_EXTERNAL_STORAGE_CODE = 101;
  private String bizToken;
  @ViewInject(2131165316)
  protected EditText card_id;
  private int comparisonType;
  private Handler handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      VerifyUserAct.this.requestCameraPerm();
    }
  };
  private String livenessType;
  private FaceIdDetectListener mDetectListener = new FaceIdDetectListener()
  {
    public void onFailed(int paramAnonymousInt, String paramAnonymousString) {}
    
    public void onSuccess(int paramAnonymousInt, String paramAnonymousString)
    {
      VerifyUserAct.this.postCheckResult();
      if (51000 != paramAnonymousInt) {
        VerifyUserAct.this.showToast("认证失败");
      }
    }
  };
  private FaceIdInitListener mInitListener = new FaceIdInitListener()
  {
    public void onFailed(int paramAnonymousInt, String paramAnonymousString)
    {
      Log.v("aaa", "");
    }
    
    public void onSuccess()
    {
      VerifyUserAct.this.stopLoad();
      FaceIdManager.getInstance(VerifyUserAct.this).startDetect();
    }
  };
  private String mName;
  private String mNum;
  private String mSign;
  private String mSignVersion;
  @ViewInject(2131165886)
  protected EditText user_name;
  
  private void enterNextPage()
  {
    FaceIdManager.getInstance(this).setLanguage(this, "zh");
    FaceIdManager.getInstance(this).setHost(this, "https://openapi.faceid.com");
    FaceIdManager.getInstance(this).init(this.bizToken);
  }
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (this.user_name.getText().toString().length() == 0)
    {
      showToast("请填写正确的姓名");
      return;
    }
    if (18 != this.card_id.getText().toString().length())
    {
      showToast("请填写正确的身份证信息");
      return;
    }
    postIdentity();
  }
  
  private void requestCameraPerm()
  {
    requestExternal();
  }
  
  private void requestExternal()
  {
    XXPermissions.with(this).constantRequest().permission(new String[] { "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" }).request(new OnPermission()
    {
      public void hasPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        VerifyUserAct.this.enterNextPage();
      }
      
      public void noPermission(List<String> paramAnonymousList, boolean paramAnonymousBoolean)
      {
        BaseActivity.showBtnBox(VerifyUserAct.this, "权限获取失败，该功能暂时无法使用，是否要开启权限？", null, new Btn2Dialog.OnBtnListener()
        {
          public void onClick()
          {
            XXPermissions.gotoPermissionSettings(VerifyUserAct.this);
          }
        });
        VerifyUserAct.this.stopLoad();
      }
    });
  }
  
  private void requestWriteExternalPerm()
  {
    if (Build.VERSION.SDK_INT >= 23)
    {
      if (checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
        requestPermissions(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 101);
      } else {
        enterNextPage();
      }
    }
    else {
      enterNextPage();
    }
  }
  
  protected void getBizToken()
  {
    final RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("sign", this.mSign);
    localRequestParams.put("sign_version", this.mSignVersion);
    localRequestParams.put("liveness_type", this.livenessType);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("");
    localStringBuilder.append(this.comparisonType);
    localRequestParams.put("comparison_type", localStringBuilder.toString());
    localRequestParams.put("idcard_name", this.mName);
    localRequestParams.put("idcard_number", this.mNum);
    localRequestParams.put("verbose", "1");
    HttpClient.post(HttpTypeEnum.FaceBizToken, localRequestParams, new JsonHttpResponseHandler()
    {
      public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, String paramAnonymousString, Throwable paramAnonymousThrowable) {}
      
      public void onFinish() {}
      
      public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, JSONObject paramAnonymousJSONObject)
      {
        if (200 == paramAnonymousInt)
        {
          VerifyUserAct.access$302(VerifyUserAct.this, paramAnonymousJSONObject.optString("biz_token"));
          paramAnonymousArrayOfHeader = new Message();
          VerifyUserAct.this.handler.sendMessage(paramAnonymousArrayOfHeader);
        }
      }
    });
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      stopLoad();
      showToast("认证成功");
      EventBus.getDefault().post("");
      finish();
      break;
    case ???: 
      stopLoad();
      verifyBegin();
    }
  }
  
  protected void initData()
  {
    setTitleStr("实名认证");
    FaceIdManager.getInstance(this).setFaceIdInitListener(this.mInitListener);
    FaceIdManager.getInstance(this).setFaceIdDetectListener(this.mDetectListener);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onResume()
  {
    super.onResume();
    stopLoad();
  }
  
  protected void postCheckResult()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("biz_token", this.bizToken);
    localRequestParams.put("sign", this.mSign);
    postNet(HttpTypeEnum.CheckIdentity, localRequestParams);
  }
  
  protected void postIdentity()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("username", this.user_name.getText().toString());
    localRequestParams.put("card", this.card_id.getText().toString());
    postNet(HttpTypeEnum.Identity, localRequestParams);
  }
  
  protected void updatePage() {}
  
  protected void verifyBegin()
  {
    startLoad();
    long l2 = System.currentTimeMillis() / 1000L;
    long l1 = (System.currentTimeMillis() + 360000L) / 1000L;
    this.mSign = GenerateSign.appSign(getResources().getString(2131558463), getResources().getString(2131558464), l2, l1).replaceAll("[\\s*\t\n\r]", "");
    this.mSignVersion = "hmac_sha1";
    this.mName = this.user_name.getText().toString();
    this.mNum = this.card_id.getText().toString();
    this.livenessType = "meglive";
    this.comparisonType = 1;
    getBizToken();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/VerifyUserAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */