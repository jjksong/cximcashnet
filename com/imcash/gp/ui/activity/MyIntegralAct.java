package com.imcash.gp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296339)
public class MyIntegralAct
  extends BaseActivity
{
  @ViewInject(2131165503)
  protected TextView integral_tv;
  
  @Event({2131165404})
  private void integralDetail(View paramView)
  {
    toPage(IntegralDetailAct.class);
  }
  
  @Event({2131165502})
  private void integralQClicked(View paramView)
  {
    toWebPage("积分说明", "file:///android_asset/integral.html");
  }
  
  @Event({2131165577})
  private void missionClicked(View paramView)
  {
    toPage(MissionAct.class);
  }
  
  @Event({2131165597})
  private void myRateClicked(View paramView)
  {
    toPage(new Intent(), FinancingCardAct.class);
  }
  
  protected void getIntegralDetail(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.optJSONObject("data").optString("integral", "000");
    this.integral_tv.setText(paramJSONObject);
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getIntegralDetail(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("我的积分");
    TextView localTextView = (TextView)findViewById(2131165718);
    localTextView.setText("兑换加息券");
    localTextView.setOnClickListener(this);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165718) {
      toPage(RateUpAct.class);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onResume()
  {
    super.onResume();
    postIntegralDetail();
  }
  
  protected void postIntegralDetail()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("pages", 0);
    localRequestParams.put("page_num", 15);
    postNet(HttpTypeEnum.Integral_Detail, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/MyIntegralAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */