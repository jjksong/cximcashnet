package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BFCReleaseRecord;
import com.imcash.gp.ui.adapter.BfcReleaseAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296314)
public class BfcReleaseAct
  extends BaseListActivity
{
  public static final String Bfc_Count_Key = "Bfc_Count_Key";
  protected BfcReleaseAdapter mAdapter;
  protected TextView mAllBfc;
  protected TextView mAllRelease;
  protected ArrayList<BFCReleaseRecord> mDatas;
  protected View mHeadView;
  protected int mPageIndex = 1;
  
  protected void getRelease(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optJSONObject("data");
    String str = paramJSONObject.optString("list");
    this.mAllRelease.setText(paramJSONObject.optString("sum", "0.00"));
    if (GetIntentData("Bfc_Count_Key") != "Bfc_Count_Key") {
      this.mAllBfc.setText((String)GetIntentData("Bfc_Count_Key"));
    }
    if (HttpTypeEnum.Bfc_Release_Down == paramHttpTypeEnum)
    {
      stopDownRefresh();
      this.mDatas.clear();
    }
    else
    {
      stopUpRefresh();
    }
    if ((str != null) && (str.length() != 0))
    {
      paramHttpTypeEnum = (ArrayList)localGson.fromJson(str, new TypeToken() {}.getType());
      if ((paramHttpTypeEnum != null) && (paramHttpTypeEnum.size() > 0))
      {
        this.mDatas.addAll(paramHttpTypeEnum);
        changeRefreshState(paramHttpTypeEnum.size(), 15);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getRelease(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("派发记录");
    this.mDatas = new ArrayList();
    this.mAdapter = new BfcReleaseAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296316, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.mAllRelease = ((TextView)this.mHeadView.findViewById(2131165238));
    this.mAllBfc = ((TextView)this.mHeadView.findViewById(2131165270));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPageIndex = 1;
    postRelease(HttpTypeEnum.Bfc_Release_Down);
  }
  
  protected void onRefreshUp()
  {
    this.mPageIndex += 1;
    postRelease(HttpTypeEnum.Bfc_Release_Up);
  }
  
  protected void postRelease(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPageIndex);
    localRequestParams.put("page_num", 15);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcReleaseAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */