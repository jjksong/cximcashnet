package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.SuperInfo;
import com.imcash.gp.ui.adapter.SuperHistroyAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296387)
public class SuperHistroyAct
  extends BaseListActivity
{
  public static final int PageLimit = 10;
  protected String gather = "";
  protected SuperHistroyAdapter mAdapter;
  protected ArrayList<SuperInfo> mDatas;
  protected int mPageIndex = 1;
  
  protected void getHistroy(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    this.gather = paramJSONObject.optString("gather", "");
    Gson localGson = new Gson();
    if (HttpTypeEnum.NodeHistroy_Down == paramHttpTypeEnum)
    {
      stopDownRefresh();
      this.mDatas.clear();
    }
    String str = paramJSONObject.optString("list", "");
    paramJSONObject = null;
    paramHttpTypeEnum = paramJSONObject;
    if (str != null)
    {
      paramHttpTypeEnum = paramJSONObject;
      if (str.length() != 0)
      {
        paramJSONObject = (ArrayList)localGson.fromJson(str, new TypeToken() {}.getType());
        paramHttpTypeEnum = paramJSONObject;
        if (paramJSONObject != null)
        {
          paramHttpTypeEnum = paramJSONObject;
          if (paramJSONObject.size() > 0)
          {
            this.mDatas.addAll(paramJSONObject);
            paramHttpTypeEnum = paramJSONObject;
          }
        }
      }
    }
    if ((paramHttpTypeEnum != null) && (paramHttpTypeEnum.size() > 0)) {
      changeRefreshState(this.mDatas.size(), 10);
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getHistroy(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("往期收益");
    this.mDatas = new ArrayList();
    this.mAdapter = new SuperHistroyAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    initListView();
    closeUpState();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPageIndex = 1;
    this.gather = "";
    postHistroy(HttpTypeEnum.NodeHistroy_Down);
  }
  
  protected void onRefreshUp()
  {
    this.mPageIndex += 1;
    postHistroy(HttpTypeEnum.NodeHistroy_Up);
  }
  
  protected void postHistroy(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPageIndex);
    localRequestParams.put("page_num", 10);
    if (this.gather.length() != 0) {
      localRequestParams.put("gather", this.gather);
    }
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/SuperHistroyAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */