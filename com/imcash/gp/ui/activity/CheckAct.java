package com.imcash.gp.ui.activity;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.view.WaveDrawable;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296320)
public class CheckAct
  extends BaseActivity
{
  @ViewInject(2131165334)
  protected TextView checking;
  @ViewInject(2131165338)
  protected ImageView circle_view;
  @ViewInject(2131165367)
  protected TextView connections;
  @ViewInject(2131165388)
  protected TextView credit;
  @ViewInject(2131165468)
  protected TextView hobby;
  protected int mPercent = 0;
  protected TimeCount mTimeCount;
  @ViewInject(2131165604)
  protected TextView next_tv;
  @ViewInject(2131165651)
  protected TextView percent_tv;
  @ViewInject(2131165652)
  protected TextView perform;
  protected String resultJson = "";
  
  @Event({2131165604})
  private void nextClicked(View paramView)
  {
    paramView = new Intent();
    paramView.putExtra("CheckResultKey", this.resultJson);
    toPage(paramView, CheckResultAct.class);
  }
  
  protected void getCheck(JSONObject paramJSONObject)
  {
    this.resultJson = paramJSONObject.toString();
    this.next_tv.setClickable(true);
    paramJSONObject = paramJSONObject.optJSONObject("data");
    this.perform.setText(paramJSONObject.optString("perform", "0"));
    this.connections.setText(paramJSONObject.optString("connections", "0"));
    this.hobby.setText(paramJSONObject.optString("hobby", "0"));
    this.credit.setText(paramJSONObject.optString("credit", "0"));
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    if (1.$SwitchMap$com$imcash$gp$http$HttpTypeEnum[paramHttpInfo.mHttpType.ordinal()] == 1) {
      getCheck(paramHttpInfo.mJsonObj);
    }
  }
  
  @SuppressLint({"WrongConstant"})
  protected void initData()
  {
    WaveDrawable localWaveDrawable = new WaveDrawable(this, 2131099767);
    this.circle_view.setImageDrawable(localWaveDrawable);
    ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    localValueAnimator.setRepeatCount(0);
    localValueAnimator.setDuration(10000L);
    localValueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
    localWaveDrawable.setIndeterminateAnimator(localValueAnimator);
    localWaveDrawable.setIndeterminate(true);
    this.mTimeCount = new TimeCount(10000L, 100L);
    this.mTimeCount.start();
    this.next_tv.setClickable(false);
  }
  
  protected void initView()
  {
    setTitleStr("ImCash指数");
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postCheck()
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    postNet(HttpTypeEnum.Check_Type, localRequestParams);
  }
  
  protected void updateCheckState()
  {
    TextView localTextView = this.percent_tv;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mPercent);
    localStringBuilder.append("%");
    localTextView.setText(localStringBuilder.toString());
    if (100 != this.mPercent) {
      this.checking.setText("正在检测...");
    } else {
      this.checking.setText("检测完成");
    }
  }
  
  protected void updatePage()
  {
    updateCheckState();
  }
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      CheckAct localCheckAct = CheckAct.this;
      localCheckAct.mPercent = 100;
      localCheckAct.updateCheckState();
      CheckAct.this.postCheck();
    }
    
    public void onTick(long paramLong)
    {
      CheckAct localCheckAct = CheckAct.this;
      localCheckAct.mPercent += 1;
      CheckAct.this.updateCheckState();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/CheckAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */