package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296319)
public class BindEMailAct
  extends BaseActivity
{
  @ViewInject(2131165418)
  protected EditText email;
  @ViewInject(2131165445)
  protected TextView get_mail_verify;
  protected TimeCount mTimeCount;
  @ViewInject(2131165891)
  protected EditText verify;
  
  @Event({2131165275})
  private void bindClicked(View paramView)
  {
    if (this.email.getText().toString().length() == 0)
    {
      showToast("邮箱账号为空");
      return;
    }
    if (!GlobalFunction.isEmail(this.email.getText().toString()))
    {
      showToast("请输入正确的邮箱地址");
      return;
    }
    if (this.verify.getText().toString().length() == 0)
    {
      showToast("请输入正确的验证码");
      return;
    }
    postBind();
  }
  
  @Event({2131165445})
  private void getMailVerify(View paramView)
  {
    if (this.email.getText().toString().length() == 0)
    {
      showToast("邮箱账号为空");
      return;
    }
    if (!GlobalFunction.isEmail(this.email.getText().toString()))
    {
      showToast("请输入正确的邮箱地址");
      return;
    }
    startTimeCount();
    postMailVerify();
  }
  
  protected void bindSuccess()
  {
    showToast("绑定成功");
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      bindSuccess();
    }
  }
  
  protected void initData()
  {
    setTitleStr("邮箱绑定");
    this.mTimeCount = new TimeCount(60000L, 1000L);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postBind()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    localRequestParams.put("email", this.email.getText().toString());
    localRequestParams.put("emailcode", this.verify.getText().toString());
    postNet(HttpTypeEnum.BindMail_Type, localRequestParams);
  }
  
  protected void postMailVerify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    localRequestParams.put("email", this.email.getText().toString());
    postNet(HttpTypeEnum.MailVerify_Type, localRequestParams);
  }
  
  protected void startTimeCount()
  {
    this.get_mail_verify.setClickable(false);
    this.mTimeCount.start();
  }
  
  protected void updatePage() {}
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      BindEMailAct.this.get_mail_verify.setClickable(true);
      BindEMailAct.this.get_mail_verify.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = BindEMailAct.this.get_mail_verify;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BindEMailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */