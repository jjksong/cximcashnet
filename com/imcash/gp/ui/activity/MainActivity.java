package com.imcash.gp.ui.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.ui.base.BaseFragment;
import com.imcash.gp.ui.base.ImcashApplication;
import com.imcash.gp.ui.fragment.BfcFragment;
import com.imcash.gp.ui.fragment.CenterFragment;
import com.imcash.gp.ui.fragment.MainFragment;
import com.imcash.gp.ui.fragment.MarketFragment;

public class MainActivity
  extends FragmentActivity
  implements View.OnClickListener
{
  public static final String Event_To_Main = "Event_To_Main";
  protected final int Default_Color = Color.rgb(148, 154, 166);
  protected final int Selected_Color = Color.rgb(17, 30, 56);
  private boolean back_action_one = false;
  private long exitTime = 0L;
  protected ImcashApplication mApplication;
  protected TextView mBottomTv_0;
  protected TextView mBottomTv_1;
  protected TextView mBottomTv_2;
  protected TextView mBottomTv_3;
  protected ImageView mBottom_0;
  protected ImageView mBottom_1;
  protected ImageView mBottom_2;
  protected ImageView mBottom_3;
  protected MainFragment mainFragment;
  
  public void changeFragment(String paramString)
  {
    FragmentManager localFragmentManager = getSupportFragmentManager();
    FragmentTransaction localFragmentTransaction = localFragmentManager.beginTransaction();
    localFragmentTransaction.show(getFragmentByTag(paramString, localFragmentTransaction));
    if (localFragmentManager.findFragmentByTag(this.mApplication.getDataCore().mCurFragTag) != null) {
      localFragmentTransaction.hide(localFragmentManager.findFragmentByTag(this.mApplication.getDataCore().mCurFragTag));
    }
    localFragmentTransaction.commit();
    switchTitle(paramString);
    this.mApplication.getDataCore().mCurFragTag = paramString;
    this.mApplication.getDataCore().mTargetTag = paramString;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getKeyCode() == 4)
    {
      switch (paramKeyEvent.getAction())
      {
      default: 
        break;
      case 1: 
        this.back_action_one = true;
        break;
      case 0: 
        if (System.currentTimeMillis() - this.exitTime > 2000L)
        {
          Toast.makeText(this, "再按一次退出程序", 0).show();
          this.exitTime = System.currentTimeMillis();
          this.back_action_one = false;
        }
        else if (this.back_action_one)
        {
          this.mApplication.getDataCore().onDestroyCurrentPageTag();
          finish();
        }
        break;
      }
      return true;
    }
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  protected String getCurrentTag()
  {
    return this.mApplication.getDataCore().mCurFragTag;
  }
  
  @SuppressLint({"Recycle"})
  protected Fragment getFragmentByTag(String paramString, FragmentTransaction paramFragmentTransaction)
  {
    Object localObject = getSupportFragmentManager();
    if ("main_fragment".equals(paramString))
    {
      if (((FragmentManager)localObject).findFragmentByTag("main_fragment") == null)
      {
        localObject = new MainFragment(this);
        this.mainFragment = ((MainFragment)localObject);
        paramFragmentTransaction.add(2131165564, (Fragment)localObject, paramString);
        paramString = (String)localObject;
      }
      else
      {
        paramString = (BaseFragment)((FragmentManager)localObject).findFragmentByTag("main_fragment");
        paramFragmentTransaction = (MainFragment)paramString;
      }
    }
    else if ("market_fragment".equals(paramString))
    {
      if (((FragmentManager)localObject).findFragmentByTag("market_fragment") == null)
      {
        localObject = new MarketFragment(this);
        paramFragmentTransaction.add(2131165564, (Fragment)localObject, paramString);
        paramString = (String)localObject;
      }
      else
      {
        paramString = (BaseFragment)((FragmentManager)localObject).findFragmentByTag("market_fragment");
      }
    }
    else if ("center_fragment".equals(paramString))
    {
      if (((FragmentManager)localObject).findFragmentByTag("center_fragment") == null)
      {
        localObject = new CenterFragment(this);
        paramFragmentTransaction.add(2131165564, (Fragment)localObject, paramString);
        paramString = (String)localObject;
      }
      else
      {
        paramString = (BaseFragment)((FragmentManager)localObject).findFragmentByTag("center_fragment");
      }
    }
    else if ("bfc_fragment".equals(paramString))
    {
      if (((FragmentManager)localObject).findFragmentByTag("bfc_fragment") == null)
      {
        localObject = new BfcFragment(this);
        paramFragmentTransaction.add(2131165564, (Fragment)localObject, paramString);
        paramString = (String)localObject;
      }
      else
      {
        paramString = (BaseFragment)((FragmentManager)localObject).findFragmentByTag("bfc_fragment");
      }
    }
    else {
      paramString = null;
    }
    return paramString;
  }
  
  protected void initFragment()
  {
    if ("".equals(getCurrentTag()))
    {
      changeFragment("main_fragment");
    }
    else
    {
      changeFragment(getCurrentTag());
      switchTitle(getCurrentTag());
    }
  }
  
  protected void initView()
  {
    this.mBottom_0 = ((ImageView)findViewById(2131165285));
    this.mBottom_1 = ((ImageView)findViewById(2131165286));
    this.mBottom_2 = ((ImageView)findViewById(2131165287));
    this.mBottom_3 = ((ImageView)findViewById(2131165288));
    this.mBottomTv_0 = ((TextView)findViewById(2131165296));
    this.mBottomTv_1 = ((TextView)findViewById(2131165297));
    this.mBottomTv_2 = ((TextView)findViewById(2131165298));
    this.mBottomTv_3 = ((TextView)findViewById(2131165299));
    findViewById(2131165563).setOnClickListener(this);
    findViewById(2131165453).setOnClickListener(this);
    findViewById(2131165574).setOnClickListener(this);
    findViewById(2131165269).setOnClickListener(this);
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (i != 2131165269)
    {
      if (i != 2131165453)
      {
        if (i != 2131165563)
        {
          if (i == 2131165574)
          {
            if ("center_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
              return;
            }
            changeFragment("center_fragment");
          }
        }
        else
        {
          if ("main_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
            return;
          }
          changeFragment("main_fragment");
        }
      }
      else
      {
        if ("market_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
          return;
        }
        changeFragment("market_fragment");
      }
    }
    else
    {
      if ("bfc_fragment".equals(this.mApplication.getDataCore().mCurFragTag)) {
        return;
      }
      changeFragment("bfc_fragment");
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296350);
    this.mApplication = ((ImcashApplication)getApplication());
    initView();
    initFragment();
  }
  
  public void onResume()
  {
    super.onResume();
    if ((this.mApplication.getDataCore().mCurFragTag != null) && (this.mApplication.getDataCore().mTargetTag != null) && (this.mApplication.getDataCore().mCurFragTag.length() != 0) && (this.mApplication.getDataCore().mTargetTag.length() != 0) && (!this.mApplication.getDataCore().mTargetTag.equals(this.mApplication.getDataCore().mCurFragTag))) {
      changeFragment(this.mApplication.getDataCore().mTargetTag);
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle) {}
  
  protected void switchTitle(String paramString)
  {
    if ("main_fragment".equals(getCurrentTag()))
    {
      this.mBottom_0.setSelected(false);
      this.mBottomTv_0.setTextColor(this.Default_Color);
    }
    else if ("market_fragment".equals(getCurrentTag()))
    {
      this.mBottom_1.setSelected(false);
      this.mBottomTv_1.setTextColor(this.Default_Color);
    }
    else if ("center_fragment".equals(getCurrentTag()))
    {
      this.mBottom_2.setSelected(false);
      this.mBottomTv_2.setTextColor(this.Default_Color);
    }
    else if ("bfc_fragment".equals(getCurrentTag()))
    {
      this.mBottom_3.setSelected(false);
      this.mBottomTv_3.setTextColor(this.Default_Color);
    }
    if ("main_fragment".equals(paramString))
    {
      this.mBottom_0.setSelected(true);
      this.mBottomTv_0.setTextColor(this.Selected_Color);
    }
    else if ("market_fragment".equals(paramString))
    {
      this.mBottom_1.setSelected(true);
      this.mBottomTv_1.setTextColor(this.Selected_Color);
    }
    else if ("center_fragment".equals(paramString))
    {
      this.mBottom_2.setSelected(true);
      this.mBottomTv_2.setTextColor(this.Selected_Color);
    }
    else if ("bfc_fragment".equals(paramString))
    {
      this.mBottom_3.setSelected(true);
      this.mBottomTv_3.setTextColor(this.Selected_Color);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */