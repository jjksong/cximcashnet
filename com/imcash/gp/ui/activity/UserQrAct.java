package com.imcash.gp.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.model.User;
import com.imcash.gp.tools.ZXingCodeUtils;
import com.imcash.gp.ui.base.BaseActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296401)
public class UserQrAct
  extends BaseActivity
{
  protected Bitmap mQrBitmap;
  @ViewInject(2131165683)
  protected ImageView qr_img;
  @ViewInject(2131165876)
  protected TextView uid;
  
  public void httpDataCallBack(HttpInfo paramHttpInfo) {}
  
  protected void initData()
  {
    setTitleStr("我的二维码");
    TextView localTextView = this.uid;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("UID:");
    localStringBuilder.append(this.mApplication.getDataCore().getUserInfo().angel_num);
    localTextView.setText(localStringBuilder.toString());
    this.mQrBitmap = ZXingCodeUtils.getInstance().createQRCode(this.mApplication.getDataCore().getUserInfo().username, 200, 200);
    this.qr_img.setImageBitmap(this.mQrBitmap);
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Bitmap localBitmap = this.mQrBitmap;
    if ((localBitmap != null) && (!localBitmap.isRecycled()))
    {
      this.mQrBitmap.recycle();
      this.mQrBitmap = null;
      System.gc();
    }
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/UserQrAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */