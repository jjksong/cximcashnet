package com.imcash.gp.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.ui.base.BaseActivity;
import com.loopj.android.http.RequestParams;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296384)
public class RegistAct
  extends BaseActivity
{
  private View.OnClickListener click = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      RegistAct.this.toPdfPage("用户协议", "用户协议.pdf");
    }
  };
  private View.OnClickListener clickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      RegistAct.this.toPdfPage("隐私条款", "隐私条款.pdf");
    }
  };
  @ViewInject(2131165415)
  protected TextView document_tv;
  @ViewInject(2131165444)
  protected EditText fundpass;
  @ViewInject(2131165448)
  protected TextView get_verify;
  @ViewInject(2131165506)
  protected EditText invite_code;
  protected boolean mIsRule = false;
  protected TimeCount mTimeCount;
  @ViewInject(2131165654)
  protected EditText phone;
  @ViewInject(2131165678)
  protected EditText pwd;
  @ViewInject(2131165752)
  protected ImageView select_but;
  @ViewInject(2131165890)
  protected EditText verification_code;
  
  @Event({2131165246})
  private void backClicked(View paramView)
  {
    finish();
  }
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (11 != this.phone.getText().toString().length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    if (this.verification_code.getText().toString().length() == 0)
    {
      showToast("请输入正确的短信验证码");
      return;
    }
    if ((6 <= this.pwd.getText().toString().length()) && (18 >= this.pwd.getText().toString().length()))
    {
      if (this.fundpass.getText().toString().length() == 0)
      {
        showToast("请输入资金密码");
        return;
      }
      if (!this.mIsRule)
      {
        showToast("需要同意《平台隐私条款》及《用户服务协议》");
        return;
      }
      postRegist();
      return;
    }
    showToast("请输入正确的密码");
  }
  
  @Event({2131165752})
  private void ruleClicked(View paramView)
  {
    this.mIsRule ^= true;
    updataRuleState();
  }
  
  @Event({2131165448})
  private void verifyClicked(View paramView)
  {
    if (11 != this.phone.getText().toString().length())
    {
      showToast("请输入正确的手机号码");
      return;
    }
    startTimeCount();
    postVerify();
  }
  
  protected void getRegistMsg(JSONObject paramJSONObject)
  {
    showToast("注册成功");
    stopLoad();
    finish();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      getRegistMsg(paramHttpInfo.mJsonObj);
      break;
    case ???: 
      showToast("短信发送成功");
    }
  }
  
  protected void initData()
  {
    this.mTimeCount = new TimeCount(60000L, 1000L);
    SpannableString localSpannableString = new SpannableString("注册/登录即代表同意《平台隐私条款》及《用户服务协议》");
    localSpannableString.setSpan(new Clickable(this.clickListener), 11, 17, 33);
    localSpannableString.setSpan(new Clickable(this.click), 20, 26, 33);
    this.document_tv.setText(localSpannableString);
    this.document_tv.setMovementMethod(LinkMovementMethod.getInstance());
  }
  
  protected void initView() {}
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  public void onDestroy()
  {
    this.mTimeCount.cancel();
    super.onDestroy();
  }
  
  protected void postRegist()
  {
    startLoad();
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.phone.getText().toString());
    localRequestParams.put("smscode", this.verification_code.getText().toString());
    localRequestParams.put("password", this.pwd.getText().toString());
    localRequestParams.put("fundpass", this.fundpass.getText().toString());
    if (this.invite_code.getText().toString().length() != 0) {
      localRequestParams.put("signid", this.invite_code.getText().toString());
    }
    postNet(HttpTypeEnum.Regist_Type, localRequestParams);
  }
  
  protected void postVerify()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("phone", this.phone.getText().toString());
    postNet(HttpTypeEnum.Get_Verify, localRequestParams);
  }
  
  protected void startTimeCount()
  {
    this.get_verify.setClickable(false);
    this.mTimeCount.start();
  }
  
  protected void updataRuleState()
  {
    if (!this.mIsRule) {
      this.select_but.setImageResource(2131099925);
    } else {
      this.select_but.setImageResource(2131099924);
    }
  }
  
  protected void updatePage()
  {
    updataRuleState();
  }
  
  class Clickable
    extends ClickableSpan
  {
    private final View.OnClickListener mListener;
    
    public Clickable(View.OnClickListener paramOnClickListener)
    {
      this.mListener = paramOnClickListener;
    }
    
    public void onClick(View paramView)
    {
      this.mListener.onClick(paramView);
    }
    
    public void updateDrawState(TextPaint paramTextPaint)
    {
      super.updateDrawState(paramTextPaint);
      paramTextPaint.setUnderlineText(false);
      paramTextPaint.setColor(Color.rgb(0, 0, 214));
    }
  }
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      RegistAct.this.get_verify.setClickable(true);
      RegistAct.this.get_verify.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = RegistAct.this.get_verify;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/RegistAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */