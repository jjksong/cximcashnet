package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.OrderDetail;
import com.imcash.gp.ui.adapter.OrderDetailAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296362)
public class OrderDetailAct
  extends BaseListActivity
{
  protected int PageLimit = 20;
  @ViewInject(2131165419)
  protected LinearLayout empty_view;
  protected OrderDetailAdapter mAdapter;
  protected ArrayList<OrderDetail> mDatas;
  protected int mPageIndex = 1;
  
  protected void getOrderDetail(HttpTypeEnum paramHttpTypeEnum, JSONObject paramJSONObject)
  {
    this.mPageIndex += 1;
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    if (HttpTypeEnum.Wallet_Bill_Down == paramHttpTypeEnum)
    {
      stopDownRefresh();
      this.mDatas.clear();
    }
    else
    {
      stopUpRefresh();
    }
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramHttpTypeEnum = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramHttpTypeEnum != null) && (paramHttpTypeEnum.size() > 0))
      {
        this.mDatas.addAll(paramHttpTypeEnum);
        changeRefreshState(paramHttpTypeEnum.size(), this.PageLimit);
      }
    }
    if (this.mDatas.size() == 0) {
      this.empty_view.setVisibility(0);
    } else {
      this.empty_view.setVisibility(8);
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
    case ???: 
      getOrderDetail(paramHttpInfo.mHttpType, paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new OrderDetailAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    onRefreshDown();
  }
  
  protected void initView()
  {
    setTitleStr("账单详情");
    initListView();
    closeUpState();
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown()
  {
    this.mPageIndex = 1;
    postOrderDetail(HttpTypeEnum.Wallet_Bill_Down);
  }
  
  protected void onRefreshUp()
  {
    postOrderDetail(HttpTypeEnum.Wallet_Bill_Up);
  }
  
  protected void postOrderDetail(HttpTypeEnum paramHttpTypeEnum)
  {
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("pages", this.mPageIndex);
    localRequestParams.put("page_num", this.PageLimit);
    postNet(paramHttpTypeEnum, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/OrderDetailAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */