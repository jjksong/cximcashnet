package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.GlobalFunction;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.TradeInfo;
import com.imcash.gp.ui.adapter.TradeAllowAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.view.Btn2Dialog.OnBtnListener;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(2131296391)
public class TradeAllowAct
  extends BaseListActivity
{
  @ViewInject(2131165351)
  protected RadioGroup coin_group;
  @ViewInject(2131165357)
  protected TextView coin_type;
  @ViewInject(2131165499)
  protected EditText input_count;
  protected TradeAllowAdapter mAdapter;
  protected String mCoinType = "BTC";
  protected ArrayList<TradeInfo> mDatas;
  
  @Event({2131165618})
  private void okClicked(View paramView)
  {
    if (this.input_count.getText().toString().length() == 0)
    {
      showToast("请输入正确的金额");
      return;
    }
    if (!isSelected())
    {
      showToast("请选择量子基金");
      return;
    }
    showBtnBox(this, "交易成功将收取千分之五手续费", null, new Btn2Dialog.OnBtnListener()
    {
      public void onClick()
      {
        TradeAllowAct.this.postSellMsg();
      }
    });
  }
  
  protected void changeToSelected(int paramInt)
  {
    for (int i = 0; i < this.mDatas.size(); i++) {
      if (i == paramInt) {
        ((TradeInfo)this.mDatas.get(i)).mIsSelected = true;
      } else {
        ((TradeInfo)this.mDatas.get(i)).mIsSelected = false;
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected void getAllowInfo(JSONObject paramJSONObject)
  {
    stopLoad();
    Gson localGson = new Gson();
    paramJSONObject = paramJSONObject.optString("data");
    this.mDatas.clear();
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
    {
      paramJSONObject = (ArrayList)localGson.fromJson(paramJSONObject, new TypeToken() {}.getType());
      if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
        this.mDatas.addAll(paramJSONObject);
      }
    }
    this.mAdapter.notifyDataSetChanged();
    this.mListView.invalidate();
  }
  
  protected TradeInfo getSelectedTrade()
  {
    for (int i = 0; i < this.mDatas.size(); i++) {
      if (((TradeInfo)this.mDatas.get(i)).mIsSelected) {
        return (TradeInfo)this.mDatas.get(i);
      }
    }
    TradeInfo localTradeInfo = null;
    return localTradeInfo;
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      showToast(paramHttpInfo.mJsonObj.optString("msg", "挂单成功"));
      this.input_count.setText("");
      postAllowInfo();
      break;
    case ???: 
      getAllowInfo(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    this.mDatas = new ArrayList();
    this.mAdapter = new TradeAllowAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    this.coin_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        TradeAllowAct.this.updataCoinType();
      }
    });
    this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        TradeAllowAct.this.changeToSelected(paramAnonymousInt);
      }
    });
  }
  
  protected void initView()
  {
    setTitleStr("挂单发布");
    initListView();
    closeRefreshAll();
  }
  
  protected boolean isSelected()
  {
    Iterator localIterator = this.mDatas.iterator();
    while (localIterator.hasNext()) {
      if (((TradeInfo)localIterator.next()).mIsSelected) {
        return true;
      }
    }
    boolean bool = false;
    return bool;
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postAllowInfo()
  {
    startLoad();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("currency_id", GlobalFunction.getCoinIndexByName(this.mCoinType));
    postNet(HttpTypeEnum.TradeAllowList, localRequestParams);
  }
  
  protected void postSellMsg()
  {
    startLoad();
    TradeInfo localTradeInfo = getSelectedTrade();
    RequestParams localRequestParams = getIdAndKeyNumParams();
    localRequestParams.put("fund_id", localTradeInfo.getFund_id());
    localRequestParams.put("amount", this.input_count.getText().toString());
    postNet(HttpTypeEnum.TradeLauch, localRequestParams);
  }
  
  protected void updataCoinType()
  {
    switch (this.coin_group.getCheckedRadioButtonId())
    {
    default: 
      break;
    case 2131165880: 
      this.mCoinType = com.imcash.gp.GlobalFunction.Constants.CoinType[4];
      break;
    case 2131165559: 
      this.mCoinType = com.imcash.gp.GlobalFunction.Constants.CoinType[2];
      break;
    case 2131165424: 
      this.mCoinType = com.imcash.gp.GlobalFunction.Constants.CoinType[1];
      break;
    case 2131165302: 
      this.mCoinType = com.imcash.gp.GlobalFunction.Constants.CoinType[0];
      break;
    case 2131165261: 
      this.mCoinType = com.imcash.gp.GlobalFunction.Constants.CoinType[3];
    }
    this.coin_type.setText(this.mCoinType);
    postAllowInfo();
  }
  
  protected void updatePage()
  {
    updataCoinType();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/TradeAllowAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */