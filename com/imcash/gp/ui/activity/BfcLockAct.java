package com.imcash.gp.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcash.gp.GlobalFunction.DataCore;
import com.imcash.gp.http.HttpInfo;
import com.imcash.gp.http.HttpTypeEnum;
import com.imcash.gp.model.BaseModel;
import com.imcash.gp.model.BfcLock;
import com.imcash.gp.model.User;
import com.imcash.gp.ui.adapter.BfcLockAdapter;
import com.imcash.gp.ui.base.BaseListActivity;
import com.imcash.gp.ui.base.ImcashApplication;
import com.loopj.android.http.RequestParams;
import java.util.ArrayList;
import org.json.JSONObject;
import org.xutils.ViewInjector;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

@ContentView(2131296297)
public class BfcLockAct
  extends BaseListActivity
{
  protected BfcLockAdapter mAdapter;
  protected ArrayList<BfcLock> mDatas;
  protected View mHeadView;
  protected TextView mTopMoney;
  
  protected void getLockMsg(JSONObject paramJSONObject)
  {
    paramJSONObject = BaseModel.getDataJson(paramJSONObject);
    Object localObject1 = this.mTopMoney;
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(paramJSONObject.optString("lock", "0.00"));
    ((StringBuilder)localObject2).append("BFC");
    ((TextView)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject1 = new Gson();
    try
    {
      paramJSONObject = paramJSONObject.getString("list");
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0))
      {
        localObject2 = new com/imcash/gp/ui/activity/BfcLockAct$1;
        ((1)localObject2).<init>(this);
        paramJSONObject = (ArrayList)((Gson)localObject1).fromJson(paramJSONObject, ((1)localObject2).getType());
        if ((paramJSONObject != null) && (paramJSONObject.size() > 0)) {
          this.mDatas.addAll(paramJSONObject);
        }
      }
    }
    catch (Exception paramJSONObject)
    {
      for (;;) {}
    }
    this.mAdapter.notifyDataSetChanged();
  }
  
  public void httpDataCallBack(HttpInfo paramHttpInfo)
  {
    switch (paramHttpInfo.mHttpType)
    {
    default: 
      break;
    case ???: 
      if (1 == paramHttpInfo.mJsonObj.optJSONObject("data").optInt("state", 0)) {
        showMessageBox(this, paramHttpInfo.mJsonObj.optJSONObject("data").optString("content", ""), "");
      }
      break;
    case ???: 
      getLockMsg(paramHttpInfo.mJsonObj);
    }
  }
  
  protected void initData()
  {
    setTitleStr("Apollo");
    this.mDatas = new ArrayList();
    this.mAdapter = new BfcLockAdapter(this, this.mDatas);
    this.mListView.setAdapter(this.mAdapter);
    postLockMsg();
    postShowMsg();
  }
  
  protected void initView()
  {
    initListView();
    closeRefreshAll();
    this.mHeadView = LayoutInflater.from(this).inflate(2131296299, null);
    this.mListView.addHeaderView(this.mHeadView);
    this.mTopMoney = ((TextView)this.mHeadView.findViewById(2131165555));
  }
  
  public void onClick(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    x.view().inject(this);
    initView();
    initData();
    updatePage();
  }
  
  protected void onRefreshDown() {}
  
  protected void onRefreshUp() {}
  
  protected void postLockMsg()
  {
    RequestParams localRequestParams = new RequestParams();
    postNet(HttpTypeEnum.Bfc_Lock, localRequestParams);
  }
  
  protected void postShowMsg()
  {
    RequestParams localRequestParams = new RequestParams();
    localRequestParams.put("id", this.mApplication.getDataCore().getUserId());
    localRequestParams.put("keynum", this.mApplication.getDataCore().getUserInfo().getKeyNumber());
    postNet(HttpTypeEnum.Bfc_LockMsg, localRequestParams);
  }
  
  protected void updatePage() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/activity/BfcLockAct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */