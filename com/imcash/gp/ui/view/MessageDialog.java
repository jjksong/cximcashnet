package com.imcash.gp.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MessageDialog
  extends AlertDialog
{
  private String TAG = "MessageDialog";
  private TextView mMessage;
  private boolean mScreen = false;
  private TextView mSure;
  private TextView mTitle;
  
  public MessageDialog(Context paramContext)
  {
    super(paramContext);
  }
  
  private void init()
  {
    this.mTitle = ((TextView)findViewById(2131165411));
    this.mMessage = ((TextView)findViewById(2131165409));
    this.mSure = ((TextView)findViewById(2131165410));
    this.mSure.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        try
        {
          MessageDialog.this.dismiss();
          return;
        }
        catch (Exception paramAnonymousView)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if (this.mScreen) {
      return true;
    }
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296427);
    init();
  }
  
  public void showDialog(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (!paramString1.equals("")))
    {
      show();
      this.mTitle.setText(paramString1);
      if ((paramString2 != null) && (!paramString2.equals("")))
      {
        this.mMessage.setVisibility(0);
        this.mMessage.setText(paramString2);
      }
      else
      {
        this.mMessage.setVisibility(8);
        this.mMessage.setText("");
      }
      return;
    }
    Log.e(this.TAG, "title is null");
  }
  
  public void showDialog(String paramString1, String paramString2, final OnBtnListener paramOnBtnListener)
  {
    showDialog(paramString1, paramString2);
    if (paramOnBtnListener != null)
    {
      this.mScreen = true;
      setCanceledOnTouchOutside(false);
    }
    else
    {
      this.mScreen = false;
      setCanceledOnTouchOutside(true);
    }
    this.mSure.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = paramOnBtnListener;
        if (paramAnonymousView != null) {
          paramAnonymousView.onClick();
        }
        try
        {
          MessageDialog.this.dismiss();
          return;
        }
        catch (Exception paramAnonymousView)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public void showDialog(String paramString1, String paramString2, OnBtnListener paramOnBtnListener, String paramString3)
  {
    showDialog(paramString1, paramString2, paramOnBtnListener);
    this.mSure.setText(paramString3);
  }
  
  public static abstract interface OnBtnListener
  {
    public abstract void onClick();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/MessageDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */