package com.imcash.gp.ui.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class HackyViewPager
  extends ViewPager
{
  private static final String TAG = "HackyViewPager";
  
  public HackyViewPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public HackyViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    try
    {
      boolean bool = super.onInterceptTouchEvent(paramMotionEvent);
      return bool;
    }
    catch (ArrayIndexOutOfBoundsException paramMotionEvent)
    {
      Log.e("HackyViewPager", "hacky viewpager error2");
      return false;
    }
    catch (IllegalArgumentException paramMotionEvent)
    {
      Log.e("HackyViewPager", "hacky viewpager error1");
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/HackyViewPager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */