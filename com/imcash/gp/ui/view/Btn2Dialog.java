package com.imcash.gp.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Btn2Dialog
  extends AlertDialog
{
  private String TAG = "Btn2Dialog";
  private TextView mCancle;
  private TextView mMessage;
  private TextView mSure;
  private TextView mTitle;
  
  public Btn2Dialog(Context paramContext)
  {
    super(paramContext);
  }
  
  private void init()
  {
    this.mTitle = ((TextView)findViewById(2131165411));
    this.mMessage = ((TextView)findViewById(2131165409));
    this.mSure = ((TextView)findViewById(2131165410));
    this.mCancle = ((TextView)findViewById(2131165407));
    this.mCancle.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        try
        {
          Btn2Dialog.this.dismiss();
          return;
        }
        catch (Exception paramAnonymousView)
        {
          for (;;) {}
        }
      }
    });
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296429);
    init();
  }
  
  public void showDialog(String paramString1, String paramString2, final OnBtnListener paramOnBtnListener)
  {
    if ((paramString1 != null) && (!paramString1.equals("")))
    {
      show();
      this.mTitle.setText(paramString1);
      if ((paramString2 != null) && (!paramString2.equals("")))
      {
        this.mMessage.setVisibility(0);
        this.mMessage.setText(paramString2);
      }
      else
      {
        this.mMessage.setVisibility(8);
        this.mMessage.setText("");
      }
      if (paramOnBtnListener != null) {
        setCanceledOnTouchOutside(false);
      } else {
        setCanceledOnTouchOutside(true);
      }
      this.mSure.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          paramAnonymousView = paramOnBtnListener;
          if (paramAnonymousView != null) {
            paramAnonymousView.onClick();
          }
          try
          {
            Btn2Dialog.this.dismiss();
            return;
          }
          catch (Exception paramAnonymousView)
          {
            for (;;) {}
          }
        }
      });
      return;
    }
    Log.e(this.TAG, "title is null");
  }
  
  public void showDialog(String paramString1, String paramString2, OnBtnListener paramOnBtnListener1, final OnBtnListener paramOnBtnListener2, String paramString3)
  {
    showDialog(paramString1, paramString2, paramOnBtnListener1);
    this.mSure.setText(paramString3);
    this.mCancle.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = paramOnBtnListener2;
        if (paramAnonymousView != null) {
          paramAnonymousView.onClick();
        }
        try
        {
          Btn2Dialog.this.dismiss();
          return;
        }
        catch (Exception paramAnonymousView)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public static abstract interface OnBtnListener
  {
    public abstract void onClick();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/Btn2Dialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */