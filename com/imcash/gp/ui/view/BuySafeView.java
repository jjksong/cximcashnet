package com.imcash.gp.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.imcash.gp.model.InsuranceDetail;
import java.util.ArrayList;

public class BuySafeView
  extends AlertDialog
{
  protected RadioGroup coin_group;
  protected RadioButton group_btn_0;
  protected RadioButton group_btn_1;
  protected RadioButton group_btn_2;
  protected RadioButton group_btn_3;
  protected TextView mBottomTv;
  public BuySafeViewCall mBuySafeViewCall;
  public ArrayList<InsuranceDetail> mDatas;
  protected int mSelectedIndex = -1;
  protected TextView rule_tv;
  
  public BuySafeView(Context paramContext, ArrayList<InsuranceDetail> paramArrayList, BuySafeViewCall paramBuySafeViewCall)
  {
    super(paramContext);
    this.mDatas = paramArrayList;
    this.mBuySafeViewCall = paramBuySafeViewCall;
  }
  
  private void init()
  {
    this.rule_tv = ((TextView)findViewById(2131165729));
    this.rule_tv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BuySafeView.this.mBuySafeViewCall.ruleClicked();
      }
    });
    this.coin_group = ((RadioGroup)findViewById(2131165351));
    this.group_btn_0 = ((RadioButton)findViewById(2131165456));
    this.group_btn_1 = ((RadioButton)findViewById(2131165457));
    this.group_btn_2 = ((RadioButton)findViewById(2131165458));
    this.group_btn_3 = ((RadioButton)findViewById(2131165459));
    Object localObject2 = this.group_btn_0;
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(((InsuranceDetail)this.mDatas.get(0)).getDays());
    ((StringBuilder)localObject1).append("天");
    ((RadioButton)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject2 = this.group_btn_1;
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(((InsuranceDetail)this.mDatas.get(1)).getDays());
    ((StringBuilder)localObject1).append("天");
    ((RadioButton)localObject2).setText(((StringBuilder)localObject1).toString());
    localObject1 = this.group_btn_2;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(((InsuranceDetail)this.mDatas.get(2)).getDays());
    ((StringBuilder)localObject2).append("天");
    ((RadioButton)localObject1).setText(((StringBuilder)localObject2).toString());
    localObject1 = this.group_btn_3;
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(((InsuranceDetail)this.mDatas.get(3)).getDays());
    ((StringBuilder)localObject2).append("天");
    ((RadioButton)localObject1).setText(((StringBuilder)localObject2).toString());
    this.mBottomTv = ((TextView)findViewById(2131165289));
    findViewById(2131165315).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BuySafeView.this.dismiss();
      }
    });
    this.mBottomTv.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BuySafeView.this.mBuySafeViewCall.okClicked(BuySafeView.this.mSelectedIndex);
      }
    });
    this.coin_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {
      public void onCheckedChanged(RadioGroup paramAnonymousRadioGroup, int paramAnonymousInt)
      {
        switch (BuySafeView.this.coin_group.getCheckedRadioButtonId())
        {
        default: 
          break;
        case 2131165459: 
          BuySafeView.this.mSelectedIndex = 3;
          break;
        case 2131165458: 
          BuySafeView.this.mSelectedIndex = 2;
          break;
        case 2131165457: 
          BuySafeView.this.mSelectedIndex = 1;
          break;
        case 2131165456: 
          BuySafeView.this.mSelectedIndex = 0;
        }
        BuySafeView.this.changeBottom();
      }
    });
  }
  
  protected void changeBottom()
  {
    TextView localTextView = this.mBottomTv;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(((InsuranceDetail)this.mDatas.get(this.mSelectedIndex)).getPrice());
    localStringBuilder.append("USDT");
    localTextView.setText(localStringBuilder.toString());
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296442);
    init();
  }
  
  public void showDialog()
  {
    show();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/BuySafeView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */