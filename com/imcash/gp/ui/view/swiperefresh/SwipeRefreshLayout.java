package com.imcash.gp.ui.view.swiperefresh;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;

public class SwipeRefreshLayout
  extends ViewGroup
{
  private static final int ALPHA_ANIMATION_DURATION = 300;
  private static final int ANIMATE_TO_START_DURATION = 200;
  private static final int ANIMATE_TO_TRIGGER_DURATION = 200;
  private static final int CIRCLE_BG_LIGHT = -328966;
  private static final int CIRCLE_DIAMETER = 40;
  private static final int CIRCLE_DIAMETER_LARGE = 56;
  private static final float DECELERATE_INTERPOLATION_FACTOR = 2.0F;
  public static final int DEFAULT = 1;
  private static final int DEFAULT_CIRCLE_TARGET = 64;
  private static final float DRAG_RATE = 0.5F;
  private static final int INVALID_POINTER = -1;
  public static final int LARGE = 0;
  private static final int[] LAYOUT_ATTRS = { 16842766 };
  private static final String LOG_TAG = "SwipeRefreshLayout";
  private static final int MAX_ALPHA = 255;
  private static final float MAX_PROGRESS_ANGLE = 0.8F;
  private static final int SCALE_DOWN_DURATION = 150;
  private static final int STARTING_PROGRESS_ALPHA = 76;
  protected int mActivePointerId = -1;
  private Animation mAlphaMaxAnimation;
  private Animation mAlphaStartAnimation;
  private final Animation mAnimateToCorrectPosition = new Animation()
  {
    public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
    {
      if (!SwipeRefreshLayout.this.mUsingCustomStart) {
        i = (int)(SwipeRefreshLayout.this.mSpinnerFinalOffset - Math.abs(SwipeRefreshLayout.this.mOriginalOffsetTop));
      } else {
        i = (int)SwipeRefreshLayout.this.mSpinnerFinalOffset;
      }
      int j = SwipeRefreshLayout.this.mFrom;
      int k = (int)((i - SwipeRefreshLayout.this.mFrom) * paramAnonymousFloat);
      int i = SwipeRefreshLayout.this.mCircleView.getTop();
      SwipeRefreshLayout.this.setTargetOffsetTopAndBottom(j + k - i, false);
      SwipeRefreshLayout.this.mProgress.setArrowScale(1.0F - paramAnonymousFloat);
    }
  };
  private final Animation mAnimateToStartPosition = new Animation()
  {
    public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
    {
      SwipeRefreshLayout.this.moveToStart(paramAnonymousFloat);
    }
  };
  private int mCircleHeight;
  protected CircleImageView mCircleView;
  private int mCircleViewIndex = -1;
  private int mCircleWidth;
  protected int mCurrentTargetOffsetTop;
  private final DecelerateInterpolator mDecelerateInterpolator;
  protected int mFrom;
  private float mInitialDownY;
  private float mInitialMotionY;
  protected boolean mIsBeingDragged;
  private OnRefreshListener mListener;
  private int mMediumAnimationDuration;
  private boolean mNotify;
  private boolean mOriginalOffsetCalculated = false;
  protected int mOriginalOffsetTop;
  private MaterialProgressDrawable mProgress;
  private Animation.AnimationListener mRefreshListener = new Animation.AnimationListener()
  {
    public void onAnimationEnd(Animation paramAnonymousAnimation)
    {
      paramAnonymousAnimation = SwipeRefreshLayout.this;
      paramAnonymousAnimation.mReturningToStart = true;
      if (paramAnonymousAnimation.mRefreshing)
      {
        SwipeRefreshLayout.this.mProgress.setAlpha(255);
        SwipeRefreshLayout.this.mProgress.start();
        if ((SwipeRefreshLayout.this.mNotify) && (SwipeRefreshLayout.this.mListener != null)) {
          SwipeRefreshLayout.this.mListener.onRefresh();
        }
      }
      else
      {
        SwipeRefreshLayout.this.mProgress.stop();
        SwipeRefreshLayout.this.mCircleView.setVisibility(8);
        SwipeRefreshLayout.this.setColorViewAlpha(255);
        if (SwipeRefreshLayout.this.mScale)
        {
          SwipeRefreshLayout.this.setAnimationProgress(0.0F);
        }
        else
        {
          paramAnonymousAnimation = SwipeRefreshLayout.this;
          paramAnonymousAnimation.setTargetOffsetTopAndBottom(paramAnonymousAnimation.mOriginalOffsetTop - SwipeRefreshLayout.this.mCurrentTargetOffsetTop, true);
        }
      }
      paramAnonymousAnimation = SwipeRefreshLayout.this;
      paramAnonymousAnimation.mCurrentTargetOffsetTop = paramAnonymousAnimation.mCircleView.getTop();
    }
    
    public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
    
    public void onAnimationStart(Animation paramAnonymousAnimation) {}
  };
  protected boolean mRefreshing = false;
  protected boolean mReturningToStart;
  private boolean mScale;
  private Animation mScaleAnimation;
  private Animation mScaleDownAnimation;
  private Animation mScaleDownToStartAnimation;
  private float mSpinnerFinalOffset;
  private float mStartingScale;
  protected View mTarget;
  private float mTotalDragDistance = -1.0F;
  protected int mTouchSlop;
  private boolean mUsingCustomStart;
  
  public SwipeRefreshLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SwipeRefreshLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.mMediumAnimationDuration = getResources().getInteger(17694721);
    setWillNotDraw(false);
    this.mDecelerateInterpolator = new DecelerateInterpolator(2.0F);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, LAYOUT_ATTRS);
    setEnabled(paramContext.getBoolean(0, true));
    paramContext.recycle();
    paramContext = getResources().getDisplayMetrics();
    this.mCircleWidth = ((int)(paramContext.density * 40.0F));
    this.mCircleHeight = ((int)(paramContext.density * 40.0F));
    createProgressView();
    ViewCompat.setChildrenDrawingOrderEnabled(this, true);
    this.mSpinnerFinalOffset = (paramContext.density * 64.0F);
    this.mTotalDragDistance = this.mSpinnerFinalOffset;
  }
  
  private void animateOffsetToCorrectPosition(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    this.mFrom = paramInt;
    this.mAnimateToCorrectPosition.reset();
    this.mAnimateToCorrectPosition.setDuration(200L);
    this.mAnimateToCorrectPosition.setInterpolator(this.mDecelerateInterpolator);
    if (paramAnimationListener != null) {
      this.mCircleView.setAnimationListener(paramAnimationListener);
    }
    this.mCircleView.clearAnimation();
    this.mCircleView.startAnimation(this.mAnimateToCorrectPosition);
  }
  
  private void animateOffsetToStartPosition(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    if (this.mScale)
    {
      startScaleDownReturnToStartAnimation(paramInt, paramAnimationListener);
    }
    else
    {
      this.mFrom = paramInt;
      this.mAnimateToStartPosition.reset();
      this.mAnimateToStartPosition.setDuration(200L);
      this.mAnimateToStartPosition.setInterpolator(this.mDecelerateInterpolator);
      if (paramAnimationListener != null) {
        this.mCircleView.setAnimationListener(paramAnimationListener);
      }
      this.mCircleView.clearAnimation();
      this.mCircleView.startAnimation(this.mAnimateToStartPosition);
    }
  }
  
  private void createProgressView()
  {
    this.mCircleView = new CircleImageView(getContext(), -328966, 20.0F);
    this.mProgress = new MaterialProgressDrawable(getContext(), this);
    this.mProgress.setBackgroundColor(-328966);
    this.mCircleView.setImageDrawable(this.mProgress);
    this.mCircleView.setVisibility(8);
    addView(this.mCircleView);
  }
  
  private float getMotionEventY(MotionEvent paramMotionEvent, int paramInt)
  {
    paramInt = MotionEventCompat.findPointerIndex(paramMotionEvent, paramInt);
    if (paramInt < 0) {
      return -1.0F;
    }
    return MotionEventCompat.getY(paramMotionEvent, paramInt);
  }
  
  private boolean isAlphaUsedForScale()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT < 11) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean isAnimationRunning(Animation paramAnimation)
  {
    boolean bool;
    if ((paramAnimation != null) && (paramAnimation.hasStarted()) && (!paramAnimation.hasEnded())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void moveToStart(float paramFloat)
  {
    int i = this.mFrom;
    setTargetOffsetTopAndBottom(i + (int)((this.mOriginalOffsetTop - i) * paramFloat) - this.mCircleView.getTop(), false);
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent)
  {
    int i = MotionEventCompat.getActionIndex(paramMotionEvent);
    if (MotionEventCompat.getPointerId(paramMotionEvent, i) == this.mActivePointerId)
    {
      if (i == 0) {
        i = 1;
      } else {
        i = 0;
      }
      this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, i);
    }
  }
  
  private void setAnimationProgress(float paramFloat)
  {
    if (isAlphaUsedForScale())
    {
      setColorViewAlpha((int)(paramFloat * 255.0F));
    }
    else
    {
      ViewCompat.setScaleX(this.mCircleView, paramFloat);
      ViewCompat.setScaleY(this.mCircleView, paramFloat);
    }
  }
  
  private void setColorViewAlpha(int paramInt)
  {
    this.mProgress.setAlpha(paramInt);
  }
  
  private void setRefreshing(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.mRefreshing != paramBoolean1)
    {
      this.mNotify = paramBoolean2;
      ensureTarget();
      this.mRefreshing = paramBoolean1;
      if (this.mRefreshing) {
        animateOffsetToCorrectPosition(this.mCurrentTargetOffsetTop, this.mRefreshListener);
      } else {
        startScaleDownAnimation(this.mRefreshListener);
      }
    }
  }
  
  private void setTargetOffsetTopAndBottom(int paramInt, boolean paramBoolean)
  {
    this.mCircleView.bringToFront();
    this.mCircleView.offsetTopAndBottom(paramInt);
    this.mCurrentTargetOffsetTop = this.mCircleView.getTop();
    if ((paramBoolean) && (Build.VERSION.SDK_INT < 11)) {
      invalidate();
    }
  }
  
  private Animation startAlphaAnimation(final int paramInt1, final int paramInt2)
  {
    if ((this.mScale) && (isAlphaUsedForScale())) {
      return null;
    }
    Animation local4 = new Animation()
    {
      public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        paramAnonymousTransformation = SwipeRefreshLayout.this.mProgress;
        int i = paramInt1;
        paramAnonymousTransformation.setAlpha((int)(i + (paramInt2 - i) * paramAnonymousFloat));
      }
    };
    local4.setDuration(300L);
    this.mCircleView.setAnimationListener(null);
    this.mCircleView.clearAnimation();
    this.mCircleView.startAnimation(local4);
    return local4;
  }
  
  @SuppressLint({"NewApi"})
  private void startProgressAlphaMaxAnimation()
  {
    this.mAlphaMaxAnimation = startAlphaAnimation(this.mProgress.getAlpha(), 255);
  }
  
  @SuppressLint({"NewApi"})
  private void startProgressAlphaStartAnimation()
  {
    this.mAlphaStartAnimation = startAlphaAnimation(this.mProgress.getAlpha(), 76);
  }
  
  private void startScaleDownAnimation(Animation.AnimationListener paramAnimationListener)
  {
    this.mScaleDownAnimation = new Animation()
    {
      public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        SwipeRefreshLayout.this.setAnimationProgress(1.0F - paramAnonymousFloat);
      }
    };
    this.mScaleDownAnimation.setDuration(150L);
    this.mCircleView.setAnimationListener(paramAnimationListener);
    this.mCircleView.clearAnimation();
    this.mCircleView.startAnimation(this.mScaleDownAnimation);
  }
  
  @SuppressLint({"NewApi"})
  private void startScaleDownReturnToStartAnimation(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    this.mFrom = paramInt;
    if (isAlphaUsedForScale()) {
      this.mStartingScale = this.mProgress.getAlpha();
    } else {
      this.mStartingScale = ViewCompat.getScaleX(this.mCircleView);
    }
    this.mScaleDownToStartAnimation = new Animation()
    {
      public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        float f2 = SwipeRefreshLayout.this.mStartingScale;
        float f1 = -SwipeRefreshLayout.this.mStartingScale;
        SwipeRefreshLayout.this.setAnimationProgress(f2 + f1 * paramAnonymousFloat);
        SwipeRefreshLayout.this.moveToStart(paramAnonymousFloat);
      }
    };
    this.mScaleDownToStartAnimation.setDuration(150L);
    if (paramAnimationListener != null) {
      this.mCircleView.setAnimationListener(paramAnimationListener);
    }
    this.mCircleView.clearAnimation();
    this.mCircleView.startAnimation(this.mScaleDownToStartAnimation);
  }
  
  private void startScaleUpAnimation(Animation.AnimationListener paramAnimationListener)
  {
    this.mCircleView.setVisibility(0);
    if (Build.VERSION.SDK_INT >= 11) {
      this.mProgress.setAlpha(255);
    }
    this.mScaleAnimation = new Animation()
    {
      public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        SwipeRefreshLayout.this.setAnimationProgress(paramAnonymousFloat);
      }
    };
    this.mScaleAnimation.setDuration(this.mMediumAnimationDuration);
    if (paramAnimationListener != null) {
      this.mCircleView.setAnimationListener(paramAnimationListener);
    }
    this.mCircleView.clearAnimation();
    this.mCircleView.startAnimation(this.mScaleAnimation);
  }
  
  public boolean canChildScrollUp()
  {
    if (Build.VERSION.SDK_INT < 14)
    {
      Object localObject = this.mTarget;
      boolean bool1 = localObject instanceof AbsListView;
      boolean bool3 = true;
      boolean bool2 = true;
      if (bool1)
      {
        localObject = (AbsListView)localObject;
        if (((AbsListView)localObject).getChildCount() > 0)
        {
          bool1 = bool2;
          if (((AbsListView)localObject).getFirstVisiblePosition() > 0) {
            break label77;
          }
          if (((AbsListView)localObject).getChildAt(0).getTop() < ((AbsListView)localObject).getPaddingTop())
          {
            bool1 = bool2;
            break label77;
          }
        }
        bool1 = false;
        label77:
        return bool1;
      }
      bool1 = bool3;
      if (!ViewCompat.canScrollVertically((View)localObject, -1)) {
        if (this.mTarget.getScrollY() > 0) {
          bool1 = bool3;
        } else {
          bool1 = false;
        }
      }
      return bool1;
    }
    return ViewCompat.canScrollVertically(this.mTarget, -1);
  }
  
  protected void ensureTarget()
  {
    if (this.mTarget == null) {
      for (int i = 0; i < getChildCount(); i++)
      {
        View localView = getChildAt(i);
        if (!localView.equals(this.mCircleView))
        {
          this.mTarget = localView;
          break;
        }
      }
    }
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2)
  {
    int i = this.mCircleViewIndex;
    if (i < 0) {
      return paramInt2;
    }
    if (paramInt2 == paramInt1 - 1) {
      return i;
    }
    if (paramInt2 >= i) {
      return paramInt2 + 1;
    }
    return paramInt2;
  }
  
  public int getProgressCircleDiameter()
  {
    CircleImageView localCircleImageView = this.mCircleView;
    int i;
    if (localCircleImageView != null) {
      i = localCircleImageView.getMeasuredHeight();
    } else {
      i = 0;
    }
    return i;
  }
  
  public boolean isRefreshing()
  {
    return this.mRefreshing;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    ensureTarget();
    int i = MotionEventCompat.getActionMasked(paramMotionEvent);
    if ((this.mReturningToStart) && (i == 0)) {
      this.mReturningToStart = false;
    }
    if ((isEnabled()) && (!this.mReturningToStart) && (!canChildScrollUp()) && (!this.mRefreshing))
    {
      if (i != 6)
      {
        float f1;
        switch (i)
        {
        default: 
          break;
        case 2: 
          i = this.mActivePointerId;
          if (i == -1)
          {
            Log.e(LOG_TAG, "Got ACTION_MOVE event but don't have an active pointer id.");
            return false;
          }
          f1 = getMotionEventY(paramMotionEvent, i);
          if (f1 == -1.0F) {
            return false;
          }
          float f2 = this.mInitialDownY;
          i = this.mTouchSlop;
          if ((f1 - f2 <= i) || (this.mIsBeingDragged)) {
            break;
          }
          this.mInitialMotionY = (f2 + i);
          this.mIsBeingDragged = true;
          this.mProgress.setAlpha(76);
          break;
        case 1: 
        case 3: 
          this.mIsBeingDragged = false;
          this.mActivePointerId = -1;
          break;
        case 0: 
          setTargetOffsetTopAndBottom(this.mOriginalOffsetTop - this.mCircleView.getTop(), true);
          this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, 0);
          this.mIsBeingDragged = false;
          f1 = getMotionEventY(paramMotionEvent, this.mActivePointerId);
          if (f1 == -1.0F) {
            return false;
          }
          this.mInitialDownY = f1;
          break;
        }
      }
      else
      {
        onSecondaryPointerUp(paramMotionEvent);
      }
      return this.mIsBeingDragged;
    }
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt1 = getMeasuredWidth();
    paramInt4 = getMeasuredHeight();
    if (getChildCount() == 0) {
      return;
    }
    if (this.mTarget == null) {
      ensureTarget();
    }
    Object localObject = this.mTarget;
    if (localObject == null) {
      return;
    }
    paramInt3 = getPaddingLeft();
    paramInt2 = getPaddingTop();
    ((View)localObject).layout(paramInt3, paramInt2, paramInt1 - getPaddingLeft() - getPaddingRight() + paramInt3, paramInt4 - getPaddingTop() - getPaddingBottom() + paramInt2);
    paramInt3 = this.mCircleView.getMeasuredWidth();
    paramInt2 = this.mCircleView.getMeasuredHeight();
    localObject = this.mCircleView;
    paramInt1 /= 2;
    paramInt3 /= 2;
    paramInt4 = this.mCurrentTargetOffsetTop;
    ((CircleImageView)localObject).layout(paramInt1 - paramInt3, paramInt4, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if (this.mTarget == null) {
      ensureTarget();
    }
    View localView = this.mTarget;
    if (localView == null) {
      return;
    }
    localView.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), 1073741824));
    this.mCircleView.measure(View.MeasureSpec.makeMeasureSpec(this.mCircleWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(this.mCircleHeight, 1073741824));
    if ((!this.mUsingCustomStart) && (!this.mOriginalOffsetCalculated))
    {
      this.mOriginalOffsetCalculated = true;
      paramInt1 = -this.mCircleView.getMeasuredHeight();
      this.mOriginalOffsetTop = paramInt1;
      this.mCurrentTargetOffsetTop = paramInt1;
    }
    this.mCircleViewIndex = -1;
    for (paramInt1 = 0; paramInt1 < getChildCount(); paramInt1++) {
      if (getChildAt(paramInt1) == this.mCircleView)
      {
        this.mCircleViewIndex = paramInt1;
        break;
      }
    }
  }
  
  @SuppressLint({"NewApi"})
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = MotionEventCompat.getActionMasked(paramMotionEvent);
    if ((this.mReturningToStart) && (i == 0)) {
      this.mReturningToStart = false;
    }
    if ((isEnabled()) && (!this.mReturningToStart) && (!canChildScrollUp()))
    {
      float f2;
      float f1;
      int j;
      switch (i)
      {
      case 4: 
      default: 
        break;
      case 6: 
        onSecondaryPointerUp(paramMotionEvent);
        break;
      case 5: 
        this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, MotionEventCompat.getActionIndex(paramMotionEvent));
        break;
      case 2: 
        i = MotionEventCompat.findPointerIndex(paramMotionEvent, this.mActivePointerId);
        if (i < 0)
        {
          Log.e(LOG_TAG, "Got ACTION_MOVE event but have an invalid active pointer id.");
          return false;
        }
        f2 = (MotionEventCompat.getY(paramMotionEvent, i) - this.mInitialMotionY) * 0.5F;
        if (this.mIsBeingDragged)
        {
          this.mProgress.showArrow(true);
          f1 = f2 / this.mTotalDragDistance;
          if (f1 < 0.0F) {
            return false;
          }
          float f4 = Math.min(1.0F, Math.abs(f1));
          double d1 = f4;
          Double.isNaN(d1);
          float f3 = (float)Math.max(d1 - 0.4D, 0.0D) * 5.0F / 3.0F;
          float f6 = Math.abs(f2);
          float f5 = this.mTotalDragDistance;
          if (this.mUsingCustomStart) {
            f1 = this.mSpinnerFinalOffset - this.mOriginalOffsetTop;
          } else {
            f1 = this.mSpinnerFinalOffset;
          }
          d1 = Math.max(0.0F, Math.min(f6 - f5, f1 * 2.0F) / f1) / 4.0F;
          double d2 = Math.pow(d1, 2.0D);
          Double.isNaN(d1);
          f5 = (float)(d1 - d2) * 2.0F;
          i = this.mOriginalOffsetTop;
          j = (int)(f1 * f4 + f1 * f5 * 2.0F);
          if (this.mCircleView.getVisibility() != 0) {
            this.mCircleView.setVisibility(0);
          }
          if (!this.mScale)
          {
            ViewCompat.setScaleX(this.mCircleView, 1.0F);
            ViewCompat.setScaleY(this.mCircleView, 1.0F);
          }
          f1 = this.mTotalDragDistance;
          if (f2 < f1)
          {
            if (this.mScale) {
              setAnimationProgress(f2 / f1);
            }
            if ((this.mProgress.getAlpha() > 76) && (!isAnimationRunning(this.mAlphaStartAnimation))) {
              startProgressAlphaStartAnimation();
            }
            this.mProgress.setStartEndTrim(0.0F, Math.min(0.8F, f3 * 0.8F));
            this.mProgress.setArrowScale(Math.min(1.0F, f3));
          }
          else if ((this.mProgress.getAlpha() < 255) && (!isAnimationRunning(this.mAlphaMaxAnimation)))
          {
            startProgressAlphaMaxAnimation();
          }
          this.mProgress.setProgressRotation((f3 * 0.4F - 0.25F + f5 * 2.0F) * 0.5F);
          setTargetOffsetTopAndBottom(i + j - this.mCurrentTargetOffsetTop, true);
        }
        break;
      case 1: 
      case 3: 
        j = this.mActivePointerId;
        if (j == -1)
        {
          if (i == 1) {
            Log.e(LOG_TAG, "Got ACTION_UP event but don't have an active pointer id.");
          }
          return false;
        }
        f1 = MotionEventCompat.getY(paramMotionEvent, MotionEventCompat.findPointerIndex(paramMotionEvent, j));
        f2 = this.mInitialMotionY;
        this.mIsBeingDragged = false;
        if ((f1 - f2) * 0.5F > this.mTotalDragDistance)
        {
          setRefreshing(true, true);
        }
        else
        {
          this.mRefreshing = false;
          this.mProgress.setStartEndTrim(0.0F, 0.0F);
          paramMotionEvent = null;
          if (!this.mScale) {
            paramMotionEvent = new Animation.AnimationListener()
            {
              public void onAnimationEnd(Animation paramAnonymousAnimation)
              {
                if (!SwipeRefreshLayout.this.mScale) {
                  SwipeRefreshLayout.this.startScaleDownAnimation(null);
                }
              }
              
              public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
              
              public void onAnimationStart(Animation paramAnonymousAnimation) {}
            };
          }
          animateOffsetToStartPosition(this.mCurrentTargetOffsetTop, paramMotionEvent);
          this.mProgress.showArrow(false);
        }
        this.mActivePointerId = -1;
        return false;
      case 0: 
        this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, 0);
        this.mIsBeingDragged = false;
      }
      return true;
    }
    return false;
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {}
  
  @Deprecated
  public void setColorScheme(int... paramVarArgs)
  {
    setColorSchemeResources(paramVarArgs);
  }
  
  public void setColorSchemeColors(int... paramVarArgs)
  {
    ensureTarget();
    this.mProgress.setColorSchemeColors(paramVarArgs);
  }
  
  public void setColorSchemeResources(int... paramVarArgs)
  {
    Resources localResources = getResources();
    int[] arrayOfInt = new int[paramVarArgs.length];
    for (int i = 0; i < paramVarArgs.length; i++) {
      arrayOfInt[i] = localResources.getColor(paramVarArgs[i]);
    }
    setColorSchemeColors(arrayOfInt);
  }
  
  public void setDistanceToTriggerSync(int paramInt)
  {
    this.mTotalDragDistance = paramInt;
  }
  
  public void setOnRefreshListener(OnRefreshListener paramOnRefreshListener)
  {
    this.mListener = paramOnRefreshListener;
  }
  
  @Deprecated
  public void setProgressBackgroundColor(int paramInt)
  {
    setProgressBackgroundColorSchemeResource(paramInt);
  }
  
  public void setProgressBackgroundColorSchemeColor(int paramInt)
  {
    this.mCircleView.setBackgroundColor(paramInt);
    this.mProgress.setBackgroundColor(paramInt);
  }
  
  public void setProgressBackgroundColorSchemeResource(int paramInt)
  {
    setProgressBackgroundColorSchemeColor(getResources().getColor(paramInt));
  }
  
  public void setProgressViewEndTarget(boolean paramBoolean, int paramInt)
  {
    this.mSpinnerFinalOffset = paramInt;
    this.mScale = paramBoolean;
    this.mCircleView.invalidate();
  }
  
  public void setProgressViewOffset(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    this.mScale = paramBoolean;
    this.mCircleView.setVisibility(8);
    this.mCurrentTargetOffsetTop = paramInt1;
    this.mOriginalOffsetTop = paramInt1;
    this.mSpinnerFinalOffset = paramInt2;
    this.mUsingCustomStart = true;
    this.mCircleView.invalidate();
  }
  
  public void setRefreshing(boolean paramBoolean)
  {
    if ((paramBoolean) && (this.mRefreshing != paramBoolean))
    {
      this.mRefreshing = paramBoolean;
      int i;
      if (!this.mUsingCustomStart) {
        i = (int)(this.mSpinnerFinalOffset + this.mOriginalOffsetTop);
      } else {
        i = (int)this.mSpinnerFinalOffset;
      }
      setTargetOffsetTopAndBottom(i - this.mCurrentTargetOffsetTop, true);
      this.mNotify = false;
      startScaleUpAnimation(this.mRefreshListener);
    }
    else
    {
      setRefreshing(paramBoolean, false);
    }
  }
  
  public void setSize(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1)) {
      return;
    }
    DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
    int i;
    if (paramInt == 0)
    {
      i = (int)(localDisplayMetrics.density * 56.0F);
      this.mCircleWidth = i;
      this.mCircleHeight = i;
    }
    else
    {
      i = (int)(localDisplayMetrics.density * 40.0F);
      this.mCircleWidth = i;
      this.mCircleHeight = i;
    }
    this.mCircleView.setImageDrawable(null);
    this.mProgress.updateSizes(paramInt);
    this.mCircleView.setImageDrawable(this.mProgress);
  }
  
  public static abstract interface OnRefreshListener
  {
    public abstract void onRefresh();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/swiperefresh/SwipeRefreshLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */