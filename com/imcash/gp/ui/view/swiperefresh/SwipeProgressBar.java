package com.imcash.gp.ui.view.swiperefresh;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

final class SwipeProgressBar
{
  private static final int ANIMATION_DURATION_MS = 2000;
  private static final int COLOR1 = -1291845632;
  private static final int COLOR2 = Integer.MIN_VALUE;
  private static final int COLOR3 = 1291845632;
  private static final int COLOR4 = 436207616;
  private static final int FINISH_ANIMATION_DURATION_MS = 1000;
  private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
  private Rect mBounds = new Rect();
  private final RectF mClipRect = new RectF();
  private int mColor1;
  private int mColor2;
  private int mColor3;
  private int mColor4;
  private long mFinishTime;
  private final Paint mPaint = new Paint();
  private View mParent;
  private boolean mRunning;
  private long mStartTime;
  private float mTriggerPercentage;
  
  public SwipeProgressBar(View paramView)
  {
    this.mParent = paramView;
    this.mColor1 = -1291845632;
    this.mColor2 = Integer.MIN_VALUE;
    this.mColor3 = 1291845632;
    this.mColor4 = 436207616;
  }
  
  private void drawCircle(Canvas paramCanvas, float paramFloat1, float paramFloat2, int paramInt, float paramFloat3)
  {
    this.mPaint.setColor(paramInt);
    paramCanvas.save();
    paramCanvas.translate(paramFloat1, paramFloat2);
    paramFloat2 = INTERPOLATOR.getInterpolation(paramFloat3);
    paramCanvas.scale(paramFloat2, paramFloat2);
    paramCanvas.drawCircle(0.0F, 0.0F, paramFloat1, this.mPaint);
    paramCanvas.restore();
  }
  
  private void drawTrigger(Canvas paramCanvas, int paramInt1, int paramInt2)
  {
    this.mPaint.setColor(this.mColor1);
    float f = paramInt1;
    paramCanvas.drawCircle(f, paramInt2, this.mTriggerPercentage * f, this.mPaint);
  }
  
  void draw(Canvas paramCanvas)
  {
    int j = this.mBounds.width();
    int i = this.mBounds.height();
    int n = j / 2;
    int m = i / 2 + this.mBounds.top;
    j = paramCanvas.save();
    paramCanvas.clipRect(this.mBounds);
    float f1;
    if ((!this.mRunning) && (this.mFinishTime <= 0L))
    {
      f1 = this.mTriggerPercentage;
      i = j;
      if (f1 > 0.0F)
      {
        i = j;
        if (f1 <= 1.0D)
        {
          drawTrigger(paramCanvas, n, m);
          i = j;
        }
      }
    }
    else
    {
      long l1 = AnimationUtils.currentAnimationTimeMillis();
      long l3 = this.mStartTime;
      long l2 = (l1 - l3) / 2000L;
      f1 = (float)((l1 - l3) % 2000L) / 20.0F;
      boolean bool = this.mRunning;
      int k = 0;
      float f2;
      if (!bool)
      {
        l3 = this.mFinishTime;
        if (l1 - l3 >= 1000L)
        {
          this.mFinishTime = 0L;
          return;
        }
        float f3 = (float)((l1 - l3) % 1000L) / 10.0F / 100.0F;
        f2 = n;
        f3 = INTERPOLATOR.getInterpolation(f3) * f2;
        this.mClipRect.set(f2 - f3, 0.0F, f2 + f3, i);
        paramCanvas.saveLayerAlpha(this.mClipRect, 0, 0);
        k = 1;
      }
      if (l2 == 0L) {
        paramCanvas.drawColor(this.mColor1);
      } else if ((f1 >= 0.0F) && (f1 < 25.0F)) {
        paramCanvas.drawColor(this.mColor4);
      } else if ((f1 >= 25.0F) && (f1 < 50.0F)) {
        paramCanvas.drawColor(this.mColor1);
      } else if ((f1 >= 50.0F) && (f1 < 75.0F)) {
        paramCanvas.drawColor(this.mColor2);
      } else {
        paramCanvas.drawColor(this.mColor3);
      }
      if ((f1 >= 0.0F) && (f1 <= 25.0F))
      {
        f2 = (f1 + 25.0F) * 2.0F / 100.0F;
        drawCircle(paramCanvas, n, m, this.mColor1, f2);
      }
      if ((f1 >= 0.0F) && (f1 <= 50.0F))
      {
        f2 = f1 * 2.0F / 100.0F;
        drawCircle(paramCanvas, n, m, this.mColor2, f2);
      }
      if ((f1 >= 25.0F) && (f1 <= 75.0F))
      {
        f2 = (f1 - 25.0F) * 2.0F / 100.0F;
        drawCircle(paramCanvas, n, m, this.mColor3, f2);
      }
      if ((f1 >= 50.0F) && (f1 <= 100.0F))
      {
        f2 = (f1 - 50.0F) * 2.0F / 100.0F;
        drawCircle(paramCanvas, n, m, this.mColor4, f2);
      }
      if ((f1 >= 75.0F) && (f1 <= 100.0F))
      {
        f1 = (f1 - 75.0F) * 2.0F / 100.0F;
        drawCircle(paramCanvas, n, m, this.mColor1, f1);
      }
      i = j;
      if (this.mTriggerPercentage > 0.0F)
      {
        i = j;
        if (k != 0)
        {
          paramCanvas.restoreToCount(j);
          i = paramCanvas.save();
          paramCanvas.clipRect(this.mBounds);
          drawTrigger(paramCanvas, n, m);
        }
      }
      ViewCompat.postInvalidateOnAnimation(this.mParent, this.mBounds.left, this.mBounds.top, this.mBounds.right, this.mBounds.bottom);
    }
    paramCanvas.restoreToCount(i);
  }
  
  boolean isRunning()
  {
    boolean bool;
    if ((!this.mRunning) && (this.mFinishTime <= 0L)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Rect localRect = this.mBounds;
    localRect.left = paramInt1;
    localRect.top = paramInt2;
    localRect.right = paramInt3;
    localRect.bottom = paramInt4;
  }
  
  void setColorScheme(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.mColor1 = paramInt1;
    this.mColor2 = paramInt2;
    this.mColor3 = paramInt3;
    this.mColor4 = paramInt4;
  }
  
  void setTriggerPercentage(float paramFloat)
  {
    this.mTriggerPercentage = paramFloat;
    this.mStartTime = 0L;
    ViewCompat.postInvalidateOnAnimation(this.mParent, this.mBounds.left, this.mBounds.top, this.mBounds.right, this.mBounds.bottom);
  }
  
  void start()
  {
    if (!this.mRunning)
    {
      this.mTriggerPercentage = 0.0F;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mRunning = true;
      this.mParent.postInvalidate();
    }
  }
  
  void stop()
  {
    if (this.mRunning)
    {
      this.mTriggerPercentage = 0.0F;
      this.mFinishTime = AnimationUtils.currentAnimationTimeMillis();
      this.mRunning = false;
      this.mParent.postInvalidate();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/swiperefresh/SwipeProgressBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */