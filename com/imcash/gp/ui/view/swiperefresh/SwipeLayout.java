package com.imcash.gp.ui.view.swiperefresh;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Build.VERSION;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.AbsListView;

public class SwipeLayout
  extends SwipeRefreshLayout
{
  private static final float ACCELERATE_INTERPOLATION_FACTOR = 1.5F;
  private static final float MAX_SWIPE_DISTANCE_FACTOR = 0.6F;
  private static final float PROGRESS_BAR_HEIGHT = 4.0F;
  private static final int REFRESH_TRIGGER_DISTANCE = 120;
  private static final String TAG = "SwipeLayout";
  public final int ALL_Module = 0;
  public final int ALL_SHUT_Module = 255;
  public final int ONLY_DOWN_Module = 238;
  public final int ONLY_UP_Module = 221;
  private int loadStyle = 238;
  private AccelerateInterpolator mAccelerateInterpolator;
  private final Runnable mCancelRefresh = new Runnable()
  {
    public void run()
    {
      SwipeLayout localSwipeLayout = SwipeLayout.this;
      localSwipeLayout.mReturningToStart = true;
      if (localSwipeLayout.mSwipeProgressBar != null)
      {
        localSwipeLayout = SwipeLayout.this;
        SwipeLayout.access$002(localSwipeLayout, localSwipeLayout.mCurrPercentage);
        SwipeLayout.this.mShrinkTrigger.setDuration(SwipeLayout.this.mMediumAnimationDuration);
        SwipeLayout.this.mShrinkTrigger.setAnimationListener(SwipeLayout.this.mShrinkAnimationListener);
        SwipeLayout.this.mShrinkTrigger.reset();
        SwipeLayout.this.mShrinkTrigger.setInterpolator(SwipeLayout.this.mAccelerateInterpolator);
        localSwipeLayout = SwipeLayout.this;
        localSwipeLayout.startAnimation(localSwipeLayout.mShrinkTrigger);
      }
    }
  };
  private float mCurrPercentage = 0.0F;
  private float mDistanceToTriggerSync = -1.0F;
  private float mFromPercentage = 0.0F;
  private float mInitialUpwardY;
  private int mMediumAnimationDuration;
  private int mPointerIndex = -1;
  private int mProgressBarHeight;
  private final Animation.AnimationListener mShrinkAnimationListener = new Animation.AnimationListener()
  {
    public void onAnimationEnd(Animation paramAnonymousAnimation)
    {
      SwipeLayout.access$202(SwipeLayout.this, 0.0F);
    }
    
    public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
    
    public void onAnimationStart(Animation paramAnonymousAnimation) {}
  };
  private Animation mShrinkTrigger = new Animation()
  {
    public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
    {
      float f1 = SwipeLayout.this.mFromPercentage;
      float f2 = SwipeLayout.this.mFromPercentage;
      SwipeLayout.this.mSwipeProgressBar.setTriggerPercentage(f1 + (0.0F - f2) * paramAnonymousFloat);
    }
  };
  private SwipeProgressBar mSwipeProgressBar;
  private boolean mUpward = false;
  private onUpwardListener mUpwardListener;
  private boolean mUpwarding = false;
  
  public SwipeLayout(Context paramContext)
  {
    super(paramContext, null);
  }
  
  public SwipeLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    createProgressView();
  }
  
  private void createProgressView()
  {
    this.mSwipeProgressBar = new SwipeProgressBar(this);
    this.mMediumAnimationDuration = getResources().getInteger(17694721);
    this.mProgressBarHeight = ((int)(getResources().getDisplayMetrics().density * 4.0F));
    this.mAccelerateInterpolator = new AccelerateInterpolator(1.5F);
    ViewCompat.setChildrenDrawingOrderEnabled(this, true);
  }
  
  private void setTriggerPercentage(float paramFloat)
  {
    if (paramFloat == 0.0F)
    {
      this.mCurrPercentage = 0.0F;
      return;
    }
    this.mCurrPercentage = paramFloat;
    this.mSwipeProgressBar.setTriggerPercentage(paramFloat);
  }
  
  private void startLoad()
  {
    this.mUpwarding = true;
    setRefreshingBottom(true);
    onUpwardListener localonUpwardListener = this.mUpwardListener;
    if (localonUpwardListener != null) {
      localonUpwardListener.onUpward();
    }
  }
  
  public boolean canChildScrollDown()
  {
    int i = Build.VERSION.SDK_INT;
    boolean bool3 = true;
    boolean bool2 = true;
    if (i < 14)
    {
      if ((this.mTarget instanceof AbsListView))
      {
        AbsListView localAbsListView = (AbsListView)this.mTarget;
        View localView = localAbsListView.getChildAt(localAbsListView.getChildCount() - 1);
        if (localView != null)
        {
          bool1 = bool2;
          if (localAbsListView.getLastVisiblePosition() == localAbsListView.getCount() - 1) {
            if (localView.getBottom() > localAbsListView.getBottom() - localAbsListView.getPaddingBottom()) {
              bool1 = bool2;
            } else {
              bool1 = false;
            }
          }
          return bool1;
        }
        return false;
      }
      boolean bool1 = bool3;
      if (!ViewCompat.canScrollVertically(this.mTarget, 1)) {
        if (this.mTarget.getHeight() - this.mTarget.getScrollY() > 0) {
          bool1 = bool3;
        } else {
          bool1 = false;
        }
      }
      return bool1;
    }
    return ViewCompat.canScrollVertically(this.mTarget, 1);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    this.mSwipeProgressBar.draw(paramCanvas);
  }
  
  protected void ensureTarget()
  {
    super.ensureTarget();
    if ((this.mDistanceToTriggerSync == -1.0F) && (getParent() != null) && (((View)getParent()).getHeight() > 0))
    {
      DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
      this.mDistanceToTriggerSync = ((int)Math.min(((View)getParent()).getHeight() * 0.6F, localDisplayMetrics.density * 120.0F));
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int m = getMeasuredWidth();
    int i1 = getMeasuredHeight();
    if (getChildCount() == 0) {
      return;
    }
    if (this.mTarget == null) {
      ensureTarget();
    }
    if (this.mTarget == null) {
      return;
    }
    this.mSwipeProgressBar.setBounds(0, i1 - this.mProgressBarHeight, m, i1);
    int i2 = this.mCircleView.getMeasuredWidth();
    int k = this.mCircleView.getMeasuredHeight();
    paramCanvas = this.mTarget;
    int n = getPaddingLeft();
    int j = getPaddingTop();
    int i = this.mCurrentTargetOffsetTop;
    int i3 = -k;
    if ((i != i3) && (this.mCurrentTargetOffsetTop < 0))
    {
      i = j + (this.mCurrentTargetOffsetTop + k);
    }
    else
    {
      i = j;
      if (this.mCurrentTargetOffsetTop != i3)
      {
        i = j;
        if (this.mCurrentTargetOffsetTop >= 0) {
          i = j + (this.mCurrentTargetOffsetTop + k);
        }
      }
    }
    i3 = getPaddingLeft();
    int i5 = getPaddingRight();
    int i6 = getPaddingTop();
    int i4 = getPaddingBottom();
    CircleImageView localCircleImageView = this.mCircleView;
    j = m / 2;
    i2 /= 2;
    localCircleImageView.layout(j - i2, this.mCurrentTargetOffsetTop, j + i2, this.mCurrentTargetOffsetTop + k);
    paramCanvas.layout(n, i, m - i3 - i5 + n, i1 - i6 - i4 + i);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.loadStyle == 255) {
      return false;
    }
    ensureTarget();
    int i = MotionEventCompat.getActionMasked(paramMotionEvent);
    boolean bool1 = canChildScrollUp();
    boolean bool2 = canChildScrollDown();
    if ((this.loadStyle < 238) && ((bool1) || (!bool2)))
    {
      if ((this.mReturningToStart) && (i == 0)) {
        this.mReturningToStart = false;
      }
      if ((isEnabled()) && (!this.mReturningToStart) && ((!bool1) || (!bool2)) && (!this.mRefreshing) && (!this.mUpwarding))
      {
        if (!bool2) {
          switch (i)
          {
          default: 
            break;
          case 2: 
            this.mPointerIndex = MotionEventCompat.findPointerIndex(paramMotionEvent, this.mActivePointerId);
            i = this.mPointerIndex;
            if (i < 0)
            {
              Log.e("SwipeLayout", "Got ACTION_MOVE event but have an invalid activepointer id.");
              return false;
            }
            float f1 = MotionEventCompat.getY(paramMotionEvent, i);
            float f2 = this.mInitialUpwardY - f1;
            if ((f2 > this.mTouchSlop) && (!this.mIsBeingDragged))
            {
              f1 = this.mDistanceToTriggerSync;
              if (f2 > f1)
              {
                setTriggerPercentage(this.mAccelerateInterpolator.getInterpolation(1.0F));
                this.mUpward = true;
              }
              else
              {
                this.mUpward = false;
                setTriggerPercentage(this.mAccelerateInterpolator.getInterpolation(f2 / f1));
              }
              return true;
            }
            break;
          case 1: 
            if (this.mUpward)
            {
              startLoad();
              this.mUpward = false;
              return true;
            }
            removeCallbacks(this.mCancelRefresh);
            postDelayed(this.mCancelRefresh, 50L);
            break;
          case 0: 
            this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, 0);
            this.mPointerIndex = MotionEventCompat.findPointerIndex(paramMotionEvent, this.mActivePointerId);
            i = this.mPointerIndex;
            if (i < 0)
            {
              Log.e("SwipeLayout", "Got ACTION_MOVE event but have an invalid activepointer id.");
              return false;
            }
            this.mInitialUpwardY = MotionEventCompat.getY(paramMotionEvent, i);
          }
        }
        if (this.loadStyle == 221) {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    else if (this.loadStyle == 221)
    {
      return false;
    }
    return super.onInterceptTouchEvent(paramMotionEvent);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = MotionEventCompat.getActionMasked(paramMotionEvent);
    if ((!this.mUpwarding) && (!this.mRefreshing))
    {
      if (!canChildScrollDown()) {
        switch (i)
        {
        default: 
          break;
        case 2: 
          this.mPointerIndex = MotionEventCompat.findPointerIndex(paramMotionEvent, this.mActivePointerId);
          i = this.mPointerIndex;
          if (i < 0)
          {
            Log.e("SwipeLayout", "Got ACTION_MOVE event but have an invalid activepointer id.");
            return false;
          }
          float f1 = MotionEventCompat.getY(paramMotionEvent, i);
          f1 = this.mInitialUpwardY - f1;
          if ((f1 > this.mTouchSlop) && (!this.mIsBeingDragged))
          {
            float f2 = this.mDistanceToTriggerSync;
            if (f1 > f2)
            {
              setTriggerPercentage(this.mAccelerateInterpolator.getInterpolation(1.0F));
              this.mUpward = true;
            }
            else
            {
              this.mUpward = false;
              setTriggerPercentage(this.mAccelerateInterpolator.getInterpolation(f1 / f2));
            }
            return true;
          }
          break;
        case 1: 
          if (!this.mIsBeingDragged)
          {
            if (this.mUpward)
            {
              startLoad();
              this.mUpward = false;
              return true;
            }
            removeCallbacks(this.mCancelRefresh);
            postDelayed(this.mCancelRefresh, 50L);
            return true;
          }
          break;
        case 0: 
          this.mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, 0);
          this.mPointerIndex = MotionEventCompat.findPointerIndex(paramMotionEvent, this.mActivePointerId);
          i = this.mPointerIndex;
          if (i < 0)
          {
            Log.e("SwipeLayout", "Got ACTION_MOVE event but have an invalid activepointer id.");
            return false;
          }
          this.mInitialUpwardY = MotionEventCompat.getY(paramMotionEvent, i);
        }
      }
      return super.onTouchEvent(paramMotionEvent);
    }
    return true;
  }
  
  public void setColorAllColors(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    setColorSchemeColors(new int[] { paramInt1, paramInt2, paramInt3, paramInt4 });
    setColorBottomColors(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setColorAllResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Resources localResources = getResources();
    paramInt1 = localResources.getColor(paramInt1);
    paramInt2 = localResources.getColor(paramInt2);
    paramInt3 = localResources.getColor(paramInt3);
    paramInt4 = localResources.getColor(paramInt4);
    setColorSchemeColors(new int[] { paramInt1, paramInt2, paramInt3, paramInt4 });
    setColorBottomColors(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setColorBottomColors(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    ensureTarget();
    this.mSwipeProgressBar.setColorScheme(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setColorBottomResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Resources localResources = getResources();
    setColorBottomColors(localResources.getColor(paramInt1), localResources.getColor(paramInt2), localResources.getColor(paramInt3), localResources.getColor(paramInt4));
  }
  
  public void setModule(int paramInt)
  {
    this.loadStyle = paramInt;
  }
  
  public void setOnUpwardListener(onUpwardListener paramonUpwardListener)
  {
    this.mUpwardListener = paramonUpwardListener;
  }
  
  public void setRefreshingBottom(boolean paramBoolean)
  {
    if ((this.mRefreshing != paramBoolean) || (this.mUpwarding != paramBoolean))
    {
      ensureTarget();
      this.mCurrPercentage = 0.0F;
      this.mUpwarding = paramBoolean;
      if (this.mUpwarding) {
        this.mSwipeProgressBar.start();
      } else {
        this.mSwipeProgressBar.stop();
      }
    }
  }
  
  public static abstract interface onUpwardListener
  {
    public abstract void onUpward();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/swiperefresh/SwipeLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */