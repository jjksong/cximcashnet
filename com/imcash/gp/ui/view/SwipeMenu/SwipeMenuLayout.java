package com.imcash.gp.ui.view.SwipeMenu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.animation.Interpolator;
import android.widget.AbsListView.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class SwipeMenuLayout
  extends FrameLayout
{
  private static final int CONTENT_VIEW_ID = 1;
  private static final int MENU_VIEW_ID = 2;
  private static final int STATE_CLOSE = 0;
  private static final int STATE_OPEN = 1;
  private int MAX_VELOCITYX = -dp2px(500);
  private int MIN_FLING = dp2px(15);
  private boolean isFling;
  private int mBaseX;
  private Interpolator mCloseInterpolator;
  private ScrollerCompat mCloseScroller;
  private View mContentView;
  private int mDownX;
  private GestureDetectorCompat mGestureDetector;
  private GestureDetector.OnGestureListener mGestureListener;
  private SwipeMenuView mMenuView;
  private Interpolator mOpenInterpolator;
  private ScrollerCompat mOpenScroller;
  private boolean mSwipEnable = true;
  private int mSwipeDirection;
  private int position;
  private int state = 0;
  
  private SwipeMenuLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  private SwipeMenuLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SwipeMenuLayout(View paramView, SwipeMenuView paramSwipeMenuView)
  {
    this(paramView, paramSwipeMenuView, null, null);
  }
  
  public SwipeMenuLayout(View paramView, SwipeMenuView paramSwipeMenuView, Interpolator paramInterpolator1, Interpolator paramInterpolator2)
  {
    super(paramView.getContext());
    this.mCloseInterpolator = paramInterpolator1;
    this.mOpenInterpolator = paramInterpolator2;
    this.mContentView = paramView;
    this.mMenuView = paramSwipeMenuView;
    this.mMenuView.setLayout(this);
    init();
  }
  
  private int dp2px(int paramInt)
  {
    return (int)TypedValue.applyDimension(1, paramInt, getContext().getResources().getDisplayMetrics());
  }
  
  private void init()
  {
    setLayoutParams(new AbsListView.LayoutParams(-1, -2));
    this.mGestureListener = new GestureDetector.SimpleOnGestureListener()
    {
      public boolean onDown(MotionEvent paramAnonymousMotionEvent)
      {
        SwipeMenuLayout.access$002(SwipeMenuLayout.this, false);
        return true;
      }
      
      public boolean onFling(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
      {
        if ((Math.abs(paramAnonymousMotionEvent1.getX() - paramAnonymousMotionEvent2.getX()) > SwipeMenuLayout.this.MIN_FLING) && (paramAnonymousFloat1 < SwipeMenuLayout.this.MAX_VELOCITYX)) {
          SwipeMenuLayout.access$002(SwipeMenuLayout.this, true);
        }
        return super.onFling(paramAnonymousMotionEvent1, paramAnonymousMotionEvent2, paramAnonymousFloat1, paramAnonymousFloat2);
      }
    };
    this.mGestureDetector = new GestureDetectorCompat(getContext(), this.mGestureListener);
    if (this.mCloseInterpolator != null) {
      this.mCloseScroller = ScrollerCompat.create(getContext(), this.mCloseInterpolator);
    } else {
      this.mCloseScroller = ScrollerCompat.create(getContext());
    }
    if (this.mOpenInterpolator != null) {
      this.mOpenScroller = ScrollerCompat.create(getContext(), this.mOpenInterpolator);
    } else {
      this.mOpenScroller = ScrollerCompat.create(getContext());
    }
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -2);
    this.mContentView.setLayoutParams(localLayoutParams);
    if (this.mContentView.getId() < 1) {
      this.mContentView.setId(1);
    }
    this.mMenuView.setId(2);
    this.mMenuView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
    addView(this.mContentView);
    addView(this.mMenuView);
  }
  
  private void swipe(int paramInt)
  {
    if (!this.mSwipEnable) {
      return;
    }
    int i;
    if (Math.signum(paramInt) != this.mSwipeDirection)
    {
      i = 0;
    }
    else
    {
      i = paramInt;
      if (Math.abs(paramInt) > this.mMenuView.getWidth()) {
        i = this.mMenuView.getWidth() * this.mSwipeDirection;
      }
    }
    Object localObject = this.mContentView;
    paramInt = -i;
    ((View)localObject).layout(paramInt, ((View)localObject).getTop(), this.mContentView.getWidth() - i, getMeasuredHeight());
    if (this.mSwipeDirection == 1)
    {
      this.mMenuView.layout(this.mContentView.getWidth() - i, this.mMenuView.getTop(), this.mContentView.getWidth() + this.mMenuView.getWidth() - i, this.mMenuView.getBottom());
    }
    else
    {
      localObject = this.mMenuView;
      ((SwipeMenuView)localObject).layout(-((SwipeMenuView)localObject).getWidth() - i, this.mMenuView.getTop(), paramInt, this.mMenuView.getBottom());
    }
  }
  
  public void closeMenu()
  {
    if (this.mCloseScroller.computeScrollOffset()) {
      this.mCloseScroller.abortAnimation();
    }
    if (this.state == 1)
    {
      this.state = 0;
      swipe(0);
    }
  }
  
  public void computeScroll()
  {
    if (this.state == 1)
    {
      if (this.mOpenScroller.computeScrollOffset())
      {
        swipe(this.mOpenScroller.getCurrX() * this.mSwipeDirection);
        postInvalidate();
      }
    }
    else if (this.mCloseScroller.computeScrollOffset())
    {
      swipe((this.mBaseX - this.mCloseScroller.getCurrX()) * this.mSwipeDirection);
      postInvalidate();
    }
  }
  
  public View getContentView()
  {
    return this.mContentView;
  }
  
  public SwipeMenuView getMenuView()
  {
    return this.mMenuView;
  }
  
  public int getPosition()
  {
    return this.position;
  }
  
  public boolean getSwipEnable()
  {
    return this.mSwipEnable;
  }
  
  public boolean isOpen()
  {
    int i = this.state;
    boolean bool = true;
    if (i != 1) {
      bool = false;
    }
    return bool;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.mContentView.layout(0, 0, getMeasuredWidth(), this.mContentView.getMeasuredHeight());
    if (this.mSwipeDirection == 1)
    {
      this.mMenuView.layout(getMeasuredWidth(), 0, getMeasuredWidth() + this.mMenuView.getMeasuredWidth(), this.mContentView.getMeasuredHeight());
    }
    else
    {
      SwipeMenuView localSwipeMenuView = this.mMenuView;
      localSwipeMenuView.layout(-localSwipeMenuView.getMeasuredWidth(), 0, 0, this.mContentView.getMeasuredHeight());
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    this.mMenuView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public boolean onSwipe(MotionEvent paramMotionEvent)
  {
    this.mGestureDetector.onTouchEvent(paramMotionEvent);
    switch (paramMotionEvent.getAction())
    {
    default: 
      break;
    case 2: 
      int j = (int)(this.mDownX - paramMotionEvent.getX());
      int i = j;
      if (this.state == 1) {
        i = j + this.mMenuView.getWidth() * this.mSwipeDirection;
      }
      swipe(i);
      break;
    case 1: 
      if (((this.isFling) || (Math.abs(this.mDownX - paramMotionEvent.getX()) > this.mMenuView.getWidth() / 2)) && (Math.signum(this.mDownX - paramMotionEvent.getX()) == this.mSwipeDirection))
      {
        smoothOpenMenu();
      }
      else
      {
        smoothCloseMenu();
        return false;
      }
      break;
    case 0: 
      this.mDownX = ((int)paramMotionEvent.getX());
      this.isFling = false;
    }
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void openMenu()
  {
    if (!this.mSwipEnable) {
      return;
    }
    if (this.state == 0)
    {
      this.state = 1;
      swipe(this.mMenuView.getWidth() * this.mSwipeDirection);
    }
  }
  
  public void setMenuHeight(int paramInt)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("pos = ");
    ((StringBuilder)localObject).append(this.position);
    ((StringBuilder)localObject).append(", height = ");
    ((StringBuilder)localObject).append(paramInt);
    Log.i("byz", ((StringBuilder)localObject).toString());
    localObject = (FrameLayout.LayoutParams)this.mMenuView.getLayoutParams();
    if (((FrameLayout.LayoutParams)localObject).height != paramInt)
    {
      ((FrameLayout.LayoutParams)localObject).height = paramInt;
      localObject = this.mMenuView;
      ((SwipeMenuView)localObject).setLayoutParams(((SwipeMenuView)localObject).getLayoutParams());
    }
  }
  
  public void setPosition(int paramInt)
  {
    this.position = paramInt;
    this.mMenuView.setPosition(paramInt);
  }
  
  public void setSwipEnable(boolean paramBoolean)
  {
    this.mSwipEnable = paramBoolean;
  }
  
  public void setSwipeDirection(int paramInt)
  {
    this.mSwipeDirection = paramInt;
  }
  
  public void smoothCloseMenu()
  {
    this.state = 0;
    if (this.mSwipeDirection == 1)
    {
      this.mBaseX = (-this.mContentView.getLeft());
      this.mCloseScroller.startScroll(0, 0, this.mMenuView.getWidth(), 0, 350);
    }
    else
    {
      this.mBaseX = this.mMenuView.getRight();
      this.mCloseScroller.startScroll(0, 0, this.mMenuView.getWidth(), 0, 350);
    }
    postInvalidate();
  }
  
  public void smoothOpenMenu()
  {
    if (!this.mSwipEnable) {
      return;
    }
    this.state = 1;
    if (this.mSwipeDirection == 1) {
      this.mOpenScroller.startScroll(-this.mContentView.getLeft(), 0, this.mMenuView.getWidth(), 0, 350);
    } else {
      this.mOpenScroller.startScroll(this.mContentView.getLeft(), 0, this.mMenuView.getWidth(), 0, 350);
    }
    postInvalidate();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/SwipeMenu/SwipeMenuLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */