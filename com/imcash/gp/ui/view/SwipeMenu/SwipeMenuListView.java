package com.imcash.gp.ui.view.SwipeMenu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ListAdapter;
import android.widget.ListView;

public class SwipeMenuListView
  extends ListView
{
  public static final int DIRECTION_LEFT = 1;
  public static final int DIRECTION_RIGHT = -1;
  private static final int TOUCH_STATE_NONE = 0;
  private static final int TOUCH_STATE_X = 1;
  private static final int TOUCH_STATE_Y = 2;
  private int MAX_X = 3;
  private int MAX_Y = 5;
  private Interpolator mCloseInterpolator;
  private int mDirection = 1;
  private float mDownX;
  private float mDownY;
  private SwipeMenuCreator mMenuCreator;
  private OnMenuItemClickListener mOnMenuItemClickListener;
  private OnMenuStateChangeListener mOnMenuStateChangeListener;
  private OnSwipeListener mOnSwipeListener;
  private Interpolator mOpenInterpolator;
  private int mTouchPosition;
  private int mTouchState;
  private SwipeMenuLayout mTouchView;
  
  public SwipeMenuListView(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public SwipeMenuListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  public SwipeMenuListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }
  
  private int dp2px(int paramInt)
  {
    return (int)TypedValue.applyDimension(1, paramInt, getContext().getResources().getDisplayMetrics());
  }
  
  public static boolean inRangeOfView(View paramView, MotionEvent paramMotionEvent)
  {
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    int j = arrayOfInt[0];
    int i = arrayOfInt[1];
    return (paramMotionEvent.getRawX() >= j) && (paramMotionEvent.getRawX() <= j + paramView.getWidth()) && (paramMotionEvent.getRawY() >= i) && (paramMotionEvent.getRawY() <= i + paramView.getHeight());
  }
  
  private void init()
  {
    this.MAX_X = dp2px(this.MAX_X);
    this.MAX_Y = dp2px(this.MAX_Y);
    this.mTouchState = 0;
  }
  
  public Interpolator getCloseInterpolator()
  {
    return this.mCloseInterpolator;
  }
  
  public Interpolator getOpenInterpolator()
  {
    return this.mOpenInterpolator;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    if (i != 0)
    {
      float f2;
      float f1;
      if (i == 2)
      {
        f2 = Math.abs(paramMotionEvent.getY() - this.mDownY);
        f1 = Math.abs(paramMotionEvent.getX() - this.mDownX);
        if ((Math.abs(f2) > this.MAX_Y) || (Math.abs(f1) > this.MAX_X)) {}
      }
      else
      {
        return super.onInterceptTouchEvent(paramMotionEvent);
      }
      if (this.mTouchState == 0) {
        if (Math.abs(f2) > this.MAX_Y)
        {
          this.mTouchState = 2;
        }
        else if (f1 > this.MAX_X)
        {
          this.mTouchState = 1;
          paramMotionEvent = this.mOnSwipeListener;
          if (paramMotionEvent != null) {
            paramMotionEvent.onSwipeStart(this.mTouchPosition);
          }
        }
      }
      return true;
    }
    this.mDownX = paramMotionEvent.getX();
    this.mDownY = paramMotionEvent.getY();
    boolean bool2 = super.onInterceptTouchEvent(paramMotionEvent);
    this.mTouchState = 0;
    this.mTouchPosition = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
    Object localObject = getChildAt(this.mTouchPosition - getFirstVisiblePosition());
    if ((localObject instanceof SwipeMenuLayout))
    {
      localSwipeMenuLayout = this.mTouchView;
      if ((localSwipeMenuLayout != null) && (localSwipeMenuLayout.isOpen()) && (!inRangeOfView(this.mTouchView.getMenuView(), paramMotionEvent))) {
        return true;
      }
      this.mTouchView = ((SwipeMenuLayout)localObject);
      this.mTouchView.setSwipeDirection(this.mDirection);
    }
    SwipeMenuLayout localSwipeMenuLayout = this.mTouchView;
    boolean bool1 = bool2;
    if (localSwipeMenuLayout != null)
    {
      bool1 = bool2;
      if (localSwipeMenuLayout.isOpen())
      {
        bool1 = bool2;
        if (localObject != this.mTouchView) {
          bool1 = true;
        }
      }
    }
    localObject = this.mTouchView;
    if (localObject != null) {
      ((SwipeMenuLayout)localObject).onSwipe(paramMotionEvent);
    }
    return bool1;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((paramMotionEvent.getAction() != 0) && (this.mTouchView == null)) {
      return super.onTouchEvent(paramMotionEvent);
    }
    int i;
    Object localObject;
    switch (paramMotionEvent.getAction())
    {
    default: 
      break;
    case 2: 
      this.mTouchPosition = (pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY()) - getHeaderViewsCount());
      if ((this.mTouchView.getSwipEnable()) && (this.mTouchPosition == this.mTouchView.getPosition()))
      {
        float f1 = Math.abs(paramMotionEvent.getY() - this.mDownY);
        float f2 = Math.abs(paramMotionEvent.getX() - this.mDownX);
        i = this.mTouchState;
        if (i == 1)
        {
          localObject = this.mTouchView;
          if (localObject != null) {
            ((SwipeMenuLayout)localObject).onSwipe(paramMotionEvent);
          }
          getSelector().setState(new int[] { 0 });
          paramMotionEvent.setAction(3);
          super.onTouchEvent(paramMotionEvent);
          return true;
        }
        if (i == 0) {
          if (Math.abs(f1) > this.MAX_Y)
          {
            this.mTouchState = 2;
          }
          else if (f2 > this.MAX_X)
          {
            this.mTouchState = 1;
            localObject = this.mOnSwipeListener;
            if (localObject != null) {
              ((OnSwipeListener)localObject).onSwipeStart(this.mTouchPosition);
            }
          }
        }
      }
      break;
    case 1: 
      if (this.mTouchState == 1)
      {
        localObject = this.mTouchView;
        if (localObject != null)
        {
          boolean bool1 = ((SwipeMenuLayout)localObject).isOpen();
          this.mTouchView.onSwipe(paramMotionEvent);
          boolean bool2 = this.mTouchView.isOpen();
          if (bool1 != bool2)
          {
            localObject = this.mOnMenuStateChangeListener;
            if (localObject != null) {
              if (bool2) {
                ((OnMenuStateChangeListener)localObject).onMenuOpen(this.mTouchPosition);
              } else {
                ((OnMenuStateChangeListener)localObject).onMenuClose(this.mTouchPosition);
              }
            }
          }
          if (!bool2)
          {
            this.mTouchPosition = -1;
            this.mTouchView = null;
          }
        }
        localObject = this.mOnSwipeListener;
        if (localObject != null) {
          ((OnSwipeListener)localObject).onSwipeEnd(this.mTouchPosition);
        }
        paramMotionEvent.setAction(3);
        super.onTouchEvent(paramMotionEvent);
        return true;
      }
      break;
    case 0: 
      i = this.mTouchPosition;
      this.mDownX = paramMotionEvent.getX();
      this.mDownY = paramMotionEvent.getY();
      this.mTouchState = 0;
      this.mTouchPosition = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
      if (this.mTouchPosition == i)
      {
        localObject = this.mTouchView;
        if ((localObject != null) && (((SwipeMenuLayout)localObject).isOpen()))
        {
          this.mTouchState = 1;
          this.mTouchView.onSwipe(paramMotionEvent);
          return true;
        }
      }
      localObject = getChildAt(this.mTouchPosition - getFirstVisiblePosition());
      SwipeMenuLayout localSwipeMenuLayout = this.mTouchView;
      if ((localSwipeMenuLayout != null) && (localSwipeMenuLayout.isOpen()))
      {
        this.mTouchView.smoothCloseMenu();
        this.mTouchView = null;
        paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
        paramMotionEvent.setAction(3);
        onTouchEvent(paramMotionEvent);
        paramMotionEvent = this.mOnMenuStateChangeListener;
        if (paramMotionEvent != null) {
          paramMotionEvent.onMenuClose(i);
        }
        return true;
      }
      if ((localObject instanceof SwipeMenuLayout))
      {
        this.mTouchView = ((SwipeMenuLayout)localObject);
        this.mTouchView.setSwipeDirection(this.mDirection);
      }
      localObject = this.mTouchView;
      if (localObject != null) {
        ((SwipeMenuLayout)localObject).onSwipe(paramMotionEvent);
      }
      break;
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setAdapter(ListAdapter paramListAdapter)
  {
    super.setAdapter(new SwipeMenuAdapter(getContext(), paramListAdapter)
    {
      public void createMenu(SwipeMenu paramAnonymousSwipeMenu)
      {
        if (SwipeMenuListView.this.mMenuCreator != null) {
          SwipeMenuListView.this.mMenuCreator.create(paramAnonymousSwipeMenu);
        }
      }
      
      public void onItemClick(SwipeMenuView paramAnonymousSwipeMenuView, SwipeMenu paramAnonymousSwipeMenu, int paramAnonymousInt)
      {
        boolean bool;
        if (SwipeMenuListView.this.mOnMenuItemClickListener != null) {
          bool = SwipeMenuListView.this.mOnMenuItemClickListener.onMenuItemClick(paramAnonymousSwipeMenuView.getPosition(), paramAnonymousSwipeMenu, paramAnonymousInt);
        } else {
          bool = false;
        }
        if ((SwipeMenuListView.this.mTouchView != null) && (!bool)) {
          SwipeMenuListView.this.mTouchView.smoothCloseMenu();
        }
      }
    });
  }
  
  public void setCloseInterpolator(Interpolator paramInterpolator)
  {
    this.mCloseInterpolator = paramInterpolator;
  }
  
  public void setMenuCreator(SwipeMenuCreator paramSwipeMenuCreator)
  {
    this.mMenuCreator = paramSwipeMenuCreator;
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.mOnMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  public void setOnMenuStateChangeListener(OnMenuStateChangeListener paramOnMenuStateChangeListener)
  {
    this.mOnMenuStateChangeListener = paramOnMenuStateChangeListener;
  }
  
  public void setOnSwipeListener(OnSwipeListener paramOnSwipeListener)
  {
    this.mOnSwipeListener = paramOnSwipeListener;
  }
  
  public void setOpenInterpolator(Interpolator paramInterpolator)
  {
    this.mOpenInterpolator = paramInterpolator;
  }
  
  public void setSwipeDirection(int paramInt)
  {
    this.mDirection = paramInt;
  }
  
  public void smoothCloseMenu()
  {
    SwipeMenuLayout localSwipeMenuLayout = this.mTouchView;
    if ((localSwipeMenuLayout != null) && (localSwipeMenuLayout.isOpen())) {
      this.mTouchView.smoothCloseMenu();
    }
  }
  
  public void smoothOpenMenu(int paramInt)
  {
    if ((paramInt >= getFirstVisiblePosition()) && (paramInt <= getLastVisiblePosition()))
    {
      View localView = getChildAt(paramInt - getFirstVisiblePosition());
      if ((localView instanceof SwipeMenuLayout))
      {
        this.mTouchPosition = paramInt;
        SwipeMenuLayout localSwipeMenuLayout = this.mTouchView;
        if ((localSwipeMenuLayout != null) && (localSwipeMenuLayout.isOpen())) {
          this.mTouchView.smoothCloseMenu();
        }
        this.mTouchView = ((SwipeMenuLayout)localView);
        this.mTouchView.setSwipeDirection(this.mDirection);
        this.mTouchView.smoothOpenMenu();
      }
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean onMenuItemClick(int paramInt1, SwipeMenu paramSwipeMenu, int paramInt2);
  }
  
  public static abstract interface OnMenuStateChangeListener
  {
    public abstract void onMenuClose(int paramInt);
    
    public abstract void onMenuOpen(int paramInt);
  }
  
  public static abstract interface OnSwipeListener
  {
    public abstract void onSwipeEnd(int paramInt);
    
    public abstract void onSwipeStart(int paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/SwipeMenu/SwipeMenuListView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */