package com.imcash.gp.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.imcash.gp.R.styleable;

public class PersonaldDetailItem
  extends RelativeLayout
{
  private TextView contentIntroductionTx;
  private boolean contentIntroduction_visibility;
  private RelativeLayout contentLayout;
  private TextView contentNameTx;
  private int content_introduction_res;
  private boolean content_layout_visibility;
  private int content_name_res;
  private TextView contextTx;
  private boolean contextTx_visibility;
  private int context_res;
  private ImageView goIconImga;
  private int go_res;
  private boolean go_visibility;
  private ImageView headPortraitImg;
  private boolean headPortrait_visibility;
  private boolean head_visibility;
  private ImageView iconImg;
  private int icon_res;
  private int item_res;
  private RelativeLayout layout;
  private int layoutBackground;
  private Context mContext = null;
  private TextView mDetailname;
  private View mRootView = null;
  private int personald_res;
  
  public PersonaldDetailItem(Context paramContext)
  {
    super(paramContext);
    this.mContext = paramContext;
    initView();
  }
  
  public PersonaldDetailItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public PersonaldDetailItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.mContext = paramContext;
    initView();
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PersonaldDetail);
    this.icon_res = paramContext.getResourceId(12, 0);
    this.item_res = paramContext.getResourceId(14, 0);
    this.personald_res = paramContext.getResourceId(9, 0);
    this.context_res = paramContext.getResourceId(3, 0);
    this.go_res = paramContext.getResourceId(7, 0);
    this.content_name_res = paramContext.getResourceId(5, 0);
    this.content_introduction_res = paramContext.getResourceId(4, 0);
    this.head_visibility = paramContext.getBoolean(11, false);
    this.content_layout_visibility = paramContext.getBoolean(1, true);
    this.headPortrait_visibility = paramContext.getBoolean(10, false);
    this.contextTx_visibility = paramContext.getBoolean(2, true);
    this.go_visibility = paramContext.getBoolean(2, true);
    this.contentIntroduction_visibility = paramContext.getBoolean(2, true);
    this.layoutBackground = paramContext.getResourceId(3, 0);
    paramInt = this.icon_res;
    if (paramInt != 0) {
      this.iconImg.setImageResource(paramInt);
    }
    paramInt = this.item_res;
    if (paramInt != 0) {
      this.mDetailname.setText(paramInt);
    }
    paramInt = this.personald_res;
    if (paramInt != 0) {
      this.headPortraitImg.setImageResource(paramInt);
    }
    paramInt = this.context_res;
    if (paramInt != 0) {
      this.contextTx.setText(paramInt);
    }
    paramInt = this.go_res;
    if (paramInt != 0) {
      this.goIconImga.setImageResource(paramInt);
    }
    paramInt = this.content_name_res;
    if (paramInt != 0) {
      this.contentNameTx.setText(paramInt);
    }
    paramInt = this.content_introduction_res;
    if (paramInt != 0) {
      this.contentIntroductionTx.setText(paramInt);
    }
    if (this.head_visibility)
    {
      this.iconImg.setVisibility(0);
      this.mDetailname.setVisibility(8);
    }
    else
    {
      this.iconImg.setVisibility(8);
      this.mDetailname.setVisibility(0);
    }
    if (this.headPortrait_visibility) {
      this.headPortraitImg.setVisibility(0);
    } else {
      this.headPortraitImg.setVisibility(8);
    }
    if (this.contextTx_visibility) {
      this.contextTx.setVisibility(0);
    } else {
      this.contextTx.setVisibility(8);
    }
    if (this.content_layout_visibility) {
      this.contentLayout.setVisibility(0);
    } else {
      this.contentLayout.setVisibility(8);
    }
    if (this.go_visibility) {
      this.goIconImga.setVisibility(0);
    } else {
      this.goIconImga.setVisibility(8);
    }
    if (this.contentIntroduction_visibility) {
      this.contentIntroductionTx.setVisibility(0);
    } else {
      this.contentIntroductionTx.setVisibility(8);
    }
    paramContext.recycle();
  }
  
  private void initView()
  {
    this.mRootView = LayoutInflater.from(this.mContext).inflate(2131296473, this);
    this.layout = ((RelativeLayout)findViewById(2131165527));
    this.iconImg = ((ImageView)findViewById(2131165463));
    this.headPortraitImg = ((ImageView)findViewById(2131165462));
    this.goIconImga = ((ImageView)findViewById(2131165451));
    this.mDetailname = ((TextView)findViewById(2131165512));
    this.contextTx = ((TextView)findViewById(2131165511));
    this.contentNameTx = ((TextView)findViewById(2131165376));
    this.contentIntroductionTx = ((TextView)findViewById(2131165374));
    this.contentLayout = ((RelativeLayout)findViewById(2131165375));
  }
  
  public void setContextTx(String paramString)
  {
    this.contextTx.setText(paramString);
  }
  
  public void setIconImage(int paramInt)
  {
    this.iconImg.setImageResource(paramInt);
  }
  
  public void setNameTx(String paramString)
  {
    this.mDetailname.setText(paramString);
  }
  
  public void setPersonaldImage(int paramInt)
  {
    this.headPortraitImg.setImageResource(paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/common/PersonaldDetailItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */