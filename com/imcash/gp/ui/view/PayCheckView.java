package com.imcash.gp.ui.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PayCheckView
  extends PopupWindow
{
  protected Activity activity;
  protected TextView get_verify;
  protected TimeCount mTimeCount;
  protected Button ok;
  protected EditText pay_pass;
  protected TextView phone_number;
  protected EditText verify;
  protected LinearLayout verify_layout;
  
  public PayCheckView(Context paramContext, final PayCheckViewCall paramPayCheckViewCall)
  {
    super(paramContext);
    this.activity = ((Activity)paramContext);
    this.mTimeCount = new TimeCount(60000L, 1000L);
    View localView = ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2131296472, null);
    this.phone_number = ((TextView)localView.findViewById(2131165656));
    this.verify = ((EditText)localView.findViewById(2131165891));
    this.get_verify = ((TextView)localView.findViewById(2131165448));
    this.pay_pass = ((EditText)localView.findViewById(2131165646));
    this.ok = ((Button)localView.findViewById(2131165617));
    this.verify_layout = ((LinearLayout)localView.findViewById(2131165893));
    setContentView(localView);
    setHeight(-1);
    setWidth(-1);
    paramContext = (RelativeLayout)localView.findViewById(2131165712);
    localView.findViewById(2131165313).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PayCheckView.this.dismiss();
      }
    });
    setFocusable(false);
    setOutsideTouchable(false);
    setBackgroundDrawable(new BitmapDrawable());
    setAnimationStyle(2131623943);
    setOnDismissListener(new PopupWindow.OnDismissListener()
    {
      public void onDismiss()
      {
        PayCheckView.this.verify.setText("");
        PayCheckView.this.pay_pass.setText("");
        new Handler().postDelayed(new Runnable()
        {
          public void run()
          {
            PayCheckView.this.backgroundAlpha(1.0F, PayCheckView.this.activity);
          }
        }, 290L);
      }
    });
    this.get_verify.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramPayCheckViewCall.postSmsClicked();
        PayCheckView.this.startTimeCount();
      }
    });
    this.ok.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramPayCheckViewCall.okCallBack(PayCheckView.this.verify.getText().toString(), PayCheckView.this.pay_pass.getText().toString());
      }
    });
  }
  
  public void backgroundAlpha(float paramFloat, Activity paramActivity)
  {
    WindowManager.LayoutParams localLayoutParams = paramActivity.getWindow().getAttributes();
    localLayoutParams.alpha = paramFloat;
    paramActivity.getWindow().setAttributes(localLayoutParams);
  }
  
  public void cancelTimeCount()
  {
    this.mTimeCount.cancel();
  }
  
  public void closeSmsLayout()
  {
    LinearLayout localLinearLayout = this.verify_layout;
    if (localLinearLayout != null) {
      localLinearLayout.setVisibility(8);
    }
  }
  
  public void setGetVerifyStr(String paramString)
  {
    this.get_verify.setText(paramString);
  }
  
  public void setPhone(String paramString)
  {
    this.phone_number.setText(paramString);
  }
  
  public void show()
  {
    backgroundAlpha(0.65F, this.activity);
    showAtLocation(this.activity.getWindow().getDecorView(), 81, 0, 0);
  }
  
  protected void startTimeCount()
  {
    this.get_verify.setClickable(false);
    this.mTimeCount.start();
  }
  
  class TimeCount
    extends CountDownTimer
  {
    public TimeCount(long paramLong1, long paramLong2)
    {
      super(paramLong2);
    }
    
    public void onFinish()
    {
      PayCheckView.this.get_verify.setClickable(true);
      PayCheckView.this.get_verify.setText("重新获取");
    }
    
    public void onTick(long paramLong)
    {
      TextView localTextView = PayCheckView.this.get_verify;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("(");
      localStringBuilder.append(paramLong / 1000L);
      localStringBuilder.append(")");
      localTextView.setText(localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/PayCheckView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */