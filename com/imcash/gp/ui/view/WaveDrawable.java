package com.imcash.gp.ui.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;
import android.view.animation.DecelerateInterpolator;

public class WaveDrawable
  extends Drawable
  implements Animatable, ValueAnimator.AnimatorUpdateListener
{
  private static final int UNDEFINED_VALUE = Integer.MIN_VALUE;
  private static final float WAVE_HEIGHT_FACTOR = 0.2F;
  private static final float WAVE_SPEED_FACTOR = 0.02F;
  private static ColorFilter sGrayFilter = new ColorMatrixColorFilter(new float[] { 1.664F, 0.472F, 0.088F, 0.0F, 0.0F, 1.664F, 0.472F, 0.088F, 0.0F, 0.0F, 1.664F, 0.472F, 0.088F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F });
  private static final PorterDuffXfermode sXfermode = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);
  private ValueAnimator mAnimator = null;
  private ColorFilter mCurFilter = null;
  private Drawable mDrawable;
  private Choreographer.FrameCallback mFrameCallback = new Choreographer.FrameCallback()
  {
    public void doFrame(long paramAnonymousLong)
    {
      WaveDrawable.this.invalidateSelf();
      if (WaveDrawable.this.mRunning) {
        Choreographer.getInstance().postFrameCallback(this);
      }
    }
  };
  private int mHeight;
  private boolean mIndeterminate = false;
  private Bitmap mMask;
  private Matrix mMatrix = new Matrix();
  private Paint mPaint;
  private float mProgress = 0.3F;
  private boolean mRunning = false;
  private int mWaveHeight = Integer.MIN_VALUE;
  private int mWaveLength = Integer.MIN_VALUE;
  private int mWaveLevel = 0;
  private int mWaveOffset = 0;
  private int mWaveStep = Integer.MIN_VALUE;
  private int mWidth;
  
  public WaveDrawable(Context paramContext, int paramInt)
  {
    if (Build.VERSION.SDK_INT >= 21) {
      paramContext = paramContext.getDrawable(paramInt);
    } else {
      paramContext = paramContext.getResources().getDrawable(paramInt);
    }
    init(paramContext);
  }
  
  public WaveDrawable(Drawable paramDrawable)
  {
    init(paramDrawable);
  }
  
  private int calculateLevel()
  {
    int i = this.mHeight;
    return (i - this.mWaveLevel) * 10000 / (i + this.mWaveHeight);
  }
  
  private ValueAnimator getDefaultAnimator()
  {
    ValueAnimator localValueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    localValueAnimator.setInterpolator(new DecelerateInterpolator());
    localValueAnimator.setRepeatMode(1);
    localValueAnimator.setRepeatCount(-1);
    localValueAnimator.setDuration(5000L);
    return localValueAnimator;
  }
  
  private void init(Drawable paramDrawable)
  {
    this.mDrawable = paramDrawable;
    this.mMatrix.reset();
    this.mPaint = new Paint();
    this.mPaint.setFilterBitmap(false);
    this.mPaint.setColor(-16777216);
    this.mPaint.setXfermode(sXfermode);
    this.mWidth = this.mDrawable.getIntrinsicWidth();
    this.mHeight = this.mDrawable.getIntrinsicHeight();
    int i = this.mWidth;
    if (i > 0)
    {
      int j = this.mHeight;
      if (j > 0)
      {
        this.mWaveLength = i;
        this.mWaveHeight = Math.max(8, (int)(j * 0.2F));
        this.mWaveStep = Math.max(1, (int)(this.mWidth * 0.02F));
        updateMask(this.mWidth, this.mWaveLength, this.mWaveHeight);
      }
    }
    setProgress(0.0F);
    start();
  }
  
  private void setProgress(float paramFloat)
  {
    this.mProgress = paramFloat;
    int i = this.mHeight;
    this.mWaveLevel = (i - (int)((this.mWaveHeight + i) * this.mProgress));
    invalidateSelf();
  }
  
  private void updateBounds(Rect paramRect)
  {
    if ((paramRect.width() > 0) && (paramRect.height() > 0))
    {
      if ((this.mWidth < 0) || (this.mHeight < 0))
      {
        this.mWidth = paramRect.width();
        this.mHeight = paramRect.height();
        if (this.mWaveHeight == Integer.MIN_VALUE) {
          this.mWaveHeight = Math.max(8, (int)(this.mHeight * 0.2F));
        }
        if (this.mWaveLength == Integer.MIN_VALUE) {
          this.mWaveLength = this.mWidth;
        }
        if (this.mWaveStep == Integer.MIN_VALUE) {
          this.mWaveStep = Math.max(1, (int)(this.mWidth * 0.02F));
        }
        updateMask(this.mWidth, this.mWaveLength, this.mWaveHeight);
      }
      return;
    }
  }
  
  private void updateMask(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt1 > 0) && (paramInt2 > 0) && (paramInt3 > 0))
    {
      float f2 = paramInt1 + paramInt2;
      float f1 = paramInt2;
      int i = (int)Math.ceil(f2 / f1);
      Bitmap localBitmap = Bitmap.createBitmap(paramInt2 * i, paramInt3, Bitmap.Config.ARGB_8888);
      Canvas localCanvas = new Canvas(localBitmap);
      Paint localPaint = new Paint(1);
      paramInt1 = paramInt3 / 2;
      Path localPath = new Path();
      float f3 = paramInt1;
      localPath.moveTo(0.0F, f3);
      float f4 = f1 / 4.0F;
      f1 = -paramInt1;
      paramInt1 = 0;
      f2 = 0.0F;
      while (paramInt1 < i * 2)
      {
        float f5 = f2 + f4;
        f2 = f5 + f4;
        localPath.quadTo(f5, f1, f2, f3);
        f1 = localBitmap.getHeight() - f1;
        paramInt1++;
      }
      f2 = localBitmap.getWidth();
      f1 = paramInt3;
      localPath.lineTo(f2, f1);
      localPath.lineTo(0.0F, f1);
      localPath.close();
      localCanvas.drawPath(localPath, localPaint);
      this.mMask = localBitmap;
      return;
    }
    Log.w("ContentValues", "updateMask: size must > 0");
    this.mMask = null;
  }
  
  public void draw(Canvas paramCanvas)
  {
    this.mDrawable.setColorFilter(sGrayFilter);
    this.mDrawable.draw(paramCanvas);
    this.mDrawable.setColorFilter(this.mCurFilter);
    if (this.mProgress <= 0.001F) {
      return;
    }
    int i = paramCanvas.saveLayer(0.0F, 0.0F, this.mWidth, this.mHeight, null, 31);
    int j = this.mWaveLevel;
    if (j > 0) {
      paramCanvas.clipRect(0, j, this.mWidth, this.mHeight);
    }
    this.mDrawable.draw(paramCanvas);
    if (this.mProgress >= 0.999F) {
      return;
    }
    this.mWaveOffset += this.mWaveStep;
    j = this.mWaveOffset;
    int k = this.mWaveLength;
    if (j > k) {
      this.mWaveOffset = (j - k);
    }
    if (this.mMask != null)
    {
      this.mMatrix.setTranslate(-this.mWaveOffset, this.mWaveLevel);
      paramCanvas.drawBitmap(this.mMask, this.mMatrix, this.mPaint);
    }
    paramCanvas.restoreToCount(i);
  }
  
  public int getIntrinsicHeight()
  {
    return this.mHeight;
  }
  
  public int getIntrinsicWidth()
  {
    return this.mWidth;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean isIndeterminate()
  {
    return this.mIndeterminate;
  }
  
  public boolean isRunning()
  {
    return this.mRunning;
  }
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    if (this.mIndeterminate)
    {
      setProgress(paramValueAnimator.getAnimatedFraction());
      if (!this.mRunning) {
        invalidateSelf();
      }
    }
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    super.onBoundsChange(paramRect);
    updateBounds(paramRect);
  }
  
  protected boolean onLevelChange(int paramInt)
  {
    setProgress(paramInt / 10000.0F);
    return true;
  }
  
  public void setAlpha(int paramInt)
  {
    this.mDrawable.setAlpha(paramInt);
  }
  
  public void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.mCurFilter = paramColorFilter;
    invalidateSelf();
  }
  
  public void setIndeterminate(boolean paramBoolean)
  {
    this.mIndeterminate = paramBoolean;
    if (this.mIndeterminate)
    {
      if (this.mAnimator == null) {
        this.mAnimator = getDefaultAnimator();
      }
      this.mAnimator.addUpdateListener(this);
      this.mAnimator.start();
    }
    else
    {
      ValueAnimator localValueAnimator = this.mAnimator;
      if (localValueAnimator != null)
      {
        localValueAnimator.removeUpdateListener(this);
        this.mAnimator.cancel();
      }
      setLevel(calculateLevel());
    }
  }
  
  public void setIndeterminateAnimator(ValueAnimator paramValueAnimator)
  {
    ValueAnimator localValueAnimator = this.mAnimator;
    if (localValueAnimator == paramValueAnimator) {
      return;
    }
    if (localValueAnimator != null)
    {
      localValueAnimator.removeUpdateListener(this);
      this.mAnimator.cancel();
    }
    this.mAnimator = paramValueAnimator;
    paramValueAnimator = this.mAnimator;
    if (paramValueAnimator != null) {
      paramValueAnimator.addUpdateListener(this);
    }
  }
  
  public void setWaveAmplitude(int paramInt)
  {
    paramInt = Math.max(1, Math.min(paramInt, this.mHeight / 2)) * 2;
    if (this.mWaveHeight != paramInt)
    {
      this.mWaveHeight = paramInt;
      updateMask(this.mWidth, this.mWaveLength, this.mWaveHeight);
      invalidateSelf();
    }
  }
  
  public void setWaveLength(int paramInt)
  {
    paramInt = Math.max(8, Math.min(this.mWidth * 2, paramInt));
    if (paramInt != this.mWaveLength)
    {
      this.mWaveLength = paramInt;
      updateMask(this.mWidth, this.mWaveLength, this.mWaveHeight);
      invalidateSelf();
    }
  }
  
  public void setWaveSpeed(int paramInt)
  {
    this.mWaveStep = Math.min(paramInt, this.mWidth / 2);
  }
  
  public void start()
  {
    this.mRunning = true;
    Choreographer.getInstance().postFrameCallback(this.mFrameCallback);
  }
  
  public void stop()
  {
    this.mRunning = false;
    Choreographer.getInstance().removeFrameCallback(this.mFrameCallback);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/WaveDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */