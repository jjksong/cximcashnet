package com.imcash.gp.ui.view.bottomdialog;

import android.graphics.drawable.Drawable;

public class Item
{
  private Drawable icon;
  private int id;
  private String title;
  
  public Item() {}
  
  public Item(int paramInt, String paramString, Drawable paramDrawable)
  {
    this.id = paramInt;
    this.title = paramString;
    this.icon = paramDrawable;
  }
  
  public Drawable getIcon()
  {
    return this.icon;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setIcon(Drawable paramDrawable)
  {
    this.icon = paramDrawable;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/bottomdialog/Item.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */