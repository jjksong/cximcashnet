package com.imcash.gp.ui.view.bottomdialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BottomDialog
{
  public static final int GRID = 1;
  public static final int HORIZONTAL = 0;
  public static final int LINEAR = 0;
  public static final int VERTICAL = 1;
  private CustomDialog customDialog;
  
  public BottomDialog(Context paramContext)
  {
    this.customDialog = new CustomDialog(paramContext);
  }
  
  public BottomDialog addItems(List<Item> paramList, OnItemClickListener paramOnItemClickListener)
  {
    this.customDialog.addItems(paramList, paramOnItemClickListener);
    return this;
  }
  
  public BottomDialog background(int paramInt)
  {
    this.customDialog.background(paramInt);
    return this;
  }
  
  public void dismiss()
  {
    CustomDialog localCustomDialog = this.customDialog;
    if (localCustomDialog != null) {
      localCustomDialog.dismiss();
    }
  }
  
  public BottomDialog inflateMenu(int paramInt, OnItemClickListener paramOnItemClickListener)
  {
    this.customDialog.inflateMenu(paramInt, paramOnItemClickListener);
    return this;
  }
  
  public BottomDialog itemClick(OnItemClickListener paramOnItemClickListener)
  {
    this.customDialog.setItemClick(paramOnItemClickListener);
    return this;
  }
  
  public BottomDialog layout(int paramInt)
  {
    this.customDialog.layout(paramInt);
    return this;
  }
  
  public BottomDialog orientation(int paramInt)
  {
    this.customDialog.orientation(paramInt);
    return this;
  }
  
  public BottomDialog show()
  {
    this.customDialog.show();
    return this;
  }
  
  public BottomDialog title(int paramInt)
  {
    this.customDialog.title(paramInt);
    return this;
  }
  
  public BottomDialog title(String paramString)
  {
    this.customDialog.title(paramString);
    return this;
  }
  
  private final class CustomDialog
    extends Dialog
  {
    private DialogAdapter adapter;
    private LinearLayout background;
    private LinearLayout container;
    private int layout;
    private int leftIcon;
    private int leftPadding;
    private int orientation;
    private int padding;
    private TextView titleView;
    private int topIcon;
    private int topPadding;
    
    CustomDialog(Context paramContext)
    {
      super(2131624104);
      init();
    }
    
    private void init()
    {
      this.padding = getContext().getResources().getDimensionPixelSize(2131034199);
      this.topPadding = getContext().getResources().getDimensionPixelSize(2131034201);
      this.leftPadding = getContext().getResources().getDimensionPixelSize(2131034199);
      this.topIcon = getContext().getResources().getDimensionPixelSize(2131034205);
      this.leftIcon = getContext().getResources().getDimensionPixelSize(2131034204);
      setContentView(2131296418);
      setCancelable(true);
      setCanceledOnTouchOutside(true);
      getWindow().setGravity(80);
      getWindow().setLayout(-1, -2);
      this.background = ((LinearLayout)findViewById(2131165249));
      this.titleView = ((TextView)findViewById(2131165822));
      this.container = ((LinearLayout)findViewById(2131165370));
      findViewById(2131165313).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          BottomDialog.CustomDialog.this.dismiss();
        }
      });
    }
    
    void addItems(List<Item> paramList, OnItemClickListener paramOnItemClickListener)
    {
      ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
      this.adapter = new DialogAdapter(paramList, this.layout, this.orientation);
      this.adapter.setItemClick(paramOnItemClickListener);
      int i = this.layout;
      if (i == 0) {
        paramList = new LinearLayoutManager(getContext(), this.orientation, false);
      } else if (i == 1) {
        paramList = new GridLayoutManager(getContext(), 5, this.orientation, false);
      } else {
        paramList = new LinearLayoutManager(getContext(), this.orientation, false);
      }
      paramOnItemClickListener = new RecyclerView(getContext());
      paramOnItemClickListener.setLayoutParams(localLayoutParams);
      paramOnItemClickListener.setLayoutManager(paramList);
      paramOnItemClickListener.setAdapter(this.adapter);
      this.container.addView(paramOnItemClickListener);
    }
    
    public void background(int paramInt)
    {
      this.background.setBackgroundResource(paramInt);
    }
    
    void inflateMenu(int paramInt, OnItemClickListener paramOnItemClickListener)
    {
      Object localObject = new SupportMenuInflater(getContext());
      MenuBuilder localMenuBuilder = new MenuBuilder(getContext());
      ((MenuInflater)localObject).inflate(paramInt, localMenuBuilder);
      ArrayList localArrayList = new ArrayList();
      for (paramInt = 0; paramInt < localMenuBuilder.size(); paramInt++)
      {
        localObject = localMenuBuilder.getItem(paramInt);
        localArrayList.add(new Item(((MenuItem)localObject).getItemId(), ((MenuItem)localObject).getTitle().toString(), ((MenuItem)localObject).getIcon()));
      }
      addItems(localArrayList, paramOnItemClickListener);
    }
    
    public void layout(int paramInt)
    {
      this.layout = paramInt;
      DialogAdapter localDialogAdapter = this.adapter;
      if (localDialogAdapter != null) {
        localDialogAdapter.setLayout(paramInt);
      }
    }
    
    public void orientation(int paramInt)
    {
      this.orientation = paramInt;
      DialogAdapter localDialogAdapter = this.adapter;
      if (localDialogAdapter != null) {
        localDialogAdapter.setOrientation(paramInt);
      }
    }
    
    void setItemClick(OnItemClickListener paramOnItemClickListener)
    {
      this.adapter.setItemClick(paramOnItemClickListener);
    }
    
    public void title(int paramInt)
    {
      title(getContext().getString(paramInt));
    }
    
    public void title(String paramString)
    {
      this.titleView.setText(paramString);
      this.titleView.setVisibility(0);
    }
    
    private class DialogAdapter
      extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
      private OnItemClickListener itemClickListener;
      private int layout;
      private List<Item> mItems = Collections.emptyList();
      private int orientation;
      
      DialogAdapter(int paramInt1, int paramInt2)
      {
        setList(paramInt1);
        this.layout = paramInt2;
        int i;
        this.orientation = i;
      }
      
      private void setList(List<Item> paramList)
      {
        Object localObject = paramList;
        if (paramList == null) {
          localObject = new ArrayList();
        }
        this.mItems = ((List)localObject);
      }
      
      public int getItemCount()
      {
        return this.mItems.size();
      }
      
      public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
      {
        final Item localItem = (Item)this.mItems.get(paramInt);
        if (this.layout == 1)
        {
          paramViewHolder = (TopHolder)paramViewHolder;
          paramViewHolder.item.setText(localItem.getTitle());
          paramViewHolder.item.setCompoundDrawablesWithIntrinsicBounds(null, paramViewHolder.icon(localItem.getIcon()), null, null);
          paramViewHolder.item.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              if (BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener != null) {
                BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener.click(localItem);
              }
            }
          });
        }
        else if (this.orientation == 0)
        {
          paramViewHolder = (TopHolder)paramViewHolder;
          paramViewHolder.item.setText(localItem.getTitle());
          paramViewHolder.item.setCompoundDrawablesWithIntrinsicBounds(null, paramViewHolder.icon(localItem.getIcon()), null, null);
          paramViewHolder.item.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              if (BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener != null) {
                BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener.click(localItem);
              }
            }
          });
        }
        else
        {
          paramViewHolder = (LeftHolder)paramViewHolder;
          paramViewHolder.item.setText(localItem.getTitle());
          paramViewHolder.item.setCompoundDrawablesWithIntrinsicBounds(paramViewHolder.icon(localItem.getIcon()), null, null, null);
          paramViewHolder.item.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              if (BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener != null) {
                BottomDialog.CustomDialog.DialogAdapter.this.itemClickListener.click(localItem);
              }
            }
          });
        }
      }
      
      public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
      {
        if (this.layout == 1) {
          return new TopHolder(new LinearLayout(paramViewGroup.getContext()));
        }
        if (this.orientation == 0) {
          return new TopHolder(new LinearLayout(paramViewGroup.getContext()));
        }
        return new LeftHolder(new LinearLayout(paramViewGroup.getContext()));
      }
      
      void setItemClick(OnItemClickListener paramOnItemClickListener)
      {
        this.itemClickListener = paramOnItemClickListener;
      }
      
      public void setLayout(int paramInt)
      {
        this.layout = paramInt;
        notifyDataSetChanged();
      }
      
      public void setOrientation(int paramInt)
      {
        this.orientation = paramInt;
        notifyDataSetChanged();
      }
      
      class LeftHolder
        extends RecyclerView.ViewHolder
      {
        private TextView item;
        
        LeftHolder(View paramView)
        {
          super();
          ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
          paramView.setLayoutParams(localLayoutParams);
          this.item = new TextView(paramView.getContext());
          this.item.setLayoutParams(localLayoutParams);
          this.item.setMaxLines(1);
          this.item.setEllipsize(TextUtils.TruncateAt.END);
          this.item.setGravity(16);
          this.item.setTextColor(ContextCompat.getColor(paramView.getContext(), 2130968613));
          this.item.setTextSize(0, BottomDialog.CustomDialog.this.getContext().getResources().getDimension(2131034233));
          this.item.setCompoundDrawablePadding(BottomDialog.CustomDialog.this.leftPadding);
          this.item.setPadding(BottomDialog.CustomDialog.this.padding, BottomDialog.CustomDialog.this.padding, BottomDialog.CustomDialog.this.padding, BottomDialog.CustomDialog.this.padding);
          this$1 = new TypedValue();
          paramView.getContext().getTheme().resolveAttribute(16843534, BottomDialog.CustomDialog.DialogAdapter.this, true);
          this.item.setBackgroundResource(BottomDialog.CustomDialog.DialogAdapter.this.resourceId);
          ((LinearLayout)paramView).addView(this.item);
        }
        
        private Drawable icon(Drawable paramDrawable)
        {
          if (paramDrawable != null)
          {
            paramDrawable = ((BitmapDrawable)paramDrawable).getBitmap();
            paramDrawable = new BitmapDrawable(BottomDialog.CustomDialog.this.getContext().getResources(), Bitmap.createScaledBitmap(paramDrawable, BottomDialog.CustomDialog.this.leftIcon, BottomDialog.CustomDialog.this.leftIcon, true));
            Drawable.ConstantState localConstantState = paramDrawable.getConstantState();
            if (localConstantState != null) {
              paramDrawable = localConstantState.newDrawable().mutate();
            }
            return DrawableCompat.wrap(paramDrawable);
          }
          return null;
        }
      }
      
      class TopHolder
        extends RecyclerView.ViewHolder
      {
        private TextView item;
        
        TopHolder(View paramView)
        {
          super();
          ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
          localLayoutParams.width = (Utils.getScreenWidth(BottomDialog.CustomDialog.this.getContext()) / 5);
          this.item = new TextView(paramView.getContext());
          this.item.setLayoutParams(localLayoutParams);
          this.item.setMaxLines(1);
          this.item.setEllipsize(TextUtils.TruncateAt.END);
          this.item.setGravity(17);
          this.item.setTextColor(ContextCompat.getColor(paramView.getContext(), 2130968677));
          this.item.setTextSize(0, BottomDialog.CustomDialog.this.getContext().getResources().getDimension(2131034234));
          this.item.setCompoundDrawablePadding(BottomDialog.CustomDialog.this.topPadding);
          this.item.setPadding(0, BottomDialog.CustomDialog.this.padding, 0, BottomDialog.CustomDialog.this.padding);
          this$1 = new TypedValue();
          paramView.getContext().getTheme().resolveAttribute(16843534, BottomDialog.CustomDialog.DialogAdapter.this, true);
          this.item.setBackgroundResource(BottomDialog.CustomDialog.DialogAdapter.this.resourceId);
          ((LinearLayout)paramView).addView(this.item);
        }
        
        private Drawable icon(Drawable paramDrawable)
        {
          if (paramDrawable != null)
          {
            paramDrawable = ((BitmapDrawable)paramDrawable).getBitmap();
            paramDrawable = new BitmapDrawable(BottomDialog.CustomDialog.this.getContext().getResources(), Bitmap.createScaledBitmap(paramDrawable, BottomDialog.CustomDialog.this.topIcon, BottomDialog.CustomDialog.this.topIcon, true));
            Drawable.ConstantState localConstantState = paramDrawable.getConstantState();
            if (localConstantState != null) {
              paramDrawable = localConstantState.newDrawable().mutate();
            }
            return DrawableCompat.wrap(paramDrawable);
          }
          return null;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/bottomdialog/BottomDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */