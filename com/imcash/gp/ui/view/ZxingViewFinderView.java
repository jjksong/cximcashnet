package com.imcash.gp.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.google.zxing.ResultPoint;
import com.imcash.gp.R.styleable;
import com.journeyapps.barcodescanner.ViewfinderView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ZxingViewFinderView
  extends ViewfinderView
{
  private static final int LASER_MOVE_DISTANCE_PER_UNIT_TIME = 10;
  private int LASER_MOVE_DIRECTION = 1;
  private int scannerBoundColor;
  private int scannerBoundCornerColor;
  private float scannerBoundCornerHeight;
  private float scannerBoundCornerWidth;
  private float scannerBoundWidth;
  private Bitmap scannerLaserBitmap;
  private int scannerLaserResId;
  private int scannerLaserTop;
  private String scannerTipText;
  private int scannerTipTextColor;
  private float scannerTipTextMargin;
  private float scannerTipTextSize;
  private boolean tipTextGravityBottom;
  
  public ZxingViewFinderView(Context paramContext, @Nullable AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getResources();
    paramAttributeSet = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.ZxingViewFinderView);
    this.scannerBoundColor = paramAttributeSet.getColor(1, paramContext.getColor(2130968771));
    this.scannerBoundWidth = paramAttributeSet.getDimension(5, 0.5F);
    this.scannerBoundCornerColor = paramAttributeSet.getColor(2, paramContext.getColor(2130968739));
    this.scannerBoundCornerWidth = paramAttributeSet.getDimension(4, 1.5F);
    this.scannerBoundCornerHeight = paramAttributeSet.getDimension(3, 24.0F);
    this.scannerLaserResId = paramAttributeSet.getResourceId(6, 0);
    this.scannerTipText = paramAttributeSet.getString(7);
    this.scannerTipTextSize = paramAttributeSet.getDimension(11, 14.0F);
    this.scannerTipTextColor = paramAttributeSet.getColor(8, paramContext.getColor(2130968771));
    this.scannerTipTextMargin = paramAttributeSet.getDimension(10, 40.0F);
    this.tipTextGravityBottom = paramAttributeSet.getBoolean(9, true);
    paramAttributeSet.recycle();
  }
  
  private void drawExteriorDarkened(Canvas paramCanvas, Rect paramRect, int paramInt1, int paramInt2)
  {
    Paint localPaint = this.paint;
    int i;
    if (this.resultBitmap != null) {
      i = this.resultColor;
    } else {
      i = this.maskColor;
    }
    localPaint.setColor(i);
    float f = paramInt1;
    paramCanvas.drawRect(0.0F, 0.0F, f, paramRect.top, this.paint);
    paramCanvas.drawRect(0.0F, paramRect.top, paramRect.left, paramRect.bottom + 1, this.paint);
    paramCanvas.drawRect(paramRect.right + 1, paramRect.top, f, paramRect.bottom + 1, this.paint);
    paramCanvas.drawRect(0.0F, paramRect.bottom + 1, f, paramInt2, this.paint);
  }
  
  private void drawFrameBound(Canvas paramCanvas, Rect paramRect)
  {
    if (this.scannerBoundWidth <= 0.0F) {
      return;
    }
    this.paint.setColor(this.scannerBoundColor);
    paramCanvas.drawRect(paramRect.left, paramRect.top, paramRect.right, paramRect.top + this.scannerBoundWidth, this.paint);
    paramCanvas.drawRect(paramRect.left, paramRect.top, paramRect.left + this.scannerBoundWidth, paramRect.bottom, this.paint);
    paramCanvas.drawRect(paramRect.right - this.scannerBoundWidth, paramRect.top, paramRect.right, paramRect.bottom, this.paint);
    paramCanvas.drawRect(paramRect.left, paramRect.bottom - this.scannerBoundWidth, paramRect.right, paramRect.bottom, this.paint);
  }
  
  private void drawFrameCorner(Canvas paramCanvas, Rect paramRect)
  {
    if ((this.scannerBoundCornerWidth > 0.0F) && (this.scannerBoundCornerHeight > 0.0F))
    {
      this.paint.setColor(this.scannerBoundCornerColor);
      paramCanvas.drawRect(paramRect.left - this.scannerBoundCornerWidth, paramRect.top - this.scannerBoundCornerWidth, paramRect.left + this.scannerBoundCornerHeight, paramRect.top, this.paint);
      paramCanvas.drawRect(paramRect.left - this.scannerBoundCornerWidth, paramRect.top - this.scannerBoundCornerWidth, paramRect.left, paramRect.top + this.scannerBoundCornerHeight, this.paint);
      paramCanvas.drawRect(paramRect.left - this.scannerBoundCornerWidth, paramRect.bottom + this.scannerBoundCornerWidth - this.scannerBoundCornerHeight, paramRect.left, paramRect.bottom + this.scannerBoundCornerWidth, this.paint);
      paramCanvas.drawRect(paramRect.left - this.scannerBoundCornerWidth, paramRect.bottom, paramRect.left - this.scannerBoundCornerWidth + this.scannerBoundCornerHeight, paramRect.bottom + this.scannerBoundCornerWidth, this.paint);
      paramCanvas.drawRect(paramRect.right + this.scannerBoundCornerWidth - this.scannerBoundCornerHeight, paramRect.top - this.scannerBoundCornerWidth, paramRect.right + this.scannerBoundCornerWidth, paramRect.top, this.paint);
      paramCanvas.drawRect(paramRect.right, paramRect.top - this.scannerBoundCornerWidth, paramRect.right + this.scannerBoundCornerWidth, paramRect.top - this.scannerBoundCornerWidth + this.scannerBoundCornerHeight, this.paint);
      paramCanvas.drawRect(paramRect.right + this.scannerBoundCornerWidth - this.scannerBoundCornerHeight, paramRect.bottom, paramRect.right + this.scannerBoundCornerWidth, paramRect.bottom + this.scannerBoundCornerWidth, this.paint);
      paramCanvas.drawRect(paramRect.right, paramRect.bottom + this.scannerBoundCornerWidth - this.scannerBoundCornerHeight, paramRect.right + this.scannerBoundCornerWidth, paramRect.bottom + this.scannerBoundCornerWidth, this.paint);
      return;
    }
  }
  
  private void drawLaserLine(Canvas paramCanvas, Rect paramRect)
  {
    int i;
    if (this.scannerLaserResId == 0)
    {
      this.paint.setColor(this.laserColor);
      this.paint.setAlpha(SCANNER_ALPHA[this.scannerAlpha]);
      this.scannerAlpha = ((this.scannerAlpha + 1) % SCANNER_ALPHA.length);
      i = paramRect.height() / 2 + paramRect.top;
      paramCanvas.drawRect(paramRect.left + 2, i - 1, paramRect.right - 1, i + 2, this.paint);
    }
    else
    {
      if (this.scannerLaserBitmap == null) {
        this.scannerLaserBitmap = BitmapFactory.decodeResource(getResources(), this.scannerLaserResId);
      }
      Bitmap localBitmap = this.scannerLaserBitmap;
      if (localBitmap != null)
      {
        i = localBitmap.getHeight();
        if (this.scannerLaserTop < paramRect.top)
        {
          this.scannerLaserTop = paramRect.top;
          this.LASER_MOVE_DIRECTION = 1;
        }
        if (this.scannerLaserTop > paramRect.bottom - i)
        {
          this.scannerLaserTop = (paramRect.bottom - i);
          this.LASER_MOVE_DIRECTION = -1;
        }
        paramRect = new Rect(paramRect.left, this.scannerLaserTop, paramRect.right, this.scannerLaserTop + i);
        paramCanvas.drawBitmap(this.scannerLaserBitmap, null, paramRect, this.paint);
        this.scannerLaserTop += this.LASER_MOVE_DIRECTION * 10;
      }
    }
  }
  
  private void drawResultPoint(Canvas paramCanvas, Rect paramRect1, Rect paramRect2)
  {
    float f2 = paramRect1.width() / paramRect2.width();
    float f1 = paramRect1.height() / paramRect2.height();
    Object localObject = this.possibleResultPoints;
    paramRect2 = this.lastPossibleResultPoints;
    int i = paramRect1.left;
    int j = paramRect1.top;
    if (((List)localObject).isEmpty())
    {
      this.lastPossibleResultPoints = null;
    }
    else
    {
      this.possibleResultPoints = new ArrayList(5);
      this.lastPossibleResultPoints = ((List)localObject);
      this.paint.setAlpha(160);
      this.paint.setColor(this.resultPointColor);
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        paramRect1 = (ResultPoint)((Iterator)localObject).next();
        paramCanvas.drawCircle((int)(paramRect1.getX() * f2) + i, (int)(paramRect1.getY() * f1) + j, 6.0F, this.paint);
      }
    }
    if (paramRect2 != null)
    {
      this.paint.setAlpha(80);
      this.paint.setColor(this.resultPointColor);
      paramRect2 = paramRect2.iterator();
      while (paramRect2.hasNext())
      {
        paramRect1 = (ResultPoint)paramRect2.next();
        paramCanvas.drawCircle((int)(paramRect1.getX() * f2) + i, (int)(paramRect1.getY() * f1) + j, 3.0F, this.paint);
      }
    }
  }
  
  private void drawTipText(Canvas paramCanvas, Rect paramRect, int paramInt)
  {
    if (TextUtils.isEmpty(this.scannerTipText)) {
      this.scannerTipText = "提示";
    }
    this.paint.setColor(this.scannerTipTextColor);
    this.paint.setTextSize(this.scannerTipTextSize);
    float f1 = this.paint.measureText(this.scannerTipText);
    float f2 = (paramInt - f1) / 2.0F;
    if (this.tipTextGravityBottom) {
      f1 = paramRect.bottom + this.scannerTipTextMargin;
    } else {
      f1 = paramRect.top - this.scannerTipTextMargin;
    }
    paramCanvas.drawText(this.scannerTipText, f2, f1, this.paint);
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    refreshSizes();
    if ((this.framingRect != null) && (this.previewFramingRect != null))
    {
      Rect localRect1 = this.framingRect;
      Rect localRect2 = this.previewFramingRect;
      int i = paramCanvas.getWidth();
      drawExteriorDarkened(paramCanvas, localRect1, i, paramCanvas.getHeight());
      if (this.resultBitmap != null)
      {
        this.paint.setAlpha(160);
        paramCanvas.drawBitmap(this.resultBitmap, null, localRect1, this.paint);
      }
      else
      {
        drawFrameBound(paramCanvas, localRect1);
        drawFrameCorner(paramCanvas, localRect1);
        drawLaserLine(paramCanvas, localRect1);
        drawTipText(paramCanvas, localRect1, i);
        drawResultPoint(paramCanvas, localRect1, localRect2);
        postInvalidateDelayed(80L, localRect1.left - 6, localRect1.top - 6, localRect1.right + 6, localRect1.bottom + 6);
      }
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/ZxingViewFinderView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */