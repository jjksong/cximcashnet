package com.imcash.gp.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Cicle
  extends View
{
  Paint paint;
  
  public Cicle(Context paramContext, @Nullable AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    this.paint = new Paint();
    this.paint.setColor(Color.rgb(240, 240, 240));
    this.paint.setStyle(Paint.Style.FILL);
    this.paint.setStrokeWidth(8.0F);
    paramCanvas.drawCircle(getWidth() / 2, getHeight() / 2, 200.0F, this.paint);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/Cicle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */