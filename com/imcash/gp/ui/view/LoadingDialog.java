package com.imcash.gp.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import com.wang.avi.AVLoadingIndicatorView;

public class LoadingDialog
  extends AlertDialog
{
  private AVLoadingIndicatorView avi;
  
  public LoadingDialog(Context paramContext)
  {
    super(paramContext, 2131624114);
  }
  
  public void dismiss()
  {
    super.dismiss();
    AVLoadingIndicatorView localAVLoadingIndicatorView = this.avi;
    if (localAVLoadingIndicatorView == null) {
      return;
    }
    localAVLoadingIndicatorView.hide();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131296426);
    setCancelable(false);
    setCanceledOnTouchOutside(false);
    this.avi = ((AVLoadingIndicatorView)findViewById(2131165245));
  }
  
  public void show()
  {
    super.show();
    this.avi.show();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/LoadingDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */