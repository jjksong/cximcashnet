package com.imcash.gp.ui.view.red;

public abstract interface OnRedPacketDialogClickListener
{
  public abstract void onCloseClick();
  
  public abstract void onOpenClick();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/red/OnRedPacketDialogClickListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */