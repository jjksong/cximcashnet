package com.imcash.gp.ui.view.red;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;
import butterknife.internal.DebouncingOnClickListener;

public class RedPacketViewHolder$$ViewBinder<T extends RedPacketViewHolder>
  implements ButterKnife.ViewBinder<T>
{
  public void bind(ButterKnife.Finder paramFinder, final T paramT, Object paramObject)
  {
    View localView = (View)paramFinder.findRequiredView(paramObject, 2131165515, "field 'mIvClose' and method 'onClick'");
    paramT.mIvClose = ((ImageView)paramFinder.castView(localView, 2131165515, "field 'mIvClose'"));
    localView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramT.onClick(paramAnonymousView);
      }
    });
    paramT.mIvAvatar = ((ImageView)paramFinder.castView((View)paramFinder.findRequiredView(paramObject, 2131165514, "field 'mIvAvatar'"), 2131165514, "field 'mIvAvatar'"));
    paramT.mTvName = ((TextView)paramFinder.castView((View)paramFinder.findRequiredView(paramObject, 2131165867, "field 'mTvName'"), 2131165867, "field 'mTvName'"));
    paramT.mTvMsg = ((TextView)paramFinder.castView((View)paramFinder.findRequiredView(paramObject, 2131165866, "field 'mTvMsg'"), 2131165866, "field 'mTvMsg'"));
    paramObject = (View)paramFinder.findRequiredView(paramObject, 2131165518, "field 'mIvOpen' and method 'onClick'");
    paramT.mIvOpen = ((ImageView)paramFinder.castView((View)paramObject, 2131165518, "field 'mIvOpen'"));
    ((View)paramObject).setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramT.onClick(paramAnonymousView);
      }
    });
  }
  
  public void unbind(T paramT)
  {
    paramT.mIvClose = null;
    paramT.mIvAvatar = null;
    paramT.mTvName = null;
    paramT.mTvMsg = null;
    paramT.mIvOpen = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/red/RedPacketViewHolder$$ViewBinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */