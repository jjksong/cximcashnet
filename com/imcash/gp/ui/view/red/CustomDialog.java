package com.imcash.gp.ui.view.red;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

public class CustomDialog
  extends Dialog
{
  private static int default_height = 120;
  private static int default_width = 160;
  
  public CustomDialog(Context paramContext, int paramInt1, int paramInt2, View paramView, int paramInt3)
  {
    super(paramContext, paramInt3);
    setContentView(paramView);
    paramView = getWindow();
    paramContext = paramView.getAttributes();
    paramContext.gravity = 17;
    paramView.setAttributes(paramContext);
  }
  
  public CustomDialog(Context paramContext, View paramView, int paramInt)
  {
    this(paramContext, default_width, default_height, paramView, paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/red/CustomDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */