package com.imcash.gp.ui.view.red;

import android.widget.ImageView;

public class FrameAnimation
{
  private static final int SELECTED_A = 1;
  private static final int SELECTED_B = 2;
  private static final int SELECTED_C = 3;
  private static final int SELECTED_D = 4;
  private AnimationListener mAnimationListener;
  private int mCurrentFrame;
  private int mCurrentSelect;
  private int mDelay;
  private int mDuration;
  private int[] mDurations;
  private int[] mFrameRess;
  private ImageView mImageView;
  private boolean mIsRepeat;
  private int mLastFrame;
  private boolean mNext;
  private boolean mPause;
  
  public FrameAnimation(ImageView paramImageView, int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    this.mImageView = paramImageView;
    this.mFrameRess = paramArrayOfInt;
    this.mDuration = paramInt1;
    this.mDelay = paramInt2;
    this.mLastFrame = (paramArrayOfInt.length - 1);
    playAndDelay(0);
  }
  
  public FrameAnimation(ImageView paramImageView, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
  {
    this.mImageView = paramImageView;
    this.mFrameRess = paramArrayOfInt;
    this.mDuration = paramInt;
    this.mLastFrame = (paramArrayOfInt.length - 1);
    this.mIsRepeat = paramBoolean;
    play(0);
  }
  
  public FrameAnimation(ImageView paramImageView, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
  {
    this.mImageView = paramImageView;
    this.mFrameRess = paramArrayOfInt1;
    this.mDurations = paramArrayOfInt2;
    this.mDelay = paramInt;
    this.mLastFrame = (paramArrayOfInt1.length - 1);
    playByDurationsAndDelay(0);
  }
  
  public FrameAnimation(ImageView paramImageView, int[] paramArrayOfInt1, int[] paramArrayOfInt2, boolean paramBoolean)
  {
    this.mImageView = paramImageView;
    this.mFrameRess = paramArrayOfInt1;
    this.mDurations = paramArrayOfInt2;
    this.mLastFrame = (paramArrayOfInt1.length - 1);
    this.mIsRepeat = paramBoolean;
    playByDurations(0);
  }
  
  private void play(final int paramInt)
  {
    this.mImageView.postDelayed(new Runnable()
    {
      public void run()
      {
        if (FrameAnimation.this.mPause)
        {
          if (FrameAnimation.this.mPause)
          {
            FrameAnimation.access$102(FrameAnimation.this, 4);
            FrameAnimation.access$202(FrameAnimation.this, paramInt);
            if (FrameAnimation.this.mAnimationListener != null) {
              FrameAnimation.this.mAnimationListener.onAnimationPause();
            }
            return;
          }
          return;
        }
        if ((paramInt == 0) && (FrameAnimation.this.mAnimationListener != null)) {
          FrameAnimation.this.mAnimationListener.onAnimationStart();
        }
        FrameAnimation.this.mImageView.setBackgroundResource(FrameAnimation.this.mFrameRess[paramInt]);
        if (paramInt == FrameAnimation.this.mLastFrame)
        {
          if (FrameAnimation.this.mIsRepeat)
          {
            if (FrameAnimation.this.mAnimationListener != null) {
              FrameAnimation.this.mAnimationListener.onAnimationRepeat();
            }
            FrameAnimation.this.play(0);
          }
          else if (FrameAnimation.this.mAnimationListener != null)
          {
            FrameAnimation.this.mAnimationListener.onAnimationEnd();
          }
        }
        else {
          FrameAnimation.this.play(paramInt + 1);
        }
      }
    }, this.mDuration);
  }
  
  private void playAndDelay(final int paramInt)
  {
    ImageView localImageView = this.mImageView;
    Runnable local2 = new Runnable()
    {
      public void run()
      {
        if (FrameAnimation.this.mPause)
        {
          if (FrameAnimation.this.mPause)
          {
            FrameAnimation.access$102(FrameAnimation.this, 2);
            FrameAnimation.access$202(FrameAnimation.this, paramInt);
            return;
          }
          return;
        }
        FrameAnimation.access$702(FrameAnimation.this, false);
        if ((paramInt == 0) && (FrameAnimation.this.mAnimationListener != null)) {
          FrameAnimation.this.mAnimationListener.onAnimationStart();
        }
        FrameAnimation.this.mImageView.setBackgroundResource(FrameAnimation.this.mFrameRess[paramInt]);
        if (paramInt == FrameAnimation.this.mLastFrame)
        {
          if (FrameAnimation.this.mAnimationListener != null) {
            FrameAnimation.this.mAnimationListener.onAnimationRepeat();
          }
          FrameAnimation.access$702(FrameAnimation.this, true);
          FrameAnimation.this.playAndDelay(0);
        }
        else
        {
          FrameAnimation.this.playAndDelay(paramInt + 1);
        }
      }
    };
    if (this.mNext)
    {
      paramInt = this.mDelay;
      if (paramInt > 0) {}
    }
    else
    {
      paramInt = this.mDuration;
    }
    localImageView.postDelayed(local2, paramInt);
  }
  
  private void playByDurations(final int paramInt)
  {
    this.mImageView.postDelayed(new Runnable()
    {
      public void run()
      {
        if (FrameAnimation.this.mPause)
        {
          if (FrameAnimation.this.mPause)
          {
            FrameAnimation.access$102(FrameAnimation.this, 3);
            FrameAnimation.access$202(FrameAnimation.this, paramInt);
            if (FrameAnimation.this.mAnimationListener != null) {
              FrameAnimation.this.mAnimationListener.onAnimationPause();
            }
            return;
          }
          return;
        }
        if ((paramInt == 0) && (FrameAnimation.this.mAnimationListener != null)) {
          FrameAnimation.this.mAnimationListener.onAnimationStart();
        }
        FrameAnimation.this.mImageView.setBackgroundResource(FrameAnimation.this.mFrameRess[paramInt]);
        if (paramInt == FrameAnimation.this.mLastFrame)
        {
          if (FrameAnimation.this.mIsRepeat)
          {
            if (FrameAnimation.this.mAnimationListener != null) {
              FrameAnimation.this.mAnimationListener.onAnimationRepeat();
            }
            FrameAnimation.this.playByDurations(0);
          }
          else if (FrameAnimation.this.mAnimationListener != null)
          {
            FrameAnimation.this.mAnimationListener.onAnimationEnd();
          }
        }
        else {
          FrameAnimation.this.playByDurations(paramInt + 1);
        }
      }
    }, this.mDurations[paramInt]);
  }
  
  private void playByDurationsAndDelay(final int paramInt)
  {
    ImageView localImageView = this.mImageView;
    Runnable local1 = new Runnable()
    {
      public void run()
      {
        if (FrameAnimation.this.mPause)
        {
          FrameAnimation.access$102(FrameAnimation.this, 1);
          FrameAnimation.access$202(FrameAnimation.this, paramInt);
          return;
        }
        if ((paramInt == 0) && (FrameAnimation.this.mAnimationListener != null)) {
          FrameAnimation.this.mAnimationListener.onAnimationStart();
        }
        FrameAnimation.this.mImageView.setBackgroundResource(FrameAnimation.this.mFrameRess[paramInt]);
        if (paramInt == FrameAnimation.this.mLastFrame)
        {
          if (FrameAnimation.this.mAnimationListener != null) {
            FrameAnimation.this.mAnimationListener.onAnimationRepeat();
          }
          FrameAnimation.access$702(FrameAnimation.this, true);
          FrameAnimation.this.playByDurationsAndDelay(0);
        }
        else
        {
          FrameAnimation.this.playByDurationsAndDelay(paramInt + 1);
        }
      }
    };
    if (this.mNext)
    {
      int i = this.mDelay;
      if (i > 0)
      {
        l = i;
        break label47;
      }
    }
    long l = this.mDurations[paramInt];
    label47:
    localImageView.postDelayed(local1, l);
  }
  
  public boolean isPause()
  {
    return this.mPause;
  }
  
  public void pauseAnimation()
  {
    this.mPause = true;
  }
  
  public void release()
  {
    pauseAnimation();
  }
  
  public void restartAnimation()
  {
    if (this.mPause)
    {
      this.mPause = false;
      switch (this.mCurrentSelect)
      {
      default: 
        break;
      case 4: 
        play(this.mCurrentFrame);
        break;
      case 3: 
        playByDurations(this.mCurrentFrame);
        break;
      case 2: 
        playAndDelay(this.mCurrentFrame);
        break;
      case 1: 
        playByDurationsAndDelay(this.mCurrentFrame);
      }
    }
  }
  
  public void setAnimationListener(AnimationListener paramAnimationListener)
  {
    this.mAnimationListener = paramAnimationListener;
  }
  
  public static abstract interface AnimationListener
  {
    public abstract void onAnimationEnd();
    
    public abstract void onAnimationPause();
    
    public abstract void onAnimationRepeat();
    
    public abstract void onAnimationStart();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/red/FrameAnimation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */