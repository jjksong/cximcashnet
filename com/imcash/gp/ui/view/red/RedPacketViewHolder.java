package com.imcash.gp.ui.view.red;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

public class RedPacketViewHolder
{
  private Context mContext;
  private FrameAnimation mFrameAnimation;
  private int[] mImgResIds = { 2131361797, 2131361800, 2131361801, 2131361802, 2131361803, 2131361804, 2131361805, 2131361805, 2131361806, 2131361807, 2131361802, 2131361798, 2131361799 };
  @Bind({2131165514})
  ImageView mIvAvatar;
  @Bind({2131165515})
  ImageView mIvClose;
  @Bind({2131165518})
  ImageView mIvOpen;
  private OnRedPacketDialogClickListener mListener;
  @Bind({2131165866})
  TextView mTvMsg;
  @Bind({2131165867})
  TextView mTvName;
  
  public RedPacketViewHolder(Context paramContext, View paramView)
  {
    this.mContext = paramContext;
    ButterKnife.bind(this, paramView);
  }
  
  @OnClick({2131165515, 2131165518})
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (i != 2131165515)
    {
      if (i == 2131165518)
      {
        if (this.mFrameAnimation != null) {
          return;
        }
        startAnim();
        paramView = this.mListener;
        if (paramView != null) {
          paramView.onOpenClick();
        }
      }
    }
    else
    {
      stopAnim();
      paramView = this.mListener;
      if (paramView != null) {
        paramView.onCloseClick();
      }
    }
  }
  
  public void setData(RedPacketEntity paramRedPacketEntity)
  {
    RequestOptions localRequestOptions = new RequestOptions();
    ((RequestOptions)localRequestOptions.centerCrop()).circleCrop();
    Glide.with(this.mContext).load(paramRedPacketEntity.avatar).apply(localRequestOptions).into(this.mIvAvatar);
    this.mTvName.setText(paramRedPacketEntity.name);
    this.mTvMsg.setText(paramRedPacketEntity.remark);
  }
  
  public void setOnRedPacketDialogClickListener(OnRedPacketDialogClickListener paramOnRedPacketDialogClickListener)
  {
    this.mListener = paramOnRedPacketDialogClickListener;
  }
  
  public void startAnim()
  {
    this.mFrameAnimation = new FrameAnimation(this.mIvOpen, this.mImgResIds, 125, true);
    this.mFrameAnimation.setAnimationListener(new FrameAnimation.AnimationListener()
    {
      public void onAnimationEnd()
      {
        Log.i("", "end");
      }
      
      public void onAnimationPause()
      {
        RedPacketViewHolder.this.mIvOpen.setBackgroundResource(2131361797);
      }
      
      public void onAnimationRepeat()
      {
        Log.i("", "repeat");
      }
      
      public void onAnimationStart()
      {
        Log.i("", "start");
      }
    });
  }
  
  public void stopAnim()
  {
    FrameAnimation localFrameAnimation = this.mFrameAnimation;
    if (localFrameAnimation != null)
    {
      localFrameAnimation.release();
      this.mFrameAnimation = null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/ui/view/red/RedPacketViewHolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */