package com.imcash.gp.third.jpush;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;
import com.imcash.gp.tools.SystemUtils;
import com.imcash.gp.ui.activity.MainActivity;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class MyReceiver
  extends BroadcastReceiver
{
  private static final int NOTIFICATION_SHOW_SHOW_AT_MOST = 3;
  private static final String TAG = "JIGUANG-Example";
  
  private static String printBundle(Bundle paramBundle)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    Iterator localIterator1 = paramBundle.keySet().iterator();
    while (localIterator1.hasNext())
    {
      String str1 = (String)localIterator1.next();
      StringBuilder localStringBuilder2;
      if (str1.equals("cn.jpush.android.NOTIFICATION_ID"))
      {
        localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append("\nkey:");
        localStringBuilder2.append(str1);
        localStringBuilder2.append(", value:");
        localStringBuilder2.append(paramBundle.getInt(str1));
        localStringBuilder1.append(localStringBuilder2.toString());
      }
      else if (str1.equals("cn.jpush.android.CONNECTION_CHANGE"))
      {
        localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append("\nkey:");
        localStringBuilder2.append(str1);
        localStringBuilder2.append(", value:");
        localStringBuilder2.append(paramBundle.getBoolean(str1));
        localStringBuilder1.append(localStringBuilder2.toString());
      }
      else if (str1.equals("cn.jpush.android.EXTRA"))
      {
        if (TextUtils.isEmpty(paramBundle.getString("cn.jpush.android.EXTRA"))) {
          Logger.i("JIGUANG-Example", "This message has no Extra data");
        } else {
          try
          {
            JSONObject localJSONObject = new org/json/JSONObject;
            localJSONObject.<init>(paramBundle.getString("cn.jpush.android.EXTRA"));
            Iterator localIterator2 = localJSONObject.keys();
            while (localIterator2.hasNext())
            {
              String str2 = (String)localIterator2.next();
              localStringBuilder2 = new java/lang/StringBuilder;
              localStringBuilder2.<init>();
              localStringBuilder2.append("\nkey:");
              localStringBuilder2.append(str1);
              localStringBuilder2.append(", value: [");
              localStringBuilder2.append(str2);
              localStringBuilder2.append(" - ");
              localStringBuilder2.append(localJSONObject.optString(str2));
              localStringBuilder2.append("]");
              localStringBuilder1.append(localStringBuilder2.toString());
            }
          }
          catch (JSONException localJSONException)
          {
            Logger.e("JIGUANG-Example", "Get message extra JSON error!");
          }
        }
      }
      else
      {
        localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append("\nkey:");
        localStringBuilder2.append(localJSONException);
        localStringBuilder2.append(", value:");
        localStringBuilder2.append(paramBundle.get(localJSONException));
        localStringBuilder1.append(localStringBuilder2.toString());
      }
    }
    return localStringBuilder1.toString();
  }
  
  private void processCustomMessage(Context paramContext, Bundle paramBundle)
  {
    NotificationCompat.Builder localBuilder = new NotificationCompat.Builder(paramContext);
    String str = paramBundle.getString("cn.jpush.android.MESSAGE");
    Object localObject1 = paramBundle.getString("cn.jpush.android.EXTRA");
    Object localObject2 = BitmapFactory.decodeResource(paramContext.getResources(), 2131361794);
    localBuilder.setAutoCancel(true).setContentText(str).setContentTitle("ImCash").setSmallIcon(2131361794).setNumber(paramBundle.getInt("cn.jpush.android.NOTIFICATION_ID")).setLargeIcon((Bitmap)localObject2);
    if (!TextUtils.isEmpty((CharSequence)localObject1)) {
      try
      {
        localObject2 = new org/json/JSONObject;
        ((JSONObject)localObject2).<init>((String)localObject1);
        if ((((JSONObject)localObject2).length() > 0) && ("1".equals(((JSONObject)localObject2).getString("sound"))))
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("android.resource://");
          ((StringBuilder)localObject1).append(paramContext.getPackageName());
          ((StringBuilder)localObject1).append("/");
          ((StringBuilder)localObject1).append(2131492864);
          localBuilder.setSound(Uri.parse(((StringBuilder)localObject1).toString()));
        }
      }
      catch (JSONException localJSONException)
      {
        localJSONException.printStackTrace();
      }
    }
    ((NotificationManager)paramContext.getSystemService("notification")).notify(paramBundle.getInt("cn.jpush.android.NOTIFICATION_ID"), localBuilder.build());
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      Bundle localBundle = paramIntent.getExtras();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("[MyReceiver] onReceive - ");
      localStringBuilder.append(paramIntent.getAction());
      localStringBuilder.append(", extras: ");
      localStringBuilder.append(printBundle(localBundle));
      Logger.d("JIGUANG-Example", localStringBuilder.toString());
      if ("cn.jpush.android.intent.REGISTRATION".equals(paramIntent.getAction()))
      {
        paramContext = localBundle.getString("cn.jpush.android.REGISTRATION_ID");
        paramIntent = new java/lang/StringBuilder;
        paramIntent.<init>();
        paramIntent.append("[MyReceiver] 接收Registration Id : ");
        paramIntent.append(paramContext);
        Logger.d("JIGUANG-Example", paramIntent.toString());
      }
      else if ("cn.jpush.android.intent.MESSAGE_RECEIVED".equals(paramIntent.getAction()))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("[MyReceiver] 接收到推送下来的自定义消息: ");
        paramContext.append(localBundle.getString("cn.jpush.android.MESSAGE"));
        Logger.d("JIGUANG-Example", paramContext.toString());
      }
      else if ("cn.jpush.android.intent.NOTIFICATION_RECEIVED".equals(paramIntent.getAction()))
      {
        Logger.d("JIGUANG-Example", "[MyReceiver] 接收到推送下来的通知");
        int i = localBundle.getInt("cn.jpush.android.NOTIFICATION_ID");
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("[MyReceiver] 接收到推送下来的通知的ID: ");
        paramContext.append(i);
        Logger.d("JIGUANG-Example", paramContext.toString());
      }
      else if ("cn.jpush.android.intent.NOTIFICATION_OPENED".equals(paramIntent.getAction()))
      {
        Logger.d("JIGUANG-Example", "[MyReceiver] 用户点击打开了通知");
        toMainPage(paramContext, localBundle);
      }
      else if ("cn.jpush.android.intent.ACTION_RICHPUSH_CALLBACK".equals(paramIntent.getAction()))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("[MyReceiver] 用户收到到RICH PUSH CALLBACK: ");
        paramContext.append(localBundle.getString("cn.jpush.android.EXTRA"));
        Logger.d("JIGUANG-Example", paramContext.toString());
      }
      else if ("cn.jpush.android.intent.CONNECTION".equals(paramIntent.getAction()))
      {
        boolean bool = paramIntent.getBooleanExtra("cn.jpush.android.CONNECTION_CHANGE", false);
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("[MyReceiver]");
        paramContext.append(paramIntent.getAction());
        paramContext.append(" connected state change to ");
        paramContext.append(bool);
        Logger.w("JIGUANG-Example", paramContext.toString());
      }
      else
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("[MyReceiver] Unhandled intent - ");
        paramContext.append(paramIntent.getAction());
        Logger.d("JIGUANG-Example", paramContext.toString());
      }
      return;
    }
    catch (Exception paramContext)
    {
      for (;;) {}
    }
  }
  
  protected void toMainPage(Context paramContext, Bundle paramBundle)
  {
    if (SystemUtils.isAppAlive(paramContext, "com.imcash.gp"))
    {
      paramBundle = new Intent(paramContext, MainActivity.class);
      paramBundle.setFlags(268435456);
      paramContext.startActivities(new Intent[] { paramBundle });
    }
    else
    {
      paramBundle = new Intent(paramContext, MainActivity.class);
      paramBundle.setFlags(268435456);
      paramContext.startActivities(new Intent[] { paramBundle });
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/jpush/MyReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */