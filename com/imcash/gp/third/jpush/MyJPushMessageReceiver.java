package com.imcash.gp.third.jpush;

import android.content.Context;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.service.JPushMessageReceiver;

public class MyJPushMessageReceiver
  extends JPushMessageReceiver
{
  public void onAliasOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    TagAliasOperatorHelper.getInstance().onAliasOperatorResult(paramContext, paramJPushMessage);
    super.onAliasOperatorResult(paramContext, paramJPushMessage);
  }
  
  public void onCheckTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    TagAliasOperatorHelper.getInstance().onCheckTagOperatorResult(paramContext, paramJPushMessage);
    super.onCheckTagOperatorResult(paramContext, paramJPushMessage);
  }
  
  public void onMobileNumberOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    TagAliasOperatorHelper.getInstance().onMobileNumberOperatorResult(paramContext, paramJPushMessage);
    super.onMobileNumberOperatorResult(paramContext, paramJPushMessage);
  }
  
  public void onTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    TagAliasOperatorHelper.getInstance().onTagOperatorResult(paramContext, paramJPushMessage);
    super.onTagOperatorResult(paramContext, paramJPushMessage);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/jpush/MyJPushMessageReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */