package com.imcash.gp.third.jpush;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.SparseArray;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.JPushMessage;
import java.util.Locale;
import java.util.Set;

public class TagAliasOperatorHelper
{
  public static final int ACTION_ADD = 1;
  public static final int ACTION_CHECK = 6;
  public static final int ACTION_CLEAN = 4;
  public static final int ACTION_DELETE = 3;
  public static final int ACTION_GET = 5;
  public static final int ACTION_SET = 2;
  public static final int DELAY_SEND_ACTION = 1;
  public static final int DELAY_SET_MOBILE_NUMBER_ACTION = 2;
  private static final String TAG = "JIGUANG-TagAliasHelper";
  private static TagAliasOperatorHelper mInstance;
  public static int sequence = 1;
  private Context context;
  private Handler delaySendHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Object localObject;
      switch (paramAnonymousMessage.what)
      {
      default: 
        break;
      case 2: 
        if ((paramAnonymousMessage.obj != null) && ((paramAnonymousMessage.obj instanceof String)))
        {
          Logger.i("JIGUANG-TagAliasHelper", "retry set mobile number");
          TagAliasOperatorHelper.sequence += 1;
          localObject = (String)paramAnonymousMessage.obj;
          TagAliasOperatorHelper.this.setActionCache.put(TagAliasOperatorHelper.sequence, localObject);
          if (TagAliasOperatorHelper.this.context != null)
          {
            paramAnonymousMessage = TagAliasOperatorHelper.this;
            paramAnonymousMessage.handleAction(paramAnonymousMessage.context, TagAliasOperatorHelper.sequence, (String)localObject);
          }
          else
          {
            Logger.e("JIGUANG-TagAliasHelper", "#unexcepted - context was null");
          }
        }
        else
        {
          Logger.w("JIGUANG-TagAliasHelper", "#unexcepted - msg obj was incorrect");
        }
        break;
      case 1: 
        if ((paramAnonymousMessage.obj != null) && ((paramAnonymousMessage.obj instanceof TagAliasOperatorHelper.TagAliasBean)))
        {
          Logger.i("JIGUANG-TagAliasHelper", "on delay time");
          TagAliasOperatorHelper.sequence += 1;
          paramAnonymousMessage = (TagAliasOperatorHelper.TagAliasBean)paramAnonymousMessage.obj;
          TagAliasOperatorHelper.this.setActionCache.put(TagAliasOperatorHelper.sequence, paramAnonymousMessage);
          if (TagAliasOperatorHelper.this.context != null)
          {
            localObject = TagAliasOperatorHelper.this;
            ((TagAliasOperatorHelper)localObject).handleAction(((TagAliasOperatorHelper)localObject).context, TagAliasOperatorHelper.sequence, paramAnonymousMessage);
          }
          else
          {
            Logger.e("JIGUANG-TagAliasHelper", "#unexcepted - context was null");
          }
        }
        else
        {
          Logger.w("JIGUANG-TagAliasHelper", "#unexcepted - msg obj was incorrect");
        }
        break;
      }
    }
  };
  private SparseArray<Object> setActionCache = new SparseArray();
  
  private boolean RetryActionIfNeeded(int paramInt, TagAliasBean paramTagAliasBean)
  {
    if (!ExampleUtil.isConnected(this.context))
    {
      Logger.w("JIGUANG-TagAliasHelper", "no network");
      return false;
    }
    if ((paramInt == 6002) || (paramInt == 6014))
    {
      Logger.d("JIGUANG-TagAliasHelper", "need retry");
      if (paramTagAliasBean != null)
      {
        Message localMessage = new Message();
        localMessage.what = 1;
        localMessage.obj = paramTagAliasBean;
        this.delaySendHandler.sendMessageDelayed(localMessage, 60000L);
        ExampleUtil.showToast(getRetryStr(paramTagAliasBean.isAliasAction, paramTagAliasBean.action, paramInt), this.context);
        return true;
      }
    }
    return false;
  }
  
  private boolean RetrySetMObileNumberActionIfNeeded(int paramInt, String paramString)
  {
    if (!ExampleUtil.isConnected(this.context))
    {
      Logger.w("JIGUANG-TagAliasHelper", "no network");
      return false;
    }
    if ((paramInt != 6002) && (paramInt != 6024)) {
      return false;
    }
    Logger.d("JIGUANG-TagAliasHelper", "need retry");
    Object localObject = new Message();
    ((Message)localObject).what = 2;
    ((Message)localObject).obj = paramString;
    this.delaySendHandler.sendMessageDelayed((Message)localObject, 60000L);
    localObject = Locale.ENGLISH;
    if (paramInt == 6002) {
      paramString = "timeout";
    } else {
      paramString = "server internal error”";
    }
    ExampleUtil.showToast(String.format((Locale)localObject, "Failed to set mobile number due to %s. Try again after 60s.", new Object[] { paramString }), this.context);
    return true;
  }
  
  private String getActionStr(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "unkonw operation";
    case 6: 
      return "check";
    case 5: 
      return "get";
    case 4: 
      return "clean";
    case 3: 
      return "delete";
    case 2: 
      return "set";
    }
    return "add";
  }
  
  public static TagAliasOperatorHelper getInstance()
  {
    if (mInstance == null) {
      try
      {
        if (mInstance == null)
        {
          TagAliasOperatorHelper localTagAliasOperatorHelper = new com/imcash/gp/third/jpush/TagAliasOperatorHelper;
          localTagAliasOperatorHelper.<init>();
          mInstance = localTagAliasOperatorHelper;
        }
      }
      finally {}
    }
    return mInstance;
  }
  
  private String getRetryStr(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    Locale localLocale = Locale.ENGLISH;
    String str3 = getActionStr(paramInt1);
    String str1;
    if (paramBoolean) {
      str1 = "alias";
    } else {
      str1 = " tags";
    }
    String str2;
    if (paramInt2 == 6002) {
      str2 = "timeout";
    } else {
      str2 = "server too busy";
    }
    return String.format(localLocale, "Failed to %s %s due to %s. Try again after 60s.", new Object[] { str3, str1, str2 });
  }
  
  public Object get(int paramInt)
  {
    return this.setActionCache.get(paramInt);
  }
  
  public void handleAction(Context paramContext, int paramInt, TagAliasBean paramTagAliasBean)
  {
    init(paramContext);
    if (paramTagAliasBean == null)
    {
      Logger.w("JIGUANG-TagAliasHelper", "tagAliasBean was null");
      return;
    }
    put(paramInt, paramTagAliasBean);
    if (paramTagAliasBean.isAliasAction)
    {
      int i = paramTagAliasBean.action;
      if (i != 5) {
        switch (i)
        {
        default: 
          Logger.w("JIGUANG-TagAliasHelper", "unsupport alias action type");
          return;
        case 3: 
          JPushInterface.deleteAlias(paramContext, paramInt);
          break;
        case 2: 
          JPushInterface.setAlias(paramContext, paramInt, paramTagAliasBean.alias);
          break;
        }
      } else {
        JPushInterface.getAlias(paramContext, paramInt);
      }
    }
    else
    {
      switch (paramTagAliasBean.action)
      {
      default: 
        Logger.w("JIGUANG-TagAliasHelper", "unsupport tag action type");
        return;
      case 6: 
        JPushInterface.checkTagBindState(paramContext, paramInt, (String)paramTagAliasBean.tags.toArray()[0]);
        break;
      case 5: 
        JPushInterface.getAllTags(paramContext, paramInt);
        break;
      case 4: 
        JPushInterface.cleanTags(paramContext, paramInt);
        break;
      case 3: 
        JPushInterface.deleteTags(paramContext, paramInt, paramTagAliasBean.tags);
        break;
      case 2: 
        JPushInterface.setTags(paramContext, paramInt, paramTagAliasBean.tags);
        break;
      case 1: 
        JPushInterface.addTags(paramContext, paramInt, paramTagAliasBean.tags);
      }
    }
  }
  
  public void handleAction(Context paramContext, int paramInt, String paramString)
  {
    put(paramInt, paramString);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("sequence:");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(",mobileNumber:");
    localStringBuilder.append(paramString);
    Logger.d("JIGUANG-TagAliasHelper", localStringBuilder.toString());
    JPushInterface.setMobileNumber(paramContext, paramInt, paramString);
  }
  
  public void init(Context paramContext)
  {
    if (paramContext != null) {
      this.context = paramContext.getApplicationContext();
    }
  }
  
  public void onAliasOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    int i = paramJPushMessage.getSequence();
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("action - onAliasOperatorResult, sequence:");
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(",alias:");
    ((StringBuilder)localObject1).append(paramJPushMessage.getAlias());
    Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject1).toString());
    init(paramContext);
    localObject1 = (TagAliasBean)this.setActionCache.get(i);
    if (localObject1 == null)
    {
      ExampleUtil.showToast("获取缓存记录失败", paramContext);
      return;
    }
    if (paramJPushMessage.getErrorCode() == 0)
    {
      paramJPushMessage = new StringBuilder();
      paramJPushMessage.append("action - modify alias Success,sequence:");
      paramJPushMessage.append(i);
      Logger.i("JIGUANG-TagAliasHelper", paramJPushMessage.toString());
      this.setActionCache.remove(i);
      paramJPushMessage = new StringBuilder();
      paramJPushMessage.append(getActionStr(((TagAliasBean)localObject1).action));
      paramJPushMessage.append(" alias success");
      paramJPushMessage = paramJPushMessage.toString();
      Logger.i("JIGUANG-TagAliasHelper", paramJPushMessage);
      ExampleUtil.showToast(paramJPushMessage, paramContext);
    }
    else
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Failed to ");
      ((StringBuilder)localObject2).append(getActionStr(((TagAliasBean)localObject1).action));
      ((StringBuilder)localObject2).append(" alias, errorCode:");
      ((StringBuilder)localObject2).append(paramJPushMessage.getErrorCode());
      localObject2 = ((StringBuilder)localObject2).toString();
      Logger.e("JIGUANG-TagAliasHelper", (String)localObject2);
      if (!RetryActionIfNeeded(paramJPushMessage.getErrorCode(), (TagAliasBean)localObject1)) {
        ExampleUtil.showToast((String)localObject2, paramContext);
      }
    }
  }
  
  public void onCheckTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    int i = paramJPushMessage.getSequence();
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("action - onCheckTagOperatorResult, sequence:");
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(",checktag:");
    ((StringBuilder)localObject1).append(paramJPushMessage.getCheckTag());
    Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject1).toString());
    init(paramContext);
    localObject1 = (TagAliasBean)this.setActionCache.get(i);
    if (localObject1 == null)
    {
      ExampleUtil.showToast("获取缓存记录失败", paramContext);
      return;
    }
    Object localObject2;
    if (paramJPushMessage.getErrorCode() == 0)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("tagBean:");
      ((StringBuilder)localObject2).append(localObject1);
      Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject2).toString());
      this.setActionCache.remove(i);
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(getActionStr(((TagAliasBean)localObject1).action));
      ((StringBuilder)localObject2).append(" tag ");
      ((StringBuilder)localObject2).append(paramJPushMessage.getCheckTag());
      ((StringBuilder)localObject2).append(" bind state success,state:");
      ((StringBuilder)localObject2).append(paramJPushMessage.getTagCheckStateResult());
      paramJPushMessage = ((StringBuilder)localObject2).toString();
      Logger.i("JIGUANG-TagAliasHelper", paramJPushMessage);
      ExampleUtil.showToast(paramJPushMessage, paramContext);
    }
    else
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Failed to ");
      ((StringBuilder)localObject2).append(getActionStr(((TagAliasBean)localObject1).action));
      ((StringBuilder)localObject2).append(" tags, errorCode:");
      ((StringBuilder)localObject2).append(paramJPushMessage.getErrorCode());
      localObject2 = ((StringBuilder)localObject2).toString();
      Logger.e("JIGUANG-TagAliasHelper", (String)localObject2);
      if (!RetryActionIfNeeded(paramJPushMessage.getErrorCode(), (TagAliasBean)localObject1)) {
        ExampleUtil.showToast((String)localObject2, paramContext);
      }
    }
  }
  
  public void onMobileNumberOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    int i = paramJPushMessage.getSequence();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("action - onMobileNumberOperatorResult, sequence:");
    ((StringBuilder)localObject).append(i);
    ((StringBuilder)localObject).append(",mobileNumber:");
    ((StringBuilder)localObject).append(paramJPushMessage.getMobileNumber());
    Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject).toString());
    init(paramContext);
    if (paramJPushMessage.getErrorCode() == 0)
    {
      paramContext = new StringBuilder();
      paramContext.append("action - set mobile number Success,sequence:");
      paramContext.append(i);
      Logger.i("JIGUANG-TagAliasHelper", paramContext.toString());
      this.setActionCache.remove(i);
    }
    else
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Failed to set mobile number, errorCode:");
      ((StringBuilder)localObject).append(paramJPushMessage.getErrorCode());
      localObject = ((StringBuilder)localObject).toString();
      Logger.e("JIGUANG-TagAliasHelper", (String)localObject);
      if (!RetrySetMObileNumberActionIfNeeded(paramJPushMessage.getErrorCode(), paramJPushMessage.getMobileNumber())) {
        ExampleUtil.showToast((String)localObject, paramContext);
      }
    }
  }
  
  public void onTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage)
  {
    int i = paramJPushMessage.getSequence();
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("action - onTagOperatorResult, sequence:");
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(",tags:");
    ((StringBuilder)localObject1).append(paramJPushMessage.getTags());
    Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject1).toString());
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("tags size:");
    ((StringBuilder)localObject1).append(paramJPushMessage.getTags().size());
    Logger.i("JIGUANG-TagAliasHelper", ((StringBuilder)localObject1).toString());
    init(paramContext);
    TagAliasBean localTagAliasBean = (TagAliasBean)this.setActionCache.get(i);
    if (localTagAliasBean == null)
    {
      ExampleUtil.showToast("获取缓存记录失败", paramContext);
      return;
    }
    if (paramJPushMessage.getErrorCode() == 0)
    {
      paramJPushMessage = new StringBuilder();
      paramJPushMessage.append("action - modify tag Success,sequence:");
      paramJPushMessage.append(i);
      Logger.i("JIGUANG-TagAliasHelper", paramJPushMessage.toString());
      this.setActionCache.remove(i);
      paramJPushMessage = new StringBuilder();
      paramJPushMessage.append(getActionStr(localTagAliasBean.action));
      paramJPushMessage.append(" tags success");
      paramJPushMessage = paramJPushMessage.toString();
      Logger.i("JIGUANG-TagAliasHelper", paramJPushMessage);
      ExampleUtil.showToast(paramJPushMessage, paramContext);
    }
    else
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Failed to ");
      ((StringBuilder)localObject1).append(getActionStr(localTagAliasBean.action));
      ((StringBuilder)localObject1).append(" tags");
      Object localObject2 = ((StringBuilder)localObject1).toString();
      localObject1 = localObject2;
      if (paramJPushMessage.getErrorCode() == 6018)
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append(", tags is exceed limit need to clean");
        localObject1 = ((StringBuilder)localObject1).toString();
      }
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(", errorCode:");
      ((StringBuilder)localObject2).append(paramJPushMessage.getErrorCode());
      localObject1 = ((StringBuilder)localObject2).toString();
      Logger.e("JIGUANG-TagAliasHelper", (String)localObject1);
      if (!RetryActionIfNeeded(paramJPushMessage.getErrorCode(), localTagAliasBean)) {
        ExampleUtil.showToast((String)localObject1, paramContext);
      }
    }
  }
  
  public void put(int paramInt, Object paramObject)
  {
    this.setActionCache.put(paramInt, paramObject);
  }
  
  public Object remove(int paramInt)
  {
    return this.setActionCache.get(paramInt);
  }
  
  public static class TagAliasBean
  {
    int action;
    String alias;
    boolean isAliasAction;
    Set<String> tags;
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("TagAliasBean{action=");
      localStringBuilder.append(this.action);
      localStringBuilder.append(", tags=");
      localStringBuilder.append(this.tags);
      localStringBuilder.append(", alias='");
      localStringBuilder.append(this.alias);
      localStringBuilder.append('\'');
      localStringBuilder.append(", isAliasAction=");
      localStringBuilder.append(this.isAliasAction);
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/jpush/TagAliasOperatorHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */