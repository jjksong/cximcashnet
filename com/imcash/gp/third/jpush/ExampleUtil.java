package com.imcash.gp.third.jpush;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;
import cn.jpush.android.api.JPushInterface;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExampleUtil
{
  public static final String KEY_APP_KEY = "JPUSH_APPKEY";
  private static final String MOBILE_NUMBER_CHARS = "^[+0-9][-0-9]{1,}$";
  public static final String PREFS_DAYS = "JPUSH_EXAMPLE_DAYS";
  public static final String PREFS_END_TIME = "PREFS_END_TIME";
  public static final String PREFS_NAME = "JPUSH_EXAMPLE";
  public static final String PREFS_START_TIME = "PREFS_START_TIME";
  
  public static String GetVersion(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
      return paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext) {}
    return "Unknown";
  }
  
  public static String getAppKey(Context paramContext)
  {
    localObject2 = null;
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      Object localObject1;
      if (paramContext != null) {
        localObject1 = paramContext.metaData;
      } else {
        localObject1 = null;
      }
      paramContext = (Context)localObject2;
      if (localObject1 != null)
      {
        localObject1 = ((Bundle)localObject1).getString("JPUSH_APPKEY");
        paramContext = (Context)localObject2;
        if (localObject1 != null) {
          try
          {
            int i = ((String)localObject1).length();
            if (i != 24) {
              paramContext = (Context)localObject2;
            } else {
              paramContext = (Context)localObject1;
            }
          }
          catch (PackageManager.NameNotFoundException paramContext)
          {
            paramContext = (Context)localObject1;
          }
        }
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;)
      {
        paramContext = (Context)localObject2;
      }
    }
    return paramContext;
  }
  
  public static String getDeviceId(Context paramContext)
  {
    return JPushInterface.getUdid(paramContext);
  }
  
  public static String getImei(Context paramContext, String paramString)
  {
    try
    {
      paramContext = ((TelephonyManager)paramContext.getSystemService("phone")).getDeviceId();
    }
    catch (Exception paramContext)
    {
      Logger.e(ExampleUtil.class.getSimpleName(), paramContext.getMessage());
      paramContext = null;
    }
    if (isReadableASCII(paramContext)) {
      return paramContext;
    }
    return paramString;
  }
  
  public static boolean isConnected(Context paramContext)
  {
    paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    boolean bool;
    if ((paramContext != null) && (paramContext.isConnected())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public static boolean isEmpty(String paramString)
  {
    if (paramString == null) {
      return true;
    }
    if (paramString.length() == 0) {
      return true;
    }
    return paramString.trim().length() == 0;
  }
  
  private static boolean isReadableASCII(CharSequence paramCharSequence)
  {
    if (TextUtils.isEmpty(paramCharSequence)) {
      return false;
    }
    try
    {
      boolean bool = Pattern.compile("[\\x20-\\x7E]+").matcher(paramCharSequence).matches();
      return bool;
    }
    catch (Throwable paramCharSequence) {}
    return true;
  }
  
  public static boolean isValidMobileNumber(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return true;
    }
    return Pattern.compile("^[+0-9][-0-9]{1,}$").matcher(paramString).matches();
  }
  
  public static boolean isValidTagAndAlias(String paramString)
  {
    return Pattern.compile("^[一-龥0-9a-zA-Z_!@#$&*+=.|]+$").matcher(paramString).matches();
  }
  
  public static void showToast(final String paramString, Context paramContext)
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        Looper.prepare();
        Toast.makeText(this.val$context, paramString, 0).show();
        Looper.loop();
      }
    }).start();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/jpush/ExampleUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */