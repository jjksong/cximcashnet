package com.imcash.gp.third.faceid;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class MultipartEntity
  implements HttpEntity
{
  public static final char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  public final byte[] BINARY_ENCODING = "Content-Transfer-Encoding: binary\r\n\r\n".getBytes();
  public final byte[] BIT_ENCODING = "Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes();
  public final String CONTENT_DISPOSITION = "Content-Disposition: ";
  public final String CONTENT_TYPE = "Content-Type: ";
  public final String NEW_LINE_STR = "\r\n";
  public final String TYPE_OCTET_STREAM = "application/octet-stream";
  public final String TYPE_TEXT_CHARSET = "text/plain; charset=UTF-8";
  public String mBoundary = null;
  public ByteArrayOutputStream mOutputStream = new ByteArrayOutputStream();
  
  private void closeSilently(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
      }
      catch (IOException paramCloseable)
      {
        paramCloseable.printStackTrace();
      }
    }
  }
  
  private final String generateBoundary()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < 30; i++)
    {
      char[] arrayOfChar = MULTIPART_CHARS;
      localStringBuffer.append(arrayOfChar[localRandom.nextInt(arrayOfChar.length)]);
    }
    return localStringBuffer.toString();
  }
  
  private byte[] getContentDispositionBytes(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    StringBuilder localStringBuilder2 = new StringBuilder();
    localStringBuilder2.append("Content-Disposition: form-data; name=\"");
    localStringBuilder2.append(paramString1);
    localStringBuilder2.append("\"");
    localStringBuilder1.append(localStringBuilder2.toString());
    if ((paramString2 != null) && (!"".equals(paramString2)))
    {
      paramString1 = new StringBuilder();
      paramString1.append("; filename=\"");
      paramString1.append(paramString2);
      paramString1.append("\"");
      localStringBuilder1.append(paramString1.toString());
    }
    localStringBuilder1.append("\r\n");
    return localStringBuilder1.toString().getBytes();
  }
  
  private void writeFirstBoundary()
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = this.mOutputStream;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("--");
    localStringBuilder.append(this.mBoundary);
    localStringBuilder.append("\r\n");
    localByteArrayOutputStream.write(localStringBuilder.toString().getBytes());
  }
  
  private void writeToOutputStream(String paramString1, byte[] paramArrayOfByte1, String paramString2, byte[] paramArrayOfByte2, String paramString3)
  {
    try
    {
      writeFirstBoundary();
      ByteArrayOutputStream localByteArrayOutputStream = this.mOutputStream;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("Content-Type: ");
      localStringBuilder.append(paramString2);
      localStringBuilder.append("\r\n");
      localByteArrayOutputStream.write(localStringBuilder.toString().getBytes());
      this.mOutputStream.write(getContentDispositionBytes(paramString1, paramString3));
      this.mOutputStream.write(paramArrayOfByte2);
      this.mOutputStream.write(paramArrayOfByte1);
      this.mOutputStream.write("\r\n".getBytes());
    }
    catch (IOException paramString1)
    {
      paramString1.printStackTrace();
    }
  }
  
  public void addBinaryPart(String paramString, byte[] paramArrayOfByte)
  {
    writeToOutputStream(paramString, paramArrayOfByte, "application/octet-stream", this.BINARY_ENCODING, paramString);
  }
  
  /* Error */
  public void addFilePart(String paramString, java.io.File paramFile)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 7
    //   6: aload 7
    //   8: astore 4
    //   10: new 151	java/io/FileInputStream
    //   13: astore 5
    //   15: aload 7
    //   17: astore 4
    //   19: aload 5
    //   21: aload_2
    //   22: invokespecial 154	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   25: aload_0
    //   26: invokespecial 141	com/imcash/gp/third/faceid/MultipartEntity:writeFirstBoundary	()V
    //   29: aload_0
    //   30: getfield 75	com/imcash/gp/third/faceid/MultipartEntity:mOutputStream	Ljava/io/ByteArrayOutputStream;
    //   33: aload_0
    //   34: aload_1
    //   35: aload_2
    //   36: invokevirtual 159	java/io/File:getName	()Ljava/lang/String;
    //   39: invokespecial 143	com/imcash/gp/third/faceid/MultipartEntity:getContentDispositionBytes	(Ljava/lang/String;Ljava/lang/String;)[B
    //   42: invokevirtual 136	java/io/ByteArrayOutputStream:write	([B)V
    //   45: aload_0
    //   46: getfield 75	com/imcash/gp/third/faceid/MultipartEntity:mOutputStream	Ljava/io/ByteArrayOutputStream;
    //   49: ldc -95
    //   51: invokevirtual 62	java/lang/String:getBytes	()[B
    //   54: invokevirtual 136	java/io/ByteArrayOutputStream:write	([B)V
    //   57: aload_0
    //   58: getfield 75	com/imcash/gp/third/faceid/MultipartEntity:mOutputStream	Ljava/io/ByteArrayOutputStream;
    //   61: aload_0
    //   62: getfield 64	com/imcash/gp/third/faceid/MultipartEntity:BINARY_ENCODING	[B
    //   65: invokevirtual 136	java/io/ByteArrayOutputStream:write	([B)V
    //   68: sipush 4096
    //   71: newarray <illegal type>
    //   73: astore_1
    //   74: aload 5
    //   76: aload_1
    //   77: invokevirtual 167	java/io/InputStream:read	([B)I
    //   80: istore_3
    //   81: iload_3
    //   82: iconst_m1
    //   83: if_icmpeq +16 -> 99
    //   86: aload_0
    //   87: getfield 75	com/imcash/gp/third/faceid/MultipartEntity:mOutputStream	Ljava/io/ByteArrayOutputStream;
    //   90: aload_1
    //   91: iconst_0
    //   92: iload_3
    //   93: invokevirtual 170	java/io/ByteArrayOutputStream:write	([BII)V
    //   96: goto -22 -> 74
    //   99: aload_0
    //   100: getfield 75	com/imcash/gp/third/faceid/MultipartEntity:mOutputStream	Ljava/io/ByteArrayOutputStream;
    //   103: invokevirtual 173	java/io/ByteArrayOutputStream:flush	()V
    //   106: aload_0
    //   107: aload 5
    //   109: invokespecial 175	com/imcash/gp/third/faceid/MultipartEntity:closeSilently	(Ljava/io/Closeable;)V
    //   112: goto +38 -> 150
    //   115: astore_1
    //   116: goto +35 -> 151
    //   119: astore_2
    //   120: aload 5
    //   122: astore_1
    //   123: goto +15 -> 138
    //   126: astore_1
    //   127: aload 4
    //   129: astore 5
    //   131: goto +20 -> 151
    //   134: astore_2
    //   135: aload 6
    //   137: astore_1
    //   138: aload_1
    //   139: astore 4
    //   141: aload_2
    //   142: invokevirtual 91	java/io/IOException:printStackTrace	()V
    //   145: aload_0
    //   146: aload_1
    //   147: invokespecial 175	com/imcash/gp/third/faceid/MultipartEntity:closeSilently	(Ljava/io/Closeable;)V
    //   150: return
    //   151: aload_0
    //   152: aload 5
    //   154: invokespecial 175	com/imcash/gp/third/faceid/MultipartEntity:closeSilently	(Ljava/io/Closeable;)V
    //   157: aload_1
    //   158: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	159	0	this	MultipartEntity
    //   0	159	1	paramString	String
    //   0	159	2	paramFile	java.io.File
    //   80	13	3	i	int
    //   8	132	4	localObject1	Object
    //   13	140	5	localObject2	Object
    //   1	135	6	localObject3	Object
    //   4	12	7	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   25	74	115	finally
    //   74	81	115	finally
    //   86	96	115	finally
    //   99	106	115	finally
    //   25	74	119	java/io/IOException
    //   74	81	119	java/io/IOException
    //   86	96	119	java/io/IOException
    //   99	106	119	java/io/IOException
    //   10	15	126	finally
    //   19	25	126	finally
    //   141	145	126	finally
    //   10	15	134	java/io/IOException
    //   19	25	134	java/io/IOException
  }
  
  public void addStringPart(String paramString1, String paramString2)
  {
    writeToOutputStream(paramString1, paramString2.getBytes(), "text/plain; charset=UTF-8", this.BIT_ENCODING, "");
  }
  
  public void consumeContent()
    throws IOException, UnsupportedOperationException
  {
    if (!isStreaming()) {
      return;
    }
    throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
  }
  
  public InputStream getContent()
  {
    return new ByteArrayInputStream(this.mOutputStream.toByteArray());
  }
  
  public Header getContentEncoding()
  {
    return null;
  }
  
  public long getContentLength()
  {
    return this.mOutputStream.toByteArray().length;
  }
  
  public Header getContentType()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("multipart/form-data; boundary=");
    localStringBuilder.append(this.mBoundary);
    return new BasicHeader("Content-Type", localStringBuilder.toString());
  }
  
  public boolean isChunked()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("--");
    ((StringBuilder)localObject).append(this.mBoundary);
    ((StringBuilder)localObject).append("--\r\n");
    localObject = ((StringBuilder)localObject).toString();
    this.mOutputStream.write(((String)localObject).getBytes());
    paramOutputStream.write(this.mOutputStream.toByteArray());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/MultipartEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */