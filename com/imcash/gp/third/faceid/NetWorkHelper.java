package com.imcash.gp.third.faceid;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.provider.Settings.System;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class NetWorkHelper
{
  public static final int NETWORK_AIRPLANE = 3;
  public static final int NETWORK_CONNETED_GPRS = 2;
  public static final int NETWORK_CONNETED_WIFI = 1;
  public static final int NETWORK_DISCONNETED = 0;
  
  public static int checkNetWorkConnection(Context paramContext)
  {
    int i = isDataConnection(paramContext);
    if (i == 1) {
      return getNetworkType(paramContext);
    }
    if (i == 0) {
      return 0;
    }
    if (i == -1) {
      return 3;
    }
    return 0;
  }
  
  public static int getNetworkType(Context paramContext)
  {
    paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if (paramContext != null)
    {
      int i = paramContext.getType();
      paramContext = paramContext.getState();
      if ((i == 1) && (paramContext == NetworkInfo.State.CONNECTED)) {
        return 1;
      }
      if ((i == 0) && (paramContext == NetworkInfo.State.CONNECTED)) {
        return 2;
      }
    }
    return 0;
  }
  
  public static int isDataConnection(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    paramContext = Settings.System.getString(paramContext.getContentResolver(), "airplane_mode_on");
    if ((paramContext != null) && (paramContext.equalsIgnoreCase("1"))) {
      return -1;
    }
    if ((localNetworkInfo != null) && (localNetworkInfo.isConnected())) {
      return 1;
    }
    return 0;
  }
  
  public static boolean isNetWorkCooection(Context paramContext)
  {
    return checkNetWorkConnection(paramContext) != 0;
  }
  
  public static boolean isNetworkAvailable(Context paramContext)
  {
    paramContext = (ConnectivityManager)paramContext.getSystemService("connectivity");
    return (paramContext.getActiveNetworkInfo() != null) && (paramContext.getActiveNetworkInfo().isAvailable());
  }
  
  public static final String makeEncodeURL(String paramString)
  {
    Object localObject = paramString;
    String str;
    if (paramString != null)
    {
      localObject = paramString;
      if (paramString.startsWith("http://"))
      {
        localObject = paramString;
        if (paramString.endsWith(".png"))
        {
          int i = paramString.lastIndexOf('/') + 1;
          try
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append(paramString.substring(0, i));
            ((StringBuilder)localObject).append(URLEncoder.encode(paramString.substring(i), "utf-8"));
            localObject = ((StringBuilder)localObject).toString();
          }
          catch (UnsupportedEncodingException localUnsupportedEncodingException)
          {
            localUnsupportedEncodingException.printStackTrace();
            str = paramString;
          }
        }
      }
    }
    return str;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/NetWorkHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */