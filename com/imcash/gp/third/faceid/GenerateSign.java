package com.imcash.gp.third.faceid;

import android.util.Base64;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class GenerateSign
{
  public static String Base64Encode(byte[] paramArrayOfByte)
  {
    return Base64.encodeToString(paramArrayOfByte, 0);
  }
  
  public static byte[] HmacSha1(String paramString1, String paramString2)
    throws Exception
  {
    return HmacSha1(paramString1.getBytes(), paramString2);
  }
  
  public static byte[] HmacSha1(byte[] paramArrayOfByte, String paramString)
    throws Exception
  {
    Mac localMac = Mac.getInstance("HmacSHA1");
    localMac.init(new SecretKeySpec(paramString.getBytes(), "HmacSHA1"));
    return localMac.doFinal(paramArrayOfByte);
  }
  
  public static String appSign(String paramString1, String paramString2, long paramLong1, long paramLong2)
  {
    try
    {
      Object localObject = new java/util/Random;
      ((Random)localObject).<init>();
      paramString1 = String.format("a=%s&b=%d&c=%d&d=%d", new Object[] { paramString1, Long.valueOf(paramLong2), Long.valueOf(paramLong1), Integer.valueOf(Math.abs(((Random)localObject).nextInt())) });
      paramString2 = HmacSha1(paramString1, paramString2);
      localObject = new byte[paramString2.length + paramString1.getBytes().length];
      System.arraycopy(paramString2, 0, localObject, 0, paramString2.length);
      System.arraycopy(paramString1.getBytes(), 0, localObject, paramString2.length, paramString1.getBytes().length);
      paramString1 = Base64Encode((byte[])localObject);
      return paramString1;
    }
    catch (Exception paramString1)
    {
      paramString1.printStackTrace();
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/GenerateSign.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */