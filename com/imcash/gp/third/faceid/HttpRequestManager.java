package com.imcash.gp.third.faceid;

import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;

public class HttpRequestManager
{
  public static final String URL_GET_BIZTOKEN = "https://openapi.faceid.com/face/v1.2/sdk/get_biz_token";
  private static HttpRequestManager instance;
  
  public static HttpRequestManager getInstance()
  {
    if (instance == null) {
      instance = new HttpRequestManager();
    }
    return instance;
  }
  
  private void sendGetRequest(Context paramContext, String paramString, final Map<String, String> paramMap, final HttpRequestCallBack paramHttpRequestCallBack)
  {
    paramString = new StringRequest(paramString, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(String paramAnonymousString)
      {
        paramHttpRequestCallBack.onSuccess(paramAnonymousString);
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError == null) {
          paramHttpRequestCallBack.onFailure(-1, "timeout exception".getBytes());
        } else if (paramAnonymousVolleyError.networkResponse == null) {
          paramHttpRequestCallBack.onFailure(-1, "timeout exception".getBytes());
        } else {
          paramHttpRequestCallBack.onFailure(paramAnonymousVolleyError.networkResponse.statusCode, paramAnonymousVolleyError.networkResponse.data);
        }
      }
    })
    {
      public Map<String, String> getHeaders()
        throws AuthFailureError
      {
        return paramMap;
      }
    };
    VolleyHelper.getInstance(paramContext).addToRequestQueue(paramString);
  }
  
  private void sendMultipartRequest(Context paramContext, String paramString, MultipartEntity paramMultipartEntity, final Map<String, String> paramMap, final HttpRequestCallBack paramHttpRequestCallBack)
  {
    paramString = new MultipartRequest(paramString, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(String paramAnonymousString)
      {
        paramHttpRequestCallBack.onSuccess(paramAnonymousString);
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError == null) {
          paramHttpRequestCallBack.onFailure(-1, "timeout exception".getBytes());
        } else if (paramAnonymousVolleyError.networkResponse == null) {
          paramHttpRequestCallBack.onFailure(-1, "timeout exception".getBytes());
        } else {
          paramHttpRequestCallBack.onFailure(paramAnonymousVolleyError.networkResponse.statusCode, paramAnonymousVolleyError.networkResponse.data);
        }
      }
    })
    {
      public Map<String, String> getHeaders()
        throws AuthFailureError
      {
        return paramMap;
      }
    };
    paramString.setmMultiPartEntity(paramMultipartEntity);
    VolleyHelper.getInstance(paramContext).addToRequestQueue(paramString);
  }
  
  private void sendPostRequest(Context paramContext, String paramString, final Map<String, String> paramMap1, final Map<String, String> paramMap2, final HttpRequestCallBack paramHttpRequestCallBack)
  {
    paramString = new StringRequest(1, paramString, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(String paramAnonymousString)
      {
        HttpRequestCallBack localHttpRequestCallBack = paramHttpRequestCallBack;
        if (localHttpRequestCallBack != null) {
          localHttpRequestCallBack.onSuccess(paramAnonymousString);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError == null)
        {
          paramAnonymousVolleyError = paramHttpRequestCallBack;
          if (paramAnonymousVolleyError != null) {
            paramAnonymousVolleyError.onFailure(-1, "timeout exception".getBytes());
          }
        }
        else if (paramAnonymousVolleyError.networkResponse == null)
        {
          paramAnonymousVolleyError = paramHttpRequestCallBack;
          if (paramAnonymousVolleyError != null) {
            paramAnonymousVolleyError.onFailure(-1, "timeout exception".getBytes());
          }
        }
        else
        {
          HttpRequestCallBack localHttpRequestCallBack = paramHttpRequestCallBack;
          if (localHttpRequestCallBack != null) {
            localHttpRequestCallBack.onFailure(paramAnonymousVolleyError.networkResponse.statusCode, paramAnonymousVolleyError.networkResponse.data);
          }
        }
      }
    })
    {
      public Map<String, String> getHeaders()
        throws AuthFailureError
      {
        return paramMap2;
      }
      
      protected Map<String, String> getParams()
        throws AuthFailureError
      {
        return paramMap1;
      }
    };
    VolleyHelper.getInstance(paramContext).addToRequestQueue(paramString);
  }
  
  public void getBizToken(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, String paramString5, String paramString6, String paramString7, byte[] paramArrayOfByte, HttpRequestCallBack paramHttpRequestCallBack)
  {
    MultipartEntity localMultipartEntity = new MultipartEntity();
    localMultipartEntity.addStringPart("sign", paramString2);
    localMultipartEntity.addStringPart("sign_version", paramString3);
    localMultipartEntity.addStringPart("liveness_type", paramString4);
    paramString2 = new StringBuilder();
    paramString2.append("");
    paramString2.append(paramInt);
    localMultipartEntity.addStringPart("comparison_type", paramString2.toString());
    if (paramInt == 1)
    {
      localMultipartEntity.addStringPart("idcard_name", paramString5);
      localMultipartEntity.addStringPart("idcard_number", paramString6);
    }
    else if (paramInt == 0)
    {
      localMultipartEntity.addStringPart("uuid", paramString7);
      localMultipartEntity.addBinaryPart("image_ref1", paramArrayOfByte);
    }
    localMultipartEntity.addStringPart("verbose", "1");
    sendMultipartRequest(paramContext, paramString1, localMultipartEntity, new HashMap(), paramHttpRequestCallBack);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/HttpRequestManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */