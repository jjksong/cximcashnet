package com.imcash.gp.third.faceid;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;

public class MultipartRequest
  extends Request<String>
{
  private Map<String, String> mHeaders = new HashMap();
  private final Response.Listener<String> mListener;
  private MultipartEntity mMultiPartEntity;
  
  public MultipartRequest(String paramString, Response.Listener<String> paramListener)
  {
    this(paramString, paramListener, null);
  }
  
  public MultipartRequest(String paramString, Response.Listener<String> paramListener, Response.ErrorListener paramErrorListener)
  {
    super(1, paramString, paramErrorListener);
    this.mListener = paramListener;
  }
  
  public void addHeader(String paramString1, String paramString2)
  {
    this.mHeaders.put(paramString1, paramString2);
  }
  
  protected void deliverResponse(String paramString)
  {
    Response.Listener localListener = this.mListener;
    if (localListener != null) {
      localListener.onResponse(paramString);
    }
  }
  
  public byte[] getBody()
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      this.mMultiPartEntity.writeTo(localByteArrayOutputStream);
    }
    catch (IOException localIOException)
    {
      Log.e("", "IOException writing to ByteArrayOutputStream");
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  public String getBodyContentType()
  {
    return this.mMultiPartEntity.getContentType().getValue();
  }
  
  public Map<String, String> getHeaders()
    throws AuthFailureError
  {
    return this.mHeaders;
  }
  
  public MultipartEntity getMultiPartEntity()
  {
    if (this.mMultiPartEntity == null) {
      this.mMultiPartEntity = new MultipartEntity();
    }
    return this.mMultiPartEntity;
  }
  
  protected Response<String> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    String str2;
    try
    {
      String str1 = new java/lang/String;
      str1.<init>(paramNetworkResponse.data, HttpHeaderParser.parseCharset(paramNetworkResponse.headers));
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str2 = new String(paramNetworkResponse.data);
    }
    return Response.success(str2, HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
  }
  
  public void setmMultiPartEntity(MultipartEntity paramMultipartEntity)
  {
    this.mMultiPartEntity = paramMultipartEntity;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/MultipartRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */