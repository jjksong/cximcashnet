package com.imcash.gp.third.faceid;

import android.content.Context;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyHelper
{
  private static Context mCtx;
  private static VolleyHelper mInstance;
  private RequestQueue mRequestQueue;
  
  private VolleyHelper(Context paramContext)
  {
    mCtx = paramContext;
    this.mRequestQueue = getRequestQueue();
  }
  
  public static VolleyHelper getInstance(Context paramContext)
  {
    try
    {
      if (mInstance == null)
      {
        VolleyHelper localVolleyHelper = new com/imcash/gp/third/faceid/VolleyHelper;
        localVolleyHelper.<init>(paramContext);
        mInstance = localVolleyHelper;
      }
      paramContext = mInstance;
      return paramContext;
    }
    finally {}
  }
  
  public <T> boolean addToRequestQueue(Request<T> paramRequest)
  {
    if (getRequestQueue() == null) {
      return false;
    }
    paramRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, 1.0F));
    getRequestQueue().add(paramRequest);
    return true;
  }
  
  public void clearRequestQueue()
  {
    if (getRequestQueue() != null) {
      getRequestQueue().cancelAll(mCtx.getApplicationContext());
    }
  }
  
  public RequestQueue getRequestQueue()
  {
    if (this.mRequestQueue == null)
    {
      Context localContext = mCtx;
      if (localContext == null) {
        return null;
      }
      this.mRequestQueue = Volley.newRequestQueue(localContext.getApplicationContext());
    }
    return this.mRequestQueue;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/VolleyHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */