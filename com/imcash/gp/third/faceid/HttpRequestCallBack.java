package com.imcash.gp.third.faceid;

public abstract interface HttpRequestCallBack
{
  public abstract void onFailure(int paramInt, byte[] paramArrayOfByte);
  
  public abstract void onSuccess(String paramString);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/third/faceid/HttpRequestCallBack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */