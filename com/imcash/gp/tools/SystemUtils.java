package com.imcash.gp.tools;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.util.Log;
import java.util.List;

public class SystemUtils
{
  public static boolean isAppAlive(Context paramContext, String paramString)
  {
    paramContext = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    for (int i = 0; i < paramContext.size(); i++) {
      if (((ActivityManager.RunningAppProcessInfo)paramContext.get(i)).processName.equals(paramString))
      {
        Log.i("NotificationLaunch", String.format("the %s is running, isAppAlive return true", new Object[] { paramString }));
        return true;
      }
    }
    Log.i("NotificationLaunch", String.format("the %s is not running, isAppAlive return false", new Object[] { paramString }));
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/SystemUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */