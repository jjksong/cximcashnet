package com.imcash.gp.tools;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.imcash.gp.ui.base.ImcashApplication;

public class GetNetworkInfo
{
  public static final int NETWORKTYPE_2G = 2;
  public static final int NETWORKTYPE_3G = 3;
  public static final int NETWORKTYPE_INVALID = 0;
  public static final int NETWORKTYPE_WAP = 1;
  public static final int NETWORKTYPE_WIFI = 4;
  public static mConnectionReceiver connectionReceiver;
  private static ImcashApplication mApp;
  public static int mNetWorkType;
  
  public static void begin(Context paramContext, Application paramApplication)
  {
    mApp = (ImcashApplication)paramApplication;
    if (connectionReceiver == null) {
      connectionReceiver = new mConnectionReceiver(null);
    }
    paramApplication = new IntentFilter();
    paramApplication.addAction("android.net.conn.CONNECTIVITY_CHANGE");
    paramContext.registerReceiver(connectionReceiver, paramApplication);
  }
  
  public static String getNetStringType()
  {
    String str = "";
    switch (mNetWorkType)
    {
    default: 
      break;
    case 4: 
      str = "wifi";
      break;
    case 3: 
      str = "3G";
      break;
    case 2: 
      str = "2G";
      break;
    case 1: 
      str = "wap";
    }
    return str;
  }
  
  public static int getNetWorkType(Context paramContext)
  {
    try
    {
      Object localObject = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if ((localObject != null) && (((NetworkInfo)localObject).isConnected()))
      {
        localObject = ((NetworkInfo)localObject).getTypeName();
        if (((String)localObject).equalsIgnoreCase("WIFI"))
        {
          mNetWorkType = 4;
        }
        else if (((String)localObject).equalsIgnoreCase("MOBILE"))
        {
          int i;
          if (TextUtils.isEmpty(Proxy.getDefaultHost()))
          {
            if (isFastMobileNetwork(paramContext)) {
              i = 3;
            } else {
              i = 2;
            }
          }
          else {
            i = 1;
          }
          mNetWorkType = i;
        }
      }
      else
      {
        mNetWorkType = 0;
      }
    }
    catch (NullPointerException localNullPointerException)
    {
      paramContext = new StringBuilder();
      paramContext.append("null // ");
      paramContext.append(localNullPointerException);
      Log.e("GetNetworkInfo", paramContext.toString());
    }
    return mNetWorkType;
  }
  
  private static boolean isFastMobileNetwork(Context paramContext)
  {
    switch (((TelephonyManager)paramContext.getSystemService("phone")).getNetworkType())
    {
    default: 
      return false;
    case 15: 
      return true;
    case 14: 
      return true;
    case 13: 
      return true;
    case 12: 
      return true;
    case 11: 
      return false;
    case 10: 
      return true;
    case 9: 
      return true;
    case 8: 
      return true;
    case 7: 
      return false;
    case 6: 
      return true;
    case 5: 
      return true;
    case 4: 
      return false;
    case 3: 
      return true;
    case 2: 
      return false;
    case 1: 
      return false;
    }
    return false;
  }
  
  public static void stop(Context paramContext)
  {
    mConnectionReceiver localmConnectionReceiver = connectionReceiver;
    if (localmConnectionReceiver != null) {
      paramContext.unregisterReceiver(localmConnectionReceiver);
    }
  }
  
  private static class mConnectionReceiver
    extends BroadcastReceiver
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if ((paramContext != null) && (paramIntent != null))
      {
        try
        {
          Object localObject = (ConnectivityManager)paramContext.getSystemService("connectivity");
          paramIntent = ((ConnectivityManager)localObject).getNetworkInfo(0);
          localObject = ((ConnectivityManager)localObject).getNetworkInfo(1);
          if ((paramIntent != null) && (localObject != null))
          {
            if ((!paramIntent.isConnected()) && (!((NetworkInfo)localObject).isConnected()))
            {
              if (!GetNetworkInfo.mApp.isAppOnForeground()) {
                return;
              }
              GetNetworkInfo.mNetWorkType = 0;
            }
            else
            {
              GetNetworkInfo.getNetWorkType(paramContext);
            }
          }
          else {
            return;
          }
        }
        catch (NullPointerException paramContext)
        {
          paramIntent = new StringBuilder();
          paramIntent.append("null // ");
          paramIntent.append(paramContext);
          Log.e("ConnectionReceiver", paramIntent.toString());
        }
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/GetNetworkInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */