package com.imcash.gp.tools;

import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5
{
  private static final String ALGORITHM = "MD5";
  private static final String LOG_TAG = "MD5";
  private static MessageDigest sDigest;
  
  static
  {
    try
    {
      sDigest = MessageDigest.getInstance("MD5");
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      Log.e("MD5", "Get MD5 Digest failed.");
    }
  }
  
  public static final String encode(String paramString)
  {
    paramString = paramString.getBytes();
    return Utility.hexString(sDigest.digest(paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/MD5.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */