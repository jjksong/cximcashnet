package com.imcash.gp.tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.Base64;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class BitmapTools
{
  public static Bitmap ImageCompressL(Bitmap paramBitmap, double paramDouble)
  {
    paramDouble = Math.sqrt(paramDouble * 1000.0D);
    Object localObject;
    if (paramBitmap.getWidth() <= paramDouble)
    {
      localObject = paramBitmap;
      if (paramBitmap.getHeight() <= paramDouble) {}
    }
    else
    {
      localObject = new Matrix();
      double d1 = paramBitmap.getWidth();
      Double.isNaN(d1);
      double d2 = paramDouble / d1;
      d1 = paramBitmap.getHeight();
      Double.isNaN(d1);
      float f = (float)Math.max(d2, paramDouble / d1);
      ((Matrix)localObject).postScale(f, f);
      localObject = Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), (Matrix)localObject, true);
    }
    return (Bitmap)localObject;
  }
  
  public static String bitmapToBase64(Bitmap paramBitmap)
  {
    if (paramBitmap != null) {
      paramBitmap = Base64.encodeToString(bitmapToBytes(paramBitmap), 0);
    } else {
      paramBitmap = null;
    }
    return paramBitmap;
  }
  
  public static String bitmapToBase64(Bitmap paramBitmap, int paramInt)
  {
    if (paramBitmap != null) {
      paramBitmap = Base64.encodeToString(bitmapToSize(paramBitmap, paramInt), 0);
    } else {
      paramBitmap = null;
    }
    return paramBitmap;
  }
  
  public static byte[] bitmapToBytes(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, localByteArrayOutputStream);
      paramBitmap = localByteArrayOutputStream.toByteArray();
    }
    else
    {
      paramBitmap = null;
    }
    return paramBitmap;
  }
  
  public static Drawable bitmapToDrawable(Bitmap paramBitmap)
  {
    return new BitmapDrawable(paramBitmap);
  }
  
  /* Error */
  public static byte[] bitmapToSize(Bitmap paramBitmap, int paramInt)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: ifnull +78 -> 81
    //   6: new 68	java/io/ByteArrayOutputStream
    //   9: astore_2
    //   10: aload_2
    //   11: invokespecial 69	java/io/ByteArrayOutputStream:<init>	()V
    //   14: aload_0
    //   15: getstatic 95	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   18: iload_1
    //   19: aload_2
    //   20: invokevirtual 79	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   23: pop
    //   24: aload_2
    //   25: invokevirtual 83	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   28: astore_0
    //   29: aload_2
    //   30: invokevirtual 98	java/io/ByteArrayOutputStream:flush	()V
    //   33: aload_2
    //   34: invokevirtual 101	java/io/ByteArrayOutputStream:close	()V
    //   37: aload_0
    //   38: astore_2
    //   39: goto +42 -> 81
    //   42: astore_2
    //   43: aload_2
    //   44: invokevirtual 104	java/io/IOException:printStackTrace	()V
    //   47: aload_0
    //   48: astore_2
    //   49: goto +32 -> 81
    //   52: astore_0
    //   53: goto +6 -> 59
    //   56: astore_0
    //   57: aconst_null
    //   58: astore_2
    //   59: aload_2
    //   60: ifnull +19 -> 79
    //   63: aload_2
    //   64: invokevirtual 98	java/io/ByteArrayOutputStream:flush	()V
    //   67: aload_2
    //   68: invokevirtual 101	java/io/ByteArrayOutputStream:close	()V
    //   71: goto +8 -> 79
    //   74: astore_2
    //   75: aload_2
    //   76: invokevirtual 104	java/io/IOException:printStackTrace	()V
    //   79: aload_0
    //   80: athrow
    //   81: aload_2
    //   82: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	paramBitmap	Bitmap
    //   0	83	1	paramInt	int
    //   1	38	2	localObject	Object
    //   42	2	2	localIOException1	IOException
    //   48	20	2	localBitmap	Bitmap
    //   74	8	2	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   29	37	42	java/io/IOException
    //   14	29	52	finally
    //   6	14	56	finally
    //   63	71	74	java/io/IOException
  }
  
  public static Bitmap byteToBitmap(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte.length != 0) {
      return BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
    }
    return null;
  }
  
  public static Drawable byteToDrawable(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null) {
      paramArrayOfByte = new ByteArrayInputStream(paramArrayOfByte);
    } else {
      paramArrayOfByte = null;
    }
    return Drawable.createFromStream(paramArrayOfByte, null);
  }
  
  public static int calculateInSampleSize(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    int j = paramBitmap.getHeight();
    int i = paramBitmap.getWidth();
    if ((j <= paramInt2) && (i <= paramInt1))
    {
      paramInt1 = 1;
    }
    else
    {
      paramInt2 = Math.round(j / paramInt2);
      i = Math.round(i / paramInt1);
      paramInt1 = i;
      if (paramInt2 < i) {
        paramInt1 = paramInt2;
      }
    }
    return paramInt1;
  }
  
  public static int calculateInSampleSize(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
  {
    int j = paramOptions.outHeight;
    int i = paramOptions.outWidth;
    if ((j <= paramInt2) && (i <= paramInt1))
    {
      paramInt1 = 1;
    }
    else
    {
      paramInt2 = Math.round(j / paramInt2);
      i = Math.round(i / paramInt1);
      paramInt1 = i;
      if (paramInt2 < i) {
        paramInt1 = paramInt2;
      }
    }
    return paramInt1;
  }
  
  public static boolean checkSDCardAvailable()
  {
    return Environment.getExternalStorageState().equals("mounted");
  }
  
  public static int commpressScale(int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2)
    {
      paramInt1 = 100;
    }
    else
    {
      paramInt2 = (int)(paramInt2 / paramInt1 * 100.0F);
      paramInt1 = paramInt2;
      if (paramInt2 == 0) {
        paramInt1 = 1;
      }
    }
    return paramInt1;
  }
  
  public static Bitmap compressImage(Bitmap paramBitmap, int paramInt)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localByteArrayOutputStream);
    for (int i = 90; localByteArrayOutputStream.toByteArray().length / 1024 > paramInt; i -= 10)
    {
      localByteArrayOutputStream.reset();
      paramBitmap.compress(Bitmap.CompressFormat.JPEG, i, localByteArrayOutputStream);
    }
    return BitmapFactory.decodeStream(new ByteArrayInputStream(localByteArrayOutputStream.toByteArray()), null, null);
  }
  
  public static Bitmap createReflectionImageWithOrigin(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int k = paramBitmap.getHeight();
    Object localObject1 = new Matrix();
    ((Matrix)localObject1).preScale(1.0F, -1.0F);
    int j = k / 2;
    Object localObject2 = Bitmap.createBitmap(paramBitmap, 0, j, i, j, (Matrix)localObject1, false);
    Bitmap localBitmap = Bitmap.createBitmap(i, j + k, Bitmap.Config.ARGB_8888);
    localObject1 = new Canvas(localBitmap);
    ((Canvas)localObject1).drawBitmap(paramBitmap, 0.0F, 0.0F, null);
    Paint localPaint = new Paint();
    float f1 = k;
    float f2 = i;
    float f3 = k + 4;
    ((Canvas)localObject1).drawRect(0.0F, f1, f2, f3, localPaint);
    ((Canvas)localObject1).drawBitmap((Bitmap)localObject2, 0.0F, f3, null);
    localObject2 = new Paint();
    ((Paint)localObject2).setShader(new LinearGradient(0.0F, paramBitmap.getHeight(), 0.0F, localBitmap.getHeight() + 4, 1895825407, 16777215, Shader.TileMode.CLAMP));
    ((Paint)localObject2).setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
    ((Canvas)localObject1).drawRect(0.0F, f1, f2, localBitmap.getHeight() + 4, (Paint)localObject2);
    return localBitmap;
  }
  
  public static Bitmap cutterBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    float f1 = paramBitmap.getWidth();
    float f2 = paramBitmap.getHeight();
    if (paramInt1 / paramInt2 > f1 / f2) {
      f2 = paramInt2 / (paramInt1 / f1);
    } else {
      f1 = paramInt1 / (paramInt2 / f2);
    }
    Bitmap localBitmap = Bitmap.createBitmap((int)f1, (int)f2, Bitmap.Config.RGB_565);
    new Canvas(localBitmap).drawBitmap(paramBitmap, 0.0F, 0.0F, new Paint());
    return localBitmap;
  }
  
  public static Bitmap decodeSampledBitmap(InputStream paramInputStream, int paramInt1, int paramInt2)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inPreferredConfig = Bitmap.Config.RGB_565;
    localOptions.inJustDecodeBounds = true;
    localOptions.inPurgeable = true;
    localOptions.inInputShareable = true;
    BitmapFactory.decodeStream(paramInputStream, null, localOptions);
    localOptions.inSampleSize = calculateInSampleSize(localOptions, paramInt1, paramInt2);
    localOptions.inJustDecodeBounds = false;
    return BitmapFactory.decodeStream(paramInputStream, null, localOptions);
  }
  
  public static void deleteAllPhoto(String paramString)
  {
    if (checkSDCardAvailable())
    {
      paramString = new File(paramString).listFiles();
      for (int i = 0; i < paramString.length; i++) {
        paramString[i].delete();
      }
    }
  }
  
  public static void deletePhotoAtPathAndName(String paramString1, String paramString2)
  {
    if (checkSDCardAvailable())
    {
      paramString1 = new File(paramString1).listFiles();
      for (int i = 0; i < paramString1.length; i++) {
        if (paramString1[i].getName().split("\\.")[0].equals(paramString2)) {
          paramString1[i].delete();
        }
      }
    }
  }
  
  public static Bitmap drawableToBitmap(Drawable paramDrawable)
  {
    int j = paramDrawable.getIntrinsicWidth();
    int i = paramDrawable.getIntrinsicHeight();
    if (paramDrawable.getOpacity() != -1) {
      localObject = Bitmap.Config.ARGB_8888;
    } else {
      localObject = Bitmap.Config.RGB_565;
    }
    Bitmap localBitmap = Bitmap.createBitmap(j, i, (Bitmap.Config)localObject);
    Object localObject = new Canvas(localBitmap);
    paramDrawable.setBounds(0, 0, j, i);
    paramDrawable.draw((Canvas)localObject);
    return localBitmap;
  }
  
  public static byte[] drawableToBytes(Drawable paramDrawable)
  {
    return bitmapToBytes(((BitmapDrawable)paramDrawable).getBitmap());
  }
  
  public static boolean findPhotoFromSDCard(String paramString1, String paramString2)
  {
    boolean bool3 = checkSDCardAvailable();
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (bool3)
    {
      bool1 = bool2;
      if (new File(paramString1).exists())
      {
        paramString1 = new File(paramString1).listFiles();
        int i = 0;
        bool1 = false;
        while (i < paramString1.length)
        {
          if (paramString1[i].getName().split("\\.")[0].equals(paramString2)) {
            bool1 = true;
          }
          i++;
        }
      }
    }
    return bool1;
  }
  
  @SuppressLint({"NewApi"})
  public static final int getBitmapByteSize(Bitmap paramBitmap)
  {
    if (Build.VERSION.SDK_INT >= 19) {
      return paramBitmap.getAllocationByteCount();
    }
    return paramBitmap.getRowBytes() * paramBitmap.getHeight();
  }
  
  public static Bitmap getPhotoFromSDCard(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append("/");
    localStringBuilder.append(paramString2);
    localStringBuilder.append(".png");
    paramString1 = BitmapFactory.decodeFile(localStringBuilder.toString());
    if (paramString1 == null) {
      return null;
    }
    return paramString1;
  }
  
  public static Bitmap getRoundedCornerBitmap(Bitmap paramBitmap, float paramFloat)
  {
    int j = paramBitmap.getWidth();
    int i = paramBitmap.getHeight();
    Bitmap localBitmap = Bitmap.createBitmap(j, i, Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    Rect localRect = new Rect(0, 0, j, i);
    RectF localRectF = new RectF(localRect);
    localPaint.setAntiAlias(true);
    localCanvas.drawARGB(0, 0, 0, 0);
    localPaint.setColor(-12434878);
    localCanvas.drawRoundRect(localRectF, paramFloat, paramFloat, localPaint);
    localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    localCanvas.drawBitmap(paramBitmap, localRect, localRect, localPaint);
    return localBitmap;
  }
  
  public static Bitmap inputStreamToBitmap(InputStream paramInputStream)
    throws Exception
  {
    return BitmapFactory.decodeStream(paramInputStream);
  }
  
  public static int readPictureDegree(String paramString)
  {
    int i = 0;
    try
    {
      ExifInterface localExifInterface = new android/media/ExifInterface;
      localExifInterface.<init>(paramString);
      int j = localExifInterface.getAttributeInt("Orientation", 1);
      if (j != 3)
      {
        if (j != 6)
        {
          if (j == 8) {
            i = 270;
          }
        }
        else {
          i = 90;
        }
      }
      else {
        i = 180;
      }
    }
    catch (IOException paramString)
    {
      paramString.printStackTrace();
    }
    return i;
  }
  
  public static Bitmap revitionImageSize(String paramString)
    throws IOException
  {
    BufferedInputStream localBufferedInputStream = new BufferedInputStream(new FileInputStream(new File(paramString)));
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeStream(localBufferedInputStream, null, localOptions);
    localBufferedInputStream.close();
    for (int i = 0;; i++) {
      if ((localOptions.outWidth >> i <= 1000) && (localOptions.outHeight >> i <= 1000))
      {
        paramString = new BufferedInputStream(new FileInputStream(new File(paramString)));
        localOptions.inSampleSize = ((int)Math.pow(2.0D, i));
        localOptions.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(paramString, null, localOptions);
      }
    }
  }
  
  public static Bitmap revitionImageSize(String paramString, int paramInt)
    throws IOException
  {
    Bitmap localBitmap = revitionImageSize(paramString);
    double d2 = paramInt;
    paramString = new ByteArrayOutputStream();
    localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, paramString);
    double d1 = paramString.toByteArray().length / 1024;
    paramString = localBitmap;
    if (d1 > d2)
    {
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 /= d2;
      d2 = localBitmap.getWidth();
      double d3 = Math.sqrt(d1);
      Double.isNaN(d2);
      d2 /= d3;
      d3 = localBitmap.getHeight();
      d1 = Math.sqrt(d1);
      Double.isNaN(d3);
      paramString = zoomImage(localBitmap, d2, d3 / d1);
    }
    return paramString;
  }
  
  public static Bitmap rotaingImageView(int paramInt, Bitmap paramBitmap)
  {
    Matrix localMatrix = new Matrix();
    localMatrix.postRotate(paramInt);
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, true);
  }
  
  /* Error */
  public static File savePhotoToSDCard(Bitmap paramBitmap, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: invokestatic 260	com/imcash/gp/tools/BitmapTools:checkSDCardAvailable	()Z
    //   3: istore_3
    //   4: aconst_null
    //   5: astore 5
    //   7: aconst_null
    //   8: astore 6
    //   10: aconst_null
    //   11: astore 7
    //   13: iload_3
    //   14: ifeq +304 -> 318
    //   17: new 333	java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial 334	java/lang/StringBuilder:<init>	()V
    //   24: astore 4
    //   26: aload 4
    //   28: invokestatic 438	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   31: invokevirtual 441	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 4
    //   37: ldc_w 340
    //   40: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload 4
    //   46: aload_1
    //   47: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: new 262	java/io/File
    //   54: dup
    //   55: aload 4
    //   57: invokevirtual 345	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokespecial 264	java/io/File:<init>	(Ljava/lang/String;)V
    //   63: astore 4
    //   65: aload 4
    //   67: invokevirtual 312	java/io/File:exists	()Z
    //   70: ifne +9 -> 79
    //   73: aload 4
    //   75: invokevirtual 444	java/io/File:mkdirs	()Z
    //   78: pop
    //   79: new 333	java/lang/StringBuilder
    //   82: dup
    //   83: invokespecial 334	java/lang/StringBuilder:<init>	()V
    //   86: astore 4
    //   88: aload 4
    //   90: invokestatic 438	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   93: invokevirtual 441	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload 4
    //   99: ldc_w 340
    //   102: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload 4
    //   108: aload_1
    //   109: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload 4
    //   115: invokevirtual 345	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   118: astore 4
    //   120: new 333	java/lang/StringBuilder
    //   123: dup
    //   124: invokespecial 334	java/lang/StringBuilder:<init>	()V
    //   127: astore_1
    //   128: aload_1
    //   129: aload_2
    //   130: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload_1
    //   135: ldc_w 342
    //   138: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   141: pop
    //   142: new 262	java/io/File
    //   145: dup
    //   146: aload 4
    //   148: aload_1
    //   149: invokevirtual 345	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   152: invokespecial 446	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   155: astore 4
    //   157: aload 7
    //   159: astore_1
    //   160: new 448	java/io/FileOutputStream
    //   163: astore_2
    //   164: aload 7
    //   166: astore_1
    //   167: aload_2
    //   168: aload 4
    //   170: invokespecial 449	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   173: aload_0
    //   174: ifnull +49 -> 223
    //   177: aload_0
    //   178: getstatic 75	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   181: bipush 100
    //   183: aload_2
    //   184: invokevirtual 79	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   187: ifeq +36 -> 223
    //   190: aload_2
    //   191: invokevirtual 450	java/io/FileOutputStream:flush	()V
    //   194: aload_2
    //   195: invokevirtual 451	java/io/FileOutputStream:close	()V
    //   198: goto +25 -> 223
    //   201: astore_0
    //   202: aload_2
    //   203: astore_1
    //   204: goto +100 -> 304
    //   207: astore_1
    //   208: aload_2
    //   209: astore_0
    //   210: aload_1
    //   211: astore_2
    //   212: goto +40 -> 252
    //   215: astore_1
    //   216: aload_2
    //   217: astore_0
    //   218: aload_1
    //   219: astore_2
    //   220: goto +60 -> 280
    //   223: aload_2
    //   224: invokevirtual 451	java/io/FileOutputStream:close	()V
    //   227: aload 4
    //   229: astore_0
    //   230: goto +90 -> 320
    //   233: astore_0
    //   234: aload_0
    //   235: invokevirtual 104	java/io/IOException:printStackTrace	()V
    //   238: aload 4
    //   240: astore_0
    //   241: goto +79 -> 320
    //   244: astore_0
    //   245: goto +59 -> 304
    //   248: astore_2
    //   249: aload 5
    //   251: astore_0
    //   252: aload_0
    //   253: astore_1
    //   254: aload 4
    //   256: invokevirtual 271	java/io/File:delete	()Z
    //   259: pop
    //   260: aload_0
    //   261: astore_1
    //   262: aload_2
    //   263: invokevirtual 104	java/io/IOException:printStackTrace	()V
    //   266: aload_0
    //   267: invokevirtual 451	java/io/FileOutputStream:close	()V
    //   270: aload 4
    //   272: astore_0
    //   273: goto +47 -> 320
    //   276: astore_2
    //   277: aload 6
    //   279: astore_0
    //   280: aload_0
    //   281: astore_1
    //   282: aload 4
    //   284: invokevirtual 271	java/io/File:delete	()Z
    //   287: pop
    //   288: aload_0
    //   289: astore_1
    //   290: aload_2
    //   291: invokevirtual 452	java/io/FileNotFoundException:printStackTrace	()V
    //   294: aload_0
    //   295: invokevirtual 451	java/io/FileOutputStream:close	()V
    //   298: aload 4
    //   300: astore_0
    //   301: goto +19 -> 320
    //   304: aload_1
    //   305: invokevirtual 451	java/io/FileOutputStream:close	()V
    //   308: goto +8 -> 316
    //   311: astore_1
    //   312: aload_1
    //   313: invokevirtual 104	java/io/IOException:printStackTrace	()V
    //   316: aload_0
    //   317: athrow
    //   318: aconst_null
    //   319: astore_0
    //   320: aload_0
    //   321: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	322	0	paramBitmap	Bitmap
    //   0	322	1	paramString1	String
    //   0	322	2	paramString2	String
    //   3	11	3	bool	boolean
    //   24	275	4	localObject1	Object
    //   5	245	5	localObject2	Object
    //   8	270	6	localObject3	Object
    //   11	154	7	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   177	198	201	finally
    //   177	198	207	java/io/IOException
    //   177	198	215	java/io/FileNotFoundException
    //   223	227	233	java/io/IOException
    //   266	270	233	java/io/IOException
    //   294	298	233	java/io/IOException
    //   160	164	244	finally
    //   167	173	244	finally
    //   254	260	244	finally
    //   262	266	244	finally
    //   282	288	244	finally
    //   290	294	244	finally
    //   160	164	248	java/io/IOException
    //   167	173	248	java/io/IOException
    //   160	164	276	java/io/FileNotFoundException
    //   167	173	276	java/io/FileNotFoundException
    //   304	308	311	java/io/IOException
  }
  
  public static Bitmap stringtoBitmap(String paramString)
  {
    try
    {
      paramString = Base64.decode(paramString, 0);
      paramString = BitmapFactory.decodeByteArray(paramString, 0, paramString.length);
    }
    catch (Exception paramString)
    {
      paramString.printStackTrace();
      paramString = null;
    }
    return paramString;
  }
  
  public static File uri2File(Uri paramUri, Activity paramActivity)
  {
    paramActivity = paramActivity.managedQuery(paramUri, new String[] { "_data" }, null, null, null);
    if (paramActivity == null)
    {
      paramActivity = paramUri.getPath();
    }
    else
    {
      int i = paramActivity.getColumnIndexOrThrow("_data");
      paramActivity.moveToFirst();
      paramActivity = paramActivity.getString(i);
    }
    paramActivity = new File(paramActivity);
    paramUri.getPath();
    return paramActivity;
  }
  
  public static Bitmap zoomBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    if (paramBitmap == null) {
      return null;
    }
    int j = paramBitmap.getWidth();
    int i = paramBitmap.getHeight();
    Matrix localMatrix = new Matrix();
    float f2 = paramInt1 / j;
    float f3 = paramInt2 / i;
    float f1 = f2;
    if (f2 < 0.01F) {
      f1 = 0.01F;
    }
    f2 = f3;
    if (f3 < 0.01F) {
      f2 = 0.01F;
    }
    localMatrix.postScale(f1, f2);
    if (j <= 0) {
      paramInt1 = 150;
    } else {
      paramInt1 = j;
    }
    if (i <= 0) {
      paramInt2 = 150;
    } else {
      paramInt2 = i;
    }
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramInt1, paramInt2, localMatrix, true);
  }
  
  public static Drawable zoomDrawable(Drawable paramDrawable, int paramInt1, int paramInt2)
  {
    if (paramDrawable == null) {
      return null;
    }
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    paramDrawable = drawableToBitmap(paramDrawable);
    Matrix localMatrix = new Matrix();
    localMatrix.postScale(paramInt1 / i, paramInt2 / j);
    return new BitmapDrawable(Bitmap.createBitmap(paramDrawable, 0, 0, i, j, localMatrix, true));
  }
  
  public static Bitmap zoomImage(Bitmap paramBitmap, double paramDouble1, double paramDouble2)
  {
    float f1 = paramBitmap.getWidth();
    float f2 = paramBitmap.getHeight();
    Matrix localMatrix = new Matrix();
    localMatrix.postScale((float)paramDouble1 / f1, (float)paramDouble2 / f2);
    return Bitmap.createBitmap(paramBitmap, 0, 0, (int)f1, (int)f2, localMatrix, true);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/BitmapTools.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */