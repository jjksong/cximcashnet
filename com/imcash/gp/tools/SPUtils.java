package com.imcash.gp.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.content.SharedPreferencesCompat.EditorCompat;
import java.util.Map;

public class SPUtils
{
  public static String FILLNAME = "config";
  
  public static void clear(Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences(FILLNAME, 0).edit();
    paramContext.clear();
    SharedPreferencesCompat.EditorCompat.getInstance().apply(paramContext);
  }
  
  public static Object get(Context paramContext, String paramString, Object paramObject)
  {
    paramContext = paramContext.getSharedPreferences(FILLNAME, 0);
    if ((paramObject instanceof String)) {
      return paramContext.getString(paramString, (String)paramObject);
    }
    if ((paramObject instanceof Integer)) {
      return Integer.valueOf(paramContext.getInt(paramString, ((Integer)paramObject).intValue()));
    }
    if ((paramObject instanceof Boolean)) {
      return Boolean.valueOf(paramContext.getBoolean(paramString, ((Boolean)paramObject).booleanValue()));
    }
    if ((paramObject instanceof Float)) {
      return Float.valueOf(paramContext.getFloat(paramString, ((Float)paramObject).floatValue()));
    }
    if ((paramObject instanceof Long)) {
      return Long.valueOf(paramContext.getLong(paramString, ((Long)paramObject).longValue()));
    }
    return null;
  }
  
  public static Map<String, ?> getAll(Context paramContext)
  {
    return paramContext.getSharedPreferences(FILLNAME, 0).getAll();
  }
  
  public static void put(Context paramContext, String paramString, Object paramObject)
  {
    paramContext = paramContext.getSharedPreferences(FILLNAME, 0).edit();
    if ((paramObject instanceof String)) {
      paramContext.putString(paramString, (String)paramObject);
    } else if ((paramObject instanceof Integer)) {
      paramContext.putInt(paramString, ((Integer)paramObject).intValue());
    } else if ((paramObject instanceof Boolean)) {
      paramContext.putBoolean(paramString, ((Boolean)paramObject).booleanValue());
    } else if ((paramObject instanceof Float)) {
      paramContext.putFloat(paramString, ((Float)paramObject).floatValue());
    } else if ((paramObject instanceof Long)) {
      paramContext.putLong(paramString, ((Long)paramObject).longValue());
    }
    SharedPreferencesCompat.EditorCompat.getInstance().apply(paramContext);
  }
  
  public static void remove(Context paramContext, String paramString)
  {
    paramContext = paramContext.getSharedPreferences(FILLNAME, 0).edit();
    paramContext.remove(paramString);
    SharedPreferencesCompat.EditorCompat.getInstance().apply(paramContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/SPUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */