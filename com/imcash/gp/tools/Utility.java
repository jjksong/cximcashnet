package com.imcash.gp.tools;

import android.app.Activity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.security.SecureRandom;
import java.util.Date;

public class Utility
{
  private static final String LABEL_App_sign = "api_sign";
  private static final String LABEL_NONCE = "nonce";
  private static final String LABEL_TIME = "timestamp";
  private static final String LABEL_UID = "uid";
  private static final int MAX_NONCE = 10;
  private static char[] sHexDigits = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  private static final SecureRandom sRandom = new SecureRandom();
  
  public static String decodeBase64(String paramString)
  {
    return new String(Base64.decode(paramString, 0));
  }
  
  public static String encodeBase64(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramInt);
    localStringBuilder.append("");
    return encodeBase64(localStringBuilder.toString());
  }
  
  public static String encodeBase64(String paramString)
  {
    return Base64.encodeToString(paramString.getBytes(), 0);
  }
  
  public static String encodeBase64(byte[] paramArrayOfByte)
  {
    return Base64.encodeToString(paramArrayOfByte, 0);
  }
  
  private static String getAPIsig(String paramString1, long paramLong, String paramString2, String paramString3)
  {
    synchronized (new StringBuilder())
    {
      ???.append(paramString1);
      ???.append(paramLong);
      ???.append(paramString2);
      ???.append(paramString3);
      paramString1 = MD5.encode(???.toString());
      ???.delete(0, ???.length());
      return paramString1;
    }
  }
  
  private static String getNonce()
  {
    byte[] arrayOfByte = new byte[5];
    sRandom.nextBytes(arrayOfByte);
    return hexString(arrayOfByte);
  }
  
  public static String getParams(String paramString)
  {
    try
    {
      Object localObject = paramString.split(":");
      long l = getTimestamp();
      String str1 = getNonce();
      String str2 = getAPIsig(paramString, l, str1, localObject[1]);
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      try
      {
        paramString.append(String.format("&uid=%s", new Object[] { localObject[1] }));
        paramString.append(String.format("&nonce=%s", new Object[] { str1 }));
        paramString.append(String.format("&timestamp=%s", new Object[] { Long.valueOf(l) }));
        paramString.append(String.format("&api_sign=%s", new Object[] { str2 }));
        localObject = paramString.toString();
        paramString.delete(0, paramString.length());
        return (String)localObject;
      }
      finally {}
      return "";
    }
    catch (Exception paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  public static String getScreenParams(Activity paramActivity)
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("&screen=");
    int i;
    if (localDisplayMetrics.heightPixels > localDisplayMetrics.widthPixels)
    {
      paramActivity = new StringBuilder();
      paramActivity.append(localDisplayMetrics.widthPixels);
      paramActivity.append("*");
      i = localDisplayMetrics.heightPixels;
    }
    else
    {
      paramActivity = new StringBuilder();
      paramActivity.append(localDisplayMetrics.heightPixels);
      paramActivity.append("*");
      i = localDisplayMetrics.widthPixels;
    }
    paramActivity.append(i);
    localStringBuilder.append(paramActivity.toString());
    return localStringBuilder.toString();
  }
  
  private static long getTimestamp()
  {
    return new Date().getTime();
  }
  
  public static String hexString(byte[] paramArrayOfByte)
  {
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 0))
    {
      int k = paramArrayOfByte.length;
      char[] arrayOfChar1 = new char[k * 2];
      int j = 0;
      int i = 0;
      while (j < k)
      {
        int m = paramArrayOfByte[j];
        int n = i + 1;
        char[] arrayOfChar2 = sHexDigits;
        arrayOfChar1[i] = arrayOfChar2[(m >>> 4 & 0xF)];
        i = n + 1;
        arrayOfChar1[n] = arrayOfChar2[(m & 0xF)];
        j++;
      }
      return new String(arrayOfChar1);
    }
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/Utility.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */