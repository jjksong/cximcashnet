package com.imcash.gp.tools;

import java.util.Random;

public class RandomUntil
{
  public static String getLargeLetter()
  {
    return String.valueOf((char)(new Random().nextInt(26) + 65));
  }
  
  public static String getLargeLetter(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < paramInt; i++) {
      localStringBuffer.append((char)(localRandom.nextInt(26) + 65));
    }
    return localStringBuffer.toString();
  }
  
  public static int getNum(int paramInt)
  {
    if (paramInt > 0) {
      return new Random().nextInt(paramInt);
    }
    return 0;
  }
  
  public static int getNum(int paramInt1, int paramInt2)
  {
    if (paramInt2 > paramInt1) {
      return new Random().nextInt(paramInt2 - paramInt1) + paramInt1;
    }
    return 0;
  }
  
  public static String getNumLargeLetter(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < paramInt; i++) {
      if (localRandom.nextInt(2) % 2 == 0) {
        localStringBuffer.append((char)(localRandom.nextInt(26) + 65));
      } else {
        localStringBuffer.append(localRandom.nextInt(10));
      }
    }
    return localStringBuffer.toString();
  }
  
  public static String getNumLargeSmallLetter(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < paramInt; i++) {
      if (localRandom.nextInt(2) % 2 == 0)
      {
        if (localRandom.nextInt(2) % 2 == 0) {
          localStringBuffer.append((char)(localRandom.nextInt(26) + 65));
        } else {
          localStringBuffer.append((char)(localRandom.nextInt(26) + 97));
        }
      }
      else {
        localStringBuffer.append(localRandom.nextInt(10));
      }
    }
    return localStringBuffer.toString();
  }
  
  public static String getNumSmallLetter(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < paramInt; i++) {
      if (localRandom.nextInt(2) % 2 == 0) {
        localStringBuffer.append((char)(localRandom.nextInt(26) + 97));
      } else {
        localStringBuffer.append(localRandom.nextInt(10));
      }
    }
    return localStringBuffer.toString();
  }
  
  public static String getSmallLetter()
  {
    return String.valueOf((char)(new Random().nextInt(26) + 97));
  }
  
  public static String getSmallLetter(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < paramInt; i++) {
      localStringBuffer.append((char)(localRandom.nextInt(26) + 97));
    }
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/RandomUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */