package com.imcash.gp.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.util.Base64;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileUtil
{
  public static File BASEFILE = ;
  public static File DEVICEFILE = new File(BASEFILE, "/imcash/");
  public static String IMAGECACHE = "imageloader/";
  public static File MAINFILE = new File(BASEFILE, "/imcash/");
  public static final String MAINPATH = "/imcash/";
  
  public static boolean checkSDCardAvailable()
  {
    return Environment.getExternalStorageState().equals("mounted");
  }
  
  public static void delete(File paramFile)
  {
    if (paramFile.isFile())
    {
      paramFile.delete();
      return;
    }
    if (paramFile.isDirectory())
    {
      File[] arrayOfFile = paramFile.listFiles();
      if ((arrayOfFile != null) && (arrayOfFile.length != 0))
      {
        for (int i = 0; i < arrayOfFile.length; i++) {
          delete(arrayOfFile[i]);
        }
        paramFile.delete();
      }
      else
      {
        paramFile.delete();
        return;
      }
    }
  }
  
  public static void deleteAllPhoto(String paramString)
  {
    paramString = new File(paramString).listFiles();
    for (int i = 0; i < paramString.length; i++) {
      paramString[i].delete();
    }
  }
  
  public static void deletePhotoAtPathAndName(String paramString1, String paramString2)
  {
    paramString1 = new File(paramString1).listFiles();
    for (int i = 0; i < paramString1.length; i++) {
      if (paramString1[i].getName().split("\\.")[0].equals(paramString2)) {
        paramString1[i].delete();
      }
    }
  }
  
  public static String encodeBase64File(File paramFile)
    throws Exception
  {
    FileInputStream localFileInputStream = new FileInputStream(paramFile);
    paramFile = new byte[(int)paramFile.length()];
    localFileInputStream.read(paramFile);
    localFileInputStream.close();
    return Base64.encodeToString(paramFile, 0);
  }
  
  public static boolean enoughSpaceOnSdCard(long paramLong)
  {
    boolean bool2 = Environment.getExternalStorageState().equals("mounted");
    boolean bool1 = false;
    if (!bool2) {
      return false;
    }
    if (paramLong < getRealSizeOnSdcard()) {
      bool1 = true;
    }
    return bool1;
  }
  
  public static boolean findAmrForSDCard(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(Environment.getExternalStorageDirectory());
    localStringBuilder.append("/");
    localStringBuilder.append(paramString1);
    if (!new File(localStringBuilder.toString()).exists()) {
      return false;
    }
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(Environment.getExternalStorageDirectory());
    localStringBuilder.append("/");
    localStringBuilder.append(paramString1);
    paramString1 = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString2);
    localStringBuilder.append(".amr");
    return new File(paramString1, localStringBuilder.toString()).exists();
  }
  
  public static boolean findJPGForSDCard(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(Environment.getExternalStorageDirectory());
    localStringBuilder.append("/");
    localStringBuilder.append(paramString1);
    if (!new File(localStringBuilder.toString()).exists()) {
      return false;
    }
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(Environment.getExternalStorageDirectory());
    localStringBuilder.append("/");
    localStringBuilder.append(paramString1);
    paramString1 = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString2);
    localStringBuilder.append(".jpg");
    return new File(paramString1, localStringBuilder.toString()).exists();
  }
  
  public static boolean findPhotoFromSDCard(String paramString1, String paramString2)
  {
    boolean bool3 = checkSDCardAvailable();
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (bool3)
    {
      bool1 = bool2;
      if (new File(paramString1).exists())
      {
        paramString1 = new File(paramString1).listFiles();
        int i = 0;
        bool1 = false;
        while (i < paramString1.length)
        {
          if (paramString1[i].getName().split("\\.")[0].equals(paramString2)) {
            bool1 = true;
          }
          i++;
        }
      }
    }
    return bool1;
  }
  
  public static byte[] getMergeBytes(byte[] paramArrayOfByte1, int paramInt1, byte[] paramArrayOfByte2, int paramInt2)
  {
    byte[] arrayOfByte = new byte[paramInt1 + paramInt2];
    int k = 0;
    int j;
    for (int i = 0;; i++)
    {
      j = k;
      if (i >= paramInt1) {
        break;
      }
      arrayOfByte[i] = paramArrayOfByte1[i];
    }
    while (j < paramInt2)
    {
      arrayOfByte[(paramInt1 + j)] = paramArrayOfByte2[j];
      j++;
    }
    return arrayOfByte;
  }
  
  public static long getRealSizeOnSdcard()
  {
    StatFs localStatFs = new StatFs(new File(Environment.getExternalStorageDirectory().getAbsolutePath()).getPath());
    long l = localStatFs.getBlockSize();
    return localStatFs.getAvailableBlocks() * l;
  }
  
  public static void init(Context paramContext)
  {
    if (checkSDCardAvailable()) {
      BASEFILE = Environment.getExternalStorageDirectory();
    } else {
      BASEFILE = paramContext.getFilesDir();
    }
    MAINFILE = new File(BASEFILE, "/imcash/");
    DEVICEFILE = new File(BASEFILE, "/imcash/");
  }
  
  public static boolean mkdir(File paramFile)
  {
    if (!paramFile.exists())
    {
      paramFile.mkdir();
      return true;
    }
    return false;
  }
  
  public static boolean mkdir(String paramString)
  {
    paramString = new File(paramString);
    if (!paramString.exists())
    {
      paramString.mkdir();
      return true;
    }
    return false;
  }
  
  public static String readAmr(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append(Environment.getExternalStorageDirectory());
    localStringBuilder1.append("/");
    localStringBuilder1.append(paramString1);
    boolean bool = new File(localStringBuilder1.toString()).exists();
    localStringBuilder1 = null;
    if (!bool) {
      return null;
    }
    StringBuilder localStringBuilder2 = new StringBuilder();
    localStringBuilder2.append(Environment.getExternalStorageDirectory());
    localStringBuilder2.append("/");
    localStringBuilder2.append(paramString1);
    paramString1 = localStringBuilder2.toString();
    localStringBuilder2 = new StringBuilder();
    localStringBuilder2.append(paramString2);
    localStringBuilder2.append(".amr");
    paramString2 = new File(paramString1, localStringBuilder2.toString());
    try
    {
      paramString1 = new java/io/FileInputStream;
      paramString1.<init>(paramString2);
      paramString2 = new byte[(int)paramString2.length()];
      int i = paramString1.read(paramString2);
      paramString1 = new byte[6];
      paramString1[0] = 35;
      paramString1[1] = 33;
      paramString1[2] = 65;
      paramString1[3] = 77;
      paramString1[4] = 82;
      paramString1[5] = 10;
      getMergeBytes(paramString1, paramString1.length, paramString2, i);
      paramString1 = Base64.encodeToString(paramString2, 0, i, 0);
    }
    catch (IOException paramString1)
    {
      paramString1.printStackTrace();
      paramString1 = localStringBuilder1;
    }
    catch (FileNotFoundException paramString1)
    {
      paramString1.printStackTrace();
      paramString1 = localStringBuilder1;
    }
    return paramString1;
  }
  
  public static Bitmap readJpg(String paramString1, String paramString2)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(Environment.getExternalStorageDirectory());
    ((StringBuilder)localObject).append("/");
    ((StringBuilder)localObject).append(paramString1);
    localObject = new File(((StringBuilder)localObject).toString());
    if (!((File)localObject).exists()) {
      ((File)localObject).mkdirs();
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(Environment.getExternalStorageDirectory());
    ((StringBuilder)localObject).append("/");
    ((StringBuilder)localObject).append(paramString1);
    localObject = ((StringBuilder)localObject).toString();
    paramString1 = new StringBuilder();
    paramString1.append(paramString2);
    paramString1.append(".jpg");
    paramString1 = new File((String)localObject, paramString1.toString());
    if (paramString1.exists()) {
      return BitmapFactory.decodeFile(paramString1.getPath());
    }
    return null;
  }
  
  /* Error */
  public static File saveAmrToSDCard(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: invokestatic 146	com/imcash/gp/tools/FileUtil:checkSDCardAvailable	()Z
    //   3: istore_3
    //   4: aconst_null
    //   5: astore 5
    //   7: aconst_null
    //   8: astore 6
    //   10: aconst_null
    //   11: astore 7
    //   13: iload_3
    //   14: ifeq +293 -> 307
    //   17: new 120	java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   24: astore 4
    //   26: aload 4
    //   28: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   31: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 4
    //   37: ldc 127
    //   39: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload 4
    //   45: aload_1
    //   46: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: new 25	java/io/File
    //   53: dup
    //   54: aload 4
    //   56: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   59: invokespecial 74	java/io/File:<init>	(Ljava/lang/String;)V
    //   62: astore 4
    //   64: aload 4
    //   66: invokevirtual 136	java/io/File:exists	()Z
    //   69: ifne +9 -> 78
    //   72: aload 4
    //   74: invokevirtual 196	java/io/File:mkdirs	()Z
    //   77: pop
    //   78: new 120	java/lang/StringBuilder
    //   81: dup
    //   82: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   85: astore 4
    //   87: aload 4
    //   89: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   92: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload 4
    //   98: ldc 127
    //   100: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload 4
    //   106: aload_1
    //   107: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload 4
    //   113: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: astore 4
    //   118: new 120	java/lang/StringBuilder
    //   121: dup
    //   122: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   125: astore_1
    //   126: aload_1
    //   127: aload_2
    //   128: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload_1
    //   133: ldc -118
    //   135: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: new 25	java/io/File
    //   142: dup
    //   143: aload 4
    //   145: aload_1
    //   146: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   149: invokespecial 140	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   152: astore 4
    //   154: aload 7
    //   156: astore_1
    //   157: new 206	java/io/FileOutputStream
    //   160: astore_2
    //   161: aload 7
    //   163: astore_1
    //   164: aload_2
    //   165: aload 4
    //   167: invokespecial 207	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   170: aload_0
    //   171: ifnull +41 -> 212
    //   174: aload_2
    //   175: aload_0
    //   176: invokevirtual 211	java/io/FileOutputStream:write	([B)V
    //   179: aload_2
    //   180: invokevirtual 214	java/io/FileOutputStream:flush	()V
    //   183: aload_2
    //   184: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   187: goto +25 -> 212
    //   190: astore_0
    //   191: aload_2
    //   192: astore_1
    //   193: goto +100 -> 293
    //   196: astore_1
    //   197: aload_2
    //   198: astore_0
    //   199: aload_1
    //   200: astore_2
    //   201: goto +40 -> 241
    //   204: astore_1
    //   205: aload_2
    //   206: astore_0
    //   207: aload_1
    //   208: astore_2
    //   209: goto +60 -> 269
    //   212: aload_2
    //   213: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   216: aload 4
    //   218: astore_0
    //   219: goto +90 -> 309
    //   222: astore_0
    //   223: aload_0
    //   224: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   227: aload 4
    //   229: astore_0
    //   230: goto +79 -> 309
    //   233: astore_0
    //   234: goto +59 -> 293
    //   237: astore_2
    //   238: aload 5
    //   240: astore_0
    //   241: aload_0
    //   242: astore_1
    //   243: aload 4
    //   245: invokevirtual 61	java/io/File:delete	()Z
    //   248: pop
    //   249: aload_0
    //   250: astore_1
    //   251: aload_2
    //   252: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   255: aload_0
    //   256: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   259: aload 4
    //   261: astore_0
    //   262: goto +47 -> 309
    //   265: astore_2
    //   266: aload 6
    //   268: astore_0
    //   269: aload_0
    //   270: astore_1
    //   271: aload 4
    //   273: invokevirtual 61	java/io/File:delete	()Z
    //   276: pop
    //   277: aload_0
    //   278: astore_1
    //   279: aload_2
    //   280: invokevirtual 191	java/io/FileNotFoundException:printStackTrace	()V
    //   283: aload_0
    //   284: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   287: aload 4
    //   289: astore_0
    //   290: goto +19 -> 309
    //   293: aload_1
    //   294: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   297: goto +8 -> 305
    //   300: astore_1
    //   301: aload_1
    //   302: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   305: aload_0
    //   306: athrow
    //   307: aconst_null
    //   308: astore_0
    //   309: aload_0
    //   310: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	311	0	paramArrayOfByte	byte[]
    //   0	311	1	paramString1	String
    //   0	311	2	paramString2	String
    //   3	11	3	bool	boolean
    //   24	264	4	localObject1	Object
    //   5	234	5	localObject2	Object
    //   8	259	6	localObject3	Object
    //   11	151	7	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   174	187	190	finally
    //   174	187	196	java/io/IOException
    //   174	187	204	java/io/FileNotFoundException
    //   212	216	222	java/io/IOException
    //   255	259	222	java/io/IOException
    //   283	287	222	java/io/IOException
    //   157	161	233	finally
    //   164	170	233	finally
    //   243	249	233	finally
    //   251	255	233	finally
    //   271	277	233	finally
    //   279	283	233	finally
    //   157	161	237	java/io/IOException
    //   164	170	237	java/io/IOException
    //   157	161	265	java/io/FileNotFoundException
    //   164	170	265	java/io/FileNotFoundException
    //   293	297	300	java/io/IOException
  }
  
  /* Error */
  public static File saveJPGToSDCard(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: invokestatic 146	com/imcash/gp/tools/FileUtil:checkSDCardAvailable	()Z
    //   3: istore_3
    //   4: aconst_null
    //   5: astore 5
    //   7: aconst_null
    //   8: astore 6
    //   10: aconst_null
    //   11: astore 7
    //   13: iload_3
    //   14: ifeq +295 -> 309
    //   17: new 120	java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   24: astore 4
    //   26: aload 4
    //   28: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   31: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 4
    //   37: ldc 127
    //   39: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload 4
    //   45: aload_1
    //   46: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: new 25	java/io/File
    //   53: dup
    //   54: aload 4
    //   56: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   59: invokespecial 74	java/io/File:<init>	(Ljava/lang/String;)V
    //   62: astore 4
    //   64: aload 4
    //   66: invokevirtual 136	java/io/File:exists	()Z
    //   69: ifne +9 -> 78
    //   72: aload 4
    //   74: invokevirtual 196	java/io/File:mkdirs	()Z
    //   77: pop
    //   78: new 120	java/lang/StringBuilder
    //   81: dup
    //   82: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   85: astore 4
    //   87: aload 4
    //   89: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   92: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload 4
    //   98: ldc 127
    //   100: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload 4
    //   106: aload_1
    //   107: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload 4
    //   113: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: astore_1
    //   117: new 120	java/lang/StringBuilder
    //   120: dup
    //   121: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   124: astore 4
    //   126: aload 4
    //   128: aload_2
    //   129: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload 4
    //   135: ldc -113
    //   137: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   140: pop
    //   141: new 25	java/io/File
    //   144: dup
    //   145: aload_1
    //   146: aload 4
    //   148: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   151: invokespecial 140	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   154: astore 4
    //   156: aload 7
    //   158: astore_1
    //   159: new 206	java/io/FileOutputStream
    //   162: astore_2
    //   163: aload 7
    //   165: astore_1
    //   166: aload_2
    //   167: aload 4
    //   169: invokespecial 207	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   172: aload_0
    //   173: ifnull +41 -> 214
    //   176: aload_2
    //   177: aload_0
    //   178: invokevirtual 211	java/io/FileOutputStream:write	([B)V
    //   181: aload_2
    //   182: invokevirtual 214	java/io/FileOutputStream:flush	()V
    //   185: aload_2
    //   186: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   189: goto +25 -> 214
    //   192: astore_0
    //   193: aload_2
    //   194: astore_1
    //   195: goto +100 -> 295
    //   198: astore_1
    //   199: aload_2
    //   200: astore_0
    //   201: aload_1
    //   202: astore_2
    //   203: goto +40 -> 243
    //   206: astore_1
    //   207: aload_2
    //   208: astore_0
    //   209: aload_1
    //   210: astore_2
    //   211: goto +60 -> 271
    //   214: aload_2
    //   215: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   218: aload 4
    //   220: astore_0
    //   221: goto +90 -> 311
    //   224: astore_0
    //   225: aload_0
    //   226: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   229: aload 4
    //   231: astore_0
    //   232: goto +79 -> 311
    //   235: astore_0
    //   236: goto +59 -> 295
    //   239: astore_2
    //   240: aload 5
    //   242: astore_0
    //   243: aload_0
    //   244: astore_1
    //   245: aload 4
    //   247: invokevirtual 61	java/io/File:delete	()Z
    //   250: pop
    //   251: aload_0
    //   252: astore_1
    //   253: aload_2
    //   254: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   257: aload_0
    //   258: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   261: aload 4
    //   263: astore_0
    //   264: goto +47 -> 311
    //   267: astore_2
    //   268: aload 6
    //   270: astore_0
    //   271: aload_0
    //   272: astore_1
    //   273: aload 4
    //   275: invokevirtual 61	java/io/File:delete	()Z
    //   278: pop
    //   279: aload_0
    //   280: astore_1
    //   281: aload_2
    //   282: invokevirtual 191	java/io/FileNotFoundException:printStackTrace	()V
    //   285: aload_0
    //   286: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   289: aload 4
    //   291: astore_0
    //   292: goto +19 -> 311
    //   295: aload_1
    //   296: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   299: goto +8 -> 307
    //   302: astore_1
    //   303: aload_1
    //   304: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   307: aload_0
    //   308: athrow
    //   309: aconst_null
    //   310: astore_0
    //   311: aload_0
    //   312: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	313	0	paramArrayOfByte	byte[]
    //   0	313	1	paramString1	String
    //   0	313	2	paramString2	String
    //   3	11	3	bool	boolean
    //   24	266	4	localObject1	Object
    //   5	236	5	localObject2	Object
    //   8	261	6	localObject3	Object
    //   11	153	7	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   176	189	192	finally
    //   176	189	198	java/io/IOException
    //   176	189	206	java/io/FileNotFoundException
    //   214	218	224	java/io/IOException
    //   257	261	224	java/io/IOException
    //   285	289	224	java/io/IOException
    //   159	163	235	finally
    //   166	172	235	finally
    //   245	251	235	finally
    //   253	257	235	finally
    //   273	279	235	finally
    //   281	285	235	finally
    //   159	163	239	java/io/IOException
    //   166	172	239	java/io/IOException
    //   159	163	267	java/io/FileNotFoundException
    //   166	172	267	java/io/FileNotFoundException
    //   295	299	302	java/io/IOException
  }
  
  /* Error */
  public static File savePhotoToSDCard(Bitmap paramBitmap, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: invokestatic 146	com/imcash/gp/tools/FileUtil:checkSDCardAvailable	()Z
    //   3: istore_3
    //   4: aconst_null
    //   5: astore 5
    //   7: aconst_null
    //   8: astore 6
    //   10: aconst_null
    //   11: astore 7
    //   13: iload_3
    //   14: ifeq +301 -> 315
    //   17: new 120	java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   24: astore 4
    //   26: aload 4
    //   28: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   31: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 4
    //   37: ldc 127
    //   39: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload 4
    //   45: aload_1
    //   46: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: new 25	java/io/File
    //   53: dup
    //   54: aload 4
    //   56: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   59: invokespecial 74	java/io/File:<init>	(Ljava/lang/String;)V
    //   62: astore 4
    //   64: aload 4
    //   66: invokevirtual 136	java/io/File:exists	()Z
    //   69: ifne +9 -> 78
    //   72: aload 4
    //   74: invokevirtual 196	java/io/File:mkdirs	()Z
    //   77: pop
    //   78: new 120	java/lang/StringBuilder
    //   81: dup
    //   82: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   85: astore 4
    //   87: aload 4
    //   89: invokestatic 21	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   92: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload 4
    //   98: ldc 127
    //   100: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload 4
    //   106: aload_1
    //   107: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload 4
    //   113: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: astore 4
    //   118: new 120	java/lang/StringBuilder
    //   121: dup
    //   122: invokespecial 121	java/lang/StringBuilder:<init>	()V
    //   125: astore_1
    //   126: aload_1
    //   127: aload_2
    //   128: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload_1
    //   133: ldc -113
    //   135: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: new 25	java/io/File
    //   142: dup
    //   143: aload 4
    //   145: aload_1
    //   146: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   149: invokespecial 140	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   152: astore 4
    //   154: aload 7
    //   156: astore_1
    //   157: new 206	java/io/FileOutputStream
    //   160: astore_2
    //   161: aload 7
    //   163: astore_1
    //   164: aload_2
    //   165: aload 4
    //   167: invokespecial 207	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   170: aload_0
    //   171: ifnull +49 -> 220
    //   174: aload_0
    //   175: getstatic 224	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   178: bipush 100
    //   180: aload_2
    //   181: invokevirtual 230	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   184: ifeq +36 -> 220
    //   187: aload_2
    //   188: invokevirtual 214	java/io/FileOutputStream:flush	()V
    //   191: aload_2
    //   192: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   195: goto +25 -> 220
    //   198: astore_0
    //   199: aload_2
    //   200: astore_1
    //   201: goto +100 -> 301
    //   204: astore_1
    //   205: aload_2
    //   206: astore_0
    //   207: aload_1
    //   208: astore_2
    //   209: goto +40 -> 249
    //   212: astore_1
    //   213: aload_2
    //   214: astore_0
    //   215: aload_1
    //   216: astore_2
    //   217: goto +60 -> 277
    //   220: aload_2
    //   221: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   224: aload 4
    //   226: astore_0
    //   227: goto +90 -> 317
    //   230: astore_0
    //   231: aload_0
    //   232: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   235: aload 4
    //   237: astore_0
    //   238: goto +79 -> 317
    //   241: astore_0
    //   242: goto +59 -> 301
    //   245: astore_2
    //   246: aload 5
    //   248: astore_0
    //   249: aload_0
    //   250: astore_1
    //   251: aload 4
    //   253: invokevirtual 61	java/io/File:delete	()Z
    //   256: pop
    //   257: aload_0
    //   258: astore_1
    //   259: aload_2
    //   260: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   263: aload_0
    //   264: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   267: aload 4
    //   269: astore_0
    //   270: goto +47 -> 317
    //   273: astore_2
    //   274: aload 6
    //   276: astore_0
    //   277: aload_0
    //   278: astore_1
    //   279: aload 4
    //   281: invokevirtual 61	java/io/File:delete	()Z
    //   284: pop
    //   285: aload_0
    //   286: astore_1
    //   287: aload_2
    //   288: invokevirtual 191	java/io/FileNotFoundException:printStackTrace	()V
    //   291: aload_0
    //   292: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   295: aload 4
    //   297: astore_0
    //   298: goto +19 -> 317
    //   301: aload_1
    //   302: invokevirtual 215	java/io/FileOutputStream:close	()V
    //   305: goto +8 -> 313
    //   308: astore_1
    //   309: aload_1
    //   310: invokevirtual 190	java/io/IOException:printStackTrace	()V
    //   313: aload_0
    //   314: athrow
    //   315: aconst_null
    //   316: astore_0
    //   317: aload_0
    //   318: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	319	0	paramBitmap	Bitmap
    //   0	319	1	paramString1	String
    //   0	319	2	paramString2	String
    //   3	11	3	bool	boolean
    //   24	272	4	localObject1	Object
    //   5	242	5	localObject2	Object
    //   8	267	6	localObject3	Object
    //   11	151	7	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   174	195	198	finally
    //   174	195	204	java/io/IOException
    //   174	195	212	java/io/FileNotFoundException
    //   220	224	230	java/io/IOException
    //   263	267	230	java/io/IOException
    //   291	295	230	java/io/IOException
    //   157	161	241	finally
    //   164	170	241	finally
    //   251	257	241	finally
    //   259	263	241	finally
    //   279	285	241	finally
    //   287	291	241	finally
    //   157	161	245	java/io/IOException
    //   164	170	245	java/io/IOException
    //   157	161	273	java/io/FileNotFoundException
    //   164	170	273	java/io/FileNotFoundException
    //   301	305	308	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/FileUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */