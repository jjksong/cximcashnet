package com.imcash.gp.tools;

import java.math.BigDecimal;

public class ArithUtil
{
  private static final int DEF_DIV_SCALE = 10;
  
  public static double add(double paramDouble1, double paramDouble2)
  {
    return new BigDecimal(Double.toString(paramDouble1)).add(new BigDecimal(Double.toString(paramDouble2))).doubleValue();
  }
  
  public static double div(double paramDouble1, double paramDouble2)
  {
    return div(paramDouble1, paramDouble2, 10);
  }
  
  public static double div(double paramDouble1, double paramDouble2, int paramInt)
  {
    if (paramInt >= 0) {
      return new BigDecimal(Double.toString(paramDouble1)).divide(new BigDecimal(Double.toString(paramDouble2)), paramInt, 4).doubleValue();
    }
    throw new IllegalArgumentException("The   scale   must   be   a   positive   integer   or   zero");
  }
  
  public static double mul(double paramDouble1, double paramDouble2)
  {
    return new BigDecimal(Double.toString(paramDouble1)).multiply(new BigDecimal(Double.toString(paramDouble2))).doubleValue();
  }
  
  public static double round(double paramDouble, int paramInt)
  {
    if (paramInt >= 0) {
      return new BigDecimal(Double.toString(paramDouble)).divide(new BigDecimal("1"), paramInt, 4).doubleValue();
    }
    throw new IllegalArgumentException("The   scale   must   be   a   positive   integer   or   zero");
  }
  
  public static double sub(double paramDouble1, double paramDouble2)
  {
    return new BigDecimal(Double.toString(paramDouble1)).subtract(new BigDecimal(Double.toString(paramDouble2))).doubleValue();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/ArithUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */