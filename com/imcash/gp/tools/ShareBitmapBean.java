package com.imcash.gp.tools;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShareBitmapBean
  implements Serializable
{
  private Bitmap bg;
  private List<BitmapBean> bitmapList;
  private List<QRCodeBean> qrCodeList;
  private List<TextBean> textList;
  
  public Bitmap getBg()
  {
    return this.bg;
  }
  
  public List<BitmapBean> getBitmapList()
  {
    return this.bitmapList;
  }
  
  public List<QRCodeBean> getQrCodeList()
  {
    return this.qrCodeList;
  }
  
  public List<TextBean> getTextList()
  {
    return this.textList;
  }
  
  public void setBg(Bitmap paramBitmap)
  {
    this.bg = paramBitmap;
  }
  
  public void setBitmapList(List<BitmapBean> paramList)
  {
    this.bitmapList = paramList;
  }
  
  public void setQrCodeList(List<QRCodeBean> paramList)
  {
    this.qrCodeList = paramList;
  }
  
  public void setTextList(List<TextBean> paramList)
  {
    this.textList = paramList;
  }
  
  public static class BitmapBean
    implements Serializable
  {
    private Bitmap bitmap;
    private int bitmapHeight;
    private int bitmapWidth;
    private int bitmapX;
    private int bitmapY;
    
    public BitmapBean(Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.bitmap = paramBitmap;
      this.bitmapWidth = paramInt1;
      this.bitmapHeight = paramInt2;
      this.bitmapX = paramInt3;
      this.bitmapY = paramInt4;
    }
    
    public Bitmap getBitmap()
    {
      return this.bitmap;
    }
    
    public int getBitmapHeight()
    {
      return this.bitmapHeight;
    }
    
    public int getBitmapWidth()
    {
      return this.bitmapWidth;
    }
    
    public int getBitmapX()
    {
      return this.bitmapX;
    }
    
    public int getBitmapY()
    {
      return this.bitmapY;
    }
    
    public void setBitmap(Bitmap paramBitmap)
    {
      this.bitmap = paramBitmap;
    }
    
    public void setBitmapHeight(int paramInt)
    {
      this.bitmapHeight = paramInt;
    }
    
    public void setBitmapWidth(int paramInt)
    {
      this.bitmapWidth = paramInt;
    }
    
    public void setBitmapX(int paramInt)
    {
      this.bitmapX = paramInt;
    }
    
    public void setBitmapY(int paramInt)
    {
      this.bitmapY = paramInt;
    }
    
    public List<BitmapBean> toList()
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(this);
      return localArrayList;
    }
  }
  
  public static class QRCodeBean
    implements Serializable
  {
    private int codeHeight;
    private String codeMsg;
    private int codeWidth;
    private int codeX;
    private int codeY;
    
    public QRCodeBean(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.codeMsg = paramString;
      this.codeWidth = paramInt1;
      this.codeHeight = paramInt2;
      this.codeX = paramInt3;
      this.codeY = paramInt4;
    }
    
    public int getCodeHeight()
    {
      return this.codeHeight;
    }
    
    public String getCodeMsg()
    {
      return this.codeMsg;
    }
    
    public int getCodeWidth()
    {
      return this.codeWidth;
    }
    
    public int getCodeX()
    {
      return this.codeX;
    }
    
    public int getCodeY()
    {
      return this.codeY;
    }
    
    public void setCodeHeight(int paramInt)
    {
      this.codeHeight = paramInt;
    }
    
    public void setCodeMsg(String paramString)
    {
      this.codeMsg = paramString;
    }
    
    public void setCodeWidth(int paramInt)
    {
      this.codeWidth = paramInt;
    }
    
    public void setCodeX(int paramInt)
    {
      this.codeX = paramInt;
    }
    
    public void setCodeY(int paramInt)
    {
      this.codeY = paramInt;
    }
    
    public List<QRCodeBean> toList()
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(this);
      return localArrayList;
    }
  }
  
  public static class TextBean
    implements Serializable
  {
    private boolean isCenter = false;
    private int sizePX;
    private String text;
    private int textColor;
    private int textX;
    private int textY;
    
    public TextBean(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.text = paramString;
      this.sizePX = paramInt1;
      this.textX = paramInt2;
      this.textY = paramInt3;
      this.textColor = paramInt4;
    }
    
    public int getSizePX()
    {
      return this.sizePX;
    }
    
    public String getText()
    {
      return this.text;
    }
    
    public int getTextColor()
    {
      return this.textColor;
    }
    
    public int getTextX()
    {
      return this.textX;
    }
    
    public int getTextY()
    {
      return this.textY;
    }
    
    public boolean isCenter()
    {
      return this.isCenter;
    }
    
    public void setCenter(boolean paramBoolean)
    {
      this.isCenter = paramBoolean;
    }
    
    public void setSizePX(int paramInt)
    {
      this.sizePX = paramInt;
    }
    
    public void setText(String paramString)
    {
      this.text = paramString;
    }
    
    public void setTextColor(int paramInt)
    {
      this.textColor = paramInt;
    }
    
    public void setTextX(int paramInt)
    {
      this.textX = paramInt;
    }
    
    public void setTextY(int paramInt)
    {
      this.textY = paramInt;
    }
    
    public List<TextBean> toList()
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(this);
      return localArrayList;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/ShareBitmapBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */