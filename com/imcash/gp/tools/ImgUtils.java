package com.imcash.gp.tools;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ImgUtils
{
  public static boolean saveImageToGallery(Context paramContext, Bitmap paramBitmap)
  {
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(Environment.getExternalStorageDirectory().getAbsolutePath());
    ((StringBuilder)localObject1).append(File.separator);
    ((StringBuilder)localObject1).append("imcash");
    Object localObject2 = new File(((StringBuilder)localObject1).toString());
    if (!((File)localObject2).exists()) {
      ((File)localObject2).mkdir();
    }
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(System.currentTimeMillis());
    ((StringBuilder)localObject1).append(".jpg");
    localObject1 = new File((File)localObject2, ((StringBuilder)localObject1).toString());
    try
    {
      localObject2 = new java/io/FileOutputStream;
      ((FileOutputStream)localObject2).<init>((File)localObject1);
      boolean bool = paramBitmap.compress(Bitmap.CompressFormat.JPEG, 60, (OutputStream)localObject2);
      ((FileOutputStream)localObject2).flush();
      ((FileOutputStream)localObject2).close();
      paramBitmap = Uri.fromFile((File)localObject1);
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("android.intent.action.MEDIA_SCANNER_SCAN_FILE", paramBitmap);
      paramContext.sendBroadcast((Intent)localObject1);
      return bool;
    }
    catch (IOException paramContext)
    {
      paramContext.printStackTrace();
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/ImgUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */