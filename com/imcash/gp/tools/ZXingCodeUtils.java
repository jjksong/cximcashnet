package com.imcash.gp.tools;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class ZXingCodeUtils
{
  private static volatile ZXingCodeUtils instance;
  
  private Bitmap addLogoToQRCode(Bitmap paramBitmap1, Bitmap paramBitmap2)
  {
    if ((paramBitmap1 != null) && (paramBitmap2 != null))
    {
      int i = paramBitmap1.getWidth();
      int k = paramBitmap1.getHeight();
      int m = paramBitmap2.getWidth();
      int j = paramBitmap2.getHeight();
      float f = i * 1.0F / 5.0F / m;
      Bitmap localBitmap = Bitmap.createBitmap(i, k, Bitmap.Config.ARGB_8888);
      try
      {
        Canvas localCanvas = new android/graphics/Canvas;
        localCanvas.<init>(localBitmap);
        localCanvas.drawBitmap(paramBitmap1, 0.0F, 0.0F, null);
        localCanvas.scale(f, f, i / 2, k / 2);
        localCanvas.drawBitmap(paramBitmap2, (i - m) / 2, (k - j) / 2, null);
        localCanvas.save();
        localCanvas.restore();
        paramBitmap1 = localBitmap;
      }
      catch (Exception paramBitmap1)
      {
        paramBitmap1 = null;
      }
      return paramBitmap1;
    }
    return paramBitmap1;
  }
  
  private static BitMatrix deleteWhite(BitMatrix paramBitMatrix)
  {
    int[] arrayOfInt = paramBitMatrix.getEnclosingRectangle();
    int m = arrayOfInt[2] + 1;
    int k = arrayOfInt[3] + 1;
    BitMatrix localBitMatrix = new BitMatrix(m, k);
    localBitMatrix.clear();
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        if (paramBitMatrix.get(arrayOfInt[0] + i, arrayOfInt[1] + j)) {
          localBitMatrix.set(i, j);
        }
      }
    }
    return localBitMatrix;
  }
  
  public static ZXingCodeUtils getInstance()
  {
    if (instance == null) {
      try
      {
        ZXingCodeUtils localZXingCodeUtils = new com/imcash/gp/tools/ZXingCodeUtils;
        localZXingCodeUtils.<init>();
        instance = localZXingCodeUtils;
      }
      finally {}
    }
    return instance;
  }
  
  public Bitmap createQRCode(String paramString, int paramInt1, int paramInt2)
  {
    return createQRCode(paramString, paramInt1, paramInt2, null);
  }
  
  public Bitmap createQRCode(String paramString, int paramInt1, int paramInt2, Bitmap paramBitmap)
  {
    try
    {
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = paramInt1;
      arrayOfInt[1] = paramInt2;
      Hashtable localHashtable = new java/util/Hashtable;
      localHashtable.<init>();
      localHashtable.put(EncodeHintType.CHARACTER_SET, "UTF-8");
      localHashtable.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      localHashtable.put(EncodeHintType.MARGIN, Integer.valueOf(1));
      Object localObject = new com/google/zxing/qrcode/QRCodeWriter;
      ((QRCodeWriter)localObject).<init>();
      localObject = deleteWhite(((QRCodeWriter)localObject).encode(paramString, BarcodeFormat.QR_CODE, paramInt1, paramInt2, localHashtable));
      int j = ((BitMatrix)localObject).getWidth();
      int i = ((BitMatrix)localObject).getHeight();
      paramString = new int[j * i];
      for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
        for (paramInt2 = 0; paramInt2 < j; paramInt2++) {
          if (((BitMatrix)localObject).get(paramInt2, paramInt1)) {
            paramString[(paramInt1 * j + paramInt2)] = -16777216;
          } else {
            paramString[(paramInt1 * j + paramInt2)] = -1;
          }
        }
      }
      localObject = Bitmap.createBitmap(j, i, Bitmap.Config.ARGB_8888);
      ((Bitmap)localObject).setPixels(paramString, 0, j, 0, 0, j, i);
      paramString = zoomImg(addLogoToQRCode((Bitmap)localObject, paramBitmap), arrayOfInt[0], arrayOfInt[1]);
      return paramString;
    }
    catch (WriterException paramString)
    {
      paramString.printStackTrace();
    }
    return null;
  }
  
  public Bitmap createQRCodeWithText(ShareBitmapBean paramShareBitmapBean)
  {
    return createQRCodeWithText(paramShareBitmapBean, null);
  }
  
  public Bitmap createQRCodeWithText(ShareBitmapBean paramShareBitmapBean, Bitmap paramBitmap)
  {
    if (paramShareBitmapBean != null)
    {
      Object localObject = paramShareBitmapBean.getBg();
      if (localObject != null)
      {
        Bitmap localBitmap = ((Bitmap)localObject).copy(Bitmap.Config.ARGB_8888, true);
        localObject = new Canvas(localBitmap);
        Paint localPaint = new Paint();
        Iterator localIterator;
        if (paramShareBitmapBean.getQrCodeList() != null)
        {
          localIterator = paramShareBitmapBean.getQrCodeList().iterator();
          while (localIterator.hasNext())
          {
            ShareBitmapBean.QRCodeBean localQRCodeBean = (ShareBitmapBean.QRCodeBean)localIterator.next();
            ((Canvas)localObject).drawBitmap(createQRCode(localQRCodeBean.getCodeMsg(), localQRCodeBean.getCodeWidth(), localQRCodeBean.getCodeHeight(), paramBitmap), localQRCodeBean.getCodeX(), localQRCodeBean.getCodeY(), localPaint);
          }
        }
        if (paramShareBitmapBean.getTextList() != null)
        {
          localIterator = paramShareBitmapBean.getTextList().iterator();
          while (localIterator.hasNext())
          {
            paramBitmap = (ShareBitmapBean.TextBean)localIterator.next();
            int i = paramBitmap.getSizePX() * 7 / 8 + paramBitmap.getTextY();
            localPaint.setColor(paramBitmap.getTextColor());
            localPaint.setTextSize(paramBitmap.getSizePX());
            if (paramBitmap.isCenter())
            {
              localPaint.setTextAlign(Paint.Align.CENTER);
              ((Canvas)localObject).drawText(paramBitmap.getText(), 360.0F, i, localPaint);
            }
            else
            {
              localPaint.setTextAlign(Paint.Align.LEFT);
              ((Canvas)localObject).drawText(paramBitmap.getText(), paramBitmap.getTextX(), i, localPaint);
            }
          }
        }
        if (paramShareBitmapBean.getBitmapList() != null)
        {
          paramShareBitmapBean = paramShareBitmapBean.getBitmapList().iterator();
          while (paramShareBitmapBean.hasNext())
          {
            paramBitmap = (ShareBitmapBean.BitmapBean)paramShareBitmapBean.next();
            ((Canvas)localObject).drawBitmap(paramBitmap.getBitmap(), paramBitmap.getBitmapX(), paramBitmap.getBitmapY(), localPaint);
          }
        }
        return localBitmap;
      }
      throw new IllegalArgumentException("background bitmap can't be null");
    }
    throw new IllegalArgumentException("shareBitmap data can't be null");
  }
  
  public Bitmap zoomImg(Bitmap paramBitmap, float paramFloat1, float paramFloat2)
  {
    if (paramBitmap == null) {
      return null;
    }
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    paramFloat1 /= i;
    paramFloat2 /= j;
    Matrix localMatrix = new Matrix();
    localMatrix.postScale(paramFloat1, paramFloat2);
    return Bitmap.createBitmap(paramBitmap, 0, 0, i, j, localMatrix, true);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/ZXingCodeUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */