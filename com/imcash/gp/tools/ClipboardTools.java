package com.imcash.gp.tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;

@SuppressLint({"NewApi"})
public class ClipboardTools
{
  public static void copy(String paramString, Context paramContext)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      ((android.content.ClipboardManager)paramContext.getSystemService("clipboard")).setText(paramString.trim());
    } else {
      ((android.text.ClipboardManager)paramContext.getSystemService("clipboard")).setText(paramString.trim());
    }
  }
  
  public static String paste(Context paramContext)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 11) {
        return ((android.content.ClipboardManager)paramContext.getSystemService("clipboard")).getText().toString().trim();
      }
      paramContext = ((android.text.ClipboardManager)paramContext.getSystemService("clipboard")).getText().toString().trim();
      return paramContext;
    }
    catch (NullPointerException paramContext) {}
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/imcash/gp/tools/ClipboardTools.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */