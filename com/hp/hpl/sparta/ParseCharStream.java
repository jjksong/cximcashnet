package com.hp.hpl.sparta;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;

class ParseCharStream
  implements ParseSource
{
  private static final char[] BEGIN_CDATA;
  private static final char[] BEGIN_ETAG;
  private static final char[] CHARREF_BEGIN;
  private static final char[] COMMENT_BEGIN;
  private static final char[] COMMENT_END;
  private static final boolean DEBUG = true;
  private static final char[] DOCTYPE_BEGIN;
  private static final char[] ENCODING;
  private static final char[] END_CDATA;
  private static final char[] END_EMPTYTAG;
  private static final char[] ENTITY_BEGIN;
  public static final int HISTORY_LENGTH = 100;
  private static final boolean H_DEBUG = false;
  private static final boolean[] IS_NAME_CHAR = new boolean[''];
  private static final char[] MARKUPDECL_BEGIN;
  private static final int MAX_COMMON_CHAR = 128;
  private static final char[] NAME_PUNCT_CHARS = { 46, 45, 95, 58 };
  private static final char[] NDATA;
  private static final char[] PI_BEGIN;
  private static final char[] PUBLIC;
  private static final char[] QU_END;
  private static final char[] SYSTEM;
  private static final int TMP_BUF_SIZE = 255;
  private static final char[] VERSION;
  private static final char[] VERSIONNUM_PUNC_CHARS;
  private static final char[] XML_BEGIN;
  private final int CBUF_SIZE = 1024;
  private final char[] cbuf_;
  private int ch_ = -2;
  private int curPos_ = 0;
  private String docTypeName_ = null;
  private final String encoding_;
  private int endPos_ = 0;
  private final Hashtable entities_ = new Hashtable();
  private boolean eos_ = false;
  private final ParseHandler handler_;
  private final CharCircBuffer history_ = null;
  private boolean isExternalDtd_ = false;
  private int lineNumber_ = -1;
  private final ParseLog log_;
  private final Hashtable pes_ = new Hashtable();
  private final Reader reader_;
  private String systemId_;
  private final char[] tmpBuf_ = new char['ÿ'];
  
  static
  {
    for (char c = '\000';; c = (char)(c + '\001'))
    {
      if (c >= '')
      {
        COMMENT_BEGIN = "<!--".toCharArray();
        COMMENT_END = "-->".toCharArray();
        PI_BEGIN = "<?".toCharArray();
        QU_END = "?>".toCharArray();
        DOCTYPE_BEGIN = "<!DOCTYPE".toCharArray();
        XML_BEGIN = "<?xml".toCharArray();
        ENCODING = "encoding".toCharArray();
        VERSION = "version".toCharArray();
        VERSIONNUM_PUNC_CHARS = new char[] { 95, 46, 58, 45 };
        MARKUPDECL_BEGIN = "<!".toCharArray();
        CHARREF_BEGIN = "&#".toCharArray();
        ENTITY_BEGIN = "<!ENTITY".toCharArray();
        NDATA = "NDATA".toCharArray();
        SYSTEM = "SYSTEM".toCharArray();
        PUBLIC = "PUBLIC".toCharArray();
        BEGIN_CDATA = "<![CDATA[".toCharArray();
        END_CDATA = "]]>".toCharArray();
        END_EMPTYTAG = "/>".toCharArray();
        BEGIN_ETAG = "</".toCharArray();
        return;
      }
      IS_NAME_CHAR[c] = isNameChar(c);
    }
  }
  
  public ParseCharStream(String paramString1, Reader paramReader, ParseLog paramParseLog, String paramString2, ParseHandler paramParseHandler)
    throws ParseException, EncodingMismatchException, IOException
  {
    this(paramString1, paramReader, null, paramParseLog, paramString2, paramParseHandler);
  }
  
  public ParseCharStream(String paramString1, Reader paramReader, char[] paramArrayOfChar, ParseLog paramParseLog, String paramString2, ParseHandler paramParseHandler)
    throws ParseException, EncodingMismatchException, IOException
  {
    ParseLog localParseLog = paramParseLog;
    if (paramParseLog == null) {
      localParseLog = ParseSource.DEFAULT_LOG;
    }
    this.log_ = localParseLog;
    if (paramString2 == null) {
      paramParseLog = null;
    } else {
      paramParseLog = paramString2.toLowerCase();
    }
    this.encoding_ = paramParseLog;
    this.entities_.put("lt", "<");
    this.entities_.put("gt", ">");
    this.entities_.put("amp", "&");
    this.entities_.put("apos", "'");
    this.entities_.put("quot", "\"");
    if (paramArrayOfChar != null)
    {
      this.cbuf_ = paramArrayOfChar;
      this.curPos_ = 0;
      this.endPos_ = this.cbuf_.length;
      this.eos_ = true;
      this.reader_ = null;
    }
    else
    {
      this.reader_ = paramReader;
      this.cbuf_ = new char['Ѐ'];
      fillBuf();
    }
    this.systemId_ = paramString1;
    this.handler_ = paramParseHandler;
    this.handler_.setParseSource(this);
    readProlog();
    this.handler_.startDocument();
    paramString1 = readElement();
    paramReader = this.docTypeName_;
    if ((paramReader != null) && (!paramReader.equals(paramString1.getTagName())))
    {
      paramArrayOfChar = this.log_;
      paramReader = new StringBuffer();
      paramReader.append("DOCTYPE name \"");
      paramReader.append(this.docTypeName_);
      paramReader.append("\" not same as tag name, \"");
      paramReader.append(paramString1.getTagName());
      paramReader.append("\" of root element");
      paramArrayOfChar.warning(paramReader.toString(), this.systemId_, getLineNumber());
    }
    for (;;)
    {
      if (!isMisc())
      {
        paramString1 = this.reader_;
        if (paramString1 != null) {
          paramString1.close();
        }
        this.handler_.endDocument();
        return;
      }
      readMisc();
    }
  }
  
  public ParseCharStream(String paramString1, char[] paramArrayOfChar, ParseLog paramParseLog, String paramString2, ParseHandler paramParseHandler)
    throws ParseException, EncodingMismatchException, IOException
  {
    this(paramString1, null, paramArrayOfChar, paramParseLog, paramString2, paramParseHandler);
  }
  
  private int fillBuf()
    throws IOException
  {
    if (this.eos_) {
      return -1;
    }
    if (this.endPos_ == this.cbuf_.length)
    {
      this.endPos_ = 0;
      this.curPos_ = 0;
    }
    Reader localReader = this.reader_;
    char[] arrayOfChar = this.cbuf_;
    int i = this.endPos_;
    i = localReader.read(arrayOfChar, i, arrayOfChar.length - i);
    if (i <= 0)
    {
      this.eos_ = true;
      return -1;
    }
    this.endPos_ += i;
    return i;
  }
  
  private int fillBuf(int paramInt)
    throws IOException
  {
    if (this.eos_) {
      return -1;
    }
    if (this.cbuf_.length - this.curPos_ < paramInt) {
      for (paramInt = 0;; paramInt++)
      {
        int j = this.curPos_;
        i = this.endPos_;
        if (j + paramInt >= i)
        {
          paramInt = i - j;
          this.endPos_ = paramInt;
          this.curPos_ = 0;
          break;
        }
        char[] arrayOfChar = this.cbuf_;
        arrayOfChar[paramInt] = arrayOfChar[(j + paramInt)];
      }
    }
    paramInt = 0;
    int i = fillBuf();
    if (i == -1)
    {
      if (paramInt == 0) {
        return -1;
      }
      return paramInt;
    }
    return paramInt + i;
  }
  
  private boolean isCdSect()
    throws ParseException, IOException
  {
    return isSymbol(BEGIN_CDATA);
  }
  
  private final boolean isChar(char paramChar)
    throws ParseException, IOException
  {
    if ((this.curPos_ >= this.endPos_) && (fillBuf() == -1)) {
      throw new ParseException(this, "unexpected end of expression.");
    }
    boolean bool;
    if (this.cbuf_[this.curPos_] == paramChar) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private final boolean isChar(char paramChar1, char paramChar2)
    throws ParseException, IOException
  {
    char c1 = this.curPos_;
    char c2 = this.endPos_;
    boolean bool = false;
    if ((c1 >= c2) && (fillBuf() == -1)) {
      return false;
    }
    c1 = this.cbuf_[this.curPos_];
    if ((c1 == paramChar1) || (c1 == paramChar2)) {
      bool = true;
    }
    return bool;
  }
  
  private final boolean isChar(char paramChar1, char paramChar2, char paramChar3, char paramChar4)
    throws ParseException, IOException
  {
    int i = this.curPos_;
    char c = this.endPos_;
    boolean bool = false;
    if ((i >= c) && (fillBuf() == -1)) {
      return false;
    }
    c = this.cbuf_[this.curPos_];
    if ((c == paramChar1) || (c == paramChar2) || (c == paramChar3) || (c == paramChar4)) {
      bool = true;
    }
    return bool;
  }
  
  private final boolean isComment()
    throws ParseException, IOException
  {
    return isSymbol(COMMENT_BEGIN);
  }
  
  private boolean isDeclSep()
    throws ParseException, IOException
  {
    boolean bool;
    if ((!isPeReference()) && (!isS())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isDocTypeDecl()
    throws ParseException, IOException
  {
    return isSymbol(DOCTYPE_BEGIN);
  }
  
  private boolean isETag()
    throws ParseException, IOException
  {
    return isSymbol(BEGIN_ETAG);
  }
  
  private boolean isEncodingDecl()
    throws ParseException, IOException
  {
    return isSymbol(ENCODING);
  }
  
  private boolean isEntityDecl()
    throws ParseException, IOException
  {
    return isSymbol(ENTITY_BEGIN);
  }
  
  private final boolean isEntityValue()
    throws ParseException, IOException
  {
    return isChar('\'', '"');
  }
  
  private static boolean isExtender(char paramChar)
  {
    if ((paramChar != '·') && (paramChar != '·') && (paramChar != 'ـ') && (paramChar != 'ๆ') && (paramChar != 'ໆ') && (paramChar != '々')) {
      switch (paramChar)
      {
      default: 
        switch (paramChar)
        {
        default: 
          switch (paramChar)
          {
          default: 
            switch (paramChar)
            {
            default: 
              return false;
            }
            break;
          }
          break;
        }
        break;
      }
    }
    return true;
  }
  
  private boolean isExternalId()
    throws ParseException, IOException
  {
    boolean bool;
    if ((!isSymbol(SYSTEM)) && (!isSymbol(PUBLIC))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private static final boolean isIn(char paramChar, char[] paramArrayOfChar)
  {
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfChar.length) {
        return false;
      }
      if (paramChar == paramArrayOfChar[i]) {
        return true;
      }
    }
  }
  
  private static boolean isLetter(char paramChar)
  {
    boolean bool;
    if ("abcdefghijklmnopqrstuvwxyz".indexOf(Character.toLowerCase(paramChar)) != -1) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private boolean isMisc()
    throws ParseException, IOException
  {
    boolean bool;
    if ((!isComment()) && (!isPi()) && (!isS())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isNameChar()
    throws ParseException, IOException
  {
    char c = peekChar();
    boolean bool;
    if (c < '') {
      int i = IS_NAME_CHAR[c];
    } else {
      bool = isNameChar(c);
    }
    return bool;
  }
  
  private static boolean isNameChar(char paramChar)
  {
    boolean bool;
    if ((!Character.isDigit(paramChar)) && (!isLetter(paramChar)) && (!isIn(paramChar, NAME_PUNCT_CHARS)) && (!isExtender(paramChar))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isPeReference()
    throws ParseException, IOException
  {
    return isChar('%');
  }
  
  private final boolean isPi()
    throws ParseException, IOException
  {
    return isSymbol(PI_BEGIN);
  }
  
  private final boolean isReference()
    throws ParseException, IOException
  {
    return isChar('&');
  }
  
  private final boolean isS()
    throws ParseException, IOException
  {
    return isChar(' ', '\t', '\r', '\n');
  }
  
  private final boolean isSymbol(char[] paramArrayOfChar)
    throws ParseException, IOException
  {
    int j = paramArrayOfChar.length;
    if ((this.endPos_ - this.curPos_ < j) && (fillBuf(j) <= 0))
    {
      this.ch_ = -1;
      return false;
    }
    char[] arrayOfChar = this.cbuf_;
    int i = this.endPos_;
    this.ch_ = arrayOfChar[(i - 1)];
    if (i - this.curPos_ < j) {
      return false;
    }
    for (i = 0;; i++)
    {
      if (i >= j) {
        return true;
      }
      if (this.cbuf_[(this.curPos_ + i)] != paramArrayOfChar[i]) {
        return false;
      }
    }
  }
  
  private boolean isVersionNumChar()
    throws ParseException, IOException
  {
    char c = peekChar();
    boolean bool;
    if ((!Character.isDigit(c)) && (('a' > c) || (c > 'z')) && (('Z' > c) || (c > 'Z')) && (!isIn(c, VERSIONNUM_PUNC_CHARS))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private boolean isXmlDecl()
    throws ParseException, IOException
  {
    return isSymbol(XML_BEGIN);
  }
  
  private final char peekChar()
    throws ParseException, IOException
  {
    if ((this.curPos_ >= this.endPos_) && (fillBuf() == -1)) {
      throw new ParseException(this, "unexpected end of expression.");
    }
    return this.cbuf_[this.curPos_];
  }
  
  private String readAttValue()
    throws ParseException, IOException
  {
    char c = readChar('\'', '"');
    StringBuffer localStringBuffer = new StringBuffer();
    for (;;)
    {
      if (isChar(c))
      {
        readChar(c);
        return localStringBuffer.toString();
      }
      if (isReference()) {
        localStringBuffer.append(readReference());
      } else {
        localStringBuffer.append(readChar());
      }
    }
  }
  
  private void readAttribute(Element paramElement)
    throws ParseException, IOException
  {
    String str2 = readName();
    readEq();
    String str1 = readAttValue();
    if (paramElement.getAttribute(str2) != null)
    {
      ParseLog localParseLog = this.log_;
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("Element ");
      localStringBuffer.append(this);
      localStringBuffer.append(" contains attribute ");
      localStringBuffer.append(str2);
      localStringBuffer.append("more than once");
      localParseLog.warning(localStringBuffer.toString(), this.systemId_, getLineNumber());
    }
    paramElement.setAttribute(str2, str1);
  }
  
  private void readCdSect()
    throws ParseException, IOException
  {
    readSymbol(BEGIN_CDATA);
    Object localObject1 = null;
    int i = 0;
    for (;;)
    {
      if (isSymbol(END_CDATA))
      {
        readSymbol(END_CDATA);
        if (localObject1 != null)
        {
          ((StringBuffer)localObject1).append(this.tmpBuf_, 0, i);
          localObject1 = ((StringBuffer)localObject1).toString().toCharArray();
          this.handler_.characters((char[])localObject1, 0, localObject1.length);
        }
        else
        {
          this.handler_.characters(this.tmpBuf_, 0, i);
        }
        return;
      }
      int j = i;
      Object localObject2 = localObject1;
      if (i >= 255)
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = new StringBuffer(i);
        }
        ((StringBuffer)localObject2).append(this.tmpBuf_, 0, i);
        j = 0;
      }
      this.tmpBuf_[j] = readChar();
      i = j + 1;
      localObject1 = localObject2;
    }
  }
  
  private final char readChar()
    throws ParseException, IOException
  {
    if ((this.curPos_ >= this.endPos_) && (fillBuf() == -1)) {
      throw new ParseException(this, "unexpected end of expression.");
    }
    if (this.cbuf_[this.curPos_] == '\n') {
      this.lineNumber_ += 1;
    }
    char[] arrayOfChar = this.cbuf_;
    int i = this.curPos_;
    this.curPos_ = (i + 1);
    return arrayOfChar[i];
  }
  
  private final char readChar(char paramChar1, char paramChar2)
    throws ParseException, IOException
  {
    char c = readChar();
    if ((c != paramChar1) && (c != paramChar2)) {
      throw new ParseException(this, c, new char[] { paramChar1, paramChar2 });
    }
    return c;
  }
  
  private final char readChar(char paramChar1, char paramChar2, char paramChar3, char paramChar4)
    throws ParseException, IOException
  {
    char c = readChar();
    if ((c != paramChar1) && (c != paramChar2) && (c != paramChar3) && (c != paramChar4)) {
      throw new ParseException(this, c, new char[] { paramChar1, paramChar2, paramChar3, paramChar4 });
    }
    return c;
  }
  
  private final void readChar(char paramChar)
    throws ParseException, IOException
  {
    char c = readChar();
    if (c == paramChar) {
      return;
    }
    throw new ParseException(this, c, paramChar);
  }
  
  private char readCharRef()
    throws ParseException, IOException
  {
    readSymbol(CHARREF_BEGIN);
    int i;
    if (isChar('x'))
    {
      readChar();
      i = 16;
    }
    else
    {
      i = 10;
    }
    int k;
    for (int j = 0;; j = k)
    {
      if (isChar(';'))
      {
        readChar(';');
        localObject = new String(this.tmpBuf_, 0, j);
        try
        {
          j = Integer.parseInt((String)localObject, i);
          return (char)j;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          ParseLog localParseLog = this.log_;
          StringBuffer localStringBuffer = new StringBuffer();
          localStringBuffer.append("\"");
          localStringBuffer.append((String)localObject);
          localStringBuffer.append("\" is not a valid ");
          if (i == 16) {
            localObject = "hexadecimal";
          } else {
            localObject = "decimal";
          }
          localStringBuffer.append((String)localObject);
          localStringBuffer.append(" number");
          localParseLog.warning(localStringBuffer.toString(), this.systemId_, getLineNumber());
          return ' ';
        }
      }
      Object localObject = this.tmpBuf_;
      k = j + 1;
      localObject[j] = readChar();
      if (k >= 255)
      {
        this.log_.warning("Tmp buffer overflow on readCharRef", this.systemId_, getLineNumber());
        return ' ';
      }
    }
  }
  
  private final void readComment()
    throws ParseException, IOException
  {
    readSymbol(COMMENT_BEGIN);
    for (;;)
    {
      if (isSymbol(COMMENT_END))
      {
        readSymbol(COMMENT_END);
        return;
      }
      readChar();
    }
  }
  
  private void readContent()
    throws ParseException, IOException
  {
    readPossibleCharData();
    int i = 1;
    for (;;)
    {
      if (i == 0) {
        return;
      }
      if (isETag()) {}
      do
      {
        i = 0;
        break;
        if (isReference())
        {
          char[] arrayOfChar = readReference();
          this.handler_.characters(arrayOfChar, 0, arrayOfChar.length);
          break;
        }
        if (isCdSect())
        {
          readCdSect();
          break;
        }
        if (isPi())
        {
          readPi();
          break;
        }
        if (isComment())
        {
          readComment();
          break;
        }
      } while (!isChar('<'));
      readElement();
      readPossibleCharData();
    }
  }
  
  private void readDeclSep()
    throws ParseException, IOException
  {
    if (isPeReference()) {
      readPeReference();
    } else {
      readS();
    }
  }
  
  private void readDocTypeDecl()
    throws ParseException, IOException
  {
    readSymbol(DOCTYPE_BEGIN);
    readS();
    this.docTypeName_ = readName();
    if (isS())
    {
      readS();
      if ((!isChar('>')) && (!isChar('[')))
      {
        this.isExternalDtd_ = true;
        readExternalId();
        if (isS()) {
          readS();
        }
      }
    }
    if (isChar('['))
    {
      readChar();
      for (;;)
      {
        if (isChar(']'))
        {
          readChar(']');
          if (!isS()) {
            break;
          }
          readS();
          break;
        }
        if (isDeclSep()) {
          readDeclSep();
        } else {
          readMarkupDecl();
        }
      }
    }
    readChar('>');
  }
  
  private void readETag(Element paramElement)
    throws ParseException, IOException
  {
    readSymbol(BEGIN_ETAG);
    String str = readName();
    if (!str.equals(paramElement.getTagName()))
    {
      ParseLog localParseLog = this.log_;
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("end tag (");
      localStringBuffer.append(str);
      localStringBuffer.append(") does not match begin tag (");
      localStringBuffer.append(paramElement.getTagName());
      localStringBuffer.append(")");
      localParseLog.warning(localStringBuffer.toString(), this.systemId_, getLineNumber());
    }
    if (isS()) {
      readS();
    }
    readChar('>');
  }
  
  private final Element readElement()
    throws ParseException, IOException
  {
    Element localElement = new Element();
    boolean bool = readEmptyElementTagOrSTag(localElement);
    this.handler_.startElement(localElement);
    if (bool)
    {
      readContent();
      readETag(localElement);
    }
    this.handler_.endElement(localElement);
    return localElement;
  }
  
  private boolean readEmptyElementTagOrSTag(Element paramElement)
    throws ParseException, IOException
  {
    readChar('<');
    paramElement.setTagName(readName());
    for (;;)
    {
      if (!isS())
      {
        if (isS()) {
          readS();
        }
        boolean bool = isChar('>');
        if (bool) {
          readChar('>');
        } else {
          readSymbol(END_EMPTYTAG);
        }
        return bool;
      }
      readS();
      if (!isChar('/', '>')) {
        readAttribute(paramElement);
      }
    }
  }
  
  private String readEncodingDecl()
    throws ParseException, IOException
  {
    readSymbol(ENCODING);
    readEq();
    char c = readChar('\'', '"');
    StringBuffer localStringBuffer = new StringBuffer();
    for (;;)
    {
      if (isChar(c))
      {
        readChar(c);
        return localStringBuffer.toString();
      }
      localStringBuffer.append(readChar());
    }
  }
  
  private void readEntityDecl()
    throws ParseException, IOException
  {
    readSymbol(ENTITY_BEGIN);
    readS();
    String str;
    Object localObject1;
    Object localObject2;
    if (isChar('%'))
    {
      readChar('%');
      readS();
      str = readName();
      readS();
      if (isEntityValue()) {
        localObject1 = readEntityValue();
      } else {
        localObject1 = readExternalId();
      }
      localObject2 = this.pes_;
    }
    else
    {
      str = readName();
      readS();
      if (isEntityValue())
      {
        localObject1 = readEntityValue();
      }
      else
      {
        if (!isExternalId()) {
          break label174;
        }
        localObject2 = readExternalId();
        if (isS()) {
          readS();
        }
        localObject1 = localObject2;
        if (isSymbol(NDATA))
        {
          readSymbol(NDATA);
          readS();
          readName();
          localObject1 = localObject2;
        }
      }
      localObject2 = this.entities_;
    }
    ((Hashtable)localObject2).put(str, localObject1);
    if (isS()) {
      readS();
    }
    readChar('>');
    return;
    label174:
    throw new ParseException(this, "expecting double-quote, \"PUBLIC\" or \"SYSTEM\" while reading entity declaration");
  }
  
  private String readEntityRef()
    throws ParseException, IOException
  {
    readChar('&');
    String str2 = readName();
    Object localObject2 = (String)this.entities_.get(str2);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      String str1 = "";
      StringBuffer localStringBuffer;
      if (this.isExternalDtd_)
      {
        localObject2 = this.log_;
        localStringBuffer = new StringBuffer();
        localStringBuffer.append("&");
        localStringBuffer.append(str2);
        localObject1 = "; not found -- possibly defined in external DTD)";
      }
      else
      {
        localObject2 = this.log_;
        localStringBuffer = new StringBuffer();
        localStringBuffer.append("No declaration of &");
        localStringBuffer.append(str2);
        localObject1 = ";";
      }
      localStringBuffer.append((String)localObject1);
      ((ParseLog)localObject2).warning(localStringBuffer.toString(), this.systemId_, getLineNumber());
      localObject1 = str1;
    }
    readChar(';');
    return (String)localObject1;
  }
  
  private final String readEntityValue()
    throws ParseException, IOException
  {
    char c = readChar('\'', '"');
    StringBuffer localStringBuffer = new StringBuffer();
    for (;;)
    {
      if (isChar(c))
      {
        readChar(c);
        return localStringBuffer.toString();
      }
      if (isPeReference()) {
        localStringBuffer.append(readPeReference());
      } else if (isReference()) {
        localStringBuffer.append(readReference());
      } else {
        localStringBuffer.append(readChar());
      }
    }
  }
  
  private final void readEq()
    throws ParseException, IOException
  {
    if (isS()) {
      readS();
    }
    readChar('=');
    if (isS()) {
      readS();
    }
  }
  
  private String readExternalId()
    throws ParseException, IOException
  {
    if (isSymbol(SYSTEM))
    {
      readSymbol(SYSTEM);
    }
    else
    {
      if (!isSymbol(PUBLIC)) {
        break label57;
      }
      readSymbol(PUBLIC);
      readS();
      readPubidLiteral();
    }
    readS();
    readSystemLiteral();
    return "(WARNING: external ID not read)";
    label57:
    throw new ParseException(this, "expecting \"SYSTEM\" or \"PUBLIC\" while reading external ID");
  }
  
  private void readMarkupDecl()
    throws ParseException, IOException
  {
    if (isPi()) {
      readPi();
    } else if (isComment()) {
      readComment();
    } else if (isEntityDecl()) {
      readEntityDecl();
    } else {
      if (!isSymbol(MARKUPDECL_BEGIN)) {
        break label116;
      }
    }
    for (;;)
    {
      if (isChar('>'))
      {
        readChar('>');
        return;
      }
      if (isChar('\'', '"'))
      {
        char c = readChar();
        for (;;)
        {
          if (isChar(c))
          {
            readChar(c);
            break;
          }
          readChar();
        }
      }
      readChar();
    }
    label116:
    throw new ParseException(this, "expecting processing instruction, comment, or \"<!\"");
  }
  
  private void readMisc()
    throws ParseException, IOException
  {
    if (isComment())
    {
      readComment();
    }
    else if (isPi())
    {
      readPi();
    }
    else
    {
      if (!isS()) {
        break label40;
      }
      readS();
    }
    return;
    label40:
    throw new ParseException(this, "expecting comment or processing instruction or space");
  }
  
  private final String readName()
    throws ParseException, IOException
  {
    this.tmpBuf_[0] = readNameStartChar();
    int i = 1;
    Object localObject2;
    for (Object localObject1 = null;; localObject1 = localObject2)
    {
      if (!isNameChar())
      {
        if (localObject1 == null) {
          return Sparta.intern(new String(this.tmpBuf_, 0, i));
        }
        ((StringBuffer)localObject1).append(this.tmpBuf_, 0, i);
        return ((StringBuffer)localObject1).toString();
      }
      int j = i;
      localObject2 = localObject1;
      if (i >= 255)
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = new StringBuffer(i);
        }
        ((StringBuffer)localObject2).append(this.tmpBuf_, 0, i);
        j = 0;
      }
      this.tmpBuf_[j] = readChar();
      i = j + 1;
    }
  }
  
  private char readNameStartChar()
    throws ParseException, IOException
  {
    char c = readChar();
    if ((!isLetter(c)) && (c != '_') && (c != ':')) {
      throw new ParseException(this, c, "letter, underscore, colon");
    }
    return c;
  }
  
  private String readPeReference()
    throws ParseException, IOException
  {
    readChar('%');
    String str = readName();
    Object localObject2 = (String)this.pes_.get(str);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = "";
      localObject2 = this.log_;
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("No declaration of %");
      localStringBuffer.append(str);
      localStringBuffer.append(";");
      ((ParseLog)localObject2).warning(localStringBuffer.toString(), this.systemId_, getLineNumber());
    }
    readChar(';');
    return (String)localObject1;
  }
  
  private final void readPi()
    throws ParseException, IOException
  {
    readSymbol(PI_BEGIN);
    for (;;)
    {
      if (isSymbol(QU_END))
      {
        readSymbol(QU_END);
        return;
      }
      readChar();
    }
  }
  
  private void readPossibleCharData()
    throws ParseException, IOException
  {
    int i;
    for (;;)
    {
      i = 0;
      int j;
      do
      {
        if ((isChar('<')) || (isChar('&')) || (isSymbol(END_CDATA))) {
          break;
        }
        this.tmpBuf_[i] = readChar();
        if ((this.tmpBuf_[i] == '\r') && (peekChar() == '\n')) {
          this.tmpBuf_[i] = readChar();
        }
        j = i + 1;
        i = j;
      } while (j != 255);
      this.handler_.characters(this.tmpBuf_, 0, 255);
    }
    if (i > 0) {
      this.handler_.characters(this.tmpBuf_, 0, i);
    }
  }
  
  private void readProlog()
    throws ParseException, EncodingMismatchException, IOException
  {
    if (isXmlDecl()) {
      readXmlDecl();
    }
    for (;;)
    {
      if (!isMisc())
      {
        if (isDocTypeDecl())
        {
          readDocTypeDecl();
          while (isMisc()) {
            readMisc();
          }
        }
        return;
      }
      readMisc();
    }
  }
  
  private final void readPubidLiteral()
    throws ParseException, IOException
  {
    readSystemLiteral();
  }
  
  private final char[] readReference()
    throws ParseException, IOException
  {
    if (isSymbol(CHARREF_BEGIN)) {
      return new char[] { readCharRef() };
    }
    return readEntityRef().toCharArray();
  }
  
  private final void readS()
    throws ParseException, IOException
  {
    readChar(' ', '\t', '\r', '\n');
    for (;;)
    {
      if (!isChar(' ', '\t', '\r', '\n')) {
        return;
      }
      readChar();
    }
  }
  
  private final void readSymbol(char[] paramArrayOfChar)
    throws ParseException, IOException
  {
    int j = paramArrayOfChar.length;
    if ((this.endPos_ - this.curPos_ < j) && (fillBuf(j) <= 0))
    {
      this.ch_ = -1;
      throw new ParseException(this, "end of XML file", paramArrayOfChar);
    }
    char[] arrayOfChar = this.cbuf_;
    int i = this.endPos_;
    this.ch_ = arrayOfChar[(i - 1)];
    if (i - this.curPos_ >= j)
    {
      int k;
      for (i = 0;; i++)
      {
        if (i >= j)
        {
          this.curPos_ += j;
          return;
        }
        arrayOfChar = this.cbuf_;
        k = this.curPos_;
        if (arrayOfChar[(k + i)] != paramArrayOfChar[i]) {
          break;
        }
      }
      throw new ParseException(this, new String(arrayOfChar, k, j), paramArrayOfChar);
    }
    throw new ParseException(this, "end of XML file", paramArrayOfChar);
  }
  
  private final void readSystemLiteral()
    throws ParseException, IOException
  {
    char c = readChar();
    for (;;)
    {
      if (peekChar() == c)
      {
        readChar(c);
        return;
      }
      readChar();
    }
  }
  
  private void readVersionInfo()
    throws ParseException, IOException
  {
    readS();
    readSymbol(VERSION);
    readEq();
    char c = readChar('\'', '"');
    readVersionNum();
    readChar(c);
  }
  
  private void readVersionNum()
    throws ParseException, IOException
  {
    do
    {
      readChar();
    } while (isVersionNumChar());
  }
  
  private void readXmlDecl()
    throws ParseException, EncodingMismatchException, IOException
  {
    readSymbol(XML_BEGIN);
    readVersionInfo();
    if (isS()) {
      readS();
    }
    if (isEncodingDecl())
    {
      String str = readEncodingDecl();
      if ((this.encoding_ != null) && (!str.toLowerCase().equals(this.encoding_))) {
        throw new EncodingMismatchException(this.systemId_, str, this.encoding_);
      }
    }
    for (;;)
    {
      if (isSymbol(QU_END))
      {
        readSymbol(QU_END);
        return;
      }
      readChar();
    }
  }
  
  final String getHistory()
  {
    return "";
  }
  
  int getLastCharRead()
  {
    return this.ch_;
  }
  
  public int getLineNumber()
  {
    return this.lineNumber_;
  }
  
  ParseLog getLog()
  {
    return this.log_;
  }
  
  public String getSystemId()
  {
    return this.systemId_;
  }
  
  public String toString()
  {
    return this.systemId_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseCharStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */