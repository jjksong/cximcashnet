package com.hp.hpl.sparta;

public class EncodingMismatchException
  extends ParseException
{
  private String declaredEncoding_;
  
  EncodingMismatchException(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, 0, i, paramString2, localStringBuffer.toString());
    this.declaredEncoding_ = paramString2;
  }
  
  String getDeclaredEncoding()
  {
    return this.declaredEncoding_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/EncodingMismatchException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */