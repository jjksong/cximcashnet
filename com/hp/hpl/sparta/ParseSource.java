package com.hp.hpl.sparta;

public abstract interface ParseSource
{
  public static final ParseLog DEFAULT_LOG = new DefaultLog();
  public static final int MAXLOOKAHEAD = 71;
  
  public abstract int getLineNumber();
  
  public abstract String getSystemId();
  
  public abstract String toString();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */