package com.hp.hpl.sparta.xpath;

public class PositionEqualsExpr
  extends BooleanExpr
{
  private final int position_;
  
  public PositionEqualsExpr(int paramInt)
  {
    this.position_ = paramInt;
  }
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public int getPosition()
  {
    return this.position_;
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("[");
    localStringBuffer.append(this.position_);
    localStringBuffer.append("]");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/PositionEqualsExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */