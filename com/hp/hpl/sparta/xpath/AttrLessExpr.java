package com.hp.hpl.sparta.xpath;

public class AttrLessExpr
  extends AttrRelationalExpr
{
  public AttrLessExpr(String paramString, int paramInt)
  {
    super(paramString, paramInt);
  }
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public String toString()
  {
    return toString("<");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrLessExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */