package com.hp.hpl.sparta.xpath;

public class TextTest
  extends NodeTest
{
  static final TextTest INSTANCE = new TextTest();
  
  public void accept(Visitor paramVisitor)
    throws XPathException
  {
    paramVisitor.visit(this);
  }
  
  public boolean isStringValue()
  {
    return true;
  }
  
  public String toString()
  {
    return "text()";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/TextTest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */