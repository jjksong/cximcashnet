package com.hp.hpl.sparta.xpath;

import com.hp.hpl.sparta.Sparta;

public abstract class AttrCompareExpr
  extends AttrExpr
{
  private final String attrValue_;
  
  AttrCompareExpr(String paramString1, String paramString2)
  {
    super(paramString1);
    this.attrValue_ = Sparta.intern(paramString2);
  }
  
  public String getAttrValue()
  {
    return this.attrValue_;
  }
  
  protected String toString(String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("[");
    localStringBuffer.append(super.toString());
    localStringBuffer.append(paramString);
    localStringBuffer.append("'");
    localStringBuffer.append(this.attrValue_);
    localStringBuffer.append("']");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrCompareExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */