package com.hp.hpl.sparta.xpath;

public abstract class BooleanExpr
{
  public abstract void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/BooleanExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */