package com.hp.hpl.sparta.xpath;

import java.io.IOException;

public class XPathException
  extends Exception
{
  private Throwable cause_ = null;
  
  XPathException(XPath paramXPath, Exception paramException)
  {
    super(localStringBuffer.toString());
    this.cause_ = paramException;
  }
  
  public XPathException(XPath paramXPath, String paramString)
  {
    super(localStringBuffer.toString());
  }
  
  XPathException(XPath paramXPath, String paramString1, SimpleStreamTokenizer paramSimpleStreamTokenizer, String paramString2)
  {
    this(paramXPath, localStringBuffer.toString());
  }
  
  private static String toString(SimpleStreamTokenizer paramSimpleStreamTokenizer)
  {
    try
    {
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      localStringBuffer.append(tokenToString(paramSimpleStreamTokenizer));
      if (paramSimpleStreamTokenizer.ttype != -1)
      {
        paramSimpleStreamTokenizer.nextToken();
        localStringBuffer.append(tokenToString(paramSimpleStreamTokenizer));
        paramSimpleStreamTokenizer.pushBack();
      }
      paramSimpleStreamTokenizer = localStringBuffer.toString();
      return paramSimpleStreamTokenizer;
    }
    catch (IOException localIOException)
    {
      paramSimpleStreamTokenizer = new StringBuffer();
      paramSimpleStreamTokenizer.append("(cannot get  info: ");
      paramSimpleStreamTokenizer.append(localIOException);
      paramSimpleStreamTokenizer.append(")");
    }
    return paramSimpleStreamTokenizer.toString();
  }
  
  private static String tokenToString(SimpleStreamTokenizer paramSimpleStreamTokenizer)
  {
    StringBuffer localStringBuffer;
    switch (paramSimpleStreamTokenizer.ttype)
    {
    default: 
      localStringBuffer = new StringBuffer();
      localStringBuffer.append((char)paramSimpleStreamTokenizer.ttype);
    case -1: 
    case -2: 
      for (paramSimpleStreamTokenizer = localStringBuffer;; paramSimpleStreamTokenizer = localStringBuffer)
      {
        paramSimpleStreamTokenizer.append("");
        return paramSimpleStreamTokenizer.toString();
        return "<end of expression>";
        localStringBuffer = new StringBuffer();
        localStringBuffer.append(paramSimpleStreamTokenizer.nval);
      }
    }
    return paramSimpleStreamTokenizer.sval;
  }
  
  public Throwable getCause()
  {
    return this.cause_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/XPathException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */