package com.hp.hpl.sparta.xpath;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

public class XPath
{
  private static final int ASSERTION = 0;
  private static Hashtable cache_ = new Hashtable();
  private boolean absolute_;
  private Stack steps_ = new Stack();
  private String string_;
  
  private XPath(String paramString)
    throws XPathException
  {
    this(paramString, new InputStreamReader(new ByteArrayInputStream(paramString.getBytes())));
  }
  
  /* Error */
  private XPath(String paramString, java.io.Reader paramReader)
    throws XPathException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 51	java/lang/Object:<init>	()V
    //   4: aload_0
    //   5: new 53	java/util/Stack
    //   8: dup
    //   9: invokespecial 54	java/util/Stack:<init>	()V
    //   12: putfield 56	com/hp/hpl/sparta/xpath/XPath:steps_	Ljava/util/Stack;
    //   15: aload_0
    //   16: aload_1
    //   17: putfield 58	com/hp/hpl/sparta/xpath/XPath:string_	Ljava/lang/String;
    //   20: new 60	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer
    //   23: astore 4
    //   25: aload 4
    //   27: aload_2
    //   28: invokespecial 63	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:<init>	(Ljava/io/Reader;)V
    //   31: aload 4
    //   33: bipush 47
    //   35: invokevirtual 67	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:ordinaryChar	(C)V
    //   38: aload 4
    //   40: bipush 46
    //   42: invokevirtual 67	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:ordinaryChar	(C)V
    //   45: aload 4
    //   47: bipush 58
    //   49: bipush 58
    //   51: invokevirtual 71	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:wordChars	(CC)V
    //   54: aload 4
    //   56: bipush 95
    //   58: bipush 95
    //   60: invokevirtual 71	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:wordChars	(CC)V
    //   63: aload 4
    //   65: invokevirtual 75	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:nextToken	()I
    //   68: bipush 47
    //   70: if_icmpne +29 -> 99
    //   73: aload_0
    //   74: iconst_1
    //   75: putfield 77	com/hp/hpl/sparta/xpath/XPath:absolute_	Z
    //   78: aload 4
    //   80: invokevirtual 75	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:nextToken	()I
    //   83: bipush 47
    //   85: if_icmpne +19 -> 104
    //   88: aload 4
    //   90: invokevirtual 75	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:nextToken	()I
    //   93: pop
    //   94: iconst_1
    //   95: istore_3
    //   96: goto +10 -> 106
    //   99: aload_0
    //   100: iconst_0
    //   101: putfield 77	com/hp/hpl/sparta/xpath/XPath:absolute_	Z
    //   104: iconst_0
    //   105: istore_3
    //   106: aload_0
    //   107: getfield 56	com/hp/hpl/sparta/xpath/XPath:steps_	Ljava/util/Stack;
    //   110: astore_2
    //   111: new 79	com/hp/hpl/sparta/xpath/Step
    //   114: astore_1
    //   115: aload_1
    //   116: aload_0
    //   117: iload_3
    //   118: aload 4
    //   120: invokespecial 82	com/hp/hpl/sparta/xpath/Step:<init>	(Lcom/hp/hpl/sparta/xpath/XPath;ZLcom/hp/hpl/sparta/xpath/SimpleStreamTokenizer;)V
    //   123: aload_2
    //   124: aload_1
    //   125: invokevirtual 86	java/util/Stack:push	(Ljava/lang/Object;)Ljava/lang/Object;
    //   128: pop
    //   129: aload 4
    //   131: getfield 89	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:ttype	I
    //   134: bipush 47
    //   136: if_icmpeq +30 -> 166
    //   139: aload 4
    //   141: getfield 89	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:ttype	I
    //   144: iconst_m1
    //   145: if_icmpne +4 -> 149
    //   148: return
    //   149: new 28	com/hp/hpl/sparta/xpath/XPathException
    //   152: astore_1
    //   153: aload_1
    //   154: aload_0
    //   155: ldc 91
    //   157: aload 4
    //   159: ldc 93
    //   161: invokespecial 96	com/hp/hpl/sparta/xpath/XPathException:<init>	(Lcom/hp/hpl/sparta/xpath/XPath;Ljava/lang/String;Lcom/hp/hpl/sparta/xpath/SimpleStreamTokenizer;Ljava/lang/String;)V
    //   164: aload_1
    //   165: athrow
    //   166: aload 4
    //   168: invokevirtual 75	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:nextToken	()I
    //   171: bipush 47
    //   173: if_icmpne +14 -> 187
    //   176: aload 4
    //   178: invokevirtual 75	com/hp/hpl/sparta/xpath/SimpleStreamTokenizer:nextToken	()I
    //   181: pop
    //   182: iconst_1
    //   183: istore_3
    //   184: goto +5 -> 189
    //   187: iconst_0
    //   188: istore_3
    //   189: aload_0
    //   190: getfield 56	com/hp/hpl/sparta/xpath/XPath:steps_	Ljava/util/Stack;
    //   193: astore_2
    //   194: new 79	com/hp/hpl/sparta/xpath/Step
    //   197: dup
    //   198: aload_0
    //   199: iload_3
    //   200: aload 4
    //   202: invokespecial 82	com/hp/hpl/sparta/xpath/Step:<init>	(Lcom/hp/hpl/sparta/xpath/XPath;ZLcom/hp/hpl/sparta/xpath/SimpleStreamTokenizer;)V
    //   205: astore_1
    //   206: goto -83 -> 123
    //   209: astore_1
    //   210: new 28	com/hp/hpl/sparta/xpath/XPathException
    //   213: dup
    //   214: aload_0
    //   215: aload_1
    //   216: invokespecial 99	com/hp/hpl/sparta/xpath/XPathException:<init>	(Lcom/hp/hpl/sparta/xpath/XPath;Ljava/lang/Exception;)V
    //   219: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	220	0	this	XPath
    //   0	220	1	paramString	String
    //   0	220	2	paramReader	java.io.Reader
    //   95	105	3	bool	boolean
    //   23	178	4	localSimpleStreamTokenizer	SimpleStreamTokenizer
    // Exception table:
    //   from	to	target	type
    //   15	94	209	java/io/IOException
    //   99	104	209	java/io/IOException
    //   106	123	209	java/io/IOException
    //   123	148	209	java/io/IOException
    //   149	166	209	java/io/IOException
    //   166	182	209	java/io/IOException
    //   189	206	209	java/io/IOException
  }
  
  private XPath(boolean paramBoolean, Step[] paramArrayOfStep)
  {
    for (int i = 0;; i++)
    {
      if (i >= paramArrayOfStep.length)
      {
        this.absolute_ = paramBoolean;
        this.string_ = null;
        return;
      }
      this.steps_.addElement(paramArrayOfStep[i]);
    }
  }
  
  private String generateString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Enumeration localEnumeration = this.steps_.elements();
    for (int i = 1;; i = 0)
    {
      if (!localEnumeration.hasMoreElements()) {
        return localStringBuffer.toString();
      }
      Step localStep = (Step)localEnumeration.nextElement();
      if ((i == 0) || (this.absolute_))
      {
        localStringBuffer.append('/');
        if (localStep.isMultiLevel()) {
          localStringBuffer.append('/');
        }
      }
      localStringBuffer.append(localStep.toString());
    }
  }
  
  public static XPath get(String paramString)
    throws XPathException
  {
    synchronized (cache_)
    {
      XPath localXPath2 = (XPath)cache_.get(paramString);
      XPath localXPath1 = localXPath2;
      if (localXPath2 == null)
      {
        localXPath1 = new com/hp/hpl/sparta/xpath/XPath;
        localXPath1.<init>(paramString);
        cache_.put(paramString, localXPath1);
      }
      return localXPath1;
    }
  }
  
  public static XPath get(boolean paramBoolean, Step[] arg1)
  {
    XPath localXPath2 = new XPath(paramBoolean, ???);
    String str = localXPath2.toString();
    synchronized (cache_)
    {
      XPath localXPath1 = (XPath)cache_.get(str);
      if (localXPath1 == null)
      {
        cache_.put(str, localXPath2);
        return localXPath2;
      }
      return localXPath1;
    }
  }
  
  public static boolean isStringValue(String paramString)
    throws XPathException, IOException
  {
    return get(paramString).isStringValue();
  }
  
  public Object clone()
  {
    Step[] arrayOfStep = new Step[this.steps_.size()];
    Enumeration localEnumeration = this.steps_.elements();
    for (int i = 0;; i++)
    {
      if (i >= arrayOfStep.length) {
        return new XPath(this.absolute_, arrayOfStep);
      }
      arrayOfStep[i] = ((Step)localEnumeration.nextElement());
    }
  }
  
  public String getIndexingAttrName()
    throws XPathException
  {
    BooleanExpr localBooleanExpr = ((Step)this.steps_.peek()).getPredicate();
    if ((localBooleanExpr instanceof AttrExistsExpr)) {
      return ((AttrExistsExpr)localBooleanExpr).getAttrName();
    }
    throw new XPathException(this, "has no indexing attribute name (must end with predicate of the form [@attrName]");
  }
  
  public String getIndexingAttrNameOfEquals()
    throws XPathException
  {
    BooleanExpr localBooleanExpr = ((Step)this.steps_.peek()).getPredicate();
    if ((localBooleanExpr instanceof AttrEqualsExpr)) {
      return ((AttrEqualsExpr)localBooleanExpr).getAttrName();
    }
    return null;
  }
  
  public Enumeration getSteps()
  {
    return this.steps_.elements();
  }
  
  public boolean isAbsolute()
  {
    return this.absolute_;
  }
  
  public boolean isStringValue()
  {
    return ((Step)this.steps_.peek()).isStringValue();
  }
  
  public String toString()
  {
    if (this.string_ == null) {
      this.string_ = generateString();
    }
    return this.string_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/XPath.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */