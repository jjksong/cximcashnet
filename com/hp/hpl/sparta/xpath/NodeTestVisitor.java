package com.hp.hpl.sparta.xpath;

public abstract interface NodeTestVisitor
{
  public abstract void visit(AllElementTest paramAllElementTest);
  
  public abstract void visit(AttrTest paramAttrTest);
  
  public abstract void visit(ElementTest paramElementTest);
  
  public abstract void visit(ParentNodeTest paramParentNodeTest)
    throws XPathException;
  
  public abstract void visit(TextTest paramTextTest);
  
  public abstract void visit(ThisNodeTest paramThisNodeTest);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/NodeTestVisitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */