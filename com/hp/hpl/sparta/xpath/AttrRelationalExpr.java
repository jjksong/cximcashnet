package com.hp.hpl.sparta.xpath;

public abstract class AttrRelationalExpr
  extends AttrExpr
{
  private final int attrValue_;
  
  AttrRelationalExpr(String paramString, int paramInt)
  {
    super(paramString);
    this.attrValue_ = paramInt;
  }
  
  public double getAttrValue()
  {
    return this.attrValue_;
  }
  
  protected String toString(String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("[");
    localStringBuffer.append(super.toString());
    localStringBuffer.append(paramString);
    localStringBuffer.append("'");
    localStringBuffer.append(this.attrValue_);
    localStringBuffer.append("']");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrRelationalExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */