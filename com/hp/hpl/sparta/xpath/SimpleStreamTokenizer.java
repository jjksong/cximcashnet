package com.hp.hpl.sparta.xpath;

import java.io.IOException;
import java.io.Reader;

public class SimpleStreamTokenizer
{
  private static final int QUOTE = -6;
  public static final int TT_EOF = -1;
  public static final int TT_NUMBER = -2;
  public static final int TT_WORD = -3;
  private static final int WHITESPACE = -5;
  private final StringBuffer buf_ = new StringBuffer();
  private final int[] charType_ = new int['Ā'];
  private char inQuote_;
  private int nextType_;
  public int nval = Integer.MIN_VALUE;
  private boolean pushedBack_;
  private final Reader reader_;
  public String sval = "";
  public int ttype = Integer.MIN_VALUE;
  
  public SimpleStreamTokenizer(Reader paramReader)
    throws IOException
  {
    int i = 0;
    this.pushedBack_ = false;
    this.inQuote_ = '\000';
    this.reader_ = paramReader;
    for (;;)
    {
      if (i >= this.charType_.length)
      {
        nextToken();
        return;
      }
      if (((65 <= i) && (i <= 90)) || ((97 <= i) && (i <= 122)) || (i == 45)) {
        this.charType_[i] = -3;
      } else if ((48 <= i) && (i <= 57)) {
        this.charType_[i] = -2;
      } else if ((i >= 0) && (i <= 32)) {
        this.charType_[i] = -5;
      } else {
        this.charType_[i] = i;
      }
      i = (char)(i + 1);
    }
  }
  
  public int nextToken()
    throws IOException
  {
    if (this.pushedBack_)
    {
      this.pushedBack_ = false;
      return this.ttype;
    }
    this.ttype = this.nextType_;
    int j;
    label215:
    do
    {
      int k = 0;
      int n;
      int i;
      int m;
      do
      {
        n = this.reader_.read();
        if (n == -1)
        {
          if (this.inQuote_ == 0) {
            i = -1;
          } else {
            throw new IOException("Unterminated quote");
          }
        }
        else {
          i = this.charType_[n];
        }
        if ((this.inQuote_ == 0) && (i == -5)) {
          m = 1;
        } else {
          m = 0;
        }
        if ((k == 0) && (m == 0)) {
          j = 0;
        } else {
          j = 1;
        }
        k = j;
      } while (m != 0);
      if ((i == 39) || (i == 34))
      {
        k = this.inQuote_;
        if (k == 0) {
          this.inQuote_ = ((char)i);
        } else if (k == i) {
          this.inQuote_ = '\000';
        }
      }
      k = this.inQuote_;
      if (k != 0) {
        i = k;
      }
      if (j == 0)
      {
        j = this.ttype;
        if (((j < -1) || (j == 39) || (j == 34)) && (this.ttype == i))
        {
          j = 0;
          break label215;
        }
      }
      j = 1;
      if (j != 0)
      {
        k = this.ttype;
        if ((k != 34) && (k != 39)) {
          switch (k)
          {
          default: 
            break;
          case -2: 
            this.nval = Integer.parseInt(this.buf_.toString());
            break;
          }
        }
        for (String str = this.buf_.toString();; str = this.buf_.toString().substring(1, this.buf_.length() - 1))
        {
          this.sval = str;
          this.buf_.setLength(0);
          break;
        }
        if (i != -5)
        {
          if (i == -6) {
            k = n;
          } else {
            k = i;
          }
          this.nextType_ = k;
        }
      }
      if ((i != 34) && (i != 39)) {}
      switch (i)
      {
      default: 
        break;
      case -3: 
      case -2: 
        this.buf_.append((char)n);
      }
    } while (j == 0);
    return this.ttype;
  }
  
  public void ordinaryChar(char paramChar)
  {
    this.charType_[paramChar] = paramChar;
  }
  
  public void pushBack()
  {
    this.pushedBack_ = true;
  }
  
  public String toString()
  {
    int i = this.ttype;
    if (i != 34)
    {
      if (i != 39) {}
      switch (i)
      {
      default: 
        localStringBuffer = new StringBuffer();
        localStringBuffer.append("'");
        localStringBuffer.append((char)this.ttype);
        localStringBuffer.append("'");
        return localStringBuffer.toString();
      case -1: 
        return "(EOF)";
      case -2: 
        return Integer.toString(this.nval);
        localStringBuffer = new StringBuffer();
        localStringBuffer.append("'");
        localStringBuffer.append(this.sval);
        localStringBuffer.append("'");
        return localStringBuffer.toString();
      }
    }
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("\"");
    localStringBuffer.append(this.sval);
    localStringBuffer.append("\"");
    return localStringBuffer.toString();
  }
  
  public void wordChars(char paramChar1, char paramChar2)
  {
    for (;;)
    {
      if (paramChar1 > paramChar2) {
        return;
      }
      this.charType_[paramChar1] = -3;
      paramChar1 = (char)(paramChar1 + '\001');
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/SimpleStreamTokenizer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */