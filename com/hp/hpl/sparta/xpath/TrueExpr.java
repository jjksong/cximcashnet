package com.hp.hpl.sparta.xpath;

public class TrueExpr
  extends BooleanExpr
{
  static final TrueExpr INSTANCE = new TrueExpr();
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public String toString()
  {
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/TrueExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */