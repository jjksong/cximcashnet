package com.hp.hpl.sparta.xpath;

public class TextExistsExpr
  extends BooleanExpr
{
  static final TextExistsExpr INSTANCE = new TextExistsExpr();
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public String toString()
  {
    return "[text()]";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/TextExistsExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */