package com.hp.hpl.sparta.xpath;

import java.io.IOException;

public class ExprFactory
{
  static BooleanExpr createExpr(XPath paramXPath, SimpleStreamTokenizer paramSimpleStreamTokenizer)
    throws XPathException, IOException
  {
    int i = paramSimpleStreamTokenizer.ttype;
    if (i != 64)
    {
      switch (i)
      {
      default: 
        throw new XPathException(paramXPath, "at beginning of expression", paramSimpleStreamTokenizer, "@, number, or text()");
      case -2: 
        i = paramSimpleStreamTokenizer.nval;
        paramSimpleStreamTokenizer.nextToken();
        return new PositionEqualsExpr(i);
      }
      if (paramSimpleStreamTokenizer.sval.equals("text"))
      {
        if (paramSimpleStreamTokenizer.nextToken() == 40)
        {
          if (paramSimpleStreamTokenizer.nextToken() == 41)
          {
            i = paramSimpleStreamTokenizer.nextToken();
            if (i != 33)
            {
              if (i != 61) {
                return TextExistsExpr.INSTANCE;
              }
              paramSimpleStreamTokenizer.nextToken();
              if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39)) {
                throw new XPathException(paramXPath, "right hand side of equals", paramSimpleStreamTokenizer, "quoted string");
              }
              paramXPath = paramSimpleStreamTokenizer.sval;
              paramSimpleStreamTokenizer.nextToken();
              return new TextEqualsExpr(paramXPath);
            }
            paramSimpleStreamTokenizer.nextToken();
            if (paramSimpleStreamTokenizer.ttype == 61)
            {
              paramSimpleStreamTokenizer.nextToken();
              if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39)) {
                throw new XPathException(paramXPath, "right hand side of !=", paramSimpleStreamTokenizer, "quoted string");
              }
              paramXPath = paramSimpleStreamTokenizer.sval;
              paramSimpleStreamTokenizer.nextToken();
              return new TextNotEqualsExpr(paramXPath);
            }
            throw new XPathException(paramXPath, "after !", paramSimpleStreamTokenizer, "=");
          }
          throw new XPathException(paramXPath, "after text(", paramSimpleStreamTokenizer, ")");
        }
        throw new XPathException(paramXPath, "after text", paramSimpleStreamTokenizer, "(");
      }
      throw new XPathException(paramXPath, "at beginning of expression", paramSimpleStreamTokenizer, "text()");
    }
    if (paramSimpleStreamTokenizer.nextToken() == -3)
    {
      String str = paramSimpleStreamTokenizer.sval;
      i = paramSimpleStreamTokenizer.nextToken();
      if (i != 33)
      {
        switch (i)
        {
        default: 
          return new AttrExistsExpr(str);
        case 62: 
          paramSimpleStreamTokenizer.nextToken();
          if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39))
          {
            if (paramSimpleStreamTokenizer.ttype == -2) {
              i = paramSimpleStreamTokenizer.nval;
            } else {
              throw new XPathException(paramXPath, "right hand side of greater-than", paramSimpleStreamTokenizer, "quoted string or number");
            }
          }
          else {
            i = Integer.parseInt(paramSimpleStreamTokenizer.sval);
          }
          paramSimpleStreamTokenizer.nextToken();
          return new AttrGreaterExpr(str, i);
        case 61: 
          paramSimpleStreamTokenizer.nextToken();
          if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39)) {
            throw new XPathException(paramXPath, "right hand side of equals", paramSimpleStreamTokenizer, "quoted string");
          }
          paramXPath = paramSimpleStreamTokenizer.sval;
          paramSimpleStreamTokenizer.nextToken();
          return new AttrEqualsExpr(str, paramXPath);
        }
        paramSimpleStreamTokenizer.nextToken();
        if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39))
        {
          if (paramSimpleStreamTokenizer.ttype == -2) {
            i = paramSimpleStreamTokenizer.nval;
          } else {
            throw new XPathException(paramXPath, "right hand side of less-than", paramSimpleStreamTokenizer, "quoted string or number");
          }
        }
        else {
          i = Integer.parseInt(paramSimpleStreamTokenizer.sval);
        }
        paramSimpleStreamTokenizer.nextToken();
        return new AttrLessExpr(str, i);
      }
      paramSimpleStreamTokenizer.nextToken();
      if (paramSimpleStreamTokenizer.ttype == 61)
      {
        paramSimpleStreamTokenizer.nextToken();
        if ((paramSimpleStreamTokenizer.ttype != 34) && (paramSimpleStreamTokenizer.ttype != 39)) {
          throw new XPathException(paramXPath, "right hand side of !=", paramSimpleStreamTokenizer, "quoted string");
        }
        paramXPath = paramSimpleStreamTokenizer.sval;
        paramSimpleStreamTokenizer.nextToken();
        return new AttrNotEqualsExpr(str, paramXPath);
      }
      throw new XPathException(paramXPath, "after !", paramSimpleStreamTokenizer, "=");
    }
    throw new XPathException(paramXPath, "after @", paramSimpleStreamTokenizer, "name");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/ExprFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */