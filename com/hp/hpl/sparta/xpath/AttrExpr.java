package com.hp.hpl.sparta.xpath;

public abstract class AttrExpr
  extends BooleanExpr
{
  private final String attrName_;
  
  AttrExpr(String paramString)
  {
    this.attrName_ = paramString;
  }
  
  public String getAttrName()
  {
    return this.attrName_;
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("@");
    localStringBuffer.append(this.attrName_);
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */