package com.hp.hpl.sparta.xpath;

public abstract class TextCompareExpr
  extends BooleanExpr
{
  private final String value_;
  
  TextCompareExpr(String paramString)
  {
    this.value_ = paramString;
  }
  
  public String getValue()
  {
    return this.value_;
  }
  
  protected String toString(String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("[text()");
    localStringBuffer.append(paramString);
    localStringBuffer.append("'");
    localStringBuffer.append(this.value_);
    localStringBuffer.append("']");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/TextCompareExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */