package com.hp.hpl.sparta.xpath;

public class AttrExistsExpr
  extends AttrExpr
{
  AttrExistsExpr(String paramString)
  {
    super(paramString);
  }
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("[");
    localStringBuffer.append(super.toString());
    localStringBuffer.append("]");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrExistsExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */