package com.hp.hpl.sparta.xpath;

public abstract interface BooleanExprVisitor
{
  public abstract void visit(AttrEqualsExpr paramAttrEqualsExpr)
    throws XPathException;
  
  public abstract void visit(AttrExistsExpr paramAttrExistsExpr)
    throws XPathException;
  
  public abstract void visit(AttrGreaterExpr paramAttrGreaterExpr)
    throws XPathException;
  
  public abstract void visit(AttrLessExpr paramAttrLessExpr)
    throws XPathException;
  
  public abstract void visit(AttrNotEqualsExpr paramAttrNotEqualsExpr)
    throws XPathException;
  
  public abstract void visit(PositionEqualsExpr paramPositionEqualsExpr)
    throws XPathException;
  
  public abstract void visit(TextEqualsExpr paramTextEqualsExpr)
    throws XPathException;
  
  public abstract void visit(TextExistsExpr paramTextExistsExpr)
    throws XPathException;
  
  public abstract void visit(TextNotEqualsExpr paramTextNotEqualsExpr)
    throws XPathException;
  
  public abstract void visit(TrueExpr paramTrueExpr);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/BooleanExprVisitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */