package com.hp.hpl.sparta.xpath;

public class ParentNodeTest
  extends NodeTest
{
  static final ParentNodeTest INSTANCE = new ParentNodeTest();
  
  public void accept(Visitor paramVisitor)
    throws XPathException
  {
    paramVisitor.visit(this);
  }
  
  public boolean isStringValue()
  {
    return false;
  }
  
  public String toString()
  {
    return "..";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/ParentNodeTest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */