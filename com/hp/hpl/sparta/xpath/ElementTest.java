package com.hp.hpl.sparta.xpath;

import com.hp.hpl.sparta.Sparta;

public class ElementTest
  extends NodeTest
{
  private final String tagName_;
  
  ElementTest(String paramString)
  {
    this.tagName_ = Sparta.intern(paramString);
  }
  
  public void accept(Visitor paramVisitor)
  {
    paramVisitor.visit(this);
  }
  
  public String getTagName()
  {
    return this.tagName_;
  }
  
  public boolean isStringValue()
  {
    return false;
  }
  
  public String toString()
  {
    return this.tagName_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/ElementTest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */