package com.hp.hpl.sparta.xpath;

public class AttrNotEqualsExpr
  extends AttrCompareExpr
{
  AttrNotEqualsExpr(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }
  
  public void accept(BooleanExprVisitor paramBooleanExprVisitor)
    throws XPathException
  {
    paramBooleanExprVisitor.visit(this);
  }
  
  public String toString()
  {
    return toString("!=");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrNotEqualsExpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */