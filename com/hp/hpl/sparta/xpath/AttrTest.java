package com.hp.hpl.sparta.xpath;

public class AttrTest
  extends NodeTest
{
  private final String attrName_;
  
  AttrTest(String paramString)
  {
    this.attrName_ = paramString;
  }
  
  public void accept(Visitor paramVisitor)
  {
    paramVisitor.visit(this);
  }
  
  public String getAttrName()
  {
    return this.attrName_;
  }
  
  public boolean isStringValue()
  {
    return true;
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("@");
    localStringBuffer.append(this.attrName_);
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/AttrTest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */