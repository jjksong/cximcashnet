package com.hp.hpl.sparta.xpath;

import java.io.IOException;

public class Step
{
  public static Step DOT = new Step(ThisNodeTest.INSTANCE, TrueExpr.INSTANCE);
  private final boolean multiLevel_;
  private final NodeTest nodeTest_;
  private final BooleanExpr predicate_;
  
  Step(NodeTest paramNodeTest, BooleanExpr paramBooleanExpr)
  {
    this.nodeTest_ = paramNodeTest;
    this.predicate_ = paramBooleanExpr;
    this.multiLevel_ = false;
  }
  
  Step(XPath paramXPath, boolean paramBoolean, SimpleStreamTokenizer paramSimpleStreamTokenizer)
    throws XPathException, IOException
  {
    this.multiLevel_ = paramBoolean;
    int i = paramSimpleStreamTokenizer.ttype;
    Object localObject;
    if (i != -3) {
      if (i != 42)
      {
        if (i != 46)
        {
          if (i == 64)
          {
            if (paramSimpleStreamTokenizer.nextToken() == -3) {
              localObject = new AttrTest(paramSimpleStreamTokenizer.sval);
            } else {
              throw new XPathException(paramXPath, "after @ in node test", paramSimpleStreamTokenizer, "name");
            }
          }
          else {
            throw new XPathException(paramXPath, "at begininning of step", paramSimpleStreamTokenizer, "'.' or '*' or name");
          }
        }
        else if (paramSimpleStreamTokenizer.nextToken() == 46)
        {
          localObject = ParentNodeTest.INSTANCE;
        }
        else
        {
          paramSimpleStreamTokenizer.pushBack();
          localObject = ThisNodeTest.INSTANCE;
        }
      }
      else {
        localObject = AllElementTest.INSTANCE;
      }
    }
    for (;;)
    {
      this.nodeTest_ = ((NodeTest)localObject);
      break;
      if (paramSimpleStreamTokenizer.sval.equals("text"))
      {
        if ((paramSimpleStreamTokenizer.nextToken() == 40) && (paramSimpleStreamTokenizer.nextToken() == 41)) {
          localObject = TextTest.INSTANCE;
        } else {
          throw new XPathException(paramXPath, "after text", paramSimpleStreamTokenizer, "()");
        }
      }
      else {
        localObject = new ElementTest(paramSimpleStreamTokenizer.sval);
      }
    }
    if (paramSimpleStreamTokenizer.nextToken() == 91)
    {
      paramSimpleStreamTokenizer.nextToken();
      this.predicate_ = ExprFactory.createExpr(paramXPath, paramSimpleStreamTokenizer);
      if (paramSimpleStreamTokenizer.ttype == 93) {
        paramSimpleStreamTokenizer.nextToken();
      } else {
        throw new XPathException(paramXPath, "after predicate expression", paramSimpleStreamTokenizer, "]");
      }
    }
    else
    {
      this.predicate_ = TrueExpr.INSTANCE;
    }
  }
  
  public NodeTest getNodeTest()
  {
    return this.nodeTest_;
  }
  
  public BooleanExpr getPredicate()
  {
    return this.predicate_;
  }
  
  public boolean isMultiLevel()
  {
    return this.multiLevel_;
  }
  
  public boolean isStringValue()
  {
    return this.nodeTest_.isStringValue();
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(this.nodeTest_.toString());
    localStringBuffer.append(this.predicate_.toString());
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/xpath/Step.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */