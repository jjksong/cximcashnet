package com.hp.hpl.sparta;

public class DOMException
  extends Exception
{
  public static final short DOMSTRING_SIZE_ERR = 2;
  public static final short HIERARCHY_REQUEST_ERR = 3;
  public static final short NOT_FOUND_ERR = 8;
  public short code;
  
  public DOMException(short paramShort, String paramString)
  {
    super(paramString);
    this.code = paramShort;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/DOMException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */