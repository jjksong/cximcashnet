package com.hp.hpl.sparta;

public abstract interface ParseLog
{
  public abstract void error(String paramString1, String paramString2, int paramInt);
  
  public abstract void note(String paramString1, String paramString2, int paramInt);
  
  public abstract void warning(String paramString1, String paramString2, int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */