package com.hp.hpl.sparta;

import com.hp.hpl.sparta.xpath.BooleanExpr;
import com.hp.hpl.sparta.xpath.ElementTest;
import com.hp.hpl.sparta.xpath.Step;
import com.hp.hpl.sparta.xpath.XPathException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Enumeration;

public abstract class Node
{
  private Object annotation_ = null;
  private Document doc_ = null;
  private int hash_ = 0;
  private Node nextSibling_ = null;
  private Element parentNode_ = null;
  private Node previousSibling_ = null;
  
  protected static void htmlEncode(Writer paramWriter, String paramString)
    throws IOException
  {
    int m = paramString.length();
    int j = 0;
    int k;
    for (int i = 0;; i = k)
    {
      if (j >= m)
      {
        if (i < m) {
          paramWriter.write(paramString, i, m - i);
        }
        return;
      }
      k = paramString.charAt(j);
      Object localObject;
      if (k >= 128)
      {
        localObject = new StringBuffer();
        ((StringBuffer)localObject).append("&#");
        ((StringBuffer)localObject).append(k);
        ((StringBuffer)localObject).append(";");
        localObject = ((StringBuffer)localObject).toString();
      }
      else if (k != 34)
      {
        if (k != 60)
        {
          if (k != 62) {
            switch (k)
            {
            default: 
              localObject = null;
              break;
            case 39: 
              localObject = "&#39;";
              break;
            case 38: 
              localObject = "&amp;";
              break;
            }
          } else {
            localObject = "&gt;";
          }
        }
        else {
          localObject = "&lt;";
        }
      }
      else
      {
        localObject = "&quot;";
      }
      k = i;
      if (localObject != null)
      {
        paramWriter.write(paramString, i, j - i);
        paramWriter.write((String)localObject);
        k = j + 1;
      }
      j++;
    }
  }
  
  public abstract Object clone();
  
  protected abstract int computeHashCode();
  
  public Object getAnnotation()
  {
    return this.annotation_;
  }
  
  public Node getNextSibling()
  {
    return this.nextSibling_;
  }
  
  public Document getOwnerDocument()
  {
    return this.doc_;
  }
  
  public Element getParentNode()
  {
    return this.parentNode_;
  }
  
  public Node getPreviousSibling()
  {
    return this.previousSibling_;
  }
  
  public int hashCode()
  {
    if (this.hash_ == 0) {
      this.hash_ = computeHashCode();
    }
    return this.hash_;
  }
  
  void insertAtEndOfLinkedList(Node paramNode)
  {
    this.previousSibling_ = paramNode;
    if (paramNode != null) {
      paramNode.nextSibling_ = this;
    }
  }
  
  Element makeMatching(Element paramElement, Step paramStep, String paramString)
    throws ParseException, XPathException
  {
    Object localObject = paramStep.getNodeTest();
    if ((localObject instanceof ElementTest))
    {
      String str = ((ElementTest)localObject).getTagName();
      localObject = new Element(str);
      paramStep.getPredicate().accept(new Node.1(this, (Element)localObject, paramElement, paramString, str));
      return (Element)localObject;
    }
    paramElement = new StringBuffer();
    paramElement.append("\"");
    paramElement.append(localObject);
    paramElement.append("\" in \"");
    paramElement.append(paramString);
    paramElement.append("\" is not an element test");
    throw new ParseException(paramElement.toString());
  }
  
  void notifyObservers()
  {
    this.hash_ = 0;
    Document localDocument = this.doc_;
    if (localDocument != null) {
      localDocument.notifyObservers();
    }
  }
  
  void removeFromLinkedList()
  {
    Node localNode = this.previousSibling_;
    if (localNode != null) {
      localNode.nextSibling_ = this.nextSibling_;
    }
    localNode = this.nextSibling_;
    if (localNode != null) {
      localNode.previousSibling_ = this.previousSibling_;
    }
    this.nextSibling_ = null;
    this.previousSibling_ = null;
  }
  
  void replaceInLinkedList(Node paramNode)
  {
    Node localNode = this.previousSibling_;
    if (localNode != null) {
      localNode.nextSibling_ = paramNode;
    }
    localNode = this.nextSibling_;
    if (localNode != null) {
      localNode.previousSibling_ = paramNode;
    }
    paramNode.nextSibling_ = this.nextSibling_;
    paramNode.previousSibling_ = this.previousSibling_;
    this.nextSibling_ = null;
    this.previousSibling_ = null;
  }
  
  public void setAnnotation(Object paramObject)
  {
    this.annotation_ = paramObject;
  }
  
  void setOwnerDocument(Document paramDocument)
  {
    this.doc_ = paramDocument;
  }
  
  void setParentNode(Element paramElement)
  {
    this.parentNode_ = paramElement;
  }
  
  public String toString()
  {
    try
    {
      Object localObject = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject).<init>();
      OutputStreamWriter localOutputStreamWriter = new java/io/OutputStreamWriter;
      localOutputStreamWriter.<init>((OutputStream)localObject);
      toString(localOutputStreamWriter);
      localOutputStreamWriter.flush();
      localObject = new String(((ByteArrayOutputStream)localObject).toByteArray());
      return (String)localObject;
    }
    catch (IOException localIOException) {}
    return super.toString();
  }
  
  abstract void toString(Writer paramWriter)
    throws IOException;
  
  public String toXml()
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(localByteArrayOutputStream);
    toXml(localOutputStreamWriter);
    localOutputStreamWriter.flush();
    return new String(localByteArrayOutputStream.toByteArray());
  }
  
  abstract void toXml(Writer paramWriter)
    throws IOException;
  
  public abstract Element xpathSelectElement(String paramString)
    throws ParseException;
  
  public abstract Enumeration xpathSelectElements(String paramString)
    throws ParseException;
  
  public abstract String xpathSelectString(String paramString)
    throws ParseException;
  
  public abstract Enumeration xpathSelectStrings(String paramString)
    throws ParseException;
  
  /* Error */
  public boolean xpathSetStrings(String paramString1, String paramString2)
    throws ParseException
  {
    // Byte code:
    //   0: aload_1
    //   1: bipush 47
    //   3: invokevirtual 201	java/lang/String:lastIndexOf	(I)I
    //   6: istore_3
    //   7: iload_3
    //   8: iconst_1
    //   9: iadd
    //   10: istore 4
    //   12: aload_1
    //   13: iload 4
    //   15: invokevirtual 205	java/lang/String:substring	(I)Ljava/lang/String;
    //   18: ldc -49
    //   20: invokevirtual 211	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   23: ifne +80 -> 103
    //   26: aload_1
    //   27: iload 4
    //   29: invokevirtual 52	java/lang/String:charAt	(I)C
    //   32: bipush 64
    //   34: if_icmpne +6 -> 40
    //   37: goto +66 -> 103
    //   40: new 104	com/hp/hpl/sparta/ParseException
    //   43: astore 6
    //   45: new 54	java/lang/StringBuffer
    //   48: astore_2
    //   49: aload_2
    //   50: invokespecial 55	java/lang/StringBuffer:<init>	()V
    //   53: aload_2
    //   54: ldc -43
    //   56: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   59: pop
    //   60: aload_2
    //   61: aload_1
    //   62: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   65: pop
    //   66: aload_2
    //   67: ldc -41
    //   69: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   72: pop
    //   73: aload_2
    //   74: aload_1
    //   75: iload 4
    //   77: invokevirtual 52	java/lang/String:charAt	(I)C
    //   80: invokevirtual 218	java/lang/StringBuffer:append	(C)Ljava/lang/StringBuffer;
    //   83: pop
    //   84: aload_2
    //   85: ldc -36
    //   87: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   90: pop
    //   91: aload 6
    //   93: aload_2
    //   94: invokevirtual 70	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   97: invokespecial 146	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;)V
    //   100: aload 6
    //   102: athrow
    //   103: iconst_0
    //   104: istore 5
    //   106: aload_1
    //   107: iconst_0
    //   108: iload_3
    //   109: invokevirtual 223	java/lang/String:substring	(II)Ljava/lang/String;
    //   112: astore 7
    //   114: aload_1
    //   115: iload 4
    //   117: invokevirtual 52	java/lang/String:charAt	(I)C
    //   120: bipush 64
    //   122: if_icmpne +129 -> 251
    //   125: aload_1
    //   126: iload_3
    //   127: iconst_2
    //   128: iadd
    //   129: invokevirtual 205	java/lang/String:substring	(I)Ljava/lang/String;
    //   132: astore 6
    //   134: aload 6
    //   136: invokevirtual 42	java/lang/String:length	()I
    //   139: ifeq +64 -> 203
    //   142: aload_0
    //   143: aload 7
    //   145: invokevirtual 225	com/hp/hpl/sparta/Node:xpathSelectElements	(Ljava/lang/String;)Ljava/util/Enumeration;
    //   148: astore 8
    //   150: aload 8
    //   152: invokeinterface 231 1 0
    //   157: ifne +6 -> 163
    //   160: goto +118 -> 278
    //   163: aload 8
    //   165: invokeinterface 234 1 0
    //   170: checkcast 119	com/hp/hpl/sparta/Element
    //   173: astore 7
    //   175: aload_2
    //   176: aload 7
    //   178: aload 6
    //   180: invokevirtual 237	com/hp/hpl/sparta/Element:getAttribute	(Ljava/lang/String;)Ljava/lang/String;
    //   183: invokevirtual 211	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   186: ifne -36 -> 150
    //   189: aload 7
    //   191: aload 6
    //   193: aload_2
    //   194: invokevirtual 241	com/hp/hpl/sparta/Element:setAttribute	(Ljava/lang/String;Ljava/lang/String;)V
    //   197: iconst_1
    //   198: istore 5
    //   200: goto -50 -> 150
    //   203: new 104	com/hp/hpl/sparta/ParseException
    //   206: astore_2
    //   207: new 54	java/lang/StringBuffer
    //   210: astore 6
    //   212: aload 6
    //   214: invokespecial 55	java/lang/StringBuffer:<init>	()V
    //   217: aload 6
    //   219: ldc -13
    //   221: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   224: pop
    //   225: aload 6
    //   227: aload_1
    //   228: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   231: pop
    //   232: aload 6
    //   234: ldc -11
    //   236: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   239: pop
    //   240: aload_2
    //   241: aload 6
    //   243: invokevirtual 70	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   246: invokespecial 146	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;)V
    //   249: aload_2
    //   250: athrow
    //   251: aload_0
    //   252: aload 7
    //   254: invokevirtual 225	com/hp/hpl/sparta/Node:xpathSelectElements	(Ljava/lang/String;)Ljava/util/Enumeration;
    //   257: astore 7
    //   259: aload 7
    //   261: invokeinterface 231 1 0
    //   266: istore 5
    //   268: aload 7
    //   270: invokeinterface 231 1 0
    //   275: ifne +6 -> 281
    //   278: iload 5
    //   280: ireturn
    //   281: aload 7
    //   283: invokeinterface 234 1 0
    //   288: checkcast 119	com/hp/hpl/sparta/Element
    //   291: astore 8
    //   293: new 247	java/util/Vector
    //   296: astore 9
    //   298: aload 9
    //   300: invokespecial 248	java/util/Vector:<init>	()V
    //   303: aload 8
    //   305: invokevirtual 251	com/hp/hpl/sparta/Element:getFirstChild	()Lcom/hp/hpl/sparta/Node;
    //   308: astore 6
    //   310: aload 6
    //   312: ifnonnull +121 -> 433
    //   315: aload 9
    //   317: invokevirtual 254	java/util/Vector:size	()I
    //   320: ifne +38 -> 358
    //   323: new 256	com/hp/hpl/sparta/Text
    //   326: astore 6
    //   328: aload 6
    //   330: aload_2
    //   331: invokespecial 257	com/hp/hpl/sparta/Text:<init>	(Ljava/lang/String;)V
    //   334: aload 6
    //   336: invokevirtual 260	com/hp/hpl/sparta/Text:getData	()Ljava/lang/String;
    //   339: invokevirtual 42	java/lang/String:length	()I
    //   342: ifle -74 -> 268
    //   345: aload 8
    //   347: aload 6
    //   349: invokevirtual 263	com/hp/hpl/sparta/Element:appendChild	(Lcom/hp/hpl/sparta/Node;)V
    //   352: iconst_1
    //   353: istore 5
    //   355: goto -87 -> 268
    //   358: aload 9
    //   360: iconst_0
    //   361: invokevirtual 267	java/util/Vector:elementAt	(I)Ljava/lang/Object;
    //   364: checkcast 256	com/hp/hpl/sparta/Text
    //   367: astore 6
    //   369: aload 6
    //   371: invokevirtual 260	com/hp/hpl/sparta/Text:getData	()Ljava/lang/String;
    //   374: aload_2
    //   375: invokevirtual 211	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   378: ifne +18 -> 396
    //   381: aload 9
    //   383: iconst_0
    //   384: invokevirtual 271	java/util/Vector:removeElementAt	(I)V
    //   387: aload 6
    //   389: aload_2
    //   390: invokevirtual 274	com/hp/hpl/sparta/Text:setData	(Ljava/lang/String;)V
    //   393: iconst_1
    //   394: istore 5
    //   396: iconst_0
    //   397: istore_3
    //   398: iload_3
    //   399: aload 9
    //   401: invokevirtual 254	java/util/Vector:size	()I
    //   404: if_icmplt +6 -> 410
    //   407: goto -139 -> 268
    //   410: aload 8
    //   412: aload 9
    //   414: iload_3
    //   415: invokevirtual 267	java/util/Vector:elementAt	(I)Ljava/lang/Object;
    //   418: checkcast 256	com/hp/hpl/sparta/Text
    //   421: invokevirtual 277	com/hp/hpl/sparta/Element:removeChild	(Lcom/hp/hpl/sparta/Node;)V
    //   424: iinc 3 1
    //   427: iconst_1
    //   428: istore 5
    //   430: goto -32 -> 398
    //   433: aload 6
    //   435: instanceof 256
    //   438: ifeq +13 -> 451
    //   441: aload 9
    //   443: aload 6
    //   445: checkcast 256	com/hp/hpl/sparta/Text
    //   448: invokevirtual 280	java/util/Vector:addElement	(Ljava/lang/Object;)V
    //   451: aload 6
    //   453: invokevirtual 282	com/hp/hpl/sparta/Node:getNextSibling	()Lcom/hp/hpl/sparta/Node;
    //   456: astore 6
    //   458: goto -148 -> 310
    //   461: astore_2
    //   462: new 54	java/lang/StringBuffer
    //   465: dup
    //   466: invokespecial 55	java/lang/StringBuffer:<init>	()V
    //   469: astore_2
    //   470: aload_2
    //   471: ldc -13
    //   473: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   476: pop
    //   477: aload_2
    //   478: aload_1
    //   479: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   482: pop
    //   483: aload_2
    //   484: ldc_w 284
    //   487: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   490: pop
    //   491: new 104	com/hp/hpl/sparta/ParseException
    //   494: dup
    //   495: aload_2
    //   496: invokevirtual 70	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   499: invokespecial 146	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;)V
    //   502: athrow
    //   503: astore_1
    //   504: new 54	java/lang/StringBuffer
    //   507: dup
    //   508: invokespecial 55	java/lang/StringBuffer:<init>	()V
    //   511: astore_2
    //   512: aload_2
    //   513: ldc_w 286
    //   516: invokevirtual 61	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   519: pop
    //   520: aload_2
    //   521: aload_1
    //   522: invokevirtual 141	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   525: pop
    //   526: new 288	java/lang/Error
    //   529: dup
    //   530: aload_2
    //   531: invokevirtual 70	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   534: invokespecial 289	java/lang/Error:<init>	(Ljava/lang/String;)V
    //   537: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	538	0	this	Node
    //   0	538	1	paramString1	String
    //   0	538	2	paramString2	String
    //   6	419	3	i	int
    //   10	106	4	j	int
    //   104	325	5	bool	boolean
    //   43	414	6	localObject1	Object
    //   112	170	7	localObject2	Object
    //   148	263	8	localObject3	Object
    //   296	146	9	localVector	java.util.Vector
    // Exception table:
    //   from	to	target	type
    //   0	7	461	java/lang/IndexOutOfBoundsException
    //   12	37	461	java/lang/IndexOutOfBoundsException
    //   40	103	461	java/lang/IndexOutOfBoundsException
    //   106	150	461	java/lang/IndexOutOfBoundsException
    //   150	160	461	java/lang/IndexOutOfBoundsException
    //   163	197	461	java/lang/IndexOutOfBoundsException
    //   203	251	461	java/lang/IndexOutOfBoundsException
    //   251	268	461	java/lang/IndexOutOfBoundsException
    //   268	278	461	java/lang/IndexOutOfBoundsException
    //   281	310	461	java/lang/IndexOutOfBoundsException
    //   315	352	461	java/lang/IndexOutOfBoundsException
    //   358	369	461	java/lang/IndexOutOfBoundsException
    //   369	393	461	java/lang/IndexOutOfBoundsException
    //   398	407	461	java/lang/IndexOutOfBoundsException
    //   410	424	461	java/lang/IndexOutOfBoundsException
    //   433	451	461	java/lang/IndexOutOfBoundsException
    //   451	458	461	java/lang/IndexOutOfBoundsException
    //   0	7	503	com/hp/hpl/sparta/DOMException
    //   12	37	503	com/hp/hpl/sparta/DOMException
    //   40	103	503	com/hp/hpl/sparta/DOMException
    //   106	150	503	com/hp/hpl/sparta/DOMException
    //   150	160	503	com/hp/hpl/sparta/DOMException
    //   163	197	503	com/hp/hpl/sparta/DOMException
    //   203	251	503	com/hp/hpl/sparta/DOMException
    //   251	268	503	com/hp/hpl/sparta/DOMException
    //   268	278	503	com/hp/hpl/sparta/DOMException
    //   281	310	503	com/hp/hpl/sparta/DOMException
    //   315	352	503	com/hp/hpl/sparta/DOMException
    //   358	369	503	com/hp/hpl/sparta/DOMException
    //   369	393	503	com/hp/hpl/sparta/DOMException
    //   398	407	503	com/hp/hpl/sparta/DOMException
    //   410	424	503	com/hp/hpl/sparta/DOMException
    //   433	451	503	com/hp/hpl/sparta/DOMException
    //   451	458	503	com/hp/hpl/sparta/DOMException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/Node.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */