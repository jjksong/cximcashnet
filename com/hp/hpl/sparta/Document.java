package com.hp.hpl.sparta;

import com.hp.hpl.sparta.xpath.XPath;
import com.hp.hpl.sparta.xpath.XPathException;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class Document
  extends Node
{
  private static final boolean DEBUG = false;
  static final Enumeration EMPTY = new EmptyEnumeration();
  private static final Integer ONE = new Integer(1);
  private final Hashtable indexible_ = (Hashtable)null;
  private Sparta.Cache indices_ = Sparta.newCache();
  private Vector observers_ = new Vector();
  private Element rootElement_ = null;
  private String systemId_;
  
  public Document()
  {
    this.systemId_ = "MEMORY";
  }
  
  Document(String paramString)
  {
    this.systemId_ = paramString;
  }
  
  private XPathVisitor visitor(String paramString, boolean paramBoolean)
    throws XPathException
  {
    Object localObject = paramString;
    if (paramString.charAt(0) != '/')
    {
      localObject = new StringBuffer();
      ((StringBuffer)localObject).append("/");
      ((StringBuffer)localObject).append(paramString);
      localObject = ((StringBuffer)localObject).toString();
    }
    return visitor(XPath.get((String)localObject), paramBoolean);
  }
  
  public void addObserver(Observer paramObserver)
  {
    this.observers_.addElement(paramObserver);
  }
  
  public Object clone()
  {
    Document localDocument = new Document(this.systemId_);
    localDocument.rootElement_ = ((Element)this.rootElement_.clone());
    return localDocument;
  }
  
  protected int computeHashCode()
  {
    return this.rootElement_.hashCode();
  }
  
  public void deleteObserver(Observer paramObserver)
  {
    this.observers_.removeElement(paramObserver);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof Document)) {
      return false;
    }
    paramObject = (Document)paramObject;
    return this.rootElement_.equals(((Document)paramObject).rootElement_);
  }
  
  public Element getDocumentElement()
  {
    return this.rootElement_;
  }
  
  public String getSystemId()
  {
    return this.systemId_;
  }
  
  void monitor(XPath paramXPath)
    throws XPathException
  {}
  
  void notifyObservers()
  {
    Enumeration localEnumeration = this.observers_.elements();
    for (;;)
    {
      if (!localEnumeration.hasMoreElements()) {
        return;
      }
      ((Observer)localEnumeration.nextElement()).update(this);
    }
  }
  
  public void setDocumentElement(Element paramElement)
  {
    this.rootElement_ = paramElement;
    this.rootElement_.setOwnerDocument(this);
    notifyObservers();
  }
  
  public void setSystemId(String paramString)
  {
    this.systemId_ = paramString;
    notifyObservers();
  }
  
  public String toString()
  {
    return this.systemId_;
  }
  
  public void toString(Writer paramWriter)
    throws IOException
  {
    this.rootElement_.toString(paramWriter);
  }
  
  public void toXml(Writer paramWriter)
    throws IOException
  {
    paramWriter.write("<?xml version=\"1.0\" ?>\n");
    this.rootElement_.toXml(paramWriter);
  }
  
  XPathVisitor visitor(XPath paramXPath, boolean paramBoolean)
    throws XPathException
  {
    if (paramXPath.isStringValue() != paramBoolean)
    {
      String str;
      if (paramBoolean) {
        str = "evaluates to element not string";
      } else {
        str = "evaluates to string not element";
      }
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("\"");
      localStringBuffer.append(paramXPath);
      localStringBuffer.append("\" evaluates to ");
      localStringBuffer.append(str);
      throw new XPathException(paramXPath, localStringBuffer.toString());
    }
    return new XPathVisitor(this, paramXPath);
  }
  
  /* Error */
  public boolean xpathEnsure(String paramString)
    throws ParseException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 205	com/hp/hpl/sparta/Document:xpathSelectElement	(Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   5: ifnull +5 -> 10
    //   8: iconst_0
    //   9: ireturn
    //   10: aload_1
    //   11: invokestatic 98	com/hp/hpl/sparta/xpath/XPath:get	(Ljava/lang/String;)Lcom/hp/hpl/sparta/xpath/XPath;
    //   14: astore_3
    //   15: aload_3
    //   16: invokevirtual 208	com/hp/hpl/sparta/xpath/XPath:getSteps	()Ljava/util/Enumeration;
    //   19: astore 4
    //   21: iconst_0
    //   22: istore_2
    //   23: aload 4
    //   25: invokeinterface 145 1 0
    //   30: ifne +219 -> 249
    //   33: aload_3
    //   34: invokevirtual 208	com/hp/hpl/sparta/xpath/XPath:getSteps	()Ljava/util/Enumeration;
    //   37: astore 5
    //   39: aload 5
    //   41: invokeinterface 148 1 0
    //   46: checkcast 210	com/hp/hpl/sparta/xpath/Step
    //   49: astore_3
    //   50: iload_2
    //   51: iconst_1
    //   52: isub
    //   53: anewarray 210	com/hp/hpl/sparta/xpath/Step
    //   56: astore 4
    //   58: iconst_0
    //   59: istore_2
    //   60: iload_2
    //   61: aload 4
    //   63: arraylength
    //   64: if_icmplt +165 -> 229
    //   67: aload_0
    //   68: getfield 47	com/hp/hpl/sparta/Document:rootElement_	Lcom/hp/hpl/sparta/Element;
    //   71: ifnonnull +17 -> 88
    //   74: aload_0
    //   75: aload_0
    //   76: aconst_null
    //   77: aload_3
    //   78: aload_1
    //   79: invokevirtual 214	com/hp/hpl/sparta/Node:makeMatching	(Lcom/hp/hpl/sparta/Element;Lcom/hp/hpl/sparta/xpath/Step;Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   82: invokevirtual 216	com/hp/hpl/sparta/Document:setDocumentElement	(Lcom/hp/hpl/sparta/Element;)V
    //   85: goto +40 -> 125
    //   88: new 81	java/lang/StringBuffer
    //   91: astore 5
    //   93: aload 5
    //   95: invokespecial 82	java/lang/StringBuffer:<init>	()V
    //   98: aload 5
    //   100: ldc 84
    //   102: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   105: pop
    //   106: aload 5
    //   108: aload_3
    //   109: invokevirtual 187	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   112: pop
    //   113: aload_0
    //   114: aload 5
    //   116: invokevirtual 92	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   119: invokevirtual 205	com/hp/hpl/sparta/Document:xpathSelectElement	(Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   122: ifnull +28 -> 150
    //   125: aload 4
    //   127: arraylength
    //   128: ifne +5 -> 133
    //   131: iconst_1
    //   132: ireturn
    //   133: aload_0
    //   134: getfield 47	com/hp/hpl/sparta/Document:rootElement_	Lcom/hp/hpl/sparta/Element;
    //   137: iconst_0
    //   138: aload 4
    //   140: invokestatic 219	com/hp/hpl/sparta/xpath/XPath:get	(Z[Lcom/hp/hpl/sparta/xpath/Step;)Lcom/hp/hpl/sparta/xpath/XPath;
    //   143: invokevirtual 220	com/hp/hpl/sparta/xpath/XPath:toString	()Ljava/lang/String;
    //   146: invokevirtual 222	com/hp/hpl/sparta/Element:xpathEnsure	(Ljava/lang/String;)Z
    //   149: ireturn
    //   150: new 201	com/hp/hpl/sparta/ParseException
    //   153: astore 4
    //   155: new 81	java/lang/StringBuffer
    //   158: astore 5
    //   160: aload 5
    //   162: invokespecial 82	java/lang/StringBuffer:<init>	()V
    //   165: aload 5
    //   167: ldc -32
    //   169: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   172: pop
    //   173: aload 5
    //   175: aload_0
    //   176: getfield 47	com/hp/hpl/sparta/Document:rootElement_	Lcom/hp/hpl/sparta/Element;
    //   179: invokevirtual 227	com/hp/hpl/sparta/Element:getTagName	()Ljava/lang/String;
    //   182: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   185: pop
    //   186: aload 5
    //   188: ldc -27
    //   190: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   193: pop
    //   194: aload 5
    //   196: aload_3
    //   197: invokevirtual 187	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   200: pop
    //   201: aload 5
    //   203: ldc -25
    //   205: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   208: pop
    //   209: aload 5
    //   211: aload_1
    //   212: invokevirtual 88	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   215: pop
    //   216: aload 4
    //   218: aload 5
    //   220: invokevirtual 92	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   223: invokespecial 232	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;)V
    //   226: aload 4
    //   228: athrow
    //   229: aload 4
    //   231: iload_2
    //   232: aload 5
    //   234: invokeinterface 148 1 0
    //   239: checkcast 210	com/hp/hpl/sparta/xpath/Step
    //   242: aastore
    //   243: iinc 2 1
    //   246: goto -186 -> 60
    //   249: aload 4
    //   251: invokeinterface 148 1 0
    //   256: pop
    //   257: iinc 2 1
    //   260: goto -237 -> 23
    //   263: astore_3
    //   264: new 201	com/hp/hpl/sparta/ParseException
    //   267: dup
    //   268: aload_1
    //   269: aload_3
    //   270: invokespecial 235	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   273: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	274	0	this	Document
    //   0	274	1	paramString	String
    //   22	236	2	i	int
    //   14	183	3	localObject1	Object
    //   263	7	3	localXPathException	XPathException
    //   19	231	4	localObject2	Object
    //   37	196	5	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   0	8	263	com/hp/hpl/sparta/xpath/XPathException
    //   10	21	263	com/hp/hpl/sparta/xpath/XPathException
    //   23	58	263	com/hp/hpl/sparta/xpath/XPathException
    //   60	85	263	com/hp/hpl/sparta/xpath/XPathException
    //   88	125	263	com/hp/hpl/sparta/xpath/XPathException
    //   125	131	263	com/hp/hpl/sparta/xpath/XPathException
    //   133	150	263	com/hp/hpl/sparta/xpath/XPathException
    //   150	229	263	com/hp/hpl/sparta/xpath/XPathException
    //   229	243	263	com/hp/hpl/sparta/xpath/XPathException
    //   249	257	263	com/hp/hpl/sparta/xpath/XPathException
  }
  
  public Index xpathGetIndex(String paramString)
    throws ParseException
  {
    try
    {
      Object localObject2 = (Index)this.indices_.get(paramString);
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject2 = XPath.get(paramString);
        localObject1 = new com/hp/hpl/sparta/Document$Index;
        ((Index)localObject1).<init>(this, (XPath)localObject2);
        this.indices_.put(paramString, localObject1);
      }
      return (Index)localObject1;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public boolean xpathHasIndex(String paramString)
  {
    boolean bool;
    if (this.indices_.get(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public Element xpathSelectElement(String paramString)
    throws ParseException
  {
    Object localObject = paramString;
    try
    {
      if (paramString.charAt(0) != '/')
      {
        localObject = new java/lang/StringBuffer;
        ((StringBuffer)localObject).<init>();
        ((StringBuffer)localObject).append("/");
        ((StringBuffer)localObject).append(paramString);
        localObject = ((StringBuffer)localObject).toString();
      }
      paramString = XPath.get((String)localObject);
      monitor(paramString);
      paramString = visitor(paramString, false).getFirstResultElement();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public Enumeration xpathSelectElements(String paramString)
    throws ParseException
  {
    Object localObject = paramString;
    try
    {
      if (paramString.charAt(0) != '/')
      {
        localObject = new java/lang/StringBuffer;
        ((StringBuffer)localObject).<init>();
        ((StringBuffer)localObject).append("/");
        ((StringBuffer)localObject).append(paramString);
        localObject = ((StringBuffer)localObject).toString();
      }
      paramString = XPath.get((String)localObject);
      monitor(paramString);
      paramString = visitor(paramString, false).getResultEnumeration();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public String xpathSelectString(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, true).getFirstResultString();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public Enumeration xpathSelectStrings(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, true).getResultEnumeration();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public class Index
    implements Document.Observer
  {
    private final String attrName_;
    private transient Sparta.Cache dict_ = null;
    private final XPath xpath_;
    
    Index(XPath paramXPath)
      throws XPathException
    {
      this.attrName_ = paramXPath.getIndexingAttrName();
      this.xpath_ = paramXPath;
      Document.this.addObserver(this);
    }
    
    /* Error */
    private void regenerate()
      throws ParseException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokestatic 53	com/hp/hpl/sparta/Sparta:newCache	()Lcom/hp/hpl/sparta/Sparta$Cache;
      //   4: putfield 28	com/hp/hpl/sparta/Document$Index:dict_	Lcom/hp/hpl/sparta/Sparta$Cache;
      //   7: aload_0
      //   8: getfield 26	com/hp/hpl/sparta/Document$Index:this$0	Lcom/hp/hpl/sparta/Document;
      //   11: aload_0
      //   12: getfield 38	com/hp/hpl/sparta/Document$Index:xpath_	Lcom/hp/hpl/sparta/xpath/XPath;
      //   15: iconst_0
      //   16: invokevirtual 57	com/hp/hpl/sparta/Document:visitor	(Lcom/hp/hpl/sparta/xpath/XPath;Z)Lcom/hp/hpl/sparta/XPathVisitor;
      //   19: invokevirtual 63	com/hp/hpl/sparta/XPathVisitor:getResultEnumeration	()Ljava/util/Enumeration;
      //   22: astore_3
      //   23: aload_3
      //   24: invokeinterface 69 1 0
      //   29: ifne +4 -> 33
      //   32: return
      //   33: aload_3
      //   34: invokeinterface 73 1 0
      //   39: checkcast 75	com/hp/hpl/sparta/Element
      //   42: astore 4
      //   44: aload 4
      //   46: aload_0
      //   47: getfield 36	com/hp/hpl/sparta/Document$Index:attrName_	Ljava/lang/String;
      //   50: invokevirtual 79	com/hp/hpl/sparta/Element:getAttribute	(Ljava/lang/String;)Ljava/lang/String;
      //   53: astore 5
      //   55: aload_0
      //   56: getfield 28	com/hp/hpl/sparta/Document$Index:dict_	Lcom/hp/hpl/sparta/Sparta$Cache;
      //   59: aload 5
      //   61: invokeinterface 85 2 0
      //   66: checkcast 87	java/util/Vector
      //   69: astore_2
      //   70: aload_2
      //   71: astore_1
      //   72: aload_2
      //   73: ifnonnull +25 -> 98
      //   76: new 87	java/util/Vector
      //   79: astore_1
      //   80: aload_1
      //   81: iconst_1
      //   82: invokespecial 90	java/util/Vector:<init>	(I)V
      //   85: aload_0
      //   86: getfield 28	com/hp/hpl/sparta/Document$Index:dict_	Lcom/hp/hpl/sparta/Sparta$Cache;
      //   89: aload 5
      //   91: aload_1
      //   92: invokeinterface 94 3 0
      //   97: pop
      //   98: aload_1
      //   99: aload 4
      //   101: invokevirtual 98	java/util/Vector:addElement	(Ljava/lang/Object;)V
      //   104: goto -81 -> 23
      //   107: astore_1
      //   108: new 47	com/hp/hpl/sparta/ParseException
      //   111: dup
      //   112: ldc 100
      //   114: aload_1
      //   115: invokespecial 103	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   118: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	119	0	this	Index
      //   71	28	1	localVector1	Vector
      //   107	8	1	localXPathException	XPathException
      //   69	4	2	localVector2	Vector
      //   22	12	3	localEnumeration	Enumeration
      //   42	58	4	localElement	Element
      //   53	37	5	str	String
      // Exception table:
      //   from	to	target	type
      //   0	23	107	com/hp/hpl/sparta/xpath/XPathException
      //   23	32	107	com/hp/hpl/sparta/xpath/XPathException
      //   33	70	107	com/hp/hpl/sparta/xpath/XPathException
      //   76	98	107	com/hp/hpl/sparta/xpath/XPathException
      //   98	104	107	com/hp/hpl/sparta/xpath/XPathException
    }
    
    public Enumeration get(String paramString)
      throws ParseException
    {
      try
      {
        if (this.dict_ == null) {
          regenerate();
        }
        paramString = (Vector)this.dict_.get(paramString);
        if (paramString == null) {
          paramString = Document.EMPTY;
        } else {
          paramString = paramString.elements();
        }
        return paramString;
      }
      finally {}
    }
    
    public int size()
      throws ParseException
    {
      try
      {
        if (this.dict_ == null) {
          regenerate();
        }
        int i = this.dict_.size();
        return i;
      }
      finally {}
    }
    
    public void update(Document paramDocument)
    {
      try
      {
        this.dict_ = null;
        return;
      }
      finally
      {
        paramDocument = finally;
        throw paramDocument;
      }
    }
  }
  
  public static abstract interface Observer
  {
    public abstract void update(Document paramDocument);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/Document.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */