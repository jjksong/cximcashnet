package com.hp.hpl.sparta;

import com.hp.hpl.sparta.xpath.AllElementTest;
import com.hp.hpl.sparta.xpath.AttrCompareExpr;
import com.hp.hpl.sparta.xpath.AttrEqualsExpr;
import com.hp.hpl.sparta.xpath.AttrExistsExpr;
import com.hp.hpl.sparta.xpath.AttrExpr;
import com.hp.hpl.sparta.xpath.AttrGreaterExpr;
import com.hp.hpl.sparta.xpath.AttrLessExpr;
import com.hp.hpl.sparta.xpath.AttrNotEqualsExpr;
import com.hp.hpl.sparta.xpath.AttrRelationalExpr;
import com.hp.hpl.sparta.xpath.AttrTest;
import com.hp.hpl.sparta.xpath.BooleanExpr;
import com.hp.hpl.sparta.xpath.ElementTest;
import com.hp.hpl.sparta.xpath.NodeTest;
import com.hp.hpl.sparta.xpath.ParentNodeTest;
import com.hp.hpl.sparta.xpath.PositionEqualsExpr;
import com.hp.hpl.sparta.xpath.Step;
import com.hp.hpl.sparta.xpath.TextCompareExpr;
import com.hp.hpl.sparta.xpath.TextEqualsExpr;
import com.hp.hpl.sparta.xpath.TextExistsExpr;
import com.hp.hpl.sparta.xpath.TextNotEqualsExpr;
import com.hp.hpl.sparta.xpath.TextTest;
import com.hp.hpl.sparta.xpath.ThisNodeTest;
import com.hp.hpl.sparta.xpath.TrueExpr;
import com.hp.hpl.sparta.xpath.Visitor;
import com.hp.hpl.sparta.xpath.XPath;
import com.hp.hpl.sparta.xpath.XPathException;
import java.util.Enumeration;
import java.util.Vector;

class XPathVisitor
  implements Visitor
{
  private static final Boolean FALSE = new Boolean(false);
  private static final Boolean TRUE = new Boolean(true);
  private Node contextNode_;
  private final BooleanStack exprStack_ = new BooleanStack(null);
  private boolean multiLevel_;
  private Object node_ = null;
  private Vector nodelistFiltered_ = new Vector();
  private final NodeListWithPosition nodelistRaw_ = new NodeListWithPosition();
  private Enumeration nodesetIterator_ = null;
  private XPath xpath_;
  
  public XPathVisitor(Document paramDocument, XPath paramXPath)
    throws XPathException
  {
    this(paramXPath, paramDocument);
  }
  
  public XPathVisitor(Element paramElement, XPath paramXPath)
    throws XPathException
  {
    this(paramXPath, paramElement);
    if (!paramXPath.isAbsolute()) {
      return;
    }
    throw new XPathException(paramXPath, "Cannot use element as context node for absolute xpath");
  }
  
  private XPathVisitor(XPath paramXPath, Node paramNode)
    throws XPathException
  {
    this.xpath_ = paramXPath;
    this.contextNode_ = paramNode;
    this.nodelistFiltered_ = new Vector(1);
    this.nodelistFiltered_.addElement(this.contextNode_);
    paramXPath = paramXPath.getSteps();
    for (;;)
    {
      if (!paramXPath.hasMoreElements()) {
        return;
      }
      paramNode = (Step)paramXPath.nextElement();
      this.multiLevel_ = paramNode.isMultiLevel();
      this.nodesetIterator_ = null;
      paramNode.getNodeTest().accept(this);
      this.nodesetIterator_ = this.nodelistRaw_.iterator();
      this.nodelistFiltered_.removeAllElements();
      paramNode = paramNode.getPredicate();
      while (this.nodesetIterator_.hasMoreElements())
      {
        this.node_ = this.nodesetIterator_.nextElement();
        paramNode.accept(this);
        if (this.exprStack_.pop().booleanValue()) {
          this.nodelistFiltered_.addElement(this.node_);
        }
      }
    }
  }
  
  private void accumulateElements(Document paramDocument)
  {
    paramDocument = paramDocument.getDocumentElement();
    this.nodelistRaw_.add(paramDocument, 1);
    if (this.multiLevel_) {
      accumulateElements(paramDocument);
    }
  }
  
  private void accumulateElements(Element paramElement)
  {
    paramElement = paramElement.getFirstChild();
    int i;
    for (int j = 0;; j = i)
    {
      if (paramElement == null) {
        return;
      }
      i = j;
      if ((paramElement instanceof Element))
      {
        NodeListWithPosition localNodeListWithPosition = this.nodelistRaw_;
        j++;
        localNodeListWithPosition.add(paramElement, j);
        i = j;
        if (this.multiLevel_)
        {
          accumulateElements((Element)paramElement);
          i = j;
        }
      }
      paramElement = paramElement.getNextSibling();
    }
  }
  
  private void accumulateMatchingElements(Document paramDocument, String paramString)
  {
    paramDocument = paramDocument.getDocumentElement();
    if (paramDocument == null) {
      return;
    }
    if (paramDocument.getTagName() == paramString) {
      this.nodelistRaw_.add(paramDocument, 1);
    }
    if (this.multiLevel_) {
      accumulateMatchingElements(paramDocument, paramString);
    }
  }
  
  private void accumulateMatchingElements(Element paramElement, String paramString)
  {
    paramElement = paramElement.getFirstChild();
    int k;
    for (int i = 0;; i = k)
    {
      if (paramElement == null) {
        return;
      }
      k = i;
      if ((paramElement instanceof Element))
      {
        Element localElement = (Element)paramElement;
        int j = i;
        if (localElement.getTagName() == paramString)
        {
          NodeListWithPosition localNodeListWithPosition = this.nodelistRaw_;
          j = i + 1;
          localNodeListWithPosition.add(localElement, j);
        }
        k = j;
        if (this.multiLevel_)
        {
          accumulateMatchingElements(localElement, paramString);
          k = j;
        }
      }
      paramElement = paramElement.getNextSibling();
    }
  }
  
  public Element getFirstResultElement()
  {
    Element localElement;
    if (this.nodelistFiltered_.size() == 0) {
      localElement = null;
    } else {
      localElement = (Element)this.nodelistFiltered_.elementAt(0);
    }
    return localElement;
  }
  
  public String getFirstResultString()
  {
    String str;
    if (this.nodelistFiltered_.size() == 0) {
      str = null;
    } else {
      str = this.nodelistFiltered_.elementAt(0).toString();
    }
    return str;
  }
  
  public Enumeration getResultEnumeration()
  {
    return this.nodelistFiltered_.elements();
  }
  
  public void visit(AllElementTest paramAllElementTest)
  {
    paramAllElementTest = this.nodelistFiltered_;
    this.nodelistRaw_.removeAllElements();
    Enumeration localEnumeration = paramAllElementTest.elements();
    for (;;)
    {
      if (!localEnumeration.hasMoreElements()) {
        return;
      }
      paramAllElementTest = localEnumeration.nextElement();
      if ((paramAllElementTest instanceof Element)) {
        accumulateElements((Element)paramAllElementTest);
      } else if ((paramAllElementTest instanceof Document)) {
        accumulateElements((Document)paramAllElementTest);
      }
    }
  }
  
  public void visit(AttrEqualsExpr paramAttrEqualsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      localObject = ((Element)localObject).getAttribute(paramAttrEqualsExpr.getAttrName());
      boolean bool = paramAttrEqualsExpr.getAttrValue().equals(localObject);
      localObject = this.exprStack_;
      if (bool) {
        paramAttrEqualsExpr = TRUE;
      } else {
        paramAttrEqualsExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramAttrEqualsExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(AttrExistsExpr paramAttrExistsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      paramAttrExistsExpr = ((Element)localObject).getAttribute(paramAttrExistsExpr.getAttrName());
      int i;
      if ((paramAttrExistsExpr != null) && (paramAttrExistsExpr.length() > 0)) {
        i = 1;
      } else {
        i = 0;
      }
      localObject = this.exprStack_;
      if (i != 0) {
        paramAttrExistsExpr = TRUE;
      } else {
        paramAttrExistsExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramAttrExistsExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(AttrGreaterExpr paramAttrGreaterExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      int i;
      if (Long.parseLong(((Element)localObject).getAttribute(paramAttrGreaterExpr.getAttrName())) > paramAttrGreaterExpr.getAttrValue()) {
        i = 1;
      } else {
        i = 0;
      }
      localObject = this.exprStack_;
      if (i != 0) {
        paramAttrGreaterExpr = TRUE;
      } else {
        paramAttrGreaterExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramAttrGreaterExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(AttrLessExpr paramAttrLessExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      int i;
      if (Long.parseLong(((Element)localObject).getAttribute(paramAttrLessExpr.getAttrName())) < paramAttrLessExpr.getAttrValue()) {
        i = 1;
      } else {
        i = 0;
      }
      localObject = this.exprStack_;
      if (i != 0) {
        paramAttrLessExpr = TRUE;
      } else {
        paramAttrLessExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramAttrLessExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(AttrNotEqualsExpr paramAttrNotEqualsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      localObject = ((Element)localObject).getAttribute(paramAttrNotEqualsExpr.getAttrName());
      boolean bool = paramAttrNotEqualsExpr.getAttrValue().equals(localObject);
      localObject = this.exprStack_;
      if ((bool ^ true)) {
        paramAttrNotEqualsExpr = TRUE;
      } else {
        paramAttrNotEqualsExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramAttrNotEqualsExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(AttrTest paramAttrTest)
  {
    Object localObject1 = this.nodelistFiltered_;
    this.nodelistRaw_.removeAllElements();
    localObject1 = ((Vector)localObject1).elements();
    for (;;)
    {
      if (!((Enumeration)localObject1).hasMoreElements()) {
        return;
      }
      Object localObject2 = (Node)((Enumeration)localObject1).nextElement();
      if ((localObject2 instanceof Element))
      {
        localObject2 = ((Element)localObject2).getAttribute(paramAttrTest.getAttrName());
        if (localObject2 != null) {
          this.nodelistRaw_.add((String)localObject2);
        }
      }
    }
  }
  
  public void visit(ElementTest paramElementTest)
  {
    String str = paramElementTest.getTagName();
    Vector localVector = this.nodelistFiltered_;
    int j = localVector.size();
    this.nodelistRaw_.removeAllElements();
    for (int i = 0;; i++)
    {
      if (i >= j) {
        return;
      }
      paramElementTest = localVector.elementAt(i);
      if ((paramElementTest instanceof Element)) {
        accumulateMatchingElements((Element)paramElementTest, str);
      } else if ((paramElementTest instanceof Document)) {
        accumulateMatchingElements((Document)paramElementTest, str);
      }
    }
  }
  
  public void visit(ParentNodeTest paramParentNodeTest)
    throws XPathException
  {
    this.nodelistRaw_.removeAllElements();
    paramParentNodeTest = this.contextNode_.getParentNode();
    if (paramParentNodeTest != null)
    {
      this.nodelistRaw_.add(paramParentNodeTest, 1);
      return;
    }
    throw new XPathException(this.xpath_, "Illegal attempt to apply \"..\" to node with no parent.");
  }
  
  public void visit(PositionEqualsExpr paramPositionEqualsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element))
    {
      localObject = (Element)localObject;
      int i;
      if (this.nodelistRaw_.position((Node)localObject) == paramPositionEqualsExpr.getPosition()) {
        i = 1;
      } else {
        i = 0;
      }
      localObject = this.exprStack_;
      if (i != 0) {
        paramPositionEqualsExpr = TRUE;
      } else {
        paramPositionEqualsExpr = FALSE;
      }
      ((BooleanStack)localObject).push(paramPositionEqualsExpr);
      return;
    }
    throw new XPathException(this.xpath_, "Cannot test position of document");
  }
  
  public void visit(TextEqualsExpr paramTextEqualsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element)) {
      for (localObject = ((Element)localObject).getFirstChild();; localObject = ((Node)localObject).getNextSibling())
      {
        if (localObject == null) {
          paramTextEqualsExpr = this.exprStack_;
        }
        for (localObject = FALSE;; localObject = TRUE)
        {
          paramTextEqualsExpr.push((Boolean)localObject);
          return;
          if ((!(localObject instanceof Text)) || (!((Text)localObject).getData().equals(paramTextEqualsExpr.getValue()))) {
            break;
          }
          paramTextEqualsExpr = this.exprStack_;
        }
      }
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(TextExistsExpr paramTextExistsExpr)
    throws XPathException
  {
    paramTextExistsExpr = this.node_;
    if ((paramTextExistsExpr instanceof Element)) {
      for (paramTextExistsExpr = ((Element)paramTextExistsExpr).getFirstChild();; paramTextExistsExpr = paramTextExistsExpr.getNextSibling())
      {
        BooleanStack localBooleanStack;
        if (paramTextExistsExpr == null) {
          localBooleanStack = this.exprStack_;
        }
        for (paramTextExistsExpr = FALSE;; paramTextExistsExpr = TRUE)
        {
          localBooleanStack.push(paramTextExistsExpr);
          return;
          if (!(paramTextExistsExpr instanceof Text)) {
            break;
          }
          localBooleanStack = this.exprStack_;
        }
      }
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(TextNotEqualsExpr paramTextNotEqualsExpr)
    throws XPathException
  {
    Object localObject = this.node_;
    if ((localObject instanceof Element)) {
      for (localObject = ((Element)localObject).getFirstChild();; localObject = ((Node)localObject).getNextSibling())
      {
        if (localObject == null) {
          paramTextNotEqualsExpr = this.exprStack_;
        }
        for (localObject = FALSE;; localObject = TRUE)
        {
          paramTextNotEqualsExpr.push((Boolean)localObject);
          return;
          if ((!(localObject instanceof Text)) || (((Text)localObject).getData().equals(paramTextNotEqualsExpr.getValue()))) {
            break;
          }
          paramTextNotEqualsExpr = this.exprStack_;
        }
      }
    }
    throw new XPathException(this.xpath_, "Cannot test attribute of document");
  }
  
  public void visit(TextTest paramTextTest)
  {
    paramTextTest = this.nodelistFiltered_;
    this.nodelistRaw_.removeAllElements();
    Enumeration localEnumeration = paramTextTest.elements();
    for (;;)
    {
      if (!localEnumeration.hasMoreElements()) {
        return;
      }
      paramTextTest = localEnumeration.nextElement();
      if ((paramTextTest instanceof Element)) {
        for (paramTextTest = ((Element)paramTextTest).getFirstChild(); paramTextTest != null; paramTextTest = paramTextTest.getNextSibling()) {
          if ((paramTextTest instanceof Text)) {
            this.nodelistRaw_.add(((Text)paramTextTest).getData());
          }
        }
      }
    }
  }
  
  public void visit(ThisNodeTest paramThisNodeTest)
  {
    this.nodelistRaw_.removeAllElements();
    this.nodelistRaw_.add(this.contextNode_, 1);
  }
  
  public void visit(TrueExpr paramTrueExpr)
  {
    this.exprStack_.push(TRUE);
  }
  
  private static class BooleanStack
  {
    private Item top_ = null;
    
    Boolean pop()
    {
      Boolean localBoolean = this.top_.bool;
      this.top_ = this.top_.prev;
      return localBoolean;
    }
    
    void push(Boolean paramBoolean)
    {
      this.top_ = new Item(paramBoolean, this.top_);
    }
    
    private static class Item
    {
      final Boolean bool;
      final Item prev;
      
      Item(Boolean paramBoolean, Item paramItem)
      {
        this.bool = paramBoolean;
        this.prev = paramItem;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/XPathVisitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */