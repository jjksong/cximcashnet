package com.hp.hpl.sparta;

import com.hp.hpl.sparta.xpath.XPath;
import com.hp.hpl.sparta.xpath.XPathException;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class Element
  extends Node
{
  private static final boolean DEBUG = false;
  private Vector attributeNames_ = null;
  private Hashtable attributes_ = null;
  private Node firstChild_ = null;
  private Node lastChild_ = null;
  private String tagName_ = null;
  
  Element() {}
  
  public Element(String paramString)
  {
    this.tagName_ = Sparta.intern(paramString);
  }
  
  private void checkInvariant() {}
  
  private boolean removeChildNoChecking(Node paramNode)
  {
    for (Node localNode = this.firstChild_;; localNode = localNode.getNextSibling())
    {
      if (localNode == null) {
        return false;
      }
      if (localNode.equals(paramNode))
      {
        if (this.firstChild_ == localNode) {
          this.firstChild_ = localNode.getNextSibling();
        }
        if (this.lastChild_ == localNode) {
          this.lastChild_ = localNode.getPreviousSibling();
        }
        localNode.removeFromLinkedList();
        localNode.setParentNode(null);
        localNode.setOwnerDocument(null);
        return true;
      }
    }
  }
  
  private void replaceChild_(Node paramNode1, Node paramNode2)
    throws DOMException
  {
    for (Node localNode = this.firstChild_; localNode != null; localNode = localNode.getNextSibling()) {
      if (localNode == paramNode2)
      {
        if (this.firstChild_ == paramNode2) {
          this.firstChild_ = paramNode1;
        }
        if (this.lastChild_ == paramNode2) {
          this.lastChild_ = paramNode1;
        }
        paramNode2.replaceInLinkedList(paramNode1);
        paramNode1.setParentNode(this);
        paramNode2.setParentNode(null);
        return;
      }
    }
    paramNode1 = new StringBuffer();
    paramNode1.append("Cannot find ");
    paramNode1.append(paramNode2);
    paramNode1.append(" in ");
    paramNode1.append(this);
    throw new DOMException((short)8, paramNode1.toString());
  }
  
  private XPathVisitor visitor(String paramString, boolean paramBoolean)
    throws XPathException
  {
    XPath localXPath = XPath.get(paramString);
    if (localXPath.isStringValue() != paramBoolean)
    {
      if (paramBoolean) {
        paramString = "evaluates to element not string";
      } else {
        paramString = "evaluates to string not element";
      }
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("\"");
      localStringBuffer.append(localXPath);
      localStringBuffer.append("\" evaluates to ");
      localStringBuffer.append(paramString);
      throw new XPathException(localXPath, localStringBuffer.toString());
    }
    return new XPathVisitor(this, localXPath);
  }
  
  public void appendChild(Node paramNode)
  {
    Object localObject = paramNode;
    if (!canHaveAsDescendent(paramNode)) {
      localObject = (Element)paramNode.clone();
    }
    appendChildNoChecking((Node)localObject);
    notifyObservers();
  }
  
  void appendChildNoChecking(Node paramNode)
  {
    Element localElement = paramNode.getParentNode();
    if (localElement != null) {
      localElement.removeChildNoChecking(paramNode);
    }
    paramNode.insertAtEndOfLinkedList(this.lastChild_);
    if (this.firstChild_ == null) {
      this.firstChild_ = paramNode;
    }
    paramNode.setParentNode(this);
    this.lastChild_ = paramNode;
    paramNode.setOwnerDocument(getOwnerDocument());
  }
  
  boolean canHaveAsDescendent(Node paramNode)
  {
    if (paramNode == this) {
      return false;
    }
    Element localElement = getParentNode();
    if (localElement == null) {
      return true;
    }
    return localElement.canHaveAsDescendent(paramNode);
  }
  
  public Object clone()
  {
    return cloneElement(true);
  }
  
  public Element cloneElement(boolean paramBoolean)
  {
    Element localElement = new Element(this.tagName_);
    Object localObject = this.attributeNames_;
    if (localObject != null)
    {
      localObject = ((Vector)localObject).elements();
      while (((Enumeration)localObject).hasMoreElements())
      {
        String str = (String)((Enumeration)localObject).nextElement();
        localElement.setAttribute(str, (String)this.attributes_.get(str));
      }
    }
    if (paramBoolean) {
      for (localObject = this.firstChild_; localObject != null; localObject = ((Node)localObject).getNextSibling()) {
        localElement.appendChild((Node)((Node)localObject).clone());
      }
    }
    return localElement;
  }
  
  public Element cloneShallow()
  {
    return cloneElement(false);
  }
  
  protected int computeHashCode()
  {
    int j = this.tagName_.hashCode();
    Object localObject = this.attributes_;
    int i = j;
    if (localObject != null)
    {
      Enumeration localEnumeration = ((Hashtable)localObject).keys();
      for (i = j; localEnumeration.hasMoreElements(); i = (i * 31 + ((String)localObject).hashCode()) * 31 + ((String)this.attributes_.get(localObject)).hashCode()) {
        localObject = (String)localEnumeration.nextElement();
      }
    }
    for (localObject = this.firstChild_;; localObject = ((Node)localObject).getNextSibling())
    {
      if (localObject == null) {
        return i;
      }
      i = i * 31 + ((Node)localObject).hashCode();
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof Element)) {
      return false;
    }
    paramObject = (Element)paramObject;
    if (!this.tagName_.equals(((Element)paramObject).tagName_)) {
      return false;
    }
    Object localObject = this.attributes_;
    int i;
    if (localObject == null) {
      i = 0;
    } else {
      i = ((Hashtable)localObject).size();
    }
    localObject = ((Element)paramObject).attributes_;
    int j;
    if (localObject == null) {
      j = 0;
    } else {
      j = ((Hashtable)localObject).size();
    }
    if (i != j) {
      return false;
    }
    localObject = this.attributes_;
    if (localObject != null)
    {
      Enumeration localEnumeration = ((Hashtable)localObject).keys();
      while (localEnumeration.hasMoreElements())
      {
        localObject = (String)localEnumeration.nextElement();
        if (!((String)this.attributes_.get(localObject)).equals((String)((Element)paramObject).attributes_.get(localObject))) {
          return false;
        }
      }
    }
    localObject = this.firstChild_;
    for (paramObject = ((Element)paramObject).firstChild_;; paramObject = ((Node)paramObject).getNextSibling())
    {
      if (localObject == null) {
        return true;
      }
      if (!localObject.equals(paramObject)) {
        return false;
      }
      localObject = ((Node)localObject).getNextSibling();
    }
  }
  
  public String getAttribute(String paramString)
  {
    Hashtable localHashtable = this.attributes_;
    if (localHashtable == null) {
      paramString = null;
    } else {
      paramString = (String)localHashtable.get(paramString);
    }
    return paramString;
  }
  
  public Enumeration getAttributeNames()
  {
    Vector localVector = this.attributeNames_;
    if (localVector == null) {
      return Document.EMPTY;
    }
    return localVector.elements();
  }
  
  public Node getFirstChild()
  {
    return this.firstChild_;
  }
  
  public Node getLastChild()
  {
    return this.lastChild_;
  }
  
  public String getTagName()
  {
    return this.tagName_;
  }
  
  public void removeAttribute(String paramString)
  {
    Hashtable localHashtable = this.attributes_;
    if (localHashtable == null) {
      return;
    }
    localHashtable.remove(paramString);
    this.attributeNames_.removeElement(paramString);
    notifyObservers();
  }
  
  public void removeChild(Node paramNode)
    throws DOMException
  {
    if (removeChildNoChecking(paramNode))
    {
      notifyObservers();
      return;
    }
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("Cannot find ");
    localStringBuffer.append(paramNode);
    localStringBuffer.append(" in ");
    localStringBuffer.append(this);
    throw new DOMException((short)8, localStringBuffer.toString());
  }
  
  public void replaceChild(Element paramElement, Node paramNode)
    throws DOMException
  {
    replaceChild_(paramElement, paramNode);
    notifyObservers();
  }
  
  public void replaceChild(Text paramText, Node paramNode)
    throws DOMException
  {
    replaceChild_(paramText, paramNode);
    notifyObservers();
  }
  
  public void setAttribute(String paramString1, String paramString2)
  {
    if (this.attributes_ == null)
    {
      this.attributes_ = new Hashtable();
      this.attributeNames_ = new Vector();
    }
    if (this.attributes_.get(paramString1) == null) {
      this.attributeNames_.addElement(paramString1);
    }
    this.attributes_.put(paramString1, paramString2);
    notifyObservers();
  }
  
  public void setTagName(String paramString)
  {
    this.tagName_ = Sparta.intern(paramString);
    notifyObservers();
  }
  
  void toString(Writer paramWriter)
    throws IOException
  {
    for (Node localNode = this.firstChild_;; localNode = localNode.getNextSibling())
    {
      if (localNode == null) {
        return;
      }
      localNode.toString(paramWriter);
    }
  }
  
  public void toXml(Writer paramWriter)
    throws IOException
  {
    Object localObject = new StringBuffer();
    ((StringBuffer)localObject).append("<");
    ((StringBuffer)localObject).append(this.tagName_);
    paramWriter.write(((StringBuffer)localObject).toString());
    localObject = this.attributeNames_;
    if (localObject != null)
    {
      Enumeration localEnumeration = ((Vector)localObject).elements();
      while (localEnumeration.hasMoreElements())
      {
        String str = (String)localEnumeration.nextElement();
        localObject = (String)this.attributes_.get(str);
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append(" ");
        localStringBuffer.append(str);
        localStringBuffer.append("=\"");
        paramWriter.write(localStringBuffer.toString());
        Node.htmlEncode(paramWriter, (String)localObject);
        paramWriter.write("\"");
      }
    }
    if (this.firstChild_ == null)
    {
      localObject = "/>";
      paramWriter.write((String)localObject);
    }
    else
    {
      paramWriter.write(">");
    }
    for (localObject = this.firstChild_;; localObject = ((Node)localObject).getNextSibling())
    {
      if (localObject == null)
      {
        localObject = new StringBuffer();
        ((StringBuffer)localObject).append("</");
        ((StringBuffer)localObject).append(this.tagName_);
        ((StringBuffer)localObject).append(">");
        localObject = ((StringBuffer)localObject).toString();
        break;
        return;
      }
      ((Node)localObject).toXml(paramWriter);
    }
  }
  
  /* Error */
  public boolean xpathEnsure(String paramString)
    throws ParseException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 271	com/hp/hpl/sparta/Element:xpathSelectElement	(Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   5: astore 4
    //   7: iconst_0
    //   8: istore_3
    //   9: aload 4
    //   11: ifnull +5 -> 16
    //   14: iconst_0
    //   15: ireturn
    //   16: aload_1
    //   17: invokestatic 105	com/hp/hpl/sparta/xpath/XPath:get	(Ljava/lang/String;)Lcom/hp/hpl/sparta/xpath/XPath;
    //   20: astore 4
    //   22: aload 4
    //   24: invokevirtual 274	com/hp/hpl/sparta/xpath/XPath:getSteps	()Ljava/util/Enumeration;
    //   27: astore 5
    //   29: iconst_0
    //   30: istore_2
    //   31: aload 5
    //   33: invokeinterface 169 1 0
    //   38: ifne +120 -> 158
    //   41: iload_2
    //   42: iconst_1
    //   43: isub
    //   44: anewarray 276	com/hp/hpl/sparta/xpath/Step
    //   47: astore 6
    //   49: aload 4
    //   51: invokevirtual 274	com/hp/hpl/sparta/xpath/XPath:getSteps	()Ljava/util/Enumeration;
    //   54: astore 5
    //   56: iload_3
    //   57: istore_2
    //   58: iload_2
    //   59: aload 6
    //   61: arraylength
    //   62: if_icmplt +76 -> 138
    //   65: aload 5
    //   67: invokeinterface 172 1 0
    //   72: checkcast 276	com/hp/hpl/sparta/xpath/Step
    //   75: astore 5
    //   77: aload 6
    //   79: arraylength
    //   80: ifne +9 -> 89
    //   83: aload_0
    //   84: astore 4
    //   86: goto +36 -> 122
    //   89: aload 4
    //   91: invokevirtual 279	com/hp/hpl/sparta/xpath/XPath:isAbsolute	()Z
    //   94: aload 6
    //   96: invokestatic 282	com/hp/hpl/sparta/xpath/XPath:get	(Z[Lcom/hp/hpl/sparta/xpath/Step;)Lcom/hp/hpl/sparta/xpath/XPath;
    //   99: invokevirtual 283	com/hp/hpl/sparta/xpath/XPath:toString	()Ljava/lang/String;
    //   102: astore 4
    //   104: aload_0
    //   105: aload 4
    //   107: invokevirtual 284	java/lang/String:toString	()Ljava/lang/String;
    //   110: invokevirtual 286	com/hp/hpl/sparta/Element:xpathEnsure	(Ljava/lang/String;)Z
    //   113: pop
    //   114: aload_0
    //   115: aload 4
    //   117: invokevirtual 271	com/hp/hpl/sparta/Element:xpathSelectElement	(Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   120: astore 4
    //   122: aload 4
    //   124: aload_0
    //   125: aload 4
    //   127: aload 5
    //   129: aload_1
    //   130: invokevirtual 290	com/hp/hpl/sparta/Node:makeMatching	(Lcom/hp/hpl/sparta/Element;Lcom/hp/hpl/sparta/xpath/Step;Ljava/lang/String;)Lcom/hp/hpl/sparta/Element;
    //   133: invokevirtual 136	com/hp/hpl/sparta/Element:appendChildNoChecking	(Lcom/hp/hpl/sparta/Node;)V
    //   136: iconst_1
    //   137: ireturn
    //   138: aload 6
    //   140: iload_2
    //   141: aload 5
    //   143: invokeinterface 172 1 0
    //   148: checkcast 276	com/hp/hpl/sparta/xpath/Step
    //   151: aastore
    //   152: iinc 2 1
    //   155: goto -97 -> 58
    //   158: aload 5
    //   160: invokeinterface 172 1 0
    //   165: pop
    //   166: iinc 2 1
    //   169: goto -138 -> 31
    //   172: astore 4
    //   174: new 267	com/hp/hpl/sparta/ParseException
    //   177: dup
    //   178: aload_1
    //   179: aload 4
    //   181: invokespecial 293	com/hp/hpl/sparta/ParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   184: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	185	0	this	Element
    //   0	185	1	paramString	String
    //   30	137	2	i	int
    //   8	49	3	j	int
    //   5	121	4	localObject1	Object
    //   172	8	4	localXPathException	XPathException
    //   27	132	5	localObject2	Object
    //   47	92	6	arrayOfStep	com.hp.hpl.sparta.xpath.Step[]
    // Exception table:
    //   from	to	target	type
    //   0	7	172	com/hp/hpl/sparta/xpath/XPathException
    //   16	29	172	com/hp/hpl/sparta/xpath/XPathException
    //   31	56	172	com/hp/hpl/sparta/xpath/XPathException
    //   58	83	172	com/hp/hpl/sparta/xpath/XPathException
    //   89	122	172	com/hp/hpl/sparta/xpath/XPathException
    //   122	136	172	com/hp/hpl/sparta/xpath/XPathException
    //   138	152	172	com/hp/hpl/sparta/xpath/XPathException
    //   158	166	172	com/hp/hpl/sparta/xpath/XPathException
  }
  
  public Element xpathSelectElement(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, false).getFirstResultElement();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public Enumeration xpathSelectElements(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, false).getResultEnumeration();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public String xpathSelectString(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, true).getFirstResultString();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
  
  public Enumeration xpathSelectStrings(String paramString)
    throws ParseException
  {
    try
    {
      paramString = visitor(paramString, true).getResultEnumeration();
      return paramString;
    }
    catch (XPathException paramString)
    {
      throw new ParseException("XPath problem", paramString);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/Element.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */