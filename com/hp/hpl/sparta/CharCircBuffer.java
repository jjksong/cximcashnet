package com.hp.hpl.sparta;

class CharCircBuffer
{
  private final int[] buf_;
  private boolean enabled_ = true;
  private int next_ = 0;
  private int total_ = 0;
  
  CharCircBuffer(int paramInt)
  {
    this.buf_ = new int[paramInt];
  }
  
  private void addRaw(int paramInt)
  {
    if (this.enabled_)
    {
      int[] arrayOfInt = this.buf_;
      int i = this.next_;
      arrayOfInt[i] = paramInt;
      this.next_ = ((i + 1) % arrayOfInt.length);
      this.total_ += 1;
    }
  }
  
  void addChar(char paramChar)
  {
    addRaw(paramChar);
  }
  
  void addInt(int paramInt)
  {
    addRaw(paramInt + 65536);
  }
  
  void addString(String paramString)
  {
    paramString = paramString.toCharArray();
    int j = paramString.length;
    for (int i = 0;; i++)
    {
      if (i >= j) {
        return;
      }
      addChar(paramString[i]);
    }
  }
  
  void disable()
  {
    this.enabled_ = false;
  }
  
  void enable()
  {
    this.enabled_ = true;
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer(this.buf_.length * 11 / 10);
    int i = this.total_;
    int[] arrayOfInt = this.buf_;
    if (i < arrayOfInt.length) {
      i = arrayOfInt.length - i;
    }
    for (i = 0;; i++)
    {
      arrayOfInt = this.buf_;
      if (i >= arrayOfInt.length) {
        return localStringBuffer.toString();
      }
      int j = arrayOfInt[((this.next_ + i) % arrayOfInt.length)];
      if (j < 65536) {
        localStringBuffer.append((char)j);
      } else {
        localStringBuffer.append(Integer.toString(j - 65536));
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/CharCircBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */