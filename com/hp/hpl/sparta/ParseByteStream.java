package com.hp.hpl.sparta;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

class ParseByteStream
  implements ParseSource
{
  private ParseCharStream parseSource_;
  
  public ParseByteStream(String paramString1, InputStream paramInputStream, ParseLog paramParseLog, String paramString2, ParseHandler paramParseHandler)
    throws ParseException, IOException
  {
    ParseLog localParseLog = paramParseLog;
    if (paramParseLog == null) {
      localParseLog = ParseSource.DEFAULT_LOG;
    }
    if (paramInputStream.markSupported())
    {
      paramInputStream.mark(ParseSource.MAXLOOKAHEAD);
      Object localObject = new byte[4];
      int i = paramInputStream.read((byte[])localObject);
      paramParseLog = paramString2;
      if (paramString2 == null) {
        paramParseLog = guessEncoding(paramString1, (byte[])localObject, i, localParseLog);
      }
      try
      {
        paramInputStream.reset();
        localObject = new java/io/InputStreamReader;
        ((InputStreamReader)localObject).<init>(paramInputStream, fixEncoding(paramParseLog));
        try
        {
          paramString2 = new com/hp/hpl/sparta/ParseCharStream;
          paramString2.<init>(paramString1, (Reader)localObject, localParseLog, paramParseLog, paramParseHandler);
          this.parseSource_ = paramString2;
        }
        catch (IOException paramString2)
        {
          paramString2 = new java/lang/StringBuffer;
          paramString2.<init>();
          paramString2.append("Problem reading with assumed encoding of ");
          paramString2.append(paramParseLog);
          paramString2.append(" so restarting with ");
          paramString2.append("euc-jp");
          localParseLog.note(paramString2.toString(), paramString1, 1);
          paramInputStream.reset();
          try
          {
            paramString2 = new java/io/InputStreamReader;
            paramString2.<init>(paramInputStream, fixEncoding("euc-jp"));
            localObject = new com/hp/hpl/sparta/ParseCharStream;
            ((ParseCharStream)localObject).<init>(paramString1, paramString2, localParseLog, null, paramParseHandler);
            this.parseSource_ = ((ParseCharStream)localObject);
          }
          catch (UnsupportedEncodingException paramString2)
          {
            paramString2 = new com/hp/hpl/sparta/ParseException;
            localObject = new java/lang/StringBuffer;
            ((StringBuffer)localObject).<init>();
            ((StringBuffer)localObject).append("\"");
            ((StringBuffer)localObject).append("euc-jp");
            ((StringBuffer)localObject).append("\" is not a supported encoding");
            paramString2.<init>(localParseLog, paramString1, 1, 0, "euc-jp", ((StringBuffer)localObject).toString());
            throw paramString2;
          }
        }
        throw new Error("Precondition violation: the InputStream passed to ParseByteStream must support mark");
      }
      catch (EncodingMismatchException paramString2)
      {
        paramString2 = paramString2.getDeclaredEncoding();
        localObject = new StringBuffer();
        ((StringBuffer)localObject).append("Encoding declaration of ");
        ((StringBuffer)localObject).append(paramString2);
        ((StringBuffer)localObject).append(" is different that assumed ");
        ((StringBuffer)localObject).append(paramParseLog);
        ((StringBuffer)localObject).append(" so restarting the parsing with the new encoding");
        localParseLog.note(((StringBuffer)localObject).toString(), paramString1, 1);
        paramInputStream.reset();
        try
        {
          paramInputStream = new InputStreamReader(paramInputStream, fixEncoding(paramString2));
          this.parseSource_ = new ParseCharStream(paramString1, paramInputStream, localParseLog, null, paramParseHandler);
          return;
        }
        catch (UnsupportedEncodingException paramInputStream)
        {
          paramInputStream = new StringBuffer();
          paramInputStream.append("\"");
          paramInputStream.append(paramString2);
          paramInputStream.append("\" is not a supported encoding");
          throw new ParseException(localParseLog, paramString1, 1, 0, paramString2, paramInputStream.toString());
        }
      }
    }
  }
  
  private static boolean equals(byte[] paramArrayOfByte, int paramInt)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramArrayOfByte[0] == (byte)(paramInt >>> 24))
    {
      bool1 = bool2;
      if (paramArrayOfByte[1] == (byte)(paramInt >>> 16 & 0xFF))
      {
        bool1 = bool2;
        if (paramArrayOfByte[2] == (byte)(paramInt >>> 8 & 0xFF))
        {
          bool1 = bool2;
          if (paramArrayOfByte[3] == (byte)(paramInt & 0xFF)) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  private static boolean equals(byte[] paramArrayOfByte, short paramShort)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramArrayOfByte[0] == (byte)(paramShort >>> 8))
    {
      bool1 = bool2;
      if (paramArrayOfByte[1] == (byte)(paramShort & 0xFF)) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private static String fixEncoding(String paramString)
  {
    String str = paramString;
    if (paramString.toLowerCase().equals("utf8")) {
      str = "UTF-8";
    }
    return str;
  }
  
  private static String guessEncoding(String paramString, byte[] paramArrayOfByte, int paramInt, ParseLog paramParseLog)
    throws IOException
  {
    if (paramInt != 4)
    {
      if (paramInt <= 0)
      {
        localObject = "no characters in input";
      }
      else
      {
        localObject = new StringBuffer();
        ((StringBuffer)localObject).append("less than 4 characters in input: \"");
        ((StringBuffer)localObject).append(new String(paramArrayOfByte, 0, paramInt));
        ((StringBuffer)localObject).append("\"");
        localObject = ((StringBuffer)localObject).toString();
      }
      paramParseLog.error((String)localObject, paramString, 1);
    }
    do
    {
      do
      {
        localObject = "UTF-8";
        break label247;
        if ((equals(paramArrayOfByte, 65279)) || (equals(paramArrayOfByte, -131072)) || (equals(paramArrayOfByte, 65534)) || (equals(paramArrayOfByte, -16842752)) || (equals(paramArrayOfByte, 60)) || (equals(paramArrayOfByte, 1006632960)) || (equals(paramArrayOfByte, 15360)) || (equals(paramArrayOfByte, 3932160))) {
          break;
        }
        if (equals(paramArrayOfByte, 3932223))
        {
          localObject = "UTF-16BE";
          break label247;
        }
        if (equals(paramArrayOfByte, 1006649088))
        {
          localObject = "UTF-16LE";
          break label247;
        }
      } while (equals(paramArrayOfByte, 1010792557));
      if (equals(paramArrayOfByte, 1282385812))
      {
        localObject = "EBCDIC";
        break label247;
      }
    } while ((!equals(paramArrayOfByte, (short)-2)) && (!equals(paramArrayOfByte, (short)65279)));
    Object localObject = "UTF-16";
    break label247;
    localObject = "UCS-4";
    label247:
    if (!((String)localObject).equals("UTF-8"))
    {
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("From start ");
      localStringBuffer.append(hex(paramArrayOfByte[0]));
      localStringBuffer.append(" ");
      localStringBuffer.append(hex(paramArrayOfByte[1]));
      localStringBuffer.append(" ");
      localStringBuffer.append(hex(paramArrayOfByte[2]));
      localStringBuffer.append(" ");
      localStringBuffer.append(hex(paramArrayOfByte[3]));
      localStringBuffer.append(" deduced encoding = ");
      localStringBuffer.append((String)localObject);
      paramParseLog.note(localStringBuffer.toString(), paramString, 1);
    }
    return (String)localObject;
  }
  
  private static String hex(byte paramByte)
  {
    String str = Integer.toHexString(paramByte);
    Object localObject = str;
    switch (str.length())
    {
    default: 
      localObject = str.substring(str.length() - 2);
    case 2: 
      return (String)localObject;
    }
    localObject = new StringBuffer();
    ((StringBuffer)localObject).append("0");
    ((StringBuffer)localObject).append(str);
    return ((StringBuffer)localObject).toString();
  }
  
  public int getLineNumber()
  {
    return this.parseSource_.getLineNumber();
  }
  
  public String getSystemId()
  {
    return this.parseSource_.getSystemId();
  }
  
  public String toString()
  {
    return this.parseSource_.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseByteStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */