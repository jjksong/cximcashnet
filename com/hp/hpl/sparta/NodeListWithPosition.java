package com.hp.hpl.sparta;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

class NodeListWithPosition
{
  private static final Integer EIGHT = new Integer(8);
  private static final Integer FIVE;
  private static final Integer FOUR;
  private static final Integer NINE = new Integer(9);
  private static final Integer ONE = new Integer(1);
  private static final Integer SEVEN;
  private static final Integer SIX;
  private static final Integer TEN = new Integer(10);
  private static final Integer THREE;
  private static final Integer TWO = new Integer(2);
  private Hashtable positions_ = new Hashtable();
  private final Vector vector_ = new Vector();
  
  static
  {
    THREE = new Integer(3);
    FOUR = new Integer(4);
    FIVE = new Integer(5);
    SIX = new Integer(6);
    SEVEN = new Integer(7);
  }
  
  private static Integer identity(Node paramNode)
  {
    return new Integer(System.identityHashCode(paramNode));
  }
  
  void add(Node paramNode, int paramInt)
  {
    this.vector_.addElement(paramNode);
    Integer localInteger;
    switch (paramInt)
    {
    default: 
      localInteger = new Integer(paramInt);
      break;
    case 10: 
      localInteger = TEN;
      break;
    case 9: 
      localInteger = NINE;
      break;
    case 8: 
      localInteger = EIGHT;
      break;
    case 7: 
      localInteger = SEVEN;
      break;
    case 6: 
      localInteger = SIX;
      break;
    case 5: 
      localInteger = FIVE;
      break;
    case 4: 
      localInteger = FOUR;
      break;
    case 3: 
      localInteger = THREE;
      break;
    case 2: 
      localInteger = TWO;
      break;
    case 1: 
      localInteger = ONE;
    }
    this.positions_.put(identity(paramNode), localInteger);
  }
  
  void add(String paramString)
  {
    this.vector_.addElement(paramString);
  }
  
  Enumeration iterator()
  {
    return this.vector_.elements();
  }
  
  int position(Node paramNode)
  {
    return ((Integer)this.positions_.get(identity(paramNode))).intValue();
  }
  
  void removeAllElements()
  {
    this.vector_.removeAllElements();
    this.positions_.clear();
  }
  
  /* Error */
  public String toString()
  {
    // Byte code:
    //   0: new 108	java/lang/StringBuffer
    //   3: astore_3
    //   4: aload_3
    //   5: ldc 110
    //   7: invokespecial 112	java/lang/StringBuffer:<init>	(Ljava/lang/String;)V
    //   10: aload_0
    //   11: getfield 55	com/hp/hpl/sparta/NodeListWithPosition:vector_	Ljava/util/Vector;
    //   14: invokevirtual 86	java/util/Vector:elements	()Ljava/util/Enumeration;
    //   17: astore_2
    //   18: aload_2
    //   19: invokeinterface 118 1 0
    //   24: ifne +15 -> 39
    //   27: aload_3
    //   28: ldc 120
    //   30: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   33: pop
    //   34: aload_3
    //   35: invokevirtual 126	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   38: areturn
    //   39: aload_2
    //   40: invokeinterface 130 1 0
    //   45: astore_1
    //   46: aload_1
    //   47: instanceof 132
    //   50: ifeq +51 -> 101
    //   53: new 108	java/lang/StringBuffer
    //   56: astore 4
    //   58: aload 4
    //   60: invokespecial 133	java/lang/StringBuffer:<init>	()V
    //   63: aload 4
    //   65: ldc -121
    //   67: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   70: pop
    //   71: aload 4
    //   73: aload_1
    //   74: invokevirtual 138	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   77: pop
    //   78: aload 4
    //   80: ldc -116
    //   82: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   85: pop
    //   86: aload 4
    //   88: invokevirtual 126	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   91: astore_1
    //   92: aload_3
    //   93: aload_1
    //   94: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   97: pop
    //   98: goto -80 -> 18
    //   101: aload_1
    //   102: checkcast 142	com/hp/hpl/sparta/Node
    //   105: astore 4
    //   107: new 108	java/lang/StringBuffer
    //   110: astore_1
    //   111: aload_1
    //   112: invokespecial 133	java/lang/StringBuffer:<init>	()V
    //   115: aload_1
    //   116: ldc -112
    //   118: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   121: pop
    //   122: aload_1
    //   123: aload 4
    //   125: invokevirtual 147	com/hp/hpl/sparta/Node:toXml	()Ljava/lang/String;
    //   128: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   131: pop
    //   132: aload_1
    //   133: ldc -107
    //   135: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   138: pop
    //   139: aload_1
    //   140: aload_0
    //   141: getfield 60	com/hp/hpl/sparta/NodeListWithPosition:positions_	Ljava/util/Hashtable;
    //   144: aload 4
    //   146: invokestatic 76	com/hp/hpl/sparta/NodeListWithPosition:identity	(Lcom/hp/hpl/sparta/Node;)Ljava/lang/Integer;
    //   149: invokevirtual 92	java/util/Hashtable:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   152: invokevirtual 138	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   155: pop
    //   156: aload_1
    //   157: ldc -105
    //   159: invokevirtual 124	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   162: pop
    //   163: aload_1
    //   164: invokevirtual 126	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   167: astore_1
    //   168: goto -76 -> 92
    //   171: astore_1
    //   172: aload_1
    //   173: invokevirtual 154	java/lang/Throwable:toString	()Ljava/lang/String;
    //   176: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	177	0	this	NodeListWithPosition
    //   45	123	1	localObject1	Object
    //   171	2	1	localIOException	java.io.IOException
    //   17	23	2	localEnumeration	Enumeration
    //   3	90	3	localStringBuffer	StringBuffer
    //   56	89	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   0	18	171	java/io/IOException
    //   18	39	171	java/io/IOException
    //   39	92	171	java/io/IOException
    //   92	98	171	java/io/IOException
    //   101	168	171	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/NodeListWithPosition.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */