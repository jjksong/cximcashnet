package com.hp.hpl.sparta;

class BuildDocument
  implements DocumentSource, ParseHandler
{
  private Element currentElement_ = null;
  private final Document doc_ = new Document();
  private final ParseLog log_;
  private ParseSource parseSource_ = null;
  
  public BuildDocument()
  {
    this(null);
  }
  
  public BuildDocument(ParseLog paramParseLog)
  {
    ParseLog localParseLog = paramParseLog;
    if (paramParseLog == null) {
      localParseLog = ParseSource.DEFAULT_LOG;
    }
    this.log_ = localParseLog;
  }
  
  public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    Element localElement = this.currentElement_;
    if ((localElement.getLastChild() instanceof Text)) {
      ((Text)localElement.getLastChild()).appendData(paramArrayOfChar, paramInt1, paramInt2);
    } else {
      localElement.appendChildNoChecking(new Text(new String(paramArrayOfChar, paramInt1, paramInt2)));
    }
  }
  
  public void endDocument() {}
  
  public void endElement(Element paramElement)
  {
    this.currentElement_ = this.currentElement_.getParentNode();
  }
  
  public Document getDocument()
  {
    return this.doc_;
  }
  
  public int getLineNumber()
  {
    ParseSource localParseSource = this.parseSource_;
    if (localParseSource != null) {
      return localParseSource.getLineNumber();
    }
    return -1;
  }
  
  public ParseSource getParseSource()
  {
    return this.parseSource_;
  }
  
  public String getSystemId()
  {
    ParseSource localParseSource = this.parseSource_;
    if (localParseSource != null) {
      return localParseSource.getSystemId();
    }
    return null;
  }
  
  public void setParseSource(ParseSource paramParseSource)
  {
    this.parseSource_ = paramParseSource;
    this.doc_.setSystemId(paramParseSource.toString());
  }
  
  public void startDocument() {}
  
  public void startElement(Element paramElement)
  {
    Element localElement = this.currentElement_;
    if (localElement == null) {
      this.doc_.setDocumentElement(paramElement);
    } else {
      localElement.appendChild(paramElement);
    }
    this.currentElement_ = paramElement;
  }
  
  public String toString()
  {
    if (this.parseSource_ != null)
    {
      StringBuffer localStringBuffer = new StringBuffer();
      localStringBuffer.append("BuildDoc: ");
      localStringBuffer.append(this.parseSource_.toString());
      return localStringBuffer.toString();
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/BuildDocument.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */