package com.hp.hpl.sparta;

abstract interface DocumentSource
  extends ParseSource
{
  public abstract Document getDocument();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/DocumentSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */