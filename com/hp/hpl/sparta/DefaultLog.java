package com.hp.hpl.sparta;

import java.io.PrintStream;

class DefaultLog
  implements ParseLog
{
  public void error(String paramString1, String paramString2, int paramInt)
  {
    PrintStream localPrintStream = System.err;
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramString2);
    localStringBuffer.append("(");
    localStringBuffer.append(paramInt);
    localStringBuffer.append("): ");
    localStringBuffer.append(paramString1);
    localStringBuffer.append(" (ERROR)");
    localPrintStream.println(localStringBuffer.toString());
  }
  
  public void note(String paramString1, String paramString2, int paramInt)
  {
    PrintStream localPrintStream = System.out;
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramString2);
    localStringBuffer.append("(");
    localStringBuffer.append(paramInt);
    localStringBuffer.append("): ");
    localStringBuffer.append(paramString1);
    localStringBuffer.append(" (NOTE)");
    localPrintStream.println(localStringBuffer.toString());
  }
  
  public void warning(String paramString1, String paramString2, int paramInt)
  {
    PrintStream localPrintStream = System.out;
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramString2);
    localStringBuffer.append("(");
    localStringBuffer.append(paramInt);
    localStringBuffer.append("): ");
    localStringBuffer.append(paramString1);
    localStringBuffer.append(" (WARNING)");
    localPrintStream.println(localStringBuffer.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/DefaultLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */