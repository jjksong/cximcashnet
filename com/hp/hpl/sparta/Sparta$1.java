package com.hp.hpl.sparta;

import java.util.Hashtable;

class Sparta$1
  implements Sparta.Internment
{
  private final Hashtable strings_ = new Hashtable();
  
  public String intern(String paramString)
  {
    String str = (String)this.strings_.get(paramString);
    if (str == null)
    {
      this.strings_.put(paramString, paramString);
      return paramString;
    }
    return str;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/Sparta$1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */