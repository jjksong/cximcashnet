package com.hp.hpl.sparta;

public class DefaultParseHandler
  implements ParseHandler
{
  private ParseSource parseSource_ = null;
  
  public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    throws ParseException
  {}
  
  public void endDocument()
    throws ParseException
  {}
  
  public void endElement(Element paramElement)
    throws ParseException
  {}
  
  public ParseSource getParseSource()
  {
    return this.parseSource_;
  }
  
  public void setParseSource(ParseSource paramParseSource)
  {
    this.parseSource_ = paramParseSource;
  }
  
  public void startDocument()
    throws ParseException
  {}
  
  public void startElement(Element paramElement)
    throws ParseException
  {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/DefaultParseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */