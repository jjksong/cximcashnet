package com.hp.hpl.sparta;

public abstract interface ParseHandler
{
  public abstract void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    throws ParseException;
  
  public abstract void endDocument()
    throws ParseException;
  
  public abstract void endElement(Element paramElement)
    throws ParseException;
  
  public abstract ParseSource getParseSource();
  
  public abstract void setParseSource(ParseSource paramParseSource);
  
  public abstract void startDocument()
    throws ParseException;
  
  public abstract void startElement(Element paramElement)
    throws ParseException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */