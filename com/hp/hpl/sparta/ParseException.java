package com.hp.hpl.sparta;

public class ParseException
  extends Exception
{
  private Throwable cause_ = null;
  private int lineNumber_ = -1;
  
  public ParseException(ParseCharStream paramParseCharStream, char paramChar1, char paramChar2)
  {
    this(paramParseCharStream, localStringBuffer.toString());
  }
  
  public ParseException(ParseCharStream paramParseCharStream, char paramChar, String paramString)
  {
    this(paramParseCharStream, localStringBuffer.toString());
  }
  
  public ParseException(ParseCharStream paramParseCharStream, char paramChar, char[] paramArrayOfChar)
  {
    this(paramParseCharStream, localStringBuffer.toString());
  }
  
  public ParseException(ParseCharStream paramParseCharStream, String paramString)
  {
    this(paramParseCharStream.getLog(), paramParseCharStream.getSystemId(), paramParseCharStream.getLineNumber(), paramParseCharStream.getLastCharRead(), paramParseCharStream.getHistory(), paramString);
  }
  
  public ParseException(ParseCharStream paramParseCharStream, String paramString1, String paramString2)
  {
    this(paramParseCharStream, localStringBuffer.toString());
  }
  
  public ParseException(ParseCharStream paramParseCharStream, String paramString, char[] paramArrayOfChar)
  {
    this(paramParseCharStream, paramString, new String(paramArrayOfChar));
  }
  
  public ParseException(ParseLog paramParseLog, String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3)
  {
    this(paramString1, paramInt1, paramInt2, paramString2, paramString3);
    paramParseLog.error(paramString3, paramString1, paramInt1);
  }
  
  public ParseException(String paramString)
  {
    super(paramString);
  }
  
  public ParseException(String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3)
  {
    super(toMessage(paramString1, paramInt1, paramInt2, paramString2, paramString3));
    this.lineNumber_ = paramInt1;
  }
  
  public ParseException(String paramString, Throwable paramThrowable)
  {
    super(localStringBuffer.toString());
    this.cause_ = paramThrowable;
  }
  
  static String charRepr(int paramInt)
  {
    Object localObject;
    if (paramInt == -1)
    {
      localObject = "EOF";
    }
    else
    {
      localObject = new StringBuffer();
      ((StringBuffer)localObject).append("");
      ((StringBuffer)localObject).append((char)paramInt);
      localObject = ((StringBuffer)localObject).toString();
    }
    return (String)localObject;
  }
  
  private static String toMessage(String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramString1);
    localStringBuffer.append("(");
    localStringBuffer.append(paramInt1);
    localStringBuffer.append("): \n");
    localStringBuffer.append(paramString2);
    localStringBuffer.append("\nLast character read was '");
    localStringBuffer.append(charRepr(paramInt2));
    localStringBuffer.append("'\n");
    localStringBuffer.append(paramString3);
    return localStringBuffer.toString();
  }
  
  private static String toString(char[] paramArrayOfChar)
  {
    StringBuffer localStringBuffer2 = new StringBuffer();
    localStringBuffer2.append(paramArrayOfChar[0]);
    for (int i = 1;; i++)
    {
      if (i >= paramArrayOfChar.length) {
        return localStringBuffer2.toString();
      }
      StringBuffer localStringBuffer1 = new StringBuffer();
      localStringBuffer1.append("or ");
      localStringBuffer1.append(paramArrayOfChar[i]);
      localStringBuffer2.append(localStringBuffer1.toString());
    }
  }
  
  public Throwable getCause()
  {
    return this.cause_;
  }
  
  public int getLineNumber()
  {
    return this.lineNumber_;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/hp/hpl/sparta/ParseException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */