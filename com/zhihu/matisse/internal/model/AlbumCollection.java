package com.zhihu.matisse.internal.model;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import com.zhihu.matisse.internal.loader.AlbumLoader;
import java.lang.ref.WeakReference;

public class AlbumCollection
  implements LoaderManager.LoaderCallbacks<Cursor>
{
  private static final int LOADER_ID = 1;
  private static final String STATE_CURRENT_SELECTION = "state_current_selection";
  private AlbumCallbacks mCallbacks;
  private WeakReference<Context> mContext;
  private int mCurrentSelection;
  private boolean mLoadFinished;
  private LoaderManager mLoaderManager;
  
  public int getCurrentSelection()
  {
    return this.mCurrentSelection;
  }
  
  public void loadAlbums()
  {
    this.mLoaderManager.initLoader(1, null, this);
  }
  
  public void onCreate(FragmentActivity paramFragmentActivity, AlbumCallbacks paramAlbumCallbacks)
  {
    this.mContext = new WeakReference(paramFragmentActivity);
    this.mLoaderManager = paramFragmentActivity.getSupportLoaderManager();
    this.mCallbacks = paramAlbumCallbacks;
  }
  
  public Loader<Cursor> onCreateLoader(int paramInt, Bundle paramBundle)
  {
    paramBundle = (Context)this.mContext.get();
    if (paramBundle == null) {
      return null;
    }
    this.mLoadFinished = false;
    return AlbumLoader.newInstance(paramBundle);
  }
  
  public void onDestroy()
  {
    LoaderManager localLoaderManager = this.mLoaderManager;
    if (localLoaderManager != null) {
      localLoaderManager.destroyLoader(1);
    }
    this.mCallbacks = null;
  }
  
  public void onLoadFinished(Loader<Cursor> paramLoader, Cursor paramCursor)
  {
    if ((Context)this.mContext.get() == null) {
      return;
    }
    if (!this.mLoadFinished)
    {
      this.mLoadFinished = true;
      this.mCallbacks.onAlbumLoad(paramCursor);
    }
  }
  
  public void onLoaderReset(Loader<Cursor> paramLoader)
  {
    if ((Context)this.mContext.get() == null) {
      return;
    }
    this.mCallbacks.onAlbumReset();
  }
  
  public void onRestoreInstanceState(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return;
    }
    this.mCurrentSelection = paramBundle.getInt("state_current_selection");
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putInt("state_current_selection", this.mCurrentSelection);
  }
  
  public void setStateCurrentSelection(int paramInt)
  {
    this.mCurrentSelection = paramInt;
  }
  
  public static abstract interface AlbumCallbacks
  {
    public abstract void onAlbumLoad(Cursor paramCursor);
    
    public abstract void onAlbumReset();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/model/AlbumCollection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */