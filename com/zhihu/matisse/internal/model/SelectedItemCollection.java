package com.zhihu.matisse.internal.model;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.net.Uri;
import android.os.Bundle;
import com.zhihu.matisse.R.plurals;
import com.zhihu.matisse.R.string;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.utils.PathUtils;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SelectedItemCollection
{
  public static final int COLLECTION_IMAGE = 1;
  public static final int COLLECTION_MIXED = 3;
  public static final int COLLECTION_UNDEFINED = 0;
  public static final int COLLECTION_VIDEO = 2;
  public static final String STATE_COLLECTION_TYPE = "state_collection_type";
  public static final String STATE_SELECTION = "state_selection";
  private int mCollectionType = 0;
  private final Context mContext;
  private Set<Item> mItems;
  
  public SelectedItemCollection(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  private int currentMaxSelectable()
  {
    SelectionSpec localSelectionSpec = SelectionSpec.getInstance();
    if (localSelectionSpec.maxSelectable > 0) {
      return localSelectionSpec.maxSelectable;
    }
    int i = this.mCollectionType;
    if (i == 1) {
      return localSelectionSpec.maxImageSelectable;
    }
    if (i == 2) {
      return localSelectionSpec.maxVideoSelectable;
    }
    return localSelectionSpec.maxSelectable;
  }
  
  private void refineCollectionType()
  {
    Iterator localIterator = this.mItems.iterator();
    int j = 0;
    int i = 0;
    while (localIterator.hasNext())
    {
      Item localItem = (Item)localIterator.next();
      int k = j;
      if (localItem.isImage())
      {
        k = j;
        if (j == 0) {
          k = 1;
        }
      }
      j = k;
      if (localItem.isVideo())
      {
        j = k;
        if (i == 0)
        {
          i = 1;
          j = k;
        }
      }
    }
    if ((j != 0) && (i != 0)) {
      this.mCollectionType = 3;
    } else if (j != 0) {
      this.mCollectionType = 1;
    } else if (i != 0) {
      this.mCollectionType = 2;
    }
  }
  
  public boolean add(Item paramItem)
  {
    if (!typeConflict(paramItem))
    {
      boolean bool = this.mItems.add(paramItem);
      if (bool)
      {
        int i = this.mCollectionType;
        if (i == 0)
        {
          if (paramItem.isImage()) {
            this.mCollectionType = 1;
          } else if (paramItem.isVideo()) {
            this.mCollectionType = 2;
          }
        }
        else if (i == 1)
        {
          if (paramItem.isVideo()) {
            this.mCollectionType = 3;
          }
        }
        else if ((i == 2) && (paramItem.isImage())) {
          this.mCollectionType = 3;
        }
      }
      return bool;
    }
    throw new IllegalArgumentException("Can't select images and videos at the same time.");
  }
  
  public List<Item> asList()
  {
    return new ArrayList(this.mItems);
  }
  
  public List<String> asListOfString()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mItems.iterator();
    while (localIterator.hasNext())
    {
      Item localItem = (Item)localIterator.next();
      localArrayList.add(PathUtils.getPath(this.mContext, localItem.getContentUri()));
    }
    return localArrayList;
  }
  
  public List<Uri> asListOfUri()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mItems.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(((Item)localIterator.next()).getContentUri());
    }
    return localArrayList;
  }
  
  public int checkedNumOf(Item paramItem)
  {
    int i = new ArrayList(this.mItems).indexOf(paramItem);
    if (i == -1) {
      i = Integer.MIN_VALUE;
    } else {
      i++;
    }
    return i;
  }
  
  public int count()
  {
    return this.mItems.size();
  }
  
  public int getCollectionType()
  {
    return this.mCollectionType;
  }
  
  public Bundle getDataWithBundle()
  {
    Bundle localBundle = new Bundle();
    localBundle.putParcelableArrayList("state_selection", new ArrayList(this.mItems));
    localBundle.putInt("state_collection_type", this.mCollectionType);
    return localBundle;
  }
  
  public IncapableCause isAcceptable(Item paramItem)
  {
    if (maxSelectableReached())
    {
      int i = currentMaxSelectable();
      try
      {
        paramItem = this.mContext.getResources().getQuantityString(R.plurals.error_over_count, i, new Object[] { Integer.valueOf(i) });
      }
      catch (NoClassDefFoundError paramItem)
      {
        paramItem = this.mContext.getString(R.string.error_over_count, new Object[] { Integer.valueOf(i) });
      }
      catch (Resources.NotFoundException paramItem)
      {
        paramItem = this.mContext.getString(R.string.error_over_count, new Object[] { Integer.valueOf(i) });
      }
      return new IncapableCause(paramItem);
    }
    if (typeConflict(paramItem)) {
      return new IncapableCause(this.mContext.getString(R.string.error_type_conflict));
    }
    return PhotoMetadataUtils.isAcceptable(this.mContext, paramItem);
  }
  
  public boolean isEmpty()
  {
    Set localSet = this.mItems;
    boolean bool;
    if ((localSet != null) && (!localSet.isEmpty())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isSelected(Item paramItem)
  {
    return this.mItems.contains(paramItem);
  }
  
  public boolean maxSelectableReached()
  {
    boolean bool;
    if (this.mItems.size() == currentMaxSelectable()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    if (paramBundle == null)
    {
      this.mItems = new LinkedHashSet();
    }
    else
    {
      this.mItems = new LinkedHashSet(paramBundle.getParcelableArrayList("state_selection"));
      this.mCollectionType = paramBundle.getInt("state_collection_type", 0);
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putParcelableArrayList("state_selection", new ArrayList(this.mItems));
    paramBundle.putInt("state_collection_type", this.mCollectionType);
  }
  
  public void overwrite(ArrayList<Item> paramArrayList, int paramInt)
  {
    if (paramArrayList.size() == 0) {
      this.mCollectionType = 0;
    } else {
      this.mCollectionType = paramInt;
    }
    this.mItems.clear();
    this.mItems.addAll(paramArrayList);
  }
  
  public boolean remove(Item paramItem)
  {
    boolean bool = this.mItems.remove(paramItem);
    if (bool) {
      if (this.mItems.size() == 0) {
        this.mCollectionType = 0;
      } else if (this.mCollectionType == 3) {
        refineCollectionType();
      }
    }
    return bool;
  }
  
  public void setDefaultSelection(List<Item> paramList)
  {
    this.mItems.addAll(paramList);
  }
  
  public boolean typeConflict(Item paramItem)
  {
    boolean bool1 = SelectionSpec.getInstance().mediaTypeExclusive;
    boolean bool2 = true;
    if (bool1)
    {
      int i;
      if (paramItem.isImage())
      {
        i = this.mCollectionType;
        bool1 = bool2;
        if (i == 2) {
          return bool1;
        }
        bool1 = bool2;
        if (i == 3) {
          return bool1;
        }
      }
      if (paramItem.isVideo())
      {
        i = this.mCollectionType;
        bool1 = bool2;
        if (i == 1) {
          return bool1;
        }
        if (i == 3) {
          return bool2;
        }
      }
    }
    bool1 = false;
    return bool1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/model/SelectedItemCollection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */