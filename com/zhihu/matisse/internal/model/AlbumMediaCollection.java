package com.zhihu.matisse.internal.model;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.loader.AlbumMediaLoader;
import java.lang.ref.WeakReference;

public class AlbumMediaCollection
  implements LoaderManager.LoaderCallbacks<Cursor>
{
  private static final String ARGS_ALBUM = "args_album";
  private static final String ARGS_ENABLE_CAPTURE = "args_enable_capture";
  private static final int LOADER_ID = 2;
  private AlbumMediaCallbacks mCallbacks;
  private WeakReference<Context> mContext;
  private LoaderManager mLoaderManager;
  
  public void load(@Nullable Album paramAlbum)
  {
    load(paramAlbum, false);
  }
  
  public void load(@Nullable Album paramAlbum, boolean paramBoolean)
  {
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("args_album", paramAlbum);
    localBundle.putBoolean("args_enable_capture", paramBoolean);
    this.mLoaderManager.initLoader(2, localBundle, this);
  }
  
  public void onCreate(@NonNull FragmentActivity paramFragmentActivity, @NonNull AlbumMediaCallbacks paramAlbumMediaCallbacks)
  {
    this.mContext = new WeakReference(paramFragmentActivity);
    this.mLoaderManager = paramFragmentActivity.getSupportLoaderManager();
    this.mCallbacks = paramAlbumMediaCallbacks;
  }
  
  public Loader<Cursor> onCreateLoader(int paramInt, Bundle paramBundle)
  {
    Context localContext = (Context)this.mContext.get();
    if (localContext == null) {
      return null;
    }
    Album localAlbum = (Album)paramBundle.getParcelable("args_album");
    if (localAlbum == null) {
      return null;
    }
    boolean bool3 = localAlbum.isAll();
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (bool3)
    {
      bool1 = bool2;
      if (paramBundle.getBoolean("args_enable_capture", false)) {
        bool1 = true;
      }
    }
    return AlbumMediaLoader.newInstance(localContext, localAlbum, bool1);
  }
  
  public void onDestroy()
  {
    LoaderManager localLoaderManager = this.mLoaderManager;
    if (localLoaderManager != null) {
      localLoaderManager.destroyLoader(2);
    }
    this.mCallbacks = null;
  }
  
  public void onLoadFinished(Loader<Cursor> paramLoader, Cursor paramCursor)
  {
    if ((Context)this.mContext.get() == null) {
      return;
    }
    this.mCallbacks.onAlbumMediaLoad(paramCursor);
  }
  
  public void onLoaderReset(Loader<Cursor> paramLoader)
  {
    if ((Context)this.mContext.get() == null) {
      return;
    }
    this.mCallbacks.onAlbumMediaReset();
  }
  
  public static abstract interface AlbumMediaCallbacks
  {
    public abstract void onAlbumMediaLoad(Cursor paramCursor);
    
    public abstract void onAlbumMediaReset();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/model/AlbumMediaCollection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */