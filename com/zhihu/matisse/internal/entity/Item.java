package com.zhihu.matisse.internal.entity;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Video.Media;
import android.support.annotation.Nullable;
import com.zhihu.matisse.MimeType;

public class Item
  implements Parcelable
{
  public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator()
  {
    @Nullable
    public Item createFromParcel(Parcel paramAnonymousParcel)
    {
      return new Item(paramAnonymousParcel, null);
    }
    
    public Item[] newArray(int paramAnonymousInt)
    {
      return new Item[paramAnonymousInt];
    }
  };
  public static final String ITEM_DISPLAY_NAME_CAPTURE = "Capture";
  public static final long ITEM_ID_CAPTURE = -1L;
  public final long duration;
  public final long id;
  public final String mimeType;
  public final long size;
  public final Uri uri;
  
  private Item(long paramLong1, String paramString, long paramLong2, long paramLong3)
  {
    this.id = paramLong1;
    this.mimeType = paramString;
    if (isImage()) {
      paramString = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    } else if (isVideo()) {
      paramString = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    } else {
      paramString = MediaStore.Files.getContentUri("external");
    }
    this.uri = ContentUris.withAppendedId(paramString, paramLong1);
    this.size = paramLong2;
    this.duration = paramLong3;
  }
  
  private Item(Parcel paramParcel)
  {
    this.id = paramParcel.readLong();
    this.mimeType = paramParcel.readString();
    this.uri = ((Uri)paramParcel.readParcelable(Uri.class.getClassLoader()));
    this.size = paramParcel.readLong();
    this.duration = paramParcel.readLong();
  }
  
  public static Item valueOf(Cursor paramCursor)
  {
    return new Item(paramCursor.getLong(paramCursor.getColumnIndex("_id")), paramCursor.getString(paramCursor.getColumnIndex("mime_type")), paramCursor.getLong(paramCursor.getColumnIndex("_size")), paramCursor.getLong(paramCursor.getColumnIndex("duration")));
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Item;
    boolean bool2 = false;
    if (!bool1) {
      return false;
    }
    paramObject = (Item)paramObject;
    bool1 = bool2;
    if (this.id == ((Item)paramObject).id)
    {
      Object localObject = this.mimeType;
      if ((localObject == null) || (!((String)localObject).equals(((Item)paramObject).mimeType)))
      {
        bool1 = bool2;
        if (this.mimeType == null)
        {
          bool1 = bool2;
          if (((Item)paramObject).mimeType != null) {}
        }
      }
      else
      {
        localObject = this.uri;
        if ((localObject == null) || (!((Uri)localObject).equals(((Item)paramObject).uri)))
        {
          bool1 = bool2;
          if (this.uri == null)
          {
            bool1 = bool2;
            if (((Item)paramObject).uri != null) {}
          }
        }
        else
        {
          bool1 = bool2;
          if (this.size == ((Item)paramObject).size)
          {
            bool1 = bool2;
            if (this.duration == ((Item)paramObject).duration) {
              bool1 = true;
            }
          }
        }
      }
    }
    return bool1;
  }
  
  public Uri getContentUri()
  {
    return this.uri;
  }
  
  public int hashCode()
  {
    int j = Long.valueOf(this.id).hashCode() + 31;
    String str = this.mimeType;
    int i = j;
    if (str != null) {
      i = j * 31 + str.hashCode();
    }
    return ((i * 31 + this.uri.hashCode()) * 31 + Long.valueOf(this.size).hashCode()) * 31 + Long.valueOf(this.duration).hashCode();
  }
  
  public boolean isCapture()
  {
    boolean bool;
    if (this.id == -1L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isGif()
  {
    String str = this.mimeType;
    if (str == null) {
      return false;
    }
    return str.equals(MimeType.GIF.toString());
  }
  
  public boolean isImage()
  {
    String str = this.mimeType;
    boolean bool = false;
    if (str == null) {
      return false;
    }
    if ((str.equals(MimeType.JPEG.toString())) || (this.mimeType.equals(MimeType.PNG.toString())) || (this.mimeType.equals(MimeType.GIF.toString())) || (this.mimeType.equals(MimeType.BMP.toString())) || (this.mimeType.equals(MimeType.WEBP.toString()))) {
      bool = true;
    }
    return bool;
  }
  
  public boolean isVideo()
  {
    String str = this.mimeType;
    boolean bool = false;
    if (str == null) {
      return false;
    }
    if ((str.equals(MimeType.MPEG.toString())) || (this.mimeType.equals(MimeType.MP4.toString())) || (this.mimeType.equals(MimeType.QUICKTIME.toString())) || (this.mimeType.equals(MimeType.THREEGPP.toString())) || (this.mimeType.equals(MimeType.THREEGPP2.toString())) || (this.mimeType.equals(MimeType.MKV.toString())) || (this.mimeType.equals(MimeType.WEBM.toString())) || (this.mimeType.equals(MimeType.TS.toString())) || (this.mimeType.equals(MimeType.AVI.toString()))) {
      bool = true;
    }
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeLong(this.id);
    paramParcel.writeString(this.mimeType);
    paramParcel.writeParcelable(this.uri, 0);
    paramParcel.writeLong(this.size);
    paramParcel.writeLong(this.duration);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/entity/Item.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */