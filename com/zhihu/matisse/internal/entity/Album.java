package com.zhihu.matisse.internal.entity;

import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.zhihu.matisse.R.string;

public class Album
  implements Parcelable
{
  public static final String ALBUM_ID_ALL = String.valueOf(-1);
  public static final String ALBUM_NAME_ALL = "All";
  public static final Parcelable.Creator<Album> CREATOR = new Parcelable.Creator()
  {
    @Nullable
    public Album createFromParcel(Parcel paramAnonymousParcel)
    {
      return new Album(paramAnonymousParcel);
    }
    
    public Album[] newArray(int paramAnonymousInt)
    {
      return new Album[paramAnonymousInt];
    }
  };
  private long mCount;
  private final String mCoverPath;
  private final String mDisplayName;
  private final String mId;
  
  Album(Parcel paramParcel)
  {
    this.mId = paramParcel.readString();
    this.mCoverPath = paramParcel.readString();
    this.mDisplayName = paramParcel.readString();
    this.mCount = paramParcel.readLong();
  }
  
  Album(String paramString1, String paramString2, String paramString3, long paramLong)
  {
    this.mId = paramString1;
    this.mCoverPath = paramString2;
    this.mDisplayName = paramString3;
    this.mCount = paramLong;
  }
  
  public static Album valueOf(Cursor paramCursor)
  {
    return new Album(paramCursor.getString(paramCursor.getColumnIndex("bucket_id")), paramCursor.getString(paramCursor.getColumnIndex("_data")), paramCursor.getString(paramCursor.getColumnIndex("bucket_display_name")), paramCursor.getLong(paramCursor.getColumnIndex("count")));
  }
  
  public void addCaptureCount()
  {
    this.mCount += 1L;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long getCount()
  {
    return this.mCount;
  }
  
  public String getCoverPath()
  {
    return this.mCoverPath;
  }
  
  public String getDisplayName(Context paramContext)
  {
    if (isAll()) {
      return paramContext.getString(R.string.album_name_all);
    }
    return this.mDisplayName;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public boolean isAll()
  {
    return ALBUM_ID_ALL.equals(this.mId);
  }
  
  public boolean isEmpty()
  {
    boolean bool;
    if (this.mCount == 0L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.mId);
    paramParcel.writeString(this.mCoverPath);
    paramParcel.writeString(this.mDisplayName);
    paramParcel.writeLong(this.mCount);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/entity/Album.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */