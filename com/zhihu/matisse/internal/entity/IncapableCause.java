package com.zhihu.matisse.internal.entity;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.zhihu.matisse.internal.ui.widget.IncapableDialog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class IncapableCause
{
  public static final int DIALOG = 1;
  public static final int NONE = 2;
  public static final int TOAST = 0;
  private int mForm = 0;
  private String mMessage;
  private String mTitle;
  
  public IncapableCause(int paramInt, String paramString)
  {
    this.mForm = paramInt;
    this.mMessage = paramString;
  }
  
  public IncapableCause(int paramInt, String paramString1, String paramString2)
  {
    this.mForm = paramInt;
    this.mTitle = paramString1;
    this.mMessage = paramString2;
  }
  
  public IncapableCause(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public IncapableCause(String paramString1, String paramString2)
  {
    this.mTitle = paramString1;
    this.mMessage = paramString2;
  }
  
  public static void handleCause(Context paramContext, IncapableCause paramIncapableCause)
  {
    if (paramIncapableCause == null) {
      return;
    }
    switch (paramIncapableCause.mForm)
    {
    default: 
      Toast.makeText(paramContext, paramIncapableCause.mMessage, 0).show();
      break;
    case 1: 
      IncapableDialog.newInstance(paramIncapableCause.mTitle, paramIncapableCause.mMessage).show(((FragmentActivity)paramContext).getSupportFragmentManager(), IncapableDialog.class.getName());
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Form {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/entity/IncapableCause.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */