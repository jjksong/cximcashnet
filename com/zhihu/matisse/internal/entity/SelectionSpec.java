package com.zhihu.matisse.internal.entity;

import android.support.annotation.StyleRes;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.R.style;
import com.zhihu.matisse.engine.ImageEngine;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnSelectedListener;
import java.util.List;
import java.util.Set;

public final class SelectionSpec
{
  public boolean autoHideToobar;
  public boolean capture;
  public CaptureStrategy captureStrategy;
  public boolean countable;
  public List<Filter> filters;
  public int gridExpectedSize;
  public boolean hasInited;
  public ImageEngine imageEngine;
  public int maxImageSelectable;
  public int maxSelectable;
  public int maxVideoSelectable;
  public boolean mediaTypeExclusive;
  public Set<MimeType> mimeTypeSet;
  public OnCheckedListener onCheckedListener;
  public OnSelectedListener onSelectedListener;
  public int orientation;
  public int originalMaxSize;
  public boolean originalable;
  public boolean showSingleMediaType;
  public int spanCount;
  @StyleRes
  public int themeId;
  public float thumbnailScale;
  
  public static SelectionSpec getCleanInstance()
  {
    SelectionSpec localSelectionSpec = getInstance();
    localSelectionSpec.reset();
    return localSelectionSpec;
  }
  
  public static SelectionSpec getInstance()
  {
    return InstanceHolder.INSTANCE;
  }
  
  private void reset()
  {
    this.mimeTypeSet = null;
    this.mediaTypeExclusive = true;
    this.showSingleMediaType = false;
    this.themeId = R.style.Matisse_Zhihu;
    this.orientation = 0;
    this.countable = false;
    this.maxSelectable = 1;
    this.maxImageSelectable = 0;
    this.maxVideoSelectable = 0;
    this.filters = null;
    this.capture = false;
    this.captureStrategy = null;
    this.spanCount = 3;
    this.gridExpectedSize = 0;
    this.thumbnailScale = 0.5F;
    this.imageEngine = new GlideEngine();
    this.hasInited = true;
    this.originalable = false;
    this.autoHideToobar = false;
    this.originalMaxSize = Integer.MAX_VALUE;
  }
  
  public boolean needOrientationRestriction()
  {
    boolean bool;
    if (this.orientation != -1) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean onlyShowImages()
  {
    boolean bool;
    if ((this.showSingleMediaType) && (MimeType.ofImage().containsAll(this.mimeTypeSet))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean onlyShowVideos()
  {
    boolean bool;
    if ((this.showSingleMediaType) && (MimeType.ofVideo().containsAll(this.mimeTypeSet))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean singleSelectionModeEnabled()
  {
    boolean bool1 = this.countable;
    boolean bool2 = true;
    if (!bool1)
    {
      bool1 = bool2;
      if (this.maxSelectable == 1) {
        return bool1;
      }
      if ((this.maxImageSelectable == 1) && (this.maxVideoSelectable == 1)) {
        return bool2;
      }
    }
    bool1 = false;
    return bool1;
  }
  
  private static final class InstanceHolder
  {
    private static final SelectionSpec INSTANCE = new SelectionSpec(null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/entity/SelectionSpec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */