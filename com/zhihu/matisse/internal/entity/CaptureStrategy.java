package com.zhihu.matisse.internal.entity;

public class CaptureStrategy
{
  public final String authority;
  public final String directory;
  public final boolean isPublic;
  
  public CaptureStrategy(boolean paramBoolean, String paramString)
  {
    this(paramBoolean, paramString, null);
  }
  
  public CaptureStrategy(boolean paramBoolean, String paramString1, String paramString2)
  {
    this.isPublic = paramBoolean;
    this.authority = paramString1;
    this.directory = paramString2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/entity/CaptureStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */