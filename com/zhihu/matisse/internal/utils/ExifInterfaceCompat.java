package com.zhihu.matisse.internal.utils;

import android.media.ExifInterface;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

final class ExifInterfaceCompat
{
  private static final int EXIF_DEGREE_FALLBACK_VALUE = -1;
  private static final String TAG = "ExifInterfaceCompat";
  
  private static Date getExifDateTime(String paramString)
  {
    try
    {
      paramString = newInstance(paramString);
      paramString = paramString.getAttribute("DateTime");
      if (TextUtils.isEmpty(paramString)) {
        return null;
      }
      try
      {
        SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
        localSimpleDateFormat.<init>("yyyy:MM:dd HH:mm:ss");
        localSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        paramString = localSimpleDateFormat.parse(paramString);
        return paramString;
      }
      catch (ParseException paramString)
      {
        Log.d(TAG, "failed to parse date taken", paramString);
        return null;
      }
      return null;
    }
    catch (IOException paramString)
    {
      Log.e(TAG, "cannot read exif", paramString);
    }
  }
  
  public static long getExifDateTimeInMillis(String paramString)
  {
    paramString = getExifDateTime(paramString);
    if (paramString == null) {
      return -1L;
    }
    return paramString.getTime();
  }
  
  public static int getExifOrientation(String paramString)
  {
    try
    {
      paramString = newInstance(paramString);
      int i = paramString.getAttributeInt("Orientation", -1);
      if (i == -1) {
        return 0;
      }
      if (i != 3)
      {
        if (i != 6)
        {
          if (i != 8) {
            return 0;
          }
          return 270;
        }
        return 90;
      }
      return 180;
    }
    catch (IOException paramString)
    {
      Log.e(TAG, "cannot read exif", paramString);
    }
    return -1;
  }
  
  public static ExifInterface newInstance(String paramString)
    throws IOException
  {
    if (paramString != null) {
      return new ExifInterface(paramString);
    }
    throw new NullPointerException("filename should not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/ExifInterfaceCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */