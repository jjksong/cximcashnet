package com.zhihu.matisse.internal.utils;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore.Audio.Media;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Video.Media;

public class PathUtils
{
  public static String getDataColumn(Context paramContext, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    try
    {
      paramUri = paramContext.getContentResolver().query(paramUri, new String[] { "_data" }, paramString, paramArrayOfString, null);
      if (paramUri != null) {
        try
        {
          if (paramUri.moveToFirst())
          {
            paramContext = paramUri.getString(paramUri.getColumnIndexOrThrow("_data"));
            if (paramUri != null) {
              paramUri.close();
            }
            return paramContext;
          }
        }
        finally
        {
          break label80;
        }
      }
      if (paramUri != null) {
        paramUri.close();
      }
      return null;
    }
    finally
    {
      paramUri = null;
      label80:
      if (paramUri != null) {
        paramUri.close();
      }
    }
  }
  
  @TargetApi(19)
  public static String getPath(Context paramContext, Uri paramUri)
  {
    boolean bool = Platform.hasKitKat();
    Object localObject = null;
    if ((bool) && (DocumentsContract.isDocumentUri(paramContext, paramUri)))
    {
      if (isExternalStorageDocument(paramUri))
      {
        paramUri = DocumentsContract.getDocumentId(paramUri).split(":");
        if ("primary".equalsIgnoreCase(paramUri[0]))
        {
          paramContext = new StringBuilder();
          paramContext.append(Environment.getExternalStorageDirectory());
          paramContext.append("/");
          paramContext.append(paramUri[1]);
          return paramContext.toString();
        }
      }
      else
      {
        if (isDownloadsDocument(paramUri))
        {
          paramUri = DocumentsContract.getDocumentId(paramUri);
          return getDataColumn(paramContext, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(paramUri).longValue()), null, null);
        }
        if (isMediaDocument(paramUri))
        {
          String[] arrayOfString = DocumentsContract.getDocumentId(paramUri).split(":");
          String str = arrayOfString[0];
          if ("image".equals(str))
          {
            paramUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
          }
          else if ("video".equals(str))
          {
            paramUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
          }
          else
          {
            paramUri = (Uri)localObject;
            if ("audio".equals(str)) {
              paramUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
          }
          return getDataColumn(paramContext, paramUri, "_id=?", new String[] { arrayOfString[1] });
        }
      }
    }
    else
    {
      if ("content".equalsIgnoreCase(paramUri.getScheme())) {
        return getDataColumn(paramContext, paramUri, null, null);
      }
      if ("file".equalsIgnoreCase(paramUri.getScheme())) {
        return paramUri.getPath();
      }
    }
    return null;
  }
  
  public static boolean isDownloadsDocument(Uri paramUri)
  {
    return "com.android.providers.downloads.documents".equals(paramUri.getAuthority());
  }
  
  public static boolean isExternalStorageDocument(Uri paramUri)
  {
    return "com.android.externalstorage.documents".equals(paramUri.getAuthority());
  }
  
  public static boolean isMediaDocument(Uri paramUri)
  {
    return "com.android.providers.media.documents".equals(paramUri.getAuthority());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/PathUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */