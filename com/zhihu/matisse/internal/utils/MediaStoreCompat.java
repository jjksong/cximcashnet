package com.zhihu.matisse.internal.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.os.EnvironmentCompat;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class MediaStoreCompat
{
  private CaptureStrategy mCaptureStrategy;
  private final WeakReference<Activity> mContext;
  private String mCurrentPhotoPath;
  private Uri mCurrentPhotoUri;
  private final WeakReference<Fragment> mFragment;
  
  public MediaStoreCompat(Activity paramActivity)
  {
    this.mContext = new WeakReference(paramActivity);
    this.mFragment = null;
  }
  
  public MediaStoreCompat(Activity paramActivity, Fragment paramFragment)
  {
    this.mContext = new WeakReference(paramActivity);
    this.mFragment = new WeakReference(paramFragment);
  }
  
  private File createImageFile()
    throws IOException
  {
    String str = String.format("JPEG_%s.jpg", new Object[] { new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date()) });
    if (this.mCaptureStrategy.isPublic)
    {
      localObject2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
      localObject1 = localObject2;
      if (!((File)localObject2).exists())
      {
        ((File)localObject2).mkdirs();
        localObject1 = localObject2;
      }
    }
    else
    {
      localObject1 = ((Activity)this.mContext.get()).getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }
    Object localObject2 = localObject1;
    if (this.mCaptureStrategy.directory != null) {
      localObject2 = new File((File)localObject1, this.mCaptureStrategy.directory);
    }
    Object localObject1 = new File((File)localObject2, str);
    if (!"mounted".equals(EnvironmentCompat.getStorageState((File)localObject1))) {
      return null;
    }
    return (File)localObject1;
  }
  
  public static boolean hasCameraFeature(Context paramContext)
  {
    return paramContext.getApplicationContext().getPackageManager().hasSystemFeature("android.hardware.camera");
  }
  
  public void dispatchCaptureIntent(Context paramContext, int paramInt)
  {
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    if (localIntent.resolveActivity(paramContext.getPackageManager()) != null)
    {
      Object localObject = null;
      try
      {
        File localFile = createImageFile();
        localObject = localFile;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
      }
      if (localObject != null)
      {
        this.mCurrentPhotoPath = ((File)localObject).getAbsolutePath();
        this.mCurrentPhotoUri = FileProvider.getUriForFile((Context)this.mContext.get(), this.mCaptureStrategy.authority, (File)localObject);
        localIntent.putExtra("output", this.mCurrentPhotoUri);
        localIntent.addFlags(2);
        if (Build.VERSION.SDK_INT < 21)
        {
          localObject = paramContext.getPackageManager().queryIntentActivities(localIntent, 65536).iterator();
          while (((Iterator)localObject).hasNext()) {
            paramContext.grantUriPermission(((ResolveInfo)((Iterator)localObject).next()).activityInfo.packageName, this.mCurrentPhotoUri, 3);
          }
        }
        paramContext = this.mFragment;
        if (paramContext != null) {
          ((Fragment)paramContext.get()).startActivityForResult(localIntent, paramInt);
        } else {
          ((Activity)this.mContext.get()).startActivityForResult(localIntent, paramInt);
        }
      }
    }
  }
  
  public String getCurrentPhotoPath()
  {
    return this.mCurrentPhotoPath;
  }
  
  public Uri getCurrentPhotoUri()
  {
    return this.mCurrentPhotoUri;
  }
  
  public void setCaptureStrategy(CaptureStrategy paramCaptureStrategy)
  {
    this.mCaptureStrategy = paramCaptureStrategy;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/MediaStoreCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */