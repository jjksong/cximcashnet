package com.zhihu.matisse.internal.utils;

import android.os.Build.VERSION;

public class Platform
{
  public static boolean hasICS()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 14) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public static boolean hasKitKat()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 19) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/Platform.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */