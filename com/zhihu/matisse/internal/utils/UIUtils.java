package com.zhihu.matisse.internal.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class UIUtils
{
  public static int spanCount(Context paramContext, int paramInt)
  {
    int i = Math.round(paramContext.getResources().getDisplayMetrics().widthPixels / paramInt);
    paramInt = i;
    if (i == 0) {
      paramInt = 1;
    }
    return paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/UIUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */