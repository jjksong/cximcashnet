package com.zhihu.matisse.internal.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.R.string;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class PhotoMetadataUtils
{
  private static final int MAX_WIDTH = 1600;
  private static final String SCHEME_CONTENT = "content";
  private static final String TAG = "PhotoMetadataUtils";
  
  private PhotoMetadataUtils()
  {
    throw new AssertionError("oops! the utility class is about to be instantiated...");
  }
  
  /* Error */
  public static Point getBitmapBound(ContentResolver paramContentResolver, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: aload 4
    //   7: astore_2
    //   8: new 35	android/graphics/BitmapFactory$Options
    //   11: astore 5
    //   13: aload 4
    //   15: astore_2
    //   16: aload 5
    //   18: invokespecial 36	android/graphics/BitmapFactory$Options:<init>	()V
    //   21: aload 4
    //   23: astore_2
    //   24: aload 5
    //   26: iconst_1
    //   27: putfield 40	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   30: aload 4
    //   32: astore_2
    //   33: aload_0
    //   34: aload_1
    //   35: invokevirtual 46	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   38: astore_0
    //   39: aload_0
    //   40: aconst_null
    //   41: aload 5
    //   43: invokestatic 52	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   46: pop
    //   47: new 54	android/graphics/Point
    //   50: dup
    //   51: aload 5
    //   53: getfield 57	android/graphics/BitmapFactory$Options:outWidth	I
    //   56: aload 5
    //   58: getfield 60	android/graphics/BitmapFactory$Options:outHeight	I
    //   61: invokespecial 63	android/graphics/Point:<init>	(II)V
    //   64: astore_1
    //   65: aload_0
    //   66: ifnull +15 -> 81
    //   69: aload_0
    //   70: invokevirtual 68	java/io/InputStream:close	()V
    //   73: goto +8 -> 81
    //   76: astore_0
    //   77: aload_0
    //   78: invokevirtual 71	java/io/IOException:printStackTrace	()V
    //   81: aload_1
    //   82: areturn
    //   83: astore_1
    //   84: aload_0
    //   85: astore_2
    //   86: goto +41 -> 127
    //   89: astore_1
    //   90: goto +7 -> 97
    //   93: astore_1
    //   94: goto +33 -> 127
    //   97: aload_0
    //   98: astore_2
    //   99: new 54	android/graphics/Point
    //   102: dup
    //   103: iconst_0
    //   104: iconst_0
    //   105: invokespecial 63	android/graphics/Point:<init>	(II)V
    //   108: astore_1
    //   109: aload_0
    //   110: ifnull +15 -> 125
    //   113: aload_0
    //   114: invokevirtual 68	java/io/InputStream:close	()V
    //   117: goto +8 -> 125
    //   120: astore_0
    //   121: aload_0
    //   122: invokevirtual 71	java/io/IOException:printStackTrace	()V
    //   125: aload_1
    //   126: areturn
    //   127: aload_2
    //   128: ifnull +15 -> 143
    //   131: aload_2
    //   132: invokevirtual 68	java/io/InputStream:close	()V
    //   135: goto +8 -> 143
    //   138: astore_0
    //   139: aload_0
    //   140: invokevirtual 71	java/io/IOException:printStackTrace	()V
    //   143: aload_1
    //   144: athrow
    //   145: astore_0
    //   146: aload_3
    //   147: astore_0
    //   148: goto -51 -> 97
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	paramContentResolver	ContentResolver
    //   0	151	1	paramUri	Uri
    //   7	125	2	localObject1	Object
    //   1	146	3	localObject2	Object
    //   3	28	4	localObject3	Object
    //   11	46	5	localOptions	android.graphics.BitmapFactory.Options
    // Exception table:
    //   from	to	target	type
    //   69	73	76	java/io/IOException
    //   39	65	83	finally
    //   39	65	89	java/io/FileNotFoundException
    //   8	13	93	finally
    //   16	21	93	finally
    //   24	30	93	finally
    //   33	39	93	finally
    //   99	109	93	finally
    //   113	117	120	java/io/IOException
    //   131	135	138	java/io/IOException
    //   8	13	145	java/io/FileNotFoundException
    //   16	21	145	java/io/FileNotFoundException
    //   24	30	145	java/io/FileNotFoundException
    //   33	39	145	java/io/FileNotFoundException
  }
  
  public static Point getBitmapSize(Uri paramUri, Activity paramActivity)
  {
    ContentResolver localContentResolver = paramActivity.getContentResolver();
    Point localPoint = getBitmapBound(localContentResolver, paramUri);
    int j = localPoint.x;
    int i = localPoint.y;
    if (shouldRotate(localContentResolver, paramUri))
    {
      j = localPoint.y;
      i = localPoint.x;
    }
    if (i == 0) {
      return new Point(1600, 1600);
    }
    paramUri = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(paramUri);
    float f2 = paramUri.widthPixels;
    float f4 = paramUri.heightPixels;
    float f1 = j;
    f2 /= f1;
    float f3 = i;
    f4 /= f3;
    if (f2 > f4) {
      return new Point((int)(f1 * f2), (int)(f3 * f4));
    }
    return new Point((int)(f1 * f2), (int)(f3 * f4));
  }
  
  public static String getPath(ContentResolver paramContentResolver, Uri paramUri)
  {
    if (paramUri == null) {
      return null;
    }
    if ("content".equals(paramUri.getScheme())) {
      try
      {
        paramUri = paramContentResolver.query(paramUri, new String[] { "_data" }, null, null, null);
        if (paramUri != null) {
          try
          {
            if (paramUri.moveToFirst())
            {
              paramContentResolver = paramUri.getString(paramUri.getColumnIndex("_data"));
              if (paramUri != null) {
                paramUri.close();
              }
              return paramContentResolver;
            }
          }
          finally
          {
            break label98;
          }
        }
        if (paramUri != null) {
          paramUri.close();
        }
        return null;
      }
      finally
      {
        paramUri = null;
        label98:
        if (paramUri != null) {
          paramUri.close();
        }
      }
    }
    return paramUri.getPath();
  }
  
  public static int getPixelsCount(ContentResolver paramContentResolver, Uri paramUri)
  {
    paramContentResolver = getBitmapBound(paramContentResolver, paramUri);
    return paramContentResolver.x * paramContentResolver.y;
  }
  
  public static float getSizeInMB(long paramLong)
  {
    Object localObject = (DecimalFormat)NumberFormat.getNumberInstance(Locale.US);
    ((DecimalFormat)localObject).applyPattern("0.0");
    String str2 = ((DecimalFormat)localObject).format((float)paramLong / 1024.0F / 1024.0F);
    String str1 = TAG;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("getSizeInMB: ");
    ((StringBuilder)localObject).append(str2);
    Log.e(str1, ((StringBuilder)localObject).toString());
    return Float.valueOf(str2.replaceAll(",", ".")).floatValue();
  }
  
  public static IncapableCause isAcceptable(Context paramContext, Item paramItem)
  {
    if (!isSelectableType(paramContext, paramItem)) {
      return new IncapableCause(paramContext.getString(R.string.error_file_type));
    }
    if (SelectionSpec.getInstance().filters != null)
    {
      Iterator localIterator = SelectionSpec.getInstance().filters.iterator();
      while (localIterator.hasNext())
      {
        IncapableCause localIncapableCause = ((Filter)localIterator.next()).filter(paramContext, paramItem);
        if (localIncapableCause != null) {
          return localIncapableCause;
        }
      }
    }
    return null;
  }
  
  private static boolean isSelectableType(Context paramContext, Item paramItem)
  {
    if (paramContext == null) {
      return false;
    }
    paramContext = paramContext.getContentResolver();
    Iterator localIterator = SelectionSpec.getInstance().mimeTypeSet.iterator();
    while (localIterator.hasNext()) {
      if (((MimeType)localIterator.next()).checkType(paramContext, paramItem.getContentUri())) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean shouldRotate(ContentResolver paramContentResolver, Uri paramUri)
  {
    boolean bool = false;
    try
    {
      paramContentResolver = ExifInterfaceCompat.newInstance(getPath(paramContentResolver, paramUri));
      int i = paramContentResolver.getAttributeInt("Orientation", -1);
      if ((i == 6) || (i == 8)) {
        bool = true;
      }
      return bool;
    }
    catch (IOException paramContentResolver)
    {
      String str = TAG;
      paramContentResolver = new StringBuilder();
      paramContentResolver.append("could not read exif info of the image: ");
      paramContentResolver.append(paramUri);
      Log.e(str, paramContentResolver.toString());
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/utils/PhotoMetadataUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */