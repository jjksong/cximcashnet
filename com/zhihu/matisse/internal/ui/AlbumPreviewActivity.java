package com.zhihu.matisse.internal.ui;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.model.AlbumMediaCollection;
import com.zhihu.matisse.internal.model.AlbumMediaCollection.AlbumMediaCallbacks;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.ui.adapter.PreviewPagerAdapter;
import com.zhihu.matisse.internal.ui.widget.CheckView;
import java.util.ArrayList;
import java.util.List;

public class AlbumPreviewActivity
  extends BasePreviewActivity
  implements AlbumMediaCollection.AlbumMediaCallbacks
{
  public static final String EXTRA_ALBUM = "extra_album";
  public static final String EXTRA_ITEM = "extra_item";
  private AlbumMediaCollection mCollection = new AlbumMediaCollection();
  private boolean mIsAlreadySetPosition;
  
  public void onAlbumMediaLoad(Cursor paramCursor)
  {
    ArrayList localArrayList = new ArrayList();
    while (paramCursor.moveToNext()) {
      localArrayList.add(Item.valueOf(paramCursor));
    }
    if (localArrayList.isEmpty()) {
      return;
    }
    paramCursor = (PreviewPagerAdapter)this.mPager.getAdapter();
    paramCursor.addAll(localArrayList);
    paramCursor.notifyDataSetChanged();
    if (!this.mIsAlreadySetPosition)
    {
      this.mIsAlreadySetPosition = true;
      int i = localArrayList.indexOf((Item)getIntent().getParcelableExtra("extra_item"));
      this.mPager.setCurrentItem(i, false);
      this.mPreviousPos = i;
    }
  }
  
  public void onAlbumMediaReset() {}
  
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (!SelectionSpec.getInstance().hasInited)
    {
      setResult(0);
      finish();
      return;
    }
    this.mCollection.onCreate(this, this);
    paramBundle = (Album)getIntent().getParcelableExtra("extra_album");
    this.mCollection.load(paramBundle);
    paramBundle = (Item)getIntent().getParcelableExtra("extra_item");
    if (this.mSpec.countable) {
      this.mCheckView.setCheckedNum(this.mSelectedCollection.checkedNumOf(paramBundle));
    } else {
      this.mCheckView.setChecked(this.mSelectedCollection.isSelected(paramBundle));
    }
    updateSize(paramBundle);
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mCollection.onDestroy();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/AlbumPreviewActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */