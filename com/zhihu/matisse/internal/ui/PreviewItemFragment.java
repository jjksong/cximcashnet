package com.zhihu.matisse.internal.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.R.string;
import com.zhihu.matisse.engine.ImageEngine;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;
import com.zhihu.matisse.listener.OnFragmentInteractionListener;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouch.OnImageViewTouchSingleTapListener;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

public class PreviewItemFragment
  extends Fragment
{
  private static final String ARGS_ITEM = "args_item";
  private OnFragmentInteractionListener mListener;
  
  public static PreviewItemFragment newInstance(Item paramItem)
  {
    PreviewItemFragment localPreviewItemFragment = new PreviewItemFragment();
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("args_item", paramItem);
    localPreviewItemFragment.setArguments(localBundle);
    return localPreviewItemFragment;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnFragmentInteractionListener))
    {
      this.mListener = ((OnFragmentInteractionListener)paramContext);
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramContext.toString());
    localStringBuilder.append(" must implement OnFragmentInteractionListener");
    throw new RuntimeException(localStringBuilder.toString());
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(R.layout.fragment_preview_item, paramViewGroup, false);
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onViewCreated(View paramView, @Nullable final Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (Item)getArguments().getParcelable("args_item");
    if (paramBundle == null) {
      return;
    }
    Object localObject = paramView.findViewById(R.id.video_play_button);
    if (paramBundle.isVideo())
    {
      ((View)localObject).setVisibility(0);
      ((View)localObject).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          paramAnonymousView = new Intent("android.intent.action.VIEW");
          paramAnonymousView.setDataAndType(paramBundle.uri, "video/*");
          try
          {
            PreviewItemFragment.this.startActivity(paramAnonymousView);
          }
          catch (ActivityNotFoundException paramAnonymousView)
          {
            Toast.makeText(PreviewItemFragment.this.getContext(), R.string.error_no_video_activity, 0).show();
          }
        }
      });
    }
    else
    {
      ((View)localObject).setVisibility(8);
    }
    localObject = (ImageViewTouch)paramView.findViewById(R.id.image_view);
    ((ImageViewTouch)localObject).setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
    ((ImageViewTouch)localObject).setSingleTapListener(new ImageViewTouch.OnImageViewTouchSingleTapListener()
    {
      public void onSingleTapConfirmed()
      {
        if (PreviewItemFragment.this.mListener != null) {
          PreviewItemFragment.this.mListener.onClick();
        }
      }
    });
    paramView = PhotoMetadataUtils.getBitmapSize(paramBundle.getContentUri(), getActivity());
    if (paramBundle.isGif()) {
      SelectionSpec.getInstance().imageEngine.loadGifImage(getContext(), paramView.x, paramView.y, (ImageView)localObject, paramBundle.getContentUri());
    } else {
      SelectionSpec.getInstance().imageEngine.loadImage(getContext(), paramView.x, paramView.y, (ImageView)localObject, paramBundle.getContentUri());
    }
  }
  
  public void resetView()
  {
    if (getView() != null) {
      ((ImageViewTouch)getView().findViewById(R.id.image_view)).resetMatrix();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/PreviewItemFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */