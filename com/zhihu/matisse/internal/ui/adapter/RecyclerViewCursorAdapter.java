package com.zhihu.matisse.internal.ui.adapter;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;

public abstract class RecyclerViewCursorAdapter<VH extends RecyclerView.ViewHolder>
  extends RecyclerView.Adapter<VH>
{
  private Cursor mCursor;
  private int mRowIDColumn;
  
  RecyclerViewCursorAdapter(Cursor paramCursor)
  {
    setHasStableIds(true);
    swapCursor(paramCursor);
  }
  
  private boolean isDataValid(Cursor paramCursor)
  {
    boolean bool;
    if ((paramCursor != null) && (!paramCursor.isClosed())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public Cursor getCursor()
  {
    return this.mCursor;
  }
  
  public int getItemCount()
  {
    if (isDataValid(this.mCursor)) {
      return this.mCursor.getCount();
    }
    return 0;
  }
  
  public long getItemId(int paramInt)
  {
    if (isDataValid(this.mCursor))
    {
      if (this.mCursor.moveToPosition(paramInt)) {
        return this.mCursor.getLong(this.mRowIDColumn);
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Could not move cursor to position ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" when trying to get an item id");
      throw new IllegalStateException(localStringBuilder.toString());
    }
    throw new IllegalStateException("Cannot lookup item id when cursor is in invalid state.");
  }
  
  public int getItemViewType(int paramInt)
  {
    if (this.mCursor.moveToPosition(paramInt)) {
      return getItemViewType(paramInt, this.mCursor);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Could not move cursor to position ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" when trying to get item view type.");
    throw new IllegalStateException(localStringBuilder.toString());
  }
  
  protected abstract int getItemViewType(int paramInt, Cursor paramCursor);
  
  public void onBindViewHolder(VH paramVH, int paramInt)
  {
    if (isDataValid(this.mCursor))
    {
      if (this.mCursor.moveToPosition(paramInt))
      {
        onBindViewHolder(paramVH, this.mCursor);
        return;
      }
      paramVH = new StringBuilder();
      paramVH.append("Could not move cursor to position ");
      paramVH.append(paramInt);
      paramVH.append(" when trying to bind view holder");
      throw new IllegalStateException(paramVH.toString());
    }
    throw new IllegalStateException("Cannot bind view holder when cursor is in invalid state.");
  }
  
  protected abstract void onBindViewHolder(VH paramVH, Cursor paramCursor);
  
  public void swapCursor(Cursor paramCursor)
  {
    if (paramCursor == this.mCursor) {
      return;
    }
    if (paramCursor != null)
    {
      this.mCursor = paramCursor;
      this.mRowIDColumn = this.mCursor.getColumnIndexOrThrow("_id");
      notifyDataSetChanged();
    }
    else
    {
      notifyItemRangeRemoved(0, getItemCount());
      this.mCursor = null;
      this.mRowIDColumn = -1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/adapter/RecyclerViewCursorAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */