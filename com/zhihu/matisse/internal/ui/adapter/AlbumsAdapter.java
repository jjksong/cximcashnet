package com.zhihu.matisse.internal.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zhihu.matisse.R.attr;
import com.zhihu.matisse.R.dimen;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.engine.ImageEngine;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import java.io.File;

public class AlbumsAdapter
  extends CursorAdapter
{
  private final Drawable mPlaceholder;
  
  public AlbumsAdapter(Context paramContext, Cursor paramCursor, int paramInt)
  {
    super(paramContext, paramCursor, paramInt);
    paramContext = paramContext.getTheme().obtainStyledAttributes(new int[] { R.attr.album_thumbnail_placeholder });
    this.mPlaceholder = paramContext.getDrawable(0);
    paramContext.recycle();
  }
  
  public AlbumsAdapter(Context paramContext, Cursor paramCursor, boolean paramBoolean)
  {
    super(paramContext, paramCursor, paramBoolean);
    paramContext = paramContext.getTheme().obtainStyledAttributes(new int[] { R.attr.album_thumbnail_placeholder });
    this.mPlaceholder = paramContext.getDrawable(0);
    paramContext.recycle();
  }
  
  public void bindView(View paramView, Context paramContext, Cursor paramCursor)
  {
    paramCursor = Album.valueOf(paramCursor);
    ((TextView)paramView.findViewById(R.id.album_name)).setText(paramCursor.getDisplayName(paramContext));
    ((TextView)paramView.findViewById(R.id.album_media_count)).setText(String.valueOf(paramCursor.getCount()));
    SelectionSpec.getInstance().imageEngine.loadThumbnail(paramContext, paramContext.getResources().getDimensionPixelSize(R.dimen.media_grid_size), this.mPlaceholder, (ImageView)paramView.findViewById(R.id.album_cover), Uri.fromFile(new File(paramCursor.getCoverPath())));
  }
  
  public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
  {
    return LayoutInflater.from(paramContext).inflate(R.layout.album_list_item, paramViewGroup, false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/adapter/AlbumsAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */