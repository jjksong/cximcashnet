package com.zhihu.matisse.internal.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.zhihu.matisse.R.attr;
import com.zhihu.matisse.R.dimen;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.ui.widget.CheckView;
import com.zhihu.matisse.internal.ui.widget.MediaGrid;
import com.zhihu.matisse.internal.ui.widget.MediaGrid.OnMediaGridClickListener;
import com.zhihu.matisse.internal.ui.widget.MediaGrid.PreBindInfo;

public class AlbumMediaAdapter
  extends RecyclerViewCursorAdapter<RecyclerView.ViewHolder>
  implements MediaGrid.OnMediaGridClickListener
{
  private static final int VIEW_TYPE_CAPTURE = 1;
  private static final int VIEW_TYPE_MEDIA = 2;
  private CheckStateListener mCheckStateListener;
  private int mImageResize;
  private OnMediaClickListener mOnMediaClickListener;
  private final Drawable mPlaceholder;
  private RecyclerView mRecyclerView;
  private final SelectedItemCollection mSelectedCollection;
  private SelectionSpec mSelectionSpec = SelectionSpec.getInstance();
  
  public AlbumMediaAdapter(Context paramContext, SelectedItemCollection paramSelectedItemCollection, RecyclerView paramRecyclerView)
  {
    super(null);
    this.mSelectedCollection = paramSelectedItemCollection;
    paramContext = paramContext.getTheme().obtainStyledAttributes(new int[] { R.attr.item_placeholder });
    this.mPlaceholder = paramContext.getDrawable(0);
    paramContext.recycle();
    this.mRecyclerView = paramRecyclerView;
  }
  
  private boolean assertAddSelection(Context paramContext, Item paramItem)
  {
    paramItem = this.mSelectedCollection.isAcceptable(paramItem);
    IncapableCause.handleCause(paramContext, paramItem);
    boolean bool;
    if (paramItem == null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private int getImageResize(Context paramContext)
  {
    if (this.mImageResize == 0)
    {
      int i = ((GridLayoutManager)this.mRecyclerView.getLayoutManager()).getSpanCount();
      this.mImageResize = ((paramContext.getResources().getDisplayMetrics().widthPixels - paramContext.getResources().getDimensionPixelSize(R.dimen.media_grid_spacing) * (i - 1)) / i);
      this.mImageResize = ((int)(this.mImageResize * this.mSelectionSpec.thumbnailScale));
    }
    return this.mImageResize;
  }
  
  private void notifyCheckStateChanged()
  {
    notifyDataSetChanged();
    CheckStateListener localCheckStateListener = this.mCheckStateListener;
    if (localCheckStateListener != null) {
      localCheckStateListener.onUpdate();
    }
  }
  
  private void setCheckStatus(Item paramItem, MediaGrid paramMediaGrid)
  {
    if (this.mSelectionSpec.countable)
    {
      int i = this.mSelectedCollection.checkedNumOf(paramItem);
      if (i > 0)
      {
        paramMediaGrid.setCheckEnabled(true);
        paramMediaGrid.setCheckedNum(i);
      }
      else if (this.mSelectedCollection.maxSelectableReached())
      {
        paramMediaGrid.setCheckEnabled(false);
        paramMediaGrid.setCheckedNum(Integer.MIN_VALUE);
      }
      else
      {
        paramMediaGrid.setCheckEnabled(true);
        paramMediaGrid.setCheckedNum(i);
      }
    }
    else if (this.mSelectedCollection.isSelected(paramItem))
    {
      paramMediaGrid.setCheckEnabled(true);
      paramMediaGrid.setChecked(true);
    }
    else if (this.mSelectedCollection.maxSelectableReached())
    {
      paramMediaGrid.setCheckEnabled(false);
      paramMediaGrid.setChecked(false);
    }
    else
    {
      paramMediaGrid.setCheckEnabled(true);
      paramMediaGrid.setChecked(false);
    }
  }
  
  public int getItemViewType(int paramInt, Cursor paramCursor)
  {
    if (Item.valueOf(paramCursor).isCapture()) {
      paramInt = 1;
    } else {
      paramInt = 2;
    }
    return paramInt;
  }
  
  protected void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, Cursor paramCursor)
  {
    Object localObject1;
    if ((paramViewHolder instanceof CaptureViewHolder))
    {
      localObject1 = (CaptureViewHolder)paramViewHolder;
      paramCursor = ((CaptureViewHolder)localObject1).mHint.getCompoundDrawables();
      paramViewHolder = paramViewHolder.itemView.getContext().getTheme().obtainStyledAttributes(new int[] { R.attr.capture_textColor });
      int j = paramViewHolder.getColor(0, 0);
      paramViewHolder.recycle();
      for (int i = 0; i < paramCursor.length; i++)
      {
        paramViewHolder = paramCursor[i];
        if (paramViewHolder != null)
        {
          Object localObject2 = paramViewHolder.getConstantState();
          if (localObject2 != null)
          {
            localObject2 = ((Drawable.ConstantState)localObject2).newDrawable().mutate();
            ((Drawable)localObject2).setColorFilter(j, PorterDuff.Mode.SRC_IN);
            ((Drawable)localObject2).setBounds(paramViewHolder.getBounds());
            paramCursor[i] = localObject2;
          }
        }
      }
      ((CaptureViewHolder)localObject1).mHint.setCompoundDrawables(paramCursor[0], paramCursor[1], paramCursor[2], paramCursor[3]);
    }
    else if ((paramViewHolder instanceof MediaViewHolder))
    {
      localObject1 = (MediaViewHolder)paramViewHolder;
      paramCursor = Item.valueOf(paramCursor);
      ((MediaViewHolder)localObject1).mMediaGrid.preBindMedia(new MediaGrid.PreBindInfo(getImageResize(((MediaViewHolder)localObject1).mMediaGrid.getContext()), this.mPlaceholder, this.mSelectionSpec.countable, paramViewHolder));
      ((MediaViewHolder)localObject1).mMediaGrid.bindMedia(paramCursor);
      ((MediaViewHolder)localObject1).mMediaGrid.setOnMediaGridClickListener(this);
      setCheckStatus(paramCursor, ((MediaViewHolder)localObject1).mMediaGrid);
    }
  }
  
  public void onCheckViewClicked(CheckView paramCheckView, Item paramItem, RecyclerView.ViewHolder paramViewHolder)
  {
    if (this.mSelectionSpec.countable)
    {
      if (this.mSelectedCollection.checkedNumOf(paramItem) == Integer.MIN_VALUE)
      {
        if (assertAddSelection(paramViewHolder.itemView.getContext(), paramItem))
        {
          this.mSelectedCollection.add(paramItem);
          notifyCheckStateChanged();
        }
      }
      else
      {
        this.mSelectedCollection.remove(paramItem);
        notifyCheckStateChanged();
      }
    }
    else if (this.mSelectedCollection.isSelected(paramItem))
    {
      this.mSelectedCollection.remove(paramItem);
      notifyCheckStateChanged();
    }
    else if (assertAddSelection(paramViewHolder.itemView.getContext(), paramItem))
    {
      this.mSelectedCollection.add(paramItem);
      notifyCheckStateChanged();
    }
  }
  
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    if (paramInt == 1)
    {
      paramViewGroup = new CaptureViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.photo_capture_item, paramViewGroup, false));
      paramViewGroup.itemView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if ((paramAnonymousView.getContext() instanceof AlbumMediaAdapter.OnPhotoCapture)) {
            ((AlbumMediaAdapter.OnPhotoCapture)paramAnonymousView.getContext()).capture();
          }
        }
      });
      return paramViewGroup;
    }
    if (paramInt == 2) {
      return new MediaViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.media_grid_item, paramViewGroup, false));
    }
    return null;
  }
  
  public void onThumbnailClicked(ImageView paramImageView, Item paramItem, RecyclerView.ViewHolder paramViewHolder)
  {
    paramImageView = this.mOnMediaClickListener;
    if (paramImageView != null) {
      paramImageView.onMediaClick(null, paramItem, paramViewHolder.getAdapterPosition());
    }
  }
  
  public void refreshSelection()
  {
    Object localObject = (GridLayoutManager)this.mRecyclerView.getLayoutManager();
    int j = ((GridLayoutManager)localObject).findFirstVisibleItemPosition();
    int k = ((GridLayoutManager)localObject).findLastVisibleItemPosition();
    if ((j != -1) && (k != -1))
    {
      Cursor localCursor = getCursor();
      for (int i = j; i <= k; i++)
      {
        localObject = this.mRecyclerView.findViewHolderForAdapterPosition(j);
        if (((localObject instanceof MediaViewHolder)) && (localCursor.moveToPosition(i))) {
          setCheckStatus(Item.valueOf(localCursor), ((MediaViewHolder)localObject).mMediaGrid);
        }
      }
      return;
    }
  }
  
  public void registerCheckStateListener(CheckStateListener paramCheckStateListener)
  {
    this.mCheckStateListener = paramCheckStateListener;
  }
  
  public void registerOnMediaClickListener(OnMediaClickListener paramOnMediaClickListener)
  {
    this.mOnMediaClickListener = paramOnMediaClickListener;
  }
  
  public void unregisterCheckStateListener()
  {
    this.mCheckStateListener = null;
  }
  
  public void unregisterOnMediaClickListener()
  {
    this.mOnMediaClickListener = null;
  }
  
  private static class CaptureViewHolder
    extends RecyclerView.ViewHolder
  {
    private TextView mHint;
    
    CaptureViewHolder(View paramView)
    {
      super();
      this.mHint = ((TextView)paramView.findViewById(R.id.hint));
    }
  }
  
  public static abstract interface CheckStateListener
  {
    public abstract void onUpdate();
  }
  
  private static class MediaViewHolder
    extends RecyclerView.ViewHolder
  {
    private MediaGrid mMediaGrid;
    
    MediaViewHolder(View paramView)
    {
      super();
      this.mMediaGrid = ((MediaGrid)paramView);
    }
  }
  
  public static abstract interface OnMediaClickListener
  {
    public abstract void onMediaClick(Album paramAlbum, Item paramItem, int paramInt);
  }
  
  public static abstract interface OnPhotoCapture
  {
    public abstract void capture();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/adapter/AlbumMediaAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */