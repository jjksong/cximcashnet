package com.zhihu.matisse.internal.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.ui.PreviewItemFragment;
import java.util.ArrayList;
import java.util.List;

public class PreviewPagerAdapter
  extends FragmentPagerAdapter
{
  private ArrayList<Item> mItems = new ArrayList();
  private OnPrimaryItemSetListener mListener;
  
  public PreviewPagerAdapter(FragmentManager paramFragmentManager, OnPrimaryItemSetListener paramOnPrimaryItemSetListener)
  {
    super(paramFragmentManager);
    this.mListener = paramOnPrimaryItemSetListener;
  }
  
  public void addAll(List<Item> paramList)
  {
    this.mItems.addAll(paramList);
  }
  
  public int getCount()
  {
    return this.mItems.size();
  }
  
  public Fragment getItem(int paramInt)
  {
    return PreviewItemFragment.newInstance((Item)this.mItems.get(paramInt));
  }
  
  public Item getMediaItem(int paramInt)
  {
    return (Item)this.mItems.get(paramInt);
  }
  
  public void setPrimaryItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    super.setPrimaryItem(paramViewGroup, paramInt, paramObject);
    paramViewGroup = this.mListener;
    if (paramViewGroup != null) {
      paramViewGroup.onPrimaryItemSet(paramInt);
    }
  }
  
  static abstract interface OnPrimaryItemSetListener
  {
    public abstract void onPrimaryItemSet(int paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/adapter/PreviewPagerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */