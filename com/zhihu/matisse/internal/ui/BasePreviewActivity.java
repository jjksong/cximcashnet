package com.zhihu.matisse.internal.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.R.string;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.ui.adapter.PreviewPagerAdapter;
import com.zhihu.matisse.internal.ui.widget.CheckRadioView;
import com.zhihu.matisse.internal.ui.widget.CheckView;
import com.zhihu.matisse.internal.ui.widget.IncapableDialog;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;
import com.zhihu.matisse.internal.utils.Platform;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnFragmentInteractionListener;
import com.zhihu.matisse.listener.OnSelectedListener;
import java.util.List;

public abstract class BasePreviewActivity
  extends AppCompatActivity
  implements View.OnClickListener, ViewPager.OnPageChangeListener, OnFragmentInteractionListener
{
  public static final String CHECK_STATE = "checkState";
  public static final String EXTRA_DEFAULT_BUNDLE = "extra_default_bundle";
  public static final String EXTRA_RESULT_APPLY = "extra_result_apply";
  public static final String EXTRA_RESULT_BUNDLE = "extra_result_bundle";
  public static final String EXTRA_RESULT_ORIGINAL_ENABLE = "extra_result_original_enable";
  protected PreviewPagerAdapter mAdapter;
  private FrameLayout mBottomToolbar;
  protected TextView mButtonApply;
  protected TextView mButtonBack;
  protected CheckView mCheckView;
  private boolean mIsToolbarHide = false;
  private CheckRadioView mOriginal;
  protected boolean mOriginalEnable;
  private LinearLayout mOriginalLayout;
  protected ViewPager mPager;
  protected int mPreviousPos = -1;
  protected final SelectedItemCollection mSelectedCollection = new SelectedItemCollection(this);
  protected TextView mSize;
  protected SelectionSpec mSpec;
  private FrameLayout mTopToolbar;
  
  private boolean assertAddSelection(Item paramItem)
  {
    paramItem = this.mSelectedCollection.isAcceptable(paramItem);
    IncapableCause.handleCause(this, paramItem);
    boolean bool;
    if (paramItem == null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private int countOverMaxSize()
  {
    int m = this.mSelectedCollection.count();
    int i = 0;
    int k;
    for (int j = 0; i < m; j = k)
    {
      Item localItem = (Item)this.mSelectedCollection.asList().get(i);
      k = j;
      if (localItem.isImage())
      {
        k = j;
        if (PhotoMetadataUtils.getSizeInMB(localItem.size) > this.mSpec.originalMaxSize) {
          k = j + 1;
        }
      }
      i++;
    }
    return j;
  }
  
  private void updateApplyButton()
  {
    int i = this.mSelectedCollection.count();
    if (i == 0)
    {
      this.mButtonApply.setText(R.string.button_sure_default);
      this.mButtonApply.setEnabled(false);
    }
    else if ((i == 1) && (this.mSpec.singleSelectionModeEnabled()))
    {
      this.mButtonApply.setText(R.string.button_sure_default);
      this.mButtonApply.setEnabled(true);
    }
    else
    {
      this.mButtonApply.setEnabled(true);
      this.mButtonApply.setText(getString(R.string.button_sure, new Object[] { Integer.valueOf(i) }));
    }
    if (this.mSpec.originalable)
    {
      this.mOriginalLayout.setVisibility(0);
      updateOriginalState();
    }
    else
    {
      this.mOriginalLayout.setVisibility(8);
    }
  }
  
  private void updateOriginalState()
  {
    this.mOriginal.setChecked(this.mOriginalEnable);
    if (!this.mOriginalEnable) {
      this.mOriginal.setColor(-1);
    }
    if ((countOverMaxSize() > 0) && (this.mOriginalEnable))
    {
      IncapableDialog.newInstance("", getString(R.string.error_over_original_size, new Object[] { Integer.valueOf(this.mSpec.originalMaxSize) })).show(getSupportFragmentManager(), IncapableDialog.class.getName());
      this.mOriginal.setChecked(false);
      this.mOriginal.setColor(-1);
      this.mOriginalEnable = false;
    }
  }
  
  public void onBackPressed()
  {
    sendBackResult(false);
    super.onBackPressed();
  }
  
  public void onClick()
  {
    if (!this.mSpec.autoHideToobar) {
      return;
    }
    if (this.mIsToolbarHide)
    {
      this.mTopToolbar.animate().setInterpolator(new FastOutSlowInInterpolator()).translationYBy(this.mTopToolbar.getMeasuredHeight()).start();
      this.mBottomToolbar.animate().translationYBy(-this.mBottomToolbar.getMeasuredHeight()).setInterpolator(new FastOutSlowInInterpolator()).start();
    }
    else
    {
      this.mTopToolbar.animate().setInterpolator(new FastOutSlowInInterpolator()).translationYBy(-this.mTopToolbar.getMeasuredHeight()).start();
      this.mBottomToolbar.animate().setInterpolator(new FastOutSlowInInterpolator()).translationYBy(this.mBottomToolbar.getMeasuredHeight()).start();
    }
    this.mIsToolbarHide ^= true;
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == R.id.button_back)
    {
      onBackPressed();
    }
    else if (paramView.getId() == R.id.button_apply)
    {
      sendBackResult(true);
      finish();
    }
  }
  
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    setTheme(SelectionSpec.getInstance().themeId);
    super.onCreate(paramBundle);
    if (!SelectionSpec.getInstance().hasInited)
    {
      setResult(0);
      finish();
      return;
    }
    setContentView(R.layout.activity_media_preview);
    if (Platform.hasKitKat()) {
      getWindow().addFlags(67108864);
    }
    this.mSpec = SelectionSpec.getInstance();
    if (this.mSpec.needOrientationRestriction()) {
      setRequestedOrientation(this.mSpec.orientation);
    }
    if (paramBundle == null)
    {
      this.mSelectedCollection.onCreate(getIntent().getBundleExtra("extra_default_bundle"));
      this.mOriginalEnable = getIntent().getBooleanExtra("extra_result_original_enable", false);
    }
    else
    {
      this.mSelectedCollection.onCreate(paramBundle);
      this.mOriginalEnable = paramBundle.getBoolean("checkState");
    }
    this.mButtonBack = ((TextView)findViewById(R.id.button_back));
    this.mButtonApply = ((TextView)findViewById(R.id.button_apply));
    this.mSize = ((TextView)findViewById(R.id.size));
    this.mButtonBack.setOnClickListener(this);
    this.mButtonApply.setOnClickListener(this);
    this.mPager = ((ViewPager)findViewById(R.id.pager));
    this.mPager.addOnPageChangeListener(this);
    this.mAdapter = new PreviewPagerAdapter(getSupportFragmentManager(), null);
    this.mPager.setAdapter(this.mAdapter);
    this.mCheckView = ((CheckView)findViewById(R.id.check_view));
    this.mCheckView.setCountable(this.mSpec.countable);
    this.mBottomToolbar = ((FrameLayout)findViewById(R.id.bottom_toolbar));
    this.mTopToolbar = ((FrameLayout)findViewById(R.id.top_toolbar));
    this.mCheckView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = BasePreviewActivity.this.mAdapter.getMediaItem(BasePreviewActivity.this.mPager.getCurrentItem());
        if (BasePreviewActivity.this.mSelectedCollection.isSelected(paramAnonymousView))
        {
          BasePreviewActivity.this.mSelectedCollection.remove(paramAnonymousView);
          if (BasePreviewActivity.this.mSpec.countable) {
            BasePreviewActivity.this.mCheckView.setCheckedNum(Integer.MIN_VALUE);
          } else {
            BasePreviewActivity.this.mCheckView.setChecked(false);
          }
        }
        else if (BasePreviewActivity.this.assertAddSelection(paramAnonymousView))
        {
          BasePreviewActivity.this.mSelectedCollection.add(paramAnonymousView);
          if (BasePreviewActivity.this.mSpec.countable) {
            BasePreviewActivity.this.mCheckView.setCheckedNum(BasePreviewActivity.this.mSelectedCollection.checkedNumOf(paramAnonymousView));
          } else {
            BasePreviewActivity.this.mCheckView.setChecked(true);
          }
        }
        BasePreviewActivity.this.updateApplyButton();
        if (BasePreviewActivity.this.mSpec.onSelectedListener != null) {
          BasePreviewActivity.this.mSpec.onSelectedListener.onSelected(BasePreviewActivity.this.mSelectedCollection.asListOfUri(), BasePreviewActivity.this.mSelectedCollection.asListOfString());
        }
      }
    });
    this.mOriginalLayout = ((LinearLayout)findViewById(R.id.originalLayout));
    this.mOriginal = ((CheckRadioView)findViewById(R.id.original));
    this.mOriginalLayout.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        int i = BasePreviewActivity.this.countOverMaxSize();
        if (i > 0)
        {
          IncapableDialog.newInstance("", BasePreviewActivity.this.getString(R.string.error_over_original_count, new Object[] { Integer.valueOf(i), Integer.valueOf(BasePreviewActivity.this.mSpec.originalMaxSize) })).show(BasePreviewActivity.this.getSupportFragmentManager(), IncapableDialog.class.getName());
          return;
        }
        paramAnonymousView = BasePreviewActivity.this;
        paramAnonymousView.mOriginalEnable = (true ^ paramAnonymousView.mOriginalEnable);
        BasePreviewActivity.this.mOriginal.setChecked(BasePreviewActivity.this.mOriginalEnable);
        if (!BasePreviewActivity.this.mOriginalEnable) {
          BasePreviewActivity.this.mOriginal.setColor(-1);
        }
        if (BasePreviewActivity.this.mSpec.onCheckedListener != null) {
          BasePreviewActivity.this.mSpec.onCheckedListener.onCheck(BasePreviewActivity.this.mOriginalEnable);
        }
      }
    });
    updateApplyButton();
  }
  
  public void onPageScrollStateChanged(int paramInt) {}
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public void onPageSelected(int paramInt)
  {
    Object localObject = (PreviewPagerAdapter)this.mPager.getAdapter();
    int i = this.mPreviousPos;
    if ((i != -1) && (i != paramInt))
    {
      ((PreviewItemFragment)((PreviewPagerAdapter)localObject).instantiateItem(this.mPager, i)).resetView();
      localObject = ((PreviewPagerAdapter)localObject).getMediaItem(paramInt);
      if (this.mSpec.countable)
      {
        i = this.mSelectedCollection.checkedNumOf((Item)localObject);
        this.mCheckView.setCheckedNum(i);
        if (i > 0) {
          this.mCheckView.setEnabled(true);
        } else {
          this.mCheckView.setEnabled(true ^ this.mSelectedCollection.maxSelectableReached());
        }
      }
      else
      {
        boolean bool = this.mSelectedCollection.isSelected((Item)localObject);
        this.mCheckView.setChecked(bool);
        if (bool) {
          this.mCheckView.setEnabled(true);
        } else {
          this.mCheckView.setEnabled(true ^ this.mSelectedCollection.maxSelectableReached());
        }
      }
      updateSize((Item)localObject);
    }
    this.mPreviousPos = paramInt;
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.mSelectedCollection.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("checkState", this.mOriginalEnable);
    super.onSaveInstanceState(paramBundle);
  }
  
  protected void sendBackResult(boolean paramBoolean)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("extra_result_bundle", this.mSelectedCollection.getDataWithBundle());
    localIntent.putExtra("extra_result_apply", paramBoolean);
    localIntent.putExtra("extra_result_original_enable", this.mOriginalEnable);
    setResult(-1, localIntent);
  }
  
  protected void updateSize(Item paramItem)
  {
    if (paramItem.isGif())
    {
      this.mSize.setVisibility(0);
      TextView localTextView = this.mSize;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(PhotoMetadataUtils.getSizeInMB(paramItem.size));
      localStringBuilder.append("M");
      localTextView.setText(localStringBuilder.toString());
    }
    else
    {
      this.mSize.setVisibility(8);
    }
    if (paramItem.isVideo()) {
      this.mOriginalLayout.setVisibility(8);
    } else if (this.mSpec.originalable) {
      this.mOriginalLayout.setVisibility(0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/BasePreviewActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */