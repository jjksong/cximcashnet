package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class PreviewViewPager
  extends ViewPager
{
  public PreviewViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  protected boolean canScroll(View paramView, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramView instanceof ImageViewTouch))
    {
      if ((!((ImageViewTouch)paramView).canScroll(paramInt1)) && (!super.canScroll(paramView, paramBoolean, paramInt1, paramInt2, paramInt3))) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      }
      return paramBoolean;
    }
    return super.canScroll(paramView, paramBoolean, paramInt1, paramInt2, paramInt3);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/PreviewViewPager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */