package com.zhihu.matisse.internal.ui.widget;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import com.zhihu.matisse.R.string;

public class IncapableDialog
  extends DialogFragment
{
  public static final String EXTRA_MESSAGE = "extra_message";
  public static final String EXTRA_TITLE = "extra_title";
  
  public static IncapableDialog newInstance(String paramString1, String paramString2)
  {
    IncapableDialog localIncapableDialog = new IncapableDialog();
    Bundle localBundle = new Bundle();
    localBundle.putString("extra_title", paramString1);
    localBundle.putString("extra_message", paramString2);
    localIncapableDialog.setArguments(localBundle);
    return localIncapableDialog;
  }
  
  @NonNull
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    String str2 = getArguments().getString("extra_title");
    String str1 = getArguments().getString("extra_message");
    paramBundle = new AlertDialog.Builder(getActivity());
    if (!TextUtils.isEmpty(str2)) {
      paramBundle.setTitle(str2);
    }
    if (!TextUtils.isEmpty(str1)) {
      paramBundle.setMessage(str1);
    }
    paramBundle.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    return paramBundle.create();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/IncapableDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */