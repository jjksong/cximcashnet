package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

public class RoundedRectangleImageView
  extends AppCompatImageView
{
  private float mRadius;
  private RectF mRectF;
  private Path mRoundedRectPath;
  
  public RoundedRectangleImageView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public RoundedRectangleImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public RoundedRectangleImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.mRadius = (paramContext.getResources().getDisplayMetrics().density * 2.0F);
    this.mRoundedRectPath = new Path();
    this.mRectF = new RectF();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    paramCanvas.clipPath(this.mRoundedRectPath);
    super.onDraw(paramCanvas);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    this.mRectF.set(0.0F, 0.0F, getMeasuredWidth(), getMeasuredHeight());
    Path localPath = this.mRoundedRectPath;
    RectF localRectF = this.mRectF;
    float f = this.mRadius;
    localPath.addRoundRect(localRectF, f, f, Path.Direction.CW);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/RoundedRectangleImageView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */