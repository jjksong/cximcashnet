package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.zhihu.matisse.R.color;
import com.zhihu.matisse.R.drawable;

public class CheckRadioView
  extends AppCompatImageView
{
  private Drawable mDrawable;
  private int mSelectedColor;
  private int mUnSelectUdColor;
  
  public CheckRadioView(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public CheckRadioView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init()
  {
    this.mSelectedColor = ResourcesCompat.getColor(getResources(), R.color.zhihu_item_checkCircle_backgroundColor, getContext().getTheme());
    this.mUnSelectUdColor = ResourcesCompat.getColor(getResources(), R.color.zhihu_check_original_radio_disable, getContext().getTheme());
    setChecked(false);
  }
  
  public void setChecked(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      setImageResource(R.drawable.ic_preview_radio_on);
      this.mDrawable = getDrawable();
      this.mDrawable.setColorFilter(this.mSelectedColor, PorterDuff.Mode.SRC_IN);
    }
    else
    {
      setImageResource(R.drawable.ic_preview_radio_off);
      this.mDrawable = getDrawable();
      this.mDrawable.setColorFilter(this.mUnSelectUdColor, PorterDuff.Mode.SRC_IN);
    }
  }
  
  public void setColor(int paramInt)
  {
    if (this.mDrawable == null) {
      this.mDrawable = getDrawable();
    }
    this.mDrawable.setColorFilter(paramInt, PorterDuff.Mode.SRC_IN);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/CheckRadioView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */