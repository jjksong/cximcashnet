package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.ListPopupWindow;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewPropertyAnimator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.zhihu.matisse.R.attr;
import com.zhihu.matisse.R.dimen;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.utils.Platform;

public class AlbumsSpinner
{
  private static final int MAX_SHOWN_COUNT = 6;
  private CursorAdapter mAdapter;
  private ListPopupWindow mListPopupWindow;
  private AdapterView.OnItemSelectedListener mOnItemSelectedListener;
  private TextView mSelected;
  
  public AlbumsSpinner(@NonNull Context paramContext)
  {
    this.mListPopupWindow = new ListPopupWindow(paramContext, null, R.attr.listPopupWindowStyle);
    this.mListPopupWindow.setModal(true);
    float f = paramContext.getResources().getDisplayMetrics().density;
    this.mListPopupWindow.setContentWidth((int)(216.0F * f));
    this.mListPopupWindow.setHorizontalOffset((int)(16.0F * f));
    this.mListPopupWindow.setVerticalOffset((int)(f * -48.0F));
    this.mListPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        AlbumsSpinner.this.onItemSelected(paramAnonymousAdapterView.getContext(), paramAnonymousInt);
        if (AlbumsSpinner.this.mOnItemSelectedListener != null) {
          AlbumsSpinner.this.mOnItemSelectedListener.onItemSelected(paramAnonymousAdapterView, paramAnonymousView, paramAnonymousInt, paramAnonymousLong);
        }
      }
    });
  }
  
  private void onItemSelected(Context paramContext, int paramInt)
  {
    this.mListPopupWindow.dismiss();
    Object localObject = this.mAdapter.getCursor();
    ((Cursor)localObject).moveToPosition(paramInt);
    localObject = Album.valueOf((Cursor)localObject).getDisplayName(paramContext);
    if (this.mSelected.getVisibility() == 0)
    {
      this.mSelected.setText((CharSequence)localObject);
    }
    else if (Platform.hasICS())
    {
      this.mSelected.setAlpha(0.0F);
      this.mSelected.setVisibility(0);
      this.mSelected.setText((CharSequence)localObject);
      this.mSelected.animate().alpha(1.0F).setDuration(paramContext.getResources().getInteger(17694722)).start();
    }
    else
    {
      this.mSelected.setVisibility(0);
      this.mSelected.setText((CharSequence)localObject);
    }
  }
  
  public void setAdapter(CursorAdapter paramCursorAdapter)
  {
    this.mListPopupWindow.setAdapter(paramCursorAdapter);
    this.mAdapter = paramCursorAdapter;
  }
  
  public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener)
  {
    this.mOnItemSelectedListener = paramOnItemSelectedListener;
  }
  
  public void setPopupAnchorView(View paramView)
  {
    this.mListPopupWindow.setAnchorView(paramView);
  }
  
  public void setSelectedTextView(TextView paramTextView)
  {
    this.mSelected = paramTextView;
    paramTextView = this.mSelected.getCompoundDrawables()[2];
    TypedArray localTypedArray = this.mSelected.getContext().getTheme().obtainStyledAttributes(new int[] { R.attr.album_element_color });
    int i = localTypedArray.getColor(0, 0);
    localTypedArray.recycle();
    paramTextView.setColorFilter(i, PorterDuff.Mode.SRC_IN);
    this.mSelected.setVisibility(8);
    this.mSelected.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        int i = paramAnonymousView.getResources().getDimensionPixelSize(R.dimen.album_item_height);
        paramAnonymousView = AlbumsSpinner.this.mListPopupWindow;
        if (AlbumsSpinner.this.mAdapter.getCount() > 6) {
          i *= 6;
        } else {
          i *= AlbumsSpinner.this.mAdapter.getCount();
        }
        paramAnonymousView.setHeight(i);
        AlbumsSpinner.this.mListPopupWindow.show();
      }
    });
    paramTextView = this.mSelected;
    paramTextView.setOnTouchListener(this.mListPopupWindow.createDragToOpenListener(paramTextView));
  }
  
  public void setSelection(Context paramContext, int paramInt)
  {
    this.mListPopupWindow.setSelection(paramInt);
    onItemSelected(paramContext, paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/AlbumsSpinner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */