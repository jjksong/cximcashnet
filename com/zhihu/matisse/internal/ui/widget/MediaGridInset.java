package com.zhihu.matisse.internal.ui.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public class MediaGridInset
  extends RecyclerView.ItemDecoration
{
  private boolean mIncludeEdge;
  private int mSpacing;
  private int mSpanCount;
  
  public MediaGridInset(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    this.mSpanCount = paramInt1;
    this.mSpacing = paramInt2;
    this.mIncludeEdge = paramBoolean;
  }
  
  public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    int j = paramRecyclerView.getChildAdapterPosition(paramView);
    int k = this.mSpanCount;
    int i = j % k;
    int m;
    if (this.mIncludeEdge)
    {
      m = this.mSpacing;
      paramRect.left = (m - i * m / k);
      paramRect.right = ((i + 1) * m / k);
      if (j < k) {
        paramRect.top = m;
      }
      paramRect.bottom = this.mSpacing;
    }
    else
    {
      m = this.mSpacing;
      paramRect.left = (i * m / k);
      paramRect.right = (m - (i + 1) * m / k);
      if (j >= k) {
        paramRect.top = m;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/MediaGridInset.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */