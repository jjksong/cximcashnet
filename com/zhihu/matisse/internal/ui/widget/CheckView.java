package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import com.zhihu.matisse.R.attr;
import com.zhihu.matisse.R.color;
import com.zhihu.matisse.R.drawable;

public class CheckView
  extends View
{
  private static final float BG_RADIUS = 11.0F;
  private static final int CONTENT_SIZE = 16;
  private static final float SHADOW_WIDTH = 6.0F;
  private static final int SIZE = 48;
  private static final float STROKE_RADIUS = 11.5F;
  private static final float STROKE_WIDTH = 3.0F;
  public static final int UNCHECKED = Integer.MIN_VALUE;
  private Paint mBackgroundPaint;
  private Drawable mCheckDrawable;
  private Rect mCheckRect;
  private boolean mChecked;
  private int mCheckedNum;
  private boolean mCountable;
  private float mDensity;
  private boolean mEnabled = true;
  private Paint mShadowPaint;
  private Paint mStrokePaint;
  private TextPaint mTextPaint;
  
  public CheckView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public CheckView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public CheckView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private Rect getCheckRect()
  {
    if (this.mCheckRect == null)
    {
      float f2 = this.mDensity;
      int i = (int)(f2 * 48.0F / 2.0F - 16.0F * f2 / 2.0F);
      float f1 = i;
      this.mCheckRect = new Rect(i, i, (int)(f2 * 48.0F - f1), (int)(f2 * 48.0F - f1));
    }
    return this.mCheckRect;
  }
  
  private void init(Context paramContext)
  {
    this.mDensity = paramContext.getResources().getDisplayMetrics().density;
    this.mStrokePaint = new Paint();
    this.mStrokePaint.setAntiAlias(true);
    this.mStrokePaint.setStyle(Paint.Style.STROKE);
    this.mStrokePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    this.mStrokePaint.setStrokeWidth(this.mDensity * 3.0F);
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(new int[] { R.attr.item_checkCircle_borderColor });
    int i = localTypedArray.getColor(0, ResourcesCompat.getColor(getResources(), R.color.zhihu_item_checkCircle_borderColor, getContext().getTheme()));
    localTypedArray.recycle();
    this.mStrokePaint.setColor(i);
    this.mCheckDrawable = ResourcesCompat.getDrawable(paramContext.getResources(), R.drawable.ic_check_white_18dp, paramContext.getTheme());
  }
  
  private void initBackgroundPaint()
  {
    if (this.mBackgroundPaint == null)
    {
      this.mBackgroundPaint = new Paint();
      this.mBackgroundPaint.setAntiAlias(true);
      this.mBackgroundPaint.setStyle(Paint.Style.FILL);
      TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(new int[] { R.attr.item_checkCircle_backgroundColor });
      int i = localTypedArray.getColor(0, ResourcesCompat.getColor(getResources(), R.color.zhihu_item_checkCircle_backgroundColor, getContext().getTheme()));
      localTypedArray.recycle();
      this.mBackgroundPaint.setColor(i);
    }
  }
  
  private void initShadowPaint()
  {
    if (this.mShadowPaint == null)
    {
      this.mShadowPaint = new Paint();
      this.mShadowPaint.setAntiAlias(true);
      Paint localPaint = this.mShadowPaint;
      float f1 = this.mDensity;
      float f2 = f1 * 48.0F / 2.0F;
      float f3 = 48.0F * f1 / 2.0F;
      int j = Color.parseColor("#00000000");
      int k = Color.parseColor("#0D000000");
      int i = Color.parseColor("#0D000000");
      int m = Color.parseColor("#00000000");
      Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
      localPaint.setShader(new RadialGradient(f2, f3, 19.0F * f1, new int[] { j, k, i, m }, new float[] { 0.21052632F, 0.5263158F, 0.68421054F, 1.0F }, localTileMode));
    }
  }
  
  private void initTextPaint()
  {
    if (this.mTextPaint == null)
    {
      this.mTextPaint = new TextPaint();
      this.mTextPaint.setAntiAlias(true);
      this.mTextPaint.setColor(-1);
      this.mTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
      this.mTextPaint.setTextSize(this.mDensity * 12.0F);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    initShadowPaint();
    float f = this.mDensity;
    paramCanvas.drawCircle(f * 48.0F / 2.0F, f * 48.0F / 2.0F, f * 19.0F, this.mShadowPaint);
    f = this.mDensity;
    paramCanvas.drawCircle(f * 48.0F / 2.0F, f * 48.0F / 2.0F, f * 11.5F, this.mStrokePaint);
    if (this.mCountable)
    {
      if (this.mCheckedNum != Integer.MIN_VALUE)
      {
        initBackgroundPaint();
        f = this.mDensity;
        paramCanvas.drawCircle(f * 48.0F / 2.0F, 48.0F * f / 2.0F, f * 11.0F, this.mBackgroundPaint);
        initTextPaint();
        String str = String.valueOf(this.mCheckedNum);
        int i = (int)(paramCanvas.getWidth() - this.mTextPaint.measureText(str)) / 2;
        int j = (int)(paramCanvas.getHeight() - this.mTextPaint.descent() - this.mTextPaint.ascent()) / 2;
        paramCanvas.drawText(str, i, j, this.mTextPaint);
      }
    }
    else if (this.mChecked)
    {
      initBackgroundPaint();
      f = this.mDensity;
      paramCanvas.drawCircle(f * 48.0F / 2.0F, 48.0F * f / 2.0F, f * 11.0F, this.mBackgroundPaint);
      this.mCheckDrawable.setBounds(getCheckRect());
      this.mCheckDrawable.draw(paramCanvas);
    }
    if (this.mEnabled) {
      f = 1.0F;
    } else {
      f = 0.5F;
    }
    setAlpha(f);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    paramInt1 = View.MeasureSpec.makeMeasureSpec((int)(this.mDensity * 48.0F), 1073741824);
    super.onMeasure(paramInt1, paramInt1);
  }
  
  public void setChecked(boolean paramBoolean)
  {
    if (!this.mCountable)
    {
      this.mChecked = paramBoolean;
      invalidate();
      return;
    }
    throw new IllegalStateException("CheckView is countable, call setCheckedNum() instead.");
  }
  
  public void setCheckedNum(int paramInt)
  {
    if (this.mCountable)
    {
      if ((paramInt != Integer.MIN_VALUE) && (paramInt <= 0)) {
        throw new IllegalArgumentException("checked num can't be negative.");
      }
      this.mCheckedNum = paramInt;
      invalidate();
      return;
    }
    throw new IllegalStateException("CheckView is not countable, call setChecked() instead.");
  }
  
  public void setCountable(boolean paramBoolean)
  {
    this.mCountable = paramBoolean;
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    if (this.mEnabled != paramBoolean)
    {
      this.mEnabled = paramBoolean;
      invalidate();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/CheckView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */