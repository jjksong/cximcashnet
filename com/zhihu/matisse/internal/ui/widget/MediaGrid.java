package com.zhihu.matisse.internal.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.engine.ImageEngine;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;

public class MediaGrid
  extends SquareFrameLayout
  implements View.OnClickListener
{
  private CheckView mCheckView;
  private ImageView mGifTag;
  private OnMediaGridClickListener mListener;
  private Item mMedia;
  private PreBindInfo mPreBindInfo;
  private ImageView mThumbnail;
  private TextView mVideoDuration;
  
  public MediaGrid(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public MediaGrid(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    LayoutInflater.from(paramContext).inflate(R.layout.media_grid_content, this, true);
    this.mThumbnail = ((ImageView)findViewById(R.id.media_thumbnail));
    this.mCheckView = ((CheckView)findViewById(R.id.check_view));
    this.mGifTag = ((ImageView)findViewById(R.id.gif));
    this.mVideoDuration = ((TextView)findViewById(R.id.video_duration));
    this.mThumbnail.setOnClickListener(this);
    this.mCheckView.setOnClickListener(this);
  }
  
  private void initCheckView()
  {
    this.mCheckView.setCountable(this.mPreBindInfo.mCheckViewCountable);
  }
  
  private void setGifTag()
  {
    ImageView localImageView = this.mGifTag;
    int i;
    if (this.mMedia.isGif()) {
      i = 0;
    } else {
      i = 8;
    }
    localImageView.setVisibility(i);
  }
  
  private void setImage()
  {
    if (this.mMedia.isGif()) {
      SelectionSpec.getInstance().imageEngine.loadGifThumbnail(getContext(), this.mPreBindInfo.mResize, this.mPreBindInfo.mPlaceholder, this.mThumbnail, this.mMedia.getContentUri());
    } else {
      SelectionSpec.getInstance().imageEngine.loadThumbnail(getContext(), this.mPreBindInfo.mResize, this.mPreBindInfo.mPlaceholder, this.mThumbnail, this.mMedia.getContentUri());
    }
  }
  
  private void setVideoDuration()
  {
    if (this.mMedia.isVideo())
    {
      this.mVideoDuration.setVisibility(0);
      this.mVideoDuration.setText(DateUtils.formatElapsedTime(this.mMedia.duration / 1000L));
    }
    else
    {
      this.mVideoDuration.setVisibility(8);
    }
  }
  
  public void bindMedia(Item paramItem)
  {
    this.mMedia = paramItem;
    setGifTag();
    initCheckView();
    setImage();
    setVideoDuration();
  }
  
  public Item getMedia()
  {
    return this.mMedia;
  }
  
  public void onClick(View paramView)
  {
    OnMediaGridClickListener localOnMediaGridClickListener = this.mListener;
    if (localOnMediaGridClickListener != null)
    {
      Object localObject = this.mThumbnail;
      if (paramView == localObject)
      {
        localOnMediaGridClickListener.onThumbnailClicked((ImageView)localObject, this.mMedia, this.mPreBindInfo.mViewHolder);
      }
      else
      {
        localObject = this.mCheckView;
        if (paramView == localObject) {
          localOnMediaGridClickListener.onCheckViewClicked((CheckView)localObject, this.mMedia, this.mPreBindInfo.mViewHolder);
        }
      }
    }
  }
  
  public void preBindMedia(PreBindInfo paramPreBindInfo)
  {
    this.mPreBindInfo = paramPreBindInfo;
  }
  
  public void removeOnMediaGridClickListener()
  {
    this.mListener = null;
  }
  
  public void setCheckEnabled(boolean paramBoolean)
  {
    this.mCheckView.setEnabled(paramBoolean);
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.mCheckView.setChecked(paramBoolean);
  }
  
  public void setCheckedNum(int paramInt)
  {
    this.mCheckView.setCheckedNum(paramInt);
  }
  
  public void setOnMediaGridClickListener(OnMediaGridClickListener paramOnMediaGridClickListener)
  {
    this.mListener = paramOnMediaGridClickListener;
  }
  
  public static abstract interface OnMediaGridClickListener
  {
    public abstract void onCheckViewClicked(CheckView paramCheckView, Item paramItem, RecyclerView.ViewHolder paramViewHolder);
    
    public abstract void onThumbnailClicked(ImageView paramImageView, Item paramItem, RecyclerView.ViewHolder paramViewHolder);
  }
  
  public static class PreBindInfo
  {
    boolean mCheckViewCountable;
    Drawable mPlaceholder;
    int mResize;
    RecyclerView.ViewHolder mViewHolder;
    
    public PreBindInfo(int paramInt, Drawable paramDrawable, boolean paramBoolean, RecyclerView.ViewHolder paramViewHolder)
    {
      this.mResize = paramInt;
      this.mPlaceholder = paramDrawable;
      this.mCheckViewCountable = paramBoolean;
      this.mViewHolder = paramViewHolder;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/widget/MediaGrid.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */