package com.zhihu.matisse.internal.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.ui.adapter.PreviewPagerAdapter;
import com.zhihu.matisse.internal.ui.widget.CheckView;
import java.util.List;

public class SelectedPreviewActivity
  extends BasePreviewActivity
{
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (!SelectionSpec.getInstance().hasInited)
    {
      setResult(0);
      finish();
      return;
    }
    paramBundle = getIntent().getBundleExtra("extra_default_bundle").getParcelableArrayList("state_selection");
    this.mAdapter.addAll(paramBundle);
    this.mAdapter.notifyDataSetChanged();
    if (this.mSpec.countable) {
      this.mCheckView.setCheckedNum(1);
    } else {
      this.mCheckView.setChecked(true);
    }
    this.mPreviousPos = 0;
    updateSize((Item)paramBundle.get(0));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/SelectedPreviewActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */