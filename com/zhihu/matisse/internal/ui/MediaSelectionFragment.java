package com.zhihu.matisse.internal.ui;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.zhihu.matisse.R.dimen;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.model.AlbumMediaCollection;
import com.zhihu.matisse.internal.model.AlbumMediaCollection.AlbumMediaCallbacks;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter.CheckStateListener;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter.OnMediaClickListener;
import com.zhihu.matisse.internal.ui.widget.MediaGridInset;
import com.zhihu.matisse.internal.utils.UIUtils;

public class MediaSelectionFragment
  extends Fragment
  implements AlbumMediaCollection.AlbumMediaCallbacks, AlbumMediaAdapter.CheckStateListener, AlbumMediaAdapter.OnMediaClickListener
{
  public static final String EXTRA_ALBUM = "extra_album";
  private AlbumMediaAdapter mAdapter;
  private final AlbumMediaCollection mAlbumMediaCollection = new AlbumMediaCollection();
  private AlbumMediaAdapter.CheckStateListener mCheckStateListener;
  private AlbumMediaAdapter.OnMediaClickListener mOnMediaClickListener;
  private RecyclerView mRecyclerView;
  private SelectionProvider mSelectionProvider;
  
  public static MediaSelectionFragment newInstance(Album paramAlbum)
  {
    MediaSelectionFragment localMediaSelectionFragment = new MediaSelectionFragment();
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("extra_album", paramAlbum);
    localMediaSelectionFragment.setArguments(localBundle);
    return localMediaSelectionFragment;
  }
  
  public void onActivityCreated(@Nullable Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    Album localAlbum = (Album)getArguments().getParcelable("extra_album");
    this.mAdapter = new AlbumMediaAdapter(getContext(), this.mSelectionProvider.provideSelectedItemCollection(), this.mRecyclerView);
    this.mAdapter.registerCheckStateListener(this);
    this.mAdapter.registerOnMediaClickListener(this);
    this.mRecyclerView.setHasFixedSize(true);
    paramBundle = SelectionSpec.getInstance();
    int i;
    if (paramBundle.gridExpectedSize > 0) {
      i = UIUtils.spanCount(getContext(), paramBundle.gridExpectedSize);
    } else {
      i = paramBundle.spanCount;
    }
    this.mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), i));
    int j = getResources().getDimensionPixelSize(R.dimen.media_grid_spacing);
    this.mRecyclerView.addItemDecoration(new MediaGridInset(i, j, false));
    this.mRecyclerView.setAdapter(this.mAdapter);
    this.mAlbumMediaCollection.onCreate(getActivity(), this);
    this.mAlbumMediaCollection.load(localAlbum, paramBundle.capture);
  }
  
  public void onAlbumMediaLoad(Cursor paramCursor)
  {
    this.mAdapter.swapCursor(paramCursor);
  }
  
  public void onAlbumMediaReset()
  {
    this.mAdapter.swapCursor(null);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof SelectionProvider))
    {
      this.mSelectionProvider = ((SelectionProvider)paramContext);
      if ((paramContext instanceof AlbumMediaAdapter.CheckStateListener)) {
        this.mCheckStateListener = ((AlbumMediaAdapter.CheckStateListener)paramContext);
      }
      if ((paramContext instanceof AlbumMediaAdapter.OnMediaClickListener)) {
        this.mOnMediaClickListener = ((AlbumMediaAdapter.OnMediaClickListener)paramContext);
      }
      return;
    }
    throw new IllegalStateException("Context must implement SelectionProvider.");
  }
  
  @Nullable
  public View onCreateView(LayoutInflater paramLayoutInflater, @Nullable ViewGroup paramViewGroup, @Nullable Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(R.layout.fragment_media_selection, paramViewGroup, false);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    this.mAlbumMediaCollection.onDestroy();
  }
  
  public void onMediaClick(Album paramAlbum, Item paramItem, int paramInt)
  {
    paramAlbum = this.mOnMediaClickListener;
    if (paramAlbum != null) {
      paramAlbum.onMediaClick((Album)getArguments().getParcelable("extra_album"), paramItem, paramInt);
    }
  }
  
  public void onUpdate()
  {
    AlbumMediaAdapter.CheckStateListener localCheckStateListener = this.mCheckStateListener;
    if (localCheckStateListener != null) {
      localCheckStateListener.onUpdate();
    }
  }
  
  public void onViewCreated(View paramView, @Nullable Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    this.mRecyclerView = ((RecyclerView)paramView.findViewById(R.id.recyclerview));
  }
  
  public void refreshMediaGrid()
  {
    this.mAdapter.notifyDataSetChanged();
  }
  
  public void refreshSelection()
  {
    this.mAdapter.refreshSelection();
  }
  
  public static abstract interface SelectionProvider
  {
    public abstract SelectedItemCollection provideSelectedItemCollection();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/ui/MediaSelectionFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */