package com.zhihu.matisse.internal.loader;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.provider.MediaStore.Files;
import android.support.v4.content.CursorLoader;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.SelectionSpec;

public class AlbumLoader
  extends CursorLoader
{
  private static final String BUCKET_ORDER_BY = "datetaken DESC";
  private static final String[] COLUMNS = { "_id", "bucket_id", "bucket_display_name", "_data", "count" };
  public static final String COLUMN_COUNT = "count";
  private static final String[] PROJECTION = { "_id", "bucket_id", "bucket_display_name", "_data", "COUNT(*) AS count" };
  private static final Uri QUERY_URI = MediaStore.Files.getContentUri("external");
  private static final String SELECTION = "(media_type=? OR media_type=?) AND _size>0) GROUP BY (bucket_id";
  private static final String[] SELECTION_ARGS = { String.valueOf(1), String.valueOf(3) };
  private static final String SELECTION_FOR_SINGLE_MEDIA_TYPE = "media_type=? AND _size>0) GROUP BY (bucket_id";
  
  private AlbumLoader(Context paramContext, String paramString, String[] paramArrayOfString)
  {
    super(paramContext, QUERY_URI, PROJECTION, paramString, paramArrayOfString, "datetaken DESC");
  }
  
  private static String[] getSelectionArgsForSingleMediaType(int paramInt)
  {
    return new String[] { String.valueOf(paramInt) };
  }
  
  public static CursorLoader newInstance(Context paramContext)
  {
    String str;
    String[] arrayOfString;
    if (SelectionSpec.getInstance().onlyShowImages())
    {
      str = "media_type=? AND _size>0) GROUP BY (bucket_id";
      arrayOfString = getSelectionArgsForSingleMediaType(1);
    }
    else if (SelectionSpec.getInstance().onlyShowVideos())
    {
      str = "media_type=? AND _size>0) GROUP BY (bucket_id";
      arrayOfString = getSelectionArgsForSingleMediaType(3);
    }
    else
    {
      str = "(media_type=? OR media_type=?) AND _size>0) GROUP BY (bucket_id";
      arrayOfString = SELECTION_ARGS;
    }
    return new AlbumLoader(paramContext, str, arrayOfString);
  }
  
  public Cursor loadInBackground()
  {
    Cursor localCursor = super.loadInBackground();
    MatrixCursor localMatrixCursor = new MatrixCursor(COLUMNS);
    String str = "";
    int j;
    if (localCursor != null)
    {
      int i = 0;
      while (localCursor.moveToNext()) {
        i += localCursor.getInt(localCursor.getColumnIndex("count"));
      }
      j = i;
      if (localCursor.moveToFirst())
      {
        str = localCursor.getString(localCursor.getColumnIndex("_data"));
        j = i;
      }
    }
    else
    {
      j = 0;
    }
    localMatrixCursor.addRow(new String[] { Album.ALBUM_ID_ALL, Album.ALBUM_ID_ALL, "All", str, String.valueOf(j) });
    return new MergeCursor(new Cursor[] { localMatrixCursor, localCursor });
  }
  
  public void onContentChanged() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/loader/AlbumLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */