package com.zhihu.matisse.internal.loader;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.provider.MediaStore.Files;
import android.support.v4.content.CursorLoader;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.utils.MediaStoreCompat;

public class AlbumMediaLoader
  extends CursorLoader
{
  private static final String ORDER_BY = "datetaken DESC";
  private static final String[] PROJECTION = { "_id", "_display_name", "mime_type", "_size", "duration" };
  private static final Uri QUERY_URI = MediaStore.Files.getContentUri("external");
  private static final String SELECTION_ALBUM = "(media_type=? OR media_type=?) AND  bucket_id=? AND _size>0";
  private static final String SELECTION_ALBUM_FOR_SINGLE_MEDIA_TYPE = "media_type=? AND  bucket_id=? AND _size>0";
  private static final String SELECTION_ALL = "(media_type=? OR media_type=?) AND _size>0";
  private static final String[] SELECTION_ALL_ARGS = { String.valueOf(1), String.valueOf(3) };
  private static final String SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE = "media_type=? AND _size>0";
  private final boolean mEnableCapture;
  
  private AlbumMediaLoader(Context paramContext, String paramString, String[] paramArrayOfString, boolean paramBoolean)
  {
    super(paramContext, QUERY_URI, PROJECTION, paramString, paramArrayOfString, "datetaken DESC");
    this.mEnableCapture = paramBoolean;
  }
  
  private static String[] getSelectionAlbumArgs(String paramString)
  {
    return new String[] { String.valueOf(1), String.valueOf(3), paramString };
  }
  
  private static String[] getSelectionAlbumArgsForSingleMediaType(int paramInt, String paramString)
  {
    return new String[] { String.valueOf(paramInt), paramString };
  }
  
  private static String[] getSelectionArgsForSingleMediaType(int paramInt)
  {
    return new String[] { String.valueOf(paramInt) };
  }
  
  public static CursorLoader newInstance(Context paramContext, Album paramAlbum, boolean paramBoolean)
  {
    String str;
    if (paramAlbum.isAll())
    {
      if (SelectionSpec.getInstance().onlyShowImages())
      {
        str = "media_type=? AND _size>0";
        paramAlbum = getSelectionArgsForSingleMediaType(1);
      }
      else if (SelectionSpec.getInstance().onlyShowVideos())
      {
        str = "media_type=? AND _size>0";
        paramAlbum = getSelectionArgsForSingleMediaType(3);
      }
      else
      {
        str = "(media_type=? OR media_type=?) AND _size>0";
        paramAlbum = SELECTION_ALL_ARGS;
      }
    }
    else
    {
      if (SelectionSpec.getInstance().onlyShowImages())
      {
        paramAlbum = getSelectionAlbumArgsForSingleMediaType(1, paramAlbum.getId());
        str = "media_type=? AND  bucket_id=? AND _size>0";
      }
      else if (SelectionSpec.getInstance().onlyShowVideos())
      {
        paramAlbum = getSelectionAlbumArgsForSingleMediaType(3, paramAlbum.getId());
        str = "media_type=? AND  bucket_id=? AND _size>0";
      }
      else
      {
        paramAlbum = getSelectionAlbumArgs(paramAlbum.getId());
        str = "(media_type=? OR media_type=?) AND  bucket_id=? AND _size>0";
      }
      paramBoolean = false;
    }
    return new AlbumMediaLoader(paramContext, str, paramAlbum, paramBoolean);
  }
  
  public Cursor loadInBackground()
  {
    Cursor localCursor = super.loadInBackground();
    if ((this.mEnableCapture) && (MediaStoreCompat.hasCameraFeature(getContext())))
    {
      MatrixCursor localMatrixCursor = new MatrixCursor(PROJECTION);
      localMatrixCursor.addRow(new Object[] { Long.valueOf(-1L), "Capture", "", Integer.valueOf(0), Integer.valueOf(0) });
      return new MergeCursor(new Cursor[] { localMatrixCursor, localCursor });
    }
    return localCursor;
  }
  
  public void onContentChanged() {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/internal/loader/AlbumMediaLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */