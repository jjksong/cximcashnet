package com.zhihu.matisse;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Set;

public final class Matisse
{
  private final WeakReference<Activity> mContext;
  private final WeakReference<Fragment> mFragment;
  
  private Matisse(Activity paramActivity)
  {
    this(paramActivity, null);
  }
  
  private Matisse(Activity paramActivity, Fragment paramFragment)
  {
    this.mContext = new WeakReference(paramActivity);
    this.mFragment = new WeakReference(paramFragment);
  }
  
  private Matisse(Fragment paramFragment)
  {
    this(paramFragment.getActivity(), paramFragment);
  }
  
  public static Matisse from(Activity paramActivity)
  {
    return new Matisse(paramActivity);
  }
  
  public static Matisse from(Fragment paramFragment)
  {
    return new Matisse(paramFragment);
  }
  
  public static boolean obtainOriginalState(Intent paramIntent)
  {
    return paramIntent.getBooleanExtra("extra_result_original_enable", false);
  }
  
  public static List<String> obtainPathResult(Intent paramIntent)
  {
    return paramIntent.getStringArrayListExtra("extra_result_selection_path");
  }
  
  public static List<Uri> obtainResult(Intent paramIntent)
  {
    return paramIntent.getParcelableArrayListExtra("extra_result_selection");
  }
  
  public SelectionCreator choose(Set<MimeType> paramSet)
  {
    return choose(paramSet, true);
  }
  
  public SelectionCreator choose(Set<MimeType> paramSet, boolean paramBoolean)
  {
    return new SelectionCreator(this, paramSet, paramBoolean);
  }
  
  @Nullable
  Activity getActivity()
  {
    return (Activity)this.mContext.get();
  }
  
  @Nullable
  Fragment getFragment()
  {
    Object localObject = this.mFragment;
    if (localObject != null) {
      localObject = (Fragment)((WeakReference)localObject).get();
    } else {
      localObject = null;
    }
    return (Fragment)localObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/Matisse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */