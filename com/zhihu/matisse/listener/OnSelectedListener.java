package com.zhihu.matisse.listener;

import android.net.Uri;
import android.support.annotation.NonNull;
import java.util.List;

public abstract interface OnSelectedListener
{
  public abstract void onSelected(@NonNull List<Uri> paramList, @NonNull List<String> paramList1);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/listener/OnSelectedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */