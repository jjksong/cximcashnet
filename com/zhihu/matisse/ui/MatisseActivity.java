package com.zhihu.matisse.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhihu.matisse.R.attr;
import com.zhihu.matisse.R.id;
import com.zhihu.matisse.R.layout;
import com.zhihu.matisse.R.string;
import com.zhihu.matisse.internal.entity.Album;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.internal.model.AlbumCollection;
import com.zhihu.matisse.internal.model.AlbumCollection.AlbumCallbacks;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.ui.AlbumPreviewActivity;
import com.zhihu.matisse.internal.ui.MediaSelectionFragment;
import com.zhihu.matisse.internal.ui.MediaSelectionFragment.SelectionProvider;
import com.zhihu.matisse.internal.ui.SelectedPreviewActivity;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter.CheckStateListener;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter.OnMediaClickListener;
import com.zhihu.matisse.internal.ui.adapter.AlbumMediaAdapter.OnPhotoCapture;
import com.zhihu.matisse.internal.ui.adapter.AlbumsAdapter;
import com.zhihu.matisse.internal.ui.widget.AlbumsSpinner;
import com.zhihu.matisse.internal.ui.widget.CheckRadioView;
import com.zhihu.matisse.internal.ui.widget.IncapableDialog;
import com.zhihu.matisse.internal.utils.MediaStoreCompat;
import com.zhihu.matisse.internal.utils.PathUtils;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnSelectedListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MatisseActivity
  extends AppCompatActivity
  implements AlbumCollection.AlbumCallbacks, AdapterView.OnItemSelectedListener, MediaSelectionFragment.SelectionProvider, View.OnClickListener, AlbumMediaAdapter.CheckStateListener, AlbumMediaAdapter.OnMediaClickListener, AlbumMediaAdapter.OnPhotoCapture
{
  public static final String CHECK_STATE = "checkState";
  public static final String EXTRA_RESULT_ORIGINAL_ENABLE = "extra_result_original_enable";
  public static final String EXTRA_RESULT_SELECTION = "extra_result_selection";
  public static final String EXTRA_RESULT_SELECTION_PATH = "extra_result_selection_path";
  private static final int REQUEST_CODE_CAPTURE = 24;
  private static final int REQUEST_CODE_PREVIEW = 23;
  private final AlbumCollection mAlbumCollection = new AlbumCollection();
  private AlbumsAdapter mAlbumsAdapter;
  private AlbumsSpinner mAlbumsSpinner;
  private TextView mButtonApply;
  private TextView mButtonPreview;
  private View mContainer;
  private View mEmptyView;
  private MediaStoreCompat mMediaStoreCompat;
  private CheckRadioView mOriginal;
  private boolean mOriginalEnable;
  private LinearLayout mOriginalLayout;
  private SelectedItemCollection mSelectedCollection = new SelectedItemCollection(this);
  private SelectionSpec mSpec;
  
  private int countOverMaxSize()
  {
    int m = this.mSelectedCollection.count();
    int j = 0;
    int k;
    for (int i = 0; j < m; i = k)
    {
      Item localItem = (Item)this.mSelectedCollection.asList().get(j);
      k = i;
      if (localItem.isImage())
      {
        k = i;
        if (PhotoMetadataUtils.getSizeInMB(localItem.size) > this.mSpec.originalMaxSize) {
          k = i + 1;
        }
      }
      j++;
    }
    return i;
  }
  
  private void onAlbumSelected(Album paramAlbum)
  {
    if ((paramAlbum.isAll()) && (paramAlbum.isEmpty()))
    {
      this.mContainer.setVisibility(8);
      this.mEmptyView.setVisibility(0);
    }
    else
    {
      this.mContainer.setVisibility(0);
      this.mEmptyView.setVisibility(8);
      paramAlbum = MediaSelectionFragment.newInstance(paramAlbum);
      getSupportFragmentManager().beginTransaction().replace(R.id.container, paramAlbum, MediaSelectionFragment.class.getSimpleName()).commitAllowingStateLoss();
    }
  }
  
  private void updateBottomToolbar()
  {
    int i = this.mSelectedCollection.count();
    if (i == 0)
    {
      this.mButtonPreview.setEnabled(false);
      this.mButtonApply.setEnabled(false);
      this.mButtonApply.setText(getString(R.string.button_sure_default));
    }
    else if ((i == 1) && (this.mSpec.singleSelectionModeEnabled()))
    {
      this.mButtonPreview.setEnabled(true);
      this.mButtonApply.setText(R.string.button_sure_default);
      this.mButtonApply.setEnabled(true);
    }
    else
    {
      this.mButtonPreview.setEnabled(true);
      this.mButtonApply.setEnabled(true);
      this.mButtonApply.setText(getString(R.string.button_sure, new Object[] { Integer.valueOf(i) }));
    }
    if (this.mSpec.originalable)
    {
      this.mOriginalLayout.setVisibility(0);
      updateOriginalState();
    }
    else
    {
      this.mOriginalLayout.setVisibility(4);
    }
  }
  
  private void updateOriginalState()
  {
    this.mOriginal.setChecked(this.mOriginalEnable);
    if ((countOverMaxSize() > 0) && (this.mOriginalEnable))
    {
      IncapableDialog.newInstance("", getString(R.string.error_over_original_size, new Object[] { Integer.valueOf(this.mSpec.originalMaxSize) })).show(getSupportFragmentManager(), IncapableDialog.class.getName());
      this.mOriginal.setChecked(false);
      this.mOriginalEnable = false;
    }
  }
  
  public void capture()
  {
    MediaStoreCompat localMediaStoreCompat = this.mMediaStoreCompat;
    if (localMediaStoreCompat != null) {
      localMediaStoreCompat.dispatchCaptureIntent(this, 24);
    }
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt2 != -1) {
      return;
    }
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if (paramInt1 == 23)
    {
      localObject1 = paramIntent.getBundleExtra("extra_result_bundle");
      localObject2 = ((Bundle)localObject1).getParcelableArrayList("state_selection");
      this.mOriginalEnable = paramIntent.getBooleanExtra("extra_result_original_enable", false);
      paramInt1 = ((Bundle)localObject1).getInt("state_collection_type", 0);
      if (paramIntent.getBooleanExtra("extra_result_apply", false))
      {
        paramIntent = new Intent();
        localObject3 = new ArrayList();
        localObject1 = new ArrayList();
        if (localObject2 != null)
        {
          localObject2 = ((ArrayList)localObject2).iterator();
          while (((Iterator)localObject2).hasNext())
          {
            Item localItem = (Item)((Iterator)localObject2).next();
            ((ArrayList)localObject3).add(localItem.getContentUri());
            ((ArrayList)localObject1).add(PathUtils.getPath(this, localItem.getContentUri()));
          }
        }
        paramIntent.putParcelableArrayListExtra("extra_result_selection", (ArrayList)localObject3);
        paramIntent.putStringArrayListExtra("extra_result_selection_path", (ArrayList)localObject1);
        paramIntent.putExtra("extra_result_original_enable", this.mOriginalEnable);
        setResult(-1, paramIntent);
        finish();
      }
      else
      {
        this.mSelectedCollection.overwrite((ArrayList)localObject2, paramInt1);
        paramIntent = getSupportFragmentManager().findFragmentByTag(MediaSelectionFragment.class.getSimpleName());
        if ((paramIntent instanceof MediaSelectionFragment)) {
          ((MediaSelectionFragment)paramIntent).refreshMediaGrid();
        }
        updateBottomToolbar();
      }
    }
    else if (paramInt1 == 24)
    {
      paramIntent = this.mMediaStoreCompat.getCurrentPhotoUri();
      localObject3 = this.mMediaStoreCompat.getCurrentPhotoPath();
      localObject2 = new ArrayList();
      ((ArrayList)localObject2).add(paramIntent);
      localObject1 = new ArrayList();
      ((ArrayList)localObject1).add(localObject3);
      localObject3 = new Intent();
      ((Intent)localObject3).putParcelableArrayListExtra("extra_result_selection", (ArrayList)localObject2);
      ((Intent)localObject3).putStringArrayListExtra("extra_result_selection_path", (ArrayList)localObject1);
      setResult(-1, (Intent)localObject3);
      if (Build.VERSION.SDK_INT < 21) {
        revokeUriPermission(paramIntent, 3);
      }
      finish();
    }
  }
  
  public void onAlbumLoad(final Cursor paramCursor)
  {
    this.mAlbumsAdapter.swapCursor(paramCursor);
    new Handler(Looper.getMainLooper()).post(new Runnable()
    {
      public void run()
      {
        paramCursor.moveToPosition(MatisseActivity.this.mAlbumCollection.getCurrentSelection());
        AlbumsSpinner localAlbumsSpinner = MatisseActivity.this.mAlbumsSpinner;
        Object localObject = MatisseActivity.this;
        localAlbumsSpinner.setSelection((Context)localObject, ((MatisseActivity)localObject).mAlbumCollection.getCurrentSelection());
        localObject = Album.valueOf(paramCursor);
        if ((((Album)localObject).isAll()) && (SelectionSpec.getInstance().capture)) {
          ((Album)localObject).addCaptureCount();
        }
        MatisseActivity.this.onAlbumSelected((Album)localObject);
      }
    });
  }
  
  public void onAlbumReset()
  {
    this.mAlbumsAdapter.swapCursor(null);
  }
  
  public void onBackPressed()
  {
    setResult(0);
    super.onBackPressed();
  }
  
  public void onClick(View paramView)
  {
    if (paramView.getId() == R.id.button_preview)
    {
      paramView = new Intent(this, SelectedPreviewActivity.class);
      paramView.putExtra("extra_default_bundle", this.mSelectedCollection.getDataWithBundle());
      paramView.putExtra("extra_result_original_enable", this.mOriginalEnable);
      startActivityForResult(paramView, 23);
    }
    else if (paramView.getId() == R.id.button_apply)
    {
      paramView = new Intent();
      paramView.putParcelableArrayListExtra("extra_result_selection", (ArrayList)this.mSelectedCollection.asListOfUri());
      paramView.putStringArrayListExtra("extra_result_selection_path", (ArrayList)this.mSelectedCollection.asListOfString());
      paramView.putExtra("extra_result_original_enable", this.mOriginalEnable);
      setResult(-1, paramView);
      finish();
    }
    else if (paramView.getId() == R.id.originalLayout)
    {
      int i = countOverMaxSize();
      if (i > 0)
      {
        IncapableDialog.newInstance("", getString(R.string.error_over_original_count, new Object[] { Integer.valueOf(i), Integer.valueOf(this.mSpec.originalMaxSize) })).show(getSupportFragmentManager(), IncapableDialog.class.getName());
        return;
      }
      this.mOriginalEnable ^= true;
      this.mOriginal.setChecked(this.mOriginalEnable);
      if (this.mSpec.onCheckedListener != null) {
        this.mSpec.onCheckedListener.onCheck(this.mOriginalEnable);
      }
    }
  }
  
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    this.mSpec = SelectionSpec.getInstance();
    setTheme(this.mSpec.themeId);
    super.onCreate(paramBundle);
    if (!this.mSpec.hasInited)
    {
      setResult(0);
      finish();
      return;
    }
    setContentView(R.layout.activity_matisse);
    if (this.mSpec.needOrientationRestriction()) {
      setRequestedOrientation(this.mSpec.orientation);
    }
    if (this.mSpec.capture)
    {
      this.mMediaStoreCompat = new MediaStoreCompat(this);
      if (this.mSpec.captureStrategy != null) {
        this.mMediaStoreCompat.setCaptureStrategy(this.mSpec.captureStrategy);
      } else {
        throw new RuntimeException("Don't forget to set CaptureStrategy.");
      }
    }
    Object localObject2 = (Toolbar)findViewById(R.id.toolbar);
    setSupportActionBar((Toolbar)localObject2);
    Object localObject1 = getSupportActionBar();
    ((ActionBar)localObject1).setDisplayShowTitleEnabled(false);
    ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(true);
    localObject1 = ((Toolbar)localObject2).getNavigationIcon();
    localObject2 = getTheme().obtainStyledAttributes(new int[] { R.attr.album_element_color });
    int i = ((TypedArray)localObject2).getColor(0, 0);
    ((TypedArray)localObject2).recycle();
    ((Drawable)localObject1).setColorFilter(i, PorterDuff.Mode.SRC_IN);
    this.mButtonPreview = ((TextView)findViewById(R.id.button_preview));
    this.mButtonApply = ((TextView)findViewById(R.id.button_apply));
    this.mButtonPreview.setOnClickListener(this);
    this.mButtonApply.setOnClickListener(this);
    this.mContainer = findViewById(R.id.container);
    this.mEmptyView = findViewById(R.id.empty_view);
    this.mOriginalLayout = ((LinearLayout)findViewById(R.id.originalLayout));
    this.mOriginal = ((CheckRadioView)findViewById(R.id.original));
    this.mOriginalLayout.setOnClickListener(this);
    this.mSelectedCollection.onCreate(paramBundle);
    if (paramBundle != null) {
      this.mOriginalEnable = paramBundle.getBoolean("checkState");
    }
    updateBottomToolbar();
    this.mAlbumsAdapter = new AlbumsAdapter(this, null, false);
    this.mAlbumsSpinner = new AlbumsSpinner(this);
    this.mAlbumsSpinner.setOnItemSelectedListener(this);
    this.mAlbumsSpinner.setSelectedTextView((TextView)findViewById(R.id.selected_album));
    this.mAlbumsSpinner.setPopupAnchorView(findViewById(R.id.toolbar));
    this.mAlbumsSpinner.setAdapter(this.mAlbumsAdapter);
    this.mAlbumCollection.onCreate(this, this);
    this.mAlbumCollection.onRestoreInstanceState(paramBundle);
    this.mAlbumCollection.loadAlbums();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.mAlbumCollection.onDestroy();
    SelectionSpec localSelectionSpec = this.mSpec;
    localSelectionSpec.onCheckedListener = null;
    localSelectionSpec.onSelectedListener = null;
  }
  
  public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.mAlbumCollection.setStateCurrentSelection(paramInt);
    this.mAlbumsAdapter.getCursor().moveToPosition(paramInt);
    paramAdapterView = Album.valueOf(this.mAlbumsAdapter.getCursor());
    if ((paramAdapterView.isAll()) && (SelectionSpec.getInstance().capture)) {
      paramAdapterView.addCaptureCount();
    }
    onAlbumSelected(paramAdapterView);
  }
  
  public void onMediaClick(Album paramAlbum, Item paramItem, int paramInt)
  {
    Intent localIntent = new Intent(this, AlbumPreviewActivity.class);
    localIntent.putExtra("extra_album", paramAlbum);
    localIntent.putExtra("extra_item", paramItem);
    localIntent.putExtra("extra_default_bundle", this.mSelectedCollection.getDataWithBundle());
    localIntent.putExtra("extra_result_original_enable", this.mOriginalEnable);
    startActivityForResult(localIntent, 23);
  }
  
  public void onNothingSelected(AdapterView<?> paramAdapterView) {}
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
    {
      onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    this.mSelectedCollection.onSaveInstanceState(paramBundle);
    this.mAlbumCollection.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("checkState", this.mOriginalEnable);
  }
  
  public void onUpdate()
  {
    updateBottomToolbar();
    if (this.mSpec.onSelectedListener != null) {
      this.mSpec.onSelectedListener.onSelected(this.mSelectedCollection.asListOfUri(), this.mSelectedCollection.asListOfString());
    }
  }
  
  public SelectedItemCollection provideSelectedItemCollection()
  {
    return this.mSelectedCollection;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/ui/MatisseActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */