package com.zhihu.matisse.filter;

import android.content.Context;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import java.util.Iterator;
import java.util.Set;

public abstract class Filter
{
  public static final int K = 1024;
  public static final int MAX = Integer.MAX_VALUE;
  public static final int MIN = 0;
  
  protected abstract Set<MimeType> constraintTypes();
  
  public abstract IncapableCause filter(Context paramContext, Item paramItem);
  
  protected boolean needFiltering(Context paramContext, Item paramItem)
  {
    Iterator localIterator = constraintTypes().iterator();
    while (localIterator.hasNext()) {
      if (((MimeType)localIterator.next()).checkType(paramContext.getContentResolver(), paramItem.getContentUri())) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/filter/Filter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */