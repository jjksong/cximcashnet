package com.zhihu.matisse;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.app.Fragment;
import com.zhihu.matisse.engine.ImageEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.internal.entity.SelectionSpec;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnSelectedListener;
import com.zhihu.matisse.ui.MatisseActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class SelectionCreator
{
  private final Matisse mMatisse;
  private final SelectionSpec mSelectionSpec;
  
  SelectionCreator(Matisse paramMatisse, @NonNull Set<MimeType> paramSet, boolean paramBoolean)
  {
    this.mMatisse = paramMatisse;
    this.mSelectionSpec = SelectionSpec.getCleanInstance();
    paramMatisse = this.mSelectionSpec;
    paramMatisse.mimeTypeSet = paramSet;
    paramMatisse.mediaTypeExclusive = paramBoolean;
    paramMatisse.orientation = -1;
  }
  
  public SelectionCreator addFilter(@NonNull Filter paramFilter)
  {
    if (this.mSelectionSpec.filters == null) {
      this.mSelectionSpec.filters = new ArrayList();
    }
    if (paramFilter != null)
    {
      this.mSelectionSpec.filters.add(paramFilter);
      return this;
    }
    throw new IllegalArgumentException("filter cannot be null");
  }
  
  public SelectionCreator autoHideToolbarOnSingleTap(boolean paramBoolean)
  {
    this.mSelectionSpec.autoHideToobar = paramBoolean;
    return this;
  }
  
  public SelectionCreator capture(boolean paramBoolean)
  {
    this.mSelectionSpec.capture = paramBoolean;
    return this;
  }
  
  public SelectionCreator captureStrategy(CaptureStrategy paramCaptureStrategy)
  {
    this.mSelectionSpec.captureStrategy = paramCaptureStrategy;
    return this;
  }
  
  public SelectionCreator countable(boolean paramBoolean)
  {
    this.mSelectionSpec.countable = paramBoolean;
    return this;
  }
  
  public void forResult(int paramInt)
  {
    Activity localActivity = this.mMatisse.getActivity();
    if (localActivity == null) {
      return;
    }
    Intent localIntent = new Intent(localActivity, MatisseActivity.class);
    Fragment localFragment = this.mMatisse.getFragment();
    if (localFragment != null) {
      localFragment.startActivityForResult(localIntent, paramInt);
    } else {
      localActivity.startActivityForResult(localIntent, paramInt);
    }
  }
  
  public SelectionCreator gridExpectedSize(int paramInt)
  {
    this.mSelectionSpec.gridExpectedSize = paramInt;
    return this;
  }
  
  public SelectionCreator imageEngine(ImageEngine paramImageEngine)
  {
    this.mSelectionSpec.imageEngine = paramImageEngine;
    return this;
  }
  
  public SelectionCreator maxOriginalSize(int paramInt)
  {
    this.mSelectionSpec.originalMaxSize = paramInt;
    return this;
  }
  
  public SelectionCreator maxSelectable(int paramInt)
  {
    if (paramInt >= 1)
    {
      if ((this.mSelectionSpec.maxImageSelectable <= 0) && (this.mSelectionSpec.maxVideoSelectable <= 0))
      {
        this.mSelectionSpec.maxSelectable = paramInt;
        return this;
      }
      throw new IllegalStateException("already set maxImageSelectable and maxVideoSelectable");
    }
    throw new IllegalArgumentException("maxSelectable must be greater than or equal to one");
  }
  
  public SelectionCreator maxSelectablePerMediaType(int paramInt1, int paramInt2)
  {
    if ((paramInt1 >= 1) && (paramInt2 >= 1))
    {
      SelectionSpec localSelectionSpec = this.mSelectionSpec;
      localSelectionSpec.maxSelectable = -1;
      localSelectionSpec.maxImageSelectable = paramInt1;
      localSelectionSpec.maxVideoSelectable = paramInt2;
      return this;
    }
    throw new IllegalArgumentException("max selectable must be greater than or equal to one");
  }
  
  public SelectionCreator originalEnable(boolean paramBoolean)
  {
    this.mSelectionSpec.originalable = paramBoolean;
    return this;
  }
  
  public SelectionCreator restrictOrientation(int paramInt)
  {
    this.mSelectionSpec.orientation = paramInt;
    return this;
  }
  
  public SelectionCreator setOnCheckedListener(@Nullable OnCheckedListener paramOnCheckedListener)
  {
    this.mSelectionSpec.onCheckedListener = paramOnCheckedListener;
    return this;
  }
  
  @NonNull
  public SelectionCreator setOnSelectedListener(@Nullable OnSelectedListener paramOnSelectedListener)
  {
    this.mSelectionSpec.onSelectedListener = paramOnSelectedListener;
    return this;
  }
  
  public SelectionCreator showSingleMediaType(boolean paramBoolean)
  {
    this.mSelectionSpec.showSingleMediaType = paramBoolean;
    return this;
  }
  
  public SelectionCreator spanCount(int paramInt)
  {
    if (paramInt >= 1)
    {
      this.mSelectionSpec.spanCount = paramInt;
      return this;
    }
    throw new IllegalArgumentException("spanCount cannot be less than 1");
  }
  
  public SelectionCreator theme(@StyleRes int paramInt)
  {
    this.mSelectionSpec.themeId = paramInt;
    return this;
  }
  
  public SelectionCreator thumbnailScale(float paramFloat)
  {
    if ((paramFloat > 0.0F) && (paramFloat <= 1.0F))
    {
      this.mSelectionSpec.thumbnailScale = paramFloat;
      return this;
    }
    throw new IllegalArgumentException("Thumbnail scale must be between (0.0, 1.0]");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/SelectionCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */