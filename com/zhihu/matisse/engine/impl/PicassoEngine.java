package com.zhihu.matisse.engine.impl;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Priority;
import com.squareup.picasso.RequestCreator;
import com.zhihu.matisse.engine.ImageEngine;

public class PicassoEngine
  implements ImageEngine
{
  public void loadGifImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri)
  {
    loadImage(paramContext, paramInt1, paramInt2, paramImageView, paramUri);
  }
  
  public void loadGifThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri)
  {
    loadThumbnail(paramContext, paramInt, paramDrawable, paramImageView, paramUri);
  }
  
  public void loadImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri)
  {
    Picasso.with(paramContext).load(paramUri).resize(paramInt1, paramInt2).priority(Picasso.Priority.HIGH).centerInside().into(paramImageView);
  }
  
  public void loadThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri)
  {
    Picasso.with(paramContext).load(paramUri).placeholder(paramDrawable).resize(paramInt, paramInt).centerCrop().into(paramImageView);
  }
  
  public boolean supportAnimatedGif()
  {
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/engine/impl/PicassoEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */