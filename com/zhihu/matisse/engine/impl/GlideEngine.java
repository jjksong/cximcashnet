package com.zhihu.matisse.engine.impl;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.GifRequestBuilder;
import com.bumptech.glide.GifTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestManager;
import com.zhihu.matisse.engine.ImageEngine;

public class GlideEngine
  implements ImageEngine
{
  public void loadGifImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri)
  {
    Glide.with(paramContext).load(paramUri).asGif().override(paramInt1, paramInt2).priority(Priority.HIGH).into(paramImageView);
  }
  
  public void loadGifThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri)
  {
    Glide.with(paramContext).load(paramUri).asBitmap().placeholder(paramDrawable).override(paramInt, paramInt).centerCrop().into(paramImageView);
  }
  
  public void loadImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri)
  {
    Glide.with(paramContext).load(paramUri).override(paramInt1, paramInt2).priority(Priority.HIGH).fitCenter().into(paramImageView);
  }
  
  public void loadThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri)
  {
    Glide.with(paramContext).load(paramUri).asBitmap().placeholder(paramDrawable).override(paramInt, paramInt).centerCrop().into(paramImageView);
  }
  
  public boolean supportAnimatedGif()
  {
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/engine/impl/GlideEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */