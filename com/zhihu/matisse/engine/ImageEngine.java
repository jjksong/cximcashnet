package com.zhihu.matisse.engine;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

public abstract interface ImageEngine
{
  public abstract void loadGifImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri);
  
  public abstract void loadGifThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri);
  
  public abstract void loadImage(Context paramContext, int paramInt1, int paramInt2, ImageView paramImageView, Uri paramUri);
  
  public abstract void loadThumbnail(Context paramContext, int paramInt, Drawable paramDrawable, ImageView paramImageView, Uri paramUri);
  
  public abstract boolean supportAnimatedGif();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/zhihu/matisse/engine/ImageEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */