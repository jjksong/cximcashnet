package com.loopj.android.http;

import android.os.Looper;
import java.lang.ref.WeakReference;

public class RequestHandle
{
  private final WeakReference<AsyncHttpRequest> request;
  
  public RequestHandle(AsyncHttpRequest paramAsyncHttpRequest)
  {
    this.request = new WeakReference(paramAsyncHttpRequest);
  }
  
  public boolean cancel(final boolean paramBoolean)
  {
    final AsyncHttpRequest localAsyncHttpRequest = (AsyncHttpRequest)this.request.get();
    if (localAsyncHttpRequest != null)
    {
      if (Looper.myLooper() == Looper.getMainLooper())
      {
        new Thread(new Runnable()
        {
          public void run()
          {
            localAsyncHttpRequest.cancel(paramBoolean);
          }
        }).start();
        return true;
      }
      return localAsyncHttpRequest.cancel(paramBoolean);
    }
    return false;
  }
  
  public Object getTag()
  {
    Object localObject = (AsyncHttpRequest)this.request.get();
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((AsyncHttpRequest)localObject).getTag();
    }
    return localObject;
  }
  
  public boolean isCancelled()
  {
    AsyncHttpRequest localAsyncHttpRequest = (AsyncHttpRequest)this.request.get();
    boolean bool;
    if ((localAsyncHttpRequest != null) && (!localAsyncHttpRequest.isCancelled())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean isFinished()
  {
    AsyncHttpRequest localAsyncHttpRequest = (AsyncHttpRequest)this.request.get();
    boolean bool;
    if ((localAsyncHttpRequest != null) && (!localAsyncHttpRequest.isDone())) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public RequestHandle setTag(Object paramObject)
  {
    AsyncHttpRequest localAsyncHttpRequest = (AsyncHttpRequest)this.request.get();
    if (localAsyncHttpRequest != null) {
      localAsyncHttpRequest.setRequestTag(paramObject);
    }
    return this;
  }
  
  public boolean shouldBeGarbageCollected()
  {
    boolean bool;
    if ((!isCancelled()) && (!isFinished())) {
      bool = false;
    } else {
      bool = true;
    }
    if (bool) {
      this.request.clear();
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/RequestHandle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */