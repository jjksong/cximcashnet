package com.loopj.android.http;

import android.text.TextUtils;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonStreamerEntity
  implements HttpEntity
{
  private static final int BUFFER_SIZE = 4096;
  private static final UnsupportedOperationException ERR_UNSUPPORTED = new UnsupportedOperationException("Unsupported operation in this implementation.");
  private static final Header HEADER_GZIP_ENCODING = new BasicHeader("Content-Encoding", "gzip");
  private static final Header HEADER_JSON_CONTENT;
  private static final byte[] JSON_FALSE;
  private static final byte[] JSON_NULL;
  private static final byte[] JSON_TRUE = "true".getBytes();
  private static final String LOG_TAG = "JsonStreamerEntity";
  private static final byte[] STREAM_CONTENTS;
  private static final byte[] STREAM_NAME;
  private static final byte[] STREAM_TYPE;
  private final byte[] buffer = new byte['က'];
  private final Header contentEncoding;
  private final byte[] elapsedField;
  private final Map<String, Object> jsonParams = new HashMap();
  private final ResponseHandlerInterface progressHandler;
  
  static
  {
    JSON_FALSE = "false".getBytes();
    JSON_NULL = "null".getBytes();
    STREAM_NAME = escape("name");
    STREAM_TYPE = escape("type");
    STREAM_CONTENTS = escape("contents");
    HEADER_JSON_CONTENT = new BasicHeader("Content-Type", "application/json");
  }
  
  public JsonStreamerEntity(ResponseHandlerInterface paramResponseHandlerInterface, boolean paramBoolean, String paramString)
  {
    this.progressHandler = paramResponseHandlerInterface;
    Object localObject = null;
    if (paramBoolean) {
      paramResponseHandlerInterface = HEADER_GZIP_ENCODING;
    } else {
      paramResponseHandlerInterface = null;
    }
    this.contentEncoding = paramResponseHandlerInterface;
    if (TextUtils.isEmpty(paramString)) {
      paramResponseHandlerInterface = (ResponseHandlerInterface)localObject;
    } else {
      paramResponseHandlerInterface = escape(paramString);
    }
    this.elapsedField = paramResponseHandlerInterface;
  }
  
  private void endMetaData(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream.write(34);
  }
  
  static byte[] escape(String paramString)
  {
    if (paramString == null) {
      return JSON_NULL;
    }
    StringBuilder localStringBuilder = new StringBuilder(128);
    localStringBuilder.append('"');
    int k = paramString.length();
    int i = -1;
    for (;;)
    {
      int j = i + 1;
      if (j >= k) {
        break;
      }
      char c = paramString.charAt(j);
      if (c != '"')
      {
        if (c != '\\')
        {
          switch (c)
          {
          default: 
            switch (c)
            {
            default: 
              if ((c > '\037') && ((c < '') || (c > '')) && ((c < ' ') || (c > '⃿')))
              {
                localStringBuilder.append(c);
                i = j;
                continue;
              }
              String str = Integer.toHexString(c);
              localStringBuilder.append("\\u");
              int m = str.length();
              for (i = 0; i < 4 - m; i++) {
                localStringBuilder.append('0');
              }
              localStringBuilder.append(str.toUpperCase(Locale.US));
              i = j;
              break;
            case '\r': 
              localStringBuilder.append("\\r");
              i = j;
              break;
            case '\f': 
              localStringBuilder.append("\\f");
              i = j;
            }
            break;
          case '\n': 
            localStringBuilder.append("\\n");
            i = j;
            break;
          case '\t': 
            localStringBuilder.append("\\t");
            i = j;
            break;
          case '\b': 
            localStringBuilder.append("\\b");
            i = j;
            break;
          }
        }
        else
        {
          localStringBuilder.append("\\\\");
          i = j;
        }
      }
      else
      {
        localStringBuilder.append("\\\"");
        i = j;
      }
    }
    localStringBuilder.append('"');
    return localStringBuilder.toString().getBytes();
  }
  
  private void writeMetaData(OutputStream paramOutputStream, String paramString1, String paramString2)
    throws IOException
  {
    paramOutputStream.write(STREAM_NAME);
    paramOutputStream.write(58);
    paramOutputStream.write(escape(paramString1));
    paramOutputStream.write(44);
    paramOutputStream.write(STREAM_TYPE);
    paramOutputStream.write(58);
    paramOutputStream.write(escape(paramString2));
    paramOutputStream.write(44);
    paramOutputStream.write(STREAM_CONTENTS);
    paramOutputStream.write(58);
    paramOutputStream.write(34);
  }
  
  private void writeToFromFile(OutputStream paramOutputStream, RequestParams.FileWrapper paramFileWrapper)
    throws IOException
  {
    writeMetaData(paramOutputStream, paramFileWrapper.file.getName(), paramFileWrapper.contentType);
    long l2 = paramFileWrapper.file.length();
    paramFileWrapper = new FileInputStream(paramFileWrapper.file);
    Base64OutputStream localBase64OutputStream = new Base64OutputStream(paramOutputStream, 18);
    long l1 = 0L;
    for (;;)
    {
      int i = paramFileWrapper.read(this.buffer);
      if (i == -1) {
        break;
      }
      localBase64OutputStream.write(this.buffer, 0, i);
      l1 += i;
      this.progressHandler.sendProgressMessage(l1, l2);
    }
    AsyncHttpClient.silentCloseOutputStream(localBase64OutputStream);
    endMetaData(paramOutputStream);
    AsyncHttpClient.silentCloseInputStream(paramFileWrapper);
  }
  
  private void writeToFromStream(OutputStream paramOutputStream, RequestParams.StreamWrapper paramStreamWrapper)
    throws IOException
  {
    writeMetaData(paramOutputStream, paramStreamWrapper.name, paramStreamWrapper.contentType);
    Base64OutputStream localBase64OutputStream = new Base64OutputStream(paramOutputStream, 18);
    for (;;)
    {
      int i = paramStreamWrapper.inputStream.read(this.buffer);
      if (i == -1) {
        break;
      }
      localBase64OutputStream.write(this.buffer, 0, i);
    }
    AsyncHttpClient.silentCloseOutputStream(localBase64OutputStream);
    endMetaData(paramOutputStream);
    if (paramStreamWrapper.autoClose) {
      AsyncHttpClient.silentCloseInputStream(paramStreamWrapper.inputStream);
    }
  }
  
  public void addPart(String paramString, Object paramObject)
  {
    this.jsonParams.put(paramString, paramObject);
  }
  
  public void consumeContent()
    throws IOException, UnsupportedOperationException
  {}
  
  public InputStream getContent()
    throws IOException, UnsupportedOperationException
  {
    throw ERR_UNSUPPORTED;
  }
  
  public Header getContentEncoding()
  {
    return this.contentEncoding;
  }
  
  public long getContentLength()
  {
    return -1L;
  }
  
  public Header getContentType()
  {
    return HEADER_JSON_CONTENT;
  }
  
  public boolean isChunked()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    if (paramOutputStream != null)
    {
      long l = System.currentTimeMillis();
      Object localObject1 = paramOutputStream;
      if (this.contentEncoding != null) {
        localObject1 = new GZIPOutputStream(paramOutputStream, 4096);
      }
      ((OutputStream)localObject1).write(123);
      paramOutputStream = this.jsonParams.keySet();
      int k = paramOutputStream.size();
      if (k > 0)
      {
        int i = 0;
        Object localObject2 = paramOutputStream.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Object localObject3 = (String)((Iterator)localObject2).next();
          int j = i + 1;
          try
          {
            paramOutputStream = this.jsonParams.get(localObject3);
            ((OutputStream)localObject1).write(escape((String)localObject3));
            ((OutputStream)localObject1).write(58);
            if (paramOutputStream == null)
            {
              ((OutputStream)localObject1).write(JSON_NULL);
            }
            else
            {
              boolean bool = paramOutputStream instanceof RequestParams.FileWrapper;
              if ((!bool) && (!(paramOutputStream instanceof RequestParams.StreamWrapper)))
              {
                if ((paramOutputStream instanceof JsonValueInterface))
                {
                  ((OutputStream)localObject1).write(((JsonValueInterface)paramOutputStream).getEscapedJsonValue());
                }
                else if ((paramOutputStream instanceof JSONObject))
                {
                  ((OutputStream)localObject1).write(paramOutputStream.toString().getBytes());
                }
                else if ((paramOutputStream instanceof JSONArray))
                {
                  ((OutputStream)localObject1).write(paramOutputStream.toString().getBytes());
                }
                else if ((paramOutputStream instanceof Boolean))
                {
                  if (((Boolean)paramOutputStream).booleanValue()) {
                    paramOutputStream = JSON_TRUE;
                  } else {
                    paramOutputStream = JSON_FALSE;
                  }
                  ((OutputStream)localObject1).write(paramOutputStream);
                }
                else if ((paramOutputStream instanceof Long))
                {
                  localObject3 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject3).<init>();
                  ((StringBuilder)localObject3).append(((Number)paramOutputStream).longValue());
                  ((StringBuilder)localObject3).append("");
                  ((OutputStream)localObject1).write(((StringBuilder)localObject3).toString().getBytes());
                }
                else if ((paramOutputStream instanceof Double))
                {
                  localObject3 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject3).<init>();
                  ((StringBuilder)localObject3).append(((Number)paramOutputStream).doubleValue());
                  ((StringBuilder)localObject3).append("");
                  ((OutputStream)localObject1).write(((StringBuilder)localObject3).toString().getBytes());
                }
                else if ((paramOutputStream instanceof Float))
                {
                  localObject3 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject3).<init>();
                  ((StringBuilder)localObject3).append(((Number)paramOutputStream).floatValue());
                  ((StringBuilder)localObject3).append("");
                  ((OutputStream)localObject1).write(((StringBuilder)localObject3).toString().getBytes());
                }
                else if ((paramOutputStream instanceof Integer))
                {
                  localObject3 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject3).<init>();
                  ((StringBuilder)localObject3).append(((Number)paramOutputStream).intValue());
                  ((StringBuilder)localObject3).append("");
                  ((OutputStream)localObject1).write(((StringBuilder)localObject3).toString().getBytes());
                }
                else
                {
                  ((OutputStream)localObject1).write(escape(paramOutputStream.toString()));
                }
              }
              else
              {
                ((OutputStream)localObject1).write(123);
                if (bool) {
                  writeToFromFile((OutputStream)localObject1, (RequestParams.FileWrapper)paramOutputStream);
                } else {
                  writeToFromStream((OutputStream)localObject1, (RequestParams.StreamWrapper)paramOutputStream);
                }
                ((OutputStream)localObject1).write(125);
              }
            }
            if (this.elapsedField == null)
            {
              i = j;
              if (j >= k) {}
            }
            else
            {
              ((OutputStream)localObject1).write(44);
              i = j;
            }
          }
          finally
          {
            if ((this.elapsedField != null) || (j < k)) {
              ((OutputStream)localObject1).write(44);
            }
          }
        }
        l = System.currentTimeMillis() - l;
        paramOutputStream = this.elapsedField;
        if (paramOutputStream != null)
        {
          ((OutputStream)localObject1).write(paramOutputStream);
          ((OutputStream)localObject1).write(58);
          paramOutputStream = new StringBuilder();
          paramOutputStream.append(l);
          paramOutputStream.append("");
          ((OutputStream)localObject1).write(paramOutputStream.toString().getBytes());
        }
        paramOutputStream = AsyncHttpClient.log;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("Uploaded JSON in ");
        ((StringBuilder)localObject2).append(Math.floor(l / 1000L));
        ((StringBuilder)localObject2).append(" seconds");
        paramOutputStream.i("JsonStreamerEntity", ((StringBuilder)localObject2).toString());
      }
      ((OutputStream)localObject1).write(125);
      ((OutputStream)localObject1).flush();
      AsyncHttpClient.silentCloseOutputStream((OutputStream)localObject1);
      return;
    }
    throw new IllegalStateException("Output stream cannot be null.");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/JsonStreamerEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */