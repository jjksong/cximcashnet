package com.loopj.android.http;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.HttpResponseException;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class RangeFileAsyncHttpResponseHandler
  extends FileAsyncHttpResponseHandler
{
  private static final String LOG_TAG = "RangeFileAsyncHttpRH";
  private boolean append = false;
  private long current = 0L;
  
  public RangeFileAsyncHttpResponseHandler(File paramFile)
  {
    super(paramFile);
  }
  
  protected byte[] getResponseData(HttpEntity paramHttpEntity)
    throws IOException
  {
    if (paramHttpEntity != null)
    {
      InputStream localInputStream = paramHttpEntity.getContent();
      long l = paramHttpEntity.getContentLength() + this.current;
      paramHttpEntity = new FileOutputStream(getTargetFile(), this.append);
      if (localInputStream != null) {
        try
        {
          byte[] arrayOfByte = new byte['က'];
          while (this.current < l)
          {
            int i = localInputStream.read(arrayOfByte);
            if ((i == -1) || (Thread.currentThread().isInterrupted())) {
              break;
            }
            this.current += i;
            paramHttpEntity.write(arrayOfByte, 0, i);
            sendProgressMessage(this.current, l);
          }
        }
        finally
        {
          localInputStream.close();
          paramHttpEntity.flush();
          paramHttpEntity.close();
        }
      }
    }
    return null;
  }
  
  public void sendResponseMessage(HttpResponse paramHttpResponse)
    throws IOException
  {
    if (!Thread.currentThread().isInterrupted())
    {
      StatusLine localStatusLine = paramHttpResponse.getStatusLine();
      if (localStatusLine.getStatusCode() == 416)
      {
        if (!Thread.currentThread().isInterrupted()) {
          sendSuccessMessage(localStatusLine.getStatusCode(), paramHttpResponse.getAllHeaders(), null);
        }
      }
      else if (localStatusLine.getStatusCode() >= 300)
      {
        if (!Thread.currentThread().isInterrupted()) {
          sendFailureMessage(localStatusLine.getStatusCode(), paramHttpResponse.getAllHeaders(), null, new HttpResponseException(localStatusLine.getStatusCode(), localStatusLine.getReasonPhrase()));
        }
      }
      else if (!Thread.currentThread().isInterrupted())
      {
        Header localHeader = paramHttpResponse.getFirstHeader("Content-Range");
        if (localHeader == null)
        {
          this.append = false;
          this.current = 0L;
        }
        else
        {
          LogInterface localLogInterface = AsyncHttpClient.log;
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("Content-Range: ");
          localStringBuilder.append(localHeader.getValue());
          localLogInterface.v("RangeFileAsyncHttpRH", localStringBuilder.toString());
        }
        sendSuccessMessage(localStatusLine.getStatusCode(), paramHttpResponse.getAllHeaders(), getResponseData(paramHttpResponse.getEntity()));
      }
    }
  }
  
  public void updateRequestHeaders(HttpUriRequest paramHttpUriRequest)
  {
    if ((this.file.exists()) && (this.file.canWrite())) {
      this.current = this.file.length();
    }
    if (this.current > 0L)
    {
      this.append = true;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("bytes=");
      localStringBuilder.append(this.current);
      localStringBuilder.append("-");
      paramHttpUriRequest.setHeader("Range", localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/RangeFileAsyncHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */