package com.loopj.android.http;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonHttpResponseHandler
  extends TextHttpResponseHandler
{
  private static final String LOG_TAG = "JsonHttpRH";
  private boolean useRFC5179CompatibilityMode = true;
  
  public JsonHttpResponseHandler()
  {
    super("UTF-8");
  }
  
  public JsonHttpResponseHandler(String paramString)
  {
    super(paramString);
  }
  
  public JsonHttpResponseHandler(String paramString, boolean paramBoolean)
  {
    super(paramString);
    this.useRFC5179CompatibilityMode = paramBoolean;
  }
  
  public JsonHttpResponseHandler(boolean paramBoolean)
  {
    super("UTF-8");
    this.useRFC5179CompatibilityMode = paramBoolean;
  }
  
  public boolean isUseRFC5179CompatibilityMode()
  {
    return this.useRFC5179CompatibilityMode;
  }
  
  public void onFailure(int paramInt, Header[] paramArrayOfHeader, String paramString, Throwable paramThrowable)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onFailure(int, Header[], String, Throwable) was not overriden, but callback was received", paramThrowable);
  }
  
  public void onFailure(int paramInt, Header[] paramArrayOfHeader, Throwable paramThrowable, JSONArray paramJSONArray)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onFailure(int, Header[], Throwable, JSONArray) was not overriden, but callback was received", paramThrowable);
  }
  
  public void onFailure(int paramInt, Header[] paramArrayOfHeader, Throwable paramThrowable, JSONObject paramJSONObject)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onFailure(int, Header[], Throwable, JSONObject) was not overriden, but callback was received", paramThrowable);
  }
  
  public final void onFailure(final int paramInt, final Header[] paramArrayOfHeader, final byte[] paramArrayOfByte, final Throwable paramThrowable)
  {
    if (paramArrayOfByte != null)
    {
      paramArrayOfHeader = new Runnable()
      {
        public void run()
        {
          try
          {
            Object localObject = JsonHttpResponseHandler.this.parseResponse(paramArrayOfByte);
            JsonHttpResponseHandler localJsonHttpResponseHandler = JsonHttpResponseHandler.this;
            Runnable local1 = new com/loopj/android/http/JsonHttpResponseHandler$2$1;
            local1.<init>(this, localObject);
            localJsonHttpResponseHandler.postRunnable(local1);
          }
          catch (JSONException localJSONException)
          {
            JsonHttpResponseHandler.this.postRunnable(new Runnable()
            {
              public void run()
              {
                JsonHttpResponseHandler.this.onFailure(JsonHttpResponseHandler.2.this.val$statusCode, JsonHttpResponseHandler.2.this.val$headers, localJSONException, (JSONObject)null);
              }
            });
          }
        }
      };
      if ((!getUseSynchronousMode()) && (!getUsePoolThread())) {
        new Thread(paramArrayOfHeader).start();
      } else {
        paramArrayOfHeader.run();
      }
    }
    else
    {
      AsyncHttpClient.log.v("JsonHttpRH", "response body is null, calling onFailure(Throwable, JSONObject)");
      onFailure(paramInt, paramArrayOfHeader, paramThrowable, (JSONObject)null);
    }
  }
  
  public void onSuccess(int paramInt, Header[] paramArrayOfHeader, String paramString)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onSuccess(int, Header[], String) was not overriden, but callback was received");
  }
  
  public void onSuccess(int paramInt, Header[] paramArrayOfHeader, JSONArray paramJSONArray)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onSuccess(int, Header[], JSONArray) was not overriden, but callback was received");
  }
  
  public void onSuccess(int paramInt, Header[] paramArrayOfHeader, JSONObject paramJSONObject)
  {
    AsyncHttpClient.log.w("JsonHttpRH", "onSuccess(int, Header[], JSONObject) was not overriden, but callback was received");
  }
  
  public final void onSuccess(final int paramInt, final Header[] paramArrayOfHeader, final byte[] paramArrayOfByte)
  {
    if (paramInt != 204)
    {
      paramArrayOfHeader = new Runnable()
      {
        public void run()
        {
          try
          {
            Object localObject = JsonHttpResponseHandler.this.parseResponse(paramArrayOfByte);
            JsonHttpResponseHandler localJsonHttpResponseHandler = JsonHttpResponseHandler.this;
            Runnable local1 = new com/loopj/android/http/JsonHttpResponseHandler$1$1;
            local1.<init>(this, localObject);
            localJsonHttpResponseHandler.postRunnable(local1);
          }
          catch (JSONException localJSONException)
          {
            JsonHttpResponseHandler.this.postRunnable(new Runnable()
            {
              public void run()
              {
                JsonHttpResponseHandler.this.onFailure(JsonHttpResponseHandler.1.this.val$statusCode, JsonHttpResponseHandler.1.this.val$headers, localJSONException, (JSONObject)null);
              }
            });
          }
        }
      };
      if ((!getUseSynchronousMode()) && (!getUsePoolThread())) {
        new Thread(paramArrayOfHeader).start();
      } else {
        paramArrayOfHeader.run();
      }
    }
    else
    {
      onSuccess(paramInt, paramArrayOfHeader, new JSONObject());
    }
  }
  
  protected Object parseResponse(byte[] paramArrayOfByte)
    throws JSONException
  {
    Object localObject = null;
    if (paramArrayOfByte == null) {
      return null;
    }
    String str2 = getResponseString(paramArrayOfByte, getCharset());
    paramArrayOfByte = (byte[])localObject;
    String str1 = str2;
    if (str2 != null)
    {
      str2 = str2.trim();
      if (this.useRFC5179CompatibilityMode)
      {
        if (!str2.startsWith("{"))
        {
          paramArrayOfByte = (byte[])localObject;
          str1 = str2;
          if (!str2.startsWith("[")) {}
        }
        else
        {
          paramArrayOfByte = new JSONTokener(str2).nextValue();
          str1 = str2;
        }
      }
      else if (((str2.startsWith("{")) && (str2.endsWith("}"))) || ((str2.startsWith("[")) && (str2.endsWith("]"))))
      {
        paramArrayOfByte = new JSONTokener(str2).nextValue();
        str1 = str2;
      }
      else
      {
        paramArrayOfByte = (byte[])localObject;
        str1 = str2;
        if (str2.startsWith("\""))
        {
          paramArrayOfByte = (byte[])localObject;
          str1 = str2;
          if (str2.endsWith("\""))
          {
            paramArrayOfByte = str2.substring(1, str2.length() - 1);
            str1 = str2;
          }
        }
      }
    }
    if (paramArrayOfByte == null) {
      paramArrayOfByte = str1;
    }
    return paramArrayOfByte;
  }
  
  public void setUseRFC5179CompatibilityMode(boolean paramBoolean)
  {
    this.useRFC5179CompatibilityMode = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/JsonHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */