package com.loopj.android.http;

import android.content.Context;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public abstract class FileAsyncHttpResponseHandler
  extends AsyncHttpResponseHandler
{
  private static final String LOG_TAG = "FileAsyncHttpRH";
  protected final boolean append;
  protected final File file;
  protected File frontendFile;
  protected final boolean renameIfExists;
  
  public FileAsyncHttpResponseHandler(Context paramContext)
  {
    this.file = getTemporaryFile(paramContext);
    this.append = false;
    this.renameIfExists = false;
  }
  
  public FileAsyncHttpResponseHandler(File paramFile)
  {
    this(paramFile, false);
  }
  
  public FileAsyncHttpResponseHandler(File paramFile, boolean paramBoolean)
  {
    this(paramFile, paramBoolean, false);
  }
  
  public FileAsyncHttpResponseHandler(File paramFile, boolean paramBoolean1, boolean paramBoolean2)
  {
    this(paramFile, paramBoolean1, paramBoolean2, false);
  }
  
  public FileAsyncHttpResponseHandler(File paramFile, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    super(paramBoolean3);
    if (paramFile != null) {
      paramBoolean3 = true;
    } else {
      paramBoolean3 = false;
    }
    Utils.asserts(paramBoolean3, "File passed into FileAsyncHttpResponseHandler constructor must not be null");
    if ((!paramFile.isDirectory()) && (!paramFile.getParentFile().isDirectory())) {
      Utils.asserts(paramFile.getParentFile().mkdirs(), "Cannot create parent directories for requested File location");
    }
    if ((paramFile.isDirectory()) && (!paramFile.mkdirs())) {
      AsyncHttpClient.log.d("FileAsyncHttpRH", "Cannot create directories for requested Directory location, might not be a problem");
    }
    this.file = paramFile;
    this.append = paramBoolean1;
    this.renameIfExists = paramBoolean2;
  }
  
  public boolean deleteTargetFile()
  {
    boolean bool;
    if ((getTargetFile() != null) && (getTargetFile().delete())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected File getOriginalFile()
  {
    boolean bool;
    if (this.file != null) {
      bool = true;
    } else {
      bool = false;
    }
    Utils.asserts(bool, "Target file is null, fatal!");
    return this.file;
  }
  
  protected byte[] getResponseData(HttpEntity paramHttpEntity)
    throws IOException
  {
    if (paramHttpEntity != null)
    {
      InputStream localInputStream = paramHttpEntity.getContent();
      long l = paramHttpEntity.getContentLength();
      paramHttpEntity = new FileOutputStream(getTargetFile(), this.append);
      if (localInputStream != null) {
        try
        {
          byte[] arrayOfByte = new byte['က'];
          int i = 0;
          for (;;)
          {
            int j = localInputStream.read(arrayOfByte);
            if ((j == -1) || (Thread.currentThread().isInterrupted())) {
              break;
            }
            i += j;
            paramHttpEntity.write(arrayOfByte, 0, j);
            sendProgressMessage(i, l);
          }
        }
        finally
        {
          AsyncHttpClient.silentCloseInputStream(localInputStream);
          paramHttpEntity.flush();
          AsyncHttpClient.silentCloseOutputStream(paramHttpEntity);
        }
      }
    }
    return null;
  }
  
  public File getTargetFile()
  {
    if (this.frontendFile == null)
    {
      File localFile;
      if (getOriginalFile().isDirectory()) {
        localFile = getTargetFileByParsingURL();
      } else {
        localFile = getOriginalFile();
      }
      this.frontendFile = localFile;
    }
    return this.frontendFile;
  }
  
  protected File getTargetFileByParsingURL()
  {
    Utils.asserts(getOriginalFile().isDirectory(), "Target file is not a directory, cannot proceed");
    boolean bool;
    if (getRequestURI() != null) {
      bool = true;
    } else {
      bool = false;
    }
    Utils.asserts(bool, "RequestURI is null, cannot proceed");
    String str = getRequestURI().toString();
    str = str.substring(str.lastIndexOf('/') + 1, str.length());
    Object localObject = new File(getOriginalFile(), str);
    if ((((File)localObject).exists()) && (this.renameIfExists))
    {
      if (!str.contains("."))
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(" (%d)");
        str = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str.substring(0, str.lastIndexOf('.')));
        ((StringBuilder)localObject).append(" (%d)");
        ((StringBuilder)localObject).append(str.substring(str.lastIndexOf('.'), str.length()));
        str = ((StringBuilder)localObject).toString();
      }
      for (int i = 0;; i++)
      {
        localObject = new File(getOriginalFile(), String.format(str, new Object[] { Integer.valueOf(i) }));
        if (!((File)localObject).exists()) {
          return (File)localObject;
        }
      }
    }
    return (File)localObject;
  }
  
  protected File getTemporaryFile(Context paramContext)
  {
    boolean bool;
    if (paramContext != null) {
      bool = true;
    } else {
      bool = false;
    }
    Utils.asserts(bool, "Tried creating temporary file without having Context");
    try
    {
      paramContext = File.createTempFile("temp_", "_handled", paramContext.getCacheDir());
      return paramContext;
    }
    catch (IOException paramContext)
    {
      AsyncHttpClient.log.e("FileAsyncHttpRH", "Cannot create temporary file", paramContext);
    }
    return null;
  }
  
  public abstract void onFailure(int paramInt, Header[] paramArrayOfHeader, Throwable paramThrowable, File paramFile);
  
  public final void onFailure(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte, Throwable paramThrowable)
  {
    onFailure(paramInt, paramArrayOfHeader, paramThrowable, getTargetFile());
  }
  
  public abstract void onSuccess(int paramInt, Header[] paramArrayOfHeader, File paramFile);
  
  public final void onSuccess(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte)
  {
    onSuccess(paramInt, paramArrayOfHeader, getTargetFile());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/FileAsyncHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */