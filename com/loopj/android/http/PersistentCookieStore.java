package com.loopj.android.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class PersistentCookieStore
  implements CookieStore
{
  private static final String COOKIE_NAME_PREFIX = "cookie_";
  private static final String COOKIE_NAME_STORE = "names";
  private static final String COOKIE_PREFS = "CookiePrefsFile";
  private static final String LOG_TAG = "PersistentCookieStore";
  private final SharedPreferences cookiePrefs;
  private final ConcurrentHashMap<String, Cookie> cookies;
  private boolean omitNonPersistentCookies;
  
  public PersistentCookieStore(Context paramContext)
  {
    int i = 0;
    this.omitNonPersistentCookies = false;
    this.cookiePrefs = paramContext.getSharedPreferences("CookiePrefsFile", 0);
    this.cookies = new ConcurrentHashMap();
    paramContext = this.cookiePrefs.getString("names", null);
    if (paramContext != null)
    {
      paramContext = TextUtils.split(paramContext, ",");
      int j = paramContext.length;
      while (i < j)
      {
        String str = paramContext[i];
        Object localObject = this.cookiePrefs;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("cookie_");
        localStringBuilder.append(str);
        localObject = ((SharedPreferences)localObject).getString(localStringBuilder.toString(), null);
        if (localObject != null)
        {
          localObject = decodeCookie((String)localObject);
          if (localObject != null) {
            this.cookies.put(str, localObject);
          }
        }
        i++;
      }
      clearExpired(new Date());
    }
  }
  
  public void addCookie(Cookie paramCookie)
  {
    if ((this.omitNonPersistentCookies) && (!paramCookie.isPersistent())) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramCookie.getName());
    localStringBuilder.append(paramCookie.getDomain());
    String str = localStringBuilder.toString();
    if (!paramCookie.isExpired(new Date())) {
      this.cookies.put(str, paramCookie);
    } else {
      this.cookies.remove(str);
    }
    SharedPreferences.Editor localEditor = this.cookiePrefs.edit();
    localEditor.putString("names", TextUtils.join(",", this.cookies.keySet()));
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("cookie_");
    localStringBuilder.append(str);
    localEditor.putString(localStringBuilder.toString(), encodeCookie(new SerializableCookie(paramCookie)));
    localEditor.commit();
  }
  
  protected String byteArrayToHexString(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramArrayOfByte.length * 2);
    int j = paramArrayOfByte.length;
    for (int i = 0; i < j; i++)
    {
      int k = paramArrayOfByte[i] & 0xFF;
      if (k < 16) {
        localStringBuilder.append('0');
      }
      localStringBuilder.append(Integer.toHexString(k));
    }
    return localStringBuilder.toString().toUpperCase(Locale.US);
  }
  
  public void clear()
  {
    SharedPreferences.Editor localEditor = this.cookiePrefs.edit();
    Iterator localIterator = this.cookies.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("cookie_");
      localStringBuilder.append(str);
      localEditor.remove(localStringBuilder.toString());
    }
    localEditor.remove("names");
    localEditor.commit();
    this.cookies.clear();
  }
  
  public boolean clearExpired(Date paramDate)
  {
    SharedPreferences.Editor localEditor = this.cookiePrefs.edit();
    Iterator localIterator = this.cookies.entrySet().iterator();
    boolean bool = false;
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject).getKey();
      if (((Cookie)((Map.Entry)localObject).getValue()).isExpired(paramDate))
      {
        this.cookies.remove(str);
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("cookie_");
        ((StringBuilder)localObject).append(str);
        localEditor.remove(((StringBuilder)localObject).toString());
        bool = true;
      }
    }
    if (bool) {
      localEditor.putString("names", TextUtils.join(",", this.cookies.keySet()));
    }
    localEditor.commit();
    return bool;
  }
  
  protected Cookie decodeCookie(String paramString)
  {
    paramString = new ByteArrayInputStream(hexStringToByteArray(paramString));
    try
    {
      ObjectInputStream localObjectInputStream = new java/io/ObjectInputStream;
      localObjectInputStream.<init>(paramString);
      paramString = ((SerializableCookie)localObjectInputStream.readObject()).getCookie();
    }
    catch (ClassNotFoundException paramString)
    {
      AsyncHttpClient.log.d("PersistentCookieStore", "ClassNotFoundException in decodeCookie", paramString);
    }
    catch (IOException paramString)
    {
      AsyncHttpClient.log.d("PersistentCookieStore", "IOException in decodeCookie", paramString);
    }
    paramString = null;
    return paramString;
  }
  
  public void deleteCookie(Cookie paramCookie)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramCookie.getName());
    ((StringBuilder)localObject).append(paramCookie.getDomain());
    localObject = ((StringBuilder)localObject).toString();
    this.cookies.remove(localObject);
    SharedPreferences.Editor localEditor = this.cookiePrefs.edit();
    paramCookie = new StringBuilder();
    paramCookie.append("cookie_");
    paramCookie.append((String)localObject);
    localEditor.remove(paramCookie.toString());
    localEditor.commit();
  }
  
  protected String encodeCookie(SerializableCookie paramSerializableCookie)
  {
    if (paramSerializableCookie == null) {
      return null;
    }
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      ObjectOutputStream localObjectOutputStream = new java/io/ObjectOutputStream;
      localObjectOutputStream.<init>(localByteArrayOutputStream);
      localObjectOutputStream.writeObject(paramSerializableCookie);
      return byteArrayToHexString(localByteArrayOutputStream.toByteArray());
    }
    catch (IOException paramSerializableCookie)
    {
      AsyncHttpClient.log.d("PersistentCookieStore", "IOException in encodeCookie", paramSerializableCookie);
    }
    return null;
  }
  
  public List<Cookie> getCookies()
  {
    return new ArrayList(this.cookies.values());
  }
  
  protected byte[] hexStringToByteArray(String paramString)
  {
    int j = paramString.length();
    byte[] arrayOfByte = new byte[j / 2];
    for (int i = 0; i < j; i += 2) {
      arrayOfByte[(i / 2)] = ((byte)((Character.digit(paramString.charAt(i), 16) << 4) + Character.digit(paramString.charAt(i + 1), 16)));
    }
    return arrayOfByte;
  }
  
  public void setOmitNonPersistentCookies(boolean paramBoolean)
  {
    this.omitNonPersistentCookies = paramBoolean;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/PersistentCookieStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */