package com.loopj.android.http;

import android.content.Context;
import android.os.Looper;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.methods.HttpEntityEnclosingRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpHead;
import cz.msebera.android.httpclient.client.methods.HttpPatch;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.params.ConnManagerParams;
import cz.msebera.android.httpclient.conn.params.ConnPerRouteBean;
import cz.msebera.android.httpclient.conn.scheme.PlainSocketFactory;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;
import cz.msebera.android.httpclient.entity.HttpEntityWrapper;
import cz.msebera.android.httpclient.impl.auth.BasicScheme;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.impl.conn.tsccm.ThreadSafeClientConnManager;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.SyncBasicHttpContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

public class AsyncHttpClient
{
  public static final int DEFAULT_MAX_CONNECTIONS = 10;
  public static final int DEFAULT_MAX_RETRIES = 5;
  public static final int DEFAULT_RETRY_SLEEP_TIME_MILLIS = 1500;
  public static final int DEFAULT_SOCKET_BUFFER_SIZE = 8192;
  public static final int DEFAULT_SOCKET_TIMEOUT = 10000;
  public static final String ENCODING_GZIP = "gzip";
  public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
  public static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
  public static final String HEADER_CONTENT_ENCODING = "Content-Encoding";
  public static final String HEADER_CONTENT_RANGE = "Content-Range";
  public static final String HEADER_CONTENT_TYPE = "Content-Type";
  public static final String LOG_TAG = "AsyncHttpClient";
  public static LogInterface log = new LogHandler();
  private final Map<String, String> clientHeaderMap;
  private int connectTimeout = 10000;
  private final DefaultHttpClient httpClient;
  private final HttpContext httpContext;
  private boolean isUrlEncodingEnabled;
  private int maxConnections = 10;
  private final Map<Context, List<RequestHandle>> requestMap;
  private int responseTimeout = 10000;
  private ExecutorService threadPool;
  
  public AsyncHttpClient()
  {
    this(false, 80, 443);
  }
  
  public AsyncHttpClient(int paramInt)
  {
    this(false, paramInt, 443);
  }
  
  public AsyncHttpClient(int paramInt1, int paramInt2)
  {
    this(false, paramInt1, paramInt2);
  }
  
  public AsyncHttpClient(SchemeRegistry paramSchemeRegistry)
  {
    boolean bool = true;
    this.isUrlEncodingEnabled = true;
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    ConnManagerParams.setTimeout(localBasicHttpParams, this.connectTimeout);
    ConnManagerParams.setMaxConnectionsPerRoute(localBasicHttpParams, new ConnPerRouteBean(this.maxConnections));
    ConnManagerParams.setMaxTotalConnections(localBasicHttpParams, 10);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, this.responseTimeout);
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, this.connectTimeout);
    HttpConnectionParams.setTcpNoDelay(localBasicHttpParams, true);
    HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, 8192);
    HttpProtocolParams.setVersion(localBasicHttpParams, HttpVersion.HTTP_1_1);
    paramSchemeRegistry = createConnectionManager(paramSchemeRegistry, localBasicHttpParams);
    if (paramSchemeRegistry == null) {
      bool = false;
    }
    Utils.asserts(bool, "Custom implementation of #createConnectionManager(SchemeRegistry, BasicHttpParams) returned null");
    this.threadPool = getDefaultThreadPool();
    this.requestMap = Collections.synchronizedMap(new WeakHashMap());
    this.clientHeaderMap = new HashMap();
    this.httpContext = new SyncBasicHttpContext(new BasicHttpContext());
    this.httpClient = new DefaultHttpClient(paramSchemeRegistry, localBasicHttpParams);
    this.httpClient.addRequestInterceptor(new HttpRequestInterceptor()
    {
      public void process(HttpRequest paramAnonymousHttpRequest, HttpContext paramAnonymousHttpContext)
      {
        if (!paramAnonymousHttpRequest.containsHeader("Accept-Encoding")) {
          paramAnonymousHttpRequest.addHeader("Accept-Encoding", "gzip");
        }
        Iterator localIterator = AsyncHttpClient.this.clientHeaderMap.keySet().iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          if (paramAnonymousHttpRequest.containsHeader(str))
          {
            paramAnonymousHttpContext = paramAnonymousHttpRequest.getFirstHeader(str);
            AsyncHttpClient.log.d("AsyncHttpClient", String.format("Headers were overwritten! (%s | %s) overwrites (%s | %s)", new Object[] { str, AsyncHttpClient.this.clientHeaderMap.get(str), paramAnonymousHttpContext.getName(), paramAnonymousHttpContext.getValue() }));
            paramAnonymousHttpRequest.removeHeader(paramAnonymousHttpContext);
          }
          paramAnonymousHttpRequest.addHeader(str, (String)AsyncHttpClient.this.clientHeaderMap.get(str));
        }
      }
    });
    this.httpClient.addResponseInterceptor(new HttpResponseInterceptor()
    {
      public void process(HttpResponse paramAnonymousHttpResponse, HttpContext paramAnonymousHttpContext)
      {
        paramAnonymousHttpContext = paramAnonymousHttpResponse.getEntity();
        if (paramAnonymousHttpContext == null) {
          return;
        }
        Object localObject = paramAnonymousHttpContext.getContentEncoding();
        if (localObject != null)
        {
          localObject = ((Header)localObject).getElements();
          int j = localObject.length;
          for (int i = 0; i < j; i++) {
            if (localObject[i].getName().equalsIgnoreCase("gzip"))
            {
              paramAnonymousHttpResponse.setEntity(new AsyncHttpClient.InflatingEntity(paramAnonymousHttpContext));
              break;
            }
          }
        }
      }
    });
    this.httpClient.addRequestInterceptor(new HttpRequestInterceptor()
    {
      public void process(HttpRequest paramAnonymousHttpRequest, HttpContext paramAnonymousHttpContext)
        throws HttpException, IOException
      {
        paramAnonymousHttpRequest = (AuthState)paramAnonymousHttpContext.getAttribute("http.auth.target-scope");
        CredentialsProvider localCredentialsProvider = (CredentialsProvider)paramAnonymousHttpContext.getAttribute("http.auth.credentials-provider");
        paramAnonymousHttpContext = (HttpHost)paramAnonymousHttpContext.getAttribute("http.target_host");
        if (paramAnonymousHttpRequest.getAuthScheme() == null)
        {
          paramAnonymousHttpContext = localCredentialsProvider.getCredentials(new AuthScope(paramAnonymousHttpContext.getHostName(), paramAnonymousHttpContext.getPort()));
          if (paramAnonymousHttpContext != null)
          {
            paramAnonymousHttpRequest.setAuthScheme(new BasicScheme());
            paramAnonymousHttpRequest.setCredentials(paramAnonymousHttpContext);
          }
        }
      }
    }, 0);
    this.httpClient.setHttpRequestRetryHandler(new RetryHandler(5, 1500));
  }
  
  public AsyncHttpClient(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    this(getDefaultSchemeRegistry(paramBoolean, paramInt1, paramInt2));
  }
  
  private HttpEntityEnclosingRequestBase addEntityToRequestBase(HttpEntityEnclosingRequestBase paramHttpEntityEnclosingRequestBase, HttpEntity paramHttpEntity)
  {
    if (paramHttpEntity != null) {
      paramHttpEntityEnclosingRequestBase.setEntity(paramHttpEntity);
    }
    return paramHttpEntityEnclosingRequestBase;
  }
  
  public static void allowRetryExceptionClass(Class<?> paramClass)
  {
    if (paramClass != null) {
      RetryHandler.addClassToWhitelist(paramClass);
    }
  }
  
  public static void blockRetryExceptionClass(Class<?> paramClass)
  {
    if (paramClass != null) {
      RetryHandler.addClassToBlacklist(paramClass);
    }
  }
  
  private void cancelRequests(List<RequestHandle> paramList, boolean paramBoolean)
  {
    if (paramList != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext()) {
        ((RequestHandle)paramList.next()).cancel(paramBoolean);
      }
    }
  }
  
  public static void endEntityViaReflection(HttpEntity paramHttpEntity)
  {
    if ((paramHttpEntity instanceof HttpEntityWrapper))
    {
      Object localObject2 = null;
      try
      {
        Field[] arrayOfField = HttpEntityWrapper.class.getDeclaredFields();
        int j = arrayOfField.length;
        Object localObject1;
        for (int i = 0;; i++)
        {
          localObject1 = localObject2;
          if (i >= j) {
            break;
          }
          localObject1 = arrayOfField[i];
          if (((Field)localObject1).getName().equals("wrappedEntity")) {
            break;
          }
        }
        if (localObject1 != null)
        {
          ((Field)localObject1).setAccessible(true);
          paramHttpEntity = (HttpEntity)((Field)localObject1).get(paramHttpEntity);
          if (paramHttpEntity != null) {
            paramHttpEntity.consumeContent();
          }
        }
      }
      catch (Throwable paramHttpEntity)
      {
        log.e("AsyncHttpClient", "wrappedEntity consume", paramHttpEntity);
      }
    }
  }
  
  private static SchemeRegistry getDefaultSchemeRegistry(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    if (paramBoolean) {
      log.d("AsyncHttpClient", "Beware! Using the fix is insecure, as it doesn't verify SSL certificates.");
    }
    int i = paramInt1;
    if (paramInt1 < 1)
    {
      i = 80;
      log.d("AsyncHttpClient", "Invalid HTTP port number specified, defaulting to 80");
    }
    paramInt1 = paramInt2;
    if (paramInt2 < 1)
    {
      paramInt1 = 443;
      log.d("AsyncHttpClient", "Invalid HTTPS port number specified, defaulting to 443");
    }
    SSLSocketFactory localSSLSocketFactory;
    if (paramBoolean) {
      localSSLSocketFactory = MySSLSocketFactory.getFixedSocketFactory();
    } else {
      localSSLSocketFactory = SSLSocketFactory.getSocketFactory();
    }
    SchemeRegistry localSchemeRegistry = new SchemeRegistry();
    localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), i));
    localSchemeRegistry.register(new Scheme("https", localSSLSocketFactory, paramInt1));
    return localSchemeRegistry;
  }
  
  public static String getUrlWithQueryString(boolean paramBoolean, String paramString, RequestParams paramRequestParams)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject1 = paramString;
    Object localObject3;
    Object localObject2;
    if (paramBoolean) {
      try
      {
        localObject3 = URLDecoder.decode(paramString, "UTF-8");
        localObject1 = new java/net/URL;
        ((URL)localObject1).<init>((String)localObject3);
        localObject3 = new java/net/URI;
        ((URI)localObject3).<init>(((URL)localObject1).getProtocol(), ((URL)localObject1).getUserInfo(), ((URL)localObject1).getHost(), ((URL)localObject1).getPort(), ((URL)localObject1).getPath(), ((URL)localObject1).getQuery(), ((URL)localObject1).getRef());
        localObject1 = ((URI)localObject3).toASCIIString();
      }
      catch (Exception localException)
      {
        log.e("AsyncHttpClient", "getUrlWithQueryString encoding URL", localException);
        localObject2 = paramString;
      }
    }
    paramString = (String)localObject2;
    if (paramRequestParams != null)
    {
      paramRequestParams = paramRequestParams.getParamString().trim();
      paramString = (String)localObject2;
      if (!paramRequestParams.equals(""))
      {
        paramString = (String)localObject2;
        if (!paramRequestParams.equals("?"))
        {
          localObject3 = new StringBuilder();
          ((StringBuilder)localObject3).append((String)localObject2);
          if (((String)localObject2).contains("?")) {
            paramString = "&";
          } else {
            paramString = "?";
          }
          ((StringBuilder)localObject3).append(paramString);
          paramString = ((StringBuilder)localObject3).toString();
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append(paramString);
          ((StringBuilder)localObject2).append(paramRequestParams);
          paramString = ((StringBuilder)localObject2).toString();
        }
      }
    }
    return paramString;
  }
  
  /* Error */
  public static boolean isInputStreamGZIPCompressed(PushbackInputStream paramPushbackInputStream)
    throws IOException
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_0
    //   3: ifnonnull +5 -> 8
    //   6: iconst_0
    //   7: ireturn
    //   8: iconst_2
    //   9: newarray <illegal type>
    //   11: astore 4
    //   13: iconst_0
    //   14: istore_1
    //   15: iload_1
    //   16: iconst_2
    //   17: if_icmpge +48 -> 65
    //   20: aload_0
    //   21: aload 4
    //   23: iload_1
    //   24: iconst_2
    //   25: iload_1
    //   26: isub
    //   27: invokevirtual 445	java/io/PushbackInputStream:read	([BII)I
    //   30: istore_2
    //   31: iload_2
    //   32: ifge +13 -> 45
    //   35: aload_0
    //   36: aload 4
    //   38: iconst_0
    //   39: iload_1
    //   40: invokevirtual 449	java/io/PushbackInputStream:unread	([BII)V
    //   43: iconst_0
    //   44: ireturn
    //   45: iload_1
    //   46: iload_2
    //   47: iadd
    //   48: istore_1
    //   49: goto -34 -> 15
    //   52: astore 5
    //   54: aload_0
    //   55: aload 4
    //   57: iconst_0
    //   58: iload_1
    //   59: invokevirtual 449	java/io/PushbackInputStream:unread	([BII)V
    //   62: aload 5
    //   64: athrow
    //   65: aload_0
    //   66: aload 4
    //   68: iconst_0
    //   69: iload_1
    //   70: invokevirtual 449	java/io/PushbackInputStream:unread	([BII)V
    //   73: ldc_w 450
    //   76: aload 4
    //   78: iconst_0
    //   79: baload
    //   80: sipush 255
    //   83: iand
    //   84: aload 4
    //   86: iconst_1
    //   87: baload
    //   88: bipush 8
    //   90: ishl
    //   91: ldc_w 451
    //   94: iand
    //   95: ior
    //   96: if_icmpne +5 -> 101
    //   99: iconst_1
    //   100: istore_3
    //   101: iload_3
    //   102: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	paramPushbackInputStream	PushbackInputStream
    //   14	56	1	i	int
    //   30	18	2	j	int
    //   1	101	3	bool	boolean
    //   11	74	4	arrayOfByte	byte[]
    //   52	11	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   20	31	52	finally
  }
  
  private HttpEntity paramsToEntity(RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramRequestParams != null) {
      try
      {
        localObject1 = paramRequestParams.getEntity(paramResponseHandlerInterface);
      }
      catch (IOException paramRequestParams)
      {
        if (paramResponseHandlerInterface != null)
        {
          paramResponseHandlerInterface.sendFailureMessage(0, null, null, paramRequestParams);
          localObject1 = localObject2;
        }
        else
        {
          paramRequestParams.printStackTrace();
          localObject1 = localObject2;
        }
      }
    }
    return (HttpEntity)localObject1;
  }
  
  public static void silentCloseInputStream(InputStream paramInputStream)
  {
    if (paramInputStream != null) {
      try
      {
        paramInputStream.close();
      }
      catch (IOException paramInputStream)
      {
        log.w("AsyncHttpClient", "Cannot close input stream", paramInputStream);
      }
    }
  }
  
  public static void silentCloseOutputStream(OutputStream paramOutputStream)
  {
    if (paramOutputStream != null) {
      try
      {
        paramOutputStream.close();
      }
      catch (IOException paramOutputStream)
      {
        log.w("AsyncHttpClient", "Cannot close output stream", paramOutputStream);
      }
    }
  }
  
  public void addHeader(String paramString1, String paramString2)
  {
    this.clientHeaderMap.put(paramString1, paramString2);
  }
  
  public void cancelAllRequests(boolean paramBoolean)
  {
    Iterator localIterator = this.requestMap.values().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (List)localIterator.next();
      if (localObject != null)
      {
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          ((RequestHandle)((Iterator)localObject).next()).cancel(paramBoolean);
        }
      }
    }
    this.requestMap.clear();
  }
  
  public void cancelRequests(Context paramContext, final boolean paramBoolean)
  {
    if (paramContext == null)
    {
      log.e("AsyncHttpClient", "Passed null Context to cancelRequests");
      return;
    }
    final List localList = (List)this.requestMap.get(paramContext);
    this.requestMap.remove(paramContext);
    if (Looper.myLooper() == Looper.getMainLooper())
    {
      paramContext = new Runnable()
      {
        public void run()
        {
          AsyncHttpClient.this.cancelRequests(localList, paramBoolean);
        }
      };
      this.threadPool.submit(paramContext);
    }
    else
    {
      cancelRequests(localList, paramBoolean);
    }
  }
  
  public void cancelRequestsByTAG(Object paramObject, boolean paramBoolean)
  {
    if (paramObject == null)
    {
      log.d("AsyncHttpClient", "cancelRequestsByTAG, passed TAG is null, cannot proceed");
      return;
    }
    Iterator localIterator1 = this.requestMap.values().iterator();
    while (localIterator1.hasNext())
    {
      Object localObject = (List)localIterator1.next();
      if (localObject != null)
      {
        Iterator localIterator2 = ((List)localObject).iterator();
        while (localIterator2.hasNext())
        {
          localObject = (RequestHandle)localIterator2.next();
          if (paramObject.equals(((RequestHandle)localObject).getTag())) {
            ((RequestHandle)localObject).cancel(paramBoolean);
          }
        }
      }
    }
  }
  
  public void clearCredentialsProvider()
  {
    this.httpClient.getCredentialsProvider().clear();
  }
  
  protected ClientConnectionManager createConnectionManager(SchemeRegistry paramSchemeRegistry, BasicHttpParams paramBasicHttpParams)
  {
    return new ThreadSafeClientConnManager(paramBasicHttpParams, paramSchemeRegistry);
  }
  
  public RequestHandle delete(Context paramContext, String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString = new HttpDelete(getURI(paramString));
    return sendRequest(this.httpClient, this.httpContext, paramString, null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle delete(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, addEntityToRequestBase(new HttpDelete(URI.create(paramString1).normalize()), paramHttpEntity), paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle delete(Context paramContext, String paramString, Header[] paramArrayOfHeader, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString = new HttpDelete(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams));
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString, null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle delete(Context paramContext, String paramString, Header[] paramArrayOfHeader, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString = new HttpDelete(getURI(paramString));
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString, null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle delete(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return delete(null, paramString, paramResponseHandlerInterface);
  }
  
  public void delete(String paramString, RequestParams paramRequestParams, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    paramString = new HttpDelete(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams));
    sendRequest(this.httpClient, this.httpContext, paramString, null, paramAsyncHttpResponseHandler, null);
  }
  
  public RequestHandle get(Context paramContext, String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, new HttpGet(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams)), null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle get(Context paramContext, String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return get(paramContext, paramString, null, paramResponseHandlerInterface);
  }
  
  public RequestHandle get(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, addEntityToRequestBase(new HttpGet(URI.create(paramString1).normalize()), paramHttpEntity), paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle get(Context paramContext, String paramString, Header[] paramArrayOfHeader, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString = new HttpGet(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams));
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString, null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle get(String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return get(null, paramString, paramRequestParams, paramResponseHandlerInterface);
  }
  
  public RequestHandle get(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return get(null, paramString, null, paramResponseHandlerInterface);
  }
  
  public int getConnectTimeout()
  {
    return this.connectTimeout;
  }
  
  protected ExecutorService getDefaultThreadPool()
  {
    return Executors.newCachedThreadPool();
  }
  
  public HttpClient getHttpClient()
  {
    return this.httpClient;
  }
  
  public HttpContext getHttpContext()
  {
    return this.httpContext;
  }
  
  public LogInterface getLogInterface()
  {
    return log;
  }
  
  public int getLoggingLevel()
  {
    return log.getLoggingLevel();
  }
  
  public int getMaxConnections()
  {
    return this.maxConnections;
  }
  
  public int getResponseTimeout()
  {
    return this.responseTimeout;
  }
  
  public ExecutorService getThreadPool()
  {
    return this.threadPool;
  }
  
  protected URI getURI(String paramString)
  {
    return URI.create(paramString).normalize();
  }
  
  public RequestHandle head(Context paramContext, String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, new HttpHead(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams)), null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle head(Context paramContext, String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return head(paramContext, paramString, null, paramResponseHandlerInterface);
  }
  
  public RequestHandle head(Context paramContext, String paramString, Header[] paramArrayOfHeader, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString = new HttpHead(getUrlWithQueryString(this.isUrlEncodingEnabled, paramString, paramRequestParams));
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString, null, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle head(String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return head(null, paramString, paramRequestParams, paramResponseHandlerInterface);
  }
  
  public RequestHandle head(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return head(null, paramString, null, paramResponseHandlerInterface);
  }
  
  public boolean isLoggingEnabled()
  {
    return log.isLoggingEnabled();
  }
  
  public boolean isUrlEncodingEnabled()
  {
    return this.isUrlEncodingEnabled;
  }
  
  protected AsyncHttpRequest newAsyncHttpRequest(DefaultHttpClient paramDefaultHttpClient, HttpContext paramHttpContext, HttpUriRequest paramHttpUriRequest, String paramString, ResponseHandlerInterface paramResponseHandlerInterface, Context paramContext)
  {
    return new AsyncHttpRequest(paramDefaultHttpClient, paramHttpContext, paramHttpUriRequest, paramResponseHandlerInterface);
  }
  
  public RequestHandle patch(Context paramContext, String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return patch(paramContext, paramString, paramsToEntity(paramRequestParams, paramResponseHandlerInterface), null, paramResponseHandlerInterface);
  }
  
  public RequestHandle patch(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, addEntityToRequestBase(new HttpPatch(getURI(paramString1)), paramHttpEntity), paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle patch(Context paramContext, String paramString1, Header[] paramArrayOfHeader, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString1 = addEntityToRequestBase(new HttpPatch(getURI(paramString1)), paramHttpEntity);
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString1, paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle patch(String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return patch(null, paramString, paramRequestParams, paramResponseHandlerInterface);
  }
  
  public RequestHandle patch(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return patch(null, paramString, null, paramResponseHandlerInterface);
  }
  
  public RequestHandle post(Context paramContext, String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return post(paramContext, paramString, paramsToEntity(paramRequestParams, paramResponseHandlerInterface), null, paramResponseHandlerInterface);
  }
  
  public RequestHandle post(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, addEntityToRequestBase(new HttpPost(getURI(paramString1)), paramHttpEntity), paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle post(Context paramContext, String paramString1, Header[] paramArrayOfHeader, RequestParams paramRequestParams, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString1 = new HttpPost(getURI(paramString1));
    if (paramRequestParams != null) {
      paramString1.setEntity(paramsToEntity(paramRequestParams, paramResponseHandlerInterface));
    }
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString1, paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle post(Context paramContext, String paramString1, Header[] paramArrayOfHeader, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString1 = addEntityToRequestBase(new HttpPost(getURI(paramString1)), paramHttpEntity);
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString1, paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle post(String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return post(null, paramString, paramRequestParams, paramResponseHandlerInterface);
  }
  
  public RequestHandle post(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return post(null, paramString, null, paramResponseHandlerInterface);
  }
  
  public RequestHandle put(Context paramContext, String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return put(paramContext, paramString, paramsToEntity(paramRequestParams, paramResponseHandlerInterface), null, paramResponseHandlerInterface);
  }
  
  public RequestHandle put(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return sendRequest(this.httpClient, this.httpContext, addEntityToRequestBase(new HttpPut(getURI(paramString1)), paramHttpEntity), paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle put(Context paramContext, String paramString1, Header[] paramArrayOfHeader, HttpEntity paramHttpEntity, String paramString2, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    paramString1 = addEntityToRequestBase(new HttpPut(getURI(paramString1)), paramHttpEntity);
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    return sendRequest(this.httpClient, this.httpContext, paramString1, paramString2, paramResponseHandlerInterface, paramContext);
  }
  
  public RequestHandle put(String paramString, RequestParams paramRequestParams, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return put(null, paramString, paramRequestParams, paramResponseHandlerInterface);
  }
  
  public RequestHandle put(String paramString, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    return put(null, paramString, null, paramResponseHandlerInterface);
  }
  
  public void removeAllHeaders()
  {
    this.clientHeaderMap.clear();
  }
  
  public void removeHeader(String paramString)
  {
    this.clientHeaderMap.remove(paramString);
  }
  
  protected RequestHandle sendRequest(DefaultHttpClient paramDefaultHttpClient, HttpContext paramHttpContext, HttpUriRequest paramHttpUriRequest, String arg4, ResponseHandlerInterface paramResponseHandlerInterface, Context paramContext)
  {
    if (paramHttpUriRequest != null)
    {
      if (paramResponseHandlerInterface != null)
      {
        if ((paramResponseHandlerInterface.getUseSynchronousMode()) && (!paramResponseHandlerInterface.getUsePoolThread())) {
          throw new IllegalArgumentException("Synchronous ResponseHandler used in AsyncHttpClient. You should create your response handler in a looper thread or use SyncHttpClient instead.");
        }
        if (??? != null) {
          if (((paramHttpUriRequest instanceof HttpEntityEnclosingRequestBase)) && (((HttpEntityEnclosingRequestBase)paramHttpUriRequest).getEntity() != null) && (paramHttpUriRequest.containsHeader("Content-Type"))) {
            log.w("AsyncHttpClient", "Passed contentType will be ignored because HttpEntity sets content type");
          } else {
            paramHttpUriRequest.setHeader("Content-Type", ???);
          }
        }
        paramResponseHandlerInterface.setRequestHeaders(paramHttpUriRequest.getAllHeaders());
        paramResponseHandlerInterface.setRequestURI(paramHttpUriRequest.getURI());
        paramDefaultHttpClient = newAsyncHttpRequest(paramDefaultHttpClient, paramHttpContext, paramHttpUriRequest, ???, paramResponseHandlerInterface, paramContext);
        this.threadPool.submit(paramDefaultHttpClient);
        paramHttpUriRequest = new RequestHandle(paramDefaultHttpClient);
        if (paramContext != null) {
          synchronized (this.requestMap)
          {
            paramHttpContext = (List)this.requestMap.get(paramContext);
            paramDefaultHttpClient = paramHttpContext;
            if (paramHttpContext == null)
            {
              paramDefaultHttpClient = new java/util/LinkedList;
              paramDefaultHttpClient.<init>();
              paramDefaultHttpClient = Collections.synchronizedList(paramDefaultHttpClient);
              this.requestMap.put(paramContext, paramDefaultHttpClient);
            }
            paramDefaultHttpClient.add(paramHttpUriRequest);
            paramDefaultHttpClient = paramDefaultHttpClient.iterator();
            while (paramDefaultHttpClient.hasNext()) {
              if (((RequestHandle)paramDefaultHttpClient.next()).shouldBeGarbageCollected()) {
                paramDefaultHttpClient.remove();
              }
            }
          }
        }
        return paramHttpUriRequest;
      }
      throw new IllegalArgumentException("ResponseHandler must not be null");
    }
    throw new IllegalArgumentException("HttpUriRequest must not be null");
  }
  
  public void setAuthenticationPreemptive(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.httpClient.addRequestInterceptor(new PreemptiveAuthorizationHttpRequestInterceptor(), 0);
    } else {
      this.httpClient.removeRequestInterceptorByClass(PreemptiveAuthorizationHttpRequestInterceptor.class);
    }
  }
  
  public void setBasicAuth(String paramString1, String paramString2)
  {
    setBasicAuth(paramString1, paramString2, false);
  }
  
  public void setBasicAuth(String paramString1, String paramString2, AuthScope paramAuthScope)
  {
    setBasicAuth(paramString1, paramString2, paramAuthScope, false);
  }
  
  public void setBasicAuth(String paramString1, String paramString2, AuthScope paramAuthScope, boolean paramBoolean)
  {
    setCredentials(paramAuthScope, new UsernamePasswordCredentials(paramString1, paramString2));
    setAuthenticationPreemptive(paramBoolean);
  }
  
  public void setBasicAuth(String paramString1, String paramString2, boolean paramBoolean)
  {
    setBasicAuth(paramString1, paramString2, null, paramBoolean);
  }
  
  public void setConnectTimeout(int paramInt)
  {
    int i = paramInt;
    if (paramInt < 1000) {
      i = 10000;
    }
    this.connectTimeout = i;
    HttpParams localHttpParams = this.httpClient.getParams();
    ConnManagerParams.setTimeout(localHttpParams, this.connectTimeout);
    HttpConnectionParams.setConnectionTimeout(localHttpParams, this.connectTimeout);
  }
  
  public void setCookieStore(CookieStore paramCookieStore)
  {
    this.httpContext.setAttribute("http.cookie-store", paramCookieStore);
  }
  
  public void setCredentials(AuthScope paramAuthScope, Credentials paramCredentials)
  {
    if (paramCredentials == null)
    {
      log.d("AsyncHttpClient", "Provided credentials are null, not setting");
      return;
    }
    CredentialsProvider localCredentialsProvider = this.httpClient.getCredentialsProvider();
    AuthScope localAuthScope = paramAuthScope;
    if (paramAuthScope == null) {
      localAuthScope = AuthScope.ANY;
    }
    localCredentialsProvider.setCredentials(localAuthScope, paramCredentials);
  }
  
  public void setEnableRedirects(boolean paramBoolean)
  {
    setEnableRedirects(paramBoolean, paramBoolean, paramBoolean);
  }
  
  public void setEnableRedirects(boolean paramBoolean1, boolean paramBoolean2)
  {
    setEnableRedirects(paramBoolean1, paramBoolean2, true);
  }
  
  public void setEnableRedirects(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.httpClient.getParams().setBooleanParameter("http.protocol.reject-relative-redirect", paramBoolean2 ^ true);
    this.httpClient.getParams().setBooleanParameter("http.protocol.allow-circular-redirects", paramBoolean3);
    this.httpClient.setRedirectHandler(new MyRedirectHandler(paramBoolean1));
  }
  
  public void setLogInterface(LogInterface paramLogInterface)
  {
    if (paramLogInterface != null) {
      log = paramLogInterface;
    }
  }
  
  public void setLoggingEnabled(boolean paramBoolean)
  {
    log.setLoggingEnabled(paramBoolean);
  }
  
  public void setLoggingLevel(int paramInt)
  {
    log.setLoggingLevel(paramInt);
  }
  
  public void setMaxConnections(int paramInt)
  {
    int i = paramInt;
    if (paramInt < 1) {
      i = 10;
    }
    this.maxConnections = i;
    ConnManagerParams.setMaxConnectionsPerRoute(this.httpClient.getParams(), new ConnPerRouteBean(this.maxConnections));
  }
  
  public void setMaxRetriesAndTimeout(int paramInt1, int paramInt2)
  {
    this.httpClient.setHttpRequestRetryHandler(new RetryHandler(paramInt1, paramInt2));
  }
  
  public void setProxy(String paramString, int paramInt)
  {
    paramString = new HttpHost(paramString, paramInt);
    this.httpClient.getParams().setParameter("http.route.default-proxy", paramString);
  }
  
  public void setProxy(String paramString1, int paramInt, String paramString2, String paramString3)
  {
    this.httpClient.getCredentialsProvider().setCredentials(new AuthScope(paramString1, paramInt), new UsernamePasswordCredentials(paramString2, paramString3));
    paramString1 = new HttpHost(paramString1, paramInt);
    this.httpClient.getParams().setParameter("http.route.default-proxy", paramString1);
  }
  
  public void setRedirectHandler(RedirectHandler paramRedirectHandler)
  {
    this.httpClient.setRedirectHandler(paramRedirectHandler);
  }
  
  public void setResponseTimeout(int paramInt)
  {
    int i = paramInt;
    if (paramInt < 1000) {
      i = 10000;
    }
    this.responseTimeout = i;
    HttpConnectionParams.setSoTimeout(this.httpClient.getParams(), this.responseTimeout);
  }
  
  public void setSSLSocketFactory(SSLSocketFactory paramSSLSocketFactory)
  {
    this.httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", paramSSLSocketFactory, 443));
  }
  
  public void setThreadPool(ExecutorService paramExecutorService)
  {
    this.threadPool = paramExecutorService;
  }
  
  public void setTimeout(int paramInt)
  {
    int i = paramInt;
    if (paramInt < 1000) {
      i = 10000;
    }
    setConnectTimeout(i);
    setResponseTimeout(i);
  }
  
  public void setURLEncodingEnabled(boolean paramBoolean)
  {
    this.isUrlEncodingEnabled = paramBoolean;
  }
  
  public void setUserAgent(String paramString)
  {
    HttpProtocolParams.setUserAgent(this.httpClient.getParams(), paramString);
  }
  
  private static class InflatingEntity
    extends HttpEntityWrapper
  {
    GZIPInputStream gzippedStream;
    PushbackInputStream pushbackStream;
    InputStream wrappedStream;
    
    public InflatingEntity(HttpEntity paramHttpEntity)
    {
      super();
    }
    
    public void consumeContent()
      throws IOException
    {
      AsyncHttpClient.silentCloseInputStream(this.wrappedStream);
      AsyncHttpClient.silentCloseInputStream(this.pushbackStream);
      AsyncHttpClient.silentCloseInputStream(this.gzippedStream);
      super.consumeContent();
    }
    
    public InputStream getContent()
      throws IOException
    {
      this.wrappedStream = this.wrappedEntity.getContent();
      this.pushbackStream = new PushbackInputStream(this.wrappedStream, 2);
      if (AsyncHttpClient.isInputStreamGZIPCompressed(this.pushbackStream))
      {
        this.gzippedStream = new GZIPInputStream(this.pushbackStream);
        return this.gzippedStream;
      }
      return this.pushbackStream;
    }
    
    public long getContentLength()
    {
      long l;
      if (this.wrappedEntity == null) {
        l = 0L;
      } else {
        l = this.wrappedEntity.getContentLength();
      }
      return l;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/AsyncHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */