package com.loopj.android.http;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.CircularRedirectException;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.impl.client.DefaultRedirectHandler;
import cz.msebera.android.httpclient.impl.client.RedirectLocations;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.net.URI;
import java.net.URISyntaxException;

class MyRedirectHandler
  extends DefaultRedirectHandler
{
  private static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  private final boolean enableRedirects;
  
  public MyRedirectHandler(boolean paramBoolean)
  {
    this.enableRedirects = paramBoolean;
  }
  
  public URI getLocationURI(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    if (paramHttpResponse != null)
    {
      Object localObject1 = paramHttpResponse.getFirstHeader("location");
      if (localObject1 != null)
      {
        Object localObject2 = ((Header)localObject1).getValue().replaceAll(" ", "%20");
        try
        {
          localObject1 = new URI((String)localObject2);
          localObject2 = paramHttpResponse.getParams();
          paramHttpResponse = (HttpResponse)localObject1;
          if (!((URI)localObject1).isAbsolute()) {
            if (!((HttpParams)localObject2).isParameterTrue("http.protocol.reject-relative-redirect"))
            {
              HttpHost localHttpHost = (HttpHost)paramHttpContext.getAttribute("http.target_host");
              if (localHttpHost != null)
              {
                paramHttpResponse = (HttpRequest)paramHttpContext.getAttribute("http.request");
                try
                {
                  URI localURI = new java/net/URI;
                  localURI.<init>(paramHttpResponse.getRequestLine().getUri());
                  paramHttpResponse = URIUtils.resolve(URIUtils.rewriteURI(localURI, localHttpHost, true), (URI)localObject1);
                }
                catch (URISyntaxException paramHttpResponse)
                {
                  throw new ProtocolException(paramHttpResponse.getMessage(), paramHttpResponse);
                }
              }
              else
              {
                throw new IllegalStateException("Target host not available in the HTTP context");
              }
            }
            else
            {
              paramHttpResponse = new StringBuilder();
              paramHttpResponse.append("Relative redirect location '");
              paramHttpResponse.append(localObject1);
              paramHttpResponse.append("' not allowed");
              throw new ProtocolException(paramHttpResponse.toString());
            }
          }
          if (((HttpParams)localObject2).isParameterFalse("http.protocol.allow-circular-redirects"))
          {
            localObject2 = (RedirectLocations)paramHttpContext.getAttribute("http.protocol.redirect-locations");
            localObject1 = localObject2;
            if (localObject2 == null)
            {
              localObject1 = new RedirectLocations();
              paramHttpContext.setAttribute("http.protocol.redirect-locations", localObject1);
            }
            if (paramHttpResponse.getFragment() != null) {
              try
              {
                paramHttpContext = new cz/msebera/android/httpclient/HttpHost;
                paramHttpContext.<init>(paramHttpResponse.getHost(), paramHttpResponse.getPort(), paramHttpResponse.getScheme());
                paramHttpContext = URIUtils.rewriteURI(paramHttpResponse, paramHttpContext, true);
              }
              catch (URISyntaxException paramHttpResponse)
              {
                throw new ProtocolException(paramHttpResponse.getMessage(), paramHttpResponse);
              }
            } else {
              paramHttpContext = paramHttpResponse;
            }
            if (!((RedirectLocations)localObject1).contains(paramHttpContext))
            {
              ((RedirectLocations)localObject1).add(paramHttpContext);
            }
            else
            {
              paramHttpResponse = new StringBuilder();
              paramHttpResponse.append("Circular redirect to '");
              paramHttpResponse.append(paramHttpContext);
              paramHttpResponse.append("'");
              throw new CircularRedirectException(paramHttpResponse.toString());
            }
          }
          return paramHttpResponse;
        }
        catch (URISyntaxException paramHttpResponse)
        {
          paramHttpContext = new StringBuilder();
          paramHttpContext.append("Invalid redirect URI: ");
          paramHttpContext.append((String)localObject2);
          throw new ProtocolException(paramHttpContext.toString(), paramHttpResponse);
        }
      }
      paramHttpContext = new StringBuilder();
      paramHttpContext.append("Received redirect response ");
      paramHttpContext.append(paramHttpResponse.getStatusLine());
      paramHttpContext.append(" but no location header");
      throw new ProtocolException(paramHttpContext.toString());
    }
    throw new IllegalArgumentException("HTTP response may not be null");
  }
  
  public boolean isRedirectRequested(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    if (!this.enableRedirects) {
      return false;
    }
    if (paramHttpResponse != null)
    {
      int i = paramHttpResponse.getStatusLine().getStatusCode();
      if (i != 307) {
        switch (i)
        {
        default: 
          return false;
        }
      }
      return true;
    }
    throw new IllegalArgumentException("HTTP response may not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/MyRedirectHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */