package com.loopj.android.http;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.utils.URLEncodedUtils;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RequestParams
  implements Serializable
{
  public static final String APPLICATION_JSON = "application/json";
  public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
  protected static final String LOG_TAG = "RequestParams";
  protected boolean autoCloseInputStreams;
  protected String contentEncoding;
  protected String elapsedFieldInJsonStreamer;
  protected final ConcurrentHashMap<String, List<FileWrapper>> fileArrayParams = new ConcurrentHashMap();
  protected final ConcurrentHashMap<String, FileWrapper> fileParams = new ConcurrentHashMap();
  protected boolean forceMultipartEntity;
  protected boolean isRepeatable;
  protected final ConcurrentHashMap<String, StreamWrapper> streamParams = new ConcurrentHashMap();
  protected final ConcurrentHashMap<String, String> urlParams = new ConcurrentHashMap();
  protected final ConcurrentHashMap<String, Object> urlParamsWithObjects = new ConcurrentHashMap();
  protected boolean useJsonStreamer;
  
  public RequestParams()
  {
    this((Map)null);
  }
  
  public RequestParams(String paramString1, final String paramString2)
  {
    this(new HashMap() {});
  }
  
  public RequestParams(Map<String, String> paramMap)
  {
    this.forceMultipartEntity = false;
    this.elapsedFieldInJsonStreamer = "_elapsed";
    this.contentEncoding = "UTF-8";
    if (paramMap != null)
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        put((String)localEntry.getKey(), (String)localEntry.getValue());
      }
    }
  }
  
  public RequestParams(Object... paramVarArgs)
  {
    int i = 0;
    this.forceMultipartEntity = false;
    this.elapsedFieldInJsonStreamer = "_elapsed";
    this.contentEncoding = "UTF-8";
    int j = paramVarArgs.length;
    if (j % 2 == 0)
    {
      while (i < j)
      {
        put(String.valueOf(paramVarArgs[i]), String.valueOf(paramVarArgs[(i + 1)]));
        i += 2;
      }
      return;
    }
    throw new IllegalArgumentException("Supplied arguments must be even");
  }
  
  private HttpEntity createFormEntity()
  {
    try
    {
      UrlEncodedFormEntity localUrlEncodedFormEntity = new UrlEncodedFormEntity(getParamsList(), this.contentEncoding);
      return localUrlEncodedFormEntity;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      AsyncHttpClient.log.e("RequestParams", "createFormEntity failed", localUnsupportedEncodingException);
    }
    return null;
  }
  
  private HttpEntity createJsonStreamerEntity(ResponseHandlerInterface paramResponseHandlerInterface)
    throws IOException
  {
    boolean bool;
    if ((this.fileParams.isEmpty()) && (this.streamParams.isEmpty())) {
      bool = false;
    } else {
      bool = true;
    }
    paramResponseHandlerInterface = new JsonStreamerEntity(paramResponseHandlerInterface, bool, this.elapsedFieldInJsonStreamer);
    Object localObject1 = this.urlParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject2).getKey(), ((Map.Entry)localObject2).getValue());
    }
    Object localObject2 = this.urlParamsWithObjects.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject1).getKey(), ((Map.Entry)localObject1).getValue());
    }
    localObject1 = this.fileParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject2).getKey(), ((Map.Entry)localObject2).getValue());
    }
    localObject1 = this.streamParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      StreamWrapper localStreamWrapper = (StreamWrapper)((Map.Entry)localObject2).getValue();
      if (localStreamWrapper.inputStream != null) {
        paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject2).getKey(), StreamWrapper.newInstance(localStreamWrapper.inputStream, localStreamWrapper.name, localStreamWrapper.contentType, localStreamWrapper.autoClose));
      }
    }
    return paramResponseHandlerInterface;
  }
  
  private HttpEntity createMultipartEntity(ResponseHandlerInterface paramResponseHandlerInterface)
    throws IOException
  {
    paramResponseHandlerInterface = new SimpleMultipartEntity(paramResponseHandlerInterface);
    paramResponseHandlerInterface.setIsRepeatable(this.isRepeatable);
    Object localObject1 = this.urlParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      paramResponseHandlerInterface.addPartWithCharset((String)((Map.Entry)localObject2).getKey(), (String)((Map.Entry)localObject2).getValue(), this.contentEncoding);
    }
    localObject1 = getParamsList(null, this.urlParamsWithObjects).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (BasicNameValuePair)((Iterator)localObject1).next();
      paramResponseHandlerInterface.addPartWithCharset(((BasicNameValuePair)localObject2).getName(), ((BasicNameValuePair)localObject2).getValue(), this.contentEncoding);
    }
    Object localObject3 = this.streamParams.entrySet().iterator();
    while (((Iterator)localObject3).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject3).next();
      localObject2 = (StreamWrapper)((Map.Entry)localObject1).getValue();
      if (((StreamWrapper)localObject2).inputStream != null) {
        paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject1).getKey(), ((StreamWrapper)localObject2).name, ((StreamWrapper)localObject2).inputStream, ((StreamWrapper)localObject2).contentType);
      }
    }
    Object localObject2 = this.fileParams.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      localObject3 = (FileWrapper)((Map.Entry)localObject1).getValue();
      paramResponseHandlerInterface.addPart((String)((Map.Entry)localObject1).getKey(), ((FileWrapper)localObject3).file, ((FileWrapper)localObject3).contentType, ((FileWrapper)localObject3).customFileName);
    }
    localObject1 = this.fileArrayParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = ((List)localEntry.getValue()).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (FileWrapper)((Iterator)localObject2).next();
        paramResponseHandlerInterface.addPart((String)localEntry.getKey(), ((FileWrapper)localObject3).file, ((FileWrapper)localObject3).contentType, ((FileWrapper)localObject3).customFileName);
      }
    }
    return paramResponseHandlerInterface;
  }
  
  private List<BasicNameValuePair> getParamsList(String paramString, Object paramObject)
  {
    LinkedList localLinkedList = new LinkedList();
    if ((paramObject instanceof Map))
    {
      Map localMap = (Map)paramObject;
      paramObject = new ArrayList(localMap.keySet());
      if ((((List)paramObject).size() > 0) && ((((List)paramObject).get(0) instanceof Comparable))) {
        Collections.sort((List)paramObject);
      }
      Iterator localIterator = ((List)paramObject).iterator();
      while (localIterator.hasNext())
      {
        paramObject = localIterator.next();
        if ((paramObject instanceof String))
        {
          Object localObject = localMap.get(paramObject);
          if (localObject != null)
          {
            if (paramString == null) {
              paramObject = (String)paramObject;
            } else {
              paramObject = String.format(Locale.US, "%s[%s]", new Object[] { paramString, paramObject });
            }
            localLinkedList.addAll(getParamsList((String)paramObject, localObject));
          }
        }
      }
    }
    int j;
    int i;
    if ((paramObject instanceof List))
    {
      paramObject = (List)paramObject;
      j = ((List)paramObject).size();
      for (i = 0; i < j; i++) {
        localLinkedList.addAll(getParamsList(String.format(Locale.US, "%s[%d]", new Object[] { paramString, Integer.valueOf(i) }), ((List)paramObject).get(i)));
      }
    }
    if ((paramObject instanceof Object[]))
    {
      paramObject = (Object[])paramObject;
      j = paramObject.length;
      for (i = 0; i < j; i++) {
        localLinkedList.addAll(getParamsList(String.format(Locale.US, "%s[%d]", new Object[] { paramString, Integer.valueOf(i) }), paramObject[i]));
      }
    }
    if ((paramObject instanceof Set))
    {
      paramObject = ((Set)paramObject).iterator();
      while (((Iterator)paramObject).hasNext()) {
        localLinkedList.addAll(getParamsList(paramString, ((Iterator)paramObject).next()));
      }
    }
    localLinkedList.add(new BasicNameValuePair(paramString, paramObject.toString()));
    return localLinkedList;
  }
  
  public void add(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString2 != null))
    {
      Object localObject2 = this.urlParamsWithObjects.get(paramString1);
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new HashSet();
        put(paramString1, localObject1);
      }
      if ((localObject1 instanceof List)) {
        ((List)localObject1).add(paramString2);
      } else if ((localObject1 instanceof Set)) {
        ((Set)localObject1).add(paramString2);
      }
    }
  }
  
  public HttpEntity getEntity(ResponseHandlerInterface paramResponseHandlerInterface)
    throws IOException
  {
    if (this.useJsonStreamer) {
      return createJsonStreamerEntity(paramResponseHandlerInterface);
    }
    if ((!this.forceMultipartEntity) && (this.streamParams.isEmpty()) && (this.fileParams.isEmpty()) && (this.fileArrayParams.isEmpty())) {
      return createFormEntity();
    }
    return createMultipartEntity(paramResponseHandlerInterface);
  }
  
  protected String getParamString()
  {
    return URLEncodedUtils.format(getParamsList(), this.contentEncoding);
  }
  
  protected List<BasicNameValuePair> getParamsList()
  {
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = this.urlParams.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localLinkedList.add(new BasicNameValuePair((String)localEntry.getKey(), (String)localEntry.getValue()));
    }
    localLinkedList.addAll(getParamsList(null, this.urlParamsWithObjects));
    return localLinkedList;
  }
  
  public boolean has(String paramString)
  {
    boolean bool;
    if ((this.urlParams.get(paramString) == null) && (this.streamParams.get(paramString) == null) && (this.fileParams.get(paramString) == null) && (this.urlParamsWithObjects.get(paramString) == null) && (this.fileArrayParams.get(paramString) == null)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public void put(String paramString, int paramInt)
  {
    if (paramString != null) {
      this.urlParams.put(paramString, String.valueOf(paramInt));
    }
  }
  
  public void put(String paramString, long paramLong)
  {
    if (paramString != null) {
      this.urlParams.put(paramString, String.valueOf(paramLong));
    }
  }
  
  public void put(String paramString, File paramFile)
    throws FileNotFoundException
  {
    put(paramString, paramFile, null, null);
  }
  
  public void put(String paramString1, File paramFile, String paramString2)
    throws FileNotFoundException
  {
    put(paramString1, paramFile, paramString2, null);
  }
  
  public void put(String paramString1, File paramFile, String paramString2, String paramString3)
    throws FileNotFoundException
  {
    if ((paramFile != null) && (paramFile.exists()))
    {
      if (paramString1 != null) {
        this.fileParams.put(paramString1, new FileWrapper(paramFile, paramString2, paramString3));
      }
      return;
    }
    throw new FileNotFoundException();
  }
  
  public void put(String paramString, InputStream paramInputStream)
  {
    put(paramString, paramInputStream, null);
  }
  
  public void put(String paramString1, InputStream paramInputStream, String paramString2)
  {
    put(paramString1, paramInputStream, paramString2, null);
  }
  
  public void put(String paramString1, InputStream paramInputStream, String paramString2, String paramString3)
  {
    put(paramString1, paramInputStream, paramString2, paramString3, this.autoCloseInputStreams);
  }
  
  public void put(String paramString1, InputStream paramInputStream, String paramString2, String paramString3, boolean paramBoolean)
  {
    if ((paramString1 != null) && (paramInputStream != null)) {
      this.streamParams.put(paramString1, StreamWrapper.newInstance(paramInputStream, paramString2, paramString3, paramBoolean));
    }
  }
  
  public void put(String paramString, Object paramObject)
  {
    if ((paramString != null) && (paramObject != null)) {
      this.urlParamsWithObjects.put(paramString, paramObject);
    }
  }
  
  public void put(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString2 != null)) {
      this.urlParams.put(paramString1, paramString2);
    }
  }
  
  public void put(String paramString1, String paramString2, File paramFile)
    throws FileNotFoundException
  {
    put(paramString1, paramFile, null, paramString2);
  }
  
  public void put(String paramString, File[] paramArrayOfFile)
    throws FileNotFoundException
  {
    put(paramString, paramArrayOfFile, null, null);
  }
  
  public void put(String paramString1, File[] paramArrayOfFile, String paramString2, String paramString3)
    throws FileNotFoundException
  {
    if (paramString1 != null)
    {
      ArrayList localArrayList = new ArrayList();
      int j = paramArrayOfFile.length;
      int i = 0;
      while (i < j)
      {
        File localFile = paramArrayOfFile[i];
        if ((localFile != null) && (localFile.exists()))
        {
          localArrayList.add(new FileWrapper(localFile, paramString2, paramString3));
          i++;
        }
        else
        {
          throw new FileNotFoundException();
        }
      }
      this.fileArrayParams.put(paramString1, localArrayList);
    }
  }
  
  public void remove(String paramString)
  {
    this.urlParams.remove(paramString);
    this.streamParams.remove(paramString);
    this.fileParams.remove(paramString);
    this.urlParamsWithObjects.remove(paramString);
    this.fileArrayParams.remove(paramString);
  }
  
  public void setAutoCloseInputStreams(boolean paramBoolean)
  {
    this.autoCloseInputStreams = paramBoolean;
  }
  
  public void setContentEncoding(String paramString)
  {
    if (paramString != null) {
      this.contentEncoding = paramString;
    } else {
      AsyncHttpClient.log.d("RequestParams", "setContentEncoding called with null attribute");
    }
  }
  
  public void setElapsedFieldInJsonStreamer(String paramString)
  {
    this.elapsedFieldInJsonStreamer = paramString;
  }
  
  public void setForceMultipartEntityContentType(boolean paramBoolean)
  {
    this.forceMultipartEntity = paramBoolean;
  }
  
  public void setHttpEntityIsRepeatable(boolean paramBoolean)
  {
    this.isRepeatable = paramBoolean;
  }
  
  public void setUseJsonStreamer(boolean paramBoolean)
  {
    this.useJsonStreamer = paramBoolean;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject2 = this.urlParams.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject1).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append((String)((Map.Entry)localObject1).getValue());
    }
    Object localObject1 = this.streamParams.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject2).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append("STREAM");
    }
    localObject2 = this.fileParams.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject1).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append("FILE");
    }
    localObject2 = this.fileArrayParams.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject1).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append("FILES(SIZE=");
      localStringBuilder.append(((List)((Map.Entry)localObject1).getValue()).size());
      localStringBuilder.append(")");
    }
    localObject1 = getParamsList(null, this.urlParamsWithObjects).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (BasicNameValuePair)((Iterator)localObject1).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append(((BasicNameValuePair)localObject2).getName());
      localStringBuilder.append("=");
      localStringBuilder.append(((BasicNameValuePair)localObject2).getValue());
    }
    return localStringBuilder.toString();
  }
  
  public static class FileWrapper
    implements Serializable
  {
    public final String contentType;
    public final String customFileName;
    public final File file;
    
    public FileWrapper(File paramFile, String paramString1, String paramString2)
    {
      this.file = paramFile;
      this.contentType = paramString1;
      this.customFileName = paramString2;
    }
  }
  
  public static class StreamWrapper
  {
    public final boolean autoClose;
    public final String contentType;
    public final InputStream inputStream;
    public final String name;
    
    public StreamWrapper(InputStream paramInputStream, String paramString1, String paramString2, boolean paramBoolean)
    {
      this.inputStream = paramInputStream;
      this.name = paramString1;
      this.contentType = paramString2;
      this.autoClose = paramBoolean;
    }
    
    static StreamWrapper newInstance(InputStream paramInputStream, String paramString1, String paramString2, boolean paramBoolean)
    {
      String str = paramString2;
      if (paramString2 == null) {
        str = "application/octet-stream";
      }
      return new StreamWrapper(paramInputStream, paramString1, str, paramBoolean);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/RequestParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */