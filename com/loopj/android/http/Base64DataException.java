package com.loopj.android.http;

import java.io.IOException;

public class Base64DataException
  extends IOException
{
  public Base64DataException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/Base64DataException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */