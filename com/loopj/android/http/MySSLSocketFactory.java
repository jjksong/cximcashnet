package com.loopj.android.http;

import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.scheme.PlainSocketFactory;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.conn.scheme.SocketFactory;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.impl.conn.tsccm.ThreadSafeClientConnManager;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MySSLSocketFactory
  extends cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory
{
  final SSLContext sslContext = SSLContext.getInstance("TLS");
  
  public MySSLSocketFactory(KeyStore paramKeyStore)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
  {
    super(paramKeyStore);
    paramKeyStore = new X509TrustManager()
    {
      public void checkClientTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
        throws CertificateException
      {}
      
      public void checkServerTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
        throws CertificateException
      {}
      
      public X509Certificate[] getAcceptedIssuers()
      {
        return null;
      }
    };
    this.sslContext.init(null, new TrustManager[] { paramKeyStore }, null);
  }
  
  public static cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory getFixedSocketFactory()
  {
    cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory localSSLSocketFactory;
    try
    {
      MySSLSocketFactory localMySSLSocketFactory = new com/loopj/android/http/MySSLSocketFactory;
      localMySSLSocketFactory.<init>(getKeystore());
      localMySSLSocketFactory.setHostnameVerifier(cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }
    catch (Throwable localThrowable)
    {
      localThrowable.printStackTrace();
      localSSLSocketFactory = cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory.getSocketFactory();
    }
    return localSSLSocketFactory;
  }
  
  public static KeyStore getKeystore()
  {
    Object localObject2 = null;
    Object localObject1;
    try
    {
      localObject1 = KeyStore.getInstance(KeyStore.getDefaultType());
      try
      {
        ((KeyStore)localObject1).load(null, null);
      }
      catch (Throwable localThrowable1) {}
      localThrowable2.printStackTrace();
    }
    catch (Throwable localThrowable2)
    {
      localObject1 = localObject2;
    }
    return (KeyStore)localObject1;
  }
  
  /* Error */
  public static KeyStore getKeystoreOfCA(java.io.InputStream paramInputStream)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: ldc 88
    //   4: invokestatic 93	java/security/cert/CertificateFactory:getInstance	(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
    //   7: astore_3
    //   8: new 95	java/io/BufferedInputStream
    //   11: astore_1
    //   12: aload_1
    //   13: aload_0
    //   14: invokespecial 98	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: aload_1
    //   18: astore_0
    //   19: aload_3
    //   20: aload_1
    //   21: invokevirtual 102	java/security/cert/CertificateFactory:generateCertificate	(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   24: astore_2
    //   25: aload_1
    //   26: invokevirtual 107	java/io/InputStream:close	()V
    //   29: aload_2
    //   30: astore_1
    //   31: goto +50 -> 81
    //   34: astore_0
    //   35: aload_0
    //   36: invokevirtual 108	java/io/IOException:printStackTrace	()V
    //   39: aload_2
    //   40: astore_1
    //   41: goto +40 -> 81
    //   44: astore_2
    //   45: goto +12 -> 57
    //   48: astore_0
    //   49: aload_2
    //   50: astore_1
    //   51: goto +75 -> 126
    //   54: astore_2
    //   55: aconst_null
    //   56: astore_1
    //   57: aload_1
    //   58: astore_0
    //   59: aload_2
    //   60: invokevirtual 109	java/security/cert/CertificateException:printStackTrace	()V
    //   63: aload_1
    //   64: ifnull +15 -> 79
    //   67: aload_1
    //   68: invokevirtual 107	java/io/InputStream:close	()V
    //   71: goto +8 -> 79
    //   74: astore_0
    //   75: aload_0
    //   76: invokevirtual 108	java/io/IOException:printStackTrace	()V
    //   79: aconst_null
    //   80: astore_1
    //   81: invokestatic 71	java/security/KeyStore:getDefaultType	()Ljava/lang/String;
    //   84: astore_0
    //   85: aload_0
    //   86: invokestatic 74	java/security/KeyStore:getInstance	(Ljava/lang/String;)Ljava/security/KeyStore;
    //   89: astore_0
    //   90: aload_0
    //   91: aconst_null
    //   92: aconst_null
    //   93: invokevirtual 78	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   96: aload_0
    //   97: ldc 111
    //   99: aload_1
    //   100: invokevirtual 115	java/security/KeyStore:setCertificateEntry	(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    //   103: goto +14 -> 117
    //   106: astore_1
    //   107: goto +6 -> 113
    //   110: astore_1
    //   111: aconst_null
    //   112: astore_0
    //   113: aload_1
    //   114: invokevirtual 116	java/lang/Exception:printStackTrace	()V
    //   117: aload_0
    //   118: areturn
    //   119: astore_1
    //   120: aload_0
    //   121: astore_2
    //   122: aload_1
    //   123: astore_0
    //   124: aload_2
    //   125: astore_1
    //   126: aload_1
    //   127: ifnull +15 -> 142
    //   130: aload_1
    //   131: invokevirtual 107	java/io/InputStream:close	()V
    //   134: goto +8 -> 142
    //   137: astore_1
    //   138: aload_1
    //   139: invokevirtual 108	java/io/IOException:printStackTrace	()V
    //   142: aload_0
    //   143: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	144	0	paramInputStream	java.io.InputStream
    //   11	89	1	localObject1	Object
    //   106	1	1	localException1	Exception
    //   110	4	1	localException2	Exception
    //   119	4	1	localObject2	Object
    //   125	6	1	localInputStream1	java.io.InputStream
    //   137	2	1	localIOException	IOException
    //   1	39	2	localCertificate	java.security.cert.Certificate
    //   44	6	2	localCertificateException1	CertificateException
    //   54	6	2	localCertificateException2	CertificateException
    //   121	4	2	localInputStream2	java.io.InputStream
    //   7	13	3	localCertificateFactory	java.security.cert.CertificateFactory
    // Exception table:
    //   from	to	target	type
    //   25	29	34	java/io/IOException
    //   19	25	44	java/security/cert/CertificateException
    //   2	17	48	finally
    //   2	17	54	java/security/cert/CertificateException
    //   67	71	74	java/io/IOException
    //   90	103	106	java/lang/Exception
    //   85	90	110	java/lang/Exception
    //   19	25	119	finally
    //   59	63	119	finally
    //   130	134	137	java/io/IOException
  }
  
  public static DefaultHttpClient getNewHttpClient(KeyStore paramKeyStore)
  {
    try
    {
      Object localObject1 = new com/loopj/android/http/MySSLSocketFactory;
      ((MySSLSocketFactory)localObject1).<init>(paramKeyStore);
      paramKeyStore = new cz/msebera/android/httpclient/conn/scheme/SchemeRegistry;
      paramKeyStore.<init>();
      Object localObject2 = new cz/msebera/android/httpclient/conn/scheme/Scheme;
      ((Scheme)localObject2).<init>("http", PlainSocketFactory.getSocketFactory(), 80);
      paramKeyStore.register((Scheme)localObject2);
      localObject2 = new cz/msebera/android/httpclient/conn/scheme/Scheme;
      ((Scheme)localObject2).<init>("https", (SocketFactory)localObject1, 443);
      paramKeyStore.register((Scheme)localObject2);
      localObject2 = new cz/msebera/android/httpclient/params/BasicHttpParams;
      ((BasicHttpParams)localObject2).<init>();
      HttpProtocolParams.setVersion((HttpParams)localObject2, HttpVersion.HTTP_1_1);
      HttpProtocolParams.setContentCharset((HttpParams)localObject2, "UTF-8");
      localObject1 = new cz/msebera/android/httpclient/impl/conn/tsccm/ThreadSafeClientConnManager;
      ((ThreadSafeClientConnManager)localObject1).<init>((HttpParams)localObject2, paramKeyStore);
      paramKeyStore = new DefaultHttpClient((ClientConnectionManager)localObject1, (HttpParams)localObject2);
      return paramKeyStore;
    }
    catch (Exception paramKeyStore) {}
    return new DefaultHttpClient();
  }
  
  public Socket createSocket()
    throws IOException
  {
    return this.sslContext.getSocketFactory().createSocket();
  }
  
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
    throws IOException
  {
    return this.sslContext.getSocketFactory().createSocket(paramSocket, paramString, paramInt, paramBoolean);
  }
  
  public void fixHttpsURLConnection()
  {
    HttpsURLConnection.setDefaultSSLSocketFactory(this.sslContext.getSocketFactory());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/MySSLSocketFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */