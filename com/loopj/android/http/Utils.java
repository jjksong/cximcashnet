package com.loopj.android.http;

class Utils
{
  public static void asserts(boolean paramBoolean, String paramString)
  {
    if (paramBoolean) {
      return;
    }
    throw new AssertionError(paramString);
  }
  
  public static <T> T notNull(T paramT, String paramString)
  {
    if (paramT != null) {
      return paramT;
    }
    paramT = new StringBuilder();
    paramT.append(paramString);
    paramT.append(" should not be null!");
    throw new IllegalArgumentException(paramT.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/Utils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */