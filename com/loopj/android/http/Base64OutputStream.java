package com.loopj.android.http;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Base64OutputStream
  extends FilterOutputStream
{
  private static final byte[] EMPTY = new byte[0];
  private int bpos = 0;
  private byte[] buffer = null;
  private final Base64.Coder coder;
  private final int flags;
  
  public Base64OutputStream(OutputStream paramOutputStream, int paramInt)
  {
    this(paramOutputStream, paramInt, true);
  }
  
  public Base64OutputStream(OutputStream paramOutputStream, int paramInt, boolean paramBoolean)
  {
    super(paramOutputStream);
    this.flags = paramInt;
    if (paramBoolean) {
      this.coder = new Base64.Encoder(paramInt, null);
    } else {
      this.coder = new Base64.Decoder(paramInt, null);
    }
  }
  
  private byte[] embiggen(byte[] paramArrayOfByte, int paramInt)
  {
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length >= paramInt)) {
      return paramArrayOfByte;
    }
    return new byte[paramInt];
  }
  
  private void flushBuffer()
    throws IOException
  {
    int i = this.bpos;
    if (i > 0)
    {
      internalWrite(this.buffer, 0, i, false);
      this.bpos = 0;
    }
  }
  
  private void internalWrite(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    throws IOException
  {
    Base64.Coder localCoder = this.coder;
    localCoder.output = embiggen(localCoder.output, this.coder.maxOutputSize(paramInt2));
    if (this.coder.process(paramArrayOfByte, paramInt1, paramInt2, paramBoolean))
    {
      this.out.write(this.coder.output, 0, this.coder.op);
      return;
    }
    throw new Base64DataException("bad base-64");
  }
  
  public void close()
    throws IOException
  {
    try
    {
      flushBuffer();
      internalWrite(EMPTY, 0, 0, true);
      Object localObject1 = null;
    }
    catch (IOException localIOException1) {}
    Object localObject2;
    try
    {
      if ((this.flags & 0x10) == 0)
      {
        this.out.close();
        localObject2 = localIOException1;
      }
      else
      {
        this.out.flush();
        localObject2 = localIOException1;
      }
    }
    catch (IOException localIOException2)
    {
      localObject2 = localIOException1;
      if (localIOException1 != null) {
        localObject2 = localIOException2;
      }
    }
    if (localObject2 == null) {
      return;
    }
    throw ((Throwable)localObject2);
  }
  
  public void write(int paramInt)
    throws IOException
  {
    if (this.buffer == null) {
      this.buffer = new byte['Ѐ'];
    }
    int i = this.bpos;
    byte[] arrayOfByte = this.buffer;
    if (i >= arrayOfByte.length)
    {
      internalWrite(arrayOfByte, 0, i, false);
      this.bpos = 0;
    }
    arrayOfByte = this.buffer;
    i = this.bpos;
    this.bpos = (i + 1);
    arrayOfByte[i] = ((byte)paramInt);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramInt2 <= 0) {
      return;
    }
    flushBuffer();
    internalWrite(paramArrayOfByte, paramInt1, paramInt2, false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/Base64OutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */