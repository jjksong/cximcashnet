package com.loopj.android.http;

import cz.msebera.android.httpclient.Header;
import org.xml.sax.helpers.DefaultHandler;

public abstract class SaxAsyncHttpResponseHandler<T extends DefaultHandler>
  extends AsyncHttpResponseHandler
{
  private static final String LOG_TAG = "SaxAsyncHttpRH";
  private T handler = null;
  
  public SaxAsyncHttpResponseHandler(T paramT)
  {
    if (paramT != null)
    {
      this.handler = paramT;
      return;
    }
    throw new Error("null instance of <T extends DefaultHandler> passed to constructor");
  }
  
  /* Error */
  protected byte[] getResponseData(cz.msebera.android.httpclient.HttpEntity paramHttpEntity)
    throws java.io.IOException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +181 -> 182
    //   4: aload_1
    //   5: invokeinterface 43 1 0
    //   10: astore 4
    //   12: aload 4
    //   14: ifnull +168 -> 182
    //   17: invokestatic 49	javax/xml/parsers/SAXParserFactory:newInstance	()Ljavax/xml/parsers/SAXParserFactory;
    //   20: invokevirtual 53	javax/xml/parsers/SAXParserFactory:newSAXParser	()Ljavax/xml/parsers/SAXParser;
    //   23: invokevirtual 59	javax/xml/parsers/SAXParser:getXMLReader	()Lorg/xml/sax/XMLReader;
    //   26: astore 5
    //   28: aload 5
    //   30: aload_0
    //   31: getfield 19	com/loopj/android/http/SaxAsyncHttpResponseHandler:handler	Lorg/xml/sax/helpers/DefaultHandler;
    //   34: invokeinterface 65 2 0
    //   39: new 67	java/io/InputStreamReader
    //   42: astore_2
    //   43: aload_2
    //   44: aload 4
    //   46: aload_0
    //   47: invokevirtual 71	com/loopj/android/http/SaxAsyncHttpResponseHandler:getCharset	()Ljava/lang/String;
    //   50: invokespecial 74	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   53: aload_2
    //   54: astore_1
    //   55: new 76	org/xml/sax/InputSource
    //   58: astore_3
    //   59: aload_2
    //   60: astore_1
    //   61: aload_3
    //   62: aload_2
    //   63: invokespecial 79	org/xml/sax/InputSource:<init>	(Ljava/io/Reader;)V
    //   66: aload_2
    //   67: astore_1
    //   68: aload 5
    //   70: aload_3
    //   71: invokeinterface 83 2 0
    //   76: aload 4
    //   78: invokestatic 89	com/loopj/android/http/AsyncHttpClient:silentCloseInputStream	(Ljava/io/InputStream;)V
    //   81: aload_2
    //   82: invokevirtual 92	java/io/InputStreamReader:close	()V
    //   85: goto +97 -> 182
    //   88: astore_3
    //   89: goto +16 -> 105
    //   92: astore_3
    //   93: goto +42 -> 135
    //   96: astore_1
    //   97: aconst_null
    //   98: astore_2
    //   99: goto +68 -> 167
    //   102: astore_3
    //   103: aconst_null
    //   104: astore_2
    //   105: aload_2
    //   106: astore_1
    //   107: getstatic 96	com/loopj/android/http/AsyncHttpClient:log	Lcom/loopj/android/http/LogInterface;
    //   110: ldc 9
    //   112: ldc 98
    //   114: aload_3
    //   115: invokeinterface 104 4 0
    //   120: aload 4
    //   122: invokestatic 89	com/loopj/android/http/AsyncHttpClient:silentCloseInputStream	(Ljava/io/InputStream;)V
    //   125: aload_2
    //   126: ifnull +56 -> 182
    //   129: goto -48 -> 81
    //   132: astore_3
    //   133: aconst_null
    //   134: astore_2
    //   135: aload_2
    //   136: astore_1
    //   137: getstatic 96	com/loopj/android/http/AsyncHttpClient:log	Lcom/loopj/android/http/LogInterface;
    //   140: ldc 9
    //   142: ldc 98
    //   144: aload_3
    //   145: invokeinterface 104 4 0
    //   150: aload 4
    //   152: invokestatic 89	com/loopj/android/http/AsyncHttpClient:silentCloseInputStream	(Ljava/io/InputStream;)V
    //   155: aload_2
    //   156: ifnull +26 -> 182
    //   159: goto -78 -> 81
    //   162: astore_3
    //   163: aload_1
    //   164: astore_2
    //   165: aload_3
    //   166: astore_1
    //   167: aload 4
    //   169: invokestatic 89	com/loopj/android/http/AsyncHttpClient:silentCloseInputStream	(Ljava/io/InputStream;)V
    //   172: aload_2
    //   173: ifnull +7 -> 180
    //   176: aload_2
    //   177: invokevirtual 92	java/io/InputStreamReader:close	()V
    //   180: aload_1
    //   181: athrow
    //   182: aconst_null
    //   183: areturn
    //   184: astore_1
    //   185: goto -3 -> 182
    //   188: astore_2
    //   189: goto -9 -> 180
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	192	0	this	SaxAsyncHttpResponseHandler
    //   0	192	1	paramHttpEntity	cz.msebera.android.httpclient.HttpEntity
    //   42	135	2	localObject1	Object
    //   188	1	2	localIOException	java.io.IOException
    //   58	13	3	localInputSource	org.xml.sax.InputSource
    //   88	1	3	localParserConfigurationException1	javax.xml.parsers.ParserConfigurationException
    //   92	1	3	localSAXException1	org.xml.sax.SAXException
    //   102	13	3	localParserConfigurationException2	javax.xml.parsers.ParserConfigurationException
    //   132	13	3	localSAXException2	org.xml.sax.SAXException
    //   162	4	3	localObject2	Object
    //   10	158	4	localInputStream	java.io.InputStream
    //   26	43	5	localXMLReader	org.xml.sax.XMLReader
    // Exception table:
    //   from	to	target	type
    //   55	59	88	javax/xml/parsers/ParserConfigurationException
    //   61	66	88	javax/xml/parsers/ParserConfigurationException
    //   68	76	88	javax/xml/parsers/ParserConfigurationException
    //   55	59	92	org/xml/sax/SAXException
    //   61	66	92	org/xml/sax/SAXException
    //   68	76	92	org/xml/sax/SAXException
    //   17	53	96	finally
    //   17	53	102	javax/xml/parsers/ParserConfigurationException
    //   17	53	132	org/xml/sax/SAXException
    //   55	59	162	finally
    //   61	66	162	finally
    //   68	76	162	finally
    //   107	120	162	finally
    //   137	150	162	finally
    //   81	85	184	java/io/IOException
    //   176	180	188	java/io/IOException
  }
  
  public abstract void onFailure(int paramInt, Header[] paramArrayOfHeader, T paramT);
  
  public void onFailure(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte, Throwable paramThrowable)
  {
    onFailure(paramInt, paramArrayOfHeader, this.handler);
  }
  
  public abstract void onSuccess(int paramInt, Header[] paramArrayOfHeader, T paramT);
  
  public void onSuccess(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte)
  {
    onSuccess(paramInt, paramArrayOfHeader, this.handler);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/SaxAsyncHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */