package com.loopj.android.http;

import android.os.Message;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;
import java.io.IOException;
import java.io.InputStream;

public abstract class DataAsyncHttpResponseHandler
  extends AsyncHttpResponseHandler
{
  private static final String LOG_TAG = "DataAsyncHttpRH";
  protected static final int PROGRESS_DATA_MESSAGE = 7;
  
  public static byte[] copyOfRange(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws ArrayIndexOutOfBoundsException, IllegalArgumentException, NullPointerException
  {
    if (paramInt1 <= paramInt2)
    {
      int i = paramArrayOfByte.length;
      if ((paramInt1 >= 0) && (paramInt1 <= i))
      {
        paramInt2 -= paramInt1;
        i = Math.min(paramInt2, i - paramInt1);
        byte[] arrayOfByte = new byte[paramInt2];
        System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, 0, i);
        return arrayOfByte;
      }
      throw new ArrayIndexOutOfBoundsException();
    }
    throw new IllegalArgumentException();
  }
  
  byte[] getResponseData(HttpEntity paramHttpEntity)
    throws IOException
  {
    if (paramHttpEntity != null)
    {
      InputStream localInputStream = paramHttpEntity.getContent();
      if (localInputStream != null)
      {
        long l2 = paramHttpEntity.getContentLength();
        if (l2 <= 2147483647L)
        {
          long l1 = l2;
          if (l2 < 0L) {
            l1 = 4096L;
          }
          try
          {
            paramHttpEntity = new cz/msebera/android/httpclient/util/ByteArrayBuffer;
            paramHttpEntity.<init>((int)l1);
            try
            {
              byte[] arrayOfByte = new byte['က'];
              for (;;)
              {
                int i = localInputStream.read(arrayOfByte);
                if ((i == -1) || (Thread.currentThread().isInterrupted())) {
                  break;
                }
                paramHttpEntity.append(arrayOfByte, 0, i);
                sendProgressDataMessage(copyOfRange(arrayOfByte, 0, i));
                sendProgressMessage(0, l1);
              }
              AsyncHttpClient.silentCloseInputStream(localInputStream);
              paramHttpEntity = paramHttpEntity.toByteArray();
            }
            finally
            {
              AsyncHttpClient.silentCloseInputStream(localInputStream);
            }
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
          }
          catch (OutOfMemoryError paramHttpEntity)
          {
            System.gc();
            throw new IOException("File too large to fit into available memory");
          }
        }
      }
    }
    paramHttpEntity = null;
    return paramHttpEntity;
  }
  
  protected void handleMessage(Message paramMessage)
  {
    super.handleMessage(paramMessage);
    if (paramMessage.what == 7)
    {
      paramMessage = (Object[])paramMessage.obj;
      if ((paramMessage != null) && (paramMessage.length >= 1)) {
        try
        {
          onProgressData((byte[])paramMessage[0]);
        }
        catch (Throwable paramMessage)
        {
          AsyncHttpClient.log.e("DataAsyncHttpRH", "custom onProgressData contains an error", paramMessage);
        }
      } else {
        AsyncHttpClient.log.e("DataAsyncHttpRH", "PROGRESS_DATA_MESSAGE didn't got enough params");
      }
    }
  }
  
  public void onProgressData(byte[] paramArrayOfByte)
  {
    AsyncHttpClient.log.d("DataAsyncHttpRH", "onProgressData(byte[]) was not overriden, but callback was received");
  }
  
  public final void sendProgressDataMessage(byte[] paramArrayOfByte)
  {
    sendMessage(obtainMessage(7, new Object[] { paramArrayOfByte }));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/DataAsyncHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */