package com.loopj.android.http;

import android.os.Looper;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.HttpResponseException;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public abstract class BinaryHttpResponseHandler
  extends AsyncHttpResponseHandler
{
  private static final String LOG_TAG = "BinaryHttpRH";
  private String[] mAllowedContentTypes = { "application/octet-stream", "image/jpeg", "image/png", "image/gif" };
  
  public BinaryHttpResponseHandler() {}
  
  public BinaryHttpResponseHandler(String[] paramArrayOfString)
  {
    if (paramArrayOfString != null) {
      this.mAllowedContentTypes = paramArrayOfString;
    } else {
      AsyncHttpClient.log.e("BinaryHttpRH", "Constructor passed allowedContentTypes was null !");
    }
  }
  
  public BinaryHttpResponseHandler(String[] paramArrayOfString, Looper paramLooper)
  {
    super(paramLooper);
    if (paramArrayOfString != null) {
      this.mAllowedContentTypes = paramArrayOfString;
    } else {
      AsyncHttpClient.log.e("BinaryHttpRH", "Constructor passed allowedContentTypes was null !");
    }
  }
  
  public String[] getAllowedContentTypes()
  {
    return this.mAllowedContentTypes;
  }
  
  public abstract void onFailure(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte, Throwable paramThrowable);
  
  public abstract void onSuccess(int paramInt, Header[] paramArrayOfHeader, byte[] paramArrayOfByte);
  
  public final void sendResponseMessage(HttpResponse paramHttpResponse)
    throws IOException
  {
    Object localObject2 = paramHttpResponse.getStatusLine();
    Object localObject1 = paramHttpResponse.getHeaders("Content-Type");
    if (localObject1.length != 1)
    {
      sendFailureMessage(((StatusLine)localObject2).getStatusCode(), paramHttpResponse.getAllHeaders(), null, new HttpResponseException(((StatusLine)localObject2).getStatusCode(), "None, or more than one, Content-Type Header found!"));
      return;
    }
    int i = 0;
    localObject1 = localObject1[0];
    String[] arrayOfString = getAllowedContentTypes();
    int k = arrayOfString.length;
    int j = 0;
    while (i < k)
    {
      String str = arrayOfString[i];
      try
      {
        boolean bool = Pattern.matches(str, ((Header)localObject1).getValue());
        if (bool) {
          j = 1;
        }
      }
      catch (PatternSyntaxException localPatternSyntaxException)
      {
        LogInterface localLogInterface = AsyncHttpClient.log;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Given pattern is not valid: ");
        localStringBuilder.append(str);
        localLogInterface.e("BinaryHttpRH", localStringBuilder.toString(), localPatternSyntaxException);
      }
      i++;
    }
    if (j == 0)
    {
      j = ((StatusLine)localObject2).getStatusCode();
      paramHttpResponse = paramHttpResponse.getAllHeaders();
      i = ((StatusLine)localObject2).getStatusCode();
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Content-Type (");
      ((StringBuilder)localObject2).append(((Header)localObject1).getValue());
      ((StringBuilder)localObject2).append(") not allowed!");
      sendFailureMessage(j, paramHttpResponse, null, new HttpResponseException(i, ((StringBuilder)localObject2).toString()));
      return;
    }
    super.sendResponseMessage(paramHttpResponse);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/BinaryHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */