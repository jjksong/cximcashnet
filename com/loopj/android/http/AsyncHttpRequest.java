package com.loopj.android.http;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.impl.client.AbstractHttpClient;
import cz.msebera.android.httpclient.protocol.HttpContext;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

public class AsyncHttpRequest
  implements Runnable
{
  private boolean cancelIsNotified;
  private final AbstractHttpClient client;
  private final HttpContext context;
  private int executionCount;
  private final AtomicBoolean isCancelled = new AtomicBoolean();
  private volatile boolean isFinished;
  private boolean isRequestPreProcessed;
  private final HttpUriRequest request;
  private final ResponseHandlerInterface responseHandler;
  
  public AsyncHttpRequest(AbstractHttpClient paramAbstractHttpClient, HttpContext paramHttpContext, HttpUriRequest paramHttpUriRequest, ResponseHandlerInterface paramResponseHandlerInterface)
  {
    this.client = ((AbstractHttpClient)Utils.notNull(paramAbstractHttpClient, "client"));
    this.context = ((HttpContext)Utils.notNull(paramHttpContext, "context"));
    this.request = ((HttpUriRequest)Utils.notNull(paramHttpUriRequest, "request"));
    this.responseHandler = ((ResponseHandlerInterface)Utils.notNull(paramResponseHandlerInterface, "responseHandler"));
  }
  
  private void makeRequest()
    throws IOException
  {
    if (isCancelled()) {
      return;
    }
    if (this.request.getURI().getScheme() != null)
    {
      Object localObject = this.responseHandler;
      if ((localObject instanceof RangeFileAsyncHttpResponseHandler)) {
        ((RangeFileAsyncHttpResponseHandler)localObject).updateRequestHeaders(this.request);
      }
      localObject = this.client.execute(this.request, this.context);
      if (isCancelled()) {
        return;
      }
      ResponseHandlerInterface localResponseHandlerInterface = this.responseHandler;
      localResponseHandlerInterface.onPreProcessResponse(localResponseHandlerInterface, (HttpResponse)localObject);
      if (isCancelled()) {
        return;
      }
      this.responseHandler.sendResponseMessage((HttpResponse)localObject);
      if (isCancelled()) {
        return;
      }
      localResponseHandlerInterface = this.responseHandler;
      localResponseHandlerInterface.onPostProcessResponse(localResponseHandlerInterface, (HttpResponse)localObject);
      return;
    }
    throw new MalformedURLException("No valid URI scheme was provided");
  }
  
  private void makeRequestWithRetries()
    throws IOException
  {
    HttpRequestRetryHandler localHttpRequestRetryHandler = this.client.getHttpRequestRetryHandler();
    StringBuilder localStringBuilder1 = null;
    int j = 1;
    label233:
    Object localObject;
    while (j != 0)
    {
      boolean bool;
      IOException localIOException2;
      try
      {
        makeRequest();
        return;
      }
      catch (Exception localException)
      {
        break label265;
      }
      catch (IOException localIOException1)
      {
        if (isCancelled()) {
          return;
        }
        i = this.executionCount + 1;
        this.executionCount = i;
        bool = localHttpRequestRetryHandler.retryRequest(localIOException1, i, this.context);
      }
      catch (NullPointerException localNullPointerException)
      {
        localIOException2 = new java/io/IOException;
        localStringBuilder1 = new java/lang/StringBuilder;
        localStringBuilder1.<init>();
        localStringBuilder1.append("NPE in HttpClient: ");
        localStringBuilder1.append(localNullPointerException.getMessage());
        localIOException2.<init>(localStringBuilder1.toString());
        i = this.executionCount + 1;
        this.executionCount = i;
        bool = localHttpRequestRetryHandler.retryRequest(localIOException2, i, this.context);
      }
      catch (UnknownHostException localUnknownHostException)
      {
        int i;
        localIOException2 = new java/io/IOException;
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>();
        localStringBuilder2.append("UnknownHostException exception: ");
        localStringBuilder2.append(localUnknownHostException.getMessage());
        localIOException2.<init>(localStringBuilder2.toString());
        if (this.executionCount > 0)
        {
          i = this.executionCount + 1;
          this.executionCount = i;
          if (localHttpRequestRetryHandler.retryRequest(localUnknownHostException, i, this.context))
          {
            bool = true;
            break label233;
          }
        }
        bool = false;
      }
      j = bool;
      localObject = localIOException2;
      if (bool)
      {
        this.responseHandler.sendRetryMessage(this.executionCount);
        j = bool;
        localObject = localIOException2;
        continue;
        label265:
        AsyncHttpClient.log.e("AsyncHttpRequest", "Unhandled exception origin cause", localIOException2);
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("Unhandled exception: ");
        ((StringBuilder)localObject).append(localIOException2.getMessage());
        localObject = new IOException(((StringBuilder)localObject).toString());
      }
    }
    throw ((Throwable)localObject);
  }
  
  private void sendCancelNotification()
  {
    try
    {
      if ((!this.isFinished) && (this.isCancelled.get()) && (!this.cancelIsNotified))
      {
        this.cancelIsNotified = true;
        this.responseHandler.sendCancelMessage();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    this.isCancelled.set(true);
    this.request.abort();
    return isCancelled();
  }
  
  public Object getTag()
  {
    return this.responseHandler.getTag();
  }
  
  public boolean isCancelled()
  {
    boolean bool = this.isCancelled.get();
    if (bool) {
      sendCancelNotification();
    }
    return bool;
  }
  
  public boolean isDone()
  {
    boolean bool;
    if ((!isCancelled()) && (!this.isFinished)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public void onPostProcessRequest(AsyncHttpRequest paramAsyncHttpRequest) {}
  
  public void onPreProcessRequest(AsyncHttpRequest paramAsyncHttpRequest) {}
  
  public void run()
  {
    if (isCancelled()) {
      return;
    }
    if (!this.isRequestPreProcessed)
    {
      this.isRequestPreProcessed = true;
      onPreProcessRequest(this);
    }
    if (isCancelled()) {
      return;
    }
    this.responseHandler.sendStartMessage();
    if (isCancelled()) {
      return;
    }
    try
    {
      makeRequestWithRetries();
    }
    catch (IOException localIOException)
    {
      if (!isCancelled()) {
        this.responseHandler.sendFailureMessage(0, null, null, localIOException);
      } else {
        AsyncHttpClient.log.e("AsyncHttpRequest", "makeRequestWithRetries returned error", localIOException);
      }
    }
    if (isCancelled()) {
      return;
    }
    this.responseHandler.sendFinishMessage();
    if (isCancelled()) {
      return;
    }
    onPostProcessRequest(this);
    this.isFinished = true;
  }
  
  public AsyncHttpRequest setRequestTag(Object paramObject)
  {
    this.responseHandler.setTag(paramObject);
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/AsyncHttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */