package com.loopj.android.http;

import cz.msebera.android.httpclient.Header;

public abstract class BaseJsonHttpResponseHandler<JSON_TYPE>
  extends TextHttpResponseHandler
{
  private static final String LOG_TAG = "BaseJsonHttpRH";
  
  public BaseJsonHttpResponseHandler()
  {
    this("UTF-8");
  }
  
  public BaseJsonHttpResponseHandler(String paramString)
  {
    super(paramString);
  }
  
  public final void onFailure(final int paramInt, final Header[] paramArrayOfHeader, final String paramString, final Throwable paramThrowable)
  {
    if (paramString != null)
    {
      paramArrayOfHeader = new Runnable()
      {
        public void run()
        {
          try
          {
            Object localObject = BaseJsonHttpResponseHandler.this.parseResponse(paramString, true);
            BaseJsonHttpResponseHandler localBaseJsonHttpResponseHandler = BaseJsonHttpResponseHandler.this;
            Runnable local1 = new com/loopj/android/http/BaseJsonHttpResponseHandler$2$1;
            local1.<init>(this, localObject);
            localBaseJsonHttpResponseHandler.postRunnable(local1);
          }
          catch (Throwable localThrowable)
          {
            AsyncHttpClient.log.d("BaseJsonHttpRH", "parseResponse thrown an problem", localThrowable);
            BaseJsonHttpResponseHandler.this.postRunnable(new Runnable()
            {
              public void run()
              {
                BaseJsonHttpResponseHandler.this.onFailure(BaseJsonHttpResponseHandler.2.this.val$statusCode, BaseJsonHttpResponseHandler.2.this.val$headers, BaseJsonHttpResponseHandler.2.this.val$throwable, BaseJsonHttpResponseHandler.2.this.val$responseString, null);
              }
            });
          }
        }
      };
      if ((!getUseSynchronousMode()) && (!getUsePoolThread())) {
        new Thread(paramArrayOfHeader).start();
      } else {
        paramArrayOfHeader.run();
      }
    }
    else
    {
      onFailure(paramInt, paramArrayOfHeader, paramThrowable, null, null);
    }
  }
  
  public abstract void onFailure(int paramInt, Header[] paramArrayOfHeader, Throwable paramThrowable, String paramString, JSON_TYPE paramJSON_TYPE);
  
  public final void onSuccess(final int paramInt, final Header[] paramArrayOfHeader, final String paramString)
  {
    if (paramInt != 204)
    {
      paramArrayOfHeader = new Runnable()
      {
        public void run()
        {
          try
          {
            Object localObject = BaseJsonHttpResponseHandler.this.parseResponse(paramString, false);
            BaseJsonHttpResponseHandler localBaseJsonHttpResponseHandler = BaseJsonHttpResponseHandler.this;
            Runnable local1 = new com/loopj/android/http/BaseJsonHttpResponseHandler$1$1;
            local1.<init>(this, localObject);
            localBaseJsonHttpResponseHandler.postRunnable(local1);
          }
          catch (Throwable localThrowable)
          {
            AsyncHttpClient.log.d("BaseJsonHttpRH", "parseResponse thrown an problem", localThrowable);
            BaseJsonHttpResponseHandler.this.postRunnable(new Runnable()
            {
              public void run()
              {
                BaseJsonHttpResponseHandler.this.onFailure(BaseJsonHttpResponseHandler.1.this.val$statusCode, BaseJsonHttpResponseHandler.1.this.val$headers, localThrowable, BaseJsonHttpResponseHandler.1.this.val$responseString, null);
              }
            });
          }
        }
      };
      if ((!getUseSynchronousMode()) && (!getUsePoolThread())) {
        new Thread(paramArrayOfHeader).start();
      } else {
        paramArrayOfHeader.run();
      }
    }
    else
    {
      onSuccess(paramInt, paramArrayOfHeader, null, null);
    }
  }
  
  public abstract void onSuccess(int paramInt, Header[] paramArrayOfHeader, String paramString, JSON_TYPE paramJSON_TYPE);
  
  protected abstract JSON_TYPE parseResponse(String paramString, boolean paramBoolean)
    throws Throwable;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/com/loopj/android/http/BaseJsonHttpResponseHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */