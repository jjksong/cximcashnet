package demo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

final class Pinyin4jAppletDemo$3
  implements ActionListener
{
  Pinyin4jAppletDemo$3(Pinyin4jAppletDemo paramPinyin4jAppletDemo)
  {
    this.this$0 = paramPinyin4jAppletDemo;
  }
  
  public void actionPerformed(ActionEvent paramActionEvent)
  {
    boolean bool;
    if (this.this$0.toneTypes[2] == (String)Pinyin4jAppletDemo.access$100(this.this$0).getSelectedItem())
    {
      Pinyin4jAppletDemo.access$200(this.this$0).setSelectedIndex(2);
      paramActionEvent = Pinyin4jAppletDemo.access$200(this.this$0);
      bool = false;
    }
    else
    {
      paramActionEvent = Pinyin4jAppletDemo.access$200(this.this$0);
      bool = true;
    }
    paramActionEvent.setEnabled(bool);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/demo/Pinyin4jAppletDemo$3.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */