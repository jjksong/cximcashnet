package lsp.com.lib;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View.MeasureSpec;
import android.view.inputmethod.InputMethodManager;
import java.util.ArrayList;
import java.util.List;

public class PasswordInputEdt
  extends AppCompatEditText
{
  private int heightSpace;
  private boolean isAutoCloseKeyBoard = true;
  private boolean isBgFill;
  private boolean isFocus = false;
  private boolean isNumber;
  private boolean isPwd;
  private List<Rect> list = new ArrayList();
  private int numLength;
  private onInputOverListener onInputOverListener;
  private PwdType pwdType;
  private int pwdType_CircleRadius;
  private int rectChooseColor;
  private int rectNormalColor;
  private Paint rectPaint;
  private int rectStroke;
  private String text = "";
  private int textColor;
  private Paint textPaint;
  private Rect textRect;
  private int txtSize;
  private int widthSpace;
  
  public PasswordInputEdt(Context paramContext)
  {
    super(paramContext);
    setAttr(null, 0);
    init();
  }
  
  public PasswordInputEdt(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setAttr(paramAttributeSet, 0);
    init();
  }
  
  public PasswordInputEdt(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setAttr(paramAttributeSet, paramInt);
    init();
  }
  
  private void init()
  {
    this.rectPaint = new Paint();
    this.textPaint = new Paint();
    this.textRect = new Rect();
    setBackgroundDrawable(null);
    setLongClickable(false);
    setTextIsSelectable(false);
    setCursorVisible(false);
    this.textPaint.setStyle(Paint.Style.FILL);
  }
  
  private void setAttr(AttributeSet paramAttributeSet, int paramInt)
  {
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.PasswordInputEdt, paramInt, 0);
    this.isPwd = localTypedArray.getBoolean(R.styleable.PasswordInputEdt_isPwd, true);
    this.isAutoCloseKeyBoard = localTypedArray.getBoolean(R.styleable.PasswordInputEdt_autoCloseKeyBoard, true);
    this.isNumber = localTypedArray.getBoolean(R.styleable.PasswordInputEdt_isNumber, true);
    this.widthSpace = localTypedArray.getDimensionPixelSize(R.styleable.PasswordInputEdt_widthSpace, (int)TypedValue.applyDimension(1, 5.0F, getResources().getDisplayMetrics()));
    this.pwdType_CircleRadius = localTypedArray.getDimensionPixelSize(R.styleable.PasswordInputEdt_circleRadius, (int)TypedValue.applyDimension(1, 5.0F, getResources().getDisplayMetrics()));
    this.heightSpace = localTypedArray.getDimensionPixelSize(R.styleable.PasswordInputEdt_heightSpace, (int)TypedValue.applyDimension(1, 5.0F, getResources().getDisplayMetrics()));
    this.rectStroke = localTypedArray.getDimensionPixelSize(R.styleable.PasswordInputEdt_rectStroke, (int)TypedValue.applyDimension(1, 2.0F, getResources().getDisplayMetrics()));
    this.txtSize = localTypedArray.getDimensionPixelSize(R.styleable.PasswordInputEdt_txtSize, (int)TypedValue.applyDimension(2, 18.0F, getResources().getDisplayMetrics()));
    this.isBgFill = localTypedArray.getBoolean(R.styleable.PasswordInputEdt_bgFill, false);
    this.numLength = localTypedArray.getInt(R.styleable.PasswordInputEdt_numLength, 6);
    this.textColor = localTypedArray.getColor(R.styleable.PasswordInputEdt_textColor, -10066330);
    this.rectNormalColor = localTypedArray.getColor(R.styleable.PasswordInputEdt_rectNormalColor, -8355712);
    this.rectChooseColor = localTypedArray.getColor(R.styleable.PasswordInputEdt_rectChooseColor, -12267935);
    if (localTypedArray.getInt(R.styleable.PasswordInputEdt_pwdType, 0) == 0) {
      paramAttributeSet = PwdType.CIRCLE;
    } else {
      paramAttributeSet = PwdType.XINGHAO;
    }
    this.pwdType = paramAttributeSet;
    localTypedArray.recycle();
  }
  
  public void closeKeybord()
  {
    ((InputMethodManager)getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
  }
  
  public boolean isBgFill()
  {
    return this.isBgFill;
  }
  
  public boolean isFocus()
  {
    return this.isFocus;
  }
  
  public boolean isPwd()
  {
    return this.isPwd;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (!this.isBgFill) {
      this.rectPaint.setStyle(Paint.Style.STROKE);
    }
    this.rectPaint.setStrokeWidth(this.rectStroke);
    this.textPaint.setColor(this.textColor);
    this.textPaint.setTextSize(this.txtSize);
    if (this.isNumber) {
      setInputType(2);
    }
    int j = Math.min(getMeasuredHeight(), getMeasuredWidth() / this.numLength);
    Object localObject;
    for (int i = 0; i < this.numLength; i++)
    {
      if ((i <= this.text.length()) && (this.isFocus)) {
        this.rectPaint.setColor(this.rectChooseColor);
      } else {
        this.rectPaint.setColor(this.rectNormalColor);
      }
      int k = i * j;
      int m = this.widthSpace;
      int n = this.heightSpace;
      localObject = new Rect(k + m, n, k + j - m, j - n);
      paramCanvas.drawRect((Rect)localObject, this.rectPaint);
      this.list.add(localObject);
    }
    for (i = 0; i < this.text.length(); i++) {
      if (this.isPwd)
      {
        switch (this.pwdType)
        {
        default: 
          break;
        case ???: 
          this.textPaint.getTextBounds("*", 0, 1, this.textRect);
          paramCanvas.drawText("*", ((Rect)this.list.get(i)).left + (((Rect)this.list.get(i)).right - ((Rect)this.list.get(i)).left) / 2 - this.textRect.width() / 2, ((Rect)this.list.get(i)).top + (((Rect)this.list.get(i)).bottom - ((Rect)this.list.get(i)).top) / 2 + this.textRect.height(), this.textPaint);
          break;
        case ???: 
          paramCanvas.drawCircle(((Rect)this.list.get(i)).centerX(), ((Rect)this.list.get(i)).centerY(), this.pwdType_CircleRadius, this.textPaint);
          break;
        }
      }
      else
      {
        localObject = this.textPaint;
        String str = this.text;
        j = i + 1;
        ((Paint)localObject).getTextBounds(str.substring(i, j), 0, 1, this.textRect);
        paramCanvas.drawText(this.text.substring(i, j), ((Rect)this.list.get(i)).left + (((Rect)this.list.get(i)).right - ((Rect)this.list.get(i)).left) / 2 - this.textRect.width() / 2, ((Rect)this.list.get(i)).top + (((Rect)this.list.get(i)).bottom - ((Rect)this.list.get(i)).top) / 2 + this.textRect.height() / 2, this.textPaint);
      }
    }
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
  {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    this.isFocus = paramBoolean;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 67) && (this.text.length() != 0))
    {
      String str = this.text;
      this.text = str.substring(0, str.length() - 1);
      invalidate();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int k = View.MeasureSpec.getMode(paramInt2);
    int i = View.MeasureSpec.getSize(paramInt2);
    int j = View.MeasureSpec.getSize(paramInt1);
    if (k != Integer.MIN_VALUE)
    {
      if (k != 1073741824) {
        paramInt1 = i;
      } else {
        paramInt1 = View.MeasureSpec.getSize(paramInt2);
      }
    }
    else {
      paramInt1 = j / this.numLength;
    }
    setMeasuredDimension(j, paramInt1);
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    super.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    Object localObject = this.text;
    if (localObject == null) {
      return;
    }
    if (((String)localObject).length() < this.numLength)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(this.text);
      ((StringBuilder)localObject).append(paramCharSequence.toString());
      this.text = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = this.onInputOverListener;
      if (localObject != null)
      {
        ((onInputOverListener)localObject).onInputOver(this.text);
        if (this.isAutoCloseKeyBoard) {
          closeKeybord();
        }
      }
    }
    if (paramCharSequence.toString().length() != 0) {
      setText("");
    }
  }
  
  public void setFocus(boolean paramBoolean)
  {
    this.isFocus = paramBoolean;
  }
  
  public void setHeightSpace(int paramInt)
  {
    this.heightSpace = paramInt;
  }
  
  public void setIsBgFill(boolean paramBoolean)
  {
    this.isBgFill = paramBoolean;
  }
  
  public void setIsNumber(boolean paramBoolean)
  {
    this.isNumber = paramBoolean;
  }
  
  public void setIsPwd(boolean paramBoolean)
  {
    this.isPwd = paramBoolean;
  }
  
  public void setNumLength(int paramInt)
  {
    this.numLength = paramInt;
  }
  
  public void setOnInputOverListener(onInputOverListener paramonInputOverListener)
  {
    this.onInputOverListener = paramonInputOverListener;
  }
  
  public void setPwdType(PwdType paramPwdType)
  {
    this.pwdType = paramPwdType;
  }
  
  public void setRectChooseColor(int paramInt)
  {
    this.rectChooseColor = paramInt;
  }
  
  public void setRectNormalColor(int paramInt)
  {
    this.rectNormalColor = paramInt;
  }
  
  public void setRectStroke(int paramInt)
  {
    this.rectStroke = paramInt;
  }
  
  public void setTextColor(int paramInt)
  {
    this.textColor = paramInt;
  }
  
  public void setTxtSize(int paramInt)
  {
    this.txtSize = paramInt;
  }
  
  public void setWidthSpace(int paramInt)
  {
    this.widthSpace = paramInt;
  }
  
  public static enum PwdType
  {
    CIRCLE,  XINGHAO;
    
    private PwdType() {}
  }
  
  public static abstract interface onInputOverListener
  {
    public abstract void onInputOver(String paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/lsp/com/lib/PasswordInputEdt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */