package cn.jiguang.net;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;

public class HttpRequest
{
  private String a;
  private int b;
  private int c;
  private Map<String, String> d;
  private Map<String, String> e;
  private Object f;
  private boolean g;
  private boolean h;
  private boolean i;
  private boolean j = true;
  private boolean k = false;
  private boolean l = false;
  private SSLTrustManager m;
  private boolean n;
  private HostnameVerifier o;
  
  public HttpRequest(String paramString)
  {
    this.a = paramString;
    this.b = -1;
    this.c = -1;
    this.e = new HashMap();
  }
  
  public HttpRequest(String paramString, Map<String, String> paramMap)
  {
    this.a = paramString;
    this.d = paramMap;
    this.b = -1;
    this.c = -1;
    this.e = new HashMap();
  }
  
  public Object getBody()
  {
    return this.f;
  }
  
  public int getConnectTimeout()
  {
    return this.b;
  }
  
  public HostnameVerifier getHostnameVerifier()
  {
    return this.o;
  }
  
  public byte[] getParas()
  {
    Object localObject = this.f;
    if (localObject != null) {
      if ((localObject instanceof String))
      {
        if (!TextUtils.isEmpty((CharSequence)localObject)) {
          return ((String)this.f).getBytes();
        }
      }
      else if ((localObject instanceof byte[])) {
        return (byte[])localObject;
      }
    }
    localObject = HttpUtils.joinParasWithEncodedValue(this.d);
    if (!TextUtils.isEmpty((CharSequence)localObject)) {
      return ((String)localObject).getBytes();
    }
    return null;
  }
  
  public Map<String, String> getParasMap()
  {
    return this.d;
  }
  
  public int getReadTimeout()
  {
    return this.c;
  }
  
  public Map<String, String> getRequestProperties()
  {
    return this.e;
  }
  
  public String getRequestProperty(String paramString)
  {
    return (String)this.e.get(paramString);
  }
  
  public SSLTrustManager getSslTrustManager()
  {
    return this.m;
  }
  
  public String getUrl()
  {
    return this.a;
  }
  
  public boolean isDoInPut()
  {
    return this.h;
  }
  
  public boolean isDoOutPut()
  {
    return this.g;
  }
  
  public boolean isHaveRspData()
  {
    return this.j;
  }
  
  public boolean isNeedErrorInput()
  {
    return this.l;
  }
  
  public boolean isNeedRetryIfHttpsFailed()
  {
    return this.n;
  }
  
  public boolean isRspDatazip()
  {
    return this.k;
  }
  
  public boolean isUseCaches()
  {
    return this.i;
  }
  
  public void setBody(Object paramObject)
  {
    this.f = paramObject;
  }
  
  public void setConnectTimeout(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.b = paramInt;
      return;
    }
    throw new IllegalArgumentException("timeout can not be negative");
  }
  
  public void setDoInPut(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public void setDoOutPut(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public void setHaveRspData(boolean paramBoolean)
  {
    this.j = paramBoolean;
  }
  
  public void setHostnameVerifier(HostnameVerifier paramHostnameVerifier)
  {
    this.o = paramHostnameVerifier;
  }
  
  public void setNeedErrorInput(boolean paramBoolean)
  {
    this.l = paramBoolean;
  }
  
  public void setNeedRetryIfHttpsFailed(boolean paramBoolean)
  {
    this.n = paramBoolean;
  }
  
  public void setParasMap(Map<String, String> paramMap)
  {
    this.d = paramMap;
  }
  
  public void setReadTimeout(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.c = paramInt;
      return;
    }
    throw new IllegalArgumentException("timeout can not be negative");
  }
  
  public void setRequestProperties(Map<String, String> paramMap)
  {
    this.e = paramMap;
  }
  
  public void setRequestProperty(String paramString1, String paramString2)
  {
    this.e.put(paramString1, paramString2);
  }
  
  public void setRspDatazip(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  public void setSslTrustManager(SSLTrustManager paramSSLTrustManager)
  {
    this.m = paramSSLTrustManager;
  }
  
  public void setUrl(String paramString)
  {
    this.a = paramString;
  }
  
  public void setUseCaches(boolean paramBoolean)
  {
    this.i = paramBoolean;
  }
  
  public void setUserAgent(String paramString)
  {
    this.e.put("User-Agent", paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/net/HttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */