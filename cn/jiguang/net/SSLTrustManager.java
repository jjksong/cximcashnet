package cn.jiguang.net;

import cn.jiguang.ac.c;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.TrustedCertificateEntry;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SSLTrustManager
  implements X509TrustManager
{
  private X509TrustManager a;
  
  public SSLTrustManager(String paramString)
  {
    try
    {
      CertificateFactory localCertificateFactory = CertificateFactory.getInstance("X.509");
      Object localObject = new java/io/ByteArrayInputStream;
      ((ByteArrayInputStream)localObject).<init>(paramString.getBytes());
      paramString = (X509Certificate)localCertificateFactory.generateCertificate((InputStream)localObject);
      ((InputStream)localObject).close();
      localObject = new java/security/KeyStore$TrustedCertificateEntry;
      ((KeyStore.TrustedCertificateEntry)localObject).<init>(paramString);
      paramString = KeyStore.getInstance(KeyStore.getDefaultType());
      paramString.load(null, null);
      paramString.setEntry("ca_root", (KeyStore.Entry)localObject, null);
      localObject = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      ((TrustManagerFactory)localObject).init(paramString);
      for (paramString : ((TrustManagerFactory)localObject).getTrustManagers()) {
        if ((paramString instanceof X509TrustManager))
        {
          this.a = ((X509TrustManager)paramString);
          return;
        }
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      paramString = new StringBuilder();
      paramString.append("init trustManager failed, error:");
      paramString.append(localThrowable);
      c.f("SSLTrustManager", paramString.toString());
    }
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    c.b("SSLTrustManager", "checkClientTrusted");
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    c.b("SSLTrustManager", "checkServerTrusted");
    if ((paramArrayOfX509Certificate != null) && (paramArrayOfX509Certificate.length != 0) && (paramArrayOfX509Certificate[0] != null))
    {
      try
      {
        paramArrayOfX509Certificate[0].checkValidity();
      }
      catch (Throwable paramString)
      {
        paramArrayOfX509Certificate = new StringBuilder();
        paramArrayOfX509Certificate.append("checkServerTrusted failed, error");
        paramString = paramString.getLocalizedMessage();
      }
      catch (CertificateNotYetValidException paramString)
      {
        paramArrayOfX509Certificate = new StringBuilder();
        paramArrayOfX509Certificate.append("checkServerTrusted: CertificateNotYetValidException:");
        paramString = paramString.getLocalizedMessage();
      }
      catch (CertificateExpiredException paramString)
      {
        paramArrayOfX509Certificate = new StringBuilder();
        paramArrayOfX509Certificate.append("checkServerTrusted: CertificateExpiredException:");
        paramString = paramString.getLocalizedMessage();
      }
      paramArrayOfX509Certificate.append(paramString);
      c.f("SSLTrustManager", paramArrayOfX509Certificate.toString());
      return;
    }
    throw new CertificateException("Check Server x509Certificates is empty");
  }
  
  public X509Certificate[] getAcceptedIssuers()
  {
    c.b("SSLTrustManager", "getAcceptedIssuers");
    return this.a.getAcceptedIssuers();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/net/SSLTrustManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */