package cn.jiguang.net;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class HttpResponse
{
  private String a;
  private String b;
  private Map<String, Object> c;
  private int d;
  private long e;
  private boolean f;
  private boolean g;
  private int h = -1;
  
  public HttpResponse()
  {
    this.c = new HashMap();
  }
  
  public HttpResponse(String paramString)
  {
    this.a = paramString;
    this.d = 0;
    this.f = false;
    this.g = false;
    this.c = new HashMap();
  }
  
  private int a()
  {
    int j = -1;
    try
    {
      String str = (String)this.c.get("cache-control");
      int i = j;
      if (!TextUtils.isEmpty(str))
      {
        int k = str.indexOf("max-age=");
        i = j;
        if (k != -1)
        {
          i = str.indexOf(",", k);
          if (i != -1) {
            str = str.substring(k + 8, i);
          } else {
            str = str.substring(k + 8);
          }
          i = Integer.parseInt(str);
        }
      }
      return i;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return -1;
  }
  
  private long b()
  {
    int i = a();
    if (i != -1) {
      return System.currentTimeMillis() + i * 1000;
    }
    if (!TextUtils.isEmpty(getExpiresHeader())) {
      return HttpUtils.parseGmtTime(getExpiresHeader());
    }
    return -1L;
  }
  
  public long getExpiredTime()
  {
    if (this.g) {
      return this.e;
    }
    this.g = true;
    long l = b();
    this.e = l;
    return l;
  }
  
  public String getExpiresHeader()
  {
    String str = null;
    try
    {
      if (this.c != null) {
        str = (String)this.c.get("expires");
      }
      return str;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }
  
  public String getResponseBody()
  {
    return this.b;
  }
  
  public int getResponseCode()
  {
    return this.h;
  }
  
  public int getType()
  {
    return this.d;
  }
  
  public String getUrl()
  {
    return this.a;
  }
  
  public boolean isExpired()
  {
    boolean bool;
    if (System.currentTimeMillis() > this.e) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isInCache()
  {
    return this.f;
  }
  
  public void setExpiredTime(long paramLong)
  {
    this.g = true;
    this.e = paramLong;
  }
  
  public HttpResponse setInCache(boolean paramBoolean)
  {
    this.f = paramBoolean;
    return this;
  }
  
  public void setResponseBody(String paramString)
  {
    this.b = paramString;
  }
  
  public void setResponseCode(int paramInt)
  {
    this.h = paramInt;
  }
  
  public void setResponseHeader(String paramString1, String paramString2)
  {
    Map localMap = this.c;
    if (localMap != null) {
      localMap.put(paramString1, paramString2);
    }
  }
  
  public void setResponseHeaders(Map<String, Object> paramMap)
  {
    this.c = paramMap;
  }
  
  public void setType(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.d = paramInt;
      return;
    }
    throw new IllegalArgumentException("The type of HttpResponse cannot be smaller than 0.");
  }
  
  public void setUrl(String paramString)
  {
    this.a = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("HttpResponse{responseBody='");
    localStringBuilder.append(this.b);
    localStringBuilder.append('\'');
    localStringBuilder.append(", responseCode=");
    localStringBuilder.append(this.h);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/net/HttpResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */