package cn.jiguang.net;

import android.text.TextUtils;
import cn.jiguang.ac.c;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class DefaultHostVerifier
  implements HostnameVerifier
{
  public String a = null;
  
  public DefaultHostVerifier(String paramString)
  {
    this.a = paramString;
  }
  
  public boolean verify(String paramString, SSLSession paramSSLSession)
  {
    paramSSLSession = new StringBuilder();
    paramSSLSession.append("host:");
    paramSSLSession.append(paramString);
    paramSSLSession.append(",checkHost:");
    paramSSLSession.append(this.a);
    c.b("DefaultHostVerifier", paramSSLSession.toString());
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    return TextUtils.equals(this.a, paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/net/DefaultHostVerifier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */