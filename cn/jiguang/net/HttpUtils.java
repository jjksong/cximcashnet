package cn.jiguang.net;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.TextUtils;
import cn.jiguang.as.b;
import cn.jiguang.as.e;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HttpUtils
{
  /* Error */
  private static HttpResponse a(Context paramContext, HttpRequest paramHttpRequest, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 15
    //   3: aconst_null
    //   4: astore 16
    //   6: aconst_null
    //   7: astore 17
    //   9: aconst_null
    //   10: astore 18
    //   12: aconst_null
    //   13: astore 19
    //   15: aconst_null
    //   16: astore 9
    //   18: aconst_null
    //   19: astore 8
    //   21: aconst_null
    //   22: astore 7
    //   24: aconst_null
    //   25: astore 10
    //   27: aconst_null
    //   28: astore 11
    //   30: aconst_null
    //   31: astore 14
    //   33: aload_1
    //   34: ifnonnull +5 -> 39
    //   37: aconst_null
    //   38: areturn
    //   39: new 31	cn/jiguang/net/HttpResponse
    //   42: dup
    //   43: aload_1
    //   44: invokevirtual 37	cn/jiguang/net/HttpRequest:getUrl	()Ljava/lang/String;
    //   47: invokespecial 40	cn/jiguang/net/HttpResponse:<init>	(Ljava/lang/String;)V
    //   50: astore 20
    //   52: aload_0
    //   53: aload_1
    //   54: invokevirtual 37	cn/jiguang/net/HttpRequest:getUrl	()Ljava/lang/String;
    //   57: invokestatic 44	cn/jiguang/net/HttpUtils:getHttpURLConnectionWithProxy	(Landroid/content/Context;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   60: astore_0
    //   61: aload_0
    //   62: invokevirtual 50	java/net/HttpURLConnection:getURL	()Ljava/net/URL;
    //   65: astore 6
    //   67: aload 6
    //   69: invokevirtual 56	java/net/URL:getPort	()I
    //   72: istore_3
    //   73: new 58	java/lang/StringBuilder
    //   76: astore 5
    //   78: aload 5
    //   80: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   83: aload 5
    //   85: ldc 61
    //   87: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload 5
    //   93: aload 6
    //   95: invokevirtual 68	java/net/URL:getHost	()Ljava/lang/String;
    //   98: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload 5
    //   104: ldc 70
    //   106: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: aload 5
    //   112: iload_3
    //   113: invokevirtual 73	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   116: pop
    //   117: ldc 75
    //   119: aload 5
    //   121: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   124: invokestatic 83	cn/jiguang/ac/c:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   127: aload_1
    //   128: aload_0
    //   129: invokestatic 86	cn/jiguang/net/HttpUtils:a	(Lcn/jiguang/net/HttpRequest;Ljava/net/HttpURLConnection;)V
    //   132: aload_0
    //   133: instanceof 88
    //   136: istore 4
    //   138: iload 4
    //   140: ifeq +162 -> 302
    //   143: aload_1
    //   144: invokevirtual 92	cn/jiguang/net/HttpRequest:getSslTrustManager	()Lcn/jiguang/net/SSLTrustManager;
    //   147: ifnull +60 -> 207
    //   150: ldc 94
    //   152: invokestatic 100	javax/net/ssl/SSLContext:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    //   155: astore 6
    //   157: aload_1
    //   158: invokevirtual 92	cn/jiguang/net/HttpRequest:getSslTrustManager	()Lcn/jiguang/net/SSLTrustManager;
    //   161: astore 12
    //   163: new 102	java/security/SecureRandom
    //   166: astore 5
    //   168: aload 5
    //   170: invokespecial 103	java/security/SecureRandom:<init>	()V
    //   173: aload 6
    //   175: aconst_null
    //   176: iconst_1
    //   177: anewarray 105	javax/net/ssl/TrustManager
    //   180: dup
    //   181: iconst_0
    //   182: aload 12
    //   184: aastore
    //   185: aload 5
    //   187: invokevirtual 109	javax/net/ssl/SSLContext:init	([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    //   190: aload 6
    //   192: ifnull +15 -> 207
    //   195: aload_0
    //   196: checkcast 88	javax/net/ssl/HttpsURLConnection
    //   199: aload 6
    //   201: invokevirtual 113	javax/net/ssl/SSLContext:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   204: invokevirtual 117	javax/net/ssl/HttpsURLConnection:setSSLSocketFactory	(Ljavax/net/ssl/SSLSocketFactory;)V
    //   207: aload_1
    //   208: invokevirtual 121	cn/jiguang/net/HttpRequest:getHostnameVerifier	()Ljavax/net/ssl/HostnameVerifier;
    //   211: ifnull +25 -> 236
    //   214: aload_0
    //   215: checkcast 88	javax/net/ssl/HttpsURLConnection
    //   218: astore 5
    //   220: aload_1
    //   221: invokevirtual 121	cn/jiguang/net/HttpRequest:getHostnameVerifier	()Ljavax/net/ssl/HostnameVerifier;
    //   224: astore 6
    //   226: aload 5
    //   228: aload 6
    //   230: invokevirtual 125	javax/net/ssl/HttpsURLConnection:setHostnameVerifier	(Ljavax/net/ssl/HostnameVerifier;)V
    //   233: goto +69 -> 302
    //   236: aload_0
    //   237: checkcast 88	javax/net/ssl/HttpsURLConnection
    //   240: astore 5
    //   242: new 127	cn/jiguang/net/DefaultHostVerifier
    //   245: dup
    //   246: aload_0
    //   247: invokevirtual 50	java/net/HttpURLConnection:getURL	()Ljava/net/URL;
    //   250: invokevirtual 68	java/net/URL:getHost	()Ljava/lang/String;
    //   253: invokespecial 128	cn/jiguang/net/DefaultHostVerifier:<init>	(Ljava/lang/String;)V
    //   256: astore 6
    //   258: goto -32 -> 226
    //   261: astore 5
    //   263: new 58	java/lang/StringBuilder
    //   266: astore 6
    //   268: aload 6
    //   270: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   273: aload 6
    //   275: ldc -126
    //   277: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   280: pop
    //   281: aload 6
    //   283: aload 5
    //   285: invokevirtual 133	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   288: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   291: pop
    //   292: ldc 75
    //   294: aload 6
    //   296: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   299: invokestatic 136	cn/jiguang/ac/c:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   302: iload_2
    //   303: ifeq +39 -> 342
    //   306: aload_0
    //   307: ldc -118
    //   309: invokevirtual 141	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   312: aload_0
    //   313: iconst_1
    //   314: invokevirtual 145	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   317: aload_0
    //   318: iconst_1
    //   319: invokevirtual 148	java/net/HttpURLConnection:setDoInput	(Z)V
    //   322: aload_1
    //   323: invokevirtual 152	cn/jiguang/net/HttpRequest:getParas	()[B
    //   326: astore 5
    //   328: aload 5
    //   330: ifnull +12 -> 342
    //   333: aload_0
    //   334: invokevirtual 156	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   337: aload 5
    //   339: invokevirtual 162	java/io/OutputStream:write	([B)V
    //   342: aload_0
    //   343: invokevirtual 165	java/net/HttpURLConnection:getResponseCode	()I
    //   346: istore_3
    //   347: aload 20
    //   349: iload_3
    //   350: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   353: aload_0
    //   354: invokevirtual 173	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   357: astore 5
    //   359: goto +47 -> 406
    //   362: astore 5
    //   364: new 58	java/lang/StringBuilder
    //   367: astore 6
    //   369: aload 6
    //   371: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   374: aload 6
    //   376: ldc -81
    //   378: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   381: pop
    //   382: aload 6
    //   384: aload 5
    //   386: invokevirtual 133	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   389: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   392: pop
    //   393: ldc 75
    //   395: aload 6
    //   397: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   400: invokestatic 177	cn/jiguang/ac/c:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   403: aconst_null
    //   404: astore 5
    //   406: aload 15
    //   408: astore 7
    //   410: aload 16
    //   412: astore 11
    //   414: aload 17
    //   416: astore 10
    //   418: aload 18
    //   420: astore 9
    //   422: aload 19
    //   424: astore 8
    //   426: aload_1
    //   427: invokevirtual 181	cn/jiguang/net/HttpRequest:isHaveRspData	()Z
    //   430: istore_2
    //   431: iload_2
    //   432: ifeq +114 -> 546
    //   435: aload 5
    //   437: ifnull +109 -> 546
    //   440: aload 15
    //   442: astore 7
    //   444: aload 16
    //   446: astore 11
    //   448: aload 17
    //   450: astore 10
    //   452: aload 18
    //   454: astore 9
    //   456: aload 19
    //   458: astore 8
    //   460: aload 5
    //   462: invokestatic 185	cn/jiguang/net/HttpUtils:readInputStream	(Ljava/io/InputStream;)[B
    //   465: astore 6
    //   467: aload 6
    //   469: astore 12
    //   471: aload 6
    //   473: ifnull +76 -> 549
    //   476: aload 6
    //   478: astore 12
    //   480: aload 15
    //   482: astore 7
    //   484: aload 16
    //   486: astore 11
    //   488: aload 17
    //   490: astore 10
    //   492: aload 18
    //   494: astore 9
    //   496: aload 19
    //   498: astore 8
    //   500: aload_1
    //   501: invokevirtual 188	cn/jiguang/net/HttpRequest:isRspDatazip	()Z
    //   504: ifeq +45 -> 549
    //   507: aload 15
    //   509: astore 7
    //   511: aload 16
    //   513: astore 11
    //   515: aload 17
    //   517: astore 10
    //   519: aload 18
    //   521: astore 9
    //   523: aload 19
    //   525: astore 8
    //   527: aload 6
    //   529: invokestatic 193	cn/jiguang/as/e:b	([B)[B
    //   532: astore 12
    //   534: goto +15 -> 549
    //   537: astore 7
    //   539: aload 6
    //   541: astore 12
    //   543: goto +6 -> 549
    //   546: aconst_null
    //   547: astore 12
    //   549: aload 14
    //   551: astore 6
    //   553: aload 12
    //   555: astore 13
    //   557: aload 12
    //   559: ifnonnull +106 -> 665
    //   562: aload 14
    //   564: astore 6
    //   566: aload 12
    //   568: astore 13
    //   570: iload_3
    //   571: sipush 200
    //   574: if_icmpeq +91 -> 665
    //   577: aload 14
    //   579: astore 6
    //   581: aload 12
    //   583: astore 13
    //   585: aload 15
    //   587: astore 7
    //   589: aload 16
    //   591: astore 11
    //   593: aload 17
    //   595: astore 10
    //   597: aload 18
    //   599: astore 9
    //   601: aload 19
    //   603: astore 8
    //   605: aload_1
    //   606: invokevirtual 196	cn/jiguang/net/HttpRequest:isNeedErrorInput	()Z
    //   609: ifeq +56 -> 665
    //   612: aload 15
    //   614: astore 7
    //   616: aload 16
    //   618: astore 11
    //   620: aload 17
    //   622: astore 10
    //   624: aload 18
    //   626: astore 9
    //   628: aload 19
    //   630: astore 8
    //   632: aload_0
    //   633: invokevirtual 199	java/net/HttpURLConnection:getErrorStream	()Ljava/io/InputStream;
    //   636: astore 6
    //   638: aload 6
    //   640: astore 7
    //   642: aload 6
    //   644: astore 11
    //   646: aload 6
    //   648: astore 10
    //   650: aload 6
    //   652: astore 9
    //   654: aload 6
    //   656: astore 8
    //   658: aload 6
    //   660: invokestatic 185	cn/jiguang/net/HttpUtils:readInputStream	(Ljava/io/InputStream;)[B
    //   663: astore 13
    //   665: aload 13
    //   667: ifnull +81 -> 748
    //   670: aload 6
    //   672: astore 7
    //   674: aload 6
    //   676: astore 11
    //   678: aload 6
    //   680: astore 10
    //   682: aload 6
    //   684: astore 9
    //   686: aload 6
    //   688: astore 8
    //   690: new 201	java/lang/String
    //   693: astore_1
    //   694: aload 6
    //   696: astore 7
    //   698: aload 6
    //   700: astore 11
    //   702: aload 6
    //   704: astore 10
    //   706: aload 6
    //   708: astore 9
    //   710: aload 6
    //   712: astore 8
    //   714: aload_1
    //   715: aload 13
    //   717: ldc -53
    //   719: invokespecial 206	java/lang/String:<init>	([BLjava/lang/String;)V
    //   722: aload 6
    //   724: astore 7
    //   726: aload 6
    //   728: astore 11
    //   730: aload 6
    //   732: astore 10
    //   734: aload 6
    //   736: astore 9
    //   738: aload 6
    //   740: astore 8
    //   742: aload 20
    //   744: aload_1
    //   745: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   748: aload 6
    //   750: astore 7
    //   752: aload 6
    //   754: astore 11
    //   756: aload 6
    //   758: astore 10
    //   760: aload 6
    //   762: astore 9
    //   764: aload 6
    //   766: astore 8
    //   768: aload_0
    //   769: aload 20
    //   771: invokestatic 212	cn/jiguang/net/HttpUtils:a	(Ljava/net/HttpURLConnection;Lcn/jiguang/net/HttpResponse;)V
    //   774: aload 5
    //   776: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   779: aload 6
    //   781: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   784: aload_0
    //   785: ifnull +872 -> 1657
    //   788: aload_0
    //   789: invokevirtual 218	java/net/HttpURLConnection:disconnect	()V
    //   792: goto +865 -> 1657
    //   795: astore_1
    //   796: aload_0
    //   797: astore 8
    //   799: aload 7
    //   801: astore_0
    //   802: aload 5
    //   804: astore 6
    //   806: aload 8
    //   808: astore 5
    //   810: goto +854 -> 1664
    //   813: astore_1
    //   814: aload_0
    //   815: astore_1
    //   816: aload 11
    //   818: astore_0
    //   819: aload 5
    //   821: astore 6
    //   823: goto +119 -> 942
    //   826: astore 9
    //   828: aload 10
    //   830: astore_1
    //   831: aload 5
    //   833: astore 8
    //   835: goto +166 -> 1001
    //   838: astore 6
    //   840: aload 9
    //   842: astore_1
    //   843: aload 5
    //   845: astore 8
    //   847: aload 6
    //   849: astore 9
    //   851: goto +369 -> 1220
    //   854: astore 9
    //   856: aload 8
    //   858: astore_1
    //   859: aload 5
    //   861: astore 8
    //   863: goto +653 -> 1516
    //   866: astore_1
    //   867: aload_0
    //   868: astore 5
    //   870: aconst_null
    //   871: astore_0
    //   872: aload 11
    //   874: astore 6
    //   876: goto +788 -> 1664
    //   879: astore_1
    //   880: aload_0
    //   881: astore_1
    //   882: aconst_null
    //   883: astore_0
    //   884: aload 9
    //   886: astore 6
    //   888: goto +54 -> 942
    //   891: astore 9
    //   893: aconst_null
    //   894: astore_1
    //   895: goto +106 -> 1001
    //   898: astore 9
    //   900: aconst_null
    //   901: astore_1
    //   902: aload 7
    //   904: astore 8
    //   906: goto +314 -> 1220
    //   909: astore 9
    //   911: aconst_null
    //   912: astore_1
    //   913: aload 10
    //   915: astore 8
    //   917: goto +599 -> 1516
    //   920: astore_1
    //   921: aconst_null
    //   922: astore_0
    //   923: aload_0
    //   924: astore 5
    //   926: aload 11
    //   928: astore 6
    //   930: goto +734 -> 1664
    //   933: astore_0
    //   934: aconst_null
    //   935: astore_0
    //   936: aload_0
    //   937: astore_1
    //   938: aload 9
    //   940: astore 6
    //   942: ldc 75
    //   944: ldc -36
    //   946: invokestatic 223	cn/jiguang/ac/c:f	(Ljava/lang/String;Ljava/lang/String;)V
    //   949: aload 20
    //   951: sipush 3007
    //   954: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   957: aload 20
    //   959: ldc -36
    //   961: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   964: aload 6
    //   966: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   969: aload_0
    //   970: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   973: aload_1
    //   974: ifnull +683 -> 1657
    //   977: aload_1
    //   978: invokevirtual 218	java/net/HttpURLConnection:disconnect	()V
    //   981: goto +676 -> 1657
    //   984: astore 7
    //   986: aload_1
    //   987: astore 5
    //   989: aload 7
    //   991: astore_1
    //   992: goto +672 -> 1664
    //   995: astore 9
    //   997: aconst_null
    //   998: astore_1
    //   999: aload_1
    //   1000: astore_0
    //   1001: aload 8
    //   1003: astore 6
    //   1005: aload_1
    //   1006: astore 7
    //   1008: aload_0
    //   1009: astore 5
    //   1011: new 58	java/lang/StringBuilder
    //   1014: astore 10
    //   1016: aload 8
    //   1018: astore 6
    //   1020: aload_1
    //   1021: astore 7
    //   1023: aload_0
    //   1024: astore 5
    //   1026: aload 10
    //   1028: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   1031: aload 8
    //   1033: astore 6
    //   1035: aload_1
    //   1036: astore 7
    //   1038: aload_0
    //   1039: astore 5
    //   1041: aload 10
    //   1043: ldc -31
    //   1045: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1048: pop
    //   1049: aload 8
    //   1051: astore 6
    //   1053: aload_1
    //   1054: astore 7
    //   1056: aload_0
    //   1057: astore 5
    //   1059: aload 10
    //   1061: aload 9
    //   1063: invokevirtual 228	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1066: pop
    //   1067: aload 8
    //   1069: astore 6
    //   1071: aload_1
    //   1072: astore 7
    //   1074: aload_0
    //   1075: astore 5
    //   1077: ldc 75
    //   1079: aload 10
    //   1081: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1084: invokestatic 223	cn/jiguang/ac/c:f	(Ljava/lang/String;Ljava/lang/String;)V
    //   1087: aload 8
    //   1089: astore 6
    //   1091: aload_1
    //   1092: astore 7
    //   1094: aload_0
    //   1095: astore 5
    //   1097: aload 20
    //   1099: sipush 3006
    //   1102: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1105: aload 8
    //   1107: astore 6
    //   1109: aload_1
    //   1110: astore 7
    //   1112: aload_0
    //   1113: astore 5
    //   1115: new 58	java/lang/StringBuilder
    //   1118: astore 10
    //   1120: aload 8
    //   1122: astore 6
    //   1124: aload_1
    //   1125: astore 7
    //   1127: aload_0
    //   1128: astore 5
    //   1130: aload 10
    //   1132: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   1135: aload 8
    //   1137: astore 6
    //   1139: aload_1
    //   1140: astore 7
    //   1142: aload_0
    //   1143: astore 5
    //   1145: aload 10
    //   1147: ldc -26
    //   1149: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1152: pop
    //   1153: aload 8
    //   1155: astore 6
    //   1157: aload_1
    //   1158: astore 7
    //   1160: aload_0
    //   1161: astore 5
    //   1163: aload 10
    //   1165: aload 9
    //   1167: invokevirtual 231	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1170: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1173: pop
    //   1174: aload 8
    //   1176: astore 6
    //   1178: aload_1
    //   1179: astore 7
    //   1181: aload_0
    //   1182: astore 5
    //   1184: aload 20
    //   1186: aload 10
    //   1188: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1191: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   1194: aload 8
    //   1196: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1199: aload_1
    //   1200: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1203: aload_0
    //   1204: ifnull +453 -> 1657
    //   1207: goto +446 -> 1653
    //   1210: astore 9
    //   1212: aconst_null
    //   1213: astore_1
    //   1214: aload_1
    //   1215: astore_0
    //   1216: aload 7
    //   1218: astore 8
    //   1220: aload 8
    //   1222: astore 6
    //   1224: aload_1
    //   1225: astore 7
    //   1227: aload_0
    //   1228: astore 5
    //   1230: aload 20
    //   1232: sipush 2998
    //   1235: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1238: aload 8
    //   1240: astore 6
    //   1242: aload_1
    //   1243: astore 7
    //   1245: aload_0
    //   1246: astore 5
    //   1248: aload 20
    //   1250: ldc -23
    //   1252: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   1255: aload 8
    //   1257: astore 6
    //   1259: aload_1
    //   1260: astore 7
    //   1262: aload_0
    //   1263: astore 5
    //   1265: aload 9
    //   1267: instanceof 235
    //   1270: ifeq +45 -> 1315
    //   1273: aload 8
    //   1275: astore 6
    //   1277: aload_1
    //   1278: astore 7
    //   1280: aload_0
    //   1281: astore 5
    //   1283: aload 20
    //   1285: sipush 3001
    //   1288: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1291: ldc -19
    //   1293: astore 10
    //   1295: aload 8
    //   1297: astore 6
    //   1299: aload_1
    //   1300: astore 7
    //   1302: aload_0
    //   1303: astore 5
    //   1305: aload 20
    //   1307: aload 10
    //   1309: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   1312: goto +89 -> 1401
    //   1315: aload 8
    //   1317: astore 6
    //   1319: aload_1
    //   1320: astore 7
    //   1322: aload_0
    //   1323: astore 5
    //   1325: aload 9
    //   1327: instanceof 239
    //   1330: ifeq +28 -> 1358
    //   1333: aload 8
    //   1335: astore 6
    //   1337: aload_1
    //   1338: astore 7
    //   1340: aload_0
    //   1341: astore 5
    //   1343: aload 20
    //   1345: sipush 3003
    //   1348: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1351: ldc -15
    //   1353: astore 10
    //   1355: goto -60 -> 1295
    //   1358: aload 8
    //   1360: astore 6
    //   1362: aload_1
    //   1363: astore 7
    //   1365: aload_0
    //   1366: astore 5
    //   1368: aload 9
    //   1370: instanceof 243
    //   1373: ifeq +28 -> 1401
    //   1376: aload 8
    //   1378: astore 6
    //   1380: aload_1
    //   1381: astore 7
    //   1383: aload_0
    //   1384: astore 5
    //   1386: aload 20
    //   1388: sipush 3005
    //   1391: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1394: ldc -11
    //   1396: astore 10
    //   1398: goto -103 -> 1295
    //   1401: aload 8
    //   1403: astore 6
    //   1405: aload_1
    //   1406: astore 7
    //   1408: aload_0
    //   1409: astore 5
    //   1411: new 58	java/lang/StringBuilder
    //   1414: astore 10
    //   1416: aload 8
    //   1418: astore 6
    //   1420: aload_1
    //   1421: astore 7
    //   1423: aload_0
    //   1424: astore 5
    //   1426: aload 10
    //   1428: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   1431: aload 8
    //   1433: astore 6
    //   1435: aload_1
    //   1436: astore 7
    //   1438: aload_0
    //   1439: astore 5
    //   1441: aload 10
    //   1443: ldc -9
    //   1445: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1448: pop
    //   1449: aload 8
    //   1451: astore 6
    //   1453: aload_1
    //   1454: astore 7
    //   1456: aload_0
    //   1457: astore 5
    //   1459: aload 10
    //   1461: aload 9
    //   1463: invokevirtual 248	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1466: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1469: pop
    //   1470: aload 8
    //   1472: astore 6
    //   1474: aload_1
    //   1475: astore 7
    //   1477: aload_0
    //   1478: astore 5
    //   1480: ldc 75
    //   1482: aload 10
    //   1484: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1487: invokestatic 223	cn/jiguang/ac/c:f	(Ljava/lang/String;Ljava/lang/String;)V
    //   1490: aload 8
    //   1492: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1495: aload_1
    //   1496: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1499: aload_0
    //   1500: ifnull +157 -> 1657
    //   1503: goto +150 -> 1653
    //   1506: astore 9
    //   1508: aconst_null
    //   1509: astore_1
    //   1510: aload_1
    //   1511: astore_0
    //   1512: aload 10
    //   1514: astore 8
    //   1516: aload 8
    //   1518: astore 6
    //   1520: aload_1
    //   1521: astore 7
    //   1523: aload_0
    //   1524: astore 5
    //   1526: new 58	java/lang/StringBuilder
    //   1529: astore 10
    //   1531: aload 8
    //   1533: astore 6
    //   1535: aload_1
    //   1536: astore 7
    //   1538: aload_0
    //   1539: astore 5
    //   1541: aload 10
    //   1543: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   1546: aload 8
    //   1548: astore 6
    //   1550: aload_1
    //   1551: astore 7
    //   1553: aload_0
    //   1554: astore 5
    //   1556: aload 10
    //   1558: ldc -6
    //   1560: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1563: pop
    //   1564: aload 8
    //   1566: astore 6
    //   1568: aload_1
    //   1569: astore 7
    //   1571: aload_0
    //   1572: astore 5
    //   1574: aload 10
    //   1576: aload 9
    //   1578: invokevirtual 251	java/net/MalformedURLException:getMessage	()Ljava/lang/String;
    //   1581: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1584: pop
    //   1585: aload 8
    //   1587: astore 6
    //   1589: aload_1
    //   1590: astore 7
    //   1592: aload_0
    //   1593: astore 5
    //   1595: ldc 75
    //   1597: aload 10
    //   1599: invokevirtual 78	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1602: invokestatic 223	cn/jiguang/ac/c:f	(Ljava/lang/String;Ljava/lang/String;)V
    //   1605: aload 8
    //   1607: astore 6
    //   1609: aload_1
    //   1610: astore 7
    //   1612: aload_0
    //   1613: astore 5
    //   1615: aload 20
    //   1617: sipush 3004
    //   1620: invokevirtual 169	cn/jiguang/net/HttpResponse:setResponseCode	(I)V
    //   1623: aload 8
    //   1625: astore 6
    //   1627: aload_1
    //   1628: astore 7
    //   1630: aload_0
    //   1631: astore 5
    //   1633: aload 20
    //   1635: ldc -3
    //   1637: invokevirtual 209	cn/jiguang/net/HttpResponse:setResponseBody	(Ljava/lang/String;)V
    //   1640: aload 8
    //   1642: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1645: aload_1
    //   1646: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1649: aload_0
    //   1650: ifnull +7 -> 1657
    //   1653: aload_0
    //   1654: invokevirtual 218	java/net/HttpURLConnection:disconnect	()V
    //   1657: aload 20
    //   1659: areturn
    //   1660: astore_1
    //   1661: aload 7
    //   1663: astore_0
    //   1664: aload 6
    //   1666: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1669: aload_0
    //   1670: invokestatic 215	cn/jiguang/as/e:a	(Ljava/io/Closeable;)V
    //   1673: aload 5
    //   1675: ifnull +8 -> 1683
    //   1678: aload 5
    //   1680: invokevirtual 218	java/net/HttpURLConnection:disconnect	()V
    //   1683: aload_1
    //   1684: athrow
    //   1685: astore 6
    //   1687: goto -1141 -> 546
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1690	0	paramContext	Context
    //   0	1690	1	paramHttpRequest	HttpRequest
    //   0	1690	2	paramBoolean	boolean
    //   72	503	3	i	int
    //   136	3	4	bool	boolean
    //   76	165	5	localObject1	Object
    //   261	23	5	localThrowable1	Throwable
    //   326	32	5	localObject2	Object
    //   362	23	5	localThrowable2	Throwable
    //   404	1275	5	localObject3	Object
    //   65	757	6	localObject4	Object
    //   838	10	6	localIOException1	java.io.IOException
    //   874	791	6	localObject5	Object
    //   1685	1	6	localThrowable3	Throwable
    //   22	488	7	localObject6	Object
    //   537	1	7	localThrowable4	Throwable
    //   587	316	7	localObject7	Object
    //   984	6	7	localObject8	Object
    //   1006	656	7	localHttpRequest	HttpRequest
    //   19	1622	8	localObject9	Object
    //   16	747	9	localObject10	Object
    //   826	15	9	localException1	Exception
    //   849	1	9	localObject11	Object
    //   854	31	9	localMalformedURLException1	java.net.MalformedURLException
    //   891	1	9	localException2	Exception
    //   898	1	9	localIOException2	java.io.IOException
    //   909	30	9	localMalformedURLException2	java.net.MalformedURLException
    //   995	171	9	localException3	Exception
    //   1210	252	9	localIOException3	java.io.IOException
    //   1506	71	9	localMalformedURLException3	java.net.MalformedURLException
    //   25	1573	10	localObject12	Object
    //   28	899	11	localObject13	Object
    //   161	421	12	localObject14	Object
    //   555	161	13	localObject15	Object
    //   31	547	14	localObject16	Object
    //   1	612	15	localObject17	Object
    //   4	613	16	localObject18	Object
    //   7	614	17	localObject19	Object
    //   10	615	18	localObject20	Object
    //   13	616	19	localObject21	Object
    //   50	1608	20	localHttpResponse	HttpResponse
    // Exception table:
    //   from	to	target	type
    //   143	190	261	java/lang/Throwable
    //   195	207	261	java/lang/Throwable
    //   207	226	261	java/lang/Throwable
    //   226	233	261	java/lang/Throwable
    //   236	258	261	java/lang/Throwable
    //   353	359	362	java/lang/Throwable
    //   500	507	537	java/lang/Throwable
    //   527	534	537	java/lang/Throwable
    //   426	431	795	finally
    //   460	467	795	finally
    //   500	507	795	finally
    //   527	534	795	finally
    //   605	612	795	finally
    //   632	638	795	finally
    //   658	665	795	finally
    //   690	694	795	finally
    //   714	722	795	finally
    //   742	748	795	finally
    //   768	774	795	finally
    //   426	431	813	java/lang/StackOverflowError
    //   460	467	813	java/lang/StackOverflowError
    //   500	507	813	java/lang/StackOverflowError
    //   527	534	813	java/lang/StackOverflowError
    //   605	612	813	java/lang/StackOverflowError
    //   632	638	813	java/lang/StackOverflowError
    //   658	665	813	java/lang/StackOverflowError
    //   690	694	813	java/lang/StackOverflowError
    //   714	722	813	java/lang/StackOverflowError
    //   742	748	813	java/lang/StackOverflowError
    //   768	774	813	java/lang/StackOverflowError
    //   426	431	826	java/lang/Exception
    //   460	467	826	java/lang/Exception
    //   500	507	826	java/lang/Exception
    //   527	534	826	java/lang/Exception
    //   605	612	826	java/lang/Exception
    //   632	638	826	java/lang/Exception
    //   658	665	826	java/lang/Exception
    //   690	694	826	java/lang/Exception
    //   714	722	826	java/lang/Exception
    //   742	748	826	java/lang/Exception
    //   768	774	826	java/lang/Exception
    //   426	431	838	java/io/IOException
    //   460	467	838	java/io/IOException
    //   500	507	838	java/io/IOException
    //   527	534	838	java/io/IOException
    //   605	612	838	java/io/IOException
    //   632	638	838	java/io/IOException
    //   658	665	838	java/io/IOException
    //   690	694	838	java/io/IOException
    //   714	722	838	java/io/IOException
    //   742	748	838	java/io/IOException
    //   768	774	838	java/io/IOException
    //   426	431	854	java/net/MalformedURLException
    //   460	467	854	java/net/MalformedURLException
    //   500	507	854	java/net/MalformedURLException
    //   527	534	854	java/net/MalformedURLException
    //   605	612	854	java/net/MalformedURLException
    //   632	638	854	java/net/MalformedURLException
    //   658	665	854	java/net/MalformedURLException
    //   690	694	854	java/net/MalformedURLException
    //   714	722	854	java/net/MalformedURLException
    //   742	748	854	java/net/MalformedURLException
    //   768	774	854	java/net/MalformedURLException
    //   61	138	866	finally
    //   143	190	866	finally
    //   195	207	866	finally
    //   207	226	866	finally
    //   226	233	866	finally
    //   236	258	866	finally
    //   263	302	866	finally
    //   306	328	866	finally
    //   333	342	866	finally
    //   342	353	866	finally
    //   353	359	866	finally
    //   364	403	866	finally
    //   61	138	879	java/lang/StackOverflowError
    //   143	190	879	java/lang/StackOverflowError
    //   195	207	879	java/lang/StackOverflowError
    //   207	226	879	java/lang/StackOverflowError
    //   226	233	879	java/lang/StackOverflowError
    //   236	258	879	java/lang/StackOverflowError
    //   263	302	879	java/lang/StackOverflowError
    //   306	328	879	java/lang/StackOverflowError
    //   333	342	879	java/lang/StackOverflowError
    //   342	353	879	java/lang/StackOverflowError
    //   353	359	879	java/lang/StackOverflowError
    //   364	403	879	java/lang/StackOverflowError
    //   61	138	891	java/lang/Exception
    //   143	190	891	java/lang/Exception
    //   195	207	891	java/lang/Exception
    //   207	226	891	java/lang/Exception
    //   226	233	891	java/lang/Exception
    //   236	258	891	java/lang/Exception
    //   263	302	891	java/lang/Exception
    //   306	328	891	java/lang/Exception
    //   333	342	891	java/lang/Exception
    //   342	353	891	java/lang/Exception
    //   353	359	891	java/lang/Exception
    //   364	403	891	java/lang/Exception
    //   61	138	898	java/io/IOException
    //   143	190	898	java/io/IOException
    //   195	207	898	java/io/IOException
    //   207	226	898	java/io/IOException
    //   226	233	898	java/io/IOException
    //   236	258	898	java/io/IOException
    //   263	302	898	java/io/IOException
    //   306	328	898	java/io/IOException
    //   333	342	898	java/io/IOException
    //   342	353	898	java/io/IOException
    //   353	359	898	java/io/IOException
    //   364	403	898	java/io/IOException
    //   61	138	909	java/net/MalformedURLException
    //   143	190	909	java/net/MalformedURLException
    //   195	207	909	java/net/MalformedURLException
    //   207	226	909	java/net/MalformedURLException
    //   226	233	909	java/net/MalformedURLException
    //   236	258	909	java/net/MalformedURLException
    //   263	302	909	java/net/MalformedURLException
    //   306	328	909	java/net/MalformedURLException
    //   333	342	909	java/net/MalformedURLException
    //   342	353	909	java/net/MalformedURLException
    //   353	359	909	java/net/MalformedURLException
    //   364	403	909	java/net/MalformedURLException
    //   52	61	920	finally
    //   52	61	933	java/lang/StackOverflowError
    //   942	964	984	finally
    //   52	61	995	java/lang/Exception
    //   52	61	1210	java/io/IOException
    //   52	61	1506	java/net/MalformedURLException
    //   1011	1016	1660	finally
    //   1026	1031	1660	finally
    //   1041	1049	1660	finally
    //   1059	1067	1660	finally
    //   1077	1087	1660	finally
    //   1097	1105	1660	finally
    //   1115	1120	1660	finally
    //   1130	1135	1660	finally
    //   1145	1153	1660	finally
    //   1163	1174	1660	finally
    //   1184	1194	1660	finally
    //   1230	1238	1660	finally
    //   1248	1255	1660	finally
    //   1265	1273	1660	finally
    //   1283	1291	1660	finally
    //   1305	1312	1660	finally
    //   1325	1333	1660	finally
    //   1343	1351	1660	finally
    //   1368	1376	1660	finally
    //   1386	1394	1660	finally
    //   1411	1416	1660	finally
    //   1426	1431	1660	finally
    //   1441	1449	1660	finally
    //   1459	1470	1660	finally
    //   1480	1490	1660	finally
    //   1526	1531	1660	finally
    //   1541	1546	1660	finally
    //   1556	1564	1660	finally
    //   1574	1585	1660	finally
    //   1595	1605	1660	finally
    //   1615	1623	1660	finally
    //   1633	1640	1660	finally
    //   460	467	1685	java/lang/Throwable
  }
  
  private static void a(HttpRequest paramHttpRequest, HttpURLConnection paramHttpURLConnection)
  {
    if ((paramHttpRequest != null) && (paramHttpURLConnection != null))
    {
      setURLConnection(paramHttpRequest.getRequestProperties(), paramHttpURLConnection);
      if (paramHttpRequest.getConnectTimeout() >= 0) {
        paramHttpURLConnection.setConnectTimeout(paramHttpRequest.getConnectTimeout());
      }
      if (paramHttpRequest.getReadTimeout() >= 0) {
        paramHttpURLConnection.setReadTimeout(paramHttpRequest.getReadTimeout());
      }
    }
  }
  
  private static void a(HttpURLConnection paramHttpURLConnection, HttpResponse paramHttpResponse)
  {
    if ((paramHttpResponse != null) && (paramHttpURLConnection != null))
    {
      paramHttpResponse.setResponseCode(paramHttpURLConnection.getResponseCode());
      paramHttpResponse.setResponseHeader("expires", paramHttpURLConnection.getHeaderField("Expires"));
      paramHttpResponse.setResponseHeader("cache-control", paramHttpURLConnection.getHeaderField("Cache-Control"));
    }
  }
  
  public static String appendParaToUrl(String paramString1, String paramString2, String paramString3)
  {
    if (TextUtils.isEmpty(paramString1)) {
      return paramString1;
    }
    StringBuilder localStringBuilder = new StringBuilder(paramString1);
    if (!paramString1.contains("?")) {
      paramString1 = "?";
    } else {
      paramString1 = "&";
    }
    localStringBuilder.append(paramString1);
    localStringBuilder.append(paramString2);
    localStringBuilder.append("=");
    localStringBuilder.append(paramString3);
    return localStringBuilder.toString();
  }
  
  public static HttpURLConnection getHttpURLConnectionWithProxy(Context paramContext, String paramString)
  {
    paramString = new URL(paramString);
    if (paramContext != null) {}
    try
    {
      if (paramContext.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", paramContext.getPackageName()) == 0)
      {
        paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if ((paramContext != null) && (paramContext.getType() != 1))
        {
          paramContext = paramContext.getExtraInfo();
          if ((paramContext != null) && ((paramContext.equals("cmwap")) || (paramContext.equals("3gwap")) || (paramContext.equals("uniwap"))))
          {
            paramContext = new java/net/InetSocketAddress;
            paramContext.<init>("10.0.0.172", 80);
            Proxy localProxy = new java/net/Proxy;
            localProxy.<init>(Proxy.Type.HTTP, paramContext);
            paramContext = (HttpURLConnection)paramString.openConnection(localProxy);
            return paramContext;
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return (HttpURLConnection)paramString.openConnection();
  }
  
  public static String getUrlWithParas(String paramString, Map<String, String> paramMap)
  {
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = "";
    }
    paramString = new StringBuilder(str);
    paramMap = joinParas(paramMap);
    if (!TextUtils.isEmpty(paramMap))
    {
      paramString.append("?");
      paramString.append(paramMap);
    }
    return paramString.toString();
  }
  
  public static String getUrlWithValueEncodeParas(String paramString, Map<String, String> paramMap)
  {
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = "";
    }
    paramString = new StringBuilder(str);
    paramMap = joinParasWithEncodedValue(paramMap);
    if (!TextUtils.isEmpty(paramMap))
    {
      paramString.append("?");
      paramString.append(paramMap);
    }
    return paramString.toString();
  }
  
  public static HttpResponse httpGet(Context paramContext, HttpRequest paramHttpRequest)
  {
    return a(paramContext, paramHttpRequest, false);
  }
  
  public static HttpResponse httpGet(Context paramContext, String paramString)
  {
    return httpGet(paramContext, new HttpRequest(paramString));
  }
  
  public static void httpGet(Context paramContext, HttpRequest paramHttpRequest, HttpListener paramHttpListener)
  {
    new a(paramContext, paramHttpListener).execute(new HttpRequest[] { paramHttpRequest });
  }
  
  public static void httpGet(Context paramContext, String paramString, HttpListener paramHttpListener)
  {
    new b(paramHttpListener, paramContext).execute(new String[] { paramString });
  }
  
  public static String httpGetString(Context paramContext, HttpRequest paramHttpRequest)
  {
    paramContext = httpGet(paramContext, paramHttpRequest);
    if (paramContext == null) {
      paramContext = null;
    } else {
      paramContext = paramContext.getResponseBody();
    }
    return paramContext;
  }
  
  public static String httpGetString(Context paramContext, String paramString)
  {
    paramContext = httpGet(paramContext, new HttpRequest(paramString));
    if (paramContext == null) {
      paramContext = null;
    } else {
      paramContext = paramContext.getResponseBody();
    }
    return paramContext;
  }
  
  public static HttpResponse httpPost(Context paramContext, HttpRequest paramHttpRequest)
  {
    return a(paramContext, paramHttpRequest, true);
  }
  
  public static HttpResponse httpPost(Context paramContext, String paramString)
  {
    return httpPost(paramContext, new HttpRequest(paramString));
  }
  
  public static String httpPostString(Context paramContext, String paramString)
  {
    paramContext = httpPost(paramContext, new HttpRequest(paramString));
    if (paramContext == null) {
      paramContext = null;
    } else {
      paramContext = paramContext.getResponseBody();
    }
    return paramContext;
  }
  
  public static String httpPostString(Context paramContext, String paramString, Map<String, String> paramMap)
  {
    paramContext = httpPost(paramContext, new HttpRequest(paramString, paramMap));
    if (paramContext == null) {
      paramContext = null;
    } else {
      paramContext = paramContext.getResponseBody();
    }
    return paramContext;
  }
  
  public static String joinParas(Map<String, String> paramMap)
  {
    if ((paramMap != null) && (paramMap.size() != 0))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = paramMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        paramMap = (Map.Entry)localIterator.next();
        localStringBuilder.append((String)paramMap.getKey());
        localStringBuilder.append("=");
        localStringBuilder.append((String)paramMap.getValue());
        if (localIterator.hasNext()) {
          localStringBuilder.append("&");
        }
      }
      return localStringBuilder.toString();
    }
    return null;
  }
  
  public static String joinParasWithEncodedValue(Map<String, String> paramMap)
  {
    localStringBuilder = new StringBuilder("");
    if ((paramMap != null) && (paramMap.size() > 0))
    {
      paramMap = paramMap.entrySet().iterator();
      try
      {
        while (paramMap.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)paramMap.next();
          localStringBuilder.append((String)localEntry.getKey());
          localStringBuilder.append("=");
          localStringBuilder.append(URLEncoder.encode((String)localEntry.getValue(), "UTF-8"));
          if (paramMap.hasNext()) {
            localStringBuilder.append("&");
          }
        }
        return localStringBuilder.toString();
      }
      catch (Exception paramMap)
      {
        paramMap.printStackTrace();
      }
    }
  }
  
  public static long parseGmtTime(String paramString)
  {
    try
    {
      long l = b.a("EEE, d MMM yyyy HH:mm:ss z").parse(paramString).getTime();
      return l;
    }
    catch (Exception paramString)
    {
      paramString.printStackTrace();
    }
    return -1L;
  }
  
  public static byte[] readInputStream(InputStream paramInputStream)
  {
    return e.b(paramInputStream);
  }
  
  public static void setURLConnection(Map<String, String> paramMap, HttpURLConnection paramHttpURLConnection)
  {
    if ((paramMap != null) && (paramMap.size() != 0) && (paramHttpURLConnection != null))
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        if (!TextUtils.isEmpty((CharSequence)localEntry.getKey())) {
          paramHttpURLConnection.setRequestProperty((String)localEntry.getKey(), (String)localEntry.getValue());
        }
      }
    }
  }
  
  public static abstract class HttpListener
  {
    protected void a() {}
    
    protected void a(HttpResponse paramHttpResponse) {}
  }
  
  private static class a
    extends AsyncTask<HttpRequest, Void, HttpResponse>
  {
    private HttpUtils.HttpListener a;
    private Context b;
    
    public a(Context paramContext, HttpUtils.HttpListener paramHttpListener)
    {
      this.a = paramHttpListener;
      this.b = paramContext;
    }
    
    protected HttpResponse a(HttpRequest... paramVarArgs)
    {
      if ((paramVarArgs != null) && (paramVarArgs.length != 0)) {
        return HttpUtils.httpGet(this.b, paramVarArgs[0]);
      }
      return null;
    }
    
    protected void a(HttpResponse paramHttpResponse)
    {
      HttpUtils.HttpListener localHttpListener = this.a;
      if (localHttpListener != null) {
        localHttpListener.a(paramHttpResponse);
      }
    }
    
    protected void onPreExecute()
    {
      HttpUtils.HttpListener localHttpListener = this.a;
      if (localHttpListener != null) {
        localHttpListener.a();
      }
    }
  }
  
  private static class b
    extends AsyncTask<String, Void, HttpResponse>
  {
    private HttpUtils.HttpListener a;
    private Context b;
    
    public b(HttpUtils.HttpListener paramHttpListener, Context paramContext)
    {
      this.a = paramHttpListener;
      this.b = paramContext;
    }
    
    protected HttpResponse a(String... paramVarArgs)
    {
      if ((paramVarArgs != null) && (paramVarArgs.length != 0)) {
        return HttpUtils.httpGet(this.b, paramVarArgs[0]);
      }
      return null;
    }
    
    protected void a(HttpResponse paramHttpResponse)
    {
      HttpUtils.HttpListener localHttpListener = this.a;
      if (localHttpListener != null) {
        localHttpListener.a(paramHttpResponse);
      }
    }
    
    protected void onPreExecute()
    {
      HttpUtils.HttpListener localHttpListener = this.a;
      if (localHttpListener != null) {
        localHttpListener.a();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/net/HttpUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */