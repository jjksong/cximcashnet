package cn.jiguang.am;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import cn.jiguang.ah.e;
import cn.jiguang.ah.j;
import cn.jiguang.ap.g;
import cn.jpush.android.service.DownloadProvider;
import java.io.File;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  implements Runnable
{
  private static final String b;
  private static final Object f = new Object();
  private static Boolean g;
  private static Boolean h;
  private static a j;
  Context a;
  private int c;
  private long[] d;
  private String e;
  private volatile boolean i;
  
  static
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(".jpush");
    localStringBuilder.append(File.separator);
    localStringBuilder.append(".shareinfo");
    localStringBuilder.append(File.separator);
    b = localStringBuilder.toString();
  }
  
  private a()
  {
    cn.jiguang.sdk.impl.b.a("share_process_executor");
  }
  
  private static ActivityInfo a(String paramString, Context paramContext)
  {
    try
    {
      Object localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).setAction("cn.jpush.android.intent.DownloadActivity");
      ((Intent)localObject).addCategory(paramString);
      ((Intent)localObject).setPackage(paramString);
      paramContext = paramContext.getPackageManager().resolveActivity((Intent)localObject, 0).activityInfo;
      if (((paramContext instanceof ActivityInfo)) && (paramContext.exported) && (paramContext.enabled))
      {
        if ("jpush.custom".equals(((ActivityInfo)paramContext).taskAffinity)) {
          if (((ActivityInfo)paramContext).theme == 16973840) {
            return (ActivityInfo)paramContext;
          }
        }
        for (paramContext = "download activity theme must config as @android:style/Theme.Translucent.NoTitleBar";; paramContext = "download activity need config taskAffinity is jpush.custom")
        {
          cn.jiguang.ai.a.c("ShareProcessManager", paramContext);
          break;
        }
      }
      return null;
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("check downloadActivity error:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject).toString());
      paramContext = new StringBuilder();
      paramContext.append("DownloadActivity is invalid in ");
      paramContext.append(paramString);
      cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
    }
  }
  
  private a a(String paramString)
  {
    Object localObject;
    try
    {
      if (TextUtils.isEmpty(paramString)) {
        return null;
      }
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = new cn/jiguang/am/a$a;
      paramString.<init>(this);
      try
      {
        long l2 = localJSONObject.optLong("u");
        String str1 = localJSONObject.optString("ak");
        String str2 = localJSONObject.optString("pn");
        localObject = localJSONObject.optString("ud");
        int m = localJSONObject.optInt("idc", -1);
        int k = localJSONObject.optInt("sv");
        long l1 = localJSONObject.optLong("uct", -1L);
        a.a(paramString, l2);
        a.a(paramString, (String)localObject);
        a.b(paramString, str1);
        a.a(paramString, m);
        a.c(paramString, str2);
        a.b(paramString, k);
        a.b(paramString, l1);
      }
      catch (Throwable localThrowable1) {}
      localObject = new StringBuilder();
    }
    catch (Throwable localThrowable2)
    {
      paramString = null;
    }
    ((StringBuilder)localObject).append("parse json to shareBean failed:");
    ((StringBuilder)localObject).append(localThrowable2.getMessage());
    cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject).toString());
    return paramString;
  }
  
  public static a a()
  {
    if (j == null) {
      synchronized (f)
      {
        if (j == null)
        {
          a locala = new cn/jiguang/am/a;
          locala.<init>();
          j = locala;
        }
      }
    }
    return j;
  }
  
  private String a(Context paramContext, String paramString1, String paramString2, HashMap<String, String> paramHashMap)
  {
    try
    {
      ContentResolver localContentResolver = paramContext.getApplicationContext().getContentResolver();
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append(paramString1);
      paramContext.append(".DownloadProvider");
      paramString1 = paramContext.toString();
      paramContext = paramString1;
      if (!paramString1.startsWith("content://"))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("content://");
        paramContext.append(paramString1);
        paramContext = paramContext.toString();
      }
      paramString1 = Uri.parse(paramContext);
      paramContext = new org/json/JSONObject;
      paramContext.<init>();
      paramString1 = paramString1.buildUpon();
      if (!TextUtils.isEmpty(paramString2)) {
        paramContext.put("kta", paramString2);
      }
      if ((paramHashMap != null) && (!paramHashMap.isEmpty()))
      {
        paramString2 = paramHashMap.entrySet().iterator();
        while (paramString2.hasNext())
        {
          paramHashMap = (Map.Entry)paramString2.next();
          paramContext.put((String)paramHashMap.getKey(), paramHashMap.getValue());
        }
      }
      paramString1.appendQueryParameter("kpgt", cn.jiguang.al.a.a(paramContext.toString()));
      paramContext = localContentResolver.getType(paramString1.build());
      return paramContext;
    }
    catch (Throwable paramString1)
    {
      paramContext = new StringBuilder();
      paramContext.append("callUriToDownloadProvider error:");
      paramContext.append(paramString1.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", paramContext.toString());
    }
    return null;
  }
  
  private static JSONObject a(Map<String, String> paramMap)
  {
    localJSONObject = new JSONObject();
    if (paramMap != null) {
      try
      {
        Object localObject;
        if (!paramMap.isEmpty())
        {
          paramMap = paramMap.entrySet().iterator();
          while (paramMap.hasNext())
          {
            localObject = (Map.Entry)paramMap.next();
            localJSONObject.put((String)((Map.Entry)localObject).getKey(), ((Map.Entry)localObject).getValue());
          }
        }
        return localJSONObject;
      }
      catch (Throwable paramMap)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("mapToJSONObject error:");
        ((StringBuilder)localObject).append(paramMap.getMessage());
        cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject).toString());
      }
    }
  }
  
  public static void a(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {}
    try
    {
      if (cn.jiguang.ap.a.a(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE"))
      {
        paramContext = b(paramString);
        if (paramContext.exists())
        {
          paramContext.delete();
        }
        else
        {
          paramString = new java/lang/StringBuilder;
          paramString.<init>();
          paramString.append("not found file in sdcard,filepath:");
          paramString.append(paramContext.getAbsolutePath());
          cn.jiguang.ai.a.c("ShareProcessManager", paramString.toString());
        }
      }
      else
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "no write sdcard permission when deletFileIfUninstall");
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    cn.jiguang.ai.a.g("ShareProcessManager", "deletFileIfUninstall failed ,context is null or pkgname is empty");
  }
  
  private boolean a(String paramString, ComponentInfo paramComponentInfo)
  {
    if ((paramComponentInfo != null) && ((paramComponentInfo instanceof ProviderInfo)))
    {
      paramComponentInfo = (ProviderInfo)paramComponentInfo;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("scan exported:");
      localStringBuilder.append(paramComponentInfo.exported);
      localStringBuilder.append(",enable:");
      localStringBuilder.append(paramComponentInfo.enabled);
      localStringBuilder.append(",authority:");
      localStringBuilder.append(paramComponentInfo.authority);
      localStringBuilder.append(",process:");
      localStringBuilder.append(paramComponentInfo.processName);
      cn.jiguang.ai.a.a("ShareProcessManager", localStringBuilder.toString());
      if ((paramComponentInfo.exported) && (paramComponentInfo.enabled) && (!TextUtils.isEmpty(paramComponentInfo.authority)))
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString);
        localStringBuilder.append(".DownloadProvider");
        if (TextUtils.equals(localStringBuilder.toString(), paramComponentInfo.authority)) {
          return true;
        }
      }
      paramString = new StringBuilder();
      paramString.append("downloadprovider config error,exported:");
      paramString.append(paramComponentInfo.exported);
      paramString.append(",enable:");
      paramString.append(paramComponentInfo.enabled);
      paramString.append(",authority:");
      paramString.append(paramComponentInfo.authority);
      paramString.append(",process:");
      paramString.append(paramComponentInfo.processName);
      cn.jiguang.ai.a.c("ShareProcessManager", paramString.toString());
    }
    return false;
  }
  
  private a b(Context paramContext, String paramString)
  {
    Object localObject2 = null;
    Object localObject1;
    try
    {
      localObject1 = a(paramContext, paramString, "asai", null);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("get type from:");
      localStringBuilder.append(paramString);
      localStringBuilder.append(",info:");
      localStringBuilder.append((String)localObject1);
      cn.jiguang.ai.a.c("ShareProcessManager", localStringBuilder.toString());
      if (localObject1 != null)
      {
        if ((!TextUtils.isEmpty((CharSequence)localObject1)) && (((String)localObject1).length() > 10))
        {
          paramContext = cn.jiguang.al.a.c((String)localObject1, "");
          if (TextUtils.isEmpty(paramContext))
          {
            cn.jiguang.ai.a.c("ShareProcessManager", "decrypt error");
            return null;
          }
          paramString = new java/lang/StringBuilder;
          paramString.<init>();
          paramString.append("parse success:");
          paramString.append(paramContext);
          cn.jiguang.ai.a.c("ShareProcessManager", paramString.toString());
          paramContext = a(paramContext);
        }
        else
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "is not shareprocessbean info");
          return (a)localObject2;
        }
      }
      else
      {
        localObject1 = localObject2;
        if (!e(paramContext)) {
          return localObject1;
        }
        paramContext = c(paramContext, paramString);
      }
      localObject1 = paramContext;
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("scanShareProcessBean error:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.h("ShareProcessManager", paramString.toString());
      localObject1 = localObject2;
    }
    return (a)localObject1;
  }
  
  private static File b(String paramString)
  {
    Object localObject = g.c(paramString);
    if (!TextUtils.isEmpty((CharSequence)localObject)) {
      paramString = (String)localObject;
    }
    File localFile = Environment.getExternalStorageDirectory();
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(b);
    ((StringBuilder)localObject).append(paramString);
    return new File(localFile, ((StringBuilder)localObject).toString());
  }
  
  private static String b()
  {
    return "cn.jpush.android.intent.DaemonService";
  }
  
  private Set<a> b(Context paramContext)
  {
    HashSet localHashSet = new HashSet();
    try
    {
      localObject1 = paramContext.getPackageManager();
      Object localObject2 = new android/content/Intent;
      ((Intent)localObject2).<init>();
      ((Intent)localObject2).setAction(b());
      int k = 0;
      localObject2 = ((PackageManager)localObject1).queryIntentServices((Intent)localObject2, 0);
      if ((localObject2 != null) && (((List)localObject2).size() != 0))
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        Object localObject4;
        Object localObject3;
        while (k < ((List)localObject2).size())
        {
          localObject4 = ((ResolveInfo)((List)localObject2).get(k)).serviceInfo;
          localObject3 = ((ServiceInfo)localObject4).name;
          localObject4 = ((ServiceInfo)localObject4).packageName;
          if ((!TextUtils.isEmpty((CharSequence)localObject3)) && (!TextUtils.isEmpty((CharSequence)localObject4)) && (!paramContext.getPackageName().equals(localObject4)) && (a((String)localObject4, cn.jiguang.ap.a.b(paramContext, (String)localObject4, DownloadProvider.class)))) {
            ((List)localObject1).add(localObject4);
          }
          k++;
        }
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append("valid size:");
        ((StringBuilder)localObject2).append(((List)localObject1).size());
        cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject2).toString());
        localObject2 = e.a(paramContext, "filter_pkg_list", localObject1);
        if ((localObject2 instanceof List)) {
          localObject1 = (List)localObject2;
        }
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append("valid end size:");
        ((StringBuilder)localObject2).append(((List)localObject1).size());
        cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject2).toString());
        localObject1 = ((List)localObject1).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject4 = (String)((Iterator)localObject1).next();
          localObject2 = b(paramContext, (String)localObject4);
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          ((StringBuilder)localObject3).append("scan share bean from:");
          ((StringBuilder)localObject3).append((String)localObject4);
          cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject3).toString());
          if (localObject2 != null) {
            localHashSet.add(localObject2);
          }
        }
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("end share bean list size:");
        paramContext.append(localHashSet.size());
        cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
      }
      else
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "query service size is empty");
        return localHashSet;
      }
    }
    catch (Throwable paramContext)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("scanOtherApp error:");
      ((StringBuilder)localObject1).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject1).toString());
    }
    return localHashSet;
  }
  
  private a c(Context paramContext, String paramString)
  {
    if (cn.jiguang.ap.a.a(paramContext, "android.permission.READ_EXTERNAL_STORAGE"))
    {
      Object localObject = b(paramString);
      String str = cn.jiguang.ap.c.c((File)localObject);
      if (TextUtils.isEmpty(str))
      {
        paramContext = new StringBuilder();
        paramContext.append("read info is empty from :");
        paramContext.append(((File)localObject).getAbsolutePath());
        cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
        return null;
      }
      localObject = a(cn.jiguang.al.a.c(str, ""));
      if (localObject != null) {
        if (cn.jiguang.ap.a.f(paramContext, a.c((a)localObject)))
        {
          if (a(a.c((a)localObject), paramContext) != null)
          {
            paramContext = new StringBuilder();
            paramContext.append("get share bean info from sdcard:");
            paramContext.append(((a)localObject).toString());
            cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
            return (a)localObject;
          }
          localObject = new StringBuilder();
        }
      }
      for (paramContext = "not config DownloadActivity in target app:";; paramContext = "parse share process bean with target app:")
      {
        ((StringBuilder)localObject).append(paramContext);
        ((StringBuilder)localObject).append(paramString);
        paramContext = ((StringBuilder)localObject).toString();
        break;
        paramString = new StringBuilder();
        paramString.append("found target app is uninsatll when scan sdcard,pkgname:");
        paramString.append(a.c((a)localObject));
        cn.jiguang.ai.a.c("ShareProcessManager", paramString.toString());
        a(paramContext, a.c((a)localObject));
        break label242;
        localObject = new StringBuilder();
      }
    }
    paramContext = "no read sdcard permission";
    cn.jiguang.ai.a.c("ShareProcessManager", paramContext);
    label242:
    return null;
  }
  
  private void c(Context paramContext)
  {
    try
    {
      if (cn.jiguang.ap.a.a(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE"))
      {
        localObject = b(paramContext.getPackageName());
        if (e(paramContext))
        {
          String str = d(paramContext);
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append("save info to sdcard:");
          paramContext.append(((File)localObject).getAbsolutePath());
          cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
          if ((!TextUtils.isEmpty(str)) && (str.length() > 10))
          {
            if (((File)localObject).exists()) {
              ((File)localObject).delete();
            }
            cn.jiguang.ap.c.a((File)localObject, str);
          }
        }
        else if (((File)localObject).exists())
        {
          ((File)localObject).delete();
        }
      }
      else
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "no write sdcard permission");
      }
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("saveShareInfoToSdCard failed:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject).toString());
    }
  }
  
  private String d(Context paramContext)
  {
    if (paramContext == null) {
      return "-1";
    }
    if ((f(paramContext)) && (cn.jiguang.sdk.impl.b.m(paramContext) != 1))
    {
      int k = cn.jiguang.sdk.impl.b.n(paramContext);
      if (k < 0)
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "[getTypeJson]idc<0,need login to get it");
        return "-3";
      }
      long l2 = cn.jiguang.sdk.impl.b.e(paramContext);
      if (l2 <= 0L)
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "[getTypeJson]uid<=0,need login to get it");
        return "-2";
      }
      Object localObject = cn.jiguang.sdk.impl.b.l(paramContext);
      long l1 = -1L;
      if (localObject != null)
      {
        this.e = ((String)((Map)localObject).get("uuid"));
        l1 = ((Long)((Map)localObject).get("ct")).longValue();
      }
      String str = cn.jiguang.sdk.impl.b.i(paramContext);
      localObject = new JSONObject();
      try
      {
        ((JSONObject)localObject).put("u", l2);
        ((JSONObject)localObject).put("p", cn.jiguang.sdk.impl.b.f(paramContext));
        ((JSONObject)localObject).put("ud", this.e);
        ((JSONObject)localObject).put("ak", str);
        ((JSONObject)localObject).put("idc", k);
        ((JSONObject)localObject).put("pn", paramContext.getPackageName());
        ((JSONObject)localObject).put("sv", 200);
        ((JSONObject)localObject).put("uct", l1);
        paramContext = cn.jiguang.al.a.a(((JSONObject)localObject).toString());
        return paramContext;
      }
      catch (JSONException paramContext)
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "[getTypeJson] to json error");
        return "2.0.0";
      }
    }
    cn.jiguang.ai.a.c("ShareProcessManager", "[getTypeJson]share process is close by action");
    return "-4";
  }
  
  private static boolean e(Context paramContext)
  {
    Object localObject = h;
    if (localObject != null) {
      return ((Boolean)localObject).booleanValue();
    }
    try
    {
      localObject = Build.MANUFACTURER;
      String str = "Xiaomi".toLowerCase();
      if ((!TextUtils.isEmpty((CharSequence)localObject)) && (TextUtils.equals(str, ((String)localObject).toLowerCase())))
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "xiaomi not use activity and sdcard");
        h = Boolean.valueOf(false);
        boolean bool = h.booleanValue();
        return bool;
      }
    }
    catch (Throwable localThrowable)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("get MANUFACTURER failed - error:");
      ((StringBuilder)localObject).append(localThrowable.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", ((StringBuilder)localObject).toString());
      if (a(paramContext.getPackageName(), paramContext) != null) {
        paramContext = Boolean.valueOf(true);
      } else {
        paramContext = Boolean.valueOf(false);
      }
      h = paramContext;
    }
    return h.booleanValue();
  }
  
  private static boolean f(Context paramContext)
  {
    Object localObject = g;
    if (localObject != null) {
      return ((Boolean)localObject).booleanValue();
    }
    if (paramContext == null)
    {
      cn.jiguang.ai.a.g("ShareProcessManager", "context is null");
      return true;
    }
    try
    {
      localObject = cn.jiguang.ap.a.b(paramContext, paramContext.getPackageName(), DownloadProvider.class);
      if (localObject == null)
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "not found download provider in manifest");
        g = Boolean.valueOf(false);
        return g.booleanValue();
      }
      if ((((ComponentInfo)localObject).enabled) && (((ComponentInfo)localObject).exported) && (!TextUtils.isEmpty(((ProviderInfo)localObject).authority)))
      {
        localObject = new android/content/Intent;
        ((Intent)localObject).<init>();
        ((Intent)localObject).setPackage(paramContext.getPackageName());
        ((Intent)localObject).setAction("cn.jiguang.android.share.close");
        paramContext = paramContext.getPackageManager().queryIntentServices((Intent)localObject, 0);
        if ((paramContext != null) && (!paramContext.isEmpty())) {}
        for (paramContext = Boolean.valueOf(false);; paramContext = Boolean.valueOf(true))
        {
          g = paramContext;
          break;
        }
        return g.booleanValue();
      }
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append("download provider config error,enable");
      paramContext.append(((ComponentInfo)localObject).enabled);
      paramContext.append(",exported:");
      paramContext.append(((ComponentInfo)localObject).exported);
      paramContext.append(",authority:");
      paramContext.append(((ProviderInfo)localObject).authority);
      cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
      g = Boolean.valueOf(false);
      boolean bool = g.booleanValue();
      return bool;
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("Get isShareProcessModeOpen error#:");
      paramContext.append(localThrowable.getMessage());
      cn.jiguang.ai.a.c("ShareProcessManager", paramContext.toString());
    }
    return true;
  }
  
  public void a(Context paramContext)
  {
    try
    {
      if ((f(paramContext)) && (cn.jiguang.sdk.impl.b.m(paramContext) != 1))
      {
        Object localObject = e.a(paramContext, "getwakeenable", null);
        if (((localObject instanceof Boolean)) && (!((Boolean)localObject).booleanValue()))
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "wake disable,not scan share app");
          return;
        }
        if (this.i)
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "isAttaching");
          return;
        }
        this.a = paramContext;
        this.i = true;
        cn.jiguang.ai.a.c("ShareProcessManager", "scanOtherApp...");
        this.c = 0;
        cn.jiguang.sdk.impl.b.a("share_process_executor", this, new int[0]);
        return;
      }
      cn.jiguang.ai.a.c("ShareProcessManager", "share process is close by action");
      a(paramContext, paramContext.getPackageName());
      return;
    }
    finally {}
  }
  
  public void a(Context paramContext, int paramInt)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("requestTimeOut,cmd:");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(",isAttaching:");
    ((StringBuilder)localObject).append(this.i);
    cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject).toString());
    if ((paramInt == 30) && (!this.i))
    {
      this.c += 1;
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("attachTimeoutTimes:");
      ((StringBuilder)localObject).append(this.c);
      ((StringBuilder)localObject).append(",requestUIDS:");
      ((StringBuilder)localObject).append(this.d);
      ((StringBuilder)localObject).append(",shareProcessUUID:");
      ((StringBuilder)localObject).append(this.e);
      cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject).toString());
      if (this.c > 2)
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "attach too many times by once scan");
        return;
      }
      localObject = this.d;
      if ((localObject != null) && (localObject.length > 0) && (!TextUtils.isEmpty(this.e)))
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "will retry attach");
        localObject = cn.jiguang.ak.b.a(cn.jiguang.sdk.impl.b.e(paramContext), this.e, this.d);
        cn.jiguang.sdk.impl.b.a(paramContext, cn.jiguang.sdk.impl.a.f, 30, 0, j.b(), 0L, (byte[])localObject);
      }
    }
  }
  
  public void a(Context paramContext, int paramInt1, int paramInt2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("request");
    if (paramInt1 == 0) {
      paramContext = "success";
    } else {
      paramContext = "failed";
    }
    localStringBuilder.append(paramContext);
    localStringBuilder.append(",cmd:");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(",code:");
    localStringBuilder.append(paramInt1);
    cn.jiguang.ai.a.c("ShareProcessManager", localStringBuilder.toString());
    if ((paramInt2 == 30) && (paramInt1 == 0)) {
      this.c = 0;
    }
  }
  
  public void a(Context paramContext, long paramLong)
  {
    try
    {
      if (TextUtils.isEmpty(this.e))
      {
        cn.jiguang.ai.a.c("ShareProcessManager", "dettachUid error,shareUUID is empty");
        return;
      }
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("dettach uid:");
      ((StringBuilder)localObject).append(paramLong);
      cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject).toString());
      localObject = cn.jiguang.ak.b.a(this.e, new long[] { paramLong });
      cn.jiguang.sdk.impl.b.a(paramContext, cn.jiguang.sdk.impl.a.f, 32, 0, j.b(), 0L, (byte[])localObject);
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("dettach uid error:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject).toString());
    }
  }
  
  public void a(Context paramContext, long paramLong, byte[] paramArrayOfByte)
  {
    if ((paramLong != 0L) && (paramArrayOfByte != null)) {
      try
      {
        Object localObject2 = cn.jiguang.ak.a.a(paramContext, paramArrayOfByte, "");
        if ((localObject2 != null) && (((cn.jiguang.ak.c)((Pair)localObject2).first).c == 3))
        {
          Object localObject1 = (ByteBuffer)((Pair)localObject2).second;
          ((ByteBuffer)localObject1).get();
          paramLong = ((ByteBuffer)localObject1).getLong();
          String str = cn.jiguang.ak.b.a((ByteBuffer)localObject1);
          if (TextUtils.isEmpty(str))
          {
            cn.jiguang.ai.a.g("ShareProcessManager", "msgContent is empty");
            return;
          }
          localObject1 = new java/io/LineNumberReader;
          Object localObject3 = new java/io/StringReader;
          ((StringReader)localObject3).<init>(str);
          ((LineNumberReader)localObject1).<init>((Reader)localObject3);
          str = ((LineNumberReader)localObject1).readLine();
          if (TextUtils.isEmpty(str))
          {
            cn.jiguang.ai.a.i("ShareProcessManager", "appid is empty");
            return;
          }
          localObject3 = ((LineNumberReader)localObject1).readLine();
          if (TextUtils.isEmpty((CharSequence)localObject3))
          {
            cn.jiguang.ai.a.i("ShareProcessManager", "senderId is empty");
            return;
          }
          if ((f(paramContext)) && (cn.jiguang.sdk.impl.b.m(paramContext) != 1))
          {
            if (!cn.jiguang.ap.a.f(paramContext, str))
            {
              a(paramContext, ((cn.jiguang.ak.c)((Pair)localObject2).first).f);
              paramArrayOfByte = new java/lang/StringBuilder;
              paramArrayOfByte.<init>();
              paramArrayOfByte.append("app not installed:");
              paramArrayOfByte.append(str);
              cn.jiguang.ai.a.c("ShareProcessManager", paramArrayOfByte.toString());
              a(paramContext, str);
              return;
            }
            Object localObject5 = Base64.encodeToString(paramArrayOfByte, 10);
            Object localObject4 = ((cn.jiguang.ak.c)((Pair)localObject2).first).d;
            paramArrayOfByte = new java/util/HashMap;
            paramArrayOfByte.<init>();
            paramArrayOfByte.put("ktm", localObject5);
            paramArrayOfByte.put("ktp", cn.jiguang.al.a.a(cn.jiguang.sdk.impl.b.e(paramContext)));
            paramArrayOfByte.put("ktma", localObject3);
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            ((StringBuilder)localObject5).append(paramLong);
            ((StringBuilder)localObject5).append("");
            paramArrayOfByte.put("mtmmi", ((StringBuilder)localObject5).toString());
            paramArrayOfByte.put("ktmfp", paramContext.getPackageName());
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            ((StringBuilder)localObject5).append(localObject4);
            ((StringBuilder)localObject5).append("");
            paramArrayOfByte.put("ktmr", ((StringBuilder)localObject5).toString());
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            ((StringBuilder)localObject5).append("dispatch share msg,appkey:");
            ((StringBuilder)localObject5).append((String)localObject3);
            ((StringBuilder)localObject5).append(",msgid:");
            ((StringBuilder)localObject5).append(paramLong);
            ((StringBuilder)localObject5).append(",rid:");
            ((StringBuilder)localObject5).append(localObject4);
            cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject5).toString());
            localObject3 = a(paramContext, str, "asm", paramArrayOfByte);
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            ((StringBuilder)localObject4).append("dispatch result:");
            ((StringBuilder)localObject4).append((String)localObject3);
            cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject4).toString());
            if (TextUtils.isEmpty((CharSequence)localObject3)) {
              if (e(paramContext))
              {
                localObject3 = a(str, paramContext);
                if (localObject3 != null)
                {
                  cn.jiguang.ai.a.c("ShareProcessManager", "will try use downloadActivity");
                  paramArrayOfByte = a(paramArrayOfByte);
                  localObject2 = new android/content/Intent;
                  ((Intent)localObject2).<init>("asm");
                  localObject4 = new android/content/ComponentName;
                  ((ComponentName)localObject4).<init>(((ActivityInfo)localObject3).packageName, ((ActivityInfo)localObject3).name);
                  ((Intent)localObject2).setComponent((ComponentName)localObject4);
                  ((Intent)localObject2).setFlags(268435456);
                  ((Intent)localObject2).addCategory(str);
                  ((Intent)localObject2).putExtra("data", paramArrayOfByte.toString());
                  paramContext.startActivity((Intent)localObject2);
                  break label792;
                }
                paramLong = ((cn.jiguang.ak.c)((Pair)localObject2).first).f;
              }
            }
            for (;;)
            {
              a(paramContext, paramLong);
              break label792;
              cn.jiguang.ai.a.c("ShareProcessManager", "app can not use downloadActivity dispatch msg");
              paramLong = ((cn.jiguang.ak.c)((Pair)localObject2).first).f;
              continue;
              if (!((String)localObject3).equals("-4")) {
                break;
              }
              paramLong = ((cn.jiguang.ak.c)((Pair)localObject2).first).f;
            }
            if (((String)localObject3).equals("0")) {}
            for (paramContext = "wait the msg reponse";; paramContext = paramContext.toString())
            {
              cn.jiguang.ai.a.c("ShareProcessManager", paramContext);
              break;
              paramContext = new java/lang/StringBuilder;
              paramContext.<init>();
              paramContext.append("provider is :");
              paramContext.append((String)localObject3);
              paramContext.append(",app is less than jcore_v125");
            }
            label792:
            ((LineNumberReader)localObject1).close();
          }
          else
          {
            a(paramContext, cn.jiguang.sdk.impl.b.e(paramContext));
            cn.jiguang.ai.a.c("ShareProcessManager", " share process is close,will not dispatch the msg and dettach mine uid");
          }
        }
        else
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "share msg cmd is not 3");
          return;
        }
      }
      catch (Throwable paramContext)
      {
        paramArrayOfByte = new StringBuilder();
        paramArrayOfByte.append("dispatchMsg error:");
        paramArrayOfByte.append(paramContext.getMessage());
        cn.jiguang.ai.a.g("ShareProcessManager", paramArrayOfByte.toString());
      }
    }
  }
  
  public void a(Context paramContext, Bundle paramBundle)
  {
    try
    {
      cn.jiguang.ai.a.c("ShareProcessManager", "doMsg");
      if (paramBundle != null)
      {
        paramBundle = paramBundle.getString("data");
        if (!TextUtils.isEmpty(paramBundle))
        {
          Object localObject1 = new org/json/JSONObject;
          ((JSONObject)localObject1).<init>(paramBundle);
          paramBundle = new java/lang/StringBuilder;
          paramBundle.<init>();
          paramBundle.append("doMsg json:");
          paramBundle.append(((JSONObject)localObject1).toString());
          cn.jiguang.ai.a.c("ShareProcessManager", paramBundle.toString());
          String str2 = ((JSONObject)localObject1).optString("ktm");
          String str3 = ((JSONObject)localObject1).optString("ktp");
          paramBundle = ((JSONObject)localObject1).optString("mtmmi");
          String str1 = ((JSONObject)localObject1).optString("ktmfp");
          Object localObject2 = ((JSONObject)localObject1).optString("ktma");
          String str4 = ((JSONObject)localObject1).optString("ktmr");
          if ((!TextUtils.isEmpty(str2)) && (!TextUtils.isEmpty(paramBundle)) && (!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty((CharSequence)localObject2)))
          {
            localObject1 = new java/util/HashMap;
            ((HashMap)localObject1).<init>();
            ((HashMap)localObject1).put("mtmmi", paramBundle);
            ((HashMap)localObject1).put("ktmfp", str1);
            ((HashMap)localObject1).put("ktma", localObject2);
            ((HashMap)localObject1).put("ktmr", str4);
            paramBundle = cn.jiguang.ak.a.a(paramContext, Base64.decode(str2, 10), str3);
            if (paramBundle != null)
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(((cn.jiguang.ak.c)paramBundle.first).f);
              ((StringBuilder)localObject2).append("");
              ((HashMap)localObject1).put("ktmu", ((StringBuilder)localObject2).toString());
              if ((f(paramContext)) && (cn.jiguang.sdk.impl.b.m(paramContext) != 1)) {
                if (((cn.jiguang.ak.c)paramBundle.first).f != cn.jiguang.sdk.impl.b.e(paramContext))
                {
                  localObject2 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject2).<init>();
                  ((StringBuilder)localObject2).append("this msg uid is :");
                  ((StringBuilder)localObject2).append(((cn.jiguang.ak.c)paramBundle.first).f);
                  ((StringBuilder)localObject2).append(",is not this app msg");
                  cn.jiguang.ai.a.c("ShareProcessManager", ((StringBuilder)localObject2).toString());
                }
              }
              for (paramBundle = "1";; paramBundle = "3")
              {
                ((HashMap)localObject1).put("asmrc", paramBundle);
                break;
                ((HashMap)localObject1).put("asmrc", "0");
                cn.jiguang.ah.b.a().a(paramContext, (cn.jiguang.ak.c)paramBundle.first, (ByteBuffer)paramBundle.second);
                break;
                cn.jiguang.ai.a.c("ShareProcessManager", "share process is closed");
              }
            }
            a(paramContext, str1, "asmr", (HashMap)localObject1);
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      paramBundle = new StringBuilder();
      paramBundle.append("doMsg error:");
      paramBundle.append(paramContext.getMessage());
      cn.jiguang.ai.a.c("ShareProcessManager", paramBundle.toString());
    }
  }
  
  public void b(Context paramContext, Bundle paramBundle)
  {
    try
    {
      paramBundle = paramBundle.getString("data");
      if (!TextUtils.isEmpty(paramBundle))
      {
        Object localObject2 = new org/json/JSONObject;
        ((JSONObject)localObject2).<init>(paramBundle);
        String str1 = ((JSONObject)localObject2).optString("mtmmi");
        Object localObject1 = ((JSONObject)localObject2).optString("ktmfp");
        String str3 = ((JSONObject)localObject2).optString("ktma");
        String str2 = ((JSONObject)localObject2).optString("ktmr");
        paramBundle = ((JSONObject)localObject2).optString("ktmu");
        localObject2 = ((JSONObject)localObject2).optString("asmrc", "0");
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("msg response,msgId:");
        localStringBuilder.append(str1);
        localStringBuilder.append(",fromPkg:");
        localStringBuilder.append((String)localObject1);
        localStringBuilder.append(",appKey:");
        localStringBuilder.append(str3);
        localStringBuilder.append(",rid:");
        localStringBuilder.append(str2);
        localStringBuilder.append(",uid:");
        localStringBuilder.append(paramBundle);
        localStringBuilder.append(",responseCode:");
        localStringBuilder.append((String)localObject2);
        cn.jiguang.ai.a.c("ShareProcessManager", localStringBuilder.toString());
        if ((((String)localObject2).equals("0")) && (!TextUtils.isEmpty(paramBundle)) && (!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str3)) && (!TextUtils.isEmpty(str2)))
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "response success,will send msg response to server");
          localObject1 = cn.jiguang.ak.b.a(0, (byte)0, Long.parseLong(str1), str3);
          cn.jiguang.sdk.impl.b.b(paramContext, cn.jiguang.sdk.impl.a.f, 4, 2, j.b(), Long.parseLong(paramBundle), (byte[])localObject1);
        }
        else if ((((String)localObject2).equals("1")) && (!TextUtils.isEmpty(paramBundle)))
        {
          a(paramContext, Long.parseLong(paramBundle));
        }
        else if ((((String)localObject2).equals("3")) && (!TextUtils.isEmpty(paramBundle)))
        {
          a(paramContext, Long.parseLong(paramBundle));
          a(paramContext, (String)localObject1);
        }
        else
        {
          cn.jiguang.ai.a.c("ShareProcessManager", "invalid msg response");
        }
      }
    }
    catch (Throwable paramBundle)
    {
      paramContext = new StringBuilder();
      paramContext.append("doMsgResponse failed:");
      paramContext.append(paramBundle.getMessage());
      cn.jiguang.ai.a.g("ShareProcessManager", paramContext.toString());
    }
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   4: invokestatic 566	cn/jiguang/sdk/impl/b:l	(Landroid/content/Context;)Ljava/util/Map;
    //   7: astore 9
    //   9: aload 9
    //   11: ifnull +40 -> 51
    //   14: aload_0
    //   15: aload 9
    //   17: ldc_w 568
    //   20: invokeinterface 571 2 0
    //   25: checkcast 116	java/lang/String
    //   28: putfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   31: aload 9
    //   33: ldc_w 575
    //   36: invokeinterface 571 2 0
    //   41: checkcast 577	java/lang/Long
    //   44: invokevirtual 581	java/lang/Long:longValue	()J
    //   47: lstore_3
    //   48: goto +7 -> 55
    //   51: ldc2_w 188
    //   54: lstore_3
    //   55: new 30	java/lang/StringBuilder
    //   58: astore 9
    //   60: aload 9
    //   62: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   65: aload 9
    //   67: ldc_w 930
    //   70: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload 9
    //   76: aload_0
    //   77: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   80: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload 9
    //   86: ldc_w 932
    //   89: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   92: pop
    //   93: aload 9
    //   95: lload_3
    //   96: invokevirtual 715	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   99: pop
    //   100: ldc -128
    //   102: aload 9
    //   104: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   107: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   110: aload_0
    //   111: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   114: invokestatic 151	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   117: ifne +17 -> 134
    //   120: lload_3
    //   121: lconst_0
    //   122: lcmp
    //   123: ifge +6 -> 129
    //   126: goto +8 -> 134
    //   129: iconst_0
    //   130: istore_1
    //   131: goto +94 -> 225
    //   134: ldc -128
    //   136: ldc_w 934
    //   139: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   142: aload_0
    //   143: invokestatic 940	java/util/UUID:randomUUID	()Ljava/util/UUID;
    //   146: invokevirtual 941	java/util/UUID:toString	()Ljava/lang/String;
    //   149: putfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   152: invokestatic 946	java/lang/System:currentTimeMillis	()J
    //   155: lstore_3
    //   156: new 30	java/lang/StringBuilder
    //   159: astore 9
    //   161: aload 9
    //   163: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   166: aload 9
    //   168: ldc_w 948
    //   171: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload 9
    //   177: aload_0
    //   178: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   181: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   184: pop
    //   185: aload 9
    //   187: ldc_w 950
    //   190: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: aload 9
    //   196: lload_3
    //   197: invokevirtual 715	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   200: pop
    //   201: ldc -128
    //   203: aload 9
    //   205: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   208: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   211: aload_0
    //   212: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   215: aload_0
    //   216: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   219: lload_3
    //   220: invokestatic 953	cn/jiguang/sdk/impl/b:a	(Landroid/content/Context;Ljava/lang/String;J)V
    //   223: iconst_1
    //   224: istore_1
    //   225: aload_0
    //   226: aload_0
    //   227: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   230: invokespecial 955	cn/jiguang/am/a:b	(Landroid/content/Context;)Ljava/util/Set;
    //   233: astore 9
    //   235: aload 9
    //   237: ifnull +851 -> 1088
    //   240: aload 9
    //   242: invokeinterface 956 1 0
    //   247: ifeq +6 -> 253
    //   250: goto +838 -> 1088
    //   253: aload_0
    //   254: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   257: invokestatic 551	cn/jiguang/sdk/impl/b:n	(Landroid/content/Context;)I
    //   260: istore_2
    //   261: new 30	java/lang/StringBuilder
    //   264: astore 10
    //   266: aload 10
    //   268: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   271: aload 10
    //   273: ldc_w 958
    //   276: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   279: pop
    //   280: aload 10
    //   282: iload_2
    //   283: invokevirtual 473	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   286: pop
    //   287: ldc -128
    //   289: aload 10
    //   291: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   294: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   297: new 438	java/util/ArrayList
    //   300: astore 12
    //   302: aload 12
    //   304: invokespecial 439	java/util/ArrayList:<init>	()V
    //   307: aload 9
    //   309: invokeinterface 264 1 0
    //   314: astore 9
    //   316: aload 9
    //   318: invokeinterface 269 1 0
    //   323: ifeq +68 -> 391
    //   326: aload 9
    //   328: invokeinterface 273 1 0
    //   333: checkcast 8	cn/jiguang/am/a$a
    //   336: astore 10
    //   338: iload_2
    //   339: aload 10
    //   341: invokestatic 961	cn/jiguang/am/a$a:a	(Lcn/jiguang/am/a$a;)I
    //   344: if_icmpne -28 -> 316
    //   347: aload 10
    //   349: invokestatic 964	cn/jiguang/am/a$a:b	(Lcn/jiguang/am/a$a;)J
    //   352: lconst_0
    //   353: lcmp
    //   354: ifle -38 -> 316
    //   357: aload_0
    //   358: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   361: invokevirtual 458	android/content/Context:getPackageName	()Ljava/lang/String;
    //   364: aload 10
    //   366: invokestatic 508	cn/jiguang/am/a$a:c	(Lcn/jiguang/am/a$a;)Ljava/lang/String;
    //   369: invokevirtual 120	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   372: ifeq +6 -> 378
    //   375: goto -59 -> 316
    //   378: aload 12
    //   380: aload 10
    //   382: invokeinterface 468 2 0
    //   387: pop
    //   388: goto -72 -> 316
    //   391: aload 12
    //   393: invokeinterface 436 1 0
    //   398: ifne +25 -> 423
    //   401: ldc -128
    //   403: ldc_w 966
    //   406: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   409: aload_0
    //   410: iconst_0
    //   411: putfield 647	cn/jiguang/am/a:i	Z
    //   414: aload_0
    //   415: aload_0
    //   416: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   419: invokespecial 968	cn/jiguang/am/a:c	(Landroid/content/Context;)V
    //   422: return
    //   423: aload_0
    //   424: aload 12
    //   426: invokeinterface 436 1 0
    //   431: newarray <illegal type>
    //   433: putfield 671	cn/jiguang/am/a:d	[J
    //   436: ldc_w 386
    //   439: astore 9
    //   441: ldc2_w 188
    //   444: lstore 7
    //   446: ldc_w 386
    //   449: astore 10
    //   451: iconst_0
    //   452: istore_2
    //   453: iload_2
    //   454: aload 12
    //   456: invokeinterface 436 1 0
    //   461: if_icmpge +309 -> 770
    //   464: new 30	java/lang/StringBuilder
    //   467: astore 11
    //   469: aload 11
    //   471: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   474: aload 11
    //   476: ldc_w 970
    //   479: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   482: pop
    //   483: aload 11
    //   485: aload 12
    //   487: iload_2
    //   488: invokeinterface 443 2 0
    //   493: invokevirtual 674	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   496: pop
    //   497: ldc -128
    //   499: aload 11
    //   501: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   504: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   507: aload_0
    //   508: getfield 671	cn/jiguang/am/a:d	[J
    //   511: iload_2
    //   512: aload 12
    //   514: iload_2
    //   515: invokeinterface 443 2 0
    //   520: checkcast 8	cn/jiguang/am/a$a
    //   523: invokestatic 964	cn/jiguang/am/a$a:b	(Lcn/jiguang/am/a$a;)J
    //   526: lastore
    //   527: new 30	java/lang/StringBuilder
    //   530: astore 11
    //   532: aload 11
    //   534: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   537: aload 11
    //   539: aload 9
    //   541: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   544: pop
    //   545: aload 11
    //   547: aload 12
    //   549: iload_2
    //   550: invokeinterface 443 2 0
    //   555: checkcast 8	cn/jiguang/am/a$a
    //   558: invokestatic 964	cn/jiguang/am/a$a:b	(Lcn/jiguang/am/a$a;)J
    //   561: invokevirtual 715	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   564: pop
    //   565: aload 11
    //   567: ldc_w 972
    //   570: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   573: pop
    //   574: aload 11
    //   576: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   579: astore 11
    //   581: aload 12
    //   583: iload_2
    //   584: invokeinterface 443 2 0
    //   589: checkcast 8	cn/jiguang/am/a$a
    //   592: invokestatic 974	cn/jiguang/am/a$a:d	(Lcn/jiguang/am/a$a;)Ljava/lang/String;
    //   595: invokestatic 151	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   598: ifne +146 -> 744
    //   601: aload 12
    //   603: iload_2
    //   604: invokeinterface 443 2 0
    //   609: checkcast 8	cn/jiguang/am/a$a
    //   612: invokestatic 976	cn/jiguang/am/a$a:e	(Lcn/jiguang/am/a$a;)J
    //   615: lconst_0
    //   616: lcmp
    //   617: ifle +127 -> 744
    //   620: aload 12
    //   622: iload_2
    //   623: invokeinterface 443 2 0
    //   628: checkcast 8	cn/jiguang/am/a$a
    //   631: invokestatic 976	cn/jiguang/am/a$a:e	(Lcn/jiguang/am/a$a;)J
    //   634: lload 7
    //   636: lcmp
    //   637: iflt +23 -> 660
    //   640: aload 10
    //   642: astore 9
    //   644: lload 7
    //   646: lstore 5
    //   648: lload 7
    //   650: ldc2_w 188
    //   653: lcmp
    //   654: ifne +98 -> 752
    //   657: goto +3 -> 660
    //   660: new 30	java/lang/StringBuilder
    //   663: astore 9
    //   665: aload 9
    //   667: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   670: aload 9
    //   672: ldc_w 978
    //   675: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   678: pop
    //   679: aload 9
    //   681: aload 12
    //   683: iload_2
    //   684: invokeinterface 443 2 0
    //   689: checkcast 8	cn/jiguang/am/a$a
    //   692: invokestatic 508	cn/jiguang/am/a$a:c	(Lcn/jiguang/am/a$a;)Ljava/lang/String;
    //   695: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   698: pop
    //   699: ldc -128
    //   701: aload 9
    //   703: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   706: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   709: aload 12
    //   711: iload_2
    //   712: invokeinterface 443 2 0
    //   717: checkcast 8	cn/jiguang/am/a$a
    //   720: invokestatic 974	cn/jiguang/am/a$a:d	(Lcn/jiguang/am/a$a;)Ljava/lang/String;
    //   723: astore 9
    //   725: aload 12
    //   727: iload_2
    //   728: invokeinterface 443 2 0
    //   733: checkcast 8	cn/jiguang/am/a$a
    //   736: invokestatic 976	cn/jiguang/am/a$a:e	(Lcn/jiguang/am/a$a;)J
    //   739: lstore 5
    //   741: goto +11 -> 752
    //   744: lload 7
    //   746: lstore 5
    //   748: aload 10
    //   750: astore 9
    //   752: iinc 2 1
    //   755: aload 9
    //   757: astore 10
    //   759: lload 5
    //   761: lstore 7
    //   763: aload 11
    //   765: astore 9
    //   767: goto -314 -> 453
    //   770: new 30	java/lang/StringBuilder
    //   773: astore 11
    //   775: aload 11
    //   777: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   780: aload 11
    //   782: ldc_w 980
    //   785: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   788: pop
    //   789: aload 11
    //   791: aload 10
    //   793: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   796: pop
    //   797: aload 11
    //   799: ldc_w 982
    //   802: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   805: pop
    //   806: aload 11
    //   808: lload 7
    //   810: invokevirtual 715	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   813: pop
    //   814: aload 11
    //   816: ldc_w 984
    //   819: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   822: pop
    //   823: aload 11
    //   825: lload_3
    //   826: invokevirtual 715	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   829: pop
    //   830: aload 11
    //   832: ldc_w 986
    //   835: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   838: pop
    //   839: aload 11
    //   841: aload_0
    //   842: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   845: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   848: pop
    //   849: ldc -128
    //   851: aload 11
    //   853: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   856: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   859: aload 10
    //   861: invokestatic 151	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   864: ifne +118 -> 982
    //   867: iload_1
    //   868: ifeq +40 -> 908
    //   871: lload 7
    //   873: lload_3
    //   874: lcmp
    //   875: ifle +11 -> 886
    //   878: ldc -128
    //   880: ldc_w 988
    //   883: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   886: aload_0
    //   887: aload 10
    //   889: putfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   892: aload_0
    //   893: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   896: aload_0
    //   897: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   900: lload 7
    //   902: invokestatic 953	cn/jiguang/sdk/impl/b:a	(Landroid/content/Context;Ljava/lang/String;J)V
    //   905: goto +85 -> 990
    //   908: lload_3
    //   909: lload 7
    //   911: lcmp
    //   912: ifeq +26 -> 938
    //   915: ldc -128
    //   917: ldc_w 990
    //   920: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   923: aload_0
    //   924: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   927: aload_0
    //   928: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   931: lload_3
    //   932: invokestatic 953	cn/jiguang/sdk/impl/b:a	(Landroid/content/Context;Ljava/lang/String;J)V
    //   935: goto +38 -> 973
    //   938: aload_0
    //   939: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   942: aload 10
    //   944: invokevirtual 120	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   947: ifne +26 -> 973
    //   950: ldc -128
    //   952: ldc_w 992
    //   955: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   958: invokestatic 946	java/lang/System:currentTimeMillis	()J
    //   961: lstore_3
    //   962: aload_0
    //   963: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   966: ldc_w 386
    //   969: lload_3
    //   970: invokestatic 953	cn/jiguang/sdk/impl/b:a	(Landroid/content/Context;Ljava/lang/String;J)V
    //   973: aload_0
    //   974: aload 10
    //   976: putfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   979: goto +11 -> 990
    //   982: ldc -128
    //   984: ldc_w 994
    //   987: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   990: new 30	java/lang/StringBuilder
    //   993: astore 10
    //   995: aload 10
    //   997: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   1000: aload 10
    //   1002: ldc_w 996
    //   1005: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1008: pop
    //   1009: aload 10
    //   1011: aload_0
    //   1012: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   1015: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1018: pop
    //   1019: aload 10
    //   1021: ldc_w 998
    //   1024: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1027: pop
    //   1028: aload 10
    //   1030: aload 9
    //   1032: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1035: pop
    //   1036: ldc -128
    //   1038: aload 10
    //   1040: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1043: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   1046: aload_0
    //   1047: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   1050: invokestatic 558	cn/jiguang/sdk/impl/b:e	(Landroid/content/Context;)J
    //   1053: aload_0
    //   1054: getfield 573	cn/jiguang/am/a:e	Ljava/lang/String;
    //   1057: aload_0
    //   1058: getfield 671	cn/jiguang/am/a:d	[J
    //   1061: invokestatic 685	cn/jiguang/ak/b:a	(JLjava/lang/String;[J)[B
    //   1064: astore 9
    //   1066: aload_0
    //   1067: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   1070: getstatic 689	cn/jiguang/sdk/impl/a:f	Ljava/lang/String;
    //   1073: bipush 30
    //   1075: iconst_0
    //   1076: invokestatic 693	cn/jiguang/ah/j:b	()J
    //   1079: lconst_0
    //   1080: aload 9
    //   1082: invokestatic 696	cn/jiguang/sdk/impl/b:a	(Landroid/content/Context;Ljava/lang/String;IIJJ[B)V
    //   1085: goto +58 -> 1143
    //   1088: ldc -128
    //   1090: ldc_w 1000
    //   1093: invokestatic 133	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   1096: goto -687 -> 409
    //   1099: astore 9
    //   1101: goto +58 -> 1159
    //   1104: astore 9
    //   1106: new 30	java/lang/StringBuilder
    //   1109: astore 10
    //   1111: aload 10
    //   1113: invokespecial 33	java/lang/StringBuilder:<init>	()V
    //   1116: aload 10
    //   1118: ldc_w 1002
    //   1121: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1124: pop
    //   1125: aload 10
    //   1127: aload 9
    //   1129: invokevirtual 674	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1132: pop
    //   1133: ldc -128
    //   1135: aload 10
    //   1137: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1140: invokestatic 765	cn/jiguang/ai/a:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   1143: aload_0
    //   1144: iconst_0
    //   1145: putfield 647	cn/jiguang/am/a:i	Z
    //   1148: aload_0
    //   1149: aload_0
    //   1150: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   1153: invokespecial 968	cn/jiguang/am/a:c	(Landroid/content/Context;)V
    //   1156: return
    //   1157: astore 9
    //   1159: aload_0
    //   1160: iconst_0
    //   1161: putfield 647	cn/jiguang/am/a:i	Z
    //   1164: aload_0
    //   1165: aload_0
    //   1166: getfield 651	cn/jiguang/am/a:a	Landroid/content/Context;
    //   1169: invokespecial 968	cn/jiguang/am/a:c	(Landroid/content/Context;)V
    //   1172: aload 9
    //   1174: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1175	0	this	a
    //   130	738	1	k	int
    //   260	493	2	m	int
    //   47	923	3	l1	long
    //   646	114	5	l2	long
    //   444	466	7	l3	long
    //   7	1074	9	localObject1	Object
    //   1099	1	9	localObject2	Object
    //   1104	24	9	localThrowable	Throwable
    //   1157	16	9	localObject3	Object
    //   264	872	10	localObject4	Object
    //   467	385	11	localObject5	Object
    //   300	426	12	localArrayList	ArrayList
    // Exception table:
    //   from	to	target	type
    //   0	9	1099	finally
    //   14	48	1099	finally
    //   55	120	1099	finally
    //   134	223	1099	finally
    //   225	235	1099	finally
    //   240	250	1099	finally
    //   253	316	1099	finally
    //   316	375	1099	finally
    //   378	388	1099	finally
    //   391	409	1099	finally
    //   423	436	1099	finally
    //   453	464	1099	finally
    //   0	9	1104	java/lang/Throwable
    //   14	48	1104	java/lang/Throwable
    //   55	120	1104	java/lang/Throwable
    //   134	223	1104	java/lang/Throwable
    //   225	235	1104	java/lang/Throwable
    //   240	250	1104	java/lang/Throwable
    //   253	316	1104	java/lang/Throwable
    //   316	375	1104	java/lang/Throwable
    //   378	388	1104	java/lang/Throwable
    //   391	409	1104	java/lang/Throwable
    //   423	436	1104	java/lang/Throwable
    //   453	464	1104	java/lang/Throwable
    //   464	640	1104	java/lang/Throwable
    //   660	741	1104	java/lang/Throwable
    //   770	867	1104	java/lang/Throwable
    //   878	886	1104	java/lang/Throwable
    //   886	905	1104	java/lang/Throwable
    //   915	935	1104	java/lang/Throwable
    //   938	973	1104	java/lang/Throwable
    //   973	979	1104	java/lang/Throwable
    //   982	990	1104	java/lang/Throwable
    //   990	1085	1104	java/lang/Throwable
    //   1088	1096	1104	java/lang/Throwable
    //   464	640	1157	finally
    //   660	741	1157	finally
    //   770	867	1157	finally
    //   878	886	1157	finally
    //   886	905	1157	finally
    //   915	935	1157	finally
    //   938	973	1157	finally
    //   973	979	1157	finally
    //   982	990	1157	finally
    //   990	1085	1157	finally
    //   1088	1096	1157	finally
    //   1106	1143	1157	finally
  }
  
  class a
  {
    private int b = -1;
    private String c;
    private long d;
    private String e;
    private String f;
    private int g;
    private long h;
    
    a() {}
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("SharePrcocessBean{idc=");
      localStringBuilder.append(this.b);
      localStringBuilder.append(", uuid='");
      localStringBuilder.append(this.c);
      localStringBuilder.append('\'');
      localStringBuilder.append(", uid=");
      localStringBuilder.append(this.d);
      localStringBuilder.append(", pkgname='");
      localStringBuilder.append(this.e);
      localStringBuilder.append('\'');
      localStringBuilder.append(", appkey='");
      localStringBuilder.append(this.f);
      localStringBuilder.append('\'');
      localStringBuilder.append(", sdkVersion=");
      localStringBuilder.append(this.g);
      localStringBuilder.append(", uuidCreateTime=");
      localStringBuilder.append(this.h);
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/am/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */