package cn.jiguang.a;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import cn.jiguang.ab.b;
import cn.jiguang.ab.d;
import cn.jiguang.api.JAnalyticsAction;
import cn.jiguang.api.JCoreManager;
import cn.jpush.android.service.DataShare;
import cn.jpush.android.service.JCommonService;
import cn.jpush.android.service.PushReceiver;

public class a
{
  public static Context a;
  public static JAnalyticsAction b;
  public static boolean c = false;
  public static String d = "JCore";
  public static boolean e = false;
  public static boolean f = false;
  public static boolean g;
  public static String h = "";
  private static Boolean i;
  private static Boolean j;
  private static ServiceConnection k = new ServiceConnection()
  {
    public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("action - onServiceConnected, ComponentName:");
      localStringBuilder.append(paramAnonymousComponentName.toString());
      cn.jiguang.ac.c.b("JCoreGobal", localStringBuilder.toString());
      cn.jiguang.ac.c.d("JCoreGobal", "Remote Service bind success.");
      DataShare.init(cn.jiguang.d.a.a.asInterface(paramAnonymousIBinder));
      if (a.a != null) {
        JCoreManager.init(a.a);
      }
    }
    
    public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("action - onServiceDisconnected, ComponentName:");
      localStringBuilder.append(paramAnonymousComponentName.toString());
      cn.jiguang.ac.c.b("JCoreGobal", localStringBuilder.toString());
    }
  };
  private static boolean l;
  
  static
  {
    g = true;
    l = false;
  }
  
  public static Context a(Context paramContext)
  {
    if ((a == null) && (paramContext != null)) {
      a = paramContext.getApplicationContext();
    }
    return a;
  }
  
  public static void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    cn.jiguang.ar.a.a("SDK_INIT", new a(paramContext, false, paramString, paramBundle));
  }
  
  public static void a(Context paramContext, boolean paramBoolean, long paramLong)
  {
    try
    {
      localObject = new android/os/Bundle;
      ((Bundle)localObject).<init>();
      ((Bundle)localObject).putBoolean("force", paramBoolean);
      ((Bundle)localObject).putLong("delay_time", paramLong);
      a(paramContext, "tcp_a2", (Bundle)localObject);
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("sendHeartBeat error:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ac.c.g("JCoreGobal", ((StringBuilder)localObject).toString());
    }
  }
  
  public static boolean a()
  {
    return false;
  }
  
  public static void b(Context paramContext, String paramString, Bundle paramBundle)
  {
    cn.jiguang.ar.a.a("SDK_SERVICE_INIT", new a(paramContext, true, paramString, paramBundle));
  }
  
  public static boolean b(Context paramContext)
  {
    try
    {
      if (i != null)
      {
        bool = i.booleanValue();
        return bool;
      }
      if (paramContext == null)
      {
        cn.jiguang.ac.c.i("JCoreGobal", "init failed,context is null");
        return false;
      }
      cn.jiguang.ac.c.e("JCoreGobal", "action:init jcore,version:2.0.0,build id:1");
      cn.jiguang.ac.c.b("JCoreGobal", "build type:release");
      a = paramContext.getApplicationContext();
      Context localContext = paramContext.getApplicationContext();
      cn.jiguang.ab.c.a();
      paramContext = d.b(localContext);
      if (((cn.jiguang.ab.c.a().c()) || (cn.jiguang.ab.c.a().b())) && (TextUtils.isEmpty(paramContext)))
      {
        i = Boolean.valueOf(false);
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("AndroidManifest.xml missing required service:");
        paramContext.append(JCommonService.class.getCanonicalName());
        paramContext.append(",please custom one service and extends JCommonService");
        cn.jiguang.ac.c.i("JCoreGobal", paramContext.toString());
        return false;
      }
      cn.jiguang.b.a.a().b();
      f(localContext);
      e(localContext);
      i = Boolean.valueOf(true);
      boolean bool = i.booleanValue();
      return bool;
    }
    finally {}
  }
  
  public static boolean c(Context paramContext)
  {
    try
    {
      if (j != null)
      {
        bool = j.booleanValue();
        return bool;
      }
      if (paramContext == null)
      {
        cn.jiguang.ac.c.i("JCoreGobal", "init failed,context is null");
        return false;
      }
      cn.jiguang.ac.c.b("JCoreGobal", "serviceInit...");
      a = paramContext.getApplicationContext();
      paramContext = paramContext.getApplicationContext();
      if (!b.d(paramContext))
      {
        j = Boolean.valueOf(false);
        bool = j.booleanValue();
        return bool;
      }
      cn.jiguang.ab.c.a();
      d(paramContext);
      j = Boolean.valueOf(true);
      cn.jiguang.b.a.a().d(paramContext);
      cn.jiguang.ah.e.a(paramContext, "service_create", null);
      boolean bool = j.booleanValue();
      return bool;
    }
    finally {}
  }
  
  private static void d(Context paramContext)
  {
    try
    {
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>();
      localObject = new cn/jpush/android/service/PushReceiver;
      ((PushReceiver)localObject).<init>();
      localIntentFilter.addAction("android.intent.action.USER_PRESENT");
      localIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
      paramContext.registerReceiver((BroadcastReceiver)localObject, localIntentFilter);
      if (!cn.jiguang.as.e.a(paramContext, PushReceiver.class))
      {
        localIntentFilter = new android/content/IntentFilter;
        localIntentFilter.<init>();
        localIntentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        localIntentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter.addDataScheme("package");
        paramContext.registerReceiver((BroadcastReceiver)localObject, localIntentFilter);
      }
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("registerPushReceiver fail:");
      ((StringBuilder)localObject).append(paramContext);
      cn.jiguang.ac.c.h("JCoreGobal", ((StringBuilder)localObject).toString());
    }
  }
  
  private static void e(Context paramContext)
  {
    Object localObject = d.b(paramContext);
    if (TextUtils.isEmpty((CharSequence)localObject))
    {
      cn.jiguang.ac.c.d("JCoreGobal", "not found commonServiceClass（JCommonService）");
      return;
    }
    if (DataShare.isBinding())
    {
      cn.jiguang.ac.c.b("JCoreGobal", "is binding service");
      return;
    }
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      localIntent.setClass(paramContext, Class.forName((String)localObject));
      if (paramContext.bindService(localIntent, k, 1))
      {
        cn.jiguang.ac.c.a("JCoreGobal", "Remote Service on binding...");
        DataShare.setBinding();
      }
      else
      {
        cn.jiguang.ac.c.a("JCoreGobal", "Remote Service bind failed");
      }
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Remote Service bind failed :");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ac.c.g("JCoreGobal", ((StringBuilder)localObject).toString());
    }
    catch (SecurityException paramContext)
    {
      cn.jiguang.ac.c.g("JCoreGobal", "Remote Service bind failed caused by SecurityException!");
    }
  }
  
  private static void f(Context paramContext)
  {
    cn.jiguang.ac.c.b("JCoreGobal", "ActivityLifecycle init");
    try
    {
      Object localObject;
      if ((Build.VERSION.SDK_INT >= 14) && ((paramContext instanceof Application)))
      {
        localObject = cn.jiguang.as.a.a(paramContext);
        String str = paramContext.getPackageName();
        if ((localObject != null) && (str != null) && (paramContext.getPackageName().equals(localObject)))
        {
          g = false;
          Application localApplication = (Application)paramContext;
          paramContext = new cn/jiguang/c/a;
          paramContext.<init>();
          localApplication.registerActivityLifecycleCallbacks(paramContext);
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append("registerActivityLifecycleCallbacks in main process,packageName:");
          paramContext.append(str);
          paramContext.append(",currentProcessName:");
          paramContext.append((String)localObject);
        }
        for (paramContext = paramContext.toString();; paramContext = paramContext.toString())
        {
          cn.jiguang.ac.c.b("JCoreGobal", paramContext);
          break;
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append("need not registerActivityLifecycleCallbacks in other process :");
          paramContext.append((String)localObject);
        }
      }
      return;
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("registerActivityLifecycleCallbacks failed:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ac.c.g("JCoreGobal", ((StringBuilder)localObject).toString());
      g = true;
    }
  }
  
  static class a
    implements Runnable
  {
    Context a;
    boolean b;
    String c;
    Bundle d;
    
    a(Context paramContext, boolean paramBoolean, String paramString, Bundle paramBundle)
    {
      this.a = paramContext;
      this.b = paramBoolean;
      this.c = paramString;
      this.d = paramBundle;
    }
    
    public void run()
    {
      try
      {
        if (this.b)
        {
          if (a.c(this.a)) {
            d.a().c(this.a, this.c, this.d);
          }
        }
        else if (a.b(this.a)) {
          d.a().a(this.a, this.c, this.d);
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */