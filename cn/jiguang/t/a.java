package cn.jiguang.t;

import java.math.BigInteger;

public class a
{
  private static final BigInteger d = BigInteger.ONE.shiftLeft(64);
  private byte[] a;
  private int b;
  private int c;
  
  public a()
  {
    this(32);
  }
  
  public a(int paramInt)
  {
    this.a = new byte[paramInt];
    this.b = 0;
    this.c = -1;
  }
  
  private void a(long paramLong, int paramInt)
  {
    long l = 1L << paramInt;
    if ((paramLong < 0L) || (paramLong > l))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" out of range for ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" bit value max:");
      localStringBuilder.append(l);
      cn.jiguang.ai.a.g("JCommonPackager", localStringBuilder.toString());
    }
  }
  
  private void b(int paramInt)
  {
    byte[] arrayOfByte = this.a;
    int i = arrayOfByte.length;
    int k = this.b;
    if (i - k >= paramInt) {
      return;
    }
    int j = arrayOfByte.length * 2;
    i = j;
    if (j < k + paramInt) {
      i = k + paramInt;
    }
    arrayOfByte = new byte[i];
    System.arraycopy(this.a, 0, arrayOfByte, 0, this.b);
    this.a = arrayOfByte;
  }
  
  public void a(int paramInt)
  {
    a(paramInt, 16);
    b(2);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >>> 8 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
  }
  
  public void a(long paramLong)
  {
    b(8);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 56 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 48 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 40 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 32 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public void a(byte[] paramArrayOfByte)
  {
    a(paramArrayOfByte.length);
    a(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    b(paramInt2);
    System.arraycopy(paramArrayOfByte, paramInt1, this.a, this.b, paramInt2);
    this.b += paramInt2;
  }
  
  public byte[] a()
  {
    int i = this.b;
    byte[] arrayOfByte = new byte[i];
    System.arraycopy(this.a, 0, arrayOfByte, 0, i);
    return arrayOfByte;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/t/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */