package cn.jiguang.ag;

import java.io.IOException;
import java.nio.ByteBuffer;

public class b
{
  private ByteBuffer a;
  private int b;
  private int c;
  
  public b(byte[] paramArrayOfByte)
  {
    this.a = ByteBuffer.wrap(paramArrayOfByte);
    this.b = -1;
    this.c = -1;
  }
  
  private void c(int paramInt)
  {
    if (paramInt <= b()) {
      return;
    }
    throw new IOException("end of input");
  }
  
  public int a()
  {
    return this.a.position();
  }
  
  public void a(int paramInt)
  {
    if (paramInt <= this.a.capacity() - this.a.position())
    {
      ByteBuffer localByteBuffer = this.a;
      localByteBuffer.limit(localByteBuffer.position() + paramInt);
      return;
    }
    throw new IllegalArgumentException("cannot set active region past end of input");
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    c(paramInt2);
    this.a.get(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public int b()
  {
    return this.a.remaining();
  }
  
  public void b(int paramInt)
  {
    if (paramInt < this.a.capacity())
    {
      this.a.position(paramInt);
      ByteBuffer localByteBuffer = this.a;
      localByteBuffer.limit(localByteBuffer.capacity());
      return;
    }
    throw new IllegalArgumentException("cannot jump past end of input");
  }
  
  public void c()
  {
    ByteBuffer localByteBuffer = this.a;
    localByteBuffer.limit(localByteBuffer.capacity());
  }
  
  public void d()
  {
    this.b = this.a.position();
    this.c = this.a.limit();
  }
  
  public void e()
  {
    int i = this.b;
    if (i >= 0)
    {
      this.a.position(i);
      this.a.limit(this.c);
      this.b = -1;
      this.c = -1;
      return;
    }
    throw new IllegalStateException("no previous state");
  }
  
  public int f()
  {
    c(1);
    return this.a.get() & 0xFF;
  }
  
  public int g()
  {
    c(2);
    return this.a.getShort() & 0xFFFF;
  }
  
  public long h()
  {
    c(4);
    return this.a.getInt() & 0xFFFFFFFF;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */