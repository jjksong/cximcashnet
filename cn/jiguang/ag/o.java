package cn.jiguang.ag;

import java.io.EOFException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public final class o
{
  protected long a;
  protected SelectionKey b;
  
  /* Error */
  public o(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 13	java/lang/Object:<init>	()V
    //   4: invokestatic 19	java/nio/channels/SocketChannel:open	()Ljava/nio/channels/SocketChannel;
    //   7: astore 5
    //   9: aload_0
    //   10: lload_1
    //   11: putfield 21	cn/jiguang/ag/o:a	J
    //   14: invokestatic 26	java/nio/channels/Selector:open	()Ljava/nio/channels/Selector;
    //   17: astore 4
    //   19: aload 5
    //   21: iconst_0
    //   22: invokevirtual 30	java/nio/channels/SocketChannel:configureBlocking	(Z)Ljava/nio/channels/SelectableChannel;
    //   25: pop
    //   26: aload_0
    //   27: aload 5
    //   29: aload 4
    //   31: iconst_1
    //   32: invokevirtual 34	java/nio/channels/SocketChannel:register	(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
    //   35: putfield 36	cn/jiguang/ag/o:b	Ljava/nio/channels/SelectionKey;
    //   38: return
    //   39: astore_3
    //   40: goto +7 -> 47
    //   43: astore_3
    //   44: aconst_null
    //   45: astore 4
    //   47: aload 4
    //   49: ifnull +8 -> 57
    //   52: aload 4
    //   54: invokevirtual 39	java/nio/channels/Selector:close	()V
    //   57: aload 5
    //   59: invokevirtual 40	java/nio/channels/SocketChannel:close	()V
    //   62: aload_3
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	o
    //   0	64	1	paramLong	long
    //   39	1	3	localObject1	Object
    //   43	20	3	localObject2	Object
    //   17	36	4	localSelector	Selector
    //   7	51	5	localSocketChannel	SocketChannel
    // Exception table:
    //   from	to	target	type
    //   19	38	39	finally
    //   14	19	43	finally
  }
  
  protected static void a(SelectionKey paramSelectionKey, long paramLong)
  {
    paramLong -= System.currentTimeMillis();
    int i;
    if (paramLong > 0L) {
      i = paramSelectionKey.selector().select(paramLong);
    } else if (paramLong == 0L) {
      i = paramSelectionKey.selector().selectNow();
    } else {
      i = 0;
    }
    if (i != 0) {
      return;
    }
    throw new SocketTimeoutException();
  }
  
  private byte[] a(int paramInt)
  {
    SocketChannel localSocketChannel = (SocketChannel)this.b.channel();
    Object localObject = new byte[paramInt];
    ByteBuffer localByteBuffer = ByteBuffer.wrap((byte[])localObject);
    this.b.interestOps(1);
    int i = 0;
    while (i < paramInt) {
      try
      {
        if (this.b.isReadable())
        {
          long l = localSocketChannel.read(localByteBuffer);
          if (l >= 0L)
          {
            int j = i + (int)l;
            i = j;
            if (j < paramInt) {
              if (System.currentTimeMillis() <= this.a)
              {
                i = j;
              }
              else
              {
                localObject = new java/net/SocketTimeoutException;
                ((SocketTimeoutException)localObject).<init>();
                throw ((Throwable)localObject);
              }
            }
          }
          else
          {
            localObject = new java/io/EOFException;
            ((EOFException)localObject).<init>();
            throw ((Throwable)localObject);
          }
        }
        else
        {
          a(this.b, this.a);
        }
      }
      finally
      {
        if (this.b.isValid()) {
          this.b.interestOps(0);
        }
      }
    }
    if (this.b.isValid()) {
      this.b.interestOps(0);
    }
    return arrayOfByte;
  }
  
  public static byte[] a(SocketAddress paramSocketAddress1, SocketAddress paramSocketAddress2, byte[] paramArrayOfByte, long paramLong)
  {
    o localo = new o(paramLong);
    if (paramSocketAddress1 != null) {}
    try
    {
      localo.a(paramSocketAddress1);
      localo.b(paramSocketAddress2);
      localo.a(paramArrayOfByte);
      paramSocketAddress1 = localo.a();
      return paramSocketAddress1;
    }
    finally
    {
      localo.b();
    }
  }
  
  void a(SocketAddress paramSocketAddress)
  {
    ((SocketChannel)this.b.channel()).socket().bind(paramSocketAddress);
  }
  
  void a(byte[] paramArrayOfByte)
  {
    SocketChannel localSocketChannel = (SocketChannel)this.b.channel();
    ByteBuffer localByteBuffer2 = ByteBuffer.wrap(new byte[] { (byte)(paramArrayOfByte.length >>> 8), (byte)(paramArrayOfByte.length & 0xFF) });
    ByteBuffer localByteBuffer1 = ByteBuffer.wrap(paramArrayOfByte);
    this.b.interestOps(4);
    int i = 0;
    try
    {
      while (i < paramArrayOfByte.length + 2) {
        if (this.b.isWritable())
        {
          long l = localSocketChannel.write(new ByteBuffer[] { localByteBuffer2, localByteBuffer1 });
          if (l >= 0L)
          {
            int j = i + (int)l;
            i = j;
            if (j < paramArrayOfByte.length + 2) {
              if (System.currentTimeMillis() <= this.a)
              {
                i = j;
              }
              else
              {
                paramArrayOfByte = new java/net/SocketTimeoutException;
                paramArrayOfByte.<init>();
                throw paramArrayOfByte;
              }
            }
          }
          else
          {
            paramArrayOfByte = new java/io/EOFException;
            paramArrayOfByte.<init>();
            throw paramArrayOfByte;
          }
        }
        else
        {
          a(this.b, this.a);
        }
      }
      return;
    }
    finally
    {
      if (this.b.isValid()) {
        this.b.interestOps(0);
      }
    }
  }
  
  byte[] a()
  {
    Object localObject = a(2);
    byte[] arrayOfByte = a(((localObject[0] & 0xFF) << 8) + (localObject[1] & 0xFF));
    localObject = (SocketChannel)this.b.channel();
    return arrayOfByte;
  }
  
  void b()
  {
    this.b.selector().close();
    this.b.channel().close();
  }
  
  void b(SocketAddress paramSocketAddress)
  {
    SocketChannel localSocketChannel = (SocketChannel)this.b.channel();
    if (localSocketChannel.connect(paramSocketAddress)) {
      return;
    }
    this.b.interestOps(8);
    try
    {
      while (!localSocketChannel.finishConnect()) {
        if (!this.b.isConnectable()) {
          a(this.b, this.a);
        }
      }
      return;
    }
    finally
    {
      if (this.b.isValid()) {
        this.b.interestOps(0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */