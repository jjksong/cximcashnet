package cn.jiguang.ag;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Arrays;

public abstract class i
  implements Serializable, Cloneable, Comparable
{
  private static final DecimalFormat e = new DecimalFormat();
  protected g a;
  protected int b;
  protected int c;
  protected long d;
  
  static
  {
    e.setMinimumIntegerDigits(3);
  }
  
  static i a(b paramb, int paramInt)
  {
    g localg = new g(paramb);
    int j = paramb.g();
    int i = paramb.g();
    if (paramInt == 0) {
      return a(localg, j, i);
    }
    return a(localg, j, i, paramb.h(), paramb.g(), paramb);
  }
  
  public static i a(g paramg, int paramInt1, int paramInt2)
  {
    return a(paramg, paramInt1, paramInt2, 0L);
  }
  
  public static i a(g paramg, int paramInt1, int paramInt2, long paramLong)
  {
    if (paramg.a()) {
      return a(paramg, paramInt1, paramInt2, paramLong, false);
    }
    throw new j(paramg);
  }
  
  private static i a(g paramg, int paramInt1, int paramInt2, long paramLong, int paramInt3, b paramb)
  {
    boolean bool;
    if (paramb != null) {
      bool = true;
    } else {
      bool = false;
    }
    paramg = a(paramg, paramInt1, paramInt2, paramLong, bool);
    if (paramb != null) {
      if (paramb.b() >= paramInt3)
      {
        paramb.a(paramInt3);
        paramg.a(paramb);
        if (paramb.b() <= 0) {
          paramb.c();
        } else {
          throw new IOException("invalid record length");
        }
      }
      else
      {
        throw new IOException("truncated record");
      }
    }
    return paramg;
  }
  
  private static final i a(g paramg, int paramInt1, int paramInt2, long paramLong, boolean paramBoolean)
  {
    m localm = new m();
    localm.a = paramg;
    localm.b = paramInt1;
    localm.c = paramInt2;
    localm.d = paramLong;
    return localm;
  }
  
  private void a(c paramc, boolean paramBoolean)
  {
    this.a.a(paramc);
    paramc.c(this.b);
    paramc.c(this.c);
    long l;
    if (paramBoolean) {
      l = 0L;
    } else {
      l = this.d;
    }
    paramc.a(l);
    int i = paramc.a();
    paramc.c(0);
    a(paramc, null, true);
    paramc.a(paramc.a() - i - 2, i);
  }
  
  private byte[] a(boolean paramBoolean)
  {
    c localc = new c();
    a(localc, paramBoolean);
    return localc.b();
  }
  
  void a(long paramLong)
  {
    this.d = paramLong;
  }
  
  abstract void a(b paramb);
  
  void a(c paramc, int paramInt, a parama)
  {
    this.a.a(paramc, parama);
    paramc.c(this.b);
    paramc.c(this.c);
  }
  
  abstract void a(c paramc, a parama, boolean paramBoolean);
  
  public boolean a(i parami)
  {
    boolean bool;
    if ((f() == parami.f()) && (this.c == parami.c) && (this.a.equals(parami.a))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public byte[] a()
  {
    c localc = new c();
    a(localc, null, true);
    return localc.b();
  }
  
  abstract String b();
  
  public String c()
  {
    return b();
  }
  
  public int compareTo(Object paramObject)
  {
    Object localObject = (i)paramObject;
    int i = 0;
    if (this == localObject) {
      return 0;
    }
    int j = this.a.compareTo(((i)localObject).a);
    if (j != 0) {
      return j;
    }
    j = this.c - ((i)localObject).c;
    if (j != 0) {
      return j;
    }
    j = this.b - ((i)localObject).b;
    if (j != 0) {
      return j;
    }
    paramObject = a();
    localObject = ((i)localObject).a();
    while ((i < paramObject.length) && (i < localObject.length))
    {
      j = (paramObject[i] & 0xFF) - (localObject[i] & 0xFF);
      if (j != 0) {
        return j;
      }
      i++;
    }
    return paramObject.length - localObject.length;
  }
  
  public g d()
  {
    return this.a;
  }
  
  public int e()
  {
    return this.b;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && ((paramObject instanceof i)))
    {
      paramObject = (i)paramObject;
      if ((this.b == ((i)paramObject).b) && (this.c == ((i)paramObject).c) && (this.a.equals(((i)paramObject).a))) {
        return Arrays.equals(a(), ((i)paramObject).a());
      }
    }
    return false;
  }
  
  public int f()
  {
    return this.b;
  }
  
  public int g()
  {
    return this.c;
  }
  
  public long h()
  {
    return this.d;
  }
  
  public int hashCode()
  {
    byte[] arrayOfByte = a(true);
    int j = 0;
    int i = 0;
    while (j < arrayOfByte.length)
    {
      i += (i << 3) + (arrayOfByte[j] & 0xFF);
      j++;
    }
    return i;
  }
  
  i i()
  {
    try
    {
      i locali = (i)clone();
      return locali;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new IllegalStateException();
    }
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(this.a);
    if (localStringBuffer.length() < 8) {
      localStringBuffer.append("\t");
    }
    if (localStringBuffer.length() < 16) {
      localStringBuffer.append("\t");
    }
    localStringBuffer.append("\t");
    String str = b();
    if (!str.equals(""))
    {
      localStringBuffer.append("\t");
      localStringBuffer.append(str);
    }
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */