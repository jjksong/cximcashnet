package cn.jiguang.ag;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;

public class g
  implements Serializable, Comparable
{
  public static final g a;
  public static final g b;
  private static final byte[] f = { 0 };
  private static final byte[] g = { 1, 42 };
  private static final DecimalFormat h = new DecimalFormat();
  private static final byte[] i = new byte['Ā'];
  private static final g j;
  private byte[] c;
  private long d;
  private int e;
  
  static
  {
    h.setMinimumIntegerDigits(3);
    for (int k = 0;; k++)
    {
      byte[] arrayOfByte = i;
      if (k >= arrayOfByte.length) {
        break;
      }
      if ((k >= 65) && (k <= 90)) {
        arrayOfByte[k] = ((byte)(k - 65 + 97));
      } else {
        i[k] = ((byte)k);
      }
    }
    a = new g();
    a.b(f, 0, 1);
    b = new g();
    b.c = new byte[0];
    j = new g();
    j.b(g, 0, 1);
  }
  
  private g() {}
  
  public g(b paramb)
  {
    byte[] arrayOfByte = new byte[64];
    int m = 0;
    int k = 0;
    while (m == 0)
    {
      int n = paramb.f();
      int i1 = n & 0xC0;
      if (i1 != 0)
      {
        if (i1 == 192)
        {
          i1 = paramb.f() + ((n & 0xFF3F) << 8);
          if (i1 < paramb.a() - 2)
          {
            n = k;
            if (k == 0)
            {
              paramb.d();
              n = 1;
            }
            paramb.b(i1);
            k = n;
          }
          else
          {
            throw new IOException("bad compression");
          }
        }
        else
        {
          throw new IOException("bad label type");
        }
      }
      else if (d() < 128)
      {
        if (n == 0)
        {
          a(f, 0, 1);
          m = 1;
        }
        else
        {
          arrayOfByte[0] = ((byte)n);
          paramb.a(arrayOfByte, 1, n);
          a(arrayOfByte, 0, 1);
        }
      }
      else {
        throw new IOException("too many labels");
      }
    }
    if (k != 0) {
      paramb.e();
    }
  }
  
  public g(g paramg, int paramInt)
  {
    int k = paramg.b();
    if (paramInt <= k)
    {
      this.c = paramg.c;
      int m = k - paramInt;
      b(m);
      for (k = 0; (k < 7) && (k < m); k++) {
        a(k, paramg.a(k + paramInt));
      }
      return;
    }
    throw new IllegalArgumentException("attempted to remove too many labels");
  }
  
  public g(String paramString, g paramg)
  {
    if (!paramString.equals(""))
    {
      if (paramString.equals("@"))
      {
        if (paramg == null) {
          b(b, this);
        } else {
          b(paramg, this);
        }
        return;
      }
      if (paramString.equals("."))
      {
        b(a, this);
        return;
      }
      byte[] arrayOfByte = new byte[64];
      int i2 = 0;
      int i1 = 0;
      int i4 = 0;
      int i5 = -1;
      int i3 = 1;
      int n = 0;
      while (i2 < paramString.length())
      {
        int m = (byte)paramString.charAt(i2);
        int i7;
        if (i4 != 0)
        {
          int k;
          if ((m >= 48) && (m <= 57) && (i1 < 3))
          {
            i1++;
            n = n * 10 + (m - 48);
            if (n <= 255)
            {
              if (i1 < 3) {
                break label391;
              }
              k = (byte)n;
              i4 = i1;
              i5 = n;
            }
            else
            {
              throw a(paramString, "bad escape");
            }
          }
          else
          {
            i4 = i1;
            i5 = n;
            k = m;
            if (i1 > 0) {
              if (i1 >= 3)
              {
                i4 = i1;
                i5 = n;
                k = m;
              }
              else
              {
                throw a(paramString, "bad escape");
              }
            }
          }
          if (i3 <= 63)
          {
            i7 = i3 + 1;
            arrayOfByte[i3] = k;
            i1 = 0;
            n = i5;
            i5 = i3;
            i3 = i1;
            i1 = i4;
          }
        }
        for (;;)
        {
          i4 = i3;
          i3 = i7;
          break;
          throw a(paramString, "label too long");
          if (m == 92)
          {
            i1 = 0;
            i4 = 1;
            n = 0;
            break;
          }
          if (m == 46)
          {
            if (i5 != -1)
            {
              arrayOfByte[0] = ((byte)(i3 - 1));
              a(paramString, arrayOfByte, 0, 1);
              i5 = -1;
              i3 = 1;
              break;
            }
            throw a(paramString, "invalid empty label");
          }
          int i6 = i5;
          if (i5 == -1) {
            i6 = i2;
          }
          if (i3 > 63) {
            break label397;
          }
          i7 = i3 + 1;
          arrayOfByte[i3] = m;
          i3 = i4;
          i5 = i6;
        }
        label391:
        i2++;
        continue;
        label397:
        throw a(paramString, "label too long");
      }
      if ((i1 > 0) && (i1 < 3)) {
        throw a(paramString, "bad escape");
      }
      if (i4 == 0)
      {
        if (i5 == -1)
        {
          arrayOfByte = f;
          n = 1;
          a(paramString, arrayOfByte, 0, 1);
        }
        else
        {
          arrayOfByte[0] = ((byte)(i3 - 1));
          a(paramString, arrayOfByte, 0, 1);
          n = 0;
        }
        if ((paramg != null) && (n == 0)) {
          a(paramString, paramg.c, paramg.a(0), paramg.d());
        }
        return;
      }
      throw a(paramString, "bad escape");
    }
    throw a(paramString, "empty name");
  }
  
  private final int a(int paramInt)
  {
    if ((paramInt == 0) && (d() == 0)) {
      return 0;
    }
    if ((paramInt >= 0) && (paramInt < d()))
    {
      if (paramInt < 7) {
        return (int)(this.d >>> (7 - paramInt) * 8) & 0xFF;
      }
      int m = 6;
      int k = a(6);
      while (m < paramInt)
      {
        k += this.c[k] + 1;
        m++;
      }
      return k;
    }
    throw new IllegalArgumentException("label out of range");
  }
  
  public static g a(g paramg1, g paramg2)
  {
    if (paramg1.a()) {
      return paramg1;
    }
    g localg = new g();
    b(paramg1, localg);
    localg.a(paramg2.c, paramg2.a(0), paramg2.d());
    return localg;
  }
  
  public static g a(String paramString)
  {
    return a(paramString, null);
  }
  
  public static g a(String paramString, g paramg)
  {
    if ((paramString.equals("@")) && (paramg != null)) {
      return paramg;
    }
    if (paramString.equals(".")) {
      return a;
    }
    return new g(paramString, paramg);
  }
  
  private static IOException a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("'");
    localStringBuilder.append(paramString1);
    localStringBuilder.append("': ");
    localStringBuilder.append(paramString2);
    return new IOException(localStringBuilder.toString());
  }
  
  private String a(byte[] paramArrayOfByte, int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int k = paramInt + 1;
    int m = paramArrayOfByte[paramInt];
    for (paramInt = k; paramInt < k + m; paramInt++)
    {
      int n = paramArrayOfByte[paramInt] & 0xFF;
      if ((n > 32) && (n < 127))
      {
        if ((n == 34) || (n == 40) || (n == 41) || (n == 46) || (n == 59) || (n == 92) || (n == 64) || (n == 36)) {
          localStringBuffer.append('\\');
        }
        localStringBuffer.append((char)n);
      }
      else
      {
        localStringBuffer.append('\\');
        localStringBuffer.append(h.format(n));
      }
    }
    return localStringBuffer.toString();
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 7) {
      return;
    }
    paramInt1 = (7 - paramInt1) * 8;
    this.d &= (255L << paramInt1 ^ 0xFFFFFFFFFFFFFFFF);
    long l = this.d;
    this.d = (paramInt2 << paramInt1 | l);
  }
  
  private final void a(String paramString, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      a(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (Exception paramArrayOfByte)
    {
      throw a(paramString, "Name too long");
    }
  }
  
  private final void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = this.c;
    int i2 = 0;
    int k;
    if (arrayOfByte == null) {
      k = 0;
    } else {
      k = arrayOfByte.length - a(0);
    }
    int i1 = paramInt1;
    int n = 0;
    int m = 0;
    while (n < paramInt2)
    {
      i3 = paramArrayOfByte[i1];
      if (i3 <= 63)
      {
        i3++;
        i1 += i3;
        m += i3;
        n++;
      }
      else
      {
        throw new IllegalStateException("invalid label");
      }
    }
    int i3 = k + m;
    if (i3 <= 255)
    {
      i1 = d();
      n = i1 + paramInt2;
      if (n <= 128)
      {
        arrayOfByte = new byte[i3];
        if (k != 0) {
          System.arraycopy(this.c, a(0), arrayOfByte, 0, k);
        }
        System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, k, m);
        this.c = arrayOfByte;
        for (paramInt1 = i2; paramInt1 < paramInt2; paramInt1++)
        {
          a(i1 + paramInt1, k);
          k += arrayOfByte[k] + 1;
        }
        b(n);
        return;
      }
      throw new IllegalStateException("too many labels");
    }
    throw new IOException();
  }
  
  private final void b(int paramInt)
  {
    this.d &= 0xFFFFFFFFFFFFFF00;
    this.d |= paramInt;
  }
  
  private static final void b(g paramg1, g paramg2)
  {
    int k = 0;
    if (paramg1.a(0) == 0)
    {
      paramg2.c = paramg1.c;
      paramg2.d = paramg1.d;
    }
    else
    {
      int n = paramg1.a(0);
      int i1 = paramg1.c.length - n;
      int m = paramg1.b();
      paramg2.c = new byte[i1];
      System.arraycopy(paramg1.c, n, paramg2.c, 0, i1);
      while ((k < m) && (k < 7))
      {
        paramg2.a(k, paramg1.a(k) - n);
        k++;
      }
      paramg2.b(m);
    }
  }
  
  private final void b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      a(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (Exception paramArrayOfByte)
    {
      for (;;) {}
    }
  }
  
  private final boolean b(byte[] paramArrayOfByte, int paramInt)
  {
    int i2 = b();
    int k = a(0);
    int m = 0;
    while (m < i2)
    {
      byte[] arrayOfByte = this.c;
      if (arrayOfByte[k] != paramArrayOfByte[paramInt]) {
        return false;
      }
      int n = k + 1;
      int i3 = arrayOfByte[k];
      if (i3 <= 63)
      {
        k = paramInt + 1;
        int i1 = 0;
        paramInt = n;
        n = i1;
        while (n < i3)
        {
          arrayOfByte = i;
          if (arrayOfByte[(this.c[paramInt] & 0xFF)] != arrayOfByte[(paramArrayOfByte[k] & 0xFF)]) {
            return false;
          }
          n++;
          k++;
          paramInt++;
        }
        n = m + 1;
        m = k;
        k = paramInt;
        paramInt = m;
        m = n;
      }
      else
      {
        throw new IllegalStateException("invalid label");
      }
    }
    return true;
  }
  
  private final int d()
  {
    return (int)(this.d & 0xFF);
  }
  
  public String a(boolean paramBoolean)
  {
    int n = b();
    if (n == 0) {
      return "@";
    }
    int m = 0;
    if ((n == 1) && (this.c[a(0)] == 0)) {
      return ".";
    }
    StringBuffer localStringBuffer = new StringBuffer();
    int k = a(0);
    while (m < n)
    {
      int i1 = this.c[k];
      if (i1 <= 63)
      {
        if (i1 == 0)
        {
          if (!paramBoolean) {
            localStringBuffer.append('.');
          }
        }
        else
        {
          if (m > 0) {
            localStringBuffer.append('.');
          }
          localStringBuffer.append(a(this.c, k));
          k += i1 + 1;
          m++;
        }
      }
      else {
        throw new IllegalStateException("invalid label");
      }
    }
    return localStringBuffer.toString();
  }
  
  public void a(c paramc)
  {
    paramc.a(c());
  }
  
  public void a(c paramc, a parama)
  {
    int n = b();
    for (int k = 0; k < n - 1; k++)
    {
      if (k == 0) {
        localObject = this;
      } else {
        localObject = new g(this, k);
      }
      int m = -1;
      if (parama != null) {
        m = parama.a((g)localObject);
      }
      if (m >= 0)
      {
        paramc.c(0xC000 | m);
        return;
      }
      if (parama != null) {
        parama.a(paramc.a(), (g)localObject);
      }
      m = a(k);
      Object localObject = this.c;
      paramc.a((byte[])localObject, m, localObject[m] + 1);
    }
    paramc.b(0);
  }
  
  public void a(c paramc, a parama, boolean paramBoolean)
  {
    if (paramBoolean) {
      a(paramc);
    } else {
      a(paramc, parama);
    }
  }
  
  public boolean a()
  {
    int k = b();
    boolean bool = false;
    if (k == 0) {
      return false;
    }
    if (this.c[a(k - 1)] == 0) {
      bool = true;
    }
    return bool;
  }
  
  public int b()
  {
    return d();
  }
  
  public byte[] c()
  {
    int i3 = b();
    if (i3 == 0) {
      return new byte[0];
    }
    byte[] arrayOfByte1 = new byte[this.c.length - a(0)];
    int m = a(0);
    int n = 0;
    int k = 0;
    while (n < i3)
    {
      byte[] arrayOfByte2 = this.c;
      int i4 = arrayOfByte2[m];
      if (i4 <= 63)
      {
        int i2 = m + 1;
        arrayOfByte1[k] = arrayOfByte2[m];
        k++;
        int i1 = 0;
        for (m = i2; i1 < i4; m++)
        {
          arrayOfByte1[k] = i[(this.c[m] & 0xFF)];
          i1++;
          k++;
        }
        n++;
      }
      else
      {
        throw new IllegalStateException("invalid label");
      }
    }
    return arrayOfByte1;
  }
  
  public int compareTo(Object paramObject)
  {
    paramObject = (g)paramObject;
    if (this == paramObject) {
      return 0;
    }
    int i2 = b();
    int i1 = ((g)paramObject).b();
    int k;
    if (i2 > i1) {
      k = i1;
    } else {
      k = i2;
    }
    for (int m = 1; m <= k; m++)
    {
      int i6 = a(i2 - m);
      int i3 = ((g)paramObject).a(i1 - m);
      int i4 = this.c[i6];
      int i5 = paramObject.c[i3];
      for (int n = 0; (n < i4) && (n < i5); n++)
      {
        byte[] arrayOfByte = i;
        int i7 = arrayOfByte[(this.c[(n + i6 + 1)] & 0xFF)] - arrayOfByte[(paramObject.c[(n + i3 + 1)] & 0xFF)];
        if (i7 != 0) {
          return i7;
        }
      }
      if (i4 != i5) {
        return i4 - i5;
      }
    }
    return i2 - i1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if ((paramObject != null) && ((paramObject instanceof g)))
    {
      paramObject = (g)paramObject;
      if (((g)paramObject).e == 0) {
        ((g)paramObject).hashCode();
      }
      if (this.e == 0) {
        hashCode();
      }
      if (((g)paramObject).e != this.e) {
        return false;
      }
      if (((g)paramObject).b() != b()) {
        return false;
      }
      return b(((g)paramObject).c, ((g)paramObject).a(0));
    }
    return false;
  }
  
  public int hashCode()
  {
    int k = this.e;
    if (k != 0) {
      return k;
    }
    int m = 0;
    for (k = a(0);; k++)
    {
      byte[] arrayOfByte = this.c;
      if (k >= arrayOfByte.length) {
        break;
      }
      m += (m << 3) + i[(arrayOfByte[k] & 0xFF)];
    }
    this.e = m;
    return this.e;
  }
  
  public String toString()
  {
    return a(false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */