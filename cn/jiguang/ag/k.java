package cn.jiguang.ag;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class k
{
  private static k d;
  private String[] a = null;
  private g[] b = null;
  private int c = -1;
  
  static {}
  
  public k()
  {
    if (d()) {
      return;
    }
    if (e()) {
      return;
    }
    if ((this.a == null) || (this.b == null)) {
      if (System.getProperty("java.vendor").indexOf("Android") != -1) {
        g();
      } else {
        f();
      }
    }
  }
  
  private int a(String paramString)
  {
    paramString = paramString.substring(6);
    try
    {
      int i = Integer.parseInt(paramString);
      if (i >= 0) {
        return i;
      }
    }
    catch (NumberFormatException paramString)
    {
      for (;;) {}
    }
    return -1;
  }
  
  private void a(int paramInt)
  {
    if ((this.c < 0) && (paramInt > 0)) {
      this.c = paramInt;
    }
  }
  
  private void a(String paramString, List paramList)
  {
    if (paramList.contains(paramString)) {
      return;
    }
    paramList.add(paramString);
  }
  
  private void a(List paramList1, List paramList2)
  {
    if ((this.a == null) && (paramList1.size() > 0)) {
      this.a = ((String[])paramList1.toArray(new String[0]));
    }
    if ((this.b == null) && (paramList2.size() > 0)) {
      this.b = ((g[])paramList2.toArray(new g[0]));
    }
  }
  
  public static k b()
  {
    try
    {
      k localk = d;
      return localk;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void b(String paramString)
  {
    for (;;)
    {
      int i;
      int j;
      try
      {
        localObject = new java/io/FileInputStream;
        ((FileInputStream)localObject).<init>(paramString);
        localBufferedReader = new BufferedReader(new InputStreamReader((InputStream)localObject));
        paramString = new ArrayList(0);
        localObject = new ArrayList(0);
        i = -1;
        j = i;
      }
      catch (FileNotFoundException paramString)
      {
        Object localObject;
        BufferedReader localBufferedReader;
        String str;
        continue;
      }
      try
      {
        str = localBufferedReader.readLine();
        if (str != null)
        {
          j = i;
          StringTokenizer localStringTokenizer;
          if (str.startsWith("nameserver"))
          {
            j = i;
            localStringTokenizer = new java/util/StringTokenizer;
            j = i;
            localStringTokenizer.<init>(str);
            j = i;
            localStringTokenizer.nextToken();
            j = i;
            a(localStringTokenizer.nextToken(), paramString);
          }
          else
          {
            j = i;
            if (str.startsWith("domain"))
            {
              j = i;
              localStringTokenizer = new java/util/StringTokenizer;
              j = i;
              localStringTokenizer.<init>(str);
              j = i;
              localStringTokenizer.nextToken();
              j = i;
              if (localStringTokenizer.hasMoreTokens())
              {
                j = i;
                if (((List)localObject).isEmpty())
                {
                  j = i;
                  b(localStringTokenizer.nextToken(), (List)localObject);
                }
              }
            }
            else
            {
              j = i;
              if (str.startsWith("search"))
              {
                j = i;
                if (!((List)localObject).isEmpty())
                {
                  j = i;
                  ((List)localObject).clear();
                }
                j = i;
                localStringTokenizer = new java/util/StringTokenizer;
                j = i;
                localStringTokenizer.<init>(str);
                j = i;
                localStringTokenizer.nextToken();
                j = i;
                if (localStringTokenizer.hasMoreTokens())
                {
                  j = i;
                  b(localStringTokenizer.nextToken(), (List)localObject);
                }
              }
              else
              {
                j = i;
                if (str.startsWith("options"))
                {
                  j = i;
                  localStringTokenizer = new java/util/StringTokenizer;
                  j = i;
                  localStringTokenizer.<init>(str);
                  j = i;
                  localStringTokenizer.nextToken();
                  int k = i;
                  i = k;
                  j = k;
                  if (localStringTokenizer.hasMoreTokens())
                  {
                    j = k;
                    str = localStringTokenizer.nextToken();
                    j = k;
                    if (str.startsWith("ndots:"))
                    {
                      j = k;
                      k = a(str);
                    }
                  }
                }
              }
            }
          }
        }
        else
        {
          j = i;
          localBufferedReader.close();
        }
      }
      catch (IOException localIOException)
      {
        i = j;
      }
    }
    a(paramString, (List)localObject);
    a(i);
  }
  
  private void b(String paramString, List paramList)
  {
    try
    {
      paramString = g.a(paramString, g.a);
      if (paramList.contains(paramString)) {
        return;
      }
      paramList.add(paramString);
      return;
    }
    catch (Exception paramString)
    {
      for (;;) {}
    }
  }
  
  public static void c()
  {
    k localk = new k();
    try
    {
      d = localk;
      return;
    }
    finally {}
  }
  
  private boolean d()
  {
    boolean bool2 = false;
    ArrayList localArrayList1 = new ArrayList(0);
    ArrayList localArrayList2 = new ArrayList(0);
    Object localObject = System.getProperty("dns.server");
    if (localObject != null)
    {
      localObject = new StringTokenizer((String)localObject, ",");
      while (((StringTokenizer)localObject).hasMoreTokens()) {
        a(((StringTokenizer)localObject).nextToken(), localArrayList1);
      }
    }
    localObject = System.getProperty("dns.search");
    if (localObject != null)
    {
      localObject = new StringTokenizer((String)localObject, ",");
      while (((StringTokenizer)localObject).hasMoreTokens()) {
        b(((StringTokenizer)localObject).nextToken(), localArrayList2);
      }
    }
    a(localArrayList1, localArrayList2);
    boolean bool1 = bool2;
    if (this.a != null)
    {
      bool1 = bool2;
      if (this.b != null) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private boolean e()
  {
    ArrayList localArrayList2 = new ArrayList(0);
    ArrayList localArrayList1 = new ArrayList(0);
    try
    {
      Class[] arrayOfClass = new Class[0];
      Object[] arrayOfObject = new Object[0];
      Object localObject2 = Class.forName("sun.net.dns.ResolverConfiguration");
      Object localObject3 = ((Class)localObject2).getDeclaredMethod("open", arrayOfClass).invoke(null, arrayOfObject);
      Object localObject1 = (List)((Class)localObject2).getMethod("nameservers", arrayOfClass).invoke(localObject3, arrayOfObject);
      localObject2 = (List)((Class)localObject2).getMethod("searchlist", arrayOfClass).invoke(localObject3, arrayOfObject);
      if (((List)localObject1).size() == 0) {
        return false;
      }
      if (((List)localObject1).size() > 0)
      {
        localObject1 = ((List)localObject1).iterator();
        while (((Iterator)localObject1).hasNext()) {
          a((String)((Iterator)localObject1).next(), localArrayList2);
        }
      }
      if (((List)localObject2).size() > 0)
      {
        localObject1 = ((List)localObject2).iterator();
        while (((Iterator)localObject1).hasNext()) {
          b((String)((Iterator)localObject1).next(), localArrayList1);
        }
      }
      a(localArrayList2, localArrayList1);
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  private void f()
  {
    b("/etc/resolv.conf");
  }
  
  private void g()
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    try
    {
      Method localMethod = Class.forName("android.os.SystemProperties").getMethod("get", new Class[] { String.class });
      String[] arrayOfString = new String[4];
      arrayOfString[0] = "net.dns1";
      arrayOfString[1] = "net.dns2";
      arrayOfString[2] = "net.dns3";
      arrayOfString[3] = "net.dns4";
      for (int i = 0; i < arrayOfString.length; i++)
      {
        String str = (String)localMethod.invoke(null, new Object[] { arrayOfString[i] });
        if ((str != null) && ((str.matches("^\\d+(\\.\\d+){3}$")) || (str.matches("^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$"))) && (!localArrayList1.contains(str))) {
          localArrayList1.add(str);
        }
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    a(localArrayList1, localArrayList2);
  }
  
  public String[] a()
  {
    return this.a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */