package cn.jiguang.ag;

import java.util.Random;

public class d
  implements Cloneable
{
  private static Random d = new Random();
  private int a;
  private int b = 256;
  private int[] c;
  
  public d()
  {
    d();
  }
  
  public d(int paramInt)
  {
    d();
    a(paramInt);
  }
  
  d(b paramb)
  {
    this(paramb.g());
    for (int i = 0;; i++)
    {
      int[] arrayOfInt = this.c;
      if (i >= arrayOfInt.length) {
        break;
      }
      arrayOfInt[i] = paramb.g();
    }
  }
  
  private void d()
  {
    this.c = new int[4];
    this.b = 256;
    this.a = -1;
  }
  
  public int a()
  {
    int i = this.a;
    if (i >= 0) {
      return i;
    }
    try
    {
      if (this.a < 0) {
        this.a = d.nextInt(65535);
      }
      i = this.a;
      return i;
    }
    finally {}
  }
  
  public void a(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt <= 65535))
    {
      this.a = paramInt;
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DNS message ID ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" is out of range");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  void a(c paramc)
  {
    paramc.c(a());
    paramc.c(this.b);
    for (int i = 0;; i++)
    {
      int[] arrayOfInt = this.c;
      if (i >= arrayOfInt.length) {
        break;
      }
      paramc.c(arrayOfInt[i]);
    }
  }
  
  public int b()
  {
    return this.b & 0xF;
  }
  
  void b(int paramInt)
  {
    int[] arrayOfInt = this.c;
    if (arrayOfInt[paramInt] != 65535)
    {
      arrayOfInt[paramInt] += 1;
      return;
    }
    throw new IllegalStateException("DNS section count cannot be incremented");
  }
  
  int c()
  {
    return this.b;
  }
  
  public int c(int paramInt)
  {
    return this.c[paramInt];
  }
  
  public Object clone()
  {
    d locald = new d();
    locald.a = this.a;
    locald.b = this.b;
    int[] arrayOfInt = this.c;
    System.arraycopy(arrayOfInt, 0, locald.c, 0, arrayOfInt.length);
    return locald;
  }
  
  String d(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(";; ->>HEADER<<- ");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(", id: ");
    localStringBuilder.append(a());
    localStringBuffer.append(localStringBuilder.toString());
    localStringBuffer.append("\n");
    localStringBuffer.append("; ");
    for (paramInt = 0; paramInt < 4; paramInt++)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(n.a(paramInt));
      localStringBuilder.append(": ");
      localStringBuilder.append(c(paramInt));
      localStringBuilder.append(" ");
      localStringBuffer.append(localStringBuilder.toString());
    }
    return localStringBuffer.toString();
  }
  
  public String toString()
  {
    return d(b());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */