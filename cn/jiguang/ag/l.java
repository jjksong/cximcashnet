package cn.jiguang.ag;

import android.text.TextUtils;
import android.util.Pair;
import cn.jiguang.ae.c;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class l
{
  private static volatile l a;
  private static final Object b = new Object();
  private long c = 86400000L;
  private long d = 1800000L;
  private final Map<String, Pair<LinkedHashSet<cn.jiguang.af.g>, Long>> e = new HashMap();
  
  private Pair<LinkedHashSet<cn.jiguang.af.g>, Boolean> a(String paramString, long paramLong1, long paramLong2)
  {
    Object localObject2 = (Pair)this.e.get(paramString);
    boolean bool = false;
    int i;
    if ((localObject2 != null) && (((Pair)localObject2).first != null) && (((LinkedHashSet)((Pair)localObject2).first).size() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    Object localObject1;
    if (i != 0) {
      localObject1 = c.a(null, cn.jiguang.ae.b.e(paramString));
    } else {
      localObject1 = ((Pair)localObject2).second;
    }
    long l1 = ((Long)localObject1).longValue();
    long l2 = System.currentTimeMillis();
    if (l2 > l1 + paramLong1) {
      return null;
    }
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    if (i != 0)
    {
      localObject2 = (String)c.a(null, cn.jiguang.ae.b.d(paramString));
      localObject1 = localLinkedHashSet;
      if (!TextUtils.isEmpty((CharSequence)localObject2))
      {
        localObject2 = ((String)localObject2).split(",");
        int j = localObject2.length;
        for (i = 0; i < j; i++)
        {
          localObject1 = cn.jiguang.af.g.a(localObject2[i]);
          if ((localObject1 != null) && (((cn.jiguang.af.g)localObject1).a())) {
            localLinkedHashSet.add(localObject1);
          }
        }
        this.e.put(paramString, new Pair(localLinkedHashSet, Long.valueOf(System.currentTimeMillis())));
        localObject1 = localLinkedHashSet;
      }
    }
    else
    {
      localObject1 = (LinkedHashSet)((Pair)localObject2).first;
    }
    if (!((LinkedHashSet)localObject1).isEmpty())
    {
      if (l2 > l1 + paramLong2) {
        bool = true;
      }
      return new Pair(localObject1, Boolean.valueOf(bool));
    }
    return null;
  }
  
  public static l a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          l locall = new cn/jiguang/ag/l;
          locall.<init>();
          a = locall;
        }
      }
    }
    return a;
  }
  
  public static LinkedHashSet<cn.jiguang.af.g> a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    try
    {
      byte[] arrayOfByte = b(paramString);
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("srv host:");
      ((StringBuilder)localObject1).append(paramString);
      cn.jiguang.ai.a.c("SRVLoader_xxx", ((StringBuilder)localObject1).toString());
      LinkedHashSet localLinkedHashSet = new LinkedHashSet();
      try
      {
        paramString = k.b().a();
        if ((paramString != null) && (paramString.length != 0))
        {
          Object localObject2 = new java/util/LinkedHashSet;
          ((LinkedHashSet)localObject2).<init>();
          Object localObject3 = cn.jiguang.aj.a.a();
          int j = paramString.length;
          for (int i = 0; i < j; i++)
          {
            localObject1 = ((cn.jiguang.aj.a)localObject3).a(null, paramString[i], 3000L, false);
            if (localObject1 != null) {
              ((Set)localObject2).add(localObject1);
            }
          }
          localObject2 = ((Set)localObject2).iterator();
          for (;;)
          {
            if (!((Iterator)localObject2).hasNext()) {
              break label489;
            }
            paramString = (InetAddress)((Iterator)localObject2).next();
            try
            {
              localObject1 = new java/net/InetSocketAddress;
              ((InetSocketAddress)localObject1).<init>(paramString, 53);
              localObject3 = o.a(null, (SocketAddress)localObject1, arrayOfByte, System.currentTimeMillis() + 1000L);
              localObject1 = new cn/jiguang/ag/e;
              ((e)localObject1).<init>((byte[])localObject3);
              localObject3 = ((e)localObject1).a();
              if (localObject3 == null) {
                break label489;
              }
              for (paramString : ((e)localObject1).b(1)) {
                if ((paramString.d() == ((i)localObject3).g()) && (paramString.c() == ((i)localObject3).e()) && (paramString.b().equals(((i)localObject3).d())))
                {
                  Iterator localIterator = paramString.a();
                  while (localIterator.hasNext())
                  {
                    m localm = (m)localIterator.next();
                    if (localm.j() > 0)
                    {
                      localObject1 = localm.k().toString();
                      if (!TextUtils.isEmpty((CharSequence)localObject1))
                      {
                        paramString = (String)localObject1;
                        if (((String)localObject1).endsWith(".")) {
                          paramString = ((String)localObject1).substring(0, ((String)localObject1).length() - 1);
                        }
                        localObject1 = new cn/jiguang/af/g;
                        ((cn.jiguang.af.g)localObject1).<init>(paramString, localm.j());
                        if (((cn.jiguang.af.g)localObject1).a()) {
                          localLinkedHashSet.add(localObject1);
                        }
                      }
                    }
                  }
                }
              }
            }
            catch (IOException localIOException)
            {
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              ((StringBuilder)localObject3).append("tcp send to ");
              ((StringBuilder)localObject3).append(paramString.getHostAddress());
              ((StringBuilder)localObject3).append(" err:");
              ((StringBuilder)localObject3).append(localIOException);
              cn.jiguang.ai.a.g("SRVLoader_xxx", ((StringBuilder)localObject3).toString());
            }
          }
        }
        return localLinkedHashSet;
      }
      catch (Throwable localThrowable)
      {
        paramString = new StringBuilder();
        paramString.append("Get default ports error with Exception:");
        paramString.append(localThrowable);
        cn.jiguang.ai.a.h("SRVLoader_xxx", paramString.toString());
      }
      label489:
      return localLinkedHashSet;
    }
    catch (IOException paramString)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("can't srv, create query:");
      localStringBuilder.append(paramString);
      cn.jiguang.ai.a.g("SRVLoader_xxx", localStringBuilder.toString());
    }
    return null;
  }
  
  private void a(String paramString, Pair<LinkedHashSet<cn.jiguang.af.g>, Long> paramPair)
  {
    if ((paramPair.first != null) && (((LinkedHashSet)paramPair.first).size() > 0) && (paramPair.second != null))
    {
      this.e.put(paramString, paramPair);
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = ((LinkedHashSet)paramPair.first).iterator();
      while (localIterator.hasNext())
      {
        localStringBuilder.append(((cn.jiguang.af.g)localIterator.next()).toString());
        localStringBuilder.append(",");
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      c.a(null, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.d(paramString).a(localStringBuilder.toString()), cn.jiguang.ae.b.e(paramString).a(paramPair.second) });
    }
  }
  
  private static byte[] b(String paramString)
  {
    return e.a(i.a(g.a(g.a(paramString), g.a), 33, 1)).c(65535);
  }
  
  public LinkedHashSet<cn.jiguang.af.g> a(String paramString, long paramLong)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    Pair localPair = a(paramString, this.c, this.d);
    Object localObject1;
    Object localObject2;
    if (localPair != null)
    {
      if ((localPair.first != null) && (((LinkedHashSet)localPair.first).size() > 0)) {
        localObject1 = (LinkedHashSet)localPair.first;
      } else {
        localObject1 = null;
      }
      localObject2 = localObject1;
      if (!((Boolean)localPair.second).booleanValue()) {
        return (LinkedHashSet<cn.jiguang.af.g>)localObject1;
      }
    }
    else
    {
      localObject2 = null;
    }
    paramString = new FutureTask(new a(paramString, this));
    cn.jiguang.sdk.impl.b.a(paramString, new int[0]);
    if (paramLong == 0L) {}
    for (paramString = new StringBuilder();; paramString = new StringBuilder())
    {
      paramString.append("use cache=");
      paramString.append(localObject2);
      cn.jiguang.ai.a.c("SRVLoader_xxx", paramString.toString());
      return (LinkedHashSet<cn.jiguang.af.g>)localObject2;
      try
      {
        paramString = (LinkedHashSet)paramString.get(paramLong, TimeUnit.MILLISECONDS);
      }
      catch (Throwable paramString)
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("run futureTask e:");
        ((StringBuilder)localObject1).append(paramString);
        cn.jiguang.ai.a.g("SRVLoader_xxx", ((StringBuilder)localObject1).toString());
        paramString = null;
      }
      if ((paramString != null) && (paramString.size() > 0))
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("use resolved result=");
        ((StringBuilder)localObject1).append(paramString);
        cn.jiguang.ai.a.c("SRVLoader_xxx", ((StringBuilder)localObject1).toString());
        return paramString;
      }
    }
  }
  
  static class a
    implements Callable<LinkedHashSet<cn.jiguang.af.g>>
  {
    private String a;
    private l b;
    
    a(String paramString, l paraml)
    {
      this.a = paramString;
      this.b = paraml;
    }
    
    public LinkedHashSet<cn.jiguang.af.g> a()
    {
      LinkedHashSet localLinkedHashSet = l.a(this.a);
      if ((localLinkedHashSet != null) && (localLinkedHashSet.size() > 0)) {
        l.a(this.b, this.a, new Pair(localLinkedHashSet, Long.valueOf(System.currentTimeMillis())));
      }
      return localLinkedHashSet;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */