package cn.jiguang.ag;

import java.util.HashMap;

class f
{
  private static Integer[] a = new Integer[64];
  private HashMap b;
  private HashMap c;
  private String d;
  private int e;
  private String f;
  private int g;
  private boolean h;
  
  static
  {
    for (int i = 0;; i++)
    {
      Integer[] arrayOfInteger = a;
      if (i >= arrayOfInteger.length) {
        break;
      }
      arrayOfInteger[i] = Integer.valueOf(i);
    }
  }
  
  public f(String paramString, int paramInt)
  {
    this.d = paramString;
    this.e = paramInt;
    this.b = new HashMap();
    this.c = new HashMap();
    this.g = Integer.MAX_VALUE;
  }
  
  private String a(String paramString)
  {
    int i = this.e;
    if (i == 2) {
      return paramString.toUpperCase();
    }
    String str = paramString;
    if (i == 3) {
      str = paramString.toLowerCase();
    }
    return str;
  }
  
  public static Integer b(int paramInt)
  {
    if (paramInt >= 0)
    {
      Integer[] arrayOfInteger = a;
      if (paramInt < arrayOfInteger.length) {
        return arrayOfInteger[paramInt];
      }
    }
    return Integer.valueOf(paramInt);
  }
  
  public void a(int paramInt)
  {
    this.g = paramInt;
  }
  
  public void a(int paramInt, String paramString)
  {
    c(paramInt);
    Integer localInteger = b(paramInt);
    paramString = a(paramString);
    this.b.put(paramString, localInteger);
    this.c.put(localInteger, paramString);
  }
  
  public void a(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public void c(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt <= this.g)) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.d);
    localStringBuilder.append(" ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append("is out of range");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public String d(int paramInt)
  {
    c(paramInt);
    Object localObject = (String)this.c.get(b(paramInt));
    if (localObject != null) {
      return (String)localObject;
    }
    String str = Integer.toString(paramInt);
    localObject = str;
    if (this.f != null)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(this.f);
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */