package cn.jiguang.ag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class e
  implements Cloneable
{
  private static i[] d = new i[0];
  private static h[] e = new h[0];
  private d a;
  private List[] b = new List[4];
  private int c;
  
  public e()
  {
    this(new d());
  }
  
  e(b paramb)
  {
    this(new d(paramb));
    int i = 0;
    while (i < 4) {
      try
      {
        int k = this.a.c(i);
        Object localObject;
        if (k > 0)
        {
          List[] arrayOfList = this.b;
          localObject = new java/util/ArrayList;
          ((ArrayList)localObject).<init>(k);
          arrayOfList[i] = localObject;
        }
        for (int j = 0; j < k; j++)
        {
          localObject = i.a(paramb, i);
          this.b[i].add(localObject);
        }
        i++;
      }
      catch (IOException paramb)
      {
        throw paramb;
      }
    }
    this.c = paramb.a();
  }
  
  private e(d paramd)
  {
    this.a = paramd;
  }
  
  public e(byte[] paramArrayOfByte)
  {
    this(new b(paramArrayOfByte));
  }
  
  private int a(c paramc, int paramInt1, a parama, int paramInt2)
  {
    int i2 = this.b[paramInt1].size();
    int k = paramc.a();
    int i = 0;
    Object localObject = null;
    int i1 = 0;
    int j = 0;
    while (i < i2)
    {
      i locali = (i)this.b[paramInt1].get(i);
      if (paramInt1 == 3)
      {
        i1++;
      }
      else
      {
        int m = k;
        int n = j;
        if (localObject != null)
        {
          m = k;
          n = j;
          if (!a(locali, (i)localObject))
          {
            m = paramc.a();
            n = i;
          }
        }
        locali.a(paramc, paramInt1, parama);
        if (paramc.a() > paramInt2)
        {
          paramc.a(m);
          return i2 - n + i1;
        }
        localObject = locali;
        j = n;
        k = m;
      }
      i++;
    }
    return i1;
  }
  
  public static e a(i parami)
  {
    e locale = new e();
    locale.a(parami, 0);
    return locale;
  }
  
  private boolean a(c paramc, int paramInt)
  {
    int i = 0;
    if (paramInt < 12) {
      return false;
    }
    paramc.a();
    this.a.a(paramc);
    a locala = new a();
    this.a.c();
    while (i < 4)
    {
      if (this.b[i] != null) {
        a(paramc, i, locala, paramInt);
      }
      i++;
    }
    return true;
  }
  
  private static boolean a(i parami1, i parami2)
  {
    boolean bool;
    if ((parami1.f() == parami2.f()) && (parami1.g() == parami2.g()) && (parami1.d().equals(parami2.d()))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public i a()
  {
    List localList = this.b[0];
    if ((localList != null) && (localList.size() != 0)) {
      return (i)localList.get(0);
    }
    return null;
  }
  
  public void a(i parami, int paramInt)
  {
    List[] arrayOfList = this.b;
    if (arrayOfList[paramInt] == null) {
      arrayOfList[paramInt] = new LinkedList();
    }
    this.a.b(paramInt);
    this.b[paramInt].add(parami);
  }
  
  public i[] a(int paramInt)
  {
    Object localObject = this.b;
    if (localObject[paramInt] == null) {
      return d;
    }
    localObject = localObject[paramInt];
    return (i[])((List)localObject).toArray(new i[((List)localObject).size()]);
  }
  
  public h[] b(int paramInt)
  {
    if (this.b[paramInt] == null) {
      return e;
    }
    LinkedList localLinkedList = new LinkedList();
    i[] arrayOfi = a(paramInt);
    HashSet localHashSet = new HashSet();
    for (paramInt = 0; paramInt < arrayOfi.length; paramInt++)
    {
      g localg = arrayOfi[paramInt].d();
      boolean bool = localHashSet.contains(localg);
      int k = 1;
      int i = k;
      if (bool) {
        for (int j = localLinkedList.size() - 1;; j--)
        {
          i = k;
          if (j < 0) {
            break;
          }
          h localh = (h)localLinkedList.get(j);
          if ((localh.c() == arrayOfi[paramInt].f()) && (localh.d() == arrayOfi[paramInt].g()) && (localh.b().equals(localg)))
          {
            localh.a(arrayOfi[paramInt]);
            i = 0;
            break;
          }
        }
      }
      if (i != 0)
      {
        localLinkedList.add(new h(arrayOfi[paramInt]));
        localHashSet.add(localg);
      }
    }
    return (h[])localLinkedList.toArray(new h[localLinkedList.size()]);
  }
  
  public byte[] c(int paramInt)
  {
    c localc = new c();
    a(localc, paramInt);
    this.c = localc.a();
    return localc.b();
  }
  
  public Object clone()
  {
    e locale = new e();
    for (int i = 0;; i++)
    {
      List[] arrayOfList = this.b;
      if (i >= arrayOfList.length) {
        break;
      }
      if (arrayOfList[i] != null) {
        locale.b[i] = new LinkedList(arrayOfList[i]);
      }
    }
    locale.a = ((d)this.a.clone());
    locale.c = this.c;
    return locale;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */