package cn.jiguang.ag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class h
  implements Serializable
{
  private List a = new ArrayList(1);
  private short b = 0;
  private short c = 0;
  
  public h() {}
  
  public h(i parami)
  {
    this();
    b(parami);
  }
  
  private String a(Iterator paramIterator)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    while (paramIterator.hasNext())
    {
      i locali = (i)paramIterator.next();
      localStringBuffer.append("[");
      localStringBuffer.append(locali.c());
      localStringBuffer.append("]");
      if (paramIterator.hasNext()) {
        localStringBuffer.append(" ");
      }
    }
    return localStringBuffer.toString();
  }
  
  private Iterator a(boolean paramBoolean1, boolean paramBoolean2)
  {
    try
    {
      int k = this.a.size();
      int j;
      if (paramBoolean1) {
        j = k - this.b;
      } else {
        j = this.b;
      }
      if (j == 0)
      {
        localObject1 = Collections.EMPTY_LIST.iterator();
        return (Iterator)localObject1;
      }
      int i;
      if (paramBoolean1)
      {
        if (!paramBoolean2)
        {
          i = 0;
        }
        else
        {
          if (this.c >= j) {
            this.c = 0;
          }
          i = this.c;
          this.c = ((short)(i + 1));
        }
      }
      else {
        i = k - this.b;
      }
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>(j);
      if (paramBoolean1)
      {
        localArrayList.addAll(this.a.subList(i, j));
        if (i == 0) {}
      }
      else
      {
        for (localObject1 = this.a.subList(0, i);; localObject1 = this.a.subList(i, k))
        {
          localArrayList.addAll((Collection)localObject1);
          break;
        }
      }
      Object localObject1 = localArrayList.iterator();
      return (Iterator)localObject1;
    }
    finally {}
  }
  
  private void b(i parami)
  {
    if (this.b == 0)
    {
      this.a.add(parami);
    }
    else
    {
      List localList = this.a;
      localList.add(localList.size() - this.b, parami);
    }
  }
  
  public Iterator a()
  {
    try
    {
      Iterator localIterator = a(true, true);
      return localIterator;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void a(i parami)
  {
    try
    {
      if (this.a.size() == 0)
      {
        b(parami);
        return;
      }
      i locali2 = f();
      if (parami.a(locali2))
      {
        i locali1 = parami;
        if (parami.h() != locali2.h()) {
          if (parami.h() > locali2.h())
          {
            locali1 = parami.i();
            locali1.a(locali2.h());
          }
          else
          {
            for (int i = 0;; i++)
            {
              locali1 = parami;
              if (i >= this.a.size()) {
                break;
              }
              locali1 = ((i)this.a.get(i)).i();
              locali1.a(parami.h());
              this.a.set(i, locali1);
            }
          }
        }
        if (!this.a.contains(locali1)) {
          b(locali1);
        }
        return;
      }
      parami = new java/lang/IllegalArgumentException;
      parami.<init>("record does not match rrset");
      throw parami;
    }
    finally {}
  }
  
  public g b()
  {
    return f().d();
  }
  
  public int c()
  {
    return f().f();
  }
  
  public int d()
  {
    return f().g();
  }
  
  public long e()
  {
    try
    {
      long l = f().h();
      return l;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public i f()
  {
    try
    {
      if (this.a.size() != 0)
      {
        localObject1 = (i)this.a.get(0);
        return (i)localObject1;
      }
      Object localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("rrset is empty");
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public String toString()
  {
    if (this.a.size() == 0) {
      return "{empty}";
    }
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("{ ");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b());
    localStringBuilder.append(" ");
    localStringBuffer.append(localStringBuilder.toString());
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(e());
    localStringBuilder.append(" ");
    localStringBuffer.append(localStringBuilder.toString());
    localStringBuffer.append(a(a(true, false)));
    if (this.b > 0)
    {
      localStringBuffer.append(" sigs: ");
      localStringBuffer.append(a(a(false, false)));
    }
    localStringBuffer.append(" }");
    return localStringBuffer.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ag/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */