package cn.jiguang.u;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import cn.jiguang.f.d;
import java.util.Iterator;
import java.util.List;

@SuppressLint({"MissingPermission"})
class b
{
  private Context a;
  private final TelephonyManager b;
  private cn.jiguang.v.a c;
  
  b(Context paramContext)
  {
    this.a = paramContext;
    this.b = ((TelephonyManager)paramContext.getSystemService("phone"));
  }
  
  cn.jiguang.v.a a()
  {
    return this.c;
  }
  
  void b()
  {
    if (this.b == null)
    {
      cn.jiguang.ai.a.g("JLocationCell", "get telephonyManager failed");
      return;
    }
    this.c = new cn.jiguang.v.a();
    this.c.a = d.h(this.a);
    Object localObject1 = this.b.getNetworkOperator();
    if (((String)localObject1).length() > 3)
    {
      this.c.b = Integer.parseInt(((String)localObject1).substring(0, 3));
      this.c.c = Integer.parseInt(((String)localObject1).substring(3));
    }
    this.c.i = this.b.getNetworkOperatorName();
    this.c.g = d.a(this.b.getNetworkType());
    this.c.h = d.a(this.a, this.b.getNetworkType());
    if (Build.VERSION.SDK_INT > 17)
    {
      localObject1 = this.b.getAllCellInfo();
      if ((localObject1 != null) && (((List)localObject1).size() > 0))
      {
        localObject1 = ((List)localObject1).iterator();
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    if (!((Iterator)localObject1).hasNext()) {
                      break;
                    }
                    localObject2 = (CellInfo)((Iterator)localObject1).next();
                  } while (localObject2 == null);
                  if (!(localObject2 instanceof CellInfoLte)) {
                    break;
                  }
                  localObject3 = (CellInfoLte)localObject2;
                  localObject2 = ((CellInfoLte)localObject3).getCellSignalStrength();
                  localObject3 = ((CellInfoLte)localObject3).getCellIdentity();
                  this.c.f = ((CellSignalStrengthLte)localObject2).getDbm();
                  this.c.e = ((CellIdentityLte)localObject3).getCi();
                  this.c.d = ((CellIdentityLte)localObject3).getTac();
                } while (this.c.e >= 268435455);
                return;
                if (!(localObject2 instanceof CellInfoGsm)) {
                  break;
                }
                localObject3 = (CellInfoGsm)localObject2;
                localObject2 = ((CellInfoGsm)localObject3).getCellSignalStrength();
                localObject3 = ((CellInfoGsm)localObject3).getCellIdentity();
                this.c.f = ((CellSignalStrengthGsm)localObject2).getDbm();
                this.c.e = ((CellIdentityGsm)localObject3).getCid();
                this.c.d = ((CellIdentityGsm)localObject3).getLac();
              } while (this.c.e >= 65535);
              return;
              if (!(localObject2 instanceof CellInfoCdma)) {
                break;
              }
              localObject3 = (CellInfoCdma)localObject2;
              localObject2 = ((CellInfoCdma)localObject3).getCellSignalStrength();
              localObject3 = ((CellInfoCdma)localObject3).getCellIdentity();
              this.c.f = ((CellSignalStrengthCdma)localObject2).getDbm();
              this.c.e = ((CellIdentityCdma)localObject3).getBasestationId();
              this.c.d = ((CellIdentityCdma)localObject3).getNetworkId();
            } while (this.c.e >= 65535);
            return;
          } while (!(localObject2 instanceof CellInfoWcdma));
          Object localObject3 = (CellInfoWcdma)localObject2;
          Object localObject2 = ((CellInfoWcdma)localObject3).getCellSignalStrength();
          localObject3 = ((CellInfoWcdma)localObject3).getCellIdentity();
          this.c.f = ((CellSignalStrengthWcdma)localObject2).getDbm();
          this.c.e = ((CellIdentityWcdma)localObject3).getCid();
          this.c.d = ((CellIdentityWcdma)localObject3).getLac();
        } while (this.c.e >= 268435455);
      }
      return;
    }
    this.b.listen(new a(null), 256);
  }
  
  private class a
    extends PhoneStateListener
  {
    private a() {}
    
    public void onSignalStrengthsChanged(SignalStrength paramSignalStrength)
    {
      try
      {
        super.onSignalStrengthsChanged(paramSignalStrength);
        b.a(b.this).f = paramSignalStrength.getGsmSignalStrength();
        paramSignalStrength = b.b(b.this).getCellLocation();
        if (paramSignalStrength == null) {
          return;
        }
        Object localObject;
        if ((paramSignalStrength instanceof GsmCellLocation))
        {
          localObject = (GsmCellLocation)paramSignalStrength;
          b.a(b.this).e = ((GsmCellLocation)localObject).getCid();
          paramSignalStrength = b.a(b.this);
        }
        for (int i = ((GsmCellLocation)localObject).getLac();; i = ((CdmaCellLocation)localObject).getNetworkId())
        {
          paramSignalStrength.d = i;
          break;
          if (!(paramSignalStrength instanceof CdmaCellLocation)) {
            break;
          }
          localObject = (CdmaCellLocation)paramSignalStrength;
          b.a(b.this).e = ((CdmaCellLocation)localObject).getBaseStationId();
          paramSignalStrength = b.a(b.this);
        }
        b.b(b.this).listen(this, 0);
        return;
      }
      catch (Throwable paramSignalStrength)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/u/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */