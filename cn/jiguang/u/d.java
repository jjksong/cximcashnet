package cn.jiguang.u;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import cn.jiguang.ai.a;
import cn.jiguang.v.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

class d
{
  private Context a;
  
  d(Context paramContext)
  {
    this.a = paramContext;
  }
  
  List<c> a()
  {
    Object localObject2 = (WifiManager)this.a.getApplicationContext().getSystemService("wifi");
    if (localObject2 == null)
    {
      a.g("JLocationWifi", "get wifiManager failed");
      return null;
    }
    if (!((WifiManager)localObject2).isWifiEnabled()) {
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    Object localObject3 = ((WifiManager)localObject2).getConnectionInfo();
    Object localObject1 = new c();
    ((c)localObject1).a = cn.jiguang.f.d.h(this.a);
    ((c)localObject1).b = cn.jiguang.f.d.e(((WifiInfo)localObject3).getSSID());
    ((c)localObject1).c = "connect";
    ((c)localObject1).d = ((WifiInfo)localObject3).getRssi();
    ((c)localObject1).e = ((WifiInfo)localObject3).getBSSID();
    localObject3 = new StringBuilder();
    ((StringBuilder)localObject3).append("connectingWifi:");
    ((StringBuilder)localObject3).append(((c)localObject1).toString());
    a.c("JLocationWifi", ((StringBuilder)localObject3).toString());
    localArrayList.add(localObject1);
    if (Build.VERSION.SDK_INT < 23) {}
    while ((cn.jiguang.f.d.a(this.a, "android.permission.ACCESS_COARSE_LOCATION")) || (cn.jiguang.f.d.a(this.a, "android.permission.ACCESS_FINE_LOCATION")))
    {
      localObject2 = ((WifiManager)localObject2).getScanResults();
      break;
    }
    for (localObject1 = "scan wifi list failed because has no Manifest.permission.LOCATION";; localObject1 = "scan wifi list failed")
    {
      a.g("JLocationWifi", (String)localObject1);
      return localArrayList;
      if ((localObject2 != null) && (((List)localObject2).size() != 0))
      {
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append("scan wifi list success:");
        ((StringBuilder)localObject3).append(localObject2);
        a.c("JLocationWifi", ((StringBuilder)localObject3).toString());
        Object localObject4 = new ArrayList((Collection)localObject2);
        Iterator localIterator2 = ((List)localObject4).iterator();
        while (localIterator2.hasNext())
        {
          localObject3 = (ScanResult)localIterator2.next();
          if (((((c)localObject1).b.equals(cn.jiguang.f.d.e(((ScanResult)localObject3).SSID))) && (((c)localObject1).e.equals(((ScanResult)localObject3).BSSID))) || (((ScanResult)localObject3).level < 65336))
          {
            ((List)localObject2).remove(localObject3);
          }
          else
          {
            Iterator localIterator1 = ((List)localObject4).iterator();
            while (localIterator1.hasNext())
            {
              ScanResult localScanResult = (ScanResult)localIterator1.next();
              if ((localScanResult != localObject3) && (((ScanResult)localObject3).SSID.equals(localScanResult.SSID)) && (((ScanResult)localObject3).BSSID.equals(localScanResult.BSSID))) {
                ((List)localObject2).remove(localObject3);
              }
            }
          }
        }
        ((List)localObject4).clear();
        Collections.sort((List)localObject2, new Comparator()
        {
          public int a(ScanResult paramAnonymousScanResult1, ScanResult paramAnonymousScanResult2)
          {
            return paramAnonymousScanResult2.level - paramAnonymousScanResult1.level;
          }
        });
        for (int i = 0; (i < ((List)localObject2).size()) && (i != 9); i++)
        {
          localObject3 = (ScanResult)((List)localObject2).get(i);
          localObject1 = cn.jiguang.f.d.e(((ScanResult)localObject3).SSID);
          localObject4 = new c();
          ((c)localObject4).a = cn.jiguang.f.d.h(this.a);
          ((c)localObject4).b = ((String)localObject1);
          ((c)localObject4).c = null;
          if (i == 0) {
            ((c)localObject4).c = "strongest";
          }
          ((c)localObject4).d = ((ScanResult)localObject3).level;
          ((c)localObject4).e = ((ScanResult)localObject3).BSSID;
          localArrayList.add(localObject4);
        }
        return localArrayList;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/u/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */