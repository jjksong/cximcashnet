package cn.jiguang.u;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import cn.jiguang.f.d;
import java.util.List;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a b;
  private Context a;
  
  private Bundle a(long paramLong, double paramDouble1, double paramDouble2)
  {
    Bundle localBundle = new Bundle();
    localBundle.putLong("time", paramLong);
    localBundle.putDouble("lot", paramDouble1);
    localBundle.putDouble("lat", paramDouble2);
    return localBundle;
  }
  
  private JSONObject a(List<cn.jiguang.v.c> paramList, cn.jiguang.v.a parama, cn.jiguang.v.b paramb)
  {
    JSONObject localJSONObject = new JSONObject();
    int j = 0;
    int i = j;
    Object localObject1;
    if (paramList != null)
    {
      i = j;
      if (!paramList.isEmpty())
      {
        localObject1 = new JSONArray();
        j = 0;
        i = 0;
        while (j < paramList.size())
        {
          try
          {
            localObject2 = new org/json/JSONObject;
            ((JSONObject)localObject2).<init>();
            cn.jiguang.v.c localc = (cn.jiguang.v.c)paramList.get(j);
            ((JSONObject)localObject2).put("itime", localc.a);
            if (localc.c != null) {
              ((JSONObject)localObject2).put("tag", localc.c);
            }
            ((JSONObject)localObject2).put("ssid", localc.b);
            ((JSONObject)localObject2).put("mac_address", localc.e);
            ((JSONObject)localObject2).put("signal_strength", localc.d);
            ((JSONObject)localObject2).put("age", 0);
            ((JSONArray)localObject1).put(localObject2);
            localJSONObject.put("wifi", localObject1);
            i = 1;
          }
          catch (JSONException localJSONException)
          {
            Object localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append("package wifi json exception:");
            ((StringBuilder)localObject2).append(localJSONException.getMessage());
            cn.jiguang.ai.a.g("JLocation", ((StringBuilder)localObject2).toString());
          }
          j++;
        }
      }
    }
    j = i;
    if (parama != null) {
      try
      {
        paramList = new org/json/JSONArray;
        paramList.<init>();
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        ((JSONObject)localObject1).put("itime", parama.a);
        ((JSONObject)localObject1).put("radio_type", parama.g);
        ((JSONObject)localObject1).put("generation", parama.h);
        ((JSONObject)localObject1).put("carrier", parama.i);
        ((JSONObject)localObject1).put("mobile_country_code", parama.b);
        ((JSONObject)localObject1).put("mobile_network_code", parama.c);
        ((JSONObject)localObject1).put("signal_strength", parama.f);
        ((JSONObject)localObject1).put("cell_id", parama.e);
        ((JSONObject)localObject1).put("location_area_code", parama.d);
        paramList.put(localObject1);
        localJSONObject.put("cell", paramList);
        j = 1;
      }
      catch (JSONException parama)
      {
        paramList = new StringBuilder();
        paramList.append("package cell json exception:");
        paramList.append(parama.getMessage());
        cn.jiguang.ai.a.g("JLocation", paramList.toString());
        j = i;
      }
    }
    i = j;
    if (paramb != null) {
      try
      {
        parama = new org/json/JSONArray;
        parama.<init>();
        paramList = new org/json/JSONObject;
        paramList.<init>();
        paramList.put("itime", paramb.a);
        paramList.put("tag", paramb.b);
        paramList.put("lat", paramb.c);
        paramList.put("lng", paramb.d);
        paramList.put("alt", paramb.e);
        paramList.put("bear", paramb.f);
        paramList.put("acc", paramb.g);
        parama.put(paramList);
        localJSONObject.put("gps", parama);
        i = 1;
      }
      catch (JSONException paramList)
      {
        parama = new StringBuilder();
        parama.append("package gps json exception:");
        parama.append(paramList.getMessage());
        cn.jiguang.ai.a.g("JLocation", parama.toString());
        i = j;
      }
    }
    if (i != 0) {
      try
      {
        d.a(this.a, localJSONObject, "loc_info");
        localJSONObject.put("local_dns", d.c());
        localJSONObject.put("network_type", d.k(this.a));
      }
      catch (JSONException parama)
      {
        paramList = new StringBuilder();
        paramList.append("package json exception:");
        paramList.append(parama.getMessage());
        cn.jiguang.ai.a.g("JLocation", paramList.toString());
      }
    }
    return localJSONObject;
  }
  
  private JSONObject a(JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("currentJson:");
    ((StringBuilder)localObject1).append(paramJSONObject1);
    ((StringBuilder)localObject1).append(",cacheJson:");
    ((StringBuilder)localObject1).append(paramJSONObject2);
    cn.jiguang.ai.a.c("JLocation", ((StringBuilder)localObject1).toString());
    if (paramJSONObject1 == null) {
      return paramJSONObject2;
    }
    if (paramJSONObject2 == null)
    {
      try
      {
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        try
        {
          paramJSONObject2 = new org/json/JSONArray;
          paramJSONObject2.<init>();
          paramJSONObject2.put(paramJSONObject1);
          ((JSONObject)localObject1).put("content", paramJSONObject2);
          paramJSONObject1 = (JSONObject)localObject1;
        }
        catch (JSONException paramJSONObject2)
        {
          paramJSONObject1 = (JSONObject)localObject1;
        }
        localObject1 = new StringBuilder();
      }
      catch (JSONException paramJSONObject1)
      {
        localObject1 = paramJSONObject2;
        paramJSONObject2 = paramJSONObject1;
        paramJSONObject1 = (JSONObject)localObject1;
      }
      ((StringBuilder)localObject1).append("mergeJson exception:");
      ((StringBuilder)localObject1).append(paramJSONObject2.getMessage());
      cn.jiguang.ai.a.g("JLocation", ((StringBuilder)localObject1).toString());
      return paramJSONObject1;
    }
    Object localObject2;
    try
    {
      localObject1 = paramJSONObject2.getJSONArray("content");
    }
    catch (JSONException localJSONException)
    {
      localObject2 = new JSONArray();
    }
    for (int i = 0; i < ((JSONArray)localObject2).length(); i++)
    {
      Object localObject3 = ((JSONArray)localObject2).optJSONObject(i);
      JSONArray localJSONArray1 = ((JSONObject)localObject3).optJSONArray("wifi");
      JSONArray localJSONArray2 = ((JSONObject)localObject3).optJSONArray("cell");
      JSONArray localJSONArray4 = ((JSONObject)localObject3).optJSONArray("gps");
      JSONArray localJSONArray5 = paramJSONObject1.optJSONArray("wifi");
      localObject3 = paramJSONObject1.optJSONArray("cell");
      JSONArray localJSONArray3 = paramJSONObject1.optJSONArray("gps");
      if ((localJSONArray1 != null) && (localJSONArray1.equals(localJSONArray5))) {
        paramJSONObject1.remove("wifi");
      }
      if ((localJSONArray2 != null) && (localJSONArray2.equals(localObject3))) {
        paramJSONObject1.remove("cell");
      }
      if ((localJSONArray4 != null) && (localJSONArray4.equals(localJSONArray3))) {
        paramJSONObject1.remove("gps");
      }
    }
    if (paramJSONObject1.length() != 0) {
      try
      {
        paramJSONObject1.put("network_type", d.k(this.a));
        paramJSONObject1.put("local_dns", d.c());
        d.a(this.a, paramJSONObject1, "loc_info");
        ((JSONArray)localObject2).put(paramJSONObject1);
      }
      catch (JSONException paramJSONObject1)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("package json exception:");
        ((StringBuilder)localObject2).append(paramJSONObject1.getMessage());
        cn.jiguang.ai.a.g("JLocation", ((StringBuilder)localObject2).toString());
      }
    }
    return paramJSONObject2;
  }
  
  public static a d()
  {
    if (b == null) {
      try
      {
        a locala = new cn/jiguang/u/a;
        locala.<init>();
        b = locala;
      }
      finally {}
    }
    return b;
  }
  
  protected void a(String paramString, JSONObject paramJSONObject)
  {
    int i = paramJSONObject.optInt("cmd");
    paramString = paramJSONObject.optJSONObject("content");
    long l;
    if (i == 5)
    {
      boolean bool = paramString.optBoolean("disable", true) ^ true;
      cn.jiguang.f.b.a(this.a, "JLocation", bool);
      if (bool)
      {
        l = paramString.optLong("frequency", 0L);
        cn.jiguang.f.b.b(this.a, "JLocation", l * 1000L);
      }
    }
    else if (i == 45)
    {
      l = paramString.optLong("interval", 0L);
      cn.jiguang.f.b.a(this.a, "JLocation", l * 1000L);
    }
  }
  
  protected boolean a()
  {
    return cn.jiguang.f.b.j(this.a, "JLocation");
  }
  
  protected boolean a(Context paramContext, String paramString)
  {
    return cn.jiguang.f.b.a(paramContext, paramString);
  }
  
  protected boolean b()
  {
    return cn.jiguang.f.b.i(this.a, "JLocation");
  }
  
  /* Error */
  protected void c(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: invokestatic 324	java/util/concurrent/Executors:newSingleThreadExecutor	()Ljava/util/concurrent/ExecutorService;
    //   3: astore 11
    //   5: aload_1
    //   6: ldc_w 326
    //   9: invokestatic 328	cn/jiguang/f/d:b	(Landroid/content/Context;Ljava/lang/String;)Z
    //   12: istore_3
    //   13: aconst_null
    //   14: astore 10
    //   16: aconst_null
    //   17: astore 9
    //   19: iload_3
    //   20: ifeq +70 -> 90
    //   23: aload_1
    //   24: invokestatic 331	cn/jiguang/f/d:j	(Landroid/content/Context;)Z
    //   27: ifeq +55 -> 82
    //   30: new 333	cn/jiguang/u/d
    //   33: dup
    //   34: aload_1
    //   35: invokespecial 336	cn/jiguang/u/d:<init>	(Landroid/content/Context;)V
    //   38: invokevirtual 339	cn/jiguang/u/d:a	()Ljava/util/List;
    //   41: astore 7
    //   43: new 112	java/lang/StringBuilder
    //   46: dup
    //   47: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   50: astore 4
    //   52: aload 4
    //   54: ldc_w 341
    //   57: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: pop
    //   61: aload 4
    //   63: aload 7
    //   65: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: ldc 125
    //   71: aload 4
    //   73: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: invokestatic 234	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   79: goto +26 -> 105
    //   82: ldc_w 343
    //   85: astore 4
    //   87: goto +8 -> 95
    //   90: ldc_w 345
    //   93: astore 4
    //   95: ldc 125
    //   97: aload 4
    //   99: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   102: aconst_null
    //   103: astore 7
    //   105: aload_1
    //   106: ldc_w 347
    //   109: invokestatic 348	cn/jiguang/f/d:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   112: ifne +30 -> 142
    //   115: aload_1
    //   116: ldc_w 350
    //   119: invokestatic 348	cn/jiguang/f/d:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   122: ifeq +6 -> 128
    //   125: goto +17 -> 142
    //   128: ldc 125
    //   130: ldc_w 352
    //   133: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   136: aconst_null
    //   137: astore 4
    //   139: goto +243 -> 382
    //   142: new 354	java/util/concurrent/FutureTask
    //   145: astore 5
    //   147: new 6	cn/jiguang/u/a$a
    //   150: astore 4
    //   152: aload 4
    //   154: aload_0
    //   155: invokespecial 357	cn/jiguang/u/a$a:<init>	(Lcn/jiguang/u/a;)V
    //   158: aload 5
    //   160: aload 4
    //   162: invokespecial 360	java/util/concurrent/FutureTask:<init>	(Ljava/util/concurrent/Callable;)V
    //   165: aload 11
    //   167: aload 5
    //   169: invokeinterface 366 2 0
    //   174: aload 5
    //   176: ldc2_w 367
    //   179: getstatic 374	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   182: invokevirtual 377	java/util/concurrent/FutureTask:get	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    //   185: checkcast 136	cn/jiguang/v/a
    //   188: astore 4
    //   190: new 112	java/lang/StringBuilder
    //   193: astore 5
    //   195: aload 5
    //   197: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   200: aload 5
    //   202: ldc_w 379
    //   205: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: pop
    //   209: aload 5
    //   211: aload 4
    //   213: invokevirtual 380	cn/jiguang/v/a:toString	()Ljava/lang/String;
    //   216: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: ldc 125
    //   222: aload 5
    //   224: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   227: invokestatic 234	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   230: goto +152 -> 382
    //   233: astore 5
    //   235: goto +18 -> 253
    //   238: astore 5
    //   240: goto +46 -> 286
    //   243: astore 5
    //   245: goto +74 -> 319
    //   248: astore 5
    //   250: aconst_null
    //   251: astore 4
    //   253: new 112	java/lang/StringBuilder
    //   256: dup
    //   257: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   260: astore 6
    //   262: aload 6
    //   264: ldc_w 382
    //   267: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload 5
    //   273: invokevirtual 383	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   276: astore 5
    //   278: goto +66 -> 344
    //   281: astore 5
    //   283: aconst_null
    //   284: astore 4
    //   286: new 112	java/lang/StringBuilder
    //   289: dup
    //   290: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   293: astore 6
    //   295: aload 6
    //   297: ldc_w 385
    //   300: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: aload 5
    //   306: invokevirtual 386	java/lang/InterruptedException:getMessage	()Ljava/lang/String;
    //   309: astore 5
    //   311: goto +33 -> 344
    //   314: astore 5
    //   316: aconst_null
    //   317: astore 4
    //   319: new 112	java/lang/StringBuilder
    //   322: dup
    //   323: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   326: astore 6
    //   328: aload 6
    //   330: ldc_w 385
    //   333: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   336: pop
    //   337: aload 5
    //   339: invokevirtual 387	java/util/concurrent/ExecutionException:getMessage	()Ljava/lang/String;
    //   342: astore 5
    //   344: aload 6
    //   346: aload 5
    //   348: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   351: pop
    //   352: ldc 125
    //   354: aload 6
    //   356: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   359: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   362: aload 4
    //   364: astore 8
    //   366: goto +20 -> 386
    //   369: astore 4
    //   371: aconst_null
    //   372: astore 4
    //   374: ldc 125
    //   376: ldc_w 389
    //   379: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   382: aload 4
    //   384: astore 8
    //   386: aload_1
    //   387: ldc_w 347
    //   390: invokestatic 348	cn/jiguang/f/d:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   393: ifne +31 -> 424
    //   396: aload_1
    //   397: ldc_w 350
    //   400: invokestatic 348	cn/jiguang/f/d:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   403: ifeq +6 -> 409
    //   406: goto +18 -> 424
    //   409: ldc 125
    //   411: ldc_w 391
    //   414: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   417: aload 10
    //   419: astore 4
    //   421: goto +309 -> 730
    //   424: new 354	java/util/concurrent/FutureTask
    //   427: astore 4
    //   429: new 9	cn/jiguang/u/a$b
    //   432: astore 5
    //   434: aload 5
    //   436: aload_0
    //   437: invokespecial 392	cn/jiguang/u/a$b:<init>	(Lcn/jiguang/u/a;)V
    //   440: aload 4
    //   442: aload 5
    //   444: invokespecial 360	java/util/concurrent/FutureTask:<init>	(Ljava/util/concurrent/Callable;)V
    //   447: aload 11
    //   449: aload 4
    //   451: invokeinterface 366 2 0
    //   456: aload 4
    //   458: ldc2_w 367
    //   461: getstatic 374	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   464: invokevirtual 377	java/util/concurrent/FutureTask:get	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    //   467: checkcast 175	cn/jiguang/v/b
    //   470: astore 4
    //   472: new 112	java/lang/StringBuilder
    //   475: astore 5
    //   477: aload 5
    //   479: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   482: aload 5
    //   484: ldc_w 394
    //   487: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   490: pop
    //   491: aload 5
    //   493: aload 4
    //   495: invokevirtual 395	cn/jiguang/v/b:toString	()Ljava/lang/String;
    //   498: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   501: pop
    //   502: ldc 125
    //   504: aload 5
    //   506: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   509: invokestatic 234	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   512: new 112	java/lang/StringBuilder
    //   515: astore 5
    //   517: aload 5
    //   519: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   522: aload 5
    //   524: aload 4
    //   526: getfield 176	cn/jiguang/v/b:a	J
    //   529: invokevirtual 398	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   532: pop
    //   533: aload 5
    //   535: ldc_w 400
    //   538: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   541: pop
    //   542: aload 5
    //   544: aload 4
    //   546: getfield 187	cn/jiguang/v/b:d	D
    //   549: invokevirtual 403	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   552: pop
    //   553: aload 5
    //   555: ldc_w 400
    //   558: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   561: pop
    //   562: aload 5
    //   564: aload 4
    //   566: getfield 180	cn/jiguang/v/b:c	D
    //   569: invokevirtual 403	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   572: pop
    //   573: aload_1
    //   574: aload 5
    //   576: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   579: invokestatic 406	cn/jiguang/f/b:n	(Landroid/content/Context;Ljava/lang/String;)V
    //   582: goto +148 -> 730
    //   585: astore 5
    //   587: goto +23 -> 610
    //   590: astore 5
    //   592: goto +51 -> 643
    //   595: astore 5
    //   597: goto +79 -> 676
    //   600: astore 5
    //   602: goto +120 -> 722
    //   605: astore 5
    //   607: aconst_null
    //   608: astore 4
    //   610: new 112	java/lang/StringBuilder
    //   613: dup
    //   614: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   617: astore 6
    //   619: aload 6
    //   621: ldc_w 408
    //   624: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   627: pop
    //   628: aload 5
    //   630: invokevirtual 383	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   633: astore 5
    //   635: goto +66 -> 701
    //   638: astore 5
    //   640: aconst_null
    //   641: astore 4
    //   643: new 112	java/lang/StringBuilder
    //   646: dup
    //   647: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   650: astore 6
    //   652: aload 6
    //   654: ldc_w 410
    //   657: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   660: pop
    //   661: aload 5
    //   663: invokevirtual 386	java/lang/InterruptedException:getMessage	()Ljava/lang/String;
    //   666: astore 5
    //   668: goto +33 -> 701
    //   671: astore 5
    //   673: aconst_null
    //   674: astore 4
    //   676: new 112	java/lang/StringBuilder
    //   679: dup
    //   680: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   683: astore 6
    //   685: aload 6
    //   687: ldc_w 410
    //   690: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   693: pop
    //   694: aload 5
    //   696: invokevirtual 387	java/util/concurrent/ExecutionException:getMessage	()Ljava/lang/String;
    //   699: astore 5
    //   701: aload 6
    //   703: aload 5
    //   705: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   708: pop
    //   709: ldc 125
    //   711: aload 6
    //   713: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   716: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   719: goto +11 -> 730
    //   722: ldc 125
    //   724: ldc_w 412
    //   727: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   730: aload 11
    //   732: ifnull +64 -> 796
    //   735: aload 11
    //   737: invokeinterface 415 1 0
    //   742: ifne +54 -> 796
    //   745: aload 11
    //   747: invokeinterface 418 1 0
    //   752: goto +44 -> 796
    //   755: astore 5
    //   757: new 112	java/lang/StringBuilder
    //   760: dup
    //   761: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   764: astore 6
    //   766: aload 6
    //   768: ldc_w 420
    //   771: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   774: pop
    //   775: aload 6
    //   777: aload 5
    //   779: invokevirtual 383	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   782: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   785: pop
    //   786: ldc 125
    //   788: aload 6
    //   790: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   793: invokestatic 134	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   796: aload_0
    //   797: aload_0
    //   798: aload 7
    //   800: aload 8
    //   802: aload 4
    //   804: invokespecial 422	cn/jiguang/u/a:a	(Ljava/util/List;Lcn/jiguang/v/a;Lcn/jiguang/v/b;)Lorg/json/JSONObject;
    //   807: aload_1
    //   808: ldc_w 424
    //   811: invokestatic 429	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONObject;
    //   814: invokespecial 431	cn/jiguang/u/a:a	(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    //   817: astore 5
    //   819: aload 5
    //   821: ifnull +49 -> 870
    //   824: new 112	java/lang/StringBuilder
    //   827: dup
    //   828: invokespecial 113	java/lang/StringBuilder:<init>	()V
    //   831: astore 4
    //   833: aload 4
    //   835: ldc_w 433
    //   838: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   841: pop
    //   842: aload 4
    //   844: aload 5
    //   846: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   849: pop
    //   850: ldc 125
    //   852: aload 4
    //   854: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   857: invokestatic 234	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   860: aload_1
    //   861: ldc_w 424
    //   864: aload 5
    //   866: invokestatic 436	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)Z
    //   869: pop
    //   870: aload_0
    //   871: aload_1
    //   872: aload_2
    //   873: invokespecial 438	cn/jiguang/f/a:c	(Landroid/content/Context;Ljava/lang/String;)V
    //   876: return
    //   877: astore 5
    //   879: goto -505 -> 374
    //   882: astore 4
    //   884: aload 9
    //   886: astore 4
    //   888: goto -166 -> 722
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	891	0	this	a
    //   0	891	1	paramContext	Context
    //   0	891	2	paramString	String
    //   12	8	3	bool	boolean
    //   50	313	4	localObject1	Object
    //   369	1	4	localTimeoutException1	java.util.concurrent.TimeoutException
    //   372	481	4	localObject2	Object
    //   882	1	4	localTimeoutException2	java.util.concurrent.TimeoutException
    //   886	1	4	localObject3	Object
    //   145	78	5	localObject4	Object
    //   233	1	5	localThrowable1	Throwable
    //   238	1	5	localInterruptedException1	InterruptedException
    //   243	1	5	localExecutionException1	java.util.concurrent.ExecutionException
    //   248	24	5	localThrowable2	Throwable
    //   276	1	5	str1	String
    //   281	24	5	localInterruptedException2	InterruptedException
    //   309	1	5	str2	String
    //   314	24	5	localExecutionException2	java.util.concurrent.ExecutionException
    //   342	233	5	localObject5	Object
    //   585	1	5	localThrowable3	Throwable
    //   590	1	5	localInterruptedException3	InterruptedException
    //   595	1	5	localExecutionException3	java.util.concurrent.ExecutionException
    //   600	1	5	localTimeoutException3	java.util.concurrent.TimeoutException
    //   605	24	5	localThrowable4	Throwable
    //   633	1	5	str3	String
    //   638	24	5	localInterruptedException4	InterruptedException
    //   666	1	5	str4	String
    //   671	24	5	localExecutionException4	java.util.concurrent.ExecutionException
    //   699	5	5	str5	String
    //   755	23	5	localThrowable5	Throwable
    //   817	48	5	localJSONObject	JSONObject
    //   877	1	5	localTimeoutException4	java.util.concurrent.TimeoutException
    //   260	529	6	localStringBuilder	StringBuilder
    //   41	758	7	localList	List
    //   364	437	8	localObject6	Object
    //   17	868	9	localObject7	Object
    //   14	404	10	localObject8	Object
    //   3	743	11	localExecutorService	java.util.concurrent.ExecutorService
    // Exception table:
    //   from	to	target	type
    //   190	230	233	java/lang/Throwable
    //   190	230	238	java/lang/InterruptedException
    //   190	230	243	java/util/concurrent/ExecutionException
    //   105	125	248	java/lang/Throwable
    //   128	136	248	java/lang/Throwable
    //   142	190	248	java/lang/Throwable
    //   105	125	281	java/lang/InterruptedException
    //   128	136	281	java/lang/InterruptedException
    //   142	190	281	java/lang/InterruptedException
    //   105	125	314	java/util/concurrent/ExecutionException
    //   128	136	314	java/util/concurrent/ExecutionException
    //   142	190	314	java/util/concurrent/ExecutionException
    //   105	125	369	java/util/concurrent/TimeoutException
    //   128	136	369	java/util/concurrent/TimeoutException
    //   142	190	369	java/util/concurrent/TimeoutException
    //   472	582	585	java/lang/Throwable
    //   472	582	590	java/lang/InterruptedException
    //   472	582	595	java/util/concurrent/ExecutionException
    //   472	582	600	java/util/concurrent/TimeoutException
    //   386	406	605	java/lang/Throwable
    //   409	417	605	java/lang/Throwable
    //   424	472	605	java/lang/Throwable
    //   386	406	638	java/lang/InterruptedException
    //   409	417	638	java/lang/InterruptedException
    //   424	472	638	java/lang/InterruptedException
    //   386	406	671	java/util/concurrent/ExecutionException
    //   409	417	671	java/util/concurrent/ExecutionException
    //   424	472	671	java/util/concurrent/ExecutionException
    //   735	752	755	java/lang/Throwable
    //   190	230	877	java/util/concurrent/TimeoutException
    //   386	406	882	java/util/concurrent/TimeoutException
    //   409	417	882	java/util/concurrent/TimeoutException
    //   424	472	882	java/util/concurrent/TimeoutException
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JLocation";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    Object localObject = cn.jiguang.s.b.a(paramContext, "rl.catch");
    if (localObject == null)
    {
      cn.jiguang.ai.a.c("JLocation", "there are no data to report");
      return;
    }
    localObject = ((JSONObject)localObject).optJSONArray("content");
    if (localObject == null)
    {
      cn.jiguang.ai.a.c("JLocation", "there are no content data to report");
      return;
    }
    d.a(paramContext, localObject);
    cn.jiguang.ai.a.c("JLocation", "clean cache json");
    cn.jiguang.s.b.b(paramContext, "rl.catch");
    super.d(paramContext, paramString);
  }
  
  public Bundle f(Context paramContext)
  {
    paramContext = cn.jiguang.f.b.d(paramContext);
    long l;
    double d1;
    double d2;
    if (TextUtils.isEmpty(paramContext))
    {
      l = 0L;
      d1 = 200.0D;
      d2 = 200.0D;
    }
    else
    {
      paramContext = paramContext.split(",");
      l = Long.valueOf(paramContext[0]).longValue();
      d1 = Double.valueOf(paramContext[1]).doubleValue();
      d2 = Double.valueOf(paramContext[2]).doubleValue();
    }
    return a(l, d1, d2);
  }
  
  class a
    implements Callable<cn.jiguang.v.a>
  {
    a() {}
    
    public cn.jiguang.v.a a()
    {
      try
      {
        b localb = new cn/jiguang/u/b;
        localb.<init>(a.a(a.this));
        localb.b();
        Object localObject;
        for (int i = 0; i < 20; i++)
        {
          localObject = localb.a();
          if (localObject != null) {
            return (cn.jiguang.v.a)localObject;
          }
          Thread.sleep(2000L);
        }
        return null;
      }
      catch (Throwable localThrowable)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("JLocationCellInfo call failed:");
        ((StringBuilder)localObject).append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JLocation", ((StringBuilder)localObject).toString());
      }
    }
  }
  
  class b
    implements Callable<cn.jiguang.v.b>
  {
    b() {}
    
    public cn.jiguang.v.b a()
    {
      try
      {
        Looper.prepare();
        localObject = new cn/jiguang/u/c;
        ((c)localObject).<init>(a.a(a.this), Looper.myLooper());
        ((c)localObject).b();
        for (int i = 0; i < 20; i++)
        {
          cn.jiguang.v.b localb = ((c)localObject).a();
          if (localb != null) {
            return localb;
          }
          Thread.sleep(1000L);
        }
        Looper.loop();
      }
      catch (Throwable localThrowable)
      {
        Object localObject = new StringBuilder();
        ((StringBuilder)localObject).append("JLocationGpsInfo call failed:");
        ((StringBuilder)localObject).append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JLocation", ((StringBuilder)localObject).toString());
      }
      return null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/u/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */