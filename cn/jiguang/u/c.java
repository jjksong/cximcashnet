package cn.jiguang.u;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import cn.jiguang.ai.a;
import cn.jiguang.f.d;
import cn.jiguang.v.b;

@SuppressLint({"MissingPermission"})
class c
{
  private Looper a;
  private Context b;
  private LocationManager c;
  private b d;
  private LocationListener e = new LocationListener()
  {
    boolean a = false;
    
    public void onLocationChanged(Location paramAnonymousLocation)
    {
      try
      {
        if (this.a)
        {
          c.a(c.this).removeUpdates(this);
          return;
        }
        c.a(c.this, paramAnonymousLocation);
        this.a = true;
        c.a(c.this).removeUpdates(this);
        c.b(c.this).quit();
      }
      catch (Throwable paramAnonymousLocation)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("onLocationChanged failed:");
        localStringBuilder.append(paramAnonymousLocation.getMessage());
        a.g("JLocationGps", localStringBuilder.toString());
      }
    }
    
    public void onProviderDisabled(String paramAnonymousString) {}
    
    public void onProviderEnabled(String paramAnonymousString) {}
    
    public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle) {}
  };
  
  c(Context paramContext, Looper paramLooper)
  {
    this.a = paramLooper;
    this.b = paramContext;
    this.c = ((LocationManager)paramContext.getSystemService("location"));
  }
  
  private void a(Location paramLocation)
  {
    if (this.d == null) {
      this.d = new b();
    }
    this.d.a = d.a(this.b, paramLocation.getTime());
    this.d.b = paramLocation.getProvider();
    this.d.c = paramLocation.getLatitude();
    this.d.d = paramLocation.getLongitude();
    this.d.f = paramLocation.getBearing();
    this.d.g = paramLocation.getAccuracy();
  }
  
  private boolean a(Location paramLocation1, Location paramLocation2)
  {
    boolean bool2 = false;
    if (paramLocation1 == null) {
      return false;
    }
    if (paramLocation2 == null) {
      return true;
    }
    long l = paramLocation1.getTime() - paramLocation2.getTime();
    int j;
    if (l > 120000L) {
      j = 1;
    } else {
      j = 0;
    }
    int k;
    if (l < -120000L) {
      k = 1;
    } else {
      k = 0;
    }
    int i;
    if (l > 0L) {
      i = 1;
    } else {
      i = 0;
    }
    if (j != 0) {
      return true;
    }
    if (k != 0) {
      return false;
    }
    int m = (int)(paramLocation1.getAccuracy() - paramLocation2.getAccuracy());
    if (m > 0) {
      j = 1;
    } else {
      j = 0;
    }
    if (m < 0) {
      k = 1;
    } else {
      k = 0;
    }
    if (m > 200) {
      m = 1;
    } else {
      m = 0;
    }
    boolean bool3 = a(paramLocation1.getProvider(), paramLocation2.getProvider());
    if (k != 0) {
      return true;
    }
    if ((i != 0) && (j == 0)) {
      return true;
    }
    boolean bool1 = bool2;
    if (i != 0)
    {
      bool1 = bool2;
      if (m == 0)
      {
        bool1 = bool2;
        if (bool3) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  private boolean a(String paramString1, String paramString2)
  {
    if (paramString1 == null)
    {
      boolean bool;
      if (paramString2 == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    return paramString1.equals(paramString2);
  }
  
  b a()
  {
    return this.d;
  }
  
  void b()
  {
    Object localObject1 = this.c;
    if (localObject1 == null)
    {
      a.g("JLocationGps", "get locationManager failed");
      return;
    }
    boolean bool = ((LocationManager)localObject1).isProviderEnabled("gps");
    int k = 1;
    Location localLocation = null;
    int i;
    if (bool)
    {
      localObject1 = this.c.getLastKnownLocation("gps");
      i = 1;
    }
    else
    {
      localObject1 = null;
      i = 0;
    }
    Object localObject2;
    int j;
    if (this.c.isProviderEnabled("network"))
    {
      localObject2 = this.c.getLastKnownLocation("network");
      j = 1;
    }
    else
    {
      localObject2 = null;
      j = 0;
    }
    if (this.c.isProviderEnabled("passive")) {
      localLocation = this.c.getLastKnownLocation("passive");
    } else {
      k = 0;
    }
    if (a((Location)localObject1, (Location)localObject2))
    {
      if (a((Location)localObject1, localLocation)) {
        break label167;
      }
    }
    else if (a((Location)localObject2, localLocation))
    {
      localObject1 = localObject2;
      break label167;
    }
    localObject1 = localLocation;
    label167:
    if (localObject1 != null)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("bestLocation:");
      ((StringBuilder)localObject2).append(((Location)localObject1).getProvider());
      a.c("JLocationGps", ((StringBuilder)localObject2).toString());
      a((Location)localObject1);
    }
    else
    {
      if ((i != 0) || (j != 0)) {
        this.c.requestLocationUpdates("network", 2000L, 0.0F, this.e);
      }
      if (k != 0) {
        this.c.requestLocationUpdates("passive", 2000L, 0.0F, this.e);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/u/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */