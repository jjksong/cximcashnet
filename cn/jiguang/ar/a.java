package cn.jiguang.ar;

import cn.jiguang.ac.c;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class a
{
  private static HashMap<String, a> a = new HashMap();
  
  static
  {
    a.put("SDK_INIT", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("SDK_SERVICE_INIT", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("SDK_MAIN", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("ACTION", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("BUILD_REPORT", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("UPLOAD_REPORT", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("REPORT_HISTORY", new a()
    {
      ExecutorService a()
      {
        return Executors.newSingleThreadExecutor();
      }
    });
    a.put("ASYNC", new a()
    {
      ExecutorService a()
      {
        return Executors.newFixedThreadPool(15);
      }
    });
  }
  
  public static void a(String paramString)
  {
    if (!a.containsKey(paramString))
    {
      a.put(paramString, new a()
      {
        ExecutorService a()
        {
          return Executors.newSingleThreadExecutor();
        }
      });
    }
    else
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("already has executor=");
      localStringBuilder.append(paramString);
      c.h("SDKWorker_XExecutor", localStringBuilder.toString());
    }
  }
  
  public static void a(String paramString, Runnable paramRunnable)
  {
    a locala = (a)a.get(paramString);
    if (locala == null)
    {
      paramRunnable = new StringBuilder();
      paramRunnable.append("the executor[");
      paramRunnable.append(paramString);
      paramRunnable.append("] is not found");
      c.h("SDKWorker_XExecutor", paramRunnable.toString());
      return;
    }
    Object localObject = null;
    try
    {
      ExecutorService localExecutorService = locala.a(true);
      localObject = localExecutorService;
      localExecutorService.execute(paramRunnable);
    }
    catch (Throwable localThrowable2)
    {
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append("execute failed, try again e:");
      localStringBuilder2.append(localThrowable2);
      c.h("SDKWorker_XExecutor", localStringBuilder2.toString());
      a((ExecutorService)localObject);
      try
      {
        locala.a(true).execute(paramRunnable);
      }
      catch (Throwable localThrowable1)
      {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("execute e:");
        localStringBuilder1.append(localThrowable2);
        c.h("SDKWorker_XExecutor", localStringBuilder1.toString());
        if (paramRunnable != null) {
          try
          {
            if (paramString.equals("BUILD_REPORT"))
            {
              paramString = new java/lang/Thread;
              paramString.<init>(paramRunnable);
              paramString.start();
            }
          }
          catch (Throwable paramString)
          {
            c.d("SDKWorker_XExecutor", "execute BUILD_REPORT last error", paramString);
          }
        }
      }
    }
  }
  
  private static void a(ExecutorService paramExecutorService)
  {
    if (paramExecutorService == null) {
      return;
    }
    try
    {
      paramExecutorService.shutdown();
      if (!paramExecutorService.awaitTermination(100L, TimeUnit.MILLISECONDS))
      {
        paramExecutorService.shutdownNow();
        if (!paramExecutorService.awaitTermination(100L, TimeUnit.MILLISECONDS)) {
          c.a("SDKWorker_XExecutor", "executor did not terminate");
        }
      }
    }
    catch (Throwable paramExecutorService)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("shutDown e:");
      localStringBuilder.append(paramExecutorService);
      c.f("SDKWorker_XExecutor", localStringBuilder.toString());
    }
    catch (InterruptedException localInterruptedException)
    {
      paramExecutorService.shutdownNow();
      c.a("SDKWorker_XExecutor", "current thread is interrupted by self");
      Thread.currentThread().interrupt();
    }
  }
  
  static abstract class a
  {
    private ExecutorService a;
    
    abstract ExecutorService a();
    
    ExecutorService a(boolean paramBoolean)
    {
      if (paramBoolean)
      {
        ExecutorService localExecutorService = this.a;
        if ((localExecutorService == null) || (localExecutorService.isShutdown())) {
          this.a = a();
        }
      }
      return this.a;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ar/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */