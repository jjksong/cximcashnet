package cn.jiguang.ac;

import android.os.Environment;
import cn.jiguang.as.e;
import cn.jiguang.as.h;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public final class b
{
  private static String a = ".jpush";
  private static String b;
  private static String c;
  private static final SimpleDateFormat d = new SimpleDateFormat("MM.dd_HH:mm:ss_SSS", Locale.ENGLISH);
  private static ArrayList<String> e = new ArrayList();
  private static boolean f = false;
  private static boolean g = false;
  
  static
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(Environment.getExternalStorageDirectory().getAbsolutePath());
    localStringBuilder.append(File.separator);
    localStringBuilder.append(a);
    localStringBuilder.append(File.separator);
    b = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(b);
    localStringBuilder.append(a);
    c = localStringBuilder.toString();
  }
  
  private static void a(String paramString)
  {
    if (g) {
      return;
    }
    try
    {
      e.add(paramString);
      if (e.size() == 500)
      {
        ArrayList localArrayList = e;
        paramString = new java/util/ArrayList;
        paramString.<init>();
        e = paramString;
        f = e.a(cn.jiguang.a.a.a, "android.permission.WRITE_EXTERNAL_STORAGE");
        if (f)
        {
          c.a("Logger", "have writable external storage, write log file");
          a(localArrayList);
        }
        else
        {
          c.a("Logger", "no writable external storage");
        }
      }
    }
    catch (Throwable paramString)
    {
      paramString.printStackTrace();
    }
    catch (ArrayIndexOutOfBoundsException paramString)
    {
      paramString.printStackTrace();
      e = new ArrayList();
    }
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, Throwable paramThrowable)
  {
    Object localObject1;
    if (paramString2 != null)
    {
      localObject1 = paramString2;
      if (!paramString2.trim().equals("")) {}
    }
    else
    {
      localObject1 = "Logger";
    }
    if (paramString3 == null) {
      return;
    }
    try
    {
      paramString2 = d;
      Object localObject2 = new java/util/Date;
      ((Date)localObject2).<init>();
      paramString2 = paramString2.format((Date)localObject2);
      localObject2 = new java/io/StringReader;
      ((StringReader)localObject2).<init>(paramString3);
      paramString3 = new java/io/BufferedReader;
      paramString3.<init>((Reader)localObject2, 256);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append("[");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("]");
      String str = h.a(((StringBuilder)localObject2).toString(), 24);
      try
      {
        for (;;)
        {
          localObject2 = paramString3.readLine();
          if (localObject2 == null) {
            break;
          }
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append(paramString2);
          ((StringBuilder)localObject1).append(" ");
          ((StringBuilder)localObject1).append(h.a(paramString1, 5));
          ((StringBuilder)localObject1).append(" ");
          ((StringBuilder)localObject1).append(str);
          ((StringBuilder)localObject1).append(" ");
          ((StringBuilder)localObject1).append((String)localObject2);
          a(((StringBuilder)localObject1).toString());
        }
        if (paramThrowable == null) {
          return;
        }
      }
      catch (IOException paramString3)
      {
        c.h("Logger", paramString3.getMessage());
      }
      paramString3 = new java/io/StringWriter;
      paramString3.<init>();
      localObject1 = new java/io/PrintWriter;
      ((PrintWriter)localObject1).<init>(paramString3);
      paramThrowable.printStackTrace((PrintWriter)localObject1);
      paramString3 = paramString3.toString();
      paramThrowable = new java/lang/StringBuilder;
      paramThrowable.<init>();
      paramThrowable.append(paramString2);
      paramThrowable.append(" ");
      paramThrowable.append(paramString1);
      paramThrowable.append(paramString3);
      a(paramThrowable.toString());
    }
    catch (Throwable paramString1)
    {
      paramString1.printStackTrace();
    }
  }
  
  private static void a(ArrayList<String> paramArrayList)
  {
    try
    {
      if ((cn.jiguang.a.a.a != null) && (!e.a(cn.jiguang.a.a.a, "android.permission.WRITE_EXTERNAL_STORAGE")))
      {
        c.f("Logger", "WRITE_EXTERNAL_STORAGE not get");
        return;
      }
      Runnable local1 = new cn/jiguang/ac/b$1;
      local1.<init>(paramArrayList);
      cn.jiguang.ar.a.a("ASYNC", local1);
    }
    catch (Throwable paramArrayList)
    {
      paramArrayList.printStackTrace();
    }
  }
  
  private static void d()
  {
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(b);
      if (!localFile.exists()) {
        return;
      }
      int k = a.length() + 1;
      int m = cn.jiguang.as.b.a.length();
      if (localFile.listFiles() != null) {
        for (localFile : localFile.listFiles()) {
          if (cn.jiguang.as.b.a(cn.jiguang.as.b.b(localFile.getName().substring(k, m + k)), 2)) {
            localFile.delete();
          }
        }
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      c.h("Logger", localThrowable.getMessage());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ac/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */