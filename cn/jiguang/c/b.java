package cn.jiguang.c;

import android.app.Application;
import android.app.TabActivity;
import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;
import cn.jiguang.ad.f;
import cn.jiguang.as.h;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b
{
  public static boolean a = false;
  public static boolean b = false;
  private static volatile b c;
  private ExecutorService d = Executors.newSingleThreadExecutor();
  private String e = null;
  private String f = null;
  private long g = 30L;
  private long h = 0L;
  private long i = 0L;
  private boolean j = true;
  private boolean k = false;
  private boolean l = true;
  private long m = 0L;
  private JSONObject n = null;
  private final Object o = new Object();
  
  public static b a()
  {
    if (c == null) {
      try
      {
        b localb = new cn/jiguang/c/b;
        localb.<init>();
        c = localb;
      }
      finally {}
    }
    return c;
  }
  
  private JSONObject a(Context paramContext, long paramLong)
  {
    this.f = b(paramContext, paramLong);
    cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { cn.jiguang.e.a.n().a(Long.valueOf(this.h)), cn.jiguang.e.a.q().a(this.f) });
    JSONObject localJSONObject = new JSONObject();
    try
    {
      a(localJSONObject);
      f.a(paramContext, localJSONObject, "active_launch");
      localJSONObject.put("session_id", this.f);
      return localJSONObject;
    }
    catch (JSONException paramContext) {}
    return null;
  }
  
  private void a(Context paramContext, JSONObject paramJSONObject)
  {
    f.a(paramContext, "push_stat_cache.json", paramJSONObject);
  }
  
  private void a(JSONObject paramJSONObject)
  {
    String str2 = cn.jiguang.as.b.a();
    String str1 = str2.split("_")[0];
    str2 = str2.split("_")[1];
    paramJSONObject.put("date", str1);
    paramJSONObject.put("time", str2);
  }
  
  private void a(JSONObject paramJSONObject, Context paramContext)
  {
    long l1 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.n())).longValue();
    if (l1 <= 0L)
    {
      l1 = this.i - this.m;
      if (l1 > 0L) {
        l1 /= 1000L;
      } else {
        l1 = 10L;
      }
      cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { cn.jiguang.e.a.n().a(Long.valueOf(this.m)) });
    }
    else
    {
      l1 = (this.i - l1) / 1000L;
    }
    paramJSONObject.put("duration", l1);
    paramJSONObject.put("itime", System.currentTimeMillis() / 1000L);
    paramJSONObject.put("session_id", this.f);
    a(paramJSONObject);
  }
  
  private String b(Context paramContext, long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    paramContext = e.f(paramContext);
    if (!TextUtils.isEmpty(paramContext)) {
      localStringBuilder.append(paramContext);
    }
    localStringBuilder.append(paramLong);
    return h.c(localStringBuilder.toString());
  }
  
  private void b(JSONObject paramJSONObject)
  {
    this.n = paramJSONObject;
  }
  
  private boolean c(Context paramContext, String paramString)
  {
    if (!this.l)
    {
      c.d("PushSA", "stat function has been disabled");
      return false;
    }
    if (paramContext == null)
    {
      c.d("PushSA", "context is null");
      return false;
    }
    if ((paramContext instanceof Application))
    {
      paramContext = new StringBuilder();
      paramContext.append("Context should be an Activity on this method : ");
      paramContext.append(paramString);
      c.i("PushSA", paramContext.toString());
      return false;
    }
    return true;
  }
  
  private boolean d(Context paramContext)
  {
    boolean bool2 = this.j;
    boolean bool1 = false;
    if (bool2)
    {
      this.j = false;
      c.b("PushSA", "statistics start");
      long l1 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.p())).longValue();
      paramContext = new StringBuilder();
      paramContext.append("lastPause:");
      paramContext.append(l1);
      paramContext.append(",latestResumeTime:");
      paramContext.append(this.h);
      paramContext.append(",interval:");
      paramContext.append(this.g * 1000L);
      paramContext.append(",a:");
      paramContext.append(this.h - l1);
      c.b("PushSA", paramContext.toString());
      if ((l1 > 0L) && (this.h - l1 <= this.g * 1000L)) {
        return bool1;
      }
    }
    else if (this.h - this.i <= this.g * 1000L)
    {
      return bool1;
    }
    bool1 = true;
    return bool1;
  }
  
  private JSONObject e(Context paramContext)
  {
    if (this.n == null) {
      this.n = f.a(paramContext, "push_stat_cache.json");
    }
    return this.n;
  }
  
  private void f(Context paramContext)
  {
    JSONArray localJSONArray;
    JSONObject localJSONObject;
    if (d(paramContext))
    {
      c.d("PushSA", "new statistics session");
      localJSONArray = new JSONArray();
      ??? = a(paramContext, this.h);
      if (??? != null) {
        localJSONArray.put(???);
      }
      synchronized (this.o)
      {
        localJSONObject = e(paramContext);
        if (localJSONObject != null)
        {
          int i1 = localJSONObject.length();
          if (i1 <= 0) {}
        }
      }
    }
    try
    {
      f.a(paramContext, localJSONObject, "active_terminate");
      g(paramContext);
      this.n = null;
      if ((localJSONObject != null) && (localJSONObject.length() > 0)) {
        localJSONArray.put(localJSONObject);
      }
      f.a(paramContext, cn.jiguang.a.a.d, localJSONArray);
      break label148;
      paramContext = finally;
      throw paramContext;
      this.f = ((String)cn.jiguang.e.b.b(paramContext, cn.jiguang.e.a.q()));
      label148:
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void g(Context paramContext)
  {
    f.a(paramContext, "push_stat_cache.json", null);
  }
  
  private void h(Context paramContext)
  {
    if (paramContext == null) {
      return;
    }
    JSONObject localJSONObject1;
    synchronized (this.o)
    {
      cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { cn.jiguang.e.a.p().a(Long.valueOf(this.i)), cn.jiguang.e.a.o().a(Long.valueOf(this.i)) });
      JSONObject localJSONObject2 = e(paramContext);
      localJSONObject1 = localJSONObject2;
      if (localJSONObject2 == null)
      {
        localJSONObject1 = new org/json/JSONObject;
        localJSONObject1.<init>();
      }
    }
    try
    {
      a(localJSONObject1, paramContext);
      b(localJSONObject1);
      a(paramContext, localJSONObject1);
      return;
      paramContext = finally;
      throw paramContext;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public void a(long paramLong)
  {
    this.g = paramLong;
  }
  
  public void a(Context paramContext)
  {
    try
    {
      if ((this.e != null) && (this.k))
      {
        this.i = System.currentTimeMillis();
        Context localContext = paramContext.getApplicationContext();
        ExecutorService localExecutorService = this.d;
        paramContext = new cn/jiguang/c/b$3;
        paramContext.<init>(this, localContext);
        localExecutorService.execute(paramContext);
      }
      return;
    }
    catch (Exception|Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public void a(Context paramContext, String paramString)
  {
    if (this.k)
    {
      c.b("PushSA", "JCoreInterface.onResume() must be called after called JCoreInterface.onPause() in last Activity or Fragment");
      return;
    }
    this.k = true;
    this.e = paramString;
    this.h = System.currentTimeMillis();
    Context localContext = paramContext.getApplicationContext();
    try
    {
      paramContext = this.d;
      paramString = new cn/jiguang/c/b$1;
      paramString.<init>(this, localContext);
      paramContext.execute(paramString);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.l = paramBoolean;
  }
  
  public void b(Context paramContext)
  {
    if (!c(paramContext, "onResume")) {
      return;
    }
    a = true;
    try
    {
      TabActivity localTabActivity = (TabActivity)paramContext;
      this.k = false;
    }
    catch (ClassCastException|Exception localClassCastException) {}
    if (this.k) {
      return;
    }
    this.k = true;
    this.h = System.currentTimeMillis();
    this.e = paramContext.getClass().getName();
    try
    {
      Context localContext = paramContext.getApplicationContext();
      ExecutorService localExecutorService = this.d;
      paramContext = new cn/jiguang/c/b$a;
      paramContext.<init>(true, localContext, this);
      localExecutorService.execute(paramContext);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public void b(Context paramContext, String paramString)
  {
    if (!this.k)
    {
      c.b("PushSA", "JCoreInterface.onPause() must be called after called JCoreInterface.onResume() in this Activity or Fragment");
      return;
    }
    this.k = false;
    Object localObject = this.e;
    if ((localObject != null) && (((String)localObject).equals(paramString)))
    {
      this.i = System.currentTimeMillis();
      paramString = paramContext.getApplicationContext();
    }
    try
    {
      localObject = this.d;
      paramContext = new cn/jiguang/c/b$2;
      paramContext.<init>(this, paramString);
      ((ExecutorService)localObject).execute(paramContext);
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    c.i("PushSA", "page name didn't match the last one passed by onResume");
  }
  
  public void c(Context paramContext)
  {
    if (!c(paramContext, "onPause")) {
      return;
    }
    b = true;
    try
    {
      TabActivity localTabActivity = (TabActivity)paramContext;
      this.k = true;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    catch (ClassCastException localClassCastException) {}
    if (!this.k) {
      return;
    }
    this.k = false;
    Object localObject = this.e;
    if ((localObject != null) && (((String)localObject).equals(paramContext.getClass().getName())))
    {
      this.i = System.currentTimeMillis();
      this.m = this.h;
    }
    try
    {
      paramContext = paramContext.getApplicationContext();
      localObject = this.d;
      a locala = new cn/jiguang/c/b$a;
      locala.<init>(false, paramContext, this);
      ((ExecutorService)localObject).execute(locala);
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    c.d("PushSA", "the activity pass by onPause didn't match last one passed by onResume");
  }
  
  static class a
    implements Runnable
  {
    boolean a;
    Context b;
    b c;
    
    public a(boolean paramBoolean, Context paramContext, b paramb)
    {
      this.a = paramBoolean;
      this.b = paramContext;
      this.c = paramb;
    }
    
    public void run()
    {
      try
      {
        if (this.a) {
          b.a(this.c, this.b);
        } else {
          b.b(this.c, this.b);
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */