package cn.jiguang.c;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import cn.jiguang.ac.c;
import cn.jiguang.ad.f;
import cn.jiguang.api.JAnalyticsAction;
import cn.jiguang.api.JCoreManager;

@TargetApi(14)
public class a
  implements Application.ActivityLifecycleCallbacks
{
  private static boolean a = false;
  private static int b;
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    paramBundle = new StringBuilder();
    paramBundle.append("onActivityCreated:");
    paramBundle.append(paramActivity.getClass().getCanonicalName());
    c.b("ActivityLifecycle", paramBundle.toString());
    try
    {
      if (cn.jiguang.a.a.b != null) {
        cn.jiguang.a.a.b.dispatchStatus(paramActivity, "onCreate");
      }
    }
    catch (Throwable paramActivity)
    {
      c.b("ActivityLifecycle", "onActivityCreated failed");
    }
  }
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(Activity paramActivity)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("onActivityPaused:");
      localStringBuilder.append(paramActivity.getClass().getCanonicalName());
      c.d("ActivityLifecycle", localStringBuilder.toString());
      if (cn.jiguang.a.a.b != null) {
        cn.jiguang.a.a.b.dispatchPause(paramActivity);
      }
      if (!cn.jiguang.a.a.g) {
        b.a().c(paramActivity);
      }
      return;
    }
    catch (Throwable paramActivity)
    {
      for (;;) {}
    }
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("onActivityResumed:");
      localStringBuilder.append(paramActivity.getClass().getCanonicalName());
      c.d("ActivityLifecycle", localStringBuilder.toString());
      if (cn.jiguang.a.a.b != null) {
        cn.jiguang.a.a.b.dispatchResume(paramActivity);
      }
      if (!cn.jiguang.a.a.g) {
        b.a().b(paramActivity);
      }
      return;
    }
    catch (Throwable paramActivity)
    {
      for (;;) {}
    }
  }
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("onActivityStarted:");
    localStringBuilder.append(paramActivity.getClass().getCanonicalName());
    c.b("ActivityLifecycle", localStringBuilder.toString());
    try
    {
      if (b == 0)
      {
        c.b("ActivityLifecycle", "isForeground");
        if (paramActivity != null)
        {
          cn.jiguang.a.a.a(paramActivity.getApplicationContext(), false, 0L);
          JCoreManager.onEvent(paramActivity.getApplicationContext(), cn.jiguang.a.a.d, 29, null, null, new Object[] { Integer.valueOf(1) });
        }
        if (cn.jiguang.a.a.b != null) {
          cn.jiguang.a.a.b.dispatchStatus(paramActivity, "onStart");
        }
      }
      b += 1;
      return;
    }
    catch (Throwable paramActivity)
    {
      for (;;) {}
    }
  }
  
  public void onActivityStopped(Activity paramActivity)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("onActivityStopped:");
      localStringBuilder.append(paramActivity.getClass().getCanonicalName());
      c.b("ActivityLifecycle", localStringBuilder.toString());
      b -= 1;
      if (b == 0) {
        c.b("ActivityLifecycle", "is not Foreground");
      }
      paramActivity = cn.jiguang.a.a.a(paramActivity);
      f.a(paramActivity, null);
      f.a(paramActivity, cn.jiguang.a.a.d, null);
      return;
    }
    catch (Throwable paramActivity)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */