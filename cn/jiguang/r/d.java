package cn.jiguang.r;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class d
{
  private static cn.jiguang.q.b a(String paramString, int paramInt, List<cn.jiguang.q.b> paramList)
  {
    if ((paramList != null) && (!paramList.isEmpty()) && (!TextUtils.isEmpty(paramString)))
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        cn.jiguang.q.b localb = (cn.jiguang.q.b)paramList.next();
        if (a(paramString, paramInt, localb)) {
          return localb;
        }
      }
    }
    return null;
  }
  
  public static String a(Context paramContext)
  {
    String str = cn.jiguang.f.d.e(paramContext, "");
    if ((!TextUtils.isEmpty(str)) && (str.length() == 14)) {
      return str;
    }
    paramContext = b.c(paramContext);
    if ((!TextUtils.isEmpty(paramContext)) && (paramContext.length() == 14)) {
      return paramContext;
    }
    return "";
  }
  
  private static List<cn.jiguang.q.b> a(String paramString, int paramInt, List<cn.jiguang.q.b> paramList, cn.jiguang.q.b paramb)
  {
    if ((paramList != null) && (!paramList.isEmpty()) && (!TextUtils.isEmpty(paramString)) && (paramb != null))
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        cn.jiguang.q.b localb = (cn.jiguang.q.b)localIterator.next();
        if (a(paramString, paramInt, localb))
        {
          localb.b = paramb.b;
          localb.c = paramb.c;
        }
      }
      return paramList;
    }
    return null;
  }
  
  private static boolean a(String paramString, int paramInt, cn.jiguang.q.b paramb)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
      if (paramString.equals(paramb.c)) {
        return true;
      }
      break;
    case 1: 
      if (paramString.equals(paramb.b)) {
        return true;
      }
      break;
    case 0: 
      if (paramString.equals(paramb.a)) {
        return true;
      }
      break;
    }
    return false;
  }
  
  public static List<cn.jiguang.q.b> b(Context paramContext)
  {
    Object localObject2 = b.a(paramContext);
    Object localObject1 = e.a(paramContext);
    ArrayList localArrayList = b.b(paramContext);
    if ((localObject1 != null) && (!((List)localObject1).isEmpty())) {
      for (int i = ((List)localObject1).size() - 1; i >= 0; i--) {
        if (((cn.jiguang.q.b)((List)localObject1).get(i)).b()) {
          ((List)localObject1).remove(i);
        }
      }
    }
    if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
    {
      localObject2 = ((List)localObject1).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        paramContext = (cn.jiguang.q.b)((Iterator)localObject2).next();
        cn.jiguang.q.b localb;
        if ((TextUtils.isEmpty(paramContext.a)) && (!TextUtils.isEmpty(paramContext.b)))
        {
          localb = a(paramContext.b, 1, localArrayList);
          if (localb != null) {
            paramContext.a = localb.a;
          }
        }
        else if ((TextUtils.isEmpty(paramContext.b)) && (!TextUtils.isEmpty(paramContext.a)))
        {
          localb = a(paramContext.a, 1, localArrayList);
          if (localb != null)
          {
            paramContext.b = localb.b;
            paramContext.c = localb.c;
          }
        }
      }
      return (List<cn.jiguang.q.b>)localObject1;
    }
    paramContext = localArrayList;
    if (localObject2 != null) {
      if (((cn.jiguang.q.b)localObject2).b())
      {
        paramContext = localArrayList;
      }
      else if ((localArrayList != null) && (localArrayList.size() == 1))
      {
        if ((!TextUtils.isEmpty(((cn.jiguang.q.b)localObject2).a)) && (((cn.jiguang.q.b)localObject2).a.equals(((cn.jiguang.q.b)localArrayList.get(0)).a)))
        {
          paramContext = localArrayList;
          if (TextUtils.isEmpty(((cn.jiguang.q.b)localObject2).b)) {
            return paramContext;
          }
          paramContext = localArrayList;
          if (((cn.jiguang.q.b)localObject2).b.equals(((cn.jiguang.q.b)localArrayList.get(0)).b)) {
            return paramContext;
          }
        }
        else if ((!TextUtils.isEmpty(((cn.jiguang.q.b)localObject2).b)) && (((cn.jiguang.q.b)localObject2).b.equals(((cn.jiguang.q.b)localArrayList.get(0)).b)))
        {
          ((cn.jiguang.q.b)localObject2).b = "";
          ((cn.jiguang.q.b)localObject2).c = "";
          paramContext = localArrayList;
          if (((cn.jiguang.q.b)localObject2).b()) {
            return paramContext;
          }
        }
        localArrayList.add(localObject2);
        paramContext = localArrayList;
      }
      else
      {
        localObject1 = a(((cn.jiguang.q.b)localObject2).a, 0, localArrayList);
        if (localObject1 == null) {
          return localArrayList;
        }
        paramContext = localArrayList;
        if (TextUtils.isEmpty(((cn.jiguang.q.b)localObject1).b))
        {
          paramContext = localArrayList;
          if (a(((cn.jiguang.q.b)localObject2).b, 1, localArrayList) == null) {
            paramContext = a(((cn.jiguang.q.b)localObject2).a, 0, localArrayList, (cn.jiguang.q.b)localObject2);
          }
        }
      }
    }
    return paramContext;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/r/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */