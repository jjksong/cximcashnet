package cn.jiguang.r;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import cn.jiguang.ae.c;
import cn.jiguang.as.h;
import cn.jiguang.f.c.a;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static String a = "";
  private static String b = "";
  private static final FileFilter c = new FileFilter()
  {
    public boolean accept(File paramAnonymousFile)
    {
      paramAnonymousFile = paramAnonymousFile.getName();
      if (paramAnonymousFile.startsWith("cpu"))
      {
        int i = 3;
        while (i < paramAnonymousFile.length()) {
          if ((paramAnonymousFile.charAt(i) >= '0') && (paramAnonymousFile.charAt(i) <= '9')) {
            i++;
          } else {
            return false;
          }
        }
        return true;
      }
      return false;
    }
  };
  
  public static int a(String paramString)
  {
    boolean bool = h.a(paramString);
    int i = -1;
    if (bool) {
      return -1;
    }
    if (paramString.equalsIgnoreCase("ChinaTelecom")) {
      i = 2;
    } else if (paramString.equalsIgnoreCase("ChinaMobile")) {
      i = 0;
    } else if (paramString.equalsIgnoreCase("ChinaUnicom")) {
      i = 1;
    }
    return i;
  }
  
  private static int a(String paramString, FileInputStream paramFileInputStream)
  {
    byte[] arrayOfByte = new byte['Ѐ'];
    try
    {
      int m = paramFileInputStream.read(arrayOfByte);
      int k;
      for (int j = 0; j < m; j = k + 1) {
        if (arrayOfByte[j] != 10)
        {
          k = j;
          if (j != 0) {}
        }
        else
        {
          int i = j;
          if (arrayOfByte[j] == 10) {
            i = j + 1;
          }
          for (j = i;; j++)
          {
            k = i;
            if (j >= m) {
              break;
            }
            k = j - i;
            if (arrayOfByte[j] != paramString.charAt(k))
            {
              k = i;
              break;
            }
            if (k == paramString.length() - 1)
            {
              i = a(arrayOfByte, j);
              return i;
            }
          }
        }
      }
    }
    catch (Throwable paramString)
    {
      for (;;) {}
    }
    return -1;
  }
  
  private static int a(byte[] paramArrayOfByte, int paramInt)
  {
    while ((paramInt < paramArrayOfByte.length) && (paramArrayOfByte[paramInt] != 10))
    {
      if ((paramArrayOfByte[paramInt] >= 48) && (paramArrayOfByte[paramInt] <= 57))
      {
        for (int i = paramInt + 1; (i < paramArrayOfByte.length) && (paramArrayOfByte[i] >= 48) && (paramArrayOfByte[i] <= 57); i++) {}
        return Integer.parseInt(new String(paramArrayOfByte, 0, paramInt, i - paramInt));
      }
      paramInt++;
    }
    return -1;
  }
  
  public static String a()
  {
    if (!TextUtils.isEmpty(a)) {
      return a;
    }
    e();
    return a;
  }
  
  public static String a(Context paramContext)
  {
    if ((paramContext != null) && (paramContext.getResources() != null))
    {
      paramContext = paramContext.getResources().getDisplayMetrics();
      if (paramContext == null) {
        return "0*0";
      }
      int i = paramContext.widthPixels;
      int j = paramContext.heightPixels;
      paramContext = new StringBuilder();
      paramContext.append(i);
      paramContext.append("*");
      paramContext.append(j);
      return paramContext.toString();
    }
    return "0*0";
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    if (paramInt != 53) {
      return null;
    }
    try
    {
      Object localObject2 = g(paramContext);
      Object localObject1 = localObject2;
      if (TextUtils.isEmpty((CharSequence)localObject2)) {
        localObject1 = f(paramContext);
      }
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((String)localObject1);
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      ((JSONObject)localObject1).put("rom_type", cn.jiguang.f.d.m(paramContext));
      ((JSONObject)localObject1).put("regid", cn.jiguang.f.d.n(paramContext));
      ((JSONObject)localObject2).put("rom_info", localObject1);
      paramContext = new org/json/JSONObject;
      paramContext.<init>();
      paramContext.put("cmd", 53);
      paramContext.put("content", localObject2);
      paramContext = paramContext.toString();
      return paramContext;
    }
    catch (JSONException paramContext) {}
    return null;
  }
  
  public static void a(Context paramContext, String paramString)
  {
    String str = Base64.encodeToString(paramString.getBytes(), 2);
    c.a(paramContext, new cn.jiguang.ae.b[] { new cn.jiguang.ae.b("cn.jpush.preferences.v2", "n_udp_report_device_info", "").a(str) });
    paramContext = new StringBuilder();
    paramContext.append("save device info:");
    paramContext.append(paramString);
    cn.jiguang.ai.a.c("JDeviceHelper", paramContext.toString());
  }
  
  public static double b(Context paramContext)
  {
    Point localPoint = new Point();
    boolean bool = paramContext instanceof Activity;
    if (bool)
    {
      Display localDisplay = ((Activity)paramContext).getWindowManager().getDefaultDisplay();
      if (Build.VERSION.SDK_INT >= 17)
      {
        localDisplay.getRealSize(localPoint);
      }
      else if (Build.VERSION.SDK_INT >= 13)
      {
        localDisplay.getSize(localPoint);
      }
      else
      {
        localPoint.x = localDisplay.getWidth();
        localPoint.y = localDisplay.getHeight();
      }
    }
    paramContext = paramContext.getResources().getDisplayMetrics();
    double d;
    int i;
    if (bool)
    {
      d = Math.pow(localPoint.x / paramContext.xdpi, 2.0D);
      i = localPoint.y;
    }
    else
    {
      d = Math.pow(paramContext.widthPixels / paramContext.xdpi, 2.0D);
      i = paramContext.heightPixels;
    }
    return Math.sqrt(d + Math.pow(i / paramContext.ydpi, 2.0D));
  }
  
  private static long b(String paramString)
  {
    try
    {
      Object localObject = new java/io/FileReader;
      ((FileReader)localObject).<init>("/proc/meminfo");
      BufferedReader localBufferedReader = new java/io/BufferedReader;
      localBufferedReader.<init>((Reader)localObject, 4096);
      do
      {
        localObject = localBufferedReader.readLine();
      } while ((localObject != null) && (!((String)localObject).contains(paramString)));
      localBufferedReader.close();
      int i = Integer.valueOf(localObject.split("\\s+")[1]).intValue();
      return i;
    }
    catch (Throwable paramString) {}
    return -1L;
  }
  
  public static String b()
  {
    if (!TextUtils.isEmpty(b)) {
      return b;
    }
    e();
    return b;
  }
  
  public static int c()
  {
    if (Build.VERSION.SDK_INT <= 10) {
      return 1;
    }
    int i;
    try
    {
      File localFile = new java/io/File;
      localFile.<init>("/sys/devices/system/cpu/");
      i = localFile.listFiles(c).length;
    }
    catch (Throwable localThrowable)
    {
      i = -1;
    }
    return i;
  }
  
  public static long c(Context paramContext)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 16)
      {
        ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
        localActivityManager.getProcessMemoryInfo(new int[] { 0 });
        paramContext = new android/app/ActivityManager$MemoryInfo;
        paramContext.<init>();
        localActivityManager.getMemoryInfo(paramContext);
        long l = paramContext.totalMem / 1024L;
        return l;
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return b("MemTotal");
  }
  
  /* Error */
  public static int d()
  {
    // Byte code:
    //   0: iconst_m1
    //   1: istore_3
    //   2: iconst_0
    //   3: istore_2
    //   4: iconst_m1
    //   5: istore_0
    //   6: iload_2
    //   7: invokestatic 338	cn/jiguang/r/a:c	()I
    //   10: if_icmpge +194 -> 204
    //   13: new 104	java/lang/StringBuilder
    //   16: astore 4
    //   18: aload 4
    //   20: invokespecial 105	java/lang/StringBuilder:<init>	()V
    //   23: aload 4
    //   25: ldc_w 340
    //   28: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload 4
    //   34: iload_2
    //   35: invokevirtual 109	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload 4
    //   41: ldc_w 342
    //   44: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload 4
    //   50: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   53: astore 4
    //   55: new 296	java/io/File
    //   58: astore 6
    //   60: aload 6
    //   62: aload 4
    //   64: invokespecial 299	java/io/File:<init>	(Ljava/lang/String;)V
    //   67: iload_0
    //   68: istore_1
    //   69: aload 6
    //   71: invokevirtual 346	java/io/File:exists	()Z
    //   74: ifeq +122 -> 196
    //   77: sipush 128
    //   80: newarray <illegal type>
    //   82: astore 5
    //   84: new 43	java/io/FileInputStream
    //   87: astore 4
    //   89: aload 4
    //   91: aload 6
    //   93: invokespecial 349	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   96: aload 4
    //   98: aload 5
    //   100: invokevirtual 47	java/io/FileInputStream:read	([B)I
    //   103: pop
    //   104: iconst_0
    //   105: istore_1
    //   106: aload 5
    //   108: iload_1
    //   109: baload
    //   110: bipush 48
    //   112: if_icmplt +25 -> 137
    //   115: aload 5
    //   117: iload_1
    //   118: baload
    //   119: bipush 57
    //   121: if_icmpgt +16 -> 137
    //   124: iload_1
    //   125: aload 5
    //   127: arraylength
    //   128: if_icmpge +9 -> 137
    //   131: iinc 1 1
    //   134: goto -28 -> 106
    //   137: new 31	java/lang/String
    //   140: astore 6
    //   142: aload 6
    //   144: aload 5
    //   146: iconst_0
    //   147: iload_1
    //   148: invokespecial 352	java/lang/String:<init>	([BII)V
    //   151: aload 6
    //   153: invokestatic 66	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   156: invokestatic 355	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   159: astore 5
    //   161: iload_0
    //   162: istore_1
    //   163: aload 5
    //   165: invokevirtual 290	java/lang/Integer:intValue	()I
    //   168: iload_0
    //   169: if_icmple +9 -> 178
    //   172: aload 5
    //   174: invokevirtual 290	java/lang/Integer:intValue	()I
    //   177: istore_1
    //   178: aload 4
    //   180: invokevirtual 356	java/io/FileInputStream:close	()V
    //   183: goto +13 -> 196
    //   186: astore 5
    //   188: aload 4
    //   190: invokevirtual 356	java/io/FileInputStream:close	()V
    //   193: aload 5
    //   195: athrow
    //   196: iinc 2 1
    //   199: iload_1
    //   200: istore_0
    //   201: goto -195 -> 6
    //   204: iload_0
    //   205: iconst_m1
    //   206: if_icmpne +59 -> 265
    //   209: new 43	java/io/FileInputStream
    //   212: astore 4
    //   214: aload 4
    //   216: ldc_w 358
    //   219: invokespecial 359	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   222: ldc_w 361
    //   225: aload 4
    //   227: invokestatic 363	cn/jiguang/r/a:a	(Ljava/lang/String;Ljava/io/FileInputStream;)I
    //   230: istore_1
    //   231: iload_1
    //   232: sipush 1000
    //   235: imul
    //   236: istore_1
    //   237: iload_1
    //   238: iload_0
    //   239: if_icmple +8 -> 247
    //   242: iload_1
    //   243: istore_0
    //   244: goto +3 -> 247
    //   247: aload 4
    //   249: invokevirtual 356	java/io/FileInputStream:close	()V
    //   252: goto +13 -> 265
    //   255: astore 5
    //   257: aload 4
    //   259: invokevirtual 356	java/io/FileInputStream:close	()V
    //   262: aload 5
    //   264: athrow
    //   265: iload_0
    //   266: ireturn
    //   267: astore 4
    //   269: iload_3
    //   270: istore_0
    //   271: goto -6 -> 265
    //   274: astore 5
    //   276: iload_0
    //   277: istore_1
    //   278: goto -100 -> 178
    // Local variable table:
    //   start	length	slot	name	signature
    //   5	272	0	i	int
    //   68	210	1	j	int
    //   3	194	2	k	int
    //   1	269	3	m	int
    //   16	242	4	localObject1	Object
    //   267	1	4	localThrowable	Throwable
    //   82	91	5	localObject2	Object
    //   186	8	5	localObject3	Object
    //   255	8	5	localObject4	Object
    //   274	1	5	localNumberFormatException	NumberFormatException
    //   58	94	6	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   96	104	186	finally
    //   124	131	186	finally
    //   137	161	186	finally
    //   163	178	186	finally
    //   222	231	255	finally
    //   6	67	267	java/lang/Throwable
    //   69	96	267	java/lang/Throwable
    //   178	183	267	java/lang/Throwable
    //   188	196	267	java/lang/Throwable
    //   209	222	267	java/lang/Throwable
    //   247	252	267	java/lang/Throwable
    //   257	265	267	java/lang/Throwable
    //   96	104	274	java/lang/NumberFormatException
    //   96	104	274	java/lang/Throwable
    //   124	131	274	java/lang/NumberFormatException
    //   124	131	274	java/lang/Throwable
    //   137	161	274	java/lang/NumberFormatException
    //   137	161	274	java/lang/Throwable
    //   163	178	274	java/lang/NumberFormatException
    //   163	178	274	java/lang/Throwable
  }
  
  public static long d(Context paramContext)
  {
    try
    {
      String str = Environment.getDataDirectory().getPath();
      paramContext = new android/os/StatFs;
      paramContext.<init>(str);
      long l = paramContext.getBlockSize();
      l = paramContext.getBlockCount() * l / 1024L;
      return l;
    }
    catch (Throwable paramContext) {}
    return -1L;
  }
  
  public static JSONArray e(Context paramContext)
  {
    if (c.a.b) {
      return new JSONArray();
    }
    Object localObject1 = d.b(paramContext.getApplicationContext());
    if (localObject1 != null)
    {
      paramContext = new JSONArray();
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (cn.jiguang.q.b)((Iterator)localObject1).next();
        if (localObject2 != null)
        {
          localObject2 = ((cn.jiguang.q.b)localObject2).a();
          if (localObject2 != null) {
            paramContext.put(localObject2);
          }
        }
      }
      return paramContext;
    }
    return null;
  }
  
  private static void e()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    for (;;)
    {
      try
      {
        localObject1 = new java/io/File;
        ((File)localObject1).<init>("/proc/cpuinfo");
        if (!((File)localObject1).exists()) {
          continue;
        }
        localBufferedReader = new java/io/BufferedReader;
        localObject1 = new java/io/FileReader;
        localObject3 = new java/io/File;
        ((File)localObject3).<init>("/proc/cpuinfo");
        ((FileReader)localObject1).<init>((File)localObject3);
        localBufferedReader.<init>((Reader)localObject1);
        localObject1 = null;
      }
      catch (Throwable localThrowable1)
      {
        Object localObject1;
        BufferedReader localBufferedReader;
        Object localObject3;
        String str;
        Object localObject4;
        int i;
        boolean bool;
        continue;
        continue;
      }
      str = localBufferedReader.readLine();
      if (str != null)
      {
        localObject4 = localObject1;
        if (str.contains("Processor"))
        {
          i = str.indexOf(":");
          localObject3 = localObject1;
          if (i >= 0)
          {
            localObject3 = localObject1;
            if (i < str.length() - 1) {
              localObject3 = str.substring(i + 1).trim();
            }
          }
          localObject4 = localObject3;
          if (localObject3 != null)
          {
            localObject4 = localObject3;
            if (!localStringBuffer.toString().contains((CharSequence)localObject3))
            {
              localStringBuffer.append((String)localObject3);
              localObject4 = localObject3;
            }
          }
        }
        bool = str.contains("Hardware");
        localObject1 = localObject4;
        if (!bool) {
          continue;
        }
      }
      try
      {
        localObject3 = str.substring(str.indexOf(":") + 1).trim();
        localObject1 = localObject4;
        if (!TextUtils.isEmpty((CharSequence)localObject3))
        {
          b = (String)localObject3;
          localObject1 = localObject4;
        }
      }
      catch (Throwable localThrowable2)
      {
        Object localObject2 = localObject4;
      }
    }
    localBufferedReader.close();
    a = localStringBuffer.toString();
  }
  
  public static String f(Context paramContext)
  {
    try
    {
      cn.jiguang.ap.b localb = cn.jiguang.ap.b.a(paramContext);
      JSONObject localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      localJSONObject2.put("sim_slots", e(paramContext));
      localJSONObject2.put("packagename", paramContext.getPackageName());
      localJSONObject2.put("appkey", cn.jiguang.f.d.b(paramContext));
      localJSONObject2.put("platform", 0);
      localJSONObject2.put("apkversion", localb.a);
      localJSONObject2.put("systemversion", localb.b);
      localJSONObject2.put("modelnumber", localb.c);
      localJSONObject2.put("basebandversion", localb.d);
      localJSONObject2.put("buildnumber", localb.e);
      localJSONObject2.put("channel", localb.f);
      JSONObject localJSONObject1 = new org/json/JSONObject;
      localJSONObject1.<init>();
      localJSONObject1.put("PushSDKVer", cn.jiguang.f.d.b(0));
      localJSONObject1.put("StatisticSDKVer", cn.jiguang.f.d.b(1));
      localJSONObject1.put("ShareSDKVer", cn.jiguang.f.d.b(2));
      localJSONObject1.put("CoreSDKVer", cn.jiguang.f.d.b(3));
      localJSONObject1.put("SspSDKVer", cn.jiguang.f.d.b(4));
      localJSONObject1.put("VerificationSDKVer", cn.jiguang.f.d.b(5));
      localJSONObject2.put("sdkver", localJSONObject1);
      localJSONObject2.put("installation", localb.g);
      localJSONObject2.put("resolution", localb.h);
      localJSONObject2.put("business", cn.jiguang.f.d.d());
      localJSONObject2.put("device_id_status", cn.jiguang.f.d.i(paramContext));
      localJSONObject2.put("device_id", cn.jiguang.f.d.g(paramContext));
      localJSONObject2.put("android_id", localb.i);
      localJSONObject2.put("mac_address", cn.jiguang.f.d.c(paramContext, ""));
      localJSONObject2.put("serial_number", localb.j);
      paramContext = localJSONObject2.toString();
    }
    catch (JSONException paramContext)
    {
      paramContext = null;
    }
    return paramContext;
  }
  
  public static String g(Context paramContext)
  {
    Object localObject = (String)c.b(paramContext, new cn.jiguang.ae.b("cn.jpush.preferences.v2", "n_udp_report_device_info", ""));
    if (TextUtils.isEmpty((CharSequence)localObject))
    {
      localObject = (String)c.b(paramContext, new cn.jiguang.ae.b("cn.jpush.preferences.v2", "udp_report_device_info", ""));
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        a(paramContext, (String)localObject);
      }
      paramContext = (Context)localObject;
    }
    else
    {
      paramContext = new String(Base64.decode((String)localObject, 2));
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("read deviceinfo:");
    ((StringBuilder)localObject).append(paramContext);
    cn.jiguang.ai.a.c("JDeviceHelper", ((StringBuilder)localObject).toString());
    return paramContext;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/r/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */