package cn.jiguang.r;

import android.annotation.SuppressLint;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import cn.jiguang.f.c.a;
import cn.jiguang.q.b;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

@SuppressLint({"MissingPermission"})
public class e
{
  private static int a;
  
  private static int a(int paramInt)
  {
    try
    {
      Object localObject = Class.forName("android.telephony.SubscriptionManager").getDeclaredMethod("getSubId", new Class[] { Integer.TYPE });
      ((Method)localObject).setAccessible(true);
      localObject = (int[])((Method)localObject).invoke(null, new Object[] { Integer.valueOf(paramInt) });
      i = paramInt;
      if (localObject.length > 0) {
        i = localObject[0];
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        int i = paramInt;
      }
    }
    return i;
  }
  
  private static int a(TelephonyManager paramTelephonyManager)
  {
    int i;
    try
    {
      i = ((Integer)paramTelephonyManager.getClass().getMethod("getSimCount", new Class[0]).invoke(paramTelephonyManager, new Object[0])).intValue();
    }
    catch (Throwable paramTelephonyManager)
    {
      i = -1;
    }
    return i;
  }
  
  public static ArrayList<b> a(Context paramContext)
  {
    if (c.a.b) {
      return null;
    }
    int i = a;
    if (((i == 0) || (i == 1)) && (a()))
    {
      a = 1;
      return b(paramContext);
    }
    i = a;
    if (((i == 0) || (i == 2)) && (c(paramContext)))
    {
      a = 2;
      return d(paramContext);
    }
    i = a;
    if (((i == 0) || (i == 3)) && (e(paramContext)))
    {
      a = 3;
      return f(paramContext);
    }
    i = a;
    if (((i == 0) || (i == 4)) && (g(paramContext)))
    {
      a = 4;
      return h(paramContext);
    }
    a = 1;
    return b(paramContext);
  }
  
  private static boolean a()
  {
    boolean bool = false;
    try
    {
      Method localMethod = TelephonyManager.class.getMethod("getSimCount", new Class[0]);
      if (localMethod != null) {
        bool = true;
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    return bool;
  }
  
  /* Error */
  private static ArrayList<b> b(Context paramContext)
  {
    // Byte code:
    //   0: new 104	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 107	java/util/ArrayList:<init>	()V
    //   7: astore 4
    //   9: aload_0
    //   10: ldc 109
    //   12: invokevirtual 115	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   15: checkcast 102	android/telephony/TelephonyManager
    //   18: astore 6
    //   20: aload 6
    //   22: invokestatic 117	cn/jiguang/r/e:a	(Landroid/telephony/TelephonyManager;)I
    //   25: istore_2
    //   26: iload_2
    //   27: ifle +213 -> 240
    //   30: aload 6
    //   32: invokevirtual 56	java/lang/Object:getClass	()Ljava/lang/Class;
    //   35: ldc 119
    //   37: iconst_1
    //   38: anewarray 19	java/lang/Class
    //   41: dup
    //   42: iconst_0
    //   43: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   46: aastore
    //   47: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   50: astore 9
    //   52: aload 9
    //   54: iconst_1
    //   55: invokevirtual 41	java/lang/reflect/Method:setAccessible	(Z)V
    //   58: aload 6
    //   60: invokevirtual 56	java/lang/Object:getClass	()Ljava/lang/Class;
    //   63: ldc 121
    //   65: iconst_1
    //   66: anewarray 19	java/lang/Class
    //   69: dup
    //   70: iconst_0
    //   71: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   74: aastore
    //   75: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   78: astore 5
    //   80: aload 5
    //   82: iconst_1
    //   83: invokevirtual 41	java/lang/reflect/Method:setAccessible	(Z)V
    //   86: aload 6
    //   88: invokevirtual 56	java/lang/Object:getClass	()Ljava/lang/Class;
    //   91: ldc 123
    //   93: iconst_1
    //   94: anewarray 19	java/lang/Class
    //   97: dup
    //   98: iconst_0
    //   99: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   102: aastore
    //   103: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   106: astore 7
    //   108: aload 7
    //   110: iconst_1
    //   111: invokevirtual 41	java/lang/reflect/Method:setAccessible	(Z)V
    //   114: iconst_0
    //   115: istore_1
    //   116: iload_1
    //   117: iload_2
    //   118: if_icmpge +122 -> 240
    //   121: iload_1
    //   122: invokestatic 125	cn/jiguang/r/e:a	(I)I
    //   125: istore_3
    //   126: new 127	cn/jiguang/q/b
    //   129: astore_0
    //   130: aload_0
    //   131: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   134: aload_0
    //   135: aload 9
    //   137: aload 6
    //   139: iconst_1
    //   140: anewarray 4	java/lang/Object
    //   143: dup
    //   144: iconst_0
    //   145: iload_1
    //   146: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   149: aastore
    //   150: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   153: checkcast 130	java/lang/String
    //   156: invokestatic 135	cn/jiguang/r/b:a	(Ljava/lang/String;)Ljava/lang/String;
    //   159: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   162: aload_0
    //   163: aload 5
    //   165: aload 6
    //   167: iconst_1
    //   168: anewarray 4	java/lang/Object
    //   171: dup
    //   172: iconst_0
    //   173: iload_3
    //   174: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   177: aastore
    //   178: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   181: checkcast 130	java/lang/String
    //   184: putfield 140	cn/jiguang/q/b:c	Ljava/lang/String;
    //   187: aload_0
    //   188: aload 7
    //   190: aload 6
    //   192: iconst_1
    //   193: anewarray 4	java/lang/Object
    //   196: dup
    //   197: iconst_0
    //   198: iload_3
    //   199: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   202: aastore
    //   203: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   206: checkcast 130	java/lang/String
    //   209: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   212: aload 4
    //   214: aload_0
    //   215: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   218: pop
    //   219: iinc 1 1
    //   222: goto -106 -> 116
    //   225: astore_0
    //   226: aload 4
    //   228: invokevirtual 149	java/util/ArrayList:clear	()V
    //   231: goto +9 -> 240
    //   234: astore_0
    //   235: aload 4
    //   237: invokevirtual 149	java/util/ArrayList:clear	()V
    //   240: aload 4
    //   242: areturn
    //   243: astore 8
    //   245: goto -33 -> 212
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	248	0	paramContext	Context
    //   115	105	1	i	int
    //   25	94	2	j	int
    //   125	74	3	k	int
    //   7	234	4	localArrayList	ArrayList
    //   78	86	5	localMethod1	Method
    //   18	173	6	localTelephonyManager	TelephonyManager
    //   106	83	7	localMethod2	Method
    //   243	1	8	localThrowable	Throwable
    //   50	86	9	localMethod3	Method
    // Exception table:
    //   from	to	target	type
    //   30	114	225	java/lang/Throwable
    //   121	134	225	java/lang/Throwable
    //   212	219	225	java/lang/Throwable
    //   9	26	234	java/lang/Throwable
    //   226	231	234	java/lang/Throwable
    //   134	212	243	java/lang/Throwable
  }
  
  private static ArrayList<Integer> b(TelephonyManager paramTelephonyManager)
  {
    localArrayList = new ArrayList();
    try
    {
      Field[] arrayOfField = TelephonyManager.class.getDeclaredFields();
      int m = arrayOfField.length;
      int k = 0;
      int i;
      for (int j = 0; k < m; j = i)
      {
        Field localField = arrayOfField[k];
        localField.setAccessible(true);
        i = j;
        if (TextUtils.equals(localField.getType().getName(), "com.android.internal.telephony.ITelephonyRegistry"))
        {
          i = j;
          if (localField.get(paramTelephonyManager) != null)
          {
            localArrayList.add(Integer.valueOf(j));
            i = j + 1;
          }
        }
        k++;
      }
      return localArrayList;
    }
    catch (Throwable paramTelephonyManager)
    {
      localArrayList.clear();
      localArrayList.add(Integer.valueOf(0));
      localArrayList.add(Integer.valueOf(1));
    }
  }
  
  private static boolean c(Context paramContext)
  {
    try
    {
      b((TelephonyManager)paramContext.getSystemService("phone"));
      Method localMethod1 = TelephonyManager.class.getDeclaredMethod("getSubscriberIdGemini", new Class[] { Integer.TYPE });
      Method localMethod2 = TelephonyManager.class.getDeclaredMethod("getDeviceIdGemini", new Class[] { Integer.TYPE });
      paramContext = TelephonyManager.class.getDeclaredMethod("getPhoneTypeGemini", new Class[] { Integer.TYPE });
      Field localField = TelephonyManager.class.getDeclaredField("mtkGeminiSupport");
      if ((localMethod1 != null) && (localMethod2 != null) && (paramContext != null) && (localField != null))
      {
        localField.setAccessible(true);
        boolean bool = ((Boolean)localField.get(null)).booleanValue();
        if (bool) {
          return true;
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  /* Error */
  @SuppressLint({"PrivateApi"})
  private static ArrayList<b> d(Context paramContext)
  {
    // Byte code:
    //   0: new 104	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 107	java/util/ArrayList:<init>	()V
    //   7: astore_3
    //   8: aload_0
    //   9: ldc 109
    //   11: invokevirtual 115	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   14: checkcast 102	android/telephony/TelephonyManager
    //   17: astore_0
    //   18: ldc 102
    //   20: ldc -75
    //   22: iconst_1
    //   23: anewarray 19	java/lang/Class
    //   26: dup
    //   27: iconst_0
    //   28: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   31: aastore
    //   32: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   35: astore 4
    //   37: ldc 102
    //   39: ldc -73
    //   41: iconst_1
    //   42: anewarray 19	java/lang/Class
    //   45: dup
    //   46: iconst_0
    //   47: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   50: aastore
    //   51: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   54: astore 9
    //   56: ldc 102
    //   58: ldc -57
    //   60: iconst_1
    //   61: anewarray 19	java/lang/Class
    //   64: dup
    //   65: iconst_0
    //   66: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   69: aastore
    //   70: invokevirtual 35	java/lang/Class:getDeclaredMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   73: astore 7
    //   75: aload_0
    //   76: invokestatic 179	cn/jiguang/r/e:b	(Landroid/telephony/TelephonyManager;)Ljava/util/ArrayList;
    //   79: astore 8
    //   81: iconst_0
    //   82: istore_1
    //   83: iload_1
    //   84: aload 8
    //   86: invokevirtual 202	java/util/ArrayList:size	()I
    //   89: if_icmpge +117 -> 206
    //   92: new 127	cn/jiguang/q/b
    //   95: astore 6
    //   97: aload 6
    //   99: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   102: aload 8
    //   104: iload_1
    //   105: invokevirtual 205	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   108: checkcast 27	java/lang/Integer
    //   111: invokevirtual 65	java/lang/Integer:intValue	()I
    //   114: istore_2
    //   115: aload 6
    //   117: aload 4
    //   119: aload_0
    //   120: iconst_1
    //   121: anewarray 4	java/lang/Object
    //   124: dup
    //   125: iconst_0
    //   126: iload_2
    //   127: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   130: aastore
    //   131: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   134: checkcast 130	java/lang/String
    //   137: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   140: aload 6
    //   142: aload 9
    //   144: aload_0
    //   145: iconst_1
    //   146: anewarray 4	java/lang/Object
    //   149: dup
    //   150: iconst_0
    //   151: iload_2
    //   152: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   155: aastore
    //   156: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   159: checkcast 130	java/lang/String
    //   162: invokestatic 135	cn/jiguang/r/b:a	(Ljava/lang/String;)Ljava/lang/String;
    //   165: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   168: aload 6
    //   170: aload 7
    //   172: aload_0
    //   173: iconst_1
    //   174: anewarray 4	java/lang/Object
    //   177: dup
    //   178: iconst_0
    //   179: iload_2
    //   180: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   183: aastore
    //   184: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   187: checkcast 130	java/lang/String
    //   190: putfield 140	cn/jiguang/q/b:c	Ljava/lang/String;
    //   193: aload_3
    //   194: aload 6
    //   196: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   199: pop
    //   200: iinc 1 1
    //   203: goto -120 -> 83
    //   206: aload_3
    //   207: areturn
    //   208: astore_0
    //   209: aconst_null
    //   210: areturn
    //   211: astore 5
    //   213: goto -20 -> 193
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	216	0	paramContext	Context
    //   82	119	1	i	int
    //   114	66	2	j	int
    //   7	200	3	localArrayList1	ArrayList
    //   35	83	4	localMethod1	Method
    //   211	1	5	localThrowable	Throwable
    //   95	100	6	localb	b
    //   73	98	7	localMethod2	Method
    //   79	24	8	localArrayList2	ArrayList
    //   54	89	9	localMethod3	Method
    // Exception table:
    //   from	to	target	type
    //   8	81	208	java/lang/Throwable
    //   83	102	208	java/lang/Throwable
    //   193	200	208	java/lang/Throwable
    //   102	193	211	java/lang/Throwable
  }
  
  @SuppressLint({"PrivateApi"})
  private static boolean e(Context paramContext)
  {
    try
    {
      Object localObject1 = Class.forName("android.telephony.MSimTelephonyManager");
      Object localObject2 = paramContext.getSystemService("phone_msim");
      paramContext = ((Class)localObject1).getMethod("getDeviceId", new Class[] { Integer.TYPE });
      localObject1 = ((Class)localObject1).getMethod("getSubscriberId", new Class[] { Integer.TYPE });
      if ((localObject2 != null) && (paramContext != null) && (localObject1 != null)) {
        return true;
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  /* Error */
  @SuppressLint({"PrivateApi"})
  private static ArrayList<b> f(Context paramContext)
  {
    // Byte code:
    //   0: new 104	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 107	java/util/ArrayList:<init>	()V
    //   7: astore_1
    //   8: ldc -48
    //   10: invokestatic 23	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   13: astore_3
    //   14: aload_0
    //   15: ldc -46
    //   17: invokevirtual 115	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   20: astore_2
    //   21: aload_3
    //   22: ldc -44
    //   24: iconst_1
    //   25: anewarray 19	java/lang/Class
    //   28: dup
    //   29: iconst_0
    //   30: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   33: aastore
    //   34: invokevirtual 61	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   37: astore_0
    //   38: aload_3
    //   39: ldc 123
    //   41: iconst_1
    //   42: anewarray 19	java/lang/Class
    //   45: dup
    //   46: iconst_0
    //   47: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   50: aastore
    //   51: invokevirtual 61	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   54: astore_3
    //   55: new 127	cn/jiguang/q/b
    //   58: astore 5
    //   60: aload 5
    //   62: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   65: aload 5
    //   67: aload_0
    //   68: aload_2
    //   69: iconst_1
    //   70: anewarray 4	java/lang/Object
    //   73: dup
    //   74: iconst_0
    //   75: iconst_0
    //   76: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   79: aastore
    //   80: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   83: checkcast 130	java/lang/String
    //   86: invokestatic 135	cn/jiguang/r/b:a	(Ljava/lang/String;)Ljava/lang/String;
    //   89: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   92: aload 5
    //   94: aload_3
    //   95: aload_2
    //   96: iconst_1
    //   97: anewarray 4	java/lang/Object
    //   100: dup
    //   101: iconst_0
    //   102: iconst_0
    //   103: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   106: aastore
    //   107: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   110: checkcast 130	java/lang/String
    //   113: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   116: aload_1
    //   117: aload 5
    //   119: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   122: pop
    //   123: new 127	cn/jiguang/q/b
    //   126: astore 4
    //   128: aload 4
    //   130: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   133: aload 4
    //   135: aload_0
    //   136: aload_2
    //   137: iconst_1
    //   138: anewarray 4	java/lang/Object
    //   141: dup
    //   142: iconst_0
    //   143: iconst_1
    //   144: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   147: aastore
    //   148: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   151: checkcast 130	java/lang/String
    //   154: invokestatic 135	cn/jiguang/r/b:a	(Ljava/lang/String;)Ljava/lang/String;
    //   157: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   160: aload 4
    //   162: aload_3
    //   163: aload_2
    //   164: iconst_1
    //   165: anewarray 4	java/lang/Object
    //   168: dup
    //   169: iconst_0
    //   170: iconst_1
    //   171: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   174: aastore
    //   175: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   178: checkcast 130	java/lang/String
    //   181: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   184: aload_1
    //   185: aload 4
    //   187: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   190: pop
    //   191: aload_1
    //   192: areturn
    //   193: astore_0
    //   194: aconst_null
    //   195: areturn
    //   196: astore 4
    //   198: goto -82 -> 116
    //   201: astore_0
    //   202: goto -18 -> 184
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	205	0	paramContext	Context
    //   7	185	1	localArrayList	ArrayList
    //   20	144	2	localObject1	Object
    //   13	150	3	localObject2	Object
    //   126	60	4	localb1	b
    //   196	1	4	localThrowable	Throwable
    //   58	60	5	localb2	b
    // Exception table:
    //   from	to	target	type
    //   8	65	193	java/lang/Throwable
    //   116	133	193	java/lang/Throwable
    //   184	191	193	java/lang/Throwable
    //   65	116	196	java/lang/Throwable
    //   133	184	201	java/lang/Throwable
  }
  
  @SuppressLint({"PrivateApi"})
  private static boolean g(Context paramContext)
  {
    try
    {
      Object localObject = Class.forName("com.android.internal.telephony.PhoneFactory");
      Method localMethod = ((Class)localObject).getMethod("getServiceName", new Class[] { String.class, Integer.TYPE });
      localObject = (String)localMethod.invoke(localObject, new Object[] { "phone", Integer.valueOf(1) });
      paramContext = (TelephonyManager)paramContext.getSystemService((String)localObject);
      if ((localMethod != null) && (localObject != null) && (paramContext != null)) {
        return true;
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  /* Error */
  @SuppressLint({"PrivateApi"})
  private static ArrayList<b> h(Context paramContext)
  {
    // Byte code:
    //   0: new 104	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 107	java/util/ArrayList:<init>	()V
    //   7: astore_1
    //   8: ldc -42
    //   10: invokestatic 23	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   13: astore_2
    //   14: aload_0
    //   15: aload_2
    //   16: ldc -40
    //   18: iconst_2
    //   19: anewarray 19	java/lang/Class
    //   22: dup
    //   23: iconst_0
    //   24: ldc -126
    //   26: aastore
    //   27: dup
    //   28: iconst_1
    //   29: getstatic 31	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   32: aastore
    //   33: invokevirtual 61	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   36: aload_2
    //   37: iconst_2
    //   38: anewarray 4	java/lang/Object
    //   41: dup
    //   42: iconst_0
    //   43: ldc 109
    //   45: aastore
    //   46: dup
    //   47: iconst_1
    //   48: iconst_1
    //   49: invokestatic 45	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   52: aastore
    //   53: invokevirtual 49	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   56: checkcast 130	java/lang/String
    //   59: invokevirtual 115	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   62: checkcast 102	android/telephony/TelephonyManager
    //   65: astore_2
    //   66: aload_0
    //   67: ldc 109
    //   69: invokevirtual 115	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   72: checkcast 102	android/telephony/TelephonyManager
    //   75: astore_3
    //   76: new 127	cn/jiguang/q/b
    //   79: astore_0
    //   80: aload_0
    //   81: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   84: aload_0
    //   85: aload_3
    //   86: invokevirtual 218	android/telephony/TelephonyManager:getSubscriberId	()Ljava/lang/String;
    //   89: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   92: aload_0
    //   93: aload_3
    //   94: invokevirtual 220	android/telephony/TelephonyManager:getDeviceId	()Ljava/lang/String;
    //   97: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   100: aload_0
    //   101: aload_3
    //   102: invokevirtual 222	android/telephony/TelephonyManager:getSimSerialNumber	()Ljava/lang/String;
    //   105: putfield 140	cn/jiguang/q/b:c	Ljava/lang/String;
    //   108: aload_1
    //   109: aload_0
    //   110: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   113: pop
    //   114: new 127	cn/jiguang/q/b
    //   117: astore_0
    //   118: aload_0
    //   119: invokespecial 128	cn/jiguang/q/b:<init>	()V
    //   122: aload_0
    //   123: aload_2
    //   124: invokevirtual 218	android/telephony/TelephonyManager:getSubscriberId	()Ljava/lang/String;
    //   127: putfield 142	cn/jiguang/q/b:b	Ljava/lang/String;
    //   130: aload_0
    //   131: aload_2
    //   132: invokevirtual 220	android/telephony/TelephonyManager:getDeviceId	()Ljava/lang/String;
    //   135: putfield 138	cn/jiguang/q/b:a	Ljava/lang/String;
    //   138: aload_0
    //   139: aload_2
    //   140: invokevirtual 222	android/telephony/TelephonyManager:getSimSerialNumber	()Ljava/lang/String;
    //   143: putfield 140	cn/jiguang/q/b:c	Ljava/lang/String;
    //   146: aload_1
    //   147: aload_0
    //   148: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   151: pop
    //   152: aload_1
    //   153: areturn
    //   154: astore_0
    //   155: aconst_null
    //   156: areturn
    //   157: astore_3
    //   158: goto -50 -> 108
    //   161: astore_2
    //   162: goto -16 -> 146
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	165	0	paramContext	Context
    //   7	146	1	localArrayList	ArrayList
    //   13	127	2	localObject	Object
    //   161	1	2	localThrowable1	Throwable
    //   75	27	3	localTelephonyManager	TelephonyManager
    //   157	1	3	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   8	84	154	java/lang/Throwable
    //   108	122	154	java/lang/Throwable
    //   146	152	154	java/lang/Throwable
    //   84	108	157	java/lang/Throwable
    //   122	146	161	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/r/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */