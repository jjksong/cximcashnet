package cn.jiguang.r;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import cn.jiguang.f.d;
import java.io.File;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class c
{
  private static final Object a = new Object();
  private static CookieManager h;
  private String b;
  private String c;
  private String d;
  private Context e;
  private int f = 0;
  private String g;
  
  public c(Context paramContext)
  {
    a(paramContext, d.e(paramContext, this.b), b.c(paramContext, this.c), d.d(paramContext, this.d));
  }
  
  public static int a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return -1;
    }
    if ((!paramString.startsWith("46000")) && (!paramString.startsWith("46002")) && (!paramString.startsWith("46007")) && (!paramString.startsWith("46008")))
    {
      if ((!paramString.startsWith("46001")) && (!paramString.startsWith("46006")) && (!paramString.startsWith("46009")))
      {
        if ((!paramString.startsWith("46003")) && (!paramString.startsWith("46005")) && (!paramString.startsWith("46011"))) {
          return -1;
        }
        return 2;
      }
      return 1;
    }
    return 0;
  }
  
  /* Error */
  private cn.jiguang.q.a a(Context paramContext, String paramString1, int paramInt, long paramLong, boolean paramBoolean, File paramFile, String paramString2)
  {
    // Byte code:
    //   0: lload 4
    //   2: ldc2_w 93
    //   5: lcmp
    //   6: iflt +18 -> 24
    //   9: lload 4
    //   11: ldc2_w 95
    //   14: lcmp
    //   15: ifle +6 -> 21
    //   18: goto +6 -> 24
    //   21: goto +8 -> 29
    //   24: ldc2_w 97
    //   27: lstore 4
    //   29: invokestatic 104	java/util/UUID:randomUUID	()Ljava/util/UUID;
    //   32: invokevirtual 108	java/util/UUID:toString	()Ljava/lang/String;
    //   35: astore 28
    //   37: ldc 110
    //   39: astore 26
    //   41: getstatic 112	cn/jiguang/r/c:h	Ljava/net/CookieManager;
    //   44: ifnonnull +13 -> 57
    //   47: new 114	java/net/CookieManager
    //   50: dup
    //   51: invokespecial 115	java/net/CookieManager:<init>	()V
    //   54: putstatic 112	cn/jiguang/r/c:h	Ljava/net/CookieManager;
    //   57: aconst_null
    //   58: astore 18
    //   60: aconst_null
    //   61: astore 16
    //   63: iconst_0
    //   64: istore 13
    //   66: aconst_null
    //   67: astore 17
    //   69: iconst_m1
    //   70: istore 9
    //   72: aload 7
    //   74: astore 22
    //   76: aload 18
    //   78: astore 19
    //   80: aload 18
    //   82: astore 20
    //   84: aload 18
    //   86: astore 21
    //   88: aload_1
    //   89: aload_2
    //   90: invokestatic 121	cn/jiguang/net/HttpUtils:getHttpURLConnectionWithProxy	(Landroid/content/Context;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   93: astore 15
    //   95: aload 15
    //   97: astore 19
    //   99: aload 15
    //   101: astore 20
    //   103: aload 15
    //   105: sipush 30000
    //   108: invokevirtual 127	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   111: aload 15
    //   113: astore 19
    //   115: aload 15
    //   117: astore 20
    //   119: aload 15
    //   121: sipush 30000
    //   124: invokevirtual 130	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   127: aload 15
    //   129: astore 19
    //   131: aload 15
    //   133: astore 20
    //   135: aload 15
    //   137: iconst_1
    //   138: invokevirtual 134	java/net/HttpURLConnection:setDoInput	(Z)V
    //   141: aload 15
    //   143: astore 19
    //   145: aload 15
    //   147: astore 20
    //   149: aload 15
    //   151: iconst_1
    //   152: invokevirtual 137	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   155: aload 15
    //   157: astore 19
    //   159: aload 15
    //   161: astore 20
    //   163: aload 15
    //   165: iconst_0
    //   166: invokevirtual 140	java/net/HttpURLConnection:setUseCaches	(Z)V
    //   169: aload 15
    //   171: astore 19
    //   173: aload 15
    //   175: astore 20
    //   177: aload 15
    //   179: ldc -114
    //   181: invokevirtual 146	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   184: aload 15
    //   186: astore 19
    //   188: aload 15
    //   190: astore 20
    //   192: aload 15
    //   194: ldc -108
    //   196: ldc -106
    //   198: invokevirtual 154	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   201: aload 15
    //   203: astore 19
    //   205: aload 15
    //   207: astore 20
    //   209: aload 15
    //   211: ldc -100
    //   213: ldc -98
    //   215: invokevirtual 161	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   218: aload 15
    //   220: astore 19
    //   222: aload 15
    //   224: astore 20
    //   226: getstatic 112	cn/jiguang/r/c:h	Ljava/net/CookieManager;
    //   229: invokevirtual 165	java/net/CookieManager:getCookieStore	()Ljava/net/CookieStore;
    //   232: invokeinterface 171 1 0
    //   237: invokeinterface 177 1 0
    //   242: istore 10
    //   244: iload 10
    //   246: ifle +42 -> 288
    //   249: aload 15
    //   251: astore 19
    //   253: aload 15
    //   255: astore 20
    //   257: aload 15
    //   259: astore 21
    //   261: aload 15
    //   263: astore 18
    //   265: aload 15
    //   267: ldc -77
    //   269: ldc -75
    //   271: getstatic 112	cn/jiguang/r/c:h	Ljava/net/CookieManager;
    //   274: invokevirtual 165	java/net/CookieManager:getCookieStore	()Ljava/net/CookieStore;
    //   277: invokeinterface 171 1 0
    //   282: invokestatic 185	android/text/TextUtils:join	(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   285: invokevirtual 154	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   288: aload 15
    //   290: astore 19
    //   292: aload 15
    //   294: iload 6
    //   296: invokevirtual 188	java/net/HttpURLConnection:setInstanceFollowRedirects	(Z)V
    //   299: aload 22
    //   301: ifnull +844 -> 1145
    //   304: aload 15
    //   306: astore 19
    //   308: new 190	java/lang/StringBuilder
    //   311: astore 18
    //   313: aload 15
    //   315: astore 19
    //   317: aload 18
    //   319: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   322: aload 15
    //   324: astore 19
    //   326: aload 18
    //   328: aload 26
    //   330: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload 15
    //   336: astore 19
    //   338: aload 18
    //   340: ldc -59
    //   342: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   345: pop
    //   346: aload 15
    //   348: astore 19
    //   350: aload 18
    //   352: aload 28
    //   354: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   357: pop
    //   358: aload 15
    //   360: astore 19
    //   362: aload 15
    //   364: ldc -57
    //   366: aload 18
    //   368: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   371: invokevirtual 154	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   374: aload 15
    //   376: astore 19
    //   378: aload 15
    //   380: invokevirtual 204	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   383: astore 20
    //   385: aload 15
    //   387: astore 19
    //   389: new 206	java/io/DataOutputStream
    //   392: astore 18
    //   394: aload 15
    //   396: astore 19
    //   398: aload 18
    //   400: aload 20
    //   402: invokespecial 209	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   405: aload 15
    //   407: astore 19
    //   409: new 211	java/lang/StringBuffer
    //   412: astore 20
    //   414: aload 15
    //   416: astore 19
    //   418: aload 20
    //   420: invokespecial 212	java/lang/StringBuffer:<init>	()V
    //   423: aload 15
    //   425: astore 19
    //   427: aload 20
    //   429: ldc -42
    //   431: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   434: pop
    //   435: aload 15
    //   437: astore 19
    //   439: aload 20
    //   441: aload 28
    //   443: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   446: pop
    //   447: aload 15
    //   449: astore 19
    //   451: aload 20
    //   453: ldc -37
    //   455: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   458: pop
    //   459: aload 15
    //   461: astore 19
    //   463: new 190	java/lang/StringBuilder
    //   466: astore 21
    //   468: aload 15
    //   470: astore 19
    //   472: aload 21
    //   474: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   477: iload 9
    //   479: istore 10
    //   481: iload 9
    //   483: istore 12
    //   485: aload 15
    //   487: astore 19
    //   489: aload 21
    //   491: ldc -35
    //   493: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   496: pop
    //   497: iload 9
    //   499: istore 10
    //   501: iload 9
    //   503: istore 12
    //   505: aload 15
    //   507: astore 19
    //   509: aload 21
    //   511: aload 8
    //   513: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   516: pop
    //   517: iload 9
    //   519: istore 10
    //   521: iload 9
    //   523: istore 12
    //   525: aload 15
    //   527: astore 19
    //   529: aload 21
    //   531: ldc -33
    //   533: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   536: pop
    //   537: iload 9
    //   539: istore 10
    //   541: iload 9
    //   543: istore 12
    //   545: aload 15
    //   547: astore 19
    //   549: aload 21
    //   551: aload 7
    //   553: invokevirtual 228	java/io/File:getName	()Ljava/lang/String;
    //   556: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   559: pop
    //   560: iload 9
    //   562: istore 10
    //   564: iload 9
    //   566: istore 12
    //   568: aload 15
    //   570: astore 19
    //   572: aload 21
    //   574: ldc -26
    //   576: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   579: pop
    //   580: iload 9
    //   582: istore 10
    //   584: iload 9
    //   586: istore 12
    //   588: aload 15
    //   590: astore 19
    //   592: aload 21
    //   594: ldc -37
    //   596: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   599: pop
    //   600: iload 9
    //   602: istore 10
    //   604: iload 9
    //   606: istore 12
    //   608: aload 15
    //   610: astore 19
    //   612: aload 20
    //   614: aload 21
    //   616: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   619: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   622: pop
    //   623: iload 9
    //   625: istore 10
    //   627: iload 9
    //   629: istore 12
    //   631: aload 15
    //   633: astore 19
    //   635: new 190	java/lang/StringBuilder
    //   638: astore 21
    //   640: iload 9
    //   642: istore 10
    //   644: iload 9
    //   646: istore 12
    //   648: aload 15
    //   650: astore 19
    //   652: aload 21
    //   654: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   657: iload 9
    //   659: istore 10
    //   661: iload 9
    //   663: istore 12
    //   665: aload 15
    //   667: astore 19
    //   669: aload 21
    //   671: ldc -24
    //   673: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   676: pop
    //   677: iload 9
    //   679: istore 10
    //   681: iload 9
    //   683: istore 12
    //   685: aload 15
    //   687: astore 19
    //   689: aload 21
    //   691: ldc -37
    //   693: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   696: pop
    //   697: iload 9
    //   699: istore 10
    //   701: iload 9
    //   703: istore 12
    //   705: aload 15
    //   707: astore 19
    //   709: aload 20
    //   711: aload 21
    //   713: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   716: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   719: pop
    //   720: iload 9
    //   722: istore 10
    //   724: iload 9
    //   726: istore 12
    //   728: aload 15
    //   730: astore 19
    //   732: aload 20
    //   734: ldc -37
    //   736: invokevirtual 217	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   739: pop
    //   740: iload 9
    //   742: istore 10
    //   744: iload 9
    //   746: istore 12
    //   748: aload 15
    //   750: astore 19
    //   752: aload 18
    //   754: aload 20
    //   756: invokevirtual 233	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   759: invokevirtual 237	java/lang/String:getBytes	()[B
    //   762: invokevirtual 241	java/io/DataOutputStream:write	([B)V
    //   765: iload 9
    //   767: istore 10
    //   769: iload 9
    //   771: istore 12
    //   773: aload 15
    //   775: astore 19
    //   777: new 243	java/io/FileInputStream
    //   780: astore 20
    //   782: iload 9
    //   784: istore 10
    //   786: iload 9
    //   788: istore 12
    //   790: aload 15
    //   792: astore 19
    //   794: aload 20
    //   796: aload 22
    //   798: invokespecial 246	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   801: iload 9
    //   803: istore 10
    //   805: iload 9
    //   807: istore 12
    //   809: aload 15
    //   811: astore 19
    //   813: sipush 1024
    //   816: newarray <illegal type>
    //   818: astore 21
    //   820: iload 9
    //   822: istore 10
    //   824: iload 9
    //   826: istore 12
    //   828: aload 15
    //   830: astore 19
    //   832: aload 20
    //   834: aload 21
    //   836: invokevirtual 252	java/io/InputStream:read	([B)I
    //   839: istore 14
    //   841: iload 14
    //   843: iconst_m1
    //   844: if_icmpeq +32 -> 876
    //   847: iload 9
    //   849: istore 11
    //   851: iload 9
    //   853: istore 10
    //   855: iload 9
    //   857: istore 12
    //   859: aload 15
    //   861: astore 19
    //   863: aload 18
    //   865: aload 21
    //   867: iconst_0
    //   868: iload 14
    //   870: invokevirtual 255	java/io/DataOutputStream:write	([BII)V
    //   873: goto -53 -> 820
    //   876: iload 9
    //   878: istore 11
    //   880: iload 9
    //   882: istore 10
    //   884: iload 9
    //   886: istore 12
    //   888: aload 15
    //   890: astore 19
    //   892: aload 20
    //   894: invokevirtual 258	java/io/InputStream:close	()V
    //   897: iload 9
    //   899: istore 11
    //   901: iload 9
    //   903: istore 10
    //   905: iload 9
    //   907: istore 12
    //   909: aload 15
    //   911: astore 19
    //   913: aload 18
    //   915: ldc -37
    //   917: invokevirtual 237	java/lang/String:getBytes	()[B
    //   920: invokevirtual 241	java/io/DataOutputStream:write	([B)V
    //   923: iload 9
    //   925: istore 11
    //   927: iload 9
    //   929: istore 10
    //   931: iload 9
    //   933: istore 12
    //   935: aload 15
    //   937: astore 19
    //   939: new 190	java/lang/StringBuilder
    //   942: astore 20
    //   944: iload 9
    //   946: istore 11
    //   948: iload 9
    //   950: istore 10
    //   952: iload 9
    //   954: istore 12
    //   956: aload 15
    //   958: astore 19
    //   960: aload 20
    //   962: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   965: iload 9
    //   967: istore 11
    //   969: iload 9
    //   971: istore 10
    //   973: iload 9
    //   975: istore 12
    //   977: aload 15
    //   979: astore 19
    //   981: aload 20
    //   983: ldc -42
    //   985: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   988: pop
    //   989: iload 9
    //   991: istore 11
    //   993: iload 9
    //   995: istore 10
    //   997: iload 9
    //   999: istore 12
    //   1001: aload 15
    //   1003: astore 19
    //   1005: aload 20
    //   1007: aload 28
    //   1009: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1012: pop
    //   1013: iload 9
    //   1015: istore 11
    //   1017: iload 9
    //   1019: istore 10
    //   1021: iload 9
    //   1023: istore 12
    //   1025: aload 15
    //   1027: astore 19
    //   1029: aload 20
    //   1031: ldc -42
    //   1033: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1036: pop
    //   1037: iload 9
    //   1039: istore 11
    //   1041: iload 9
    //   1043: istore 10
    //   1045: iload 9
    //   1047: istore 12
    //   1049: aload 15
    //   1051: astore 19
    //   1053: aload 20
    //   1055: ldc -37
    //   1057: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1060: pop
    //   1061: iload 9
    //   1063: istore 11
    //   1065: iload 9
    //   1067: istore 10
    //   1069: iload 9
    //   1071: istore 12
    //   1073: aload 15
    //   1075: astore 19
    //   1077: aload 18
    //   1079: aload 20
    //   1081: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1084: invokevirtual 237	java/lang/String:getBytes	()[B
    //   1087: invokevirtual 241	java/io/DataOutputStream:write	([B)V
    //   1090: iload 9
    //   1092: istore 11
    //   1094: iload 9
    //   1096: istore 10
    //   1098: iload 9
    //   1100: istore 12
    //   1102: aload 15
    //   1104: astore 19
    //   1106: aload 18
    //   1108: invokevirtual 261	java/io/DataOutputStream:flush	()V
    //   1111: iload 9
    //   1113: istore 11
    //   1115: iload 9
    //   1117: istore 10
    //   1119: iload 9
    //   1121: istore 12
    //   1123: aload 15
    //   1125: astore 19
    //   1127: aload 18
    //   1129: invokevirtual 262	java/io/DataOutputStream:close	()V
    //   1132: goto +13 -> 1145
    //   1135: astore 18
    //   1137: goto +5 -> 1142
    //   1140: astore 18
    //   1142: goto +782 -> 1924
    //   1145: iload 9
    //   1147: istore 11
    //   1149: iload 9
    //   1151: istore 10
    //   1153: iload 9
    //   1155: istore 12
    //   1157: aload 15
    //   1159: astore 19
    //   1161: aload 15
    //   1163: invokevirtual 265	java/net/HttpURLConnection:getResponseCode	()I
    //   1166: istore 9
    //   1168: iload 9
    //   1170: istore 11
    //   1172: iload 9
    //   1174: istore 10
    //   1176: iload 9
    //   1178: istore 12
    //   1180: aload 15
    //   1182: astore 19
    //   1184: aload 15
    //   1186: invokevirtual 269	java/net/HttpURLConnection:getHeaderFields	()Ljava/util/Map;
    //   1189: astore 27
    //   1191: aload 16
    //   1193: astore 24
    //   1195: aload 17
    //   1197: astore 21
    //   1199: aload 16
    //   1201: astore 25
    //   1203: aload 17
    //   1205: astore 20
    //   1207: aload 16
    //   1209: astore 23
    //   1211: aload 17
    //   1213: astore 22
    //   1215: aload 15
    //   1217: astore 19
    //   1219: aload 16
    //   1221: astore 18
    //   1223: aload_0
    //   1224: aload 27
    //   1226: invokespecial 272	cn/jiguang/r/c:a	(Ljava/util/Map;)V
    //   1229: iload 9
    //   1231: sipush 302
    //   1234: if_icmpne +159 -> 1393
    //   1237: aload 16
    //   1239: astore 24
    //   1241: aload 17
    //   1243: astore 21
    //   1245: aload 16
    //   1247: astore 25
    //   1249: aload 17
    //   1251: astore 20
    //   1253: aload 16
    //   1255: astore 23
    //   1257: aload 17
    //   1259: astore 22
    //   1261: aload 15
    //   1263: astore 19
    //   1265: aload 16
    //   1267: astore 18
    //   1269: aload 15
    //   1271: ldc_w 274
    //   1274: invokevirtual 278	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   1277: astore 27
    //   1279: iload_3
    //   1280: iflt +83 -> 1363
    //   1283: aload 16
    //   1285: astore 24
    //   1287: aload 17
    //   1289: astore 21
    //   1291: aload 16
    //   1293: astore 25
    //   1295: aload 17
    //   1297: astore 20
    //   1299: aload 16
    //   1301: astore 23
    //   1303: aload 17
    //   1305: astore 22
    //   1307: aload 15
    //   1309: astore 19
    //   1311: aload 16
    //   1313: astore 18
    //   1315: aload_0
    //   1316: aload_1
    //   1317: aload 27
    //   1319: iload_3
    //   1320: iconst_1
    //   1321: isub
    //   1322: lconst_0
    //   1323: iload 6
    //   1325: aconst_null
    //   1326: aconst_null
    //   1327: invokespecial 280	cn/jiguang/r/c:a	(Landroid/content/Context;Ljava/lang/String;IJZLjava/io/File;Ljava/lang/String;)Lcn/jiguang/q/a;
    //   1330: astore 17
    //   1332: aload 16
    //   1334: ifnull +16 -> 1350
    //   1337: aload 16
    //   1339: invokevirtual 258	java/io/InputStream:close	()V
    //   1342: goto +8 -> 1350
    //   1345: astore_1
    //   1346: aload_1
    //   1347: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   1350: aload 15
    //   1352: ifnull +8 -> 1360
    //   1355: aload 15
    //   1357: invokevirtual 286	java/net/HttpURLConnection:disconnect	()V
    //   1360: aload 17
    //   1362: areturn
    //   1363: aload 16
    //   1365: ifnull +16 -> 1381
    //   1368: aload 16
    //   1370: invokevirtual 258	java/io/InputStream:close	()V
    //   1373: goto +8 -> 1381
    //   1376: astore_1
    //   1377: aload_1
    //   1378: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   1381: aload 15
    //   1383: ifnull +8 -> 1391
    //   1386: aload 15
    //   1388: invokevirtual 286	java/net/HttpURLConnection:disconnect	()V
    //   1391: aconst_null
    //   1392: areturn
    //   1393: iload 9
    //   1395: sipush 200
    //   1398: if_icmpne +381 -> 1779
    //   1401: aload 16
    //   1403: astore 24
    //   1405: aload 17
    //   1407: astore 21
    //   1409: aload 16
    //   1411: astore 25
    //   1413: aload 17
    //   1415: astore 20
    //   1417: aload 16
    //   1419: astore 23
    //   1421: aload 17
    //   1423: astore 22
    //   1425: aload 15
    //   1427: astore 19
    //   1429: aload 16
    //   1431: astore 18
    //   1433: aload 15
    //   1435: invokevirtual 290	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   1438: astore 16
    //   1440: aload 16
    //   1442: astore 24
    //   1444: aload 17
    //   1446: astore 21
    //   1448: aload 16
    //   1450: astore 25
    //   1452: aload 17
    //   1454: astore 20
    //   1456: aload 16
    //   1458: astore 23
    //   1460: aload 17
    //   1462: astore 22
    //   1464: aload 15
    //   1466: astore 19
    //   1468: aload 16
    //   1470: astore 18
    //   1472: aload 15
    //   1474: invokevirtual 269	java/net/HttpURLConnection:getHeaderFields	()Ljava/util/Map;
    //   1477: astore 17
    //   1479: aload 16
    //   1481: astore 24
    //   1483: aload 17
    //   1485: astore 21
    //   1487: aload 16
    //   1489: astore 25
    //   1491: aload 17
    //   1493: astore 20
    //   1495: aload 16
    //   1497: astore 23
    //   1499: aload 17
    //   1501: astore 22
    //   1503: aload 15
    //   1505: astore 19
    //   1507: aload 16
    //   1509: astore 18
    //   1511: aload 16
    //   1513: invokestatic 293	cn/jiguang/f/d:a	(Ljava/io/InputStream;)[B
    //   1516: astore 27
    //   1518: aload 27
    //   1520: ifnull +92 -> 1612
    //   1523: aload 16
    //   1525: astore 24
    //   1527: aload 17
    //   1529: astore 21
    //   1531: aload 16
    //   1533: astore 25
    //   1535: aload 17
    //   1537: astore 20
    //   1539: aload 16
    //   1541: astore 23
    //   1543: aload 17
    //   1545: astore 22
    //   1547: aload 15
    //   1549: astore 19
    //   1551: aload 16
    //   1553: astore 18
    //   1555: aload 27
    //   1557: arraylength
    //   1558: ifle +54 -> 1612
    //   1561: aload 16
    //   1563: astore 24
    //   1565: aload 17
    //   1567: astore 21
    //   1569: aload 16
    //   1571: astore 25
    //   1573: aload 17
    //   1575: astore 20
    //   1577: aload 16
    //   1579: astore 23
    //   1581: aload 17
    //   1583: astore 22
    //   1585: aload 15
    //   1587: astore 19
    //   1589: aload 16
    //   1591: astore 18
    //   1593: new 59	java/lang/String
    //   1596: dup
    //   1597: aload 27
    //   1599: ldc -106
    //   1601: invokespecial 296	java/lang/String:<init>	([BLjava/lang/String;)V
    //   1604: astore 27
    //   1606: aload 27
    //   1608: astore_1
    //   1609: goto +7 -> 1616
    //   1612: ldc_w 298
    //   1615: astore_1
    //   1616: aload 16
    //   1618: ifnull +16 -> 1634
    //   1621: aload 16
    //   1623: invokevirtual 258	java/io/InputStream:close	()V
    //   1626: goto +8 -> 1634
    //   1629: astore_2
    //   1630: aload_2
    //   1631: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   1634: aload 15
    //   1636: ifnull +8 -> 1644
    //   1639: aload 15
    //   1641: invokevirtual 286	java/net/HttpURLConnection:disconnect	()V
    //   1644: iload 9
    //   1646: sipush 200
    //   1649: if_icmplt +14 -> 1663
    //   1652: iload 9
    //   1654: sipush 300
    //   1657: if_icmpge +6 -> 1663
    //   1660: goto +106 -> 1766
    //   1663: iload 9
    //   1665: sipush 400
    //   1668: if_icmplt +86 -> 1754
    //   1671: iload 9
    //   1673: sipush 500
    //   1676: if_icmpge +78 -> 1754
    //   1679: sipush 400
    //   1682: iload 9
    //   1684: if_icmpne +10 -> 1694
    //   1687: ldc_w 300
    //   1690: astore_1
    //   1691: goto +75 -> 1766
    //   1694: sipush 401
    //   1697: iload 9
    //   1699: if_icmpne +6 -> 1705
    //   1702: goto +60 -> 1762
    //   1705: sipush 404
    //   1708: iload 9
    //   1710: if_icmpne +6 -> 1716
    //   1713: goto +49 -> 1762
    //   1716: sipush 406
    //   1719: iload 9
    //   1721: if_icmpne +6 -> 1727
    //   1724: goto +38 -> 1762
    //   1727: sipush 408
    //   1730: iload 9
    //   1732: if_icmpne +6 -> 1738
    //   1735: goto +27 -> 1762
    //   1738: sipush 409
    //   1741: iload 9
    //   1743: if_icmpne +6 -> 1749
    //   1746: goto +16 -> 1762
    //   1749: aconst_null
    //   1750: astore_1
    //   1751: goto +15 -> 1766
    //   1754: iload 9
    //   1756: sipush 500
    //   1759: if_icmplt +3 -> 1762
    //   1762: ldc_w 302
    //   1765: astore_1
    //   1766: new 304	cn/jiguang/q/a
    //   1769: dup
    //   1770: iload 9
    //   1772: aload 17
    //   1774: aload_1
    //   1775: invokespecial 307	cn/jiguang/q/a:<init>	(ILjava/util/Map;Ljava/lang/String;)V
    //   1778: areturn
    //   1779: aload 16
    //   1781: ifnull +18 -> 1799
    //   1784: aload 16
    //   1786: invokevirtual 258	java/io/InputStream:close	()V
    //   1789: goto +10 -> 1799
    //   1792: astore 18
    //   1794: aload 18
    //   1796: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   1799: aload 15
    //   1801: astore 18
    //   1803: aload 16
    //   1805: astore 20
    //   1807: aload 17
    //   1809: astore 19
    //   1811: iload 9
    //   1813: istore 10
    //   1815: aload 15
    //   1817: ifnull +528 -> 2345
    //   1820: goto +504 -> 2324
    //   1823: astore 20
    //   1825: aload 24
    //   1827: astore 16
    //   1829: aload 21
    //   1831: astore 17
    //   1833: goto +95 -> 1928
    //   1836: astore 21
    //   1838: aload 25
    //   1840: astore 16
    //   1842: aload 20
    //   1844: astore 17
    //   1846: goto +217 -> 2063
    //   1849: astore 20
    //   1851: aload 23
    //   1853: astore 16
    //   1855: aload 22
    //   1857: astore 17
    //   1859: goto +343 -> 2202
    //   1862: astore 18
    //   1864: iload 11
    //   1866: istore 9
    //   1868: goto +56 -> 1924
    //   1871: astore 18
    //   1873: iload 10
    //   1875: istore 9
    //   1877: goto +182 -> 2059
    //   1880: astore 18
    //   1882: iload 12
    //   1884: istore 9
    //   1886: goto +312 -> 2198
    //   1889: astore 18
    //   1891: goto +33 -> 1924
    //   1894: astore 18
    //   1896: goto +163 -> 2059
    //   1899: astore 18
    //   1901: goto +297 -> 2198
    //   1904: astore 18
    //   1906: goto +153 -> 2059
    //   1909: astore 18
    //   1911: goto +287 -> 2198
    //   1914: astore_1
    //   1915: goto +554 -> 2469
    //   1918: astore 18
    //   1920: aload 20
    //   1922: astore 15
    //   1924: aload 18
    //   1926: astore 20
    //   1928: aload 15
    //   1930: astore 19
    //   1932: aload 16
    //   1934: astore 18
    //   1936: new 190	java/lang/StringBuilder
    //   1939: astore 21
    //   1941: aload 15
    //   1943: astore 19
    //   1945: aload 16
    //   1947: astore 18
    //   1949: aload 21
    //   1951: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   1954: aload 15
    //   1956: astore 19
    //   1958: aload 16
    //   1960: astore 18
    //   1962: aload 21
    //   1964: ldc_w 309
    //   1967: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1970: pop
    //   1971: aload 15
    //   1973: astore 19
    //   1975: aload 16
    //   1977: astore 18
    //   1979: aload 21
    //   1981: aload 20
    //   1983: invokevirtual 312	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1986: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1989: pop
    //   1990: aload 15
    //   1992: astore 19
    //   1994: aload 16
    //   1996: astore 18
    //   1998: ldc_w 314
    //   2001: aload 21
    //   2003: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2006: invokestatic 318	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   2009: aload 16
    //   2011: ifnull +18 -> 2029
    //   2014: aload 16
    //   2016: invokevirtual 258	java/io/InputStream:close	()V
    //   2019: goto +10 -> 2029
    //   2022: astore 18
    //   2024: aload 18
    //   2026: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   2029: aload 15
    //   2031: astore 18
    //   2033: aload 16
    //   2035: astore 20
    //   2037: aload 17
    //   2039: astore 19
    //   2041: iload 9
    //   2043: istore 10
    //   2045: aload 15
    //   2047: ifnull +298 -> 2345
    //   2050: goto +274 -> 2324
    //   2053: astore 18
    //   2055: aload 21
    //   2057: astore 15
    //   2059: aload 18
    //   2061: astore 21
    //   2063: aload 15
    //   2065: astore 19
    //   2067: aload 16
    //   2069: astore 18
    //   2071: new 190	java/lang/StringBuilder
    //   2074: astore 20
    //   2076: aload 15
    //   2078: astore 19
    //   2080: aload 16
    //   2082: astore 18
    //   2084: aload 20
    //   2086: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   2089: aload 15
    //   2091: astore 19
    //   2093: aload 16
    //   2095: astore 18
    //   2097: aload 20
    //   2099: ldc_w 309
    //   2102: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2105: pop
    //   2106: aload 15
    //   2108: astore 19
    //   2110: aload 16
    //   2112: astore 18
    //   2114: aload 20
    //   2116: aload 21
    //   2118: invokevirtual 319	java/lang/AssertionError:getMessage	()Ljava/lang/String;
    //   2121: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2124: pop
    //   2125: aload 15
    //   2127: astore 19
    //   2129: aload 16
    //   2131: astore 18
    //   2133: ldc_w 314
    //   2136: aload 20
    //   2138: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2141: invokestatic 318	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   2144: aload 16
    //   2146: ifnull +18 -> 2164
    //   2149: aload 16
    //   2151: invokevirtual 258	java/io/InputStream:close	()V
    //   2154: goto +10 -> 2164
    //   2157: astore 18
    //   2159: aload 18
    //   2161: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   2164: aload 15
    //   2166: astore 18
    //   2168: aload 16
    //   2170: astore 20
    //   2172: aload 17
    //   2174: astore 19
    //   2176: iload 9
    //   2178: istore 10
    //   2180: aload 15
    //   2182: ifnull +163 -> 2345
    //   2185: goto +139 -> 2324
    //   2188: astore 19
    //   2190: aload 18
    //   2192: astore 15
    //   2194: aload 19
    //   2196: astore 18
    //   2198: aload 18
    //   2200: astore 20
    //   2202: aload 15
    //   2204: astore 19
    //   2206: aload 16
    //   2208: astore 18
    //   2210: new 190	java/lang/StringBuilder
    //   2213: astore 21
    //   2215: aload 15
    //   2217: astore 19
    //   2219: aload 16
    //   2221: astore 18
    //   2223: aload 21
    //   2225: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   2228: aload 15
    //   2230: astore 19
    //   2232: aload 16
    //   2234: astore 18
    //   2236: aload 21
    //   2238: ldc_w 309
    //   2241: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2244: pop
    //   2245: aload 15
    //   2247: astore 19
    //   2249: aload 16
    //   2251: astore 18
    //   2253: aload 21
    //   2255: aload 20
    //   2257: invokevirtual 320	javax/net/ssl/SSLPeerUnverifiedException:getMessage	()Ljava/lang/String;
    //   2260: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2263: pop
    //   2264: aload 15
    //   2266: astore 19
    //   2268: aload 16
    //   2270: astore 18
    //   2272: ldc_w 314
    //   2275: aload 21
    //   2277: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2280: invokestatic 318	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   2283: aload 16
    //   2285: ifnull +18 -> 2303
    //   2288: aload 16
    //   2290: invokevirtual 258	java/io/InputStream:close	()V
    //   2293: goto +10 -> 2303
    //   2296: astore 18
    //   2298: aload 18
    //   2300: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   2303: aload 15
    //   2305: astore 18
    //   2307: aload 16
    //   2309: astore 20
    //   2311: aload 17
    //   2313: astore 19
    //   2315: iload 9
    //   2317: istore 10
    //   2319: aload 15
    //   2321: ifnull +24 -> 2345
    //   2324: aload 15
    //   2326: invokevirtual 286	java/net/HttpURLConnection:disconnect	()V
    //   2329: iload 9
    //   2331: istore 10
    //   2333: aload 17
    //   2335: astore 19
    //   2337: aload 16
    //   2339: astore 20
    //   2341: aload 15
    //   2343: astore 18
    //   2345: iload 10
    //   2347: sipush 404
    //   2350: if_icmpeq +109 -> 2459
    //   2353: iload 10
    //   2355: sipush 200
    //   2358: if_icmpeq +13 -> 2371
    //   2361: aload_1
    //   2362: invokestatic 324	cn/jiguang/f/d:l	(Landroid/content/Context;)Z
    //   2365: ifne +6 -> 2371
    //   2368: goto +91 -> 2459
    //   2371: iload 13
    //   2373: iconst_3
    //   2374: if_icmplt +17 -> 2391
    //   2377: new 304	cn/jiguang/q/a
    //   2380: dup
    //   2381: iconst_m1
    //   2382: aload 19
    //   2384: ldc_w 326
    //   2387: invokespecial 307	cn/jiguang/q/a:<init>	(ILjava/util/Map;Ljava/lang/String;)V
    //   2390: areturn
    //   2391: iinc 13 1
    //   2394: lload 4
    //   2396: invokestatic 332	java/lang/Thread:sleep	(J)V
    //   2399: goto +45 -> 2444
    //   2402: astore 15
    //   2404: new 190	java/lang/StringBuilder
    //   2407: dup
    //   2408: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   2411: astore 16
    //   2413: aload 16
    //   2415: ldc_w 309
    //   2418: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2421: pop
    //   2422: aload 16
    //   2424: aload 15
    //   2426: invokevirtual 333	java/lang/InterruptedException:getMessage	()Ljava/lang/String;
    //   2429: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2432: pop
    //   2433: ldc_w 314
    //   2436: aload 16
    //   2438: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2441: invokestatic 318	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   2444: aload 20
    //   2446: astore 16
    //   2448: aload 19
    //   2450: astore 17
    //   2452: iload 10
    //   2454: istore 9
    //   2456: goto -2384 -> 72
    //   2459: aconst_null
    //   2460: areturn
    //   2461: astore_1
    //   2462: aload 18
    //   2464: astore 16
    //   2466: goto -551 -> 1915
    //   2469: aload 16
    //   2471: ifnull +16 -> 2487
    //   2474: aload 16
    //   2476: invokevirtual 258	java/io/InputStream:close	()V
    //   2479: goto +8 -> 2487
    //   2482: astore_2
    //   2483: aload_2
    //   2484: invokevirtual 283	java/io/IOException:printStackTrace	()V
    //   2487: aload 19
    //   2489: ifnull +8 -> 2497
    //   2492: aload 19
    //   2494: invokevirtual 286	java/net/HttpURLConnection:disconnect	()V
    //   2497: aload_1
    //   2498: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2499	0	this	c
    //   0	2499	1	paramContext	Context
    //   0	2499	2	paramString1	String
    //   0	2499	3	paramInt	int
    //   0	2499	4	paramLong	long
    //   0	2499	6	paramBoolean	boolean
    //   0	2499	7	paramFile	File
    //   0	2499	8	paramString2	String
    //   70	2385	9	i	int
    //   242	2211	10	j	int
    //   849	1016	11	k	int
    //   483	1400	12	m	int
    //   64	2328	13	n	int
    //   839	30	14	i1	int
    //   93	2249	15	localObject1	Object
    //   2402	23	15	localInterruptedException	InterruptedException
    //   61	2414	16	localObject2	Object
    //   67	2384	17	localObject3	Object
    //   58	1070	18	localObject4	Object
    //   1135	1	18	localException1	Exception
    //   1140	1	18	localException2	Exception
    //   1221	371	18	localObject5	Object
    //   1792	3	18	localIOException1	IOException
    //   1801	1	18	localObject6	Object
    //   1862	1	18	localException3	Exception
    //   1871	1	18	localAssertionError1	AssertionError
    //   1880	1	18	localSSLPeerUnverifiedException1	javax.net.ssl.SSLPeerUnverifiedException
    //   1889	1	18	localException4	Exception
    //   1894	1	18	localAssertionError2	AssertionError
    //   1899	1	18	localSSLPeerUnverifiedException2	javax.net.ssl.SSLPeerUnverifiedException
    //   1904	1	18	localAssertionError3	AssertionError
    //   1909	1	18	localSSLPeerUnverifiedException3	javax.net.ssl.SSLPeerUnverifiedException
    //   1918	7	18	localException5	Exception
    //   1934	63	18	localObject7	Object
    //   2022	3	18	localIOException2	IOException
    //   2031	1	18	localObject8	Object
    //   2053	7	18	localAssertionError4	AssertionError
    //   2069	63	18	localObject9	Object
    //   2157	3	18	localIOException3	IOException
    //   2166	105	18	localObject10	Object
    //   2296	3	18	localIOException4	IOException
    //   2305	158	18	localObject11	Object
    //   78	2097	19	localObject12	Object
    //   2188	7	19	localSSLPeerUnverifiedException4	javax.net.ssl.SSLPeerUnverifiedException
    //   2204	289	19	localObject13	Object
    //   82	1724	20	localObject14	Object
    //   1823	20	20	localException6	Exception
    //   1849	72	20	localSSLPeerUnverifiedException5	javax.net.ssl.SSLPeerUnverifiedException
    //   1926	519	20	localObject15	Object
    //   86	1744	21	localObject16	Object
    //   1836	1	21	localAssertionError5	AssertionError
    //   1939	337	21	localObject17	Object
    //   74	1782	22	localObject18	Object
    //   1209	643	23	localObject19	Object
    //   1193	633	24	localObject20	Object
    //   1201	638	25	localObject21	Object
    //   39	290	26	str1	String
    //   1189	418	27	localObject22	Object
    //   35	973	28	str2	String
    // Exception table:
    //   from	to	target	type
    //   489	497	1135	java/lang/Exception
    //   509	517	1135	java/lang/Exception
    //   529	537	1135	java/lang/Exception
    //   549	560	1135	java/lang/Exception
    //   572	580	1135	java/lang/Exception
    //   592	600	1135	java/lang/Exception
    //   612	623	1135	java/lang/Exception
    //   635	640	1135	java/lang/Exception
    //   652	657	1135	java/lang/Exception
    //   669	677	1135	java/lang/Exception
    //   689	697	1135	java/lang/Exception
    //   709	720	1135	java/lang/Exception
    //   732	740	1135	java/lang/Exception
    //   752	765	1135	java/lang/Exception
    //   777	782	1135	java/lang/Exception
    //   794	801	1135	java/lang/Exception
    //   813	820	1135	java/lang/Exception
    //   832	841	1135	java/lang/Exception
    //   308	313	1140	java/lang/Exception
    //   317	322	1140	java/lang/Exception
    //   326	334	1140	java/lang/Exception
    //   338	346	1140	java/lang/Exception
    //   350	358	1140	java/lang/Exception
    //   362	374	1140	java/lang/Exception
    //   378	385	1140	java/lang/Exception
    //   389	394	1140	java/lang/Exception
    //   398	405	1140	java/lang/Exception
    //   409	414	1140	java/lang/Exception
    //   418	423	1140	java/lang/Exception
    //   427	435	1140	java/lang/Exception
    //   439	447	1140	java/lang/Exception
    //   451	459	1140	java/lang/Exception
    //   463	468	1140	java/lang/Exception
    //   472	477	1140	java/lang/Exception
    //   1337	1342	1345	java/io/IOException
    //   1368	1373	1376	java/io/IOException
    //   1621	1626	1629	java/io/IOException
    //   1784	1789	1792	java/io/IOException
    //   1223	1229	1823	java/lang/Exception
    //   1269	1279	1823	java/lang/Exception
    //   1315	1332	1823	java/lang/Exception
    //   1433	1440	1823	java/lang/Exception
    //   1472	1479	1823	java/lang/Exception
    //   1511	1518	1823	java/lang/Exception
    //   1555	1561	1823	java/lang/Exception
    //   1593	1606	1823	java/lang/Exception
    //   1223	1229	1836	java/lang/AssertionError
    //   1269	1279	1836	java/lang/AssertionError
    //   1315	1332	1836	java/lang/AssertionError
    //   1433	1440	1836	java/lang/AssertionError
    //   1472	1479	1836	java/lang/AssertionError
    //   1511	1518	1836	java/lang/AssertionError
    //   1555	1561	1836	java/lang/AssertionError
    //   1593	1606	1836	java/lang/AssertionError
    //   1223	1229	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1269	1279	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1315	1332	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1433	1440	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1472	1479	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1511	1518	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1555	1561	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   1593	1606	1849	javax/net/ssl/SSLPeerUnverifiedException
    //   863	873	1862	java/lang/Exception
    //   892	897	1862	java/lang/Exception
    //   913	923	1862	java/lang/Exception
    //   939	944	1862	java/lang/Exception
    //   960	965	1862	java/lang/Exception
    //   981	989	1862	java/lang/Exception
    //   1005	1013	1862	java/lang/Exception
    //   1029	1037	1862	java/lang/Exception
    //   1053	1061	1862	java/lang/Exception
    //   1077	1090	1862	java/lang/Exception
    //   1106	1111	1862	java/lang/Exception
    //   1127	1132	1862	java/lang/Exception
    //   1161	1168	1862	java/lang/Exception
    //   1184	1191	1862	java/lang/Exception
    //   489	497	1871	java/lang/AssertionError
    //   509	517	1871	java/lang/AssertionError
    //   529	537	1871	java/lang/AssertionError
    //   549	560	1871	java/lang/AssertionError
    //   572	580	1871	java/lang/AssertionError
    //   592	600	1871	java/lang/AssertionError
    //   612	623	1871	java/lang/AssertionError
    //   635	640	1871	java/lang/AssertionError
    //   652	657	1871	java/lang/AssertionError
    //   669	677	1871	java/lang/AssertionError
    //   689	697	1871	java/lang/AssertionError
    //   709	720	1871	java/lang/AssertionError
    //   732	740	1871	java/lang/AssertionError
    //   752	765	1871	java/lang/AssertionError
    //   777	782	1871	java/lang/AssertionError
    //   794	801	1871	java/lang/AssertionError
    //   813	820	1871	java/lang/AssertionError
    //   832	841	1871	java/lang/AssertionError
    //   863	873	1871	java/lang/AssertionError
    //   892	897	1871	java/lang/AssertionError
    //   913	923	1871	java/lang/AssertionError
    //   939	944	1871	java/lang/AssertionError
    //   960	965	1871	java/lang/AssertionError
    //   981	989	1871	java/lang/AssertionError
    //   1005	1013	1871	java/lang/AssertionError
    //   1029	1037	1871	java/lang/AssertionError
    //   1053	1061	1871	java/lang/AssertionError
    //   1077	1090	1871	java/lang/AssertionError
    //   1106	1111	1871	java/lang/AssertionError
    //   1127	1132	1871	java/lang/AssertionError
    //   1161	1168	1871	java/lang/AssertionError
    //   1184	1191	1871	java/lang/AssertionError
    //   489	497	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   509	517	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   529	537	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   549	560	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   572	580	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   592	600	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   612	623	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   635	640	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   652	657	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   669	677	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   689	697	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   709	720	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   732	740	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   752	765	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   777	782	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   794	801	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   813	820	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   832	841	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   863	873	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   892	897	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   913	923	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   939	944	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   960	965	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   981	989	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1005	1013	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1029	1037	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1053	1061	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1077	1090	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1106	1111	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1127	1132	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1161	1168	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   1184	1191	1880	javax/net/ssl/SSLPeerUnverifiedException
    //   292	299	1889	java/lang/Exception
    //   292	299	1894	java/lang/AssertionError
    //   308	313	1894	java/lang/AssertionError
    //   317	322	1894	java/lang/AssertionError
    //   326	334	1894	java/lang/AssertionError
    //   338	346	1894	java/lang/AssertionError
    //   350	358	1894	java/lang/AssertionError
    //   362	374	1894	java/lang/AssertionError
    //   378	385	1894	java/lang/AssertionError
    //   389	394	1894	java/lang/AssertionError
    //   398	405	1894	java/lang/AssertionError
    //   409	414	1894	java/lang/AssertionError
    //   418	423	1894	java/lang/AssertionError
    //   427	435	1894	java/lang/AssertionError
    //   439	447	1894	java/lang/AssertionError
    //   451	459	1894	java/lang/AssertionError
    //   463	468	1894	java/lang/AssertionError
    //   472	477	1894	java/lang/AssertionError
    //   292	299	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   308	313	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   317	322	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   326	334	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   338	346	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   350	358	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   362	374	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   378	385	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   389	394	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   398	405	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   409	414	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   418	423	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   427	435	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   439	447	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   451	459	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   463	468	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   472	477	1899	javax/net/ssl/SSLPeerUnverifiedException
    //   103	111	1904	java/lang/AssertionError
    //   119	127	1904	java/lang/AssertionError
    //   135	141	1904	java/lang/AssertionError
    //   149	155	1904	java/lang/AssertionError
    //   163	169	1904	java/lang/AssertionError
    //   177	184	1904	java/lang/AssertionError
    //   192	201	1904	java/lang/AssertionError
    //   209	218	1904	java/lang/AssertionError
    //   226	244	1904	java/lang/AssertionError
    //   103	111	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   119	127	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   135	141	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   149	155	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   163	169	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   177	184	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   192	201	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   209	218	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   226	244	1909	javax/net/ssl/SSLPeerUnverifiedException
    //   88	95	1914	finally
    //   103	111	1914	finally
    //   119	127	1914	finally
    //   135	141	1914	finally
    //   149	155	1914	finally
    //   163	169	1914	finally
    //   177	184	1914	finally
    //   192	201	1914	finally
    //   209	218	1914	finally
    //   226	244	1914	finally
    //   265	288	1914	finally
    //   292	299	1914	finally
    //   308	313	1914	finally
    //   317	322	1914	finally
    //   326	334	1914	finally
    //   338	346	1914	finally
    //   350	358	1914	finally
    //   362	374	1914	finally
    //   378	385	1914	finally
    //   389	394	1914	finally
    //   398	405	1914	finally
    //   409	414	1914	finally
    //   418	423	1914	finally
    //   427	435	1914	finally
    //   439	447	1914	finally
    //   451	459	1914	finally
    //   463	468	1914	finally
    //   472	477	1914	finally
    //   489	497	1914	finally
    //   509	517	1914	finally
    //   529	537	1914	finally
    //   549	560	1914	finally
    //   572	580	1914	finally
    //   592	600	1914	finally
    //   612	623	1914	finally
    //   635	640	1914	finally
    //   652	657	1914	finally
    //   669	677	1914	finally
    //   689	697	1914	finally
    //   709	720	1914	finally
    //   732	740	1914	finally
    //   752	765	1914	finally
    //   777	782	1914	finally
    //   794	801	1914	finally
    //   813	820	1914	finally
    //   832	841	1914	finally
    //   863	873	1914	finally
    //   892	897	1914	finally
    //   913	923	1914	finally
    //   939	944	1914	finally
    //   960	965	1914	finally
    //   981	989	1914	finally
    //   1005	1013	1914	finally
    //   1029	1037	1914	finally
    //   1053	1061	1914	finally
    //   1077	1090	1914	finally
    //   1106	1111	1914	finally
    //   1127	1132	1914	finally
    //   1161	1168	1914	finally
    //   1184	1191	1914	finally
    //   88	95	1918	java/lang/Exception
    //   103	111	1918	java/lang/Exception
    //   119	127	1918	java/lang/Exception
    //   135	141	1918	java/lang/Exception
    //   149	155	1918	java/lang/Exception
    //   163	169	1918	java/lang/Exception
    //   177	184	1918	java/lang/Exception
    //   192	201	1918	java/lang/Exception
    //   209	218	1918	java/lang/Exception
    //   226	244	1918	java/lang/Exception
    //   265	288	1918	java/lang/Exception
    //   2014	2019	2022	java/io/IOException
    //   88	95	2053	java/lang/AssertionError
    //   265	288	2053	java/lang/AssertionError
    //   2149	2154	2157	java/io/IOException
    //   88	95	2188	javax/net/ssl/SSLPeerUnverifiedException
    //   265	288	2188	javax/net/ssl/SSLPeerUnverifiedException
    //   2288	2293	2296	java/io/IOException
    //   2394	2399	2402	java/lang/InterruptedException
    //   1223	1229	2461	finally
    //   1269	1279	2461	finally
    //   1315	1332	2461	finally
    //   1433	1440	2461	finally
    //   1472	1479	2461	finally
    //   1511	1518	2461	finally
    //   1555	1561	2461	finally
    //   1593	1606	2461	finally
    //   1936	1941	2461	finally
    //   1949	1954	2461	finally
    //   1962	1971	2461	finally
    //   1979	1990	2461	finally
    //   1998	2009	2461	finally
    //   2071	2076	2461	finally
    //   2084	2089	2461	finally
    //   2097	2106	2461	finally
    //   2114	2125	2461	finally
    //   2133	2144	2461	finally
    //   2210	2215	2461	finally
    //   2223	2228	2461	finally
    //   2236	2245	2461	finally
    //   2253	2264	2461	finally
    //   2272	2283	2461	finally
    //   2474	2479	2482	java/io/IOException
  }
  
  private String a(int paramInt)
  {
    this.g = cn.jiguang.o.b.a(this.e, paramInt);
    return this.g;
  }
  
  public static String a(Context paramContext)
  {
    if (paramContext == null) {
      return "";
    }
    String str1 = d.e(paramContext, "");
    String str2 = b.c(paramContext, "");
    paramContext = d.d(paramContext, "");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str1);
    localStringBuilder.append(str2);
    localStringBuilder.append(paramContext);
    return d.c(localStringBuilder.toString());
  }
  
  private String a(String paramString, cn.jiguang.q.a parama)
  {
    if (a(this.e, parama)) {
      return d(paramString);
    }
    return null;
  }
  
  private String a(TreeMap<String, String> paramTreeMap)
  {
    if ((paramTreeMap != null) && (paramTreeMap.size() != 0))
    {
      Object localObject = paramTreeMap.entrySet().iterator();
      paramTreeMap = new StringBuilder();
      while (((Iterator)localObject).hasNext()) {
        paramTreeMap.append(((Map.Entry)((Iterator)localObject).next()).getValue());
      }
      localObject = cn.jiguang.o.b.c(this.e);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramTreeMap.toString());
      localStringBuilder.append((String)localObject);
      return d.c(localStringBuilder.toString()).toUpperCase();
    }
    return null;
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    h = new CookieManager();
    h.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    CookieHandler.setDefault(h);
    this.e = paramContext;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
  }
  
  private void a(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("Set-Cookie");
    if (paramMap != null)
    {
      paramMap = paramMap.iterator();
      while (paramMap.hasNext())
      {
        String str = (String)paramMap.next();
        h.getCookieStore().add(null, (HttpCookie)HttpCookie.parse(str).get(0));
      }
    }
  }
  
  private boolean a(Context paramContext, cn.jiguang.q.a parama)
  {
    if (paramContext == null) {
      return false;
    }
    if (parama == null) {
      return false;
    }
    File localFile = d.f(paramContext, "resp.raw");
    if (localFile == null) {
      return false;
    }
    StringBuilder localStringBuilder = new StringBuilder("");
    if ((parama.c != null) && (parama.c.size() > 0))
    {
      Iterator localIterator = parama.c.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (Map.Entry)localIterator.next();
        if (((Map.Entry)localObject).getKey() != null)
        {
          localStringBuilder.append((String)((Map.Entry)localObject).getKey());
          localStringBuilder.append(": ");
        }
        localObject = ((List)((Map.Entry)localObject).getValue()).iterator();
        if (((Iterator)localObject).hasNext()) {
          for (;;)
          {
            localStringBuilder.append((String)((Iterator)localObject).next());
            if (!((Iterator)localObject).hasNext()) {
              break;
            }
            localStringBuilder.append(", ");
          }
        }
        localStringBuilder.append("\n");
      }
    }
    localStringBuilder.append("\r\n\r\n");
    if (!TextUtils.isEmpty(parama.b)) {
      localStringBuilder.append(parama.b);
    }
    if (!d.a(localFile, localStringBuilder.toString())) {
      return false;
    }
    parama = new ArrayList();
    parama.add(localFile);
    paramContext = d.f(paramContext, "resp.zip");
    if (paramContext == null) {
      return false;
    }
    cn.jiguang.s.b.a(paramContext);
    try
    {
      cn.jiguang.s.b.a(parama, paramContext);
      return true;
    }
    catch (IOException paramContext)
    {
      parama = new StringBuilder();
      parama.append("report phoneNumber exception:");
      parama.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JDevicePNWorker", parama.toString());
    }
    return false;
  }
  
  /* Error */
  private boolean a(String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: new 355	java/util/TreeMap
    //   3: dup
    //   4: invokespecial 479	java/util/TreeMap:<init>	()V
    //   7: astore 4
    //   9: aload_1
    //   10: invokestatic 55	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   13: ifne +13 -> 26
    //   16: aload 4
    //   18: ldc_w 481
    //   21: aload_1
    //   22: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   25: pop
    //   26: aload_2
    //   27: invokestatic 55	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   30: ifne +13 -> 43
    //   33: aload 4
    //   35: ldc_w 487
    //   38: aload_2
    //   39: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   42: pop
    //   43: aload_3
    //   44: invokestatic 55	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   47: ifne +13 -> 60
    //   50: aload 4
    //   52: ldc_w 489
    //   55: aload_3
    //   56: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   59: pop
    //   60: aload 4
    //   62: ldc_w 491
    //   65: aload_0
    //   66: getfield 336	cn/jiguang/r/c:e	Landroid/content/Context;
    //   69: invokestatic 493	cn/jiguang/o/b:a	(Landroid/content/Context;)Ljava/lang/String;
    //   72: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   75: pop
    //   76: aload 4
    //   78: ldc_w 495
    //   81: aload_0
    //   82: getfield 336	cn/jiguang/r/c:e	Landroid/content/Context;
    //   85: invokestatic 497	cn/jiguang/o/b:b	(Landroid/content/Context;)Ljava/lang/String;
    //   88: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   91: pop
    //   92: aload 4
    //   94: ldc_w 499
    //   97: invokestatic 503	cn/jiguang/o/a:a	()Ljava/lang/String;
    //   100: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   103: pop
    //   104: aload 4
    //   106: ldc_w 505
    //   109: aload_0
    //   110: aload 4
    //   112: invokespecial 507	cn/jiguang/r/c:a	(Ljava/util/TreeMap;)Ljava/lang/String;
    //   115: invokevirtual 485	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   118: pop
    //   119: ldc_w 298
    //   122: astore_1
    //   123: aload 4
    //   125: invokevirtual 360	java/util/TreeMap:entrySet	()Ljava/util/Set;
    //   128: invokeinterface 366 1 0
    //   133: astore_3
    //   134: aload_1
    //   135: astore_2
    //   136: aload_3
    //   137: invokeinterface 372 1 0
    //   142: ifeq +89 -> 231
    //   145: aload_3
    //   146: invokeinterface 376 1 0
    //   151: checkcast 378	java/util/Map$Entry
    //   154: astore_1
    //   155: new 190	java/lang/StringBuilder
    //   158: astore 4
    //   160: aload 4
    //   162: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   165: aload 4
    //   167: aload_2
    //   168: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload 4
    //   174: ldc_w 509
    //   177: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: aload 4
    //   183: aload_1
    //   184: invokeinterface 445 1 0
    //   189: invokevirtual 384	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload 4
    //   195: ldc_w 511
    //   198: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   201: pop
    //   202: aload 4
    //   204: aload_1
    //   205: invokeinterface 381 1 0
    //   210: checkcast 59	java/lang/String
    //   213: ldc -106
    //   215: invokestatic 517	java/net/URLEncoder:encode	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   218: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload 4
    //   224: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   227: astore_1
    //   228: goto -94 -> 134
    //   231: aload_0
    //   232: getfield 336	cn/jiguang/r/c:e	Landroid/content/Context;
    //   235: astore_3
    //   236: new 190	java/lang/StringBuilder
    //   239: astore_1
    //   240: aload_1
    //   241: invokespecial 191	java/lang/StringBuilder:<init>	()V
    //   244: aload_1
    //   245: aload_0
    //   246: getfield 343	cn/jiguang/r/c:g	Ljava/lang/String;
    //   249: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: aload_1
    //   254: ldc_w 519
    //   257: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   260: pop
    //   261: aload_1
    //   262: aload_2
    //   263: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload_0
    //   268: aload_3
    //   269: aload_1
    //   270: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   273: bipush 10
    //   275: ldc2_w 520
    //   278: iconst_0
    //   279: aconst_null
    //   280: aconst_null
    //   281: invokespecial 280	cn/jiguang/r/c:a	(Landroid/content/Context;Ljava/lang/String;IJZLjava/io/File;Ljava/lang/String;)Lcn/jiguang/q/a;
    //   284: astore 5
    //   286: aload 5
    //   288: getfield 523	cn/jiguang/q/a:a	I
    //   291: sipush 200
    //   294: if_icmpeq +5 -> 299
    //   297: iconst_0
    //   298: ireturn
    //   299: aload_0
    //   300: aload 5
    //   302: getfield 454	cn/jiguang/q/a:b	Ljava/lang/String;
    //   305: invokespecial 526	cn/jiguang/r/c:c	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   308: astore 4
    //   310: aconst_null
    //   311: astore_1
    //   312: aconst_null
    //   313: astore_3
    //   314: aload 4
    //   316: ifnull +36 -> 352
    //   319: aload 4
    //   321: ldc_w 528
    //   324: iconst_m1
    //   325: invokevirtual 534	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   328: sipush 200
    //   331: if_icmpeq +5 -> 336
    //   334: iconst_0
    //   335: ireturn
    //   336: aload_0
    //   337: aload 4
    //   339: ldc_w 536
    //   342: invokevirtual 539	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   345: invokespecial 541	cn/jiguang/r/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   348: astore_1
    //   349: goto +68 -> 417
    //   352: aload 5
    //   354: getfield 440	cn/jiguang/q/a:c	Ljava/util/Map;
    //   357: ifnonnull +14 -> 371
    //   360: aload 5
    //   362: getfield 454	cn/jiguang/q/a:b	Ljava/lang/String;
    //   365: invokestatic 55	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   368: ifne +49 -> 417
    //   371: getstatic 24	cn/jiguang/r/c:a	Ljava/lang/Object;
    //   374: astore 4
    //   376: aload 4
    //   378: monitorenter
    //   379: aload_0
    //   380: iconst_0
    //   381: putfield 28	cn/jiguang/r/c:f	I
    //   384: aload_0
    //   385: aload_2
    //   386: aload 5
    //   388: invokespecial 543	cn/jiguang/r/c:a	(Ljava/lang/String;Lcn/jiguang/q/a;)Ljava/lang/String;
    //   391: astore_1
    //   392: aload_0
    //   393: getfield 336	cn/jiguang/r/c:e	Landroid/content/Context;
    //   396: ldc_w 432
    //   399: invokevirtual 548	android/content/Context:deleteFile	(Ljava/lang/String;)Z
    //   402: pop
    //   403: aload_0
    //   404: getfield 336	cn/jiguang/r/c:e	Landroid/content/Context;
    //   407: ldc_w 465
    //   410: invokevirtual 548	android/content/Context:deleteFile	(Ljava/lang/String;)Z
    //   413: pop
    //   414: aload 4
    //   416: monitorexit
    //   417: aload_1
    //   418: invokestatic 55	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   421: ifne +10 -> 431
    //   424: aload_0
    //   425: aload_1
    //   426: invokespecial 550	cn/jiguang/r/c:e	(Ljava/lang/String;)V
    //   429: iconst_1
    //   430: ireturn
    //   431: iconst_0
    //   432: ireturn
    //   433: astore_1
    //   434: aload 4
    //   436: monitorexit
    //   437: aload_1
    //   438: athrow
    //   439: astore_1
    //   440: iconst_0
    //   441: ireturn
    //   442: astore_1
    //   443: aload_2
    //   444: astore_1
    //   445: goto -311 -> 134
    //   448: astore_1
    //   449: aload_3
    //   450: astore_1
    //   451: goto -59 -> 392
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	454	0	this	c
    //   0	454	1	paramString1	String
    //   0	454	2	paramString2	String
    //   0	454	3	paramString3	String
    //   284	103	5	locala	cn.jiguang.q.a
    // Exception table:
    //   from	to	target	type
    //   379	384	433	finally
    //   384	392	433	finally
    //   392	417	433	finally
    //   434	437	433	finally
    //   231	286	439	java/lang/Throwable
    //   155	228	442	java/io/UnsupportedEncodingException
    //   384	392	448	java/lang/Throwable
  }
  
  private String b(String paramString)
  {
    String str = f(paramString);
    if (TextUtils.isEmpty(str)) {
      return null;
    }
    paramString = str;
    if (!Patterns.PHONE.matcher(str).matches()) {
      paramString = null;
    }
    return paramString;
  }
  
  private JSONObject c(String paramString)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>(paramString);
      }
    }
    catch (Exception paramString)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("report phoneNumber exception:");
      ((StringBuilder)localObject1).append(paramString.getMessage());
      cn.jiguang.ai.a.g("JDevicePNWorker", ((StringBuilder)localObject1).toString());
      localObject1 = localObject2;
    }
    return (JSONObject)localObject1;
  }
  
  private String d(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    Object localObject2 = null;
    if (bool) {
      return null;
    }
    try
    {
      Object localObject3 = this.e;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(this.g);
      ((StringBuilder)localObject1).append("statistic/query?");
      ((StringBuilder)localObject1).append(paramString);
      localObject3 = a((Context)localObject3, ((StringBuilder)localObject1).toString(), 10, 30000L, false, d.f(this.e, "resp.zip"), "resp_data");
      if (((cn.jiguang.q.a)localObject3).a != 200) {
        return null;
      }
      localObject1 = c(((cn.jiguang.q.a)localObject3).b);
      if (localObject1 != null)
      {
        if (((JSONObject)localObject1).optInt("code", -1) != 200) {
          return null;
        }
        localObject1 = b(((JSONObject)localObject1).optString("num"));
      }
      else if (((cn.jiguang.q.a)localObject3).c == null)
      {
        localObject1 = localObject2;
        if (TextUtils.isEmpty(((cn.jiguang.q.a)localObject3).b)) {}
      }
      else
      {
        int i = this.f;
        if (i > 4) {
          return null;
        }
        this.f = (i + 1);
        try
        {
          localObject1 = a(paramString, (cn.jiguang.q.a)localObject3);
        }
        catch (Throwable paramString)
        {
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("report phoneNumber throwable:");
          ((StringBuilder)localObject1).append(paramString.getMessage());
          cn.jiguang.ai.a.g("JDevicePNWorker", ((StringBuilder)localObject1).toString());
          localObject1 = localObject2;
        }
      }
      return (String)localObject1;
    }
    catch (Throwable paramString)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("report phoneNumber throwable:");
      ((StringBuilder)localObject1).append(paramString.getMessage());
      cn.jiguang.ai.a.g("JDevicePNWorker", ((StringBuilder)localObject1).toString());
    }
    return null;
  }
  
  private void e(String paramString)
  {
    cn.jiguang.o.b.d(this.e, paramString);
    Object localObject2;
    try
    {
      Object localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      ((JSONObject)localObject1).put("num", paramString);
      if (!TextUtils.isEmpty(this.b)) {
        ((JSONObject)localObject1).put("imei", this.b);
      }
      if (!TextUtils.isEmpty(this.d)) {
        ((JSONObject)localObject1).put("imsi", this.d);
      }
      if (!TextUtils.isEmpty(this.c)) {
        ((JSONObject)localObject1).put("iccid", this.c);
      }
      paramString = ((JSONObject)localObject1).toString();
      try
      {
        localObject1 = d.g(paramString);
        paramString = (String)localObject1;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
      if (TextUtils.isEmpty(paramString)) {
        return;
      }
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      d.a(this.e, (JSONObject)localObject2, "nb");
      ((JSONObject)localObject2).put("content", paramString);
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      paramString.append("collect success:");
      paramString.append(localObject2);
      cn.jiguang.ai.a.c("JDevicePNWorker", paramString.toString());
      d.a(this.e, localObject2);
      cn.jiguang.o.b.a(this.e, false);
    }
    catch (Throwable paramString)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("report phoneNumber throwable:");
      paramString = paramString.getMessage();
    }
    catch (JSONException paramString)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("report phoneNumber exception:");
      paramString = paramString.getMessage();
    }
    ((StringBuilder)localObject2).append(paramString);
    cn.jiguang.ai.a.g("JDevicePNWorker", ((StringBuilder)localObject2).toString());
  }
  
  private String f(String paramString)
  {
    try
    {
      paramString = d.b(paramString, cn.jiguang.o.b.c(this.e).substring(0, 16));
      return paramString;
    }
    catch (Exception localException)
    {
      paramString = new StringBuilder();
      paramString.append("report phoneNumber exception:");
      paramString.append(localException.getMessage());
      cn.jiguang.ai.a.g("JDevicePNWorker", paramString.toString());
    }
    return null;
  }
  
  public void a()
  {
    try
    {
      if ((TextUtils.isEmpty(this.b)) && (TextUtils.isEmpty(this.c)) && (TextUtils.isEmpty(this.d))) {
        return;
      }
      localObject = cn.jiguang.o.b.d(this.e);
      if ((!TextUtils.isEmpty((CharSequence)localObject)) && (!cn.jiguang.o.b.e(this.e)))
      {
        e((String)localObject);
      }
      else
      {
        cn.jiguang.o.b.f(this.e);
        int i = a(this.d);
        if (i != -1)
        {
          a(i);
          if (!TextUtils.isEmpty(this.g)) {
            a(this.b, this.c, this.d);
          }
        }
        else
        {
          i = 0;
          localObject = "";
          boolean bool;
          do
          {
            int j;
            for (;;)
            {
              if (i >= 3) {
                return;
              }
              a(i);
              j = i + 1;
              i = j;
              if (!TextUtils.isEmpty(this.g))
              {
                if (!((String)localObject).equals(this.g)) {
                  break;
                }
                i = j;
              }
            }
            localObject = this.g;
            bool = a(this.b, this.c, this.d);
            i = j;
          } while (!bool);
        }
      }
    }
    catch (Exception localException)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("report phoneNumber exception:");
      ((StringBuilder)localObject).append(localException.getMessage());
      cn.jiguang.ai.a.g("JDevicePNWorker", ((StringBuilder)localObject).toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/r/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */