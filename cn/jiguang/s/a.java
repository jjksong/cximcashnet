package cn.jiguang.s;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import java.nio.ByteBuffer;
import org.json.JSONObject;

public class a
{
  private static long a;
  
  static
  {
    cn.jiguang.f.d.a("JCommon");
    cn.jiguang.f.d.a("JLocation");
    cn.jiguang.f.d.a("JArp");
  }
  
  private static int a(Object paramObject)
  {
    int i;
    if (((paramObject instanceof Bundle)) && (((Bundle)paramObject).getBoolean("login"))) {
      i = 2;
    } else {
      i = 0;
    }
    return i;
  }
  
  public static Object a(Context paramContext, String paramString, Object paramObject)
  {
    if (paramContext == null) {}
    try
    {
      cn.jiguang.ai.a.g("JCommonActionHelper", "context is null,give up continue");
      return null;
    }
    catch (Throwable paramString)
    {
      int i;
      int k;
      int j;
      Bundle localBundle;
      paramContext = new StringBuilder();
      paramContext.append("onEvent throwable:");
      paramContext.append(paramString.getMessage());
      cn.jiguang.ai.a.g("JCommonActionHelper", paramContext.toString());
    }
    if (TextUtils.isEmpty(paramString))
    {
      cn.jiguang.ai.a.g("JCommonActionHelper", "action is null,give up continue");
      return null;
    }
    i = paramString.hashCode();
    k = 6;
    j = 0;
    switch (i)
    {
    default: 
      break;
    case 1947980230: 
      if (paramString.equals("service_create")) {
        i = 6;
      }
      break;
    case 1660517635: 
      if (paramString.equals("on_register")) {
        i = 5;
      }
      break;
    case 1567409176: 
      if (paramString.equals("filter_pkg_list")) {
        i = 3;
      }
      break;
    case 805881853: 
      if (paramString.equals("getwakeenable")) {
        i = 2;
      }
      break;
    case 781805572: 
      if (paramString.equals("deviceinfo")) {
        i = 4;
      }
      break;
    case 385278662: 
      if (paramString.equals("periodtask")) {
        i = 0;
      }
      break;
    case -24181482: 
      if (paramString.equals("get_loc_info")) {
        i = 1;
      }
      break;
    }
    i = -1;
    switch (i)
    {
    default: 
      break;
    case 6: 
      cn.jiguang.y.a.d().f(paramContext);
      break;
    case 5: 
      cn.jiguang.y.a.d().g(paramContext);
      break;
    case 4: 
      return cn.jiguang.n.a.d().e(paramContext);
    case 3: 
      return cn.jiguang.y.a.d().a(paramContext, paramObject);
    case 2: 
      return cn.jiguang.y.a.d().e(paramContext);
    case 1: 
      return cn.jiguang.u.a.d().f(paramContext);
    case 0: 
      a(paramContext, paramObject);
    }
    if ((paramObject instanceof Bundle)) {
      localBundle = (Bundle)paramObject;
    } else {
      localBundle = null;
    }
    if (localBundle != null)
    {
      switch (paramString.hashCode())
      {
      default: 
        break;
      case 2063320150: 
        if (paramString.equals("set_sdktype_info")) {
          i = k;
        }
        break;
      case 1391522972: 
        if (paramString.equals("lbsforenry")) {
          i = 2;
        }
        break;
      case 112894784: 
        if (paramString.equals("waked")) {
          i = 5;
        }
        break;
      case 98618: 
        if (paramString.equals("cmd")) {
          i = 4;
        }
        break;
      case -691876648: 
        if (paramString.equals("set_ctrl_url")) {
          i = 3;
        }
        break;
      case -955005568: 
        if (paramString.equals("lbsenable")) {
          i = 1;
        }
        break;
      case -955011762: 
        if (paramString.equals("init_local_ctrl")) {
          i = 0;
        }
        break;
      case -1485564867: 
        if (paramString.equals("notification_state")) {
          i = 7;
        }
        break;
      }
      i = -1;
      switch (i)
      {
      default: 
        break;
      case 7: 
        cn.jiguang.w.a.a(paramContext, localBundle.getInt("scence"));
        break;
      case 6: 
        cn.jiguang.x.a.d().a(paramContext, localBundle);
        break;
      case 5: 
        localBundle.getString("from_package");
        i = localBundle.getInt("type");
        cn.jiguang.y.a.d().a(paramContext, localBundle, i);
        break;
      case 4: 
        a(paramContext, localBundle);
        break;
      case 3: 
        cn.jiguang.aa.b.a = localBundle.getString("test_wake_controll_url");
        break;
      case 1: 
        cn.jiguang.f.b.b(paramContext, "JLocation", localBundle.getBoolean("enable"));
        break;
      case 0: 
        cn.jiguang.f.c.a.b = localBundle.getBoolean("google");
        cn.jiguang.f.c.a.c = localBundle.getBoolean("internal_use");
      }
    }
    if ((paramObject instanceof Intent)) {
      paramObject = (Intent)paramObject;
    } else {
      paramObject = null;
    }
    if (paramObject != null)
    {
      if ((paramString.hashCode() == 1563192504) && (paramString.equals("get_receiver"))) {
        i = j;
      } else {
        i = -1;
      }
      if (i == 0) {
        cn.jiguang.h.b.d().a(paramContext, (Intent)paramObject);
      }
    }
    return null;
  }
  
  private static JSONObject a(Bundle paramBundle)
  {
    try
    {
      localObject = ByteBuffer.wrap(paramBundle.getByteArray("RESPONSE_BODY"));
      a = ((ByteBuffer)localObject).getLong();
      paramBundle = new byte[((ByteBuffer)localObject).getShort()];
      ((ByteBuffer)localObject).get(paramBundle);
      localObject = new java/lang/String;
      ((String)localObject).<init>(paramBundle, "UTF-8");
      paramBundle = new java/lang/StringBuilder;
      paramBundle.<init>();
      paramBundle.append("parseBundle2Json content: ");
      paramBundle.append((String)localObject);
      cn.jiguang.ai.a.c("JCommonActionHelper", paramBundle.toString());
      paramBundle = new JSONObject((String)localObject);
      return paramBundle;
    }
    catch (Exception paramBundle)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("parseBundle2Json exception:");
      ((StringBuilder)localObject).append(paramBundle.getMessage());
      cn.jiguang.ai.a.g("JCommonActionHelper", ((StringBuilder)localObject).toString());
    }
    return null;
  }
  
  private static void a(Context paramContext, int paramInt)
  {
    long l = c.a();
    String str = cn.jiguang.r.a.a(paramContext, paramInt);
    Object localObject = c.a(a, str);
    Bundle localBundle = new Bundle();
    localBundle.putInt("cmd", 25);
    localBundle.putInt("ver", 1);
    localBundle.putLong("rid", l);
    localBundle.putLong("timeout", 0L);
    localBundle.putByteArray("body", (byte[])localObject);
    if (!TextUtils.isEmpty(str))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("tcp report deviceInfo:");
      ((StringBuilder)localObject).append(str);
      cn.jiguang.ai.a.c("JCommonActionHelper", ((StringBuilder)localObject).toString());
    }
    cn.jiguang.f.d.a(paramContext, localBundle);
  }
  
  private static void a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = a(paramBundle);
    if (paramBundle == null) {
      return;
    }
    int i = paramBundle.optInt("cmd");
    a(paramContext, i);
    switch (i)
    {
    default: 
      break;
    case 55: 
      cn.jiguang.f.d.a(paramContext, paramBundle);
      break;
    case 54: 
      cn.jiguang.h.d.d().a(paramContext, paramBundle);
      break;
    case 52: 
      cn.jiguang.k.a.d().b(paramContext, paramBundle);
      break;
    case 51: 
      cn.jiguang.n.c.d().a(paramContext, paramBundle);
      break;
    case 50: 
      cn.jiguang.y.a.d().c(paramContext, paramBundle);
      break;
    case 44: 
      cn.jiguang.h.d.d().b(paramContext);
      break;
    case 9: 
      cn.jiguang.h.c.d().b(paramContext);
      break;
    case 5: 
    case 45: 
      if (cn.jiguang.sdk.impl.b.q(paramContext)) {
        cn.jiguang.u.a.d().b(paramContext, paramBundle);
      }
      break;
    case 4: 
      cn.jiguang.h.a.d().b(paramContext);
    }
  }
  
  private static void a(Context paramContext, Object paramObject)
  {
    cn.jiguang.k.a.d().c(paramContext);
    if (cn.jiguang.sdk.impl.b.q(paramContext)) {
      cn.jiguang.u.a.d().c(paramContext);
    }
    cn.jiguang.w.a.a(paramContext, a(paramObject));
    cn.jiguang.h.a.d().a(paramContext);
    cn.jiguang.h.b.d().a(paramContext);
    cn.jiguang.h.d.d().a(paramContext);
    cn.jiguang.n.a.d().a(paramContext);
    cn.jiguang.n.b.d().a(paramContext);
    cn.jiguang.y.a.d().a(paramContext);
    cn.jiguang.y.a.d().c(paramContext, null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/s/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */