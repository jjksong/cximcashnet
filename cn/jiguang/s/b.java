package cn.jiguang.s;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ai.a;
import cn.jiguang.api.JCoreManager;
import cn.jiguang.f.d;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.json.JSONObject;

public class b
{
  public static JSONObject a(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {
      try
      {
        try
        {
          paramContext = JCoreManager.onEvent(paramContext, "JCOMMON", 42, null, null, new Object[] { paramString });
          if ((paramContext instanceof JSONObject))
          {
            paramContext = (JSONObject)paramContext;
            return paramContext;
          }
          return null;
        }
        finally {}
        return null;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("readJson throwable:");
        paramString.append(paramContext.getMessage());
        a.g("JCommonFileHelper", paramString.toString());
      }
    }
  }
  
  public static void a(File paramFile)
  {
    if (paramFile != null) {
      try
      {
        if (paramFile.exists()) {
          paramFile.delete();
        }
      }
      catch (Throwable localThrowable)
      {
        paramFile = new StringBuilder();
        paramFile.append("delete throwable:");
        paramFile.append(localThrowable.getMessage());
        a.g("JCommonFileHelper", paramFile.toString());
      }
    }
  }
  
  private static void a(File paramFile, ZipOutputStream paramZipOutputStream, String paramString)
  {
    try
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(paramString);
      if (paramString.trim().length() == 0) {
        paramString = "";
      } else {
        paramString = File.separator;
      }
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append(paramFile.getName());
      localObject = ((StringBuilder)localObject).toString();
      paramString = new java/lang/String;
      paramString.<init>(((String)localObject).getBytes("8859_1"), "GB2312");
      boolean bool = paramFile.isDirectory();
      int i = 0;
      if (bool)
      {
        paramFile = paramFile.listFiles();
        if (paramFile != null)
        {
          int j = paramFile.length;
          while (i < j)
          {
            a(paramFile[i], paramZipOutputStream, paramString);
            i++;
          }
        }
      }
      else
      {
        byte[] arrayOfByte = new byte[1048576];
        localObject = new java/io/BufferedInputStream;
        FileInputStream localFileInputStream = new java/io/FileInputStream;
        localFileInputStream.<init>(paramFile);
        ((BufferedInputStream)localObject).<init>(localFileInputStream, 1048576);
        paramFile = new java/util/zip/ZipEntry;
        paramFile.<init>(paramString);
        paramZipOutputStream.putNextEntry(paramFile);
        for (;;)
        {
          i = ((BufferedInputStream)localObject).read(arrayOfByte);
          if (i == -1) {
            break;
          }
          paramZipOutputStream.write(arrayOfByte, 0, i);
        }
        ((BufferedInputStream)localObject).close();
        paramZipOutputStream.flush();
        paramZipOutputStream.closeEntry();
      }
    }
    catch (Throwable paramZipOutputStream)
    {
      paramFile = new StringBuilder();
      paramFile.append("zipFile throwable:");
      paramFile.append(paramZipOutputStream.getMessage());
      a.g("JCommonFileHelper", paramFile.toString());
    }
  }
  
  public static void a(Collection<File> paramCollection, File paramFile)
  {
    paramFile = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(paramFile), 1048576));
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      a((File)paramCollection.next(), paramFile, "");
    }
    paramFile.close();
  }
  
  public static boolean a(Context paramContext, String paramString1, String paramString2)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString1))) {
      try
      {
        try
        {
          paramContext = d.f(paramContext, paramString1);
          if (paramContext == null) {
            return false;
          }
          boolean bool = d.a(paramContext, paramString2);
          return bool;
        }
        finally {}
        return false;
      }
      catch (Throwable paramString1)
      {
        paramContext = new StringBuilder();
        paramContext.append("writeString throwable:");
        paramContext.append(paramString1.getMessage());
        a.g("JCommonFileHelper", paramContext.toString());
      }
    }
  }
  
  public static boolean a(Context paramContext, String paramString, JSONObject paramJSONObject)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)) && (paramJSONObject != null)) {
      try
      {
        try
        {
          paramContext = JCoreManager.onEvent(paramContext, "JCOMMON", 41, null, null, new Object[] { paramString, paramJSONObject });
          boolean bool;
          if ((paramContext instanceof Boolean)) {
            bool = ((Boolean)paramContext).booleanValue();
          } else {
            bool = false;
          }
          return bool;
        }
        finally {}
        return false;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("writeJson throwable:");
        paramString.append(paramContext.getMessage());
        a.g("JCommonFileHelper", paramString.toString());
      }
    }
  }
  
  public static void b(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {
      try
      {
        if (!a(paramContext, paramString, null)) {
          try
          {
            paramContext.deleteFile(paramString);
          }
          finally {}
        }
        return;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("cleanJson throwable:");
        paramString.append(paramContext.getMessage());
        a.g("JCommonFileHelper", paramString.toString());
      }
    }
  }
  
  public static String c(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {
      try
      {
        try
        {
          paramContext = d.f(paramContext, paramString);
          if (paramContext == null) {
            return null;
          }
          paramContext = d.a(paramContext);
          return paramContext;
        }
        finally {}
        return null;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("readString throwable:");
        paramString.append(paramContext.getMessage());
        a.g("JCommonFileHelper", paramString.toString());
      }
    }
  }
  
  public static void d(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {
      try
      {
        try
        {
          paramContext = d.f(paramContext, paramString);
          if (paramContext == null) {
            return;
          }
          d.a(paramContext, "");
        }
        finally {}
        return;
      }
      catch (Throwable paramString)
      {
        paramContext = new StringBuilder();
        paramContext.append("cleanString throwable:");
        paramContext.append(paramString.getMessage());
        a.g("JCommonFileHelper", paramContext.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/s/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */