package cn.jiguang.aq;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.Random;

public class e
{
  private static SharedPreferences a;
  
  private static long a(long paramLong)
  {
    long l = 2L;
    if (paramLong % 2L == 0L) {
      l = 1L;
    }
    return (paramLong + l) % 32767L;
  }
  
  private static long a(Context paramContext, long paramLong)
  {
    long l2 = c(paramContext).getLong("next_rid", paramLong);
    long l1 = l2;
    if (l2 != paramLong)
    {
      l1 = a(l2);
      c(paramContext).edit().putLong("next_rid", l1).apply();
    }
    return l1;
  }
  
  public static void a(Context paramContext)
  {
    a = paramContext.getSharedPreferences("cn.jpush.preferences.support.rid", 0);
  }
  
  public static long b(Context paramContext)
  {
    try
    {
      long l2 = a(paramContext, -1L);
      long l1 = l2;
      if (l2 == -1L)
      {
        Random localRandom = new java/util/Random;
        localRandom.<init>();
        l1 = a(Math.abs(localRandom.nextInt(32767)));
        c(paramContext).edit().putLong("next_rid", l1).apply();
      }
      return l1;
    }
    finally {}
  }
  
  private static SharedPreferences c(Context paramContext)
  {
    if (a == null) {
      a(paramContext);
    }
    return a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aq/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */