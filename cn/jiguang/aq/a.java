package cn.jiguang.aq;

import android.text.TextUtils;
import cn.jiguang.api.JAction;
import cn.jiguang.api.JActionExtra;
import cn.jiguang.api.JCoreManager;
import java.util.HashMap;

public class a
{
  public static HashMap<String, g> a = new HashMap();
  public static HashMap<String, JAction> b = new HashMap();
  public static HashMap<String, JActionExtra> c = new HashMap();
  private static volatile a d;
  private static final Object e = new Object();
  
  public static a a()
  {
    if (d == null) {
      synchronized (e)
      {
        if (d == null)
        {
          a locala = new cn/jiguang/aq/a;
          locala.<init>();
          d = locala;
        }
      }
    }
    return d;
  }
  
  public JAction a(String paramString)
  {
    if (b.containsKey(paramString)) {
      return (JAction)b.get(paramString);
    }
    return null;
  }
  
  public void a(String paramString1, String paramString2)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("addAction type:");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(",action:");
    ((StringBuilder)localObject).append(paramString2);
    d.a("DispacthManager", ((StringBuilder)localObject).toString());
    if (!TextUtils.isEmpty(paramString2))
    {
      if (!a.containsKey(paramString1))
      {
        localObject = new g();
        JCoreManager.addDispatchAction(paramString1, g.class.getCanonicalName());
        a.put(paramString1, localObject);
      }
      if (!b.containsKey(paramString1)) {
        try
        {
          localObject = Class.forName(paramString2).newInstance();
          if ((localObject instanceof JAction)) {
            b.put(paramString1, (JAction)localObject);
          }
        }
        catch (Throwable localThrowable)
        {
          paramString1 = new StringBuilder();
          paramString1.append("#unexcepted - instance ");
          paramString1.append(paramString2);
          paramString1.append(" class failed:");
          paramString1.append(localThrowable);
          d.d("DispacthManager", paramString1.toString());
        }
      }
    }
  }
  
  public JActionExtra b(String paramString)
  {
    if (c.containsKey(paramString)) {
      return (JActionExtra)c.get(paramString);
    }
    return null;
  }
  
  public void b(String paramString1, String paramString2)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("addActionExtra type:");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(",action:");
    ((StringBuilder)localObject).append(paramString2);
    d.a("DispacthManager", ((StringBuilder)localObject).toString());
    if (!TextUtils.isEmpty(paramString2))
    {
      if (!a.containsKey(paramString1))
      {
        localObject = new g();
        JCoreManager.addDispatchAction(paramString1, g.class.getCanonicalName());
        a.put(paramString1, localObject);
      }
      if (!c.containsKey(paramString1)) {
        try
        {
          localObject = Class.forName(paramString2).newInstance();
          if ((localObject instanceof JActionExtra)) {
            c.put(paramString1, (JActionExtra)localObject);
          }
        }
        catch (Throwable localThrowable)
        {
          paramString1 = new StringBuilder();
          paramString1.append("#unexcepted - instance ");
          paramString1.append(paramString2);
          paramString1.append(" class failed:");
          paramString1.append(localThrowable);
          d.d("DispacthManager", paramString1.toString());
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aq/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */