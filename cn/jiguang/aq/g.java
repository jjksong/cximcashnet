package cn.jiguang.aq;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import cn.jiguang.api.JAction;
import cn.jiguang.api.JActionExtra;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.JDispatchAction;
import cn.jiguang.api.SdkType;
import java.nio.ByteBuffer;

public class g
  extends JDispatchAction
{
  private JAction a(String paramString)
  {
    return a.a().a(paramString);
  }
  
  private JActionExtra b(String paramString)
  {
    return a.a().b(paramString);
  }
  
  public Object beforLogin(Context paramContext, String paramString1, int paramInt, String paramString2)
  {
    paramString1 = b(paramString1);
    if (paramString1 != null) {
      return paramString1.beforLogin(paramContext, paramInt, paramString2);
    }
    return null;
  }
  
  public Object beforRegister(Context paramContext, String paramString1, int paramInt, String paramString2)
  {
    paramString1 = b(paramString1);
    if (paramString1 != null) {
      return paramString1.beforRegister(paramContext, paramInt, paramString2);
    }
    return null;
  }
  
  public boolean checkAction(String paramString, int paramInt)
  {
    paramString = b(paramString);
    if (paramString != null) {
      return paramString.checkAction(paramInt);
    }
    return true;
  }
  
  public void dispatchMessage(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, ByteBuffer paramByteBuffer)
  {
    JAction localJAction = a(paramString);
    if (localJAction != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("dispatchMessage ,command:");
      localStringBuilder.append(paramInt1);
      localStringBuilder.append(",ver:");
      localStringBuilder.append(paramInt2);
      localStringBuilder.append(",rid:");
      localStringBuilder.append(paramLong1);
      localStringBuilder.append(",reuqestId:");
      localStringBuilder.append(paramLong2);
      d.b("SupportDispatchAction", localStringBuilder.toString());
      if (paramString.equals(SdkType.JMESSAGE.name())) {
        paramString = new c(false, paramByteBuffer.limit() + 20, paramInt2, paramInt1, paramLong1, JCoreInterface.getSid(), JCoreInterface.getUid());
      } else {
        paramString = new c(false, paramInt2, paramInt1, paramLong2);
      }
      localJAction.dispatchMessage(paramContext, 0L, paramInt1, paramString, paramByteBuffer);
    }
  }
  
  public void dispatchTimeOutMessage(Context paramContext, String paramString, long paramLong, int paramInt)
  {
    paramString = a(paramString);
    if (paramString != null) {
      paramString.dispatchTimeOutMessage(paramContext, 0L, paramLong, paramInt);
    }
  }
  
  public short getLogPriority(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return 1;
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return 2;
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return 4;
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return 5;
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return 3;
    }
    return 0;
  }
  
  public short getLoginFlag(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return 1;
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return 4;
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return 8;
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return 128;
    }
    if (paramString.equals(SdkType.JMESSAGE.name())) {
      return 32;
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return 256;
    }
    return 0;
  }
  
  public short getRegFlag(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return 1;
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return 4;
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return 8;
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return 128;
    }
    if (paramString.equals(SdkType.JMESSAGE.name())) {
      return 32;
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return 256;
    }
    return 0;
  }
  
  public short getRegPriority(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return 0;
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return 1;
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return 2;
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return 4;
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return 5;
    }
    return 3;
  }
  
  public String getReportVersionKey(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return "sdk_ver";
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return "statistics_sdk_ver";
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return "share_sdk_ver";
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return "ssp_sdk_ver";
    }
    if (paramString.equals(SdkType.JMESSAGE.name())) {
      return "im_sdk_ver";
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return "verification_sdk_ver";
    }
    return null;
  }
  
  public String getSdkVersion(String paramString)
  {
    paramString = a(paramString);
    if (paramString != null) {
      return paramString.getSdkVersion();
    }
    return "";
  }
  
  public short getUserCtrlProperty(String paramString)
  {
    if (paramString.equals(SdkType.JPUSH.name())) {
      return 1;
    }
    if (paramString.equals(SdkType.JMESSAGE.name())) {
      return 2;
    }
    if (paramString.equals(SdkType.JANALYTICS.name())) {
      return 4;
    }
    if (paramString.equals(SdkType.JSHARE.name())) {
      return 5;
    }
    if (paramString.equals(SdkType.JSSP.name())) {
      return 9;
    }
    if (paramString.equals(SdkType.JVERIFICATION.name())) {
      return 10;
    }
    return 6;
  }
  
  public void handleMessage(Context paramContext, String paramString, Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("handleMessage,sdkType:");
    localStringBuilder.append(paramString);
    d.b("SupportDispatchAction", localStringBuilder.toString());
    paramString = a(paramString);
    if (paramString != null) {
      paramString.handleMessage(paramContext, 0L, paramObject);
    }
  }
  
  public boolean isSupportedCMD(String paramString, int paramInt)
  {
    paramString = a(paramString);
    if (paramString != null) {
      return paramString.isSupportedCMD(paramInt);
    }
    return false;
  }
  
  public void onActionRun(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
  {
    paramString1 = a(paramString1);
    if (paramString1 != null) {
      paramString1.onActionRun(paramContext, 0L, paramBundle, (Handler)null);
    }
  }
  
  public void onEvent(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2)
  {
    paramString1 = a(paramString1);
    if (paramString1 != null) {
      paramString1.onEvent(paramContext, 0L, paramInt1);
    }
  }
  
  public Object onSendData(Context paramContext, String paramString, long paramLong, int paramInt1, int paramInt2)
  {
    paramString = b(paramString);
    if (paramString != null) {
      return paramString.onSendData(paramContext, 0L, paramLong, paramInt1, paramInt2);
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aq/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */