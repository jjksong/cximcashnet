package cn.jiguang.aq;

import cn.jiguang.api.utils.ProtocolUtil;
import java.nio.ByteBuffer;

public class c
{
  int a;
  int b;
  int c;
  Long d;
  int e;
  long f;
  private boolean g = false;
  
  public c(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, long paramLong1, int paramInt4, long paramLong2)
  {
    this.g = paramBoolean;
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    this.d = Long.valueOf(paramLong1);
    this.e = paramInt4;
    this.f = paramLong2;
  }
  
  public c(boolean paramBoolean, int paramInt1, int paramInt2, long paramLong)
  {
    this(paramBoolean, 0, paramInt1, paramInt2, paramLong, 0, 0L);
  }
  
  public c(boolean paramBoolean, byte[] paramArrayOfByte)
  {
    this.g = paramBoolean;
    paramArrayOfByte = ByteBuffer.wrap(paramArrayOfByte);
    this.a = paramArrayOfByte.getShort();
    this.a &= 0x7FFF;
    this.b = paramArrayOfByte.get();
    this.c = paramArrayOfByte.get();
    this.d = Long.valueOf(paramArrayOfByte.getLong());
    this.d = Long.valueOf(this.d.longValue() & 0xFFFF);
    if (paramBoolean) {
      this.e = paramArrayOfByte.getInt();
    }
    this.f = paramArrayOfByte.getLong();
  }
  
  public int a()
  {
    return this.c;
  }
  
  public void a(int paramInt)
  {
    this.a = paramInt;
  }
  
  public void a(long paramLong)
  {
    this.f = paramLong;
  }
  
  public Long b()
  {
    return this.d;
  }
  
  public void b(int paramInt)
  {
    this.e = paramInt;
  }
  
  public long c()
  {
    return this.f;
  }
  
  public int d()
  {
    return this.e;
  }
  
  public int e()
  {
    return this.b;
  }
  
  public byte[] f()
  {
    if (this.a != 0)
    {
      ByteBuffer localByteBuffer = ByteBuffer.allocate(24);
      localByteBuffer.putShort((short)this.a);
      localByteBuffer.put((byte)this.b);
      localByteBuffer.put((byte)this.c);
      localByteBuffer.putLong(this.d.longValue());
      if (this.g) {
        localByteBuffer.putInt(this.e);
      }
      localByteBuffer.putLong(this.f);
      localByteBuffer.flip();
      return ProtocolUtil.getBytesConsumed(localByteBuffer);
    }
    throw new IllegalStateException("The head is not initialized yet.");
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[JHead] - len:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", version:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", command:");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", rid:");
    localStringBuilder.append(this.d);
    Object localObject;
    if (this.g)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(", sid:");
      ((StringBuilder)localObject).append(this.e);
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = "";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", juid:");
    localStringBuilder.append(this.f);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aq/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */