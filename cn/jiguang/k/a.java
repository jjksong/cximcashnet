package cn.jiguang.k;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import cn.jiguang.api.ReportCallBack;
import cn.jiguang.f.b;
import cn.jiguang.f.d;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a c;
  private Context a;
  private JSONObject b;
  private String d;
  private boolean e;
  
  private void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, List<cn.jiguang.l.a> paramList)
  {
    try
    {
      if (this.b == null)
      {
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        this.b = localJSONObject;
      }
      this.b.put("ssid", paramString1);
      this.b.put("bssid", paramString2);
      this.b.put("local_ip", paramString3);
      this.b.put("local_mac", paramString4);
      this.b.put("netmask", paramString5);
      this.b.put("gateway", paramString8);
      this.b.put("dhcp", paramString9);
      paramString1 = new org/json/JSONArray;
      paramString1.<init>();
      if (!TextUtils.isEmpty(paramString6)) {
        paramString1.put(paramString6);
      }
      if (!TextUtils.isEmpty(paramString7)) {
        paramString1.put(paramString7);
      }
      this.b.put("dns", paramString1);
      paramString1 = new org/json/JSONArray;
      paramString1.<init>();
      paramString4 = paramList.iterator();
      while (paramString4.hasNext())
      {
        paramString3 = (cn.jiguang.l.a)paramString4.next();
        paramString2 = new org/json/JSONObject;
        paramString2.<init>();
        paramString2.put("ip", paramString3.a);
        paramString2.put("mac", paramString3.d);
        paramString1.put(paramString2);
      }
      this.b.put("data", paramString1);
    }
    catch (JSONException paramString2)
    {
      paramString1 = new StringBuilder();
      paramString1.append("packageJson exception: ");
      paramString1.append(paramString2.getMessage());
      cn.jiguang.ai.a.g("JArp", paramString1.toString());
    }
  }
  
  public static a d()
  {
    if (c == null) {
      try
      {
        a locala = new cn/jiguang/k/a;
        locala.<init>();
        c = locala;
      }
      finally {}
    }
    return c;
  }
  
  protected void a(String paramString, JSONObject paramJSONObject)
  {
    this.e = true;
    paramString = paramJSONObject.optJSONObject("content");
    boolean bool = paramString.optBoolean("disable") ^ true;
    b.a(this.a, "JArp", bool);
    if (bool)
    {
      long l = paramString.optLong("frequency", 0L) * 1000L;
      if (l == 0L)
      {
        b.a(this.a, true);
      }
      else
      {
        b.a(this.a, false);
        b.b(this.a, "JArp", l);
      }
    }
  }
  
  protected boolean a(Context paramContext, String paramString)
  {
    return true;
  }
  
  protected boolean b()
  {
    return b.i(this.a, "JArp");
  }
  
  protected boolean b(Context paramContext, String paramString)
  {
    long l = System.currentTimeMillis();
    if (TextUtils.isEmpty(this.d)) {
      this.d = "";
    }
    paramString = new StringBuilder();
    paramString.append("JArp");
    paramString.append(this.d);
    boolean bool;
    if (l - b.g(paramContext, paramString.toString()) > b.h(paramContext, "JArp")) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    if (!d.a(paramContext, "android.permission.ACCESS_WIFI_STATE"))
    {
      cn.jiguang.ai.a.g("JArp", "collect arp failed because has no permission Manifest.permission.ACCESS_WIFI_STATE");
      return;
    }
    if (!d.k(paramContext).toUpperCase().startsWith("WIFI"))
    {
      cn.jiguang.ai.a.g("JArp", "collect arp failed because networkType is not wifi");
      return;
    }
    if (TextUtils.isEmpty(d.d(paramContext)))
    {
      cn.jiguang.ai.a.g("JArp", "collect arp failed because can't get registerId");
      return;
    }
    Object localObject2 = (WifiManager)paramContext.getApplicationContext().getSystemService("wifi");
    if (localObject2 == null)
    {
      cn.jiguang.ai.a.c("JArp", "collect arp failed because get wifiManager failed");
      return;
    }
    Object localObject1 = ((WifiManager)localObject2).getConnectionInfo();
    paramString = "";
    String str1 = "";
    if (localObject1 != null)
    {
      str1 = d.e(((WifiInfo)localObject1).getSSID());
      paramString = ((WifiInfo)localObject1).getBSSID();
    }
    if (TextUtils.isEmpty(paramString)) {
      paramString = "";
    }
    if (TextUtils.isEmpty(str1)) {
      str1 = "";
    }
    if (TextUtils.isEmpty(paramString)) {
      localObject1 = str1;
    } else {
      localObject1 = paramString;
    }
    this.d = ((String)localObject1);
    if ((b.c(paramContext)) && (b.m(paramContext, this.d)))
    {
      paramContext = new StringBuilder();
      paramContext.append("collect arp failed because this wifi 【");
      paramContext.append(this.d);
      paramContext.append("】 can't report twice");
      cn.jiguang.ai.a.g("JArp", paramContext.toString());
      return;
    }
    boolean bool = b(paramContext, "JArp");
    if ((!this.e) && (!bool))
    {
      paramContext = new StringBuilder();
      paramContext.append("collect arp failed because this wifi 【");
      paramContext.append(this.d);
      paramContext.append("】 is not in report time");
      cn.jiguang.ai.a.g("JArp", paramContext.toString());
      return;
    }
    this.e = false;
    Object localObject6 = ((WifiManager)localObject2).getDhcpInfo();
    if (localObject6 == null) {
      return;
    }
    byte[] arrayOfByte = cn.jiguang.m.a.a(((DhcpInfo)localObject6).ipAddress);
    localObject2 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).ipAddress);
    localObject1 = localObject2;
    if (TextUtils.equals((CharSequence)localObject2, "0.0.0.0")) {
      localObject1 = "";
    }
    String str2 = d.c(paramContext, "");
    localObject2 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).netmask);
    paramContext = (Context)localObject2;
    if (TextUtils.equals((CharSequence)localObject2, "0.0.0.0")) {
      paramContext = "";
    }
    Object localObject3 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).dns1);
    localObject2 = localObject3;
    if (TextUtils.equals((CharSequence)localObject3, "0.0.0.0")) {
      localObject2 = "";
    }
    Object localObject4 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).dns2);
    localObject3 = localObject4;
    if (TextUtils.equals((CharSequence)localObject4, "0.0.0.0")) {
      localObject3 = "";
    }
    Object localObject5 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).gateway);
    localObject4 = localObject5;
    if (TextUtils.equals((CharSequence)localObject5, "0.0.0.0")) {
      localObject4 = "";
    }
    localObject6 = cn.jiguang.m.a.a(((DhcpInfo)localObject6).serverAddress);
    localObject5 = localObject6;
    if (TextUtils.equals((CharSequence)localObject6, "0.0.0.0")) {
      localObject5 = "";
    }
    cn.jiguang.m.a.a((String)localObject5, arrayOfByte);
    localObject6 = cn.jiguang.m.a.a((String)localObject5);
    if ((localObject6 != null) && (!((List)localObject6).isEmpty()))
    {
      cn.jiguang.ai.a.c("JArp", "collect arp success");
      a(str1, paramString, (String)localObject1, str2, paramContext, (String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (List)localObject6);
    }
    else
    {
      cn.jiguang.ai.a.g("JArp", "collect arp failed because can't get arp info");
    }
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JArp";
  }
  
  protected void d(final Context paramContext, final String paramString)
  {
    JSONObject localJSONObject = this.b;
    if (localJSONObject == null)
    {
      cn.jiguang.ai.a.c("JArp", "there are no data to report");
      return;
    }
    d.a(paramContext, localJSONObject, "mac_list");
    d.a(paramContext, this.b, new ReportCallBack()
    {
      public void onFinish(int paramAnonymousInt)
      {
        if (TextUtils.isEmpty(a.a(a.this))) {
          a.a(a.this, "");
        }
        Context localContext = paramContext;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("JArp");
        localStringBuilder.append(a.a(a.this));
        b.f(localContext, localStringBuilder.toString());
        if (b.h(paramContext, "JArp") == 0L) {
          b.l(paramContext, a.a(a.this));
        }
        a.a(a.this, paramContext, paramString);
      }
    });
    this.b = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/k/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */