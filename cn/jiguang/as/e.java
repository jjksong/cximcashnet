package cn.jiguang.as;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import cn.jiguang.ap.a;
import cn.jiguang.ap.h;
import cn.jiguang.ap.j;
import java.io.Closeable;
import java.io.InputStream;
import java.util.List;

public class e
{
  public static ComponentInfo a(Context paramContext, String paramString, Class<?> paramClass)
  {
    return a.a(paramContext, paramString, paramClass);
  }
  
  public static Object a(Context paramContext, String paramString, Bundle paramBundle)
  {
    return cn.jiguang.ah.e.a(paramContext, paramString, paramBundle);
  }
  
  public static String a()
  {
    return a.b();
  }
  
  public static String a(Context paramContext)
  {
    return a.g(paramContext);
  }
  
  public static void a(Closeable paramCloseable)
  {
    j.a(paramCloseable);
  }
  
  public static boolean a(Context paramContext, Class<?> paramClass)
  {
    for (;;)
    {
      boolean bool1;
      boolean bool2;
      try
      {
        PackageManager localPackageManager = paramContext.getPackageManager();
        Intent localIntent = new android/content/Intent;
        localIntent.<init>(paramContext, paramClass);
        bool1 = localPackageManager.queryBroadcastReceivers(localIntent, 0).isEmpty();
        bool2 = bool1 ^ true;
        bool1 = bool2;
        if (bool2) {}
      }
      catch (Throwable paramContext)
      {
        continue;
      }
      try
      {
        paramContext = a.b(paramContext, paramContext.getPackageName(), paramClass);
        if (paramContext != null) {
          bool1 = true;
        } else {
          bool1 = false;
        }
      }
      catch (Throwable paramContext)
      {
        bool1 = bool2;
      }
    }
    return bool1;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    return a.a(paramContext, paramString);
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    return j.a(paramInputStream);
  }
  
  public static byte[] a(byte[] paramArrayOfByte)
  {
    return j.a(paramArrayOfByte);
  }
  
  public static boolean b(Context paramContext)
  {
    return a.c(paramContext);
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    return a.g(paramContext, paramString);
  }
  
  public static byte[] b(InputStream paramInputStream)
  {
    return j.b(paramInputStream);
  }
  
  public static byte[] b(byte[] paramArrayOfByte)
  {
    return j.b(paramArrayOfByte);
  }
  
  public static int c(Context paramContext)
  {
    return h.a(paramContext);
  }
  
  @SuppressLint({"MissingPermission"})
  public static String c(Context paramContext, String paramString)
  {
    return a.c(paramContext, paramString);
  }
  
  public static String d(Context paramContext)
  {
    return a.j(paramContext);
  }
  
  public static String d(Context paramContext, String paramString)
  {
    return a.e(paramContext, paramString);
  }
  
  public static String e(Context paramContext)
  {
    return h.b(paramContext);
  }
  
  public static void e(Context paramContext, String paramString)
  {
    a.a(paramContext, paramString, -1);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */