package cn.jiguang.as;

import android.util.Base64;
import cn.jiguang.ac.c;
import java.lang.reflect.Constructor;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class g
{
  private static <T> T a(Class<T> paramClass, Object[] paramArrayOfObject, Class<?>[] paramArrayOfClass)
  {
    return (T)paramClass.getConstructor(paramArrayOfClass).newInstance(paramArrayOfObject);
  }
  
  public static String a()
  {
    return a(Math.abs(new SecureRandom().nextInt()) & 0xFFFFFF);
  }
  
  private static String a(long paramLong)
  {
    long l1;
    long l2;
    switch ((int)paramLong % 10)
    {
    default: 
      l1 = 8L * paramLong;
      l2 = 74L;
    }
    for (;;)
    {
      break;
      l1 = 37L * paramLong;
      l2 = 91L;
      continue;
      l1 = 29L * paramLong;
      l2 = 41L;
      continue;
      l1 = 31L * paramLong;
      l2 = 39L;
      continue;
      l1 = 7L * paramLong;
      l2 = 68L;
      continue;
      l1 = 17L * paramLong;
      l2 = 49L;
      continue;
      l1 = 13L * paramLong;
      l2 = 96L;
      continue;
      l1 = 3L * paramLong;
      l2 = 73L;
      continue;
      l1 = 23L * paramLong;
      l2 = 15L;
      continue;
      l1 = 5L * paramLong;
      l2 = 88L;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JCKP");
    localStringBuilder.append(l1 + paramLong % l2);
    return h.c(localStringBuilder.toString());
  }
  
  public static String a(String paramString)
  {
    String str = "";
    try
    {
      paramString = c(paramString, "DFA84B10B7ACDD25");
    }
    catch (Exception paramString)
    {
      c.f("", "Unexpected - failed to AES encrypt.");
      paramString = str;
    }
    return paramString;
  }
  
  public static String a(String paramString1, String paramString2)
  {
    return new String(Base64.encode(a(paramString1, c(paramString2)), 2), "UTF-8");
  }
  
  private static IvParameterSpec a(byte[] paramArrayOfByte)
  {
    try
    {
      paramArrayOfByte = (IvParameterSpec)a(IvParameterSpec.class, new Object[] { paramArrayOfByte }, new Class[] { byte[].class });
      return paramArrayOfByte;
    }
    catch (Exception paramArrayOfByte)
    {
      paramArrayOfByte.printStackTrace();
    }
    return null;
  }
  
  private static byte[] a(String paramString, RSAPublicKey paramRSAPublicKey)
  {
    Cipher localCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
    localCipher.init(1, paramRSAPublicKey);
    return localCipher.doFinal(paramString.getBytes());
  }
  
  public static byte[] a(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    paramString1 = d(paramString1, "utf-8");
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    localCipher.init(1, new SecretKeySpec(paramString1, "AES"), a(paramString2.getBytes("utf-8")));
    return localCipher.doFinal(paramArrayOfByte);
  }
  
  public static String b(String paramString)
  {
    try
    {
      paramString = b(paramString, "DFA84B10B7ACDD25");
      return paramString;
    }
    catch (Exception paramString)
    {
      c.f("", "Unexpected - failed to AES decrypt.");
    }
    return "";
  }
  
  private static String b(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    try
    {
      if (paramString2.length() != 16) {
        return null;
      }
      byte[] arrayOfByte = d(paramString2, "ASCII");
      paramString2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
      SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
      localSecretKeySpec.<init>(arrayOfByte, "AES");
      paramString2.init(2, localSecretKeySpec, a(arrayOfByte));
      paramString1 = Base64.decode(paramString1, 2);
      paramString1 = new String(paramString2.doFinal(paramString1));
      return paramString1;
    }
    catch (Exception paramString1) {}
    return null;
  }
  
  private static String c(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    if (paramString2.length() != 16) {
      return null;
    }
    paramString2 = d(paramString2, "ASCII");
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    localCipher.init(1, new SecretKeySpec(paramString2, "AES"), a(paramString2));
    return Base64.encodeToString(localCipher.doFinal(paramString1.getBytes()), 2);
  }
  
  private static RSAPublicKey c(String paramString)
  {
    try
    {
      byte[] arrayOfByte = Base64.decode(paramString, 2);
      KeyFactory localKeyFactory = KeyFactory.getInstance("RSA");
      paramString = new java/security/spec/X509EncodedKeySpec;
      paramString.<init>(arrayOfByte);
      paramString = (RSAPublicKey)localKeyFactory.generatePublic(paramString);
      return paramString;
    }
    catch (Throwable paramString) {}
    return null;
  }
  
  private static byte[] d(String paramString1, String paramString2)
  {
    byte[] arrayOfByte1 = new byte[paramString1.length()];
    byte[] arrayOfByte2 = paramString1.substring(0, paramString1.length() / 2).getBytes(paramString2);
    paramString1 = paramString1.substring(paramString1.length() / 2).getBytes(paramString2);
    System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, arrayOfByte2.length);
    System.arraycopy(paramString1, 0, arrayOfByte1, arrayOfByte2.length, paramString1.length);
    return arrayOfByte1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */