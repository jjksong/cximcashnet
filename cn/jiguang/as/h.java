package cn.jiguang.as;

import android.text.TextUtils;
import cn.jiguang.ac.c;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class h
{
  private static Pattern a = Pattern.compile("((2[0-4]\\d|25[0-5]|[01]?\\d{1,2})\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d{1,2})");
  private static SimpleDateFormat b = new SimpleDateFormat("mm:ss:SSS", Locale.ENGLISH);
  
  public static String a(String paramString, int paramInt)
  {
    int j = paramString.length();
    if (j >= paramInt) {
      return paramString;
    }
    for (int i = 0; i < paramInt - j; i++)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" ");
      paramString = localStringBuilder.toString();
    }
    return paramString;
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    try
    {
      byte[] arrayOfByte = MessageDigest.getInstance("SHA1").digest(paramArrayOfByte);
      paramArrayOfByte = new java/lang/StringBuffer;
      paramArrayOfByte.<init>();
      for (int i = 0; i < arrayOfByte.length; i++)
      {
        int j = arrayOfByte[i] & 0xFF;
        if (j < 16) {
          paramArrayOfByte.append("0");
        }
        paramArrayOfByte.append(Integer.toHexString(j));
      }
      paramArrayOfByte = paramArrayOfByte.toString();
      return paramArrayOfByte;
    }
    catch (Throwable paramArrayOfByte)
    {
      c.b("StringUtils", "Get SHA1 error");
    }
    return "";
  }
  
  public static boolean a(String paramString)
  {
    if (paramString == null) {
      return true;
    }
    if (paramString.length() == 0) {
      return true;
    }
    return paramString.trim().length() == 0;
  }
  
  public static String b(String paramString)
  {
    if (a(paramString)) {
      return "";
    }
    return Pattern.compile("[^\\w#$@\\-一-龥]+").matcher(paramString).replaceAll("");
  }
  
  public static String c(String paramString)
  {
    try
    {
      Object localObject = MessageDigest.getInstance("MD5");
      paramString = paramString.toCharArray();
      byte[] arrayOfByte = new byte[paramString.length];
      int j = 0;
      for (int i = 0; i < paramString.length; i++) {
        arrayOfByte[i] = ((byte)paramString[i]);
      }
      paramString = ((MessageDigest)localObject).digest(arrayOfByte);
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      for (i = j; i < paramString.length; i++)
      {
        j = paramString[i] & 0xFF;
        if (j < 16) {
          ((StringBuffer)localObject).append("0");
        }
        ((StringBuffer)localObject).append(Integer.toHexString(j));
      }
      paramString = ((StringBuffer)localObject).toString();
      return paramString;
    }
    catch (Exception paramString) {}
    return "";
  }
  
  public static String d(String paramString)
  {
    try
    {
      paramString = MessageDigest.getInstance("MD5").digest(paramString.getBytes("utf-8"));
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      for (int i = 0; i < paramString.length; i++)
      {
        int j = paramString[i] & 0xFF;
        if (j < 16) {
          localStringBuffer.append("0");
        }
        localStringBuffer.append(Integer.toHexString(j));
      }
      paramString = localStringBuffer.toString();
      return paramString;
    }
    catch (Throwable paramString)
    {
      c.b("StringUtils", "Get MD5 error");
    }
    return "";
  }
  
  public static boolean e(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    int i = paramString.indexOf(":");
    String str = paramString;
    if (i >= 0) {
      if (i == paramString.lastIndexOf(":")) {
        str = paramString.substring(0, i);
      } else {
        return false;
      }
    }
    return a.matcher(str).matches();
  }
  
  public static boolean f(String paramString)
  {
    boolean bool2 = TextUtils.isEmpty(paramString);
    boolean bool1 = false;
    if (bool2) {
      return false;
    }
    int i = paramString.indexOf(":");
    if (i == -1) {
      return false;
    }
    if (paramString.lastIndexOf(":") != i) {
      bool1 = true;
    }
    return bool1;
  }
  
  public static boolean g(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    try
    {
      boolean bool = Pattern.compile("[\\x20-\\x7E]+").matcher(paramString).matches();
      return bool;
    }
    catch (Throwable paramString) {}
    return true;
  }
  
  public static byte[] h(String paramString)
  {
    try
    {
      localObject = paramString.getBytes("UTF-8");
      return (byte[])localObject;
    }
    catch (Throwable localThrowable)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("stringToUtf8Bytes error:");
      ((StringBuilder)localObject).append(localThrowable.getMessage());
      c.i("StringUtils", ((StringBuilder)localObject).toString());
    }
    return paramString.getBytes();
  }
  
  public static String i(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return paramString;
    }
    try
    {
      String str = a(paramString.getBytes("utf-8"));
      return str;
    }
    catch (Throwable localThrowable)
    {
      localThrowable.printStackTrace();
    }
    return paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */