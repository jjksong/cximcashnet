package cn.jiguang.as;

import android.content.Context;
import android.os.Build.VERSION;
import android.provider.Settings.System;
import android.text.TextUtils;
import cn.jiguang.e.a;
import cn.jiguang.e.b;
import java.io.File;
import java.util.UUID;

public class c
{
  public static int a = 1;
  
  public static String a(Context paramContext)
  {
    Object localObject = (String)b.a(paramContext, a.a());
    if (h.g((String)localObject))
    {
      a = 3;
      return (String)localObject;
    }
    localObject = b(paramContext, (String)localObject);
    if (h.g((String)localObject))
    {
      a = 1;
      d(paramContext, (String)localObject);
      b.a(paramContext, new a[] { a.a().a(localObject) });
      return (String)localObject;
    }
    String str1 = b(paramContext);
    if (h.g(str1))
    {
      a = 2;
      c(paramContext, str1);
      b.a(paramContext, new a[] { a.a().a(str1) });
      return str1;
    }
    localObject = "";
    if (Build.VERSION.SDK_INT < 23) {
      localObject = e.c(paramContext, str1);
    }
    String str3 = e.a(paramContext);
    String str2 = e.d(paramContext, "");
    str1 = UUID.randomUUID().toString();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(str3);
    localStringBuilder.append(str2);
    localStringBuilder.append(str1);
    str2 = h.c(localStringBuilder.toString());
    localObject = str2;
    if (TextUtils.isEmpty(str2)) {
      localObject = str1;
    }
    b.a(paramContext, new a[] { a.a().a(localObject) });
    a = 0;
    c(paramContext, (String)localObject);
    d(paramContext, (String)localObject);
    return (String)localObject;
  }
  
  public static void a(Context paramContext, String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      c(paramContext, paramString);
      d(paramContext, paramString);
      b.a(paramContext, new a[] { a.a().a(paramString) });
    }
  }
  
  private static String b(Context paramContext)
  {
    if (e.a(paramContext, "android.permission.READ_EXTERNAL_STORAGE"))
    {
      String str = e.a();
      if (!TextUtils.isEmpty(str))
      {
        paramContext = new StringBuilder();
        paramContext.append(str);
        paramContext.append(".push_deviceid");
        paramContext = d.d(new File(paramContext.toString()));
        if (paramContext != null)
        {
          int i = paramContext.indexOf("\n");
          if (i < 0) {
            return paramContext.trim();
          }
          return paramContext.substring(0, i).trim();
        }
      }
      else
      {
        cn.jiguang.ac.c.h("DeviceIdUtils", "can't get sdcard data path");
      }
    }
    return null;
  }
  
  private static String b(Context paramContext, String paramString)
  {
    String str = paramString;
    if (e.a(paramContext, "android.permission.WRITE_SETTINGS")) {
      try
      {
        str = Settings.System.getString(paramContext.getContentResolver(), "dig");
      }
      catch (Throwable paramContext)
      {
        cn.jiguang.ac.c.h("DeviceIdUtils", "Can not read from settings");
        str = paramString;
      }
    }
    return str;
  }
  
  private static String c(Context paramContext, String paramString)
  {
    if (e.a(paramContext, "android.permission.WRITE_SETTINGS")) {
      try
      {
        boolean bool = Settings.System.putString(paramContext.getContentResolver(), "dig", paramString);
        if (bool) {
          return paramString;
        }
      }
      catch (Throwable paramContext)
      {
        cn.jiguang.ac.c.h("DeviceIdUtils", "Can not write settings");
      }
    }
    return null;
  }
  
  private static String d(Context paramContext, String paramString)
  {
    if (e.a(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE")) {}
    try
    {
      paramContext = e.a();
      if (!TextUtils.isEmpty(paramContext))
      {
        File localFile = new java/io/File;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramContext);
        localStringBuilder.append(".push_deviceid");
        localFile.<init>(localStringBuilder.toString());
        d.a(localFile, paramString);
        return paramString;
      }
      cn.jiguang.ac.c.h("DeviceIdUtils", "can't get sdcard data path");
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */