package cn.jiguang.as;

import cn.jiguang.ac.c;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public class b
{
  public static String a = "yyyyMMdd_HHmm";
  private static final Object b = new Object();
  private static ConcurrentHashMap<String, ThreadLocal<SimpleDateFormat>> c = new ConcurrentHashMap();
  
  public static String a()
  {
    return a("yyyy-MM-dd_HH:mm:ss").format(new Date());
  }
  
  public static SimpleDateFormat a(String paramString)
  {
    ThreadLocal localThreadLocal = (ThreadLocal)c.get(paramString);
    Object localObject1 = localThreadLocal;
    if (localThreadLocal == null) {
      synchronized (b)
      {
        localThreadLocal = (ThreadLocal)c.get(paramString);
        localObject1 = localThreadLocal;
        if (localThreadLocal == null)
        {
          localObject1 = new cn/jiguang/as/b$a;
          ((a)localObject1).<init>(paramString);
          c.put(paramString, localObject1);
        }
      }
    }
    return (SimpleDateFormat)((ThreadLocal)localObject1).get();
  }
  
  public static boolean a(Date paramDate, int paramInt)
  {
    if (paramDate == null) {
      return false;
    }
    Calendar localCalendar2 = Calendar.getInstance();
    Calendar localCalendar1 = Calendar.getInstance();
    localCalendar1.setTimeInMillis(paramDate.getTime());
    localCalendar2.roll(6, -paramInt);
    return localCalendar2.after(localCalendar1);
  }
  
  public static String b()
  {
    return a(a).format(new Date());
  }
  
  public static Date b(String paramString)
  {
    Object localObject = a(a);
    try
    {
      localObject = ((SimpleDateFormat)localObject).parse(paramString);
      return (Date)localObject;
    }
    catch (ParseException localParseException)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("parse filename datetime error - ");
      ((StringBuilder)localObject).append(paramString);
      c.c("DateUtil", ((StringBuilder)localObject).toString(), localParseException);
    }
    return null;
  }
  
  private static class a
    extends ThreadLocal<SimpleDateFormat>
  {
    private String a;
    
    a(String paramString)
    {
      this.a = paramString;
    }
    
    protected SimpleDateFormat a()
    {
      return new SimpleDateFormat(this.a, Locale.ENGLISH);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */