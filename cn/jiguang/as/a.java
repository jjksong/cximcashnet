package cn.jiguang.as;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Process;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a
{
  private static String a;
  
  public static ProviderInfo a(Context paramContext, String paramString1, String paramString2)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString1)) && (!TextUtils.isEmpty(paramString2))) {}
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramString1, 8);
      if ((paramContext.providers != null) && (paramContext.providers.length != 0)) {
        for (paramString1 : paramContext.providers)
        {
          boolean bool = paramString2.equals(paramString1.authority);
          if (bool) {
            return paramString1;
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return null;
  }
  
  public static String a(Context paramContext)
  {
    if (!TextUtils.isEmpty(a)) {
      return a;
    }
    try
    {
      localObject = cn.jiguang.a.a.a(paramContext);
      paramContext = null;
      if (localObject != null) {
        paramContext = (ActivityManager)((Context)localObject).getSystemService("activity");
      }
      if (paramContext != null)
      {
        int i = Process.myPid();
        localObject = paramContext.getRunningAppProcesses().iterator();
        while (((Iterator)localObject).hasNext())
        {
          paramContext = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject).next();
          if (paramContext.pid == i) {
            a = paramContext.processName;
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("#unexcepted - getCurProcessName failed:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      c.g("AndroidUtil", ((StringBuilder)localObject).toString());
    }
    return a;
  }
  
  public static String a(Context paramContext, String paramString)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      ComponentName localComponentName = new android/content/ComponentName;
      localComponentName.<init>(paramContext.getPackageName(), paramString);
      paramString = localPackageManager.getServiceInfo(localComponentName, 128);
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append("process name:");
      paramContext.append(paramString.processName);
      c.a("AndroidUtil", paramContext.toString());
      paramContext = paramString.processName;
      return paramContext;
    }
    catch (Throwable paramContext) {}
    return "";
  }
  
  public static List<String> a(Context paramContext, Intent paramIntent, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    try
    {
      paramIntent = paramContext.getPackageManager().queryIntentServices(paramIntent, 0);
      paramContext = paramContext.getPackageManager();
      paramIntent = paramIntent.iterator();
      while (paramIntent.hasNext())
      {
        ResolveInfo localResolveInfo = (ResolveInfo)paramIntent.next();
        if (localResolveInfo.serviceInfo != null)
        {
          String str = localResolveInfo.serviceInfo.name;
          if (!TextUtils.isEmpty(str))
          {
            int j = 1;
            int i = j;
            if (!TextUtils.isEmpty(paramString))
            {
              i = j;
              if (paramContext.checkPermission(paramString, localResolveInfo.activityInfo.packageName) != 0) {
                i = 0;
              }
            }
            if (i != 0) {
              localArrayList.add(str);
            }
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return localArrayList;
  }
  
  public static List<String> a(Context paramContext, String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length != 0))
    {
      ArrayList localArrayList = new ArrayList();
      int j = paramArrayOfString.length;
      for (int i = 0; i < j; i++)
      {
        String str = paramArrayOfString[i];
        if (!e.a(paramContext, str)) {
          localArrayList.add(str);
        }
      }
      return localArrayList;
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/as/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */