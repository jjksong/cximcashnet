package cn.jiguang.ak;

import android.content.Context;
import cn.jiguang.ap.d;
import java.nio.ByteBuffer;
import org.json.JSONArray;
import org.json.JSONObject;

public class b
{
  public static String a(ByteBuffer paramByteBuffer)
  {
    byte[] arrayOfByte = new byte[paramByteBuffer.getShort()];
    paramByteBuffer.get(arrayOfByte);
    try
    {
      paramByteBuffer = new String(arrayOfByte, "UTF-8");
      return paramByteBuffer;
    }
    catch (Throwable paramByteBuffer) {}
    return null;
  }
  
  public static byte[] a(int paramInt, byte paramByte, long paramLong, String paramString)
  {
    d locald = new d(20480);
    locald.b(paramInt);
    locald.a(paramByte);
    locald.b(paramLong);
    locald.a(paramString);
    return locald.b();
  }
  
  public static byte[] a(long paramLong1, int paramInt, long paramLong2, short paramShort)
  {
    d locald = new d(20480);
    locald.b(0);
    locald.a(4);
    locald.a(2);
    locald.b(paramLong1);
    locald.a(paramInt);
    locald.b(paramLong2);
    locald.a(paramShort);
    locald.b(locald.a(), 0);
    return locald.b();
  }
  
  public static byte[] a(long paramLong1, long paramLong2, String paramString1, String paramString2, String paramString3, long paramLong3, byte paramByte, int paramInt, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    d locald = new d(20480);
    locald.b(0);
    locald.a(23);
    locald.a(1);
    locald.b(paramLong1);
    locald.a(0L);
    locald.b(paramLong2);
    locald.a(97);
    locald.a(0);
    locald.b(0);
    locald.a(paramString1);
    locald.a(paramString3);
    locald.a(paramString2);
    locald.a(0);
    locald.a(paramLong3);
    locald.a(paramByte);
    locald.a(paramInt);
    locald.a(paramString4);
    locald.a(paramString5);
    locald.a(paramString6);
    locald.a(paramString7);
    locald.b(locald.a(), 0);
    return locald.b();
  }
  
  public static byte[] a(long paramLong1, String paramString1, String paramString2, String paramString3, String paramString4, long paramLong2, String paramString5)
  {
    int i = cn.jiguang.al.a.a();
    d locald = new d(20480);
    locald.b(0);
    locald.a(19);
    locald.a(0);
    locald.b(paramLong1);
    locald.a(i);
    locald.b(0L);
    locald.a(paramString1);
    locald.a(paramString2);
    locald.a(paramString3);
    locald.a(0);
    locald.a(paramString4);
    locald.a(paramLong2);
    locald.a(paramString5);
    locald.b(locald.a(), 0);
    return locald.b();
  }
  
  public static byte[] a(long paramLong, String paramString, long[] paramArrayOfLong)
  {
    d locald = new d(20480);
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("device_id", paramString);
      paramString = new org/json/JSONArray;
      paramString.<init>();
      if (paramArrayOfLong != null)
      {
        paramString.put(paramLong);
        int j = paramArrayOfLong.length;
        for (int i = 0; i < j; i++) {
          paramString.put(paramArrayOfLong[i]);
        }
      }
      localJSONObject.put("uids", paramString);
      paramArrayOfLong = new java/lang/StringBuilder;
      paramArrayOfLong.<init>();
      paramArrayOfLong.append("attach uids:");
      paramArrayOfLong.append(paramString.toString());
      cn.jiguang.ai.a.c("CorePackage", paramArrayOfLong.toString());
      locald.a(localJSONObject.toString());
      paramString = locald.b();
      return paramString;
    }
    catch (Throwable paramArrayOfLong)
    {
      paramString = new StringBuilder();
      paramString.append("packageAttachInfo:");
      paramString.append(paramArrayOfLong);
      cn.jiguang.ai.a.g("CorePackage", paramString.toString());
    }
    return null;
  }
  
  public static byte[] a(Context paramContext, int paramInt1, int paramInt2, long paramLong1, byte[] paramArrayOfByte, long paramLong2)
  {
    d locald = new d(20480);
    locald.b(0);
    locald.a(paramInt2);
    locald.a(paramInt1);
    locald.b(paramLong1);
    locald.a(cn.jiguang.sdk.impl.a.g);
    paramLong1 = paramLong2;
    if (paramLong2 == 0L) {
      paramLong1 = cn.jiguang.sdk.impl.b.e(paramContext);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("packageSendData use uid:");
    localStringBuilder.append(paramLong1);
    cn.jiguang.ai.a.c("CorePackage", localStringBuilder.toString());
    locald.b(paramLong1);
    locald.a(paramArrayOfByte);
    locald.b(locald.a(), 0);
    return a(paramContext, locald.b());
  }
  
  public static byte[] a(Context paramContext, byte[] paramArrayOfByte)
  {
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length != 0))
    {
      int i = paramArrayOfByte.length - 24;
      byte[] arrayOfByte1 = new byte[24];
      byte[] arrayOfByte2 = new byte[i];
      System.arraycopy(paramArrayOfByte, 0, arrayOfByte1, 0, 24);
      System.arraycopy(paramArrayOfByte, 24, arrayOfByte2, 0, i);
      paramContext = cn.jiguang.al.a.a(paramContext);
      try
      {
        paramContext = cn.jiguang.al.a.a(paramContext, arrayOfByte2);
        i = paramContext.length + 24;
        paramArrayOfByte = new byte[i];
        System.arraycopy(arrayOfByte1, 0, paramArrayOfByte, 0, 24);
        System.arraycopy(paramContext, 0, paramArrayOfByte, 24, paramContext.length);
        paramArrayOfByte[0] = ((byte)(i >>> 8 & 0xFF));
        paramArrayOfByte[1] = ((byte)(i & 0xFF));
        paramArrayOfByte[0] = ((byte)(paramArrayOfByte[0] | 0x80));
        paramArrayOfByte[4] = 1;
        return paramArrayOfByte;
      }
      catch (Exception paramContext)
      {
        cn.jiguang.ai.a.h("CorePackage", "encrpt data failed");
        return null;
      }
    }
    return paramArrayOfByte;
  }
  
  public static byte[] a(String paramString, long[] paramArrayOfLong)
  {
    d locald = new d(20480);
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("device_id", paramString);
      paramString = new org/json/JSONArray;
      paramString.<init>();
      if (paramArrayOfLong != null)
      {
        int j = paramArrayOfLong.length;
        for (int i = 0; i < j; i++) {
          paramString.put(paramArrayOfLong[i]);
        }
      }
      localJSONObject.put("uids", paramString);
      locald.a(localJSONObject.toString());
      paramString = locald.b();
      return paramString;
    }
    catch (Throwable paramString)
    {
      paramArrayOfLong = new StringBuilder();
      paramArrayOfLong.append("packageDetachInfo:");
      paramArrayOfLong.append(paramString);
      cn.jiguang.ai.a.g("CorePackage", paramArrayOfLong.toString());
    }
    return null;
  }
  
  public static byte[] a(short paramShort1, short paramShort2, String paramString)
  {
    d locald = new d(20480);
    locald.a(paramShort1);
    locald.a(paramShort2);
    locald.a(paramString);
    return locald.b();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ak/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */