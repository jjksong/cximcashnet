package cn.jiguang.ak;

import java.nio.ByteBuffer;

public class c
{
  public int a;
  public int b;
  public int c;
  public Long d;
  public int e;
  public long f;
  private boolean g = false;
  
  public c(boolean paramBoolean, byte[] paramArrayOfByte)
  {
    try
    {
      this.g = paramBoolean;
      paramArrayOfByte = ByteBuffer.wrap(paramArrayOfByte);
      this.a = paramArrayOfByte.getShort();
      this.a &= 0x7FFF;
      this.b = paramArrayOfByte.get();
      this.c = paramArrayOfByte.get();
      this.d = Long.valueOf(paramArrayOfByte.getLong());
      this.d = Long.valueOf(this.d.longValue() & 0xFFFF);
      if (paramBoolean) {
        this.e = paramArrayOfByte.getInt();
      }
      this.f = paramArrayOfByte.getLong();
      return;
    }
    catch (Throwable paramArrayOfByte)
    {
      for (;;) {}
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[JHead] - len:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", version:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", command:");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", rid:");
    localStringBuilder.append(this.d);
    Object localObject;
    if (this.g)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(", sid:");
      ((StringBuilder)localObject).append(this.e);
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = "";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", juid:");
    localStringBuilder.append(this.f);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ak/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */