package cn.jiguang.ak;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import java.nio.ByteBuffer;

public class a
{
  public static Pair<c, ByteBuffer> a(Context paramContext, byte[] paramArrayOfByte, String paramString)
  {
    if (paramArrayOfByte.length < 20)
    {
      cn.jiguang.ai.a.h("JCommands", "Error: received body-length short than common head-length, return null");
      return null;
    }
    Object localObject = new byte[20];
    System.arraycopy(paramArrayOfByte, 0, localObject, 0, 20);
    c localc = new c(false, (byte[])localObject);
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("parsed head - ");
    ((StringBuilder)localObject).append(localc.toString());
    cn.jiguang.ai.a.c("JCommands", ((StringBuilder)localObject).toString());
    int i = localc.a - 20;
    if (i < 0)
    {
      cn.jiguang.ai.a.h("JCommands", "Error: totalBytes length error with no encrypted, return null");
      return null;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("body size:");
    ((StringBuilder)localObject).append(i);
    cn.jiguang.ai.a.a("JCommands", ((StringBuilder)localObject).toString());
    localObject = paramString;
    if (TextUtils.isEmpty(paramString)) {
      localObject = cn.jiguang.al.a.a(paramContext);
    }
    paramString = new byte[i];
    System.arraycopy(paramArrayOfByte, 20, paramString, 0, i);
    paramContext = new StringBuilder();
    paramContext.append("aes decode pwd:");
    paramContext.append((String)localObject);
    cn.jiguang.ai.a.c("JCommands", paramContext.toString());
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      try
      {
        paramContext = ByteBuffer.wrap(cn.jiguang.al.a.b((String)localObject, paramString));
      }
      catch (Exception paramArrayOfByte)
      {
        paramContext = new StringBuilder();
        paramContext.append("decryptBytes error:");
        paramContext.append(paramArrayOfByte.getMessage());
        cn.jiguang.ai.a.c("JCommands", paramContext.toString());
        return null;
      }
    }
    else
    {
      System.arraycopy(paramArrayOfByte, 20, paramString, 0, i);
      paramContext = ByteBuffer.wrap(paramString);
    }
    return new Pair(localc, paramContext);
  }
  
  public static boolean a(Context paramContext, byte[] paramArrayOfByte)
  {
    try
    {
      Pair localPair = a(paramContext, paramArrayOfByte, "");
      if (localPair != null)
      {
        long l2 = cn.jiguang.sdk.impl.b.e(paramContext);
        long l1 = ((c)localPair.first).f;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("uid:");
        localStringBuilder.append(l2);
        localStringBuilder.append(",msgUid:");
        localStringBuilder.append(l1);
        localStringBuilder.append(",cmd:");
        localStringBuilder.append(((c)localPair.first).c);
        cn.jiguang.ai.a.c("JCommands", localStringBuilder.toString());
        if ((l2 != 0L) && (l1 != 0L) && (l2 != l1))
        {
          cn.jiguang.ai.a.c("JCommands", "recv other app msg");
          cn.jiguang.am.a.a().a(paramContext, l1, paramArrayOfByte);
        }
        else
        {
          cn.jiguang.ah.b.a().a(paramContext, (c)localPair.first, (ByteBuffer)localPair.second);
        }
        return true;
      }
    }
    catch (Throwable paramArrayOfByte)
    {
      paramContext = new StringBuilder();
      paramContext.append("dispatchMessage error:");
      paramContext.append(paramArrayOfByte.getMessage());
      cn.jiguang.ai.a.h("JCommands", paramContext.toString());
      paramArrayOfByte.printStackTrace();
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ak/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */