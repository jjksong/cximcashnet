package cn.jiguang.ak;

import cn.jiguang.api.JCoreManager;
import java.nio.ByteBuffer;

public class e
{
  public int a;
  public long b;
  public String c;
  public String d;
  public String e;
  private final c f;
  private ByteBuffer g;
  private String h;
  private String i;
  
  public e(c paramc, ByteBuffer paramByteBuffer)
  {
    this.f = paramc;
    if (paramByteBuffer != null)
    {
      this.g = paramByteBuffer;
      a();
    }
    else
    {
      cn.jiguang.ai.a.g("RegisterResponse", "No body to parse.");
    }
  }
  
  private void a()
  {
    try
    {
      this.a = this.g.getShort();
    }
    catch (Throwable localThrowable1)
    {
      this.a = 10000;
    }
    if (this.a > 0)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Response error - code:");
      ((StringBuilder)localObject).append(this.a);
      cn.jiguang.ai.a.i("RegisterResponse", ((StringBuilder)localObject).toString());
    }
    Object localObject = this.g;
    int j = this.a;
    if (j == 0)
    {
      try
      {
        this.b = ((ByteBuffer)localObject).getLong();
        this.c = b.a((ByteBuffer)localObject);
        this.d = b.a((ByteBuffer)localObject);
      }
      catch (Throwable localThrowable2)
      {
        this.a = 10000;
      }
    }
    else if (j == 1007)
    {
      this.h = b.a(localThrowable2);
    }
    else if (j == 1012)
    {
      try
      {
        this.i = b.a(localThrowable2);
      }
      catch (Throwable localThrowable3)
      {
        this.a = 10000;
      }
      cn.jiguang.ae.a.a(JCoreManager.getAppContext(null), this.i);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[RegisterResponse] - code:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", juid:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", password:");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", regId:");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", deviceId:");
    localStringBuilder.append(this.e);
    localStringBuilder.append(", connectInfo:");
    localStringBuilder.append(this.i);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ak/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */