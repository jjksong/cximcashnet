package cn.jiguang.ak;

import cn.jiguang.api.JCoreManager;
import java.nio.ByteBuffer;

public class d
{
  public int a;
  public int b;
  public int c;
  public int d;
  private final c e;
  private ByteBuffer f;
  private int g;
  private String h;
  private String i;
  
  public d(c paramc, ByteBuffer paramByteBuffer)
  {
    this.e = paramc;
    if (paramByteBuffer != null)
    {
      this.f = paramByteBuffer;
      a();
    }
    else
    {
      cn.jiguang.ai.a.g("LoginResponse", "No body to parse.");
    }
  }
  
  private void a()
  {
    try
    {
      this.a = this.f.getShort();
    }
    catch (Throwable localThrowable1)
    {
      this.a = 10000;
    }
    if (this.a > 0)
    {
      StringBuilder localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append("Response error - code:");
      localStringBuilder1.append(this.a);
      cn.jiguang.ai.a.i("LoginResponse", localStringBuilder1.toString());
    }
    Object localObject = this.f;
    this.d = -1;
    int j = this.a;
    if (j == 0)
    {
      try
      {
        this.b = ((ByteBuffer)localObject).getInt();
        this.g = ((ByteBuffer)localObject).getShort();
        this.h = b.a((ByteBuffer)localObject);
        this.c = ((ByteBuffer)localObject).getInt();
      }
      catch (Throwable localThrowable2)
      {
        this.a = 10000;
      }
      try
      {
        this.d = ((ByteBuffer)localObject).get();
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>();
        localStringBuilder2.append("idc parse success, value:");
        localStringBuilder2.append(this.d);
        cn.jiguang.ai.a.c("LoginResponse", localStringBuilder2.toString());
      }
      catch (Throwable localThrowable3)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("parse idc failed, error:");
        ((StringBuilder)localObject).append(localThrowable3);
        cn.jiguang.ai.a.g("LoginResponse", ((StringBuilder)localObject).toString());
      }
    }
    else if (j == 1012)
    {
      try
      {
        this.i = b.a((ByteBuffer)localObject);
      }
      catch (Throwable localThrowable4)
      {
        this.a = 10000;
      }
      cn.jiguang.ae.a.a(JCoreManager.getAppContext(null), this.i);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[LoginResponse] - code:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(",sid:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", serverVersion:");
    localStringBuilder.append(this.g);
    localStringBuilder.append(", sessionKey:");
    localStringBuilder.append(this.h);
    localStringBuilder.append(", serverTime:");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", idc:");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", connectInfo:");
    localStringBuilder.append(this.i);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ak/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */