package cn.jiguang.y;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import cn.jpush.android.service.WakedResultReceiver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a a;
  private Context b;
  private WakedResultReceiver c;
  private HashMap<String, WeakReference<ServiceConnection>> d = new HashMap();
  
  private a()
  {
    cn.jiguang.f.d.a("JWake");
  }
  
  private List<cn.jiguang.z.b> a(Context paramContext, List<cn.jiguang.z.c> paramList)
  {
    if ((paramList != null) && (paramList.size() > 0))
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("from_package", paramContext.getPackageName());
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        Object localObject3 = (cn.jiguang.z.c)localIterator.next();
        if (((cn.jiguang.z.c)localObject3).c == 2)
        {
          paramList = new StringBuilder();
          paramList.append("command this app is not allow to wake package:");
          paramList.append(((cn.jiguang.z.c)localObject3).a);
          paramList.append(",service:");
          paramList.append(((cn.jiguang.z.c)localObject3).b);
          cn.jiguang.ai.a.g("JWake", paramList.toString());
        }
        else
        {
          if ((Build.VERSION.SDK_INT >= 26) && (((cn.jiguang.z.c)localObject3).c >= 26)) {
            j = 2;
          } else {
            j = 3;
          }
          int i = j;
          if (!TextUtils.isEmpty(((cn.jiguang.z.c)localObject3).d)) {
            i = j | 0x4;
          }
          int j = i;
          if (!cn.jiguang.aa.a.a(paramContext))
          {
            j = i;
            if (cn.jiguang.aa.a.b(paramContext)) {
              j = i | 0x8;
            }
          }
          paramList = new ComponentName(((cn.jiguang.z.c)localObject3).a, ((cn.jiguang.z.c)localObject3).b);
          cn.jiguang.z.b localb = new cn.jiguang.z.b();
          localb.a = paramList;
          i = j & 0x2;
          Object localObject1;
          Object localObject4;
          if ((i == 0) && ((j & 0x1) == 0))
          {
            paramList = null;
          }
          else
          {
            localObject1 = new Intent();
            ((Intent)localObject1).setComponent(paramList);
            if (Build.VERSION.SDK_INT >= 12) {
              ((Intent)localObject1).setFlags(32);
            }
            localObject4 = cn.jiguang.aa.c.a(localHashMap);
            paramList = (List<cn.jiguang.z.c>)localObject1;
            if (localObject4 != null)
            {
              ((Intent)localObject1).putExtras((Bundle)localObject4);
              paramList = (List<cn.jiguang.z.c>)localObject1;
            }
          }
          boolean bool1;
          if (i != 0)
          {
            try
            {
              localObject4 = new cn/jiguang/y/a$d;
              ((d)localObject4).<init>(this, null);
              boolean bool2 = paramContext.getApplicationContext().bindService(paramList, (ServiceConnection)localObject4, 1);
              bool1 = bool2;
              if (bool2)
              {
                localObject1 = this.d;
                Object localObject5 = new java/lang/StringBuilder;
                ((StringBuilder)localObject5).<init>();
                ((StringBuilder)localObject5).append(((cn.jiguang.z.c)localObject3).a);
                ((StringBuilder)localObject5).append(((cn.jiguang.z.c)localObject3).b);
                String str = ((StringBuilder)localObject5).toString();
                localObject5 = new java/lang/ref/WeakReference;
                ((WeakReference)localObject5).<init>(localObject4);
                ((HashMap)localObject1).put(str, localObject5);
                bool1 = bool2;
              }
            }
            catch (Throwable localThrowable1)
            {
              localObject4 = new StringBuilder();
              ((StringBuilder)localObject4).append("bindService throwable:");
              ((StringBuilder)localObject4).append(localThrowable1.getMessage());
              cn.jiguang.ai.a.g("JWake", ((StringBuilder)localObject4).toString());
              bool1 = false;
            }
            localb.b.put(Integer.valueOf(2), Boolean.valueOf(bool1));
          }
          if ((j & 0x1) != 0)
          {
            try
            {
              paramList = paramContext.startService(paramList);
              if (paramList != null) {
                bool1 = true;
              }
            }
            catch (Throwable localThrowable2)
            {
              paramList = new StringBuilder();
              paramList.append("startService throwable:");
              paramList.append(localThrowable2.getMessage());
              cn.jiguang.ai.a.g("JWake", paramList.toString());
              bool1 = false;
            }
            localb.b.put(Integer.valueOf(1), Boolean.valueOf(bool1));
          }
          if ((j & 0x4) != 0)
          {
            try
            {
              if (!TextUtils.isEmpty(((cn.jiguang.z.c)localObject3).d))
              {
                localObject4 = paramContext.getApplicationContext().getContentResolver();
                localObject2 = ((cn.jiguang.z.c)localObject3).d;
                paramList = (List<cn.jiguang.z.c>)localObject2;
                if (!((String)localObject2).startsWith("content://"))
                {
                  paramList = new java/lang/StringBuilder;
                  paramList.<init>();
                  paramList.append("content://");
                  paramList.append((String)localObject2);
                  paramList = paramList.toString();
                }
                localObject3 = cn.jiguang.aa.c.b(localHashMap);
                localObject2 = paramList;
                if (!TextUtils.isEmpty((CharSequence)localObject3))
                {
                  localObject2 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject2).<init>();
                  ((StringBuilder)localObject2).append(paramList);
                  ((StringBuilder)localObject2).append((String)localObject3);
                  localObject2 = ((StringBuilder)localObject2).toString();
                }
                paramList = ((ContentResolver)localObject4).query(Uri.parse((String)localObject2), null, null, null, null);
                if ((paramList != null) && (!paramList.isClosed())) {
                  paramList.close();
                }
                bool1 = true;
              }
            }
            catch (Throwable paramList)
            {
              Object localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append("getContentResolver throwable:");
              ((StringBuilder)localObject2).append(paramList.getMessage());
              cn.jiguang.ai.a.g("JWake", ((StringBuilder)localObject2).toString());
              bool1 = false;
            }
            localb.b.put(Integer.valueOf(4), Boolean.valueOf(bool1));
          }
          paramList = new StringBuilder();
          paramList.append("wakeResult:");
          paramList.append(localb.toString());
          cn.jiguang.ai.a.c("JWake", paramList.toString());
          localArrayList.add(localb);
        }
      }
      return localArrayList;
    }
    cn.jiguang.ai.a.g("JWake", "there are no wakeTarget");
    return null;
  }
  
  private void a(Context paramContext, int paramInt)
  {
    if (paramContext == null)
    {
      cn.jiguang.ai.a.g("JWake", "context is null,can not notify waked");
      return;
    }
    this.c = i(paramContext);
    if (this.c == null)
    {
      cn.jiguang.ai.a.g("JWake", "waked receiver is null");
      return;
    }
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("1", paramContext);
    localHashMap.put("2", Integer.valueOf(paramInt));
    this.c.onWakeMap(localHashMap);
  }
  
  private void a(Context paramContext, Bundle paramBundle, int paramInt, boolean paramBoolean)
  {
    a(paramContext, paramInt);
    if (!cn.jiguang.aa.c.b(paramContext))
    {
      cn.jiguang.ai.a.c("JWake", "Not need report waked");
      return;
    }
    String str = paramBundle.getString("from_package");
    paramBundle = str;
    if (str == null) {
      paramBundle = "";
    }
    paramBundle = cn.jiguang.aa.d.a(paramBundle, paramInt, paramBoolean);
    if (paramBundle == null) {
      return;
    }
    cn.jiguang.aa.d.a(paramContext, "android_awake_target2", paramBundle);
  }
  
  private void a(cn.jiguang.z.a parama)
  {
    cn.jiguang.f.b.c(this.b, "JWakeConfigHelper");
    cn.jiguang.f.b.a(this.b, "JWakeConfigHelper", parama.f);
    cn.jiguang.f.b.a(this.b, "JWake", parama.e);
    cn.jiguang.f.b.b(this.b, "JWake", parama.g);
    boolean bool1 = parama.a;
    boolean bool2 = true;
    if ((bool1) && (parama.c)) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    cn.jiguang.f.b.a(this.b, "JWake", bool1);
    if ((parama.b) && (parama.d)) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    cn.jiguang.f.b.a(this.b, "JWakeComponentHelper", bool1);
    cn.jiguang.aa.a.a(this.b, bool1);
  }
  
  public static a d()
  {
    if (a == null) {
      try
      {
        a locala = new cn/jiguang/y/a;
        locala.<init>();
        a = locala;
      }
      finally {}
    }
    return a;
  }
  
  private void d(Context paramContext, JSONObject paramJSONObject)
  {
    try
    {
      Object localObject1 = cn.jiguang.s.b.c(paramContext, "bwct.catch");
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append("read cmd wakeTarget:");
      ((StringBuilder)localObject2).append((String)localObject1);
      cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject2).toString());
      if (paramJSONObject != null)
      {
        try
        {
          paramJSONObject = paramJSONObject.getJSONObject("content");
          int i = paramJSONObject.optInt("type", 1);
          localObject2 = paramJSONObject.optString("pkgName", "");
          String str = paramJSONObject.optString("serviceName", "");
          paramJSONObject = new java/util/ArrayList;
          paramJSONObject.<init>();
          cn.jiguang.z.c localc = new cn/jiguang/z/c;
          localc.<init>();
          localc.a = ((String)localObject2);
          localc.b = str;
          localc.c = i;
          paramJSONObject.add(localc);
          localObject2 = cn.jiguang.aa.c.a(cn.jiguang.aa.d.a((String)localObject1, (String)localObject2, str, i));
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("write cmd wakeTarget:");
          ((StringBuilder)localObject1).append((String)localObject2);
          cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject1).toString());
          cn.jiguang.s.b.a(paramContext, "bwct.catch", (String)localObject2);
        }
        catch (JSONException paramJSONObject)
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append("stop wake,the json form cmd is illegal:");
          paramContext.append(paramJSONObject.getMessage());
          cn.jiguang.ai.a.g("JWake", paramContext.toString());
          return;
        }
      }
      else
      {
        if ((localObject1 == null) || (TextUtils.isEmpty((CharSequence)localObject1))) {
          break label339;
        }
        paramJSONObject = cn.jiguang.aa.c.a((String)localObject1);
      }
      paramJSONObject = a(paramContext, paramJSONObject);
      if (!cn.jiguang.aa.c.b(paramContext))
      {
        cn.jiguang.ai.a.g("JWake", "user has set Manifest not report");
        return;
      }
      if ((paramJSONObject != null) && (!paramJSONObject.isEmpty()))
      {
        paramJSONObject = cn.jiguang.aa.d.a(paramContext, paramJSONObject);
        if (paramJSONObject != null)
        {
          cn.jiguang.f.d.a(paramContext, paramJSONObject, "app_awake");
          cn.jiguang.f.d.a(paramContext, paramJSONObject);
          cn.jiguang.f.b.c(paramContext, "JWakecmd");
        }
        else
        {
          cn.jiguang.ai.a.g("JWake", "there no cmd report data");
          return;
          label339:
          cn.jiguang.ai.a.g("JWake", "there are no cache cmd wakeTarget");
          return;
        }
      }
    }
    catch (Throwable paramContext)
    {
      paramJSONObject = new StringBuilder();
      paramJSONObject.append("parse throwable:");
      paramJSONObject.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWake", paramJSONObject.toString());
    }
  }
  
  private List<cn.jiguang.z.c> e()
  {
    Object localObject1 = cn.jiguang.s.b.a(this.b, "bwc.catch");
    Object localObject2 = cn.jiguang.aa.b.b(this.b, (JSONObject)localObject1);
    localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = new cn.jiguang.z.a();
    }
    List localList = cn.jiguang.aa.c.a(this.b);
    if ((localList != null) && (!localList.isEmpty()))
    {
      localObject2 = new HashMap();
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        cn.jiguang.z.c localc = (cn.jiguang.z.c)localIterator.next();
        ((Map)localObject2).put(localc.a, localc);
      }
      localObject1 = cn.jiguang.aa.c.a((cn.jiguang.z.a)localObject1, new ArrayList(((Map)localObject2).keySet()));
      localList.clear();
      localIterator = ((List)localObject1).iterator();
      while (localIterator.hasNext())
      {
        localObject1 = (String)localIterator.next();
        if (((Map)localObject2).containsKey(localObject1)) {
          localList.add(((Map)localObject2).get(localObject1));
        }
      }
      return localList;
    }
    return null;
  }
  
  private cn.jiguang.z.a h(Context paramContext)
  {
    boolean bool = cn.jiguang.f.b.a(paramContext, "JWakeConfigHelper");
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("isRequestWakeConfigEnable:");
    ((StringBuilder)localObject).append(bool);
    cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject).toString());
    if (bool)
    {
      JSONObject localJSONObject = cn.jiguang.aa.b.a(paramContext);
      if (localJSONObject != null)
      {
        cn.jiguang.z.a locala = cn.jiguang.aa.b.b(paramContext, localJSONObject);
        localObject = locala;
        if (locala != null)
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("wakeConfig:");
          ((StringBuilder)localObject).append(locala);
          cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject).toString());
          cn.jiguang.aa.b.a(paramContext, localJSONObject);
          a(locala);
          localObject = locala;
        }
      }
      else
      {
        paramContext = cn.jiguang.aa.b.b(paramContext, cn.jiguang.s.b.a(paramContext, "bwc.catch"));
        localObject = paramContext;
        if (paramContext == null) {
          localObject = new cn.jiguang.z.a();
        }
      }
    }
    else
    {
      paramContext = cn.jiguang.aa.b.b(paramContext, cn.jiguang.s.b.a(paramContext, "bwc.catch"));
      localObject = paramContext;
      if (paramContext == null) {
        localObject = new cn.jiguang.z.a();
      }
    }
    return (cn.jiguang.z.a)localObject;
  }
  
  private WakedResultReceiver i(Context paramContext)
  {
    Object localObject = this.c;
    if (localObject != null) {
      return (WakedResultReceiver)localObject;
    }
    try
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).setAction("cn.jpush.android.intent.WakedReceiver");
      ((Intent)localObject).setPackage(paramContext.getPackageName());
      ((Intent)localObject).addCategory(paramContext.getPackageName());
      paramContext = paramContext.getPackageManager().queryBroadcastReceivers((Intent)localObject, 0);
      if ((paramContext != null) && (paramContext.size() != 0))
      {
        paramContext = (WakedResultReceiver)Class.forName(((ResolveInfo)paramContext.get(0)).activityInfo.name).newInstance();
        return paramContext;
      }
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("find waked receiver throwable:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWake", ((StringBuilder)localObject).toString());
    }
    return null;
  }
  
  public Object a(Context paramContext, Object paramObject)
  {
    if ((paramObject instanceof List))
    {
      paramObject = (List)paramObject;
      return cn.jiguang.aa.c.a(cn.jiguang.aa.b.b(paramContext, cn.jiguang.s.b.a(paramContext, "bwc.catch")), (List)paramObject);
    }
    return paramObject;
  }
  
  public void a(Context paramContext, Bundle paramBundle, int paramInt)
  {
    try
    {
      String str = d(paramContext);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("executeWakedAction: [");
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("] from broadcast");
      cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject).toString());
      boolean bool = cn.jiguang.f.d.o(paramContext);
      localObject = new cn/jiguang/y/a$e;
      ((e)localObject).<init>(this, paramContext, paramBundle, paramInt, bool);
      cn.jiguang.f.d.a("JWake", (Runnable)localObject);
    }
    catch (Throwable paramContext)
    {
      paramBundle = new StringBuilder();
      paramBundle.append("executeWakedAction failed:");
      paramBundle.append(paramContext.getLocalizedMessage());
      cn.jiguang.ai.a.c("JWake", paramBundle.toString());
    }
  }
  
  protected boolean a(Context paramContext, String paramString)
  {
    boolean bool3 = cn.jiguang.f.b.a(paramContext, "JWake");
    paramString = h(paramContext);
    boolean bool2 = false;
    if (paramString == null)
    {
      cn.jiguang.ai.a.g("JWake", "wakeConfig is null");
      return false;
    }
    boolean bool1 = cn.jiguang.f.b.i(paramContext, "JWakeComponentHelper");
    paramString = new StringBuilder();
    paramString.append("isComponentEnable:");
    paramString.append(bool1);
    cn.jiguang.ai.a.c("JWake", paramString.toString());
    cn.jiguang.aa.a.a(paramContext, bool1);
    bool1 = bool2;
    if (cn.jiguang.f.b.i(paramContext, "JWake"))
    {
      bool1 = bool2;
      if (bool3) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  protected boolean b(Context paramContext, String paramString)
  {
    return super.b(paramContext, paramString);
  }
  
  protected void c(Context paramContext, String paramString)
  {
    Object localObject = e();
    if ((localObject != null) && (!((List)localObject).isEmpty()))
    {
      JSONObject localJSONObject = cn.jiguang.aa.d.a(a(paramContext, (List)localObject));
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("wake json:");
      ((StringBuilder)localObject).append(localJSONObject);
      cn.jiguang.ai.a.c("JWake", ((StringBuilder)localObject).toString());
      cn.jiguang.aa.d.a(paramContext, "android_awake2", localJSONObject);
      super.c(paramContext, paramString);
      return;
    }
    cn.jiguang.ai.a.c("JWake", "there are no app need wake");
  }
  
  public void c(Context paramContext, JSONObject paramJSONObject)
  {
    String str2 = d(paramContext);
    String str1;
    if (paramJSONObject == null)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("executeWakeAction: [");
      localStringBuilder.append(str2);
      str1 = "] from cmd";
    }
    else
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("executeWakeAction: [");
      localStringBuilder.append(str2);
      str1 = "] from heartBeat";
    }
    localStringBuilder.append(str1);
    cn.jiguang.ai.a.c("JWake", localStringBuilder.toString());
    boolean bool = a();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str2);
    localStringBuilder.append(" isActionUserEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JWake", localStringBuilder.toString());
    if (bool) {
      cn.jiguang.f.d.a("JWake", new c(paramContext, paramJSONObject));
    }
  }
  
  public String d(Context paramContext)
  {
    this.b = paramContext;
    return "JWake";
  }
  
  /* Error */
  protected void d(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 289	cn/jiguang/aa/c:b	(Landroid/content/Context;)Z
    //   4: ifne +12 -> 16
    //   7: ldc 42
    //   9: ldc_w 418
    //   12: invokestatic 121	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   15: return
    //   16: ldc_w 601
    //   19: monitorenter
    //   20: aload_1
    //   21: ldc_w 601
    //   24: invokestatic 445	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONObject;
    //   27: astore 4
    //   29: ldc_w 601
    //   32: monitorexit
    //   33: aload 4
    //   35: astore_3
    //   36: aload 4
    //   38: ifnonnull +11 -> 49
    //   41: new 378	org/json/JSONObject
    //   44: astore_3
    //   45: aload_3
    //   46: invokespecial 602	org/json/JSONObject:<init>	()V
    //   49: aload_3
    //   50: ldc_w 376
    //   53: invokevirtual 606	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   56: astore 4
    //   58: aload 4
    //   60: astore_3
    //   61: aload 4
    //   63: ifnonnull +11 -> 74
    //   66: new 608	org/json/JSONArray
    //   69: astore_3
    //   70: aload_3
    //   71: invokespecial 609	org/json/JSONArray:<init>	()V
    //   74: aload_3
    //   75: invokevirtual 612	org/json/JSONArray:length	()I
    //   78: ifne +12 -> 90
    //   81: ldc 42
    //   83: ldc_w 614
    //   86: invokestatic 253	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   89: return
    //   90: aload_0
    //   91: aload_1
    //   92: aload_2
    //   93: invokespecial 616	cn/jiguang/f/a:d	(Landroid/content/Context;Ljava/lang/String;)V
    //   96: aload_1
    //   97: aload_1
    //   98: aload_3
    //   99: invokestatic 619	cn/jiguang/aa/d:a	(Landroid/content/Context;Lorg/json/JSONArray;)Lorg/json/JSONArray;
    //   102: invokestatic 431	cn/jiguang/f/d:a	(Landroid/content/Context;Ljava/lang/Object;)V
    //   105: ldc_w 601
    //   108: monitorenter
    //   109: aload_1
    //   110: ldc_w 601
    //   113: aconst_null
    //   114: invokestatic 408	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    //   117: pop
    //   118: ldc_w 601
    //   121: monitorexit
    //   122: goto +52 -> 174
    //   125: astore_1
    //   126: ldc_w 601
    //   129: monitorexit
    //   130: aload_1
    //   131: athrow
    //   132: astore_1
    //   133: ldc_w 601
    //   136: monitorexit
    //   137: aload_1
    //   138: athrow
    //   139: astore_1
    //   140: new 98	java/lang/StringBuilder
    //   143: dup
    //   144: invokespecial 99	java/lang/StringBuilder:<init>	()V
    //   147: astore_2
    //   148: aload_2
    //   149: ldc_w 621
    //   152: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_2
    //   157: aload_1
    //   158: invokevirtual 192	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   161: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: ldc 42
    //   167: aload_2
    //   168: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   171: invokestatic 121	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   174: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	175	0	this	a
    //   0	175	1	paramContext	Context
    //   0	175	2	paramString	String
    //   35	64	3	localObject1	Object
    //   27	35	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   109	122	125	finally
    //   126	130	125	finally
    //   20	33	132	finally
    //   133	137	132	finally
    //   16	20	139	java/lang/Throwable
    //   41	49	139	java/lang/Throwable
    //   49	58	139	java/lang/Throwable
    //   66	74	139	java/lang/Throwable
    //   74	89	139	java/lang/Throwable
    //   90	109	139	java/lang/Throwable
    //   130	132	139	java/lang/Throwable
    //   137	139	139	java/lang/Throwable
  }
  
  public Object e(Context paramContext)
  {
    return Boolean.valueOf(cn.jiguang.f.b.i(paramContext, "JWake"));
  }
  
  public void f(Context paramContext)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeWakeAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from first launch");
    cn.jiguang.ai.a.c("JWake", localStringBuilder.toString());
    cn.jiguang.f.d.a("JWake", new a(paramContext));
  }
  
  public void g(Context paramContext)
  {
    cn.jiguang.f.d.a("JWake", new b(paramContext, null));
  }
  
  public class a
    implements Runnable
  {
    private Context b;
    
    a(Context paramContext)
    {
      this.b = paramContext;
    }
    
    public void run()
    {
      try
      {
        a.this.d(this.b);
        if (!cn.jiguang.f.b.i(this.b, "JWake")) {
          cn.jiguang.ai.a.g("JWake", "can't wake because wakeConfig not allow");
        } else {
          a.this.c(this.b, "JWake");
        }
        if (a.this.b(this.b, "JWake")) {
          a.this.d(this.b, "JWake");
        }
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("dealAction throwable:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JWake", localStringBuilder.toString());
      }
    }
  }
  
  public class b
    implements Runnable
  {
    private Context b;
    
    private b(Context paramContext)
    {
      this.b = paramContext;
    }
    
    public void run()
    {
      try
      {
        cn.jiguang.r.a.a(this.b, cn.jiguang.r.a.f(this.b));
        a.a(a.this, this.b);
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("RegisterAction failed:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JWake", localStringBuilder.toString());
      }
    }
  }
  
  public class c
    implements Runnable
  {
    private Context b;
    private JSONObject c;
    
    c(Context paramContext, JSONObject paramJSONObject)
    {
      this.b = paramContext;
      this.c = paramJSONObject;
    }
    
    public void run()
    {
      try
      {
        long l1 = cn.jiguang.f.b.d(this.b, "JWakecmd");
        long l2 = System.currentTimeMillis();
        if ((this.c == null) && (l2 - l1 < 3600000L)) {
          cn.jiguang.ai.a.g("JWake", "is not cmd wake time");
        } else {
          a.a(a.this, this.b, this.c);
        }
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("WakeAction failed:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JWake", localStringBuilder.toString());
      }
    }
  }
  
  private class d
    implements ServiceConnection
  {
    private d() {}
    
    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
      if (paramComponentName == null) {
        return;
      }
      try
      {
        paramIBinder = new java/lang/StringBuilder;
        paramIBinder.<init>();
        paramIBinder.append(paramComponentName.getPackageName());
        paramIBinder.append(paramComponentName.getClassName());
        paramIBinder = paramIBinder.toString();
        if (TextUtils.isEmpty(paramIBinder)) {
          return;
        }
        a.a(a.this).remove(paramIBinder);
        paramComponentName = new java/lang/StringBuilder;
        paramComponentName.<init>();
        paramComponentName.append("cacheServiceConnectionMap remove ");
        paramComponentName.append(paramIBinder);
        cn.jiguang.ai.a.c("JWake", paramComponentName.toString());
        a.b(a.this).getApplicationContext().unbindService(this);
      }
      catch (Throwable paramIBinder)
      {
        paramComponentName = new StringBuilder();
        paramComponentName.append("onServiceConnected throwable");
        paramComponentName.append(paramIBinder.getMessage());
        cn.jiguang.ai.a.g("JWake", paramComponentName.toString());
      }
    }
    
    public void onServiceDisconnected(ComponentName paramComponentName) {}
  }
  
  public class e
    implements Runnable
  {
    private Context b;
    private Bundle c;
    private int d;
    private boolean e;
    
    e(Context paramContext, Bundle paramBundle, int paramInt, boolean paramBoolean)
    {
      this.b = paramContext;
      this.c = paramBundle;
      this.d = paramInt;
      this.e = paramBoolean;
    }
    
    public void run()
    {
      try
      {
        Object localObject2 = new java/lang/Object;
        localObject2.<init>();
        try
        {
          a.a(a.this, this.b, this.c, this.d, this.e);
        }
        finally {}
        StringBuilder localStringBuilder;
        return;
      }
      catch (Throwable localThrowable)
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("dealAction throwable:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JWake", localStringBuilder.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/y/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */