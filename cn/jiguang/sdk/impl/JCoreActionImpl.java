package cn.jiguang.sdk.impl;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ah.c;
import cn.jiguang.ah.e;
import cn.jiguang.ah.h;
import cn.jiguang.ah.i;
import cn.jiguang.api.JCoreAction;
import cn.jiguang.api.JCoreManager;

public class JCoreActionImpl
  extends JCoreAction
{
  private boolean a;
  
  private void a(Context paramContext)
  {
    for (;;)
    {
      int i;
      try
      {
        boolean bool = this.a;
        if (bool) {
          return;
        }
        if (paramContext == null) {
          return;
        }
        cn.jiguang.ai.a.c("JCoreActionImpl", "init jcore impl ,version:2.0.0");
        i = 1;
        this.a = true;
      }
      finally {}
      try
      {
        if ((JCoreManager.onEvent(paramContext, a.f, 32, "", null, new Object[0]) instanceof Bundle))
        {
          Object localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("hb:");
          ((StringBuilder)localObject).append(a.d);
          ((StringBuilder)localObject).append(",google:");
          ((StringBuilder)localObject).append(a.b);
          ((StringBuilder)localObject).append(",internal:");
          ((StringBuilder)localObject).append(a.c);
          cn.jiguang.ai.a.c("JCoreActionImpl", ((StringBuilder)localObject).toString());
          localObject = new android/os/Bundle;
          ((Bundle)localObject).<init>();
          if (!a.b) {
            if (!TextUtils.isEmpty("")) {
              i = 2;
            } else {
              i = 0;
            }
          }
          ((Bundle)localObject).putString("name", "core");
          ((Bundle)localObject).putInt("custom", i);
          ((Bundle)localObject).putInt("dynamic", 0);
          e.a(paramContext, "set_sdktype_info", localObject);
        }
      }
      catch (Throwable localThrowable) {}
    }
    c.b(paramContext);
    c.a(paramContext);
  }
  
  public void handleAction(Context paramContext, String paramString, Bundle paramBundle)
  {
    a(paramContext);
    if (TextUtils.isEmpty(paramString))
    {
      cn.jiguang.ai.a.h("JCoreActionImpl", "handleAction Failed,action is empty");
      return;
    }
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("handleAction action:");
    ((StringBuilder)localObject).append(paramString);
    cn.jiguang.ai.a.c("JCoreActionImpl", ((StringBuilder)localObject).toString());
    localObject = "";
    if (paramBundle != null) {
      localObject = paramBundle.getString("sdk_type");
    }
    if (paramString.equals("a1"))
    {
      if (paramBundle != null) {
        try
        {
          paramString = paramBundle.getString("report_data");
        }
        catch (Throwable paramString)
        {
          break label107;
        }
      } else {
        paramString = null;
      }
      b.a(paramContext, paramString);
      return;
      label107:
      paramContext = new StringBuilder();
      paramContext.append("report failed:");
      paramContext.append(paramString.getMessage());
      cn.jiguang.ai.a.g("JCoreActionImpl", paramContext.toString());
    }
    else if (paramString.startsWith("tcp_"))
    {
      i.a().a(paramContext, paramString, paramBundle);
    }
    else if (paramString.equals("a2"))
    {
      h.a().a(paramContext, true);
    }
    else if (paramString.equals("a3"))
    {
      cn.jiguang.ah.b.a().a(paramContext, (String)localObject, paramBundle);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/sdk/impl/JCoreActionImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */