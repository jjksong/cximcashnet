package cn.jiguang.sdk.impl;

import android.content.Context;
import android.os.Bundle;
import cn.jiguang.ah.i;
import cn.jiguang.api.JCoreManager;
import java.util.Map;

public class b
{
  public static long a(Context paramContext, long paramLong)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 19, null, null, new Object[] { Long.valueOf(paramLong) });
    if ((paramContext instanceof Long)) {
      return ((Long)paramContext).longValue();
    }
    return paramLong / 1000L;
  }
  
  public static Object a()
  {
    return JCoreManager.onEvent(null, a.f, 43, null, null, new Object[0]);
  }
  
  public static String a(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 5, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    JCoreManager.onEvent(paramContext, a.f, 57, null, null, new Object[] { Integer.valueOf(paramInt) });
  }
  
  public static void a(Context paramContext, long paramLong, String paramString1, String paramString2)
  {
    JCoreManager.onEvent(paramContext, a.f, 35, null, null, new Object[] { Long.valueOf(paramLong), paramString1, paramString2 });
  }
  
  public static void a(Context paramContext, Object paramObject)
  {
    JCoreManager.onEvent(paramContext, a.f, 39, null, null, new Object[] { paramObject });
  }
  
  public static void a(Context paramContext, String paramString)
  {
    JCoreManager.onEvent(paramContext, a.f, 36, null, null, new Object[] { paramString });
  }
  
  public static void a(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, byte[] paramArrayOfByte)
  {
    a(paramContext, paramString, paramInt1, paramInt2, paramLong1, paramLong2, paramArrayOfByte, true);
  }
  
  private static void a(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, byte[] paramArrayOfByte, boolean paramBoolean)
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("cmd", paramInt1);
    localBundle.putInt("ver", paramInt2);
    localBundle.putLong("rid", paramLong1);
    localBundle.putLong("timeout", paramLong2);
    localBundle.putByteArray("body", paramArrayOfByte);
    if (paramBoolean) {
      paramInt1 = 17;
    } else {
      paramInt1 = 16;
    }
    JCoreManager.onEvent(paramContext, paramString, paramInt1, null, localBundle, new Object[0]);
  }
  
  public static void a(Context paramContext, String paramString, long paramLong)
  {
    JCoreManager.onEvent(paramContext, a.f, 44, null, null, new Object[] { paramString, Long.valueOf(paramLong) });
  }
  
  public static void a(Context paramContext, boolean paramBoolean, long paramLong)
  {
    JCoreManager.onEvent(paramContext, a.f, 40, null, null, new Object[] { Boolean.valueOf(paramBoolean), Long.valueOf(paramLong) });
  }
  
  /* Error */
  public static void a(Bundle paramBundle)
  {
    // Byte code:
    //   0: ldc 90
    //   2: ldc 92
    //   4: invokestatic 98	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   7: aload_0
    //   8: ifnull +175 -> 183
    //   11: aload_0
    //   12: invokevirtual 102	android/os/Bundle:isEmpty	()Z
    //   15: ifeq +6 -> 21
    //   18: goto +165 -> 183
    //   21: ldc 103
    //   23: istore_2
    //   24: aload_0
    //   25: ldc 105
    //   27: getstatic 109	cn/jiguang/sdk/impl/a:d	I
    //   30: invokevirtual 113	android/os/Bundle:getInt	(Ljava/lang/String;I)I
    //   33: istore_3
    //   34: iload_3
    //   35: bipush 30
    //   37: if_icmpge +9 -> 46
    //   40: bipush 30
    //   42: istore_1
    //   43: goto +14 -> 57
    //   46: iload_3
    //   47: istore_1
    //   48: iload_3
    //   49: ldc 103
    //   51: if_icmple +6 -> 57
    //   54: ldc 103
    //   56: istore_1
    //   57: iload_1
    //   58: putstatic 109	cn/jiguang/sdk/impl/a:d	I
    //   61: new 115	java/lang/StringBuilder
    //   64: astore 4
    //   66: aload 4
    //   68: invokespecial 116	java/lang/StringBuilder:<init>	()V
    //   71: aload 4
    //   73: ldc 118
    //   75: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload 4
    //   81: iload_1
    //   82: invokevirtual 125	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: ldc 90
    //   88: aload 4
    //   90: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   93: invokestatic 98	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   96: aload_0
    //   97: ldc -125
    //   99: getstatic 134	cn/jiguang/sdk/impl/a:e	I
    //   102: invokevirtual 113	android/os/Bundle:getInt	(Ljava/lang/String;I)I
    //   105: istore_1
    //   106: iload_1
    //   107: ldc 103
    //   109: if_icmple +8 -> 117
    //   112: iload_2
    //   113: istore_1
    //   114: goto +3 -> 117
    //   117: iload_1
    //   118: istore_2
    //   119: iload_1
    //   120: getstatic 109	cn/jiguang/sdk/impl/a:d	I
    //   123: iconst_5
    //   124: iadd
    //   125: if_icmpge +9 -> 134
    //   128: getstatic 109	cn/jiguang/sdk/impl/a:d	I
    //   131: iconst_5
    //   132: iadd
    //   133: istore_2
    //   134: iload_2
    //   135: putstatic 134	cn/jiguang/sdk/impl/a:e	I
    //   138: new 115	java/lang/StringBuilder
    //   141: astore 4
    //   143: aload 4
    //   145: invokespecial 116	java/lang/StringBuilder:<init>	()V
    //   148: aload 4
    //   150: ldc -120
    //   152: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload 4
    //   158: iload_2
    //   159: invokevirtual 125	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: ldc 90
    //   165: aload 4
    //   167: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   170: invokestatic 98	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   173: aload_0
    //   174: ldc -118
    //   176: iconst_m1
    //   177: invokevirtual 113	android/os/Bundle:getInt	(Ljava/lang/String;I)I
    //   180: invokestatic 143	cn/jiguang/aj/b:a	(I)V
    //   183: return
    //   184: astore 4
    //   186: goto -90 -> 96
    //   189: astore 4
    //   191: goto -18 -> 173
    //   194: astore_0
    //   195: goto -12 -> 183
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	198	0	paramBundle	Bundle
    //   42	84	1	i	int
    //   23	136	2	j	int
    //   33	19	3	k	int
    //   64	102	4	localStringBuilder	StringBuilder
    //   184	1	4	localThrowable1	Throwable
    //   189	1	4	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   24	34	184	java/lang/Throwable
    //   57	96	184	java/lang/Throwable
    //   96	106	189	java/lang/Throwable
    //   119	134	189	java/lang/Throwable
    //   134	173	189	java/lang/Throwable
    //   173	183	194	java/lang/Throwable
  }
  
  public static void a(Runnable paramRunnable, int... paramVarArgs)
  {
    JCoreManager.onEvent(null, null, 12, null, null, new Object[] { paramRunnable });
  }
  
  public static void a(String paramString)
  {
    JCoreManager.onEvent(null, null, 13, paramString, null, new Object[0]);
  }
  
  public static void a(String paramString, Runnable paramRunnable, int... paramVarArgs)
  {
    JCoreManager.onEvent(null, null, 11, paramString, null, new Object[] { paramRunnable });
  }
  
  public static String b(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 8, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static void b(Context paramContext, int paramInt)
  {
    JCoreManager.onEvent(paramContext, a.f, 51, "", null, new Object[] { Integer.valueOf(paramInt) });
  }
  
  public static void b(Context paramContext, long paramLong)
  {
    if (paramLong > 0L) {
      JCoreManager.onEvent(paramContext, a.f, 37, null, null, new Object[] { Long.valueOf(paramLong) });
    }
  }
  
  public static void b(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, byte[] paramArrayOfByte)
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("cmd", paramInt1);
    localBundle.putInt("ver", paramInt2);
    localBundle.putLong("rid", paramLong1);
    localBundle.putLong("timeout", 0L);
    localBundle.putByteArray("body", paramArrayOfByte);
    localBundle.putLong("uid", paramLong2);
    JCoreManager.onEvent(paramContext, paramString, 59, null, localBundle, new Object[0]);
  }
  
  public static boolean b()
  {
    return i.a().d();
  }
  
  public static int c(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 33, null, null, new Object[0]);
    if ((paramContext instanceof Integer)) {
      return ((Integer)paramContext).intValue();
    }
    return 1;
  }
  
  public static String d(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 6, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static long e(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 20, null, null, new Object[0]);
    if ((paramContext instanceof Long)) {
      return ((Long)paramContext).longValue();
    }
    return 0L;
  }
  
  public static String f(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 34, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static String g(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 4, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static boolean h(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 21, null, null, new Object[0]);
    if ((paramContext instanceof Boolean)) {
      return ((Boolean)paramContext).booleanValue();
    }
    return false;
  }
  
  public static String i(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 7, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static long j(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 19, null, null, new Object[0]);
    if ((paramContext instanceof Long)) {
      return ((Long)paramContext).longValue();
    }
    return System.currentTimeMillis() / 1000L;
  }
  
  public static void k(Context paramContext)
  {
    JCoreManager.onEvent(paramContext, a.f, 38, null, null, new Object[0]);
  }
  
  public static Map l(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, a.f, 45, null, null, new Object[0]);
    if ((paramContext instanceof Map)) {
      return (Map)paramContext;
    }
    return null;
  }
  
  public static int m(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, a.f, 47, null, null, new Object[0]);
    if ((paramContext instanceof Integer)) {
      return ((Integer)paramContext).intValue();
    }
    return 0;
  }
  
  public static int n(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, a.f, 46, null, null, new Object[0]);
    if ((paramContext instanceof Integer)) {
      return ((Integer)paramContext).intValue();
    }
    return -1;
  }
  
  public static void o(Context paramContext)
  {
    JCoreManager.onEvent(paramContext, a.f, 48, null, null, new Object[0]);
  }
  
  public static boolean p(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, "", 53, null, null, new Object[0]);
    if ((paramContext instanceof Boolean)) {
      return ((Boolean)paramContext).booleanValue();
    }
    return true;
  }
  
  public static boolean q(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 58, null, null, new Object[0]);
    if ((paramContext instanceof Boolean)) {
      return ((Boolean)paramContext).booleanValue();
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/sdk/impl/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */