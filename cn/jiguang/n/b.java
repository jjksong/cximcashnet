package cn.jiguang.n;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import cn.jiguang.f.d;
import org.json.JSONException;
import org.json.JSONObject;

public class b
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile b c;
  private Context a;
  private JSONObject b;
  
  public static b d()
  {
    if (c == null) {
      try
      {
        b localb = new cn/jiguang/n/b;
        localb.<init>();
        c = localb;
      }
      finally {}
    }
    return c;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    try
    {
      paramString = new android/content/IntentFilter;
      paramString.<init>("android.intent.action.BATTERY_CHANGED");
      paramContext = paramContext.registerReceiver(null, paramString);
      if (paramContext == null) {
        return;
      }
      int j = paramContext.getIntExtra("level", -1);
      int k = paramContext.getIntExtra("scale", -1);
      int i = paramContext.getIntExtra("status", -1);
      switch (i)
      {
      default: 
        break;
      case 5: 
        i = 3;
        break;
      case 3: 
      case 4: 
        i = 1;
        break;
      case 2: 
        i = 2;
        break;
      case 1: 
        i = 0;
      }
      int m = paramContext.getIntExtra("voltage", -1);
      int n = paramContext.getIntExtra("temperature", -1);
      if (this.b == null)
      {
        paramContext = new org/json/JSONObject;
        paramContext.<init>();
        this.b = paramContext;
      }
      this.b.put("level", j);
      this.b.put("scale", k);
      this.b.put("status", i);
      this.b.put("voltage", m);
      this.b.put("temperature", n);
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append("collect success:");
      paramContext.append(this.b);
      cn.jiguang.ai.a.c("JDeviceBattery", paramContext.toString());
    }
    catch (JSONException paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("packageJson exception: ");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JDeviceBattery", paramString.toString());
    }
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JDeviceBattery";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    JSONObject localJSONObject = this.b;
    if (localJSONObject == null)
    {
      cn.jiguang.ai.a.g("JDeviceBattery", "there are no data to report");
      return;
    }
    d.a(paramContext, localJSONObject, "battery");
    d.a(paramContext, this.b);
    super.d(paramContext, paramString);
    this.b = null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/n/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */