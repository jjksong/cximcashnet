package cn.jiguang.n;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.f.d;
import cn.jiguang.o.b;
import org.json.JSONArray;
import org.json.JSONObject;

public class c
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile c c;
  private Context a;
  private boolean b;
  
  public static c d()
  {
    if (c == null) {
      try
      {
        c localc = new cn/jiguang/n/c;
        localc.<init>();
        c = localc;
      }
      finally {}
    }
    return c;
  }
  
  protected void a(String paramString, JSONObject paramJSONObject)
  {
    try
    {
      int i = paramJSONObject.optInt("cmd");
      paramString = paramJSONObject.optJSONObject("content");
      if (i != 51) {
        return;
      }
      i = 0;
      this.b = paramString.optBoolean("uploadnumber", false);
      String str2 = paramString.optString("version", "");
      String str1 = paramString.optString("app_id", "");
      String str3 = paramString.optString("app_secret", "");
      JSONArray localJSONArray = paramString.optJSONArray("carriers");
      if (localJSONArray != null) {
        while (i < localJSONArray.length())
        {
          paramString = localJSONArray.getJSONObject(i);
          if (paramString != null)
          {
            String str4 = paramString.optString("carrier", "");
            paramJSONObject = paramString.optString("url", "");
            if ((!TextUtils.isEmpty(str4)) && (!TextUtils.isEmpty(paramJSONObject)))
            {
              int j = cn.jiguang.r.a.a(str4);
              paramString = paramJSONObject;
              if (!paramJSONObject.startsWith("http://"))
              {
                paramString = new java/lang/StringBuilder;
                paramString.<init>();
                paramString.append("http://");
                paramString.append(paramJSONObject);
                paramString = paramString.toString();
              }
              paramJSONObject = paramString;
              if (!paramString.endsWith("/"))
              {
                paramJSONObject = new java/lang/StringBuilder;
                paramJSONObject.<init>();
                paramJSONObject.append(paramString);
                paramJSONObject.append("/");
                paramJSONObject = paramJSONObject.toString();
              }
              if (j != -1) {
                b.a(this.a, j, paramJSONObject);
              }
              paramString = new java/lang/StringBuilder;
              paramString.<init>();
              paramString.append("carrier:");
              paramString.append(str4);
              paramString.append(" url:");
              paramString.append(paramJSONObject);
              paramString.append(" providerIndex:");
              paramString.append(j);
              cn.jiguang.ai.a.c("JDevicePhoneNumber", paramString.toString());
            }
          }
          i++;
        }
      }
      if (!TextUtils.isEmpty(str2)) {
        b.a(this.a, str2);
      }
      if (TextUtils.isEmpty(str1)) {
        b.b(this.a, str1);
      }
      if (TextUtils.isEmpty(str3)) {
        b.c(this.a, str3);
      }
    }
    catch (Throwable paramString)
    {
      paramJSONObject = new StringBuilder();
      paramJSONObject.append("parse throwable:");
      paramJSONObject.append(paramString.getMessage());
      cn.jiguang.ai.a.g("JDevicePhoneNumber", paramJSONObject.toString());
    }
  }
  
  protected boolean b()
  {
    return this.b;
  }
  
  protected void c(Context paramContext, String paramString) {}
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JDevicePhoneNumber";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    try
    {
      b.a(paramContext, true);
      if (!d.k(paramContext).toUpperCase().startsWith("WIFI"))
      {
        paramString = new cn/jiguang/r/c;
        paramString.<init>(paramContext);
        paramString.a();
      }
      else
      {
        cn.jiguang.ai.a.g("JDevicePhoneNumber", "collect failed because current networkType is  wifi");
      }
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("report throwable:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JDevicePhoneNumber", paramString.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/n/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */