package cn.jiguang.n;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import cn.jiguang.api.ReportCallBack;
import cn.jiguang.f.b;
import cn.jiguang.f.c.a;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a c;
  private Context a;
  private JSONObject b;
  private String d;
  
  public static a d()
  {
    if (c == null) {
      try
      {
        a locala = new cn/jiguang/n/a;
        locala.<init>();
        c = locala;
      }
      finally {}
    }
    return c;
  }
  
  private JSONObject f(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jiguang.ai.a.g("JDevice", "when getDeviceInfo, context can't be null");
      return null;
    }
    Object localObject;
    try
    {
      String str13 = cn.jiguang.r.a.a();
      String str12 = cn.jiguang.r.a.a(paramContext);
      String str11 = String.format(Locale.ENGLISH, "%.1f", new Object[] { Double.valueOf(cn.jiguang.r.a.b(paramContext)) });
      String str10 = String.format(Locale.ENGLISH, Build.VERSION.RELEASE, new Object[0]);
      String str9 = String.format(Locale.ENGLISH, Build.MODEL, new Object[0]);
      String str8 = String.format(Locale.ENGLISH, Build.BRAND, new Object[0]);
      String str7 = String.format(Locale.ENGLISH, Build.PRODUCT, new Object[0]);
      String str2;
      if (c.a.b) {
        str2 = "";
      } else {
        str2 = String.format(Locale.ENGLISH, Build.SERIAL, new Object[0]);
      }
      String str6 = String.format(Locale.ENGLISH, Build.FINGERPRINT, new Object[0]);
      String str5 = paramContext.getResources().getConfiguration().locale.toString();
      String str4 = String.format(Locale.ENGLISH, Build.MANUFACTURER, new Object[0]);
      long l1 = TimeZone.getDefault().getRawOffset() / 3600000L;
      String str3 = cn.jiguang.f.d.c(paramContext, "");
      String str1;
      if (l1 > 0L)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("+");
        ((StringBuilder)localObject).append(l1);
        str1 = ((StringBuilder)localObject).toString();
      }
      else
      {
        if (l1 < 0L)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("-");
          ((StringBuilder)localObject).append(l1);
        }
        for (;;)
        {
          str1 = ((StringBuilder)localObject).toString();
          break;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("");
          ((StringBuilder)localObject).append(l1);
        }
      }
      long l2 = cn.jiguang.r.a.c(paramContext);
      l1 = cn.jiguang.r.a.d(paramContext);
      int j = cn.jiguang.r.a.c();
      String str14 = cn.jiguang.r.a.b();
      int i = cn.jiguang.r.a.d();
      if (c.a.b) {
        localObject = "";
      } else {
        localObject = cn.jiguang.r.d.a(paramContext);
      }
      JSONArray localJSONArray = cn.jiguang.r.a.e(paramContext);
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      paramContext = str13;
      if (TextUtils.isEmpty(str13)) {
        paramContext = "";
      }
      localJSONObject.put("cpu_info", paramContext);
      localJSONObject.put("cpu_count", j);
      localJSONObject.put("cpu_max_freq", i);
      localJSONObject.put("cpu_hardware", str14);
      localJSONObject.put("ram", l2);
      localJSONObject.put("rom", l1);
      paramContext = str12;
      if (TextUtils.isEmpty(str12)) {
        paramContext = "";
      }
      localJSONObject.put("resolution", paramContext);
      paramContext = str11;
      if (TextUtils.isEmpty(str11)) {
        paramContext = "";
      }
      localJSONObject.put("screensize", paramContext);
      paramContext = str10;
      if (TextUtils.isEmpty(str10)) {
        paramContext = "";
      }
      localJSONObject.put("os_version", paramContext);
      paramContext = str9;
      if (TextUtils.isEmpty(str9)) {
        paramContext = "";
      }
      localJSONObject.put("model", paramContext);
      paramContext = str8;
      if (TextUtils.isEmpty(str8)) {
        paramContext = "";
      }
      localJSONObject.put("brand", paramContext);
      paramContext = str7;
      if (TextUtils.isEmpty(str7)) {
        paramContext = "";
      }
      localJSONObject.put("product", paramContext);
      if (TextUtils.isEmpty(str2)) {
        str2 = "";
      }
      localJSONObject.put("serial", str2);
      if (TextUtils.isEmpty(str6)) {
        paramContext = "";
      } else {
        paramContext = str6;
      }
      localJSONObject.put("fingerprint", paramContext);
      if (TextUtils.isEmpty(str5)) {
        paramContext = "";
      } else {
        paramContext = str5;
      }
      localJSONObject.put("language", paramContext);
      if (TextUtils.isEmpty(str4)) {
        paramContext = "";
      } else {
        paramContext = str4;
      }
      localJSONObject.put("manufacturer", paramContext);
      if (TextUtils.isEmpty(str1)) {
        paramContext = "";
      } else {
        paramContext = str1;
      }
      localJSONObject.put("timezone", paramContext);
      if (TextUtils.isEmpty(str3)) {
        paramContext = "";
      } else {
        paramContext = str3;
      }
      localJSONObject.put("mac", paramContext);
      paramContext = (Context)localObject;
      if (TextUtils.isEmpty((CharSequence)localObject)) {
        paramContext = "";
      }
      localJSONObject.put("meid", paramContext);
      localJSONObject.put("sim_slots", localJSONArray);
      return localJSONObject;
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("getDeviceInfo exception: ");
      paramContext = paramContext.getMessage();
    }
    catch (JSONException paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("package json exception: ");
      paramContext = paramContext.getMessage();
    }
    ((StringBuilder)localObject).append(paramContext);
    cn.jiguang.ai.a.g("JDevice", ((StringBuilder)localObject).toString());
    return null;
  }
  
  private static String g(Context paramContext)
  {
    try
    {
      String str4 = cn.jiguang.f.d.a(paramContext);
      String str1 = cn.jiguang.f.d.b(paramContext);
      Object localObject1 = "";
      Object localObject2;
      try
      {
        localObject2 = paramContext.getPackageName();
        localObject1 = localObject2;
        localObject3 = paramContext.getPackageManager().getPackageInfo((String)localObject2, 0);
        paramContext = (Context)localObject2;
        localObject2 = localObject3;
      }
      catch (Throwable paramContext)
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append("getPackageManager failed:");
        ((StringBuilder)localObject2).append(paramContext.getMessage());
        cn.jiguang.ai.a.g("JDevice", ((StringBuilder)localObject2).toString());
        localObject2 = null;
        paramContext = (Context)localObject1;
      }
      if (localObject2 == null) {
        localObject1 = "";
      } else {
        localObject1 = ((PackageInfo)localObject2).versionName;
      }
      if (localObject2 == null) {
        localObject2 = "";
      } else {
        localObject2 = String.valueOf(((PackageInfo)localObject2).versionCode);
      }
      String str3 = cn.jiguang.f.d.a();
      String str2 = String.valueOf(cn.jiguang.f.d.b());
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(str4);
      localStringBuilder.append(",");
      Object localObject3 = str1;
      if (TextUtils.isEmpty(str1)) {
        localObject3 = "";
      }
      localStringBuilder.append((String)localObject3);
      localStringBuilder.append(",");
      localObject3 = localObject1;
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        localObject3 = "";
      }
      localStringBuilder.append((String)localObject3);
      localStringBuilder.append(",");
      localObject1 = localObject2;
      if (TextUtils.isEmpty((CharSequence)localObject2)) {
        localObject1 = "";
      }
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append(",");
      localObject1 = str3;
      if (TextUtils.isEmpty(str3)) {
        localObject1 = "";
      }
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append(",");
      localObject1 = str2;
      if (TextUtils.isEmpty(str2)) {
        localObject1 = "";
      }
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append(",");
      localObject1 = paramContext;
      if (TextUtils.isEmpty(paramContext)) {
        localObject1 = "";
      }
      localStringBuilder.append((String)localObject1);
      paramContext = localStringBuilder.toString();
      return paramContext;
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("getCurrentCondition throwable: ");
      paramContext.append(localThrowable.getMessage());
      cn.jiguang.ai.a.g("JDevice", paramContext.toString());
    }
    return null;
  }
  
  protected boolean b(Context paramContext, String paramString)
  {
    if (!b.b(paramContext, paramString)) {
      return false;
    }
    Object localObject = this.b;
    if (localObject == null)
    {
      cn.jiguang.ai.a.g("JDevice", "there are no data to report");
      return false;
    }
    localObject = ((JSONObject)localObject).toString();
    if (TextUtils.isEmpty((CharSequence)localObject)) {
      return false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(g(paramContext));
    this.d = cn.jiguang.f.d.d(localStringBuilder.toString());
    localObject = b.f(paramContext);
    if ((!TextUtils.isEmpty(this.d)) && (!TextUtils.equals(this.d, (CharSequence)localObject)))
    {
      cn.jiguang.ai.a.c("JDevice", "device detail is change");
      return super.b(paramContext, paramString);
    }
    cn.jiguang.ai.a.c("JDevice", "device detail is not change");
    return false;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    this.b = f(paramContext);
    if (this.b != null)
    {
      paramContext = new StringBuilder();
      paramContext.append("collect success:");
      paramContext.append(this.b);
      cn.jiguang.ai.a.c("JDevice", paramContext.toString());
    }
    else
    {
      cn.jiguang.ai.a.g("JDevice", "collect failed");
    }
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JDevice";
  }
  
  protected void d(final Context paramContext, final String paramString)
  {
    JSONObject localJSONObject = this.b;
    if (localJSONObject == null)
    {
      cn.jiguang.ai.a.c("JDevice", "there are no data to report");
      return;
    }
    cn.jiguang.f.d.a(paramContext, localJSONObject, "device_info");
    cn.jiguang.f.d.a(paramContext, this.b, new ReportCallBack()
    {
      public void onFinish(int paramAnonymousInt)
      {
        if (paramAnonymousInt != 0) {
          return;
        }
        b.f(paramContext, paramString);
        if (TextUtils.isEmpty(a.a(a.this))) {
          return;
        }
        b.p(paramContext, a.a(a.this));
        a.a(a.this, paramContext, paramString);
      }
    });
    this.b = null;
  }
  
  public Object e(Context paramContext)
  {
    return f(paramContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/n/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */