package cn.jiguang.x;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.f.b;
import cn.jiguang.f.d;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a b;
  private Context a;
  private Bundle c;
  private String d = "";
  private int e = 0;
  private int f = 0;
  
  private JSONObject a(String paramString, int paramInt1, int paramInt2)
  {
    try
    {
      JSONObject localJSONObject1 = new org/json/JSONObject;
      localJSONObject1.<init>();
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>();
      JSONObject localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      localJSONArray.put(paramInt1);
      localJSONArray.put(paramInt2);
      localJSONObject2.put(paramString, localJSONArray);
      localJSONObject1.put("type", "sdk_type");
      localJSONObject1.put("sdk", localJSONObject2);
      return localJSONObject1;
    }
    catch (JSONException localJSONException)
    {
      paramString = new StringBuilder();
      paramString.append("package json exception: ");
      paramString.append(localJSONException.getMessage());
      cn.jiguang.ai.a.g("JType", paramString.toString());
    }
    return null;
  }
  
  private static boolean a(Context paramContext, String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    if ((!TextUtils.isEmpty(paramString2)) && (paramInt1 >= 0) && (paramInt2 >= 0))
    {
      paramContext = b.k(paramContext, paramString1);
      paramString1 = new StringBuilder();
      paramString1.append(paramInt1);
      paramString1.append(",");
      paramString1.append(paramInt2);
      return paramContext.equals(paramString1.toString()) ^ true;
    }
    return false;
  }
  
  public static a d()
  {
    if (b == null) {
      try
      {
        a locala = new cn/jiguang/x/a;
        locala.<init>();
        b = locala;
      }
      finally {}
    }
    return b;
  }
  
  protected void a(String paramString, Bundle paramBundle)
  {
    this.c = paramBundle;
  }
  
  protected void c(Context paramContext, String paramString) {}
  
  protected boolean c()
  {
    Object localObject1 = this.c;
    if (localObject1 == null) {
      return false;
    }
    this.d = ((Bundle)localObject1).getString("name");
    this.e = this.c.getInt("custom", 0);
    this.f = this.c.getInt("dynamic", 0);
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("parseBundle type:");
    ((StringBuilder)localObject1).append(this.d);
    ((StringBuilder)localObject1).append(",custom:");
    ((StringBuilder)localObject1).append(this.e);
    ((StringBuilder)localObject1).append(",dynamic:");
    ((StringBuilder)localObject1).append(this.f);
    cn.jiguang.ai.a.c("JType", ((StringBuilder)localObject1).toString());
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("JType");
    ((StringBuilder)localObject1).append(this.d);
    localObject1 = ((StringBuilder)localObject1).toString();
    boolean bool = a(this.a, (String)localObject1, this.d, this.e, this.f);
    if (!bool)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("type [");
      ((StringBuilder)localObject1).append(this.d);
      ((StringBuilder)localObject1).append("] data not change");
      cn.jiguang.ai.a.c("JType", ((StringBuilder)localObject1).toString());
    }
    else
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(this.e);
      ((StringBuilder)localObject2).append(",");
      ((StringBuilder)localObject2).append(this.f);
      localObject2 = ((StringBuilder)localObject2).toString();
      b.a(this.a, (String)localObject1, (String)localObject2);
    }
    return bool;
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JType";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    paramString = a(this.d, this.e, this.f);
    if (paramString == null)
    {
      cn.jiguang.ai.a.g("JType", "there are no data to report");
      return;
    }
    d.a(paramContext, paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/x/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */