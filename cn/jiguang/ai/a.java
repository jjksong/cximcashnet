package cn.jiguang.ai;

import cn.jiguang.api.JCoreManager;

public class a
{
  private static final String a = cn.jiguang.sdk.impl.a.f;
  
  private static void a(int paramInt, boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable)
  {
    JCoreManager.onEvent(null, a, 18, paramString1, null, new Object[] { Integer.valueOf(paramInt), Boolean.valueOf(paramBoolean), paramString2, paramThrowable });
  }
  
  public static void a(String paramString1, String paramString2)
  {
    a(2, true, paramString1, paramString2, null);
  }
  
  public static void a(String paramString1, String paramString2, Throwable paramThrowable)
  {
    a(5, true, paramString1, paramString2, paramThrowable);
  }
  
  public static void b(String paramString1, String paramString2)
  {
    a(2, false, paramString1, paramString2, null);
  }
  
  public static void b(String paramString1, String paramString2, Throwable paramThrowable)
  {
    a(6, true, paramString1, paramString2, paramThrowable);
  }
  
  public static void c(String paramString1, String paramString2)
  {
    a(3, true, paramString1, paramString2, null);
  }
  
  public static void c(String paramString1, String paramString2, Throwable paramThrowable)
  {
    a(6, false, paramString1, paramString2, paramThrowable);
  }
  
  public static void d(String paramString1, String paramString2)
  {
    a(3, false, paramString1, paramString2, null);
  }
  
  public static void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    c(paramString1, paramString2, paramThrowable);
    if (!JCoreManager.isInternal()) {
      return;
    }
    throw new RuntimeException(paramThrowable);
  }
  
  public static void e(String paramString1, String paramString2)
  {
    a(4, true, paramString1, paramString2, null);
  }
  
  public static void f(String paramString1, String paramString2)
  {
    a(4, false, paramString1, paramString2, null);
  }
  
  public static void g(String paramString1, String paramString2)
  {
    a(5, true, paramString1, paramString2, null);
  }
  
  public static void h(String paramString1, String paramString2)
  {
    a(5, false, paramString1, paramString2, null);
  }
  
  public static void i(String paramString1, String paramString2)
  {
    a(6, true, paramString1, paramString2, null);
  }
  
  public static void j(String paramString1, String paramString2)
  {
    a(6, false, paramString1, paramString2, null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ai/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */