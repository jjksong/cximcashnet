package cn.jiguang.ad;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static volatile a c;
  private static final Object d = new Object();
  private long a;
  private Map<String, Set<String>> b = new HashMap();
  
  public static a a()
  {
    if (c == null) {
      synchronized (d)
      {
        if (c == null)
        {
          a locala = new cn/jiguang/ad/a;
          locala.<init>();
          c = locala;
        }
      }
    }
    return c;
  }
  
  private void a(JSONObject paramJSONObject)
  {
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0)) {}
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      Iterator localIterator = paramJSONObject.keys();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        JSONArray localJSONArray = paramJSONObject.optJSONArray(str);
        LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
        localLinkedHashSet.<init>();
        if (localJSONArray != null) {
          for (int i = 0; i < localJSONArray.length(); i++) {
            localLinkedHashSet.add(localJSONArray.getString(i));
          }
        }
        localHashMap.put(str, localLinkedHashSet);
      }
      if (!localHashMap.isEmpty()) {
        this.b = localHashMap;
      }
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;) {}
    }
  }
  
  public a a(Context paramContext)
  {
    try
    {
      long l = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.t())).longValue();
      if ((this.a == 0L) || (this.a != l))
      {
        this.a = l;
        String str = (String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.r());
        boolean bool = TextUtils.isEmpty(str);
        if (!bool)
        {
          paramContext = new org/json/JSONObject;
          paramContext.<init>(str);
          a(paramContext);
        }
      }
    }
    catch (Throwable|JSONException paramContext)
    {
      for (;;) {}
    }
    return this;
  }
  
  public String a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return "normal";
    }
    int i = -1;
    switch (paramString.hashCode())
    {
    default: 
      break;
    case 1973539834: 
      if (paramString.equals("identify_account")) {
        i = 8;
      }
      break;
    case 1350272347: 
      if (paramString.equals("android_awake_target")) {
        i = 3;
      }
      break;
    case 907150721: 
      if (paramString.equals("detach_account")) {
        i = 9;
      }
      break;
    case 93223301: 
      if (paramString.equals("awake")) {
        i = 4;
      }
      break;
    case -31313123: 
      if (paramString.equals("android_awake2")) {
        i = 0;
      }
      break;
    case -693746763: 
      if (paramString.equals("android_awake")) {
        i = 1;
      }
      break;
    case -820729752: 
      if (paramString.equals("active_terminate")) {
        i = 6;
      }
      break;
    case -1039745817: 
      if (paramString.equals("normal")) {
        i = 11;
      }
      break;
    case -1051289244: 
      if (paramString.equals("active_user")) {
        i = 7;
      }
      break;
    case -1091230153: 
      if (paramString.equals("android_awake_target2")) {
        i = 2;
      }
      break;
    case -1177318867: 
      if (paramString.equals("account")) {
        i = 10;
      }
      break;
    case -1245458676: 
      if (paramString.equals("active_launch")) {
        i = 5;
      }
      break;
    }
    String str;
    switch (i)
    {
    default: 
      str = paramString;
      if (this.b.containsKey(paramString)) {
        break;
      }
    case 11: 
      str = "normal";
      break;
    case 8: 
    case 9: 
    case 10: 
      str = "account";
      break;
    case 5: 
    case 6: 
    case 7: 
      str = "active_user";
      break;
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
      str = "awake";
    }
    return str;
  }
  
  public String a(Set<String> paramSet)
  {
    if (paramSet != null) {}
    try
    {
      if (!paramSet.isEmpty())
      {
        String str = null;
        Iterator localIterator = paramSet.iterator();
        paramSet = str;
        while (localIterator.hasNext())
        {
          str = a((String)localIterator.next());
          if (paramSet == null)
          {
            paramSet = str;
          }
          else if (!paramSet.equals(str))
          {
            c.f("AddressGroupManager", "Report JSONArray belong more than one space, using normal-space");
            return "normal";
          }
        }
        return paramSet;
      }
      return "normal";
    }
    catch (Throwable paramSet) {}
    return "normal";
  }
  
  public void a(Context paramContext, JSONObject paramJSONObject)
  {
    Object localObject = paramJSONObject.optJSONArray("sis_ips");
    l2 = 3600000L;
    try
    {
      l1 = paramJSONObject.getLong("ttl");
      if (l1 < 0L) {
        l1 = l2;
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        JSONObject localJSONObject;
        cn.jiguang.e.a locala1;
        cn.jiguang.e.a locala3;
        cn.jiguang.e.a locala2;
        long l1 = l2;
      }
    }
    localJSONObject = paramJSONObject.optJSONObject("ips");
    locala1 = cn.jiguang.e.a.s().a(((JSONArray)localObject).toString());
    locala3 = cn.jiguang.e.a.u().a(Long.valueOf(l1 * 1000L));
    locala2 = cn.jiguang.e.a.t().a(Long.valueOf(System.currentTimeMillis()));
    localObject = cn.jiguang.e.a.r();
    if (localJSONObject != null) {
      paramJSONObject = localJSONObject.toString();
    } else {
      paramJSONObject = "";
    }
    cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { locala1, locala3, locala2, ((cn.jiguang.e.a)localObject).a(paramJSONObject) });
    a(localJSONObject);
  }
  
  public a b(Context paramContext)
  {
    try
    {
      long l3 = System.currentTimeMillis();
      a(paramContext);
      long l2 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.u())).longValue();
      long l1;
      if (l2 < 0L)
      {
        l1 = 3600000L;
      }
      else
      {
        l1 = l2;
        if (l2 < 60000L) {
          l1 = 60000L;
        }
      }
      l2 = l1;
      if (l1 > 604800000L) {
        l2 = 604800000L;
      }
      SimpleDateFormat localSimpleDateFormat = cn.jiguang.as.b.a("yyyy-MM-dd HH:mm:ss");
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("lastUpdateTime=");
      Date localDate = new java/util/Date;
      localDate.<init>(this.a);
      localStringBuilder.append(localSimpleDateFormat.format(localDate));
      localStringBuilder.append(" now=");
      localDate = new java/util/Date;
      localDate.<init>(l3);
      localStringBuilder.append(localSimpleDateFormat.format(localDate));
      localStringBuilder.append(" expire=");
      localStringBuilder.append(l2 / 1000L);
      c.b("AddressGroupManager", localStringBuilder.toString());
      if ((this.a == 0L) || (this.a + l2 < l3))
      {
        c.b("AddressGroupManager", "cache invalid, fetch new urls");
        boolean bool;
        if ((this.b != null) && (!this.b.isEmpty())) {
          bool = false;
        } else {
          bool = true;
        }
        e.a(paramContext, bool);
      }
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("refresh e");
      paramContext.append(localThrowable);
      c.f("AddressGroupManager", paramContext.toString());
    }
    return this;
  }
  
  public Set<String> b(Set<String> paramSet)
  {
    Object localObject;
    Iterator localIterator;
    if ((paramSet != null) && (!paramSet.isEmpty()))
    {
      localObject = null;
      localIterator = paramSet.iterator();
      paramSet = (Set<String>)localObject;
    }
    while (localIterator.hasNext())
    {
      localObject = a((String)localIterator.next());
      localObject = (Set)this.b.get(localObject);
      if ((localObject != null) && (!((Set)localObject).isEmpty()))
      {
        if (paramSet != null)
        {
          paramSet.retainAll((Collection)localObject);
          localObject = paramSet;
        }
        paramSet = (Set<String>)localObject;
        if (!((Set)localObject).isEmpty()) {
          break;
        }
      }
      else
      {
        return (Set)this.b.get("normal");
      }
    }
    return paramSet;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */