package cn.jiguang.ad;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e
{
  private static a a;
  private static volatile FutureTask<?> b;
  private static final Object c = new Object();
  private static final LinkedHashSet<String> d = new LinkedHashSet();
  private static final LinkedHashSet<String> e = new LinkedHashSet();
  
  static
  {
    d.add("https://tsis.jpush.cn");
  }
  
  public static LinkedHashSet<String> a()
  {
    if ((cn.jiguang.a.a.a()) && (!e.isEmpty())) {
      return e;
    }
    return d;
  }
  
  public static void a(Context arg0, boolean paramBoolean)
  {
    if (a == null) {
      a = new a(???, null);
    }
    if ((b == null) || (b.isCancelled()) || (b.isDone())) {}
    synchronized (c)
    {
      if ((b != null) && (!b.isCancelled()))
      {
        boolean bool = b.isDone();
        if (!bool) {}
      }
      else
      {
        try
        {
          localObject1 = new java/util/concurrent/FutureTask;
          ((FutureTask)localObject1).<init>(a, null);
          b = (FutureTask)localObject1;
          cn.jiguang.ar.a.a("ASYNC", b);
        }
        catch (Throwable localThrowable2)
        {
          Object localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("new sis task e:");
          ((StringBuilder)localObject1).append(localThrowable2);
          c.f("ReportSis", ((StringBuilder)localObject1).toString());
        }
      }
      if (paramBoolean)
      {
        try
        {
          b.get(10L, TimeUnit.SECONDS);
        }
        catch (Throwable localThrowable1)
        {
          ??? = new StringBuilder();
        }
        catch (TimeoutException localTimeoutException)
        {
          ??? = new StringBuilder();
        }
        catch (ExecutionException localExecutionException)
        {
          ??? = new StringBuilder();
        }
        catch (InterruptedException localInterruptedException)
        {
          ??? = new StringBuilder();
        }
        ???.append("sis task e:");
        ???.append(localInterruptedException);
        c.f("ReportSis", ???.toString());
      }
      return;
    }
  }
  
  private static boolean b(Context paramContext, String paramString1, String paramString2)
  {
    paramString2 = b.a(paramString1, paramString2, paramContext, true, 3, 2);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("report sis code[");
    localStringBuilder.append(paramString2.a());
    localStringBuilder.append("] from url=");
    localStringBuilder.append(paramString1);
    localStringBuilder.append("\n body=");
    localStringBuilder.append(paramString2.b());
    c.b("ReportSis", localStringBuilder.toString());
    if (paramString2.a() == 0)
    {
      paramString1 = paramString2.b();
      if (!TextUtils.isEmpty(paramString1)) {
        try
        {
          paramString2 = new org/json/JSONObject;
          paramString2.<init>(paramString1);
          paramString1 = paramString2.getJSONObject("ret");
          a.a().a(paramContext, paramString1);
          return true;
        }
        catch (Throwable paramContext)
        {
          paramString1 = new StringBuilder();
          paramString1.append("getUrls e:");
          paramString1.append(paramContext);
          c.f("ReportSis", paramString1.toString());
        }
      }
    }
    return false;
  }
  
  private static String c(Context paramContext)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      String str1 = cn.jiguang.ab.e.f(paramContext);
      l2 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.c())).longValue();
      int i = cn.jiguang.as.e.c(paramContext);
      String str2 = cn.jiguang.as.e.e(paramContext);
      localJSONObject.put("type", i);
      localJSONObject.put("appkey", str1);
      localJSONObject.put("sdkver", "2.0.0");
      localJSONObject.put("platform", 0);
      l1 = 0L;
      if (l2 != 0L) {
        localJSONObject.put("uid", l2);
      }
      if (str2 != null) {
        localJSONObject.put("opera", str2);
      }
      paramContext = cn.jiguang.as.e.a(paramContext, "get_loc_info", null);
      boolean bool = paramContext instanceof Bundle;
      d1 = 200.0D;
      if (!bool) {}
    }
    catch (Throwable paramContext)
    {
      long l2;
      long l1;
      double d1;
      double d3;
      double d2;
      label175:
      for (;;) {}
    }
    try
    {
      paramContext = (Bundle)paramContext;
      d3 = paramContext.getDouble("lat");
    }
    catch (Throwable paramContext)
    {
      break label189;
    }
    try
    {
      d2 = paramContext.getDouble("lot");
      d1 = d2;
      l2 = paramContext.getLong("time");
      l1 = l2;
      d1 = d2;
    }
    catch (Throwable paramContext)
    {
      break label175;
    }
    d2 = d3;
    d3 = d1;
    d1 = d2;
    d2 = d3;
    break label193;
    label189:
    d2 = 200.0D;
    label193:
    if ((d1 > -90.0D) && (d1 < 90.0D) && (d2 > -180.0D) && (d2 < 180.0D))
    {
      localJSONObject.put("lat", d1);
      localJSONObject.put("lng", d2);
      localJSONObject.put("time", l1);
    }
    return localJSONObject.toString();
  }
  
  private static LinkedHashSet<String> d(Context paramContext)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    paramContext = (String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.s());
    if (TextUtils.isEmpty(paramContext)) {
      return localLinkedHashSet;
    }
    try
    {
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>(paramContext);
      for (int i = 0; i < localJSONArray.length(); i++) {
        localLinkedHashSet.add(localJSONArray.optString(i));
      }
    }
    catch (JSONException paramContext)
    {
      for (;;) {}
    }
    return localLinkedHashSet;
  }
  
  private static class a
    implements Runnable
  {
    private final Context a;
    
    private a(Context paramContext)
    {
      this.a = paramContext;
    }
    
    public void run()
    {
      try
      {
        String str = e.a(this.a);
        Object localObject2 = e.b(this.a);
        ((LinkedHashSet)localObject2).addAll(e.a());
        Object localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append("sis urls=");
        ((StringBuilder)localObject1).append(((LinkedHashSet)localObject2).toString());
        ((StringBuilder)localObject1).append(" post json=");
        ((StringBuilder)localObject1).append(str);
        c.b("ReportSis", ((StringBuilder)localObject1).toString());
        if (!cn.jiguang.as.e.b(this.a))
        {
          c.f("ReportSis", "give up sis, because network is not connected");
          return;
        }
        localObject2 = ((LinkedHashSet)localObject2).iterator();
        boolean bool;
        do
        {
          do
          {
            if (!((Iterator)localObject2).hasNext()) {
              break;
            }
            localObject1 = (String)((Iterator)localObject2).next();
          } while (TextUtils.isEmpty((CharSequence)localObject1));
          bool = e.a(this.a, (String)localObject1, str);
        } while (!bool);
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */