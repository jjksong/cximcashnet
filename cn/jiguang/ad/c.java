package cn.jiguang.ad;

import android.text.TextUtils;
import cn.jiguang.as.d;
import cn.jiguang.as.f;
import java.io.File;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class c
{
  public long a;
  public JSONObject b;
  private File c;
  private JSONObject d;
  private boolean e;
  private boolean f;
  
  private c(File paramFile, JSONObject paramJSONObject1, JSONObject paramJSONObject2, boolean paramBoolean)
  {
    this.c = paramFile;
    this.d = paramJSONObject1;
    this.b = paramJSONObject2;
    this.a = paramFile.length();
    this.f = paramBoolean;
    this.e = paramBoolean;
  }
  
  public static c a(File paramFile, Set<String> paramSet)
  {
    JSONObject localJSONObject = a(paramFile);
    if (localJSONObject != null) {
      return new c(paramFile, localJSONObject, f.a(localJSONObject, paramSet), false);
    }
    d.a(paramFile);
    return null;
  }
  
  public static c a(File paramFile, JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = a(paramFile);
    if (localJSONObject != null) {
      return new c(paramFile, localJSONObject, paramJSONObject, true);
    }
    d.a(paramFile);
    return null;
  }
  
  public static JSONObject a(File paramFile)
  {
    try
    {
      Object localObject = d.d(paramFile);
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        paramFile = new org/json/JSONObject;
        paramFile.<init>((String)localObject);
        localObject = paramFile.optJSONArray("content");
        if (localObject != null)
        {
          int i = ((JSONArray)localObject).length();
          if (i > 0) {
            return paramFile;
          }
        }
      }
    }
    catch (Throwable paramFile)
    {
      for (;;) {}
    }
    return null;
  }
  
  public boolean a(c paramc, File paramFile)
  {
    int i = 1;
    if (paramc != null) {}
    try
    {
      if (this.a + paramc.a <= 40960L)
      {
        paramFile = this.d.getJSONArray("content");
        JSONArray localJSONArray = paramc.d.getJSONArray("content");
        for (i = 0; i < localJSONArray.length(); i++) {
          paramFile.put(localJSONArray.getJSONObject(i));
        }
        this.a += paramc.a;
        this.e = true;
        d.a(paramc.c);
        return true;
      }
      if (!paramFile.exists()) {
        paramFile.mkdirs();
      }
      paramc = new java/io/File;
      paramc.<init>(paramFile, this.c.getName());
      if (this.f) {
        f.a(this.d, this.b);
      }
      if (this.c.equals(paramc)) {
        i = 0;
      }
      if ((this.e) || (i != 0)) {
        d.a(paramc, this.d.toString());
      }
      if (i != 0) {
        d.a(this.c);
      }
    }
    catch (Throwable paramc)
    {
      for (;;) {}
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */