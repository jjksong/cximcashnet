package cn.jiguang.ad;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import cn.jiguang.api.JCoreManager;
import cn.jiguang.api.ReportCallBack;
import cn.jiguang.as.h;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f
{
  public static JSONObject a;
  public static boolean b = true;
  private static String c = "";
  private static String d = "/v3/report";
  private static String e = "";
  
  public static int a(Context paramContext, Set<String> paramSet, JSONObject paramJSONObject, File paramFile)
  {
    if (paramJSONObject != null) {}
    try
    {
      if (paramJSONObject.length() != 0)
      {
        if (!cn.jiguang.as.e.b(paramContext))
        {
          cn.jiguang.ac.c.f("ReportUtils", "no network, give up upload");
          return -2;
        }
        Object localObject1 = a(paramContext, paramSet);
        if ((localObject1 != null) && (!((Set)localObject1).isEmpty()))
        {
          Object localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(" type=");
          ((StringBuilder)localObject2).append(paramSet);
          localObject2 = ((StringBuilder)localObject2).toString();
          Iterator localIterator = ((Set)localObject1).iterator();
          while (localIterator.hasNext())
          {
            localObject1 = (String)localIterator.next();
            if (TextUtils.isEmpty((CharSequence)localObject1))
            {
              cn.jiguang.ac.c.f("ReportUtils", "can't get url, give up upload");
            }
            else
            {
              paramSet = (Set<String>)localObject1;
              if (!((String)localObject1).endsWith(d))
              {
                paramSet = new java/lang/StringBuilder;
                paramSet.<init>();
                paramSet.append((String)localObject1);
                paramSet.append(d);
                paramSet = paramSet.toString();
              }
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append("upload");
              ((StringBuilder)localObject1).append((String)localObject2);
              ((StringBuilder)localObject1).append(" to url:");
              ((StringBuilder)localObject1).append(paramSet);
              cn.jiguang.ac.c.b("ReportUtils", ((StringBuilder)localObject1).toString());
              paramSet = b.a(paramSet, paramJSONObject.toString(), paramContext, true, 3, 1);
              int i = paramSet.a();
              if (i != -3)
              {
                switch (i)
                {
                default: 
                  paramSet = new java/lang/StringBuilder;
                  paramSet.<init>();
                  paramSet.append("upload");
                  paramSet.append((String)localObject2);
                  paramSet.append(" failed");
                  cn.jiguang.ac.c.b("ReportUtils", paramSet.toString());
                  break;
                case 0: 
                  paramContext = new java/lang/StringBuilder;
                  paramContext.<init>();
                  paramContext.append("upload success json=");
                  paramContext.append(cn.jiguang.as.f.a(paramJSONObject));
                  cn.jiguang.ac.c.b("ReportUtils", paramContext.toString());
                  cn.jiguang.as.d.a(paramFile);
                  return 0;
                case -1: 
                  localObject1 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject1).<init>();
                  ((StringBuilder)localObject1).append("upload");
                  ((StringBuilder)localObject1).append((String)localObject2);
                  ((StringBuilder)localObject1).append(" error:");
                  ((StringBuilder)localObject1).append(paramSet.b());
                  cn.jiguang.ac.c.b("ReportUtils", ((StringBuilder)localObject1).toString());
                  break;
                }
              }
              else
              {
                d.a(paramContext, new String[] { cn.jiguang.ab.e.f(paramContext) });
                return -2;
              }
            }
          }
          return -1;
        }
        cn.jiguang.ac.c.f("ReportUtils", "can't get url, give up upload");
        return -1;
      }
      cn.jiguang.ac.c.f("ReportUtils", "upload content is empty, do nothing");
      return -1;
    }
    catch (Throwable paramContext)
    {
      paramSet = new StringBuilder();
      paramSet.append("upload failed, error:");
      paramSet.append(paramContext);
      cn.jiguang.ac.c.f("ReportUtils", paramSet.toString());
    }
    return -1;
  }
  
  private static String a()
  {
    if ((JCoreManager.isTestEnv()) && (!TextUtils.isEmpty(e))) {
      return e;
    }
    return "stats.jpush.cn";
  }
  
  public static String a(Context paramContext, String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString1);
    Object localObject = null;
    if (bool) {
      return null;
    }
    long l = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.c())).longValue();
    if (l == 0L)
    {
      cn.jiguang.ac.c.b("ReportUtils", " miss uid,generate report token failed");
      return null;
    }
    paramContext = h.d((String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.e()));
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(l);
    localStringBuilder.append(paramContext);
    localStringBuilder.append(paramString1);
    paramContext = h.i(localStringBuilder.toString());
    if (h.a(paramContext)) {
      return null;
    }
    paramString1 = new StringBuilder();
    paramString1.append(l);
    paramString1.append(":");
    paramString1.append(paramContext);
    paramString1.append(":");
    paramString1.append(paramString2);
    paramContext = paramString1.toString();
    try
    {
      paramContext = Base64.encodeToString(paramContext.getBytes(), 10);
    }
    catch (Exception paramContext)
    {
      cn.jiguang.ac.c.h("getBasicAuthorization", "basic authorization encode failed");
      paramContext = (Context)localObject;
    }
    return paramContext;
  }
  
  public static String a(String paramString)
  {
    try
    {
      paramString = cn.jiguang.as.g.a(paramString, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCi0v4VEUhOdfIEfFCrPC72pcDsQF/luTmr4q34NY0EZYGKzfQuTrUAm916P52HCgF+342gjZ/Nvijts5543qYNyoLvgtu4NRcHJmuGI/w7qifhfsivYeoEj9wYphXOyB9HUjzwn1BtAih+1RyUrcErCi249yabUOIKQygPZ9OXXQIDAQAB");
    }
    catch (Throwable paramString)
    {
      cn.jiguang.ac.c.h("getBasicAuthorization", "basic authorization encode failed");
      paramString = null;
    }
    return paramString;
  }
  
  private static ArrayList<JSONArray> a(JSONArray paramJSONArray, int paramInt1, int paramInt2)
  {
    ArrayList localArrayList = new ArrayList();
    if ((paramJSONArray != null) && (paramJSONArray.length() != 0))
    {
      if (paramJSONArray.length() == 1)
      {
        localArrayList.add(paramJSONArray);
        return localArrayList;
      }
      Object localObject1 = new JSONArray();
      int j = paramJSONArray.length() - 1;
      int i = 0;
      int k = 0;
      while (j >= 0)
      {
        JSONObject localJSONObject = paramJSONArray.optJSONObject(j);
        try
        {
          int i1 = cn.jiguang.as.f.b(localJSONObject);
          if (i1 != 0)
          {
            int m = i + i1;
            if (m > paramInt2) {
              break;
            }
            int n = k + i1;
            if (n > paramInt1)
            {
              if (((JSONArray)localObject1).length() > 0) {
                localArrayList.add(localObject1);
              }
              localObject2 = new org/json/JSONArray;
              ((JSONArray)localObject2).<init>();
              try
              {
                ((JSONArray)localObject2).put(localJSONObject);
                localObject1 = localObject2;
                i = i1;
              }
              catch (Throwable localThrowable1)
              {
                localObject1 = localObject2;
                break label194;
              }
            }
            else
            {
              ((JSONArray)localObject1).put(localThrowable1);
              i = n;
            }
            k = i;
            i = m;
          }
        }
        catch (Throwable localThrowable2)
        {
          label194:
          Object localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append("partition exception:");
          ((StringBuilder)localObject2).append(localThrowable2.getMessage());
          cn.jiguang.ac.c.f("ReportUtils", ((StringBuilder)localObject2).toString());
        }
        j--;
      }
      if (((JSONArray)localObject1).length() > 0) {
        localArrayList.add(localObject1);
      }
    }
    return localArrayList;
  }
  
  public static LinkedHashSet<String> a(Context paramContext, Set<String> paramSet)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    Object localObject1 = a.a().b(paramContext).b(paramSet);
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = ((Set)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (String)((Iterator)localObject1).next();
        if (!TextUtils.isEmpty((CharSequence)localObject2)) {
          localLinkedHashSet.add(localObject2);
        }
      }
    }
    localObject1 = a();
    if (!TextUtils.isEmpty((CharSequence)localObject1))
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("https://");
      ((StringBuilder)localObject2).append((String)localObject1);
      localLinkedHashSet.add(((StringBuilder)localObject2).toString());
    }
    paramContext = (String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.a(true));
    if (!TextUtils.isEmpty(paramContext)) {
      for (localObject2 : paramContext.split(","))
      {
        if (h.e((String)localObject2))
        {
          if (!b) {
            continue;
          }
          paramContext = new StringBuilder();
        }
        else if (h.f((String)localObject2))
        {
          if (b) {
            continue;
          }
          paramContext = new StringBuilder();
        }
        else
        {
          paramContext = new StringBuilder();
        }
        paramContext.append("https://");
        paramContext.append((String)localObject2);
        localLinkedHashSet.add(paramContext.toString());
      }
    }
    paramContext = new StringBuilder();
    paramContext.append("types=");
    paramContext.append(paramSet);
    paramContext.append(" find urls=");
    paramContext.append(localLinkedHashSet);
    cn.jiguang.ac.c.b("ReportUtils", paramContext.toString());
    return localLinkedHashSet;
  }
  
  public static Set<String> a(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null) {
      return new HashSet();
    }
    return c(paramJSONObject.optJSONArray("content"));
  }
  
  public static JSONObject a(Context paramContext)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("platform", "a");
      long l = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.c())).longValue();
      if (l == 0L)
      {
        cn.jiguang.ac.c.f("ReportUtils", "miss uid when wrap container info");
        return null;
      }
      localJSONObject.put("uid", l);
      String str = cn.jiguang.ab.e.f(paramContext);
      if (h.a(str))
      {
        cn.jiguang.ac.c.h("ReportUtils", "miss app_key when wrap container info");
        return null;
      }
      localJSONObject.put("app_key", str);
      cn.jiguang.ab.a.a().a(localJSONObject);
      localJSONObject.put("core_sdk_ver", "2.0.0");
      str = cn.jiguang.ab.e.g(paramContext);
      if (!h.a(str)) {
        localJSONObject.put("channel", str);
      } else {
        cn.jiguang.ac.c.g("ReportUtils", "miss channel when wrap container info,but continue report...");
      }
      paramContext = cn.jiguang.ab.e.i(paramContext);
      if (!h.a((String)paramContext.first)) {
        localJSONObject.put("app_version", paramContext.first);
      } else {
        cn.jiguang.ac.c.g("ReportUtils", "miss app version when wrap container info,but continue report...");
      }
      return localJSONObject;
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("wrapContainerInfo exception:");
      paramContext.append(localThrowable);
      cn.jiguang.ac.c.f("ReportUtils", paramContext.toString());
    }
    return null;
  }
  
  public static JSONObject a(Context paramContext, String paramString)
  {
    try
    {
      if (TextUtils.isEmpty(paramString))
      {
        cn.jiguang.ac.c.b("ReportUtils", "file_name is null , give up read ");
        return null;
      }
      paramContext = cn.jiguang.as.d.d(cn.jiguang.as.d.a(paramContext, paramString));
      if (h.a(paramContext))
      {
        cn.jiguang.ac.c.b("ReportUtils", "read String is empty");
        return null;
      }
      paramContext = new JSONObject(paramContext.trim());
      return paramContext;
    }
    catch (Throwable paramContext)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("can't build ");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" into JsonObject, give up read :");
      localStringBuilder.append(paramContext);
      cn.jiguang.ac.c.b("ReportUtils", localStringBuilder.toString());
    }
    return null;
  }
  
  public static JSONObject a(Context paramContext, JSONObject paramJSONObject, String paramString)
  {
    JSONObject localJSONObject = paramJSONObject;
    if (paramJSONObject == null) {
      localJSONObject = new JSONObject();
    }
    try
    {
      localJSONObject.put("itime", cn.jiguang.e.c.b(paramContext));
      localJSONObject.put("type", paramString);
      localJSONObject.put("account_id", cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.m()));
    }
    catch (JSONException paramContext)
    {
      paramJSONObject = new StringBuilder();
      paramJSONObject.append("fillBase exception:");
      paramJSONObject.append(paramContext);
      cn.jiguang.ac.c.f("ReportUtils", paramJSONObject.toString());
    }
    return localJSONObject;
  }
  
  private static JSONObject a(JSONArray paramJSONArray, JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("content", paramJSONArray);
      cn.jiguang.as.f.a(localJSONObject, paramJSONObject);
      return localJSONObject;
    }
    catch (JSONException paramJSONArray)
    {
      for (;;) {}
    }
  }
  
  public static void a(Context paramContext, Object paramObject)
  {
    try
    {
      if (b(paramObject))
      {
        Runnable local1 = new cn/jiguang/ad/f$1;
        local1.<init>(paramObject, paramContext);
        cn.jiguang.ar.a.a("BUILD_REPORT", local1);
      }
      else
      {
        cn.jiguang.ac.c.b("ReportUtils", "data is invalid or empty");
      }
      d.b(paramContext);
    }
    catch (Throwable paramContext)
    {
      paramObject = new StringBuilder();
      ((StringBuilder)paramObject).append("report e:");
      ((StringBuilder)paramObject).append(paramContext);
      cn.jiguang.ac.c.f("ReportUtils", ((StringBuilder)paramObject).toString());
    }
  }
  
  public static void a(Context paramContext, String paramString, Object paramObject)
  {
    try
    {
      cn.jiguang.ac.c.b("ReportUtils", "going to report data at push service");
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      if (!TextUtils.isEmpty(paramString)) {
        localBundle.putString("sdk_type", paramString);
      }
      if (b(paramObject)) {
        localBundle.putString("report_data", paramObject.toString());
      }
      cn.jiguang.a.a.a(paramContext, "a1", localBundle);
    }
    catch (Throwable paramContext)
    {
      try
      {
        cn.jiguang.ac.c.c("ReportUtils", "reportAtPushService", paramContext);
      }
      catch (Throwable paramContext)
      {
        cn.jiguang.ac.c.c("ReportUtils", "reportAtPushService", paramContext);
      }
    }
  }
  
  public static void a(Context paramContext, JSONObject paramJSONObject, ReportCallBack paramReportCallBack)
  {
    b(paramContext, paramJSONObject, paramReportCallBack);
  }
  
  public static boolean a(Context paramContext, String paramString, JSONObject paramJSONObject)
  {
    try
    {
      if (h.a(paramString))
      {
        cn.jiguang.ac.c.b("ReportUtils", "file_name is null , give up save ");
        return false;
      }
      if (paramContext == null)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("context is null , give up save ");
        paramContext.append(paramString);
        cn.jiguang.ac.c.b("ReportUtils", paramContext.toString());
        return false;
      }
      if (paramJSONObject != null) {
        paramJSONObject = paramJSONObject.toString();
      } else {
        paramJSONObject = "";
      }
      boolean bool = cn.jiguang.as.d.a(cn.jiguang.as.d.a(paramContext, paramString), paramJSONObject);
      return bool;
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("writeLogFile e:");
      paramString.append(paramContext);
      cn.jiguang.ac.c.b("ReportUtils", paramString.toString());
    }
    return false;
  }
  
  private static String b(JSONObject paramJSONObject)
  {
    if (paramJSONObject != null) {
      return paramJSONObject.optString("type");
    }
    return null;
  }
  
  private static JSONArray b(JSONArray paramJSONArray)
  {
    if ((paramJSONArray != null) && (paramJSONArray.length() != 0))
    {
      JSONArray localJSONArray = new JSONArray();
      for (int i = 0; i < paramJSONArray.length(); i++)
      {
        JSONObject localJSONObject = paramJSONArray.optJSONObject(i);
        if ((localJSONObject != null) && (localJSONObject.length() > 0)) {
          localJSONArray.put(localJSONObject);
        }
      }
      return localJSONArray;
    }
    return paramJSONArray;
  }
  
  private static void b(Context paramContext, JSONArray paramJSONArray, Set<String> paramSet)
  {
    try
    {
      String str = a.a().a(paramContext).a(paramSet);
      JSONObject localJSONObject1 = a(paramContext);
      boolean bool;
      if (localJSONObject1 != null) {
        bool = true;
      } else {
        bool = false;
      }
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(File.separator);
      ((StringBuilder)localObject1).append(str);
      ((StringBuilder)localObject1).append(File.separator);
      if (bool) {
        str = "tmp";
      } else {
        str = "nowrap";
      }
      ((StringBuilder)localObject1).append(str);
      str = ((StringBuilder)localObject1).toString();
      paramJSONArray = a(paramJSONArray, 40960, 204800).iterator();
      while (paramJSONArray.hasNext())
      {
        localObject1 = (JSONArray)paramJSONArray.next();
        try
        {
          JSONObject localJSONObject2 = a((JSONArray)localObject1, localJSONObject1);
          localObject1 = d.a(paramContext, str, localJSONObject2, bool);
          Object localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append("save report types=");
          ((StringBuilder)localObject2).append(paramSet);
          ((StringBuilder)localObject2).append(" at ");
          ((StringBuilder)localObject2).append(str);
          ((StringBuilder)localObject2).append(File.separator);
          ((StringBuilder)localObject2).append(((File)localObject1).getName());
          cn.jiguang.ac.c.b("ReportUtils", ((StringBuilder)localObject2).toString());
          if (bool)
          {
            localObject2 = new cn/jiguang/ad/f$2;
            ((2)localObject2).<init>(paramContext, paramSet, localJSONObject2, (File)localObject1);
            cn.jiguang.ar.a.a("UPLOAD_REPORT", (Runnable)localObject2);
          }
        }
        catch (Throwable localThrowable)
        {
          cn.jiguang.ac.c.b("ReportUtils", "buildReport [for item]", localThrowable);
        }
      }
      return;
    }
    catch (Throwable paramContext)
    {
      paramJSONArray = new StringBuilder();
      paramJSONArray.append("report exception:");
      paramJSONArray.append(paramContext.getMessage());
      cn.jiguang.ac.c.h("ReportUtils", paramJSONArray.toString());
    }
  }
  
  private static void b(Context paramContext, JSONObject paramJSONObject, ReportCallBack paramReportCallBack)
  {
    if (paramJSONObject != null) {
      try
      {
        if (paramJSONObject.length() > 0)
        {
          Object localObject2 = a(paramContext);
          if (localObject2 == null)
          {
            cn.jiguang.ac.c.h("ReportUtils", "wrap data failed");
            if (paramReportCallBack != null) {
              paramReportCallBack.onFinish(-1);
            }
          }
          else
          {
            Object localObject1 = new org/json/JSONArray;
            ((JSONArray)localObject1).<init>();
            localObject1 = a(((JSONArray)localObject1).put(paramJSONObject), (JSONObject)localObject2);
            paramJSONObject = b(paramJSONObject);
            localObject2 = new java/util/HashSet;
            ((HashSet)localObject2).<init>();
            ((HashSet)localObject2).add(paramJSONObject);
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("reportWithoutStore type=");
            localStringBuilder.append(paramJSONObject);
            cn.jiguang.ac.c.b("ReportUtils", localStringBuilder.toString());
            int i = a(paramContext, (Set)localObject2, (JSONObject)localObject1, null);
            if (paramReportCallBack != null) {
              paramReportCallBack.onFinish(i);
            }
          }
        }
      }
      catch (Throwable paramReportCallBack)
      {
        paramJSONObject = new StringBuilder();
        paramJSONObject.append("reportWithoutStore exception:");
        paramJSONObject.append(paramReportCallBack.getMessage());
        cn.jiguang.ac.c.h("ReportUtils", paramJSONObject.toString());
      }
    }
    d.b(paramContext);
  }
  
  private static boolean b(Object paramObject)
  {
    boolean bool4 = paramObject instanceof String;
    boolean bool2 = true;
    boolean bool3 = true;
    boolean bool1 = true;
    if (bool4)
    {
      if (((String)paramObject).length() <= 2) {
        bool1 = false;
      }
      return bool1;
    }
    if ((paramObject instanceof JSONObject))
    {
      if (((JSONObject)paramObject).length() > 0) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
      return bool1;
    }
    if ((paramObject instanceof JSONArray))
    {
      if (b((JSONArray)paramObject).length() > 0) {
        bool1 = bool3;
      } else {
        bool1 = false;
      }
      return bool1;
    }
    return false;
  }
  
  private static Set<String> c(JSONArray paramJSONArray)
  {
    HashSet localHashSet = new HashSet();
    if (paramJSONArray != null) {
      for (int i = 0; i < paramJSONArray.length(); i++)
      {
        String str2 = b(paramJSONArray.optJSONObject(i));
        String str1 = str2;
        if (str2 == null) {
          str1 = "";
        }
        localHashSet.add(str1);
      }
    }
    return localHashSet;
  }
  
  private static JSONArray c(Object paramObject)
  {
    for (;;)
    {
      try
      {
        boolean bool = paramObject instanceof String;
        if (bool) {
          try
          {
            JSONArray localJSONArray1 = new org/json/JSONArray;
            localJSONArray1.<init>((String)paramObject);
            paramObject = localJSONArray1;
          }
          catch (Throwable localThrowable1) {}
        }
      }
      catch (Throwable localThrowable2)
      {
        JSONArray localJSONArray2;
        Object localObject;
        int i;
        paramObject = new StringBuilder();
        ((StringBuilder)paramObject).append("adapt JSONArray e:");
        ((StringBuilder)paramObject).append(localThrowable2);
        cn.jiguang.ac.c.f("ReportUtils", ((StringBuilder)paramObject).toString());
      }
      try
      {
        localJSONArray2 = new org/json/JSONArray;
        localJSONArray2.<init>();
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>((String)paramObject);
        paramObject = localJSONArray2.put(localObject);
      }
      catch (Throwable paramObject) {}
    }
    if ((paramObject instanceof JSONObject))
    {
      localObject = new org/json/JSONArray;
      ((JSONArray)localObject).<init>();
      paramObject = ((JSONArray)localObject).put(paramObject);
    }
    else if ((paramObject instanceof JSONArray))
    {
      paramObject = (JSONArray)paramObject;
    }
    else
    {
      paramObject = null;
    }
    paramObject = b((JSONArray)paramObject);
    if (paramObject != null)
    {
      i = ((JSONArray)paramObject).length();
      if (i > 0) {
        return (JSONArray)paramObject;
      }
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */