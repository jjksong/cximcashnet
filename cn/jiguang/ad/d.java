package cn.jiguang.ad;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ab.e;
import cn.jiguang.as.d.a;
import cn.jiguang.e.b;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class d
{
  private static String a;
  private static String b;
  private static ConcurrentHashMap<File, Boolean> c = new ConcurrentHashMap();
  private static final AtomicBoolean d = new AtomicBoolean(false);
  
  public static File a(Context paramContext, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(e(paramContext));
    localStringBuilder.append(paramString);
    localStringBuilder.append(File.separator);
    localStringBuilder.append(UUID.randomUUID().toString());
    return new File(localStringBuilder.toString());
  }
  
  public static File a(Context paramContext, String paramString, JSONObject paramJSONObject, boolean paramBoolean)
  {
    paramContext = a(paramContext, paramString);
    if (paramBoolean) {
      c.put(paramContext, Boolean.valueOf(true));
    }
    cn.jiguang.as.d.a(paramContext, paramJSONObject.toString());
    return paramContext;
  }
  
  public static FileFilter a()
  {
    new FileFilter()
    {
      public boolean accept(File paramAnonymousFile)
      {
        boolean bool;
        if ((paramAnonymousFile.getName().length() != 24) && (!paramAnonymousFile.getName().equals("0"))) {
          bool = true;
        } else {
          bool = false;
        }
        return bool;
      }
    };
  }
  
  public static void a(Context paramContext)
  {
    int i = 0;
    d.a locala = new d.a(false, true, "jpush_stat_history", 1);
    String str = e.f(paramContext);
    paramContext = cn.jiguang.as.d.a(paramContext.getFilesDir(), new FileFilter[] { locala, a() });
    if (paramContext != null)
    {
      int j = paramContext.length;
      while (i < j)
      {
        locala = paramContext[i];
        try
        {
          localObject = new java/io/File;
          ((File)localObject).<init>(locala, "nowrap");
          cn.jiguang.as.d.e((File)localObject);
          File localFile = new java/io/File;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(locala.getParent());
          ((StringBuilder)localObject).append(File.separator);
          ((StringBuilder)localObject).append(str);
          ((StringBuilder)localObject).append(File.separator);
          ((StringBuilder)localObject).append(locala.getName());
          localFile.<init>(((StringBuilder)localObject).toString());
          if (!localFile.getParentFile().exists()) {
            localFile.getParentFile().mkdirs();
          }
          locala.renameTo(localFile);
        }
        catch (Throwable localThrowable)
        {
          Object localObject = new StringBuilder();
          ((StringBuilder)localObject).append("e=");
          ((StringBuilder)localObject).append(localThrowable);
          cn.jiguang.ac.c.b("updateByAppKey", ((StringBuilder)localObject).toString());
        }
        i++;
      }
    }
  }
  
  public static void a(Context paramContext, String... paramVarArgs)
  {
    int j = 0;
    int i;
    int k;
    if ((paramVarArgs != null) && (paramVarArgs.length != 0))
    {
      FileFilter[] arrayOfFileFilter2 = new FileFilter[paramVarArgs.length + 1];
      for (i = 0;; i = k)
      {
        arrayOfFileFilter1 = arrayOfFileFilter2;
        if (i >= paramVarArgs.length) {
          break;
        }
        k = i + 1;
        arrayOfFileFilter2[k] = d.a.a(paramVarArgs[i]);
      }
    }
    FileFilter[] arrayOfFileFilter1 = new FileFilter[1];
    arrayOfFileFilter1[0] = new d.a(false, true, "jpush_stat_history", 1);
    paramContext = cn.jiguang.as.d.a(paramContext.getFilesDir(), arrayOfFileFilter1);
    if (paramContext != null)
    {
      k = paramContext.length;
      for (i = j; i < k; i++) {
        cn.jiguang.as.d.e(paramContext[i]);
      }
    }
  }
  
  public static void a(File paramFile)
  {
    c.remove(paramFile);
  }
  
  public static void b(Context paramContext)
  {
    if (d.get())
    {
      cn.jiguang.ac.c.b("ReportHistory", "isRunning, no need report");
      return;
    }
    cn.jiguang.ar.a.a("REPORT_HISTORY", new Runnable()
    {
      /* Error */
      public void run()
      {
        // Byte code:
        //   0: invokestatic 26	cn/jiguang/ad/d:b	()Ljava/util/concurrent/atomic/AtomicBoolean;
        //   3: iconst_1
        //   4: invokevirtual 32	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
        //   7: aload_0
        //   8: getfield 16	cn/jiguang/ad/d$2:a	Landroid/content/Context;
        //   11: invokestatic 36	cn/jiguang/ad/d:c	(Landroid/content/Context;)Ljava/lang/String;
        //   14: iconst_1
        //   15: anewarray 38	java/io/FileFilter
        //   18: dup
        //   19: iconst_0
        //   20: getstatic 43	cn/jiguang/as/d$a:b	Lcn/jiguang/as/d$a;
        //   23: aastore
        //   24: invokestatic 48	cn/jiguang/as/d:a	(Ljava/lang/String;[Ljava/io/FileFilter;)[Ljava/io/File;
        //   27: astore_3
        //   28: aload_3
        //   29: ifnull +33 -> 62
        //   32: aload_3
        //   33: arraylength
        //   34: istore_2
        //   35: iconst_0
        //   36: istore_1
        //   37: iload_1
        //   38: iload_2
        //   39: if_icmpge +76 -> 115
        //   42: aload_3
        //   43: iload_1
        //   44: aaload
        //   45: astore 4
        //   47: aload_0
        //   48: getfield 16	cn/jiguang/ad/d$2:a	Landroid/content/Context;
        //   51: aload 4
        //   53: invokestatic 51	cn/jiguang/ad/d:a	(Landroid/content/Context;Ljava/io/File;)V
        //   56: iinc 1 1
        //   59: goto -22 -> 37
        //   62: ldc 53
        //   64: ldc 55
        //   66: invokestatic 60	cn/jiguang/ac/c:b	(Ljava/lang/String;Ljava/lang/String;)V
        //   69: goto +46 -> 115
        //   72: astore_3
        //   73: goto +50 -> 123
        //   76: astore_3
        //   77: new 62	java/lang/StringBuilder
        //   80: astore 4
        //   82: aload 4
        //   84: invokespecial 63	java/lang/StringBuilder:<init>	()V
        //   87: aload 4
        //   89: ldc 65
        //   91: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   94: pop
        //   95: aload 4
        //   97: aload_3
        //   98: invokevirtual 73	java/lang/Throwable:getMessage	()Ljava/lang/String;
        //   101: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   104: pop
        //   105: ldc 53
        //   107: aload 4
        //   109: invokevirtual 76	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   112: invokestatic 79	cn/jiguang/ac/c:h	(Ljava/lang/String;Ljava/lang/String;)V
        //   115: invokestatic 26	cn/jiguang/ad/d:b	()Ljava/util/concurrent/atomic/AtomicBoolean;
        //   118: iconst_0
        //   119: invokevirtual 32	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
        //   122: return
        //   123: invokestatic 26	cn/jiguang/ad/d:b	()Ljava/util/concurrent/atomic/AtomicBoolean;
        //   126: iconst_0
        //   127: invokevirtual 32	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
        //   130: aload_3
        //   131: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	132	0	this	2
        //   36	21	1	i	int
        //   34	6	2	j	int
        //   27	16	3	arrayOfFile	File[]
        //   72	1	3	localObject1	Object
        //   76	55	3	localThrowable	Throwable
        //   45	63	4	localObject2	Object
        // Exception table:
        //   from	to	target	type
        //   0	28	72	finally
        //   32	35	72	finally
        //   47	56	72	finally
        //   62	69	72	finally
        //   77	115	72	finally
        //   0	28	76	java/lang/Throwable
        //   32	35	76	java/lang/Throwable
        //   47	56	76	java/lang/Throwable
        //   62	69	76	java/lang/Throwable
      }
    });
  }
  
  private static void b(Context paramContext, File paramFile)
  {
    if (((Long)b.a(paramContext, cn.jiguang.e.a.c())).longValue() == 0L)
    {
      cn.jiguang.ac.c.b("ReportHistory", "can't get uid, skip upload history");
      return;
    }
    Object localObject1 = cn.jiguang.as.d.a(paramFile, new FileFilter[] { d.a.a });
    if ((localObject1 != null) && (localObject1.length != 0))
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("process space=");
      ((StringBuilder)localObject2).append(d(paramFile));
      ((StringBuilder)localObject2).append(" history[");
      ((StringBuilder)localObject2).append(localObject1.length);
      ((StringBuilder)localObject2).append("]");
      cn.jiguang.ac.c.b("ReportHistory", ((StringBuilder)localObject2).toString());
      paramFile = Arrays.asList((Object[])localObject1);
      Collections.sort(paramFile, new Comparator()
      {
        public int a(File paramAnonymousFile1, File paramAnonymousFile2)
        {
          long l = paramAnonymousFile1.lastModified() - paramAnonymousFile2.lastModified();
          int i;
          if (l < 0L) {
            i = 1;
          } else if (l == 0L) {
            i = 0;
          } else {
            i = -1;
          }
          return i;
        }
      });
      paramFile = paramFile.iterator();
      while (paramFile.hasNext())
      {
        File localFile = (File)paramFile.next();
        try
        {
          localObject1 = c.a(localFile);
          if (localObject1 == null)
          {
            cn.jiguang.as.d.a(localFile);
          }
          else
          {
            localObject2 = f.a((JSONObject)localObject1);
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("report history types=");
            localStringBuilder.append(localObject2);
            cn.jiguang.ac.c.b("ReportHistory", localStringBuilder.toString());
            int i = f.a(paramContext, (Set)localObject2, (JSONObject)localObject1, localFile);
            if (i != -2) {}
          }
        }
        catch (Throwable localThrowable)
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append("upload e:");
          ((StringBuilder)localObject2).append(localThrowable);
          cn.jiguang.ac.c.f("ReportHistory", ((StringBuilder)localObject2).toString());
        }
      }
    }
  }
  
  private static void b(File paramFile)
  {
    Object localObject1 = d.a.a;
    int i = 0;
    localObject1 = cn.jiguang.as.d.a(paramFile, new FileFilter[] { localObject1 });
    if ((localObject1 != null) && (localObject1.length > 1))
    {
      List localList = Arrays.asList((Object[])localObject1);
      Collections.sort(localList, new Comparator()
      {
        public int a(File paramAnonymousFile1, File paramAnonymousFile2)
        {
          long l = paramAnonymousFile1.lastModified() - paramAnonymousFile2.lastModified();
          int i;
          if (l < 0L) {
            i = 1;
          } else if (l == 0L) {
            i = 0;
          } else {
            i = -1;
          }
          return i;
        }
      });
      Iterator localIterator = localList.iterator();
      Object localObject2;
      while (localIterator.hasNext())
      {
        localObject2 = (File)localIterator.next();
        int j = (int)(i + ((File)localObject2).length());
        i = j;
        if (j > 1048576)
        {
          cn.jiguang.as.d.a((File)localObject2);
          i = j;
        }
      }
      if (localList.size() < localObject1.length)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("trim space history=");
        ((StringBuilder)localObject2).append(d(paramFile));
        ((StringBuilder)localObject2).append(",");
        ((StringBuilder)localObject2).append(localObject1.length);
        ((StringBuilder)localObject2).append(" to ");
        ((StringBuilder)localObject2).append(localList.size());
        cn.jiguang.ac.c.b("ReportHistory", ((StringBuilder)localObject2).toString());
      }
    }
  }
  
  private static Set<String> c()
  {
    HashSet localHashSet = new HashSet();
    localHashSet.add("uid");
    localHashSet.add("app_key");
    localHashSet.add("sdk_ver");
    localHashSet.add("core_sdk_ver");
    localHashSet.add("share_sdk_ver");
    localHashSet.add("statistics_sdk_ver");
    localHashSet.add("channel");
    localHashSet.add("app_version");
    return localHashSet;
  }
  
  private static void c(Context paramContext, File paramFile)
  {
    try
    {
      JSONObject localJSONObject = f.a(paramContext);
      if (cn.jiguang.as.f.c(localJSONObject)) {
        return;
      }
      paramContext = new java/io/File;
      paramContext.<init>(paramFile, "nowrap");
      int i = 1;
      File[] arrayOfFile = cn.jiguang.as.d.a(paramContext, new FileFilter[] { d.a.a });
      if ((arrayOfFile != null) && (arrayOfFile.length != 0))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("process space=");
        paramContext.append(d(paramFile));
        paramContext.append(" nowrap[");
        paramContext.append(arrayOfFile.length);
        paramContext.append("]");
        cn.jiguang.ac.c.b("ReportHistory", paramContext.toString());
        File localFile = new java/io/File;
        localFile.<init>(paramFile, "tmp");
        for (paramFile = c.a(arrayOfFile[0], localJSONObject); i < arrayOfFile.length; paramFile = paramContext)
        {
          c localc = c.a(arrayOfFile[i], localJSONObject);
          if (localc == null)
          {
            paramContext = paramFile;
          }
          else if (paramFile != null)
          {
            paramContext = paramFile;
            if (!paramFile.a(localc, localFile)) {}
          }
          else
          {
            paramContext = localc;
          }
          i++;
        }
        if (paramFile != null) {
          paramFile.a(null, localFile);
        }
      }
      else {}
    }
    catch (Throwable paramContext)
    {
      paramFile = new StringBuilder();
      paramFile.append("processNowrap e:");
      paramFile.append(paramContext);
      cn.jiguang.ac.c.f("ReportHistory", paramFile.toString());
    }
  }
  
  private static void c(File paramFile)
  {
    try
    {
      Object localObject2 = new java/io/File;
      ((File)localObject2).<init>(paramFile, "tmp");
      Object localObject1 = d.a.a;
      int i = 0;
      localObject1 = cn.jiguang.as.d.a((File)localObject2, new FileFilter[] { localObject1 });
      if ((localObject1 != null) && (localObject1.length != 0))
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append("process space=");
        ((StringBuilder)localObject2).append(d(paramFile));
        ((StringBuilder)localObject2).append(" tmp[");
        ((StringBuilder)localObject2).append(localObject1.length);
        ((StringBuilder)localObject2).append("]");
        cn.jiguang.ac.c.b("ReportHistory", ((StringBuilder)localObject2).toString());
        localObject2 = c();
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>();
        c localc1 = c.a(cn.jiguang.as.d.a(cn.jiguang.as.d.a(paramFile, new FileFilter[] { d.a.a })), (Set)localObject2);
        if (localc1 != null) {
          localHashMap.put(String.valueOf(localc1.b), localc1);
        }
        int j = localObject1.length;
        while (i < j)
        {
          localc1 = localObject1[i];
          if (!Boolean.TRUE.equals(c.get(localc1)))
          {
            c localc2 = c.a(localc1, (Set)localObject2);
            if (localc2 != null)
            {
              String str = String.valueOf(localc2.b);
              localc1 = (c)localHashMap.get(str);
              if (localc1 == null) {}
              while (!localc1.a(localc2, paramFile))
              {
                localHashMap.put(str, localc2);
                break;
              }
            }
          }
          i++;
        }
        localObject1 = localHashMap.values().iterator();
      }
      while (((Iterator)localObject1).hasNext())
      {
        ((c)((Iterator)localObject1).next()).a(null, paramFile);
        continue;
        return;
      }
    }
    catch (Throwable localThrowable)
    {
      paramFile = new StringBuilder();
      paramFile.append("processTmp e:");
      paramFile.append(localThrowable);
      cn.jiguang.ac.c.f("ReportHistory", paramFile.toString());
    }
  }
  
  private static String d(Context paramContext)
  {
    try
    {
      if (!TextUtils.isEmpty(b))
      {
        paramContext = b;
        return paramContext;
      }
      Object localObject;
      if (a == null)
      {
        localObject = cn.jiguang.as.a.a(paramContext);
        if (!TextUtils.isEmpty((CharSequence)localObject))
        {
          if (((String)localObject).equals(paramContext.getPackageName())) {}
          StringBuilder localStringBuilder;
          for (localObject = "";; localObject = ((String)localObject).replaceFirst(localStringBuilder.toString(), "_"))
          {
            a = (String)localObject;
            break;
            localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append(paramContext.getPackageName());
            localStringBuilder.append(":");
          }
        }
      }
      if (a != null)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("jpush_stat_history");
        ((StringBuilder)localObject).append(a);
        localObject = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = "jpush_stat_history";
      }
      paramContext = cn.jiguang.as.d.a(paramContext, (String)localObject);
      if (paramContext != null)
      {
        b = paramContext.getAbsolutePath();
        paramContext = b;
        return paramContext;
      }
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append("jpush_stat_history");
      paramContext.append(a);
      paramContext = paramContext.toString();
      return paramContext;
    }
    finally {}
  }
  
  private static String d(File paramFile)
  {
    try
    {
      File localFile = paramFile.getParentFile();
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(localFile.getParentFile().getName());
      ((StringBuilder)localObject).append(File.separator);
      ((StringBuilder)localObject).append(localFile.getName());
      ((StringBuilder)localObject).append(File.separator);
      ((StringBuilder)localObject).append(paramFile.getName());
      localObject = ((StringBuilder)localObject).toString();
      return (String)localObject;
    }
    catch (Throwable localThrowable) {}
    return paramFile.getAbsolutePath();
  }
  
  private static void d(Context paramContext, File paramFile)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("upload space=");
    localStringBuilder.append(d(paramFile));
    cn.jiguang.ac.c.b("ReportHistory", localStringBuilder.toString());
    c(paramContext, paramFile);
    c(paramFile);
    b(paramContext, paramFile);
    b(paramFile);
  }
  
  private static String e(Context paramContext)
  {
    Object localObject2 = e.f(paramContext);
    Object localObject1 = localObject2;
    if (TextUtils.isEmpty((CharSequence)localObject2)) {
      localObject1 = "0";
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(d(paramContext));
    ((StringBuilder)localObject2).append(File.separator);
    ((StringBuilder)localObject2).append((String)localObject1);
    return ((StringBuilder)localObject2).toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */