package cn.jiguang.ad;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import cn.jiguang.as.h;
import cn.jiguang.net.HttpRequest;
import cn.jiguang.net.HttpResponse;
import cn.jiguang.net.HttpUtils;
import cn.jiguang.net.SSLTrustManager;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class b
{
  public static SSLTrustManager a;
  
  public static g a(String paramString1, String paramString2, Context paramContext, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    try
    {
      Object localObject = paramString2.getBytes("UTF-8");
      paramString2 = (String)localObject;
      if (paramBoolean) {
        try
        {
          paramString2 = cn.jiguang.as.e.a((byte[])localObject);
        }
        catch (IOException paramString2)
        {
          paramString1 = new java/lang/StringBuilder;
          paramString1.<init>();
          paramString1.append("zip err:");
          paramString1.append(paramString2.getMessage());
          return new g(-2, paramString1.toString());
        }
      }
      localObject = new cn/jiguang/net/HttpRequest;
      ((HttpRequest)localObject).<init>(paramString1);
      a((HttpRequest)localObject, paramContext, paramString2, paramInt2);
      while (paramInt1 > 0)
      {
        paramInt1--;
        paramString2 = HttpUtils.httpPost(paramContext, (HttpRequest)localObject);
        paramInt2 = paramString2.getResponseCode();
        paramString1 = new java/lang/StringBuilder;
        paramString1.<init>();
        paramString1.append("status code:");
        paramString1.append(paramInt2);
        paramString1.append(" retry left:");
        paramString1.append(paramInt1);
        c.c("HttpHelper", paramString1.toString());
        if (paramInt2 != 200)
        {
          if (paramInt2 != 401)
          {
            if ((paramInt2 != 404) && (paramInt2 != 410) && (paramInt2 != 429))
            {
              if (paramInt2 != 503)
              {
                if (paramInt2 != 3005)
                {
                  if (paramInt2 >= 500) {
                    return new g(-1, paramString2.getResponseBody());
                  }
                  return new g(-2, paramString2.getResponseBody());
                }
              }
              else {
                return new g(-2, paramString2.getResponseBody());
              }
            }
            else {
              return new g(-1, paramString2.getResponseBody());
            }
          }
          else {
            return new g(-3, paramString2.getResponseBody());
          }
        }
        else
        {
          paramString1 = new g(0, paramString2.getResponseBody());
          return paramString1;
        }
      }
      return new g(-2, "Failed - retry enough");
    }
    catch (Throwable paramString2) {}catch (Exception paramString1)
    {
      break label399;
    }
    catch (AssertionError paramString1) {}catch (UnsupportedEncodingException paramString2)
    {
      paramString1 = new java/lang/StringBuilder;
      paramString1.<init>();
      paramString1.append("Exception - ");
      paramString1.append(paramString2.getMessage());
      paramString1 = new g(-2, paramString1.toString());
      return paramString1;
    }
    paramString1 = new StringBuilder();
    paramString1.append("Exception - ");
    paramString1.append(paramString2.getMessage());
    return new g(-2, paramString1.toString());
    label399:
    paramString2 = new StringBuilder();
    paramString2.append("Exception - ");
    paramString2.append(paramString1.getMessage());
    return new g(-2, paramString2.toString());
    paramString2 = new StringBuilder();
    paramString2.append("Catch AssertionError to avoid http close crash - ");
    paramString2.append(paramString1.getMessage());
    return new g(-2, paramString2.toString());
  }
  
  private static void a(HttpRequest paramHttpRequest, Context paramContext, byte[] paramArrayOfByte, int paramInt)
  {
    paramHttpRequest.setConnectTimeout(30000);
    paramHttpRequest.setReadTimeout(30000);
    paramHttpRequest.setDoOutPut(true);
    paramHttpRequest.setDoInPut(true);
    paramHttpRequest.setUseCaches(false);
    String str = cn.jiguang.as.g.a();
    Object localObject;
    if (paramInt == 2) {
      localObject = "0102030405060708";
    } else {
      localObject = "iop203040506aPk!";
    }
    paramArrayOfByte = cn.jiguang.as.g.a(paramArrayOfByte, str, (String)localObject);
    paramHttpRequest.setBody(paramArrayOfByte);
    paramHttpRequest.setRequestProperty("Content-Length", String.valueOf(paramArrayOfByte.length));
    paramHttpRequest.setNeedRetryIfHttpsFailed(true);
    if (a == null) {
      try
      {
        if (!TextUtils.isEmpty("-----BEGIN CERTIFICATE-----\nMIIDjjCCAnagAwIBAgIQAzrx5qcRqaC7KGSxHQn65TANBgkqhkiG9w0BAQsFADBh\nMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\nd3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBH\nMjAeFw0xMzA4MDExMjAwMDBaFw0zODAxMTUxMjAwMDBaMGExCzAJBgNVBAYTAlVT\nMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\nb20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IEcyMIIBIjANBgkqhkiG\n9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuzfNNNx7a8myaJCtSnX/RrohCgiN9RlUyfuI\n2/Ou8jqJkTx65qsGGmvPrC3oXgkkRLpimn7Wo6h+4FR1IAWsULecYxpsMNzaHxmx\n1x7e/dfgy5SDN67sH0NO3Xss0r0upS/kqbitOtSZpLYl6ZtrAGCSYP9PIUkY92eQ\nq2EGnI/yuum06ZIya7XzV+hdG82MHauVBJVJ8zUtluNJbd134/tJS7SsVQepj5Wz\ntCO7TG1F8PapspUwtP1MVYwnSlcUfIKdzXOS0xZKBgyMUNGPHgm+F6HmIcr9g+UQ\nvIOlCsRnKPZzFBQ9RnbDhxSJITRNrw9FDKZJobq7nMWxM4MphQIDAQABo0IwQDAP\nBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAdBgNVHQ4EFgQUTiJUIBiV\n5uNu5g/6+rkS7QYXjzkwDQYJKoZIhvcNAQELBQADggEBAGBnKJRvDkhj6zHd6mcY\n1Yl9PMWLSn/pvtsrF9+wX3N3KjITOYFnQoQj8kVnNeyIv/iPsGEMNKSuIEyExtv4\nNeF22d+mQrvHRAiGfzZ0JFrabA0UWTW98kndth/Jsw1HKj2ZL7tcu7XUIOGZX1NG\nFdtom/DzMNU+MeKNhJ7jitralj41E6Vf8PlwUHBHQRFXGU7Aj64GxJUTFy8bJZ91\n8rGOmaFvE7FBcf6IKshPECBV1/MUReXgRPTqh5Uykw7+U0b6LJ3/iyK5S9kJRaTe\npLiaWN0bfVKfjllDiIGknibVb63dDcY3fe0Dkhvld1927jyNxF1WW6LZZm6zNTfl\nMrY=\n-----END CERTIFICATE-----"))
        {
          localObject = new cn/jiguang/net/SSLTrustManager;
          ((SSLTrustManager)localObject).<init>("-----BEGIN CERTIFICATE-----\nMIIDjjCCAnagAwIBAgIQAzrx5qcRqaC7KGSxHQn65TANBgkqhkiG9w0BAQsFADBh\nMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\nd3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBH\nMjAeFw0xMzA4MDExMjAwMDBaFw0zODAxMTUxMjAwMDBaMGExCzAJBgNVBAYTAlVT\nMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\nb20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IEcyMIIBIjANBgkqhkiG\n9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuzfNNNx7a8myaJCtSnX/RrohCgiN9RlUyfuI\n2/Ou8jqJkTx65qsGGmvPrC3oXgkkRLpimn7Wo6h+4FR1IAWsULecYxpsMNzaHxmx\n1x7e/dfgy5SDN67sH0NO3Xss0r0upS/kqbitOtSZpLYl6ZtrAGCSYP9PIUkY92eQ\nq2EGnI/yuum06ZIya7XzV+hdG82MHauVBJVJ8zUtluNJbd134/tJS7SsVQepj5Wz\ntCO7TG1F8PapspUwtP1MVYwnSlcUfIKdzXOS0xZKBgyMUNGPHgm+F6HmIcr9g+UQ\nvIOlCsRnKPZzFBQ9RnbDhxSJITRNrw9FDKZJobq7nMWxM4MphQIDAQABo0IwQDAP\nBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAdBgNVHQ4EFgQUTiJUIBiV\n5uNu5g/6+rkS7QYXjzkwDQYJKoZIhvcNAQELBQADggEBAGBnKJRvDkhj6zHd6mcY\n1Yl9PMWLSn/pvtsrF9+wX3N3KjITOYFnQoQj8kVnNeyIv/iPsGEMNKSuIEyExtv4\nNeF22d+mQrvHRAiGfzZ0JFrabA0UWTW98kndth/Jsw1HKj2ZL7tcu7XUIOGZX1NG\nFdtom/DzMNU+MeKNhJ7jitralj41E6Vf8PlwUHBHQRFXGU7Aj64GxJUTFy8bJZ91\n8rGOmaFvE7FBcf6IKshPECBV1/MUReXgRPTqh5Uykw7+U0b6LJ3/iyK5S9kJRaTe\npLiaWN0bfVKfjllDiIGknibVb63dDcY3fe0Dkhvld1927jyNxF1WW6LZZm6zNTfl\nMrY=\n-----END CERTIFICATE-----");
          a = (SSLTrustManager)localObject;
        }
      }
      catch (Throwable localThrowable) {}
    }
    SSLTrustManager localSSLTrustManager = a;
    if (localSSLTrustManager != null) {
      paramHttpRequest.setSslTrustManager(localSSLTrustManager);
    }
    paramHttpRequest.setRequestProperty("Accept", "application/jason");
    paramHttpRequest.setRequestProperty("Accept-Encoding", "gzip");
    paramHttpRequest.setRequestProperty("X-App-Key", cn.jiguang.ab.e.f(paramContext));
    if (paramInt == 2)
    {
      paramHttpRequest.setHaveRspData(true);
      paramHttpRequest.setRspDatazip(true);
      paramHttpRequest.setNeedErrorInput(true);
      paramContext = f.a(str);
    }
    else
    {
      paramHttpRequest.setHaveRspData(false);
      paramHttpRequest.setRspDatazip(false);
      paramContext = f.a(paramContext, h.a(paramArrayOfByte), str);
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("Basic ");
      paramArrayOfByte.append(paramContext);
      paramContext = paramArrayOfByte.toString();
    }
    paramHttpRequest.setRequestProperty("Authorization", paramContext);
    paramHttpRequest.setRequestProperty("Charset", "UTF-8");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ad/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */