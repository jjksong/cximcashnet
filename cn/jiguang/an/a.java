package cn.jiguang.an;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public abstract class a
  implements Closeable
{
  protected ByteBuffer a = ByteBuffer.allocate(49152);
  protected SocketChannel b;
  protected int c;
  protected Selector d;
  protected boolean e = false;
  
  public int a(String paramString, int paramInt)
  {
    if (this.a == null) {
      this.a = ByteBuffer.allocate(49152);
    }
    this.a.clear();
    this.c = 0;
    this.e = true;
    return 0;
  }
  
  public abstract int a(byte[] paramArrayOfByte);
  
  public ByteBuffer a()
  {
    return a(0);
  }
  
  public abstract ByteBuffer a(int paramInt);
  
  protected ByteBuffer b(int paramInt)
  {
    int i = this.c;
    if (i >= paramInt)
    {
      this.c = (i - paramInt);
      Object localObject = new byte[paramInt];
      this.a.flip();
      this.a.get((byte[])localObject, 0, paramInt);
      localObject = ByteBuffer.wrap((byte[])localObject);
      this.a.compact();
      return (ByteBuffer)localObject;
    }
    return null;
  }
  
  public boolean b()
  {
    if (this.e)
    {
      SocketChannel localSocketChannel = this.b;
      if ((localSocketChannel != null) && (localSocketChannel.isConnected())) {
        return true;
      }
    }
    boolean bool = false;
    return bool;
  }
  
  protected int c()
  {
    if (this.c < 20) {
      return 0;
    }
    int i = this.a.position();
    this.a.position(0);
    int j = this.a.getShort();
    this.a.position(i);
    return j & 0x7FFF;
  }
  
  public void close()
  {
    if (!b()) {
      cn.jiguang.ai.a.c("BaseSocket", "this connect has closed");
    }
    this.e = false;
    ByteBuffer localByteBuffer = this.a;
    if (localByteBuffer != null) {
      localByteBuffer.clear();
    }
    this.c = 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/an/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */