package cn.jiguang.an;

import cn.jiguang.ah.g;
import cn.jiguang.ap.j;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class b
  extends a
{
  private ByteBuffer f = ByteBuffer.allocate(8192);
  
  private boolean b(byte[] paramArrayOfByte)
  {
    try
    {
      if (!b())
      {
        cn.jiguang.ai.a.c("NioSocketClient", "send error - connect was invalid");
        return false;
      }
      if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 0))
      {
        int i = this.b.write(ByteBuffer.wrap(paramArrayOfByte));
        if (i > 0)
        {
          paramArrayOfByte = new java/lang/StringBuilder;
          paramArrayOfByte.<init>();
          paramArrayOfByte.append("isWritable has send len:");
          paramArrayOfByte.append(i);
          cn.jiguang.ai.a.a("NioSocketClient", paramArrayOfByte.toString());
        }
        else if (i < 0)
        {
          paramArrayOfByte = new java/lang/StringBuilder;
          paramArrayOfByte.<init>();
          paramArrayOfByte.append("isWritable error:");
          paramArrayOfByte.append(i);
          cn.jiguang.ai.a.a("NioSocketClient", paramArrayOfByte.toString());
          return false;
        }
        return true;
      }
      cn.jiguang.ai.a.c("NioSocketClient", "send error - invalide buffer");
      return false;
    }
    catch (Exception localException)
    {
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("send data error:");
      paramArrayOfByte.append(localException.getMessage());
      cn.jiguang.ai.a.h("NioSocketClient", paramArrayOfByte.toString());
      close();
    }
    return false;
  }
  
  /* Error */
  public int a(String paramString, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: iload_2
    //   5: invokespecial 88	cn/jiguang/an/a:a	(Ljava/lang/String;I)I
    //   8: pop
    //   9: sipush 64542
    //   12: istore_3
    //   13: aload_0
    //   14: invokestatic 92	java/nio/channels/SocketChannel:open	()Ljava/nio/channels/SocketChannel;
    //   17: putfield 39	cn/jiguang/an/b:b	Ljava/nio/channels/SocketChannel;
    //   20: aload_0
    //   21: invokestatic 97	java/nio/channels/Selector:open	()Ljava/nio/channels/Selector;
    //   24: putfield 101	cn/jiguang/an/b:d	Ljava/nio/channels/Selector;
    //   27: aload_0
    //   28: getfield 39	cn/jiguang/an/b:b	Ljava/nio/channels/SocketChannel;
    //   31: iconst_0
    //   32: invokevirtual 105	java/nio/channels/SocketChannel:configureBlocking	(Z)Ljava/nio/channels/SelectableChannel;
    //   35: pop
    //   36: new 107	java/net/InetSocketAddress
    //   39: astore 7
    //   41: aload 7
    //   43: aload_1
    //   44: iload_2
    //   45: invokespecial 110	java/net/InetSocketAddress:<init>	(Ljava/lang/String;I)V
    //   48: aload_0
    //   49: getfield 39	cn/jiguang/an/b:b	Ljava/nio/channels/SocketChannel;
    //   52: aload 7
    //   54: invokevirtual 114	java/nio/channels/SocketChannel:connect	(Ljava/net/SocketAddress;)Z
    //   57: pop
    //   58: ldc 28
    //   60: ldc 116
    //   62: invokestatic 36	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   65: invokestatic 122	java/lang/System:currentTimeMillis	()J
    //   68: lstore 5
    //   70: aload_0
    //   71: getfield 39	cn/jiguang/an/b:b	Ljava/nio/channels/SocketChannel;
    //   74: invokevirtual 125	java/nio/channels/SocketChannel:finishConnect	()Z
    //   77: ifne +52 -> 129
    //   80: aload_0
    //   81: getfield 129	cn/jiguang/an/b:e	Z
    //   84: ifne +16 -> 100
    //   87: ldc 28
    //   89: ldc -125
    //   91: invokestatic 36	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   94: aload_0
    //   95: monitorexit
    //   96: sipush 64545
    //   99: ireturn
    //   100: ldc2_w 132
    //   103: invokestatic 139	java/lang/Thread:sleep	(J)V
    //   106: invokestatic 122	java/lang/System:currentTimeMillis	()J
    //   109: lload 5
    //   111: lsub
    //   112: ldc2_w 140
    //   115: lcmp
    //   116: ifle -46 -> 70
    //   119: aload_0
    //   120: invokevirtual 83	cn/jiguang/an/b:close	()V
    //   123: aload_0
    //   124: monitorexit
    //   125: sipush 64542
    //   128: ireturn
    //   129: aload_0
    //   130: getfield 129	cn/jiguang/an/b:e	Z
    //   133: ifne +16 -> 149
    //   136: ldc 28
    //   138: ldc -113
    //   140: invokestatic 36	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   143: aload_0
    //   144: monitorexit
    //   145: sipush 64545
    //   148: ireturn
    //   149: ldc 28
    //   151: ldc -111
    //   153: invokestatic 36	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   156: aload_0
    //   157: getfield 39	cn/jiguang/an/b:b	Ljava/nio/channels/SocketChannel;
    //   160: aload_0
    //   161: getfield 101	cn/jiguang/an/b:d	Ljava/nio/channels/Selector;
    //   164: iconst_1
    //   165: invokevirtual 149	java/nio/channels/SocketChannel:register	(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
    //   168: pop
    //   169: aload_0
    //   170: monitorexit
    //   171: iconst_0
    //   172: ireturn
    //   173: astore 7
    //   175: new 51	java/lang/StringBuilder
    //   178: astore_1
    //   179: aload_1
    //   180: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   183: aload_1
    //   184: ldc -105
    //   186: invokevirtual 58	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   189: pop
    //   190: aload_1
    //   191: aload 7
    //   193: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: ldc 28
    //   199: aload_1
    //   200: invokevirtual 65	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   203: invokestatic 80	cn/jiguang/ai/a:h	(Ljava/lang/String;Ljava/lang/String;)V
    //   206: aload_0
    //   207: invokevirtual 83	cn/jiguang/an/b:close	()V
    //   210: aload 7
    //   212: instanceof 156
    //   215: istore 4
    //   217: iload 4
    //   219: ifeq +8 -> 227
    //   222: iload_3
    //   223: istore_2
    //   224: goto +7 -> 231
    //   227: sipush 64536
    //   230: istore_2
    //   231: aload_0
    //   232: monitorexit
    //   233: iload_2
    //   234: ireturn
    //   235: astore_1
    //   236: aload_0
    //   237: monitorexit
    //   238: aload_1
    //   239: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	240	0	this	b
    //   0	240	1	paramString	String
    //   0	240	2	paramInt	int
    //   12	211	3	i	int
    //   215	3	4	bool	boolean
    //   68	42	5	l	long
    //   39	14	7	localInetSocketAddress	java.net.InetSocketAddress
    //   173	38	7	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   13	70	173	java/lang/Throwable
    //   70	94	173	java/lang/Throwable
    //   100	123	173	java/lang/Throwable
    //   129	143	173	java/lang/Throwable
    //   149	169	173	java/lang/Throwable
    //   2	9	235	finally
    //   13	70	235	finally
    //   70	94	235	finally
    //   100	123	235	finally
    //   129	143	235	finally
    //   149	169	235	finally
    //   175	217	235	finally
  }
  
  public int a(byte[] paramArrayOfByte)
  {
    int i = 103;
    if (paramArrayOfByte == null)
    {
      cn.jiguang.ai.a.c("NioSocketClient", "sendData failed, send data was null");
      return 103;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("send data length:");
    localStringBuilder.append(paramArrayOfByte.length);
    cn.jiguang.ai.a.c("NioSocketClient", localStringBuilder.toString());
    if (paramArrayOfByte.length >= 8128)
    {
      cn.jiguang.ai.a.c("NioSocketClient", "sendData failed, data length must less than 8128");
      return 6026;
    }
    if (b(paramArrayOfByte)) {
      i = 0;
    }
    return i;
  }
  
  public ByteBuffer a(int paramInt)
  {
    try
    {
      if (b())
      {
        int i = c();
        if (i > 0)
        {
          localObject1 = b(i);
          if (localObject1 != null) {
            return (ByteBuffer)localObject1;
          }
        }
        int k = 1048576;
        int j = 0;
        while ((b()) && (this.c < k))
        {
          if (paramInt > 0) {
            i = this.d.select(paramInt);
          } else {
            i = this.d.select();
          }
          if (i == 0)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("readSelect:");
            ((StringBuilder)localObject1).append(i);
            ((StringBuilder)localObject1).append(",time out:");
            ((StringBuilder)localObject1).append(paramInt);
            cn.jiguang.ai.a.c("NioSocketClient", ((StringBuilder)localObject1).toString());
            if (paramInt > 0)
            {
              localObject1 = new cn/jiguang/ah/g;
              ((g)localObject1).<init>(64542, "recv time out");
              throw ((Throwable)localObject1);
            }
          }
          else
          {
            localObject1 = this.d.selectedKeys().iterator();
            int m = j;
            i = k;
            for (;;)
            {
              k = i;
              j = m;
              if (!((Iterator)localObject1).hasNext()) {
                break;
              }
              Object localObject2 = (SelectionKey)((Iterator)localObject1).next();
              SocketChannel localSocketChannel = (SocketChannel)((SelectionKey)localObject2).channel();
              if (((SelectionKey)localObject2).isReadable())
              {
                j = localSocketChannel.read(this.f);
                if (j >= 0)
                {
                  this.f.flip();
                  m = this.f.limit();
                  if (this.a.remaining() >= m)
                  {
                    this.a.put(this.f);
                    this.c += m;
                    this.f.compact();
                    if (this.c < 20)
                    {
                      localObject2 = new java/lang/StringBuilder;
                      ((StringBuilder)localObject2).<init>();
                      ((StringBuilder)localObject2).append("totalbuf can not parse head:");
                      ((StringBuilder)localObject2).append(this.c);
                      ((StringBuilder)localObject2).append(",peerNetData len:");
                      ((StringBuilder)localObject2).append(m);
                      ((StringBuilder)localObject2).append(",read:");
                      ((StringBuilder)localObject2).append(j);
                      cn.jiguang.ai.a.c("NioSocketClient", ((StringBuilder)localObject2).toString());
                    }
                    else
                    {
                      i = c();
                    }
                  }
                  else
                  {
                    localObject2 = new cn/jiguang/ah/g;
                    localObject1 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject1).<init>();
                    ((StringBuilder)localObject1).append("the total buf remaining less than readLen,remaining:");
                    ((StringBuilder)localObject1).append(this.a.remaining());
                    ((StringBuilder)localObject1).append(",readLen:");
                    ((StringBuilder)localObject1).append(m);
                    ((g)localObject2).<init>(64540, ((StringBuilder)localObject1).toString());
                    throw ((Throwable)localObject2);
                  }
                }
                else
                {
                  localObject1 = new cn/jiguang/ah/g;
                  localObject2 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject2).<init>();
                  ((StringBuilder)localObject2).append("read len < 0:");
                  ((StringBuilder)localObject2).append(j);
                  ((g)localObject1).<init>(64540, ((StringBuilder)localObject2).toString());
                  throw ((Throwable)localObject1);
                }
              }
              else
              {
                ((SelectionKey)localObject2).isWritable();
              }
              ((Iterator)localObject1).remove();
            }
          }
        }
        if (k != 1048576)
        {
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("read len:");
          ((StringBuilder)localObject1).append(j);
          ((StringBuilder)localObject1).append(",recvTotalLen:");
          ((StringBuilder)localObject1).append(this.c);
          ((StringBuilder)localObject1).append(",shouldLen:");
          ((StringBuilder)localObject1).append(k);
          cn.jiguang.ai.a.c("NioSocketClient", ((StringBuilder)localObject1).toString());
          localObject1 = b(k);
          if (localObject1 != null) {
            return (ByteBuffer)localObject1;
          }
          throw new g(64535, "parse error");
        }
        throw new g(64539, "recv empty data or tcp has close");
      }
      Object localObject1 = new cn/jiguang/ah/g;
      ((g)localObject1).<init>(64545, "recv error,the connect is invalid");
      throw ((Throwable)localObject1);
    }
    catch (Throwable localThrowable)
    {
      if (!(localThrowable instanceof SocketTimeoutException))
      {
        if ((localThrowable instanceof g)) {
          throw ((g)localThrowable);
        }
        throw new g(64539, localThrowable.getMessage());
      }
      throw new g(64542, localThrowable.getMessage());
    }
  }
  
  public void close()
  {
    cn.jiguang.ai.a.c("NioSocketClient", "close this connect");
    super.close();
    if (this.d != null) {}
    try
    {
      this.d.close();
      j.a(this.b);
      this.b = null;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/an/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */