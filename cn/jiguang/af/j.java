package cn.jiguang.af;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import cn.jiguang.ae.c;
import cn.jiguang.api.JCoreManager;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class j
{
  private LinkedHashSet<g> a = new LinkedHashSet();
  private k b;
  private h c;
  private p d = new p(5, 2000, null);
  private f e = new f();
  private o f;
  
  public j(k paramk)
  {
    this.b = paramk;
  }
  
  private void a(LinkedHashSet<g> paramLinkedHashSet)
  {
    paramLinkedHashSet.removeAll(this.a);
    if (!paramLinkedHashSet.isEmpty())
    {
      paramLinkedHashSet = e.a(this.b.a, paramLinkedHashSet, 0L);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("connect: last good sis info");
      localStringBuilder.append(paramLinkedHashSet);
      cn.jiguang.ai.a.c("SisConn_xxx", localStringBuilder.toString());
      paramLinkedHashSet = paramLinkedHashSet.iterator();
      while ((paramLinkedHashSet.hasNext()) && (!a((g)paramLinkedHashSet.next()))) {}
    }
  }
  
  private void a(LinkedHashSet<g> paramLinkedHashSet, long paramLong)
  {
    this.e.a();
    this.d.b();
    paramLinkedHashSet.removeAll(this.a);
    Object localObject1 = e.a(this.b.a, paramLinkedHashSet, System.currentTimeMillis());
    paramLinkedHashSet = new StringBuilder();
    paramLinkedHashSet.append("connect: new sis info=");
    paramLinkedHashSet.append(localObject1);
    cn.jiguang.ai.a.c("SisConn_xxx", paramLinkedHashSet.toString());
    if (((LinkedHashSet)localObject1).isEmpty()) {
      return;
    }
    Object localObject2 = ((LinkedHashSet)localObject1).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      paramLinkedHashSet = (g)((Iterator)localObject2).next();
      if ((SystemClock.uptimeMillis() >= paramLong) || (a(paramLinkedHashSet))) {
        return;
      }
    }
    cn.jiguang.ai.a.c("SisConn_xxx", "after connect use new sis, wait connect Result");
    long l = paramLong - SystemClock.uptimeMillis();
    if (l <= 0L) {
      return;
    }
    if (this.d.a(l) != null) {
      return;
    }
    if (this.f != null) {
      return;
    }
    paramLinkedHashSet = new HashSet();
    localObject2 = ((LinkedHashSet)localObject1).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (g)((Iterator)localObject2).next();
      if (this.a.contains(localObject1)) {
        paramLinkedHashSet.add(((g)localObject1).a);
      }
    }
    int i = this.b.b();
    boolean bool;
    if ((i != 1) && (i != 0)) {
      bool = false;
    } else {
      bool = true;
    }
    localObject1 = g.a((String)c.a(this.b.a, cn.jiguang.ae.b.a(bool)));
    if (localObject1 != null)
    {
      localObject2 = cn.jiguang.aj.a.a().b(this.b.a, ((g)localObject1).a, 3000L, false);
      if ((localObject2 != null) && (localObject2.length > 0))
      {
        ((g)localObject1).c = localObject2[0];
        l = paramLong - SystemClock.uptimeMillis();
        if (l < 10L) {
          return;
        }
        this.f = new o(this.b, (g)localObject1, paramLinkedHashSet);
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("second sis, addr=");
        ((StringBuilder)localObject2).append(localObject1);
        ((StringBuilder)localObject2).append(", failIps=");
        ((StringBuilder)localObject2).append(paramLinkedHashSet);
        cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject2).toString());
        paramLinkedHashSet = new FutureTask(this.f);
        try
        {
          JCoreManager.onEvent(null, null, 11, "ASYNC", null, new Object[] { paramLinkedHashSet });
          localObject1 = (l)paramLinkedHashSet.get(l, TimeUnit.MILLISECONDS);
          if (localObject1 != null) {
            paramLinkedHashSet = ((l)localObject1).a;
          } else {
            paramLinkedHashSet = null;
          }
          if ((paramLinkedHashSet != null) && (!paramLinkedHashSet.isEmpty()))
          {
            this.b.a(((l)localObject1).f);
            a(paramLinkedHashSet, paramLong);
          }
        }
        catch (Throwable paramLinkedHashSet)
        {
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("second sis e:");
          ((StringBuilder)localObject1).append(paramLinkedHashSet);
          cn.jiguang.ai.a.g("SisConn_xxx", ((StringBuilder)localObject1).toString());
        }
      }
    }
  }
  
  private boolean a()
  {
    String str = cn.jiguang.ap.h.c(this.b.a);
    boolean bool = a(this.b.a, str);
    long l = ((Long)c.a(this.b.a, cn.jiguang.ae.b.r())).longValue();
    if ((!bool) && (!cn.jiguang.ap.j.a(l, 180000L))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private static boolean a(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return true;
    }
    String str = (String)c.a(paramContext, cn.jiguang.ae.b.k());
    paramContext = new StringBuilder();
    paramContext.append("newType=");
    paramContext.append(paramString);
    paramContext.append(" last=");
    paramContext.append(str);
    cn.jiguang.ai.a.c("SisConn_xxx", paramContext.toString());
    return paramString.equalsIgnoreCase(str) ^ true;
  }
  
  private boolean a(g paramg)
  {
    if (this.c.a)
    {
      paramg = new cn.jiguang.ah.g(64545, null);
      this.d.a(paramg);
      return true;
    }
    if (this.d.a()) {
      return true;
    }
    if ((paramg != null) && (paramg.a()) && (!this.a.contains(paramg)))
    {
      Object localObject1 = cn.jiguang.aj.a.a().b(this.b.a, paramg.a, 3000L, this.b.a());
      if ((localObject1 != null) && (localObject1.length != 0))
      {
        localObject1 = cn.jiguang.ap.j.a(Arrays.asList((Object[])localObject1)).iterator();
        for (;;)
        {
          if (((Iterator)localObject1).hasNext())
          {
            Object localObject2 = (InetAddress)((Iterator)localObject1).next();
            if (this.c.a)
            {
              paramg = new cn.jiguang.ah.g(64545, null);
              break;
            }
            if (this.d.a()) {
              return true;
            }
            localObject2 = new g((InetAddress)localObject2, paramg.b);
            if ((!this.a.contains(localObject2)) && (this.e.a((g)localObject2)))
            {
              this.a.add(localObject2);
              localObject1 = this.d;
              ((p)localObject1).a(new b(this.b, (p)localObject1, this.e, this.c));
            }
          }
        }
        this.a.add(paramg);
      }
    }
    return false;
  }
  
  private void b()
  {
    Object localObject2 = g.a((String)c.a(this.b.a, cn.jiguang.ae.b.b(true)));
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("connect: use last good v4 address=");
    ((StringBuilder)localObject1).append(localObject2);
    cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject1).toString());
    if (a((g)localObject2)) {
      return;
    }
    localObject1 = g.a((String)c.a(this.b.a, cn.jiguang.ae.b.b(false)));
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("connect: use last good v6 address=");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject2).toString());
    if (a((g)localObject1)) {
      return;
    }
    localObject1 = c();
    if (localObject1 != null) {
      ((LinkedHashSet)localObject1).removeAll(this.a);
    }
    localObject1 = cn.jiguang.ap.j.a((Collection)localObject1);
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("connect: use defaultConn=");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject2).toString());
    localObject2 = ((LinkedList)localObject1).iterator();
    while (((Iterator)localObject2).hasNext()) {
      if (a((g)((Iterator)localObject2).next())) {
        return;
      }
    }
    localObject2 = cn.jiguang.ag.l.a().a(cn.jiguang.ae.a.b(this.b.a), 10000L);
    ((LinkedList)localObject1).clear();
    if (localObject2 != null)
    {
      ((LinkedHashSet)localObject2).removeAll(this.a);
      localObject1 = cn.jiguang.ap.j.a((Collection)localObject2);
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("connect: use srv address");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject2).toString());
    localObject1 = ((LinkedList)localObject1).iterator();
    while ((((Iterator)localObject1).hasNext()) && (!a((g)((Iterator)localObject1).next()))) {}
  }
  
  private LinkedHashSet<g> c()
  {
    try
    {
      Object localObject1 = cn.jiguang.ae.a.a(this.b.a);
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append("load Default Conn, from host=");
      ((StringBuilder)localObject2).append((String)localObject1);
      cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject2).toString());
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        return null;
      }
      localObject1 = cn.jiguang.aj.a.a().b(this.b.a, (String)localObject1, 3000L, this.b.a());
      if ((localObject1 != null) && (localObject1.length != 0))
      {
        localObject1 = cn.jiguang.ap.j.a(Arrays.asList((Object[])localObject1));
        if ((localObject1 != null) && (!((LinkedList)localObject1).isEmpty()))
        {
          localObject2 = ((InetAddress)((LinkedList)localObject1).get(0)).getHostAddress();
          localObject1 = new java/util/LinkedHashSet;
          ((LinkedHashSet)localObject1).<init>();
          g localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7000);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7002);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7003);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7004);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7005);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7006);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7007);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7008);
          ((LinkedHashSet)localObject1).add(localg);
          localg = new cn/jiguang/af/g;
          localg.<init>((String)localObject2, 7009);
          ((LinkedHashSet)localObject1).add(localg);
          return (LinkedHashSet<g>)localObject1;
        }
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    return null;
  }
  
  public cn.jiguang.an.a a(h paramh)
  {
    this.c = paramh;
    cn.jiguang.ai.a.c("SisConn_xxx", "start sisAndConnect...");
    this.e = new f();
    long l = SystemClock.uptimeMillis();
    Object localObject = g.b((String)c.a(this.b.a, cn.jiguang.ae.b.o()));
    int i;
    if ((localObject != null) && (!((LinkedHashSet)localObject).isEmpty())) {
      i = 1;
    } else {
      i = 0;
    }
    boolean bool = a();
    if ((i != 0) && (bool)) {
      a((LinkedHashSet)localObject);
    }
    l locall = this.b.a(12000L);
    if (locall != null) {
      paramh = locall.a;
    } else {
      paramh = null;
    }
    if ((paramh != null) && (!paramh.isEmpty()))
    {
      this.b.a(locall.f);
      a(paramh, l + 12000L);
    }
    else if ((i != 0) && (!bool))
    {
      a((LinkedHashSet)localObject);
    }
    b();
    cn.jiguang.ai.a.c("SisConn_xxx", "wait final result...");
    paramh = this.d.a(60000L);
    if ((paramh instanceof cn.jiguang.an.a))
    {
      cn.jiguang.ai.a.c("SisConn_xxx", "connect succeed");
      return (cn.jiguang.an.a)paramh;
    }
    if ((paramh instanceof Exception))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("all sis and connect failed, e:");
      ((StringBuilder)localObject).append(paramh);
      cn.jiguang.ai.a.c("SisConn_xxx", ((StringBuilder)localObject).toString());
      throw ((Exception)paramh);
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("all sis and connect failed:");
    ((StringBuilder)localObject).append(paramh);
    cn.jiguang.ai.a.g("SisConn_xxx", ((StringBuilder)localObject).toString());
    throw new cn.jiguang.ah.g(1, null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */