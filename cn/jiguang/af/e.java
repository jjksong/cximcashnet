package cn.jiguang.af;

import android.content.Context;
import cn.jiguang.ae.b;
import cn.jiguang.ae.c;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Random;

public class e
{
  private static final Comparator<d> a = new Comparator()
  {
    public int a(d paramAnonymousd1, d paramAnonymousd2)
    {
      if (paramAnonymousd1.equals(paramAnonymousd2)) {
        return 0;
      }
      if (paramAnonymousd1.b > paramAnonymousd2.b) {
        return -1;
      }
      if (paramAnonymousd1.b < paramAnonymousd2.b) {
        return 1;
      }
      if (paramAnonymousd1.e > paramAnonymousd2.e) {
        return -1;
      }
      if (paramAnonymousd1.e < paramAnonymousd2.e) {
        return 1;
      }
      if ((paramAnonymousd1.d != 0L) && (paramAnonymousd2.d != 0L))
      {
        if (paramAnonymousd1.d < paramAnonymousd2.d) {
          return -1;
        }
        if (paramAnonymousd1.d > paramAnonymousd2.d) {
          return 1;
        }
      }
      if ((paramAnonymousd1.c != 0L) && (paramAnonymousd2.c != 0L))
      {
        if (paramAnonymousd1.c > paramAnonymousd2.c + 180000L) {
          return -1;
        }
        if (paramAnonymousd1.c < paramAnonymousd2.c - 180000L) {
          return 1;
        }
      }
      return 0;
    }
  };
  
  public static LinkedHashSet<g> a(Context paramContext, LinkedHashSet<g> paramLinkedHashSet, long paramLong)
  {
    int i;
    if (paramLinkedHashSet != null) {
      i = paramLinkedHashSet.size();
    } else {
      i = 0;
    }
    if (i == 0) {
      return new LinkedHashSet();
    }
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = paramLinkedHashSet.iterator();
    int j = 1;
    Object localObject;
    while (localIterator.hasNext())
    {
      g localg = (g)localIterator.next();
      if (localg.a())
      {
        b localb = b.f(localg.toString());
        localObject = d.a((String)c.a(paramContext, localb));
        paramLinkedHashSet = (LinkedHashSet<g>)localObject;
        if (localObject == null) {
          paramLinkedHashSet = new d(localg);
        }
        i = j;
        if (j != 0)
        {
          paramLinkedHashSet.e = 1;
          i = 0;
        }
        if (paramLong > 0L)
        {
          paramLinkedHashSet.c = paramLong;
          c.a(paramContext, new b[] { localb.a(paramLinkedHashSet.a()) });
        }
        localLinkedList.add(paramLinkedHashSet);
        j = i;
      }
    }
    paramLinkedHashSet = a(localLinkedList, a);
    paramContext = new LinkedHashSet();
    paramLinkedHashSet = paramLinkedHashSet.iterator();
    while (paramLinkedHashSet.hasNext())
    {
      localObject = ((LinkedList)paramLinkedHashSet.next()).iterator();
      while (((Iterator)localObject).hasNext()) {
        paramContext.add(((d)((Iterator)localObject).next()).a);
      }
    }
    return paramContext;
  }
  
  private static <T> LinkedList<LinkedList<T>> a(Collection<T> paramCollection, Comparator<T> paramComparator)
  {
    int i;
    if (paramCollection != null) {
      i = paramCollection.size();
    } else {
      i = 0;
    }
    if (i == 0) {
      return new LinkedList();
    }
    LinkedList localLinkedList1 = new LinkedList();
    if (i == 1)
    {
      localLinkedList1.add(new LinkedList(paramCollection));
      return localLinkedList1;
    }
    SecureRandom localSecureRandom = new SecureRandom();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Object localObject = paramCollection.next();
      LinkedList localLinkedList2;
      for (i = 0; i < localLinkedList1.size(); i++)
      {
        localLinkedList2 = (LinkedList)localLinkedList1.get(i);
        int j = paramComparator.compare(localObject, localLinkedList2.getFirst());
        if (j == 0) {
          localLinkedList2.add(localSecureRandom.nextInt(localLinkedList2.size() + 1), localObject);
        }
        for (;;)
        {
          i = 1;
          break label196;
          if (j >= 0) {
            break;
          }
          localLinkedList2 = new LinkedList();
          localLinkedList2.add(localObject);
          localLinkedList1.add(i, localLinkedList2);
        }
      }
      i = 0;
      label196:
      if (i == 0)
      {
        localLinkedList2 = new LinkedList();
        localLinkedList2.add(localObject);
        localLinkedList1.add(localLinkedList2);
      }
    }
    return localLinkedList1;
  }
  
  public static void a(Context paramContext, g paramg, int paramInt, long paramLong)
  {
    if (paramg != null)
    {
      b localb = b.f(paramg.toString());
      d locald2 = d.a((String)c.a(paramContext, localb));
      d locald1 = locald2;
      if (locald2 == null) {
        locald1 = new d(paramg);
      }
      locald1.d = paramLong;
      locald1.b = paramInt;
      c.a(paramContext, new b[] { localb.a(locald1.a()) });
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */