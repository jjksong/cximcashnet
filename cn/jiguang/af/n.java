package cn.jiguang.af;

import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class n
{
  public int a;
  public String b;
  public String c;
  public long d;
  public String e;
  public double f;
  public double g;
  public long h;
  private int i = 0;
  private int j = 0;
  
  public n(int paramInt, String paramString1, String paramString2, long paramLong1, String paramString3, double paramDouble1, double paramDouble2, long paramLong2)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramLong1;
    this.e = paramString3;
    this.f = paramDouble1;
    this.g = paramDouble2;
    this.h = paramLong2;
  }
  
  private static boolean a(double paramDouble1, double paramDouble2)
  {
    boolean bool;
    if ((paramDouble1 > -90.0D) && (paramDouble1 < 90.0D) && (paramDouble2 > -180.0D) && (paramDouble2 < 180.0D)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public JSONObject a(Set<String> paramSet)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("type", this.a);
      localJSONObject.put("appkey", this.b);
      localJSONObject.put("sdkver", this.c);
      localJSONObject.put("platform", 0);
      if (this.d != 0L) {
        localJSONObject.put("uid", this.d);
      }
      if (this.e != null) {
        localJSONObject.put("opera", this.e);
      }
      if (a(this.f, this.g))
      {
        localJSONObject.put("lat", this.f);
        localJSONObject.put("lng", this.g);
        localJSONObject.put("time", this.h);
      }
      if ((paramSet != null) && (!paramSet.isEmpty()))
      {
        JSONArray localJSONArray = new org/json/JSONArray;
        localJSONArray.<init>();
        paramSet = paramSet.iterator();
        while (paramSet.hasNext()) {
          localJSONArray.put((String)paramSet.next());
        }
        localJSONObject.put("fail_ips", localJSONArray);
      }
      if (this.i != 0) {
        localJSONObject.put("ips_flag", this.i);
      }
      if (this.j != 0) {
        localJSONObject.put("report_flag", this.j);
      }
    }
    catch (JSONException paramSet)
    {
      paramSet.printStackTrace();
    }
    return localJSONObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */