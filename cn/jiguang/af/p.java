package cn.jiguang.af;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class p
{
  private final int a;
  private final ThreadPoolExecutor b;
  private final Handler c;
  private final AtomicBoolean d = new AtomicBoolean(true);
  private final a<Object> e;
  private volatile long f = 0L;
  private Object g;
  private CountDownLatch h = new CountDownLatch(1);
  
  public p(int paramInt1, int paramInt2, a<Object> parama)
  {
    this.a = paramInt2;
    this.e = parama;
    this.b = new ThreadPoolExecutor(paramInt1, paramInt1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue())
    {
      protected void afterExecute(Runnable paramAnonymousRunnable, Throwable paramAnonymousThrowable)
      {
        if ((getActiveCount() <= 1) && (getQueue().isEmpty()) && (!p.a(p.this).hasMessages(233)))
        {
          p.b(p.this).set(true);
          p.c(p.this).countDown();
        }
      }
    };
    this.c = new a(this.b);
  }
  
  private long b(long paramLong)
  {
    try
    {
      paramLong = this.f + paramLong;
      long l = SystemClock.uptimeMillis();
      if (paramLong < l)
      {
        this.f = l;
        return -1L;
      }
      this.f = paramLong;
      return paramLong;
    }
    finally {}
  }
  
  private static Looper d()
  {
    Object localObject = new HandlerThread("Step_xxx")
    {
      public void run()
      {
        try
        {
          super.run();
          return;
        }
        catch (Throwable localThrowable)
        {
          for (;;) {}
        }
      }
    };
    ((HandlerThread)localObject).start();
    Looper localLooper = ((HandlerThread)localObject).getLooper();
    localObject = localLooper;
    if (localLooper == null) {
      localObject = Looper.getMainLooper();
    }
    return (Looper)localObject;
  }
  
  public Object a(long paramLong)
  {
    for (;;)
    {
      Object localObject1;
      try
      {
        if (a())
        {
          localObject1 = this.g;
          return localObject1;
        }
        boolean bool = this.d.get();
        if (bool) {
          return null;
        }
        if (paramLong == -1L) {}
      }
      finally {}
      try
      {
        this.h.await(paramLong, TimeUnit.MILLISECONDS);
      }
      catch (InterruptedException localInterruptedException) {}
    }
    this.h.await();
    if (this.h.getCount() == 0L)
    {
      localObject1 = new java/util/concurrent/CountDownLatch;
      ((CountDownLatch)localObject1).<init>(1);
      this.h = ((CountDownLatch)localObject1);
    }
    localObject1 = this.g;
    return localObject1;
  }
  
  public void a(Object paramObject)
  {
    if (a()) {
      return;
    }
    if (paramObject != null)
    {
      this.g = paramObject;
      this.h.countDown();
      a locala = this.e;
      if (locala != null) {
        locala.a(paramObject);
      }
      this.c.removeMessages(233);
      this.b.shutdownNow();
    }
  }
  
  public void a(Callable<?> paramCallable)
  {
    try
    {
      long l = b(this.a);
      if (l < 0L)
      {
        this.b.submit(paramCallable);
      }
      else
      {
        Message localMessage = Message.obtain();
        localMessage.what = 233;
        localMessage.obj = paramCallable;
        this.c.sendMessageAtTime(localMessage, l);
      }
      this.d.set(false);
      return;
    }
    catch (Throwable paramCallable)
    {
      for (;;) {}
    }
  }
  
  public boolean a()
  {
    boolean bool;
    if (this.g != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void b()
  {
    try
    {
      if (this.b != null)
      {
        this.b.getQueue().clear();
        this.c.removeMessages(233);
      }
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("clean executor e:");
      localStringBuilder.append(localThrowable);
      Log.w("Step_xxx", localStringBuilder.toString());
    }
  }
  
  static class a
    extends Handler
  {
    private final ExecutorService a;
    
    a(ExecutorService paramExecutorService)
    {
      super();
      this.a = paramExecutorService;
    }
    
    public void handleMessage(Message paramMessage)
    {
      try
      {
        if (!this.a.isShutdown()) {
          this.a.submit((Callable)paramMessage.obj);
        } else {
          cn.jiguang.ai.a.g("Step_xxx", "executor is shutdown");
        }
      }
      catch (Throwable localThrowable)
      {
        paramMessage = new StringBuilder();
        paramMessage.append("handleMessage e:");
        paramMessage.append(localThrowable);
        cn.jiguang.ai.a.i("Step_xxx", paramMessage.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */