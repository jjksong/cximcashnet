package cn.jiguang.af;

import android.content.Context;
import cn.jiguang.ae.b;
import cn.jiguang.ae.c;
import cn.jiguang.ap.j;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

public class i
  implements Callable<l>
{
  private LinkedHashSet<g> a = new LinkedHashSet();
  private k b;
  
  public i(k paramk)
  {
    this.b = paramk;
  }
  
  private LinkedHashSet<g> a(Context paramContext)
  {
    LinkedHashSet localLinkedHashSet = g.b((String)c.a(paramContext, b.t()));
    if (localLinkedHashSet != null)
    {
      paramContext = localLinkedHashSet;
      if (!localLinkedHashSet.isEmpty()) {}
    }
    else
    {
      paramContext = g.a(cn.jiguang.ae.a.b());
    }
    return paramContext;
  }
  
  private void a(p paramp, f paramf)
  {
    Object localObject1 = g.a(cn.jiguang.ae.a.a());
    ((LinkedHashSet)localObject1).removeAll(this.a);
    Object localObject2 = j.a((Collection)localObject1);
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("main sis: sis host=");
    ((StringBuilder)localObject1).append(localObject2);
    cn.jiguang.ai.a.c("Sis_xxx", ((StringBuilder)localObject1).toString());
    localObject1 = ((List)localObject2).iterator();
    while (((Iterator)localObject1).hasNext()) {
      if (a((g)((Iterator)localObject1).next(), paramp, paramf)) {
        return;
      }
    }
    localObject2 = g.a((String)c.a(this.b.a, b.a(true)));
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("main sis: last good sis v4 address=");
    ((StringBuilder)localObject1).append(localObject2);
    cn.jiguang.ai.a.c("Sis_xxx", ((StringBuilder)localObject1).toString());
    if (a((g)localObject2, paramp, paramf)) {
      return;
    }
    localObject1 = g.a((String)c.a(this.b.a, b.a(false)));
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("main sis: last good sis v6 address=");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("Sis_xxx", ((StringBuilder)localObject2).toString());
    a((g)localObject1, paramp, paramf);
  }
  
  private boolean a(g paramg, p paramp, f paramf)
  {
    if (paramp.a()) {
      return true;
    }
    if ((paramg != null) && (paramg.a()) && (!this.a.contains(paramg)))
    {
      Object localObject1 = cn.jiguang.aj.a.a().b(this.b.a, paramg.a, 3000L, this.b.a());
      if ((localObject1 != null) && (localObject1.length != 0))
      {
        localObject1 = j.a(Arrays.asList((Object[])localObject1)).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          Object localObject2 = (InetAddress)((Iterator)localObject1).next();
          if (paramp.a()) {
            return true;
          }
          localObject2 = new g((InetAddress)localObject2, paramg.b);
          if ((!this.a.contains(localObject2)) && (paramf.a((g)localObject2)))
          {
            this.a.add(localObject2);
            paramp.a(new o(this.b, paramp, paramf));
          }
        }
        this.a.add(paramg);
      }
    }
    return false;
  }
  
  private void b(p paramp, f paramf)
  {
    Object localObject1 = a(this.b.a);
    ((LinkedHashSet)localObject1).removeAll(this.a);
    localObject1 = j.a((Collection)localObject1);
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("main sis: default sis");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("Sis_xxx", ((StringBuilder)localObject2).toString());
    localObject2 = ((List)localObject1).iterator();
    while (((Iterator)localObject2).hasNext()) {
      if (a((g)((Iterator)localObject2).next(), paramp, paramf)) {
        return;
      }
    }
    localObject2 = cn.jiguang.ag.l.a().a(cn.jiguang.ae.a.c(), 10000L);
    ((List)localObject1).clear();
    if (localObject2 != null)
    {
      ((LinkedHashSet)localObject2).removeAll(this.a);
      localObject1 = j.a((Collection)localObject2);
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("main sis: sis srv");
    ((StringBuilder)localObject2).append(localObject1);
    cn.jiguang.ai.a.c("Sis_xxx", ((StringBuilder)localObject2).toString());
    localObject1 = ((List)localObject1).iterator();
    while ((((Iterator)localObject1).hasNext()) && (!a((g)((Iterator)localObject1).next(), paramp, paramf))) {}
  }
  
  public l a()
  {
    Object localObject1 = new f();
    p localp = new p(5, 2000, null);
    a(localp, (f)localObject1);
    cn.jiguang.ai.a.c("Sis_xxx", "main sis: after host and last good, wait Result");
    Object localObject2 = localp.a(60000L);
    if ((localObject2 instanceof l)) {
      return (l)localObject2;
    }
    b(localp, (f)localObject1);
    cn.jiguang.ai.a.c("Sis_xxx", "main sis: after default and srv, wait Result");
    localObject1 = localp.a(60000L);
    if ((localObject1 instanceof l)) {
      return (l)localObject1;
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */