package cn.jiguang.af;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import cn.jiguang.ap.f;
import cn.jiguang.ap.h;
import cn.jiguang.ap.i;
import cn.jiguang.ap.j;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Random;
import org.json.JSONObject;

public class c
{
  public static int a(Context paramContext, cn.jiguang.an.a parama)
  {
    long l = cn.jiguang.sdk.impl.b.e(paramContext);
    Object localObject2 = cn.jiguang.ap.g.j(cn.jiguang.sdk.impl.b.f(paramContext));
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = "";
    }
    String str3 = cn.jiguang.sdk.impl.b.i(paramContext);
    Object localObject3 = cn.jiguang.ah.b.a();
    String str2 = ((cn.jiguang.ah.b)localObject3).e();
    int i = ((cn.jiguang.ah.b)localObject3).a(paramContext);
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("Login with - juid:");
    ((StringBuilder)localObject2).append(l);
    ((StringBuilder)localObject2).append(", appKey:");
    ((StringBuilder)localObject2).append(str3);
    ((StringBuilder)localObject2).append(", sdkVersion:");
    ((StringBuilder)localObject2).append(str2);
    ((StringBuilder)localObject2).append(", pluginPlatformType:");
    ((StringBuilder)localObject2).append(i);
    cn.jiguang.ai.a.f("ConnectingHelper", ((StringBuilder)localObject2).toString());
    int k = ((cn.jiguang.ah.b)localObject3).c();
    int j = h.a(paramContext);
    String str4 = cn.jiguang.sdk.impl.b.b(paramContext);
    localObject3 = cn.jiguang.ah.e.a(paramContext, "get_loc_info", null);
    localObject2 = new JSONObject();
    if ((localObject3 instanceof Bundle)) {}
    try
    {
      localObject3 = (Bundle)localObject3;
      try
      {
        ((JSONObject)localObject2).put("lat", ((Bundle)localObject3).getDouble("lat", 200.0D));
        ((JSONObject)localObject2).put("lng", ((Bundle)localObject3).getDouble("lot", 200.0D));
        ((JSONObject)localObject2).put("time", ((Bundle)localObject3).getLong("time"));
      }
      catch (Throwable localThrowable1) {}
    }
    catch (Throwable localThrowable2)
    {
      String str1;
      String str5;
      StringBuilder localStringBuilder;
      for (;;) {}
    }
    if (((JSONObject)localObject2).length() > 0) {
      localObject2 = ((JSONObject)localObject2).toString();
    } else {
      localObject2 = "";
    }
    str1 = g(paramContext);
    str1 = cn.jiguang.ap.g.b(String.format(Locale.ENGLISH, str1, new Object[0]));
    if (str1 == null) {
      str1 = "";
    } else {
      str1 = str1.toUpperCase();
    }
    str5 = cn.jiguang.sdk.impl.b.a(paramContext);
    localStringBuilder = new StringBuilder();
    localStringBuilder.append("login - juid:");
    localStringBuilder.append(l);
    localStringBuilder.append(", flag:");
    localStringBuilder.append(k);
    localStringBuilder.append(" netType:");
    localStringBuilder.append(j);
    localStringBuilder.append(" deviceId:");
    localStringBuilder.append(str4);
    localStringBuilder.append(" locInfo:");
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(" countryCode:");
    localStringBuilder.append(str1);
    localStringBuilder.append(" accountId:");
    localStringBuilder.append(str5);
    localStringBuilder.append(",sdkver:");
    localStringBuilder.append(str2);
    cn.jiguang.ai.a.c("ConnectingHelper", localStringBuilder.toString());
    localObject1 = cn.jiguang.ak.b.a(c(paramContext), l, (String)localObject1, str3, str2, k, i, j, str4, (String)localObject2, str1, str5);
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("pluginPlatformType:0b");
    ((StringBuilder)localObject2).append(Integer.toBinaryString(i & 0xFF));
    cn.jiguang.ai.a.e("ConnectingHelper", ((StringBuilder)localObject2).toString());
    localObject1 = cn.jiguang.ak.b.a(paramContext, (byte[])localObject1);
    if ((localObject1 != null) && (localObject1.length >= 1))
    {
      if (parama.a((byte[])localObject1) != 0) {
        return -1;
      }
      try
      {
        parama = parama.a(20000);
        parama = cn.jiguang.ak.a.a(paramContext, parama.array(), "");
        if ((parama != null) && (parama.first != null) && (parama.second != null) && (((cn.jiguang.ak.c)parama.first).c == 1))
        {
          parama = new cn.jiguang.ak.d((cn.jiguang.ak.c)parama.first, (ByteBuffer)parama.second);
          cn.jiguang.ai.a.c("ConnectingHelper", parama.toString());
          j = parama.a;
          cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.c().a(Integer.valueOf(parama.d)) });
          if (j == 0)
          {
            cn.jiguang.sdk.impl.a.g = parama.b;
            l = parama.c * 1000L;
            cn.jiguang.sdk.impl.b.b(paramContext, l);
            cn.jiguang.sdk.impl.b.b(paramContext, parama.d);
            paramContext = new StringBuilder();
            paramContext.append("Login succeed - sid:");
            paramContext.append(cn.jiguang.sdk.impl.a.g);
            paramContext.append(", serverTime;");
            paramContext.append(l);
            cn.jiguang.ai.a.f("ConnectingHelper", paramContext.toString());
          }
          else
          {
            paramContext = new StringBuilder();
            paramContext.append("Login failed with server error - code:");
            paramContext.append(f.a(j));
            cn.jiguang.ai.a.h("ConnectingHelper", paramContext.toString());
          }
          return j;
        }
        paramContext = "Login failed - can't parse a Login Response";
      }
      catch (cn.jiguang.ah.g parama)
      {
        for (;;)
        {
          paramContext = new StringBuilder();
          paramContext.append("Login failed - recv msg failed wit error:");
          paramContext.append(parama);
          paramContext = paramContext.toString();
        }
      }
      cn.jiguang.ai.a.h("ConnectingHelper", paramContext);
      return -1;
    }
    return -1;
  }
  
  public static void a(Context paramContext)
  {
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.o().a(null), cn.jiguang.ae.b.p().a(null), cn.jiguang.ae.b.c(false).a(null), cn.jiguang.ae.b.c(true).a(null), cn.jiguang.ae.b.t().a(null), cn.jiguang.ae.b.q().a(null), cn.jiguang.ae.b.r().a(null), cn.jiguang.ae.b.a(true).a(null), cn.jiguang.ae.b.a(false).a(null), cn.jiguang.ae.b.b(true).a(null), cn.jiguang.ae.b.b(false).a(null) });
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.d().a(null), cn.jiguang.ae.b.e().a(null), cn.jiguang.ae.b.f().a(null) });
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    a(paramContext, paramInt, true);
  }
  
  public static void a(Context paramContext, int paramInt, boolean paramBoolean)
  {
    Object localObject1;
    if (paramBoolean)
    {
      localObject1 = f.a(paramInt);
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("Register Failed with server error - code:");
      ((StringBuilder)localObject2).append(paramInt);
      cn.jiguang.ai.a.j("ConnectingHelper", ((StringBuilder)localObject2).toString());
      if (!TextUtils.isEmpty((CharSequence)localObject1))
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("Local error description: ");
        ((StringBuilder)localObject2).append((String)localObject1);
        cn.jiguang.ai.a.h("ConnectingHelper", ((StringBuilder)localObject2).toString());
      }
      cn.jiguang.ah.b.a().a(paramContext, 0, paramInt, (String)localObject1);
    }
    Object localObject2 = cn.jiguang.sdk.impl.b.i(paramContext);
    if (paramInt != 11)
    {
      if (paramInt != 1012)
      {
        if (paramInt != 10001)
        {
          switch (paramInt)
          {
          default: 
            paramContext = new StringBuilder();
            paramContext.append("Unhandled server response error code - ");
            paramContext.append(paramInt);
          case 1009: 
          case 1008: 
          case 1007: 
            for (paramContext = paramContext.toString();; paramContext = "IMEI is duplicated reported by server. Give up now. ")
            {
              cn.jiguang.ai.a.e("ConnectingHelper", paramContext);
              return;
              localObject1 = new StringBuilder();
              ((StringBuilder)localObject1).append(" AppKey:");
              ((StringBuilder)localObject1).append((String)localObject2);
              localObject2 = " 非android AppKey";
              break;
              localObject1 = new StringBuilder();
              ((StringBuilder)localObject1).append(" AppKey:");
              ((StringBuilder)localObject1).append((String)localObject2);
              localObject2 = " 是无效的AppKey,请确认与JIGUANG web端的AppKey一致";
              break;
            }
          case 1006: 
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append("包名: ");
            ((StringBuilder)localObject1).append(paramContext.getPackageName());
            localObject2 = " 不存在";
            break;
          case 1005: 
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append("包名: ");
            ((StringBuilder)localObject1).append(paramContext.getPackageName());
            ((StringBuilder)localObject1).append(" 与 AppKey:");
            ((StringBuilder)localObject1).append((String)localObject2);
            localObject2 = "不匹配";
          }
          ((StringBuilder)localObject1).append((String)localObject2);
        }
        for (localObject1 = ((StringBuilder)localObject1).toString();; localObject1 = " 未在manifest中配置AppKey")
        {
          cn.jiguang.ap.a.a(paramContext, (String)localObject1, -1);
          break;
        }
      }
      a(paramContext);
    }
  }
  
  private static byte[] a(String paramString, int paramInt1, byte[] paramArrayOfByte, boolean paramBoolean, int paramInt2)
  {
    try
    {
      if ((!TextUtils.isEmpty(paramString)) && (paramString.length() == 2) && (paramArrayOfByte != null) && (paramArrayOfByte.length != 0))
      {
        cn.jiguang.ap.d locald = new cn/jiguang/ap/d;
        locald.<init>(300);
        int i = 0;
        locald.b(0);
        locald.a(paramString.getBytes());
        locald.a(paramInt1);
        locald.b(paramInt2);
        locald.a(paramArrayOfByte);
        locald.b(locald.a(), 0);
        paramInt1 = i;
        if (paramBoolean) {
          paramInt1 = (byte)1;
        }
        locald.a((byte)(paramInt1 | 0x10), 4);
        paramString = locald.b();
        return paramString;
      }
      paramString = new java/lang/IllegalArgumentException;
      paramString.<init>("flag or body length error");
      throw paramString;
    }
    finally {}
  }
  
  public static byte[] a(String paramString1, String paramString2)
  {
    paramString2 = cn.jiguang.ap.g.i(paramString2);
    boolean bool = false;
    try
    {
      byte[] arrayOfByte = j.a(paramString2);
      i = arrayOfByte.length;
      j = paramString2.length;
      if (i < j)
      {
        paramString2 = arrayOfByte;
        bool = true;
      }
    }
    catch (IOException localIOException)
    {
      int i;
      int j;
      for (;;) {}
    }
    i = paramString2.length;
    j = cn.jiguang.al.a.b();
    return a(paramString1, j, cn.jiguang.al.a.a(cn.jiguang.al.a.a(j), paramString2), bool, i);
  }
  
  public static byte[] a(DatagramSocket paramDatagramSocket, DatagramPacket paramDatagramPacket)
  {
    paramDatagramSocket.setSoTimeout(6000);
    paramDatagramSocket.send(paramDatagramPacket);
    paramDatagramPacket = new byte['Ѐ'];
    paramDatagramPacket = new DatagramPacket(paramDatagramPacket, paramDatagramPacket.length);
    cn.jiguang.ai.a.d("ConnectingHelper", "udp Receiving...");
    paramDatagramSocket.receive(paramDatagramPacket);
    paramDatagramSocket = new byte[paramDatagramPacket.getLength()];
    System.arraycopy(paramDatagramPacket.getData(), 0, paramDatagramSocket, 0, paramDatagramSocket.length);
    return paramDatagramSocket;
  }
  
  /* Error */
  public static byte[] a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +179 -> 180
    //   4: aload_0
    //   5: arraylength
    //   6: ifeq +174 -> 180
    //   9: aload_0
    //   10: invokestatic 463	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   13: astore_0
    //   14: aload_0
    //   15: invokevirtual 466	java/nio/ByteBuffer:getShort	()S
    //   18: pop
    //   19: aload_0
    //   20: invokevirtual 466	java/nio/ByteBuffer:getShort	()S
    //   23: pop
    //   24: aload_0
    //   25: invokevirtual 469	java/nio/ByteBuffer:getInt	()I
    //   28: istore_2
    //   29: iload_2
    //   30: ldc_w 470
    //   33: iand
    //   34: i2l
    //   35: lstore_3
    //   36: aload_0
    //   37: invokevirtual 466	java/nio/ByteBuffer:getShort	()S
    //   40: pop
    //   41: aload_0
    //   42: invokevirtual 473	java/nio/ByteBuffer:remaining	()I
    //   45: istore_1
    //   46: iload_1
    //   47: newarray <illegal type>
    //   49: astore 5
    //   51: aload_0
    //   52: aload 5
    //   54: iconst_0
    //   55: iload_1
    //   56: invokevirtual 477	java/nio/ByteBuffer:get	([BII)Ljava/nio/ByteBuffer;
    //   59: pop
    //   60: aload 5
    //   62: astore_0
    //   63: lload_3
    //   64: lconst_0
    //   65: lcmp
    //   66: ifeq +49 -> 115
    //   69: lload_3
    //   70: invokestatic 418	cn/jiguang/al/a:a	(J)Ljava/lang/String;
    //   73: astore_0
    //   74: aload_0
    //   75: aload 5
    //   77: invokestatic 479	cn/jiguang/al/a:b	(Ljava/lang/String;[B)[B
    //   80: astore_0
    //   81: aload_0
    //   82: ifnull +6 -> 88
    //   85: goto +30 -> 115
    //   88: new 10	cn/jiguang/ah/g
    //   91: astore_0
    //   92: aload_0
    //   93: iconst_5
    //   94: ldc_w 481
    //   97: invokespecial 484	cn/jiguang/ah/g:<init>	(ILjava/lang/String;)V
    //   100: aload_0
    //   101: athrow
    //   102: astore_0
    //   103: new 10	cn/jiguang/ah/g
    //   106: dup
    //   107: iconst_5
    //   108: ldc_w 481
    //   111: invokespecial 484	cn/jiguang/ah/g:<init>	(ILjava/lang/String;)V
    //   114: athrow
    //   115: aload_0
    //   116: astore 5
    //   118: iload_2
    //   119: bipush 24
    //   121: iushr
    //   122: iconst_1
    //   123: iand
    //   124: iconst_1
    //   125: if_icmpne +9 -> 134
    //   128: aload_0
    //   129: invokestatic 486	cn/jiguang/ap/j:b	([B)[B
    //   132: astore 5
    //   134: aload 5
    //   136: areturn
    //   137: astore_0
    //   138: new 44	java/lang/StringBuilder
    //   141: dup
    //   142: invokespecial 48	java/lang/StringBuilder:<init>	()V
    //   145: astore 5
    //   147: aload 5
    //   149: ldc_w 488
    //   152: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload 5
    //   158: aload_0
    //   159: invokevirtual 491	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   162: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   165: pop
    //   166: new 10	cn/jiguang/ah/g
    //   169: dup
    //   170: iconst_4
    //   171: aload 5
    //   173: invokevirtual 71	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   176: invokespecial 484	cn/jiguang/ah/g:<init>	(ILjava/lang/String;)V
    //   179: athrow
    //   180: new 10	cn/jiguang/ah/g
    //   183: dup
    //   184: iconst_4
    //   185: ldc_w 493
    //   188: invokespecial 484	cn/jiguang/ah/g:<init>	(ILjava/lang/String;)V
    //   191: athrow
    //   192: astore 5
    //   194: aload_0
    //   195: astore 5
    //   197: goto -63 -> 134
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	200	0	paramArrayOfByte	byte[]
    //   45	11	1	i	int
    //   28	94	2	j	int
    //   35	35	3	l	long
    //   49	123	5	localObject	Object
    //   192	1	5	localIOException	IOException
    //   195	1	5	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   74	81	102	java/lang/Exception
    //   88	102	102	java/lang/Exception
    //   9	29	137	java/lang/Throwable
    //   36	60	137	java/lang/Throwable
    //   128	134	192	java/io/IOException
  }
  
  public static int b(Context paramContext, cn.jiguang.an.a parama)
  {
    String str1 = d(paramContext);
    Object localObject1 = cn.jiguang.ap.b.a(paramContext).a;
    Object localObject2 = e(paramContext);
    String str3 = f(paramContext);
    long l = cn.jiguang.ah.b.a().b();
    String str2 = cn.jiguang.sdk.impl.b.a(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Register with: key:");
    localStringBuilder.append(str1);
    localStringBuilder.append(", apkVersion:");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(", clientInfo:");
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(", extKey:");
    localStringBuilder.append(str3);
    localStringBuilder.append(",reg business:");
    localStringBuilder.append(l);
    localStringBuilder.append(" accountId:");
    localStringBuilder.append(str2);
    cn.jiguang.ai.a.c("ConnectingHelper", localStringBuilder.toString());
    localObject1 = cn.jiguang.ak.b.a(paramContext, cn.jiguang.ak.b.a(c(paramContext), str1, (String)localObject1, (String)localObject2, str3, l, str2));
    if (localObject1 == null)
    {
      cn.jiguang.ai.a.h("ConnectingHelper", "Register failed - encrytor reg info failed");
      return -1;
    }
    if (parama.a((byte[])localObject1) != 0)
    {
      cn.jiguang.ai.a.h("ConnectingHelper", "Register failed - send reg info failed");
      return -1;
    }
    try
    {
      parama = parama.a(20000);
      parama = cn.jiguang.ak.a.a(paramContext, parama.array(), "");
      if ((parama != null) && (parama.first != null) && (parama.second != null) && (((cn.jiguang.ak.c)parama.first).c == 0))
      {
        localObject2 = new cn.jiguang.ak.e((cn.jiguang.ak.c)parama.first, (ByteBuffer)parama.second);
        parama = new StringBuilder();
        parama.append("register response:");
        parama.append(localObject2);
        cn.jiguang.ai.a.c("ConnectingHelper", parama.toString());
        int i = ((cn.jiguang.ak.e)localObject2).a;
        cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.b().a(Integer.valueOf(i)) });
        if (i == 0)
        {
          l = ((cn.jiguang.ak.e)localObject2).b;
          parama = ((cn.jiguang.ak.e)localObject2).c;
          localObject1 = ((cn.jiguang.ak.e)localObject2).d;
          str1 = ((cn.jiguang.ak.e)localObject2).e;
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append("Register succeed - juid:");
          ((StringBuilder)localObject2).append(l);
          ((StringBuilder)localObject2).append(", registrationId:");
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(", deviceId:");
          ((StringBuilder)localObject2).append(str1);
          cn.jiguang.ai.a.f("ConnectingHelper", ((StringBuilder)localObject2).toString());
          if ((!cn.jiguang.ap.g.a((String)localObject1)) && (0L != l))
          {
            cn.jiguang.sdk.impl.b.a(paramContext, str1);
            cn.jiguang.sdk.impl.b.a(paramContext, l, parama, (String)localObject1);
          }
          else
          {
            cn.jiguang.ai.a.j("ConnectingHelper", "Unexpected: registrationId/juid should not be empty. ");
            return -1;
          }
        }
        return i;
      }
      cn.jiguang.ai.a.h("ConnectingHelper", "Register failed - can't parse a Register Response");
      return -1;
    }
    catch (cn.jiguang.ah.g paramContext)
    {
      parama = new StringBuilder();
      parama.append("Register failed - recv msg failed with error:");
      parama.append(paramContext);
      cn.jiguang.ai.a.h("ConnectingHelper", parama.toString());
    }
    return -1;
  }
  
  private static String b(String paramString1, String paramString2)
  {
    if (!cn.jiguang.ap.g.a(paramString1)) {
      return paramString1;
    }
    return paramString2;
  }
  
  public static void b(Context paramContext)
  {
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.b(true).a(null), cn.jiguang.ae.b.b(false).a(null) });
  }
  
  public static long c(Context paramContext)
  {
    try
    {
      long l2 = ((Long)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.n())).longValue();
      long l1 = l2;
      if (l2 == -1L)
      {
        Random localRandom = new java/util/Random;
        localRandom.<init>();
        l1 = Math.abs(localRandom.nextInt(10000));
      }
      l2 = 2L;
      if (l1 % 2L == 0L) {
        l2 = 1L;
      }
      l1 = (l1 + l2) % '✐';
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.n().a(Long.valueOf(l1)) });
      return l1;
    }
    finally {}
  }
  
  private static String d(Context paramContext)
  {
    String str2 = i.a(paramContext);
    String str1 = cn.jiguang.ap.b.a(paramContext).p;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b(str2, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(b(str1, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(paramContext.getPackageName());
    localStringBuilder.append("$$");
    localStringBuilder.append(cn.jiguang.sdk.impl.b.i(paramContext));
    return localStringBuilder.toString();
  }
  
  private static String e(Context paramContext)
  {
    String str = cn.jiguang.ah.b.a().d();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("regVersion:");
    localStringBuilder.append(str);
    cn.jiguang.ai.a.c("ConnectingHelper", localStringBuilder.toString());
    cn.jiguang.ap.b localb = cn.jiguang.ap.b.a(paramContext);
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(b(localb.b, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(b(localb.c, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(b(localb.d, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(b(localb.e, " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(b(cn.jiguang.sdk.impl.b.d(paramContext), " "));
    localStringBuilder.append("$$");
    localStringBuilder.append(str);
    localStringBuilder.append("$$");
    localStringBuilder.append(localb.g);
    localStringBuilder.append("$$");
    localStringBuilder.append(localb.h);
    return localStringBuilder.toString();
  }
  
  private static String f(Context paramContext)
  {
    String str2 = cn.jiguang.ap.b.a(paramContext).j;
    String str1 = str2;
    if ("unknown".equalsIgnoreCase(str2)) {
      str1 = " ";
    }
    String str3 = cn.jiguang.sdk.impl.b.b(paramContext);
    int i = cn.jiguang.sdk.impl.b.c(paramContext);
    String str5 = cn.jiguang.ap.b.a(paramContext).q;
    String str4 = cn.jiguang.ap.b.a(paramContext).i;
    str2 = "";
    if (cn.jiguang.sdk.impl.b.q(paramContext)) {
      str2 = cn.jiguang.ap.a.e(paramContext, "");
    }
    paramContext = new StringBuilder();
    paramContext.append(i);
    paramContext.append("$$");
    paramContext.append(b(str3, " "));
    paramContext.append("$$");
    paramContext.append(b(str5, " "));
    paramContext.append("$$");
    paramContext.append(b(str4, " "));
    paramContext.append("$$");
    paramContext.append(b(str2, " "));
    paramContext.append("$$");
    paramContext.append(b(str1, " "));
    return paramContext.toString();
  }
  
  /* Error */
  private static String g(Context paramContext)
  {
    // Byte code:
    //   0: invokestatic 644	cn/jiguang/api/JCoreManager:isInternal	()Z
    //   3: istore_1
    //   4: aconst_null
    //   5: astore_3
    //   6: aload_3
    //   7: astore_2
    //   8: iload_1
    //   9: ifeq +80 -> 89
    //   12: aload_0
    //   13: getstatic 646	cn/jiguang/sdk/impl/a:f	Ljava/lang/String;
    //   16: bipush 32
    //   18: ldc 28
    //   20: aconst_null
    //   21: iconst_0
    //   22: anewarray 4	java/lang/Object
    //   25: invokestatic 650	cn/jiguang/api/JCoreManager:onEvent	(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;[Ljava/lang/Object;)Ljava/lang/Object;
    //   28: astore 4
    //   30: aload_3
    //   31: astore_2
    //   32: aload 4
    //   34: instanceof 100
    //   37: ifeq +52 -> 89
    //   40: aload 4
    //   42: checkcast 100	android/os/Bundle
    //   45: ldc_w 652
    //   48: invokevirtual 655	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   51: astore_2
    //   52: new 44	java/lang/StringBuilder
    //   55: astore_3
    //   56: aload_3
    //   57: invokespecial 48	java/lang/StringBuilder:<init>	()V
    //   60: aload_3
    //   61: ldc_w 657
    //   64: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload_3
    //   69: aload_2
    //   70: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: ldc 68
    //   76: aload_3
    //   77: invokevirtual 71	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   80: invokestatic 170	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   83: goto +6 -> 89
    //   86: astore_2
    //   87: aload_3
    //   88: astore_2
    //   89: aload_2
    //   90: invokestatic 329	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   93: ifne +5 -> 98
    //   96: aload_2
    //   97: areturn
    //   98: aload_0
    //   99: invokestatic 660	cn/jiguang/ap/a:l	(Landroid/content/Context;)Ljava/lang/String;
    //   102: areturn
    //   103: astore_3
    //   104: goto -21 -> 83
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	107	0	paramContext	Context
    //   3	6	1	bool	boolean
    //   7	63	2	localObject1	Object
    //   86	1	2	localThrowable1	Throwable
    //   88	9	2	localStringBuilder1	StringBuilder
    //   5	83	3	localStringBuilder2	StringBuilder
    //   103	1	3	localThrowable2	Throwable
    //   28	13	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   12	30	86	java/lang/Throwable
    //   32	52	86	java/lang/Throwable
    //   52	83	103	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */