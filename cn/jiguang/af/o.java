package cn.jiguang.af;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import cn.jiguang.ae.c;
import cn.jiguang.ap.h;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Set;
import java.util.concurrent.Callable;

public class o
  implements Callable<l>
{
  private final k a;
  private final p b;
  private final f c;
  private Set<String> d;
  private g e;
  
  public o(k paramk, g paramg, Set<String> paramSet)
  {
    this.a = paramk;
    this.b = null;
    this.c = null;
    this.e = paramg;
    this.d = paramSet;
  }
  
  public o(k paramk, p paramp, f paramf)
  {
    this.a = paramk;
    this.b = paramp;
    this.c = paramf;
  }
  
  private static void a(Context paramContext, l paraml)
  {
    if ((paraml != null) && (!paraml.a()))
    {
      String str3 = g.a(paraml.a);
      String str1 = g.a(paraml.b);
      String str2 = h.c(paramContext);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("updateSisInfo ips=");
      localStringBuilder.append(str3);
      localStringBuilder.append(" sslIps=");
      localStringBuilder.append(str1);
      localStringBuilder.append(" net=");
      localStringBuilder.append(str2);
      cn.jiguang.ai.a.c("SisTask_xxx", localStringBuilder.toString());
      c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.o().a(str3), cn.jiguang.ae.b.p().a(str1), cn.jiguang.ae.b.c(false).a(g.a(paraml.c)), cn.jiguang.ae.b.c(true).a(g.a(paraml.d)), cn.jiguang.ae.b.t().a(g.a(paraml.e)), cn.jiguang.ae.b.q().a(Boolean.valueOf(paraml.g)), cn.jiguang.ae.b.r().a(Long.valueOf(SystemClock.elapsedRealtime())) });
      c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.k().a(str2) });
    }
  }
  
  /* Error */
  private Object[] a(DatagramSocket paramDatagramSocket, InetAddress paramInetAddress, int paramInt)
  {
    // Byte code:
    //   0: iconst_2
    //   1: anewarray 5	java/lang/Object
    //   4: astore 6
    //   6: aload 6
    //   8: iconst_1
    //   9: iconst_0
    //   10: invokestatic 151	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   13: aastore
    //   14: aload_0
    //   15: getfield 25	cn/jiguang/af/o:a	Lcn/jiguang/af/k;
    //   18: aload_0
    //   19: getfield 33	cn/jiguang/af/o:d	Ljava/util/Set;
    //   22: invokevirtual 156	cn/jiguang/af/k:a	(Ljava/util/Set;)[B
    //   25: astore 7
    //   27: new 158	java/net/DatagramPacket
    //   30: dup
    //   31: aload 7
    //   33: aload 7
    //   35: arraylength
    //   36: aload_2
    //   37: iload_3
    //   38: invokespecial 161	java/net/DatagramPacket:<init>	([BILjava/net/InetAddress;I)V
    //   41: astore 7
    //   43: invokestatic 164	android/os/SystemClock:uptimeMillis	()J
    //   46: lstore 4
    //   48: aload_1
    //   49: aload 7
    //   51: invokestatic 169	cn/jiguang/af/c:a	(Ljava/net/DatagramSocket;Ljava/net/DatagramPacket;)[B
    //   54: astore_1
    //   55: aload 6
    //   57: iconst_1
    //   58: invokestatic 164	android/os/SystemClock:uptimeMillis	()J
    //   61: lload 4
    //   63: lsub
    //   64: invokestatic 133	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   67: aastore
    //   68: aload_1
    //   69: invokestatic 172	cn/jiguang/af/c:a	([B)[B
    //   72: astore_1
    //   73: new 40	cn/jiguang/af/l
    //   76: dup
    //   77: new 174	java/lang/String
    //   80: dup
    //   81: aload_1
    //   82: invokespecial 177	java/lang/String:<init>	([B)V
    //   85: invokespecial 180	cn/jiguang/af/l:<init>	(Ljava/lang/String;)V
    //   88: astore_1
    //   89: aload_1
    //   90: invokevirtual 43	cn/jiguang/af/l:a	()Z
    //   93: ifeq +15 -> 108
    //   96: aload 6
    //   98: iconst_0
    //   99: bipush 6
    //   101: invokestatic 151	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   104: aastore
    //   105: aload 6
    //   107: areturn
    //   108: aload_0
    //   109: getfield 25	cn/jiguang/af/o:a	Lcn/jiguang/af/k;
    //   112: getfield 183	cn/jiguang/af/k:a	Landroid/content/Context;
    //   115: aload_1
    //   116: invokestatic 185	cn/jiguang/af/o:a	(Landroid/content/Context;Lcn/jiguang/af/l;)V
    //   119: aload_1
    //   120: new 48	cn/jiguang/af/g
    //   123: dup
    //   124: aload_2
    //   125: iload_3
    //   126: invokespecial 188	cn/jiguang/af/g:<init>	(Ljava/net/InetAddress;I)V
    //   129: putfield 191	cn/jiguang/af/l:f	Lcn/jiguang/af/g;
    //   132: aload 6
    //   134: iconst_0
    //   135: aload_1
    //   136: aastore
    //   137: aload 6
    //   139: areturn
    //   140: astore_1
    //   141: aload 6
    //   143: iconst_0
    //   144: aload_1
    //   145: invokevirtual 194	cn/jiguang/ah/g:a	()I
    //   148: invokestatic 151	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   151: aastore
    //   152: aload 6
    //   154: areturn
    //   155: astore_1
    //   156: aload 6
    //   158: iconst_0
    //   159: iconst_3
    //   160: invokestatic 151	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   163: aastore
    //   164: aload 6
    //   166: iconst_1
    //   167: invokestatic 164	android/os/SystemClock:uptimeMillis	()J
    //   170: lload 4
    //   172: lsub
    //   173: invokestatic 133	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   176: aastore
    //   177: aload 6
    //   179: areturn
    //   180: astore_1
    //   181: aload 6
    //   183: iconst_0
    //   184: aload_1
    //   185: invokevirtual 194	cn/jiguang/ah/g:a	()I
    //   188: invokestatic 151	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   191: aastore
    //   192: aload 6
    //   194: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	195	0	this	o
    //   0	195	1	paramDatagramSocket	DatagramSocket
    //   0	195	2	paramInetAddress	InetAddress
    //   0	195	3	paramInt	int
    //   46	125	4	l	long
    //   4	189	6	arrayOfObject	Object[]
    //   25	25	7	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   68	73	140	cn/jiguang/ah/g
    //   48	55	155	java/lang/Exception
    //   14	27	180	cn/jiguang/ah/g
  }
  
  public l a()
  {
    try
    {
      if (this.c != null)
      {
        int i = this.a.b();
        return a(this.c.a(i));
      }
      localObject = a(this.e);
      return (l)localObject;
    }
    catch (Throwable localThrowable)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("run e:");
      ((StringBuilder)localObject).append(localThrowable);
      cn.jiguang.ai.a.g("SisTask_xxx", ((StringBuilder)localObject).toString());
    }
    return null;
  }
  
  public l a(g paramg)
  {
    if ((paramg != null) && (paramg.c != null)) {
      try
      {
        Object localObject = new java/net/DatagramSocket;
        ((DatagramSocket)localObject).<init>();
        if (!TextUtils.isEmpty(cn.jiguang.ae.a.a)) {
          paramg.c = InetAddress.getByName(cn.jiguang.ae.a.a);
        }
        if (cn.jiguang.ae.a.b > 0) {
          paramg.b = cn.jiguang.ae.a.b;
        }
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("send sis:");
        localStringBuilder.append(paramg.c);
        localStringBuilder.append(" port:");
        localStringBuilder.append(paramg.b);
        cn.jiguang.ai.a.c("SisTask_xxx", localStringBuilder.toString());
        localObject = a((DatagramSocket)localObject, paramg.c, paramg.b);
        if ((localObject[0] instanceof l))
        {
          boolean bool = paramg.c instanceof Inet4Address;
          this.a.a(bool);
          c.a(this.a.a, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.a(bool).a(paramg.toString()) });
          if (this.b != null) {
            this.b.a(localObject[0]);
          }
          return (l)localObject[0];
        }
        int i = ((Integer)localObject[0]).intValue();
        long l = ((Long)localObject[1]).longValue();
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append("sis failed(");
        ((StringBuilder)localObject).append(i);
        ((StringBuilder)localObject).append("):");
        ((StringBuilder)localObject).append(paramg.c);
        ((StringBuilder)localObject).append(" port:");
        ((StringBuilder)localObject).append(paramg.b);
        cn.jiguang.ai.a.g("SisTask_xxx", ((StringBuilder)localObject).toString());
        this.a.a(1, paramg.c.getHostAddress(), paramg.b, cn.jiguang.sdk.impl.b.j(this.a.a), l, i);
      }
      catch (Throwable localThrowable)
      {
        paramg = new StringBuilder();
        paramg.append("sis e:");
        paramg.append(localThrowable);
        cn.jiguang.ai.a.g("SisTask_xxx", paramg.toString());
      }
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */