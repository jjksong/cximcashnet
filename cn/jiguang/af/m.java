package cn.jiguang.af;

import android.text.TextUtils;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class m
{
  public String a;
  public int b;
  public g c;
  public long d;
  public long e;
  public long f;
  public int g;
  public double h;
  public double i;
  public long j;
  public int k;
  
  public static m a(JSONObject paramJSONObject)
  {
    if ((paramJSONObject != null) && (paramJSONObject.length() != 0)) {
      try
      {
        m localm = new cn/jiguang/af/m;
        localm.<init>();
        localm.a = paramJSONObject.optString("appkey");
        localm.b = paramJSONObject.getInt("type");
        localm.c = g.a(paramJSONObject.getString("addr"));
        localm.e = paramJSONObject.getLong("rtime");
        localm.f = paramJSONObject.getLong("interval");
        localm.g = paramJSONObject.getInt("net");
        localm.k = paramJSONObject.getInt("code");
        localm.d = paramJSONObject.optLong("uid");
        localm.h = paramJSONObject.optDouble("lat");
        localm.i = paramJSONObject.optDouble("lng");
        localm.j = paramJSONObject.optLong("ltime");
        return localm;
      }
      catch (JSONException paramJSONObject)
      {
        paramJSONObject.printStackTrace();
      }
    }
    return null;
  }
  
  public static LinkedList<m> a(String paramString)
  {
    LinkedList localLinkedList = new LinkedList();
    if (!TextUtils.isEmpty(paramString)) {}
    try
    {
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>(paramString);
      for (int m = 0; m < localJSONArray.length(); m++) {
        localLinkedList.add(a(localJSONArray.getJSONObject(m)));
      }
    }
    catch (JSONException paramString)
    {
      for (;;) {}
    }
    return localLinkedList;
  }
  
  private static boolean a(double paramDouble1, double paramDouble2)
  {
    boolean bool;
    if ((paramDouble1 > -90.0D) && (paramDouble1 < 90.0D) && (paramDouble2 > -180.0D) && (paramDouble2 < 180.0D)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      if (!TextUtils.isEmpty(this.a)) {
        localJSONObject.put("appkey", this.a);
      }
      localJSONObject.put("type", this.b);
      localJSONObject.put("addr", this.c.toString());
      localJSONObject.put("rtime", this.e);
      localJSONObject.put("interval", this.f);
      localJSONObject.put("net", this.g);
      localJSONObject.put("code", this.k);
      if (this.d != 0L) {
        localJSONObject.put("uid", this.d);
      }
      if (a(this.h, this.i))
      {
        localJSONObject.put("lat", this.h);
        localJSONObject.put("lng", this.i);
        localJSONObject.put("ltime", this.j);
      }
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */