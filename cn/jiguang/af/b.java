package cn.jiguang.af;

import android.os.SystemClock;
import android.text.TextUtils;
import cn.jiguang.ae.c;
import cn.jiguang.ap.j;
import java.io.Closeable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.concurrent.Callable;

public class b
  implements Callable<cn.jiguang.an.a>
{
  private final k a;
  private final p b;
  private final f c;
  private final h d;
  
  public b(k paramk, p paramp, f paramf, h paramh)
  {
    this.b = paramp;
    this.a = paramk;
    this.c = paramf;
    this.d = paramh;
  }
  
  public cn.jiguang.an.a a()
  {
    try
    {
      if (this.c != null)
      {
        int i = this.a.b();
        localObject = a(this.c.a(i));
        return (cn.jiguang.an.a)localObject;
      }
    }
    catch (Throwable localThrowable)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("run e:");
      ((StringBuilder)localObject).append(localThrowable);
      cn.jiguang.ai.a.g("ConnTask_xxx", ((StringBuilder)localObject).toString());
    }
    return null;
  }
  
  public cn.jiguang.an.a a(g paramg)
  {
    if (this.b.a()) {
      return null;
    }
    Object localObject = this.d;
    if ((localObject != null) && (!((h)localObject).a))
    {
      if (paramg == null) {
        return null;
      }
      if (!TextUtils.isEmpty(cn.jiguang.ae.a.c))
      {
        paramg.a = cn.jiguang.ae.a.c;
        paramg.c = InetAddress.getByName(cn.jiguang.ae.a.c);
      }
      if (cn.jiguang.ae.a.d > 0) {
        paramg.b = cn.jiguang.ae.a.d;
      }
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Open connection with ip=");
      ((StringBuilder)localObject).append(paramg.c);
      ((StringBuilder)localObject).append(", port:");
      ((StringBuilder)localObject).append(paramg.b);
      cn.jiguang.ai.a.d("ConnTask_xxx", ((StringBuilder)localObject).toString());
      long l1 = SystemClock.uptimeMillis();
      localObject = new cn.jiguang.an.b();
      int i = ((cn.jiguang.an.a)localObject).a(paramg.a, paramg.b);
      if (this.b.a())
      {
        j.a((Closeable)localObject);
        return null;
      }
      if (this.d.a)
      {
        paramg = new cn.jiguang.ah.g(64545, null);
        this.b.a(paramg);
        j.a((Closeable)localObject);
        return null;
      }
      if (i == 0)
      {
        boolean bool;
        if ((!(paramg.c instanceof Inet4Address)) && (!cn.jiguang.ap.g.e(paramg.a))) {
          bool = false;
        } else {
          bool = true;
        }
        c.a(this.a.a, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.b(bool).a(paramg.toString()) });
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("Succeed to open connection - ip:");
        localStringBuilder.append(paramg.c);
        localStringBuilder.append(", port:");
        localStringBuilder.append(paramg.b);
        cn.jiguang.ai.a.f("ConnTask_xxx", localStringBuilder.toString());
        this.b.a(localObject);
        e.a(this.a.a, paramg, 1, 0L);
        return (cn.jiguang.an.a)localObject;
      }
      l1 = SystemClock.uptimeMillis() - l1;
      long l2 = cn.jiguang.sdk.impl.b.j(this.a.a);
      this.a.a(2, paramg.a, paramg.b, l2, l1, i);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Failed(");
      localStringBuilder.append(i);
      localStringBuilder.append(") to open connection - ip:");
      localStringBuilder.append(paramg.c);
      localStringBuilder.append(", port:");
      localStringBuilder.append(paramg.b);
      localStringBuilder.append(", cost:");
      localStringBuilder.append(l1);
      cn.jiguang.ai.a.e("ConnTask_xxx", localStringBuilder.toString());
      e.a(this.a.a, paramg, -1, l1);
      j.a((Closeable)localObject);
      return null;
    }
    paramg = new cn.jiguang.ah.g(64545, null);
    this.b.a(paramg);
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */