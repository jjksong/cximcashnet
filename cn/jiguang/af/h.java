package cn.jiguang.af;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ah.g;
import cn.jiguang.ah.i;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class h
  implements Runnable
{
  public boolean a;
  private Context b;
  private cn.jiguang.an.a c;
  private ExecutorService d;
  
  static
  {
    cn.jiguang.sdk.impl.b.a("NetworkingClient");
  }
  
  public h(Context paramContext)
  {
    this.b = paramContext;
    this.c = new cn.jiguang.an.b();
  }
  
  private void a(ByteBuffer paramByteBuffer)
  {
    cn.jiguang.sdk.impl.b.a("NetworkingClient", new q(this.b, paramByteBuffer.array()), new int[0]);
  }
  
  private static void a(ExecutorService paramExecutorService)
  {
    if (paramExecutorService == null) {
      return;
    }
    try
    {
      paramExecutorService.shutdown();
      if (!paramExecutorService.awaitTermination(100L, TimeUnit.MILLISECONDS))
      {
        paramExecutorService.shutdownNow();
        if (!paramExecutorService.awaitTermination(100L, TimeUnit.MILLISECONDS)) {
          cn.jiguang.ai.a.a("NetworkingClient", "executor did not terminate");
        }
      }
    }
    catch (Throwable localThrowable)
    {
      paramExecutorService = new StringBuilder();
      paramExecutorService.append("shutDown e:");
      paramExecutorService.append(localThrowable);
      cn.jiguang.ai.a.g("NetworkingClient", paramExecutorService.toString());
    }
    catch (InterruptedException localInterruptedException)
    {
      paramExecutorService.shutdownNow();
      cn.jiguang.ai.a.a("NetworkingClient", "current thread is interrupted by self");
      Thread.currentThread().interrupt();
    }
  }
  
  private boolean a(int paramInt)
  {
    if (this.a) {
      return false;
    }
    if (paramInt <= 0)
    {
      cn.jiguang.ai.a.d("NetworkingClient", "login error,retry login too many times");
      f();
      e();
      return false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("loginTimes:");
    localStringBuilder.append(paramInt);
    cn.jiguang.ai.a.c("NetworkingClient", localStringBuilder.toString());
    if (!d()) {
      return false;
    }
    int i = c.a(this.b, this.c);
    if (i < 0)
    {
      e();
      return false;
    }
    if (i > 0)
    {
      f();
      if (i == 108)
      {
        cn.jiguang.sdk.impl.b.k(this.b);
        return a(paramInt - 1);
      }
      b(i);
      return false;
    }
    i.a().a(this.b, "tcp_a10", null);
    return true;
  }
  
  private boolean a(Context paramContext)
  {
    cn.jiguang.ap.b.a(paramContext);
    paramContext = new j(k.a(paramContext));
    try
    {
      this.c = paramContext.a(this);
      if (!a(2))
      {
        cn.jiguang.ai.a.g("NetworkingClient", "login failed");
        return false;
      }
      return true;
    }
    catch (Exception localException)
    {
      e();
      paramContext = new StringBuilder();
      paramContext.append("sis and connect failed:");
      paramContext.append(localException);
      cn.jiguang.ai.a.g("NetworkingClient", paramContext.toString());
    }
    return false;
  }
  
  private void b(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action - onLoginFailed - respCode:");
    localStringBuilder.append(paramInt);
    cn.jiguang.ai.a.a("NetworkingClient", localStringBuilder.toString());
    new Bundle().putInt("resCode", paramInt);
    i.a().a(this.b, "tcp_a12", null);
  }
  
  private boolean d()
  {
    if ((!cn.jiguang.sdk.impl.b.h(this.b)) || (TextUtils.isEmpty(cn.jiguang.sdk.impl.b.f(this.b))))
    {
      int i = c.b(this.b, this.c);
      if (i != 0)
      {
        Bundle localBundle = new Bundle();
        localBundle.putInt("resCode", i);
        i.a().a(this.b, "tcp_a13", localBundle);
        f();
        e();
        return false;
      }
      i.a().a(this.b, "tcp_a11", null);
    }
    return true;
  }
  
  private void e()
  {
    cn.jiguang.ai.a.d("NetworkingClient", "Action - closeConnection");
    cn.jiguang.ap.j.a(this.c);
    i.a().a(this.b, "tcp_a19", null);
  }
  
  private void f()
  {
    c.b(this.b);
  }
  
  public void a()
  {
    try
    {
      this.d = Executors.newSingleThreadExecutor();
      try
      {
        this.d.execute(this);
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("execute networkingClient exception :");
        localStringBuilder.append(localThrowable);
        cn.jiguang.ai.a.h("NetworkingClient", localStringBuilder.toString());
      }
      return;
    }
    finally {}
  }
  
  public void b()
  {
    try
    {
      cn.jiguang.ai.a.d("NetworkingClient", "Action - stop");
      cn.jiguang.ap.j.a(this.c);
      this.a = true;
      a(this.d);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public cn.jiguang.an.a c()
  {
    return this.c;
  }
  
  public void run()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Begin to run in ConnectingThread - id:");
    localStringBuilder.append(Thread.currentThread().getId());
    cn.jiguang.ai.a.f("NetworkingClient", localStringBuilder.toString());
    try
    {
      if (!a(this.b))
      {
        cn.jiguang.ai.a.d("NetworkingClient", "prepare Push Channel failed , returned");
        return;
      }
      for (;;)
      {
        if (!this.a)
        {
          cn.jiguang.ai.a.d("NetworkingClient", "Network listening...");
          try
          {
            localObject = this.c.a();
            a((ByteBuffer)localObject);
            localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Received bytes - len:");
            localStringBuilder.append(((ByteBuffer)localObject).array().length);
            localStringBuilder.append(", pkg:");
            localStringBuilder.append(cn.jiguang.ap.a.a(this.b));
            cn.jiguang.ai.a.d("NetworkingClient", localStringBuilder.toString());
          }
          catch (g localg)
          {
            Object localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append(" recv failed with error:");
            ((StringBuilder)localObject).append(localg);
            ((StringBuilder)localObject).append(" ,No Break!!");
            cn.jiguang.ai.a.h("NetworkingClient", ((StringBuilder)localObject).toString());
          }
        }
      }
      if (!this.a) {
        break label209;
      }
    }
    catch (Throwable localThrowable)
    {
      cn.jiguang.ai.a.d("NetworkingClient", "run exception", localThrowable);
    }
    cn.jiguang.ai.a.d("NetworkingClient", "Break receiving by wantStop");
    label209:
    e();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */