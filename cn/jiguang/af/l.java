package cn.jiguang.af;

import android.text.TextUtils;
import cn.jiguang.ai.a;
import java.util.LinkedHashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class l
{
  LinkedHashSet<g> a;
  LinkedHashSet<g> b;
  LinkedHashSet<g> c;
  LinkedHashSet<g> d;
  LinkedHashSet<g> e;
  transient g f;
  boolean g;
  
  l(String paramString)
  {
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>(paramString);
        this.a = a(localJSONObject, "ips");
        this.b = a(localJSONObject, "ssl_ips");
        this.c = a(localJSONObject, "http_report");
        this.d = a(localJSONObject, "https_report");
        this.e = a(localJSONObject, "sis_ips");
        this.g = localJSONObject.optBoolean("data_report");
        paramString = new java/lang/StringBuilder;
        paramString.<init>();
        paramString.append("get sis=");
        paramString.append(localJSONObject.toString(2));
        a.c("sis", paramString.toString());
      }
      return;
    }
    catch (Throwable paramString)
    {
      for (;;) {}
    }
  }
  
  private LinkedHashSet<g> a(JSONObject paramJSONObject, String paramString)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    try
    {
      paramJSONObject = paramJSONObject.optJSONArray(paramString);
      if (paramJSONObject != null) {
        for (int i = 0; i < paramJSONObject.length(); i++)
        {
          paramString = g.a(paramJSONObject.optString(i, null));
          if ((paramString != null) && (paramString.a())) {
            localLinkedHashSet.add(paramString);
          }
        }
      }
    }
    catch (Throwable paramJSONObject)
    {
      for (;;) {}
    }
    return localLinkedHashSet;
  }
  
  public boolean a()
  {
    LinkedHashSet localLinkedHashSet = this.a;
    if ((localLinkedHashSet == null) || (localLinkedHashSet.isEmpty()))
    {
      localLinkedHashSet = this.b;
      if ((localLinkedHashSet == null) || (localLinkedHashSet.isEmpty())) {}
    }
    else
    {
      return false;
    }
    boolean bool = true;
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */