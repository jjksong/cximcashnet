package cn.jiguang.af;

import cn.jiguang.ai.a;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class f
{
  private LinkedHashSet<g> a = new LinkedHashSet();
  private LinkedHashSet<g> b = new LinkedHashSet();
  private List<Boolean> c = new ArrayList();
  
  private g a(boolean paramBoolean1, boolean paramBoolean2)
  {
    LinkedHashSet localLinkedHashSet;
    if (paramBoolean1) {
      localLinkedHashSet = this.b;
    } else {
      localLinkedHashSet = this.a;
    }
    if (paramBoolean2) {
      localObject = null;
    } else if (paramBoolean1) {
      localObject = this.a;
    } else {
      localObject = this.b;
    }
    if ((localObject != null) && (!((LinkedHashSet)localObject).isEmpty()) && ((localLinkedHashSet == null) || (localLinkedHashSet.isEmpty()) || (a(paramBoolean1)))) {}
    for (Object localObject = a((Collection)localObject);; localObject = a(localLinkedHashSet)) {
      return (g)localObject;
    }
  }
  
  private static <T> T a(Collection<T> paramCollection)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramCollection != null) {
      if (paramCollection.isEmpty())
      {
        localObject1 = localObject2;
      }
      else
      {
        Iterator localIterator = paramCollection.iterator();
        localObject1 = localObject2;
        if (localIterator.hasNext())
        {
          localObject1 = localIterator.next();
          paramCollection.remove(localObject1);
        }
      }
    }
    return (T)localObject1;
  }
  
  private boolean a(boolean paramBoolean)
  {
    if (this.c.size() < 2) {
      return false;
    }
    for (int i = this.c.size() - 1; i >= this.c.size() - 2; i--) {
      if (((Boolean)this.c.get(i)).booleanValue() != paramBoolean) {
        return false;
      }
    }
    return true;
  }
  
  public g a(int paramInt)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("current ipv4List=");
    ((StringBuilder)localObject).append(this.a);
    ((StringBuilder)localObject).append(" ipv6List=");
    ((StringBuilder)localObject).append(this.b);
    a.c("IpPool_xxx", ((StringBuilder)localObject).toString());
    switch (paramInt)
    {
    default: 
      localObject = null;
      break;
    case 3: 
      localObject = a(true, false);
      break;
    case 2: 
      localObject = a(true, true);
      break;
    case 1: 
      localObject = a(false, true);
      break;
    case 0: 
      localObject = a(false, false);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("get ipPort=");
    localStringBuilder.append(localObject);
    a.c("IpPool_xxx", localStringBuilder.toString());
    if (localObject != null) {
      if ((((g)localObject).c instanceof Inet4Address)) {
        this.c.add(Boolean.valueOf(false));
      } else if ((((g)localObject).c instanceof Inet6Address)) {
        this.c.add(Boolean.valueOf(true));
      }
    }
    return (g)localObject;
  }
  
  public void a()
  {
    try
    {
      this.a.clear();
      this.b.clear();
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  public boolean a(g paramg)
  {
    if ((paramg != null) && (paramg.a()))
    {
      if ((paramg.c instanceof Inet4Address)) {}
      for (LinkedHashSet localLinkedHashSet = this.a;; localLinkedHashSet = this.b)
      {
        return localLinkedHashSet.add(paramg);
        if (!(paramg.c instanceof Inet6Address)) {
          break;
        }
      }
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */