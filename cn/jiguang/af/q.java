package cn.jiguang.af;

import android.content.Context;
import cn.jiguang.ah.i;

class q
  implements Runnable
{
  private byte[] a;
  private Context b;
  
  q(Context paramContext, byte[] paramArrayOfByte)
  {
    this.b = paramContext;
    this.a = paramArrayOfByte;
  }
  
  public void run()
  {
    try
    {
      i.a().a(this.b, "tcp_a21", null);
      cn.jiguang.ak.a.a(this.b, this.a);
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("TcpRecvAction failed:");
      localStringBuilder.append(localThrowable.getMessage());
      cn.jiguang.ai.a.g("TcpRecvAction", localStringBuilder.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */