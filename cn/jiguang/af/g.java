package cn.jiguang.af;

import android.text.TextUtils;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

public class g
  implements Serializable
{
  public String a;
  public int b;
  public transient InetAddress c;
  
  public g(String paramString, int paramInt)
  {
    this.a = paramString;
    this.b = paramInt;
  }
  
  public g(InetAddress paramInetAddress, int paramInt)
  {
    this.a = paramInetAddress.getHostAddress();
    this.b = paramInt;
    this.c = paramInetAddress;
  }
  
  public static g a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    int k = paramString.lastIndexOf(":");
    if (k < 0) {
      return null;
    }
    try
    {
      int m = Integer.decode(paramString.substring(k + 1)).intValue();
      if (k == 0)
      {
        paramString = "";
      }
      else
      {
        int i;
        if (paramString.startsWith("[")) {
          i = 1;
        } else {
          i = 0;
        }
        int j = k;
        if (paramString.charAt(k - 1) == ']') {
          j = k - 1;
        }
        paramString = paramString.substring(i, j);
      }
      paramString = new g(paramString, m);
      return paramString;
    }
    catch (Exception paramString) {}
    return null;
  }
  
  public static String a(LinkedHashSet<g> paramLinkedHashSet)
  {
    if ((paramLinkedHashSet != null) && (!paramLinkedHashSet.isEmpty()))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      paramLinkedHashSet = paramLinkedHashSet.iterator();
      while (paramLinkedHashSet.hasNext())
      {
        localStringBuilder.append(((g)paramLinkedHashSet.next()).toString());
        localStringBuilder.append(",");
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      return localStringBuilder.toString();
    }
    return null;
  }
  
  public static LinkedHashSet<g> a(LinkedHashMap<String, Integer> paramLinkedHashMap)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    paramLinkedHashMap = paramLinkedHashMap.entrySet().iterator();
    while (paramLinkedHashMap.hasNext())
    {
      Object localObject = (Map.Entry)paramLinkedHashMap.next();
      localObject = new g((String)((Map.Entry)localObject).getKey(), ((Integer)((Map.Entry)localObject).getValue()).intValue());
      if (((g)localObject).a()) {
        localLinkedHashSet.add(localObject);
      }
    }
    return localLinkedHashSet;
  }
  
  public static boolean a(String paramString, int paramInt)
  {
    boolean bool;
    if ((!TextUtils.isEmpty(paramString)) && (paramInt > 0) && (paramInt <= 65535)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public static LinkedHashSet<g> b(String paramString)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    if (!TextUtils.isEmpty(paramString))
    {
      String[] arrayOfString = paramString.split(",");
      int j = arrayOfString.length;
      for (int i = 0; i < j; i++)
      {
        paramString = a(arrayOfString[i]);
        if ((paramString != null) && (paramString.a())) {
          localLinkedHashSet.add(paramString);
        }
      }
    }
    return localLinkedHashSet;
  }
  
  public boolean a()
  {
    if (!TextUtils.isEmpty(this.a))
    {
      int i = this.b;
      if ((i > 0) && (i <= 65535)) {
        return true;
      }
    }
    boolean bool = false;
    return bool;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      g localg = (g)paramObject;
      if (this.b == localg.b)
      {
        paramObject = this.a;
        if (paramObject != null ? ((String)paramObject).equals(localg.a) : localg.a == null) {}
      }
      else
      {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public int hashCode()
  {
    String str = this.a;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    return i * 31 + this.b;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder;
    if (this.a.contains(":"))
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append(this.a);
    }
    for (String str = "]:";; str = ":")
    {
      localStringBuilder.append(str);
      localStringBuilder.append(this.b);
      return localStringBuilder.toString();
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(this.a);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */