package cn.jiguang.af;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class d
{
  final g a;
  int b;
  long c;
  long d;
  int e;
  
  public d(g paramg)
  {
    this.a = paramg;
  }
  
  public static d a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      Object localObject = localJSONObject.getString("ip");
      int i = localJSONObject.getInt("port");
      paramString = new cn/jiguang/af/g;
      paramString.<init>((String)localObject, i);
      localObject = new cn/jiguang/af/d;
      ((d)localObject).<init>(paramString);
      ((d)localObject).b = localJSONObject.optInt("status");
      ((d)localObject).c = localJSONObject.optLong("fetch_time");
      ((d)localObject).d = localJSONObject.optLong("cost");
      ((d)localObject).e = localJSONObject.optInt("prefer");
      return (d)localObject;
    }
    catch (JSONException paramString) {}
    return null;
  }
  
  public String a()
  {
    try
    {
      Object localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      ((JSONObject)localObject).put("ip", this.a.a);
      ((JSONObject)localObject).put("port", this.a.b);
      ((JSONObject)localObject).put("status", this.b);
      ((JSONObject)localObject).put("fetch_time", this.c);
      ((JSONObject)localObject).put("cost", this.d);
      ((JSONObject)localObject).put("prefer", this.e);
      localObject = ((JSONObject)localObject).toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof d)) {
      return false;
    }
    d locald = (d)paramObject;
    if (this.b != locald.b) {
      return false;
    }
    if (this.c != locald.c) {
      return false;
    }
    if (this.d != locald.d) {
      return false;
    }
    if (this.e != locald.e) {
      return false;
    }
    paramObject = this.a;
    if (paramObject != null) {
      bool = ((g)paramObject).equals(locald.a);
    } else if (locald.a != null) {
      bool = false;
    }
    return bool;
  }
  
  public int hashCode()
  {
    g localg = this.a;
    int i;
    if (localg != null) {
      i = localg.hashCode();
    } else {
      i = 0;
    }
    int k = this.b;
    long l = this.c;
    int j = (int)(l ^ l >>> 32);
    l = this.d;
    return (((i * 31 + k) * 31 + j) * 31 + (int)(l ^ l >>> 32)) * 31 + this.e;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("IpInfo{ipPort=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", status=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", fetchTime=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", cost=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", prefer=");
    localStringBuilder.append(this.e);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */