package cn.jiguang.af;

import android.content.Context;
import android.os.Bundle;
import cn.jiguang.ah.e;
import cn.jiguang.ai.a;
import cn.jiguang.ap.h;
import cn.jiguang.ap.j;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONObject;

public class k
{
  private static k c;
  Context a;
  public final LinkedList<m> b;
  private int d = 0;
  private byte[] e;
  private int f;
  
  public k(Context paramContext)
  {
    this.a = paramContext;
    this.b = m.a((String)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.l()));
  }
  
  public static k a(Context paramContext)
  {
    if (c == null) {
      try
      {
        if (c == null)
        {
          k localk = new cn/jiguang/af/k;
          localk.<init>(paramContext);
          c = localk;
        }
      }
      finally {}
    }
    return c;
  }
  
  private n a(int paramInt)
  {
    String str1 = cn.jiguang.sdk.impl.b.i(this.a);
    long l3 = cn.jiguang.sdk.impl.b.e(this.a);
    String str2 = h.b(this.a);
    long l1 = System.currentTimeMillis();
    Object localObject = e.a(this.a, "get_loc_info", null);
    boolean bool = localObject instanceof Bundle;
    double d1 = 200.0D;
    if (bool)
    {
      try
      {
        localObject = (Bundle)localObject;
        d3 = ((Bundle)localObject).getDouble("lat");
        long l2;
        d3 = d2;
      }
      catch (Throwable localThrowable1)
      {
        try
        {
          d2 = ((Bundle)localObject).getDouble("lot");
          d1 = d2;
          l2 = ((Bundle)localObject).getLong("time");
          l1 = l2;
          d1 = d2;
          d2 = d3;
        }
        catch (Throwable localThrowable2)
        {
          for (;;)
          {
            double d3;
            double d2 = d3;
          }
        }
        localThrowable1 = localThrowable1;
        d2 = 200.0D;
      }
      d2 = d1;
    }
    else
    {
      d2 = 200.0D;
      d3 = d1;
    }
    return new n(paramInt, str1, "2.0.0", l3, str2, d3, d2, l1);
  }
  
  private void a(m paramm)
  {
    try
    {
      this.b.add(paramm);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("addSisReportInfo:");
      ((StringBuilder)localObject).append(paramm.a().toString());
      a.c("SisConnContext", ((StringBuilder)localObject).toString());
      while (this.b.size() > 30) {
        this.b.removeFirst();
      }
      localObject = new org/json/JSONArray;
      ((JSONArray)localObject).<init>();
      paramm = this.b.iterator();
      while (paramm.hasNext()) {
        ((JSONArray)localObject).put(((m)paramm.next()).a());
      }
      cn.jiguang.ae.c.a(this.a, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.l().a(((JSONArray)localObject).toString()) });
      return;
    }
    finally {}
  }
  
  private boolean a(InetAddress paramInetAddress, int paramInt, DatagramSocket paramDatagramSocket, byte[] paramArrayOfByte)
  {
    boolean bool = false;
    try
    {
      DatagramPacket localDatagramPacket = new java/net/DatagramPacket;
      localDatagramPacket.<init>(paramArrayOfByte, paramArrayOfByte.length, paramInetAddress, paramInt);
      paramInt = j.c(c.a(c.a(paramDatagramSocket, localDatagramPacket)));
      if (paramInt == 0) {
        bool = true;
      }
      return bool;
    }
    catch (Throwable paramInetAddress)
    {
      paramDatagramSocket = new StringBuilder();
      paramDatagramSocket.append("report failed : ");
      paramDatagramSocket.append(paramInetAddress);
      a.i("SisConnContext", paramDatagramSocket.toString());
    }
    return false;
  }
  
  public l a(long paramLong)
  {
    Object localObject = new FutureTask(new i(this));
    this.d = 0;
    cn.jiguang.sdk.impl.b.a((Runnable)localObject, new int[0]);
    long l = paramLong;
    if (paramLong < 10L) {
      l = 10L;
    }
    try
    {
      localObject = (l)((FutureTask)localObject).get(l, TimeUnit.MILLISECONDS);
      return (l)localObject;
    }
    catch (ExecutionException|InterruptedException|TimeoutException localExecutionException) {}
    return null;
  }
  
  public void a(int paramInt1, String paramString, int paramInt2, long paramLong1, long paramLong2, int paramInt3)
  {
    if (!g.a(paramString, paramInt2)) {
      return;
    }
    m localm = new m();
    localm.a = cn.jiguang.sdk.impl.b.i(this.a);
    localm.b = paramInt1;
    localm.c = new g(paramString, paramInt2);
    localm.e = paramLong1;
    localm.f = paramLong2;
    localm.k = paramInt3;
    localm.g = h.a(this.a);
    localm.d = cn.jiguang.sdk.impl.b.e(this.a);
    localm.h = 200.0D;
    localm.i = 200.0D;
    localm.j = System.currentTimeMillis();
    paramString = e.a(this.a, "get_loc_info", null);
    if ((paramString instanceof Bundle)) {}
    try
    {
      paramString = (Bundle)paramString;
      localm.h = paramString.getDouble("lat");
      localm.i = paramString.getDouble("lot");
      localm.j = paramString.getLong("time");
      a(localm);
      return;
    }
    catch (Throwable paramString)
    {
      for (;;) {}
    }
  }
  
  public void a(final g paramg)
  {
    if (!((Boolean)cn.jiguang.ae.c.a(this.a, cn.jiguang.ae.b.q())).booleanValue()) {
      return;
    }
    if (j.a(((Long)cn.jiguang.ae.c.a(this.a, cn.jiguang.ae.b.s())).longValue(), 3600000L)) {
      cn.jiguang.sdk.impl.b.a(new Runnable()
      {
        /* Error */
        public void run()
        {
          // Byte code:
          //   0: new 29	java/net/DatagramSocket
          //   3: astore_2
          //   4: aload_2
          //   5: invokespecial 30	java/net/DatagramSocket:<init>	()V
          //   8: aload_2
          //   9: astore_1
          //   10: aload_0
          //   11: getfield 18	cn/jiguang/af/k$1:b	Lcn/jiguang/af/k;
          //   14: getfield 33	cn/jiguang/af/k:a	Landroid/content/Context;
          //   17: invokestatic 39	cn/jiguang/ae/b:l	()Lcn/jiguang/ae/b;
          //   20: invokestatic 44	cn/jiguang/ae/c:a	(Landroid/content/Context;Lcn/jiguang/ae/b;)Ljava/lang/Object;
          //   23: checkcast 46	java/lang/String
          //   26: astore_3
          //   27: aload_2
          //   28: astore_1
          //   29: aload_3
          //   30: invokestatic 52	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
          //   33: ifeq +54 -> 87
          //   36: aload_2
          //   37: astore_1
          //   38: ldc 54
          //   40: ldc 56
          //   42: invokestatic 62	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
          //   45: aload_2
          //   46: invokevirtual 65	java/net/DatagramSocket:close	()V
          //   49: goto +37 -> 86
          //   52: astore_2
          //   53: new 67	java/lang/StringBuilder
          //   56: dup
          //   57: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   60: astore_1
          //   61: aload_1
          //   62: ldc 70
          //   64: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   67: pop
          //   68: aload_1
          //   69: aload_2
          //   70: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
          //   73: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   76: pop
          //   77: ldc 54
          //   79: aload_1
          //   80: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   83: invokestatic 84	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
          //   86: return
          //   87: aload_2
          //   88: astore_1
          //   89: ldc 86
          //   91: aload_3
          //   92: invokestatic 91	cn/jiguang/af/c:a	(Ljava/lang/String;Ljava/lang/String;)[B
          //   95: astore 4
          //   97: aload_2
          //   98: astore_1
          //   99: new 67	java/lang/StringBuilder
          //   102: astore 5
          //   104: aload_2
          //   105: astore_1
          //   106: aload 5
          //   108: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   111: aload_2
          //   112: astore_1
          //   113: aload 5
          //   115: ldc 93
          //   117: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   120: pop
          //   121: aload_2
          //   122: astore_1
          //   123: aload 5
          //   125: aload 4
          //   127: arraylength
          //   128: invokevirtual 96	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
          //   131: pop
          //   132: aload_2
          //   133: astore_1
          //   134: aload 5
          //   136: ldc 98
          //   138: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   141: pop
          //   142: aload_2
          //   143: astore_1
          //   144: aload 5
          //   146: aload_0
          //   147: getfield 20	cn/jiguang/af/k$1:a	Lcn/jiguang/af/g;
          //   150: getfield 103	cn/jiguang/af/g:c	Ljava/net/InetAddress;
          //   153: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
          //   156: pop
          //   157: aload_2
          //   158: astore_1
          //   159: aload 5
          //   161: ldc 108
          //   163: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   166: pop
          //   167: aload_2
          //   168: astore_1
          //   169: aload 5
          //   171: aload_0
          //   172: getfield 20	cn/jiguang/af/k$1:a	Lcn/jiguang/af/g;
          //   175: getfield 111	cn/jiguang/af/g:b	I
          //   178: invokevirtual 96	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
          //   181: pop
          //   182: aload_2
          //   183: astore_1
          //   184: ldc 54
          //   186: aload 5
          //   188: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   191: invokestatic 62	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
          //   194: aload_2
          //   195: astore_1
          //   196: aload_0
          //   197: getfield 18	cn/jiguang/af/k$1:b	Lcn/jiguang/af/k;
          //   200: aload_0
          //   201: getfield 20	cn/jiguang/af/k$1:a	Lcn/jiguang/af/g;
          //   204: getfield 103	cn/jiguang/af/g:c	Ljava/net/InetAddress;
          //   207: aload_0
          //   208: getfield 20	cn/jiguang/af/k$1:a	Lcn/jiguang/af/g;
          //   211: getfield 111	cn/jiguang/af/g:b	I
          //   214: aload_2
          //   215: aload 4
          //   217: invokestatic 114	cn/jiguang/af/k:a	(Lcn/jiguang/af/k;Ljava/net/InetAddress;ILjava/net/DatagramSocket;[B)Z
          //   220: ifeq +108 -> 328
          //   223: aload_2
          //   224: astore_1
          //   225: new 67	java/lang/StringBuilder
          //   228: astore 4
          //   230: aload_2
          //   231: astore_1
          //   232: aload 4
          //   234: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   237: aload_2
          //   238: astore_1
          //   239: aload 4
          //   241: ldc 116
          //   243: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   246: pop
          //   247: aload_2
          //   248: astore_1
          //   249: aload 4
          //   251: aload_3
          //   252: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   255: pop
          //   256: aload_2
          //   257: astore_1
          //   258: ldc 54
          //   260: aload 4
          //   262: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   265: invokestatic 62	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
          //   268: aload_2
          //   269: astore_1
          //   270: aload_0
          //   271: getfield 18	cn/jiguang/af/k$1:b	Lcn/jiguang/af/k;
          //   274: getfield 33	cn/jiguang/af/k:a	Landroid/content/Context;
          //   277: iconst_1
          //   278: anewarray 35	cn/jiguang/ae/b
          //   281: dup
          //   282: iconst_0
          //   283: invokestatic 119	cn/jiguang/ae/b:s	()Lcn/jiguang/ae/b;
          //   286: invokestatic 125	android/os/SystemClock:elapsedRealtime	()J
          //   289: invokestatic 131	java/lang/Long:valueOf	(J)Ljava/lang/Long;
          //   292: invokevirtual 134	cn/jiguang/ae/b:a	(Ljava/lang/Object;)Lcn/jiguang/ae/b;
          //   295: aastore
          //   296: invokestatic 137	cn/jiguang/ae/c:a	(Landroid/content/Context;[Lcn/jiguang/ae/b;)V
          //   299: aload_2
          //   300: astore_1
          //   301: aload_0
          //   302: getfield 18	cn/jiguang/af/k$1:b	Lcn/jiguang/af/k;
          //   305: getfield 33	cn/jiguang/af/k:a	Landroid/content/Context;
          //   308: iconst_1
          //   309: anewarray 35	cn/jiguang/ae/b
          //   312: dup
          //   313: iconst_0
          //   314: invokestatic 39	cn/jiguang/ae/b:l	()Lcn/jiguang/ae/b;
          //   317: aconst_null
          //   318: invokevirtual 134	cn/jiguang/ae/b:a	(Ljava/lang/Object;)Lcn/jiguang/ae/b;
          //   321: aastore
          //   322: invokestatic 137	cn/jiguang/ae/c:a	(Landroid/content/Context;[Lcn/jiguang/ae/b;)V
          //   325: goto +48 -> 373
          //   328: aload_2
          //   329: astore_1
          //   330: new 67	java/lang/StringBuilder
          //   333: astore 4
          //   335: aload_2
          //   336: astore_1
          //   337: aload 4
          //   339: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   342: aload_2
          //   343: astore_1
          //   344: aload 4
          //   346: ldc -117
          //   348: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   351: pop
          //   352: aload_2
          //   353: astore_1
          //   354: aload 4
          //   356: aload_3
          //   357: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   360: pop
          //   361: aload_2
          //   362: astore_1
          //   363: ldc 54
          //   365: aload 4
          //   367: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   370: invokestatic 84	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
          //   373: aload_2
          //   374: invokevirtual 65	java/net/DatagramSocket:close	()V
          //   377: goto +118 -> 495
          //   380: astore_1
          //   381: new 67	java/lang/StringBuilder
          //   384: dup
          //   385: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   388: astore_2
          //   389: goto +81 -> 470
          //   392: astore_3
          //   393: goto +12 -> 405
          //   396: astore_1
          //   397: aconst_null
          //   398: astore_2
          //   399: goto +102 -> 501
          //   402: astore_3
          //   403: aconst_null
          //   404: astore_2
          //   405: aload_2
          //   406: astore_1
          //   407: new 67	java/lang/StringBuilder
          //   410: astore 4
          //   412: aload_2
          //   413: astore_1
          //   414: aload 4
          //   416: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   419: aload_2
          //   420: astore_1
          //   421: aload 4
          //   423: ldc -115
          //   425: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   428: pop
          //   429: aload_2
          //   430: astore_1
          //   431: aload 4
          //   433: aload_3
          //   434: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
          //   437: pop
          //   438: aload_2
          //   439: astore_1
          //   440: ldc 54
          //   442: aload 4
          //   444: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   447: invokestatic 84	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
          //   450: aload_2
          //   451: ifnull +44 -> 495
          //   454: aload_2
          //   455: invokevirtual 65	java/net/DatagramSocket:close	()V
          //   458: goto +37 -> 495
          //   461: astore_1
          //   462: new 67	java/lang/StringBuilder
          //   465: dup
          //   466: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   469: astore_2
          //   470: aload_2
          //   471: ldc 70
          //   473: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   476: pop
          //   477: aload_2
          //   478: aload_1
          //   479: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
          //   482: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   485: pop
          //   486: ldc 54
          //   488: aload_2
          //   489: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   492: invokestatic 84	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
          //   495: return
          //   496: astore_3
          //   497: aload_1
          //   498: astore_2
          //   499: aload_3
          //   500: astore_1
          //   501: aload_2
          //   502: ifnull +44 -> 546
          //   505: aload_2
          //   506: invokevirtual 65	java/net/DatagramSocket:close	()V
          //   509: goto +37 -> 546
          //   512: astore_2
          //   513: new 67	java/lang/StringBuilder
          //   516: dup
          //   517: invokespecial 68	java/lang/StringBuilder:<init>	()V
          //   520: astore_3
          //   521: aload_3
          //   522: ldc 70
          //   524: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   527: pop
          //   528: aload_3
          //   529: aload_2
          //   530: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
          //   533: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   536: pop
          //   537: ldc 54
          //   539: aload_3
          //   540: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   543: invokestatic 84	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
          //   546: aload_1
          //   547: athrow
          // Local variable table:
          //   start	length	slot	name	signature
          //   0	548	0	this	1
          //   9	354	1	localObject1	Object
          //   380	1	1	localThrowable1	Throwable
          //   396	1	1	localObject2	Object
          //   406	34	1	localObject3	Object
          //   461	37	1	localThrowable2	Throwable
          //   500	47	1	localObject4	Object
          //   3	43	2	localDatagramSocket	DatagramSocket
          //   52	322	2	localThrowable3	Throwable
          //   388	118	2	localObject5	Object
          //   512	18	2	localThrowable4	Throwable
          //   26	331	3	str	String
          //   392	1	3	localThrowable5	Throwable
          //   402	32	3	localThrowable6	Throwable
          //   496	4	3	localObject6	Object
          //   520	20	3	localStringBuilder1	StringBuilder
          //   95	348	4	localObject7	Object
          //   102	85	5	localStringBuilder2	StringBuilder
          // Exception table:
          //   from	to	target	type
          //   45	49	52	java/lang/Throwable
          //   373	377	380	java/lang/Throwable
          //   10	27	392	java/lang/Throwable
          //   29	36	392	java/lang/Throwable
          //   38	45	392	java/lang/Throwable
          //   89	97	392	java/lang/Throwable
          //   99	104	392	java/lang/Throwable
          //   106	111	392	java/lang/Throwable
          //   113	121	392	java/lang/Throwable
          //   123	132	392	java/lang/Throwable
          //   134	142	392	java/lang/Throwable
          //   144	157	392	java/lang/Throwable
          //   159	167	392	java/lang/Throwable
          //   169	182	392	java/lang/Throwable
          //   184	194	392	java/lang/Throwable
          //   196	223	392	java/lang/Throwable
          //   225	230	392	java/lang/Throwable
          //   232	237	392	java/lang/Throwable
          //   239	247	392	java/lang/Throwable
          //   249	256	392	java/lang/Throwable
          //   258	268	392	java/lang/Throwable
          //   270	299	392	java/lang/Throwable
          //   301	325	392	java/lang/Throwable
          //   330	335	392	java/lang/Throwable
          //   337	342	392	java/lang/Throwable
          //   344	352	392	java/lang/Throwable
          //   354	361	392	java/lang/Throwable
          //   363	373	392	java/lang/Throwable
          //   0	8	396	finally
          //   0	8	402	java/lang/Throwable
          //   454	458	461	java/lang/Throwable
          //   10	27	496	finally
          //   29	36	496	finally
          //   38	45	496	finally
          //   89	97	496	finally
          //   99	104	496	finally
          //   106	111	496	finally
          //   113	121	496	finally
          //   123	132	496	finally
          //   134	142	496	finally
          //   144	157	496	finally
          //   159	167	496	finally
          //   169	182	496	finally
          //   184	194	496	finally
          //   196	223	496	finally
          //   225	230	496	finally
          //   232	237	496	finally
          //   239	247	496	finally
          //   249	256	496	finally
          //   258	268	496	finally
          //   270	299	496	finally
          //   301	325	496	finally
          //   330	335	496	finally
          //   337	342	496	finally
          //   344	352	496	finally
          //   354	361	496	finally
          //   363	373	496	finally
          //   407	412	496	finally
          //   414	419	496	finally
          //   421	429	496	finally
          //   431	438	496	finally
          //   440	450	496	finally
          //   505	509	512	java/lang/Throwable
        }
      }, new int[0]);
    } else {
      a.c("SisConnContext", "sis report: not yet");
    }
  }
  
  public void a(boolean paramBoolean)
  {
    int j = this.d;
    int i;
    if (paramBoolean) {
      i = 1;
    } else {
      i = 2;
    }
    this.d = (i | j);
    cn.jiguang.aj.b.a().a(this.a, this.d);
  }
  
  public boolean a()
  {
    return false;
  }
  
  public byte[] a(Set<String> paramSet)
  {
    int i = h.a(this.a);
    if ((this.e == null) || (i != this.f)) {
      this.f = i;
    }
    try
    {
      this.e = c.a("UG", a(i).a(paramSet).toString());
      return this.e;
    }
    catch (Exception localException)
    {
      paramSet = new StringBuilder();
      paramSet.append("Failed to package data - ");
      paramSet.append(localException);
      throw new cn.jiguang.ah.g(2, paramSet.toString());
    }
  }
  
  public int b()
  {
    if (this.d == 0) {
      this.d = cn.jiguang.aj.b.a().a(this.a);
    }
    int i = cn.jiguang.aj.b.a().b(this.d);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ipvsupport=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", prefer=");
    localStringBuilder.append(i);
    a.c("SisConnContext", localStringBuilder.toString());
    return i;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/af/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */