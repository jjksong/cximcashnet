package cn.jiguang.api;

import android.content.Context;
import android.os.Bundle;
import java.nio.ByteBuffer;

public abstract class JDispatchAction
{
  public Object beforLogin(Context paramContext, String paramString1, int paramInt, String paramString2)
  {
    return null;
  }
  
  public Object beforRegister(Context paramContext, String paramString1, int paramInt, String paramString2)
  {
    return null;
  }
  
  public boolean checkAction(String paramString, int paramInt)
  {
    return true;
  }
  
  public abstract void dispatchMessage(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, ByteBuffer paramByteBuffer);
  
  public void dispatchTimeOutMessage(Context paramContext, String paramString, long paramLong, int paramInt) {}
  
  public abstract short getLogPriority(String paramString);
  
  public abstract short getLoginFlag(String paramString);
  
  public Bundle getPInfo(String paramString)
  {
    return null;
  }
  
  public abstract short getRegFlag(String paramString);
  
  public abstract short getRegPriority(String paramString);
  
  public abstract String getReportVersionKey(String paramString);
  
  public abstract String getSdkVersion(String paramString);
  
  public abstract short getUserCtrlProperty(String paramString);
  
  public void handleMessage(Context paramContext, String paramString, Object paramObject) {}
  
  public abstract boolean isSupportedCMD(String paramString, int paramInt);
  
  public void onActionRun(Context paramContext, String paramString1, String paramString2, Bundle paramBundle) {}
  
  public void onEvent(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2) {}
  
  public Object onSendData(Context paramContext, String paramString, long paramLong, int paramInt1, int paramInt2)
  {
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/JDispatchAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */