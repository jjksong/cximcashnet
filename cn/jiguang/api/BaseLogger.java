package cn.jiguang.api;

public abstract class BaseLogger
{
  public static void flushCached2File() {}
  
  private void log(int paramInt, boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable)
  {
    JCoreManager.onEvent(null, getCommonTag(), 18, paramString1, null, new Object[] { Integer.valueOf(paramInt), Boolean.valueOf(paramBoolean), paramString2, paramThrowable });
  }
  
  public void _d(String paramString1, String paramString2, Object... paramVarArgs) {}
  
  public void d(String paramString1, String paramString2)
  {
    log(3, true, paramString1, paramString2, null);
  }
  
  public void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(3, true, paramString1, paramString2, paramThrowable);
  }
  
  public void dd(String paramString1, String paramString2)
  {
    log(3, false, paramString1, paramString2, null);
  }
  
  public void dd(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(3, false, paramString1, paramString2, paramThrowable);
  }
  
  public void e(String paramString1, String paramString2)
  {
    log(6, true, paramString1, paramString2, null);
  }
  
  public void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(6, true, paramString1, paramString2, paramThrowable);
  }
  
  public void ee(String paramString1, String paramString2)
  {
    log(6, false, paramString1, paramString2, null);
  }
  
  public void ee(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(6, false, paramString1, paramString2, paramThrowable);
  }
  
  public abstract String getCommonTag();
  
  public void i(String paramString1, String paramString2)
  {
    log(4, true, paramString1, paramString2, null);
  }
  
  public void i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(4, true, paramString1, paramString2, paramThrowable);
  }
  
  public void ii(String paramString1, String paramString2)
  {
    log(4, false, paramString1, paramString2, null);
  }
  
  public void ii(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(4, false, paramString1, paramString2, paramThrowable);
  }
  
  public void v(String paramString1, String paramString2)
  {
    log(2, true, paramString1, paramString2, null);
  }
  
  public void v(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(2, true, paramString1, paramString2, paramThrowable);
  }
  
  public void vv(String paramString1, String paramString2)
  {
    log(2, false, paramString1, paramString2, null);
  }
  
  public void vv(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(2, false, paramString1, paramString2, paramThrowable);
  }
  
  public void w(String paramString1, String paramString2)
  {
    log(5, true, paramString1, paramString2, null);
  }
  
  public void w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(5, true, paramString1, paramString2, paramThrowable);
  }
  
  public void ww(String paramString1, String paramString2)
  {
    log(5, false, paramString1, paramString2, null);
  }
  
  public void ww(String paramString1, String paramString2, Throwable paramThrowable)
  {
    log(5, false, paramString1, paramString2, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/BaseLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */