package cn.jiguang.api;

import cn.jiguang.api.utils.ProtocolUtil;
import cn.jiguang.aq.c;
import cn.jiguang.aq.d;
import cn.jiguang.aq.f;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public abstract class JProtocol
{
  public static final int DEFAULT_RESP_CODE = 0;
  public static final int DEFAULT_RID = 0;
  public static final int DEFAULT_SID = 0;
  public static final int NO_JUID = -1;
  public static final int NO_RESP_CODE = -1;
  public static final int NO_SID = -1;
  private static final int PACKET_SIZE = 7168;
  private static final String TAG = "JProtocol";
  protected ByteBuffer body;
  protected c head;
  private boolean isRequest;
  
  public JProtocol(boolean paramBoolean, int paramInt1, int paramInt2, long paramLong)
  {
    this.isRequest = paramBoolean;
    this.head = new c(paramBoolean, paramInt1, paramInt2, paramLong);
    this.body = ByteBuffer.allocate(7168);
  }
  
  public JProtocol(boolean paramBoolean, int paramInt1, int paramInt2, long paramLong1, int paramInt3, long paramLong2)
  {
    this.isRequest = paramBoolean;
    this.head = new c(paramBoolean, 0, paramInt1, paramInt2, paramLong1, paramInt3, paramLong2);
    this.body = ByteBuffer.allocate(7168);
  }
  
  public JProtocol(boolean paramBoolean, Object paramObject, ByteBuffer paramByteBuffer)
  {
    this.isRequest = paramBoolean;
    this.head = ((c)paramObject);
    if (paramByteBuffer != null)
    {
      this.body = paramByteBuffer;
      parseBody();
    }
    else
    {
      d.c("JProtocol", "No body to parse.");
    }
  }
  
  public JProtocol(boolean paramBoolean, ByteBuffer paramByteBuffer, byte[] paramArrayOfByte)
  {
    this.isRequest = paramBoolean;
    try
    {
      localObject = new cn/jiguang/aq/c;
      ((c)localObject).<init>(paramBoolean, paramArrayOfByte);
      this.head = ((c)localObject);
    }
    catch (Exception paramArrayOfByte)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("create JHead failed:");
      ((StringBuilder)localObject).append(paramArrayOfByte.getMessage());
      d.c("JProtocol", ((StringBuilder)localObject).toString());
    }
    if (paramByteBuffer != null)
    {
      this.body = paramByteBuffer;
      parseBody();
    }
    else
    {
      d.c("JProtocol", "No body to parse.");
    }
  }
  
  public static byte[] parseHead(Object paramObject)
  {
    if (paramObject == null) {}
    for (paramObject = "object was null";; paramObject = "unknow Object")
    {
      d.c("JProtocol", (String)paramObject);
      return null;
      if ((paramObject instanceof c)) {
        return ((c)paramObject).f();
      }
    }
  }
  
  private final byte[] toBytes()
  {
    Object localObject = new ByteArrayOutputStream();
    byte[] arrayOfByte = ProtocolUtil.getBytes(this.body);
    if (arrayOfByte == null)
    {
      d.c("JProtocol", "toBytes bodyBytes  is  null");
      return null;
    }
    c localc = this.head;
    int i;
    if (this.isRequest) {
      i = 24;
    } else {
      i = 20;
    }
    localc.a(i + arrayOfByte.length);
    try
    {
      ((ByteArrayOutputStream)localObject).write(this.head.f());
      ((ByteArrayOutputStream)localObject).write(arrayOfByte);
      arrayOfByte = ((ByteArrayOutputStream)localObject).toByteArray();
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("Final - len:");
      ((StringBuilder)localObject).append(arrayOfByte.length);
      ((StringBuilder)localObject).append(", bytes: ");
      ((StringBuilder)localObject).append(f.a(arrayOfByte));
      d.b("JProtocol", ((StringBuilder)localObject).toString());
      return arrayOfByte;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public ByteBuffer getBody()
  {
    return this.body;
  }
  
  public int getCommand()
  {
    return this.head.a();
  }
  
  public c getHead()
  {
    return this.head;
  }
  
  public long getJuid()
  {
    return this.head.c();
  }
  
  public abstract String getName();
  
  public Long getRid()
  {
    return this.head.b();
  }
  
  public int getSid()
  {
    return this.head.d();
  }
  
  public int getVersion()
  {
    return this.head.e();
  }
  
  protected abstract boolean isNeedParseeErrorMsg();
  
  protected abstract void parseBody();
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    String str;
    if (this.isRequest) {
      str = "[Request]";
    } else {
      str = "[Response]";
    }
    localStringBuilder.append(str);
    localStringBuilder.append(" - ");
    localStringBuilder.append(this.head.toString());
    return localStringBuilder.toString();
  }
  
  protected abstract void writeBody();
  
  public final byte[] writeBodyAndToBytes()
  {
    this.body.clear();
    writeBody();
    this.body.flip();
    return toBytes();
  }
  
  protected void writeBytes(byte[] paramArrayOfByte)
  {
    this.body.put(paramArrayOfByte);
  }
  
  protected void writeInt1(int paramInt)
  {
    this.body.put((byte)paramInt);
  }
  
  protected void writeInt2(int paramInt)
  {
    this.body.putShort((short)paramInt);
  }
  
  protected void writeInt4(int paramInt)
  {
    this.body.putInt(paramInt);
  }
  
  protected void writeLong8(long paramLong)
  {
    this.body.putLong(paramLong);
  }
  
  protected void writeTlv2(String paramString)
  {
    this.body.put(ProtocolUtil.tlv2ToByteArray(paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/JProtocol.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */