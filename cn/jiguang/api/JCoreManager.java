package cn.jiguang.api;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ab.d;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;
import cn.jpush.android.service.DataShare;
import java.lang.reflect.Method;
import java.util.List;

public class JCoreManager
{
  public static final boolean IG = false;
  public static final boolean SD = false;
  public static final String SDK_NAME = "";
  public static final String SDK_VERSION = "2.0.0";
  private static final String TAG = "JCoreManager";
  
  public static void addDispatchAction(String paramString1, String paramString2)
  {
    cn.jiguang.ab.a.a().a(paramString1, paramString2);
  }
  
  public static Context getAppContext(Context paramContext)
  {
    return cn.jiguang.a.a.a(paramContext);
  }
  
  @Deprecated
  public static boolean getConnectionState(Context paramContext)
  {
    try
    {
      boolean bool = DataShare.alreadyBound();
      if (bool) {
        try
        {
          bool = DataShare.getInstance().isPushLoggedIn();
          return bool;
        }
        catch (Throwable localThrowable)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("isPushLoggedIn e:");
          ((StringBuilder)localObject).append(localThrowable);
          c.h("JCoreManager", ((StringBuilder)localObject).toString());
        }
      }
      Object localObject = getAppContext(paramContext);
      String str = cn.jiguang.as.a.a((Context)localObject);
      paramContext = d.a((Context)localObject);
      if ((!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty(paramContext)) && (str.equals(paramContext))) {
        return cn.jiguang.sdk.impl.b.b();
      }
      paramContext = d.a().b((Context)localObject, "isTcpLoggedIn", null);
      if ((paramContext != null) && (paramContext.containsKey("state"))) {
        return paramContext.getBoolean("state");
      }
      bool = cn.jiguang.sdk.impl.b.b();
      return bool;
    }
    catch (Throwable paramContext) {}
    return false;
  }
  
  public static boolean getDebugMode()
  {
    return cn.jiguang.a.a.c;
  }
  
  public static void init(Context paramContext)
  {
    cn.jiguang.a.a.f = cn.jiguang.a.a.e;
    cn.jiguang.a.a.e = true;
    cn.jiguang.a.a.a(paramContext, "tcp_a1", null);
    if (((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.w())).longValue() <= 0L) {
      cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { cn.jiguang.e.a.w().a(Long.valueOf(System.currentTimeMillis())) });
    }
  }
  
  public static void initCrashHandler(Context paramContext)
  {
    cn.jiguang.b.a.a().a(paramContext);
  }
  
  public static boolean isInternal()
  {
    return false;
  }
  
  public static boolean isTestEnv()
  {
    return cn.jiguang.a.a.a();
  }
  
  public static Object onEvent(Context paramContext, String paramString1, int paramInt, String paramString2, Bundle paramBundle, Object... paramVarArgs)
  {
    return e.a(paramContext, paramString1, paramInt, paramString2, paramBundle, paramVarArgs);
  }
  
  public static void requestPermission(Context paramContext)
  {
    if (paramContext == null) {}
    for (paramContext = "[requestPermission] context was null";; paramContext = "[requestPermission] context must instanceof Activity")
    {
      c.g("JCoreManager", paramContext);
      return;
      if ((paramContext instanceof Activity)) {
        break;
      }
    }
    if ((Build.VERSION.SDK_INT >= 23) && (paramContext.getApplicationInfo().targetSdkVersion >= 23)) {
      try
      {
        List localList = cn.jiguang.as.a.a(paramContext, new String[] { "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_PHONE_STATE" });
        if ((localList != null) && (!localList.isEmpty()))
        {
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("lackPermissions:");
          localStringBuilder.append(localList);
          c.a("JCoreManager", localStringBuilder.toString());
          Class.forName("android.app.Activity").getDeclaredMethod("requestPermissions", new Class[] { String[].class, Integer.TYPE }).invoke(paramContext, new Object[] { localList.toArray(new String[localList.size()]), Integer.valueOf(1) });
        }
        else {}
      }
      catch (Exception localException)
      {
        paramContext = new StringBuilder();
        paramContext.append("#unexcepted - requestPermission e:");
        paramContext.append(localException.getMessage());
        c.g("JCoreManager", paramContext.toString());
      }
    }
  }
  
  public static void setAnalysisAction(JAnalyticsAction paramJAnalyticsAction)
  {
    cn.jiguang.a.a.b = paramJAnalyticsAction;
  }
  
  public static void setDebugMode(boolean paramBoolean)
  {
    cn.jiguang.a.a.c = paramBoolean;
  }
  
  public static void setSDKConfigs(Context paramContext, Bundle paramBundle)
  {
    e.a(paramContext, cn.jiguang.a.a.d, 55, null, paramBundle, new Object[0]);
  }
  
  public static void stopCrashHandler(Context paramContext)
  {
    cn.jiguang.b.a.a().b(paramContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/JCoreManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */