package cn.jiguang.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import cn.jiguang.e.a;
import cn.jiguang.e.b;

public class MultiSpHelper
{
  private static final String SP_FILE = "cn.jpush.android.user.profile";
  private static final String TAG = "MultiSpHelper";
  private static SharedPreferences sharedPref;
  
  public static void commitBoolean(Context paramContext, String paramString, boolean paramBoolean)
  {
    getSp(paramContext).edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public static void commitInt(Context paramContext, String paramString, int paramInt)
  {
    getSp(paramContext).edit().putInt(paramString, paramInt).apply();
  }
  
  public static void commitLong(Context paramContext, String paramString, long paramLong)
  {
    getSp(paramContext).edit().putLong(paramString, paramLong).apply();
  }
  
  public static void commitString(Context paramContext, String paramString1, String paramString2)
  {
    getSp(paramContext).edit().putString(paramString1, paramString2).apply();
  }
  
  public static boolean getBoolean(Context paramContext, String paramString, boolean paramBoolean)
  {
    return getSp(paramContext).getBoolean(paramString, paramBoolean);
  }
  
  public static int getInt(Context paramContext, String paramString, int paramInt)
  {
    if (paramString.equals("jpush_register_code")) {
      return ((Integer)b.a(paramContext, new a("cn.jpush.android.user.profile", paramString, Integer.valueOf(paramInt)).y())).intValue();
    }
    return getSp(paramContext).getInt(paramString, paramInt);
  }
  
  public static long getLong(Context paramContext, String paramString, long paramLong)
  {
    return getSp(paramContext).getLong(paramString, paramLong);
  }
  
  private static SharedPreferences getSp(Context paramContext)
  {
    if (sharedPref == null) {
      init(paramContext);
    }
    return sharedPref;
  }
  
  public static String getString(Context paramContext, String paramString1, String paramString2)
  {
    return getSp(paramContext).getString(paramString1, paramString2);
  }
  
  private static void init(Context paramContext)
  {
    sharedPref = paramContext.getSharedPreferences("cn.jpush.android.user.profile", 0);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/MultiSpHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */