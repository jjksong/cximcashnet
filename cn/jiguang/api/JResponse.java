package cn.jiguang.api;

import cn.jiguang.api.utils.ByteBufferUtils;
import java.nio.ByteBuffer;

public abstract class JResponse
  extends JProtocol
{
  public int code;
  
  public JResponse(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3, String paramString)
  {
    super(false, paramInt1, paramInt2, paramLong1, -1, paramLong2);
    this.code = paramInt3;
  }
  
  public JResponse(Object paramObject, ByteBuffer paramByteBuffer)
  {
    super(false, paramObject, paramByteBuffer);
  }
  
  public JResponse(ByteBuffer paramByteBuffer, byte[] paramArrayOfByte)
  {
    super(false, paramByteBuffer, paramArrayOfByte);
  }
  
  protected void parseBody()
  {
    if (isNeedParseeErrorMsg()) {
      this.code = ByteBufferUtils.getShort(this.body, this);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JResponse{code=");
    localStringBuilder.append(this.code);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  protected void writeBody()
  {
    int i = this.code;
    if (i >= 0) {
      writeInt2(i);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/JResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */