package cn.jiguang.api.utils;

import cn.jiguang.api.JResponse;
import cn.jiguang.aq.d;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class ByteBufferUtils
{
  public static final int ERROR_CODE = 10000;
  private static final String TAG = "ByteBufferUtils";
  
  private static String generalExtraStr(Throwable paramThrowable, JResponse paramJResponse, ByteBuffer paramByteBuffer)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    if (paramJResponse != null)
    {
      localStringBuilder1.append(paramJResponse.toString());
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append("|bytebuffer:");
      if (paramByteBuffer == null) {
        paramJResponse = "byteBuffer is null";
      } else {
        paramJResponse = paramByteBuffer.toString();
      }
      localStringBuilder2.append(paramJResponse);
      localStringBuilder1.append(localStringBuilder2.toString());
    }
    paramJResponse = new StringBuilder();
    paramJResponse.append("byteBuffer info:");
    paramJResponse.append(localStringBuilder1.toString());
    d.f("ByteBufferUtils", paramJResponse.toString());
    try
    {
      paramJResponse = new java/io/StringWriter;
      paramJResponse.<init>();
      paramByteBuffer = new java/io/PrintWriter;
      paramByteBuffer.<init>(paramJResponse);
      paramThrowable.printStackTrace(paramByteBuffer);
      paramJResponse = paramJResponse.toString();
      paramThrowable = new java/lang/StringBuilder;
      paramThrowable.<init>();
      paramThrowable.append("parse data error stackTrace:");
      paramThrowable.append(paramJResponse);
      d.f("ByteBufferUtils", paramThrowable.toString());
      return localStringBuilder1.toString();
    }
    catch (Exception paramThrowable)
    {
      for (;;) {}
    }
  }
  
  public static Byte get(ByteBuffer paramByteBuffer, JResponse paramJResponse)
  {
    Throwable localThrowable3;
    try
    {
      byte b = paramByteBuffer.get();
      return Byte.valueOf(b);
    }
    catch (Exception localException)
    {
      Throwable localThrowable1 = localException.fillInStackTrace();
    }
    catch (BufferOverflowException localBufferOverflowException)
    {
      Throwable localThrowable2 = localBufferOverflowException.fillInStackTrace();
    }
    catch (BufferUnderflowException localBufferUnderflowException)
    {
      localThrowable3 = localBufferUnderflowException.fillInStackTrace();
    }
    onException(localThrowable3, paramJResponse, paramByteBuffer);
    if (paramJResponse != null) {
      paramJResponse.code = 10000;
    }
    return null;
  }
  
  public static ByteBuffer get(ByteBuffer paramByteBuffer, byte[] paramArrayOfByte, JResponse paramJResponse)
  {
    try
    {
      paramArrayOfByte = paramByteBuffer.get(paramArrayOfByte);
      return paramArrayOfByte;
    }
    catch (Exception paramArrayOfByte)
    {
      paramArrayOfByte = paramArrayOfByte.fillInStackTrace();
    }
    catch (BufferOverflowException paramArrayOfByte)
    {
      paramArrayOfByte = paramArrayOfByte.fillInStackTrace();
    }
    catch (BufferUnderflowException paramArrayOfByte)
    {
      paramArrayOfByte = paramArrayOfByte.fillInStackTrace();
    }
    onException(paramArrayOfByte, paramJResponse, paramByteBuffer);
    if (paramJResponse != null) {
      paramJResponse.code = 10000;
    }
    return null;
  }
  
  public static int getInt(ByteBuffer paramByteBuffer, JResponse paramJResponse)
  {
    Throwable localThrowable3;
    try
    {
      int i = paramByteBuffer.getInt();
      return i;
    }
    catch (Exception localException)
    {
      Throwable localThrowable1 = localException.fillInStackTrace();
    }
    catch (BufferOverflowException localBufferOverflowException)
    {
      Throwable localThrowable2 = localBufferOverflowException.fillInStackTrace();
    }
    catch (BufferUnderflowException localBufferUnderflowException)
    {
      localThrowable3 = localBufferUnderflowException.fillInStackTrace();
    }
    onException(localThrowable3, paramJResponse, paramByteBuffer);
    if (paramJResponse != null) {
      paramJResponse.code = 10000;
    }
    return -1;
  }
  
  public static long getLong(ByteBuffer paramByteBuffer, JResponse paramJResponse)
  {
    Throwable localThrowable3;
    try
    {
      long l = paramByteBuffer.getLong();
      return l;
    }
    catch (Exception localException)
    {
      Throwable localThrowable1 = localException.fillInStackTrace();
    }
    catch (BufferOverflowException localBufferOverflowException)
    {
      Throwable localThrowable2 = localBufferOverflowException.fillInStackTrace();
    }
    catch (BufferUnderflowException localBufferUnderflowException)
    {
      localThrowable3 = localBufferUnderflowException.fillInStackTrace();
    }
    onException(localThrowable3, paramJResponse, paramByteBuffer);
    if (paramJResponse != null) {
      paramJResponse.code = 10000;
    }
    return 0L;
  }
  
  public static short getShort(ByteBuffer paramByteBuffer, JResponse paramJResponse)
  {
    Throwable localThrowable3;
    try
    {
      short s = paramByteBuffer.getShort();
      return s;
    }
    catch (Exception localException)
    {
      Throwable localThrowable1 = localException.fillInStackTrace();
    }
    catch (BufferOverflowException localBufferOverflowException)
    {
      Throwable localThrowable2 = localBufferOverflowException.fillInStackTrace();
    }
    catch (BufferUnderflowException localBufferUnderflowException)
    {
      localThrowable3 = localBufferUnderflowException.fillInStackTrace();
    }
    onException(localThrowable3, paramJResponse, paramByteBuffer);
    if (paramJResponse != null) {
      paramJResponse.code = 10000;
    }
    return -1;
  }
  
  private static void onException(Throwable paramThrowable, JResponse paramJResponse, ByteBuffer paramByteBuffer)
  {
    generalExtraStr(paramThrowable, paramJResponse, paramByteBuffer);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/utils/ByteBufferUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */