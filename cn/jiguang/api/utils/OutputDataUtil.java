package cn.jiguang.api.utils;

import cn.jiguang.aq.d;
import java.math.BigInteger;

public class OutputDataUtil
{
  private static final String TAG = "OutputDataUtil";
  private static final BigInteger TWO_64 = BigInteger.ONE.shiftLeft(64);
  private byte[] array;
  private int pos;
  private int saved_pos;
  
  public OutputDataUtil()
  {
    this(32);
  }
  
  public OutputDataUtil(int paramInt)
  {
    this.array = new byte[paramInt];
    this.pos = 0;
    this.saved_pos = -1;
  }
  
  private void check(long paramLong, int paramInt)
  {
    long l = 1L << paramInt;
    if ((paramLong < 0L) || (paramLong > l))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" out of range for ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" bit value max:");
      localStringBuilder.append(l);
      d.c("OutputDataUtil", localStringBuilder.toString());
    }
  }
  
  public static int encodeZigZag32(int paramInt)
  {
    return paramInt >> 31 ^ paramInt << 1;
  }
  
  public static long encodeZigZag64(long paramLong)
  {
    return paramLong >> 63 ^ paramLong << 1;
  }
  
  private void need(int paramInt)
  {
    byte[] arrayOfByte = this.array;
    int i = arrayOfByte.length;
    int k = this.pos;
    if (i - k >= paramInt) {
      return;
    }
    int j = arrayOfByte.length * 2;
    i = j;
    if (j < k + paramInt) {
      i = k + paramInt;
    }
    arrayOfByte = new byte[i];
    System.arraycopy(this.array, 0, arrayOfByte, 0, this.pos);
    this.array = arrayOfByte;
  }
  
  public int current()
  {
    return this.pos;
  }
  
  public void jump(int paramInt)
  {
    if (paramInt <= this.pos)
    {
      this.pos = paramInt;
      return;
    }
    throw new IllegalArgumentException("cannot jump past end of data");
  }
  
  public void restore()
  {
    int i = this.saved_pos;
    if (i >= 0)
    {
      this.pos = i;
      this.saved_pos = -1;
      return;
    }
    throw new IllegalStateException("no previous state");
  }
  
  public void save()
  {
    this.saved_pos = this.pos;
  }
  
  public byte[] toByteArray()
  {
    int i = this.pos;
    byte[] arrayOfByte = new byte[i];
    System.arraycopy(this.array, 0, arrayOfByte, 0, i);
    return arrayOfByte;
  }
  
  public void writeByteArray(byte[] paramArrayOfByte)
  {
    writeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void writeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    need(paramInt2);
    System.arraycopy(paramArrayOfByte, paramInt1, this.array, this.pos, paramInt2);
    this.pos += paramInt2;
  }
  
  public void writeByteArrayincludeLength(byte[] paramArrayOfByte)
  {
    writeU16(paramArrayOfByte.length);
    writeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void writeCountedString(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte.length <= 255)
    {
      need(paramArrayOfByte.length + 1);
      byte[] arrayOfByte = this.array;
      int i = this.pos;
      this.pos = (i + 1);
      arrayOfByte[i] = ((byte)(0xFF & paramArrayOfByte.length));
      writeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
      return;
    }
    throw new IllegalArgumentException("Invalid counted string");
  }
  
  public void writeRawLittleEndian16(int paramInt)
  {
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >> 8 & 0xFF));
  }
  
  public void writeRawLittleEndian32(int paramInt)
  {
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >> 8 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >> 16 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >> 24 & 0xFF));
  }
  
  public void writeRawLittleEndian64(long paramLong)
  {
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)paramLong & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 8) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 16) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 24) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 32) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 40) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 48) & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)((int)(paramLong >> 56) & 0xFF));
  }
  
  public void writeU16(int paramInt)
  {
    check(paramInt, 16);
    need(2);
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >>> 8 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
  }
  
  public void writeU16At(int paramInt1, int paramInt2)
  {
    check(paramInt1, 16);
    if (paramInt2 <= this.pos - 2)
    {
      byte[] arrayOfByte = this.array;
      arrayOfByte[paramInt2] = ((byte)(paramInt1 >>> 8 & 0xFF));
      arrayOfByte[(paramInt2 + 1)] = ((byte)(paramInt1 & 0xFF));
      return;
    }
    throw new IllegalArgumentException("cannot write past end of data");
  }
  
  public void writeU32(long paramLong)
  {
    check(paramLong, 32);
    need(4);
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public void writeU32At(long paramLong, int paramInt)
  {
    check(paramLong, 32);
    if (paramInt <= this.pos - 4)
    {
      byte[] arrayOfByte = this.array;
      int i = paramInt + 1;
      arrayOfByte[paramInt] = ((byte)(int)(paramLong >>> 24 & 0xFF));
      paramInt = i + 1;
      arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
      arrayOfByte[paramInt] = ((byte)(int)(paramLong >>> 8 & 0xFF));
      arrayOfByte[(paramInt + 1)] = ((byte)(int)(paramLong & 0xFF));
      return;
    }
    throw new IllegalArgumentException("cannot write past end of data");
  }
  
  public void writeU64(long paramLong)
  {
    need(8);
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 56 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 48 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 40 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 32 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public void writeU64At(long paramLong, int paramInt)
  {
    byte[] arrayOfByte = this.array;
    int j = paramInt + 1;
    arrayOfByte[paramInt] = ((byte)(int)(paramLong >>> 56 & 0xFF));
    int i = j + 1;
    arrayOfByte[j] = ((byte)(int)(paramLong >>> 48 & 0xFF));
    paramInt = i + 1;
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 40 & 0xFF));
    i = paramInt + 1;
    arrayOfByte[paramInt] = ((byte)(int)(paramLong >>> 32 & 0xFF));
    paramInt = i + 1;
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = paramInt + 1;
    arrayOfByte[paramInt] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    arrayOfByte[(i + 1)] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public void writeU8(int paramInt)
  {
    check(paramInt, 8);
    need(1);
    byte[] arrayOfByte = this.array;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
  }
  
  public void writeU8At(int paramInt1, int paramInt2)
  {
    check(paramInt1, 8);
    if (paramInt2 <= this.pos - 1)
    {
      this.array[paramInt2] = ((byte)(paramInt1 & 0xFF));
      return;
    }
    throw new IllegalArgumentException("cannot write past end of data");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/utils/OutputDataUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */