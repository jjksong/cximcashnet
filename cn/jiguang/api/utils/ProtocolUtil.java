package cn.jiguang.api.utils;

import cn.jiguang.api.JResponse;
import cn.jiguang.aq.d;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class ProtocolUtil
{
  private static final String ENCODING_UTF_8 = "UTF-8";
  private static final String TAG = "ProtocolUtil";
  
  public static void fillIntData(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    System.arraycopy(int2ByteArray(paramInt1), 0, paramArrayOfByte, paramInt2, 4);
  }
  
  public static void fillStringData(byte[] paramArrayOfByte, String paramString, int paramInt)
  {
    paramString = paramString.getBytes();
    System.arraycopy(paramString, 0, paramArrayOfByte, paramInt, paramString.length);
  }
  
  public static byte[] fixedStringToBytes(String paramString, int paramInt)
  {
    byte[] arrayOfByte;
    if ((paramString != null) && (!"".equals(paramString))) {
      arrayOfByte = null;
    }
    try
    {
      paramString = paramString.getBytes("UTF-8");
      if (paramString == null) {
        return new byte[] { 0, 0, 0, 0 };
      }
      arrayOfByte = getDefaultByte(paramInt);
      if (paramString.length <= paramInt) {
        paramInt = paramString.length;
      }
      System.arraycopy(paramString, 0, arrayOfByte, 0, paramInt);
      return arrayOfByte;
      return new byte[] { 0, 0, 0, 0 };
    }
    catch (UnsupportedEncodingException paramString)
    {
      for (;;)
      {
        paramString = arrayOfByte;
      }
    }
  }
  
  public static byte[] getBytes(ByteBuffer paramByteBuffer)
  {
    try
    {
      byte[] arrayOfByte = new byte[paramByteBuffer.remaining()];
      paramByteBuffer.asReadOnlyBuffer().flip();
      paramByteBuffer.get(arrayOfByte);
      return arrayOfByte;
    }
    catch (Exception paramByteBuffer)
    {
      return null;
    }
    catch (NegativeArraySizeException paramByteBuffer)
    {
      d.c("ProtocolUtil", "[getBytes] - ByteBuffer error.");
    }
    return null;
  }
  
  public static byte[] getBytesConsumed(ByteBuffer paramByteBuffer)
  {
    try
    {
      byte[] arrayOfByte = new byte[paramByteBuffer.remaining()];
      paramByteBuffer.get(arrayOfByte);
      return arrayOfByte;
    }
    catch (Exception paramByteBuffer)
    {
      return null;
    }
    catch (NegativeArraySizeException paramByteBuffer)
    {
      d.c("ProtocolUtil", "[getBytesConsumed] - ByteBuffer error.");
    }
    return null;
  }
  
  public static byte[] getDefaultByte(int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    for (paramInt = 0; paramInt < arrayOfByte.length; paramInt++) {
      arrayOfByte[0] = 0;
    }
    return arrayOfByte;
  }
  
  public static String getString(ByteBuffer paramByteBuffer, int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    paramByteBuffer.get(arrayOfByte);
    try
    {
      paramByteBuffer = new String(arrayOfByte, "UTF-8");
      return paramByteBuffer;
    }
    catch (UnsupportedEncodingException paramByteBuffer) {}
    return null;
  }
  
  public static String getTlv2(ByteBuffer paramByteBuffer)
  {
    try
    {
      byte[] arrayOfByte = new byte[paramByteBuffer.getShort()];
      paramByteBuffer.get(arrayOfByte);
      paramByteBuffer = new String(arrayOfByte, "UTF-8");
      return paramByteBuffer;
    }
    catch (UnsupportedEncodingException|Exception paramByteBuffer) {}
    return null;
  }
  
  public static String getTlv2(ByteBuffer paramByteBuffer, JResponse paramJResponse)
  {
    int i = ByteBufferUtils.getShort(paramByteBuffer, paramJResponse);
    if (i < 0)
    {
      d.c("ProtocolUtil", "[getTlv2] - ByteBuffer error.");
      return null;
    }
    byte[] arrayOfByte = new byte[i];
    ByteBufferUtils.get(paramByteBuffer, arrayOfByte, paramJResponse);
    try
    {
      paramByteBuffer = new String(arrayOfByte, "UTF-8");
      return paramByteBuffer;
    }
    catch (Throwable paramByteBuffer) {}
    return null;
  }
  
  public static byte[] int2ByteArray(int paramInt)
  {
    return new byte[] { (byte)(paramInt >>> 24), (byte)(paramInt >>> 16), (byte)(paramInt >>> 8), (byte)paramInt };
  }
  
  public static byte[] long2ByteArray(long paramLong)
  {
    return new byte[] { (byte)(int)(paramLong >>> 56), (byte)(int)(paramLong >>> 48), (byte)(int)(paramLong >>> 40), (byte)(int)(paramLong >>> 32), (byte)(int)(paramLong >>> 24), (byte)(int)(paramLong >>> 16), (byte)(int)(paramLong >>> 8), (byte)(int)paramLong };
  }
  
  public static byte[] short2ByteArray(short paramShort)
  {
    return new byte[] { (byte)(paramShort >>> 8), (byte)paramShort };
  }
  
  public static byte[] tlv2ToByteArray(String paramString)
  {
    byte[] arrayOfByte;
    if ((paramString != null) && (!"".equals(paramString))) {
      arrayOfByte = null;
    }
    try
    {
      paramString = paramString.getBytes("UTF-8");
      if (paramString == null) {
        return new byte[] { 0, 0 };
      }
      short s = (short)paramString.length;
      arrayOfByte = new byte[s + 2];
      System.arraycopy(short2ByteArray(s), 0, arrayOfByte, 0, 2);
      System.arraycopy(paramString, 0, arrayOfByte, 2, s);
      return arrayOfByte;
    }
    catch (UnsupportedEncodingException paramString)
    {
      for (;;)
      {
        paramString = arrayOfByte;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/utils/ProtocolUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */