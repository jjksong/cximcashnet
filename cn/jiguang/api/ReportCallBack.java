package cn.jiguang.api;

public abstract interface ReportCallBack
{
  public static final int ERROR = -2;
  public static final int FAILED = -1;
  public static final int OK = 0;
  
  public abstract void onFinish(int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/ReportCallBack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */