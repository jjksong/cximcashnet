package cn.jiguang.api;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import cn.jiguang.aq.a;
import cn.jiguang.aq.b;
import cn.jiguang.aq.c;
import cn.jiguang.aq.d;
import cn.jiguang.aq.e;
import org.json.JSONObject;

public class JCoreInterface
{
  public static String DAEMON_ACTION = "cn.jpush.android.intent.DaemonService";
  private static final String TAG = "JCoreInterface";
  private static Context mApplicationContext;
  
  public static void asyncExecute(Runnable paramRunnable, int... paramVarArgs)
  {
    JCoreManager.onEvent(null, null, 12, null, null, new Object[] { paramRunnable });
  }
  
  public static boolean canCallDirect()
  {
    return false;
  }
  
  public static void execute(String paramString, Runnable paramRunnable, int... paramVarArgs)
  {
    JCoreManager.onEvent(null, null, 11, paramString, null, new Object[] { paramRunnable });
  }
  
  public static JSONObject fillBaseReport(JSONObject paramJSONObject, String paramString)
  {
    paramJSONObject = JCoreManager.onEvent(null, null, 26, null, null, new Object[] { paramJSONObject, paramString });
    if ((paramJSONObject instanceof JSONObject)) {
      return (JSONObject)paramJSONObject;
    }
    return null;
  }
  
  public static String getAccountId()
  {
    Object localObject = JCoreManager.onEvent(null, null, 5, null, null, new Object[0]);
    if ((localObject instanceof String)) {
      return (String)localObject;
    }
    return "";
  }
  
  public static String getAppKey()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 7, null, null, new Object[0]);
    if ((localObject instanceof String)) {
      return (String)localObject;
    }
    return "";
  }
  
  public static String getChannel()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 6, null, null, new Object[0]);
    if ((localObject instanceof String)) {
      return (String)localObject;
    }
    return "";
  }
  
  public static String getCommonConfigAppkey()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 7, null, null, new Object[0]);
    if ((localObject instanceof String)) {
      return (String)localObject;
    }
    return "";
  }
  
  public static boolean getConnectionState(Context paramContext)
  {
    return JCoreManager.getConnectionState(mApplicationContext);
  }
  
  public static String getDaemonAction()
  {
    return DAEMON_ACTION;
  }
  
  public static boolean getDebugMode()
  {
    return JCoreManager.getDebugMode();
  }
  
  public static String getDeviceId(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 8, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static String getHttpConfig(Context paramContext, String paramString)
  {
    return "";
  }
  
  public static int getJCoreSDKVersionInt()
  {
    int i = 0;
    Object localObject = JCoreManager.onEvent(null, null, 25, null, null, new Object[0]);
    if ((localObject instanceof Integer)) {
      i = ((Integer)localObject).intValue();
    }
    return i;
  }
  
  public static long getNextRid()
  {
    return e.b(mApplicationContext);
  }
  
  public static String getRegistrationID(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, null, 4, null, null, new Object[0]);
    if ((paramContext instanceof String)) {
      return (String)paramContext;
    }
    return "";
  }
  
  public static long getReportTime()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 19, null, null, new Object[0]);
    if ((localObject instanceof Long)) {
      return ((Long)localObject).longValue();
    }
    return System.currentTimeMillis() / 1000L;
  }
  
  public static boolean getRuningFlag()
  {
    try
    {
      boolean bool = Build.BRAND.equals("nubia");
      if (bool) {
        return true;
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static int getSid()
  {
    return 0;
  }
  
  public static long getUid()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 20, null, null, new Object[0]);
    if ((localObject instanceof Long)) {
      return ((Long)localObject).longValue();
    }
    return 0L;
  }
  
  public static boolean init(Context paramContext, boolean paramBoolean)
  {
    if (paramContext != null) {
      mApplicationContext = paramContext;
    }
    JCoreManager.init(paramContext);
    return true;
  }
  
  public static void initAction(String paramString, Class<? extends JAction> paramClass)
  {
    a.a().a(paramString, paramClass.getName());
  }
  
  public static void initActionExtra(String paramString, Class<? extends JActionExtra> paramClass)
  {
    a.a().b(paramString, paramClass.getName());
  }
  
  public static void initCrashHandler(Context paramContext)
  {
    JCoreManager.initCrashHandler(paramContext);
  }
  
  public static boolean isTcpConnected()
  {
    return JCoreManager.getConnectionState(mApplicationContext);
  }
  
  public static boolean isValidRegistered()
  {
    Object localObject = JCoreManager.onEvent(mApplicationContext, null, 21, null, null, new Object[0]);
    if ((localObject instanceof Boolean)) {
      return ((Boolean)localObject).booleanValue();
    }
    return false;
  }
  
  public static void onFragmentPause(Context paramContext, String paramString)
  {
    JCoreManager.onEvent(paramContext, SdkType.JPUSH.name(), 56, "f_pause", null, new Object[] { paramString });
  }
  
  public static void onFragmentResume(Context paramContext, String paramString)
  {
    JCoreManager.onEvent(paramContext, SdkType.JPUSH.name(), 56, "f_resume", null, new Object[] { paramString });
  }
  
  public static void onKillProcess(Context paramContext)
  {
    JCoreManager.onEvent(paramContext, SdkType.JPUSH.name(), 56, "kill", null, new Object[0]);
  }
  
  public static void onPause(Context paramContext)
  {
    JCoreManager.onEvent(paramContext, SdkType.JPUSH.name(), 56, "pause", null, new Object[0]);
  }
  
  public static void onResume(Context paramContext)
  {
    JCoreManager.onEvent(paramContext, SdkType.JPUSH.name(), 56, "resume", null, new Object[0]);
  }
  
  public static void processCtrlReport(int paramInt)
  {
    JCoreManager.onEvent(null, null, 24, null, null, new Object[] { Integer.valueOf(paramInt) });
  }
  
  public static void putSingleExecutor(String paramString)
  {
    JCoreManager.onEvent(null, null, 13, paramString, null, new Object[0]);
  }
  
  public static void register(Context paramContext)
  {
    d.b("JCoreInterface", "Action - init registerOnly:");
    if (paramContext != null) {
      mApplicationContext = paramContext;
    }
    JCoreManager.init(paramContext);
  }
  
  public static void report(Context paramContext, JSONObject paramJSONObject, boolean paramBoolean)
  {
    JCoreManager.onEvent(paramContext, "JSupport", 14, null, null, new Object[] { paramJSONObject });
  }
  
  public static boolean reportHttpData(Context paramContext, Object paramObject, String paramString)
  {
    JCoreManager.onEvent(paramContext, paramString, 14, null, null, new Object[] { paramObject });
    return true;
  }
  
  public static void requestPermission(Context paramContext)
  {
    JCoreManager.requestPermission(paramContext);
  }
  
  public static void restart(Context paramContext, String paramString, Bundle paramBundle, boolean paramBoolean)
  {
    JCoreManager.onEvent(paramContext, paramString, 1, null, null, new Object[0]);
  }
  
  private static void send(Context paramContext, String paramString, int paramInt1, byte[] paramArrayOfByte, int paramInt2, boolean paramBoolean)
  {
    if (paramArrayOfByte != null) {
      try
      {
        if (paramArrayOfByte.length > 24)
        {
          Object localObject = new byte[24];
          byte[] arrayOfByte = new byte[paramArrayOfByte.length - 24];
          System.arraycopy(paramArrayOfByte, 0, localObject, 0, 24);
          System.arraycopy(paramArrayOfByte, 24, arrayOfByte, 0, paramArrayOfByte.length - 24);
          paramArrayOfByte = new cn/jiguang/aq/c;
          paramArrayOfByte.<init>(true, (byte[])localObject);
          localObject = new android/os/Bundle;
          ((Bundle)localObject).<init>();
          ((Bundle)localObject).putInt("cmd", paramArrayOfByte.a());
          ((Bundle)localObject).putInt("ver", paramArrayOfByte.e());
          ((Bundle)localObject).putLong("rid", paramArrayOfByte.b().longValue());
          ((Bundle)localObject).putLong("timeout", paramInt2);
          ((Bundle)localObject).putByteArray("body", arrayOfByte);
          if (paramBoolean) {
            paramInt1 = 17;
          } else {
            paramInt1 = 50;
          }
          JCoreManager.onEvent(paramContext, paramString, paramInt1, null, (Bundle)localObject, new Object[0]);
        }
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("send failed:");
        paramString.append(paramContext.getMessage());
        d.e("JCoreInterface", paramString.toString());
      }
    }
  }
  
  public static void sendAction(Context paramContext, String paramString, Bundle paramBundle)
  {
    if (paramBundle != null) {
      try
      {
        JCoreManager.onEvent(paramContext, paramString, 3, paramBundle.getString("action"), paramBundle, new Object[0]);
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("sendAction failed:");
        paramString.append(paramContext);
        d.e("JCoreInterface", paramString.toString());
      }
    }
  }
  
  public static void sendData(Context paramContext, String paramString, int paramInt, byte[] paramArrayOfByte)
  {
    send(paramContext, paramString, paramInt, paramArrayOfByte, 0, false);
  }
  
  public static void sendRequestData(Context paramContext, String paramString, int paramInt, byte[] paramArrayOfByte)
  {
    send(paramContext, paramString, 0, paramArrayOfByte, paramInt, true);
  }
  
  public static void setAccountId(String paramString)
  {
    JCoreManager.onEvent(null, null, 22, paramString, null, new Object[0]);
  }
  
  public static void setAnalysisAction(JAnalyticsAction paramJAnalyticsAction)
  {
    if (paramJAnalyticsAction != null) {
      JCoreManager.setAnalysisAction(paramJAnalyticsAction);
    }
  }
  
  public static void setCanLaunchedStoppedService(boolean paramBoolean) {}
  
  public static void setDaemonAction(String paramString)
  {
    DAEMON_ACTION = paramString;
  }
  
  public static void setDebugMode(boolean paramBoolean)
  {
    JCoreManager.setDebugMode(paramBoolean);
  }
  
  public static void setImLBSEnable(Context paramContext, boolean paramBoolean)
  {
    d.b("JCoreInterface", "action - setImLBSEnable-control");
    JCoreManager.onEvent(paramContext, null, 27, null, null, new Object[] { Boolean.valueOf(paramBoolean) });
  }
  
  public static void setLocationReportDelay(Context paramContext, long paramLong)
  {
    JCoreManager.onEvent(paramContext, null, 28, null, null, new Object[] { Long.valueOf(paramLong) });
  }
  
  public static void setPowerSaveMode(Context paramContext, boolean paramBoolean) {}
  
  public static void setTestConn(boolean paramBoolean) {}
  
  public static void setTestConnIPPort(String paramString, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action - setTestConnIPPort ip:");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" port:");
    localStringBuilder.append(paramInt);
    d.b("JCoreInterface", localStringBuilder.toString());
  }
  
  public static Bundle si(Context paramContext, int paramInt, Bundle paramBundle)
  {
    if (paramContext != null) {
      mApplicationContext = paramContext.getApplicationContext();
    }
    return b.a(paramContext, paramInt, paramBundle);
  }
  
  public static void stop(Context paramContext, String paramString, Bundle paramBundle)
  {
    JCoreManager.onEvent(paramContext, paramString, 0, null, null, new Object[0]);
  }
  
  public static void stopCrashHandler(Context paramContext)
  {
    JCoreManager.stopCrashHandler(paramContext);
  }
  
  public static void testCountryCode(String paramString)
  {
    JCoreManager.onEvent(null, null, 23, paramString, null, new Object[0]);
  }
  
  public static void triggerSceneCheck(Context paramContext, int paramInt)
  {
    JCoreManager.onEvent(paramContext, null, 29, null, null, new Object[] { Integer.valueOf(paramInt) });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/api/JCoreInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */