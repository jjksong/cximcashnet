package cn.jiguang.ab;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import cn.jiguang.api.JDispatchAction;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static volatile a a;
  private static final Object b = new Object();
  private static HashMap<String, JDispatchAction> c = new HashMap();
  private static HashMap<String, String> d = new HashMap();
  
  public static a a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          a locala = new cn/jiguang/ab/a;
          locala.<init>();
          a = locala;
        }
      }
    }
    return a;
  }
  
  public static HashMap<String, String> b()
  {
    return d;
  }
  
  public void a(Context paramContext, String paramString, Object paramObject)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("onSended type:");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(",actionMap size:");
    ((StringBuilder)localObject).append(c.size());
    c.a("ActionManager", ((StringBuilder)localObject).toString());
    if (TextUtils.isEmpty(paramString))
    {
      paramString = c.entrySet().iterator();
      while (paramString.hasNext())
      {
        localObject = (Map.Entry)paramString.next();
        ((JDispatchAction)((Map.Entry)localObject).getValue()).handleMessage(paramContext, (String)((Map.Entry)localObject).getKey(), paramObject);
      }
    }
    localObject = (JDispatchAction)c.get(paramString);
    if (localObject != null) {
      ((JDispatchAction)localObject).handleMessage(paramContext, paramString, paramObject);
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("addAction type:");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(",action:");
    ((StringBuilder)localObject).append(paramString2);
    c.b("ActionManager", ((StringBuilder)localObject).toString());
    if (!TextUtils.isEmpty(paramString2))
    {
      if (!c.containsKey(paramString1)) {
        try
        {
          localObject = Class.forName(paramString2).newInstance();
          if ((localObject instanceof JDispatchAction))
          {
            d.put(paramString1, paramString2);
            c.put(paramString1, (JDispatchAction)localObject);
          }
          else
          {
            c.f("ActionManager", "this action is not a JDispatchAction,please check and extends JDispatchAction");
          }
        }
        catch (Throwable paramString1)
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("#unexcepted - instance ");
          ((StringBuilder)localObject).append(paramString2);
          ((StringBuilder)localObject).append(" class failed:");
          ((StringBuilder)localObject).append(paramString1);
          c.g("ActionManager", ((StringBuilder)localObject).toString());
        }
      }
      c.b("ActionManager", "has same type action");
    }
  }
  
  public boolean a(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null)
    {
      c.f("ActionManager", "wrapSdkVersionInfo failed ,container is null");
      return false;
    }
    try
    {
      paramJSONObject.put("core_sdk_ver", "2.0.0");
      Iterator localIterator = c.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
        paramJSONObject.put(localJDispatchAction.getReportVersionKey((String)localEntry.getKey()), localJDispatchAction.getSdkVersion((String)localEntry.getKey()));
      }
      return true;
    }
    catch (JSONException paramJSONObject)
    {
      paramJSONObject.printStackTrace();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */