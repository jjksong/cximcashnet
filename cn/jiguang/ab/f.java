package cn.jiguang.ab;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import cn.jiguang.as.g;
import cn.jiguang.e.b;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class f
{
  private static Boolean a;
  
  private static String a(Context paramContext)
  {
    if (paramContext == null) {
      return "-1";
    }
    if ((b(paramContext)) && (((Integer)b.a(paramContext, cn.jiguang.e.a.k())).intValue() != 1))
    {
      int i = ((Integer)b.a(paramContext, cn.jiguang.e.a.f())).intValue();
      if (i < 0)
      {
        c.b("LocalShareProcessHelper", "[getTypeJson]idc<0,need login to get it");
        return "-3";
      }
      long l1 = ((Long)b.a(paramContext, cn.jiguang.e.a.c())).longValue();
      if (l1 <= 0L)
      {
        c.b("LocalShareProcessHelper", "[getTypeJson]uid<=0,need login to get it");
        return "-2";
      }
      String str2 = (String)b.a(paramContext, cn.jiguang.e.a.i());
      long l2 = ((Long)b.a(paramContext, cn.jiguang.e.a.j())).longValue();
      String str1 = e.f(paramContext);
      JSONObject localJSONObject = new JSONObject();
      try
      {
        localJSONObject.put("u", l1);
        localJSONObject.put("p", b.a(paramContext, cn.jiguang.e.a.e()));
        localJSONObject.put("ud", str2);
        localJSONObject.put("ak", str1);
        localJSONObject.put("idc", i);
        localJSONObject.put("pn", paramContext.getPackageName());
        localJSONObject.put("sv", 200);
        localJSONObject.put("uct", l2);
        paramContext = g.a(localJSONObject.toString());
        return paramContext;
      }
      catch (JSONException paramContext)
      {
        c.b("LocalShareProcessHelper", "[getTypeJson] to json error");
        return "2.0.0";
      }
    }
    c.b("LocalShareProcessHelper", "[getTypeJson]share process is close by action");
    return "-4";
  }
  
  public static String a(Context paramContext, Uri paramUri)
  {
    if (paramUri == null) {
      return "2.0.0";
    }
    try
    {
      paramUri = paramUri.getQueryParameter("kpgt");
      if (TextUtils.isEmpty(paramUri)) {
        return "2.0.0";
      }
      Object localObject = g.b(paramUri);
      if (TextUtils.isEmpty((CharSequence)localObject)) {
        return "-6";
      }
      paramUri = new org/json/JSONObject;
      paramUri.<init>((String)localObject);
      localObject = paramUri.optString("kta");
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("action:");
      localStringBuilder.append((String)localObject);
      c.b("LocalShareProcessHelper", localStringBuilder.toString());
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        if (((String)localObject).equals("asai")) {
          return a(paramContext);
        }
        if (((String)localObject).equals("asm"))
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("recv msg:");
          ((StringBuilder)localObject).append(paramUri.toString());
          c.b("LocalShareProcessHelper", ((StringBuilder)localObject).toString());
          if ((b(paramContext)) && (((Integer)b.a(paramContext, cn.jiguang.e.a.k())).intValue() != 1))
          {
            localObject = new android/os/Bundle;
            ((Bundle)localObject).<init>();
            ((Bundle)localObject).putString("data", paramUri.toString());
            e.a(paramContext, cn.jiguang.a.a.d, "asm", (Bundle)localObject);
            return "0";
          }
          c.b("LocalShareProcessHelper", "share process is closed!");
          return "-4";
        }
        if (((String)localObject).equals("asmr"))
        {
          localObject = new android/os/Bundle;
          ((Bundle)localObject).<init>();
          ((Bundle)localObject).putString("data", paramUri.toString());
          e.a(paramContext, cn.jiguang.a.a.d, "asmr", (Bundle)localObject);
        }
      }
    }
    catch (Throwable paramContext)
    {
      paramUri = new StringBuilder();
      paramUri.append("parseUriFromProvider failed:");
      paramUri.append(paramContext.getMessage());
      c.f("LocalShareProcessHelper", paramUri.toString());
    }
    return "2.0.0";
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null) {}
    try
    {
      if ("asm".equals(paramIntent.getAction()))
      {
        paramIntent = paramIntent.getExtras();
        e.a(paramContext, cn.jiguang.a.a.d, "asm", paramIntent);
      }
      else
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("shareActionRun intent error:");
        if (paramIntent == null) {
          paramContext = "null";
        } else {
          paramContext = paramIntent.getAction();
        }
        localStringBuilder.append(paramContext);
        c.b("LocalShareProcessHelper", localStringBuilder.toString());
      }
    }
    catch (Throwable paramIntent)
    {
      paramContext = new StringBuilder();
      paramContext.append("shareActionRun error:");
      paramContext.append(paramIntent.getMessage());
      c.f("LocalShareProcessHelper", paramContext.toString());
    }
  }
  
  private static boolean b(Context paramContext)
  {
    Object localObject = a;
    if (localObject != null) {
      return ((Boolean)localObject).booleanValue();
    }
    if (paramContext == null)
    {
      c.f("LocalShareProcessHelper", "context is null");
      return true;
    }
    try
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).setPackage(paramContext.getPackageName());
      ((Intent)localObject).setAction("cn.jiguang.android.share.close");
      paramContext = paramContext.getPackageManager().queryIntentServices((Intent)localObject, 0);
      if ((paramContext != null) && (!paramContext.isEmpty())) {}
      for (paramContext = Boolean.valueOf(false);; paramContext = Boolean.valueOf(true))
      {
        a = paramContext;
        break;
      }
      return a.booleanValue();
    }
    catch (Throwable paramContext)
    {
      c.a("LocalShareProcessHelper", "Get isShareProcessModeOpen error#", paramContext);
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */