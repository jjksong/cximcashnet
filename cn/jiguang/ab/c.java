package cn.jiguang.ab;

import android.content.Context;

public class c
{
  private static volatile c h;
  private static final Object i = new Object();
  private boolean a = false;
  private boolean b = false;
  private boolean c = false;
  private boolean d = false;
  private boolean e = false;
  private boolean f = false;
  private boolean g = false;
  
  public static c a()
  {
    if (h == null) {
      synchronized (i)
      {
        if (h == null)
        {
          c localc = new cn/jiguang/ab/c;
          localc.<init>();
          h = localc;
        }
      }
    }
    return h;
  }
  
  private boolean d()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jpush.android.api.JPushInterface");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("isPluginJpushSDK:");
      localStringBuilder.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
      bool = false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("isPluginJpushSDK:");
    localStringBuilder.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
    return bool;
  }
  
  private boolean e()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jpush.im.android.api.JMessageClient");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append("isPluginJMessageSDK:");
      localStringBuilder2.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder2.toString());
      bool = false;
    }
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append("isPluginJMessageSDK:");
    localStringBuilder1.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder1.toString());
    return bool;
  }
  
  private boolean f()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jiguang.analytics.android.api.JAnalyticsInterface");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append("isPluginJanalyticsSDK:");
      localStringBuilder2.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder2.toString());
      bool = false;
    }
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append("isPluginJanalyticsSDK:");
    localStringBuilder1.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder1.toString());
    return bool;
  }
  
  private boolean g()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jiguang.share.android.api.JShareInterface");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("isPluginJshareSDK:");
      localStringBuilder.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
      bool = false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("isPluginJshareSDK:");
    localStringBuilder.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
    return bool;
  }
  
  private boolean h()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jiguang.adsdk.api.JSSPInterface");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("isPluginJSspSDK:");
      localStringBuilder.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
      bool = false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("isPluginJSspSDK:");
    localStringBuilder.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
    return bool;
  }
  
  private boolean i()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jiguang.g.a");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append("isPluginJCommonSDK:");
      localStringBuilder2.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder2.toString());
      bool = false;
    }
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append("isPluginJCommonSDK:");
    localStringBuilder1.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder1.toString());
    return bool;
  }
  
  private boolean j()
  {
    boolean bool;
    try
    {
      Class.forName("cn.jiguang.verifysdk.api.JVerificationInterface");
      bool = true;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("isPluginJVerificationSDK:");
      localStringBuilder.append(localClassNotFoundException.getMessage());
      cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
      bool = false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("isPluginJVerificationSDK:");
    localStringBuilder.append(bool);
    cn.jiguang.ac.c.a("JClientsHelper", localStringBuilder.toString());
    return bool;
  }
  
  public boolean a(Context paramContext)
  {
    boolean bool;
    if ((!this.b) && (!this.a)) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public boolean b()
  {
    return this.b;
  }
  
  public boolean c()
  {
    return this.a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */