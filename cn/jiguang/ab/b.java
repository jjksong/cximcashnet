package cn.jiguang.ab;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import cn.jiguang.as.h;
import cn.jiguang.e.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class b
{
  public static String a = ".permission.JPUSH_MESSAGE";
  private static String b = "";
  private static String c;
  private static Pair<String, Integer> d;
  private static final ArrayList<String> e = new ArrayList();
  private static final ArrayList<String> f = new ArrayList();
  private static final ArrayList<String> g;
  
  static
  {
    e.add("android.permission.INTERNET");
    e.add("android.permission.ACCESS_NETWORK_STATE");
    f.add("android.permission.WAKE_LOCK");
    f.add("android.permission.VIBRATE");
    f.add("android.permission.CHANGE_WIFI_STATE");
    g = new ArrayList();
    g.add("android.permission.ACCESS_FINE_LOCATION");
    g.add("android.permission.ACCESS_COARSE_LOCATION");
    g.add("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS");
    g.add("android.permission.ACCESS_WIFI_STATE");
  }
  
  public static String a(Context paramContext)
  {
    if ((!TextUtils.isEmpty(b)) || (paramContext != null)) {}
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      if ((paramContext == null) || (paramContext.metaData == null)) {
        break label82;
      }
      b = a(paramContext.metaData, "JPUSH_APPKEY");
      if (TextUtils.isEmpty(b)) {
        break label82;
      }
      b = b.toLowerCase(Locale.getDefault());
    }
    catch (Throwable paramContext)
    {
      label82:
      for (;;) {}
    }
    cn.jiguang.ac.c.b("CheckManifestHelper", "[getAppKey] context is null");
    return b;
  }
  
  public static String a(Bundle paramBundle, String paramString)
  {
    if (paramBundle == null) {
      return null;
    }
    paramBundle = paramBundle.get(paramString);
    if (paramBundle == null) {
      return null;
    }
    return paramBundle.toString();
  }
  
  public static void a(Context paramContext, String paramString)
  {
    c = paramString;
    cn.jiguang.e.b.a(paramContext, new a[] { a.l().a(paramString) });
    b(paramContext);
  }
  
  public static String b(Context paramContext)
  {
    if ((c == null) && (paramContext != null)) {}
    try
    {
      c = e.c(paramContext);
      if (c != null)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("get option channel - ");
        paramContext.append(c);
      }
      for (;;)
      {
        cn.jiguang.ac.c.c("CheckManifestHelper", paramContext.toString());
        break;
        paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
        if ((paramContext != null) && (paramContext.metaData != null))
        {
          c = a(paramContext.metaData, "JPUSH_CHANNEL");
          if (!TextUtils.isEmpty(c)) {
            c = h.b(c);
          }
        }
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("manifest:channel - ");
        paramContext.append(c);
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return c;
  }
  
  public static Pair<String, Integer> c(Context paramContext)
  {
    if (d == null) {
      try
      {
        PackageInfo localPackageInfo = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
        Object localObject = localPackageInfo.versionName;
        paramContext = (Context)localObject;
        if (((String)localObject).length() > 30) {
          paramContext = ((String)localObject).substring(0, 30);
        }
        localObject = new android/util/Pair;
        ((Pair)localObject).<init>(paramContext, Integer.valueOf(localPackageInfo.versionCode));
        d = (Pair)localObject;
      }
      catch (Throwable paramContext)
      {
        cn.jiguang.ac.c.c("CheckManifestHelper", "NO versionCode or versionName defined in manifest.");
      }
    }
    return d;
  }
  
  public static boolean d(Context paramContext)
  {
    Object localObject1 = a(paramContext);
    if (TextUtils.isEmpty((CharSequence)localObject1))
    {
      cn.jiguang.ac.c.i("CheckManifestHelper", "errorcode:10001,metadata: JCore appKey - not defined in manifest");
      cn.jiguang.as.e.e(paramContext, " 未在manifest中配置AppKey");
      return false;
    }
    if (((String)localObject1).length() != 24)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("errorcode:1008,Invalid appKey : ");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(", Please get your Appkey from JIGUANG web console!");
      cn.jiguang.ac.c.i("CheckManifestHelper", ((StringBuilder)localObject2).toString());
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(" AppKey:");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" 是无效的AppKey,请确认与JIGUANG web端的AppKey一致");
      cn.jiguang.as.e.e(paramContext, ((StringBuilder)localObject2).toString());
      return false;
    }
    b(paramContext);
    if ((c.a().c()) || (c.a().b()))
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(paramContext.getPackageName());
      ((StringBuilder)localObject1).append(a);
      localObject1 = ((StringBuilder)localObject1).toString();
      if (!cn.jiguang.as.e.b(paramContext, (String)localObject1))
      {
        paramContext = new StringBuilder();
        paramContext.append("The permission should be defined - ");
        paramContext.append((String)localObject1);
        cn.jiguang.ac.c.i("CheckManifestHelper", paramContext.toString());
        return false;
      }
      e.add(localObject1);
    }
    Object localObject2 = e.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (String)((Iterator)localObject2).next();
      if (!cn.jiguang.as.e.a(paramContext, (String)localObject1))
      {
        paramContext = new StringBuilder();
        paramContext.append("The permissoin is required - ");
        paramContext.append((String)localObject1);
        cn.jiguang.ac.c.i("CheckManifestHelper", paramContext.toString());
        return false;
      }
    }
    if (Build.VERSION.SDK_INT < 23)
    {
      if (!cn.jiguang.as.e.a(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE"))
      {
        cn.jiguang.ac.c.i("CheckManifestHelper", "The permissoin is required - android.permission.WRITE_EXTERNAL_STORAGE");
        return false;
      }
      if (!cn.jiguang.as.e.a(paramContext, "android.permission.WRITE_SETTINGS"))
      {
        cn.jiguang.ac.c.i("CheckManifestHelper", "The permissoin is required - android.permission.WRITE_SETTINGS");
        return false;
      }
    }
    localObject2 = f.iterator();
    StringBuilder localStringBuilder;
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (String)((Iterator)localObject2).next();
      if (!cn.jiguang.as.e.a(paramContext, (String)localObject1))
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("We recommend you add the permission - ");
        localStringBuilder.append((String)localObject1);
        cn.jiguang.ac.c.f("CheckManifestHelper", localStringBuilder.toString());
      }
    }
    localObject1 = g.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (String)((Iterator)localObject1).next();
      if (!cn.jiguang.as.e.a(paramContext, (String)localObject2))
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("We recommend you add the permission - ");
        localStringBuilder.append((String)localObject2);
        localStringBuilder.append(", otherwise you can not locate the devices.");
        cn.jiguang.ac.c.f("CheckManifestHelper", localStringBuilder.toString());
      }
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */