package cn.jiguang.ab;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ac.c;
import cn.jiguang.api.JCoreAction;
import cn.jiguang.as.e;
import cn.jiguang.sdk.impl.JCoreActionImpl;
import cn.jpush.android.service.DataShare;
import cn.jpush.android.service.JCommonService;
import java.util.List;

public class d
{
  public static JCoreAction a = new JCoreActionImpl();
  private static final Object b = new Object();
  private static volatile d c;
  private static String d;
  private static String e;
  private static String f;
  
  public static d a()
  {
    if (c == null) {
      synchronized (b)
      {
        if (c == null)
        {
          d locald = new cn/jiguang/ab/d;
          locald.<init>();
          c = locald;
        }
      }
    }
    return c;
  }
  
  public static String a(Context paramContext)
  {
    String str = e;
    if (str != null) {
      return str;
    }
    str = b(paramContext);
    if (TextUtils.isEmpty(str)) {
      e = "";
    }
    for (;;)
    {
      return e;
      e = cn.jiguang.as.a.a(paramContext, str);
      paramContext = new StringBuilder();
      paramContext.append("user serviceProcess is:");
      paramContext.append(e);
      c.b("JCommonServiceHelper", paramContext.toString());
    }
  }
  
  public static String b(Context paramContext)
  {
    try
    {
      if (d != null) {
        return d;
      }
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).setAction("cn.jiguang.user.service.action");
      ((Intent)localObject).setPackage(paramContext.getPackageName());
      localObject = cn.jiguang.as.a.a(paramContext, (Intent)localObject, "");
      if ((localObject != null) && (((List)localObject).size() >= 1))
      {
        Class localClass = Class.forName((String)((List)localObject).get(0));
        if ((localClass != null) && (JCommonService.class.isAssignableFrom(localClass)))
        {
          d = (String)((List)localObject).get(0);
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("found userServiceClass :");
          ((StringBuilder)localObject).append(d);
          ((StringBuilder)localObject).append(" by getCommonServiceNames");
          c.e("JCommonServiceHelper", ((StringBuilder)localObject).toString());
        }
      }
    }
    catch (Throwable localThrowable)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("getUserServiceClass failed:");
      ((StringBuilder)localObject).append(localThrowable.getMessage());
      c.b("JCommonServiceHelper", ((StringBuilder)localObject).toString());
    }
    if (TextUtils.isEmpty(d))
    {
      paramContext = e.a(paramContext, paramContext.getPackageName(), JCommonService.class);
      if (paramContext != null)
      {
        d = paramContext.name;
        paramContext = new StringBuilder();
        paramContext.append("found userServiceClass :");
        paramContext.append(d);
        paramContext.append(" by getComponentInfo");
        c.e("JCommonServiceHelper", paramContext.toString());
      }
    }
    if (TextUtils.isEmpty(d)) {
      d = "";
    }
    return d;
  }
  
  private static String c(Context paramContext)
  {
    Object localObject = f;
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramContext.getPackageName());
    ((StringBuilder)localObject).append(".DataProvider");
    localObject = ((StringBuilder)localObject).toString();
    localObject = cn.jiguang.as.a.a(paramContext, paramContext.getPackageName(), (String)localObject);
    if (localObject != null) {
      localObject = ((ProviderInfo)localObject).processName;
    } else {
      localObject = null;
    }
    String str = a(paramContext);
    if ((!TextUtils.isEmpty((CharSequence)localObject)) && (((String)localObject).equals(str)))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("content://");
      ((StringBuilder)localObject).append(paramContext.getPackageName());
      ((StringBuilder)localObject).append(".DataProvider");
      ((StringBuilder)localObject).append("/");
      f = ((StringBuilder)localObject).toString();
    }
    else
    {
      f = "";
      c.f("JCommonServiceHelper", "provider process is NOT service process");
    }
    return f;
  }
  
  private boolean d(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 11)
      {
        Object localObject = c(paramContext);
        if (!TextUtils.isEmpty((CharSequence)localObject))
        {
          localObject = Uri.parse((String)localObject);
          if (paramContext.getContentResolver().call((Uri)localObject, paramString, "", paramBundle) != null)
          {
            paramContext = new java/lang/StringBuilder;
            paramContext.<init>();
            paramContext.append("doActionByProvider, action=");
            paramContext.append(paramString);
            c.b("JCommonServiceHelper", paramContext.toString());
            return true;
          }
          c.f("JCommonServiceHelper", "provider call, can't get result");
        }
        else
        {
          c.b("JCommonServiceHelper", "uri is null, check dataProvider config and process");
        }
      }
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("provider call:");
      paramString.append(paramContext.getMessage());
      c.g("JCommonServiceHelper", paramString.toString());
    }
    return false;
  }
  
  private boolean e(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      String str = b(paramContext);
      if (!TextUtils.isEmpty(str))
      {
        Intent localIntent = new android/content/Intent;
        localIntent.<init>();
        localIntent.setClass(paramContext, Class.forName(str));
        localIntent.setAction(paramString);
        if (paramBundle != null) {
          localIntent.putExtras(paramBundle);
        }
        if (paramContext.startService(localIntent) != null)
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append("doActionByService, action=");
          paramContext.append(paramString);
          c.b("JCommonServiceHelper", paramContext.toString());
          return true;
        }
      }
      for (paramContext = "startService, can't find component";; paramContext = "can't get serviceClassName, check service config")
      {
        c.f("JCommonServiceHelper", paramContext);
        break;
      }
      return false;
    }
    catch (Throwable paramString)
    {
      paramContext = new StringBuilder();
      paramContext.append("throwable ,cant start service");
      paramContext.append(paramString.getMessage());
      paramContext.append(", will use aidl to do action");
      c.g("JCommonServiceHelper", paramContext.toString());
    }
  }
  
  private boolean f(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      if (DataShare.alreadyBound())
      {
        DataShare.getInstance().onAction(paramString, paramBundle);
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("doActionByAidl, action=");
        paramContext.append(paramString);
        c.b("JCommonServiceHelper", paramContext.toString());
        return true;
      }
      c.f("JCommonServiceHelper", "aidl is null, cant do action");
      return false;
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("throwable , remote call failed, error:");
      paramString.append(paramContext);
      c.g("JCommonServiceHelper", paramString.toString());
    }
    return false;
  }
  
  public void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("onAction action:");
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append(" bundle:");
      if (paramBundle == null) {
        str = "null";
      } else {
        str = paramBundle.toString();
      }
      ((StringBuilder)localObject).append(str);
      c.b("JCommonServiceHelper", ((StringBuilder)localObject).toString());
      if (TextUtils.isEmpty(b(paramContext)))
      {
        cn.jiguang.a.a.b(paramContext, paramString, paramBundle);
        return;
      }
      String str = cn.jiguang.as.a.a(paramContext);
      localObject = a(paramContext);
      if ((!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty((CharSequence)localObject)) && (str.equals(localObject)))
      {
        cn.jiguang.a.a.b(paramContext, paramString, paramBundle);
        return;
      }
      if ((!f(paramContext, paramString, paramBundle)) && (!d(paramContext, paramString, paramBundle))) {
        e(paramContext, paramString, paramBundle);
      }
    }
    catch (Throwable paramContext)
    {
      c.e("JCommonServiceHelper", "onAction failed", paramContext);
    }
  }
  
  public Bundle b(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 11)
      {
        Object localObject = c(paramContext);
        if (!TextUtils.isEmpty((CharSequence)localObject))
        {
          localObject = Uri.parse((String)localObject);
          return paramContext.getContentResolver().call((Uri)localObject, paramString, "call", paramBundle);
        }
        c.b("JCommonServiceHelper", "uri is null, check dataProvider config and process");
      }
    }
    catch (Throwable paramString)
    {
      paramContext = new StringBuilder();
      paramContext.append("provider call:");
      paramContext.append(paramString.getMessage());
      c.g("JCommonServiceHelper", paramContext.toString());
    }
    return null;
  }
  
  public void c(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("callAction action:");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" bundle:");
      if (paramBundle == null) {
        localObject = "null";
      } else {
        localObject = paramBundle.toString();
      }
      localStringBuilder.append((String)localObject);
      c.d("JCommonServiceHelper", localStringBuilder.toString());
      Object localObject = cn.jiguang.a.a.a(paramContext);
      paramContext = new cn/jiguang/ab/d$a;
      paramContext.<init>(this, (Context)localObject, paramString, paramBundle);
      cn.jiguang.ar.a.a("ACTION", paramContext);
    }
    catch (Throwable paramContext)
    {
      c.e("JCommonServiceHelper", "callAction failed", paramContext);
    }
  }
  
  class a
    implements Runnable
  {
    private String b;
    private Bundle c;
    private Context d;
    
    public a(Context paramContext, String paramString, Bundle paramBundle)
    {
      this.b = paramString;
      this.c = paramBundle;
      this.d = paramContext;
    }
    
    public void run()
    {
      try
      {
        d.a.handleAction(this.d, this.b, this.c);
      }
      catch (Throwable localThrowable)
      {
        localThrowable.printStackTrace();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */