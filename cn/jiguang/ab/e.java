package cn.jiguang.ab;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.jiguang.ad.d;
import cn.jiguang.ad.f;
import cn.jiguang.api.JCoreManager;
import cn.jiguang.api.ReportCallBack;
import cn.jiguang.as.h;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class e
{
  private static Bundle a(Context paramContext, int paramInt, Bundle paramBundle)
  {
    try
    {
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      if (paramInt != 4096)
      {
        if (paramInt != 4098)
        {
          if (paramInt != 36864) {
            return null;
          }
          a(paramBundle);
          return null;
        }
        JCoreManager.init(paramContext);
        return localBundle;
      }
      if (paramBundle != null) {
        c(paramContext, b.a(paramBundle, "arg1"));
      }
      return localBundle;
    }
    catch (Throwable paramBundle)
    {
      paramContext = new StringBuilder();
      paramContext.append("si e:");
      paramContext.append(paramBundle);
      cn.jiguang.ac.c.h("JCoreHelper", paramContext.toString());
    }
    return null;
  }
  
  public static Object a(Context paramContext, String paramString1, int paramInt, String paramString2, Bundle paramBundle, Object... paramVarArgs)
  {
    try
    {
      localContext = cn.jiguang.a.a.a(paramContext);
      i = 1;
      switch (paramInt)
      {
      }
    }
    catch (Throwable paramString1)
    {
      Context localContext;
      int i;
      paramContext = new StringBuilder();
      paramContext.append("onEvent:");
      paramContext.append(paramString1.getMessage());
      cn.jiguang.ac.c.b("JCoreHelper", paramContext.toString());
    }
    paramContext = "tcp_a20";
    for (;;)
    {
      a(localContext, paramString1, paramBundle, paramContext);
      break;
      return Boolean.valueOf(a(localContext));
      if ((paramVarArgs != null) && (paramVarArgs.length > 0) && ((paramVarArgs[0] instanceof Integer)))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("SET_SHARE_PROCESS_STATE state:");
        paramContext.append(paramVarArgs[0]);
        cn.jiguang.ac.c.b("JCoreHelper", paramContext.toString());
        paramContext = new cn.jiguang.e.a[1];
        paramContext[0] = cn.jiguang.e.a.k().a(Integer.valueOf(((Integer)paramVarArgs[0]).intValue()));
        for (;;)
        {
          cn.jiguang.e.b.a(localContext, paramContext);
          break label2276;
          if ("resume".equals(paramString2))
          {
            if (!cn.jiguang.a.a.g) {
              break label2276;
            }
            cn.jiguang.c.b.a().b(localContext);
            break label2276;
          }
          if ("pause".equals(paramString2))
          {
            if (!cn.jiguang.a.a.g) {
              break label2276;
            }
            cn.jiguang.c.b.a().c(localContext);
            break label2276;
          }
          if ("kill".equals(paramString2))
          {
            cn.jiguang.c.b.a().a(localContext);
            break label2276;
          }
          if ("enable".equals(paramString2))
          {
            if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Boolean))) {
              break label2276;
            }
            cn.jiguang.c.b.a().a(((Boolean)paramVarArgs[0]).booleanValue());
            break label2276;
          }
          if ("s_timeout".equals(paramString2))
          {
            if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Long))) {
              break label2276;
            }
            cn.jiguang.c.b.a().a(((Long)paramVarArgs[0]).longValue());
            break label2276;
          }
          if ("f_resume".equals(paramString2))
          {
            if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof String))) {
              break label2276;
            }
            cn.jiguang.c.b.a().a(localContext, (String)paramVarArgs[0]);
            break label2276;
          }
          if ((!"f_pause".equals(paramString2)) || (paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof String))) {
            break label2276;
          }
          cn.jiguang.c.b.a().b(localContext, (String)paramVarArgs[0]);
          break label2276;
          b(paramBundle);
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 2)) {
            break label2276;
          }
          paramContext = (String)paramVarArgs[0];
          i = ((Integer)paramVarArgs[1]).intValue();
          paramInt = ((Integer)paramVarArgs[2]).intValue();
          paramString1 = new android/os/Bundle;
          paramString1.<init>();
          paramString1.putString("name", paramContext);
          paramString1.putInt("custom", i);
          paramString1.putInt("dynamic", paramInt);
          cn.jiguang.ah.e.a(localContext, "set_sdktype_info", paramString1);
          break label2276;
          return Boolean.valueOf(c.a().a(localContext));
          a(paramBundle);
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length != 1) || (!(paramVarArgs[0] instanceof Integer))) {
            break label2276;
          }
          paramContext = new cn.jiguang.e.a[1];
          paramContext[0] = cn.jiguang.e.a.f().a(Integer.valueOf(((Integer)paramVarArgs[0]).intValue()));
          continue;
          paramContext = "tcp_a4";
          break;
          return Boolean.valueOf(cn.jiguang.a.a.f);
          d.a(localContext);
          break label2276;
          return cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.k());
          return cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.f());
          paramContext = new java/util/HashMap;
          paramContext.<init>();
          paramString1 = (String)cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.i());
          long l = ((Long)cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.j())).longValue();
          paramInt = ((Integer)cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.k())).intValue();
          paramContext.put("uuid", paramString1);
          paramContext.put("ct", Long.valueOf(l));
          paramContext.put("state", Integer.valueOf(paramInt));
          return paramContext;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 1) || (!(paramVarArgs[0] instanceof String)) || (!(paramVarArgs[1] instanceof Long))) {
            break label2276;
          }
          paramContext = new cn.jiguang.e.a[2];
          paramContext[0] = cn.jiguang.e.a.i().a((String)paramVarArgs[0]);
          paramContext[1] = cn.jiguang.e.a.j().a(Long.valueOf(((Long)paramVarArgs[1]).longValue()));
          continue;
          return a.b();
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof String))) {
            break label2276;
          }
          return f.a(localContext, (String)paramVarArgs[0]);
          if ((paramVarArgs == null) || (paramVarArgs.length <= 1) || (!(paramVarArgs[0] instanceof String)) || (!(paramVarArgs[1] instanceof JSONObject))) {
            break label2276;
          }
          return Boolean.valueOf(f.a(localContext, (String)paramVarArgs[0], (JSONObject)paramVarArgs[1]));
          if ((paramVarArgs == null) || (paramVarArgs.length <= 1) || (!(paramVarArgs[0] instanceof Boolean)) || (!(paramVarArgs[1] instanceof Long))) {
            break label2276;
          }
          cn.jiguang.a.a.a(localContext, ((Boolean)paramVarArgs[0]).booleanValue(), ((Long)paramVarArgs[1]).longValue());
          break label2276;
          if (paramVarArgs != null) {
            paramContext = paramVarArgs[0];
          } else {
            paramContext = null;
          }
          f.a(localContext, paramContext);
          break label2276;
          cn.jiguang.e.b.a(localContext, "cn.jiguang.sdk.user.profile");
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Long))) {
            break label2276;
          }
          cn.jiguang.e.c.b(localContext, ((Long)paramVarArgs[0]).longValue());
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof String))) {
            break label2276;
          }
          cn.jiguang.as.c.a(localContext, (String)paramVarArgs[0]);
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 2) || (!(paramVarArgs[0] instanceof Long)) || (!(paramVarArgs[1] instanceof String)) || (!(paramVarArgs[2] instanceof String))) {
            break label2276;
          }
          paramContext = new cn.jiguang.e.a[3];
          paramContext[0] = cn.jiguang.e.a.c().a(Long.valueOf(((Long)paramVarArgs[0]).longValue()));
          paramContext[1] = cn.jiguang.e.a.e().a((String)paramVarArgs[1]);
          paramContext[2] = cn.jiguang.e.a.d().a((String)paramVarArgs[2]);
          continue;
          return cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.e());
          return Integer.valueOf(cn.jiguang.as.c.a);
          paramContext = new android/os/Bundle;
          paramContext.<init>();
          paramContext.putBoolean("google", false);
          paramContext.putBoolean("internal_use", false);
          paramContext.putString("test_country", cn.jiguang.a.a.h);
          return paramContext;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Intent))) {
            break label2276;
          }
          a.a().a(localContext, paramString1, paramVarArgs[0]);
          break label2276;
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Integer))) {
            break label2276;
          }
          return a(localContext, ((Integer)paramVarArgs[0]).intValue(), paramBundle);
          if (paramVarArgs == null) {
            break label2276;
          }
          if (paramVarArgs.length > 0) {
            paramInt = i;
          } else {
            paramInt = 0;
          }
          if (!(paramVarArgs[0] instanceof Integer & paramInt)) {
            break label2276;
          }
          paramContext = new android/os/Bundle;
          paramContext.<init>();
          paramContext.putInt("scence", ((Integer)paramVarArgs[0]).intValue());
          paramString1 = cn.jiguang.a.a.d;
          paramString2 = "notification_state";
          for (;;)
          {
            a(localContext, paramString1, paramString2, paramContext);
            break;
            if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Long))) {
              break;
            }
            paramContext = new android/os/Bundle;
            paramContext.<init>();
            paramContext.putLong("forenry", ((Long)paramVarArgs[0]).longValue());
            paramString1 = cn.jiguang.a.a.d;
            paramString2 = "lbsforenry";
            continue;
            if ((paramVarArgs != null) && (paramVarArgs.length > 1) && ((paramVarArgs[0] instanceof JSONObject)) && ((paramVarArgs[1] == null) || ((paramVarArgs[1] instanceof String)))) {
              return f.a(localContext, (JSONObject)paramVarArgs[0], (String)paramVarArgs[1]);
            }
            if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Boolean))) {
              break;
            }
            paramContext = new android/os/Bundle;
            paramContext.<init>();
            paramContext.putBoolean("enable", ((Boolean)paramVarArgs[0]).booleanValue());
            paramString1 = cn.jiguang.a.a.d;
            paramString2 = "lbsenable";
          }
          return Integer.valueOf(200);
          if ((paramVarArgs == null) || (paramVarArgs.length <= 0) || (!(paramVarArgs[0] instanceof Integer))) {
            break label2276;
          }
          paramContext = new android/os/Bundle;
          paramContext.<init>();
          paramContext.putInt("cmd", ((Integer)paramVarArgs[0]).intValue());
          a(localContext, cn.jiguang.a.a.d, "old_cmd", null);
          break label2276;
          paramContext = new cn.jiguang.e.a[1];
          paramContext[0] = cn.jiguang.e.a.m().a(paramString2);
        }
        return Boolean.valueOf(cn.jiguang.e.c.a(localContext));
        return cn.jiguang.e.b.a(localContext, cn.jiguang.e.a.c());
        if ((paramVarArgs != null) && (paramVarArgs.length > 0) && ((paramVarArgs[0] instanceof Long))) {
          return Long.valueOf(cn.jiguang.e.c.a(localContext, ((Long)paramVarArgs[0]).longValue()));
        }
        return Long.valueOf(cn.jiguang.e.c.b(localContext));
        if ((paramVarArgs != null) && (paramVarArgs.length > 2))
        {
          if ((paramVarArgs.length > 3) && ((paramVarArgs[3] instanceof Throwable))) {
            paramContext = (Throwable)paramVarArgs[3];
          } else {
            paramContext = null;
          }
          a(paramString1, paramString2, ((Boolean)paramVarArgs[1]).booleanValue(), ((Integer)paramVarArgs[0]).intValue(), (String)paramVarArgs[2], paramContext);
          break;
          paramContext = "tcp_a5";
          continue;
          paramContext = "tcp_a3";
          continue;
          if ((paramVarArgs != null) && (paramVarArgs.length > 1) && ((paramVarArgs[1] instanceof ReportCallBack)))
          {
            a(localContext, paramString1, paramVarArgs[0], (ReportCallBack)paramVarArgs[1]);
            break;
            if ((paramVarArgs != null) && (paramVarArgs.length > 0))
            {
              a(localContext, paramString1, paramVarArgs[0]);
              break;
              cn.jiguang.ar.a.a(paramString2);
              break;
              if ((paramVarArgs != null) && (paramVarArgs.length > 0) && ((paramVarArgs[0] instanceof Runnable)))
              {
                cn.jiguang.ar.a.a("ASYNC", (Runnable)paramVarArgs[0]);
                break;
                if ((paramVarArgs != null) && (paramVarArgs.length > 0) && ((paramVarArgs[0] instanceof Runnable)))
                {
                  cn.jiguang.ar.a.a(paramString2, (Runnable)paramVarArgs[0]);
                  break;
                  a(localContext, paramString2, paramBundle);
                  break;
                  c(localContext, paramString2);
                  break;
                  return e(localContext);
                  return f(localContext);
                  return g(localContext);
                  return d(localContext);
                  return b(localContext);
                  a(localContext, paramString1, paramString2, paramBundle);
                  break;
                  b(localContext, paramString2, paramBundle);
                  break;
                  b(localContext, paramString1);
                  break;
                  a(localContext, paramString1);
                }
              }
            }
          }
        }
      }
    }
    label2276:
    return null;
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    cn.jiguang.ah.e.a(paramContext, "get_receiver", paramIntent);
    Object localObject1 = paramIntent.getAction();
    if (localObject1 == null)
    {
      cn.jiguang.ac.c.f("JCoreHelper", "onReceive empty action");
      return;
    }
    Object localObject3;
    boolean bool;
    if (((String)localObject1).equals("android.intent.action.USER_PRESENT"))
    {
      cn.jiguang.ac.c.d("JCoreHelper", "onReceiveandroid.intent.action.USER_PRESENT");
      cn.jiguang.a.a.a(paramContext, true, 0L);
    }
    else if (((String)localObject1).equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE"))
    {
      localObject3 = (NetworkInfo)paramIntent.getParcelableExtra("networkInfo");
      if (localObject3 == null)
      {
        cn.jiguang.ac.c.f("JCoreHelper", "Not found networkInfo");
        return;
      }
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("Connection state changed to - ");
      ((StringBuilder)localObject1).append(((NetworkInfo)localObject3).toString());
      cn.jiguang.ac.c.b("JCoreHelper", ((StringBuilder)localObject1).toString());
      if ((2 != ((NetworkInfo)localObject3).getType()) && (3 != ((NetworkInfo)localObject3).getType()))
      {
        bool = paramIntent.getBooleanExtra("noConnectivity", false);
        localObject1 = paramIntent.getExtras();
        paramIntent = (Intent)localObject1;
        if (localObject1 == null) {
          paramIntent = new Bundle();
        }
        if (bool)
        {
          cn.jiguang.ac.c.b("JCoreHelper", "No any network is connected");
          paramIntent.putBoolean("connected", false);
        }
        else
        {
          try
          {
            if (NetworkInfo.State.CONNECTED == ((NetworkInfo)localObject3).getState())
            {
              cn.jiguang.ac.c.b("JCoreHelper", "Network is connected.");
              paramIntent.putBoolean("connected", true);
            }
            else if (NetworkInfo.State.DISCONNECTED == ((NetworkInfo)localObject3).getState())
            {
              cn.jiguang.ac.c.b("JCoreHelper", "Network is disconnected.");
              paramIntent.putBoolean("connected", false);
            }
            else
            {
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append("other network state - ");
              ((StringBuilder)localObject1).append(((NetworkInfo)localObject3).getState());
              ((StringBuilder)localObject1).append(". Do nothing.");
              cn.jiguang.ac.c.b("JCoreHelper", ((StringBuilder)localObject1).toString());
            }
          }
          catch (Throwable localThrowable)
          {
            paramIntent.putBoolean("connected", cn.jiguang.as.e.b(paramContext));
          }
        }
        cn.jiguang.a.a.a(paramContext, "tcp_a15", paramIntent);
      }
      else
      {
        cn.jiguang.ac.c.b("JCoreHelper", "MMS or SUPL network state change, to do nothing!");
      }
    }
    else if ((!localThrowable.equals("android.os.action.DEVICE_IDLE_MODE_CHANGED")) && (!localThrowable.equals("android.os.action.POWER_SAVE_MODE_CHANGED")))
    {
      if ((!localThrowable.equals("noti_open_proxy")) || (!paramIntent.getBooleanExtra("debug_notification", false))) {
        break label632;
      }
      paramIntent = paramIntent.getStringExtra("toastText");
      if (TextUtils.isEmpty(paramIntent)) {
        break label632;
      }
      paramContext = Toast.makeText(paramContext, paramIntent, 0);
    }
    try
    {
      localObject2 = paramContext.getView();
      if ((localObject2 instanceof LinearLayout))
      {
        localObject2 = ((LinearLayout)localObject2).getChildAt(0);
        if ((localObject2 instanceof TextView))
        {
          localObject2 = (TextView)localObject2;
          if (!h.a(paramIntent)) {
            ((TextView)localObject2).setText(paramIntent);
          }
          ((TextView)localObject2).setTextSize(13.0F);
        }
      }
    }
    catch (Exception paramIntent)
    {
      Object localObject2;
      for (;;) {}
    }
    paramContext.show();
    return;
    paramIntent = (PowerManager)paramContext.getSystemService("power");
    if (paramIntent != null) {
      try
      {
        localObject3 = Class.forName("android.os.PowerManager");
        if (localObject3 != null)
        {
          if (((String)localObject2).equals("android.os.action.DEVICE_IDLE_MODE_CHANGED"))
          {
            localObject2 = ((Class)localObject3).getDeclaredMethod("isDeviceIdleMode", new Class[0]);
            if (localObject2 == null) {}
          }
          else
          {
            for (paramIntent = (Boolean)((Method)localObject2).invoke(paramIntent, new Object[0]);; paramIntent = (Boolean)((Method)localObject2).invoke(paramIntent, new Object[0]))
            {
              bool = paramIntent.booleanValue();
              break label579;
              if (!((String)localObject2).equals("android.os.action.POWER_SAVE_MODE_CHANGED")) {
                break;
              }
              localObject2 = ((Class)localObject3).getDeclaredMethod("isPowerSaveMode", new Class[0]);
              if (localObject2 == null) {
                break;
              }
            }
          }
          bool = true;
          label579:
          if (!bool)
          {
            cn.jiguang.ac.c.b("JCoreHelper", "doze or powersave mode exit.");
            cn.jiguang.a.a.a(paramContext, true, 0L);
          }
        }
      }
      catch (Throwable paramIntent)
      {
        paramContext = new StringBuilder();
        paramContext.append("handle DEVICE_IDLE_MODE_CHANGED or POWER_SAVE_MODE_CHANGED fail:");
        paramContext.append(paramIntent);
        cn.jiguang.ac.c.h("JCoreHelper", paramContext.toString());
      }
    }
    label632:
  }
  
  public static void a(Context paramContext, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("sdk_type", paramString);
    cn.jiguang.a.a.a(paramContext, "tcp_a8", localBundle);
  }
  
  public static void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    cn.jiguang.a.a.a(paramContext, paramString, paramBundle);
  }
  
  public static void a(Context paramContext, String paramString1, Bundle paramBundle, String paramString2)
  {
    if (paramBundle != null)
    {
      paramBundle.putString("sdk_type", paramString1);
      cn.jiguang.a.a.a(paramContext, paramString2, paramBundle);
    }
  }
  
  public static void a(Context paramContext, String paramString, Object paramObject)
  {
    f.a(paramContext, paramString, paramObject);
  }
  
  public static void a(Context paramContext, String paramString, Object paramObject, ReportCallBack paramReportCallBack)
  {
    f.a(paramContext, (JSONObject)paramObject, paramReportCallBack);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
  {
    Bundle localBundle = paramBundle;
    if (paramBundle == null) {
      localBundle = new Bundle();
    }
    paramBundle = new StringBuilder();
    paramBundle.append("runActionWithService action:");
    paramBundle.append(paramString2);
    cn.jiguang.ac.c.c("JCoreHelper", paramBundle.toString());
    localBundle.putString("sdk_type", paramString1);
    localBundle.putString("internal_action", paramString2);
    cn.jiguang.a.a.a(paramContext, "a3", localBundle);
  }
  
  public static void a(Bundle paramBundle) {}
  
  public static void a(String paramString1, String paramString2, boolean paramBoolean, int paramInt, String paramString3, Throwable paramThrowable)
  {
    cn.jiguang.ac.a.a(paramString1, paramString2, paramBoolean, paramInt, paramString3, paramThrowable);
  }
  
  public static boolean a(Context paramContext)
  {
    cn.jiguang.ac.c.b("JCoreHelper", "canShowLbsPermissionDialog");
    long l1 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.x())).longValue();
    long l2 = ((Long)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.w())).longValue();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("lbs permission dialog shield, firstInit=");
    localStringBuilder.append(l2);
    localStringBuilder.append(", delay=");
    localStringBuilder.append(l1);
    cn.jiguang.ac.c.b("JCoreHelper", localStringBuilder.toString());
    boolean bool2 = false;
    boolean bool1 = false;
    if (l2 > 0L)
    {
      if (l1 > 0L)
      {
        if (System.currentTimeMillis() > l2 + l1) {
          bool1 = true;
        }
        return bool1;
      }
      return true;
    }
    cn.jiguang.e.b.a(paramContext, new cn.jiguang.e.a[] { cn.jiguang.e.a.w().a(Long.valueOf(System.currentTimeMillis())) });
    bool1 = bool2;
    if (l1 <= 0L) {
      bool1 = true;
    }
    return bool1;
  }
  
  public static String b(Context paramContext)
  {
    return (String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.d());
  }
  
  public static void b(Context paramContext, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("sdk_type", paramString);
    cn.jiguang.a.a.a(paramContext, "tcp_a9", localBundle);
  }
  
  public static void b(Context paramContext, String paramString, Bundle paramBundle)
  {
    cn.jiguang.a.a.b(paramContext, paramString, paramBundle);
  }
  
  private static void b(Bundle paramBundle)
  {
    if ((paramBundle != null) && (!paramBundle.isEmpty())) {}
    try
    {
      cn.jiguang.sdk.impl.b.a(paramBundle);
      int i = paramBundle.getInt("ipv_config", -1);
      if ((i != 2) && (i != 3))
      {
        if ((i == 0) || (i == 1)) {
          f.b = true;
        }
      }
      else {
        f.b = false;
      }
      return;
    }
    catch (Throwable paramBundle)
    {
      for (;;) {}
    }
  }
  
  public static Bundle c(Context paramContext, String paramString, Bundle paramBundle)
  {
    if ("isTcpLoggedIn".equals(paramString))
    {
      boolean bool = cn.jiguang.sdk.impl.b.b();
      paramContext = new Bundle();
      paramContext.putBoolean("state", bool);
      return paramContext;
    }
    return null;
  }
  
  public static String c(Context paramContext)
  {
    return (String)cn.jiguang.e.b.b(paramContext, cn.jiguang.e.a.l());
  }
  
  public static void c(Context paramContext, String paramString)
  {
    b.a(paramContext, paramString);
  }
  
  public static String d(Context paramContext)
  {
    return (String)cn.jiguang.e.b.a(paramContext, cn.jiguang.e.a.m());
  }
  
  public static String e(Context paramContext)
  {
    return cn.jiguang.as.c.a(paramContext);
  }
  
  public static String f(Context paramContext)
  {
    return b.a(paramContext);
  }
  
  public static String g(Context paramContext)
  {
    return b.b(paramContext);
  }
  
  public static Object h(Context paramContext)
  {
    return cn.jiguang.ah.e.a(paramContext, "deviceinfo", null);
  }
  
  public static Pair<String, Integer> i(Context paramContext)
  {
    return b.c(paramContext);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ab/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */