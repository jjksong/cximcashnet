package cn.jiguang.ao;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import java.util.HashMap;
import java.util.Map;

public class b
{
  private static volatile b a;
  private static final Object b = new Object();
  private boolean c = false;
  private Map<Integer, a> d = new HashMap();
  private Handler e;
  private HandlerThread f;
  
  public static b a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          b localb = new cn/jiguang/ao/b;
          localb.<init>();
          a = localb;
        }
      }
    }
    return a;
  }
  
  public void a(int paramInt, long paramLong, a parama)
  {
    if (this.e == null) {
      return;
    }
    parama.b = paramLong;
    parama.c = 1;
    this.d.put(Integer.valueOf(paramInt), parama);
    if (this.e.hasMessages(paramInt))
    {
      cn.jiguang.ai.a.g("TaskHandlerManager", "registerFixedAction,same action in handler,will replace");
      this.e.removeMessages(paramInt);
    }
    this.e.sendEmptyMessageDelayed(paramInt, paramLong);
  }
  
  public void a(Context paramContext)
  {
    try
    {
      boolean bool = this.c;
      if (bool) {
        return;
      }
      if (paramContext == null)
      {
        cn.jiguang.ai.a.c("TaskHandlerManager", "init context is null");
        return;
      }
      cn.jiguang.ai.a.c("TaskHandlerManager", "init task manager...");
      try
      {
        if ((this.f == null) || (!this.f.isAlive()))
        {
          paramContext = new cn/jiguang/ao/b$1;
          paramContext.<init>(this, "TaskHandlerManager");
          this.f = paramContext;
          this.f.start();
        }
        a locala = new cn/jiguang/ao/b$a;
        if (this.f.getLooper() == null) {
          paramContext = Looper.getMainLooper();
        } else {
          paramContext = this.f.getLooper();
        }
        locala.<init>(this, paramContext);
        this.e = locala;
      }
      catch (Exception paramContext)
      {
        paramContext = new cn/jiguang/ao/b$a;
        paramContext.<init>(this, Looper.getMainLooper());
        this.e = paramContext;
      }
      this.c = true;
      return;
    }
    finally {}
  }
  
  public boolean a(int paramInt)
  {
    Handler localHandler = this.e;
    if (localHandler == null) {
      return false;
    }
    return localHandler.hasMessages(paramInt);
  }
  
  public void b(int paramInt)
  {
    if (this.e == null) {
      return;
    }
    this.d.remove(Integer.valueOf(paramInt));
    this.e.removeMessages(paramInt);
  }
  
  public void b(int paramInt, long paramLong, a parama)
  {
    if (this.e == null) {
      return;
    }
    parama.c = 2;
    this.d.put(Integer.valueOf(paramInt), parama);
    if (this.e.hasMessages(paramInt))
    {
      cn.jiguang.ai.a.g("TaskHandlerManager", "sendMsg,same action in handler,will replace");
      this.e.removeMessages(paramInt);
    }
    this.e.sendEmptyMessageDelayed(paramInt, paramLong);
  }
  
  class a
    extends Handler
  {
    public a(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      try
      {
        if (b.a(b.this).containsKey(Integer.valueOf(paramMessage.what)))
        {
          a locala = (a)b.a(b.this).get(Integer.valueOf(paramMessage.what));
          if (locala != null)
          {
            locala.a(paramMessage);
            if (locala.c == 1) {
              sendEmptyMessageDelayed(paramMessage.what, locala.b);
            } else {
              b.a(b.this).remove(Integer.valueOf(paramMessage.what));
            }
          }
        }
      }
      catch (Throwable paramMessage)
      {
        paramMessage.printStackTrace();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ao/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */