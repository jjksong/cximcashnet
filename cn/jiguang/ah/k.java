package cn.jiguang.ah;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ae.c;
import cn.jiguang.api.JDispatchAction;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class k
{
  private static volatile k a;
  private static final Object b = new Object();
  private Map<Long, String> c = new HashMap();
  
  public static k a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          k localk = new cn/jiguang/ah/k;
          localk.<init>();
          a = localk;
        }
      }
    }
    return a;
  }
  
  private String a(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = (String)c.a(paramContext, cn.jiguang.ae.b.a(paramString1));
    if ((!TextUtils.isEmpty(paramString2)) && (!paramString2.equals(paramContext))) {
      return paramString2;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("need not ");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(" userctrl,newest version:");
    paramContext = paramString2;
    if (TextUtils.isEmpty(paramString2)) {
      paramContext = "null";
    }
    localStringBuilder.append(paramContext);
    cn.jiguang.ai.a.a("UserCtrlHelper", localStringBuilder.toString());
    return "";
  }
  
  private void a(Context paramContext, short paramShort, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("sendUserCtrlInfo sdkType:");
    localStringBuilder.append(paramString2);
    localStringBuilder.append(",property:");
    localStringBuilder.append(paramShort);
    localStringBuilder.append(",verInfo:");
    localStringBuilder.append(paramString1);
    cn.jiguang.ai.a.c("UserCtrlHelper", localStringBuilder.toString());
    long l = j.b();
    paramString1 = cn.jiguang.ak.b.a(paramShort, (short)1, paramString1);
    cn.jiguang.sdk.impl.b.a(paramContext, cn.jiguang.sdk.impl.a.f, 26, 0, l, 10000L, paramString1);
    this.c.put(Long.valueOf(l), paramString2);
  }
  
  public void a(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jiguang.ai.a.g("UserCtrlHelper", "handleUserCtrl failed,context is null");
      return;
    }
    Object localObject = b.a;
    if ((localObject != null) && (!((HashMap)localObject).isEmpty()))
    {
      localObject = ((HashMap)localObject).entrySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
        JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
        if (localJDispatchAction != null)
        {
          String str = a(paramContext, (String)localEntry.getKey(), localJDispatchAction.getSdkVersion((String)localEntry.getKey()));
          if (!TextUtils.isEmpty(str)) {
            a(paramContext, localJDispatchAction.getUserCtrlProperty((String)localEntry.getKey()), str, (String)localEntry.getKey());
          }
        }
      }
    }
  }
  
  public void a(Context paramContext, long paramLong)
  {
    Object localObject1 = (String)this.c.remove(Long.valueOf(paramLong));
    if (!TextUtils.isEmpty((CharSequence)localObject1))
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("userCtrlSuccess rid:");
      ((StringBuilder)localObject2).append(paramLong);
      ((StringBuilder)localObject2).append(",sdkType:");
      ((StringBuilder)localObject2).append((String)localObject1);
      cn.jiguang.ai.a.c("UserCtrlHelper", ((StringBuilder)localObject2).toString());
      localObject2 = b.a().b((String)localObject1, "");
      if (!TextUtils.isEmpty((CharSequence)localObject2))
      {
        c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.a((String)localObject1).a(localObject2) });
      }
      else
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("userCtrlSuccess but not found sdkversion by sdkType:");
        ((StringBuilder)localObject2).append((String)localObject1);
        cn.jiguang.ai.a.c("UserCtrlHelper", ((StringBuilder)localObject2).toString());
      }
    }
    else
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("userCtrlSuccess but not found rid:");
      ((StringBuilder)localObject1).append(paramLong);
      cn.jiguang.ai.a.c("UserCtrlHelper", ((StringBuilder)localObject1).toString());
    }
    localObject1 = this.c;
    if ((localObject1 != null) && (((Map)localObject1).isEmpty()) && (!cn.jiguang.sdk.impl.b.p(paramContext))) {
      i.a().a(paramContext, "tcp_a20", null);
    }
  }
  
  public void a(Context paramContext, long paramLong, int paramInt)
  {
    paramContext = (String)this.c.remove(Long.valueOf(paramLong));
    if (!TextUtils.isEmpty(paramContext))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("onUserCtrlFailed rid:");
      localStringBuilder.append(paramLong);
      localStringBuilder.append(",sdkType:");
      localStringBuilder.append(paramContext);
      localStringBuilder.append(",errorCode:");
      localStringBuilder.append(paramInt);
      cn.jiguang.ai.a.c("UserCtrlHelper", localStringBuilder.toString());
      if (TextUtils.isEmpty(b.a().b(paramContext, "")))
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("onUserCtrlFailed but not found sdkversion by sdkType:");
        localStringBuilder.append(paramContext);
        cn.jiguang.ai.a.c("UserCtrlHelper", localStringBuilder.toString());
      }
    }
    else
    {
      paramContext = new StringBuilder();
      paramContext.append("onUserCtrlFailed but not found rid:");
      paramContext.append(paramLong);
      cn.jiguang.ai.a.c("UserCtrlHelper", paramContext.toString());
    }
  }
  
  public void b(Context paramContext, long paramLong)
  {
    paramContext = new StringBuilder();
    paramContext.append("onUserCtrlTimeout rid:");
    paramContext.append(paramLong);
    cn.jiguang.ai.a.c("UserCtrlHelper", paramContext.toString());
    this.c.remove(Long.valueOf(paramLong));
  }
  
  public boolean b(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jiguang.ai.a.h("UserCtrlHelper", "get isNeedUserCtrl failed,context is null");
      return false;
    }
    Object localObject = b.a;
    if ((localObject != null) && (!((HashMap)localObject).isEmpty()))
    {
      localObject = ((HashMap)localObject).entrySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
        JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
        if ((localJDispatchAction != null) && (!TextUtils.isEmpty(a(paramContext, (String)localEntry.getKey(), localJDispatchAction.getSdkVersion((String)localEntry.getKey()))))) {
          return true;
        }
      }
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */