package cn.jiguang.ah;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class i
{
  private static volatile i i;
  private static final Object j = new Object();
  private cn.jiguang.af.h a;
  private int b;
  private int c;
  private int d = 0;
  private int e = 0;
  private long f;
  private boolean g;
  private boolean h = true;
  private Context k;
  private boolean l = false;
  private final AtomicBoolean m = new AtomicBoolean(false);
  private cn.jiguang.ao.a n = new cn.jiguang.ao.a()
  {
    public void a(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage != null)
      {
        int i = paramAnonymousMessage.what;
        i locali;
        Object localObject;
        if (i != 1011) {
          if (i != 1022) {
            if (i != 2000) {
              switch (i)
              {
              default: 
                break;
              case 1006: 
              case 1007: 
                locali = i.this;
                paramAnonymousMessage = i.a(locali);
                localObject = "tcp_a2";
              }
            }
          }
        }
        for (;;)
        {
          locali.a(paramAnonymousMessage, (String)localObject, null);
          break;
          paramAnonymousMessage = new Bundle();
          boolean bool = false;
          break label114;
          paramAnonymousMessage = new Bundle();
          bool = true;
          label114:
          paramAnonymousMessage.putBoolean("force", bool);
          localObject = i.this;
          ((i)localObject).a(i.a((i)localObject), "tcp_a16", paramAnonymousMessage);
          break;
          k.a().a(i.a(i.this));
          break;
          locali = i.this;
          paramAnonymousMessage = i.a(locali);
          localObject = "tcp_a17";
          continue;
          locali = i.this;
          paramAnonymousMessage = i.a(locali);
          localObject = "tcp_a14";
        }
      }
    }
  };
  
  public static i a()
  {
    if (i == null) {
      synchronized (j)
      {
        if (i == null)
        {
          i locali = new cn/jiguang/ah/i;
          locali.<init>();
          i = locali;
        }
      }
    }
    return i;
  }
  
  private void a(int paramInt)
  {
    this.b = paramInt;
    if (paramInt == 1012) {
      cn.jiguang.af.c.a(this.k);
    }
    h();
  }
  
  private void b(Context paramContext)
  {
    cn.jiguang.ai.a.c("JCoreTCPManager", "handleStop...");
    if (((Boolean)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.a())).booleanValue())
    {
      cn.jiguang.ai.a.d("JCoreTCPManager", "tcp already stoped");
      return;
    }
    if (!b.a().a(0))
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "Action: handleStopPush - can't stop tcp");
      return;
    }
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.a().a(Boolean.valueOf(true)) });
    h();
  }
  
  private void c(Context paramContext)
  {
    cn.jiguang.ai.a.c("JCoreTCPManager", "handleResume...");
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.a().a(Boolean.valueOf(false)) });
    if (this.g) {}
    for (paramContext = "[handleResume] is loggedin";; paramContext = "[handleResume] tcp is connecting...")
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", paramContext);
      return;
      if (this.a == null) {
        break;
      }
    }
    j();
  }
  
  private void c(Bundle paramBundle)
  {
    if (this.m.get()) {}
    for (paramBundle = "isBeating, skip this time";; paramBundle = "No need to rtc, Because it have succeed recently")
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", paramBundle);
      return;
      boolean bool = false;
      if (paramBundle != null) {
        bool = paramBundle.getBoolean("force", false);
      }
      if ((bool) || (!k())) {
        break;
      }
    }
    cn.jiguang.ai.a.d("JCoreTCPManager", "Send heart beat");
    cn.jiguang.ao.b.a().b(1005);
    if (this.g) {
      m();
    } else {
      cn.jiguang.ai.a.c("JCoreTCPManager", "socket is closed or push isn't login");
    }
  }
  
  private void e()
  {
    cn.jiguang.ai.a.d("JCoreTCPManager", "Action - onLoggedIn");
    if (!this.g) {
      b.a().a(this.k, 1, 0, "success");
    }
    this.g = true;
    b();
    this.d = 0;
    this.e = 0;
    Bundle localBundle = new Bundle();
    localBundle.putBoolean("login", true);
    e.a(this.k, "periodtask", localBundle);
    if (!f()) {
      return;
    }
    cn.jiguang.ao.b.a().b(2000, 2000L, this.n);
    cn.jiguang.am.a.a().a(this.k);
    j.a().a(this.k);
    h.a().a(this.k, true);
  }
  
  private boolean f()
  {
    if ((!cn.jiguang.sdk.impl.b.p(this.k)) && (!k.a().b(this.k)))
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "not keep tcp");
      this.h = false;
      h();
      return false;
    }
    return true;
  }
  
  private void g()
  {
    cn.jiguang.ai.a.d("JCoreTCPManager", "Action - onDisconnected");
    if (this.g) {
      b.a().a(this.k, -1, -1, "push connect break");
    }
    this.g = false;
    if ((this.a == null) && (((Boolean)cn.jiguang.ae.c.a(this.k, cn.jiguang.ae.b.a())).booleanValue()))
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "push already stopped!!!");
      return;
    }
    this.e = 0;
    h();
    i();
    this.d += 1;
  }
  
  private void h()
  {
    cn.jiguang.af.h localh = this.a;
    if (localh != null)
    {
      localh.b();
      this.a = null;
    }
    else
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "tcp has stopeed");
    }
  }
  
  private void i()
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Action - retryConnect - disconnectedTimes:");
    ((StringBuilder)localObject).append(this.d);
    cn.jiguang.ai.a.d("JCoreTCPManager", ((StringBuilder)localObject).toString());
    if (!cn.jiguang.ap.a.c(this.k.getApplicationContext())) {}
    for (localObject = "[retryConnect] network is not connect";; localObject = ((StringBuilder)localObject).toString())
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", (String)localObject);
      return;
      if (this.c <= 0) {
        break;
      }
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("[retryConnect] registerErrCode >0,registerErrCode:");
      ((StringBuilder)localObject).append(this.c);
    }
    int i4 = cn.jiguang.ap.a.b(this.k.getApplicationContext());
    int i3 = (int)(Math.pow(2.0D, this.d) * 3.0D * 1000.0D);
    int i5 = cn.jiguang.sdk.impl.a.d;
    int i2 = i5 * 1000 / 2;
    int i1 = i3;
    if (i3 > i2) {
      i1 = i2;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("[retryConnect] mDisconnectedTimes:");
    ((StringBuilder)localObject).append(this.d);
    ((StringBuilder)localObject).append(",chargedLever:");
    ((StringBuilder)localObject).append(i4);
    ((StringBuilder)localObject).append(",heartbeatInterval:");
    ((StringBuilder)localObject).append(i5);
    ((StringBuilder)localObject).append(",delayTime:");
    ((StringBuilder)localObject).append(i1);
    cn.jiguang.ai.a.c("JCoreTCPManager", ((StringBuilder)localObject).toString());
    if (i4 == 1 ? this.d < 30 : this.d < 5)
    {
      if (!cn.jiguang.ao.b.a().a(1011))
      {
        cn.jiguang.ao.b.a().b(1011, i1, this.n);
        return;
      }
      localObject = "Already has MSG_RESTART_CONN";
    }
    else
    {
      localObject = "Give up to retry connect.";
    }
    cn.jiguang.ai.a.c("JCoreTCPManager", (String)localObject);
  }
  
  private void j()
  {
    try
    {
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append("Action - restartNetworkingClient, pid:");
      ((StringBuilder)localObject1).append(Process.myPid());
      cn.jiguang.ai.a.d("JCoreTCPManager", ((StringBuilder)localObject1).toString());
      if (!this.h)
      {
        cn.jiguang.ai.a.f("JCoreTCPManager", "need not keep tcp,next start app will re login");
        return;
      }
      if (!cn.jiguang.ap.a.c(this.k.getApplicationContext()))
      {
        cn.jiguang.ai.a.f("JCoreTCPManager", "No network connection. Give up to start connection thread.");
        return;
      }
      if (((Boolean)cn.jiguang.ae.c.a(this.k, cn.jiguang.ae.b.a())).booleanValue())
      {
        cn.jiguang.ai.a.d("JCoreTCPManager", "[restartNetworkingClient] tcp has close by active");
        return;
      }
      if ((this.c != 1005) && (this.c != 1006) && (this.c != 1008) && (this.c != 1009))
      {
        if (this.b == 102)
        {
          cn.jiguang.ai.a.h("JCoreTCPManager", "login failed:102,give up start connection thread.reset from next app start");
          return;
        }
        if (this.a != null)
        {
          cn.jiguang.ai.a.d("JCoreTCPManager", "NetworkingClient is running");
          return;
        }
        localObject1 = new cn/jiguang/af/h;
        ((cn.jiguang.af.h)localObject1).<init>(this.k.getApplicationContext());
        this.a = ((cn.jiguang.af.h)localObject1);
        this.a.a();
        return;
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append("[restartNetworkingClient] registerErrCode >0,registerErrCode:");
      ((StringBuilder)localObject1).append(this.c);
      cn.jiguang.ai.a.c("JCoreTCPManager", ((StringBuilder)localObject1).toString());
      return;
    }
    finally {}
  }
  
  private boolean k()
  {
    boolean bool;
    if (System.currentTimeMillis() - this.f < 18000L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  private void l()
  {
    this.m.set(false);
    this.e += 1;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action - onHeartbeatTimeout - timeoutTimes:");
    localStringBuilder.append(this.e);
    cn.jiguang.ai.a.d("JCoreTCPManager", localStringBuilder.toString());
    cn.jiguang.ai.a.b("JCoreTCPManager", "============================================================");
    if ((this.a != null) && (!this.g))
    {
      cn.jiguang.ai.a.d("JCoreTCPManager", "Is connecting now. Give up to retry.");
      return;
    }
    if ((this.g) && (this.e <= 1))
    {
      cn.jiguang.ai.a.d("JCoreTCPManager", "Already logged in. Give up to retry.");
      cn.jiguang.ao.b.a().b(1005, 5000L, this.n);
      return;
    }
    h();
    i();
  }
  
  private void m()
  {
    this.m.set(true);
    cn.jiguang.ao.b.a().b(1022);
    long l2 = cn.jiguang.af.c.c(this.k);
    long l1 = cn.jiguang.sdk.impl.b.e(this.k);
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("heartbeat - juid:");
    ((StringBuilder)localObject).append(l1);
    ((StringBuilder)localObject).append(", flag:");
    ((StringBuilder)localObject).append(1);
    cn.jiguang.ai.a.c("JCoreTCPManager", ((StringBuilder)localObject).toString());
    localObject = cn.jiguang.ak.b.a(Long.valueOf(l2).longValue(), cn.jiguang.sdk.impl.a.g, l1, (short)1);
    localObject = cn.jiguang.ak.b.a(this.k, (byte[])localObject);
    if (localObject != null) {
      this.a.c().a((byte[])localObject);
    } else {
      cn.jiguang.ai.a.h("JCoreTCPManager", "send hb failed:sendData is null");
    }
    cn.jiguang.ao.b.a().b(1022, 10000L, this.n);
  }
  
  private void n()
  {
    cn.jiguang.ai.a.d("JCoreTCPManager", "Action - onHeartbeatSucceed");
    b.a().a(this.k, 19, 0, "ack success");
  }
  
  public void a(Context paramContext)
  {
    try
    {
      boolean bool = this.l;
      if (bool) {
        return;
      }
      if (paramContext == null)
      {
        cn.jiguang.ai.a.c("JCoreTCPManager", "init context is null");
        return;
      }
      cn.jiguang.ai.a.c("JCoreTCPManager", "init tcp manager...");
      this.k = paramContext.getApplicationContext();
      cn.jiguang.sdk.impl.b.a("JCoreTCPManager");
      cn.jiguang.ao.b.a().a(this.k);
      h.a().a(paramContext, true);
      this.l = true;
      return;
    }
    finally {}
  }
  
  public void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    a(paramContext);
    cn.jiguang.sdk.impl.b.a("JCoreTCPManager", new a(paramContext, paramString, paramBundle), new int[0]);
  }
  
  public void a(Bundle paramBundle)
  {
    if (((Boolean)cn.jiguang.ae.c.a(this.k, cn.jiguang.ae.b.a())).booleanValue())
    {
      cn.jiguang.ai.a.d("JCoreTCPManager", "[rtc] tcp has close by active");
      return;
    }
    boolean bool = true;
    long l1;
    if (paramBundle != null)
    {
      bool = paramBundle.getBoolean("force", true);
      l1 = paramBundle.getLong("delay_time", 0L);
    }
    else
    {
      l1 = 0L;
    }
    if (this.a == null)
    {
      j();
    }
    else
    {
      if (l1 <= 0L)
      {
        c(paramBundle);
      }
      else
      {
        int i1 = 1004;
        if (bool)
        {
          cn.jiguang.ao.b.a().b(1005);
          cn.jiguang.ao.b.a().b(1004);
        }
        paramBundle = cn.jiguang.ao.b.a();
        if (!bool) {
          i1 = 1005;
        }
        paramBundle.b(i1, l1, this.n);
      }
      paramBundle = new StringBuilder();
      paramBundle.append("send rtc force=");
      paramBundle.append(bool);
      paramBundle.append(" delay=");
      paramBundle.append(l1);
      cn.jiguang.ai.a.e("JCoreTCPManager", paramBundle.toString());
    }
  }
  
  public void b()
  {
    cn.jiguang.ao.b.a().b(1022);
    this.f = System.currentTimeMillis();
    this.e = 0;
    this.m.set(false);
    cn.jiguang.ai.a.e("JCoreTCPManager", "update rtc state");
  }
  
  public void b(Bundle paramBundle)
  {
    if (((Boolean)cn.jiguang.ae.c.a(this.k, cn.jiguang.ae.b.a())).booleanValue())
    {
      cn.jiguang.ai.a.g("JCoreTCPManager", "[netWorkChanged] tcp has close by active");
      return;
    }
    cn.jiguang.ao.b.a().b(1006);
    cn.jiguang.ao.b.a().b(1007);
    if (paramBundle.getBoolean("connected", false))
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "Handle connected state.");
      if (this.a == null) {
        j();
      } else {
        cn.jiguang.ao.b.a().b(1006, 3000L, this.n);
      }
    }
    else
    {
      cn.jiguang.ai.a.c("JCoreTCPManager", "Handle disconnected state.");
      cn.jiguang.ao.b.a().b(1007, 3000L, this.n);
    }
  }
  
  public cn.jiguang.af.h c()
  {
    return this.a;
  }
  
  public boolean d()
  {
    return this.g;
  }
  
  class a
    implements Runnable
  {
    private Context b;
    private String c;
    private Bundle d;
    
    public a(Context paramContext, String paramString, Bundle paramBundle)
    {
      this.b = paramContext;
      this.c = paramString;
      this.d = paramBundle;
    }
    
    public void run()
    {
      try
      {
        Object localObject1;
        if (this.c.equals("tcp_a1"))
        {
          if (i.b(i.this) == null) {
            localObject1 = i.this;
          }
        }
        else {
          for (;;)
          {
            i.c((i)localObject1);
            break;
            byte[] arrayOfByte;
            int i;
            int j;
            long l2;
            long l1;
            Object localObject2;
            if ((!this.c.equals("tcp_a3")) && (!this.c.equals("tcp_a4")) && (!this.c.equals("tcp_a20")))
            {
              if (this.c.equals("tcp_a5"))
              {
                if (this.d == null) {
                  break;
                }
                arrayOfByte = this.d.getByteArray("body");
                i = this.d.getInt("cmd", -1);
                j = this.d.getInt("ver", -1);
                l2 = this.d.getLong("rid", -1L);
                localObject1 = this.d.getString("sdk_type");
                l1 = this.d.getLong("timeout");
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                ((StringBuilder)localObject2).append("send quest,cmd:");
                ((StringBuilder)localObject2).append(i);
                ((StringBuilder)localObject2).append(",ver:");
                ((StringBuilder)localObject2).append(j);
                ((StringBuilder)localObject2).append(",rid:");
                ((StringBuilder)localObject2).append(l2);
                ((StringBuilder)localObject2).append(",body size:");
                ((StringBuilder)localObject2).append(arrayOfByte.length);
                cn.jiguang.ai.a.c("JCoreTCPManager", ((StringBuilder)localObject2).toString());
                if ((i >= 0) && (j >= 0) && (l2 >= 0L) && (!TextUtils.isEmpty((CharSequence)localObject1)))
                {
                  j.a().a(this.b, l2, i, j, arrayOfByte, (String)localObject1, l1);
                  break;
                }
                return;
              }
              if (this.c.equals("tcp_a19"))
              {
                i.d(i.this);
                break;
              }
              if (this.c.equals("tcp_a11"))
              {
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                ((StringBuilder)localObject1).append("resgiter success:");
                ((StringBuilder)localObject1).append(cn.jiguang.sdk.impl.b.g(this.b));
                cn.jiguang.ai.a.d("JCoreTCPManager", ((StringBuilder)localObject1).toString());
                b.a().a(this.b, 0, 0, cn.jiguang.sdk.impl.b.g(this.b));
                e.a(this.b, "on_register", null);
                break;
              }
              if (this.c.equals("tcp_a10"))
              {
                i.e(i.this);
                break;
              }
              if (this.c.equals("tcp_a9"))
              {
                i.a(i.this, this.b);
                break;
              }
              if (this.c.equals("tcp_a8"))
              {
                i.b(i.this, this.b);
                break;
              }
              if (this.c.equals("tcp_a2"))
              {
                h.a().a(this.b, false);
                i.this.a(this.d);
                break;
              }
              if (this.c.equals("tcp_a13"))
              {
                cn.jiguang.ai.a.d("JCoreTCPManager", "resgiter failed");
                if (this.d == null) {
                  break;
                }
                i = this.d.getInt("resCode", 0);
                i.a(i.this, i);
                cn.jiguang.af.c.a(this.b, i);
                break;
              }
              if (this.c.equals("tcp_a12"))
              {
                cn.jiguang.ai.a.d("JCoreTCPManager", "login failed");
                if (this.d == null) {
                  break;
                }
                i = this.d.getInt("resCode", 0);
                i.b(i.this, i);
                break;
              }
              if (this.c.equals("tcp_a14"))
              {
                localObject1 = i.this;
              }
              else
              {
                if (this.c.equals("tcp_a15"))
                {
                  i.this.b(this.d);
                  break;
                }
                if (this.c.equals("tcp_a16"))
                {
                  i.a(i.this, this.d);
                  break;
                }
                if (this.c.equals("tcp_a17"))
                {
                  i.f(i.this);
                  break;
                }
                if (this.c.equals("tcp_a18"))
                {
                  i.g(i.this);
                  break;
                }
                if (this.c.equals("tcp_a6"))
                {
                  if (this.d == null) {
                    break;
                  }
                  l1 = this.d.getLong("rid", -1L);
                  if (l1 <= 0L) {
                    break;
                  }
                  j.a().b(this.b, l1);
                  break;
                }
                if (this.c.equals("tcp_a7"))
                {
                  if (this.d == null) {
                    break;
                  }
                  l1 = this.d.getLong("rid", -1L);
                  if (l1 <= 0L) {
                    break;
                  }
                  j.a().a(this.b, l1);
                  break;
                }
                if (this.c.equals("tcp_a20"))
                {
                  i.h(i.this);
                  break;
                }
                if (!this.c.equals("tcp_a21")) {
                  break;
                }
                i.this.b();
                break;
              }
            }
            else
            {
              if (i.b(i.this) != null)
              {
                if (i.b(i.this).c() == null) {
                  break;
                }
                arrayOfByte = this.d.getByteArray("body");
                j = this.d.getInt("cmd", -1);
                i = this.d.getInt("ver", -1);
                l2 = this.d.getLong("rid", -1L);
                localObject2 = this.d.getString("sdk_type");
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                ((StringBuilder)localObject1).append("send data,cmd:");
                ((StringBuilder)localObject1).append(j);
                ((StringBuilder)localObject1).append(",,ver:");
                ((StringBuilder)localObject1).append(i);
                ((StringBuilder)localObject1).append(",rid:");
                ((StringBuilder)localObject1).append(l2);
                ((StringBuilder)localObject1).append(",body size:");
                ((StringBuilder)localObject1).append(arrayOfByte.length);
                cn.jiguang.ai.a.c("JCoreTCPManager", ((StringBuilder)localObject1).toString());
                if ((j >= 0) && (i >= 0) && (l2 >= 0L))
                {
                  if (this.c.equals("tcp_a3"))
                  {
                    j.a().a(this.b, l2, j, i, arrayOfByte, (String)localObject2);
                    break;
                  }
                  if (this.c.equals("tcp_a20"))
                  {
                    l1 = this.d.getLong("uid", 0L);
                    if (l1 == 0L)
                    {
                      cn.jiguang.ai.a.g("JCoreTCPManager", "share response uid is 0");
                      return;
                    }
                  }
                  else
                  {
                    l1 = 0L;
                  }
                  localObject1 = cn.jiguang.ak.b.a(this.b, j, i, l2, arrayOfByte, l1);
                  i.a().c().c().a((byte[])localObject1);
                  break;
                }
                return;
              }
              cn.jiguang.ai.a.c("JCoreTCPManager", "send data failed:tcp breaked,will restart");
              localObject1 = i.this;
            }
          }
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        localThrowable.printStackTrace();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */