package cn.jiguang.ah;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import cn.jiguang.af.c;
import cn.jiguang.af.h;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class j
{
  private static volatile j a;
  private static final Object b = new Object();
  private static long e = 1L;
  private Map<Long, d> c = new HashMap();
  private cn.jiguang.ao.a d = new cn.jiguang.ao.a()
  {
    public void a(Message paramAnonymousMessage)
    {
      long l = paramAnonymousMessage.what - 100000;
      paramAnonymousMessage = new Bundle();
      paramAnonymousMessage.putLong("rid", l);
      i.a().a(cn.jiguang.sdk.impl.a.a, "tcp_a6", paramAnonymousMessage);
    }
  };
  
  public static j a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          j localj = new cn/jiguang/ah/j;
          localj.<init>();
          a = localj;
        }
      }
    }
    return a;
  }
  
  private byte[] a(Context paramContext, d paramd)
  {
    return cn.jiguang.ak.b.a(paramContext, paramd.d, paramd.e, paramd.f, paramd.g, 0L);
  }
  
  public static long b()
  {
    e += 1L;
    if (e >= 2147483647L) {
      e = 1L;
    }
    return e;
  }
  
  public d a(long paramLong)
  {
    return (d)this.c.get(Long.valueOf(paramLong));
  }
  
  public void a(Context paramContext)
  {
    if (this.c.isEmpty())
    {
      cn.jiguang.ai.a.c("TcpRequestManager", "no cache request");
      return;
    }
    Iterator localIterator = this.c.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (((d)localEntry.getValue()).j)
      {
        long l = System.nanoTime() - ((d)localEntry.getValue()).h;
        StringBuilder localStringBuilder;
        if (((d)localEntry.getValue()).i - l >= 10000L)
        {
          ((d)localEntry.getValue()).a();
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("send again:");
          localStringBuilder.append(localEntry.getValue());
          cn.jiguang.ai.a.c("TcpRequestManager", localStringBuilder.toString());
          i.a().c().c().a(a(paramContext, (d)localEntry.getValue()));
        }
        else
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append("shoud not send again by 10000ms,hasRequestTime:");
          localStringBuilder.append(l);
          localStringBuilder.append(",timeout:");
          localStringBuilder.append(((d)localEntry.getValue()).i);
          cn.jiguang.ai.a.c("TcpRequestManager", localStringBuilder.toString());
        }
      }
    }
  }
  
  public void a(Context paramContext, long paramLong)
  {
    d locald = (d)this.c.remove(Long.valueOf(paramLong));
    if (locald != null)
    {
      if (locald.j) {
        cn.jiguang.ao.b.a().b((int)(paramLong + 100000L));
      }
      paramContext = new StringBuilder();
      paramContext.append("handle reponse :");
      paramContext.append(locald);
      cn.jiguang.ai.a.c("TcpRequestManager", paramContext.toString());
    }
  }
  
  public void a(Context paramContext, long paramLong, int paramInt1, int paramInt2, byte[] paramArrayOfByte, String paramString)
  {
    long l = c.c(paramContext);
    if (this.c.containsKey(Long.valueOf(l)))
    {
      cn.jiguang.ai.a.h("TcpRequestManager", "Generator same rid,not do this msg");
      return;
    }
    paramArrayOfByte = new d(paramLong, paramString, paramInt1, paramInt2, l, 0L, paramArrayOfByte);
    if (i.a().d()) {
      i.a().c().c().a(a(paramContext, paramArrayOfByte));
    }
    this.c.put(Long.valueOf(l), paramArrayOfByte);
  }
  
  public void a(Context paramContext, long paramLong1, int paramInt1, int paramInt2, byte[] paramArrayOfByte, String paramString, long paramLong2)
  {
    long l;
    if (paramInt1 == 10)
    {
      l = paramLong1;
    }
    else
    {
      l = c.c(paramContext);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Generator new rid:");
      localStringBuilder.append(l);
      cn.jiguang.ai.a.c("TcpRequestManager", localStringBuilder.toString());
      if (this.c.containsKey(Long.valueOf(l)))
      {
        cn.jiguang.ai.a.h("TcpRequestManager", "Generator same rid,not do this msg");
        return;
      }
    }
    if (paramLong2 <= 0L) {
      paramLong2 = 10000L;
    }
    paramArrayOfByte = new d(paramLong1, paramString, paramInt1, paramInt2, l, paramLong2, paramArrayOfByte);
    if (i.a().d()) {
      i.a().c().c().a(a(paramContext, paramArrayOfByte));
    }
    paramArrayOfByte.h = System.nanoTime();
    this.c.put(Long.valueOf(l), paramArrayOfByte);
    cn.jiguang.ao.b.a().b((int)(l + 100000L), paramLong2, this.d);
  }
  
  public void b(Context paramContext, long paramLong)
  {
    d locald = (d)this.c.remove(Long.valueOf(paramLong));
    if (locald == null)
    {
      paramContext = new StringBuilder();
      paramContext.append("not found requst by rid:");
      paramContext.append(paramLong);
      cn.jiguang.ai.a.g("TcpRequestManager", paramContext.toString());
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("request time out:");
    localStringBuilder.append(locald);
    cn.jiguang.ai.a.c("TcpRequestManager", localStringBuilder.toString());
    b.a().a(paramContext, locald.c, locald.b, locald.d);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */