package cn.jiguang.ah;

public class g
  extends Exception
{
  public final int a;
  
  public g(int paramInt, String paramString)
  {
    super(paramString);
    this.a = paramInt;
  }
  
  public int a()
  {
    return this.a;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JException(");
    localStringBuilder.append(this.a);
    localStringBuilder.append("):");
    localStringBuilder.append(getLocalizedMessage());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */