package cn.jiguang.ah;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import cn.jpush.android.service.AlarmReceiver;

public class a
{
  public static void a(Context paramContext)
  {
    try
    {
      Object localObject = new android/content/Intent;
      ((Intent)localObject).<init>(paramContext, AlarmReceiver.class);
      localObject = PendingIntent.getBroadcast(paramContext, 0, (Intent)localObject, 0);
      ((AlarmManager)paramContext.getSystemService("alarm")).cancel((PendingIntent)localObject);
    }
    catch (Throwable paramContext)
    {
      cn.jiguang.ai.a.g("AlarmHelper", "Cancel heartbeat alarm failed.");
    }
  }
  
  public static void b(Context paramContext)
  {
    long l1 = cn.jiguang.sdk.impl.a.e * 1000;
    long l2 = System.currentTimeMillis() + l1;
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Reset heartbeat alarm, wait ");
    ((StringBuilder)localObject).append(l1);
    ((StringBuilder)localObject).append("ms.");
    cn.jiguang.ai.a.e("AlarmHelper", ((StringBuilder)localObject).toString());
    localObject = PendingIntent.getBroadcast(paramContext, 0, new Intent(paramContext, AlarmReceiver.class), 0);
    paramContext = (AlarmManager)paramContext.getSystemService("alarm");
    try
    {
      if (Build.VERSION.SDK_INT >= 19) {
        paramContext.setWindow(0, l2, 0L, (PendingIntent)localObject);
      } else {
        paramContext.setInexactRepeating(0, l2, l1, (PendingIntent)localObject);
      }
    }
    catch (Exception localException)
    {
      paramContext = new StringBuilder();
      paramContext.append("can't trigger alarm cause by exception:");
      paramContext.append(localException.getMessage());
      cn.jiguang.ai.a.h("AlarmHelper", paramContext.toString());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */