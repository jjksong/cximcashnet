package cn.jiguang.ah;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ak.c;
import cn.jiguang.api.JDispatchAction;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class b
{
  public static HashMap<String, JDispatchAction> a = new HashMap();
  private static volatile b b;
  private static final Object c = new Object();
  
  public static b a()
  {
    if (b == null) {
      synchronized (c)
      {
        if (b == null)
        {
          Object localObject2 = new cn/jiguang/ah/b;
          ((b)localObject2).<init>();
          b = (b)localObject2;
          localObject2 = cn.jiguang.sdk.impl.b.a();
          if ((localObject2 instanceof HashMap))
          {
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("actiom map size:");
            localStringBuilder.append(((HashMap)localObject2).size());
            cn.jiguang.ai.a.c("DispatchActionManager", localStringBuilder.toString());
            b.a((HashMap)localObject2);
            b.a(cn.jiguang.sdk.impl.a.f, f.class.getCanonicalName());
          }
        }
      }
    }
    return b;
  }
  
  private void b(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    Object localObject = null;
    if ((paramInt1 == 0) && (paramInt2 == 0)) {}
    try
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>("cn.jpush.android.intent.REGISTRATION");
      ((Intent)localObject).putExtra("cn.jpush.android.REGISTRATION_ID", paramString);
      paramString = (String)localObject;
      break label92;
      if (paramInt1 != -1)
      {
        paramString = (String)localObject;
        if (paramInt1 != 1) {}
      }
      else
      {
        paramString = new android/content/Intent;
        paramString.<init>("cn.jpush.android.intent.CONNECTION");
        if (paramInt1 == -1) {
          paramString.putExtra("cn.jpush.android.CONNECTION_CHANGE", false);
        } else {
          paramString.putExtra("cn.jpush.android.CONNECTION_CHANGE", true);
        }
      }
      label92:
      if (paramString != null)
      {
        localObject = paramContext.getPackageName();
        paramString.addCategory((String)localObject);
        paramString.setPackage((String)localObject);
        cn.jiguang.ap.a.a(paramContext, paramString);
      }
    }
    catch (Throwable paramString)
    {
      paramContext = new StringBuilder();
      paramContext.append("sendToOldPushUser failed:");
      paramContext.append(paramString.getMessage());
      cn.jiguang.ai.a.g("DispatchActionManager", paramContext.toString());
    }
  }
  
  public byte a(Context paramContext)
  {
    Iterator localIterator = a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)((Map.Entry)localObject).getValue();
      if (localJDispatchAction != null)
      {
        localObject = localJDispatchAction.beforLogin(paramContext, (String)((Map.Entry)localObject).getKey(), 23, "platformtype");
        if ((localObject instanceof Byte)) {
          return ((Byte)localObject).byteValue();
        }
      }
    }
    return 0;
  }
  
  public void a(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    Iterator localIterator = a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
      if (localJDispatchAction != null) {
        localJDispatchAction.onEvent(paramContext, (String)localEntry.getKey(), paramInt1, paramInt2, paramString);
      }
    }
    b(paramContext, paramInt1, paramInt2, paramString);
  }
  
  public void a(Context paramContext, c paramc, ByteBuffer paramByteBuffer)
  {
    if (paramc == null)
    {
      cn.jiguang.ai.a.g("DispatchActionManager", "Action - dispatchMessage unexcepted - head was null");
      return;
    }
    Object localObject1 = j.a().a(paramc.d.longValue());
    if (localObject1 != null)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("dispacth msg with reuqest :");
      ((StringBuilder)localObject2).append(localObject1);
      cn.jiguang.ai.a.c("DispatchActionManager", ((StringBuilder)localObject2).toString());
      localObject2 = (JDispatchAction)a.get(((d)localObject1).c);
      if (localObject2 != null) {
        ((JDispatchAction)localObject2).dispatchMessage(paramContext, ((d)localObject1).c, paramc.c, paramc.b, paramc.d.longValue(), ((d)localObject1).b, paramByteBuffer);
      }
      paramByteBuffer = new Bundle();
      paramByteBuffer.putLong("rid", paramc.d.longValue());
      i.a().a(paramContext, "tcp_a7", paramByteBuffer);
      return;
    }
    Object localObject2 = a.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject2).next();
      localObject1 = (JDispatchAction)localEntry.getValue();
      if ((localObject1 != null) && (((JDispatchAction)localObject1).isSupportedCMD((String)localEntry.getKey(), paramc.c))) {
        ((JDispatchAction)localObject1).dispatchMessage(paramContext, (String)localEntry.getKey(), paramc.c, paramc.b, paramc.d.longValue(), -1L, paramByteBuffer);
      }
    }
  }
  
  public void a(Context paramContext, String paramString, long paramLong, int paramInt)
  {
    if ((!TextUtils.isEmpty(paramString)) && (paramString.equals(cn.jiguang.sdk.impl.a.f)))
    {
      if (paramInt != 26)
      {
        if ((paramInt == 30) || (paramInt == 32)) {
          cn.jiguang.am.a.a().a(paramContext, paramInt);
        }
      }
      else {
        k.a().b(paramContext, paramLong);
      }
    }
    else
    {
      JDispatchAction localJDispatchAction = (JDispatchAction)a.get(paramString);
      if (localJDispatchAction != null)
      {
        localJDispatchAction.dispatchTimeOutMessage(paramContext, paramString, paramLong, paramInt);
      }
      else
      {
        paramContext = new StringBuilder();
        paramContext.append("not found dispatch action by sdktype:");
        paramContext.append(paramString);
        cn.jiguang.ai.a.g("DispatchActionManager", paramContext.toString());
      }
    }
  }
  
  public void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    if (paramBundle == null)
    {
      cn.jiguang.ai.a.h("DispatchActionManager", "run action bundle is null");
      return;
    }
    if (TextUtils.isEmpty(paramString))
    {
      cn.jiguang.ai.a.h("DispatchActionManager", "run action sdktype is empty");
      return;
    }
    String str = paramString;
    if (cn.jiguang.sdk.impl.a.f.contains(paramString)) {
      str = cn.jiguang.sdk.impl.a.f;
    }
    paramString = (JDispatchAction)a.get(str);
    if (paramString == null)
    {
      paramContext = new StringBuilder();
      paramContext.append("dispacth action is null by sdktype:");
      paramContext.append(str);
      cn.jiguang.ai.a.h("DispatchActionManager", paramContext.toString());
      return;
    }
    paramString.onActionRun(paramContext, str, paramBundle.getString("internal_action"), paramBundle);
  }
  
  public void a(String paramString1, String paramString2)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("addAction type:");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(",action:");
    ((StringBuilder)localObject).append(paramString2);
    cn.jiguang.ai.a.c("DispatchActionManager", ((StringBuilder)localObject).toString());
    if (!TextUtils.isEmpty(paramString2))
    {
      if (!a.containsKey(paramString1)) {
        try
        {
          localObject = Class.forName(paramString2).newInstance();
          if ((localObject instanceof JDispatchAction)) {
            a.put(paramString1, (JDispatchAction)localObject);
          } else {
            cn.jiguang.ai.a.g("DispatchActionManager", "this action is not a JDispatchAction,please check and extends JDispatchAction");
          }
        }
        catch (Throwable paramString1)
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("#unexcepted - instance ");
          ((StringBuilder)localObject).append(paramString2);
          ((StringBuilder)localObject).append(" class failed:");
          ((StringBuilder)localObject).append(paramString1);
          cn.jiguang.ai.a.h("DispatchActionManager", ((StringBuilder)localObject).toString());
        }
      }
      cn.jiguang.ai.a.c("DispatchActionManager", "has same type action");
    }
  }
  
  public void a(HashMap<String, String> paramHashMap)
  {
    if ((paramHashMap != null) && (!paramHashMap.isEmpty())) {
      paramHashMap = paramHashMap.entrySet().iterator();
    }
    while (paramHashMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramHashMap.next();
      a((String)localEntry.getKey(), (String)localEntry.getValue());
      continue;
      cn.jiguang.ai.a.g("DispatchActionManager", "init map is empty");
    }
  }
  
  public boolean a(int paramInt)
  {
    Iterator localIterator = a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
      if (localJDispatchAction != null) {
        try
        {
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("isAllowAction actionType:");
          localStringBuilder.append(paramInt);
          localStringBuilder.append(",sdktype:");
          localStringBuilder.append((String)localEntry.getKey());
          localStringBuilder.append(",action:");
          localStringBuilder.append(localJDispatchAction.checkAction((String)localEntry.getKey(), paramInt));
          cn.jiguang.ai.a.d("DispatchActionManager", localStringBuilder.toString());
          boolean bool = localJDispatchAction.checkAction((String)localEntry.getKey(), paramInt);
          if (!bool) {
            return false;
          }
        }
        catch (Throwable localThrowable)
        {
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append("isAllowAction error:");
          localStringBuilder.append(localThrowable.getMessage());
          cn.jiguang.ai.a.g("DispatchActionManager", localStringBuilder.toString());
        }
      }
    }
    return true;
  }
  
  public String b(int paramInt)
  {
    Iterator localIterator = a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
      if ((localJDispatchAction != null) && (localJDispatchAction.getRegPriority((String)localEntry.getKey()) == paramInt)) {
        return localJDispatchAction.getSdkVersion((String)localEntry.getKey());
      }
    }
    return "";
  }
  
  public String b(Context paramContext)
  {
    Iterator localIterator = a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)((Map.Entry)localObject).getValue();
      if (localJDispatchAction != null)
      {
        localObject = localJDispatchAction.beforLogin(paramContext, (String)((Map.Entry)localObject).getKey(), 23, "platformregid");
        if ((localObject instanceof String)) {
          return (String)localObject;
        }
      }
    }
    return "";
  }
  
  public String b(String paramString1, String paramString2)
  {
    Object localObject = (JDispatchAction)a.get(paramString1);
    if (localObject != null)
    {
      localObject = ((JDispatchAction)localObject).getSdkVersion(paramString1);
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        return (String)localObject;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString1);
      localStringBuilder.append(" sdk action sdkversion:");
      localStringBuilder.append((String)localObject);
      cn.jiguang.ai.a.a("DispatchActionManager", localStringBuilder.toString());
    }
    else
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramString1);
      ((StringBuilder)localObject).append(" sdk action is null");
      cn.jiguang.ai.a.a("DispatchActionManager", ((StringBuilder)localObject).toString());
    }
    return paramString2;
  }
  
  public short b()
  {
    Iterator localIterator = a.entrySet().iterator();
    short s1 = 0;
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
      if (localJDispatchAction != null)
      {
        short s2 = localJDispatchAction.getRegFlag((String)localEntry.getKey());
        if (s2 != 0) {
          s1 = (short)(s1 | s2);
        }
      }
    }
    return s1;
  }
  
  public short c()
  {
    Iterator localIterator = a.entrySet().iterator();
    short s1 = 0;
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JDispatchAction localJDispatchAction = (JDispatchAction)localEntry.getValue();
      if (localJDispatchAction != null)
      {
        short s2 = localJDispatchAction.getLoginFlag((String)localEntry.getKey());
        if (s2 != 0) {
          s1 = (short)(s1 | s2);
        }
      }
    }
    return s1;
  }
  
  public String d()
  {
    Object localObject1 = a.entrySet().iterator();
    int i = 3;
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      j = ((JDispatchAction)((Map.Entry)localObject2).getValue()).getRegPriority((String)((Map.Entry)localObject2).getKey());
      if (i < j) {
        i = j;
      }
    }
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("max reg priority:");
    ((StringBuilder)localObject1).append(i);
    cn.jiguang.ai.a.c("DispatchActionManager", ((StringBuilder)localObject1).toString());
    localObject1 = "";
    for (int j = 0; j <= i; j++)
    {
      if (j == 3)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject1 = "2.0.0|";
      }
      for (;;)
      {
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject2).toString();
        break;
        Object localObject3 = a.entrySet().iterator();
        JDispatchAction localJDispatchAction;
        do
        {
          localObject2 = localObject1;
          if (!((Iterator)localObject3).hasNext()) {
            break;
          }
          localObject2 = (Map.Entry)((Iterator)localObject3).next();
          localJDispatchAction = (JDispatchAction)((Map.Entry)localObject2).getValue();
        } while (localJDispatchAction.getRegPriority((String)((Map.Entry)localObject2).getKey()) != j);
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append(localJDispatchAction.getSdkVersion((String)((Map.Entry)localObject2).getKey()));
        localObject2 = ((StringBuilder)localObject3).toString();
        localObject3 = new StringBuilder();
        ((StringBuilder)localObject3).append((String)localObject2);
        localObject1 = "|";
        localObject2 = localObject3;
      }
    }
    return ((String)localObject1).substring(0, ((String)localObject1).length() - 1);
  }
  
  public String e()
  {
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(cn.jiguang.ap.a.a("2.0.0"));
    ((StringBuilder)localObject1).append("|");
    localObject1 = ((StringBuilder)localObject1).toString();
    Object localObject2 = a.entrySet().iterator();
    int i = 0;
    Object localObject3;
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      j = ((JDispatchAction)((Map.Entry)localObject3).getValue()).getLogPriority((String)((Map.Entry)localObject3).getKey());
      if (i < j) {
        i = j;
      }
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("max login priority:");
    ((StringBuilder)localObject2).append(i);
    cn.jiguang.ai.a.c("DispatchActionManager", ((StringBuilder)localObject2).toString());
    for (int j = 1; j <= i; j++)
    {
      Object localObject4 = a.entrySet().iterator();
      do
      {
        localObject2 = localObject1;
        if (!((Iterator)localObject4).hasNext()) {
          break;
        }
        localObject2 = (Map.Entry)((Iterator)localObject4).next();
        localObject3 = (JDispatchAction)((Map.Entry)localObject2).getValue();
      } while (((JDispatchAction)localObject3).getLogPriority((String)((Map.Entry)localObject2).getKey()) != j);
      localObject4 = new StringBuilder();
      ((StringBuilder)localObject4).append((String)localObject1);
      ((StringBuilder)localObject4).append(cn.jiguang.ap.a.a(((JDispatchAction)localObject3).getSdkVersion((String)((Map.Entry)localObject2).getKey())));
      localObject2 = ((StringBuilder)localObject4).toString();
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append("|");
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    return ((String)localObject1).substring(0, ((String)localObject1).length() - 1);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */