package cn.jiguang.ah;

import android.content.Context;
import android.os.Message;
import android.os.SystemClock;
import cn.jiguang.ae.c;

public class h
{
  private static volatile h a;
  private static final Object b = new Object();
  private Context c;
  private cn.jiguang.ao.a d = new cn.jiguang.ao.a()
  {
    public void a(Message paramAnonymousMessage)
    {
      paramAnonymousMessage = new StringBuilder();
      paramAnonymousMessage.append("time is up, next period=");
      paramAnonymousMessage.append(cn.jiguang.sdk.impl.a.d * 1000);
      cn.jiguang.ai.a.c("PeriodWorker", paramAnonymousMessage.toString());
      paramAnonymousMessage = h.this;
      h.a(paramAnonymousMessage, h.a(paramAnonymousMessage));
    }
  };
  private long e;
  
  public static h a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          h localh = new cn/jiguang/ah/h;
          localh.<init>();
          a = localh;
        }
      }
    }
    return a;
  }
  
  private void b(Context paramContext)
  {
    this.e = SystemClock.elapsedRealtime();
    if (((Boolean)c.a(paramContext, cn.jiguang.ae.b.a())).booleanValue()) {
      a.a(paramContext);
    } else {
      a.b(paramContext);
    }
  }
  
  private void c(Context paramContext)
  {
    cn.jiguang.ai.a.c("PeriodWorker", "periodTask...");
    b(paramContext);
    cn.jiguang.sdk.impl.b.a(paramContext, false, 0L);
    b.a().a(paramContext, 19, 0, "periodTask");
    e.a(paramContext, "periodtask", null);
  }
  
  public void a(Context paramContext)
  {
    this.c = paramContext;
    cn.jiguang.ao.b.a().a(8000, cn.jiguang.sdk.impl.a.d * 1000, this.d);
  }
  
  public void a(Context paramContext, boolean paramBoolean)
  {
    cn.jiguang.ai.a.e("PeriodWorker", "PeriodWorker resume");
    if ((this.e > 0L) && (SystemClock.elapsedRealtime() > this.e + (cn.jiguang.sdk.impl.a.d + 5) * 1000))
    {
      cn.jiguang.ai.a.e("PeriodWorker", "schedule time is expired, execute now");
      a(paramContext);
      c(paramContext);
    }
    else if (paramBoolean)
    {
      a(paramContext);
      b(paramContext);
    }
    else
    {
      cn.jiguang.ai.a.c("PeriodWorker", "need not change period task");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */