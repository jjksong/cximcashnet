package cn.jiguang.ah;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ai.a;
import cn.jiguang.ap.g;

public class c
{
  public static void a(Context paramContext)
  {
    String str1 = (String)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.h());
    String str2 = cn.jiguang.sdk.impl.b.i(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("appkey=");
    localStringBuilder.append(str2);
    localStringBuilder.append(" last=");
    localStringBuilder.append(str1);
    a.c("InitHelper", localStringBuilder.toString());
    if ((g.a(str1)) || ("null".equals(str1)) || (!str1.equalsIgnoreCase(str2)))
    {
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.h().a(str2) });
      a.d("InitHelper", "We found the appKey is changed or register appkey is empty. Will re-register.");
      cn.jiguang.sdk.impl.b.k(paramContext);
    }
  }
  
  public static void b(Context paramContext)
  {
    String str = (String)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.g());
    if (((TextUtils.isEmpty(str)) || (str.startsWith("1."))) && ("2.0.0".startsWith("2."))) {
      cn.jiguang.sdk.impl.b.o(paramContext);
    }
    if ((TextUtils.isEmpty(str)) || (!"2.0.0".equals(str))) {
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.g().a("2.0.0") });
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */