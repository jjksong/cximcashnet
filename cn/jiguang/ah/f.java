package cn.jiguang.ah;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.ae.b;
import cn.jiguang.ae.c;
import cn.jiguang.api.JDispatchAction;
import java.nio.ByteBuffer;

public class f
  extends JDispatchAction
{
  private void a(Context paramContext, long paramLong, int paramInt, ByteBuffer paramByteBuffer)
  {
    if (paramInt != 19)
    {
      if ((paramInt != 30) && (paramInt != 32)) {}
      switch (paramInt)
      {
      default: 
        break;
      case 26: 
        paramInt = paramByteBuffer.getShort();
        if (paramInt == 0) {
          k.a().a(paramContext, paramLong);
        } else {
          k.a().a(paramContext, paramLong, paramInt);
        }
        break;
      case 25: 
        Bundle localBundle = new Bundle();
        localBundle.putByteArray("RESPONSE_BODY", paramByteBuffer.array());
        e.a(paramContext, "cmd", localBundle);
        break;
        cn.jiguang.am.a.a().a(paramContext, 0, paramInt);
        break;
      }
    }
    else
    {
      i.a().a(paramContext, "tcp_a18", null);
    }
  }
  
  public void dispatchMessage(Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, ByteBuffer paramByteBuffer)
  {
    try
    {
      a(paramContext, paramLong2, paramInt1, paramByteBuffer);
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("dispatchMessage failed:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JCoreDispatchAction", paramString.toString());
    }
  }
  
  public short getLogPriority(String paramString)
  {
    return 0;
  }
  
  public short getLoginFlag(String paramString)
  {
    return 0;
  }
  
  public short getRegFlag(String paramString)
  {
    return 0;
  }
  
  public short getRegPriority(String paramString)
  {
    return 3;
  }
  
  public String getReportVersionKey(String paramString)
  {
    return "core_sdk_ver";
  }
  
  public String getSdkVersion(String paramString)
  {
    return "2.0.0";
  }
  
  public short getUserCtrlProperty(String paramString)
  {
    return 6;
  }
  
  public void handleMessage(Context paramContext, String paramString, Object paramObject) {}
  
  public boolean isSupportedCMD(String paramString, int paramInt)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (paramInt != 0)
    {
      bool1 = bool2;
      if (paramInt != 1)
      {
        bool1 = bool2;
        if (paramInt != 19)
        {
          bool1 = bool2;
          if (paramInt != 25)
          {
            bool1 = bool2;
            if (paramInt != 26)
            {
              bool1 = bool2;
              if (paramInt != 30) {
                if (paramInt == 32) {
                  bool1 = bool2;
                } else {
                  bool1 = false;
                }
              }
            }
          }
        }
      }
    }
    return bool1;
  }
  
  public void onActionRun(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
  {
    if (paramBundle != null) {
      try
      {
        if (!TextUtils.isEmpty(paramString2)) {
          if (paramString2.equals("asm"))
          {
            cn.jiguang.am.a.a().a(paramContext, paramBundle);
          }
          else if (paramString2.equals("asmr"))
          {
            cn.jiguang.am.a.a().b(paramContext, paramBundle);
          }
          else
          {
            if (paramString2.equals("lbsenable")) {}
            boolean bool;
            do
            {
              do
              {
                e.a(paramContext, paramString2, paramBundle);
                break;
                bool = paramString2.equals("lbsforenry");
                if (bool) {
                  try
                  {
                    long l = paramBundle.getLong("forenry");
                    paramString1 = new java/lang/StringBuilder;
                    paramString1.<init>();
                    paramString1.append("setLbsPermissionDialogShieldDelay=");
                    paramString1.append(l);
                    cn.jiguang.ai.a.c("JCoreDispatchAction", paramString1.toString());
                    paramString2 = b.m();
                    if (l > 0L) {
                      paramString1 = Long.valueOf(l);
                    } else {
                      paramString1 = null;
                    }
                    c.a(paramContext, new b[] { paramString2.a(paramString1) });
                  }
                  catch (Throwable paramContext)
                  {
                    paramString1 = new java/lang/StringBuilder;
                    paramString1.<init>();
                    paramString1.append("onActionRun failed:");
                    paramString1.append(paramContext);
                    cn.jiguang.ai.a.i("JCoreDispatchAction", paramString1.toString());
                  }
                }
              } while (paramString2.equals("notification_state"));
              bool = paramString2.equals("old_cmd");
            } while (bool);
          }
        }
        return;
      }
      catch (Throwable paramContext)
      {
        paramString1 = new StringBuilder();
        paramString1.append("onActionRun failed:");
        paramString1.append(paramContext.getMessage());
        cn.jiguang.ai.a.g("JCoreDispatchAction", paramString1.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ah/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */