package cn.jiguang.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.jiguang.f.c.a;
import cn.jiguang.f.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class b
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile b d;
  private Context a;
  private Set<String> b;
  private List<cn.jiguang.i.a> c;
  
  private void a(String paramString1, int paramInt1, String paramString2, int paramInt2)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("action", paramString2);
      localJSONObject.put("appid", paramString1);
      localJSONObject.put("source", paramInt2);
      if (paramInt1 != 64536) {
        localJSONObject.put("install_type", paramInt1);
      }
      d.a(this.a, localJSONObject, "app_add_rmv");
      d.a(this.a, localJSONObject);
    }
    catch (JSONException paramString2)
    {
      paramString1 = new StringBuilder();
      paramString1.append("package json exception:");
      paramString1.append(paramString2.getMessage());
      cn.jiguang.ai.a.g("JAppMovement", paramString1.toString());
    }
  }
  
  private boolean a(int paramInt, String paramString)
  {
    if (paramInt == 1)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("the ");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" app is system app,need dealAction all apps,to executeMovementAction JAppAll");
      cn.jiguang.ai.a.g("JAppMovement", localStringBuilder.toString());
      return true;
    }
    return false;
  }
  
  private void b(Context paramContext, Intent paramIntent)
  {
    Object localObject = paramIntent.getAction();
    if ((localObject != null) && (!TextUtils.isEmpty((CharSequence)localObject)))
    {
      if ((!((String)localObject).equals("android.intent.action.PACKAGE_ADDED")) && (!((String)localObject).equals("android.intent.action.PACKAGE_REMOVED"))) {
        return;
      }
      paramIntent = paramIntent.getDataString();
      if ((paramIntent != null) && (!TextUtils.isEmpty(paramIntent)) && (paramIntent.length() > 8) && (paramIntent.startsWith("package:")))
      {
        String str = paramIntent.substring(8);
        paramIntent = new StringBuilder();
        paramIntent.append("receive the action'");
        paramIntent.append((String)localObject);
        paramIntent.append(",package:");
        paramIntent.append(str);
        cn.jiguang.ai.a.c("JAppMovement", paramIntent.toString());
        boolean bool = ((String)localObject).equals("android.intent.action.PACKAGE_ADDED");
        int j = 0;
        int i;
        if (bool)
        {
          i = cn.jiguang.j.a.a(cn.jiguang.j.a.a(paramContext, str));
          paramIntent = new StringBuilder();
          paramIntent.append("report add app:");
          paramIntent.append(str);
          cn.jiguang.ai.a.c("JAppMovement", paramIntent.toString());
          paramIntent = "add";
        }
        else
        {
          paramIntent = new StringBuilder();
          paramIntent.append("report remove app:");
          paramIntent.append(str);
          cn.jiguang.ai.a.c("JAppMovement", paramIntent.toString());
          i = 64536;
          paramIntent = "rmv";
        }
        a(str, i, paramIntent, 0);
        this.b = e();
        paramIntent = this.b;
        if ((paramIntent != null) && (!paramIntent.isEmpty()))
        {
          i = j;
          try
          {
            if (((String)localObject).equals("android.intent.action.PACKAGE_ADDED"))
            {
              this.b.add(str);
              i = 1;
            }
            if (((String)localObject).equals("android.intent.action.PACKAGE_REMOVED"))
            {
              this.b.remove(str);
              i = 1;
            }
            if ((i == 0) || (this.b == null)) {
              break label450;
            }
            paramIntent = cn.jiguang.j.a.a(this.b);
            if (TextUtils.isEmpty(paramIntent)) {
              break label450;
            }
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append("update installedAppList cache:");
            ((StringBuilder)localObject).append(this.b);
            cn.jiguang.ai.a.c("JAppMovement", ((StringBuilder)localObject).toString());
            cn.jiguang.s.b.a(paramContext, "bal.catch", paramIntent);
          }
          catch (Throwable paramIntent)
          {
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append("cache appList add remove failed:");
            ((StringBuilder)localObject).append(paramIntent.getMessage());
            cn.jiguang.ai.a.g("JAppMovement", ((StringBuilder)localObject).toString());
          }
        }
        cn.jiguang.ai.a.g("JAppMovement", "get cache appList failed");
        label450:
        cn.jiguang.ai.a.c("JAppMovement", "executeAction: [JAppMovement]");
        c(paramContext, "JAppMovement");
        d(paramContext, "JAppMovement");
      }
      return;
    }
    paramContext = new StringBuilder();
    paramContext.append("the action'");
    paramContext.append((String)localObject);
    paramContext.append("'is illegal");
    cn.jiguang.ai.a.g("JAppMovement", paramContext.toString());
  }
  
  public static b d()
  {
    if (d == null) {
      try
      {
        b localb = new cn/jiguang/h/b;
        localb.<init>();
        d = localb;
      }
      finally {}
    }
    return d;
  }
  
  private Set<String> e()
  {
    String str = cn.jiguang.s.b.c(this.a, "bal.catch");
    boolean bool = TextUtils.isEmpty(str);
    Object localObject2 = null;
    if (bool) {
      return null;
    }
    Object localObject1 = localObject2;
    if (str != null)
    {
      localObject1 = localObject2;
      if (!TextUtils.isEmpty(str)) {
        localObject1 = cn.jiguang.j.a.a(str);
      }
    }
    return (Set<String>)localObject1;
  }
  
  public void a(Context paramContext, Intent paramIntent)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeMovementAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from broadcast");
    cn.jiguang.ai.a.c("JAppMovement", localStringBuilder.toString());
    if (a()) {
      d.a("JCommon", new a(paramContext, paramIntent));
    }
  }
  
  protected boolean a()
  {
    boolean bool = c.a.b;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("for googlePlay:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JAppMovement", localStringBuilder.toString());
    return bool ^ true;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    paramString = cn.jiguang.j.a.a(paramContext, true);
    if ((paramString != null) && (!paramString.isEmpty()))
    {
      cn.jiguang.ai.a.c("JAppMovement", "collect installedAppList success");
      if ((paramString.size() == 1) && (((cn.jiguang.i.a)paramString.get(0)).b.equals(paramContext.getPackageName())))
      {
        cn.jiguang.ai.a.g("JAppMovement", "installedAppList only has one app and this app is itself");
        return;
      }
      if (this.b == null) {
        this.b = e();
      }
      Object localObject1 = this.b;
      if ((localObject1 != null) && (!((Set)localObject1).isEmpty()))
      {
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("get installedAppList cache:");
        ((StringBuilder)localObject1).append(this.b);
        cn.jiguang.ai.a.c("JAppMovement", ((StringBuilder)localObject1).toString());
        this.c = new ArrayList(paramString);
        Object localObject2 = paramString.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject1 = (cn.jiguang.i.a)((Iterator)localObject2).next();
          if (this.b.remove(((cn.jiguang.i.a)localObject1).b)) {
            this.c.remove(localObject1);
          }
        }
        if ((this.b.isEmpty()) && (this.c.isEmpty()))
        {
          cn.jiguang.ai.a.c("JAppMovement", "installedAppList has no change");
        }
        else
        {
          localObject2 = cn.jiguang.j.a.a(paramString);
          if (!TextUtils.isEmpty((CharSequence)localObject2))
          {
            localObject1 = new StringBuilder();
            ((StringBuilder)localObject1).append("update installedAppList cache:");
            ((StringBuilder)localObject1).append(paramString);
            cn.jiguang.ai.a.c("JAppMovement", ((StringBuilder)localObject1).toString());
            cn.jiguang.s.b.a(paramContext, "bal.catch", (String)localObject2);
          }
        }
        return;
      }
      cn.jiguang.ai.a.g("JAppMovement", "current appList cache is empty,need collect appList first,to executeMovementAction JAppAll");
      a.d().b(paramContext);
      return;
    }
    cn.jiguang.ai.a.g("JAppMovement", "collect installedAppList failed");
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JAppMovement";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    Object localObject1 = this.c;
    if ((localObject1 != null) && (!((List)localObject1).isEmpty())) {
      localObject1 = this.c.iterator();
    }
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (cn.jiguang.i.a)((Iterator)localObject1).next();
      if (!a(((cn.jiguang.i.a)localObject2).e, "add"))
      {
        a(((cn.jiguang.i.a)localObject2).b, ((cn.jiguang.i.a)localObject2).e, "add", 1);
        super.d(paramContext, paramString);
      }
      else
      {
        a.d().b(paramContext);
        continue;
        cn.jiguang.ai.a.g("JAppMovement", "there are no add app data to report");
      }
    }
    localObject1 = this.b;
    if ((localObject1 != null) && (!((Set)localObject1).isEmpty())) {
      localObject2 = this.b.iterator();
    }
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (String)((Iterator)localObject2).next();
      if (!a(64536, "rmv"))
      {
        a((String)localObject1, 64536, "rmv", 1);
        super.d(paramContext, paramString);
      }
      else
      {
        a.d().b(paramContext);
        continue;
        cn.jiguang.ai.a.g("JAppMovement", "there are no remove app data to report");
      }
    }
    this.c = null;
    this.b = null;
  }
  
  public class a
    implements Runnable
  {
    private Context b;
    private Intent c;
    
    a(Context paramContext, Intent paramIntent)
    {
      this.b = paramContext;
      this.c = paramIntent;
    }
    
    public void run()
    {
      try
      {
        b.a(b.this, this.b, this.c);
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("dealMovementAction throwable:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JAppMovement", localStringBuilder.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/h/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */