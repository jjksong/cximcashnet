package cn.jiguang.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import cn.jiguang.f.c.a;
import cn.jiguang.f.d;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class c
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile c c;
  private Context a;
  private String[] b;
  
  public static c d()
  {
    if (c == null) {
      try
      {
        c localc = new cn/jiguang/h/c;
        localc.<init>();
        c = localc;
      }
      finally {}
    }
    return c;
  }
  
  protected boolean a()
  {
    boolean bool = c.a.b;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("for googlePlay:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JAppPermission", localStringBuilder.toString());
    return bool ^ true;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    try
    {
      this.b = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 4096).requestedPermissions;
      if ((this.b != null) && (this.b.length > 0))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("collect success:");
        paramContext.append(Arrays.toString(this.b));
      }
      for (paramContext = paramContext.toString();; paramContext = "collect failed")
      {
        cn.jiguang.ai.a.c("JAppPermission", paramContext);
        break;
      }
      return;
    }
    catch (Throwable paramContext)
    {
      this.b = null;
      paramString = new StringBuilder();
      paramString.append("collect throwable:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JAppPermission", paramString.toString());
    }
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JAppPermission";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    Object localObject1 = this.b;
    if ((localObject1 != null) && (localObject1.length != 0))
    {
      int n = localObject1.length;
      localObject1 = new StringBuilder("[");
      String str2 = d.b(paramContext);
      long l = d.c(paramContext);
      int k = 0;
      int i = 0;
      int j = 0;
      while (k < n)
      {
        Object localObject2 = this.b[k];
        String str1;
        if (i == 0) {
          str1 = "\"";
        } else {
          str1 = ",\"";
        }
        ((StringBuilder)localObject1).append(str1);
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("\"");
        int m = k + 1;
        i++;
        if ((i < 50) && (((StringBuilder)localObject1).length() <= 1000))
        {
          k = m;
          if (m != n) {}
        }
        else
        {
          ((StringBuilder)localObject1).append("]");
          str1 = String.format(Locale.ENGLISH, "{\"total\":%d,\"page\":%d,\"senderid\":\"%s\",\"uid\":%s,\"permission_list\":%s}", new Object[] { Integer.valueOf(n), Integer.valueOf(j), str2, Long.valueOf(l), ((StringBuilder)localObject1).toString() });
          localObject1 = new JSONObject();
          try
          {
            ((JSONObject)localObject1).put("data", str1);
          }
          catch (JSONException localJSONException)
          {
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append("package json exception:");
            ((StringBuilder)localObject2).append(localJSONException.getMessage());
            cn.jiguang.ai.a.g("JAppPermission", ((StringBuilder)localObject2).toString());
          }
          d.a(paramContext, (JSONObject)localObject1, "android_permissions");
          d.a(paramContext, localObject1);
          super.d(paramContext, paramString);
          j++;
          localObject1 = new StringBuilder("[");
          i = 0;
          k = m;
        }
      }
      this.b = null;
      return;
    }
    cn.jiguang.ai.a.g("JAppPermission", "there are no data to report");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/h/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */