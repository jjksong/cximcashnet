package cn.jiguang.h;

import android.annotation.SuppressLint;
import android.content.Context;
import cn.jiguang.f.c.a;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile d d;
  private Context a;
  private List<cn.jiguang.i.a> b;
  private List<cn.jiguang.i.b> c;
  
  public static d d()
  {
    if (d == null) {
      try
      {
        d locald = new cn/jiguang/h/d;
        locald.<init>();
        d = locald;
      }
      finally {}
    }
    return d;
  }
  
  protected void a(String paramString, JSONObject paramJSONObject)
  {
    paramString = paramJSONObject.optJSONObject("content");
    try
    {
      long l = paramString.getLong("interval");
      if (l == -1L)
      {
        cn.jiguang.f.b.a(this.a, "JAppRunning", false);
      }
      else if (l == 0L)
      {
        cn.jiguang.f.b.a(this.a, "JAppRunning", true);
      }
      else
      {
        int j = paramString.optInt("app_type", 0);
        int i = paramString.optInt("process_type", 0);
        cn.jiguang.f.b.a(this.a, "JAppRunning", true);
        cn.jiguang.f.b.a(this.a, j);
        cn.jiguang.f.b.b(this.a, i);
        if (l > 0L) {
          cn.jiguang.f.b.b(this.a, "JAppRunning", l);
        }
      }
    }
    catch (JSONException paramJSONObject)
    {
      paramString = new StringBuilder();
      paramString.append("parse interval exception:");
      paramString.append(paramJSONObject.getMessage());
      cn.jiguang.ai.a.g("JAppRunning", paramString.toString());
    }
  }
  
  protected boolean a()
  {
    boolean bool = c.a.b;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("for googlePlay:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JAppRunning", localStringBuilder.toString());
    return bool ^ true;
  }
  
  protected boolean b()
  {
    return cn.jiguang.f.b.i(this.a, "JAppRunning");
  }
  
  protected void c(Context paramContext, String paramString)
  {
    int i = cn.jiguang.f.b.a(paramContext);
    int j = cn.jiguang.f.b.b(paramContext);
    if (i > 0)
    {
      this.b = cn.jiguang.j.a.a(paramContext);
    }
    else
    {
      paramContext = new StringBuilder();
      paramContext.append("can't collect runningApp because reportRunningAppType:");
      paramContext.append(i);
      cn.jiguang.ai.a.g("JAppRunning", paramContext.toString());
    }
    if (j > 0)
    {
      this.c = cn.jiguang.j.b.a(j);
    }
    else
    {
      paramContext = new StringBuilder();
      paramContext.append("can't collect runningProcess because reportRunningProcessType:");
      paramContext.append(j);
      cn.jiguang.ai.a.g("JAppRunning", paramContext.toString());
    }
    paramContext = this.b;
    if ((paramContext != null) && (!paramContext.isEmpty())) {
      cn.jiguang.ai.a.c("JAppRunning", "collect runningAppList success");
    }
    paramContext = this.c;
    if ((paramContext != null) && (!paramContext.isEmpty())) {
      cn.jiguang.ai.a.c("JAppRunning", "collect runningProcessList success");
    }
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JAppRunning";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    Object localObject1 = this.b;
    if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
    {
      localObject2 = new JSONArray();
      localObject3 = this.b.iterator();
      for (;;)
      {
        localObject1 = localObject2;
        if (!((Iterator)localObject3).hasNext()) {
          break;
        }
        localObject1 = ((cn.jiguang.i.a)((Iterator)localObject3).next()).a(128);
        if (localObject1 != null) {
          ((JSONArray)localObject2).put(localObject1);
        }
      }
    }
    cn.jiguang.ai.a.g("JAppRunning", "there are no running app to report");
    localObject1 = null;
    Object localObject2 = this.c;
    if ((localObject2 != null) && (!((List)localObject2).isEmpty()))
    {
      localObject2 = new JSONArray();
      Iterator localIterator = this.c.iterator();
      for (;;)
      {
        localObject3 = localObject2;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject3 = ((cn.jiguang.i.b)localIterator.next()).a(128);
        if (localObject3 != null) {
          ((JSONArray)localObject2).put(localObject3);
        }
      }
    }
    cn.jiguang.ai.a.g("JAppRunning", "there are no running process to report");
    Object localObject3 = null;
    this.b = null;
    this.c = null;
    int m = 0;
    int j = 0;
    int k = 0;
    try
    {
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      int i = m;
      if (localObject1 != null)
      {
        j = k;
        i = m;
        try
        {
          if (((JSONArray)localObject1).length() > 0)
          {
            j = k;
            ((JSONObject)localObject2).put("app", localObject1);
            i = 1;
          }
        }
        catch (JSONException localJSONException1)
        {
          localObject1 = localObject2;
          break label336;
        }
      }
      k = i;
      localObject1 = localObject2;
      if (localJSONException1 != null)
      {
        j = i;
        k = i;
        localObject1 = localObject2;
        if (localJSONException1.length() > 0)
        {
          j = i;
          ((JSONObject)localObject2).put("process", localJSONException1);
          k = 1;
          localObject1 = localObject2;
        }
      }
    }
    catch (JSONException localJSONException2)
    {
      localObject1 = null;
      label336:
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("package json exception:");
      ((StringBuilder)localObject2).append(localJSONException2.getMessage());
      cn.jiguang.ai.a.g("JAppRunning", ((StringBuilder)localObject2).toString());
      k = j;
    }
    if (k != 0)
    {
      cn.jiguang.f.d.a(paramContext, (JSONObject)localObject1, "app_running");
      cn.jiguang.f.d.a(paramContext, localObject1);
      super.d(paramContext, paramString);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/h/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */