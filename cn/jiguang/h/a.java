package cn.jiguang.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.f.c.a;
import cn.jiguang.f.d;
import cn.jiguang.s.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  extends cn.jiguang.f.a
{
  @SuppressLint({"StaticFieldLeak"})
  private static volatile a c;
  private Context a;
  private List<cn.jiguang.i.a> b;
  
  private JSONArray a(List<cn.jiguang.i.a> paramList)
  {
    JSONArray localJSONArray = new JSONArray();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      paramList = (cn.jiguang.i.a)localIterator.next();
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("name", paramList.a);
      localJSONObject.put("pkg", paramList.b);
      localJSONObject.put("ver_name", paramList.c);
      localJSONObject.put("ver_code", paramList.d);
      localJSONObject.put("install_type", paramList.e);
      localJSONArray.put(localJSONObject);
    }
    return localJSONArray;
  }
  
  public static a d()
  {
    if (c == null) {
      try
      {
        a locala = new cn/jiguang/h/a;
        locala.<init>();
        c = locala;
      }
      finally {}
    }
    return c;
  }
  
  protected boolean a()
  {
    boolean bool = c.a.b;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("for googlePlay:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JAppAll", localStringBuilder.toString());
    return bool ^ true;
  }
  
  protected void c(Context paramContext, String paramString)
  {
    this.b = cn.jiguang.j.a.a(paramContext, true);
    paramString = this.b;
    if ((paramString != null) && (!paramString.isEmpty()))
    {
      cn.jiguang.ai.a.c("JAppAll", "collect success");
      String str = cn.jiguang.j.a.a(this.b);
      if (!TextUtils.isEmpty(str))
      {
        paramString = new StringBuilder();
        paramString.append("save appList [");
        paramString.append(str);
        paramString.append("]");
        cn.jiguang.ai.a.c("JAppAll", paramString.toString());
        b.d(paramContext, "bal.catch");
        b.a(paramContext, "bal.catch", str);
      }
      return;
    }
    cn.jiguang.ai.a.g("JAppAll", "collect failed, installedAppList is empty");
  }
  
  protected String d(Context paramContext)
  {
    this.a = paramContext;
    return "JAppAll";
  }
  
  protected void d(Context paramContext, String paramString)
  {
    try
    {
      JSONArray localJSONArray;
      ArrayList localArrayList;
      int i;
      int j;
      if ((this.b != null) && (!this.b.isEmpty()))
      {
        localJSONArray = a(this.b);
        if ((localJSONArray != null) && (localJSONArray.length() != 0))
        {
          localArrayList = cn.jiguang.j.a.a(localJSONArray);
          if ((localArrayList != null) && (!localArrayList.isEmpty()))
          {
            i = 0;
            j = localArrayList.size();
          }
        }
      }
      while (i < j)
      {
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        localJSONArray = (JSONArray)localArrayList.get(i);
        i++;
        localJSONObject.put("slice_index", i);
        localJSONObject.put("slice_count", j);
        localJSONObject.put("data", localJSONArray);
        d.a(paramContext, localJSONObject, "app_list");
        d.a(paramContext, localJSONObject);
        super.d(paramContext, paramString);
        continue;
        return;
        cn.jiguang.ai.a.g("JAppAll", "there are no data to report");
        return;
      }
    }
    catch (JSONException paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("package json exception:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JAppAll", paramString.toString());
      this.b = null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/h/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */