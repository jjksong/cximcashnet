package cn.jiguang.z;

import android.content.Intent;

public class c
{
  public String a;
  public String b;
  public int c;
  public String d;
  public Intent e;
  
  public c() {}
  
  public c(String paramString1, String paramString2, int paramInt)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramInt;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JWakeTargetInfo{packageName='");
    localStringBuilder.append(this.a);
    localStringBuilder.append('\'');
    localStringBuilder.append(", serviceName='");
    localStringBuilder.append(this.b);
    localStringBuilder.append('\'');
    localStringBuilder.append(", targetVersion=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", providerAuthority='");
    localStringBuilder.append(this.d);
    localStringBuilder.append('\'');
    localStringBuilder.append(", dActivityIntent=");
    localStringBuilder.append(this.e);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/z/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */