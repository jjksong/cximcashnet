package cn.jiguang.al;

import android.content.Context;
import android.util.Base64;
import cn.jiguang.ap.e;
import cn.jiguang.ap.g;
import cn.jiguang.sdk.impl.b;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class a
{
  private static Integer a;
  
  public static int a()
  {
    Integer localInteger = a;
    if (localInteger != null) {
      return localInteger.intValue();
    }
    a = Integer.valueOf(Math.abs(new SecureRandom().nextInt()));
    return a.intValue();
  }
  
  public static String a(long paramLong)
  {
    long l1;
    long l2;
    switch ((int)(paramLong % 10L))
    {
    default: 
      l1 = 8L * paramLong;
      l2 = 74L;
    }
    for (;;)
    {
      break;
      l1 = 37L * paramLong;
      l2 = 91L;
      continue;
      l1 = 29L * paramLong;
      l2 = 41L;
      continue;
      l1 = 31L * paramLong;
      l2 = 39L;
      continue;
      l1 = 7L * paramLong;
      l2 = 68L;
      continue;
      l1 = 17L * paramLong;
      l2 = 49L;
      continue;
      l1 = 13L * paramLong;
      l2 = 96L;
      continue;
      l1 = 3L * paramLong;
      l2 = 73L;
      continue;
      l1 = 23L * paramLong;
      l2 = 15L;
      continue;
      l1 = 5L * paramLong;
      l2 = 88L;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JCKP");
    localStringBuilder.append(l1 + paramLong % l2);
    return g.c(localStringBuilder.toString());
  }
  
  public static String a(Context paramContext)
  {
    long l = b.e(paramContext);
    if (l > 0L) {
      return a(l);
    }
    return a(a());
  }
  
  public static String a(String paramString)
  {
    String str = "";
    try
    {
      paramString = b(paramString, "DFA84B10B7ACDD25");
    }
    catch (Exception paramString)
    {
      cn.jiguang.ai.a.g("", "Unexpected - failed to AES encrypt.");
      paramString = str;
    }
    return paramString;
  }
  
  public static String a(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    try
    {
      if (paramString2.length() != 16) {
        return null;
      }
      byte[] arrayOfByte = d(paramString2, "ASCII");
      paramString2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
      SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
      localSecretKeySpec.<init>(arrayOfByte, "AES");
      paramString2.init(2, localSecretKeySpec, a(arrayOfByte));
      paramString1 = Base64.decode(paramString1, 2);
      paramString1 = new String(paramString2.doFinal(paramString1));
      return paramString1;
    }
    catch (Exception paramString1) {}
    return null;
  }
  
  private static IvParameterSpec a(byte[] paramArrayOfByte)
  {
    try
    {
      paramArrayOfByte = (IvParameterSpec)e.a(IvParameterSpec.class, new Object[] { paramArrayOfByte }, new Class[] { byte[].class });
      return paramArrayOfByte;
    }
    catch (Exception paramArrayOfByte)
    {
      paramArrayOfByte.printStackTrace();
    }
    return null;
  }
  
  public static byte[] a(String paramString, byte[] paramArrayOfByte)
  {
    Object localObject = d(paramString, "utf-8");
    paramString = paramString.substring(0, 16);
    localObject = new SecretKeySpec((byte[])localObject, "AES");
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    localCipher.init(1, (Key)localObject, a(paramString.getBytes("utf-8")));
    return localCipher.doFinal(paramArrayOfByte);
  }
  
  public static int b()
  {
    return Math.abs(new SecureRandom().nextInt()) & 0xFFFFFF;
  }
  
  public static String b(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    if (paramString2.length() != 16) {
      return null;
    }
    paramString2 = d(paramString2, "ASCII");
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    localCipher.init(1, new SecretKeySpec(paramString2, "AES"), a(paramString2));
    return Base64.encodeToString(localCipher.doFinal(paramString1.getBytes()), 2);
  }
  
  public static byte[] b(String paramString, byte[] paramArrayOfByte)
  {
    Object localObject = d(paramString, "utf-8");
    paramString = paramString.substring(0, 16);
    SecretKeySpec localSecretKeySpec = new SecretKeySpec((byte[])localObject, "AES");
    localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    ((Cipher)localObject).init(2, localSecretKeySpec, a(paramString.getBytes("utf-8")));
    return ((Cipher)localObject).doFinal(paramArrayOfByte);
  }
  
  public static String c(String paramString1, String paramString2)
  {
    try
    {
      paramString1 = a(paramString1, "DFA84B10B7ACDD25");
      return paramString1;
    }
    catch (Exception paramString1)
    {
      cn.jiguang.ai.a.g("", "Unexpected - failed to AES decrypt.");
    }
    return paramString2;
  }
  
  private static byte[] d(String paramString1, String paramString2)
  {
    byte[] arrayOfByte1 = new byte[paramString1.length()];
    byte[] arrayOfByte2 = paramString1.substring(0, paramString1.length() / 2).getBytes(paramString2);
    paramString1 = paramString1.substring(paramString1.length() / 2).getBytes(paramString2);
    System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, arrayOfByte2.length);
    System.arraycopy(paramString1, 0, arrayOfByte1, arrayOfByte2.length, paramString1.length);
    return arrayOfByte1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/al/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */