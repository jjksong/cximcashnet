package cn.jiguang.q;

import android.text.TextUtils;
import org.json.JSONObject;

public class b
{
  public String a;
  public String b;
  public String c;
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      String str;
      if (TextUtils.isEmpty(this.a)) {
        str = "";
      } else {
        str = this.a;
      }
      localJSONObject.put("imei", str);
      if (TextUtils.isEmpty(this.c)) {
        str = "";
      } else {
        str = this.c;
      }
      localJSONObject.put("iccid", str);
      if (TextUtils.isEmpty(this.b)) {
        str = "";
      } else {
        str = this.b;
      }
      localJSONObject.put("imsi", str);
      return localJSONObject;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  public boolean b()
  {
    boolean bool;
    if ((TextUtils.isEmpty(this.a)) && (TextUtils.isEmpty(this.b))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("JDeviceSimInfo{imei='");
    localStringBuilder.append(this.a);
    localStringBuilder.append('\'');
    localStringBuilder.append(", imsi='");
    localStringBuilder.append(this.b);
    localStringBuilder.append('\'');
    localStringBuilder.append(", iccid='");
    localStringBuilder.append(this.c);
    localStringBuilder.append('\'');
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/q/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */