package cn.jiguang.f;

import android.content.Context;
import android.content.pm.ComponentInfo;
import android.os.Bundle;
import cn.jiguang.ap.c;
import cn.jiguang.ap.g;
import cn.jiguang.ap.h;
import cn.jiguang.ap.j;
import cn.jiguang.api.JCoreManager;
import java.io.File;
import java.io.InputStream;
import org.json.JSONObject;

public class d
{
  public static long a(Context paramContext, long paramLong)
  {
    return cn.jiguang.sdk.impl.b.a(paramContext, paramLong);
  }
  
  public static ComponentInfo a(Context paramContext, String paramString, Class<?> paramClass)
  {
    return cn.jiguang.ap.a.a(paramContext, paramString, paramClass);
  }
  
  public static String a()
  {
    return "2.0.0";
  }
  
  public static String a(int paramInt)
  {
    return h.a(paramInt);
  }
  
  public static String a(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.d(paramContext);
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    return h.a(paramContext, paramInt);
  }
  
  public static String a(File paramFile)
  {
    return c.c(paramFile);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    return cn.jiguang.al.a.c(paramString1, paramString2);
  }
  
  public static JSONObject a(Context paramContext, JSONObject paramJSONObject, String paramString)
  {
    paramContext = JCoreManager.onEvent(paramContext, "JCOMMON", 26, null, null, new Object[] { paramJSONObject, paramString });
    if ((paramContext instanceof JSONObject)) {
      return (JSONObject)paramContext;
    }
    return paramJSONObject;
  }
  
  public static void a(Context paramContext, Bundle paramBundle)
  {
    JCoreManager.onEvent(paramContext, "JCOMMON", 16, null, paramBundle, new Object[0]);
  }
  
  public static void a(Context paramContext, Object paramObject)
  {
    JCoreManager.onEvent(paramContext, "JCOMMON", 39, null, null, new Object[] { paramObject });
  }
  
  public static void a(Context paramContext, Object paramObject1, Object paramObject2)
  {
    JCoreManager.onEvent(paramContext, "JCOMMON", 15, null, null, new Object[] { paramObject1, paramObject2 });
  }
  
  public static void a(Context paramContext, JSONObject paramJSONObject)
  {
    try
    {
      paramJSONObject = paramJSONObject.getJSONObject("content");
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("jsonContent:");
      localStringBuilder.append(paramJSONObject);
      cn.jiguang.ai.a.c("JCommonPresenter", localStringBuilder.toString());
      int i = paramJSONObject.optInt("state", -1);
      if (i == -1)
      {
        cn.jiguang.ai.a.g("JCommonPresenter", "unknow state");
        return;
      }
      if (i == 0)
      {
        cn.jiguang.ai.a.c("JCommonPresenter", "turn on share process");
        cn.jiguang.sdk.impl.b.a(paramContext, i);
      }
      else if (i == 1)
      {
        cn.jiguang.ai.a.c("JCommonPresenter", "turn off share process");
        cn.jiguang.sdk.impl.b.a(paramContext, i);
        cn.jiguang.am.a.a().a(paramContext, cn.jiguang.sdk.impl.b.e(paramContext));
        cn.jiguang.am.a.a(paramContext, paramContext.getPackageName());
      }
      else
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("#exception - unsupport state:");
        paramContext.append(i);
        cn.jiguang.ai.a.g("JCommonPresenter", paramContext.toString());
      }
    }
    catch (Exception paramContext)
    {
      paramJSONObject = new StringBuilder();
      paramJSONObject.append("configReportRunningApp exception:");
      paramJSONObject.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JCommonPresenter", paramJSONObject.toString());
    }
  }
  
  public static void a(String paramString)
  {
    cn.jiguang.sdk.impl.b.a(paramString);
  }
  
  public static void a(String paramString, Runnable paramRunnable)
  {
    cn.jiguang.sdk.impl.b.a(paramString, paramRunnable, new int[0]);
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    return cn.jiguang.ap.a.a(paramContext, paramString);
  }
  
  public static boolean a(File paramFile, String paramString)
  {
    return c.a(paramFile, paramString);
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    return j.b(paramInputStream);
  }
  
  public static int b()
  {
    return 200;
  }
  
  public static String b(int paramInt)
  {
    return cn.jiguang.ah.b.a().b(paramInt);
  }
  
  public static String b(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.i(paramContext);
  }
  
  public static String b(String paramString)
  {
    return g.d(paramString);
  }
  
  public static String b(String paramString1, String paramString2)
  {
    return cn.jiguang.al.a.a(paramString1, paramString2);
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    return cn.jiguang.ap.a.g(paramContext, paramString);
  }
  
  public static long c(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.e(paramContext);
  }
  
  public static String c()
  {
    return cn.jiguang.ap.a.c();
  }
  
  public static String c(Context paramContext, String paramString)
  {
    return cn.jiguang.ap.a.e(paramContext, paramString);
  }
  
  public static String c(String paramString)
  {
    return g.c(paramString);
  }
  
  public static long d()
  {
    return cn.jiguang.ah.b.a().b();
  }
  
  public static String d(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.g(paramContext);
  }
  
  public static String d(Context paramContext, String paramString)
  {
    return cn.jiguang.ap.a.d(paramContext, paramString);
  }
  
  public static String d(String paramString)
  {
    return g.j(paramString);
  }
  
  public static String e(Context paramContext, String paramString)
  {
    return cn.jiguang.ap.a.c(paramContext, paramString);
  }
  
  public static String e(String paramString)
  {
    return g.b(paramString);
  }
  
  public static boolean e(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.h(paramContext);
  }
  
  public static File f(Context paramContext, String paramString)
  {
    return c.a(paramContext, paramString);
  }
  
  public static String f(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.f(paramContext);
  }
  
  public static byte[] f(String paramString)
  {
    return g.i(paramString);
  }
  
  public static String g(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.b(paramContext);
  }
  
  public static String g(String paramString)
  {
    return cn.jiguang.al.a.a(paramString);
  }
  
  public static long h(Context paramContext)
  {
    return cn.jiguang.sdk.impl.b.j(paramContext);
  }
  
  public static int i(Context paramContext)
  {
    return ((Integer)JCoreManager.onEvent(paramContext, "JCOMMON", 33, null, null, new Object[0])).intValue();
  }
  
  public static boolean j(Context paramContext)
  {
    return cn.jiguang.ap.a.i(paramContext);
  }
  
  public static String k(Context paramContext)
  {
    return cn.jiguang.ap.a.j(paramContext);
  }
  
  public static boolean l(Context paramContext)
  {
    return cn.jiguang.ap.a.c(paramContext);
  }
  
  public static byte m(Context paramContext)
  {
    return cn.jiguang.ah.b.a().a(paramContext);
  }
  
  public static String n(Context paramContext)
  {
    return cn.jiguang.ah.b.a().b(paramContext);
  }
  
  public static boolean o(Context paramContext)
  {
    paramContext = JCoreManager.onEvent(paramContext, "JCOMMON", 49, null, null, new Object[0]);
    boolean bool;
    if ((paramContext instanceof Boolean)) {
      bool = ((Boolean)paramContext).booleanValue();
    } else {
      bool = true;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/f/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */