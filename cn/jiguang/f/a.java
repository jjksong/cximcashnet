package cn.jiguang.f;

import android.content.Context;
import android.os.Bundle;
import org.json.JSONObject;

public abstract class a
{
  private void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    a(paramString, paramBundle);
    boolean bool = c();
    paramBundle = new StringBuilder();
    paramBundle.append(paramString);
    paramBundle.append(" isActionBundleEnable:");
    paramBundle.append(bool);
    cn.jiguang.ai.a.c("JCommon", paramBundle.toString());
    if (bool)
    {
      c(paramContext, paramString);
      d(paramContext, paramString);
    }
  }
  
  private void a(Context paramContext, String paramString, JSONObject paramJSONObject)
  {
    a(paramString, paramJSONObject);
    boolean bool = b();
    paramJSONObject = new StringBuilder();
    paramJSONObject.append(paramString);
    paramJSONObject.append(" isActionCommandEnable:");
    paramJSONObject.append(bool);
    cn.jiguang.ai.a.c("JCommon", paramJSONObject.toString());
    if (bool)
    {
      c(paramContext, paramString);
      d(paramContext, paramString);
    }
  }
  
  private boolean a(String paramString)
  {
    boolean bool3 = a();
    boolean bool2 = b();
    boolean bool1;
    if ((bool3) && (bool2)) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" isActionEnable:");
    localStringBuilder.append(bool1);
    localStringBuilder.append(",actionUserEnable:");
    localStringBuilder.append(bool3);
    localStringBuilder.append(",actionCommandEnable:");
    localStringBuilder.append(bool2);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    return bool1;
  }
  
  private void e(Context paramContext, String paramString)
  {
    boolean bool = a(paramContext, paramString);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" isBusinessEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (bool) {
      c(paramContext, paramString);
    }
    bool = b(paramContext, paramString);
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" isReportEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (bool) {
      d(paramContext, paramString);
    }
  }
  
  private void f(Context paramContext, String paramString)
  {
    c(paramContext, paramString);
    d(paramContext, paramString);
  }
  
  public Object a(Context paramContext, Object paramObject)
  {
    return null;
  }
  
  public void a(Context paramContext)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from heartBeat");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (a(str)) {
      d.a("JCommon", new c(paramContext, str));
    }
  }
  
  public void a(Context paramContext, Bundle paramBundle)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeBundleAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from bundle");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    boolean bool = a();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append(" isActionUserEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (bool) {
      d.a("JCommon", new a(paramContext, str, paramBundle));
    }
  }
  
  public void a(Context paramContext, JSONObject paramJSONObject)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeJsonAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from cmd");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    boolean bool = a();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append(" isActionUserEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (bool) {
      d.a("JCommon", new d(paramContext, str, paramJSONObject));
    }
  }
  
  protected void a(String paramString, Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" parseJson:");
      localStringBuilder.append(paramBundle.toString());
      cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    }
  }
  
  protected void a(String paramString, JSONObject paramJSONObject) {}
  
  protected boolean a()
  {
    return true;
  }
  
  protected boolean a(Context paramContext, String paramString)
  {
    return b.b(paramContext, paramString);
  }
  
  public void b(Context paramContext)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeCommandAction: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from cmd");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (a(str)) {
      d.a("JCommon", new b(paramContext, str));
    }
  }
  
  public void b(Context paramContext, JSONObject paramJSONObject)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeCommandActionSingle: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from cmd");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    boolean bool = a();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append(" isActionUserEnable:");
    localStringBuilder.append(bool);
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (bool) {
      d.a(str, new d(paramContext, str, paramJSONObject));
    }
  }
  
  protected boolean b()
  {
    return true;
  }
  
  protected boolean b(Context paramContext, String paramString)
  {
    return b.b(paramContext, paramString);
  }
  
  public void c(Context paramContext)
  {
    String str = d(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("executeActionSingle: [");
    localStringBuilder.append(str);
    localStringBuilder.append("] from heartBeat");
    cn.jiguang.ai.a.c("JCommon", localStringBuilder.toString());
    if (a(str)) {
      d.a(str, new c(paramContext, str));
    }
  }
  
  protected void c(Context paramContext, String paramString)
  {
    b.c(paramContext, paramString);
  }
  
  protected boolean c()
  {
    return true;
  }
  
  protected abstract String d(Context paramContext);
  
  protected void d(Context paramContext, String paramString)
  {
    b.f(paramContext, paramString);
  }
  
  public Object e(Context paramContext)
  {
    return null;
  }
  
  public class a
    implements Runnable
  {
    private Context b;
    private String c;
    private Bundle d;
    
    a(Context paramContext, String paramString, Bundle paramBundle)
    {
      this.b = paramContext;
      this.c = paramString;
      this.d = paramBundle;
    }
    
    public void run()
    {
      try
      {
        a.a(a.this, this.b, this.c, this.d);
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("BundleAction failed:");
        localStringBuilder.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JCommon", localStringBuilder.toString());
      }
    }
  }
  
  public class b
    implements Runnable
  {
    private Context b;
    private String c;
    
    public b(Context paramContext, String paramString)
    {
      this.b = paramContext;
      this.c = paramString;
    }
    
    public void run()
    {
      try
      {
        a.b(a.this, this.b, this.c);
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  }
  
  public class c
    implements Runnable
  {
    private Context b;
    private String c;
    
    c(Context paramContext, String paramString)
    {
      this.b = paramContext;
      this.c = paramString;
    }
    
    public void run()
    {
      try
      {
        a.a(a.this, this.b, this.c);
      }
      catch (Throwable localThrowable)
      {
        localThrowable.printStackTrace();
      }
    }
  }
  
  public class d
    implements Runnable
  {
    private Context b;
    private String c;
    private JSONObject d;
    
    d(Context paramContext, String paramString, JSONObject paramJSONObject)
    {
      this.b = paramContext;
      this.c = paramString;
      this.d = paramJSONObject;
    }
    
    public void run()
    {
      try
      {
        a.a(a.this, this.b, this.c, this.d);
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/f/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */