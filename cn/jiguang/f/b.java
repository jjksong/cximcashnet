package cn.jiguang.f;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import cn.jiguang.ai.a;
import java.util.HashMap;

@SuppressLint({"CommitPrefEdits"})
public class b
{
  private static SharedPreferences a;
  private static HashMap<String, Long> b;
  
  public static int a(Context paramContext)
  {
    return h(paramContext).getInt("rrat", 1);
  }
  
  private static String a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(paramString2);
    return localStringBuilder.toString();
  }
  
  private static void a()
  {
    b = new HashMap();
    b.put(a("JLocation", "_bi"), Long.valueOf(900000L));
    b.put(a("JWake", "_bi"), Long.valueOf(3600000L));
    b.put(a("JWakeConfigHelper", "_bi"), Long.valueOf(3600000L));
    b.put(a("JAppAll", "_ri"), Long.valueOf(604800000L));
    b.put(a("JAppMovement", "_ri"), Long.valueOf(3600000L));
    b.put(a("JAppRunning", "_ri"), Long.valueOf(3600000L));
    b.put(a("JArp", "_ri"), Long.valueOf(3600000L));
    b.put(a("JDeviceBattery", "_ri"), Long.valueOf(3600000L));
    b.put(a("JDevice", "_ri"), Long.valueOf(86400000L));
    b.put(a("JLocation", "_ri"), Long.valueOf(3600000L));
    b.put(a("JWake", "_ri"), Long.valueOf(3600000L));
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    h(paramContext).edit().putInt("rrat", paramInt).apply();
  }
  
  public static void a(Context paramContext, String paramString, long paramLong)
  {
    if (paramLong < 0L) {
      return;
    }
    if ((paramString.contains("JLocation")) && (paramLong > h(paramContext, paramString))) {
      return;
    }
    String str = a(paramString, "_bi");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("update ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" businessInterval:");
    localStringBuilder.append(paramLong);
    a.e("JCommonConfig", localStringBuilder.toString());
    h(paramContext).edit().putLong(str, paramLong).apply();
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2)
  {
    h(paramContext).edit().putString(paramString1, paramString2).apply();
  }
  
  public static void a(Context paramContext, String paramString, boolean paramBoolean)
  {
    paramString = a(paramString, "_ace");
    h(paramContext).edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    h(paramContext).edit().putBoolean("JArponceEnable", paramBoolean).apply();
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    long l1 = System.currentTimeMillis();
    long l3 = d(paramContext, paramString);
    long l2 = e(paramContext, paramString);
    boolean bool;
    if (l1 - l3 > l2) {
      bool = true;
    } else {
      bool = false;
    }
    paramContext = new StringBuilder();
    paramContext.append("is ");
    paramContext.append(paramString);
    paramContext.append(" businessTime:");
    paramContext.append(bool);
    paramContext.append(",curTime:");
    paramContext.append(l1);
    paramContext.append(",lastBusinessTime:");
    paramContext.append(l3);
    paramContext.append(",businessInterval:");
    paramContext.append(l2);
    a.e("JCommonConfig", paramContext.toString());
    return bool;
  }
  
  public static int b(Context paramContext)
  {
    return h(paramContext).getInt("rrpt", 1);
  }
  
  private static long b(String paramString1, String paramString2)
  {
    try
    {
      long l = ((Long)b.get(a(paramString1, paramString2))).longValue();
      return l;
    }
    catch (NullPointerException paramString1) {}
    return 0L;
  }
  
  public static void b(Context paramContext, int paramInt)
  {
    h(paramContext).edit().putInt("rrpt", paramInt).apply();
  }
  
  public static void b(Context paramContext, String paramString, long paramLong)
  {
    if (paramLong < 0L) {
      return;
    }
    String str = a(paramString, "_ri");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("update ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" reportInterval:");
    localStringBuilder.append(paramLong);
    a.e("JCommonConfig", localStringBuilder.toString());
    h(paramContext).edit().putLong(str, paramLong).apply();
  }
  
  public static void b(Context paramContext, String paramString, boolean paramBoolean)
  {
    paramString = a(paramString, "_aue");
    h(paramContext).edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    long l2 = System.currentTimeMillis();
    long l3 = g(paramContext, paramString);
    long l1 = h(paramContext, paramString);
    boolean bool;
    if (l2 - l3 > l1) {
      bool = true;
    } else {
      bool = false;
    }
    paramContext = new StringBuilder();
    paramContext.append("is ");
    paramContext.append(paramString);
    paramContext.append(" reportTime:");
    paramContext.append(bool);
    paramContext.append(",curTime:");
    paramContext.append(l2);
    paramContext.append(",lastReportTime:");
    paramContext.append(l3);
    paramContext.append(",reportInterval:");
    paramContext.append(l1);
    a.e("JCommonConfig", paramContext.toString());
    return bool;
  }
  
  public static void c(Context paramContext, String paramString)
  {
    if (paramString.contains("JApp")) {
      return;
    }
    String str = a(paramString, "_blt");
    long l = System.currentTimeMillis();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("update ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" lastBusinessTime");
    a.e("JCommonConfig", localStringBuilder.toString());
    h(paramContext).edit().putLong(str, l).apply();
  }
  
  public static boolean c(Context paramContext)
  {
    return h(paramContext).getBoolean("JArponceEnable", false);
  }
  
  public static long d(Context paramContext, String paramString)
  {
    paramString = a(paramString, "_blt");
    return h(paramContext).getLong(paramString, 0L);
  }
  
  public static String d(Context paramContext)
  {
    String str = a("JLocation", "info");
    return h(paramContext).getString(str, "");
  }
  
  public static long e(Context paramContext, String paramString)
  {
    String str = a(paramString, "_bi");
    long l = b(paramString, "_bi");
    return h(paramContext).getLong(str, l);
  }
  
  public static String e(Context paramContext)
  {
    return h(paramContext).getString("JNotificationState", "");
  }
  
  public static String f(Context paramContext)
  {
    return h(paramContext).getString("JDevicesession", "");
  }
  
  public static void f(Context paramContext, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("update ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" lastReportTime");
    a.e("JCommonConfig", localStringBuilder.toString());
    long l = System.currentTimeMillis();
    paramString = a(paramString, "_rlt");
    h(paramContext).edit().putLong(paramString, l).apply();
  }
  
  public static long g(Context paramContext, String paramString)
  {
    paramString = a(paramString, "_rlt");
    return h(paramContext).getLong(paramString, 0L);
  }
  
  private static void g(Context paramContext)
  {
    a = paramContext.getSharedPreferences("cn.jiguang.common", 0);
    a();
  }
  
  public static long h(Context paramContext, String paramString)
  {
    String str = a(paramString, "_ri");
    long l = b(paramString, "_ri");
    return h(paramContext).getLong(str, l);
  }
  
  private static SharedPreferences h(Context paramContext)
  {
    if (a == null) {
      g(paramContext);
    }
    return a;
  }
  
  public static boolean i(Context paramContext, String paramString)
  {
    String str = a(paramString, "_ace");
    if (paramString.equals("JArp")) {
      paramContext = h(paramContext);
    }
    for (boolean bool = false;; bool = true)
    {
      return paramContext.getBoolean(str, bool);
      paramContext = h(paramContext);
    }
  }
  
  public static boolean j(Context paramContext, String paramString)
  {
    paramString = a(paramString, "_aue");
    return h(paramContext).getBoolean(paramString, true);
  }
  
  public static String k(Context paramContext, String paramString)
  {
    paramString = a("JType", paramString);
    return h(paramContext).getString(paramString, "0,0");
  }
  
  public static void l(Context paramContext, String paramString)
  {
    paramString = a("JArp", paramString);
    h(paramContext).edit().putBoolean(paramString, true).apply();
  }
  
  public static boolean m(Context paramContext, String paramString)
  {
    paramString = a("JArp", paramString);
    return h(paramContext).getBoolean(paramString, false);
  }
  
  public static void n(Context paramContext, String paramString)
  {
    String str = a("JLocation", "info");
    h(paramContext).edit().putString(str, paramString).apply();
  }
  
  public static void o(Context paramContext, String paramString)
  {
    h(paramContext).edit().putString("JNotificationState", paramString).apply();
  }
  
  public static void p(Context paramContext, String paramString)
  {
    a.e("JCommonConfig", "update deviceSession");
    h(paramContext).edit().putString("JDevicesession", paramString).apply();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/f/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */