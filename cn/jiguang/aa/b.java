package cn.jiguang.aa;

import android.content.Context;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b
{
  public static String a = "";
  
  private static String a(byte[] paramArrayOfByte)
  {
    try
    {
      byte[] arrayOfByte = MessageDigest.getInstance("MD5").digest(paramArrayOfByte);
      paramArrayOfByte = new java/lang/StringBuilder;
      paramArrayOfByte.<init>();
      int j = arrayOfByte.length;
      for (int i = 0; i < j; i++)
      {
        int k = arrayOfByte[i] & 0xFF;
        if (k < 16) {
          paramArrayOfByte.append("0");
        }
        paramArrayOfByte.append(Integer.toHexString(k));
      }
      paramArrayOfByte = paramArrayOfByte.toString();
      return paramArrayOfByte;
    }
    catch (Throwable localThrowable)
    {
      paramArrayOfByte = new StringBuilder();
      paramArrayOfByte.append("get md5 throwable:");
      paramArrayOfByte.append(localThrowable.getMessage());
      cn.jiguang.ai.a.g("JWakeConfigHelper", paramArrayOfByte.toString());
    }
    return "";
  }
  
  private static List<String> a(JSONObject paramJSONObject, String paramString)
  {
    JSONArray localJSONArray = paramJSONObject.optJSONArray(paramString);
    if (localJSONArray != null)
    {
      paramString = new ArrayList();
      for (int i = 0;; i++)
      {
        paramJSONObject = paramString;
        if (i >= localJSONArray.length()) {
          break;
        }
        paramString.add(localJSONArray.get(i).toString());
      }
    }
    paramJSONObject = null;
    return paramJSONObject;
  }
  
  /* Error */
  public static JSONObject a(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_0
    //   4: invokestatic 104	cn/jiguang/f/d:e	(Landroid/content/Context;)Z
    //   7: ifne +12 -> 19
    //   10: ldc 54
    //   12: ldc 106
    //   14: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   17: aconst_null
    //   18: areturn
    //   19: aload_0
    //   20: invokestatic 110	cn/jiguang/f/d:b	(Landroid/content/Context;)Ljava/lang/String;
    //   23: astore 9
    //   25: aload 9
    //   27: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   30: ifeq +12 -> 42
    //   33: ldc 54
    //   35: ldc 118
    //   37: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   40: aconst_null
    //   41: areturn
    //   42: aload_0
    //   43: invokestatic 122	cn/jiguang/f/d:c	(Landroid/content/Context;)J
    //   46: lstore_2
    //   47: lload_2
    //   48: lconst_0
    //   49: lcmp
    //   50: ifne +12 -> 62
    //   53: ldc 54
    //   55: ldc 124
    //   57: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   60: aconst_null
    //   61: areturn
    //   62: aload_0
    //   63: invokestatic 127	cn/jiguang/f/d:f	(Landroid/content/Context;)Ljava/lang/String;
    //   66: astore 8
    //   68: aload 8
    //   70: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   73: ifeq +12 -> 85
    //   76: ldc 54
    //   78: ldc -127
    //   80: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   83: aconst_null
    //   84: areturn
    //   85: ldc -125
    //   87: astore 7
    //   89: aload 7
    //   91: astore 5
    //   93: getstatic 136	cn/jiguang/f/c$a:c	Z
    //   96: ifeq +58 -> 154
    //   99: aload 7
    //   101: astore 5
    //   103: getstatic 138	cn/jiguang/aa/b:a	Ljava/lang/String;
    //   106: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   109: ifne +45 -> 154
    //   112: new 28	java/lang/StringBuilder
    //   115: astore 5
    //   117: aload 5
    //   119: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   122: aload 5
    //   124: ldc -116
    //   126: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: pop
    //   130: aload 5
    //   132: getstatic 138	cn/jiguang/aa/b:a	Ljava/lang/String;
    //   135: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: ldc 54
    //   141: aload 5
    //   143: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   146: invokestatic 142	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   149: getstatic 138	cn/jiguang/aa/b:a	Ljava/lang/String;
    //   152: astore 5
    //   154: new 144	java/util/HashMap
    //   157: astore 7
    //   159: aload 7
    //   161: invokespecial 145	java/util/HashMap:<init>	()V
    //   164: new 144	java/util/HashMap
    //   167: astore 12
    //   169: aload 12
    //   171: invokespecial 145	java/util/HashMap:<init>	()V
    //   174: getstatic 150	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   177: astore 11
    //   179: getstatic 153	android/os/Build:MODEL	Ljava/lang/String;
    //   182: astore 10
    //   184: aload 12
    //   186: ldc -101
    //   188: aload 9
    //   190: invokevirtual 159	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   193: pop
    //   194: aload 12
    //   196: ldc -95
    //   198: lload_2
    //   199: invokestatic 167	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   202: invokevirtual 159	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   205: pop
    //   206: aload 11
    //   208: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   211: ifne +13 -> 224
    //   214: aload 12
    //   216: ldc -87
    //   218: aload 11
    //   220: invokevirtual 159	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   223: pop
    //   224: aload 10
    //   226: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   229: ifne +13 -> 242
    //   232: aload 12
    //   234: ldc -85
    //   236: aload 10
    //   238: invokevirtual 159	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   241: pop
    //   242: aload 5
    //   244: aload 12
    //   246: invokestatic 177	cn/jiguang/net/HttpUtils:getUrlWithValueEncodeParas	(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    //   249: astore 5
    //   251: new 28	java/lang/StringBuilder
    //   254: astore 10
    //   256: aload 10
    //   258: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   261: aload 10
    //   263: ldc -77
    //   265: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   268: pop
    //   269: aload 10
    //   271: aload 5
    //   273: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   276: pop
    //   277: ldc 54
    //   279: aload 10
    //   281: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   284: invokestatic 142	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   287: aload 7
    //   289: ldc -75
    //   291: ldc -73
    //   293: invokeinterface 186 3 0
    //   298: pop
    //   299: aload 7
    //   301: ldc -68
    //   303: ldc -66
    //   305: invokeinterface 186 3 0
    //   310: pop
    //   311: aload 7
    //   313: ldc -64
    //   315: ldc -62
    //   317: invokeinterface 186 3 0
    //   322: pop
    //   323: aload 7
    //   325: ldc -60
    //   327: aload 9
    //   329: invokeinterface 186 3 0
    //   334: pop
    //   335: ldc 8
    //   337: ldc -58
    //   339: invokevirtual 202	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   342: invokestatic 204	cn/jiguang/aa/b:a	([B)Ljava/lang/String;
    //   345: astore 9
    //   347: aload 8
    //   349: invokestatic 207	cn/jiguang/f/d:b	(Ljava/lang/String;)Ljava/lang/String;
    //   352: astore 10
    //   354: new 28	java/lang/StringBuilder
    //   357: astore 8
    //   359: aload 8
    //   361: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   364: aload 8
    //   366: lload_2
    //   367: invokevirtual 210	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload 8
    //   373: aload 10
    //   375: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   378: pop
    //   379: aload 8
    //   381: aload 9
    //   383: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   386: pop
    //   387: aload 8
    //   389: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   392: invokestatic 207	cn/jiguang/f/d:b	(Ljava/lang/String;)Ljava/lang/String;
    //   395: astore 8
    //   397: aload 8
    //   399: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   402: ifeq +5 -> 407
    //   405: aconst_null
    //   406: areturn
    //   407: new 28	java/lang/StringBuilder
    //   410: astore 9
    //   412: aload 9
    //   414: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   417: aload 9
    //   419: lload_2
    //   420: invokevirtual 210	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   423: pop
    //   424: aload 9
    //   426: ldc -44
    //   428: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   431: pop
    //   432: aload 9
    //   434: aload 8
    //   436: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload 9
    //   442: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   445: invokevirtual 215	java/lang/String:getBytes	()[B
    //   448: bipush 10
    //   450: invokestatic 221	android/util/Base64:encodeToString	([BI)Ljava/lang/String;
    //   453: astore 9
    //   455: aload 9
    //   457: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   460: ifeq +5 -> 465
    //   463: aconst_null
    //   464: areturn
    //   465: new 28	java/lang/StringBuilder
    //   468: astore 8
    //   470: aload 8
    //   472: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   475: aload 8
    //   477: ldc -33
    //   479: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   482: pop
    //   483: aload 8
    //   485: aload 9
    //   487: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   490: pop
    //   491: aload 7
    //   493: ldc -31
    //   495: aload 8
    //   497: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   500: invokeinterface 186 3 0
    //   505: pop
    //   506: aload 7
    //   508: ldc -29
    //   510: ldc -58
    //   512: invokeinterface 186 3 0
    //   517: pop
    //   518: aload_0
    //   519: aload 5
    //   521: invokestatic 231	cn/jiguang/net/HttpUtils:getHttpURLConnectionWithProxy	(Landroid/content/Context;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   524: astore_0
    //   525: aload_0
    //   526: sipush 30000
    //   529: invokevirtual 237	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   532: aload_0
    //   533: sipush 30000
    //   536: invokevirtual 240	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   539: aload 7
    //   541: invokeinterface 244 1 0
    //   546: invokeinterface 250 1 0
    //   551: astore 7
    //   553: aload 7
    //   555: invokeinterface 256 1 0
    //   560: ifeq +42 -> 602
    //   563: aload 7
    //   565: invokeinterface 260 1 0
    //   570: checkcast 262	java/util/Map$Entry
    //   573: astore 5
    //   575: aload_0
    //   576: aload 5
    //   578: invokeinterface 265 1 0
    //   583: checkcast 163	java/lang/String
    //   586: aload 5
    //   588: invokeinterface 268 1 0
    //   593: checkcast 163	java/lang/String
    //   596: invokevirtual 271	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   599: goto -46 -> 553
    //   602: aload_0
    //   603: instanceof 273
    //   606: istore 4
    //   608: iload 4
    //   610: ifeq +138 -> 748
    //   613: ldc_w 275
    //   616: invokestatic 280	javax/net/ssl/SSLContext:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    //   619: astore 5
    //   621: new 282	cn/jiguang/net/SSLTrustManager
    //   624: astore 8
    //   626: aload 8
    //   628: ldc_w 284
    //   631: invokespecial 287	cn/jiguang/net/SSLTrustManager:<init>	(Ljava/lang/String;)V
    //   634: new 289	java/security/SecureRandom
    //   637: astore 7
    //   639: aload 7
    //   641: invokespecial 290	java/security/SecureRandom:<init>	()V
    //   644: aload 5
    //   646: aconst_null
    //   647: iconst_1
    //   648: anewarray 292	javax/net/ssl/TrustManager
    //   651: dup
    //   652: iconst_0
    //   653: aload 8
    //   655: aastore
    //   656: aload 7
    //   658: invokevirtual 296	javax/net/ssl/SSLContext:init	([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    //   661: aload_0
    //   662: checkcast 273	javax/net/ssl/HttpsURLConnection
    //   665: aload 5
    //   667: invokevirtual 300	javax/net/ssl/SSLContext:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   670: invokevirtual 304	javax/net/ssl/HttpsURLConnection:setSSLSocketFactory	(Ljavax/net/ssl/SSLSocketFactory;)V
    //   673: aload_0
    //   674: checkcast 273	javax/net/ssl/HttpsURLConnection
    //   677: astore 7
    //   679: new 306	cn/jiguang/net/DefaultHostVerifier
    //   682: astore 5
    //   684: aload 5
    //   686: aload_0
    //   687: invokevirtual 310	java/net/HttpURLConnection:getURL	()Ljava/net/URL;
    //   690: invokevirtual 315	java/net/URL:getHost	()Ljava/lang/String;
    //   693: invokespecial 316	cn/jiguang/net/DefaultHostVerifier:<init>	(Ljava/lang/String;)V
    //   696: aload 7
    //   698: aload 5
    //   700: invokevirtual 320	javax/net/ssl/HttpsURLConnection:setHostnameVerifier	(Ljavax/net/ssl/HostnameVerifier;)V
    //   703: goto +45 -> 748
    //   706: astore 5
    //   708: new 28	java/lang/StringBuilder
    //   711: astore 7
    //   713: aload 7
    //   715: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   718: aload 7
    //   720: ldc_w 322
    //   723: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   726: pop
    //   727: aload 7
    //   729: aload 5
    //   731: invokevirtual 52	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   734: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   737: pop
    //   738: ldc 54
    //   740: aload 7
    //   742: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   745: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   748: aload_0
    //   749: invokevirtual 325	java/net/HttpURLConnection:getResponseCode	()I
    //   752: sipush 200
    //   755: if_icmpne +181 -> 936
    //   758: aload_0
    //   759: invokevirtual 329	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   762: astore 5
    //   764: aload 5
    //   766: astore 8
    //   768: aload 5
    //   770: astore 9
    //   772: aload 5
    //   774: astore 7
    //   776: aload 5
    //   778: astore 6
    //   780: new 331	java/io/ByteArrayOutputStream
    //   783: astore 11
    //   785: aload 5
    //   787: astore 8
    //   789: aload 5
    //   791: astore 9
    //   793: aload 5
    //   795: astore 7
    //   797: aload 5
    //   799: astore 6
    //   801: aload 11
    //   803: invokespecial 332	java/io/ByteArrayOutputStream:<init>	()V
    //   806: aload 5
    //   808: astore 8
    //   810: aload 5
    //   812: astore 9
    //   814: aload 5
    //   816: astore 7
    //   818: aload 5
    //   820: astore 6
    //   822: sipush 1024
    //   825: newarray <illegal type>
    //   827: astore 10
    //   829: aload 5
    //   831: astore 8
    //   833: aload 5
    //   835: astore 9
    //   837: aload 5
    //   839: astore 7
    //   841: aload 5
    //   843: astore 6
    //   845: aload 5
    //   847: aload 10
    //   849: invokevirtual 338	java/io/InputStream:read	([B)I
    //   852: istore_1
    //   853: iload_1
    //   854: iconst_m1
    //   855: if_icmpeq +31 -> 886
    //   858: aload 5
    //   860: astore 8
    //   862: aload 5
    //   864: astore 9
    //   866: aload 5
    //   868: astore 7
    //   870: aload 5
    //   872: astore 6
    //   874: aload 11
    //   876: aload 10
    //   878: iconst_0
    //   879: iload_1
    //   880: invokevirtual 342	java/io/ByteArrayOutputStream:write	([BII)V
    //   883: goto -54 -> 829
    //   886: aload 5
    //   888: astore 8
    //   890: aload 5
    //   892: astore 9
    //   894: aload 5
    //   896: astore 7
    //   898: aload 5
    //   900: astore 6
    //   902: new 163	java/lang/String
    //   905: astore 10
    //   907: aload 5
    //   909: astore 8
    //   911: aload 5
    //   913: astore 9
    //   915: aload 5
    //   917: astore 7
    //   919: aload 5
    //   921: astore 6
    //   923: aload 10
    //   925: aload 11
    //   927: invokevirtual 345	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   930: invokespecial 348	java/lang/String:<init>	([B)V
    //   933: goto +10 -> 943
    //   936: aconst_null
    //   937: astore 5
    //   939: aload 5
    //   941: astore 10
    //   943: aload 5
    //   945: astore 8
    //   947: aload 5
    //   949: astore 9
    //   951: aload 5
    //   953: astore 7
    //   955: aload 5
    //   957: astore 6
    //   959: aload 10
    //   961: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   964: ifne +212 -> 1176
    //   967: aload 5
    //   969: astore 8
    //   971: aload 5
    //   973: astore 9
    //   975: aload 5
    //   977: astore 7
    //   979: aload 5
    //   981: astore 6
    //   983: new 28	java/lang/StringBuilder
    //   986: astore 11
    //   988: aload 5
    //   990: astore 8
    //   992: aload 5
    //   994: astore 9
    //   996: aload 5
    //   998: astore 7
    //   1000: aload 5
    //   1002: astore 6
    //   1004: aload 11
    //   1006: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1009: aload 5
    //   1011: astore 8
    //   1013: aload 5
    //   1015: astore 9
    //   1017: aload 5
    //   1019: astore 7
    //   1021: aload 5
    //   1023: astore 6
    //   1025: aload 11
    //   1027: ldc_w 350
    //   1030: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1033: pop
    //   1034: aload 5
    //   1036: astore 8
    //   1038: aload 5
    //   1040: astore 9
    //   1042: aload 5
    //   1044: astore 7
    //   1046: aload 5
    //   1048: astore 6
    //   1050: aload 11
    //   1052: aload 10
    //   1054: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1057: pop
    //   1058: aload 5
    //   1060: astore 8
    //   1062: aload 5
    //   1064: astore 9
    //   1066: aload 5
    //   1068: astore 7
    //   1070: aload 5
    //   1072: astore 6
    //   1074: ldc 54
    //   1076: aload 11
    //   1078: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1081: invokestatic 142	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   1084: aload 5
    //   1086: astore 8
    //   1088: aload 5
    //   1090: astore 9
    //   1092: aload 5
    //   1094: astore 7
    //   1096: aload 5
    //   1098: astore 6
    //   1100: new 63	org/json/JSONObject
    //   1103: dup
    //   1104: aload 10
    //   1106: invokespecial 351	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   1109: astore 10
    //   1111: aload 5
    //   1113: ifnull +52 -> 1165
    //   1116: aload 5
    //   1118: invokevirtual 354	java/io/InputStream:close	()V
    //   1121: goto +44 -> 1165
    //   1124: astore 6
    //   1126: new 28	java/lang/StringBuilder
    //   1129: dup
    //   1130: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1133: astore 5
    //   1135: aload 5
    //   1137: ldc_w 356
    //   1140: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1143: pop
    //   1144: aload 5
    //   1146: aload 6
    //   1148: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1151: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1154: pop
    //   1155: ldc 54
    //   1157: aload 5
    //   1159: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1162: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1165: aload_0
    //   1166: ifnull +7 -> 1173
    //   1169: aload_0
    //   1170: invokevirtual 360	java/net/HttpURLConnection:disconnect	()V
    //   1173: aload 10
    //   1175: areturn
    //   1176: aload 5
    //   1178: ifnull +52 -> 1230
    //   1181: aload 5
    //   1183: invokevirtual 354	java/io/InputStream:close	()V
    //   1186: goto +44 -> 1230
    //   1189: astore 6
    //   1191: new 28	java/lang/StringBuilder
    //   1194: dup
    //   1195: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1198: astore 5
    //   1200: aload 5
    //   1202: ldc_w 356
    //   1205: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1208: pop
    //   1209: aload 5
    //   1211: aload 6
    //   1213: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1216: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1219: pop
    //   1220: ldc 54
    //   1222: aload 5
    //   1224: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1227: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1230: aload_0
    //   1231: ifnull +511 -> 1742
    //   1234: aload_0
    //   1235: invokevirtual 360	java/net/HttpURLConnection:disconnect	()V
    //   1238: goto +504 -> 1742
    //   1241: astore 7
    //   1243: aload 8
    //   1245: astore 5
    //   1247: goto +502 -> 1749
    //   1250: astore 8
    //   1252: aload 9
    //   1254: astore 7
    //   1256: goto +61 -> 1317
    //   1259: astore 8
    //   1261: goto +200 -> 1461
    //   1264: astore 8
    //   1266: aload 6
    //   1268: astore 7
    //   1270: goto +335 -> 1605
    //   1273: astore 5
    //   1275: goto +482 -> 1757
    //   1278: astore 8
    //   1280: aconst_null
    //   1281: astore 7
    //   1283: goto +34 -> 1317
    //   1286: astore 8
    //   1288: aconst_null
    //   1289: astore 7
    //   1291: goto +170 -> 1461
    //   1294: astore 8
    //   1296: aconst_null
    //   1297: astore 7
    //   1299: goto +306 -> 1605
    //   1302: astore 5
    //   1304: aconst_null
    //   1305: astore_0
    //   1306: goto +451 -> 1757
    //   1309: astore 8
    //   1311: aconst_null
    //   1312: astore 7
    //   1314: aload 7
    //   1316: astore_0
    //   1317: aload 7
    //   1319: astore 5
    //   1321: aload_0
    //   1322: astore 6
    //   1324: new 28	java/lang/StringBuilder
    //   1327: astore 9
    //   1329: aload 7
    //   1331: astore 5
    //   1333: aload_0
    //   1334: astore 6
    //   1336: aload 9
    //   1338: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1341: aload 7
    //   1343: astore 5
    //   1345: aload_0
    //   1346: astore 6
    //   1348: aload 9
    //   1350: ldc_w 356
    //   1353: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1356: pop
    //   1357: aload 7
    //   1359: astore 5
    //   1361: aload_0
    //   1362: astore 6
    //   1364: aload 9
    //   1366: aload 8
    //   1368: invokevirtual 361	org/json/JSONException:getMessage	()Ljava/lang/String;
    //   1371: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1374: pop
    //   1375: aload 7
    //   1377: astore 5
    //   1379: aload_0
    //   1380: astore 6
    //   1382: ldc 54
    //   1384: aload 9
    //   1386: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1389: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1392: aload 7
    //   1394: ifnull +52 -> 1446
    //   1397: aload 7
    //   1399: invokevirtual 354	java/io/InputStream:close	()V
    //   1402: goto +44 -> 1446
    //   1405: astore 5
    //   1407: new 28	java/lang/StringBuilder
    //   1410: dup
    //   1411: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1414: astore 6
    //   1416: aload 6
    //   1418: ldc_w 356
    //   1421: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1424: pop
    //   1425: aload 6
    //   1427: aload 5
    //   1429: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1432: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1435: pop
    //   1436: ldc 54
    //   1438: aload 6
    //   1440: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1443: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1446: aload_0
    //   1447: ifnull +295 -> 1742
    //   1450: goto +288 -> 1738
    //   1453: astore 8
    //   1455: aconst_null
    //   1456: astore 7
    //   1458: aload 7
    //   1460: astore_0
    //   1461: aload 7
    //   1463: astore 5
    //   1465: aload_0
    //   1466: astore 6
    //   1468: new 28	java/lang/StringBuilder
    //   1471: astore 9
    //   1473: aload 7
    //   1475: astore 5
    //   1477: aload_0
    //   1478: astore 6
    //   1480: aload 9
    //   1482: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1485: aload 7
    //   1487: astore 5
    //   1489: aload_0
    //   1490: astore 6
    //   1492: aload 9
    //   1494: ldc_w 356
    //   1497: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1500: pop
    //   1501: aload 7
    //   1503: astore 5
    //   1505: aload_0
    //   1506: astore 6
    //   1508: aload 9
    //   1510: aload 8
    //   1512: invokevirtual 362	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1515: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1518: pop
    //   1519: aload 7
    //   1521: astore 5
    //   1523: aload_0
    //   1524: astore 6
    //   1526: ldc 54
    //   1528: aload 9
    //   1530: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1533: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1536: aload 7
    //   1538: ifnull +52 -> 1590
    //   1541: aload 7
    //   1543: invokevirtual 354	java/io/InputStream:close	()V
    //   1546: goto +44 -> 1590
    //   1549: astore 6
    //   1551: new 28	java/lang/StringBuilder
    //   1554: dup
    //   1555: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1558: astore 5
    //   1560: aload 5
    //   1562: ldc_w 356
    //   1565: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1568: pop
    //   1569: aload 5
    //   1571: aload 6
    //   1573: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1576: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1579: pop
    //   1580: ldc 54
    //   1582: aload 5
    //   1584: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1587: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1590: aload_0
    //   1591: ifnull +151 -> 1742
    //   1594: goto +144 -> 1738
    //   1597: astore 8
    //   1599: aconst_null
    //   1600: astore 7
    //   1602: aload 7
    //   1604: astore_0
    //   1605: aload 7
    //   1607: astore 5
    //   1609: aload_0
    //   1610: astore 6
    //   1612: new 28	java/lang/StringBuilder
    //   1615: astore 9
    //   1617: aload 7
    //   1619: astore 5
    //   1621: aload_0
    //   1622: astore 6
    //   1624: aload 9
    //   1626: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1629: aload 7
    //   1631: astore 5
    //   1633: aload_0
    //   1634: astore 6
    //   1636: aload 9
    //   1638: ldc_w 356
    //   1641: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1644: pop
    //   1645: aload 7
    //   1647: astore 5
    //   1649: aload_0
    //   1650: astore 6
    //   1652: aload 9
    //   1654: aload 8
    //   1656: invokevirtual 363	java/io/UnsupportedEncodingException:getMessage	()Ljava/lang/String;
    //   1659: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1662: pop
    //   1663: aload 7
    //   1665: astore 5
    //   1667: aload_0
    //   1668: astore 6
    //   1670: ldc 54
    //   1672: aload 9
    //   1674: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1677: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1680: aload 7
    //   1682: ifnull +52 -> 1734
    //   1685: aload 7
    //   1687: invokevirtual 354	java/io/InputStream:close	()V
    //   1690: goto +44 -> 1734
    //   1693: astore 6
    //   1695: new 28	java/lang/StringBuilder
    //   1698: dup
    //   1699: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1702: astore 5
    //   1704: aload 5
    //   1706: ldc_w 356
    //   1709: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1712: pop
    //   1713: aload 5
    //   1715: aload 6
    //   1717: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1720: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1723: pop
    //   1724: ldc 54
    //   1726: aload 5
    //   1728: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1731: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1734: aload_0
    //   1735: ifnull +7 -> 1742
    //   1738: aload_0
    //   1739: invokevirtual 360	java/net/HttpURLConnection:disconnect	()V
    //   1742: aconst_null
    //   1743: areturn
    //   1744: astore 7
    //   1746: aload 6
    //   1748: astore_0
    //   1749: aload 5
    //   1751: astore 6
    //   1753: aload 7
    //   1755: astore 5
    //   1757: aload 6
    //   1759: ifnull +52 -> 1811
    //   1762: aload 6
    //   1764: invokevirtual 354	java/io/InputStream:close	()V
    //   1767: goto +44 -> 1811
    //   1770: astore 6
    //   1772: new 28	java/lang/StringBuilder
    //   1775: dup
    //   1776: invokespecial 31	java/lang/StringBuilder:<init>	()V
    //   1779: astore 7
    //   1781: aload 7
    //   1783: ldc_w 356
    //   1786: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1789: pop
    //   1790: aload 7
    //   1792: aload 6
    //   1794: invokevirtual 357	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1797: invokevirtual 37	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1800: pop
    //   1801: ldc 54
    //   1803: aload 7
    //   1805: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1808: invokestatic 60	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   1811: aload_0
    //   1812: ifnull +7 -> 1819
    //   1815: aload_0
    //   1816: invokevirtual 360	java/net/HttpURLConnection:disconnect	()V
    //   1819: aload 5
    //   1821: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1822	0	paramContext	Context
    //   852	28	1	i	int
    //   46	374	2	l	long
    //   606	3	4	bool	boolean
    //   91	608	5	localObject1	Object
    //   706	24	5	localThrowable	Throwable
    //   762	484	5	localObject2	Object
    //   1273	1	5	localObject3	Object
    //   1302	1	5	localObject4	Object
    //   1319	59	5	localObject5	Object
    //   1405	23	5	localException1	Exception
    //   1463	357	5	localObject6	Object
    //   1	1098	6	localObject7	Object
    //   1124	23	6	localException2	Exception
    //   1189	78	6	localException3	Exception
    //   1322	203	6	localObject8	Object
    //   1549	23	6	localException4	Exception
    //   1610	59	6	localContext	Context
    //   1693	54	6	localException5	Exception
    //   1751	12	6	localObject9	Object
    //   1770	23	6	localException6	Exception
    //   87	1008	7	localObject10	Object
    //   1241	1	7	localObject11	Object
    //   1254	432	7	localObject12	Object
    //   1744	10	7	localObject13	Object
    //   1779	25	7	localStringBuilder	StringBuilder
    //   66	1178	8	localObject14	Object
    //   1250	1	8	localJSONException1	JSONException
    //   1259	1	8	localIOException1	java.io.IOException
    //   1264	1	8	localUnsupportedEncodingException1	java.io.UnsupportedEncodingException
    //   1278	1	8	localJSONException2	JSONException
    //   1286	1	8	localIOException2	java.io.IOException
    //   1294	1	8	localUnsupportedEncodingException2	java.io.UnsupportedEncodingException
    //   1309	58	8	localJSONException3	JSONException
    //   1453	58	8	localIOException3	java.io.IOException
    //   1597	58	8	localUnsupportedEncodingException3	java.io.UnsupportedEncodingException
    //   23	1650	9	localObject15	Object
    //   182	992	10	localObject16	Object
    //   177	900	11	localObject17	Object
    //   167	78	12	localHashMap	java.util.HashMap
    // Exception table:
    //   from	to	target	type
    //   613	703	706	java/lang/Throwable
    //   1116	1121	1124	java/lang/Exception
    //   1181	1186	1189	java/lang/Exception
    //   780	785	1241	finally
    //   801	806	1241	finally
    //   822	829	1241	finally
    //   845	853	1241	finally
    //   874	883	1241	finally
    //   902	907	1241	finally
    //   923	933	1241	finally
    //   959	967	1241	finally
    //   983	988	1241	finally
    //   1004	1009	1241	finally
    //   1025	1034	1241	finally
    //   1050	1058	1241	finally
    //   1074	1084	1241	finally
    //   1100	1111	1241	finally
    //   780	785	1250	org/json/JSONException
    //   801	806	1250	org/json/JSONException
    //   822	829	1250	org/json/JSONException
    //   845	853	1250	org/json/JSONException
    //   874	883	1250	org/json/JSONException
    //   902	907	1250	org/json/JSONException
    //   923	933	1250	org/json/JSONException
    //   959	967	1250	org/json/JSONException
    //   983	988	1250	org/json/JSONException
    //   1004	1009	1250	org/json/JSONException
    //   1025	1034	1250	org/json/JSONException
    //   1050	1058	1250	org/json/JSONException
    //   1074	1084	1250	org/json/JSONException
    //   1100	1111	1250	org/json/JSONException
    //   780	785	1259	java/io/IOException
    //   801	806	1259	java/io/IOException
    //   822	829	1259	java/io/IOException
    //   845	853	1259	java/io/IOException
    //   874	883	1259	java/io/IOException
    //   902	907	1259	java/io/IOException
    //   923	933	1259	java/io/IOException
    //   959	967	1259	java/io/IOException
    //   983	988	1259	java/io/IOException
    //   1004	1009	1259	java/io/IOException
    //   1025	1034	1259	java/io/IOException
    //   1050	1058	1259	java/io/IOException
    //   1074	1084	1259	java/io/IOException
    //   1100	1111	1259	java/io/IOException
    //   780	785	1264	java/io/UnsupportedEncodingException
    //   801	806	1264	java/io/UnsupportedEncodingException
    //   822	829	1264	java/io/UnsupportedEncodingException
    //   845	853	1264	java/io/UnsupportedEncodingException
    //   874	883	1264	java/io/UnsupportedEncodingException
    //   902	907	1264	java/io/UnsupportedEncodingException
    //   923	933	1264	java/io/UnsupportedEncodingException
    //   959	967	1264	java/io/UnsupportedEncodingException
    //   983	988	1264	java/io/UnsupportedEncodingException
    //   1004	1009	1264	java/io/UnsupportedEncodingException
    //   1025	1034	1264	java/io/UnsupportedEncodingException
    //   1050	1058	1264	java/io/UnsupportedEncodingException
    //   1074	1084	1264	java/io/UnsupportedEncodingException
    //   1100	1111	1264	java/io/UnsupportedEncodingException
    //   525	553	1273	finally
    //   553	599	1273	finally
    //   602	608	1273	finally
    //   613	703	1273	finally
    //   708	748	1273	finally
    //   748	764	1273	finally
    //   525	553	1278	org/json/JSONException
    //   553	599	1278	org/json/JSONException
    //   602	608	1278	org/json/JSONException
    //   613	703	1278	org/json/JSONException
    //   708	748	1278	org/json/JSONException
    //   748	764	1278	org/json/JSONException
    //   525	553	1286	java/io/IOException
    //   553	599	1286	java/io/IOException
    //   602	608	1286	java/io/IOException
    //   613	703	1286	java/io/IOException
    //   708	748	1286	java/io/IOException
    //   748	764	1286	java/io/IOException
    //   525	553	1294	java/io/UnsupportedEncodingException
    //   553	599	1294	java/io/UnsupportedEncodingException
    //   602	608	1294	java/io/UnsupportedEncodingException
    //   613	703	1294	java/io/UnsupportedEncodingException
    //   708	748	1294	java/io/UnsupportedEncodingException
    //   748	764	1294	java/io/UnsupportedEncodingException
    //   3	17	1302	finally
    //   19	40	1302	finally
    //   42	47	1302	finally
    //   53	60	1302	finally
    //   62	83	1302	finally
    //   93	99	1302	finally
    //   103	154	1302	finally
    //   154	224	1302	finally
    //   224	242	1302	finally
    //   242	405	1302	finally
    //   407	463	1302	finally
    //   465	525	1302	finally
    //   3	17	1309	org/json/JSONException
    //   19	40	1309	org/json/JSONException
    //   42	47	1309	org/json/JSONException
    //   53	60	1309	org/json/JSONException
    //   62	83	1309	org/json/JSONException
    //   93	99	1309	org/json/JSONException
    //   103	154	1309	org/json/JSONException
    //   154	224	1309	org/json/JSONException
    //   224	242	1309	org/json/JSONException
    //   242	405	1309	org/json/JSONException
    //   407	463	1309	org/json/JSONException
    //   465	525	1309	org/json/JSONException
    //   1397	1402	1405	java/lang/Exception
    //   3	17	1453	java/io/IOException
    //   19	40	1453	java/io/IOException
    //   42	47	1453	java/io/IOException
    //   53	60	1453	java/io/IOException
    //   62	83	1453	java/io/IOException
    //   93	99	1453	java/io/IOException
    //   103	154	1453	java/io/IOException
    //   154	224	1453	java/io/IOException
    //   224	242	1453	java/io/IOException
    //   242	405	1453	java/io/IOException
    //   407	463	1453	java/io/IOException
    //   465	525	1453	java/io/IOException
    //   1541	1546	1549	java/lang/Exception
    //   3	17	1597	java/io/UnsupportedEncodingException
    //   19	40	1597	java/io/UnsupportedEncodingException
    //   42	47	1597	java/io/UnsupportedEncodingException
    //   53	60	1597	java/io/UnsupportedEncodingException
    //   62	83	1597	java/io/UnsupportedEncodingException
    //   93	99	1597	java/io/UnsupportedEncodingException
    //   103	154	1597	java/io/UnsupportedEncodingException
    //   154	224	1597	java/io/UnsupportedEncodingException
    //   224	242	1597	java/io/UnsupportedEncodingException
    //   242	405	1597	java/io/UnsupportedEncodingException
    //   407	463	1597	java/io/UnsupportedEncodingException
    //   465	525	1597	java/io/UnsupportedEncodingException
    //   1685	1690	1693	java/lang/Exception
    //   1324	1329	1744	finally
    //   1336	1341	1744	finally
    //   1348	1357	1744	finally
    //   1364	1375	1744	finally
    //   1382	1392	1744	finally
    //   1468	1473	1744	finally
    //   1480	1485	1744	finally
    //   1492	1501	1744	finally
    //   1508	1519	1744	finally
    //   1526	1536	1744	finally
    //   1612	1617	1744	finally
    //   1624	1629	1744	finally
    //   1636	1645	1744	finally
    //   1652	1663	1744	finally
    //   1670	1680	1744	finally
    //   1762	1767	1770	java/lang/Exception
  }
  
  public static void a(Context paramContext, JSONObject paramJSONObject)
  {
    if (paramJSONObject != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("write wakeConfigJson:");
      localStringBuilder.append(paramJSONObject);
      cn.jiguang.ai.a.c("JWakeConfigHelper", localStringBuilder.toString());
      cn.jiguang.s.b.a(paramContext, "bwc.catch", paramJSONObject);
    }
  }
  
  public static cn.jiguang.z.a b(Context paramContext, JSONObject paramJSONObject)
  {
    cn.jiguang.z.a locala = new cn.jiguang.z.a();
    if (paramJSONObject == null) {
      return locala;
    }
    try
    {
      int i = paramJSONObject.optInt("app_wakeup_stat", -1);
      if (i < 0) {
        return locala;
      }
      int j = paramJSONObject.optInt("app_report_app_list_threshold", -1);
      if (j > 0) {
        cn.jiguang.f.b.b(paramContext, "JAppAll", j * 1000);
      }
      j = paramJSONObject.optInt("app_wakeup_threshold", -1) * 1000;
      if (j > 0) {
        locala.e = j;
      }
      j = paramJSONObject.optInt("app_get_threshold", -1) * 1000;
      if (j > 0) {
        locala.f = j;
      }
      j = paramJSONObject.optInt("app_report_threshold", -1) * 1000;
      if (j > 0) {
        locala.g = j;
      }
      boolean bool2 = false;
      if (i == 0) {}
      while (i == 1)
      {
        locala.c = true;
        break;
      }
      if (i == 2)
      {
        locala.c = false;
      }
      else
      {
        str = paramJSONObject.optString("errmsg");
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("parseWakeConfigJson error: ");
        paramContext.append(str);
        cn.jiguang.ai.a.g("JWakeConfigHelper", paramContext.toString());
      }
      i = paramJSONObject.optInt("app_wakeup_disable", -1);
      if (i == 0) {}
      do
      {
        locala.d = true;
        break;
        if (i == 1)
        {
          locala.d = false;
          break;
        }
      } while (i == 2);
      if (!"disable".equals(paramJSONObject.optString("app_wakeup_config", "enable"))) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      locala.a = bool1;
      boolean bool1 = bool2;
      if (!"disable".equals(paramJSONObject.optString("app_wakeuped_config", "enable"))) {
        bool1 = true;
      }
      locala.b = bool1;
      String str = paramJSONObject.optString("app_package_config", "disable");
      paramContext = str;
      if (str.isEmpty()) {
        paramContext = "disable";
      }
      locala.h = paramContext;
      locala.i = a(paramJSONObject, "app_package_list");
      locala.j = a(paramJSONObject, "app_blacklist");
    }
    catch (JSONException paramJSONObject)
    {
      paramContext = new StringBuilder();
      paramContext.append("parseWakeConfig exception:");
      paramContext.append(paramJSONObject.toString());
      cn.jiguang.ai.a.g("JWakeConfigHelper", paramContext.toString());
    }
    return locala;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aa/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */