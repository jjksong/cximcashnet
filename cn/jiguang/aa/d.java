package cn.jiguang.aa;

import android.content.ComponentName;
import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ai.a;
import cn.jiguang.z.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d
{
  public static List<cn.jiguang.z.c> a(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    cn.jiguang.z.c localc = new cn.jiguang.z.c();
    localc.a = paramString2;
    localc.b = paramString3;
    localc.c = paramInt;
    Object localObject;
    if ((paramString1 != null) && (!TextUtils.isEmpty(paramString1)))
    {
      paramString1 = c.a(paramString1);
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("cache cmd wakeTargets:");
      ((StringBuilder)localObject).append(paramString1);
      a.c("JWakePackageHelper", ((StringBuilder)localObject).toString());
    }
    else
    {
      paramString1 = null;
    }
    int j = 1;
    int i = 1;
    if (paramString1 == null)
    {
      localObject = new ArrayList();
    }
    else
    {
      Iterator localIterator = paramString1.iterator();
      for (;;)
      {
        j = i;
        localObject = paramString1;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject = (cn.jiguang.z.c)localIterator.next();
        if (((cn.jiguang.z.c)localObject).a.equals(paramString2))
        {
          ((cn.jiguang.z.c)localObject).b = paramString3;
          ((cn.jiguang.z.c)localObject).c = paramInt;
          i = 0;
        }
      }
    }
    if (j != 0) {
      ((List)localObject).add(localc);
    }
    return (List<cn.jiguang.z.c>)localObject;
  }
  
  public static JSONArray a(Context paramContext, JSONArray paramJSONArray)
  {
    if (paramJSONArray != null) {
      try
      {
        if (paramJSONArray.length() > 0)
        {
          Object localObject6 = new java/util/HashMap;
          ((HashMap)localObject6).<init>();
          Object localObject5 = new java/util/HashMap;
          ((HashMap)localObject5).<init>();
          Object localObject7;
          Object localObject8;
          for (int i = 0; i < paramJSONArray.length(); i++)
          {
            localObject2 = paramJSONArray.getJSONObject(i);
            localObject1 = ((JSONObject)localObject2).optString("type");
            int j;
            if ("android_awake2".equals(localObject1))
            {
              localObject2 = ((JSONObject)localObject2).optJSONArray("target");
              if ((localObject2 == null) || (((JSONArray)localObject2).length() <= 0)) {
                break;
              }
              for (j = 0; j < ((JSONArray)localObject2).length(); j++)
              {
                localObject1 = ((JSONArray)localObject2).optJSONObject(j);
                localObject3 = ((JSONObject)localObject1).optString("package");
                int k = ((JSONObject)localObject1).optInt("count");
                if (((Map)localObject6).containsKey(localObject3)) {}
                for (localObject1 = Integer.valueOf(((Integer)((Map)localObject6).get(localObject3)).intValue() + k);; localObject1 = Integer.valueOf(k))
                {
                  ((Map)localObject6).put(localObject3, localObject1);
                  break;
                }
              }
            }
            if ("android_awake_target2".equals(localObject1))
            {
              localObject7 = ((JSONObject)localObject2).optString("package");
              if (((JSONObject)localObject2).optBoolean("app_alive")) {
                localObject1 = "active";
              } else {
                localObject1 = "dead";
              }
              j = ((JSONObject)localObject2).optInt("wake_type");
              if (((Map)localObject5).containsKey(localObject7))
              {
                localObject8 = (Map)((Map)localObject5).get(localObject7);
                if (((Map)localObject8).containsKey(localObject1))
                {
                  localObject4 = (Map)((Map)localObject8).get(localObject1);
                  if (((Map)localObject4).containsKey(Integer.valueOf(j)))
                  {
                    localObject3 = Integer.valueOf(((Integer)((Map)localObject4).get(Integer.valueOf(j))).intValue() + 1);
                    localObject2 = Integer.valueOf(j);
                  }
                  for (;;)
                  {
                    ((Map)localObject4).put(localObject2, localObject3);
                    localObject2 = localObject4;
                    break;
                    localObject2 = Integer.valueOf(j);
                    localObject3 = Integer.valueOf(1);
                  }
                }
                for (;;)
                {
                  ((Map)localObject8).put(localObject1, localObject2);
                  break;
                  localObject2 = new java/util/HashMap;
                  ((HashMap)localObject2).<init>();
                  ((HashMap)localObject2).put(Integer.valueOf(j), Integer.valueOf(1));
                }
                ((Map)localObject5).put(localObject7, localObject8);
              }
              else
              {
                localObject2 = new java/util/HashMap;
                ((HashMap)localObject2).<init>();
                ((Map)localObject2).put(Integer.valueOf(j), Integer.valueOf(1));
                localObject3 = new java/util/HashMap;
                ((HashMap)localObject3).<init>();
                ((Map)localObject3).put(localObject1, localObject2);
                ((Map)localObject5).put(localObject7, localObject3);
              }
            }
            else
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append("unkown type :");
              ((StringBuilder)localObject2).append((String)localObject1);
              a.g("JWakePackageHelper", ((StringBuilder)localObject2).toString());
            }
          }
          localObject1 = new org/json/JSONArray;
          ((JSONArray)localObject1).<init>();
          Object localObject2 = new org/json/JSONArray;
          ((JSONArray)localObject2).<init>();
          Object localObject4 = ((Map)localObject6).entrySet().iterator();
          while (((Iterator)localObject4).hasNext())
          {
            localObject6 = (Map.Entry)((Iterator)localObject4).next();
            localObject3 = new org/json/JSONObject;
            ((JSONObject)localObject3).<init>();
            ((JSONObject)localObject3).put("package", ((Map.Entry)localObject6).getKey());
            ((JSONObject)localObject3).put("count", ((Map.Entry)localObject6).getValue());
            ((JSONArray)localObject2).put(localObject3);
          }
          if (((JSONArray)localObject2).length() > 0)
          {
            localObject3 = new org/json/JSONObject;
            ((JSONObject)localObject3).<init>();
            ((JSONObject)localObject3).put("target", localObject2);
            ((JSONObject)localObject3).put("itime", cn.jiguang.f.d.h(paramContext));
            ((JSONObject)localObject3).put("type", "android_awake2");
            ((JSONArray)localObject1).put(localObject3);
          }
          Object localObject3 = new org/json/JSONObject;
          ((JSONObject)localObject3).<init>();
          localObject2 = new org/json/JSONArray;
          ((JSONArray)localObject2).<init>();
          localObject5 = ((Map)localObject5).entrySet().iterator();
          while (((Iterator)localObject5).hasNext())
          {
            localObject6 = (Map.Entry)((Iterator)localObject5).next();
            localObject4 = new org/json/JSONObject;
            ((JSONObject)localObject4).<init>();
            ((JSONObject)localObject4).put("package", ((Map.Entry)localObject6).getKey());
            localObject7 = ((Map)((Map.Entry)localObject6).getValue()).entrySet().iterator();
            while (((Iterator)localObject7).hasNext())
            {
              JSONArray localJSONArray = new org/json/JSONArray;
              localJSONArray.<init>();
              localObject6 = (Map.Entry)((Iterator)localObject7).next();
              localObject8 = ((Map)((Map.Entry)localObject6).getValue()).entrySet().iterator();
              while (((Iterator)localObject8).hasNext())
              {
                JSONObject localJSONObject = new org/json/JSONObject;
                localJSONObject.<init>();
                Map.Entry localEntry = (Map.Entry)((Iterator)localObject8).next();
                localJSONObject.put("wake_type", localEntry.getKey());
                localJSONObject.put("count", localEntry.getValue());
                localJSONArray.put(localJSONObject);
              }
              ((JSONObject)localObject4).put((String)((Map.Entry)localObject6).getKey(), localJSONArray);
            }
            ((JSONArray)localObject2).put(localObject4);
          }
          if (((JSONArray)localObject2).length() > 0)
          {
            ((JSONObject)localObject3).put("from", localObject2);
            ((JSONObject)localObject3).put("itime", cn.jiguang.f.d.h(paramContext));
            ((JSONObject)localObject3).put("type", "android_awake_target2");
            ((JSONArray)localObject1).put(localObject3);
          }
          i = ((JSONArray)localObject1).length();
          if (i > 0) {
            return (JSONArray)localObject1;
          }
        }
      }
      catch (Throwable paramContext)
      {
        Object localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append("merge wake json failed:");
        ((StringBuilder)localObject1).append(paramContext.getMessage());
        a.h("JWakePackageHelper", ((StringBuilder)localObject1).toString());
      }
    }
    return paramJSONArray;
  }
  
  public static JSONObject a(Context paramContext, List<b> paramList)
  {
    try
    {
      JSONObject localJSONObject1 = new org/json/JSONObject;
      localJSONObject1.<init>();
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>();
      boolean bool = false;
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        b localb = (b)localIterator.next();
        JSONObject localJSONObject2 = new org/json/JSONObject;
        localJSONObject2.<init>();
        HashMap localHashMap = localb.b;
        paramList = localHashMap.keySet().iterator();
        while (paramList.hasNext()) {
          if (((Boolean)localHashMap.get(Integer.valueOf(((Integer)paramList.next()).intValue()))).booleanValue()) {
            bool = true;
          }
        }
        localJSONObject2.put("awake_from", paramContext.getApplicationContext().getPackageName());
        localJSONObject2.put("awake_to", localb.a.getPackageName());
        localJSONObject2.put("awake_class", localb.a.getClassName());
        localJSONObject2.put("awake_count", 1);
        localJSONObject2.put("success", bool);
        localJSONArray.put(localJSONObject2);
      }
      localJSONObject1.put("awake_path", localJSONArray);
      return localJSONObject1;
    }
    catch (JSONException paramList)
    {
      paramContext = new StringBuilder();
      paramContext.append("package cmd report json exception:");
      paramContext.append(paramList.getMessage());
      a.g("JWakePackageHelper", paramContext.toString());
    }
    return null;
  }
  
  public static JSONObject a(String paramString, int paramInt, boolean paramBoolean)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    try
    {
      paramString = new org/json/JSONObject;
      paramString.<init>();
      paramString.put("wake_type", paramInt);
      paramString.put("package", str);
      paramString.put("app_alive", paramBoolean);
      return paramString;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
    return null;
  }
  
  public static JSONObject a(List<b> paramList)
  {
    if ((paramList != null) && (paramList.size() != 0))
    {
      JSONArray localJSONArray = new JSONArray();
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        Object localObject = (b)paramList.next();
        try
        {
          if ((((b)localObject).b != null) && (!((b)localObject).b.isEmpty()))
          {
            JSONObject localJSONObject = new org/json/JSONObject;
            localJSONObject.<init>();
            localJSONObject.put("package", ((b)localObject).a.getPackageName());
            localJSONObject.put("count", ((b)localObject).b.size());
            localJSONArray.put(localJSONObject);
          }
        }
        catch (Throwable localThrowable)
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("formatReportData:");
          ((StringBuilder)localObject).append(localThrowable);
          a.i("JWakePackageHelper", ((StringBuilder)localObject).toString());
        }
      }
      paramList = new JSONObject();
      try
      {
        paramList.put("target", localJSONArray);
      }
      catch (JSONException localJSONException)
      {
        localJSONException.printStackTrace();
      }
      return paramList;
    }
    a.c("JWakePackageHelper", "wakeUpResult is empty, no need report");
    return null;
  }
  
  /* Error */
  public static void a(Context paramContext, String paramString, JSONObject paramJSONObject)
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +4 -> 5
    //   4: return
    //   5: new 35	java/lang/StringBuilder
    //   8: astore_3
    //   9: aload_3
    //   10: invokespecial 36	java/lang/StringBuilder:<init>	()V
    //   13: aload_3
    //   14: ldc_w 296
    //   17: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   20: pop
    //   21: aload_3
    //   22: aload_2
    //   23: invokevirtual 297	org/json/JSONObject:toString	()Ljava/lang/String;
    //   26: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: ldc 47
    //   32: aload_3
    //   33: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   36: invokestatic 56	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   39: aload_2
    //   40: ldc -59
    //   42: aload_0
    //   43: invokestatic 203	cn/jiguang/f/d:h	(Landroid/content/Context;)J
    //   46: invokevirtual 206	org/json/JSONObject:put	(Ljava/lang/String;J)Lorg/json/JSONObject;
    //   49: pop
    //   50: aload_2
    //   51: ldc 105
    //   53: aload_1
    //   54: invokevirtual 189	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   57: pop
    //   58: ldc_w 299
    //   61: monitorenter
    //   62: aload_0
    //   63: ldc_w 299
    //   66: invokestatic 304	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONObject;
    //   69: astore_3
    //   70: ldc_w 299
    //   73: monitorexit
    //   74: aload_3
    //   75: astore_1
    //   76: aload_3
    //   77: ifnonnull +11 -> 88
    //   80: new 107	org/json/JSONObject
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 183	org/json/JSONObject:<init>	()V
    //   88: aload_1
    //   89: ldc_w 306
    //   92: invokevirtual 119	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   95: astore 4
    //   97: aload 4
    //   99: astore_3
    //   100: aload 4
    //   102: ifnonnull +11 -> 113
    //   105: new 92	org/json/JSONArray
    //   108: astore_3
    //   109: aload_3
    //   110: invokespecial 173	org/json/JSONArray:<init>	()V
    //   113: aload_3
    //   114: aload_2
    //   115: invokevirtual 195	org/json/JSONArray:put	(Ljava/lang/Object;)Lorg/json/JSONArray;
    //   118: pop
    //   119: aload_1
    //   120: ldc_w 306
    //   123: aload_3
    //   124: invokevirtual 189	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   127: pop
    //   128: ldc_w 299
    //   131: monitorenter
    //   132: aload_0
    //   133: ldc_w 299
    //   136: aload_1
    //   137: invokestatic 309	cn/jiguang/s/b:a	(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)Z
    //   140: pop
    //   141: ldc_w 299
    //   144: monitorexit
    //   145: goto +60 -> 205
    //   148: astore_0
    //   149: ldc_w 299
    //   152: monitorexit
    //   153: aload_0
    //   154: athrow
    //   155: astore_0
    //   156: aload_0
    //   157: invokevirtual 278	org/json/JSONException:printStackTrace	()V
    //   160: goto +45 -> 205
    //   163: astore_0
    //   164: ldc_w 299
    //   167: monitorexit
    //   168: aload_0
    //   169: athrow
    //   170: astore_1
    //   171: new 35	java/lang/StringBuilder
    //   174: dup
    //   175: invokespecial 36	java/lang/StringBuilder:<init>	()V
    //   178: astore_0
    //   179: aload_0
    //   180: ldc_w 311
    //   183: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload_0
    //   188: aload_1
    //   189: invokevirtual 213	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   192: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: ldc 47
    //   198: aload_0
    //   199: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   202: invokestatic 172	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   205: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	206	0	paramContext	Context
    //   0	206	1	paramString	String
    //   0	206	2	paramJSONObject	JSONObject
    //   8	116	3	localObject	Object
    //   95	6	4	localJSONArray	JSONArray
    // Exception table:
    //   from	to	target	type
    //   132	145	148	finally
    //   149	153	148	finally
    //   113	132	155	org/json/JSONException
    //   153	155	155	org/json/JSONException
    //   62	74	163	finally
    //   164	168	163	finally
    //   5	62	170	java/lang/Throwable
    //   80	88	170	java/lang/Throwable
    //   88	97	170	java/lang/Throwable
    //   105	113	170	java/lang/Throwable
    //   113	132	170	java/lang/Throwable
    //   153	155	170	java/lang/Throwable
    //   156	160	170	java/lang/Throwable
    //   168	170	170	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aa/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */