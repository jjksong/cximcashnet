package cn.jiguang.aa;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class c
{
  private static Boolean a;
  
  public static Bundle a(HashMap<String, String> paramHashMap)
  {
    if ((paramHashMap != null) && (!paramHashMap.isEmpty()))
    {
      Bundle localBundle = new Bundle();
      Iterator localIterator = paramHashMap.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localBundle.putString(str, (String)paramHashMap.get(str));
      }
      return localBundle;
    }
    return null;
  }
  
  public static String a(List<cn.jiguang.z.c> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramList.size(); i++)
    {
      String str1 = ((cn.jiguang.z.c)paramList.get(i)).a;
      String str2 = ((cn.jiguang.z.c)paramList.get(i)).b;
      int j = ((cn.jiguang.z.c)paramList.get(i)).c;
      localStringBuilder.append(str1);
      localStringBuilder.append("|");
      localStringBuilder.append(str2);
      localStringBuilder.append("|");
      localStringBuilder.append(j);
      localStringBuilder.append("$");
    }
    return localStringBuilder.toString();
  }
  
  public static List<cn.jiguang.z.c> a(Context paramContext)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localObject1 = paramContext.getPackageManager();
      Object localObject2 = new android/content/Intent;
      ((Intent)localObject2).<init>();
      ((Intent)localObject2).setAction("cn.jpush.android.intent.DaemonService");
      int i = 0;
      localObject2 = ((PackageManager)localObject1).queryIntentServices((Intent)localObject2, 0);
      if ((localObject2 != null) && (((List)localObject2).size() != 0))
      {
        while (i < ((List)localObject2).size())
        {
          Object localObject4 = ((ResolveInfo)((List)localObject2).get(i)).serviceInfo;
          String str = ((ServiceInfo)localObject4).name;
          Object localObject3 = ((ServiceInfo)localObject4).packageName;
          if ((str != null) && (localObject3 != null) && (!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty((CharSequence)localObject3)) && (((ServiceInfo)localObject4).exported) && (((ServiceInfo)localObject4).enabled) && (!paramContext.getPackageName().equals(localObject3)))
          {
            localObject4 = a.a(paramContext, (PackageManager)localObject1, (String)localObject3, str);
            if (localObject4 != null)
            {
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              ((StringBuilder)localObject3).append("wakeTarget:");
              ((StringBuilder)localObject3).append(((cn.jiguang.z.c)localObject4).toString());
              cn.jiguang.ai.a.c("JWakeHelper", ((StringBuilder)localObject3).toString());
              localArrayList.add(localObject4);
            }
          }
          i++;
        }
        return localArrayList;
      }
      return null;
    }
    catch (Throwable paramContext)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("getWakeTargetList throwable:");
      ((StringBuilder)localObject1).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWakeHelper", ((StringBuilder)localObject1).toString());
    }
    return null;
  }
  
  public static List<String> a(cn.jiguang.z.a parama, List<String> paramList)
  {
    if (parama == null) {
      return paramList;
    }
    return c(parama, b(parama, paramList));
  }
  
  public static List<cn.jiguang.z.c> a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    String[] arrayOfString = paramString.split("\\$");
    ArrayList localArrayList = new ArrayList();
    int j = arrayOfString.length;
    for (int i = 0; i < j; i++)
    {
      Object localObject = arrayOfString[i].split("\\|");
      paramString = localObject[0];
      String str = localObject[1];
      int k = Integer.valueOf(localObject[2]).intValue();
      localObject = new cn.jiguang.z.c();
      ((cn.jiguang.z.c)localObject).a = paramString;
      ((cn.jiguang.z.c)localObject).b = str;
      ((cn.jiguang.z.c)localObject).c = k;
      localArrayList.add(localObject);
    }
    return localArrayList;
  }
  
  private static List<String> a(List<String> paramList1, List<String> paramList2)
  {
    return a(paramList1, paramList2, false);
  }
  
  private static List<String> a(List<String> paramList1, List<String> paramList2, boolean paramBoolean)
  {
    if ((paramList1 != null) && (paramList1.size() != 0))
    {
      ArrayList localArrayList = new ArrayList();
      paramList2 = paramList2.iterator();
      while (paramList2.hasNext())
      {
        String str = (String)paramList2.next();
        StringBuilder localStringBuilder;
        if (paramList1.contains(str)) {
          if (paramBoolean)
          {
            localStringBuilder = new StringBuilder();
            localStringBuilder.append(str);
            localStringBuilder.append(" in the white list");
            cn.jiguang.ai.a.c("JWakeHelper", localStringBuilder.toString());
            localArrayList.add(str);
          }
          else
          {
            localStringBuilder = new StringBuilder();
            localStringBuilder.append(str);
            localStringBuilder.append(" in the black list");
            cn.jiguang.ai.a.c("JWakeHelper", localStringBuilder.toString());
            continue;
          }
        }
        if (!paramBoolean)
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(str);
          localStringBuilder.append(" not in the global black list");
          cn.jiguang.ai.a.c("JWakeHelper", localStringBuilder.toString());
          localArrayList.add(str);
        }
      }
      return localArrayList;
    }
    return paramList2;
  }
  
  public static String b(HashMap<String, String> paramHashMap)
  {
    if ((paramHashMap != null) && (!paramHashMap.isEmpty()))
    {
      Uri.Builder localBuilder = new Uri.Builder();
      Iterator localIterator = paramHashMap.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localBuilder.appendQueryParameter(str, (String)paramHashMap.get(str));
      }
      return localBuilder.toString();
    }
    return null;
  }
  
  private static List<String> b(cn.jiguang.z.a parama, List<String> paramList)
  {
    if ((!TextUtils.isEmpty(parama.h)) && (!parama.h.equals("disable")))
    {
      List localList = parama.i;
      parama = parama.h;
      int i = -1;
      int j = parama.hashCode();
      if (j != -1321148966)
      {
        if ((j == 1942574248) && (parama.equals("include"))) {
          i = 1;
        }
      }
      else if (parama.equals("exclude")) {
        i = 0;
      }
      switch (i)
      {
      default: 
        return paramList;
      case 1: 
        return b(localList, paramList);
      }
      return a(localList, paramList);
    }
    return paramList;
  }
  
  private static List<String> b(List<String> paramList1, List<String> paramList2)
  {
    return a(paramList1, paramList2, true);
  }
  
  public static boolean b(Context paramContext)
  {
    try
    {
      if (a != null) {
        return a.booleanValue();
      }
      if (paramContext == null) {
        return true;
      }
      Object localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).setPackage(paramContext.getPackageName());
      ((Intent)localObject).setAction("cn.jpush.android.WAKED_NOT_REPORT");
      paramContext = paramContext.getPackageManager().queryIntentServices((Intent)localObject, 0);
      if ((paramContext != null) && (!paramContext.isEmpty())) {}
      for (paramContext = Boolean.valueOf(false);; paramContext = Boolean.valueOf(true))
      {
        a = paramContext;
        break;
      }
      return a.booleanValue();
    }
    catch (Throwable paramContext)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("isUserReportEnable throwable:");
      ((StringBuilder)localObject).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWakeHelper", ((StringBuilder)localObject).toString());
    }
  }
  
  private static List<String> c(cn.jiguang.z.a parama, List<String> paramList)
  {
    return a(parama.j, paramList);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aa/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */