package cn.jiguang.aa;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.text.TextUtils;
import cn.jiguang.f.d;
import cn.jiguang.z.c;
import cn.jpush.android.service.DaemonService;
import cn.jpush.android.service.DownloadProvider;
import java.util.Iterator;
import java.util.List;

public class a
{
  private static Boolean a;
  private static Boolean b;
  private static final String c = "Xiaomi".toLowerCase();
  
  private static Intent a(Context paramContext, PackageManager paramPackageManager, String paramString)
  {
    try
    {
      paramContext = new android/content/Intent;
      paramContext.<init>();
      paramContext.setAction("cn.jpush.android.intent.DActivity");
      paramContext.addCategory(paramString);
      paramContext.setPackage(paramString);
      paramPackageManager = paramPackageManager.resolveActivity(paramContext, 0);
      if (paramPackageManager == null)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("getDActivity resolveInfo was null from:");
        paramContext.append(paramString);
        cn.jiguang.ai.a.g("JWakeComponentHelper", paramContext.toString());
        return null;
      }
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("target actvity name:");
      ((StringBuilder)localObject).append(paramPackageManager.activityInfo.name);
      ((StringBuilder)localObject).append(", theme:");
      ((StringBuilder)localObject).append(paramPackageManager.activityInfo.theme);
      ((StringBuilder)localObject).append(",exported:");
      ((StringBuilder)localObject).append(paramPackageManager.activityInfo.exported);
      cn.jiguang.ai.a.c("JWakeComponentHelper", ((StringBuilder)localObject).toString());
      if ((paramPackageManager.activityInfo.exported) && (paramPackageManager.activityInfo.enabled) && ("jpush.custom".equals(paramPackageManager.activityInfo.taskAffinity)))
      {
        if (paramPackageManager.activityInfo.theme != 16973840)
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append(paramPackageManager.activityInfo.name);
          paramContext.append("activity theme must config as @android:style/Theme.Translucent.NoTitleBar");
          cn.jiguang.ai.a.g("JWakeComponentHelper", paramContext.toString());
          return null;
        }
        localObject = new android/content/ComponentName;
        ((ComponentName)localObject).<init>(paramString, paramPackageManager.activityInfo.name);
        paramContext.setComponent((ComponentName)localObject);
        paramPackageManager = new java/lang/StringBuilder;
        paramPackageManager.<init>();
        paramPackageManager.append("dIntent:");
        paramPackageManager.append(paramContext);
        cn.jiguang.ai.a.c("JWakeComponentHelper", paramPackageManager.toString());
        return paramContext;
      }
      cn.jiguang.ai.a.g("JWakeComponentHelper", "activity muse be exported and enabled, and taskAffinity must be jpush.custom");
      return null;
    }
    catch (Throwable paramContext)
    {
      paramPackageManager = new StringBuilder();
      paramPackageManager.append("get deeplink activity error#");
      paramPackageManager.append(paramContext);
      cn.jiguang.ai.a.g("JWakeComponentHelper", paramPackageManager.toString());
    }
    return null;
  }
  
  static c a(Context paramContext, PackageManager paramPackageManager, String paramString1, String paramString2)
  {
    if ((paramPackageManager != null) && (!TextUtils.isEmpty(paramString1)))
    {
      try
      {
        Object localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(paramString1);
        ((StringBuilder)localObject1).append(".permission.JPUSH_MESSAGE");
        int k = paramPackageManager.checkPermission(((StringBuilder)localObject1).toString(), paramString1);
        localObject1 = paramPackageManager.getApplicationInfo(paramString1, 128);
        if ((localObject1 == null) || (((ApplicationInfo)localObject1).metaData == null)) {
          break label361;
        }
        Object localObject2 = d.b(paramContext);
        Object localObject3 = new android/content/Intent;
        ((Intent)localObject3).<init>();
        ((Intent)localObject3).setClassName(paramString1, "cn.jpush.android.service.PushService");
        int j = 0;
        localObject3 = paramPackageManager.queryIntentServices((Intent)localObject3, 0);
        int i = j;
        if (localObject3 != null)
        {
          i = j;
          if (((List)localObject3).size() != 0) {
            i = 1;
          }
        }
        if ((k != 0) || (i == 0) || (TextUtils.isEmpty((CharSequence)localObject2)) || (((String)localObject2).length() != 24)) {
          break label361;
        }
        localObject2 = new cn/jiguang/z/c;
        ((c)localObject2).<init>(paramString1, paramString2, ((ApplicationInfo)localObject1).targetSdkVersion);
        paramString2 = d.a(paramContext, paramString1, DownloadProvider.class);
        if ((paramString2 instanceof ProviderInfo))
        {
          localObject1 = (ProviderInfo)paramString2;
          if ((((ProviderInfo)localObject1).exported) && (((ProviderInfo)localObject1).enabled) && (((ProviderInfo)localObject1).authority != null))
          {
            paramString2 = new java/lang/StringBuilder;
            paramString2.<init>();
            paramString2.append(paramString1);
            paramString2.append(".DownloadProvider");
            if (TextUtils.equals(paramString2.toString(), ((ProviderInfo)localObject1).authority)) {
              ((c)localObject2).d = ((ProviderInfo)localObject1).authority;
            }
          }
        }
        if ((b(paramContext)) && (!a(paramContext))) {
          ((c)localObject2).e = a(paramContext, paramPackageManager, paramString1);
        }
        return (c)localObject2;
      }
      catch (Throwable paramPackageManager)
      {
        paramContext = new StringBuilder();
        paramContext.append("checkWhetherToStart throwable:");
        paramPackageManager = paramPackageManager.getMessage();
      }
      catch (PackageManager.NameNotFoundException paramPackageManager)
      {
        paramContext = new StringBuilder();
        paramContext.append("checkWhetherToStart exception:");
        paramPackageManager = paramPackageManager.toString();
      }
      paramContext.append(paramPackageManager);
      cn.jiguang.ai.a.g("JWakeComponentHelper", paramContext.toString());
    }
    label361:
    return null;
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    a(paramContext, paramBoolean, "cn.jpush.android.intent.DaemonService", DaemonService.class);
    a(paramContext, paramBoolean, DownloadProvider.class);
  }
  
  private static void a(Context paramContext, boolean paramBoolean, Class paramClass)
  {
    if (paramContext == null) {}
    try
    {
      cn.jiguang.ai.a.h("JWakeComponentHelper", "context is null, give up setComponentEnabled");
      return;
    }
    catch (Throwable paramContext)
    {
      PackageManager localPackageManager;
      int i;
      ComponentName localComponentName;
      paramClass = new StringBuilder();
      paramClass.append("setContentProviderEnabled throwable:");
      paramClass.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWakeComponentHelper", paramClass.toString());
    }
    localPackageManager = paramContext.getApplicationContext().getPackageManager();
    if (localPackageManager == null)
    {
      cn.jiguang.ai.a.h("JWakeComponentHelper", "PackageManager is null, give up setComponentEnabled");
      return;
    }
    if (paramBoolean) {
      i = 1;
    } else {
      i = 2;
    }
    localComponentName = new android/content/ComponentName;
    localComponentName.<init>(paramContext, paramClass);
    if (localPackageManager.getComponentEnabledSetting(localComponentName) == i)
    {
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append(localComponentName.getClassName());
      paramContext.append(" enabled is :");
      paramContext.append(paramBoolean);
      paramContext.append(", no need repeat set.");
      cn.jiguang.ai.a.c("JWakeComponentHelper", paramContext.toString());
      return;
    }
    paramContext = new java/lang/StringBuilder;
    paramContext.<init>();
    paramContext.append(localComponentName);
    paramContext.append(" setDownloadProviderEnabledSetting newState: ");
    paramContext.append(i);
    cn.jiguang.ai.a.c("JWakeComponentHelper", paramContext.toString());
    localPackageManager.setComponentEnabledSetting(localComponentName, i, 1);
  }
  
  private static void a(Context paramContext, boolean paramBoolean, String paramString, Class paramClass)
  {
    if (paramContext == null) {}
    try
    {
      cn.jiguang.ai.a.h("JWakeComponentHelper", "context is null, give up setComponentEnabled");
      return;
    }
    catch (Throwable paramContext)
    {
      PackageManager localPackageManager;
      int i;
      Object localObject1;
      paramString = new StringBuilder();
      paramString.append("setServiceEnabled throwable:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JWakeComponentHelper", paramString.toString());
    }
    localPackageManager = paramContext.getApplicationContext().getPackageManager();
    if (localPackageManager == null)
    {
      cn.jiguang.ai.a.h("JWakeComponentHelper", "PackageManager is null, give up setComponentEnabled");
      return;
    }
    paramContext = paramContext.getPackageName();
    if (paramBoolean) {
      i = 1;
    } else {
      i = 2;
    }
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>();
    ((Intent)localObject1).setPackage(paramContext);
    ((Intent)localObject1).setAction(paramString);
    paramContext = localPackageManager.queryIntentServices((Intent)localObject1, 512);
    if ((paramContext != null) && (!paramContext.isEmpty())) {
      paramContext = paramContext.iterator();
    }
    while (paramContext.hasNext())
    {
      paramString = (ResolveInfo)paramContext.next();
      if (paramString != null)
      {
        Object localObject2 = paramString.serviceInfo;
        if (localObject2 != null)
        {
          paramString = ((ServiceInfo)localObject2).name;
          boolean bool = TextUtils.isEmpty(paramString);
          if (!bool)
          {
            try
            {
              localObject1 = Class.forName(paramString);
              if (localObject1 == null) {
                continue;
              }
              if (paramClass.isAssignableFrom((Class)localObject1))
              {
                localObject1 = new android/content/ComponentName;
                ((ComponentName)localObject1).<init>(((ServiceInfo)localObject2).packageName, ((ServiceInfo)localObject2).name);
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                ((StringBuilder)localObject2).append(localObject1);
                ((StringBuilder)localObject2).append(" setComponentEnabledSetting newState: ");
                ((StringBuilder)localObject2).append(i);
                cn.jiguang.ai.a.c("JWakeComponentHelper", ((StringBuilder)localObject2).toString());
                if (localPackageManager.getComponentEnabledSetting((ComponentName)localObject1) == i)
                {
                  localObject1 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject1).<init>();
                  ((StringBuilder)localObject1).append("DaemonService  enabled is :");
                  ((StringBuilder)localObject1).append(paramBoolean);
                  ((StringBuilder)localObject1).append(", no need repeat set.");
                  cn.jiguang.ai.a.c("JWakeComponentHelper", ((StringBuilder)localObject1).toString());
                  break;
                }
                localPackageManager.setComponentEnabledSetting((ComponentName)localObject1, i, 1);
                continue;
              }
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              ((StringBuilder)localObject1).append("give up setting, as ");
              ((StringBuilder)localObject1).append(paramString);
              ((StringBuilder)localObject1).append(" is not extend from: ");
              ((StringBuilder)localObject1).append(paramClass.getName());
              cn.jiguang.ai.a.h("JWakeComponentHelper", ((StringBuilder)localObject1).toString());
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
              StringBuilder localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>();
              localStringBuilder.append("cant't find service class:");
              localStringBuilder.append(paramString);
              cn.jiguang.ai.a.g("JWakeComponentHelper", localStringBuilder.toString());
            }
            continue;
            cn.jiguang.ai.a.g("JWakeComponentHelper", "cant't find DaemonService");
          }
        }
      }
    }
  }
  
  public static boolean a(Context paramContext)
  {
    Boolean localBoolean = b;
    if (localBoolean != null) {
      return localBoolean.booleanValue();
    }
    boolean bool;
    if (c(paramContext)) {
      bool = true;
    } else {
      bool = false;
    }
    b = Boolean.valueOf(bool);
    return b.booleanValue();
  }
  
  private static boolean a(Context paramContext, String paramString)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramString);
      localIntent.addCategory(paramContext.getPackageName());
      boolean bool = localPackageManager.queryIntentActivities(localIntent, 0).isEmpty();
      return bool ^ true;
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("hasActivityIntentFilter error:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.h("JWakeComponentHelper", paramString.toString());
    }
    return false;
  }
  
  public static boolean b(Context paramContext)
  {
    Boolean localBoolean = a;
    if (localBoolean != null) {
      return localBoolean.booleanValue();
    }
    boolean bool;
    if (a(paramContext, "cn.jpush.android.intent.DActivity")) {
      bool = true;
    } else {
      bool = false;
    }
    a = Boolean.valueOf(bool);
    return a.booleanValue();
  }
  
  private static boolean c(Context paramContext)
  {
    try
    {
      paramContext = Build.MANUFACTURER;
      if (!TextUtils.isEmpty(paramContext))
      {
        boolean bool = TextUtils.equals(c, paramContext.toLowerCase());
        if (bool) {
          return true;
        }
      }
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("get MANUFACTURER failed - error:");
      paramContext.append(localThrowable);
      cn.jiguang.ai.a.h("JWakeComponentHelper", paramContext.toString());
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aa/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */