package cn.jiguang.w;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import android.text.TextUtils;
import cn.jiguang.f.b;
import cn.jiguang.f.d;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.json.JSONObject;

public class a
{
  public static void a(Context paramContext, int paramInt)
  {
    String str = b.e(paramContext);
    boolean bool4 = new a().a(paramContext);
    boolean bool1 = TextUtils.isEmpty(str);
    boolean bool3 = true;
    if (bool1)
    {
      bool1 = bool3;
    }
    else
    {
      boolean bool2;
      if (TextUtils.equals("ON", str))
      {
        bool1 = false;
        bool2 = true;
      }
      else
      {
        if (TextUtils.equals("OFF", str)) {
          bool1 = false;
        } else {
          bool1 = true;
        }
        bool2 = false;
      }
      if (!bool1)
      {
        if (bool2 != bool4) {
          bool1 = bool3;
        } else {
          bool1 = false;
        }
      }
      else {
        cn.jiguang.ai.a.c("JNotificationState", "notification state do not changed");
      }
    }
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("lastCacheNotificationState:");
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(",currentNotificationSate:");
    ((StringBuilder)localObject).append(bool4);
    ((StringBuilder)localObject).append(",isNeedReport:");
    ((StringBuilder)localObject).append(bool1);
    ((StringBuilder)localObject).append(",triggerScene:");
    ((StringBuilder)localObject).append(paramInt);
    cn.jiguang.ai.a.c("JNotificationState", ((StringBuilder)localObject).toString());
    if (bool1) {
      try
      {
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>();
        ((JSONObject)localObject).put("notification_state", bool4);
        ((JSONObject)localObject).put("imei", d.e(paramContext, d.e(paramContext, "")));
        ((JSONObject)localObject).put("device_id", d.g(paramContext));
        ((JSONObject)localObject).put("trigger_scene", paramInt);
        d.a(paramContext, (JSONObject)localObject, "android_notification_state");
        d.a(paramContext, localObject);
        if (bool4) {
          localObject = "ON";
        } else {
          localObject = "OFF";
        }
        b.o(paramContext, (String)localObject);
      }
      catch (Throwable localThrowable)
      {
        paramContext = new StringBuilder();
        paramContext.append("report notification state failed, error:");
        paramContext.append(localThrowable.getMessage());
        cn.jiguang.ai.a.g("JNotificationState", paramContext.toString());
      }
    }
    cn.jiguang.ai.a.c("JNotificationState", "do not need report notification state");
  }
  
  private boolean a(Context paramContext)
  {
    if (Build.VERSION.SDK_INT >= 24) {
      return b(paramContext);
    }
    return c(paramContext);
  }
  
  @TargetApi(24)
  private boolean b(Context paramContext)
  {
    try
    {
      boolean bool = ((NotificationManager)paramContext.getSystemService("notification")).areNotificationsEnabled();
      return bool;
    }
    catch (Throwable paramContext)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("invoke areNotificationsEnabled method failed, error:");
      localStringBuilder.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JNotificationState", localStringBuilder.toString());
    }
    return true;
  }
  
  private boolean c(Context paramContext)
  {
    boolean bool = true;
    try
    {
      localObject1 = (AppOpsManager)paramContext.getSystemService("appops");
      Object localObject2 = paramContext.getApplicationInfo();
      paramContext = paramContext.getApplicationContext().getPackageName();
      int i = ((ApplicationInfo)localObject2).uid;
      localObject2 = Class.forName(AppOpsManager.class.getName());
      i = ((Integer)((Class)localObject2).getMethod("checkOpNoThrow", new Class[] { Integer.TYPE, Integer.TYPE, String.class }).invoke(localObject1, new Object[] { Integer.valueOf(((Integer)((Class)localObject2).getDeclaredField("OP_POST_NOTIFICATION").get(Integer.class)).intValue()), Integer.valueOf(i), paramContext })).intValue();
      if (i != 0) {
        bool = false;
      }
      return bool;
    }
    catch (Throwable paramContext)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("getNotificationStateCommon failed, other error:");
      ((StringBuilder)localObject1).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("JNotificationState", ((StringBuilder)localObject1).toString());
      return true;
    }
    catch (ClassNotFoundException|NoSuchMethodException|NoSuchFieldException|InvocationTargetException|IllegalAccessException|RuntimeException paramContext)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/w/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */