package cn.jiguang.m;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import cn.jiguang.s.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a
{
  public static String a(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramInt & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramInt >> 8 & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramInt >> 16 & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramInt >> 24 & 0xFF);
    return localStringBuffer.toString();
  }
  
  public static List<cn.jiguang.l.a> a(String paramString)
  {
    Object localObject1 = d.a(new String[] { "cat /proc/net/arp" }, 1);
    if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
    {
      ArrayList localArrayList = new ArrayList();
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (String)((Iterator)localObject1).next();
        if (!TextUtils.isEmpty((CharSequence)localObject2))
        {
          localObject2 = b((String)localObject2);
          if ((localObject2 != null) && (((cn.jiguang.l.a)localObject2).c.equals("0x2")) && (!paramString.equals(((cn.jiguang.l.a)localObject2).a)) && (!((cn.jiguang.l.a)localObject2).d.equals("00:00:00:00:00:00"))) {
            localArrayList.add(localObject2);
          }
        }
      }
      return localArrayList;
    }
    cn.jiguang.ai.a.g("JArpHelper", "execute command failed");
    return null;
  }
  
  public static void a(String paramString, byte[] paramArrayOfByte)
  {
    new a(paramString, 300).a(paramArrayOfByte, 0, 255);
  }
  
  public static byte[] a(long paramLong)
  {
    return new byte[] { (byte)(int)(paramLong & 0xFF), (byte)(int)(paramLong >> 8 & 0xFF), (byte)(int)(paramLong >> 16 & 0xFF), (byte)(int)(paramLong >> 24 & 0xFF) };
  }
  
  private static cn.jiguang.l.a b(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    byte[] arrayOfByte = paramString.getBytes();
    cn.jiguang.l.a locala = new cn.jiguang.l.a();
    int m = 0;
    int k = 0;
    int i = 0;
    while (m < arrayOfByte.length - 1)
    {
      int n = m + 1;
      m = n;
      if (arrayOfByte[n] == 32)
      {
        m = n - k;
        int j = i;
        if (m > 1)
        {
          paramString = new String(arrayOfByte, k, m);
          if (i == 0)
          {
            locala.a = paramString;
          }
          else if (i == 1)
          {
            locala.b = paramString;
          }
          else if (i == 2)
          {
            locala.c = paramString;
          }
          else if (i == 3)
          {
            locala.d = paramString;
            break;
          }
          j = i + 1;
        }
        k = n + 1;
        m = n;
        i = j;
      }
    }
    return locala;
  }
  
  private static String b(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(paramArrayOfByte[0] & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramArrayOfByte[1] & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramArrayOfByte[2] & 0xFF);
    localStringBuffer.append('.');
    localStringBuffer.append(paramArrayOfByte[3] & 0xFF);
    return localStringBuffer.toString();
  }
  
  private static class a
  {
    HandlerThread a;
    Handler b;
    private int c;
    private String d;
    
    a(String paramString, int paramInt)
    {
      this.c = paramInt;
      this.d = paramString;
      this.a = new HandlerThread("ping timer");
      this.a.start();
      this.b = new Handler(this.a.getLooper(), new Handler.Callback()
      {
        public boolean handleMessage(Message paramAnonymousMessage)
        {
          if ((paramAnonymousMessage != null) && (paramAnonymousMessage.what == 1))
          {
            paramAnonymousMessage = (Thread)paramAnonymousMessage.obj;
            if (paramAnonymousMessage != null) {
              paramAnonymousMessage.interrupt();
            }
          }
          return false;
        }
      });
    }
    
    private void b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      byte[] arrayOfByte = new byte[4];
      arrayOfByte[0] = paramArrayOfByte[0];
      arrayOfByte[1] = paramArrayOfByte[1];
      arrayOfByte[2] = paramArrayOfByte[2];
      arrayOfByte[3] = 0;
      Thread localThread = Thread.currentThread();
      while (paramInt1 < paramInt2)
      {
        arrayOfByte[3] = ((byte)paramInt1);
        if (arrayOfByte[3] != paramArrayOfByte[3])
        {
          String str = a.a(arrayOfByte);
          if (!str.equalsIgnoreCase(this.d))
          {
            this.b.removeCallbacksAndMessages(null);
            Message localMessage = this.b.obtainMessage(1);
            localMessage.obj = localThread;
            Object localObject = new Bundle();
            ((Bundle)localObject).putString("ip", str);
            localMessage.setData((Bundle)localObject);
            this.b.sendMessageDelayed(localMessage, this.c);
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append("ping -c 1 -w 1 ");
            ((StringBuilder)localObject).append(str);
            d.a(new String[] { ((StringBuilder)localObject).toString() }, 0);
          }
        }
        paramInt1++;
      }
    }
    
    void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      b(paramArrayOfByte, paramInt1, paramInt2);
      this.a.quit();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/m/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */