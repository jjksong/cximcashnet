package cn.jiguang.e;

import android.content.Context;
import android.text.TextUtils;

public class c
{
  private static volatile Long a;
  private static volatile Long b;
  
  public static long a(Context paramContext, long paramLong)
  {
    return (paramLong + c(paramContext)) / 1000L;
  }
  
  public static boolean a(Context paramContext)
  {
    if (((Long)b.a(paramContext, a.c())).longValue() <= 0L) {}
    for (paramContext = "isValidRegistered uid <= 0";; paramContext = "isValidRegistered regId is empty")
    {
      cn.jiguang.ac.c.a("SpHelper", paramContext);
      return false;
      if (!TextUtils.isEmpty((String)b.a(paramContext, a.d()))) {
        break;
      }
    }
    return true;
  }
  
  public static long b(Context paramContext)
  {
    return a(paramContext, System.currentTimeMillis());
  }
  
  public static void b(Context paramContext, long paramLong)
  {
    if (paramLong > 0L)
    {
      long l = System.currentTimeMillis();
      a = Long.valueOf(paramLong);
      b = Long.valueOf(l);
      b.a(paramContext, new a[] { a.h().a(Long.valueOf(paramLong)), a.g().a(Long.valueOf(l)) });
    }
  }
  
  public static long c(Context paramContext)
  {
    if ((a != null) && (b != null)) {
      return a.longValue() - b.longValue();
    }
    long l1 = ((Long)b.a(paramContext, a.g())).longValue();
    long l2 = ((Long)b.a(paramContext, a.h())).longValue();
    if ((l1 != 0L) && (l2 != 0L))
    {
      a = Long.valueOf(l2);
      b = Long.valueOf(l1);
      return l2 - l1;
    }
    return 0L;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/e/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */