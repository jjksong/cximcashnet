package cn.jiguang.e;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.text.TextUtils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class b
{
  private static final Map<String, SharedPreferences> a = new ConcurrentHashMap();
  
  public static <T> T a(Context paramContext, a<T> parama)
  {
    paramContext = b(paramContext, parama);
    if (paramContext != null) {
      return paramContext;
    }
    return (T)parama.c;
  }
  
  private static <T> T a(SharedPreferences paramSharedPreferences, String paramString, T paramT)
  {
    if ((paramSharedPreferences != null) && (paramSharedPreferences.contains(paramString))) {}
    try
    {
      if ((paramT instanceof Boolean)) {
        return Boolean.valueOf(paramSharedPreferences.getBoolean(paramString, ((Boolean)paramT).booleanValue()));
      }
      if ((paramT instanceof String)) {
        return paramSharedPreferences.getString(paramString, (String)paramT);
      }
      if ((paramT instanceof Integer)) {
        return Integer.valueOf(paramSharedPreferences.getInt(paramString, ((Integer)paramT).intValue()));
      }
      if ((paramT instanceof Long)) {
        return Long.valueOf(paramSharedPreferences.getLong(paramString, ((Long)paramT).longValue()));
      }
      if ((paramT instanceof Float))
      {
        float f = paramSharedPreferences.getFloat(paramString, ((Float)paramT).floatValue());
        return Float.valueOf(f);
      }
    }
    catch (Throwable paramSharedPreferences)
    {
      for (;;) {}
    }
    return null;
  }
  
  private static <T> void a(Context paramContext, a<T> parama1, a<T> parama2)
  {
    if (b(paramContext, parama1) == null)
    {
      Object localObject = b(paramContext, parama2);
      if (localObject != null)
      {
        a(paramContext, new a[] { parama1.a(localObject) });
        a(paramContext, new a[] { parama2.a(null) });
      }
    }
  }
  
  public static void a(Context paramContext, String paramString)
  {
    paramContext = c(paramContext, paramString);
    if (paramContext != null) {
      paramContext.edit().clear().apply();
    }
  }
  
  public static void a(Context paramContext, a<?>... paramVarArgs)
  {
    if ((paramVarArgs != null) && (paramVarArgs.length > 0))
    {
      int i = 0;
      paramContext = c(paramContext, paramVarArgs[0].a);
      if (paramContext != null)
      {
        paramContext = paramContext.edit();
        int j = paramVarArgs.length;
        while (i < j)
        {
          a<?> locala = paramVarArgs[i];
          a(paramContext, locala.b, locala.c);
          i++;
        }
        paramContext.commit();
      }
    }
  }
  
  private static <T> void a(SharedPreferences.Editor paramEditor, String paramString, T paramT)
  {
    if (paramEditor != null) {
      if (paramT == null) {
        paramEditor.remove(paramString);
      } else if ((paramT instanceof Boolean)) {
        paramEditor.putBoolean(paramString, ((Boolean)paramT).booleanValue());
      } else if ((paramT instanceof String)) {
        paramEditor.putString(paramString, (String)paramT);
      } else if ((paramT instanceof Integer)) {
        paramEditor.putInt(paramString, ((Integer)paramT).intValue());
      } else if ((paramT instanceof Long)) {
        paramEditor.putLong(paramString, ((Long)paramT).longValue());
      } else if ((paramT instanceof Float)) {
        paramEditor.putFloat(paramString, ((Float)paramT).floatValue());
      }
    }
  }
  
  private static SharedPreferences b(Context paramContext, String paramString)
  {
    paramContext = cn.jiguang.a.a.a(paramContext);
    if (paramContext != null)
    {
      if (Build.VERSION.SDK_INT >= 11) {
        paramContext.getSharedPreferences(paramString, 4);
      }
      return paramContext.getSharedPreferences(paramString, 0);
    }
    return null;
  }
  
  public static <T> T b(Context paramContext, a<T> parama)
  {
    Object localObject2 = a(c(paramContext, parama.a), parama.b, parama.c);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = localObject2;
      if (parama.d) {
        localObject1 = a(b(paramContext, parama.a), parama.b, parama.c);
      }
    }
    if (localObject1 != null)
    {
      parama.a(localObject1);
      return (T)localObject1;
    }
    return null;
  }
  
  private static SharedPreferences c(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    SharedPreferences localSharedPreferences2 = (SharedPreferences)a.get(paramString);
    SharedPreferences localSharedPreferences1 = localSharedPreferences2;
    if (localSharedPreferences2 == null)
    {
      paramContext = cn.jiguang.a.a.a(paramContext);
      localSharedPreferences1 = localSharedPreferences2;
      if (paramContext != null)
      {
        localSharedPreferences1 = paramContext.getSharedPreferences(paramString, 0);
        a.put(paramString, localSharedPreferences1);
        d(paramContext, paramString);
      }
    }
    return localSharedPreferences1;
  }
  
  private static void d(Context paramContext, String paramString)
  {
    Object localObject = (String)a(paramContext, a.v());
    if (((TextUtils.isEmpty((CharSequence)localObject)) || (((String)localObject).startsWith("1."))) && ("2.0.0".startsWith("2.")))
    {
      if (paramString.equals(a.e))
      {
        a(paramContext, a.i(), a.i().a("cn.jpush.android.user.profile"));
        a(paramContext, a.j(), a.j().a("cn.jpush.android.user.profile"));
        localObject = a.k();
        paramString = a.k().a("cn.jpush.android.user.profile");
      }
      for (;;)
      {
        a(paramContext, (a)localObject, paramString);
        break;
        if (paramString.equals("cn.jiguang.sdk.user.set.profile"))
        {
          a(paramContext, a.l(), a.l().a("cn.jpush.preferences.v2"));
          localObject = a.m();
          paramString = a.m().a("cn.jpush.android.user.profile");
        }
        else
        {
          if (!paramString.equals("cn.jiguang.sdk.user.profile")) {
            break;
          }
          a(paramContext, a.c(), a.c().a("cn.jpush.android.user.profile").b("device_uid"));
          a(paramContext, a.d(), a.d().a("cn.jpush.android.user.profile").b("device_registration_id"));
          localObject = a.e();
          paramString = a.e().a("cn.jpush.android.user.profile").b("device_password");
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/e/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */