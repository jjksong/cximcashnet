package cn.jiguang.e;

public class a<T>
{
  public static String e = "cn.jiguang.sdk.share.profile";
  public static String f = "cn.jpush.preferences.v2";
  String a;
  String b;
  T c;
  boolean d;
  
  public a(String paramString1, String paramString2, T paramT)
  {
    this.a = paramString1;
    this.b = paramString2;
    if (paramT != null)
    {
      this.c = paramT;
      return;
    }
    throw new IllegalArgumentException("default value can not be null");
  }
  
  public static a<String> a()
  {
    return new a("cn.jpush.android.user.profile", "devcie_id_generated", "");
  }
  
  public static a<String> a(boolean paramBoolean)
  {
    String str;
    if (paramBoolean) {
      str = "default_https_report";
    } else {
      str = "default_http_report";
    }
    return new a("cn.jiguang.sdk.address", str, "").y();
  }
  
  public static a<Boolean> b()
  {
    return new a("cn.jpush.android.user.profile", "upload_crash", Boolean.valueOf(true));
  }
  
  public static a<Long> c()
  {
    return new a("cn.jiguang.sdk.user.profile", "key_uid", Long.valueOf(0L)).y();
  }
  
  public static a<String> d()
  {
    return new a("cn.jiguang.sdk.user.profile", "key_rid", "").y();
  }
  
  public static a<String> e()
  {
    return new a("cn.jiguang.sdk.user.profile", "key_pwd", "").y();
  }
  
  public static a<Integer> f()
  {
    return new a("cn.jiguang.sdk.user.profile", "idc", Integer.valueOf(-1)).y();
  }
  
  public static a<Long> g()
  {
    return new a("cn.jiguang.sdk.user.profile", "login_local_time", Long.valueOf(-1L));
  }
  
  public static a<Long> h()
  {
    return new a("cn.jiguang.sdk.user.profile", "login_server_time", Long.valueOf(-1L));
  }
  
  public static a<String> i()
  {
    return new a(e, "key_share_process_uuid", "").y();
  }
  
  public static a<Long> j()
  {
    return new a(e, "key_share_process_uuid_creattime", Long.valueOf(-1L)).y();
  }
  
  public static a<Integer> k()
  {
    return new a(e, "sp_state", Integer.valueOf(-1)).y();
  }
  
  public static a<String> l()
  {
    return new a("cn.jiguang.sdk.user.set.profile", "option_channel", "");
  }
  
  public static a<String> m()
  {
    return new a("cn.jiguang.sdk.user.set.profile", "analytics_account_id", "");
  }
  
  public static a<Long> n()
  {
    return new a("Push_Page_Config", "css", Long.valueOf(0L));
  }
  
  public static a<Long> o()
  {
    return new a("Push_Page_Config", "cse", Long.valueOf(0L));
  }
  
  public static a<Long> p()
  {
    return new a("Push_Page_Config", "last_pause", Long.valueOf(-1L));
  }
  
  public static a<String> q()
  {
    return new a("Push_Page_Config", "session_id", "");
  }
  
  public static a<String> r()
  {
    return new a("cn.jiguang.sdk.report", "report_urls", "");
  }
  
  public static a<String> s()
  {
    return new a("cn.jiguang.sdk.report", "report_sis_urls", "");
  }
  
  public static a<Long> t()
  {
    return new a("cn.jiguang.sdk.report", "last_update_report_urls", Long.valueOf(0L));
  }
  
  public static a<Long> u()
  {
    return new a("cn.jiguang.sdk.report", "report_urls_ttl_millis", Long.valueOf(3600000L));
  }
  
  public static a<String> v()
  {
    return new a(f, "sdk_version", "").y();
  }
  
  public static a<Long> w()
  {
    return new a(f, "first_init", Long.valueOf(0L));
  }
  
  public static a<Long> x()
  {
    return new a(f, "lbs_delay", Long.valueOf(0L));
  }
  
  public a<T> a(T paramT)
  {
    this.c = paramT;
    return this;
  }
  
  public a<T> a(String paramString)
  {
    this.a = paramString;
    return this;
  }
  
  public a<T> b(String paramString)
  {
    this.b = paramString;
    return this;
  }
  
  public a<T> y()
  {
    this.d = true;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/e/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */