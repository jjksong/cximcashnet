package cn.jiguang.ae;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.text.TextUtils;
import cn.jiguang.api.JCoreManager;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class c
{
  private static final Map<String, SharedPreferences> a = new ConcurrentHashMap();
  
  private static SharedPreferences a(Context paramContext, String paramString)
  {
    paramContext = JCoreManager.getAppContext(paramContext);
    if (paramContext != null)
    {
      if (Build.VERSION.SDK_INT >= 11) {
        paramContext.getSharedPreferences(paramString, 4);
      }
      return paramContext.getSharedPreferences(paramString, 0);
    }
    return null;
  }
  
  public static <T> T a(Context paramContext, b<T> paramb)
  {
    paramContext = b(paramContext, paramb);
    if (paramContext != null) {
      return paramContext;
    }
    return (T)paramb.c;
  }
  
  private static <T> T a(SharedPreferences paramSharedPreferences, String paramString, T paramT)
  {
    if ((paramSharedPreferences != null) && (paramSharedPreferences.contains(paramString))) {}
    try
    {
      if ((paramT instanceof Boolean)) {
        return Boolean.valueOf(paramSharedPreferences.getBoolean(paramString, ((Boolean)paramT).booleanValue()));
      }
      if ((paramT instanceof String)) {
        return paramSharedPreferences.getString(paramString, (String)paramT);
      }
      if ((paramT instanceof Integer)) {
        return Integer.valueOf(paramSharedPreferences.getInt(paramString, ((Integer)paramT).intValue()));
      }
      if ((paramT instanceof Long)) {
        return Long.valueOf(paramSharedPreferences.getLong(paramString, ((Long)paramT).longValue()));
      }
      if ((paramT instanceof Float))
      {
        float f = paramSharedPreferences.getFloat(paramString, ((Float)paramT).floatValue());
        return Float.valueOf(f);
      }
    }
    catch (Throwable paramSharedPreferences)
    {
      for (;;) {}
    }
    return null;
  }
  
  private static <T> void a(Context paramContext, b<T> paramb1, b<T> paramb2)
  {
    if (b(paramContext, paramb1) == null)
    {
      Object localObject = b(paramContext, paramb2);
      if (localObject != null)
      {
        a(paramContext, new b[] { paramb1.a(localObject) });
        a(paramContext, new b[] { paramb2.a(null) });
      }
    }
  }
  
  public static void a(Context paramContext, b<?>... paramVarArgs)
  {
    if ((paramVarArgs != null) && (paramVarArgs.length > 0))
    {
      int i = 0;
      paramContext = b(paramContext, paramVarArgs[0].a);
      if (paramContext != null)
      {
        paramContext = paramContext.edit();
        int j = paramVarArgs.length;
        while (i < j)
        {
          b<?> localb = paramVarArgs[i];
          a(paramContext, localb.b, localb.c);
          i++;
        }
        paramContext.commit();
      }
    }
  }
  
  private static <T> void a(SharedPreferences.Editor paramEditor, String paramString, T paramT)
  {
    if (paramEditor != null) {
      if (paramT == null) {
        paramEditor.remove(paramString);
      } else if ((paramT instanceof Boolean)) {
        paramEditor.putBoolean(paramString, ((Boolean)paramT).booleanValue());
      } else if ((paramT instanceof String)) {
        paramEditor.putString(paramString, (String)paramT);
      } else if ((paramT instanceof Integer)) {
        paramEditor.putInt(paramString, ((Integer)paramT).intValue());
      } else if ((paramT instanceof Long)) {
        paramEditor.putLong(paramString, ((Long)paramT).longValue());
      } else if ((paramT instanceof Float)) {
        paramEditor.putFloat(paramString, ((Float)paramT).floatValue());
      }
    }
  }
  
  private static SharedPreferences b(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    SharedPreferences localSharedPreferences2 = (SharedPreferences)a.get(paramString);
    SharedPreferences localSharedPreferences1 = localSharedPreferences2;
    if (localSharedPreferences2 == null)
    {
      paramContext = JCoreManager.getAppContext(paramContext);
      localSharedPreferences1 = localSharedPreferences2;
      if (paramContext != null)
      {
        localSharedPreferences1 = paramContext.getSharedPreferences(paramString, 0);
        a.put(paramString, localSharedPreferences1);
        c(paramContext, paramString);
      }
    }
    return localSharedPreferences1;
  }
  
  public static <T> T b(Context paramContext, b<T> paramb)
  {
    Object localObject2 = a(b(paramContext, paramb.a), paramb.b, paramb.c);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = localObject2;
      if (paramb.d) {
        localObject1 = a(a(paramContext, paramb.a), paramb.b, paramb.c);
      }
    }
    if (localObject1 != null)
    {
      paramb.a(localObject1);
      return (T)localObject1;
    }
    return null;
  }
  
  private static void c(Context paramContext, String paramString)
  {
    String str = (String)a(paramContext, b.g());
    if (((TextUtils.isEmpty(str)) || (str.startsWith("1."))) && ("2.0.0".startsWith("2.")))
    {
      b localb;
      if (paramString.equals("cn.jiguang.sdk.address"))
      {
        a(paramContext, b.u(), b.u().h("cn.jpush.android.user.profile").i("conn"));
        paramString = b.v();
        localb = b.v().h("cn.jpush.android.user.profile");
      }
      for (str = "srv";; str = "imei")
      {
        a(paramContext, paramString, localb.i(str));
        break;
        if (!paramString.equals(b.e)) {
          break;
        }
        a(paramContext, b.h(), b.h().i("device_registered_appkey"));
        paramString = b.i();
        localb = b.i();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ae/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */