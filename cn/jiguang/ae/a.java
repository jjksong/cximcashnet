package cn.jiguang.ae;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.JCoreManager;
import java.util.LinkedHashMap;
import org.json.JSONObject;

public class a
{
  public static String a;
  public static int b;
  public static String c;
  public static int d;
  private static final LinkedHashMap<String, Integer> e = new LinkedHashMap();
  private static final LinkedHashMap<String, Integer> f;
  private static final LinkedHashMap<String, Integer> g = new LinkedHashMap();
  private static final LinkedHashMap<String, Integer> h = new LinkedHashMap();
  private static String i = "";
  private static String j = "";
  private static String k = "";
  
  static
  {
    e.put("s.jpush.cn", Integer.valueOf(19000));
    e.put("sis.jpush.io", Integer.valueOf(19000));
    e.put("easytomessage.com", Integer.valueOf(19000));
    f = new LinkedHashMap();
    f.put("123.196.118.23", Integer.valueOf(19000));
    f.put("103.229.215.60", Integer.valueOf(19000));
    f.put("117.121.49.100", Integer.valueOf(19000));
  }
  
  public static String a(Context paramContext)
  {
    if ((JCoreManager.isTestEnv()) && (!TextUtils.isEmpty(i))) {
      return i;
    }
    paramContext = (String)c.b(paramContext, b.u());
    if (!TextUtils.isEmpty(paramContext)) {
      return paramContext;
    }
    return "im64.jpush.cn";
  }
  
  public static LinkedHashMap<String, Integer> a()
  {
    if ((JCoreManager.isTestEnv()) && (!g.isEmpty())) {
      return g;
    }
    return e;
  }
  
  public static void a(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString))
    {
      cn.jiguang.ai.a.g("HostConfig", "conn info was empty");
      return;
    }
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("get conn info=");
    ((StringBuilder)localObject).append(paramString);
    cn.jiguang.ai.a.c("HostConfig", ((StringBuilder)localObject).toString());
    try
    {
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(paramString);
      String str = ((JSONObject)localObject).optString("srv");
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      paramString.append("save srvHost:");
      paramString.append(str);
      cn.jiguang.ai.a.c("HostConfig", paramString.toString());
      if (!TextUtils.isEmpty(str)) {
        c.a(paramContext, new b[] { b.v().a(str) });
      }
      localObject = ((JSONObject)localObject).optString("conn");
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      paramString.append("save connHost:");
      paramString.append((String)localObject);
      cn.jiguang.ai.a.c("HostConfig", paramString.toString());
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        c.a(paramContext, new b[] { b.u().a(localObject) });
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public static String b(Context paramContext)
  {
    if ((JCoreManager.isTestEnv()) && (!TextUtils.isEmpty(j))) {
      return j;
    }
    paramContext = (String)c.b(paramContext, b.v());
    if (!TextUtils.isEmpty(paramContext)) {
      return paramContext;
    }
    return "_im64._tcp.jpush.cn";
  }
  
  public static LinkedHashMap<String, Integer> b()
  {
    if ((JCoreManager.isTestEnv()) && (!h.isEmpty())) {
      return h;
    }
    return f;
  }
  
  public static String c()
  {
    if ((JCoreManager.isTestEnv()) && (!TextUtils.isEmpty(k))) {
      return k;
    }
    return "_psis._udp.jpush.cn";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ae/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */