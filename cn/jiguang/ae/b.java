package cn.jiguang.ae;

public class b<T>
{
  public static String e = "cn.jpush.preferences.v2";
  String a;
  String b;
  T c;
  boolean d;
  
  public b(String paramString1, String paramString2, T paramT)
  {
    this.a = paramString1;
    this.b = paramString2;
    if (paramT != null)
    {
      this.c = paramT;
      return;
    }
    throw new IllegalArgumentException("default value can not be null");
  }
  
  public static b<Boolean> a()
  {
    return new b("cn.jpush.android.user.profile", "is_tcp_close", Boolean.valueOf(false));
  }
  
  public static b<String> a(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("sdk_version_");
    localStringBuilder.append(paramString);
    return new b("cn.jpush.android.user.profile", localStringBuilder.toString(), "");
  }
  
  public static b<String> a(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("last_good_sis_address");
    String str;
    if (paramBoolean) {
      str = "_V4";
    } else {
      str = "_V6";
    }
    localStringBuilder.append(str);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), "");
  }
  
  public static b<Integer> b()
  {
    return new b("cn.jpush.android.user.profile", "jpush_register_code", Integer.valueOf(-1)).x();
  }
  
  public static b<String> b(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("dns_");
    localStringBuilder.append(paramString);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), "");
  }
  
  public static b<String> b(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("last_good_conn");
    String str;
    if (paramBoolean) {
      str = "_V4";
    } else {
      str = "_V6";
    }
    localStringBuilder.append(str);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), "");
  }
  
  public static b<Integer> c()
  {
    return new b("cn.jiguang.sdk.user.profile", "idc", Integer.valueOf(-1)).x();
  }
  
  public static b<Long> c(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("dns_last_update_");
    localStringBuilder.append(paramString);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), Long.valueOf(0L));
  }
  
  public static b<String> c(boolean paramBoolean)
  {
    String str;
    if (paramBoolean) {
      str = "default_https_report";
    } else {
      str = "default_http_report";
    }
    return new b("cn.jiguang.sdk.address", str, "").x();
  }
  
  public static b<Long> d()
  {
    return new b("cn.jiguang.sdk.user.profile", "key_uid", Long.valueOf(0L)).x();
  }
  
  public static b<String> d(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("srv_");
    localStringBuilder.append(paramString);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), "");
  }
  
  public static b<String> e()
  {
    return new b("cn.jiguang.sdk.user.profile", "key_rid", "").x();
  }
  
  public static b<Long> e(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("srv_last_update_");
    localStringBuilder.append(paramString);
    return new b("cn.jiguang.sdk.address", localStringBuilder.toString(), Long.valueOf(0L));
  }
  
  public static b<String> f()
  {
    return new b("cn.jiguang.sdk.user.profile", "key_pwd", "").x();
  }
  
  public static b<String> f(String paramString)
  {
    return new b("IpInfos", paramString, "");
  }
  
  public static b<String> g()
  {
    return new b(e, "sdk_version", "").x();
  }
  
  public static b<Integer> g(String paramString)
  {
    return new b("netinfo", paramString, Integer.valueOf(0));
  }
  
  public static b<String> h()
  {
    return new b(e, "device_config_appkey", "");
  }
  
  public static b<String> i()
  {
    return new b(e, "i_new", "");
  }
  
  public static b<String> j()
  {
    return new b(e, "push_udid", "");
  }
  
  public static b<String> k()
  {
    return new b(e, "last_connection_type", "");
  }
  
  public static b<String> l()
  {
    return new b(e, "sis_report_history", "");
  }
  
  public static b<Long> m()
  {
    return new b(e, "lbs_delay", Long.valueOf(0L));
  }
  
  public static b<Long> n()
  {
    return new b("cn.jpush.preferences.v2.rid", "next_rid", Long.valueOf(-1L));
  }
  
  public static b<String> o()
  {
    return new b("cn.jiguang.sdk.address", "ips_in_last_good_sis", "");
  }
  
  public static b<String> p()
  {
    return new b("cn.jiguang.sdk.address", "ssl_ips_in_last_good_sis", "");
  }
  
  public static b<Boolean> q()
  {
    return new b("cn.jiguang.sdk.address", "udp_data_report", Boolean.valueOf(false));
  }
  
  public static b<Long> r()
  {
    return new b("cn.jiguang.sdk.address", "sis_last_update", Long.valueOf(0L));
  }
  
  public static b<Long> s()
  {
    return new b("cn.jiguang.sdk.address", "last_sis_report_time", Long.valueOf(0L));
  }
  
  public static b<String> t()
  {
    return new b("cn.jiguang.sdk.address", "default_sis_ips", "");
  }
  
  public static b<String> u()
  {
    return new b("cn.jiguang.sdk.address", "default_conn", "");
  }
  
  public static b<String> v()
  {
    return new b("cn.jiguang.sdk.address", "default_conn_srv", "");
  }
  
  public static b<String> w()
  {
    return new b("PrefsFile", "key", "");
  }
  
  private b<T> x()
  {
    this.d = true;
    return this;
  }
  
  public b<T> a(T paramT)
  {
    this.c = paramT;
    return this;
  }
  
  public b<T> h(String paramString)
  {
    this.a = paramString;
    return this;
  }
  
  public b<T> i(String paramString)
  {
    this.b = paramString;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ae/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */