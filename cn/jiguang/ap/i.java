package cn.jiguang.ap;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Base64;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class i
{
  private static List<String> a = new ArrayList();
  
  static
  {
    a.add("358673013795895");
    a.add("004999010640000");
    a.add("00000000000000");
    a.add("000000000000000");
  }
  
  private static String a()
  {
    String str = a.b();
    if (str == null) {
      return null;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append(".push_udid");
    return localStringBuilder.toString();
  }
  
  public static String a(Context paramContext)
  {
    String str2 = (String)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.i());
    String str1 = str2;
    if (!TextUtils.isEmpty(str2)) {
      str1 = new String(Base64.decode(str2, 2));
    }
    if (a(str1)) {
      return str1;
    }
    str1 = b(paramContext);
    if (!TextUtils.isEmpty(str1)) {
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.i().a(Base64.encodeToString(str1.getBytes(), 2)) });
    }
    return str1;
  }
  
  private static boolean a(String paramString)
  {
    if (!g.g(paramString)) {
      return false;
    }
    if (paramString.length() < 10) {
      return false;
    }
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      if (paramString.startsWith((String)localIterator.next())) {
        return false;
      }
    }
    return true;
  }
  
  private static String b(Context paramContext)
  {
    try
    {
      String str = b.a(paramContext).q;
      if (a(str)) {
        return str;
      }
      str = b.a(paramContext).i;
      if ((a(str)) && (!"9774d56d682e549c".equals(str.toLowerCase(Locale.getDefault())))) {
        return str;
      }
      str = c(paramContext);
      if (a(str)) {
        return str;
      }
      return "";
    }
    catch (Exception localException)
    {
      cn.jiguang.ai.a.b("UDIDUtils", "", localException);
      paramContext = d(paramContext);
      if (a(paramContext)) {
        return paramContext;
      }
    }
    return "";
  }
  
  private static String c(Context paramContext)
  {
    String str = "";
    if (cn.jiguang.sdk.impl.b.q(paramContext)) {
      str = a.h(paramContext);
    }
    if (a(str)) {
      return str;
    }
    str = d(paramContext);
    paramContext = str;
    if (str == null) {
      paramContext = " ";
    }
    return paramContext;
  }
  
  private static String d(Context paramContext)
  {
    cn.jiguang.ai.a.c("UDIDUtils", "Action:getSavedUuid");
    Object localObject = (String)cn.jiguang.ae.c.b(paramContext, cn.jiguang.ae.b.w());
    if (!g.a((String)localObject)) {
      return (String)localObject;
    }
    if (!a.a()) {
      return f(paramContext);
    }
    String str = (String)cn.jiguang.ae.c.a(paramContext, cn.jiguang.ae.b.j());
    localObject = str;
    if (TextUtils.isEmpty(str))
    {
      if (Build.VERSION.SDK_INT < 23) {}
      while ((a.a(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE")) && (a.a(paramContext, "android.permission.READ_EXTERNAL_STORAGE")))
      {
        localObject = e(paramContext);
        break;
      }
      return f(paramContext);
    }
    return (String)localObject;
  }
  
  private static String e(Context paramContext)
  {
    Object localObject1 = a();
    if (!g.a((String)localObject1)) {
      localObject1 = new File((String)localObject1);
    } else {
      localObject1 = null;
    }
    Object localObject2 = c.c((File)localObject1);
    if (!TextUtils.isEmpty((CharSequence)localObject2))
    {
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.j().a(localObject2) });
      paramContext = new StringBuilder();
      paramContext.append("Got sdcard file saved udid - ");
      paramContext.append((String)localObject2);
      cn.jiguang.ai.a.e("UDIDUtils", paramContext.toString());
      return (String)localObject2;
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(System.currentTimeMillis());
    ((StringBuilder)localObject2).append("");
    localObject2 = g.j(UUID.nameUUIDFromBytes(((StringBuilder)localObject2).toString().getBytes()).toString());
    cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.j().a(localObject2) });
    c.a((File)localObject1, (String)localObject2);
    return (String)localObject2;
  }
  
  private static String f(Context paramContext)
  {
    cn.jiguang.ae.b localb = cn.jiguang.ae.b.w();
    String str2 = (String)cn.jiguang.ae.c.b(paramContext, localb);
    String str1 = str2;
    if (str2 == null)
    {
      str1 = UUID.randomUUID().toString();
      cn.jiguang.ae.c.a(paramContext, new cn.jiguang.ae.b[] { localb.a(str1) });
    }
    return str1;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */