package cn.jiguang.ap;

import android.os.SystemClock;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class j
{
  public static <T> LinkedList<T> a(Collection<T> paramCollection)
  {
    LinkedList localLinkedList = new LinkedList();
    int j = 0;
    if (paramCollection != null) {
      i = paramCollection.size();
    } else {
      i = 0;
    }
    if (i == 0) {
      return localLinkedList;
    }
    if (i == 1)
    {
      localLinkedList.addAll(paramCollection);
      return localLinkedList;
    }
    SecureRandom localSecureRandom = new SecureRandom();
    Iterator localIterator = paramCollection.iterator();
    int i = j;
    while (localIterator.hasNext())
    {
      paramCollection = localIterator.next();
      i++;
      localLinkedList.add(localSecureRandom.nextInt(i), paramCollection);
    }
    return localLinkedList;
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (Throwable paramCloseable)
    {
      for (;;) {}
    }
  }
  
  public static boolean a(long paramLong1, long paramLong2)
  {
    if (paramLong2 > 0L)
    {
      long l = SystemClock.elapsedRealtime();
      if (paramLong1 <= 0L) {
        return true;
      }
      if (l <= paramLong1) {
        return true;
      }
      return l > paramLong1 + paramLong2;
    }
    throw new AssertionError();
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    if (paramInputStream != null)
    {
      byte[] arrayOfByte = new byte[paramInputStream.available()];
      paramInputStream.read(arrayOfByte);
      return arrayOfByte;
    }
    throw new IOException("InputStream is null");
  }
  
  /* Error */
  public static byte[] a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +71 -> 72
    //   4: aload_0
    //   5: arraylength
    //   6: ifne +6 -> 12
    //   9: goto +63 -> 72
    //   12: new 90	java/io/ByteArrayOutputStream
    //   15: dup
    //   16: invokespecial 91	java/io/ByteArrayOutputStream:<init>	()V
    //   19: astore_3
    //   20: new 93	java/util/zip/GZIPOutputStream
    //   23: astore_2
    //   24: aload_2
    //   25: aload_3
    //   26: invokespecial 96	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   29: aload_2
    //   30: aload_0
    //   31: invokevirtual 102	java/io/OutputStream:write	([B)V
    //   34: aload_2
    //   35: invokevirtual 103	java/io/OutputStream:close	()V
    //   38: aload_3
    //   39: invokevirtual 107	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   42: astore_0
    //   43: aload_3
    //   44: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   47: aload_2
    //   48: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   51: aload_0
    //   52: areturn
    //   53: astore_1
    //   54: aload_2
    //   55: astore_0
    //   56: goto +6 -> 62
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_0
    //   62: aload_3
    //   63: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   66: aload_0
    //   67: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   70: aload_1
    //   71: athrow
    //   72: aload_0
    //   73: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	paramArrayOfByte	byte[]
    //   53	1	1	localObject1	Object
    //   59	12	1	localObject2	Object
    //   23	32	2	localGZIPOutputStream	java.util.zip.GZIPOutputStream
    //   19	44	3	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    // Exception table:
    //   from	to	target	type
    //   29	43	53	finally
    //   20	29	59	finally
  }
  
  /* Error */
  public static byte[] b(InputStream paramInputStream)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +64 -> 65
    //   4: new 90	java/io/ByteArrayOutputStream
    //   7: astore_3
    //   8: aload_3
    //   9: invokespecial 91	java/io/ByteArrayOutputStream:<init>	()V
    //   12: sipush 1024
    //   15: newarray <illegal type>
    //   17: astore_2
    //   18: aload_0
    //   19: aload_2
    //   20: invokevirtual 80	java/io/InputStream:read	([B)I
    //   23: istore_1
    //   24: iload_1
    //   25: iconst_m1
    //   26: if_icmpeq +13 -> 39
    //   29: aload_3
    //   30: aload_2
    //   31: iconst_0
    //   32: iload_1
    //   33: invokevirtual 113	java/io/ByteArrayOutputStream:write	([BII)V
    //   36: goto -18 -> 18
    //   39: aload_3
    //   40: invokevirtual 107	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   43: astore_0
    //   44: aload_3
    //   45: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   48: aload_0
    //   49: areturn
    //   50: astore_2
    //   51: aload_3
    //   52: astore_0
    //   53: goto +6 -> 59
    //   56: astore_2
    //   57: aconst_null
    //   58: astore_0
    //   59: aload_0
    //   60: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   63: aload_2
    //   64: athrow
    //   65: new 82	java/io/IOException
    //   68: dup
    //   69: ldc 84
    //   71: invokespecial 87	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   74: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	75	0	paramInputStream	InputStream
    //   23	10	1	i	int
    //   17	14	2	arrayOfByte	byte[]
    //   50	1	2	localObject1	Object
    //   56	8	2	localObject2	Object
    //   7	45	3	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    // Exception table:
    //   from	to	target	type
    //   12	18	50	finally
    //   18	24	50	finally
    //   29	36	50	finally
    //   39	44	50	finally
    //   4	12	56	finally
  }
  
  /* Error */
  public static byte[] b(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +107 -> 108
    //   4: aload_0
    //   5: arraylength
    //   6: ifne +6 -> 12
    //   9: goto +99 -> 108
    //   12: new 90	java/io/ByteArrayOutputStream
    //   15: dup
    //   16: invokespecial 91	java/io/ByteArrayOutputStream:<init>	()V
    //   19: astore_3
    //   20: new 115	java/io/ByteArrayInputStream
    //   23: dup
    //   24: aload_0
    //   25: invokespecial 117	java/io/ByteArrayInputStream:<init>	([B)V
    //   28: astore 4
    //   30: new 119	java/util/zip/GZIPInputStream
    //   33: astore_2
    //   34: aload_2
    //   35: aload 4
    //   37: invokespecial 122	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   40: sipush 256
    //   43: newarray <illegal type>
    //   45: astore_0
    //   46: aload_2
    //   47: aload_0
    //   48: invokevirtual 123	java/util/zip/GZIPInputStream:read	([B)I
    //   51: istore_1
    //   52: iload_1
    //   53: iflt +13 -> 66
    //   56: aload_3
    //   57: aload_0
    //   58: iconst_0
    //   59: iload_1
    //   60: invokevirtual 113	java/io/ByteArrayOutputStream:write	([BII)V
    //   63: goto -17 -> 46
    //   66: aload_3
    //   67: invokevirtual 107	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   70: astore_0
    //   71: aload_3
    //   72: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   75: aload 4
    //   77: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   80: aload_2
    //   81: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   84: aload_0
    //   85: areturn
    //   86: astore_0
    //   87: goto +6 -> 93
    //   90: astore_0
    //   91: aconst_null
    //   92: astore_2
    //   93: aload_3
    //   94: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   97: aload 4
    //   99: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   102: aload_2
    //   103: invokestatic 109	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   106: aload_0
    //   107: athrow
    //   108: aload_0
    //   109: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	paramArrayOfByte	byte[]
    //   51	9	1	i	int
    //   33	70	2	localGZIPInputStream	java.util.zip.GZIPInputStream
    //   19	75	3	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    //   28	70	4	localByteArrayInputStream	java.io.ByteArrayInputStream
    // Exception table:
    //   from	to	target	type
    //   40	46	86	finally
    //   46	52	86	finally
    //   56	63	86	finally
    //   66	71	86	finally
    //   30	40	90	finally
  }
  
  public static short c(byte[] paramArrayOfByte)
  {
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length != 0))
    {
      if (paramArrayOfByte.length == 1) {}
      for (int i = paramArrayOfByte[0];; i = (short)(paramArrayOfByte[1] & 0xFF) | i)
      {
        return (short)i;
        i = (short)((paramArrayOfByte[0] & 0xFF) << 8);
      }
    }
    throw new Exception("byte could not be empty");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */