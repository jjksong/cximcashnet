package cn.jiguang.ap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class e
{
  public static Object a(Class<?> paramClass, String paramString, Object[] paramArrayOfObject, Class[] paramArrayOfClass)
  {
    a(paramClass);
    a(paramArrayOfClass, paramArrayOfObject);
    return paramClass.getMethod(paramString, paramArrayOfClass).invoke(paramClass, paramArrayOfObject);
  }
  
  public static <T> T a(Class<T> paramClass, Object[] paramArrayOfObject, Class<?>[] paramArrayOfClass)
  {
    a(paramClass);
    a(paramArrayOfClass, paramArrayOfObject);
    return (T)paramClass.getConstructor(paramArrayOfClass).newInstance(paramArrayOfObject);
  }
  
  public static Object a(Object paramObject, String paramString, Object[] paramArrayOfObject, Class[] paramArrayOfClass)
  {
    a(paramObject);
    a(paramArrayOfClass, paramArrayOfObject);
    return paramObject.getClass().getMethod(paramString, paramArrayOfClass).invoke(paramObject, paramArrayOfObject);
  }
  
  private static void a(Object paramObject)
  {
    if (paramObject != null) {
      return;
    }
    throw new Exception("owner can not be null");
  }
  
  private static void a(Class<?>[] paramArrayOfClass, Object[] paramArrayOfObject)
  {
    int j = 0;
    int i;
    if (paramArrayOfObject != null) {
      i = paramArrayOfObject.length;
    } else {
      i = 0;
    }
    if (paramArrayOfClass != null) {
      j = paramArrayOfClass.length;
    }
    if (i == j) {
      return;
    }
    throw new Exception("argClasses' size is not equal to args' size");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */