package cn.jiguang.ap;

import android.content.Context;
import cn.jiguang.ai.a;
import java.io.File;
import java.io.UnsupportedEncodingException;

public class c
{
  public static File a(Context paramContext, String paramString)
  {
    if (paramContext != null)
    {
      paramContext = paramContext.getFilesDir();
      if (paramContext != null) {
        return new File(paramContext, paramString);
      }
    }
    paramContext = new StringBuilder();
    paramContext.append("can't get file :");
    paramContext.append(paramString);
    a.g("FileUtils", paramContext.toString());
    return null;
  }
  
  public static void a(File paramFile)
  {
    if ((paramFile != null) && (!paramFile.exists()))
    {
      File localFile = paramFile.getParentFile();
      if ((localFile != null) && (!localFile.exists())) {
        localFile.mkdirs();
      }
    }
    try
    {
      paramFile.createNewFile();
      return;
    }
    catch (Throwable paramFile)
    {
      for (;;) {}
    }
  }
  
  public static boolean a(File paramFile, String paramString)
  {
    if (paramString != null) {
      try
      {
        paramString = paramString.getBytes("UTF-8");
      }
      catch (Throwable paramString)
      {
        break label28;
      }
    } else {
      paramString = null;
    }
    boolean bool = a(paramFile, paramString);
    return bool;
    label28:
    paramFile = new StringBuilder();
    paramFile.append("getBytes exception:");
    paramFile.append(paramString);
    a.g("FileUtils", paramFile.toString());
    return false;
  }
  
  /* Error */
  public static boolean a(File paramFile, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +162 -> 163
    //   4: aload_0
    //   5: invokevirtual 78	java/io/File:isDirectory	()Z
    //   8: ifeq +6 -> 14
    //   11: goto +152 -> 163
    //   14: aconst_null
    //   15: astore 4
    //   17: aconst_null
    //   18: astore 5
    //   20: aload_1
    //   21: astore_3
    //   22: aload_1
    //   23: ifnonnull +10 -> 33
    //   26: aload 5
    //   28: astore_1
    //   29: iconst_0
    //   30: newarray <illegal type>
    //   32: astore_3
    //   33: aload 5
    //   35: astore_1
    //   36: aload_0
    //   37: invokestatic 80	cn/jiguang/ap/c:a	(Ljava/io/File;)V
    //   40: aload 5
    //   42: astore_1
    //   43: new 82	java/io/FileOutputStream
    //   46: astore_2
    //   47: aload 5
    //   49: astore_1
    //   50: aload_2
    //   51: aload_0
    //   52: invokespecial 84	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   55: aload_2
    //   56: aload_3
    //   57: invokevirtual 88	java/io/FileOutputStream:write	([B)V
    //   60: aload_2
    //   61: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   64: iconst_1
    //   65: ireturn
    //   66: astore_0
    //   67: aload_2
    //   68: astore_1
    //   69: goto +88 -> 157
    //   72: astore_3
    //   73: goto +11 -> 84
    //   76: astore_0
    //   77: goto +80 -> 157
    //   80: astore_3
    //   81: aload 4
    //   83: astore_2
    //   84: aload_2
    //   85: astore_1
    //   86: new 20	java/lang/StringBuilder
    //   89: astore 4
    //   91: aload_2
    //   92: astore_1
    //   93: aload 4
    //   95: invokespecial 23	java/lang/StringBuilder:<init>	()V
    //   98: aload_2
    //   99: astore_1
    //   100: aload 4
    //   102: ldc 95
    //   104: invokevirtual 29	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   107: pop
    //   108: aload_2
    //   109: astore_1
    //   110: aload 4
    //   112: aload_3
    //   113: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   116: pop
    //   117: aload_2
    //   118: astore_1
    //   119: aload 4
    //   121: ldc 97
    //   123: invokevirtual 29	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload_2
    //   128: astore_1
    //   129: aload 4
    //   131: aload_0
    //   132: invokevirtual 100	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   135: invokevirtual 29	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_2
    //   140: astore_1
    //   141: ldc 31
    //   143: aload 4
    //   145: invokevirtual 35	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   148: invokestatic 41	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   151: aload_2
    //   152: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   155: iconst_0
    //   156: ireturn
    //   157: aload_1
    //   158: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   161: aload_0
    //   162: athrow
    //   163: ldc 31
    //   165: ldc 102
    //   167: invokestatic 41	cn/jiguang/ai/a:g	(Ljava/lang/String;Ljava/lang/String;)V
    //   170: iconst_0
    //   171: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	172	0	paramFile	File
    //   0	172	1	paramArrayOfByte	byte[]
    //   46	106	2	localObject1	Object
    //   21	36	3	arrayOfByte	byte[]
    //   72	1	3	localThrowable1	Throwable
    //   80	33	3	localThrowable2	Throwable
    //   15	129	4	localStringBuilder	StringBuilder
    //   18	30	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   55	60	66	finally
    //   55	60	72	java/lang/Throwable
    //   29	33	76	finally
    //   36	40	76	finally
    //   43	47	76	finally
    //   50	55	76	finally
    //   86	91	76	finally
    //   93	98	76	finally
    //   100	108	76	finally
    //   110	117	76	finally
    //   119	127	76	finally
    //   129	139	76	finally
    //   141	151	76	finally
    //   29	33	80	java/lang/Throwable
    //   36	40	80	java/lang/Throwable
    //   43	47	80	java/lang/Throwable
    //   50	55	80	java/lang/Throwable
  }
  
  /* Error */
  public static byte[] b(File paramFile)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +108 -> 109
    //   4: aload_0
    //   5: invokevirtual 49	java/io/File:exists	()Z
    //   8: ifeq +101 -> 109
    //   11: aload_0
    //   12: invokevirtual 78	java/io/File:isDirectory	()Z
    //   15: ifeq +6 -> 21
    //   18: goto +91 -> 109
    //   21: new 106	java/io/FileInputStream
    //   24: astore_1
    //   25: aload_1
    //   26: aload_0
    //   27: invokespecial 107	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   30: aload_1
    //   31: astore_0
    //   32: aload_1
    //   33: invokestatic 110	cn/jiguang/ap/j:a	(Ljava/io/InputStream;)[B
    //   36: astore_2
    //   37: aload_1
    //   38: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   41: aload_2
    //   42: areturn
    //   43: astore_2
    //   44: goto +12 -> 56
    //   47: astore_1
    //   48: aconst_null
    //   49: astore_0
    //   50: goto +53 -> 103
    //   53: astore_2
    //   54: aconst_null
    //   55: astore_1
    //   56: aload_1
    //   57: astore_0
    //   58: new 20	java/lang/StringBuilder
    //   61: astore_3
    //   62: aload_1
    //   63: astore_0
    //   64: aload_3
    //   65: invokespecial 23	java/lang/StringBuilder:<init>	()V
    //   68: aload_1
    //   69: astore_0
    //   70: aload_3
    //   71: ldc 112
    //   73: invokevirtual 29	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: pop
    //   77: aload_1
    //   78: astore_0
    //   79: aload_3
    //   80: aload_2
    //   81: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload_1
    //   86: astore_0
    //   87: ldc 31
    //   89: aload_3
    //   90: invokevirtual 35	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   93: invokestatic 115	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   96: aload_1
    //   97: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   100: aconst_null
    //   101: areturn
    //   102: astore_1
    //   103: aload_0
    //   104: invokestatic 93	cn/jiguang/ap/j:a	(Ljava/io/Closeable;)V
    //   107: aload_1
    //   108: athrow
    //   109: aconst_null
    //   110: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	111	0	paramFile	File
    //   24	14	1	localFileInputStream	java.io.FileInputStream
    //   47	1	1	localObject1	Object
    //   55	42	1	localCloseable	java.io.Closeable
    //   102	6	1	localObject2	Object
    //   36	6	2	arrayOfByte	byte[]
    //   43	1	2	localThrowable1	Throwable
    //   53	28	2	localThrowable2	Throwable
    //   61	29	3	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   32	37	43	java/lang/Throwable
    //   21	30	47	finally
    //   21	30	53	java/lang/Throwable
    //   32	37	102	finally
    //   58	62	102	finally
    //   64	68	102	finally
    //   70	77	102	finally
    //   79	85	102	finally
    //   87	96	102	finally
  }
  
  public static String c(File paramFile)
  {
    paramFile = b(paramFile);
    if (paramFile == null) {
      return null;
    }
    try
    {
      paramFile = new String(paramFile, "UTF-8");
      return paramFile;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramFile = new StringBuilder();
      paramFile.append("can't encoding, give up read :");
      paramFile.append(localUnsupportedEncodingException);
      a.c("FileUtils", paramFile.toString());
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */