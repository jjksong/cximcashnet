package cn.jiguang.ap;

import android.util.SparseArray;
import cn.jiguang.ai.a;

public class f
{
  private static final SparseArray<String> a = new SparseArray();
  
  static
  {
    a.put(0, "OK");
    a.put(64535, "Exceed buffer size. Please contact support.");
    a.put(64536, "Connection failed. Please check your connection and retry later!");
    a.put(64538, "Sending failed or timeout. Please Retry later!");
    a.put(64539, "Receiving failed or timeout. Please Retry later!");
    a.put(64540, "Connection is closed. Please Retry later!");
    a.put(64542, "Response timeout. Please Retry later!");
    a.put(64543, "Invalid socket. Please Retry later!");
    a.put(11, "Failed to register!");
    a.put(1005, "Your appKey and android package name are not matched. Please double check them according to Application you created on Portal.");
    a.put(1006, "You android package name is not exist, Please register your pacakge name in Portal.");
    a.put(1007, "Invalid Imei, Register again.");
    a.put(1008, "Invalid appKey, Please get your Appkey from JIGUANG web console!");
    a.put(1009, "Your appKey is related to a non-Android App.Please use your Android App's appKey, or add an Android app for this appKey.");
    a.put(10000, "Receiver data parse error");
    a.put(102, "102 - Incorrect password");
    a.put(108, "108 - Incorrect uid");
    a.put(1012, "Server reject this connection, will clear cache and restart connect.");
  }
  
  public static String a(int paramInt)
  {
    if (a.get(paramInt) == null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Unknown error code - ");
      localStringBuilder.append(paramInt);
      a.c("StatusCode", localStringBuilder.toString());
      return null;
    }
    return (String)a.get(paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */