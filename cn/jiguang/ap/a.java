package cn.jiguang.ap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.Toast;
import cn.jpush.android.service.PushReceiver;
import java.io.File;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class a
{
  private static String a = "";
  private static String b = "";
  private static String c = "";
  
  public static int a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return 0;
    }
    paramString = paramString.split("\\.");
    return (Integer.parseInt(paramString[0]) << 16) + (Integer.parseInt(paramString[1]) << 8) + Integer.parseInt(paramString[2]);
  }
  
  public static ComponentInfo a(Context paramContext, String paramString, Class<?> paramClass)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)) && (paramClass != null)) {
      try
      {
        boolean bool = Service.class.isAssignableFrom(paramClass);
        int j = 0;
        if (bool) {
          i = 4;
        } else if (BroadcastReceiver.class.isAssignableFrom(paramClass)) {
          i = 2;
        } else if (Activity.class.isAssignableFrom(paramClass)) {
          i = 1;
        } else if (ContentProvider.class.isAssignableFrom(paramClass)) {
          i = 8;
        } else {
          i = 0;
        }
        paramContext = paramContext.getPackageManager().getPackageInfo(paramString, i);
        if (i != 4)
        {
          if (i != 8) {
            switch (i)
            {
            default: 
              paramContext = null;
              break;
            case 2: 
              paramContext = paramContext.receivers;
              break;
            case 1: 
              paramContext = paramContext.activities;
              break;
            }
          } else {
            paramContext = paramContext.providers;
          }
        }
        else {
          paramContext = paramContext.services;
        }
        if (paramContext == null) {
          return null;
        }
        int k = paramContext.length;
        for (int i = j; i < k; i++)
        {
          paramString = paramContext[i];
          bool = paramClass.isAssignableFrom(Class.forName(paramString.name));
          if (bool) {
            return paramString;
          }
        }
        return null;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("hasComponent error:");
        paramString.append(paramContext.getMessage());
        cn.jiguang.ai.a.h("AndroidUtil", paramString.toString());
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action - hasComponent, invalide param, context:");
    localStringBuilder.append(paramContext);
    localStringBuilder.append(",packageName:");
    localStringBuilder.append(paramString);
    localStringBuilder.append(",cls:");
    localStringBuilder.append(paramClass);
    cn.jiguang.ai.a.g("AndroidUtil", localStringBuilder.toString());
    return null;
  }
  
  public static String a(Context paramContext)
  {
    if (!TextUtils.isEmpty(a)) {
      return a;
    }
    a = paramContext.getPackageName();
    return a;
  }
  
  public static String a(Context paramContext, String paramString1, String paramString2)
  {
    try
    {
      try
      {
        paramContext = (String)e.a(paramContext.getClassLoader().loadClass("android.os.SystemProperties"), "get", new Object[] { paramString1, paramString2 }, new Class[] { String.class, String.class });
      }
      catch (Exception paramContext)
      {
        paramContext = "";
      }
      return paramContext;
    }
    catch (IllegalArgumentException paramContext)
    {
      throw paramContext;
    }
  }
  
  private static List<String> a(Context paramContext, Intent paramIntent, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    try
    {
      paramIntent = paramContext.getPackageManager().queryBroadcastReceivers(paramIntent, 0);
      paramContext = paramContext.getPackageManager();
      Iterator localIterator = paramIntent.iterator();
      while (localIterator.hasNext())
      {
        ResolveInfo localResolveInfo = (ResolveInfo)localIterator.next();
        if (localResolveInfo.activityInfo != null)
        {
          paramIntent = localResolveInfo.activityInfo.name;
          if (!TextUtils.isEmpty(paramIntent))
          {
            int j = 1;
            int i = j;
            if (!TextUtils.isEmpty(paramString))
            {
              i = j;
              if (paramContext.checkPermission(paramString, localResolveInfo.activityInfo.packageName) != 0) {
                i = 0;
              }
            }
            if (i != 0) {
              localArrayList.add(paramIntent);
            }
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return localArrayList;
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext.sendBroadcast(paramIntent);
    }
    catch (Throwable localThrowable)
    {
      b(paramContext, paramIntent);
    }
  }
  
  public static void a(Context paramContext, String paramString, int paramInt)
  {
    if (!f(paramContext))
    {
      cn.jiguang.ai.a.c("AndroidUtil", "not debuggable");
      return;
    }
    if (!a(paramContext, PushReceiver.class))
    {
      b(paramContext, paramString);
      return;
    }
    cn.jiguang.ai.a.c("AndroidUtil", "action:showPermanentNotification");
    Intent localIntent = new Intent(paramContext, PushReceiver.class);
    localIntent.setAction("noti_open_proxy");
    localIntent.addCategory(paramContext.getPackageName());
    localIntent.putExtra("debug_notification", true);
    localIntent.putExtra("toastText", paramString);
    localIntent.putExtra("type", paramInt);
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(paramContext, 0, localIntent, 134217728);
    NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
    try
    {
      paramInt = paramContext.getPackageManager().getApplicationInfo(paramContext.getApplicationContext().getPackageName(), 0).icon;
    }
    catch (Throwable localThrowable)
    {
      cn.jiguang.ai.a.c("AndroidUtil", "failed to get application info and icon.", localThrowable);
      paramInt = 17301586;
    }
    long l = System.currentTimeMillis();
    Notification localNotification;
    if (Build.VERSION.SDK_INT >= 11)
    {
      paramContext = new Notification.Builder(paramContext.getApplicationContext()).setContentTitle("Jiguang提示：包名和AppKey不匹配").setContentText("请到 Portal 上获取您的包名和AppKey并更新AndroidManifest相应字段").setContentIntent(localPendingIntent).setSmallIcon(paramInt).setTicker(paramString).setWhen(l);
      if (Build.VERSION.SDK_INT >= 26) {
        paramContext.setChannelId("JPush_Notification");
      }
      paramContext = paramContext.getNotification();
      paramContext.flags = 34;
    }
    else
    {
      localNotification = new Notification(paramInt, paramString, l);
      localNotification.flags = 34;
    }
    try
    {
      e.a(localNotification, "setLatestEventInfo", new Object[] { paramContext, "Jiguang提示：包名和AppKey不匹配", "请到 Portal 上获取您的包名和AppKey并更新AndroidManifest相应字段", localPendingIntent }, new Class[] { Context.class, CharSequence.class, CharSequence.class, PendingIntent.class });
      paramContext = localNotification;
      localNotificationManager.notify(paramString.hashCode(), paramContext);
      return;
    }
    catch (Exception paramContext)
    {
      for (;;) {}
    }
  }
  
  public static boolean a()
  {
    boolean bool;
    try
    {
      bool = Environment.getExternalStorageState().equals("mounted");
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("isSdcardExist exception: ");
      localStringBuilder.append(localThrowable);
      cn.jiguang.ai.a.g("AndroidUtil", localStringBuilder.toString());
      bool = false;
    }
    if (!bool) {
      cn.jiguang.ai.a.c("AndroidUtil", "SDCard is not mounted");
    }
    return bool;
  }
  
  public static boolean a(Context paramContext, Class<?> paramClass)
  {
    for (;;)
    {
      boolean bool1;
      boolean bool2;
      try
      {
        PackageManager localPackageManager = paramContext.getPackageManager();
        Intent localIntent = new android/content/Intent;
        localIntent.<init>(paramContext, paramClass);
        bool1 = localPackageManager.queryBroadcastReceivers(localIntent, 0).isEmpty();
        bool2 = bool1 ^ true;
        bool1 = bool2;
        if (bool2) {}
      }
      catch (Throwable paramContext)
      {
        continue;
      }
      try
      {
        paramContext = a(paramContext, paramContext.getPackageName(), paramClass);
        if (paramContext != null) {
          bool1 = true;
        } else {
          bool1 = false;
        }
      }
      catch (Throwable paramContext)
      {
        bool1 = bool2;
      }
    }
    return bool1;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    boolean bool2 = false;
    boolean bool1;
    try
    {
      int i = Build.VERSION.SDK_INT;
      bool1 = true;
      if (i >= 23)
      {
        if (paramContext.getApplicationInfo().targetSdkVersion >= 23)
        {
          bool1 = bool2;
          if (paramContext.checkSelfPermission(paramString) != 0) {
            return bool1;
          }
        }
      }
      else {
        return true;
      }
      paramString = AppOpsManager.permissionToOp(paramString);
      if (paramString != null)
      {
        i = ((AppOpsManager)paramContext.getSystemService("appops")).noteProxyOpNoThrow(paramString, paramContext.getPackageName());
        if (i != 0) {
          bool1 = false;
        }
      }
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder();
      paramString.append("checkPermission error:");
      paramString.append(paramContext.getMessage());
      cn.jiguang.ai.a.g("AndroidUtil", paramString.toString());
      bool1 = bool2;
    }
    return bool1;
  }
  
  public static int b(Context paramContext)
  {
    if (paramContext == null) {
      return -1;
    }
    Object localObject2 = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    Object localObject1 = null;
    try
    {
      paramContext = paramContext.registerReceiver(null, (IntentFilter)localObject2);
    }
    catch (Exception paramContext)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("getChargedStatus unkown exception:");
      ((StringBuilder)localObject2).append(paramContext.getMessage());
      cn.jiguang.ai.a.g("AndroidUtil", ((StringBuilder)localObject2).toString());
      paramContext = (Context)localObject1;
    }
    catch (SecurityException paramContext)
    {
      cn.jiguang.ai.a.g("AndroidUtil", "getChargedStatus SecurityException");
      paramContext = (Context)localObject1;
    }
    if (paramContext == null) {
      return -1;
    }
    int i = paramContext.getIntExtra("status", -1);
    if ((i != 2) && (i != 5)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0) {
      return -1;
    }
    return paramContext.getIntExtra("plugged", -1);
  }
  
  public static ComponentInfo b(Context paramContext, String paramString, Class<?> paramClass)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)) && (paramClass != null)) {
      try
      {
        boolean bool = Service.class.isAssignableFrom(paramClass);
        int j = 0;
        if (bool) {
          i = 4;
        } else if (BroadcastReceiver.class.isAssignableFrom(paramClass)) {
          i = 2;
        } else if (Activity.class.isAssignableFrom(paramClass)) {
          i = 1;
        } else if (ContentProvider.class.isAssignableFrom(paramClass)) {
          i = 8;
        } else {
          i = 0;
        }
        paramContext = paramContext.getPackageManager().getPackageInfo(paramString, i);
        if (i != 4)
        {
          if (i != 8) {
            switch (i)
            {
            default: 
              paramContext = null;
              break;
            case 2: 
              paramContext = paramContext.receivers;
              break;
            case 1: 
              paramContext = paramContext.activities;
              break;
            }
          } else {
            paramContext = paramContext.providers;
          }
        }
        else {
          paramContext = paramContext.services;
        }
        if (paramContext == null) {
          return null;
        }
        paramString = paramClass.getName();
        int k = paramContext.length;
        for (int i = j; i < k; i++)
        {
          paramClass = paramContext[i];
          bool = paramString.equals(paramClass.name);
          if (bool) {
            return paramClass;
          }
        }
        return null;
      }
      catch (Throwable paramContext)
      {
        paramString = new StringBuilder();
        paramString.append("hasComponent error:");
        paramString.append(paramContext.getMessage());
        cn.jiguang.ai.a.h("AndroidUtil", paramString.toString());
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action - hasComponent, invalide param, context:");
    localStringBuilder.append(paramContext);
    localStringBuilder.append(",packageName:");
    localStringBuilder.append(paramString);
    localStringBuilder.append(",cls:");
    localStringBuilder.append(paramClass);
    cn.jiguang.ai.a.g("AndroidUtil", localStringBuilder.toString());
    return null;
  }
  
  public static String b()
  {
    try
    {
      try
      {
        String str = Environment.getExternalStorageDirectory().getPath();
      }
      catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
      {
        localArrayIndexOutOfBoundsException.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      CharSequence localCharSequence;
      Object localObject;
      for (;;) {}
    }
    localCharSequence = null;
    localObject = localCharSequence;
    if (!TextUtils.isEmpty(localCharSequence))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(localCharSequence);
      ((StringBuilder)localObject).append("/data/");
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  public static void b(Context paramContext, Intent paramIntent)
  {
    try
    {
      Object localObject1 = a(paramContext, paramIntent, null);
      if ((localObject1 != null) && (!((List)localObject1).isEmpty())) {
        localObject1 = ((List)localObject1).iterator();
      }
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (String)((Iterator)localObject1).next();
        try
        {
          Intent localIntent = (Intent)paramIntent.clone();
          ComponentName localComponentName = new android/content/ComponentName;
          localComponentName.<init>(paramContext.getPackageName(), (String)localObject2);
          localIntent.setComponent(localComponentName);
          paramContext.sendBroadcast(localIntent);
        }
        catch (Exception localException)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append("sendBroadcast failed again:");
          ((StringBuilder)localObject2).append(localException.getMessage());
          ((StringBuilder)localObject2).append(", action:");
          ((StringBuilder)localObject2).append(paramIntent.getAction());
          cn.jiguang.ai.a.h("AndroidUtil", ((StringBuilder)localObject2).toString());
        }
        continue;
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext.append("sendBroadcast failed again: receiver not found, action:");
        paramContext.append(paramIntent.getAction());
        cn.jiguang.ai.a.h("AndroidUtil", paramContext.toString());
      }
    }
    catch (Throwable paramContext)
    {
      paramIntent = new StringBuilder();
      paramIntent.append("tryAgainSendBrocast failed:");
      paramIntent.append(paramContext.getMessage());
      cn.jiguang.ai.a.h("AndroidUtil", paramIntent.toString());
    }
  }
  
  public static void b(Context paramContext, final String paramString)
  {
    new Handler(Looper.getMainLooper()).post(new Runnable()
    {
      public void run()
      {
        Toast.makeText(this.a, paramString, 0).show();
      }
    });
  }
  
  public static String c()
  {
    try
    {
      do
      {
        Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
        Enumeration localEnumeration2;
        while (!localEnumeration2.hasMoreElements())
        {
          if (!localEnumeration1.hasMoreElements()) {
            break;
          }
          localEnumeration2 = ((NetworkInterface)localEnumeration1.nextElement()).getInetAddresses();
        }
        localObject = (InetAddress)localEnumeration2.nextElement();
      } while ((((InetAddress)localObject).isLoopbackAddress()) || (!(localObject instanceof Inet4Address)));
      Object localObject = ((InetAddress)localObject).getHostAddress();
      return (String)localObject;
    }
    catch (Exception localException)
    {
      cn.jiguang.ai.a.g("AndroidUtil", "getPhoneIp:");
      localException.printStackTrace();
    }
    return "";
  }
  
  @SuppressLint({"MissingPermission"})
  public static String c(Context paramContext, String paramString)
  {
    boolean bool = cn.jiguang.sdk.impl.a.b;
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (!bool)
    {
      localObject1 = localObject2;
      try
      {
        if (a(paramContext, "android.permission.READ_PHONE_STATE")) {
          localObject1 = ((TelephonyManager)paramContext.getSystemService("phone")).getDeviceId();
        }
      }
      catch (Exception paramContext)
      {
        cn.jiguang.ai.a.i("AndroidUtil", paramContext.getMessage());
        localObject1 = localObject2;
      }
    }
    if (g.g((String)localObject1)) {
      return (String)localObject1;
    }
    return paramString;
  }
  
  public static boolean c(Context paramContext)
  {
    boolean bool2 = false;
    try
    {
      if (a(paramContext, "android.permission.ACCESS_NETWORK_STATE"))
      {
        paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
        boolean bool1 = bool2;
        if (paramContext != null)
        {
          boolean bool3 = paramContext.isConnected();
          bool1 = bool2;
          if (bool3) {
            bool1 = true;
          }
        }
        return bool1;
      }
    }
    catch (Throwable paramContext)
    {
      paramContext.printStackTrace();
    }
    return false;
  }
  
  @SuppressLint({"MissingPermission"})
  public static String d(Context paramContext, String paramString)
  {
    Object localObject = paramString;
    if (!cn.jiguang.sdk.impl.a.b)
    {
      localObject = paramString;
      try
      {
        if (a(paramContext, "android.permission.READ_PHONE_STATE")) {
          localObject = ((TelephonyManager)paramContext.getSystemService("phone")).getSubscriberId();
        }
      }
      catch (Throwable paramContext)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("getImsi failed:");
        ((StringBuilder)localObject).append(paramContext.getMessage());
        cn.jiguang.ai.a.g("AndroidUtil", ((StringBuilder)localObject).toString());
        localObject = paramString;
      }
    }
    return (String)localObject;
  }
  
  public static boolean d(Context paramContext)
  {
    paramContext = paramContext.getApplicationInfo().sourceDir;
    if (g.a(paramContext))
    {
      cn.jiguang.ai.a.i("AndroidUtil", "Unexpected: cannot get pk installed path");
      return false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Current pk installed path: ");
    localStringBuilder.append(paramContext);
    cn.jiguang.ai.a.c("AndroidUtil", localStringBuilder.toString());
    if (paramContext.startsWith("/system/app/")) {
      return true;
    }
    if (paramContext.startsWith("/data/app/")) {
      return false;
    }
    cn.jiguang.ai.a.e("AndroidUtil", "NOTE: the pk does not installed in system/data. ");
    return false;
  }
  
  public static String e(Context paramContext)
  {
    if ((paramContext != null) && (paramContext.getResources() != null))
    {
      paramContext = paramContext.getResources().getDisplayMetrics();
      if (paramContext == null) {
        return "0*0";
      }
      int i = paramContext.widthPixels;
      int j = paramContext.heightPixels;
      paramContext = new StringBuilder();
      paramContext.append(i);
      paramContext.append("*");
      paramContext.append(j);
      return paramContext.toString();
    }
    return "0*0";
  }
  
  public static String e(Context paramContext, String paramString)
  {
    if (!cn.jiguang.sdk.impl.a.b)
    {
      Object localObject = m(paramContext);
      if (!g.h((String)localObject)) {
        paramContext = n(paramContext);
      } else {
        paramContext = (Context)localObject;
      }
      localObject = paramContext;
      if (!g.h(paramContext)) {
        localObject = paramString;
      }
      paramContext = new StringBuilder();
      paramContext.append("getWifiMac:");
      paramContext.append((String)localObject);
      cn.jiguang.ai.a.c("AndroidUtil", paramContext.toString());
      return (String)localObject;
    }
    return paramString;
  }
  
  /* Error */
  public static boolean f(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 393	android/content/Context:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
    //   4: getfield 637	android/content/pm/ApplicationInfo:flags	I
    //   7: iconst_2
    //   8: iand
    //   9: ifeq +8 -> 17
    //   12: iconst_1
    //   13: istore_3
    //   14: goto +5 -> 19
    //   17: iconst_0
    //   18: istore_3
    //   19: new 92	java/lang/StringBuilder
    //   22: astore 6
    //   24: aload 6
    //   26: invokespecial 95	java/lang/StringBuilder:<init>	()V
    //   29: aload 6
    //   31: ldc_w 639
    //   34: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload 6
    //   40: iload_3
    //   41: invokevirtual 642	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   44: pop
    //   45: ldc 107
    //   47: aload 6
    //   49: invokevirtual 110	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   52: invokestatic 220	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   55: new 644	javax/security/auth/x500/X500Principal
    //   58: astore 7
    //   60: aload 7
    //   62: ldc_w 646
    //   65: invokespecial 647	javax/security/auth/x500/X500Principal:<init>	(Ljava/lang/String;)V
    //   68: iconst_3
    //   69: anewarray 26	java/lang/String
    //   72: astore 8
    //   74: aload 8
    //   76: iconst_0
    //   77: ldc_w 649
    //   80: aastore
    //   81: aload 8
    //   83: iconst_1
    //   84: ldc_w 651
    //   87: aastore
    //   88: aload 8
    //   90: iconst_2
    //   91: ldc_w 653
    //   94: aastore
    //   95: aload_0
    //   96: invokevirtual 58	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   99: aload_0
    //   100: invokevirtual 136	android/content/Context:getPackageName	()Ljava/lang/String;
    //   103: bipush 64
    //   105: invokevirtual 64	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   108: getfield 657	android/content/pm/PackageInfo:signatures	[Landroid/content/pm/Signature;
    //   111: astore 10
    //   113: ldc_w 659
    //   116: invokestatic 665	java/security/cert/CertificateFactory:getInstance	(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
    //   119: astore 9
    //   121: aload 10
    //   123: arraylength
    //   124: istore_2
    //   125: iconst_0
    //   126: istore_1
    //   127: iconst_0
    //   128: istore 4
    //   130: iload 4
    //   132: istore_3
    //   133: iload_1
    //   134: iload_2
    //   135: if_icmpge +248 -> 383
    //   138: aload 10
    //   140: iload_1
    //   141: aaload
    //   142: astore 6
    //   144: iload 4
    //   146: istore_3
    //   147: new 667	java/io/ByteArrayInputStream
    //   150: astore_0
    //   151: iload 4
    //   153: istore_3
    //   154: aload_0
    //   155: aload 6
    //   157: invokevirtual 673	android/content/pm/Signature:toByteArray	()[B
    //   160: invokespecial 676	java/io/ByteArrayInputStream:<init>	([B)V
    //   163: iload 4
    //   165: istore_3
    //   166: aload 9
    //   168: aload_0
    //   169: invokevirtual 680	java/security/cert/CertificateFactory:generateCertificate	(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   172: checkcast 682	java/security/cert/X509Certificate
    //   175: astore 6
    //   177: iload 4
    //   179: istore_3
    //   180: aload 6
    //   182: invokevirtual 686	java/security/cert/X509Certificate:getSubjectX500Principal	()Ljavax/security/auth/x500/X500Principal;
    //   185: aload 7
    //   187: invokevirtual 687	javax/security/auth/x500/X500Principal:equals	(Ljava/lang/Object;)Z
    //   190: istore 4
    //   192: iload 4
    //   194: istore_3
    //   195: new 92	java/lang/StringBuilder
    //   198: astore_0
    //   199: iload 4
    //   201: istore_3
    //   202: aload_0
    //   203: invokespecial 95	java/lang/StringBuilder:<init>	()V
    //   206: iload 4
    //   208: istore_3
    //   209: aload_0
    //   210: ldc_w 689
    //   213: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: iload 4
    //   219: istore_3
    //   220: aload_0
    //   221: iload 4
    //   223: invokevirtual 642	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: iload 4
    //   229: istore_3
    //   230: ldc 107
    //   232: aload_0
    //   233: invokevirtual 110	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   236: invokestatic 220	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   239: iload 4
    //   241: ifeq +9 -> 250
    //   244: iload 4
    //   246: istore_3
    //   247: goto +136 -> 383
    //   250: aconst_null
    //   251: astore_0
    //   252: iload 4
    //   254: istore_3
    //   255: aload 6
    //   257: invokevirtual 686	java/security/cert/X509Certificate:getSubjectX500Principal	()Ljavax/security/auth/x500/X500Principal;
    //   260: invokevirtual 690	javax/security/auth/x500/X500Principal:getName	()Ljava/lang/String;
    //   263: astore 6
    //   265: aload 6
    //   267: astore_0
    //   268: iload 4
    //   270: istore_3
    //   271: new 92	java/lang/StringBuilder
    //   274: astore 6
    //   276: iload 4
    //   278: istore_3
    //   279: aload 6
    //   281: invokespecial 95	java/lang/StringBuilder:<init>	()V
    //   284: iload 4
    //   286: istore_3
    //   287: aload 6
    //   289: ldc_w 692
    //   292: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: pop
    //   296: iload 4
    //   298: istore_3
    //   299: aload 6
    //   301: aload_0
    //   302: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: pop
    //   306: iload 4
    //   308: istore_3
    //   309: ldc 107
    //   311: aload 6
    //   313: invokevirtual 110	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   316: invokestatic 220	cn/jiguang/ai/a:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   319: aload_0
    //   320: ifnull +54 -> 374
    //   323: iload 4
    //   325: istore_3
    //   326: aload_0
    //   327: aload 8
    //   329: iconst_0
    //   330: aaload
    //   331: invokevirtual 695	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   334: ifeq +40 -> 374
    //   337: iload 4
    //   339: istore_3
    //   340: aload_0
    //   341: aload 8
    //   343: iconst_1
    //   344: aaload
    //   345: invokevirtual 695	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   348: ifeq +26 -> 374
    //   351: iload 4
    //   353: istore_3
    //   354: aload_0
    //   355: aload 8
    //   357: iconst_2
    //   358: aaload
    //   359: invokevirtual 695	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   362: istore 5
    //   364: iload 5
    //   366: ifeq +8 -> 374
    //   369: iconst_1
    //   370: istore_3
    //   371: goto +12 -> 383
    //   374: iinc 1 1
    //   377: goto -247 -> 130
    //   380: astore_0
    //   381: iconst_0
    //   382: istore_3
    //   383: iload_3
    //   384: ireturn
    //   385: astore_0
    //   386: goto -3 -> 383
    //   389: astore 6
    //   391: goto -123 -> 268
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	394	0	paramContext	Context
    //   126	249	1	i	int
    //   124	12	2	j	int
    //   13	371	3	bool1	boolean
    //   128	224	4	bool2	boolean
    //   362	3	5	bool3	boolean
    //   22	290	6	localObject	Object
    //   389	1	6	localException	Exception
    //   58	128	7	localX500Principal	javax.security.auth.x500.X500Principal
    //   72	284	8	arrayOfString	String[]
    //   119	48	9	localCertificateFactory	java.security.cert.CertificateFactory
    //   111	28	10	arrayOfSignature	android.content.pm.Signature[]
    // Exception table:
    //   from	to	target	type
    //   0	12	380	java/lang/Throwable
    //   19	74	380	java/lang/Throwable
    //   95	125	380	java/lang/Throwable
    //   147	151	385	java/lang/Throwable
    //   154	163	385	java/lang/Throwable
    //   166	177	385	java/lang/Throwable
    //   180	192	385	java/lang/Throwable
    //   195	199	385	java/lang/Throwable
    //   202	206	385	java/lang/Throwable
    //   209	217	385	java/lang/Throwable
    //   220	227	385	java/lang/Throwable
    //   230	239	385	java/lang/Throwable
    //   255	265	385	java/lang/Throwable
    //   271	276	385	java/lang/Throwable
    //   279	284	385	java/lang/Throwable
    //   287	296	385	java/lang/Throwable
    //   299	306	385	java/lang/Throwable
    //   309	319	385	java/lang/Throwable
    //   326	337	385	java/lang/Throwable
    //   340	351	385	java/lang/Throwable
    //   354	364	385	java/lang/Throwable
    //   255	265	389	java/lang/Exception
  }
  
  public static boolean f(Context paramContext, String paramString)
  {
    if (g.a(paramString)) {
      return false;
    }
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramString, 0);
      if (paramContext != null) {
        return true;
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static String g(Context paramContext)
  {
    String str = "";
    if (cn.jiguang.sdk.impl.a.b) {
      return "";
    }
    try
    {
      paramContext = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
      if (g.g(paramContext)) {
        return paramContext;
      }
      return "";
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        paramContext = str;
      }
    }
  }
  
  public static boolean g(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString))) {
      try
      {
        paramContext.getPackageManager().getPermissionInfo(paramString, 128);
        return true;
      }
      catch (Throwable paramString)
      {
        paramContext = new StringBuilder();
        paramContext.append("hasPermissionDefined error:");
        paramContext.append(paramString.getMessage());
        cn.jiguang.ai.a.h("AndroidUtil", paramContext.toString());
        return false;
      }
    }
    throw new IllegalArgumentException("empty params");
  }
  
  public static String h(Context paramContext)
  {
    try
    {
      paramContext = e(paramContext, "");
      if ((paramContext != null) && (!paramContext.equals("")))
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append("MAC addr info---- ");
        localStringBuilder.append(paramContext);
        cn.jiguang.ai.a.e("AndroidUtil", localStringBuilder.toString());
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramContext.toLowerCase());
        localStringBuilder.append(Build.MODEL);
        paramContext = g.c(localStringBuilder.toString());
        return paramContext;
      }
      return null;
    }
    catch (Exception paramContext)
    {
      cn.jiguang.ai.a.b("AndroidUtil", "", paramContext);
    }
    return null;
  }
  
  public static boolean i(Context paramContext)
  {
    String str = a(paramContext, "ro.product.brand", "");
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("brand = ");
    ((StringBuilder)localObject).append(str);
    cn.jiguang.ai.a.c("AndroidUtil", ((StringBuilder)localObject).toString());
    localObject = a(paramContext, "ro.miui.ui.version.name", "");
    if ((!TextUtils.isEmpty(str)) && ("Xiaomi".equals(str)) && (!TextUtils.isEmpty((CharSequence)localObject)))
    {
      paramContext = a(paramContext, "ro.build.version.incremental", "");
      if ((!TextUtils.isEmpty(paramContext)) && (paramContext.startsWith("V7.1")))
      {
        cn.jiguang.ai.a.g("AndroidUtil", "7.1 will not get wifi list");
        return false;
      }
    }
    return true;
  }
  
  public static String j(Context paramContext)
  {
    try
    {
      paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if (paramContext == null) {
        return "Unknown";
      }
      String str1 = paramContext.getTypeName();
      String str2 = paramContext.getSubtypeName();
      if (str1 == null)
      {
        paramContext = "Unknown";
      }
      else
      {
        paramContext = str1;
        if (!g.a(str2))
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append(str1);
          paramContext.append(",");
          paramContext.append(str2);
          paramContext = paramContext.toString();
        }
      }
      return paramContext;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
    return "Unknown";
  }
  
  public static String k(Context paramContext)
  {
    String str = "";
    localObject2 = str;
    try
    {
      WifiManager localWifiManager = (WifiManager)paramContext.getApplicationContext().getSystemService("wifi");
      localObject1 = str;
      if (localWifiManager != null)
      {
        localObject1 = str;
        localObject2 = str;
        if (a(paramContext, "android.permission.ACCESS_WIFI_STATE"))
        {
          localObject2 = str;
          paramContext = localWifiManager.getConnectionInfo().getBSSID();
          if (paramContext != null)
          {
            localObject1 = paramContext;
            localObject2 = paramContext;
            if (!paramContext.startsWith("02:00:00:")) {}
          }
          else
          {
            localObject1 = "";
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        Object localObject1 = localObject2;
      }
    }
    return (String)localObject1;
  }
  
  public static String l(Context paramContext)
  {
    Object localObject2 = null;
    Object localObject3 = null;
    int j = -1;
    Object localObject1 = localObject2;
    int i = j;
    if (paramContext != null) {
      try
      {
        paramContext = paramContext.getApplicationContext().getSystemService("country_detector");
        if (paramContext != null)
        {
          Object localObject4 = paramContext.getClass().getDeclaredMethod("detectCountry", new Class[0]);
          localObject1 = localObject2;
          i = j;
          if (localObject4 == null) {
            break label206;
          }
          localObject4 = ((Method)localObject4).invoke(paramContext, new Object[0]);
          localObject1 = localObject2;
          i = j;
          if (localObject4 == null) {
            break label206;
          }
          paramContext = (String)localObject4.getClass().getDeclaredMethod("getCountryIso", new Class[0]).invoke(localObject4, new Object[0]);
          try
          {
            i = ((Integer)localObject4.getClass().getDeclaredMethod("getSource", new Class[0]).invoke(localObject4, new Object[0])).intValue();
            localObject1 = paramContext;
          }
          catch (Throwable localThrowable1)
          {
            localObject1 = paramContext;
            break label172;
          }
        }
        else
        {
          cn.jiguang.ai.a.c("AndroidUtil", "country_detector is null");
          localObject1 = localThrowable1;
          i = j;
        }
      }
      catch (Throwable localThrowable2)
      {
        localObject1 = localObject3;
        label172:
        paramContext = new StringBuilder();
        paramContext.append("getCountryCode failed, error :");
        paramContext.append(localThrowable2);
        cn.jiguang.ai.a.i("AndroidUtil", paramContext.toString());
        i = j;
      }
    }
    label206:
    paramContext = new StringBuilder();
    paramContext.append("get CountCode = ");
    paramContext.append((String)localObject1);
    paramContext.append(" source = ");
    paramContext.append(i);
    cn.jiguang.ai.a.c("AndroidUtil", paramContext.toString());
    if ((i != 0) && (i != 1)) {
      return "";
    }
    return (String)localObject1;
  }
  
  @SuppressLint({"MissingPermission"})
  private static String m(Context paramContext)
  {
    Object localObject2 = "";
    Object localObject1 = localObject2;
    if (!cn.jiguang.sdk.impl.a.b)
    {
      localObject1 = localObject2;
      if (Build.VERSION.SDK_INT < 23)
      {
        localObject1 = localObject2;
        if (a(paramContext, "android.permission.ACCESS_WIFI_STATE"))
        {
          localObject1 = localObject2;
          try
          {
            paramContext = ((WifiManager)paramContext.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getMacAddress();
            localObject1 = paramContext;
            localObject2 = new java/lang/StringBuilder;
            localObject1 = paramContext;
            ((StringBuilder)localObject2).<init>();
            localObject1 = paramContext;
            ((StringBuilder)localObject2).append("mac address from WifiManager:");
            localObject1 = paramContext;
            ((StringBuilder)localObject2).append(paramContext);
            localObject1 = paramContext;
            cn.jiguang.ai.a.c("AndroidUtil", ((StringBuilder)localObject2).toString());
            localObject1 = paramContext;
          }
          catch (Exception paramContext)
          {
            cn.jiguang.ai.a.a("AndroidUtil", "get mac from wifiManager failed ", paramContext);
          }
        }
      }
    }
    return (String)localObject1;
  }
  
  private static String n(Context paramContext)
  {
    Object localObject1 = "";
    Object localObject2 = localObject1;
    Object localObject3;
    if (!cn.jiguang.sdk.impl.a.b)
    {
      boolean bool1;
      try
      {
        if (a(paramContext, "android.permission.ACCESS_WIFI_STATE")) {
          bool1 = ((WifiManager)paramContext.getApplicationContext().getSystemService("wifi")).isWifiEnabled();
        } else {
          bool1 = false;
        }
        localObject2 = localObject1;
        try
        {
          Object localObject4 = NetworkInterface.getNetworkInterfaces();
          do
          {
            do
            {
              do
              {
                localObject2 = localObject1;
                paramContext = (Context)localObject1;
                bool2 = bool1;
                if (!((Enumeration)localObject4).hasMoreElements()) {
                  break;
                }
                localObject2 = localObject1;
                paramContext = (NetworkInterface)((Enumeration)localObject4).nextElement();
                localObject2 = localObject1;
              } while (!"wlan0".equalsIgnoreCase(paramContext.getName()));
              localObject2 = localObject1;
              paramContext = paramContext.getHardwareAddress();
            } while (paramContext == null);
            localObject2 = localObject1;
          } while (paramContext.length == 0);
          localObject2 = localObject1;
          localObject4 = new java/lang/StringBuilder;
          localObject2 = localObject1;
          ((StringBuilder)localObject4).<init>();
          localObject2 = localObject1;
          int j = paramContext.length;
          for (int i = 0; i < j; i++)
          {
            byte b1 = paramContext[i];
            localObject2 = localObject1;
            ((StringBuilder)localObject4).append(String.format(Locale.ENGLISH, "%02x:", new Object[] { Byte.valueOf(b1) }));
          }
          localObject2 = localObject1;
          if (((StringBuilder)localObject4).length() > 0)
          {
            localObject2 = localObject1;
            ((StringBuilder)localObject4).deleteCharAt(((StringBuilder)localObject4).length() - 1);
          }
          localObject2 = localObject1;
          paramContext = ((StringBuilder)localObject4).toString();
          localObject2 = paramContext;
          localObject1 = new java/lang/StringBuilder;
          localObject2 = paramContext;
          ((StringBuilder)localObject1).<init>();
          localObject2 = paramContext;
          ((StringBuilder)localObject1).append("mac address from NetworkInterface:");
          localObject2 = paramContext;
          ((StringBuilder)localObject1).append(paramContext);
          localObject2 = paramContext;
          cn.jiguang.ai.a.c("AndroidUtil", ((StringBuilder)localObject1).toString());
          bool2 = bool1;
        }
        catch (Exception localException1)
        {
          paramContext = (Context)localObject2;
          localObject2 = localException1;
        }
        localStringBuilder = new StringBuilder();
      }
      catch (Exception localException2)
      {
        bool1 = false;
        paramContext = localException1;
      }
      StringBuilder localStringBuilder;
      localStringBuilder.append("get mac from NetworkInterface failed:");
      localStringBuilder.append(localException2.getMessage());
      cn.jiguang.ai.a.g("AndroidUtil", localStringBuilder.toString());
      boolean bool2 = bool1;
      localObject3 = paramContext;
      if (!bool2)
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("mac address is dropped, which is ");
        localStringBuilder.append(paramContext);
        cn.jiguang.ai.a.c("AndroidUtil", localStringBuilder.toString());
        localObject3 = "";
      }
    }
    return (String)localObject3;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */