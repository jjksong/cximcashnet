package cn.jiguang.ap;

import cn.jiguang.ai.a;
import java.math.BigInteger;

public class d
{
  private static final BigInteger d = BigInteger.ONE.shiftLeft(64);
  private byte[] a;
  private int b;
  private int c;
  
  public d()
  {
    this(32);
  }
  
  public d(int paramInt)
  {
    this.a = new byte[paramInt];
    this.b = 0;
    this.c = -1;
  }
  
  private void a(long paramLong, int paramInt)
  {
    long l = 1L << paramInt;
    if ((paramLong < 0L) || (paramLong > l))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" out of range for ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" bit value max:");
      localStringBuilder.append(l);
      a.g("Outputer", localStringBuilder.toString());
    }
  }
  
  private void c(int paramInt)
  {
    byte[] arrayOfByte = this.a;
    int i = arrayOfByte.length;
    int k = this.b;
    if (i - k >= paramInt) {
      return;
    }
    int j = arrayOfByte.length * 2;
    i = j;
    if (j < k + paramInt) {
      i = k + paramInt;
    }
    arrayOfByte = new byte[i];
    System.arraycopy(this.a, 0, arrayOfByte, 0, this.b);
    this.a = arrayOfByte;
  }
  
  public int a()
  {
    return this.b;
  }
  
  public void a(int paramInt)
  {
    a(paramInt, 8);
    c(1);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    a(paramInt1, 8);
    if (paramInt2 <= this.b - 1)
    {
      this.a[paramInt2] = ((byte)(paramInt1 & 0xFF));
      return;
    }
    throw new IllegalArgumentException("cannot write past end of data");
  }
  
  public void a(long paramLong)
  {
    a(paramLong, 32);
    c(4);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public void a(String paramString)
  {
    paramString = g.i(paramString);
    b(paramString.length);
    a(paramString, 0, paramString.length);
  }
  
  public void a(byte[] paramArrayOfByte)
  {
    a(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    c(paramInt2);
    System.arraycopy(paramArrayOfByte, paramInt1, this.a, this.b, paramInt2);
    this.b += paramInt2;
  }
  
  public void b(int paramInt)
  {
    a(paramInt, 16);
    c(2);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt >>> 8 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
  }
  
  public void b(int paramInt1, int paramInt2)
  {
    a(paramInt1, 16);
    if (paramInt2 <= this.b - 2)
    {
      byte[] arrayOfByte = this.a;
      arrayOfByte[paramInt2] = ((byte)(paramInt1 >>> 8 & 0xFF));
      arrayOfByte[(paramInt2 + 1)] = ((byte)(paramInt1 & 0xFF));
      return;
    }
    throw new IllegalArgumentException("cannot write past end of data");
  }
  
  public void b(long paramLong)
  {
    c(8);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 56 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 48 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 40 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 32 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 24 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 16 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong >>> 8 & 0xFF));
    i = this.b;
    this.b = (i + 1);
    arrayOfByte[i] = ((byte)(int)(paramLong & 0xFF));
  }
  
  public byte[] b()
  {
    int i = this.b;
    byte[] arrayOfByte = new byte[i];
    System.arraycopy(this.a, 0, arrayOfByte, 0, i);
    return arrayOfByte;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */