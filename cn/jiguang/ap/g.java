package cn.jiguang.ap;

import android.text.TextUtils;
import cn.jiguang.ai.a;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class g
{
  private static Pattern a = Pattern.compile("((2[0-4]\\d|25[0-5]|[01]?\\d{1,2})\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d{1,2})");
  private static SimpleDateFormat b = new SimpleDateFormat("HHH:mm:ss:SSS", Locale.ENGLISH);
  
  public static String a(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      return "";
    }
    StringBuffer localStringBuffer = new StringBuffer(paramArrayOfByte.length * 2);
    int j = paramArrayOfByte.length;
    for (int i = 0; i < j; i++) {
      a(localStringBuffer, paramArrayOfByte[i]);
    }
    return localStringBuffer.toString();
  }
  
  private static void a(StringBuffer paramStringBuffer, byte paramByte)
  {
    paramStringBuffer.append("0123456789ABCDEF".charAt(paramByte >> 4 & 0xF));
    paramStringBuffer.append("0123456789ABCDEF".charAt(paramByte & 0xF));
  }
  
  public static boolean a(String paramString)
  {
    if (paramString == null) {
      return true;
    }
    if (paramString.length() == 0) {
      return true;
    }
    return paramString.trim().length() == 0;
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, 0);
  }
  
  public static boolean a(String paramString1, String paramString2, int paramInt)
  {
    int i = paramInt;
    if (paramInt < 0) {
      i = 0;
    }
    if (paramString1.length() < paramString2.length() + i) {
      return false;
    }
    for (paramInt = 0; paramInt < paramString2.length(); paramInt++)
    {
      char c2 = paramString1.charAt(i + paramInt);
      char c1 = paramString2.charAt(paramInt);
      if ((c2 != c1) && (Character.toLowerCase(c2) != Character.toLowerCase(c1))) {
        return false;
      }
    }
    return true;
  }
  
  public static String b(String paramString)
  {
    if (a(paramString)) {
      return "";
    }
    return Pattern.compile("[^\\w#$@\\-一-龥]+").matcher(paramString).replaceAll("");
  }
  
  public static String c(String paramString)
  {
    try
    {
      Object localObject = MessageDigest.getInstance("MD5");
      paramString = paramString.toCharArray();
      byte[] arrayOfByte = new byte[paramString.length];
      int j = 0;
      for (int i = 0; i < paramString.length; i++) {
        arrayOfByte[i] = ((byte)paramString[i]);
      }
      paramString = ((MessageDigest)localObject).digest(arrayOfByte);
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      for (i = j; i < paramString.length; i++)
      {
        j = paramString[i] & 0xFF;
        if (j < 16) {
          ((StringBuffer)localObject).append("0");
        }
        ((StringBuffer)localObject).append(Integer.toHexString(j));
      }
      paramString = ((StringBuffer)localObject).toString();
      return paramString;
    }
    catch (Exception paramString) {}
    return "";
  }
  
  public static String d(String paramString)
  {
    try
    {
      byte[] arrayOfByte = MessageDigest.getInstance("MD5").digest(paramString.getBytes("utf-8"));
      paramString = new java/lang/StringBuffer;
      paramString.<init>();
      for (int i = 0; i < arrayOfByte.length; i++)
      {
        int j = arrayOfByte[i] & 0xFF;
        if (j < 16) {
          paramString.append("0");
        }
        paramString.append(Integer.toHexString(j));
      }
      paramString = paramString.toString();
      return paramString;
    }
    catch (Throwable paramString)
    {
      a.c("StringUtils", "Get MD5 error");
    }
    return "";
  }
  
  public static boolean e(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    int i = paramString.indexOf(":");
    String str = paramString;
    if (i >= 0) {
      if (i == paramString.lastIndexOf(":")) {
        str = paramString.substring(0, i);
      } else {
        return false;
      }
    }
    return a.matcher(str).matches();
  }
  
  public static boolean f(String paramString)
  {
    boolean bool2 = TextUtils.isEmpty(paramString);
    boolean bool1 = false;
    if (bool2) {
      return false;
    }
    int i = paramString.indexOf(":");
    if (i == -1) {
      return false;
    }
    if (paramString.lastIndexOf(":") != i) {
      bool1 = true;
    }
    return bool1;
  }
  
  public static boolean g(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    try
    {
      boolean bool = Pattern.compile("[\\x20-\\x7E]+").matcher(paramString).matches();
      return bool;
    }
    catch (Throwable paramString) {}
    return true;
  }
  
  public static boolean h(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    try
    {
      boolean bool = Pattern.compile("([A-Fa-f0-9]{2}[-:]){5,}[A-Fa-f0-9]{2}").matcher(paramString).matches();
      return bool;
    }
    catch (Throwable paramString) {}
    return true;
  }
  
  public static byte[] i(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return new byte[0];
    }
    try
    {
      byte[] arrayOfByte = paramString.getBytes("UTF-8");
      return arrayOfByte;
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("stringToUtf8Bytes error:");
      localStringBuilder.append(localThrowable.getMessage());
      a.j("StringUtils", localStringBuilder.toString());
    }
    return paramString.getBytes();
  }
  
  public static String j(String paramString)
  {
    if ((paramString != null) && (!"".equals(paramString))) {}
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      localMessageDigest.update(paramString.getBytes());
      paramString = a(localMessageDigest.digest());
      return paramString;
    }
    catch (NoSuchAlgorithmException paramString)
    {
      for (;;) {}
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */