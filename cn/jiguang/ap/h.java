package cn.jiguang.ap;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import cn.jiguang.ai.a;
import java.lang.reflect.InvocationTargetException;

public class h
{
  public static int a(Context paramContext)
  {
    paramContext = c(paramContext);
    if (!TextUtils.isEmpty(paramContext))
    {
      if ("wifi".equals(paramContext)) {
        return 1;
      }
      if ("2g".equals(paramContext)) {
        return 2;
      }
      if ("3g".equals(paramContext)) {
        return 3;
      }
      if ("4g".equals(paramContext)) {
        return 4;
      }
    }
    return 0;
  }
  
  public static String a(int paramInt)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("getRadioType - networkType:");
    ((StringBuilder)localObject).append(paramInt);
    a.a("TeleonyManagerUtils", ((StringBuilder)localObject).toString());
    if ((paramInt != 4) && (paramInt != 7) && (paramInt != 5) && (paramInt != 6) && (paramInt != 12) && (paramInt != 14))
    {
      if (paramInt == 13) {
        localObject = "lte";
      } else {
        localObject = "gsm";
      }
    }
    else {
      localObject = "cdma";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("getRadioType - radioType:");
    localStringBuilder.append((String)localObject);
    a.a("TeleonyManagerUtils", localStringBuilder.toString());
    return (String)localObject;
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    Object localObject2 = c(paramContext);
    paramContext = new StringBuilder();
    paramContext.append("getCurrentNetType - type:");
    paramContext.append((String)localObject2);
    a.a("TeleonyManagerUtils", paramContext.toString());
    paramContext = (Context)localObject2;
    if (TextUtils.isEmpty((CharSequence)localObject2))
    {
      localObject1 = localObject2;
      try
      {
        paramContext = Integer.TYPE;
        localObject1 = localObject2;
        Object localObject3 = e.a(TelephonyManager.class, "getNetworkClass", new Object[] { Integer.valueOf(paramInt) }, new Class[] { paramContext });
        localObject1 = localObject2;
        if (((Integer)localObject3).intValue() == 0)
        {
          paramContext = "unknown";
        }
        else
        {
          localObject1 = localObject2;
          if (((Integer)localObject3).intValue() == 1)
          {
            paramContext = "2g";
          }
          else
          {
            localObject1 = localObject2;
            if (((Integer)localObject3).intValue() == 2)
            {
              paramContext = "3g";
            }
            else
            {
              paramContext = (Context)localObject2;
              localObject1 = localObject2;
              if (((Integer)localObject3).intValue() == 3) {
                paramContext = "4g";
              }
            }
          }
        }
        localObject1 = paramContext;
        localObject2 = new java/lang/StringBuilder;
        localObject1 = paramContext;
        ((StringBuilder)localObject2).<init>();
        localObject1 = paramContext;
        ((StringBuilder)localObject2).append("step2 - type:");
        localObject1 = paramContext;
        ((StringBuilder)localObject2).append(paramContext);
        localObject1 = paramContext;
        a.c("TeleonyManagerUtils", ((StringBuilder)localObject2).toString());
      }
      catch (NoSuchMethodException|IllegalAccessException|InvocationTargetException|Exception paramContext)
      {
        paramContext = (Context)localObject1;
      }
    }
    Object localObject1 = paramContext;
    if (TextUtils.isEmpty(paramContext)) {
      localObject1 = "unknown";
    }
    return (String)localObject1;
  }
  
  private static String b(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("getNetworkClass networkType:");
    localStringBuilder.append(paramInt);
    a.a("TeleonyManagerUtils", localStringBuilder.toString());
    switch (paramInt)
    {
    default: 
      switch (paramInt)
      {
      default: 
        return "unknown";
      }
    case 13: 
      return "4g";
    case 3: 
    case 5: 
    case 6: 
    case 8: 
    case 9: 
    case 10: 
    case 12: 
    case 14: 
    case 15: 
      return "3g";
    }
    return "2g";
    return "4g";
    return "3g";
    return "2g";
  }
  
  public static String b(Context paramContext)
  {
    Object localObject = (TelephonyManager)paramContext.getSystemService("phone");
    paramContext = "";
    try
    {
      localObject = ((TelephonyManager)localObject).getNetworkOperator();
      paramContext = (Context)localObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return paramContext;
  }
  
  public static String c(Context paramContext)
  {
    String str = "unknown";
    try
    {
      NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      paramContext = str;
      if (localNetworkInfo != null)
      {
        if (localNetworkInfo.getType() == 1) {}
        for (paramContext = "wifi";; paramContext = b(localNetworkInfo.getSubtype()))
        {
          break;
          paramContext = str;
          if (localNetworkInfo.getType() != 0) {
            break;
          }
        }
      }
      return paramContext;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
      paramContext = str;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/ap/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */