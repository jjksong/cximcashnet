package cn.jiguang.aj;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import cn.jiguang.ae.c;
import cn.jiguang.ap.g;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class a
{
  private static volatile a a;
  private static final Object b = new Object();
  private long c = 36000000L;
  private long d = 900000L;
  private final Map<String, Pair<InetAddress[], Long>> e = new HashMap();
  
  private Pair<InetAddress[], Boolean> a(Context paramContext, String paramString, long paramLong1, long paramLong2)
  {
    Pair localPair = (Pair)this.e.get(paramString);
    boolean bool = false;
    int i;
    if ((localPair != null) && (localPair.first != null)) {
      i = 0;
    } else {
      i = 1;
    }
    Object localObject;
    if (i != 0) {
      localObject = c.a(paramContext, cn.jiguang.ae.b.c(paramString));
    } else {
      localObject = localPair.second;
    }
    long l2 = ((Long)localObject).longValue();
    long l1 = System.currentTimeMillis();
    if (l1 > l2 + paramLong1) {
      return null;
    }
    if (i != 0)
    {
      localObject = a(paramContext, paramString);
      paramContext = (Context)localObject;
      if (localObject != null)
      {
        paramContext = new Pair(localObject, Long.valueOf(l2));
        this.e.put(paramString, paramContext);
        paramContext = (Context)localObject;
      }
    }
    else
    {
      paramContext = (InetAddress[])localPair.first;
    }
    if (paramContext != null)
    {
      if (l1 > l2 + paramLong2) {
        bool = true;
      }
      return new Pair(paramContext, Boolean.valueOf(bool));
    }
    return null;
  }
  
  public static a a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          a locala = new cn/jiguang/aj/a;
          locala.<init>();
          a = locala;
        }
      }
    }
    return a;
  }
  
  private InetAddress a(String paramString)
  {
    if ((g.e(paramString)) || (g.f(paramString))) {
      try
      {
        paramString = InetAddress.getByName(paramString);
      }
      catch (UnknownHostException localUnknownHostException)
      {
        paramString = new StringBuilder();
        paramString.append("dns resolve failed:");
        paramString.append(localUnknownHostException);
        cn.jiguang.ai.a.g("DNSLoader_xxx", paramString.toString());
      }
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  private InetAddress[] a(Context paramContext, String paramString)
  {
    paramContext = (String)c.a(paramContext, cn.jiguang.ae.b.b(paramString));
    if (TextUtils.isEmpty(paramContext)) {
      return null;
    }
    paramContext = paramContext.split(",");
    LinkedList localLinkedList = new LinkedList();
    int j = paramContext.length;
    for (int i = 0; i < j; i++)
    {
      paramString = a(paramContext[i]);
      if (paramString != null) {
        localLinkedList.add(paramString);
      }
    }
    if (localLinkedList.isEmpty()) {
      return null;
    }
    return (InetAddress[])localLinkedList.toArray(new InetAddress[0]);
  }
  
  public InetAddress a(Context paramContext, String paramString, long paramLong, boolean paramBoolean)
  {
    paramContext = b(paramContext, paramString, paramLong, paramBoolean);
    if ((paramContext != null) && (paramContext.length > 0)) {
      return paramContext[0];
    }
    return null;
  }
  
  public InetAddress[] b(Context paramContext, String paramString, long paramLong, boolean paramBoolean)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    Object localObject1 = a(paramString);
    if (localObject1 != null) {
      return new InetAddress[] { localObject1 };
    }
    Pair localPair = a(paramContext, paramString, this.c, this.d);
    Object localObject2;
    if (localPair != null)
    {
      if (localPair.first != null) {
        localObject1 = (InetAddress[])localPair.first;
      } else {
        localObject1 = null;
      }
      localObject2 = localObject1;
      if (((Boolean)localPair.second).booleanValue()) {
        break label162;
      }
      if (paramBoolean) {
        cn.jiguang.sdk.impl.b.a(new FutureTask(new a(paramContext, paramString, this)), new int[0]);
      }
      paramContext = new StringBuilder();
    }
    for (;;)
    {
      paramContext.append("use cache=");
      paramContext.append(Arrays.toString((Object[])localObject1));
      cn.jiguang.ai.a.c("DNSLoader_xxx", paramContext.toString());
      return (InetAddress[])localObject1;
      localObject2 = null;
      label162:
      paramContext = new FutureTask(new a(paramContext, paramString, this));
      cn.jiguang.sdk.impl.b.a(paramContext, new int[0]);
      if (paramLong == 0L)
      {
        paramContext = new StringBuilder();
        localObject1 = localObject2;
      }
      else
      {
        try
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append("waiting dns for ");
          ((StringBuilder)localObject1).append(paramString);
          cn.jiguang.ai.a.c("DNSLoader_xxx", ((StringBuilder)localObject1).toString());
          paramContext = (InetAddress[])paramContext.get(paramLong, TimeUnit.MILLISECONDS);
        }
        catch (Throwable paramContext)
        {
          paramString = new StringBuilder();
          paramString.append("run futureTask e:");
          paramString.append(paramContext);
          cn.jiguang.ai.a.g("DNSLoader_xxx", paramString.toString());
          paramContext = null;
        }
        if (paramContext != null)
        {
          paramString = new StringBuilder();
          paramString.append("use resolved result=");
          paramString.append(Arrays.toString(paramContext));
          cn.jiguang.ai.a.c("DNSLoader_xxx", paramString.toString());
          return paramContext;
        }
        paramContext = new StringBuilder();
        localObject1 = localObject2;
      }
    }
  }
  
  static class a
    implements Callable<InetAddress[]>
  {
    private Context a;
    private String b;
    private a c;
    
    a(Context paramContext, String paramString, a parama)
    {
      this.a = paramContext;
      this.b = paramString;
      this.c = parama;
    }
    
    public InetAddress[] a()
    {
      StringBuilder localStringBuilder1 = null;
      StringBuilder localStringBuilder2;
      Object localObject1;
      try
      {
        InetAddress[] arrayOfInetAddress = InetAddress.getAllByName(this.b);
      }
      catch (UnknownHostException localUnknownHostException)
      {
        localStringBuilder2 = new StringBuilder();
        localStringBuilder2.append("dns resolve failed:");
        localStringBuilder2.append(localUnknownHostException);
        cn.jiguang.ai.a.g("DNSLoader_xxx", localStringBuilder2.toString());
        localObject1 = null;
      }
      label167:
      long l;
      if ((localObject1 != null) && (localObject1.length > 0))
      {
        ArrayList localArrayList = new ArrayList();
        localStringBuilder2 = new StringBuilder();
        int i1 = localObject1.length;
        int k = 0;
        int i = 0;
        int m;
        for (int j = 0; k < i1; j = m)
        {
          Object localObject2 = localObject1[k];
          if ((i < 3) && ((localObject2 instanceof Inet4Address))) {
            i++;
          }
          for (;;)
          {
            m = j;
            n = 1;
            j = i;
            i = m;
            break label167;
            if ((j >= 3) || (!(localObject2 instanceof Inet6Address))) {
              break;
            }
            j++;
          }
          m = i;
          int n = 0;
          i = j;
          j = m;
          if (n != 0)
          {
            localArrayList.add(localObject2);
            localStringBuilder2.append(((InetAddress)localObject2).getHostAddress());
            localStringBuilder2.append(",");
          }
          if ((j == 3) && (i == 3)) {
            break;
          }
          k++;
          m = i;
          i = j;
        }
        localObject1 = localStringBuilder1;
        if (!localArrayList.isEmpty())
        {
          localObject1 = (InetAddress[])localArrayList.toArray(new InetAddress[0]);
          l = System.currentTimeMillis();
          localStringBuilder1 = new StringBuilder();
          localStringBuilder1.append("update dns cache url=");
          localStringBuilder1.append(this.b);
          localStringBuilder1.append(" resolved=");
          localStringBuilder1.append(Arrays.toString((Object[])localObject1));
          cn.jiguang.ai.a.c("DNSLoader_xxx", localStringBuilder1.toString());
          a.a(this.c).put(this.b, new Pair(localObject1, Long.valueOf(l)));
        }
      }
      try
      {
        localStringBuilder2.deleteCharAt(localStringBuilder2.length() - 1);
        c.a(this.a, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.b(this.b).a(localStringBuilder2.toString()), cn.jiguang.ae.b.c(this.b).a(Long.valueOf(l)) });
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
      return (InetAddress[])localObject1;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aj/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */