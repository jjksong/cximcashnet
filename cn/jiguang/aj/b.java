package cn.jiguang.aj;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.ae.c;
import cn.jiguang.ap.g;
import cn.jiguang.ap.h;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class b
{
  private static int a;
  private static b b;
  private final Map<String, Integer> c = new HashMap();
  
  private int a(boolean paramBoolean)
  {
    try
    {
      Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
      int i = 0;
      while (localEnumeration1.hasMoreElements())
      {
        Object localObject = (NetworkInterface)localEnumeration1.nextElement();
        if (!g.a(((NetworkInterface)localObject).getName(), "dummy"))
        {
          boolean bool1 = g.a(((NetworkInterface)localObject).getName(), "wlan");
          if ((!paramBoolean) || (bool1))
          {
            Enumeration localEnumeration2 = ((NetworkInterface)localObject).getInetAddresses();
            int j = i;
            for (;;)
            {
              i = j;
              if (!localEnumeration2.hasMoreElements()) {
                break;
              }
              localObject = (InetAddress)localEnumeration2.nextElement();
              if (!((InetAddress)localObject).isLoopbackAddress())
              {
                boolean bool2 = a((InetAddress)localObject);
                if (bool2)
                {
                  if (bool1) {
                    return 3;
                  }
                  j = 1;
                }
              }
            }
          }
        }
      }
      if (i != 0) {
        return 0;
      }
      return 1;
    }
    catch (Exception localException)
    {
      cn.jiguang.ai.a.g("IpvxHelper_xxx", "checkIpvxSupport:");
    }
    return 0;
  }
  
  public static b a()
  {
    if (b == null) {
      try
      {
        if (b == null)
        {
          b localb = new cn/jiguang/aj/b;
          localb.<init>();
          b = localb;
        }
      }
      finally {}
    }
    return b;
  }
  
  public static void a(int paramInt)
  {
    if ((paramInt <= 3) && (paramInt >= 0)) {
      a = paramInt;
    }
  }
  
  private static boolean a(InetAddress paramInetAddress)
  {
    try
    {
      if ((paramInetAddress instanceof Inet6Address))
      {
        boolean bool = paramInetAddress.getHostAddress().substring(0, 4).equalsIgnoreCase("fe80");
        if (!bool) {
          return true;
        }
      }
    }
    catch (Throwable paramInetAddress)
    {
      for (;;) {}
    }
    return false;
  }
  
  public int a(Context paramContext)
  {
    int j = 0;
    int i = j;
    try
    {
      String str = h.c(paramContext);
      Object localObject2 = "";
      i = j;
      boolean bool2 = "wifi".equals(str);
      localObject1 = localObject2;
      if (bool2)
      {
        localObject1 = localObject2;
        i = j;
        if (cn.jiguang.sdk.impl.b.q(paramContext))
        {
          i = j;
          localObject1 = cn.jiguang.ap.a.k(paramContext);
        }
      }
      i = j;
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1) ^ true;
      if ((!bool2) || (bool1))
      {
        i = j;
        Map localMap = this.c;
        i = j;
        localObject2 = new java/lang/StringBuilder;
        i = j;
        ((StringBuilder)localObject2).<init>();
        i = j;
        ((StringBuilder)localObject2).append(str);
        i = j;
        ((StringBuilder)localObject2).append((String)localObject1);
        i = j;
        localObject2 = (Integer)localMap.get(((StringBuilder)localObject2).toString());
        if (localObject2 != null)
        {
          i = j;
          if (((Integer)localObject2).intValue() != 0)
          {
            i = j;
            paramContext = new java/lang/StringBuilder;
            i = j;
            paramContext.<init>();
            i = j;
            paramContext.append("net=");
            i = j;
            paramContext.append(str);
            i = j;
            paramContext.append(" ");
            i = j;
            paramContext.append((String)localObject1);
            i = j;
            paramContext.append(" get cache support=");
            i = j;
            paramContext.append(localObject2);
            i = j;
            cn.jiguang.ai.a.c("IpvxHelper_xxx", paramContext.toString());
            i = j;
            return ((Integer)localObject2).intValue();
          }
        }
      }
      if (bool1)
      {
        i = j;
        j = ((Integer)c.a(paramContext, cn.jiguang.ae.b.g((String)localObject1))).intValue();
        i = j;
      }
      try
      {
        localObject2 = new java/lang/StringBuilder;
        i = j;
        ((StringBuilder)localObject2).<init>();
        i = j;
        ((StringBuilder)localObject2).append("net=");
        i = j;
        ((StringBuilder)localObject2).append(str);
        i = j;
        ((StringBuilder)localObject2).append(" ");
        i = j;
        ((StringBuilder)localObject2).append((String)localObject1);
        i = j;
        ((StringBuilder)localObject2).append(" get wifi history support=");
        i = j;
        ((StringBuilder)localObject2).append(j);
        i = j;
        cn.jiguang.ai.a.c("IpvxHelper_xxx", ((StringBuilder)localObject2).toString());
        i = j;
        break label352;
        i = 0;
        label352:
        j = i;
        if (i == 0)
        {
          int k = a(bool2);
          i = k;
          localObject2 = new java/lang/StringBuilder;
          i = k;
          ((StringBuilder)localObject2).<init>();
          i = k;
          ((StringBuilder)localObject2).append("net=");
          i = k;
          ((StringBuilder)localObject2).append(str);
          i = k;
          ((StringBuilder)localObject2).append(" ");
          i = k;
          ((StringBuilder)localObject2).append((String)localObject1);
          i = k;
          ((StringBuilder)localObject2).append(" get networkinterface support=");
          i = k;
          ((StringBuilder)localObject2).append(k);
          i = k;
          cn.jiguang.ai.a.c("IpvxHelper_xxx", ((StringBuilder)localObject2).toString());
          j = k;
          if (bool1)
          {
            i = k;
            c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.g((String)localObject1).a(Integer.valueOf(k)) });
            j = k;
          }
        }
      }
      catch (Throwable paramContext)
      {
        break label580;
      }
      if (bool2)
      {
        i = j;
        if (!bool1) {}
      }
      else
      {
        i = j;
        localObject2 = this.c;
        i = j;
        paramContext = new java/lang/StringBuilder;
        i = j;
        paramContext.<init>();
        i = j;
        paramContext.append(str);
        i = j;
        paramContext.append((String)localObject1);
        i = j;
        ((Map)localObject2).put(paramContext.toString(), Integer.valueOf(j));
        i = j;
      }
    }
    catch (Throwable paramContext)
    {
      label580:
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("getPreferVx e:");
      ((StringBuilder)localObject1).append(paramContext);
      cn.jiguang.ai.a.g("IpvxHelper_xxx", ((StringBuilder)localObject1).toString());
    }
    return i;
  }
  
  public void a(Context paramContext, int paramInt)
  {
    String str = h.c(paramContext);
    Object localObject2 = "";
    boolean bool2 = "wifi".equals(str);
    Object localObject1 = localObject2;
    if (bool2)
    {
      localObject1 = localObject2;
      if (cn.jiguang.sdk.impl.b.q(paramContext)) {
        localObject1 = cn.jiguang.ap.a.k(paramContext);
      }
    }
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1) ^ true;
    if ((!bool2) || (bool1))
    {
      Map localMap = this.c;
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(str);
      ((StringBuilder)localObject2).append((String)localObject1);
      localMap.put(((StringBuilder)localObject2).toString(), Integer.valueOf(paramInt));
    }
    if (bool1) {
      c.a(paramContext, new cn.jiguang.ae.b[] { cn.jiguang.ae.b.g((String)localObject1).a(Integer.valueOf(paramInt)) });
    }
  }
  
  public int b(int paramInt)
  {
    int i = a;
    if ((i != 2) && (i != 1))
    {
      switch (paramInt)
      {
      default: 
        return i;
      case 2: 
        return 2;
      }
      return 1;
    }
    return a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/aj/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */