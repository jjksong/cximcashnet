package cn.jiguang.o;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public class a
{
  private static final Object a = new Object();
  private static ConcurrentHashMap<String, ThreadLocal<SimpleDateFormat>> b = new ConcurrentHashMap();
  
  public static String a()
  {
    return a("yyyyMMddHHmmss").format(new Date());
  }
  
  private static SimpleDateFormat a(String paramString)
  {
    ThreadLocal localThreadLocal = (ThreadLocal)b.get(paramString);
    Object localObject1 = localThreadLocal;
    if (localThreadLocal == null) {
      synchronized (a)
      {
        localThreadLocal = (ThreadLocal)b.get(paramString);
        localObject1 = localThreadLocal;
        if (localThreadLocal == null)
        {
          localObject1 = new cn/jiguang/o/a$a;
          ((a)localObject1).<init>(paramString);
          b.put(paramString, localObject1);
        }
      }
    }
    return (SimpleDateFormat)((ThreadLocal)localObject1).get();
  }
  
  private static class a
    extends ThreadLocal<SimpleDateFormat>
  {
    private String a;
    
    a(String paramString)
    {
      this.a = paramString;
    }
    
    protected SimpleDateFormat a()
    {
      return new SimpleDateFormat(this.a, Locale.ENGLISH);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/o/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */