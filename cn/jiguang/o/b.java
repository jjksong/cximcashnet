package cn.jiguang.o;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import cn.jiguang.as.h;
import cn.jiguang.f.d;
import cn.jiguang.r.c;

public class b
{
  private static SharedPreferences a;
  
  public static String a(Context paramContext)
  {
    return b(paramContext, "number_version", "1.3.0");
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < 3))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("number_url");
      localStringBuilder.append(paramInt);
      return b(paramContext, localStringBuilder.toString(), "http://182.92.20.189:9099/");
    }
    return "http://182.92.20.189:9099/";
  }
  
  public static void a(Context paramContext, int paramInt, String paramString)
  {
    if ((paramInt >= 0) && (paramInt < 3))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("number_url");
      localStringBuilder.append(paramInt);
      a(paramContext, localStringBuilder.toString(), paramString);
    }
  }
  
  public static void a(Context paramContext, String paramString)
  {
    a(paramContext, "number_version", paramString);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2)
  {
    h(paramContext).edit().putString(paramString1, d.g(paramString2)).apply();
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    h(paramContext).edit().putBoolean("nb_upload", paramBoolean).apply();
  }
  
  public static String b(Context paramContext)
  {
    return b(paramContext, "number_appid", "7");
  }
  
  public static String b(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = h(paramContext).getString(paramString1, "");
    if (h.a(paramContext)) {
      return paramString2;
    }
    return d.a(paramContext, paramString2);
  }
  
  public static void b(Context paramContext, String paramString)
  {
    a(paramContext, "number_appid", paramString);
  }
  
  public static String c(Context paramContext)
  {
    return b(paramContext, "number_appsecret", "2b90de0f1f88eaf49593f1d827b19c63");
  }
  
  public static void c(Context paramContext, String paramString)
  {
    a(paramContext, "number_appsecret", paramString);
  }
  
  public static String d(Context paramContext)
  {
    String str2 = c.a(paramContext);
    String str1 = str2;
    if (h.a(str2)) {
      str1 = "number_num";
    }
    return b(paramContext, str1, "");
  }
  
  public static void d(Context paramContext, String paramString)
  {
    String str2 = c.a(paramContext);
    String str1 = str2;
    if (h.a(str2)) {
      str1 = "number_num";
    }
    a(paramContext, str1, paramString);
  }
  
  public static boolean e(Context paramContext)
  {
    return h(paramContext).getBoolean("nb_upload", false);
  }
  
  public static void f(Context paramContext)
  {
    h(paramContext).edit().putLong("nb_lasttime", System.currentTimeMillis()).apply();
  }
  
  private static void g(Context paramContext)
  {
    a = paramContext.getSharedPreferences("cn.jiguang.common.pn", 0);
  }
  
  private static SharedPreferences h(Context paramContext)
  {
    if (a == null) {
      g(paramContext);
    }
    return a;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/o/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */