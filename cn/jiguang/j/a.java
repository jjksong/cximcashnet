package cn.jiguang.j;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.text.TextUtils;
import cn.jiguang.s.d;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"PackageManagerGetSignatures"})
public class a
{
  private static String a = "JAppHelper";
  
  public static int a(ApplicationInfo paramApplicationInfo)
  {
    if (paramApplicationInfo == null) {
      return -1;
    }
    try
    {
      int i = paramApplicationInfo.flags;
      int j = 1;
      int k = 0;
      if ((i & 0x1) != 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0)
      {
        i = k;
        if ((paramApplicationInfo.flags & 0x80) != 0) {
          i = 1;
        }
        if (i != 0) {
          j = 2;
        }
        return j;
      }
      localObject = paramApplicationInfo.sourceDir;
      if (TextUtils.isEmpty((CharSequence)localObject)) {
        return -1;
      }
      if (!((String)localObject).startsWith("/system/"))
      {
        boolean bool = ((String)localObject).contains(paramApplicationInfo.packageName);
        if (bool) {
          return 0;
        }
      }
      return 3;
    }
    catch (Throwable localThrowable)
    {
      paramApplicationInfo = a;
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("getAppInstalledType throwable:");
      ((StringBuilder)localObject).append(localThrowable.getMessage());
      cn.jiguang.ai.a.g(paramApplicationInfo, ((StringBuilder)localObject).toString());
    }
    return -1;
  }
  
  public static ApplicationInfo a(Context paramContext, String paramString)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramString, 0);
      return paramContext;
    }
    catch (Throwable paramContext)
    {
      paramString = a;
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("getApplicationInfo throwable:");
      localStringBuilder.append(paramContext.getMessage());
      cn.jiguang.ai.a.g(paramString, localStringBuilder.toString());
    }
    return null;
  }
  
  public static String a(List<cn.jiguang.i.a> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramList.size(); i++)
    {
      localStringBuilder.append(((cn.jiguang.i.a)paramList.get(i)).b);
      if (i != paramList.size() - 1) {
        localStringBuilder.append("&&");
      }
    }
    return localStringBuilder.toString();
  }
  
  public static String a(Set<String> paramSet)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    paramSet = paramSet.iterator();
    while (paramSet.hasNext())
    {
      localStringBuilder.append((String)paramSet.next());
      localStringBuilder.append("&&");
    }
    return localStringBuilder.toString();
  }
  
  public static ArrayList<JSONArray> a(JSONArray paramJSONArray)
  {
    if ((paramJSONArray != null) && (paramJSONArray.length() != 0))
    {
      Object localObject1;
      Object localObject2;
      try
      {
        localObject1 = new org/json/JSONArray;
        ((JSONArray)localObject1).<init>();
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        int j = 0;
        int i;
        for (int k = 0; j < paramJSONArray.length(); k = i)
        {
          JSONObject localJSONObject = paramJSONArray.optJSONObject(j);
          localObject2 = localObject1;
          i = k;
          if (localJSONObject != null) {
            if (localJSONObject.length() == 0)
            {
              localObject2 = localObject1;
              i = k;
            }
            else
            {
              i = localJSONObject.toString().getBytes("UTF-8").length;
              k += i;
              if (k > 102400)
              {
                if (((JSONArray)localObject1).length() > 0) {
                  localArrayList.add(localObject1);
                }
                localObject2 = new org/json/JSONArray;
                ((JSONArray)localObject2).<init>();
                ((JSONArray)localObject2).put(localJSONObject);
              }
              else
              {
                ((JSONArray)localObject1).put(localJSONObject);
                i = k;
                localObject2 = localObject1;
              }
            }
          }
          j++;
          localObject1 = localObject2;
        }
        if (((JSONArray)localObject1).length() > 0) {
          localArrayList.add(localObject1);
        }
        return localArrayList;
      }
      catch (Throwable paramJSONArray)
      {
        localObject1 = a;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("partition throwable:");
        paramJSONArray = paramJSONArray.getMessage();
      }
      catch (UnsupportedEncodingException paramJSONArray)
      {
        localObject1 = a;
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append("partition exception:");
        paramJSONArray = paramJSONArray.getMessage();
      }
      ((StringBuilder)localObject2).append(paramJSONArray);
      cn.jiguang.ai.a.g((String)localObject1, ((StringBuilder)localObject2).toString());
    }
    return null;
  }
  
  public static List<cn.jiguang.i.a> a(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 21) {
      paramContext = b(paramContext);
    } else {
      paramContext = c(paramContext);
    }
    return paramContext;
  }
  
  public static List<cn.jiguang.i.a> a(Context paramContext, boolean paramBoolean)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      cn.jiguang.ai.a.c(a, "getInstalledApps by api");
      paramContext = localPackageManager.getInstalledPackages(0);
    }
    catch (Throwable paramContext)
    {
      cn.jiguang.ai.a.c(a, "getInstalledApps by shell");
      paramContext = a(localPackageManager);
    }
    if ((paramContext != null) && (!paramContext.isEmpty())) {
      return a(localPackageManager, paramContext, paramBoolean);
    }
    return null;
  }
  
  private static List<PackageInfo> a(PackageManager paramPackageManager)
  {
    int i = 0;
    try
    {
      List localList = d.a(new String[] { "pm list package" }, 1);
      if ((localList != null) && (!localList.isEmpty()))
      {
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        while (i < localList.size())
        {
          localObject1 = (String)localList.get(i);
          if ((!TextUtils.isEmpty((CharSequence)localObject1)) && (((String)localObject1).startsWith("package:")))
          {
            String str2 = ((String)localObject1).substring(8);
            Object localObject2 = a;
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append("execute command packageName:");
            ((StringBuilder)localObject1).append(str2);
            cn.jiguang.ai.a.c((String)localObject2, ((StringBuilder)localObject1).toString());
            if (!TextUtils.isEmpty(str2))
            {
              localObject1 = new android/content/pm/PackageInfo;
              ((PackageInfo)localObject1).<init>();
              try
              {
                localObject2 = paramPackageManager.getPackageInfo(str2, 64);
                localObject1 = localObject2;
              }
              catch (PackageManager.NameNotFoundException localNameNotFoundException)
              {
                ((PackageInfo)localObject1).packageName = str2;
              }
              localArrayList.add(localObject1);
            }
          }
          i++;
        }
        return localArrayList;
      }
      cn.jiguang.ai.a.g(a, "execute command pm list package failed");
      return null;
    }
    catch (Throwable paramPackageManager)
    {
      String str1 = a;
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("getInstalledPackagesByShell throwable:");
      ((StringBuilder)localObject1).append(paramPackageManager.getMessage());
      cn.jiguang.ai.a.g(str1, ((StringBuilder)localObject1).toString());
    }
    return null;
  }
  
  private static List<cn.jiguang.i.a> a(PackageManager paramPackageManager, List<PackageInfo> paramList, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramList.size(); i++)
    {
      PackageInfo localPackageInfo = (PackageInfo)paramList.get(i);
      if ((paramBoolean) || (localPackageInfo.versionName != null))
      {
        cn.jiguang.i.a locala = new cn.jiguang.i.a();
        locala.a = localPackageInfo.applicationInfo.loadLabel(paramPackageManager).toString();
        locala.b = localPackageInfo.packageName;
        locala.c = localPackageInfo.versionName;
        locala.d = localPackageInfo.versionCode;
        locala.e = a(localPackageInfo.applicationInfo);
        localArrayList.add(locala);
      }
    }
    return localArrayList;
  }
  
  public static Set<String> a(String paramString)
  {
    paramString = paramString.split("&&");
    HashSet localHashSet = new HashSet();
    Collections.addAll(localHashSet, paramString);
    return localHashSet;
  }
  
  private static cn.jiguang.i.a b(Context paramContext, String paramString)
  {
    try
    {
      paramString = c(paramContext, paramString);
      if (paramString == null)
      {
        cn.jiguang.ai.a.g(a, "getAppInfoFromPackage failed because packageInfo is null");
        return null;
      }
      paramContext = paramString.applicationInfo.loadLabel(paramContext.getPackageManager()).toString();
      localObject = new cn/jiguang/i/a;
      ((cn.jiguang.i.a)localObject).<init>();
      ((cn.jiguang.i.a)localObject).a = b(paramContext);
      ((cn.jiguang.i.a)localObject).b = paramString.packageName;
      ((cn.jiguang.i.a)localObject).d = paramString.versionCode;
      ((cn.jiguang.i.a)localObject).c = paramString.versionName;
      ((cn.jiguang.i.a)localObject).e = a(paramString.applicationInfo);
      return (cn.jiguang.i.a)localObject;
    }
    catch (Throwable paramString)
    {
      Object localObject = a;
      paramContext = new StringBuilder();
      paramContext.append("getAppInfoFromPackage throwable:");
      paramContext.append(paramString.getMessage());
      cn.jiguang.ai.a.g((String)localObject, paramContext.toString());
    }
    return null;
  }
  
  private static String b(String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      Object localObject = c(paramString);
      String str1;
      String str3;
      try
      {
        byte[] arrayOfByte = paramString.getBytes();
        if (arrayOfByte.length <= 30) {
          return paramString;
        }
        str1 = new java/lang/String;
        str1.<init>(arrayOfByte, 0, 30, "UTF-8");
        str1 = ((String)localObject).substring(0, str1.length());
        return str1;
      }
      catch (Throwable localThrowable)
      {
        str1 = a;
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("getAppName throwable:");
        String str2 = localThrowable.getMessage();
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        str1 = a;
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append("getAppName exception:");
        str3 = localUnsupportedEncodingException.getMessage();
      }
      ((StringBuilder)localObject).append(str3);
      cn.jiguang.ai.a.g(str1, ((StringBuilder)localObject).toString());
    }
    return paramString;
  }
  
  private static List<cn.jiguang.i.a> b(Context paramContext)
  {
    Object localObject1 = (ActivityManager)paramContext.getSystemService("activity");
    try
    {
      localObject2 = ((ActivityManager)localObject1).getRunningAppProcesses();
      localObject1 = new HashSet();
      Object localObject3 = ((List)localObject2).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject2 = ((ActivityManager.RunningAppProcessInfo)((Iterator)localObject3).next()).pkgList;
        if ((localObject2 != null) && (localObject2.length > 0)) {
          Collections.addAll((Collection)localObject1, (Object[])localObject2);
        }
      }
      localObject2 = new ArrayList();
      localObject1 = ((HashSet)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject3 = b(paramContext, (String)((Iterator)localObject1).next());
        if (localObject3 != null) {
          ((List)localObject2).add(localObject3);
        }
      }
      return (List<cn.jiguang.i.a>)localObject2;
    }
    catch (Throwable localThrowable)
    {
      paramContext = a;
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append("getRunningAppInfoBelowL throwable:");
      ((StringBuilder)localObject2).append(localThrowable.getMessage());
      cn.jiguang.ai.a.g(paramContext, ((StringBuilder)localObject2).toString());
    }
    return null;
  }
  
  private static PackageInfo c(Context paramContext, String paramString)
  {
    try
    {
      PackageInfo localPackageInfo = paramContext.getPackageManager().getPackageInfo(paramString, 0);
      return localPackageInfo;
    }
    catch (Throwable localThrowable)
    {
      try
      {
        int i = paramString.indexOf(":");
        if (i <= 0) {
          return null;
        }
        paramString = paramString.substring(0, i);
        paramContext = paramContext.getPackageManager().getPackageInfo(paramString, 0);
        return paramContext;
      }
      catch (Throwable paramString)
      {
        paramContext = a;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("getPackageInfo throwable:");
        localStringBuilder.append(paramString.getMessage());
        cn.jiguang.ai.a.g(paramContext, localStringBuilder.toString());
      }
    }
    return null;
  }
  
  private static String c(String paramString)
  {
    return Pattern.compile("\n|\r|\r\n|\n\r|\t").matcher(paramString).replaceAll("");
  }
  
  private static List<cn.jiguang.i.a> c(Context paramContext)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = b.a(1).iterator();
    while (localIterator.hasNext())
    {
      cn.jiguang.i.a locala = b(paramContext, ((cn.jiguang.i.b)localIterator.next()).d);
      if (locala != null) {
        localHashMap.put(locala.b, locala);
      }
    }
    return new ArrayList(localHashMap.values());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/j/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */