package cn.jiguang.j;

import android.text.TextUtils;
import cn.jiguang.ai.a;
import cn.jiguang.s.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class b
{
  private static final Pattern a = Pattern.compile("^zygote[0-9]*$");
  
  private static int a(LinkedList<String> paramLinkedList, String paramString, int paramInt)
  {
    int i = paramLinkedList.indexOf(paramString);
    if (i == -1) {
      i = paramLinkedList.indexOf(paramString.toLowerCase(Locale.ENGLISH));
    }
    int j = i;
    if (i == -1) {
      j = paramInt;
    }
    return j;
  }
  
  private static cn.jiguang.i.b a(String paramString, Map<String, Integer> paramMap)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    paramString = paramString.split("\\s+");
    int i = paramString.length;
    if (i < 3) {
      return null;
    }
    try
    {
      cn.jiguang.i.b localb = new cn/jiguang/i/b;
      localb.<init>();
      if ((paramMap != null) && (!paramMap.isEmpty()))
      {
        localb.a = paramString[((Integer)paramMap.get("USER")).intValue()];
        localb.b = paramString[((Integer)paramMap.get("PID")).intValue()];
        localb.c = paramString[((Integer)paramMap.get("PPID")).intValue()];
      }
      for (paramString = paramString[((Integer)paramMap.get("NAME")).intValue()];; paramString = paramString[(i - 1)])
      {
        localb.d = paramString;
        break;
        localb.a = paramString[0];
        localb.b = paramString[1];
        localb.c = paramString[2];
      }
      return localb;
    }
    catch (Throwable paramString)
    {
      paramMap = new StringBuilder();
      paramMap.append("parseCommandResult throwable:");
      paramMap.append(paramString.getMessage());
      a.g("JProcessHelper", paramMap.toString());
    }
    return null;
  }
  
  public static List<cn.jiguang.i.b> a(int paramInt)
  {
    try
    {
      Object localObject2 = d.a(new String[] { "ps" }, 1);
      if ((localObject2 != null) && (!((List)localObject2).isEmpty()))
      {
        HashSet localHashSet = new java/util/HashSet;
        localHashSet.<init>();
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        localObject1 = a((String)((List)localObject2).remove(0));
        localObject2 = ((List)localObject2).iterator();
        cn.jiguang.i.b localb;
        while (((Iterator)localObject2).hasNext())
        {
          localb = a((String)((Iterator)localObject2).next(), (Map)localObject1);
          if (localb != null) {
            if ((paramInt != 3) && (b(localb.c)))
            {
              if (c(localb.d)) {
                localHashSet.add(localb.b);
              }
            }
            else if (!"ps".equals(localb.d)) {
              localArrayList.add(localb);
            }
          }
        }
        if ((paramInt == 1) && (!localHashSet.isEmpty()))
        {
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          paramInt = 0;
          for (;;)
          {
            localObject1 = localObject2;
            if (paramInt != 0) {
              break;
            }
            localObject1 = localArrayList.iterator();
            paramInt = 1;
            while (((Iterator)localObject1).hasNext())
            {
              localb = (cn.jiguang.i.b)((Iterator)localObject1).next();
              if (localHashSet.contains(localb.c))
              {
                ((List)localObject2).add(localb);
                localHashSet.add(localb.b);
                ((Iterator)localObject1).remove();
                paramInt = 0;
              }
            }
          }
        }
        localObject1 = localArrayList;
        return (List<cn.jiguang.i.b>)localObject1;
      }
      a.c("JProcessHelper", "execute command failed");
      return null;
    }
    catch (Throwable localThrowable)
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("getRunningProcessInfo throwable:");
      ((StringBuilder)localObject1).append(localThrowable.getMessage());
      a.g("JProcessHelper", ((StringBuilder)localObject1).toString());
    }
    return null;
  }
  
  private static Map<String, Integer> a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    LinkedList localLinkedList = new LinkedList();
    Collections.addAll(localLinkedList, paramString.split("\\s+"));
    int i = localLinkedList.size() - 1;
    paramString = new HashMap();
    int j = a(localLinkedList, "USER", 0);
    int k = a(localLinkedList, "PID", 1);
    int n = a(localLinkedList, "PPID", 2);
    int m = a(localLinkedList, "NAME", i);
    if ((j == 0) && (k == 1) && (n == 2) && (m == i)) {
      return null;
    }
    return paramString;
  }
  
  private static boolean b(String paramString)
  {
    boolean bool;
    if ((!paramString.equals("0")) && (!paramString.equals("1")) && (!paramString.equals("2"))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  private static boolean c(String paramString)
  {
    return a.matcher(paramString).matches();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/j/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */