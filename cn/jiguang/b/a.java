package cn.jiguang.b;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import cn.jiguang.ad.f;
import cn.jiguang.api.ReportCallBack;
import cn.jiguang.as.d;
import cn.jiguang.e.b;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.regex.PatternSyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  implements Thread.UncaughtExceptionHandler
{
  private static a b = new a();
  private static int c = 1048576;
  public boolean a = true;
  private Thread.UncaughtExceptionHandler d = null;
  
  public static a a()
  {
    return b;
  }
  
  /* Error */
  private JSONArray a(Context paramContext, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 59
    //   3: invokestatic 64	cn/jiguang/as/d:a	(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    //   6: invokestatic 67	cn/jiguang/as/d:d	(Ljava/io/File;)Ljava/lang/String;
    //   9: astore 9
    //   11: aload 9
    //   13: invokestatic 73	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   16: istore 6
    //   18: aconst_null
    //   19: astore 8
    //   21: iconst_0
    //   22: istore 5
    //   24: iconst_0
    //   25: istore 4
    //   27: aload 8
    //   29: astore 7
    //   31: iload 5
    //   33: istore_3
    //   34: iload 6
    //   36: ifne +21 -> 57
    //   39: new 75	org/json/JSONArray
    //   42: astore 7
    //   44: aload 7
    //   46: aload 9
    //   48: invokespecial 78	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   51: aload 9
    //   53: invokevirtual 84	java/lang/String:length	()I
    //   56: istore_3
    //   57: aload_0
    //   58: aload_1
    //   59: aload 7
    //   61: iload_3
    //   62: aload_2
    //   63: invokespecial 87	cn/jiguang/b/a:a	(Landroid/content/Context;Lorg/json/JSONArray;ILjava/lang/Throwable;)Lorg/json/JSONArray;
    //   66: areturn
    //   67: astore 7
    //   69: aload 8
    //   71: astore 7
    //   73: iload 5
    //   75: istore_3
    //   76: goto -19 -> 57
    //   79: astore 8
    //   81: iload 4
    //   83: istore_3
    //   84: goto -27 -> 57
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	87	0	this	a
    //   0	87	1	paramContext	Context
    //   0	87	2	paramThrowable	Throwable
    //   33	51	3	i	int
    //   25	57	4	j	int
    //   22	52	5	k	int
    //   16	19	6	bool	boolean
    //   29	31	7	localObject1	Object
    //   67	1	7	localJSONException1	JSONException
    //   71	1	7	localObject2	Object
    //   19	51	8	localObject3	Object
    //   79	1	8	localJSONException2	JSONException
    //   9	43	9	str	String
    // Exception table:
    //   from	to	target	type
    //   39	51	67	org/json/JSONException
    //   51	57	79	org/json/JSONException
  }
  
  private JSONArray a(Context paramContext, JSONArray paramJSONArray, int paramInt, Throwable paramThrowable)
  {
    long l1 = System.currentTimeMillis() + cn.jiguang.e.c.c(paramContext);
    Object localObject1 = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter((Writer)localObject1));
    Object localObject2 = ((StringWriter)localObject1).toString();
    localObject1 = paramJSONArray;
    if (paramJSONArray == null) {
      localObject1 = new JSONArray();
    }
    int k = 0;
    int i = 0;
    for (;;)
    {
      paramJSONArray = null;
      try
      {
        if (i < ((JSONArray)localObject1).length())
        {
          paramJSONArray = ((JSONArray)localObject1).optJSONObject(i);
          if ((paramJSONArray != null) && (((String)localObject2).equals(paramJSONArray.getString("stacktrace"))))
          {
            paramJSONArray.put("count", paramJSONArray.getInt("count") + 1);
            paramJSONArray.put("crashtime", l1);
          }
          else
          {
            i++;
            continue;
          }
        }
        if (paramJSONArray == null)
        {
          paramJSONArray = new org/json/JSONObject;
          paramJSONArray.<init>();
          paramJSONArray.put("crashtime", l1);
          paramJSONArray.put("stacktrace", localObject2);
          paramJSONArray.put("message", b(paramThrowable));
          paramJSONArray.put("count", 1);
          paramJSONArray.put("networktype", cn.jiguang.as.e.d(paramContext));
          paramThrowable = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 1);
          if (paramThrowable != null)
          {
            if (paramThrowable.versionName == null) {
              paramContext = "null";
            } else {
              paramContext = paramThrowable.versionName;
            }
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append(paramThrowable.versionCode);
            ((StringBuilder)localObject2).append("");
            paramThrowable = ((StringBuilder)localObject2).toString();
            paramJSONArray.put("versionname", paramContext);
            paramJSONArray.put("versioncode", paramThrowable);
          }
          if (paramInt + paramJSONArray.toString().length() < c)
          {
            ((JSONArray)localObject1).put(paramJSONArray);
          }
          else
          {
            long l2 = -1L;
            int j = 0;
            paramInt = k;
            while (paramInt < ((JSONArray)localObject1).length())
            {
              paramContext = ((JSONArray)localObject1).optJSONObject(paramInt);
              l1 = l2;
              i = j;
              if (paramContext != null)
              {
                long l3 = paramContext.optLong("crashtime");
                if (l2 != -1L)
                {
                  l1 = l2;
                  i = j;
                  if (l3 >= l2) {}
                }
                else
                {
                  i = paramInt;
                  l1 = l3;
                }
              }
              paramInt++;
              l2 = l1;
              j = i;
            }
            ((JSONArray)localObject1).put(j, paramJSONArray);
          }
        }
      }
      catch (Throwable paramContext)
      {
        for (;;) {}
      }
    }
    return (JSONArray)localObject1;
  }
  
  private void a(Context paramContext, JSONArray paramJSONArray)
  {
    if (paramJSONArray != null) {
      paramJSONArray = paramJSONArray.toString();
    } else {
      paramJSONArray = null;
    }
    if (!TextUtils.isEmpty(paramJSONArray))
    {
      paramContext = d.a(paramContext, "jpush_uncaughtexception_file");
      if (paramContext != null) {
        d.a(paramContext, paramJSONArray);
      }
    }
  }
  
  private void a(Throwable paramThrowable)
  {
    if (!this.a) {
      return;
    }
    Context localContext = cn.jiguang.a.a.a(null);
    if (localContext != null)
    {
      paramThrowable = a(localContext, paramThrowable);
      c(localContext);
      a(localContext, paramThrowable);
    }
    else
    {
      cn.jiguang.ac.c.h("JPushCrashHandler", "handleException failed: context is null");
    }
  }
  
  private String b(Throwable paramThrowable)
  {
    str = paramThrowable.toString();
    try
    {
      String[] arrayOfString = str.split(":");
      paramThrowable = str;
      if (arrayOfString.length > 1)
      {
        for (int i = arrayOfString.length - 1;; i--)
        {
          paramThrowable = str;
          if (i < 0) {
            break label75;
          }
          if ((arrayOfString[i].endsWith("Exception")) || (arrayOfString[i].endsWith("Error"))) {
            break;
          }
        }
        paramThrowable = arrayOfString[i];
      }
    }
    catch (NullPointerException|PatternSyntaxException paramThrowable)
    {
      for (;;)
      {
        label75:
        paramThrowable = str;
      }
    }
    return paramThrowable;
  }
  
  public static void c(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jiguang.ac.c.f("JPushCrashHandler", "Action - deleteCrashLog context is null");
      return;
    }
    d.a(d.a(paramContext, "jpush_uncaughtexception_file"));
  }
  
  private static JSONArray e(Context paramContext)
  {
    paramContext = d.d(d.a(paramContext, "jpush_uncaughtexception_file"));
    if (TextUtils.isEmpty(paramContext)) {
      return null;
    }
    try
    {
      paramContext = new JSONArray(paramContext);
      return paramContext;
    }
    catch (JSONException paramContext) {}
    return null;
  }
  
  private JSONObject f(Context paramContext)
  {
    Object localObject2 = e(paramContext);
    Object localObject1 = null;
    if (localObject2 == null) {
      return null;
    }
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("crashlogs", localObject2);
      localJSONObject.put("network_type", cn.jiguang.as.e.d(paramContext));
      f.a(paramContext, localJSONObject, "crash_log");
      localObject2 = cn.jiguang.ab.e.h(paramContext);
      paramContext = (Context)localObject1;
      if ((localObject2 instanceof JSONObject)) {
        paramContext = (JSONObject)localObject2;
      }
      if ((paramContext != null) && (paramContext.length() > 0)) {
        localJSONObject.put("device_info", paramContext);
      }
    }
    catch (Exception paramContext)
    {
      for (;;) {}
    }
    return localJSONObject;
  }
  
  public void a(Context paramContext)
  {
    if (!this.a)
    {
      this.a = true;
      b.a(cn.jiguang.a.a.a(paramContext), new cn.jiguang.e.a[] { cn.jiguang.e.a.b().a(Boolean.valueOf(true)) });
    }
  }
  
  public void b()
  {
    if (this.d == null) {
      this.d = Thread.getDefaultUncaughtExceptionHandler();
    }
    Thread.setDefaultUncaughtExceptionHandler(this);
  }
  
  public void b(Context paramContext)
  {
    if (this.a)
    {
      this.a = false;
      b.a(cn.jiguang.a.a.a(paramContext), new cn.jiguang.e.a[] { cn.jiguang.e.a.b().a(Boolean.valueOf(false)) });
    }
  }
  
  public void d(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jiguang.ac.c.f("JPushCrashHandler", "Action - reportCrashLog context is null");
      return;
    }
    if (!cn.jiguang.e.c.a(paramContext)) {
      return;
    }
    try
    {
      paramContext = new cn/jiguang/b/a$a;
      paramContext.<init>(this);
      paramContext.start();
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder();
      paramContext.append("report crash e:");
      paramContext.append(localThrowable);
      cn.jiguang.ac.c.h("JPushCrashHandler", paramContext.toString());
    }
  }
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    if (this.a)
    {
      cn.jiguang.ac.c.a("JPushCrashHandler", "enable crash report");
      a(paramThrowable);
      try
      {
        a locala = new cn/jiguang/b/a$a;
        locala.<init>(this);
        locala.start();
        locala.join(2000L);
      }
      catch (Throwable localThrowable)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("report crash e:");
        localStringBuilder.append(localThrowable);
        cn.jiguang.ac.c.h("JPushCrashHandler", localStringBuilder.toString());
      }
      catch (InterruptedException localInterruptedException) {}
    }
    else
    {
      cn.jiguang.ac.c.a("JPushCrashHandler", "disable crash report");
    }
    Thread.UncaughtExceptionHandler localUncaughtExceptionHandler = this.d;
    if (localUncaughtExceptionHandler != this) {
      localUncaughtExceptionHandler.uncaughtException(paramThread, paramThrowable);
    }
    throw new RuntimeException(paramThrowable);
  }
  
  class a
    extends Thread
    implements ReportCallBack
  {
    a() {}
    
    public void onFinish(int paramInt)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("ReportDirect finish : ");
      localStringBuilder.append(paramInt);
      cn.jiguang.ac.c.f("JPushCrashHandler", localStringBuilder.toString());
      if (paramInt == 0) {
        a.c(cn.jiguang.a.a.a(null));
      }
    }
    
    public void run()
    {
      try
      {
        Context localContext = cn.jiguang.a.a.a(null);
        if (localContext == null)
        {
          cn.jiguang.ac.c.f("JPushCrashHandler", "ReportDirect context is null");
          return;
        }
        localObject = a.a(a.this, localContext);
        if (localObject != null) {
          f.a(localContext, (JSONObject)localObject, this);
        }
      }
      catch (Throwable localThrowable)
      {
        Object localObject = new StringBuilder();
        ((StringBuilder)localObject).append("run report crash e:");
        ((StringBuilder)localObject).append(localThrowable);
        cn.jiguang.ac.c.h("JPushCrashHandler", ((StringBuilder)localObject).toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jiguang/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */