package cn.jpush.android.e;

import android.content.Context;
import cn.jpush.android.data.c;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class h
{
  public static Queue<c> a;
  
  private static void a(Context paramContext, String paramString)
  {
    if (paramContext == null) {
      return;
    }
    try
    {
      File localFile = paramContext.getFilesDir();
      if (localFile == null) {
        return;
      }
      paramContext = new java/io/File;
      paramContext.<init>(localFile, paramString);
      if (paramContext.exists()) {
        paramContext.delete();
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  private static <T> void a(Context paramContext, String paramString, ArrayList<T> paramArrayList)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnonnull +7 -> 11
    //   7: ldc 2
    //   9: monitorexit
    //   10: return
    //   11: aload_2
    //   12: ifnonnull +7 -> 19
    //   15: ldc 2
    //   17: monitorexit
    //   18: return
    //   19: new 37	java/io/ObjectOutputStream
    //   22: astore_3
    //   23: aload_3
    //   24: aload_0
    //   25: aload_1
    //   26: iconst_0
    //   27: invokevirtual 41	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    //   30: invokespecial 44	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   33: aload_3
    //   34: aload_2
    //   35: invokevirtual 48	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   38: aload_3
    //   39: invokevirtual 51	java/io/ObjectOutputStream:close	()V
    //   42: ldc 2
    //   44: monitorexit
    //   45: return
    //   46: astore_0
    //   47: goto +69 -> 116
    //   50: astore_1
    //   51: new 53	java/lang/StringBuilder
    //   54: astore_0
    //   55: aload_0
    //   56: ldc 55
    //   58: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   61: aload_0
    //   62: aload_1
    //   63: invokevirtual 62	java/io/IOException:getMessage	()Ljava/lang/String;
    //   66: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: ldc 68
    //   72: aload_0
    //   73: invokevirtual 71	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: invokestatic 77	cn/jpush/android/e/g:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   79: ldc 2
    //   81: monitorexit
    //   82: return
    //   83: astore_1
    //   84: new 53	java/lang/StringBuilder
    //   87: astore_0
    //   88: aload_0
    //   89: ldc 79
    //   91: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   94: aload_0
    //   95: aload_1
    //   96: invokevirtual 80	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
    //   99: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: ldc 68
    //   105: aload_0
    //   106: invokevirtual 71	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   109: invokestatic 77	cn/jpush/android/e/g:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   112: ldc 2
    //   114: monitorexit
    //   115: return
    //   116: ldc 2
    //   118: monitorexit
    //   119: aload_0
    //   120: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	121	0	paramContext	Context
    //   0	121	1	paramString	String
    //   0	121	2	paramArrayList	ArrayList<T>
    //   22	17	3	localObjectOutputStream	java.io.ObjectOutputStream
    // Exception table:
    //   from	to	target	type
    //   19	42	46	finally
    //   51	79	46	finally
    //   84	112	46	finally
    //   19	42	50	java/io/IOException
    //   19	42	83	java/io/FileNotFoundException
  }
  
  public static boolean a(Context paramContext, c paramc)
  {
    Object localObject3;
    if (a == null)
    {
      a = new ConcurrentLinkedQueue();
      try
      {
        Object localObject1 = b(paramContext, "msg_queue");
        if ((localObject1 != null) && (!((ArrayList)localObject1).isEmpty()))
        {
          localObject3 = ((ArrayList)localObject1).iterator();
          while (((Iterator)localObject3).hasNext())
          {
            localObject1 = (c)((Iterator)localObject3).next();
            a.offer(localObject1);
          }
        }
        if (paramContext != null) {
          break label113;
        }
      }
      catch (Exception localException)
      {
        localObject3 = new StringBuilder("init lastMsgQueue failed:");
        ((StringBuilder)localObject3).append(localException.getMessage());
        g.c("MsgQueueUtils", ((StringBuilder)localObject3).toString());
      }
    }
    g.c("MsgQueueUtils", "#unexcepted - context was null");
    return false;
    label113:
    if (paramc == null) {
      g.c("MsgQueueUtils", "#unexcepted - entityKey was null");
    }
    if (a.contains(paramc)) {
      return true;
    }
    if (a.size() >= 200) {
      a.poll();
    }
    a.offer(paramc);
    try
    {
      localObject3 = b(paramContext, "msg_queue");
      Object localObject2 = localObject3;
      if (localObject3 == null)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
      }
      if (((ArrayList)localObject2).size() >= 50) {
        ((ArrayList)localObject2).remove(0);
      }
      ((ArrayList)localObject2).add(paramc);
      a(paramContext, "msg_queue", (ArrayList)localObject2);
    }
    catch (Exception paramc)
    {
      paramContext = new StringBuilder("msg save in sp failed:");
      paramContext.append(paramc.getMessage());
      g.c("MsgQueueUtils", paramContext.toString());
    }
    return false;
  }
  
  /* Error */
  private static <T> ArrayList<T> b(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: aconst_null
    //   4: astore 5
    //   6: aconst_null
    //   7: astore 6
    //   9: aload_0
    //   10: ifnonnull +8 -> 18
    //   13: ldc 2
    //   15: monitorexit
    //   16: aconst_null
    //   17: areturn
    //   18: new 99	java/util/ArrayList
    //   21: astore 4
    //   23: aload 4
    //   25: invokespecial 141	java/util/ArrayList:<init>	()V
    //   28: aload 6
    //   30: astore_2
    //   31: new 155	java/io/ObjectInputStream
    //   34: astore_3
    //   35: aload 6
    //   37: astore_2
    //   38: aload_3
    //   39: aload_0
    //   40: aload_1
    //   41: invokevirtual 159	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   44: invokespecial 162	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   47: aload_3
    //   48: invokevirtual 165	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   51: checkcast 99	java/util/ArrayList
    //   54: astore_2
    //   55: aload_3
    //   56: invokevirtual 166	java/io/ObjectInputStream:close	()V
    //   59: aload_2
    //   60: astore_0
    //   61: goto +35 -> 96
    //   64: astore_0
    //   65: aload_3
    //   66: astore_2
    //   67: goto +34 -> 101
    //   70: astore_2
    //   71: goto +7 -> 78
    //   74: astore_0
    //   75: goto +26 -> 101
    //   78: aload_3
    //   79: astore_2
    //   80: aload_0
    //   81: aload_1
    //   82: invokestatic 168	cn/jpush/android/e/h:a	(Landroid/content/Context;Ljava/lang/String;)V
    //   85: aload_3
    //   86: ifnull +7 -> 93
    //   89: aload_3
    //   90: invokevirtual 166	java/io/ObjectInputStream:close	()V
    //   93: aload 4
    //   95: astore_0
    //   96: ldc 2
    //   98: monitorexit
    //   99: aload_0
    //   100: areturn
    //   101: aload_2
    //   102: ifnull +7 -> 109
    //   105: aload_2
    //   106: invokevirtual 166	java/io/ObjectInputStream:close	()V
    //   109: aload_0
    //   110: athrow
    //   111: astore_0
    //   112: ldc 2
    //   114: monitorexit
    //   115: aload_0
    //   116: athrow
    //   117: astore_2
    //   118: aload 5
    //   120: astore_3
    //   121: goto -43 -> 78
    //   124: astore_0
    //   125: aload_2
    //   126: astore_0
    //   127: goto -31 -> 96
    //   130: astore_0
    //   131: goto -38 -> 93
    //   134: astore_1
    //   135: goto -26 -> 109
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	138	0	paramContext	Context
    //   0	138	1	paramString	String
    //   30	37	2	localObject1	Object
    //   70	1	2	localException1	Exception
    //   79	27	2	localObject2	Object
    //   117	9	2	localException2	Exception
    //   34	87	3	localObject3	Object
    //   21	73	4	localArrayList	ArrayList
    //   4	115	5	localObject4	Object
    //   7	29	6	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   47	55	64	finally
    //   47	55	70	java/lang/Exception
    //   31	35	74	finally
    //   38	47	74	finally
    //   80	85	74	finally
    //   18	28	111	finally
    //   55	59	111	finally
    //   89	93	111	finally
    //   105	109	111	finally
    //   109	111	111	finally
    //   31	35	117	java/lang/Exception
    //   38	47	117	java/lang/Exception
    //   55	59	124	java/io/IOException
    //   89	93	130	java/io/IOException
    //   105	109	134	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */