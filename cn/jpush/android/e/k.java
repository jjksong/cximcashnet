package cn.jpush.android.e;

import java.lang.reflect.Method;

public final class k
{
  public static Object a(Object paramObject, String paramString, Class[] paramArrayOfClass, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject.length == paramArrayOfClass.length)
    {
      paramArrayOfClass = paramObject.getClass().getMethod(paramString, paramArrayOfClass);
      boolean bool = paramArrayOfClass.isAccessible();
      if (!bool) {
        paramArrayOfClass.setAccessible(true);
      }
      paramString = null;
      try
      {
        paramObject = paramArrayOfClass.invoke(paramObject, paramArrayOfObject);
        paramString = (String)paramObject;
        paramObject = null;
      }
      catch (Exception paramObject) {}
      if (!bool) {
        paramArrayOfClass.setAccessible(false);
      }
      if (paramObject == null) {
        return paramString;
      }
      throw ((Throwable)paramObject);
    }
    throw new IllegalArgumentException("argClasses' size is not equal to args' size");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */