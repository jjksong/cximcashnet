package cn.jpush.android.e;

public final class e
{
  private static double a(double paramDouble)
  {
    return paramDouble * 3.141592653589793D / 180.0D;
  }
  
  public static double a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    paramDouble2 = a(paramDouble2);
    paramDouble4 = a(paramDouble4);
    paramDouble1 = a(paramDouble1);
    paramDouble3 = a(paramDouble3);
    return Math.round(Math.asin(Math.sqrt(Math.pow(Math.sin((paramDouble2 - paramDouble4) / 2.0D), 2.0D) + Math.cos(paramDouble2) * Math.cos(paramDouble4) * Math.pow(Math.sin((paramDouble1 - paramDouble3) / 2.0D), 2.0D))) * 2.0D * 6378137.0D * 10000.0D) / 10000L;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */