package cn.jpush.android.e;

import android.text.TextUtils;
import java.util.ArrayList;
import org.json.JSONArray;

public final class b
{
  public static ArrayList<String> a(JSONArray paramJSONArray)
  {
    ArrayList localArrayList = new ArrayList();
    if ((paramJSONArray != null) && (paramJSONArray.length() != 0)) {
      for (int i = 0; i < paramJSONArray.length(); i++)
      {
        String str = paramJSONArray.optString(i);
        if (!TextUtils.isEmpty(str)) {
          localArrayList.add(str);
        }
      }
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */