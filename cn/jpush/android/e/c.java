package cn.jpush.android.e;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public final class c
{
  public static String a(Context paramContext, String paramString)
  {
    try
    {
      paramString = d(paramContext, paramString);
      if (paramString == null) {
        return "";
      }
      if (paramString.isFile()) {
        paramString.delete();
      }
      if (!paramString.exists()) {
        paramString.mkdirs();
      }
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      paramContext.append(paramString.getAbsolutePath());
      paramContext.append(File.separator);
      paramContext = paramContext.toString();
      return paramContext;
    }
    catch (Throwable paramContext) {}
    return "";
  }
  
  public static String a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return "";
    }
    return paramString.substring(paramString.lastIndexOf("/") + 1, paramString.length());
  }
  
  private static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (IOException paramCloseable)
      {
        paramCloseable.printStackTrace();
      }
    }
  }
  
  private static boolean a(File paramFile)
  {
    try
    {
      if (!paramFile.exists()) {
        return false;
      }
      if (paramFile.isFile()) {
        return paramFile.delete();
      }
      String[] arrayOfString = paramFile.list();
      if (arrayOfString != null)
      {
        int j = arrayOfString.length;
        for (int i = 0; i < j; i++)
        {
          String str = arrayOfString[i];
          File localFile = new java/io/File;
          localFile.<init>(paramFile, str);
          if (localFile.isDirectory()) {
            a(localFile);
          } else {
            localFile.delete();
          }
        }
      }
      boolean bool = paramFile.delete();
      return bool;
    }
    catch (Exception paramFile) {}
    return false;
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    if (paramString2 != null) {}
    try
    {
      paramString2 = paramString2.getBytes("UTF-8");
      break label16;
      paramString2 = null;
      label16:
      boolean bool = b(paramString1, paramString2);
      return bool;
    }
    catch (Exception paramString1) {}
    return false;
  }
  
  public static boolean a(String paramString, byte[] paramArrayOfByte)
  {
    return (paramArrayOfByte != null) && (paramArrayOfByte.length > 0) && (b(paramString, paramArrayOfByte));
  }
  
  public static String b(Context paramContext, String paramString)
  {
    try
    {
      Object localObject2;
      if (a.a())
      {
        localObject2 = Environment.getExternalStorageDirectory().getAbsolutePath();
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("/Android/data/");
        ((StringBuilder)localObject1).append(paramContext.getPackageName());
        ((StringBuilder)localObject1).append(File.separator);
        ((StringBuilder)localObject1).append(paramString);
        ((StringBuilder)localObject1).append(File.separator);
        paramString = ((StringBuilder)localObject1).toString();
        paramContext = new java/io/File;
        paramContext.<init>(paramString);
        if (!paramContext.exists()) {
          paramContext.mkdirs();
        }
        return paramString;
      }
      Object localObject1 = d(paramContext, "rich");
      if ((localObject1 != null) && (((File)localObject1).exists()) && (((File)localObject1).isDirectory()))
      {
        localObject2 = ((File)localObject1).listFiles();
        if ((localObject2 != null) && (localObject2.length > 10))
        {
          localObject1 = new cn/jpush/android/e/d;
          ((d)localObject1).<init>();
          Arrays.sort((Object[])localObject2, (Comparator)localObject1);
          a(localObject2[(localObject2.length - 1)]);
        }
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("rich");
      ((StringBuilder)localObject1).append(File.separator);
      ((StringBuilder)localObject1).append(paramString);
      paramContext = a(paramContext, ((StringBuilder)localObject1).toString());
      return paramContext;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
    return "";
  }
  
  /* Error */
  private static boolean b(String paramString, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 58	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   4: ifeq +5 -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aload_1
    //   10: astore_2
    //   11: aload_1
    //   12: ifnonnull +7 -> 19
    //   15: iconst_0
    //   16: newarray <illegal type>
    //   18: astore_2
    //   19: aconst_null
    //   20: astore_3
    //   21: new 16	java/io/File
    //   24: astore_1
    //   25: aload_1
    //   26: aload_0
    //   27: invokespecial 131	java/io/File:<init>	(Ljava/lang/String;)V
    //   30: aload_1
    //   31: invokevirtual 26	java/io/File:exists	()Z
    //   34: ifne +29 -> 63
    //   37: aload_1
    //   38: invokevirtual 153	java/io/File:getParentFile	()Ljava/io/File;
    //   41: astore_0
    //   42: aload_0
    //   43: ifnull +15 -> 58
    //   46: aload_0
    //   47: invokevirtual 26	java/io/File:exists	()Z
    //   50: ifne +8 -> 58
    //   53: aload_0
    //   54: invokevirtual 29	java/io/File:mkdirs	()Z
    //   57: pop
    //   58: aload_1
    //   59: invokevirtual 156	java/io/File:createNewFile	()Z
    //   62: pop
    //   63: new 158	java/io/FileOutputStream
    //   66: astore_0
    //   67: aload_0
    //   68: aload_1
    //   69: invokespecial 161	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   72: aload_0
    //   73: aload_2
    //   74: invokevirtual 165	java/io/FileOutputStream:write	([B)V
    //   77: aload_0
    //   78: invokestatic 167	cn/jpush/android/e/c:a	(Ljava/io/Closeable;)V
    //   81: iconst_1
    //   82: ireturn
    //   83: astore_1
    //   84: goto +6 -> 90
    //   87: astore_1
    //   88: aload_3
    //   89: astore_0
    //   90: aload_0
    //   91: invokestatic 167	cn/jpush/android/e/c:a	(Ljava/io/Closeable;)V
    //   94: aload_1
    //   95: athrow
    //   96: astore_0
    //   97: aconst_null
    //   98: astore_0
    //   99: aload_0
    //   100: invokestatic 167	cn/jpush/android/e/c:a	(Ljava/io/Closeable;)V
    //   103: iconst_0
    //   104: ireturn
    //   105: astore_0
    //   106: goto -43 -> 63
    //   109: astore_1
    //   110: goto -11 -> 99
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	113	0	paramString	String
    //   0	113	1	paramArrayOfByte	byte[]
    //   10	64	2	arrayOfByte	byte[]
    //   20	69	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   72	77	83	finally
    //   21	42	87	finally
    //   46	58	87	finally
    //   58	63	87	finally
    //   63	72	87	finally
    //   21	42	96	java/lang/Exception
    //   46	58	96	java/lang/Exception
    //   63	72	96	java/lang/Exception
    //   58	63	105	java/lang/Exception
    //   72	77	109	java/lang/Exception
  }
  
  public static String c(Context paramContext, String paramString)
  {
    try
    {
      if (a.a())
      {
        String str = Environment.getExternalStorageDirectory().getAbsolutePath();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str);
        localStringBuilder.append("/Android/data/");
        localStringBuilder.append(paramContext.getPackageName());
        localStringBuilder.append(File.separator);
        localStringBuilder.append(paramString);
        paramString = localStringBuilder.toString();
        paramContext = new java/io/File;
        paramContext.<init>(paramString);
        if (paramContext.exists()) {
          return paramString;
        }
        g.c("DirectoryUtils", "Can't find developer picture resource in SDCard.");
        return "";
      }
      g.c("DirectoryUtils", "No SDCard found.");
      return "";
    }
    catch (Exception paramContext)
    {
      g.c("DirectoryUtils", "Get developer picture resource failed.");
      paramContext.printStackTrace();
    }
    return "";
  }
  
  private static File d(Context paramContext, String paramString)
  {
    if (paramContext != null)
    {
      paramContext = paramContext.getFilesDir();
      if (paramContext != null) {
        return new File(paramContext, paramString);
      }
    }
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */