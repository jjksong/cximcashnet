package cn.jpush.android.e;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.b.e;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class a
{
  public static Intent a(Context paramContext, cn.jpush.android.data.b paramb, boolean paramBoolean)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("isUpdateVersion", false);
    localIntent.putExtra("body", paramb);
    localIntent.setAction("cn.jpush.android.ui.PushActivity");
    localIntent.addCategory(paramContext.getPackageName());
    localIntent.addFlags(536870912);
    if ((!f(paramContext)) && (Build.VERSION.SDK_INT >= 11)) {
      localIntent.addFlags(32768);
    }
    a(paramContext, localIntent);
    return localIntent;
  }
  
  private static ComponentInfo a(Context paramContext, String paramString, Class<?> paramClass)
  {
    if ((paramContext != null) && (!TextUtils.isEmpty(paramString)) && (paramClass != null)) {
      try
      {
        boolean bool = Service.class.isAssignableFrom(paramClass);
        int j = 0;
        if (bool) {
          i = 4;
        } else if (BroadcastReceiver.class.isAssignableFrom(paramClass)) {
          i = 2;
        } else if (Activity.class.isAssignableFrom(paramClass)) {
          i = 1;
        } else if (ContentProvider.class.isAssignableFrom(paramClass)) {
          i = 8;
        } else {
          i = 0;
        }
        paramContext = paramContext.getPackageManager().getPackageInfo(paramString, i);
        if (i != 4)
        {
          if (i != 8) {
            switch (i)
            {
            default: 
              paramContext = null;
              break;
            case 2: 
              paramContext = paramContext.receivers;
              break;
            case 1: 
              paramContext = paramContext.activities;
              break;
            }
          } else {
            paramContext = paramContext.providers;
          }
        }
        else {
          paramContext = paramContext.services;
        }
        if (paramContext == null) {
          return null;
        }
        paramClass = paramClass.getName();
        int k = paramContext.length;
        for (int i = j; i < k; i++)
        {
          paramString = paramContext[i];
          bool = paramClass.equals(paramString.name);
          if (bool) {
            return paramString;
          }
        }
        return null;
      }
      catch (Throwable paramString)
      {
        paramContext = new StringBuilder("hasComponent error:");
        paramContext.append(paramString.getMessage());
        g.c("AndroidUtil", paramContext.toString());
      }
    }
  }
  
  /* Error */
  public static Object a(File paramFile)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 157	java/io/File:exists	()Z
    //   4: istore_1
    //   5: aconst_null
    //   6: astore_2
    //   7: iload_1
    //   8: ifeq +78 -> 86
    //   11: aload_0
    //   12: invokevirtual 160	java/io/File:isDirectory	()Z
    //   15: ifeq +6 -> 21
    //   18: goto +68 -> 86
    //   21: new 162	java/io/ObjectInputStream
    //   24: astore_3
    //   25: new 164	java/io/FileInputStream
    //   28: astore 4
    //   30: aload 4
    //   32: aload_0
    //   33: invokespecial 167	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   36: aload_3
    //   37: aload 4
    //   39: invokespecial 170	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   42: aload_3
    //   43: invokevirtual 174	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   46: astore_0
    //   47: aload_3
    //   48: invokevirtual 177	java/io/ObjectInputStream:close	()V
    //   51: goto +8 -> 59
    //   54: astore_2
    //   55: aload_2
    //   56: invokevirtual 180	java/lang/Throwable:printStackTrace	()V
    //   59: aload_0
    //   60: areturn
    //   61: astore_0
    //   62: aload_3
    //   63: astore_2
    //   64: goto +4 -> 68
    //   67: astore_0
    //   68: aload_2
    //   69: ifnull +15 -> 84
    //   72: aload_2
    //   73: invokevirtual 177	java/io/ObjectInputStream:close	()V
    //   76: goto +8 -> 84
    //   79: astore_2
    //   80: aload_2
    //   81: invokevirtual 180	java/lang/Throwable:printStackTrace	()V
    //   84: aload_0
    //   85: athrow
    //   86: aconst_null
    //   87: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	88	0	paramFile	File
    //   4	4	1	bool	boolean
    //   6	1	2	localObject1	Object
    //   54	2	2	localThrowable1	Throwable
    //   63	10	2	localObject2	Object
    //   79	2	2	localThrowable2	Throwable
    //   24	39	3	localObjectInputStream	java.io.ObjectInputStream
    //   28	10	4	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   47	51	54	java/lang/Throwable
    //   42	47	61	finally
    //   21	42	67	finally
    //   72	76	79	java/lang/Throwable
  }
  
  public static String a(Context paramContext, String paramString)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(Build.VERSION.RELEASE);
    ((StringBuilder)localObject).append(",");
    ((StringBuilder)localObject).append(Integer.toString(Build.VERSION.SDK_INT));
    String str5 = ((StringBuilder)localObject).toString();
    String str4 = Build.MODEL;
    String str2 = j.a(paramContext, "gsm.version.baseband", "baseband");
    String str3 = Build.DEVICE;
    String str1 = JCoreInterface.getChannel();
    localObject = str1;
    if (TextUtils.isEmpty(str1)) {
      localObject = " ";
    }
    str1 = e(paramContext);
    paramContext = new JSONObject();
    try
    {
      paramContext.put("androidSdkVersion", str5);
      paramContext.put("model", str4);
      paramContext.put("baseband", str2);
      paramContext.put("device", str3);
      paramContext.put("channel", localObject);
      paramContext.put("network", str1);
      paramContext.put("url", paramString);
      return paramContext.toString();
    }
    catch (JSONException paramString)
    {
      for (;;) {}
    }
  }
  
  public static String a(String paramString)
  {
    if ((paramString != null) && (!"".equals(paramString))) {}
    try
    {
      Object localObject = MessageDigest.getInstance("MD5");
      ((MessageDigest)localObject).update(paramString.getBytes());
      paramString = ((MessageDigest)localObject).digest();
      if (paramString == null) {
        return "";
      }
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>(paramString.length * 2);
      int j = paramString.length;
      for (int i = 0; i < j; i++)
      {
        int k = paramString[i];
        ((StringBuffer)localObject).append("0123456789ABCDEF".charAt(k >> 4 & 0xF));
        ((StringBuffer)localObject).append("0123456789ABCDEF".charAt(k & 0xF));
      }
      paramString = ((StringBuffer)localObject).toString();
      return paramString;
    }
    catch (NoSuchAlgorithmException paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  public static List<String> a(Context paramContext, Intent paramIntent, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    try
    {
      paramIntent = paramContext.getPackageManager().queryBroadcastReceivers(paramIntent, 0);
      paramContext = paramContext.getPackageManager();
      Iterator localIterator = paramIntent.iterator();
      while (localIterator.hasNext())
      {
        paramIntent = (ResolveInfo)localIterator.next();
        if (paramIntent.activityInfo != null)
        {
          String str = paramIntent.activityInfo.name;
          if (!TextUtils.isEmpty(str))
          {
            int j = 1;
            int i = j;
            if (!TextUtils.isEmpty(paramString))
            {
              i = j;
              if (paramContext.checkPermission(paramString, paramIntent.activityInfo.packageName) != 0) {
                i = 0;
              }
            }
            if (i != 0) {
              localArrayList.add(str);
            }
          }
        }
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return localArrayList;
  }
  
  private static void a(Context paramContext, Intent paramIntent)
  {
    if (Build.VERSION.SDK_INT < 21) {}
    try
    {
      Object localObject1 = paramContext.getPackageManager().queryIntentActivities(paramIntent, 0).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (ResolveInfo)((Iterator)localObject1).next();
        if (((ResolveInfo)localObject2).activityInfo != null)
        {
          localObject2 = ((ResolveInfo)localObject2).activityInfo.name;
          if (!TextUtils.isEmpty((CharSequence)localObject2))
          {
            localObject1 = new android/content/ComponentName;
            ((ComponentName)localObject1).<init>(paramContext, (String)localObject2);
            paramIntent.setComponent((ComponentName)localObject1);
          }
        }
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public static void a(Context paramContext, cn.jpush.android.data.b paramb)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>("cn.jpush.android.intent.MESSAGE_RECEIVED");
      localIntent.putExtra("cn.jpush.android.APPKEY", paramb.r);
      localIntent.putExtra("cn.jpush.android.MESSAGE", paramb.l);
      localIntent.putExtra("cn.jpush.android.CONTENT_TYPE", paramb.m);
      localIntent.putExtra("cn.jpush.android.TITLE", paramb.o);
      localIntent.putExtra("cn.jpush.android.EXTRA", paramb.p);
      localIntent.putExtra("cn.jpush.android.MSG_ID", paramb.e);
      if (paramb.a()) {
        localIntent.putExtra("cn.jpush.android.FILE_PATH", paramb.T);
      }
      localIntent.addCategory(paramb.q);
      localIntent.setPackage(paramContext.getPackageName());
      paramContext.sendBroadcast(localIntent, String.format(Locale.ENGLISH, "%s.permission.JPUSH_MESSAGE", new Object[] { paramb.q }));
      if (paramb.g != 0) {
        e.a(paramb.e, "", paramb.g, 1018, paramContext);
      } else {
        e.a(paramb.e, 1018, null, paramContext);
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public static void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    if (paramBundle == null)
    {
      g.d("AndroidUtil", "Bundle should not be null for sendBroadcast.");
      return;
    }
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramString);
      paramBundle.putString("cn.jpush.android.APPKEY", JCoreInterface.getAppKey());
      localIntent.putExtras(paramBundle);
      paramBundle = paramContext.getPackageName();
      localIntent.addCategory(paramBundle);
      localIntent.setPackage(paramBundle);
      paramContext.sendBroadcast(localIntent, String.format(Locale.ENGLISH, "%s.permission.JPUSH_MESSAGE", new Object[] { paramBundle }));
      return;
    }
    catch (Exception paramContext)
    {
      paramBundle = new StringBuilder("sendBroadcast error:");
      paramBundle.append(paramContext.getMessage());
      paramBundle.append(",action:");
      paramBundle.append(paramString);
      g.c("AndroidUtil", paramBundle.toString());
    }
  }
  
  public static void a(WebSettings paramWebSettings)
  {
    paramWebSettings.setUseWideViewPort(true);
    paramWebSettings.setLoadWithOverviewMode(true);
    paramWebSettings.setDomStorageEnabled(true);
    paramWebSettings.setJavaScriptEnabled(true);
    paramWebSettings.setDefaultTextEncodingName("UTF-8");
    paramWebSettings.setSupportZoom(true);
    paramWebSettings.setBuiltInZoomControls(true);
    if (Build.VERSION.SDK_INT >= 11) {
      paramWebSettings.setDisplayZoomControls(false);
    }
    paramWebSettings.setCacheMode(2);
    paramWebSettings.setSaveFormData(false);
    paramWebSettings.setSavePassword(false);
  }
  
  public static void a(WebView paramWebView)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 11)
      {
        paramWebView.removeJavascriptInterface("searchBoxJavaBridge_");
        paramWebView.removeJavascriptInterface("accessibility");
        paramWebView.removeJavascriptInterface("accessibilityTraversal");
      }
      if (Build.VERSION.SDK_INT >= 21) {
        paramWebView.getSettings().setMixedContentMode(0);
      }
      return;
    }
    catch (Throwable paramWebView)
    {
      for (;;) {}
    }
  }
  
  public static boolean a()
  {
    boolean bool;
    try
    {
      bool = Environment.getExternalStorageState().equals("mounted");
    }
    catch (Throwable localThrowable)
    {
      bool = false;
    }
    return bool;
  }
  
  public static boolean a(Context paramContext)
  {
    try
    {
      paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if (paramContext != null)
      {
        boolean bool = paramContext.isConnected();
        if (bool) {
          return true;
        }
      }
      return false;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
    return false;
  }
  
  public static boolean a(Context paramContext, Class<?> paramClass)
  {
    boolean bool1;
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramContext, paramClass);
      bool1 = localPackageManager.queryBroadcastReceivers(localIntent, 0).isEmpty();
      boolean bool2 = bool1 ^ true;
      bool1 = bool2;
      if (bool2) {
        return bool1;
      }
      try
      {
        paramContext = a(paramContext, paramContext.getPackageName(), paramClass);
        if (paramContext != null) {
          bool1 = true;
        } else {
          bool1 = false;
        }
      }
      catch (Throwable paramContext)
      {
        bool1 = bool2;
      }
      paramClass = new StringBuilder("hasReceiverResolves error:");
    }
    catch (Throwable paramContext)
    {
      bool1 = false;
    }
    paramClass.append(paramContext.getMessage());
    g.a("AndroidUtil", paramClass.toString());
    return bool1;
  }
  
  public static boolean a(Context paramContext, String paramString, boolean paramBoolean)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramString);
      localIntent.addCategory(paramContext.getPackageName());
      paramBoolean = localPackageManager.queryBroadcastReceivers(localIntent, 0).isEmpty();
      return !paramBoolean;
    }
    catch (Throwable paramString)
    {
      paramContext = new StringBuilder("hasReceiverIntentFilter error:");
      paramContext.append(paramString.getMessage());
      g.a("AndroidUtil", paramContext.toString());
    }
    return false;
  }
  
  /* Error */
  public static boolean a(File paramFile, Object paramObject)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 160	java/io/File:isDirectory	()Z
    //   4: ifeq +5 -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aconst_null
    //   10: astore 4
    //   12: aconst_null
    //   13: astore_3
    //   14: aload_0
    //   15: ifnull +36 -> 51
    //   18: aload_0
    //   19: invokevirtual 157	java/io/File:exists	()Z
    //   22: ifne +29 -> 51
    //   25: aload_0
    //   26: invokevirtual 543	java/io/File:getParentFile	()Ljava/io/File;
    //   29: astore_2
    //   30: aload_2
    //   31: ifnull +15 -> 46
    //   34: aload_2
    //   35: invokevirtual 157	java/io/File:exists	()Z
    //   38: ifne +8 -> 46
    //   41: aload_2
    //   42: invokevirtual 546	java/io/File:mkdirs	()Z
    //   45: pop
    //   46: aload_0
    //   47: invokevirtual 549	java/io/File:createNewFile	()Z
    //   50: pop
    //   51: new 551	java/io/ObjectOutputStream
    //   54: astore_2
    //   55: new 553	java/io/FileOutputStream
    //   58: astore 5
    //   60: aload 5
    //   62: aload_0
    //   63: invokespecial 554	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   66: aload_2
    //   67: aload 5
    //   69: invokespecial 557	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   72: aload_2
    //   73: aload_1
    //   74: invokevirtual 561	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   77: aload_2
    //   78: invokevirtual 562	java/io/ObjectOutputStream:close	()V
    //   81: goto +8 -> 89
    //   84: astore_0
    //   85: aload_0
    //   86: invokevirtual 180	java/lang/Throwable:printStackTrace	()V
    //   89: iconst_1
    //   90: ireturn
    //   91: astore_0
    //   92: aload_2
    //   93: astore_1
    //   94: goto +12 -> 106
    //   97: astore_0
    //   98: aload_2
    //   99: astore_0
    //   100: goto +24 -> 124
    //   103: astore_0
    //   104: aload_3
    //   105: astore_1
    //   106: aload_1
    //   107: ifnull +15 -> 122
    //   110: aload_1
    //   111: invokevirtual 562	java/io/ObjectOutputStream:close	()V
    //   114: goto +8 -> 122
    //   117: astore_1
    //   118: aload_1
    //   119: invokevirtual 180	java/lang/Throwable:printStackTrace	()V
    //   122: aload_0
    //   123: athrow
    //   124: aload_0
    //   125: ifnull +15 -> 140
    //   128: aload_0
    //   129: invokevirtual 562	java/io/ObjectOutputStream:close	()V
    //   132: goto +8 -> 140
    //   135: astore_0
    //   136: aload_0
    //   137: invokevirtual 180	java/lang/Throwable:printStackTrace	()V
    //   140: iconst_0
    //   141: ireturn
    //   142: astore_0
    //   143: aload 4
    //   145: astore_0
    //   146: goto -22 -> 124
    //   149: astore_2
    //   150: goto -99 -> 51
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	153	0	paramFile	File
    //   0	153	1	paramObject	Object
    //   29	70	2	localObject1	Object
    //   149	1	2	localThrowable	Throwable
    //   13	92	3	localObject2	Object
    //   10	134	4	localObject3	Object
    //   58	10	5	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   77	81	84	java/lang/Throwable
    //   72	77	91	finally
    //   72	77	97	java/lang/Throwable
    //   18	30	103	finally
    //   34	46	103	finally
    //   46	51	103	finally
    //   51	72	103	finally
    //   110	114	117	java/lang/Throwable
    //   128	132	135	java/lang/Throwable
    //   18	30	142	java/lang/Throwable
    //   34	46	142	java/lang/Throwable
    //   51	72	142	java/lang/Throwable
    //   46	51	149	java/lang/Throwable
  }
  
  public static Intent b(Context paramContext)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      paramContext = paramContext.getApplicationContext().getPackageName();
      if (!TextUtils.isEmpty(paramContext))
      {
        paramContext = localPackageManager.getLaunchIntentForPackage(paramContext);
        if (paramContext != null) {}
      }
    }
    catch (Throwable paramContext)
    {
      int i;
      paramContext = null;
    }
    try
    {
      g.c("AndroidUtil", "Can't get launch intent for this package!");
      return null;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    i = 268435456;
    if (Build.VERSION.SDK_INT >= 11) {
      i = 268468224;
    }
    paramContext.addFlags(i);
    break label78;
    g.c("AndroidUtil", "The package with the given name cannot be found!");
    return null;
    label78:
    return paramContext;
  }
  
  public static void b(Context paramContext, Intent paramIntent, String paramString)
  {
    Object localObject1 = paramIntent.getAction();
    if (("cn.jpush.android.intent.NOTIFICATION_RECEIVED".equals(localObject1)) || ("cn.jpush.android.intent.NOTIFICATION_OPENED".equals(localObject1)))
    {
      localObject1 = a(paramContext, paramIntent, paramString);
      if (!((List)localObject1).isEmpty())
      {
        localObject1 = ((List)localObject1).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          String str = (String)((Iterator)localObject1).next();
          try
          {
            Intent localIntent = (Intent)paramIntent.clone();
            localObject2 = new android/content/ComponentName;
            ((ComponentName)localObject2).<init>(paramContext.getPackageName(), str);
            localIntent.setComponent((ComponentName)localObject2);
            if (TextUtils.isEmpty(paramString)) {
              paramContext.sendBroadcast(localIntent);
            } else {
              paramContext.sendBroadcast(localIntent, paramString);
            }
          }
          catch (Exception localException)
          {
            Object localObject2 = new StringBuilder("sendBroadcast failed again:");
            ((StringBuilder)localObject2).append(localException.getMessage());
            ((StringBuilder)localObject2).append(", action:");
            ((StringBuilder)localObject2).append(paramIntent.getAction());
            g.c("AndroidUtil", ((StringBuilder)localObject2).toString());
          }
        }
        return;
      }
      paramContext = new StringBuilder("sendBroadcast failed again: receiver not found, action:");
      paramContext.append(paramIntent.getAction());
      g.c("AndroidUtil", paramContext.toString());
    }
  }
  
  public static boolean b(Context paramContext, Class<?> paramClass)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramContext, paramClass);
      boolean bool = localPackageManager.queryIntentActivities(localIntent, 0).isEmpty();
      return !bool;
    }
    catch (Throwable paramClass)
    {
      paramContext = new StringBuilder("hasActivityResolves error:");
      paramContext.append(paramClass.getMessage());
      g.a("AndroidUtil", paramContext.toString());
    }
    return false;
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    if (paramContext != null) {}
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        if (paramContext.getPackageManager().checkPermission(paramString, paramContext.getPackageName()) == 0) {
          return true;
        }
      }
      else
      {
        paramContext = new java/lang/IllegalArgumentException;
        paramContext.<init>("empty params");
        throw paramContext;
      }
    }
    catch (Throwable paramContext)
    {
      paramContext.printStackTrace();
    }
    return false;
  }
  
  public static boolean b(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(paramString);
      boolean bool = localFile.exists();
      return bool;
    }
    catch (Throwable paramString) {}
    return false;
  }
  
  public static boolean c(Context paramContext)
  {
    try
    {
      if (!MultiSpHelper.getBoolean(paramContext, "notification_enabled", true))
      {
        g.b("AndroidUtil", "Notification was disabled by JPushInterface.setPushTime !");
        return false;
      }
      paramContext = cn.jpush.android.b.b(paramContext);
      if (TextUtils.isEmpty(paramContext)) {
        return true;
      }
      Object localObject2 = paramContext.split("_");
      Object localObject1 = localObject2[0];
      localObject2 = localObject2[1];
      localObject1 = ((String)localObject1).toCharArray();
      String[] arrayOfString = ((String)localObject2).split("\\^");
      localObject2 = Calendar.getInstance();
      int k = ((Calendar)localObject2).get(7);
      int j = ((Calendar)localObject2).get(11);
      int m = localObject1.length;
      for (int i = 0; i < m; i++) {
        if (k == Integer.valueOf(String.valueOf(localObject1[i])).intValue() + 1)
        {
          int n = Integer.valueOf(arrayOfString[0]).intValue();
          int i1 = Integer.valueOf(arrayOfString[1]).intValue();
          if ((j >= n) && (j <= i1)) {
            return true;
          }
        }
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Current time is out of the push time - ");
      ((StringBuilder)localObject1).append(paramContext);
      g.b("AndroidUtil", ((StringBuilder)localObject1).toString());
      return false;
    }
    catch (Exception paramContext) {}
    return true;
  }
  
  public static boolean c(Context paramContext, String paramString)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramString);
      localIntent.addCategory(paramContext.getPackageName());
      boolean bool = localPackageManager.queryIntentActivities(localIntent, 0).isEmpty();
      if (bool) {
        return false;
      }
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder("hasActivityIntentFilter error:");
      paramString.append(paramContext.getMessage());
      g.a("AndroidUtil", paramString.toString());
    }
    return true;
  }
  
  public static void d(Context paramContext, String paramString)
  {
    try
    {
      Intent localIntent = b(paramContext);
      if (localIntent != null)
      {
        if (!TextUtils.isEmpty(paramString)) {
          localIntent.putExtra("extra", paramString);
        }
        paramContext.startActivity(localIntent);
      }
      return;
    }
    catch (Throwable paramContext)
    {
      paramString = new StringBuilder("startMainActivity error:");
      paramString.append(paramContext.getMessage());
      g.a("AndroidUtil", paramString.toString());
    }
  }
  
  public static boolean d(Context paramContext)
  {
    paramContext = MultiSpHelper.getString(paramContext, "setting_silence_push_time", "");
    if (TextUtils.isEmpty(paramContext)) {
      return false;
    }
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramContext);
      int i = localJSONObject.optInt("startHour", -1);
      int n = localJSONObject.optInt("startMins", -1);
      int m = localJSONObject.optInt("endHour", -1);
      int i1 = localJSONObject.optInt("endtMins", -1);
      if ((i >= 0) && (n >= 0) && (m >= 0) && (i1 >= 0) && (n <= 59) && (i1 <= 59) && (m <= 23) && (i <= 23))
      {
        paramContext = Calendar.getInstance();
        int k = paramContext.get(11);
        int j = paramContext.get(12);
        if (i < m)
        {
          if (((k <= i) || (k >= m)) && ((k != i) || (j < n)) && ((k != m) || (j > i1))) {
            return false;
          }
        }
        else if (i == m)
        {
          if (n >= i1)
          {
            if ((k == i) && (j > i1) && (j < n)) {
              return false;
            }
          }
          else if ((k != i) || (j < n) || (j > i1)) {
            return false;
          }
        }
        else {
          if ((i <= m) || (((k <= i) || (k > 23)) && ((k < 0) || (k >= m)) && ((k != i) || (j < n)) && ((k != m) || (j > i1)))) {
            break label359;
          }
        }
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>("Current time is in the range of silence time - ");
        paramContext.append(i);
        paramContext.append(":");
        paramContext.append(n);
        paramContext.append(" ~ ");
        paramContext.append(m);
        paramContext.append(":");
        paramContext.append(i1);
        g.b("AndroidUtil", paramContext.toString());
        return true;
      }
    }
    catch (JSONException paramContext)
    {
      label359:
      for (;;) {}
    }
    return false;
  }
  
  private static String e(Context paramContext)
  {
    try
    {
      paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if (paramContext == null) {
        return "Unknown";
      }
      String str1 = paramContext.getTypeName();
      String str2 = paramContext.getSubtypeName();
      if (str1 == null)
      {
        paramContext = "Unknown";
      }
      else
      {
        paramContext = str1;
        if (!TextUtils.isEmpty(str2))
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>();
          paramContext.append(str1);
          paramContext.append(",");
          paramContext.append(str2);
          paramContext = paramContext.toString();
        }
      }
      return paramContext;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
    return "Unknown";
  }
  
  public static boolean e(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramString, 0);
      if (paramContext != null) {
        return true;
      }
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  private static boolean f(Context paramContext)
  {
    Object localObject = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    if (localObject == null) {
      return false;
    }
    Iterator localIterator = ((List)localObject).iterator();
    while (localIterator.hasNext())
    {
      localObject = (ActivityManager.RunningAppProcessInfo)localIterator.next();
      if ((((ActivityManager.RunningAppProcessInfo)localObject).processName.equals(paramContext.getPackageName())) && (((ActivityManager.RunningAppProcessInfo)localObject).importance == 100)) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/e/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */