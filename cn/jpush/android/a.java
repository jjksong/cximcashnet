package cn.jpush.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jiguang.api.SdkType;
import cn.jpush.android.e.g;
import cn.jpush.android.service.ServiceInterface;
import cn.jpush.android.service.e;
import cn.jpush.android.ui.PopWinActivity;
import cn.jpush.android.ui.PushActivity;
import java.util.concurrent.atomic.AtomicInteger;

public final class a
{
  public static final String a = SdkType.JPUSH.name();
  public static int b;
  public static String c;
  public static String d;
  public static Context e;
  public static boolean f = false;
  private static final AtomicInteger g = new AtomicInteger(-1);
  private static final Object h = new Object();
  
  public static boolean a(Context paramContext)
  {
    return c(paramContext) == 0;
  }
  
  public static Context b(Context paramContext)
  {
    if ((e == null) && (paramContext != null))
    {
      e = paramContext.getApplicationContext();
      c = paramContext.getPackageName();
    }
    return e;
  }
  
  private static int c(Context paramContext)
  {
    if (g.get() == 0) {
      return 0;
    }
    synchronized (h)
    {
      if (g.get() == 0) {
        return 0;
      }
      Context localContext = b(paramContext);
      int i = 1;
      if (localContext == null)
      {
        g.set(1);
        return 1;
      }
      g.a("AndroidUtil", "action:checkValidManifest");
      if (!cn.jpush.android.e.a.b(localContext, PushActivity.class))
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>("AndroidManifest.xml missing required activity: ");
        paramContext.append(PushActivity.class.getCanonicalName());
        paramContext = paramContext.toString();
      }
      else
      {
        if (!cn.jpush.android.e.a.b(localContext, PopWinActivity.class))
        {
          paramContext = new java/lang/StringBuilder;
          paramContext.<init>("AndroidManifest.xml missing activity: ");
          paramContext.append(PopWinActivity.class.getCanonicalName());
          g.c("AndroidUtil", paramContext.toString());
          g.c("AndroidUtil", "You will unable to use pop-window rich push type.");
        }
        if (cn.jpush.android.e.a.c(localContext, "cn.jpush.android.ui.PushActivity")) {
          break label161;
        }
        paramContext = "AndroidManifest.xml missing required intent filter for PushActivity: cn.jpush.android.ui.PushActivity";
      }
      g.d("AndroidUtil", paramContext);
      i = 0;
      label161:
      if (i == 0)
      {
        g.set(2);
        return 2;
      }
      g.set(0);
      for (;;)
      {
        try
        {
          paramContext = MultiSpHelper.getString(localContext, "jpush_sdk_version", "");
          if (!"3.2.0".equals(paramContext)) {
            TextUtils.isEmpty(paramContext);
          }
          MultiSpHelper.commitString(localContext, "jpush_sdk_version", "3.2.0");
        }
        catch (Throwable paramContext)
        {
          continue;
        }
        try
        {
          paramContext = localContext.getPackageManager().getApplicationInfo(localContext.getPackageName(), 0);
          if (paramContext == null)
          {
            g.d("JPushGlobal", "JPush get NULL appInfo.");
          }
          else
          {
            b = paramContext.icon;
            d = localContext.getPackageManager().getApplicationLabel(paramContext).toString();
          }
        }
        catch (Throwable localThrowable)
        {
          paramContext = new StringBuilder("get packageIconId and appName error:");
          paramContext.append(localThrowable);
          g.c("JPushGlobal", paramContext.toString());
        }
      }
      ServiceInterface.a(localContext);
      paramContext = new Bundle();
      e.a(localContext, paramContext, "intent.START_GEOFENCE");
      JCoreInterface.sendAction(localContext, a, paramContext);
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */