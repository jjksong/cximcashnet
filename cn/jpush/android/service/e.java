package cn.jpush.android.service;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.a.f;
import cn.jpush.android.api.c;
import cn.jpush.android.b;
import cn.jpush.android.b.o;
import cn.jpush.android.b.r;
import cn.jpush.android.d.d;
import cn.jpush.android.data.JPushLocalNotification;

public final class e
{
  private static e b;
  private Context a;
  
  private e(Context paramContext)
  {
    this.a = paramContext;
  }
  
  public static e a(Context paramContext)
  {
    if (b == null) {
      b = new e(paramContext);
    }
    return b;
  }
  
  public static void a(Context paramContext, Bundle paramBundle, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b(paramContext));
    localStringBuilder.append(".");
    localStringBuilder.append(paramString);
    paramBundle.putString("action", localStringBuilder.toString());
  }
  
  private static String b(Context paramContext)
  {
    String str2 = cn.jpush.android.a.c;
    String str1 = str2;
    if (TextUtils.isEmpty(str2))
    {
      str1 = str2;
      if (paramContext != null) {
        str1 = paramContext.getPackageName();
      }
    }
    paramContext = str1;
    if (str1 == null) {
      paramContext = "";
    }
    return paramContext;
  }
  
  public final void a()
  {
    a.a(this.a).d(this.a);
  }
  
  public final void a(Bundle paramBundle, Handler paramHandler)
  {
    Object localObject = new StringBuilder("bundle:");
    if (paramBundle != null) {
      paramHandler = paramBundle.toString();
    } else {
      paramHandler = "";
    }
    ((StringBuilder)localObject).append(paramHandler);
    cn.jpush.android.e.g.a("PushServiceCore", ((StringBuilder)localObject).toString());
    if (paramBundle == null) {
      return;
    }
    localObject = paramBundle.getString("action");
    if (localObject != null)
    {
      paramHandler = new StringBuilder("Action - handleServiceAction - action:");
      paramHandler.append((String)localObject);
      cn.jpush.android.e.g.a("PushServiceCore", paramHandler.toString());
      paramHandler = new StringBuilder();
      paramHandler.append(b(this.a));
      paramHandler.append(".");
      String str = paramHandler.toString();
      paramHandler = (Handler)localObject;
      if (((String)localObject).startsWith(str)) {
        paramHandler = ((String)localObject).substring(str.length());
      }
      int i;
      long l;
      if ("intent.MULTI_PROCESS".equals(paramHandler))
      {
        switch (paramBundle.getInt("multi_type"))
        {
        case 5: 
        default: 
          break;
        case 10: 
          c.a(this.a, true);
          break;
        case 9: 
          i = paramBundle.getInt("notification_id");
          c.a(this.a, i, true);
          break;
        case 8: 
          a.a(this.a).b(this.a);
          break;
        case 7: 
          l = paramBundle.getLong("local_notification_id");
          a.a(this.a).a(this.a, l);
          break;
        case 6: 
          paramBundle = (JPushLocalNotification)paramBundle.getSerializable("local_notification");
          a.a(this.a).a(this.a, paramBundle, true);
          break;
        case 4: 
          paramBundle = paramBundle.getString("silence_push_time");
          b.a(this.a, paramBundle, true);
          break;
        case 3: 
          paramBundle = paramBundle.getString("enable_push_time");
          b.b(this.a, paramBundle, true);
          break;
        case 2: 
          i = paramBundle.getInt("notification_maxnum");
          b.a(this.a, i, true);
          break;
        case 1: 
          paramHandler = paramBundle.getString("notification_buidler_id");
          paramBundle = paramBundle.getString("notification_buidler");
          b.a(this.a, paramHandler, paramBundle, true);
          break;
        }
        return;
      }
      if ("intent.STOPPUSH".equals(paramHandler))
      {
        MultiSpHelper.commitInt(this.a, "service_stoped", 1);
        return;
      }
      if (paramHandler.equals("intent.RESTOREPUSH"))
      {
        MultiSpHelper.commitInt(this.a, "service_stoped", 0);
        return;
      }
      if ("intent.ALIAS_TAGS".equals(paramHandler))
      {
        r.a(this.a, paramBundle);
        return;
      }
      if ("intent.plugin.platform.REQUEST_REGID".equals(paramHandler))
      {
        cn.jpush.android.d.a.a().a(this.a, paramBundle);
        return;
      }
      if ("intent.plugin.platform.ON_MESSAGING".equals(paramHandler))
      {
        paramHandler = paramBundle.getString("appId");
        localObject = paramBundle.getString("senderId");
        str = paramBundle.getString("JMessageExtra");
        byte b1 = paramBundle.getByte("platform");
        paramBundle = new StringBuilder("appId:");
        paramBundle.append(String.valueOf(paramHandler));
        paramBundle.append(",senderId:");
        paramBundle.append(String.valueOf(localObject));
        paramBundle.append(",JMessageExtra:");
        paramBundle.append(String.valueOf(str));
        Log.d("PushServiceCore", paramBundle.toString());
        if (!TextUtils.isEmpty(str))
        {
          paramBundle = paramHandler;
          if (TextUtils.isEmpty(paramHandler)) {
            paramBundle = this.a.getPackageName();
          }
          paramHandler = (Handler)localObject;
          if (TextUtils.isEmpty((CharSequence)localObject)) {
            paramHandler = JCoreInterface.getAppKey();
          }
          o.a(this.a, paramBundle, paramHandler, str, 0L, b1);
        }
      }
      else
      {
        if ("action_notification_show".equals(paramHandler))
        {
          d.a().a(this.a, paramBundle);
          return;
        }
        if ("intent.MOBILE_NUMBER".equals(paramHandler))
        {
          cn.jpush.android.b.g.a().a(this.a, paramBundle);
          return;
        }
        if ("intent.START_GEOFENCE".equals(paramHandler))
        {
          f.a(this.a).a();
          return;
        }
        if ("intent.ACTION_GEOFENCE_TRIGGER".equals(paramHandler))
        {
          f.a(this.a);
          return;
        }
        if ("intent.SET_GEOFENCE_INTERVAL".equals(paramHandler))
        {
          l = paramBundle.getLong("interval");
          f.a(this.a).a(l);
          return;
        }
        if ("intent.SET_GEOFENCE_MAXNUM".equals(paramHandler))
        {
          i = paramBundle.getInt("geofence_num");
          f.a(this.a).a(i);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */