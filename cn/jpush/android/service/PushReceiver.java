package cn.jpush.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;
import cn.jiguang.api.JCoreManager;

public class PushReceiver
  extends BroadcastReceiver
{
  private static final String TAG = "PushReceiver";
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append("onReceive:");
      ((StringBuilder)localObject).append(paramIntent.getAction());
      c.b("PushReceiver", ((StringBuilder)localObject).toString());
      localObject = paramIntent.getStringExtra("sdktype");
      JCoreManager.onEvent(paramContext.getApplicationContext(), (String)localObject, 31, null, null, new Object[] { paramIntent });
      e.a(paramContext.getApplicationContext(), paramIntent);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/PushReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */