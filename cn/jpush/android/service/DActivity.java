package cn.jpush.android.service;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import cn.jiguang.ac.c;
import cn.jiguang.y.a;

public class DActivity
  extends Activity
{
  private static final String TAG = "DActivity";
  
  private void handleStart()
  {
    Object localObject;
    try
    {
      a locala = a.d();
      if (getIntent() != null) {
        localObject = getIntent().getExtras();
      } else {
        localObject = null;
      }
      locala.a(this, (Bundle)localObject, 8);
    }
    catch (Throwable localThrowable1)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("handle start error#");
      ((StringBuilder)localObject).append(localThrowable1);
      c.b("DActivity", ((StringBuilder)localObject).toString());
    }
    try
    {
      finish();
    }
    catch (Throwable localThrowable2)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("finish error#");
      ((StringBuilder)localObject).append(localThrowable2);
      c.b("DActivity", ((StringBuilder)localObject).toString());
    }
  }
  
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    c.b("DActivity", "DActivity oncreate");
    handleStart();
  }
  
  protected final void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    c.b("DActivity", "DActivity onNewIntent");
    handleStart();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */