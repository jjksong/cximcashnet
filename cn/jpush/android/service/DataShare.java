package cn.jpush.android.service;

import android.os.Bundle;
import android.os.IBinder;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;
import cn.jiguang.d.a.a;
import cn.jiguang.sdk.impl.b;

public class DataShare
  extends a.a
{
  private static final String TAG = "DataShare";
  private static cn.jiguang.d.a instance;
  private static boolean isBinding = false;
  
  public static boolean alreadyBound()
  {
    boolean bool;
    if (instance != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public static cn.jiguang.d.a getInstance()
  {
    return instance;
  }
  
  public static void init(cn.jiguang.d.a parama)
  {
    if (parama != instance) {
      instance = parama;
    }
    isBinding = false;
  }
  
  public static boolean isBinding()
  {
    return isBinding;
  }
  
  public static void setBinding()
  {
    isBinding = true;
  }
  
  public IBinder getBinderByType(String paramString1, String paramString2)
  {
    return null;
  }
  
  public boolean isPushLoggedIn()
  {
    c.a("DataShare", "pushLogin status by aidl");
    return b.b();
  }
  
  public void onAction(String paramString, Bundle paramBundle)
  {
    if (paramString != null) {
      try
      {
        e.b(cn.jiguang.a.a.a, paramString, paramBundle);
      }
      catch (Throwable paramString)
      {
        paramBundle = new StringBuilder();
        paramBundle.append("onAction error:");
        paramBundle.append(paramString.getMessage());
        c.g("DataShare", paramBundle.toString());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DataShare.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */