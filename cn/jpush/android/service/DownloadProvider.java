package cn.jpush.android.service;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import cn.jiguang.a.a;
import cn.jiguang.ab.f;
import cn.jiguang.ac.c;
import cn.jiguang.ah.e;
import cn.jiguang.api.JCoreManager;

public class DownloadProvider
  extends ContentProvider
{
  private static final String TAG = "DownloadProvider";
  
  private void init()
  {
    try
    {
      JCoreManager.init(getContext().getApplicationContext());
    }
    catch (Throwable localThrowable)
    {
      c.a("DownloadProvider", "");
    }
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    init();
    return 0;
  }
  
  public String getType(Uri paramUri)
  {
    init();
    return f.a(getContext(), paramUri);
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    init();
    return null;
  }
  
  public boolean onCreate()
  {
    return false;
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    paramArrayOfString1 = new StringBuilder();
    paramArrayOfString1.append("DownloadProvider query:");
    paramArrayOfString1.append(paramUri);
    c.b("DownloadProvider", paramArrayOfString1.toString());
    try
    {
      paramUri = paramUri.getQueryParameter("from_package");
      paramArrayOfString1 = new android/os/Bundle;
      paramArrayOfString1.<init>();
      paramArrayOfString1.putString("from_package", paramUri);
      paramArrayOfString1.putInt("type", 4);
      paramArrayOfString1.putBoolean("live", a.f);
      e.a(getContext(), "waked", paramArrayOfString1);
      init();
    }
    catch (Throwable paramUri)
    {
      paramArrayOfString1 = new StringBuilder();
      paramArrayOfString1.append("wake error:");
      paramArrayOfString1.append(paramUri.getMessage());
      c.f("DownloadProvider", paramArrayOfString1.toString());
    }
    return null;
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    init();
    return 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DownloadProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */