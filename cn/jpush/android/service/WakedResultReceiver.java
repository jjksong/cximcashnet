package cn.jpush.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Map;

public class WakedResultReceiver
  extends BroadcastReceiver
{
  public static final String CONTEXT_KEY = "1";
  public static final String WAKE_TYPE_KEY = "2";
  
  public final void onReceive(Context paramContext, Intent paramIntent) {}
  
  public void onWake(int paramInt) {}
  
  public void onWake(Context paramContext, int paramInt) {}
  
  public final void onWakeMap(Map paramMap)
  {
    try
    {
      Object localObject = paramMap.get("2");
      int i = -1;
      if (localObject != null) {
        i = ((Integer)localObject).intValue();
      }
      onWake(i);
      localObject = paramMap.get("1");
      paramMap = null;
      if (localObject != null) {
        paramMap = (Context)localObject;
      }
      onWake(paramMap, i);
      return;
    }
    catch (Throwable paramMap)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/WakedResultReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */