package cn.jpush.android.service;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import cn.jiguang.ab.f;
import cn.jiguang.ac.c;

public class DownloadActivity
  extends Activity
{
  private static final String TAG = "DownloadActivity";
  
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    c.b("DownloadActivity", "DownloadActivity onCreate");
    paramBundle = getIntent();
    try
    {
      f.a(getApplicationContext(), paramBundle);
      finish();
      return;
    }
    catch (Throwable paramBundle)
    {
      for (;;) {}
    }
  }
  
  protected final void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    c.b("DownloadActivity", "DownloadActivity onNewIntent");
    try
    {
      f.a(getApplicationContext(), paramIntent);
      finish();
      return;
    }
    catch (Throwable paramIntent)
    {
      for (;;) {}
    }
  }
  
  protected final void onRestart()
  {
    super.onRestart();
  }
  
  protected final void onResume()
  {
    super.onResume();
  }
  
  protected final void onStart()
  {
    super.onStart();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DownloadActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */