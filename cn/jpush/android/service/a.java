package cn.jpush.android.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.data.JPushLocalNotification;
import cn.jpush.android.data.d;
import cn.jpush.android.e.g;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class a
{
  private static volatile a a;
  private static ExecutorService b = ;
  private static final Object f = new Object();
  private Handler c = null;
  private Context d = null;
  private String e = "";
  
  private a(Context paramContext)
  {
    this.d = paramContext;
    this.e = this.d.getPackageName();
  }
  
  public static a a(Context paramContext)
  {
    if (a == null) {
      synchronized (f)
      {
        if (a == null)
        {
          a locala = new cn/jpush/android/service/a;
          locala.<init>(paramContext);
          a = locala;
        }
      }
    }
    return a;
  }
  
  private void a(long paramLong1, long paramLong2)
  {
    try
    {
      if (this.c != null)
      {
        c localc = new cn/jpush/android/service/c;
        localc.<init>(this, paramLong1);
        if (paramLong2 <= 0L)
        {
          this.c.post(localc);
          return;
        }
        this.c.postDelayed(localc, paramLong2);
      }
      return;
    }
    finally {}
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    Intent localIntent = new Intent("cn.jpush.android.intent.NOTIFICATION_RECEIVED_PROXY");
    localIntent.putExtra("senderId", paramString3);
    localIntent.putExtra("appId", paramString2);
    localIntent.putExtra("message", paramString1);
    localIntent.putExtra("notificaion_type", 1);
    localIntent.addCategory(paramString2);
    paramString1 = new StringBuilder();
    paramString1.append(paramString2);
    paramString1.append(".permission.JPUSH_MESSAGE");
    paramContext.sendOrderedBroadcast(localIntent, paramString1.toString());
  }
  
  private boolean a(Context paramContext, JPushLocalNotification paramJPushLocalNotification)
  {
    try
    {
      g.a("JPushLocalNotificationCenter", "add LocalNotification");
      long l1 = System.currentTimeMillis();
      long l2 = paramJPushLocalNotification.getBroadcastTime() - l1;
      ServiceInterface.d(paramContext);
      long l3 = paramJPushLocalNotification.getNotificationId();
      try
      {
        paramContext = d.a(paramContext);
        if (paramContext.a(l3, 0) != null) {
          paramContext.b(l3, 1, 0, 0, paramJPushLocalNotification.toJSON(), paramJPushLocalNotification.getBroadcastTime(), l1);
        } else {
          paramContext.a(l3, 1, 0, 0, paramJPushLocalNotification.toJSON(), paramJPushLocalNotification.getBroadcastTime(), l1);
        }
      }
      catch (Exception paramContext)
      {
        paramContext.printStackTrace();
      }
      if (l2 < 300000L)
      {
        a(paramJPushLocalNotification.getNotificationId(), l2);
        return true;
      }
      return true;
    }
    finally {}
  }
  
  /* Error */
  private void e(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokestatic 163	cn/jpush/android/data/d:a	(Landroid/content/Context;)Lcn/jpush/android/data/d;
    //   6: astore 6
    //   8: aload 6
    //   10: iconst_0
    //   11: invokevirtual 185	cn/jpush/android/data/d:a	(Z)Z
    //   14: istore_2
    //   15: iload_2
    //   16: ifeq +218 -> 234
    //   19: aconst_null
    //   20: astore 5
    //   22: aconst_null
    //   23: astore_3
    //   24: aload 6
    //   26: iconst_1
    //   27: invokestatic 145	java/lang/System:currentTimeMillis	()J
    //   30: invokevirtual 188	cn/jpush/android/data/d:a	(IJ)Landroid/database/Cursor;
    //   33: astore 4
    //   35: aload 4
    //   37: invokeinterface 194 1 0
    //   42: ifeq +65 -> 107
    //   45: aload 4
    //   47: invokestatic 197	cn/jpush/android/data/d:a	(Landroid/database/Cursor;)Lcn/jpush/android/data/e;
    //   50: astore_3
    //   51: aload_3
    //   52: ifnull +43 -> 95
    //   55: aload_0
    //   56: aload_1
    //   57: aload_3
    //   58: invokevirtual 201	cn/jpush/android/data/e:d	()Ljava/lang/String;
    //   61: aload_0
    //   62: getfield 41	cn/jpush/android/service/a:e	Ljava/lang/String;
    //   65: ldc 39
    //   67: invokespecial 127	cn/jpush/android/service/a:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   70: aload 6
    //   72: aload_3
    //   73: invokevirtual 203	cn/jpush/android/data/e:a	()J
    //   76: iconst_0
    //   77: iconst_0
    //   78: iconst_0
    //   79: aload_3
    //   80: invokevirtual 201	cn/jpush/android/data/e:d	()Ljava/lang/String;
    //   83: aload_3
    //   84: invokevirtual 205	cn/jpush/android/data/e:f	()J
    //   87: aload_3
    //   88: invokevirtual 207	cn/jpush/android/data/e:e	()J
    //   91: invokevirtual 172	cn/jpush/android/data/d:b	(JIIILjava/lang/String;JJ)J
    //   94: pop2
    //   95: aload 4
    //   97: invokeinterface 210 1 0
    //   102: istore_2
    //   103: iload_2
    //   104: ifne -59 -> 45
    //   107: aload 4
    //   109: ifnull +10 -> 119
    //   112: aload 4
    //   114: invokeinterface 213 1 0
    //   119: aload 6
    //   121: iconst_0
    //   122: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   125: aload_0
    //   126: monitorexit
    //   127: return
    //   128: astore_1
    //   129: goto +85 -> 214
    //   132: astore_3
    //   133: aload 4
    //   135: astore_1
    //   136: aload_3
    //   137: astore 4
    //   139: goto +15 -> 154
    //   142: astore_1
    //   143: aload_3
    //   144: astore 4
    //   146: goto +68 -> 214
    //   149: astore 4
    //   151: aload 5
    //   153: astore_1
    //   154: aload_1
    //   155: astore_3
    //   156: new 107	java/lang/StringBuilder
    //   159: astore 5
    //   161: aload_1
    //   162: astore_3
    //   163: aload 5
    //   165: ldc -38
    //   167: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   170: aload_1
    //   171: astore_3
    //   172: aload 5
    //   174: aload 4
    //   176: invokevirtual 222	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   179: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload_1
    //   184: astore_3
    //   185: ldc -124
    //   187: aload 5
    //   189: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   192: invokestatic 224	cn/jpush/android/e/g:c	(Ljava/lang/String;Ljava/lang/String;)V
    //   195: aload_1
    //   196: ifnull +9 -> 205
    //   199: aload_1
    //   200: invokeinterface 213 1 0
    //   205: aload 6
    //   207: iconst_0
    //   208: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   211: aload_0
    //   212: monitorexit
    //   213: return
    //   214: aload 4
    //   216: ifnull +10 -> 226
    //   219: aload 4
    //   221: invokeinterface 213 1 0
    //   226: aload 6
    //   228: iconst_0
    //   229: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   232: aload_1
    //   233: athrow
    //   234: aload_0
    //   235: monitorexit
    //   236: return
    //   237: astore_1
    //   238: aload_0
    //   239: monitorexit
    //   240: aload_1
    //   241: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	242	0	this	a
    //   0	242	1	paramContext	Context
    //   14	90	2	bool	boolean
    //   23	65	3	locale	cn.jpush.android.data.e
    //   132	12	3	localException1	Exception
    //   155	30	3	localContext	Context
    //   33	112	4	localObject	Object
    //   149	71	4	localException2	Exception
    //   20	168	5	localStringBuilder	StringBuilder
    //   6	221	6	locald	d
    // Exception table:
    //   from	to	target	type
    //   35	45	128	finally
    //   45	51	128	finally
    //   55	95	128	finally
    //   95	103	128	finally
    //   35	45	132	java/lang/Exception
    //   45	51	132	java/lang/Exception
    //   55	95	132	java/lang/Exception
    //   95	103	132	java/lang/Exception
    //   24	35	142	finally
    //   156	161	142	finally
    //   163	170	142	finally
    //   172	183	142	finally
    //   185	195	142	finally
    //   24	35	149	java/lang/Exception
    //   2	15	237	finally
    //   112	119	237	finally
    //   119	125	237	finally
    //   199	205	237	finally
    //   205	211	237	finally
    //   219	226	237	finally
    //   226	234	237	finally
  }
  
  /* Error */
  public final boolean a(Context paramContext, long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc -124
    //   4: ldc -29
    //   6: invokestatic 139	cn/jpush/android/e/g:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   9: lload_2
    //   10: l2i
    //   11: i2l
    //   12: lstore_2
    //   13: aload_1
    //   14: invokestatic 163	cn/jpush/android/data/d:a	(Landroid/content/Context;)Lcn/jpush/android/data/d;
    //   17: astore_1
    //   18: aload_1
    //   19: lload_2
    //   20: iconst_0
    //   21: invokevirtual 166	cn/jpush/android/data/d:a	(JI)Lcn/jpush/android/data/e;
    //   24: ifnull +22 -> 46
    //   27: aload_1
    //   28: lload_2
    //   29: invokevirtual 230	cn/jpush/android/data/d:a	(J)I
    //   32: pop
    //   33: aload_0
    //   34: getfield 37	cn/jpush/android/service/a:d	Landroid/content/Context;
    //   37: lload_2
    //   38: l2i
    //   39: invokestatic 236	cn/jpush/android/api/JPushInterface:clearNotificationById	(Landroid/content/Context;I)V
    //   42: aload_0
    //   43: monitorexit
    //   44: iconst_1
    //   45: ireturn
    //   46: aload_0
    //   47: monitorexit
    //   48: iconst_0
    //   49: ireturn
    //   50: astore_1
    //   51: aload_1
    //   52: invokevirtual 177	java/lang/Exception:printStackTrace	()V
    //   55: aload_0
    //   56: monitorexit
    //   57: iconst_0
    //   58: ireturn
    //   59: astore_1
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	a
    //   0	64	1	paramContext	Context
    //   0	64	2	paramLong	long
    // Exception table:
    //   from	to	target	type
    //   13	42	50	java/lang/Exception
    //   2	9	59	finally
    //   13	42	59	finally
    //   51	55	59	finally
  }
  
  public final boolean a(Context paramContext, JPushLocalNotification paramJPushLocalNotification, boolean paramBoolean)
  {
    if (paramBoolean) {}
    try
    {
      a(paramContext, paramJPushLocalNotification);
      break label61;
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
      localBundle.putInt("multi_type", 6);
      localBundle.putSerializable("local_notification", paramJPushLocalNotification);
      JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
      label61:
      return true;
    }
    finally {}
  }
  
  public final void b(Context paramContext)
  {
    try
    {
      g.a("JPushLocalNotificationCenter", "clear all local notification ");
      paramContext = d.a(paramContext).a();
      if ((paramContext != null) && (paramContext.length > 0))
      {
        int j = paramContext.length;
        for (int i = 0; i < j; i++)
        {
          int k = paramContext[i];
          JPushInterface.clearNotificationById(this.d, k);
        }
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public final void c(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokestatic 163	cn/jpush/android/data/d:a	(Landroid/content/Context;)Lcn/jpush/android/data/d;
    //   6: astore 8
    //   8: aload 8
    //   10: iconst_0
    //   11: invokevirtual 185	cn/jpush/android/data/d:a	(Z)Z
    //   14: istore_2
    //   15: iload_2
    //   16: ifeq +180 -> 196
    //   19: aconst_null
    //   20: astore 7
    //   22: aconst_null
    //   23: astore 6
    //   25: aload 6
    //   27: astore_1
    //   28: aload 7
    //   30: astore 5
    //   32: invokestatic 145	java/lang/System:currentTimeMillis	()J
    //   35: lstore_3
    //   36: aload 6
    //   38: astore_1
    //   39: aload 7
    //   41: astore 5
    //   43: aload 8
    //   45: lload_3
    //   46: ldc2_w 178
    //   49: invokevirtual 279	cn/jpush/android/data/d:a	(JJ)Landroid/database/Cursor;
    //   52: astore 6
    //   54: aload 6
    //   56: astore_1
    //   57: aload 6
    //   59: astore 5
    //   61: aload 6
    //   63: invokeinterface 194 1 0
    //   68: ifeq +64 -> 132
    //   71: aload 6
    //   73: astore_1
    //   74: aload 6
    //   76: astore 5
    //   78: aload 6
    //   80: invokestatic 197	cn/jpush/android/data/d:a	(Landroid/database/Cursor;)Lcn/jpush/android/data/e;
    //   83: astore 7
    //   85: aload 7
    //   87: ifnull +26 -> 113
    //   90: aload 6
    //   92: astore_1
    //   93: aload 6
    //   95: astore 5
    //   97: aload_0
    //   98: aload 7
    //   100: invokevirtual 203	cn/jpush/android/data/e:a	()J
    //   103: aload 7
    //   105: invokevirtual 205	cn/jpush/android/data/e:f	()J
    //   108: lload_3
    //   109: lsub
    //   110: invokespecial 181	cn/jpush/android/service/a:a	(JJ)V
    //   113: aload 6
    //   115: astore_1
    //   116: aload 6
    //   118: astore 5
    //   120: aload 6
    //   122: invokeinterface 210 1 0
    //   127: istore_2
    //   128: iload_2
    //   129: ifne -58 -> 71
    //   132: aload 6
    //   134: ifnull +10 -> 144
    //   137: aload 6
    //   139: invokeinterface 213 1 0
    //   144: aload 8
    //   146: iconst_0
    //   147: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   150: aload_0
    //   151: monitorexit
    //   152: return
    //   153: astore 5
    //   155: aload_1
    //   156: ifnull +9 -> 165
    //   159: aload_1
    //   160: invokeinterface 213 1 0
    //   165: aload 8
    //   167: iconst_0
    //   168: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   171: aload 5
    //   173: athrow
    //   174: astore_1
    //   175: aload 5
    //   177: ifnull +10 -> 187
    //   180: aload 5
    //   182: invokeinterface 213 1 0
    //   187: aload 8
    //   189: iconst_0
    //   190: invokevirtual 216	cn/jpush/android/data/d:b	(Z)V
    //   193: aload_0
    //   194: monitorexit
    //   195: return
    //   196: aload_0
    //   197: monitorexit
    //   198: return
    //   199: astore_1
    //   200: aload_0
    //   201: monitorexit
    //   202: aload_1
    //   203: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	this	a
    //   0	204	1	paramContext	Context
    //   14	115	2	bool	boolean
    //   35	74	3	l	long
    //   30	89	5	localObject1	Object
    //   153	28	5	localObject2	Object
    //   23	115	6	localCursor	android.database.Cursor
    //   20	84	7	locale	cn.jpush.android.data.e
    //   6	182	8	locald	d
    // Exception table:
    //   from	to	target	type
    //   32	36	153	finally
    //   43	54	153	finally
    //   61	71	153	finally
    //   78	85	153	finally
    //   97	113	153	finally
    //   120	128	153	finally
    //   32	36	174	java/lang/Exception
    //   43	54	174	java/lang/Exception
    //   61	71	174	java/lang/Exception
    //   78	85	174	java/lang/Exception
    //   97	113	174	java/lang/Exception
    //   120	128	174	java/lang/Exception
    //   2	15	199	finally
    //   137	144	199	finally
    //   144	150	199	finally
    //   159	165	199	finally
    //   165	174	199	finally
    //   180	187	199	finally
    //   187	193	199	finally
  }
  
  public final void d(Context paramContext)
  {
    b.execute(new b(this, paramContext));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */