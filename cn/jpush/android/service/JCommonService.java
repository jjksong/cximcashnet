package cn.jpush.android.service;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.text.TextUtils;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;
import cn.jiguang.d.a.a;

public class JCommonService
  extends Service
{
  private static final String TAG = "JCommonService";
  private static a.a mBinder;
  
  public final IBinder onBind(Intent paramIntent)
  {
    return mBinder;
  }
  
  public final void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public final void onCreate()
  {
    super.onCreate();
    if (mBinder == null) {
      mBinder = new DataShare();
    }
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
  }
  
  public final void onLowMemory()
  {
    super.onLowMemory();
  }
  
  public final void onRebind(Intent paramIntent)
  {
    super.onRebind(paramIntent);
  }
  
  public final void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if ((paramIntent != null) && (!TextUtils.isEmpty(paramIntent.getAction()))) {
      e.b(this, paramIntent.getAction(), paramIntent.getExtras());
    } else {
      c.f("JCommonService", "onStartCommand intent is empty or action is empty");
    }
    return super.onStartCommand(paramIntent, paramInt1, paramInt2);
  }
  
  public final void onTaskRemoved(Intent paramIntent)
  {
    super.onTaskRemoved(paramIntent);
  }
  
  public final void onTrimMemory(int paramInt)
  {
    super.onTrimMemory(paramInt);
  }
  
  public final boolean onUnbind(Intent paramIntent)
  {
    return super.onUnbind(paramIntent);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/JCommonService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */