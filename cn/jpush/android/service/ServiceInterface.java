package cn.jpush.android.service;

import android.content.Context;
import android.os.Bundle;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.api.DefaultPushNotificationBuilder;
import cn.jpush.android.api.c;
import cn.jpush.android.b;
import cn.jpush.android.d.d;
import cn.jpush.android.e.g;
import java.util.ArrayList;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceInterface
{
  public static String a()
  {
    return "3.2.0";
  }
  
  public static void a(Context paramContext)
  {
    if (d(paramContext)) {
      return;
    }
    JCoreInterface.restart(paramContext, cn.jpush.android.a.a, new Bundle(), false);
    d.a().a(paramContext);
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    MultiSpHelper.commitInt(paramContext, "service_stoped", 1);
    Bundle localBundle = new Bundle();
    e.a(paramContext, localBundle, "intent.STOPPUSH");
    localBundle.putString("app", paramContext.getPackageName());
    JCoreInterface.stop(paramContext, cn.jpush.android.a.a, localBundle);
  }
  
  public static void a(Context paramContext, Integer paramInteger, DefaultPushNotificationBuilder paramDefaultPushNotificationBuilder)
  {
    if (paramContext == null)
    {
      g.d("ServiceInterface", "Null context, please init JPush!");
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramInteger);
    b.a(paramContext, localStringBuilder.toString(), paramDefaultPushNotificationBuilder.toString(), false);
  }
  
  public static void a(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (!d(paramContext))) {
      b.a(paramContext, paramString, false);
    }
  }
  
  public static void a(Context paramContext, String paramString, Set<String> paramSet, long paramLong, cn.jpush.android.api.a parama)
  {
    Bundle localBundle = new Bundle();
    e.a(paramContext, localBundle, "intent.ALIAS_TAGS");
    localBundle.putString("alias", paramString);
    if (paramSet != null) {
      paramString = new ArrayList(paramSet);
    } else {
      paramString = null;
    }
    localBundle.putStringArrayList("tags", paramString);
    localBundle.putLong("seq_id", paramLong);
    paramString = new StringBuilder();
    int j = 0;
    if (parama != null) {
      i = parama.e;
    } else {
      i = 0;
    }
    paramString.append(i);
    localBundle.putString("proto_type", paramString.toString());
    paramString = new StringBuilder();
    int i = j;
    if (parama != null) {
      i = parama.f;
    }
    paramString.append(i);
    localBundle.putString("protoaction_type", paramString.toString());
    JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
  }
  
  public static boolean a(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("startHour", paramInt1);
      localJSONObject.put("startMins", paramInt2);
      localJSONObject.put("endHour", paramInt3);
      localJSONObject.put("endtMins", paramInt4);
      a(paramContext, localJSONObject.toString());
      return true;
    }
    catch (JSONException paramContext) {}
    return false;
  }
  
  public static void b(Context paramContext)
  {
    if (paramContext == null)
    {
      g.c("ServiceInterface", "clearAllNotification - context is null!");
      return;
    }
    c.a(paramContext.getApplicationContext(), false);
  }
  
  public static void b(Context paramContext, int paramInt)
  {
    MultiSpHelper.commitInt(paramContext, "service_stoped", 0);
    Bundle localBundle = new Bundle();
    e.a(paramContext, localBundle, "intent.RESTOREPUSH");
    localBundle.putString("app", paramContext.getPackageName());
    JCoreInterface.restart(paramContext, cn.jpush.android.a.a, localBundle, true);
  }
  
  public static void c(Context paramContext, int paramInt)
  {
    if (paramContext == null)
    {
      g.c("ServiceInterface", "setNotificationNumber - context is null!");
      return;
    }
    b.a(paramContext, paramInt, false);
  }
  
  public static boolean c(Context paramContext)
  {
    return MultiSpHelper.getInt(paramContext, "service_stoped", 0) > 0;
  }
  
  public static boolean d(Context paramContext)
  {
    boolean bool = c(paramContext);
    if (bool) {
      g.a("ServiceInterface", "The service is stopped, it will give up all the actions until you call resumePush method to resume the service.");
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/ServiceInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */