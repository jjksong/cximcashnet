package cn.jpush.android.service;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.api.TagAliasCallback;
import cn.jpush.android.api.b;
import cn.jpush.android.e.g;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public final class f
{
  private static volatile f a;
  private static final Object d = new Object();
  private TagAliasReceiver b;
  private ConcurrentHashMap<Long, cn.jpush.android.api.a> c = new ConcurrentHashMap();
  private AtomicBoolean e = new AtomicBoolean(false);
  
  private cn.jpush.android.api.a a(long paramLong)
  {
    return (cn.jpush.android.api.a)this.c.get(Long.valueOf(paramLong));
  }
  
  public static f a()
  {
    if (a == null) {
      synchronized (d)
      {
        if (a == null)
        {
          f localf = new cn/jpush/android/service/f;
          localf.<init>();
          a = localf;
        }
      }
    }
    return a;
  }
  
  private static String a(cn.jpush.android.api.a parama)
  {
    if (parama == null) {
      return null;
    }
    try
    {
      if ((parama.b != null) && (parama.b.size() > 0))
      {
        parama = (String)parama.b.toArray()[0];
        return parama;
      }
    }
    catch (Throwable parama)
    {
      for (;;) {}
    }
    return null;
  }
  
  private void a(Context paramContext, int paramInt, long paramLong)
  {
    paramContext = a(paramLong);
    if (paramContext == null) {
      return;
    }
    a(paramContext, paramInt);
    b(paramLong);
  }
  
  private static void a(cn.jpush.android.api.a parama, int paramInt)
  {
    if ((parama.e == 0) && (parama.c != null)) {
      parama.c.gotResult(paramInt, parama.a, parama.b);
    }
  }
  
  private void b(long paramLong)
  {
    this.c.remove(Long.valueOf(paramLong));
  }
  
  private void b(Context paramContext)
  {
    Object localObject1 = this.c;
    if ((localObject1 != null) && (!((ConcurrentHashMap)localObject1).isEmpty()))
    {
      Object localObject2 = new ArrayList();
      Iterator localIterator = this.c.entrySet().iterator();
      while (localIterator.hasNext())
      {
        localObject1 = (Map.Entry)localIterator.next();
        if (((cn.jpush.android.api.a)((Map.Entry)localObject1).getValue()).a(20000L)) {
          ((ArrayList)localObject2).add(((Map.Entry)localObject1).getKey());
        }
      }
      localObject1 = ((ArrayList)localObject2).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Long)((Iterator)localObject1).next();
        a(paramContext, b.c, ((Long)localObject2).longValue());
      }
    }
  }
  
  private void c(Context paramContext)
  {
    try
    {
      b(paramContext);
      if ((this.e.get()) && (this.c != null))
      {
        boolean bool = this.c.isEmpty();
        if (bool)
        {
          try
          {
            if (this.b != null)
            {
              paramContext.unregisterReceiver(this.b);
              this.b = null;
            }
          }
          catch (Exception localException)
          {
            paramContext = "other exception";
            g.c("TagAliasOperator", paramContext, localException);
          }
          catch (IllegalArgumentException localIllegalArgumentException)
          {
            for (;;)
            {
              paramContext = "Receiver not registered, cannot call unregisterReceiver";
            }
          }
          this.e.set(false);
        }
      }
      return;
    }
    finally {}
  }
  
  public final JPushMessage a(Intent paramIntent)
  {
    long l = paramIntent.getLongExtra("tagalias_seqid", -1L);
    bool2 = false;
    int i = paramIntent.getIntExtra("tagalias_errorcode", 0);
    cn.jpush.android.api.a locala = a(l);
    if (locala == null) {
      return null;
    }
    a().b(l);
    bool1 = bool2;
    if (i == 0) {}
    try
    {
      if (locala.f == 5)
      {
        if (locala.e == 1)
        {
          paramIntent = paramIntent.getStringArrayListExtra("tags");
          bool1 = bool2;
          if (paramIntent != null)
          {
            HashSet localHashSet = new java/util/HashSet;
            localHashSet.<init>(paramIntent);
            locala.b = localHashSet;
            bool1 = bool2;
          }
        }
        else
        {
          bool1 = bool2;
          if (locala.e == 2)
          {
            locala.a = paramIntent.getStringExtra("alias");
            bool1 = bool2;
          }
        }
      }
      else
      {
        bool1 = bool2;
        if (locala.f == 6) {
          bool1 = paramIntent.getBooleanExtra("validated", false);
        }
      }
    }
    catch (Throwable paramIntent)
    {
      for (;;)
      {
        bool1 = bool2;
      }
    }
    paramIntent = new JPushMessage();
    paramIntent.setErrorCode(i);
    paramIntent.setSequence(locala.d);
    if (locala.e == 1)
    {
      if (locala.f == 6)
      {
        paramIntent.setCheckTag(a(locala));
        paramIntent.setTagCheckStateResult(bool1);
        paramIntent.setTagCheckOperator(true);
      }
      else
      {
        paramIntent.setTags(locala.b);
      }
    }
    else {
      paramIntent.setAlias(locala.a);
    }
    return paramIntent;
  }
  
  public final void a(Context paramContext)
  {
    IntentFilter localIntentFilter;
    label90:
    try
    {
      boolean bool = this.e.get();
      if (bool) {}
    }
    finally {}
    try
    {
      localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>();
      localIntentFilter.addCategory(cn.jpush.android.a.c);
      localIntentFilter.addAction("cn.jpush.android.intent.TAG_ALIAS_TIMEOUT");
      localIntentFilter.addAction("cn.jpush.android.intent.TAG_ALIAS_CALLBACK");
      if (this.b == null)
      {
        TagAliasReceiver localTagAliasReceiver = new cn/jpush/android/service/TagAliasReceiver;
        localTagAliasReceiver.<init>();
        this.b = localTagAliasReceiver;
      }
      paramContext.registerReceiver(this.b, localIntentFilter);
      this.e.set(true);
      return;
    }
    catch (Exception paramContext)
    {
      break label90;
    }
  }
  
  public final void a(Context paramContext, int paramInt, long paramLong, cn.jpush.android.api.a parama)
  {
    if (parama != null) {
      if (parama.e == 0)
      {
        if (parama.c != null)
        {
          parama.c.gotResult(paramInt, parama.a, parama.b);
          b(paramLong);
        }
      }
      else {
        try
        {
          Intent localIntent = new android/content/Intent;
          localIntent.<init>();
          localIntent.addCategory(cn.jpush.android.a.c);
          localIntent.setAction("cn.jpush.android.intent.RECEIVE_MESSAGE");
          localIntent.setPackage(paramContext.getPackageName());
          if (parama.e == 1) {
            localIntent.putExtra("message_type", 1);
          } else {
            localIntent.putExtra("message_type", 2);
          }
          localIntent.putExtra("tagalias_errorcode", paramInt);
          localIntent.putExtra("tagalias_seqid", paramLong);
          paramContext.sendBroadcast(localIntent);
          return;
        }
        catch (Throwable paramContext)
        {
          parama = new StringBuilder("onTagaliasTimeout error:");
          parama.append(paramContext.getMessage());
          g.c("TagAliasOperator", parama.toString());
        }
      }
    }
  }
  
  public final void a(Context paramContext, long paramLong, int paramInt, Intent paramIntent)
  {
    cn.jpush.android.api.a locala;
    if ("cn.jpush.android.intent.TAG_ALIAS_TIMEOUT".equals(paramIntent.getAction()))
    {
      a(paramContext, paramInt, paramLong);
    }
    else
    {
      locala = a(paramLong);
      if (locala != null)
      {
        a().b(paramLong);
        if (paramIntent == null) {}
      }
    }
    try
    {
      if (locala.f == 5)
      {
        if (locala.e == 1)
        {
          ArrayList localArrayList = paramIntent.getStringArrayListExtra("tags");
          if (localArrayList != null)
          {
            paramIntent = new java/util/HashSet;
            paramIntent.<init>(localArrayList);
            locala.b = paramIntent;
          }
        }
        else if (locala.e == 2)
        {
          locala.a = paramIntent.getStringExtra("alias");
        }
      }
      else if (locala.f == 6) {
        paramIntent.getBooleanExtra("validated", false);
      }
    }
    catch (Throwable paramIntent)
    {
      for (;;) {}
    }
    a(locala, paramInt);
    c(paramContext);
  }
  
  public final void a(Context paramContext, Long paramLong, cn.jpush.android.api.a parama)
  {
    b(paramContext);
    this.c.put(paramLong, parama);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */