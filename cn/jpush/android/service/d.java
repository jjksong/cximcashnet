package cn.jpush.android.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.b.e;
import cn.jpush.android.e.a;
import cn.jpush.android.e.g;

public final class d
{
  private static d a;
  
  public static d a()
  {
    if (a == null) {
      a = new d();
    }
    return a;
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    Object localObject1 = paramIntent.getStringExtra("cn.jpush.android.MSG_ID");
    if (!TextUtils.isEmpty((CharSequence)localObject1))
    {
      localObject2 = paramIntent.getStringExtra("cn.jpush.android.NOTIFICATION_TYPE");
      int j = 0;
      byte b = paramIntent.getByteExtra("platform", (byte)0);
      i = j;
      if (localObject2 != null)
      {
        i = j;
        if ("1".equals(localObject2)) {
          i = 1;
        }
      }
      if (1 != i) {
        if (b == 0) {
          e.a((String)localObject1, 1000, null, paramContext);
        } else {
          JPushInterface.reportNotificationOpened(paramContext, (String)localObject1, b);
        }
      }
    }
    int i = b(paramContext, paramIntent);
    if (i == 2) {
      return;
    }
    boolean bool = a.a(paramContext, "cn.jpush.android.intent.NOTIFICATION_OPENED", true);
    if ((i == 1) && (!bool))
    {
      g.a("PushReceiverCore", "No ACTION_NOTIFICATION_OPENED defined in manifest");
      return;
    }
    if (!bool)
    {
      g.a("PushReceiverCore", "No ACTION_NOTIFICATION_OPENED defined in manifest, open the default main activity");
      a.d(paramContext, null);
      return;
    }
    Intent localIntent = new Intent("cn.jpush.android.intent.NOTIFICATION_OPENED");
    Object localObject2 = "";
    localObject1 = localObject2;
    try
    {
      Bundle localBundle = paramIntent.getExtras();
      if (localBundle != null)
      {
        localObject1 = localObject2;
        localIntent.putExtras(localBundle);
      }
      localObject1 = localObject2;
      localObject2 = paramIntent.getStringExtra("app");
      paramIntent = (Intent)localObject2;
      localObject1 = localObject2;
      if (TextUtils.isEmpty((CharSequence)localObject2))
      {
        localObject1 = localObject2;
        paramIntent = paramContext.getPackageName();
      }
      localObject1 = paramIntent;
      localIntent.addCategory(paramIntent);
      localObject1 = paramIntent;
      localIntent.setPackage(paramContext.getPackageName());
      localObject1 = paramIntent;
      localObject2 = new java/lang/StringBuilder;
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).<init>("Send broadcast to app: ");
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).append(paramIntent);
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).append(" action=");
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).append(localIntent.getAction());
      localObject1 = paramIntent;
      g.a("PushReceiverCore", ((StringBuilder)localObject2).toString());
      localObject1 = paramIntent;
      localObject2 = new java/lang/StringBuilder;
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).<init>();
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).append(paramIntent);
      localObject1 = paramIntent;
      ((StringBuilder)localObject2).append(".permission.JPUSH_MESSAGE");
      localObject1 = paramIntent;
      paramContext.sendBroadcast(localIntent, ((StringBuilder)localObject2).toString());
      return;
    }
    catch (Throwable localThrowable)
    {
      paramIntent = new StringBuilder("onNotificationOpen sendBrocat error:");
      paramIntent.append(localThrowable.getMessage());
      g.c("PushReceiverCore", paramIntent.toString());
      paramIntent = new StringBuilder();
      paramIntent.append((String)localObject1);
      paramIntent.append(".permission.JPUSH_MESSAGE");
      a.b(paramContext, localIntent, paramIntent.toString());
    }
  }
  
  /* Error */
  private static int b(Context paramContext, Intent paramIntent)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -106
    //   3: invokevirtual 154	android/content/Intent:getSerializableExtra	(Ljava/lang/String;)Ljava/io/Serializable;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull +19 -> 27
    //   11: aload_1
    //   12: instanceof 156
    //   15: ifeq +12 -> 27
    //   18: aload_1
    //   19: checkcast 156	cn/jpush/android/data/g
    //   22: astore 4
    //   24: goto +6 -> 30
    //   27: aconst_null
    //   28: astore 4
    //   30: aload 4
    //   32: ifnull +256 -> 288
    //   35: aload 4
    //   37: getfield 160	cn/jpush/android/data/g:X	Ljava/lang/String;
    //   40: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   43: ifeq +6 -> 49
    //   46: goto +242 -> 288
    //   49: aload 4
    //   51: getfield 163	cn/jpush/android/data/g:U	Ljava/lang/String;
    //   54: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   57: ifeq +23 -> 80
    //   60: aload 4
    //   62: getfield 166	cn/jpush/android/data/g:aa	Ljava/lang/String;
    //   65: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   68: ifeq +12 -> 80
    //   71: aload 4
    //   73: aload_0
    //   74: invokevirtual 106	android/content/Context:getPackageName	()Ljava/lang/String;
    //   77: putfield 166	cn/jpush/android/data/g:aa	Ljava/lang/String;
    //   80: aload_0
    //   81: aload 4
    //   83: getfield 166	cn/jpush/android/data/g:aa	Ljava/lang/String;
    //   86: invokestatic 170	cn/jpush/android/e/a:e	(Landroid/content/Context;Ljava/lang/String;)Z
    //   89: istore_2
    //   90: iload_2
    //   91: ifeq +50 -> 141
    //   94: aload 4
    //   96: getfield 160	cn/jpush/android/data/g:X	Ljava/lang/String;
    //   99: iconst_0
    //   100: invokestatic 174	android/content/Intent:parseUri	(Ljava/lang/String;I)Landroid/content/Intent;
    //   103: astore_1
    //   104: aload_1
    //   105: ldc -81
    //   107: invokevirtual 179	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   110: pop
    //   111: goto +136 -> 247
    //   114: astore_1
    //   115: aconst_null
    //   116: astore_1
    //   117: aload_1
    //   118: astore_3
    //   119: aload 4
    //   121: getfield 181	cn/jpush/android/data/g:e	Ljava/lang/String;
    //   124: sipush 992
    //   127: aconst_null
    //   128: aload_0
    //   129: invokestatic 53	cn/jpush/android/b/e:a	(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V
    //   132: goto +115 -> 247
    //   135: astore_1
    //   136: aload_3
    //   137: astore_1
    //   138: goto +109 -> 247
    //   141: aload 4
    //   143: getfield 181	cn/jpush/android/data/g:e	Ljava/lang/String;
    //   146: sipush 988
    //   149: aconst_null
    //   150: aload_0
    //   151: invokestatic 53	cn/jpush/android/b/e:a	(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V
    //   154: aload 4
    //   156: getfield 185	cn/jpush/android/data/g:Y	I
    //   159: tableswitch	default:+21->180, 0:+78->237, 1:+24->183
    //   180: goto +65 -> 245
    //   183: aload 4
    //   185: getfield 188	cn/jpush/android/data/g:Z	Ljava/lang/String;
    //   188: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   191: istore_2
    //   192: iload_2
    //   193: ifne +52 -> 245
    //   196: aload 4
    //   198: getfield 188	cn/jpush/android/data/g:Z	Ljava/lang/String;
    //   201: iconst_0
    //   202: invokestatic 174	android/content/Intent:parseUri	(Ljava/lang/String;I)Landroid/content/Intent;
    //   205: astore_1
    //   206: aload_1
    //   207: ldc -81
    //   209: invokevirtual 179	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   212: pop
    //   213: goto +34 -> 247
    //   216: astore_1
    //   217: aconst_null
    //   218: astore_1
    //   219: aload_1
    //   220: astore_3
    //   221: aload 4
    //   223: getfield 181	cn/jpush/android/data/g:e	Ljava/lang/String;
    //   226: sipush 991
    //   229: aconst_null
    //   230: aload_0
    //   231: invokestatic 53	cn/jpush/android/b/e:a	(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V
    //   234: goto +13 -> 247
    //   237: aload_0
    //   238: invokestatic 191	cn/jpush/android/e/a:b	(Landroid/content/Context;)Landroid/content/Intent;
    //   241: astore_1
    //   242: goto +5 -> 247
    //   245: aconst_null
    //   246: astore_1
    //   247: aload_1
    //   248: ifnull +25 -> 273
    //   251: aload_0
    //   252: aload_1
    //   253: invokevirtual 195	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   256: goto +17 -> 273
    //   259: astore_1
    //   260: aload 4
    //   262: getfield 181	cn/jpush/android/data/g:e	Ljava/lang/String;
    //   265: sipush 987
    //   268: aconst_null
    //   269: aload_0
    //   270: invokestatic 53	cn/jpush/android/b/e:a	(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V
    //   273: aload 4
    //   275: getfield 163	cn/jpush/android/data/g:U	Ljava/lang/String;
    //   278: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   281: ifeq +5 -> 286
    //   284: iconst_1
    //   285: ireturn
    //   286: iconst_2
    //   287: ireturn
    //   288: iconst_0
    //   289: ireturn
    //   290: astore_1
    //   291: goto -264 -> 27
    //   294: astore_1
    //   295: goto -50 -> 245
    //   298: astore_3
    //   299: goto -182 -> 117
    //   302: astore_3
    //   303: goto -84 -> 219
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	306	0	paramContext	Context
    //   0	306	1	paramIntent	Intent
    //   89	104	2	bool	boolean
    //   118	103	3	localIntent	Intent
    //   298	1	3	localThrowable1	Throwable
    //   302	1	3	localThrowable2	Throwable
    //   22	252	4	localg	cn.jpush.android.data.g
    // Exception table:
    //   from	to	target	type
    //   94	104	114	java/lang/Throwable
    //   119	132	135	java/lang/Throwable
    //   221	234	135	java/lang/Throwable
    //   196	206	216	java/lang/Throwable
    //   251	256	259	java/lang/Throwable
    //   0	7	290	java/lang/Throwable
    //   11	24	290	java/lang/Throwable
    //   80	90	294	java/lang/Throwable
    //   141	180	294	java/lang/Throwable
    //   183	192	294	java/lang/Throwable
    //   237	242	294	java/lang/Throwable
    //   104	111	298	java/lang/Throwable
    //   206	213	302	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */