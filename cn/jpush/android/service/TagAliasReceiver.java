package cn.jpush.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import cn.jpush.android.e.g;

public class TagAliasReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null)
    {
      g.c("TagAliasReceiver", "TagAliasOperator onReceive intent is null");
      return;
    }
    long l = paramIntent.getLongExtra("tagalias_seqid", -1L);
    int i = paramIntent.getIntExtra("tagalias_errorcode", 0);
    if (l == -1L) {
      return;
    }
    f.a().a(paramContext.getApplicationContext(), l, i, paramIntent);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/TagAliasReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */