package cn.jpush.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.b.a;

public abstract class JPushMessageReceiver
  extends BroadcastReceiver
{
  public void onAliasOperatorResult(Context paramContext, JPushMessage paramJPushMessage) {}
  
  public void onCheckTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage) {}
  
  public void onMobileNumberOperatorResult(Context paramContext, JPushMessage paramJPushMessage) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    a.a().a(paramContext.getApplicationContext(), this, paramIntent);
  }
  
  public void onTagOperatorResult(Context paramContext, JPushMessage paramJPushMessage) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/JPushMessageReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */