package cn.jpush.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import cn.jiguang.ac.c;
import cn.jiguang.y.a;

public class DaemonService
  extends Service
{
  private static final String TAG = "DaemonService";
  
  public IBinder onBind(Intent paramIntent)
  {
    a locala = a.d();
    if (paramIntent != null) {
      paramIntent = paramIntent.getExtras();
    } else {
      paramIntent = null;
    }
    locala.a(this, paramIntent, 2);
    return new MyBinder();
  }
  
  public void onCreate()
  {
    super.onCreate();
    c.b("DaemonService", "action onCreate");
  }
  
  public void onDestroy()
  {
    c.b("DaemonService", "action onDestroy");
    super.onDestroy();
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    a locala = a.d();
    Bundle localBundle;
    if (paramIntent != null) {
      localBundle = paramIntent.getExtras();
    } else {
      localBundle = null;
    }
    locala.a(this, localBundle, 1);
    return super.onStartCommand(paramIntent, paramInt1, paramInt2);
  }
  
  public class MyBinder
    extends Binder
  {
    public MyBinder() {}
    
    public DaemonService getService()
    {
      return DaemonService.this;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DaemonService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */