package cn.jpush.android.service;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import cn.jiguang.ab.e;
import cn.jiguang.ac.c;

public class DataProvider
  extends ContentProvider
{
  private static final String TAG = "DataProvider";
  
  public Bundle call(String paramString1, String paramString2, Bundle paramBundle)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("method:");
      localStringBuilder.append(paramString1);
      localStringBuilder.append(", arg=");
      localStringBuilder.append(paramString2);
      c.b("DataProvider", localStringBuilder.toString());
      if ("call".equals(paramString2)) {
        return e.c(getContext(), paramString1, paramBundle);
      }
      e.b(getContext(), paramString1, paramBundle);
      paramString1 = new Bundle();
      return paramString1;
    }
    catch (Throwable paramString2)
    {
      paramString1 = new StringBuilder();
      paramString1.append("call e:");
      paramString1.append(paramString2);
      c.f("DataProvider", paramString1.toString());
    }
    return null;
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }
  
  public String getType(Uri paramUri)
  {
    return null;
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    return null;
  }
  
  public boolean onCreate()
  {
    c.b("DataProvider", "onCreate");
    return true;
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    return null;
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/service/DataProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */