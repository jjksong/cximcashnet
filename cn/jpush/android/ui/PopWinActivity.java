package cn.jpush.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.jpush.android.b.e;
import cn.jpush.android.e.k;
import cn.jpush.android.f.a.f;
import cn.jpush.android.service.d;
import java.io.File;

public class PopWinActivity
  extends Activity
{
  public static f a;
  private String b;
  private WebView c;
  private cn.jpush.android.data.b d = null;
  
  private void a()
  {
    f localf = a;
    try
    {
      k.a(this.c, "addJavascriptInterface", new Class[] { Object.class, String.class }, new Object[] { localf, "JPushWeb" });
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  public final void a(String paramString)
  {
    cn.jpush.android.data.b localb = this.d;
    if ((localb != null) && (this.c != null) && ((localb instanceof cn.jpush.android.data.g)))
    {
      if (!TextUtils.isEmpty(paramString))
      {
        ((cn.jpush.android.data.g)this.d).a = paramString;
        paramString = new Intent(this, PushActivity.class);
        paramString.putExtra("body", this.d);
        paramString.putExtra("from_way", true);
        paramString.setFlags(335544320);
        startActivity(paramString);
      }
      finish();
    }
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    e.a(this.b, 1006, null, this);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getIntent() != null) {
      try
      {
        if (getIntent().getBooleanExtra("isNotification", false))
        {
          d.a();
          d.a(getApplicationContext(), getIntent());
          finish();
          return;
        }
        Object localObject2 = getIntent();
        paramBundle = (cn.jpush.android.data.b)((Intent)localObject2).getSerializableExtra("body");
        Object localObject1;
        if (paramBundle == null)
        {
          cn.jpush.android.e.g.a("PopWinActivity", "parse entity form plugin plateform");
          if (((Intent)localObject2).getData() != null) {
            paramBundle = ((Intent)localObject2).getData().toString();
          } else {
            paramBundle = null;
          }
          localObject1 = paramBundle;
          if (TextUtils.isEmpty(paramBundle))
          {
            localObject1 = paramBundle;
            if (((Intent)localObject2).getExtras() != null) {
              localObject1 = ((Intent)localObject2).getExtras().getString("JMessageExtra");
            }
          }
          paramBundle = cn.jpush.android.d.c.a(this, (String)localObject1, "");
        }
        this.d = paramBundle;
        if (this.d != null)
        {
          this.b = this.d.e;
          int i = getResources().getIdentifier("jpush_popwin_layout", "layout", getPackageName());
          if (i == 0) {
            cn.jpush.android.e.g.d("PopWinActivity", "Please add layout resource jpush_popwin_layout.xml to res/layout !");
          }
          for (;;)
          {
            finish();
            break label401;
            setContentView(i);
            i = getResources().getIdentifier("wvPopwin", "id", getPackageName());
            if (i == 0)
            {
              cn.jpush.android.e.g.d("PopWinActivity", "Please use default code in jpush_popwin_layout.xml!");
            }
            else
            {
              this.c = ((WebView)findViewById(i));
              if (this.c != null) {
                break;
              }
              cn.jpush.android.e.g.d("PopWinActivity", "Can not get webView in layout file!");
            }
          }
          this.c.setScrollbarFadingEnabled(true);
          this.c.setScrollBarStyle(33554432);
          paramBundle = this.c.getSettings();
          paramBundle.setDomStorageEnabled(true);
          cn.jpush.android.e.a.a(paramBundle);
          cn.jpush.android.e.a.a(this.c);
          paramBundle.setSavePassword(false);
          this.c.setBackgroundColor(0);
          paramBundle = new cn/jpush/android/f/a/f;
          paramBundle.<init>(this, this.d);
          a = paramBundle;
          if (Build.VERSION.SDK_INT >= 17)
          {
            cn.jpush.android.e.g.a("PopWinActivity", "Android sdk version greater than or equal to 17, Java—Js interact by annotation!");
            a();
          }
          paramBundle = this.c;
          localObject1 = new cn/jpush/android/f/a/a;
          ((cn.jpush.android.f.a.a)localObject1).<init>("JPushWeb", cn.jpush.android.f.a.b.class, null, null);
          paramBundle.setWebChromeClient((WebChromeClient)localObject1);
          paramBundle = this.c;
          localObject1 = new cn/jpush/android/ui/c;
          ((c)localObject1).<init>(this.d, this);
          paramBundle.setWebViewClient((WebViewClient)localObject1);
          cn.jpush.android.f.a.b.setWebViewHelper(a);
          label401:
          localObject1 = (cn.jpush.android.data.g)this.d;
          paramBundle = ((cn.jpush.android.data.g)localObject1).ah;
          localObject1 = ((cn.jpush.android.data.g)localObject1).a;
          if (!TextUtils.isEmpty(paramBundle))
          {
            localObject2 = new java/io/File;
            ((File)localObject2).<init>(paramBundle.replace("file://", ""));
            if (((File)localObject2).exists())
            {
              this.c.loadUrl(paramBundle);
              break label472;
            }
          }
          this.c.loadUrl((String)localObject1);
          label472:
          e.a(this.b, 1000, null, this);
          return;
        }
        cn.jpush.android.e.g.c("PopWinActivity", "Warning，null message entity! Close PopWinActivity!");
        finish();
        return;
      }
      catch (Exception paramBundle)
      {
        cn.jpush.android.e.g.d("PopWinActivity", "Extra data is not serializable!");
        paramBundle.printStackTrace();
        finish();
        return;
      }
    }
    cn.jpush.android.e.g.c("PopWinActivity", "PopWinActivity get NULL intent!");
    finish();
  }
  
  protected void onDestroy()
  {
    WebView localWebView = this.c;
    if (localWebView != null)
    {
      localWebView.removeAllViews();
      this.c.destroy();
      this.c = null;
    }
    super.onDestroy();
  }
  
  protected void onPause()
  {
    super.onPause();
    if ((this.c != null) && (Build.VERSION.SDK_INT >= 11)) {
      this.c.onPause();
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    if (this.c != null)
    {
      if (Build.VERSION.SDK_INT >= 11) {
        this.c.onResume();
      }
      cn.jpush.android.f.a.b.setWebViewHelper(a);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/ui/PopWinActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */