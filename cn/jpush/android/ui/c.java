package cn.jpush.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.jpush.android.data.b;
import java.util.Locale;

public final class c
  extends WebViewClient
{
  private final b a;
  private final Context b;
  private boolean c = false;
  
  public c(b paramb, Context paramContext)
  {
    this.a = paramb;
    this.b = paramContext;
  }
  
  public final void onLoadResource(WebView paramWebView, String paramString)
  {
    super.onLoadResource(paramWebView, paramString);
  }
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
  }
  
  public final void onReceivedSslError(WebView paramWebView, SslErrorHandler paramSslErrorHandler, SslError paramSslError)
  {
    if (this.c)
    {
      paramSslErrorHandler.proceed();
      return;
    }
    paramWebView = this.b;
    if ((paramWebView != null) && (!paramWebView.getClass().isAssignableFrom(Activity.class))) {
      try
      {
        paramWebView = new android/app/AlertDialog$Builder;
        paramWebView.<init>(this.b);
        paramWebView.setTitle("提示");
        paramWebView.setMessage("SSL 证书异常，是否继续加载？");
        paramSslError = new cn/jpush/android/ui/d;
        paramSslError.<init>(this, paramSslErrorHandler);
        paramWebView.setNegativeButton("否", paramSslError);
        paramSslError = new cn/jpush/android/ui/e;
        paramSslError.<init>(this, paramSslErrorHandler);
        paramWebView.setPositiveButton("是", paramSslError);
        paramWebView.setCancelable(false);
        paramWebView.create().show();
        return;
      }
      catch (Throwable paramWebView)
      {
        paramSslErrorHandler.cancel();
        return;
      }
    }
    paramSslErrorHandler.cancel();
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    Context localContext = paramWebView.getContext();
    try
    {
      paramWebView.getSettings().setSavePassword(false);
      cn.jpush.android.e.a.a(paramWebView);
      Object localObject1 = String.format(Locale.ENGLISH, "{\"url\":\"%s\"}", new Object[] { paramString });
      if (this.a.H)
      {
        paramWebView = new android/content/Intent;
        paramWebView.<init>("android.intent.action.VIEW", Uri.parse(paramString));
        localContext.startActivity(paramWebView);
        cn.jpush.android.b.e.a(this.a.e, 1016, (String)localObject1, cn.jpush.android.a.e);
        return true;
      }
      if (paramString.endsWith(".mp3"))
      {
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>("android.intent.action.VIEW");
        ((Intent)localObject1).setDataAndType(Uri.parse(paramString), "audio/*");
        paramWebView.getContext().startActivity((Intent)localObject1);
        return true;
      }
      if ((!paramString.endsWith(".mp4")) && (!paramString.endsWith(".3gp")))
      {
        if (paramString.endsWith(".apk"))
        {
          localObject1 = new android/content/Intent;
          ((Intent)localObject1).<init>("android.intent.action.VIEW", Uri.parse(paramString));
          paramWebView.getContext().startActivity((Intent)localObject1);
          return true;
        }
        if (paramString.startsWith("http"))
        {
          cn.jpush.android.b.e.a(this.a.e, 1016, (String)localObject1, cn.jpush.android.a.e);
        }
        else if (paramString.startsWith("mailto"))
        {
          paramWebView = paramString;
          if (paramString.lastIndexOf("direct=") < 0)
          {
            paramWebView = paramString;
            if (!paramString.startsWith("mailto"))
            {
              if (paramString.indexOf("?") > 0)
              {
                paramWebView = new java/lang/StringBuilder;
                paramWebView.<init>();
                paramWebView.append(paramString);
                paramWebView.append("&direct=false");
              }
              else
              {
                paramWebView = new java/lang/StringBuilder;
                paramWebView.<init>();
                paramWebView.append(paramString);
                paramWebView.append("?direct=false");
              }
              paramWebView = paramWebView.toString();
              paramWebView.lastIndexOf("direct=");
            }
          }
          int i = paramWebView.indexOf("?");
          Object localObject2 = paramWebView.substring(0, i);
          String str = paramWebView.substring(i);
          paramString = null;
          paramWebView = paramString;
          if (((String)localObject2).startsWith("mailto"))
          {
            localObject2 = ((String)localObject2).split(":");
            paramWebView = paramString;
            if (localObject2.length == 2)
            {
              i = str.indexOf("title=");
              int j = str.indexOf("&content=");
              paramString = str.substring(i + 6, j);
              str = str.substring(j + 9);
              localObject2 = localObject2[1];
              paramWebView = new android/content/Intent;
              paramWebView.<init>("android.intent.action.SEND");
              paramWebView.setType("plain/text");
              paramWebView.putExtra("android.intent.extra.EMAIL", new String[] { localObject2 });
              paramWebView.putExtra("android.intent.extra.SUBJECT", paramString);
              paramWebView.putExtra("android.intent.extra.TEXT", str);
            }
          }
          if (paramWebView != null) {
            localContext.startActivity(paramWebView);
          }
          cn.jpush.android.b.e.a(this.a.e, 1016, (String)localObject1, cn.jpush.android.a.e);
          return true;
        }
        return false;
      }
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("android.intent.action.VIEW");
      ((Intent)localObject1).setDataAndType(Uri.parse(paramString), "video/*");
      paramWebView.getContext().startActivity((Intent)localObject1);
    }
    catch (Exception paramWebView)
    {
      for (;;) {}
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/ui/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */