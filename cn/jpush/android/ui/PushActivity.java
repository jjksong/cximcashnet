package cn.jpush.android.ui;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import cn.jpush.android.b.e;
import cn.jpush.android.data.b;
import cn.jpush.android.e.a;
import java.util.List;

public class PushActivity
  extends Activity
{
  private int a = 0;
  private boolean b = false;
  private String c;
  private FullScreenView d = null;
  private Handler e;
  
  private void c()
  {
    if (getIntent() != null) {
      try
      {
        this.b = getIntent().getBooleanExtra("from_way", false);
        Intent localIntent = getIntent();
        Object localObject1 = (b)localIntent.getSerializableExtra("body");
        Object localObject2;
        if (localObject1 == null)
        {
          cn.jpush.android.e.g.a("PushActivity", "parse entity form plugin plateform");
          localObject1 = null;
          if (localIntent.getData() != null) {
            localObject1 = localIntent.getData().toString();
          }
          localObject2 = localObject1;
          if (TextUtils.isEmpty((CharSequence)localObject1))
          {
            localObject2 = localObject1;
            if (localIntent.getExtras() != null) {
              localObject2 = localIntent.getExtras().getString("JMessageExtra");
            }
          }
          localObject1 = cn.jpush.android.d.c.a(this, (String)localObject2, "");
        }
        if (localObject1 != null)
        {
          this.c = ((b)localObject1).e;
          if (localObject1 != null) {
            if (((b)localObject1).s != 0) {
              cn.jpush.android.api.c.a(this, (b)localObject1, 0);
            }
          }
        }
        for (;;)
        {
          finish();
          break;
          localObject2 = new android/os/Message;
          ((Message)localObject2).<init>();
          ((Message)localObject2).what = 1;
          ((Message)localObject2).obj = localObject1;
          this.e.sendMessageDelayed((Message)localObject2, 500L);
          break;
          cn.jpush.android.e.g.c("PushActivity", "Null message entity! Close PushActivity!");
          finish();
          return;
          cn.jpush.android.e.g.c("PushActivity", "Warning，null message entity! Close PushActivity!");
        }
        return;
      }
      catch (Exception localException)
      {
        cn.jpush.android.e.g.d("PushActivity", "Extra data is not serializable!");
        localException.printStackTrace();
        finish();
        return;
      }
    }
    cn.jpush.android.e.g.c("PushActivity", "PushActivity get NULL intent!");
    finish();
  }
  
  public final void a()
  {
    runOnUiThread(new f(this));
  }
  
  public final void b()
  {
    finish();
    if (1 == this.a) {
      try
      {
        Object localObject = (ActivityManager)getSystemService("activity");
        ComponentName localComponentName = ((ActivityManager.RunningTaskInfo)((ActivityManager)localObject).getRunningTasks(1).get(0)).baseActivity;
        localObject = ((ActivityManager.RunningTaskInfo)((ActivityManager)localObject).getRunningTasks(1).get(0)).topActivity;
        if ((localComponentName != null) && (localObject != null) && (((ComponentName)localObject).toString().equals(localComponentName.toString()))) {
          a.d(this, null);
        }
        return;
      }
      catch (Exception localException)
      {
        cn.jpush.android.e.g.c("PushActivity", "Get running tasks failed.");
        a.d(this, null);
      }
    }
  }
  
  public void onBackPressed()
  {
    FullScreenView localFullScreenView = this.d;
    if ((localFullScreenView != null) && (localFullScreenView.webviewCanGoBack()))
    {
      this.d.webviewGoBack();
      return;
    }
    e.a(this.c, 1006, null, this);
    b();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.e = new g(this);
    c();
  }
  
  protected void onDestroy()
  {
    FullScreenView localFullScreenView = this.d;
    if (localFullScreenView != null) {
      localFullScreenView.destory();
    }
    if (this.e.hasMessages(2)) {
      this.e.removeMessages(2);
    }
    super.onDestroy();
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    setIntent(paramIntent);
    c();
  }
  
  protected void onPause()
  {
    super.onPause();
    FullScreenView localFullScreenView = this.d;
    if (localFullScreenView != null) {
      localFullScreenView.pause();
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    FullScreenView localFullScreenView = this.d;
    if (localFullScreenView != null) {
      localFullScreenView.resume();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/ui/PushActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */