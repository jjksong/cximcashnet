package cn.jpush.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.jpush.android.e.k;
import cn.jpush.android.f.a.f;

public class FullScreenView
  extends LinearLayout
{
  private static final String TAG = "FullScreenView";
  public static f webViewHelper;
  private View.OnClickListener btnBackClickListener = new a(this);
  private ImageButton imgBtnBack;
  private final Context mContext;
  private WebView mWebView;
  private ProgressBar pushPrograssBar;
  private RelativeLayout rlTitleBar;
  private TextView tvTitle;
  
  public FullScreenView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
  }
  
  private void quitFullScreen()
  {
    try
    {
      WindowManager.LayoutParams localLayoutParams = ((Activity)this.mContext).getWindow().getAttributes();
      localLayoutParams.flags &= 0xFBFF;
      ((Activity)this.mContext).getWindow().setAttributes(localLayoutParams);
      ((Activity)this.mContext).getWindow().clearFlags(512);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void reflectAddJsInterface()
  {
    f localf = webViewHelper;
    try
    {
      k.a(this.mWebView, "addJavascriptInterface", new Class[] { Object.class, String.class }, new Object[] { localf, "JPushWeb" });
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  public void destory()
  {
    removeAllViews();
    WebView localWebView = this.mWebView;
    if (localWebView != null)
    {
      localWebView.removeAllViews();
      this.mWebView.clearSslPreferences();
      this.mWebView.destroy();
      this.mWebView = null;
    }
  }
  
  public void initModule(Context paramContext, cn.jpush.android.data.b paramb)
  {
    Object localObject = (cn.jpush.android.data.g)paramb;
    String str = ((cn.jpush.android.data.g)localObject).b;
    setFocusable(true);
    this.mWebView = ((WebView)findViewById(getResources().getIdentifier("fullWebView", "id", paramContext.getPackageName())));
    this.rlTitleBar = ((RelativeLayout)findViewById(getResources().getIdentifier("rlRichpushTitleBar", "id", paramContext.getPackageName())));
    this.tvTitle = ((TextView)findViewById(getResources().getIdentifier("tvRichpushTitle", "id", paramContext.getPackageName())));
    this.imgBtnBack = ((ImageButton)findViewById(getResources().getIdentifier("imgRichpushBtnBack", "id", paramContext.getPackageName())));
    this.pushPrograssBar = ((ProgressBar)findViewById(getResources().getIdentifier("pushPrograssBar", "id", paramContext.getPackageName())));
    if ((this.mWebView == null) || (this.rlTitleBar == null) || (this.tvTitle == null) || (this.imgBtnBack == null))
    {
      cn.jpush.android.e.g.d("FullScreenView", "Please use default code in jpush_webview_layout.xml!");
      ((Activity)this.mContext).finish();
    }
    if (1 == ((cn.jpush.android.data.g)localObject).ac)
    {
      this.rlTitleBar.setVisibility(8);
      ((Activity)paramContext).getWindow().setFlags(1024, 1024);
    }
    else
    {
      this.tvTitle.setText(str);
      this.imgBtnBack.setOnClickListener(this.btnBackClickListener);
    }
    this.mWebView.setScrollbarFadingEnabled(true);
    this.mWebView.setScrollBarStyle(33554432);
    localObject = this.mWebView.getSettings();
    cn.jpush.android.e.a.a((WebSettings)localObject);
    cn.jpush.android.e.a.a(this.mWebView);
    ((WebSettings)localObject).setSavePassword(false);
    webViewHelper = new f(paramContext, paramb);
    if (Build.VERSION.SDK_INT >= 17)
    {
      cn.jpush.android.e.g.a("FullScreenView", "Android sdk version greater than or equal to 17, Java—Js interact by annotation!");
      reflectAddJsInterface();
    }
    this.mWebView.setWebChromeClient(new cn.jpush.android.f.a.a("JPushWeb", cn.jpush.android.f.a.b.class, this.pushPrograssBar, this.tvTitle));
    this.mWebView.setWebViewClient(new c(paramb, paramContext));
    cn.jpush.android.f.a.b.setWebViewHelper(webViewHelper);
  }
  
  public void loadUrl(String paramString)
  {
    WebView localWebView = this.mWebView;
    if (localWebView != null) {
      localWebView.loadUrl(paramString);
    }
  }
  
  public void pause()
  {
    if ((this.mWebView != null) && (Build.VERSION.SDK_INT >= 11)) {
      this.mWebView.onPause();
    }
  }
  
  public void resume()
  {
    if (this.mWebView != null)
    {
      if (Build.VERSION.SDK_INT >= 11) {
        this.mWebView.onResume();
      }
      cn.jpush.android.f.a.b.setWebViewHelper(webViewHelper);
    }
  }
  
  public void showTitleBar()
  {
    Object localObject = this.rlTitleBar;
    if ((localObject != null) && (((RelativeLayout)localObject).getVisibility() == 8))
    {
      this.rlTitleBar.setVisibility(0);
      quitFullScreen();
      this.imgBtnBack.setOnClickListener(this.btnBackClickListener);
      localObject = this.mWebView;
      if (localObject != null) {
        ((WebView)localObject).postDelayed(new b(this), 1000L);
      }
    }
  }
  
  public boolean webviewCanGoBack()
  {
    WebView localWebView = this.mWebView;
    if (localWebView != null) {
      return localWebView.canGoBack();
    }
    return false;
  }
  
  public void webviewGoBack()
  {
    WebView localWebView = this.mWebView;
    if (localWebView != null) {
      localWebView.goBack();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/ui/FullScreenView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */