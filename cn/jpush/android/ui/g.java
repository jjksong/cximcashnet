package cn.jpush.android.ui;

import android.os.Handler;
import android.os.Message;
import cn.jpush.android.data.b;
import java.lang.ref.WeakReference;

final class g
  extends Handler
{
  private final WeakReference<PushActivity> a;
  
  public g(PushActivity paramPushActivity)
  {
    this.a = new WeakReference(paramPushActivity);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    b localb = (b)paramMessage.obj;
    PushActivity localPushActivity = (PushActivity)this.a.get();
    if (localPushActivity == null) {
      return;
    }
    switch (paramMessage.what)
    {
    default: 
      break;
    case 2: 
      localPushActivity.b();
      break;
    case 1: 
      localPushActivity.setRequestedOrientation(1);
      PushActivity.a(localPushActivity, localb);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/ui/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */