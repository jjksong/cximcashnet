package cn.jpush.android.d;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.SdkType;
import cn.jpush.android.a;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.JThirdPlatFormInterface;
import cn.jpush.android.e.g;
import cn.jpush.android.service.PushReceiver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class d
{
  private static Map<String, Byte> b = new HashMap();
  private static d d;
  private static final Object e = new Object();
  private List<JThirdPlatFormInterface> a = new ArrayList();
  private boolean c;
  
  static
  {
    b.put("cn.jpush.android.thirdpush.meizu.MeizuPushManager", Byte.valueOf((byte)3));
    b.put("cn.jpush.android.thirdpush.xiaomi.XMPushManager", Byte.valueOf((byte)1));
    b.put("cn.jpush.android.thirdpush.huawei.HWPushManager", Byte.valueOf((byte)2));
    b.put("cn.jpush.android.thirdpush.fcm.FCMPushManager", Byte.valueOf((byte)8));
    b.put("cn.jpush.android.thridpush.oppo.OPushManager", Byte.valueOf((byte)4));
    b.put("cn.jpush.android.thirdpush.vivo.VivoPushManager", Byte.valueOf((byte)5));
  }
  
  public static d a()
  {
    if (d == null) {
      synchronized (e)
      {
        if (d == null)
        {
          d locald = new cn/jpush/android/d/d;
          locald.<init>();
          d = locald;
        }
      }
    }
    return d;
  }
  
  private e a(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      e locale = new e(this);
      locale.c = paramBundle.getString("data");
      locale.a = paramBundle.getString("msg_id");
      locale.b = paramBundle.getInt("noti_id", 0);
      locale.d = paramBundle.getByte("platform", (byte)-1).byteValue();
      paramBundle = locale;
    }
    else
    {
      paramBundle = null;
    }
    return paramBundle;
  }
  
  private void a(Context paramContext, byte paramByte, String paramString)
  {
    Context localContext = paramContext;
    if (paramContext == null) {
      localContext = a.e;
    }
    if (localContext == null) {
      return;
    }
    g(localContext);
    paramContext = this.a.iterator();
    while (paramContext.hasNext())
    {
      JThirdPlatFormInterface localJThirdPlatFormInterface = (JThirdPlatFormInterface)paramContext.next();
      if (localJThirdPlatFormInterface.getRomType(localContext) == paramByte)
      {
        a(localContext, localJThirdPlatFormInterface);
        if (a(localContext, paramByte, paramString)) {
          b(localContext, paramByte, paramString);
        }
      }
    }
  }
  
  private static void a(Context paramContext, JThirdPlatFormInterface paramJThirdPlatFormInterface)
  {
    if ((paramJThirdPlatFormInterface != null) && (paramJThirdPlatFormInterface.isNeedClearToken(paramContext)))
    {
      cn.jpush.android.b.b(paramContext, paramJThirdPlatFormInterface.getRomType(paramContext), false);
      cn.jpush.android.b.a(paramContext, paramJThirdPlatFormInterface.getRomType(paramContext), null);
    }
  }
  
  private static boolean a(Context paramContext, int paramInt, String paramString)
  {
    if (!cn.jpush.android.b.b(paramContext, paramInt)) {
      return true;
    }
    return !TextUtils.equals(cn.jpush.android.b.a(paramContext, paramInt), paramString);
  }
  
  private static void b(Context paramContext, byte paramByte, String paramString)
  {
    cn.jpush.android.b.b(paramContext, paramByte, false);
    cn.jpush.android.b.a(paramContext, paramByte, paramString);
    Bundle localBundle = new Bundle();
    cn.jpush.android.service.e.a(paramContext, localBundle, "intent.plugin.platform.REQUEST_REGID");
    localBundle.putString("plugin.platform.regid ", paramString);
    localBundle.putByte("plugin.platform.type", paramByte);
    JCoreInterface.sendAction(paramContext, a.a, localBundle);
  }
  
  private void g(Context paramContext)
  {
    try
    {
      boolean bool = this.c;
      if (bool) {
        return;
      }
      if (paramContext == null) {
        return;
      }
      Iterator localIterator = b.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject1 = (Map.Entry)localIterator.next();
        try
        {
          Object localObject2 = paramContext.getClassLoader().loadClass((String)((Map.Entry)localObject1).getKey());
          if (localObject2 == null) {
            continue;
          }
          localObject2 = ((Class)localObject2).newInstance();
          if ((localObject2 == null) || (!(localObject2 instanceof JThirdPlatFormInterface))) {
            continue;
          }
          ((JThirdPlatFormInterface)localObject2).init(paramContext);
          if (((JThirdPlatFormInterface)localObject2).isSupport(paramContext))
          {
            this.a.add((JThirdPlatFormInterface)localObject2);
            continue;
          }
          localObject2 = (Byte)((Map.Entry)localObject1).getValue();
          cn.jpush.android.b.b(paramContext, ((Byte)localObject2).byteValue(), false);
          cn.jpush.android.b.a(paramContext, ((Byte)localObject2).byteValue(), null);
        }
        catch (Throwable localThrowable)
        {
          localObject1 = (Byte)((Map.Entry)localObject1).getValue();
          cn.jpush.android.b.b(paramContext, ((Byte)localObject1).byteValue(), false);
          cn.jpush.android.b.a(paramContext, ((Byte)localObject1).byteValue(), null);
        }
        if (((localThrowable instanceof RuntimeException)) && (localThrowable.getMessage().contains("Please check")) && (JCoreInterface.getDebugMode()))
        {
          paramContext = new java/lang/RuntimeException;
          paramContext.<init>(localThrowable);
          throw paramContext;
        }
      }
      this.c = true;
      return;
    }
    finally {}
  }
  
  public final void a(Context paramContext)
  {
    g(paramContext);
    if (!JPushInterface.isPushStopped(paramContext.getApplicationContext()))
    {
      Iterator localIterator = this.a.iterator();
      while (localIterator.hasNext())
      {
        JThirdPlatFormInterface localJThirdPlatFormInterface = (JThirdPlatFormInterface)localIterator.next();
        try
        {
          localJThirdPlatFormInterface.register(paramContext);
        }
        catch (Throwable localThrowable)
        {
          g.c("ThirdPushManager", "Third push register failed#", localThrowable);
        }
      }
    }
  }
  
  public final void a(Context paramContext, Bundle paramBundle)
  {
    if (paramBundle != null) {}
    try
    {
      e locale = a(paramBundle);
      cn.jpush.android.data.b localb = c.a(paramContext, locale.c, locale.a);
      localb.k = true;
      localb.h = true;
      localb.g = locale.d;
      cn.jpush.android.api.c.a(paramContext, localb);
      a(paramContext, "action_notification_arrived", paramBundle);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  public final void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    StringBuilder localStringBuilder = new StringBuilder("doAction,action:");
    localStringBuilder.append(paramString);
    localStringBuilder.append(",bundle:");
    localStringBuilder.append(paramBundle);
    g.a("ThirdPushManager", localStringBuilder.toString());
    if (!TextUtils.isEmpty(paramString))
    {
      if (paramString.equals("action_notification_arrived"))
      {
        paramString = a(paramBundle);
        if (paramString != null) {
          c.a(paramContext, paramString.c, paramString.a, paramString.b, paramString.d, false);
        }
        return;
      }
      if (paramString.equals("action_notification_clicked"))
      {
        paramString = a(paramBundle);
        if (paramString != null) {
          c.a(paramContext, paramString.c, paramString.a, paramString.b, paramString.d, true);
        }
        return;
      }
      if (paramString.equals("action_notification_show"))
      {
        cn.jpush.android.service.e.a(paramContext, paramBundle, "action_notification_show");
        JCoreInterface.sendAction(paramContext, a.a, paramBundle);
        return;
      }
      if ((paramString.equals("action_register_token")) && (paramBundle != null))
      {
        paramString = paramBundle.getString("token");
        a(paramContext, paramBundle.getByte("platform", (byte)-1).byteValue(), paramString);
      }
    }
  }
  
  public final void b(Context paramContext)
  {
    g(paramContext);
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      ((JThirdPlatFormInterface)localIterator.next()).resumePush(paramContext);
    }
  }
  
  public final void b(Context paramContext, Bundle paramBundle)
  {
    g(paramContext);
    byte b1 = paramBundle.getByte("platform", (byte)-1).byteValue();
    if (b1 <= 0) {
      return;
    }
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      paramBundle = (JThirdPlatFormInterface)localIterator.next();
      if (paramBundle.getRomType(paramContext) == b1) {
        a(paramContext, b1, paramBundle.getToken(paramContext));
      }
    }
  }
  
  public final void c(Context paramContext)
  {
    g(paramContext);
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      ((JThirdPlatFormInterface)localIterator.next()).stopPush(paramContext);
    }
  }
  
  public final byte d(Context paramContext)
  {
    g(paramContext);
    Iterator localIterator = this.a.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      JThirdPlatFormInterface localJThirdPlatFormInterface = (JThirdPlatFormInterface)localIterator.next();
      byte b1 = localJThirdPlatFormInterface.getRomType(paramContext);
      i = (byte)(i | b1);
      String str = cn.jpush.android.b.a(paramContext, b1);
      boolean bool = cn.jpush.android.b.b(paramContext, b1);
      int j;
      if (localJThirdPlatFormInterface.getRomType(paramContext) == 8)
      {
        j = (byte)(i | 0x8);
        i = j;
        if (bool)
        {
          i = j;
          if (TextUtils.isEmpty(str)) {
            break;
          }
        }
      }
      else
      {
        int k;
        for (b1 = j | 0x20;; k = j | 0x80)
        {
          i = (byte)b1;
          break;
          j = i;
          if (localJThirdPlatFormInterface.getRomType(paramContext) == 2) {
            j = (byte)(i | 0x40);
          }
          if (bool)
          {
            i = j;
            if (!TextUtils.isEmpty(str)) {
              break;
            }
          }
        }
      }
    }
    return i;
  }
  
  public final String e(Context paramContext)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      JThirdPlatFormInterface localJThirdPlatFormInterface = (JThirdPlatFormInterface)localIterator.next();
      if (localJThirdPlatFormInterface.getRomType(paramContext) != 8) {
        return cn.jpush.android.b.a(paramContext, localJThirdPlatFormInterface.getRomType(paramContext));
      }
    }
    return null;
  }
  
  public final void f(Context paramContext)
  {
    Context localContext = paramContext;
    if (paramContext == null) {
      localContext = a.e;
    }
    if (localContext == null) {
      return;
    }
    g(localContext);
    paramContext = this.a.iterator();
    while (paramContext.hasNext())
    {
      Object localObject1 = (JThirdPlatFormInterface)paramContext.next();
      if (((JThirdPlatFormInterface)localObject1).needSendToMainProcess())
      {
        Object localObject2 = ((JThirdPlatFormInterface)localObject1).getToken(localContext);
        if (!TextUtils.isEmpty((CharSequence)localObject2)) {
          a(localContext, ((JThirdPlatFormInterface)localObject1).getRomType(localContext), (String)localObject2);
        } else {
          try
          {
            localObject2 = new android/content/Intent;
            ((Intent)localObject2).<init>(localContext, PushReceiver.class);
            ((Intent)localObject2).setAction("intent.plugin.platform.REFRESSH_REGID");
            Bundle localBundle = new android/os/Bundle;
            localBundle.<init>();
            localBundle.putString("sdktype", SdkType.JPUSH.name());
            localBundle.putByte("platform", ((JThirdPlatFormInterface)localObject1).getRomType(localContext));
            ((Intent)localObject2).putExtras(localBundle);
            ((Intent)localObject2).setPackage(localContext.getPackageName());
            localContext.sendBroadcast((Intent)localObject2);
          }
          catch (Throwable localThrowable)
          {
            localObject1 = new StringBuilder("send ACTION_PLUGIN_PALTFORM_REFRESSH_REGID failed:");
            ((StringBuilder)localObject1).append(localThrowable);
            g.c("ThirdPushManager", ((StringBuilder)localObject1).toString());
          }
        }
      }
      else
      {
        a(localContext, (JThirdPlatFormInterface)localObject1);
        if (((JThirdPlatFormInterface)localObject1).getRomType(localContext) == 2)
        {
          ((JThirdPlatFormInterface)localObject1).getToken(localContext);
        }
        else
        {
          String str = ((JThirdPlatFormInterface)localObject1).getToken(localContext);
          if (a(localContext, ((JThirdPlatFormInterface)localObject1).getRomType(localContext), str)) {
            b(localContext, ((JThirdPlatFormInterface)localObject1).getRomType(localContext), str);
          }
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/d/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */