package cn.jpush.android.d;

final class b
{
  public byte a;
  public String b;
  public long c;
  public byte[] d;
  public int e;
  
  public b(a parama, byte paramByte, String paramString, long paramLong, byte[] paramArrayOfByte)
  {
    this.a = paramByte;
    this.b = paramString;
    this.c = paramLong;
    this.d = paramArrayOfByte;
    this.e = 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("PluginPlatformRegIDBean{pluginPlatformType=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", regid='");
    localStringBuilder.append(this.b);
    localStringBuilder.append('\'');
    localStringBuilder.append(", rid=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", retryCount=");
    localStringBuilder.append(this.e);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/d/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */