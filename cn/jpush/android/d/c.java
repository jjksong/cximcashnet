package cn.jpush.android.d;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.jpush.android.b.e;
import cn.jpush.android.data.b;
import org.json.JSONObject;

public final class c
{
  public static b a(Context paramContext, String paramString1, String paramString2)
  {
    cn.jpush.android.data.g localg = new cn.jpush.android.data.g();
    byte b = 0;
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString1);
      localg.e = localJSONObject.optString("_jmsgid_");
      if (localg.e.isEmpty()) {
        localg.e = localJSONObject.optString("msg_id");
      }
      localg.g = ((byte)localJSONObject.optInt("rom_type"));
      int i = localJSONObject.optInt("show_type", -1);
      paramString1 = localJSONObject.optJSONObject("m_content");
      if (paramString1 != null)
      {
        localg.x = paramString1.optString("n_content");
        localg.w = paramString1.optString("n_title");
        localg.p = paramString1.optString("n_extras");
        localg.v = paramString1.optInt("n_flag", 1);
        paramString1 = paramString1.optJSONObject("rich_content");
        if (paramString1 != null)
        {
          localg.a(true);
          localg.a(paramString1);
          localg.d = 3;
        }
        else
        {
          localg.d = 4;
          localg.ac = -1;
        }
      }
      else
      {
        localg.x = localJSONObject.optString("n_content");
        localg.w = localJSONObject.optString("n_title");
        localg.p = localJSONObject.optString("n_extras");
        localg.g = ((byte)localJSONObject.optInt("rom_type"));
      }
      if (i != -1) {
        localg.d = i;
      }
      localg.s = 0;
      localg.t = true;
      return localg;
    }
    catch (Throwable paramString1)
    {
      paramString1 = "NO MSGID";
      if (!TextUtils.isEmpty(localg.e))
      {
        paramString1 = localg.e;
        b = localg.g;
      }
      e.a(paramString1, paramString2, b, 996, paramContext);
    }
    return null;
  }
  
  private static void a(Context paramContext, b paramb, String paramString, int paramInt)
  {
    if (!TextUtils.isEmpty(paramb.e))
    {
      Intent localIntent = new Intent("cn.jpush.android.intent.NOTIFICATION_OPENED");
      try
      {
        cn.jpush.android.api.c.a(localIntent, cn.jpush.android.api.c.a(paramb), paramInt);
        localIntent.putExtra("sdktype", cn.jpush.android.a.a);
        String str;
        if (TextUtils.isEmpty(paramb.q)) {
          str = paramContext.getPackageName();
        } else {
          str = paramb.q;
        }
        localIntent.addCategory(str);
        localIntent.setPackage(paramContext.getPackageName());
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str);
        localStringBuilder.append(".permission.JPUSH_MESSAGE");
        paramContext.sendBroadcast(localIntent, localStringBuilder.toString());
        e.a(paramb.e, paramString, paramb.g, 1000, paramContext);
        return;
      }
      catch (Throwable paramb)
      {
        paramString = new StringBuilder("onNotificationOpen sendBrocat error:");
        paramString.append(paramb.getMessage());
        cn.jpush.android.e.g.c("PluginPlatformsNotificationHelper", paramString.toString());
        paramb = new StringBuilder();
        paramb.append(paramContext.getPackageName());
        paramb.append(".permission.JPUSH_MESSAGE");
        cn.jpush.android.e.a.b(paramContext, localIntent, paramb.toString());
      }
    }
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, int paramInt, byte paramByte, boolean paramBoolean)
  {
    if (paramContext == null) {
      paramContext = "context was null";
    }
    for (;;)
    {
      cn.jpush.android.e.g.c("PluginPlatformsNotificationHelper", paramContext);
      return;
      if (TextUtils.isEmpty(paramString1))
      {
        paramContext = "content was null";
      }
      else
      {
        paramString1 = a(paramContext, paramString1, paramString2);
        if (paramString1 == null)
        {
          paramContext = "entity was null";
        }
        else
        {
          if (!TextUtils.isEmpty(paramString1.e)) {
            break;
          }
          paramContext = "message id was empty";
        }
      }
    }
    paramString1.g = paramByte;
    if (paramBoolean)
    {
      if ((paramString1 instanceof cn.jpush.android.data.g))
      {
        if (((cn.jpush.android.data.g)paramString1).ac == -1)
        {
          a(paramContext, paramString1, paramString2, paramInt);
          return;
        }
        paramString1 = cn.jpush.android.api.c.c(paramContext, paramString1);
        if (paramString1 != null)
        {
          paramString1.addFlags(268435456);
          paramContext.getApplicationContext().startActivity(paramString1);
        }
      }
    }
    else if ((paramString1 instanceof cn.jpush.android.data.g))
    {
      cn.jpush.android.api.c.a(paramContext, cn.jpush.android.api.c.a(paramString1), paramInt, null, paramContext.getPackageName(), paramString1);
      e.a(paramString1.e, paramString2, paramString1.g, 1018, paramContext);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/d/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */