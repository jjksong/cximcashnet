package cn.jpush.android.d;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.utils.OutputDataUtil;
import cn.jpush.android.e.g;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class a
{
  private static volatile a b;
  private Map<Byte, b> a = new HashMap();
  
  public static a a()
  {
    if (b == null) {
      try
      {
        if (b == null)
        {
          a locala = new cn/jpush/android/d/a;
          locala.<init>();
          b = locala;
        }
      }
      finally {}
    }
    return b;
  }
  
  private b a(long paramLong)
  {
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (((b)localEntry.getValue()).c == paramLong) {
        return (b)localEntry.getValue();
      }
    }
    return null;
  }
  
  private void a(Context paramContext, b paramb)
  {
    try
    {
      JCoreInterface.sendRequestData(paramContext, cn.jpush.android.a.a, 10000, paramb.d);
      return;
    }
    finally
    {
      paramContext = finally;
      throw paramContext;
    }
  }
  
  public final void a(Context paramContext, long paramLong)
  {
    b localb = a(paramLong);
    StringBuilder localStringBuilder = new StringBuilder("onUpdateRidSuccess rid:");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ,pluginPlatformRegIDBean:");
    localStringBuilder.append(String.valueOf(localb));
    g.b("PluginPlatformRidUpdate", localStringBuilder.toString());
    if (localb != null)
    {
      cn.jpush.android.b.a(paramContext, localb.a, localb.b);
      cn.jpush.android.b.b(paramContext, localb.a, true);
      this.a.remove(Byte.valueOf(localb.a));
    }
  }
  
  public final void a(Context paramContext, long paramLong, int paramInt)
  {
    b localb = a(paramLong);
    StringBuilder localStringBuilder = new StringBuilder("onUpdateRidFailed rid:");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(",errorCode:");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" ,pluginPlatformRegIDBean:");
    localStringBuilder.append(String.valueOf(localb));
    g.b("PluginPlatformRidUpdate", localStringBuilder.toString());
    if (localb != null)
    {
      if (localb.e < 3)
      {
        localb.e += 1;
        a(paramContext, localb);
        return;
      }
      this.a.remove(Byte.valueOf(localb.a));
    }
  }
  
  public final void a(Context paramContext, Bundle paramBundle)
  {
    try
    {
      byte b1 = paramBundle.getByte("plugin.platform.type", (byte)0).byteValue();
      if (b1 != 0)
      {
        boolean bool = JCoreInterface.isTcpConnected();
        if (!bool) {
          return;
        }
        String str = paramBundle.getString("plugin.platform.regid ");
        if (this.a.containsKey(Byte.valueOf(b1)))
        {
          bool = TextUtils.equals(((b)this.a.get(Byte.valueOf(b1))).b, str);
          if (bool) {
            return;
          }
        }
        long l2 = JCoreInterface.getNextRid();
        long l1 = JCoreInterface.getUid();
        int i = JCoreInterface.getSid();
        Object localObject = new cn/jiguang/api/utils/OutputDataUtil;
        ((OutputDataUtil)localObject).<init>(20480);
        ((OutputDataUtil)localObject).writeU16(0);
        ((OutputDataUtil)localObject).writeU8(1);
        ((OutputDataUtil)localObject).writeU8(27);
        ((OutputDataUtil)localObject).writeU64(l2);
        ((OutputDataUtil)localObject).writeU32(i);
        ((OutputDataUtil)localObject).writeU64(l1);
        if (TextUtils.isEmpty(str)) {
          paramBundle = new byte[0];
        } else {
          paramBundle = cn.jpush.a.a.a.a(str);
        }
        ((OutputDataUtil)localObject).writeByteArrayincludeLength(paramBundle);
        ((OutputDataUtil)localObject).writeU8(b1);
        ((OutputDataUtil)localObject).writeU16At(((OutputDataUtil)localObject).current(), 0);
        paramBundle = ((OutputDataUtil)localObject).toByteArray();
        localObject = new cn/jpush/android/d/b;
        ((b)localObject).<init>(this, b1, str, l2, paramBundle);
        this.a.put(Byte.valueOf(b1), localObject);
        a(paramContext, (b)localObject);
      }
      return;
    }
    finally {}
  }
  
  public final void b(Context paramContext, long paramLong)
  {
    b localb = a(paramLong);
    StringBuilder localStringBuilder = new StringBuilder("onUpdateRidTimeout rid:");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ,pluginPlatformRegIDBean:");
    localStringBuilder.append(String.valueOf(localb));
    g.b("PluginPlatformRidUpdate", localStringBuilder.toString());
    if (localb != null)
    {
      if (localb.e < 3)
      {
        localb.e += 1;
        a(paramContext, localb);
        return;
      }
      this.a.remove(Byte.valueOf(localb.a));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/d/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */