package cn.jpush.android.data;

import android.text.TextUtils;
import java.io.Serializable;

public final class c
  implements Serializable
{
  public String a;
  public String b;
  
  public c() {}
  
  public c(b paramb)
  {
    this.a = paramb.e;
    this.b = paramb.f;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof c)) {
      return false;
    }
    paramObject = (c)paramObject;
    if ((!TextUtils.isEmpty(this.a)) && (!TextUtils.isEmpty(((c)paramObject).a)))
    {
      if (!TextUtils.equals(this.a, ((c)paramObject).a)) {
        return false;
      }
      if ((TextUtils.isEmpty(this.b)) && (TextUtils.isEmpty(((c)paramObject).b))) {
        return true;
      }
      if ((!TextUtils.isEmpty(this.b)) && (!TextUtils.isEmpty(((c)paramObject).b)) && (TextUtils.equals(this.b, ((c)paramObject).b))) {
        return true;
      }
    }
    return false;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("msg_id = ");
    localStringBuilder.append(this.a);
    localStringBuilder.append(",  override_msg_id = ");
    localStringBuilder.append(this.b);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */