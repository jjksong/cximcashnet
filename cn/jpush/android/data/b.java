package cn.jpush.android.data;

import android.content.Context;
import android.text.TextUtils;
import cn.jpush.android.b.e;
import cn.jpush.android.b.m;
import cn.jpush.android.e.c;
import cn.jpush.android.e.g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public abstract class b
  implements Serializable
{
  public String A;
  public String B;
  public int C;
  public String D;
  public boolean E = false;
  public boolean F = false;
  public boolean G = false;
  public boolean H = false;
  public int I = -1;
  public ArrayList<String> J = null;
  public String K;
  public long L;
  public String M;
  public boolean N;
  public boolean O;
  public long P;
  public double Q = -1.0D;
  public double R = -1.0D;
  public String S;
  public String T;
  public String U;
  public String V;
  public String W;
  public String X;
  public int Y;
  public String Z;
  private boolean a = false;
  public String aa;
  public int ab;
  public int d;
  public String e;
  public String f;
  public byte g = 0;
  public boolean h;
  public int i;
  public int j = 0;
  public boolean k;
  public String l;
  public String m;
  public int n = -1;
  public String o;
  public String p;
  public String q;
  public String r;
  public int s;
  public boolean t;
  public List<String> u = null;
  public int v;
  public String w;
  public String x;
  public int y;
  public String z;
  
  static boolean a(ArrayList<String> paramArrayList, Context paramContext, String paramString1, String paramString2, boolean paramBoolean)
  {
    boolean bool4 = m.a(paramString1);
    boolean bool3 = true;
    boolean bool1 = true;
    boolean bool2 = bool3;
    if (bool4)
    {
      bool2 = bool3;
      if (paramContext != null)
      {
        bool2 = bool3;
        if (paramArrayList.size() > 0)
        {
          bool2 = bool3;
          if (!TextUtils.isEmpty(paramString2))
          {
            Iterator localIterator = paramArrayList.iterator();
            for (;;)
            {
              bool2 = bool1;
              if (!localIterator.hasNext()) {
                break;
              }
              paramArrayList = (String)localIterator.next();
              Object localObject;
              if ((paramArrayList != null) && (!paramArrayList.startsWith("http://")))
              {
                localObject = new StringBuilder();
                ((StringBuilder)localObject).append(paramString1);
                ((StringBuilder)localObject).append(paramArrayList);
                localObject = ((StringBuilder)localObject).toString();
              }
              else
              {
                localObject = paramArrayList;
              }
              byte[] arrayOfByte = cn.jpush.android.c.a.a((String)localObject, 5, 5000L, 4);
              if (arrayOfByte != null)
              {
                localObject = paramArrayList;
                try
                {
                  if (paramArrayList.startsWith("http://")) {
                    localObject = c.a(paramArrayList);
                  }
                  if (!paramBoolean)
                  {
                    paramArrayList = new java/lang/StringBuilder;
                    paramArrayList.<init>();
                    paramArrayList.append(c.a(paramContext, paramString2));
                    paramArrayList.append((String)localObject);
                  }
                  for (;;)
                  {
                    paramArrayList = paramArrayList.toString();
                    break;
                    paramArrayList = new java/lang/StringBuilder;
                    paramArrayList.<init>();
                    paramArrayList.append(c.b(paramContext, paramString2));
                    paramArrayList.append((String)localObject);
                  }
                  c.a(paramArrayList, arrayOfByte);
                }
                catch (Exception paramArrayList)
                {
                  g.c("Entity", "Write storage error,  create img file fail.", paramArrayList);
                  break label268;
                }
              }
              else
              {
                e.a(paramString2, 1020, cn.jpush.android.e.a.a(paramContext, (String)localObject), paramContext);
                label268:
                bool1 = false;
              }
            }
          }
        }
      }
    }
    return bool2;
  }
  
  public abstract void a(Context paramContext);
  
  public final void a(boolean paramBoolean)
  {
    this.a = true;
  }
  
  public final boolean a()
  {
    return this.a;
  }
  
  public final boolean a(Context paramContext, JSONObject paramJSONObject)
  {
    boolean bool;
    if (paramJSONObject.optInt("full_screen", 0) > 0) {
      bool = true;
    } else {
      bool = false;
    }
    this.t = bool;
    this.v = paramJSONObject.optInt("n_flag", 0);
    this.w = paramJSONObject.optString("n_title", "");
    this.x = paramJSONObject.optString("n_content", "");
    this.y = paramJSONObject.optInt("n_style", 0);
    this.z = paramJSONObject.optString("n_big_text", "");
    this.A = paramJSONObject.optString("n_big_pic_path", "");
    this.B = paramJSONObject.optString("n_inbox", "");
    this.p = paramJSONObject.optString("n_extras", "");
    this.C = paramJSONObject.optInt("n_priority", 0);
    this.D = paramJSONObject.optString("n_category", "");
    this.n = paramJSONObject.optInt("n_alert_type", -1);
    this.V = paramJSONObject.optString("n_small_icon", "");
    this.W = paramJSONObject.optString("n_large_icon", "");
    this.U = paramJSONObject.optString("n_source", "");
    JSONObject localJSONObject = paramJSONObject.optJSONObject("n_intent");
    if (localJSONObject != null)
    {
      this.X = localJSONObject.optString("n_url", "");
      this.Y = localJSONObject.optInt("n_fail_handle_type", 0);
      this.Z = localJSONObject.optString("n_fail_handle_url", "");
      this.aa = localJSONObject.optString("n_package_name", "");
      this.ab = localJSONObject.optInt("n_builder_id", 0);
    }
    if (TextUtils.isEmpty(this.w))
    {
      if (!this.k)
      {
        e.a(this.e, 996, null, paramContext);
        return false;
      }
      this.w = cn.jpush.android.a.d;
    }
    paramContext = m.a(paramContext, this.e, paramJSONObject, "ad_content");
    if (paramContext == null) {
      return (this.k) && (this.h);
    }
    if ((this.k) && (this.h)) {
      this.a = true;
    }
    return a(paramContext);
  }
  
  protected abstract boolean a(JSONObject paramJSONObject);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */