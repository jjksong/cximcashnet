package cn.jpush.android.data;

import android.text.TextUtils;
import cn.jpush.android.e.g;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class JPushLocalNotification
  implements Serializable
{
  private int a = 1;
  private String b = "";
  private String c = "00";
  private String d = "00";
  private long e = 0L;
  private String f;
  private String g;
  private String h;
  private long i;
  private long j = 1L;
  private int k = 1;
  private String l = "";
  private String m = "";
  
  private static void a(String paramString1, String paramString2, JSONObject paramJSONObject)
  {
    if (!TextUtils.isEmpty(paramString2)) {
      paramJSONObject.put(paramString1, paramString2);
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (JPushLocalNotification)paramObject;
      if (this.j == ((JPushLocalNotification)paramObject).j) {
        return true;
      }
    }
    return false;
  }
  
  public long getBroadcastTime()
  {
    return this.e;
  }
  
  public long getBuilderId()
  {
    return this.i;
  }
  
  public String getContent()
  {
    return this.f;
  }
  
  public String getExtras()
  {
    return this.h;
  }
  
  public long getNotificationId()
  {
    return this.j;
  }
  
  public String getTitle()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    long l1 = this.j;
    return (int)(l1 ^ l1 >>> 32);
  }
  
  public void setBroadcastTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    if ((paramInt1 >= 0) && (paramInt2 > 0) && (paramInt2 <= 12) && (paramInt3 > 0) && (paramInt3 <= 31) && (paramInt4 >= 0) && (paramInt4 <= 23) && (paramInt5 >= 0) && (paramInt5 <= 59) && (paramInt6 >= 0) && (paramInt6 <= 59))
    {
      Object localObject = Calendar.getInstance();
      ((Calendar)localObject).set(paramInt1, paramInt2 - 1, paramInt3, paramInt4, paramInt5, paramInt6);
      localObject = ((Calendar)localObject).getTime();
      long l1 = System.currentTimeMillis();
      if (((Date)localObject).getTime() < l1)
      {
        this.e = l1;
        return;
      }
      this.e = ((Date)localObject).getTime();
      return;
    }
    g.d("JPushLocalNotification", "Set time fail! Please check your args!");
  }
  
  public void setBroadcastTime(long paramLong)
  {
    this.e = paramLong;
  }
  
  public void setBroadcastTime(Date paramDate)
  {
    this.e = paramDate.getTime();
  }
  
  public void setBuilderId(long paramLong)
  {
    this.i = paramLong;
  }
  
  public void setContent(String paramString)
  {
    this.f = paramString;
  }
  
  public void setExtras(String paramString)
  {
    this.h = paramString;
  }
  
  public void setNotificationId(long paramLong)
  {
    this.j = ((int)paramLong);
  }
  
  public void setTitle(String paramString)
  {
    this.g = paramString;
  }
  
  public String toJSON()
  {
    JSONObject localJSONObject1 = new JSONObject();
    try
    {
      Object localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      if (!TextUtils.isEmpty(this.h))
      {
        JSONObject localJSONObject2 = new org/json/JSONObject;
        localJSONObject2.<init>(this.h);
        ((JSONObject)localObject).put("n_extras", localJSONObject2);
      }
      a("n_content", this.f, (JSONObject)localObject);
      a("n_title", this.g, (JSONObject)localObject);
      a("n_content", this.f, (JSONObject)localObject);
      ((JSONObject)localObject).put("ad_t", 0);
      localJSONObject1.put("m_content", localObject);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(this.j);
      a("msg_id", ((StringBuilder)localObject).toString(), localJSONObject1);
      a("content_type", this.m, localJSONObject1);
      a("override_msg_id", this.l, localJSONObject1);
      localJSONObject1.put("n_only", this.k);
      localJSONObject1.put("n_builder_id", this.i);
      localJSONObject1.put("show_type", 3);
      localJSONObject1.put("notificaion_type", 1);
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject1.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/JPushLocalNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */