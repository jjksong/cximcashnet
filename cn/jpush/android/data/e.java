package cn.jpush.android.data;

public final class e
{
  private long a = 0L;
  private int b = 0;
  private int c = 0;
  private int d = 0;
  private String e = "";
  private long f = 0L;
  private long g = 0L;
  
  public final long a()
  {
    return this.a;
  }
  
  public final void a(int paramInt)
  {
    this.b = paramInt;
  }
  
  public final void a(long paramLong)
  {
    this.a = paramLong;
  }
  
  public final void a(String paramString)
  {
    this.e = paramString;
  }
  
  public final int b()
  {
    return this.b;
  }
  
  public final void b(int paramInt)
  {
    this.c = paramInt;
  }
  
  public final void b(long paramLong)
  {
    this.g = paramLong;
  }
  
  public final int c()
  {
    return this.c;
  }
  
  public final void c(int paramInt)
  {
    this.d = paramInt;
  }
  
  public final void c(long paramLong)
  {
    this.f = paramLong;
  }
  
  public final String d()
  {
    return this.e;
  }
  
  public final long e()
  {
    return this.g;
  }
  
  public final long f()
  {
    return this.f;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("LocalNotificationDBData [ln_id=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", ln_count=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", ln_remove=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", ln_type=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", ln_extra=");
    localStringBuilder.append(this.e);
    localStringBuilder.append(", ln_trigger_time=");
    localStringBuilder.append(this.f);
    localStringBuilder.append(", ln_add_time=");
    localStringBuilder.append(this.g);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */