package cn.jpush.android.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class d
  extends f
{
  private static final String[] a = { "_id", "ln_id", "ln_count", "ln_remove", "ln_type", "ln_extra", "ln_trigger_time", "ln_add_time" };
  private static volatile d b;
  private static final Object c = new Object();
  
  private d(Context paramContext)
  {
    super(paramContext, "jpush_local_notification.db", null, 1);
  }
  
  public static d a(Context paramContext)
  {
    if (b == null) {
      synchronized (c)
      {
        if (b == null)
        {
          d locald = new cn/jpush/android/data/d;
          locald.<init>(paramContext.getApplicationContext());
          b = locald;
        }
      }
    }
    return b;
  }
  
  public static e a(Cursor paramCursor)
  {
    if ((paramCursor != null) && (paramCursor.getCount() != 0)) {
      try
      {
        e locale = new cn/jpush/android/data/e;
        locale.<init>();
        locale.a(paramCursor.getLong(1));
        locale.a(paramCursor.getInt(2));
        locale.b(paramCursor.getInt(3));
        locale.c(paramCursor.getInt(4));
        locale.a(paramCursor.getString(5));
        locale.c(paramCursor.getLong(6));
        locale.b(paramCursor.getLong(7));
        return locale;
      }
      catch (Exception paramCursor)
      {
        paramCursor.getStackTrace();
      }
    }
    return null;
  }
  
  /* Error */
  public final int a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: invokevirtual 106	cn/jpush/android/data/d:a	(Z)Z
    //   5: ifeq +77 -> 82
    //   8: new 108	java/lang/StringBuilder
    //   11: astore 4
    //   13: aload 4
    //   15: ldc 110
    //   17: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   20: aload 4
    //   22: lload_1
    //   23: invokevirtual 116	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   26: pop
    //   27: aload 4
    //   29: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   32: astore 4
    //   34: aload_0
    //   35: invokevirtual 124	cn/jpush/android/data/d:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   38: ldc 126
    //   40: aload 4
    //   42: aconst_null
    //   43: invokevirtual 132	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   46: istore_3
    //   47: aload_0
    //   48: iconst_1
    //   49: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   52: iload_3
    //   53: ireturn
    //   54: astore 4
    //   56: goto +18 -> 74
    //   59: astore 4
    //   61: aload 4
    //   63: invokevirtual 138	java/lang/Exception:printStackTrace	()V
    //   66: aload_0
    //   67: iconst_1
    //   68: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   71: goto +11 -> 82
    //   74: aload_0
    //   75: iconst_1
    //   76: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   79: aload 4
    //   81: athrow
    //   82: iconst_0
    //   83: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	d
    //   0	84	1	paramLong	long
    //   46	7	3	i	int
    //   11	30	4	localObject1	Object
    //   54	1	4	localObject2	Object
    //   59	21	4	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   8	47	54	finally
    //   61	66	54	finally
    //   8	47	59	java/lang/Exception
  }
  
  /* Error */
  public final long a(long paramLong1, int paramInt1, int paramInt2, int paramInt3, String paramString, long paramLong2, long paramLong3)
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: invokevirtual 106	cn/jpush/android/data/d:a	(Z)Z
    //   5: ifeq +139 -> 144
    //   8: new 141	android/content/ContentValues
    //   11: astore 11
    //   13: aload 11
    //   15: invokespecial 142	android/content/ContentValues:<init>	()V
    //   18: aload 11
    //   20: ldc 18
    //   22: lload_1
    //   23: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   26: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   29: aload 11
    //   31: ldc 20
    //   33: iconst_1
    //   34: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   37: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   40: aload 11
    //   42: ldc 22
    //   44: iconst_0
    //   45: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   48: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   51: aload 11
    //   53: ldc 24
    //   55: iconst_0
    //   56: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   59: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   62: aload 11
    //   64: ldc 26
    //   66: aload 6
    //   68: invokevirtual 163	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   71: aload 11
    //   73: ldc 28
    //   75: lload 7
    //   77: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   80: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   83: aload 11
    //   85: ldc 30
    //   87: lload 9
    //   89: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   92: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   95: aload_0
    //   96: invokevirtual 124	cn/jpush/android/data/d:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   99: ldc 126
    //   101: aconst_null
    //   102: aload 11
    //   104: invokevirtual 167	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   107: lstore_1
    //   108: aload_0
    //   109: iconst_1
    //   110: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   113: goto +33 -> 146
    //   116: astore 6
    //   118: goto +18 -> 136
    //   121: astore 6
    //   123: aload 6
    //   125: invokevirtual 138	java/lang/Exception:printStackTrace	()V
    //   128: aload_0
    //   129: iconst_1
    //   130: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   133: goto +11 -> 144
    //   136: aload_0
    //   137: iconst_1
    //   138: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   141: aload 6
    //   143: athrow
    //   144: lconst_0
    //   145: lstore_1
    //   146: lload_1
    //   147: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	148	0	this	d
    //   0	148	1	paramLong1	long
    //   0	148	3	paramInt1	int
    //   0	148	4	paramInt2	int
    //   0	148	5	paramInt3	int
    //   0	148	6	paramString	String
    //   0	148	7	paramLong2	long
    //   0	148	9	paramLong3	long
    //   11	92	11	localContentValues	android.content.ContentValues
    // Exception table:
    //   from	to	target	type
    //   8	108	116	finally
    //   123	128	116	finally
    //   8	108	121	java/lang/Exception
  }
  
  public final Cursor a(int paramInt, long paramLong)
  {
    Object localObject = new StringBuilder("ln_count=");
    ((StringBuilder)localObject).append(1);
    ((StringBuilder)localObject).append(" and ln_trigger_time");
    ((StringBuilder)localObject).append("<");
    ((StringBuilder)localObject).append(paramLong);
    localObject = ((StringBuilder)localObject).toString();
    try
    {
      localObject = getReadableDatabase().query(true, "t_localnotification", a, (String)localObject, null, null, null, null, null);
      return (Cursor)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final Cursor a(long paramLong1, long paramLong2)
  {
    Object localObject = new StringBuilder("ln_count>0 and ln_trigger_time<");
    ((StringBuilder)localObject).append(300000L + paramLong1);
    ((StringBuilder)localObject).append(" and ln_trigger_time");
    ((StringBuilder)localObject).append(">");
    ((StringBuilder)localObject).append(paramLong1);
    localObject = ((StringBuilder)localObject).toString();
    try
    {
      localObject = getReadableDatabase().query(true, "t_localnotification", a, (String)localObject, null, null, null, null, null);
      return (Cursor)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final e a(long paramLong, int paramInt)
  {
    if (a(false))
    {
      Cursor localCursor2 = null;
      Cursor localCursor1 = localCursor2;
      try
      {
        Object localObject2 = new java/lang/StringBuilder;
        localCursor1 = localCursor2;
        ((StringBuilder)localObject2).<init>("ln_id=");
        localCursor1 = localCursor2;
        ((StringBuilder)localObject2).append(paramLong);
        localCursor1 = localCursor2;
        ((StringBuilder)localObject2).append(" and ln_type");
        localCursor1 = localCursor2;
        ((StringBuilder)localObject2).append("=0");
        localCursor1 = localCursor2;
        localObject2 = ((StringBuilder)localObject2).toString();
        localCursor1 = localCursor2;
        localCursor2 = getReadableDatabase().query(true, "t_localnotification", a, (String)localObject2, null, null, null, null, null);
        if (localCursor2 != null)
        {
          localCursor1 = localCursor2;
          localCursor2.moveToFirst();
        }
        localCursor1 = localCursor2;
        localObject2 = a(localCursor2);
        return (e)localObject2;
      }
      finally
      {
        if (localCursor1 != null) {
          localCursor1.close();
        }
        b(false);
      }
    }
    throw new Exception("open database failed");
  }
  
  /* Error */
  public final int[] a()
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: invokevirtual 106	cn/jpush/android/data/d:a	(Z)Z
    //   5: istore_2
    //   6: aconst_null
    //   7: astore 6
    //   9: aconst_null
    //   10: astore_3
    //   11: aconst_null
    //   12: astore 4
    //   14: aconst_null
    //   15: astore 5
    //   17: iload_2
    //   18: ifeq +297 -> 315
    //   21: aload_0
    //   22: invokevirtual 183	cn/jpush/android/data/d:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   25: iconst_1
    //   26: ldc 126
    //   28: iconst_1
    //   29: anewarray 14	java/lang/String
    //   32: dup
    //   33: iconst_0
    //   34: ldc 18
    //   36: aastore
    //   37: ldc -42
    //   39: aconst_null
    //   40: aconst_null
    //   41: aconst_null
    //   42: aconst_null
    //   43: aconst_null
    //   44: invokevirtual 187	android/database/sqlite/SQLiteDatabase:query	(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   47: astore 4
    //   49: aload 5
    //   51: astore_3
    //   52: aload 4
    //   54: ifnull +78 -> 132
    //   57: aload 5
    //   59: astore_3
    //   60: aload 6
    //   62: astore 5
    //   64: aload 4
    //   66: invokeinterface 66 1 0
    //   71: ifle +61 -> 132
    //   74: aload 6
    //   76: astore 5
    //   78: aload 4
    //   80: invokeinterface 66 1 0
    //   85: newarray <illegal type>
    //   87: astore_3
    //   88: aload 4
    //   90: invokeinterface 203 1 0
    //   95: pop
    //   96: iconst_0
    //   97: istore_1
    //   98: aload_3
    //   99: iload_1
    //   100: aload 4
    //   102: iconst_0
    //   103: invokeinterface 80 2 0
    //   108: iastore
    //   109: iinc 1 1
    //   112: aload 4
    //   114: invokeinterface 217 1 0
    //   119: istore_2
    //   120: iload_2
    //   121: ifne -23 -> 98
    //   124: goto +8 -> 132
    //   127: astore 5
    //   129: goto +95 -> 224
    //   132: aload_3
    //   133: astore 5
    //   135: aload_0
    //   136: iconst_1
    //   137: invokevirtual 106	cn/jpush/android/data/d:a	(Z)Z
    //   140: istore_2
    //   141: iload_2
    //   142: ifeq +30 -> 172
    //   145: aload_0
    //   146: invokevirtual 124	cn/jpush/android/data/d:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   149: ldc -37
    //   151: invokevirtual 222	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   154: iconst_1
    //   155: istore_1
    //   156: goto +18 -> 174
    //   159: astore_3
    //   160: iconst_1
    //   161: istore_1
    //   162: goto +125 -> 287
    //   165: astore 5
    //   167: iconst_1
    //   168: istore_1
    //   169: goto +80 -> 249
    //   172: iconst_0
    //   173: istore_1
    //   174: aload 4
    //   176: ifnull +10 -> 186
    //   179: aload 4
    //   181: invokeinterface 208 1 0
    //   186: aload_0
    //   187: iconst_0
    //   188: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   191: aload_3
    //   192: astore 4
    //   194: iload_1
    //   195: ifeq +120 -> 315
    //   198: aload_0
    //   199: iconst_1
    //   200: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   203: aload_3
    //   204: astore 4
    //   206: goto +109 -> 315
    //   209: astore_3
    //   210: goto +21 -> 231
    //   213: astore_3
    //   214: aload 5
    //   216: astore 6
    //   218: aload_3
    //   219: astore 5
    //   221: aload 6
    //   223: astore_3
    //   224: goto +23 -> 247
    //   227: astore_3
    //   228: aconst_null
    //   229: astore 4
    //   231: iconst_0
    //   232: istore_1
    //   233: goto +54 -> 287
    //   236: astore 5
    //   238: aconst_null
    //   239: astore 6
    //   241: aload_3
    //   242: astore 4
    //   244: aload 6
    //   246: astore_3
    //   247: iconst_0
    //   248: istore_1
    //   249: aload 5
    //   251: invokevirtual 138	java/lang/Exception:printStackTrace	()V
    //   254: aload 4
    //   256: ifnull +10 -> 266
    //   259: aload 4
    //   261: invokeinterface 208 1 0
    //   266: aload_0
    //   267: iconst_0
    //   268: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   271: iload_1
    //   272: ifeq +8 -> 280
    //   275: aload_0
    //   276: iconst_1
    //   277: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   280: aload_3
    //   281: astore 4
    //   283: goto +32 -> 315
    //   286: astore_3
    //   287: aload 4
    //   289: ifnull +10 -> 299
    //   292: aload 4
    //   294: invokeinterface 208 1 0
    //   299: aload_0
    //   300: iconst_0
    //   301: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   304: iload_1
    //   305: ifeq +8 -> 313
    //   308: aload_0
    //   309: iconst_1
    //   310: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   313: aload_3
    //   314: athrow
    //   315: aload 4
    //   317: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	318	0	this	d
    //   97	208	1	i	int
    //   5	137	2	bool	boolean
    //   10	123	3	localObject1	Object
    //   159	45	3	localObject2	Object
    //   209	1	3	localObject3	Object
    //   213	6	3	localException1	Exception
    //   223	1	3	localObject4	Object
    //   227	15	3	localObject5	Object
    //   246	35	3	localObject6	Object
    //   286	28	3	localObject7	Object
    //   12	304	4	localObject8	Object
    //   15	62	5	localObject9	Object
    //   127	1	5	localException2	Exception
    //   133	1	5	localObject10	Object
    //   165	50	5	localException3	Exception
    //   219	1	5	localException4	Exception
    //   236	14	5	localException5	Exception
    //   7	238	6	localObject11	Object
    // Exception table:
    //   from	to	target	type
    //   88	96	127	java/lang/Exception
    //   98	109	127	java/lang/Exception
    //   112	120	127	java/lang/Exception
    //   145	154	159	finally
    //   145	154	165	java/lang/Exception
    //   64	74	209	finally
    //   78	88	209	finally
    //   88	96	209	finally
    //   98	109	209	finally
    //   112	120	209	finally
    //   135	141	209	finally
    //   64	74	213	java/lang/Exception
    //   78	88	213	java/lang/Exception
    //   135	141	213	java/lang/Exception
    //   21	49	227	finally
    //   21	49	236	java/lang/Exception
    //   249	254	286	finally
  }
  
  /* Error */
  public final long b(long paramLong1, int paramInt1, int paramInt2, int paramInt3, String paramString, long paramLong2, long paramLong3)
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: invokevirtual 106	cn/jpush/android/data/d:a	(Z)Z
    //   5: ifeq +171 -> 176
    //   8: new 108	java/lang/StringBuilder
    //   11: astore 11
    //   13: aload 11
    //   15: ldc 110
    //   17: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   20: aload 11
    //   22: lload_1
    //   23: invokevirtual 116	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   26: pop
    //   27: aload 11
    //   29: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   32: astore 11
    //   34: new 141	android/content/ContentValues
    //   37: astore 12
    //   39: aload 12
    //   41: invokespecial 142	android/content/ContentValues:<init>	()V
    //   44: aload 12
    //   46: ldc 18
    //   48: lload_1
    //   49: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   52: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   55: aload 12
    //   57: ldc 20
    //   59: iload_3
    //   60: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   63: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   66: aload 12
    //   68: ldc 22
    //   70: iload 4
    //   72: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   75: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   78: aload 12
    //   80: ldc 24
    //   82: iconst_0
    //   83: invokestatic 157	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   86: invokevirtual 160	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   89: aload 12
    //   91: ldc 26
    //   93: aload 6
    //   95: invokevirtual 163	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   98: aload 12
    //   100: ldc 28
    //   102: lload 7
    //   104: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   107: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   110: aload 12
    //   112: ldc 30
    //   114: lload 9
    //   116: invokestatic 148	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   119: invokevirtual 152	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   122: aload_0
    //   123: invokevirtual 124	cn/jpush/android/data/d:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   126: ldc 126
    //   128: aload 12
    //   130: aload 11
    //   132: aconst_null
    //   133: invokevirtual 226	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   136: istore_3
    //   137: iload_3
    //   138: i2l
    //   139: lstore_1
    //   140: aload_0
    //   141: iconst_1
    //   142: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   145: goto +33 -> 178
    //   148: astore 6
    //   150: goto +18 -> 168
    //   153: astore 6
    //   155: aload 6
    //   157: invokevirtual 138	java/lang/Exception:printStackTrace	()V
    //   160: aload_0
    //   161: iconst_1
    //   162: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   165: goto +11 -> 176
    //   168: aload_0
    //   169: iconst_1
    //   170: invokevirtual 135	cn/jpush/android/data/d:b	(Z)V
    //   173: aload 6
    //   175: athrow
    //   176: lconst_0
    //   177: lstore_1
    //   178: lload_1
    //   179: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	180	0	this	d
    //   0	180	1	paramLong1	long
    //   0	180	3	paramInt1	int
    //   0	180	4	paramInt2	int
    //   0	180	5	paramInt3	int
    //   0	180	6	paramString	String
    //   0	180	7	paramLong2	long
    //   0	180	9	paramLong3	long
    //   11	120	11	localObject	Object
    //   37	92	12	localContentValues	android.content.ContentValues
    // Exception table:
    //   from	to	target	type
    //   8	137	148	finally
    //   155	160	148	finally
    //   8	137	153	java/lang/Exception
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    try
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE t_localnotification (_id INTEGER PRIMARY KEY AUTOINCREMENT ,ln_id long not null,ln_count integer not null,ln_remove integer not null,ln_type integer not null,ln_extra text ,ln_trigger_time long ,ln_add_time long );");
      return;
    }
    catch (Exception paramSQLiteDatabase)
    {
      for (;;) {}
    }
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS t_localnotification");
    onCreate(paramSQLiteDatabase);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */