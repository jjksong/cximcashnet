package cn.jpush.android.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import java.io.File;

public abstract class f
  extends SQLiteOpenHelper
{
  private volatile int a = 0;
  private volatile int b = 0;
  private volatile SQLiteDatabase c;
  private volatile SQLiteDatabase d;
  private final Object e = new Object();
  private final Object f = new Object();
  private final Context g;
  private final String h;
  private final int i;
  private final SQLiteDatabase.CursorFactory j;
  
  public f(Context paramContext, String paramString, SQLiteDatabase.CursorFactory paramCursorFactory, int paramInt)
  {
    super(paramContext, paramString, null, 1);
    this.g = paramContext;
    this.h = paramString;
    this.i = 1;
    this.j = null;
  }
  
  public final boolean a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    try
    {
      synchronized (this.e)
      {
        getWritableDatabase();
        this.b += 1;
        return true;
      }
      synchronized (this.f)
      {
        getReadableDatabase();
        this.a += 1;
        return true;
      }
      return false;
    }
    catch (Exception localException) {}
  }
  
  public final void b(boolean paramBoolean)
  {
    int n = 1;
    int m = 1;
    if (paramBoolean)
    {
      localObject3 = this.e;
      k = m;
      try
      {
        if (this.d != null)
        {
          k = m;
          if (this.d.isOpen())
          {
            k = this.b - 1;
            this.b = k;
            if (k <= 0) {
              k = m;
            } else {
              k = 0;
            }
          }
        }
        if (k != 0)
        {
          this.b = 0;
          if (this.d != null) {
            this.d.close();
          }
          this.d = null;
        }
        return;
      }
      finally {}
    }
    Object localObject3 = this.f;
    int k = n;
    try
    {
      if (this.c != null)
      {
        k = n;
        if (this.c.isOpen())
        {
          k = this.a - 1;
          this.a = k;
          if (k <= 0) {
            k = n;
          } else {
            k = 0;
          }
        }
      }
      if (k != 0)
      {
        this.a = 0;
        if (this.c != null) {
          this.c.close();
        }
        this.c = null;
      }
      return;
    }
    finally {}
  }
  
  @Deprecated
  public void close() {}
  
  public SQLiteDatabase getReadableDatabase()
  {
    if ((this.c == null) || (!this.c.isOpen())) {
      synchronized (this.f)
      {
        if (this.c != null)
        {
          boolean bool = this.c.isOpen();
          if (bool) {
            break label105;
          }
        }
      }
    }
    try
    {
      getWritableDatabase();
      String str = this.g.getDatabasePath(this.h).getPath();
      this.c = SQLiteDatabase.openDatabase(str, this.j, 1);
      if (this.c.getVersion() == this.i)
      {
        this.a = 0;
        onOpen(this.c);
        label105:
        return this.c;
      }
      SQLiteException localSQLiteException2 = new android/database/sqlite/SQLiteException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Can't upgrade read-only database from version ");
      localStringBuilder.append(this.c.getVersion());
      localStringBuilder.append(" to ");
      localStringBuilder.append(this.i);
      localStringBuilder.append(": ");
      localStringBuilder.append(str);
      localSQLiteException2.<init>(localStringBuilder.toString());
      throw localSQLiteException2;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (SQLiteException localSQLiteException1)
    {
      for (;;) {}
    }
  }
  
  public SQLiteDatabase getWritableDatabase()
  {
    if ((this.d == null) || (!this.d.isOpen())) {}
    synchronized (this.e)
    {
      if ((this.d == null) || (!this.d.isOpen()))
      {
        this.b = 0;
        this.d = super.getWritableDatabase();
        if (Build.VERSION.SDK_INT >= 11) {
          this.d.enableWriteAheadLogging();
        }
      }
      return this.d;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */