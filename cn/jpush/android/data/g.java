package cn.jpush.android.data;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.net.HttpResponse;
import cn.jpush.android.b.e;
import cn.jpush.android.b.m;
import java.util.ArrayList;
import org.json.JSONObject;

public final class g
  extends b
  implements Runnable
{
  public String a;
  public int ac;
  public int ad;
  public ArrayList<String> ae = new ArrayList();
  public String af = "";
  public String ag = "";
  public String ah;
  private transient Context ai;
  public String b;
  public int c;
  
  public g()
  {
    this.s = 0;
  }
  
  public final void a(Context paramContext)
  {
    this.ai = cn.jpush.android.a.b(paramContext);
    JCoreInterface.asyncExecute(this, new int[0]);
  }
  
  public final boolean a(JSONObject paramJSONObject)
  {
    this.a = paramJSONObject.optString("e_url", "").trim();
    this.b = paramJSONObject.optString("e_title", "").trim();
    if ((!TextUtils.isEmpty(this.a)) && (!m.a(this.a)))
    {
      StringBuilder localStringBuilder = new StringBuilder("http://");
      localStringBuilder.append(this.a);
      this.a = localStringBuilder.toString();
    }
    this.ac = paramJSONObject.optInt("e_rich_type", 0);
    this.c = paramJSONObject.optInt("e_jump_mode", 0);
    this.ad = paramJSONObject.optInt("e_show", 0);
    int i = this.ac;
    if ((3 == i) || (2 == i) || (1 == i)) {
      this.ae = cn.jpush.android.e.b.a(paramJSONObject.optJSONArray("e_eres"));
    }
    this.af = paramJSONObject.optString("from_num", "");
    this.ag = paramJSONObject.optString("to_num", "");
    return true;
  }
  
  public final void run()
  {
    if (this.ad != 0) {
      return;
    }
    String str1 = this.e;
    String str2 = this.a;
    Object localObject2 = this.A;
    JCoreInterface.triggerSceneCheck(this.ai.getApplicationContext(), 3);
    int i = this.ac;
    if (i == 0)
    {
      if ((this.y == 3) && (!TextUtils.isEmpty((CharSequence)localObject2)))
      {
        if ((!((String)localObject2).startsWith("http://")) && (!((String)localObject2).startsWith("https://"))) {
          if (cn.jpush.android.e.a.b(this.ai, "android.permission.READ_EXTERNAL_STORAGE"))
          {
            localObject1 = cn.jpush.android.e.c.c(this.ai, (String)localObject2);
            if (TextUtils.isEmpty((CharSequence)localObject1)) {}
          }
        }
        do
        {
          this.A = ((String)localObject1);
          break label319;
          localObject1 = "Get developer picture failed, show basic notification only.";
          break label312;
          localObject1 = "No permission to read resource from storage, show basic notification only.";
          break label312;
          if (!cn.jpush.android.e.a.b(this.ai, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            break;
          }
          localObject1 = this.ai;
          if ((((String)localObject2).endsWith(".jpg")) || (((String)localObject2).endsWith(".png")))
          {
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append(str1);
            ((StringBuilder)localObject3).append(((String)localObject2).substring(((String)localObject2).lastIndexOf(".")));
            str2 = ((StringBuilder)localObject3).toString();
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append(cn.jpush.android.e.c.b((Context)localObject1, str1));
            ((StringBuilder)localObject3).append(str2);
            localObject1 = ((StringBuilder)localObject3).toString();
            localObject2 = cn.jpush.android.c.a.a((String)localObject2, 5, 5000L, 4);
            if ((localObject2 != null) && (cn.jpush.android.e.c.a((String)localObject1, (byte[])localObject2))) {}
          }
          else
          {
            localObject1 = "";
          }
        } while (!TextUtils.isEmpty((CharSequence)localObject1));
        localObject1 = "Get network picture failed, show basic notification only.";
        break label312;
        localObject1 = "No permission to write resource to storage, show basic notification only.";
        label312:
        cn.jpush.android.e.g.c("ShowEntity", (String)localObject1);
      }
      label319:
      e.a(str1, 995, null, this.ai);
      cn.jpush.android.api.c.a(this.ai, this);
      return;
    }
    if (4 == i)
    {
      this.ah = str2;
      e.a(str1, 995, null, this.ai);
      cn.jpush.android.api.c.a(this.ai, this);
      return;
    }
    if (!cn.jpush.android.e.a.b(this.ai, "android.permission.WRITE_EXTERNAL_STORAGE"))
    {
      cn.jpush.android.e.g.d("ShowEntity", "Rich-push needs the permission of WRITE_EXTERNAL_STORAGE, please request it.");
      e.a(str1, 1014, null, this.ai);
      return;
    }
    boolean bool = TextUtils.isEmpty(str2);
    int j = 1;
    if (!bool) {
      for (i = 0; i < 4; i++)
      {
        localObject1 = cn.jpush.android.c.a.a(str2, 5, 5000L);
        if ((localObject1 != null) && (((HttpResponse)localObject1).getResponseCode() == 200))
        {
          localObject1 = ((HttpResponse)localObject1).getResponseBody();
          i = 1;
          break label476;
        }
      }
    }
    Object localObject1 = null;
    i = 0;
    label476:
    Object localObject3 = cn.jpush.android.e.c.b(this.ai, str1);
    if (i != 0)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append(str1);
      ((StringBuilder)localObject2).append(".html");
      localObject2 = ((StringBuilder)localObject2).toString();
      str2 = str2.substring(0, str2.lastIndexOf("/") + 1);
      if (!this.ae.isEmpty())
      {
        if (!b.a(this.ae, this.ai, str2, str1, a()))
        {
          e.a(str1, 1014, null, this.ai);
          cn.jpush.android.api.c.a(this.ai, this);
          return;
        }
        StringBuilder localStringBuilder = new StringBuilder("img src=\"");
        localStringBuilder.append(str2);
        str2 = localStringBuilder.toString();
        localStringBuilder = new StringBuilder("img src=\"");
        localStringBuilder.append((String)localObject3);
        localObject1 = ((String)localObject1).replaceAll(str2, localStringBuilder.toString());
        if ((!TextUtils.isEmpty((CharSequence)localObject1)) && (cn.jpush.android.e.c.a((String)localObject2, (String)localObject1))) {
          i = j;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          localObject1 = new StringBuilder("file://");
          ((StringBuilder)localObject1).append((String)localObject2);
          this.ah = ((StringBuilder)localObject1).toString();
          e.a(str1, 995, null, this.ai);
        }
        else
        {
          e.a(str1, 1014, null, this.ai);
        }
      }
      else
      {
        this.ah = this.a;
      }
      cn.jpush.android.api.c.a(this.ai, this);
      return;
    }
    e.a(str1, 1014, null, this.ai);
    e.a(str1, 1021, cn.jpush.android.e.a.a(this.ai, str2), this.ai);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/data/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */