package cn.jpush.android;

import android.content.Context;
import android.os.Bundle;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.c;
import cn.jpush.android.b.p;
import cn.jpush.android.service.e;

public final class b
{
  public static int a(Context paramContext)
  {
    return MultiSpHelper.getInt(paramContext, "notification_num", JPushInterface.a);
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("pluginPlatformRegid_v2");
    localStringBuilder.append(paramInt);
    return MultiSpHelper.getString(paramContext, localStringBuilder.toString(), null);
  }
  
  public static void a(Context paramContext, int paramInt, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("pluginPlatformRegid_v2");
    localStringBuilder.append(paramInt);
    MultiSpHelper.commitString(paramContext, localStringBuilder.toString(), paramString);
  }
  
  public static void a(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      int i = p.b();
      if (paramInt < i) {
        c.a(paramContext, i - paramInt);
      }
      MultiSpHelper.commitInt(paramContext, "notification_num", paramInt);
      return;
    }
    Bundle localBundle = new Bundle();
    e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
    localBundle.putInt("multi_type", 2);
    localBundle.putInt("notification_maxnum", paramInt);
    JCoreInterface.sendAction(paramContext, a.a, localBundle);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, boolean paramBoolean)
  {
    if ((!paramBoolean) && (!JCoreInterface.canCallDirect()))
    {
      localObject = new Bundle();
      e.a(paramContext, (Bundle)localObject, "intent.MULTI_PROCESS");
      ((Bundle)localObject).putInt("multi_type", 1);
      ((Bundle)localObject).putString("notification_buidler_id", paramString1);
      ((Bundle)localObject).putString("notification_buidler", paramString2);
      JCoreInterface.sendAction(paramContext, a.a, (Bundle)localObject);
      return;
    }
    Object localObject = new StringBuilder("jpush_save_custom_builder");
    ((StringBuilder)localObject).append(paramString1);
    MultiSpHelper.commitString(paramContext, ((StringBuilder)localObject).toString(), paramString2);
  }
  
  public static void a(Context paramContext, String paramString, boolean paramBoolean)
  {
    if ((!paramBoolean) && (!JCoreInterface.canCallDirect()))
    {
      Bundle localBundle = new Bundle();
      e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
      localBundle.putInt("multi_type", 4);
      localBundle.putString("silence_push_time", paramString);
      JCoreInterface.sendAction(paramContext, a.a, localBundle);
      return;
    }
    MultiSpHelper.commitString(paramContext, "setting_silence_push_time", paramString);
  }
  
  public static String b(Context paramContext)
  {
    return MultiSpHelper.getString(paramContext, "setting_push_time", "");
  }
  
  public static void b(Context paramContext, int paramInt, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder("pluginPlatformRegidupload");
    localStringBuilder.append(paramInt);
    MultiSpHelper.commitBoolean(paramContext, localStringBuilder.toString(), paramBoolean);
  }
  
  public static void b(Context paramContext, String paramString, boolean paramBoolean)
  {
    if ((!paramBoolean) && (!JCoreInterface.canCallDirect()))
    {
      Bundle localBundle = new Bundle();
      e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
      localBundle.putInt("multi_type", 3);
      localBundle.putString("enable_push_time", paramString);
      JCoreInterface.sendAction(paramContext, a.a, localBundle);
      return;
    }
    MultiSpHelper.commitString(paramContext, "setting_push_time", paramString);
  }
  
  public static boolean b(Context paramContext, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("pluginPlatformRegidupload");
    localStringBuilder.append(paramInt);
    return MultiSpHelper.getBoolean(paramContext, localStringBuilder.toString(), false);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */