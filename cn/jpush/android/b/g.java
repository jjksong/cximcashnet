package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jiguang.api.utils.OutputDataUtil;
import cn.jpush.android.api.b;
import cn.jpush.android.e.i;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class g
{
  private static ConcurrentLinkedQueue<Long> a = new ConcurrentLinkedQueue();
  private static final Object c = new Object();
  private static g d;
  private ConcurrentHashMap<Long, h> b = new ConcurrentHashMap();
  
  /* Error */
  public static cn.jpush.android.api.JPushMessage a(Intent paramIntent)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload 4
    //   5: astore_3
    //   6: aload_0
    //   7: ifnull +49 -> 56
    //   10: aload_0
    //   11: ldc 37
    //   13: iconst_m1
    //   14: invokevirtual 43	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
    //   17: istore_2
    //   18: aload_0
    //   19: ldc 45
    //   21: iconst_m1
    //   22: invokevirtual 43	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
    //   25: istore_1
    //   26: aload_0
    //   27: ldc 47
    //   29: invokevirtual 51	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   32: astore_0
    //   33: new 53	cn/jpush/android/api/JPushMessage
    //   36: astore_3
    //   37: aload_3
    //   38: invokespecial 54	cn/jpush/android/api/JPushMessage:<init>	()V
    //   41: aload_3
    //   42: iload_2
    //   43: invokevirtual 58	cn/jpush/android/api/JPushMessage:setSequence	(I)V
    //   46: aload_3
    //   47: iload_1
    //   48: invokevirtual 61	cn/jpush/android/api/JPushMessage:setErrorCode	(I)V
    //   51: aload_3
    //   52: aload_0
    //   53: invokevirtual 65	cn/jpush/android/api/JPushMessage:setMobileNumber	(Ljava/lang/String;)V
    //   56: aload_3
    //   57: areturn
    //   58: astore_0
    //   59: aload 4
    //   61: astore_3
    //   62: goto -6 -> 56
    //   65: astore_0
    //   66: goto -10 -> 56
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	69	0	paramIntent	Intent
    //   25	23	1	i	int
    //   17	26	2	j	int
    //   5	57	3	localObject1	Object
    //   1	59	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   10	41	58	java/lang/Throwable
    //   41	56	65	java/lang/Throwable
  }
  
  public static g a()
  {
    try
    {
      if (d == null) {
        synchronized (c)
        {
          if (d == null)
          {
            g localg = new cn/jpush/android/b/g;
            localg.<init>();
            d = localg;
          }
        }
      }
      ??? = d;
      return (g)???;
    }
    finally {}
  }
  
  public static void a(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      localIntent.addCategory(cn.jpush.android.a.c);
      localIntent.setAction("cn.jpush.android.intent.RECEIVE_MESSAGE");
      localIntent.setPackage(paramContext.getPackageName());
      localIntent.putExtra("message_type", 3);
      localIntent.putExtra("sequence", paramInt1);
      localIntent.putExtra("code", paramInt2);
      localIntent.putExtra("mobile_number", paramString);
      paramContext.sendBroadcast(localIntent);
      return;
    }
    catch (Throwable paramString)
    {
      paramContext = new StringBuilder("onResult error:");
      paramContext.append(paramString);
      cn.jpush.android.e.g.c("MobileNumberHelper", paramContext.toString());
    }
  }
  
  public final void a(Context paramContext, long paramLong, int paramInt)
  {
    if (this.b.size() != 0)
    {
      h localh = (h)this.b.remove(Long.valueOf(paramLong));
      if (localh != null)
      {
        int i;
        if (paramInt == 0)
        {
          MultiSpHelper.commitString(paramContext, "mobile_number", localh.b);
          i = paramInt;
        }
        else if (paramInt == 11)
        {
          i = b.y;
        }
        else
        {
          i = paramInt;
          if (paramInt == 10) {
            i = b.x;
          }
        }
        a(paramContext, localh.a, i, localh.b);
      }
    }
  }
  
  public final void a(Context paramContext, Bundle paramBundle)
  {
    int j = paramBundle.getInt("sequence", 0);
    String str = paramBundle.getString("mobile_number");
    paramBundle = MultiSpHelper.getString(paramContext, "mobile_number", null);
    if ((paramBundle != null) && (TextUtils.equals(str, paramBundle)))
    {
      cn.jpush.android.e.g.a("MobileNumberHelper", "already set this mobile number");
      a(paramContext, j, b.a, str);
      return;
    }
    if (paramBundle != null) {
      MultiSpHelper.commitString(paramContext, "mobile_number", null);
    }
    long l2 = System.currentTimeMillis();
    if (a.size() < 3) {}
    for (;;)
    {
      a.offer(Long.valueOf(l2));
      i = 0;
      break label166;
      l1 = l2 - ((Long)a.element()).longValue();
      if (l1 < 0L)
      {
        a.clear();
        i = 2;
        break label166;
      }
      if (l1 <= 10000L) {
        break;
      }
      while (a.size() >= 3) {
        a.poll();
      }
    }
    int i = 1;
    label166:
    if (i != 0)
    {
      if (i == 1) {
        i = b.l;
      } else {
        i = b.n;
      }
      a(paramContext, j, i, str);
      return;
    }
    i = i.c(str);
    if (i != 0)
    {
      paramBundle = new StringBuilder("Invalid mobile number: ");
      paramBundle.append(str);
      paramBundle.append(", will not set mobile number this time.");
      cn.jpush.android.e.g.a("MobileNumberHelper", paramBundle.toString());
      a(paramContext, j, i, str);
      return;
    }
    long l1 = JCoreInterface.getNextRid();
    l2 = JCoreInterface.getUid();
    i = JCoreInterface.getSid();
    OutputDataUtil localOutputDataUtil = new OutputDataUtil(20480);
    localOutputDataUtil.writeU16(0);
    localOutputDataUtil.writeU8(1);
    localOutputDataUtil.writeU8(26);
    localOutputDataUtil.writeU64(l1);
    localOutputDataUtil.writeU32(i);
    localOutputDataUtil.writeU64(l2);
    localOutputDataUtil.writeU8(7);
    localOutputDataUtil.writeU8(1);
    if (str != null) {
      paramBundle = cn.jpush.a.a.a.a(str);
    } else {
      paramBundle = new byte[0];
    }
    localOutputDataUtil.writeByteArrayincludeLength(paramBundle);
    localOutputDataUtil.writeU16At(localOutputDataUtil.current(), 0);
    paramBundle = localOutputDataUtil.toByteArray();
    this.b.put(Long.valueOf(l1), new h(this, j, str));
    JCoreInterface.sendRequestData(paramContext, cn.jpush.android.a.a, 20000, paramBundle);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */