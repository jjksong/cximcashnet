package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import cn.jpush.android.service.JPushMessageReceiver;

public class a
{
  private static a a;
  private Handler b;
  
  private a()
  {
    try
    {
      HandlerThread localHandlerThread = new android/os/HandlerThread;
      localHandlerThread.<init>("MessageReceiver");
      localHandlerThread.start();
      Handler localHandler = new android/os/Handler;
      localHandler.<init>(localHandlerThread.getLooper());
      this.b = localHandler;
      return;
    }
    catch (Throwable localThrowable)
    {
      this.b = new Handler();
    }
  }
  
  public static a a()
  {
    if (a == null) {
      try
      {
        if (a == null)
        {
          a locala = new cn/jpush/android/b/a;
          locala.<init>();
          a = locala;
        }
      }
      finally {}
    }
    return a;
  }
  
  public final void a(Context paramContext, JPushMessageReceiver paramJPushMessageReceiver, Intent paramIntent)
  {
    this.b.post(new b(this, paramContext, paramJPushMessageReceiver, paramIntent));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */