package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.service.JPushMessageReceiver;
import cn.jpush.android.service.f;

final class b
  implements Runnable
{
  private Context b;
  private JPushMessageReceiver c;
  private Intent d;
  
  public b(a parama, Context paramContext, JPushMessageReceiver paramJPushMessageReceiver, Intent paramIntent)
  {
    this.b = paramContext;
    this.c = paramJPushMessageReceiver;
    this.d = paramIntent;
  }
  
  public final void run()
  {
    Object localObject = this.d;
    if (localObject == null) {
      return;
    }
    if ("cn.jpush.android.intent.RECEIVE_MESSAGE".equals(((Intent)localObject).getAction()))
    {
      int i = this.d.getIntExtra("message_type", -1);
      localObject = null;
      if ((1 != i) && (2 != i))
      {
        if (3 == i)
        {
          g.a();
          localObject = g.a(this.d);
        }
      }
      else {
        localObject = f.a().a(this.d);
      }
      if (localObject == null) {
        return;
      }
      if (i == 1)
      {
        if (((JPushMessage)localObject).isTagCheckOperator())
        {
          this.c.onCheckTagOperatorResult(this.b, (JPushMessage)localObject);
          return;
        }
        this.c.onTagOperatorResult(this.b, (JPushMessage)localObject);
        return;
      }
      if (i == 2)
      {
        this.c.onAliasOperatorResult(this.b, (JPushMessage)localObject);
        return;
      }
      if (i == 3) {
        this.c.onMobileNumberOperatorResult(this.b, (JPushMessage)localObject);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */