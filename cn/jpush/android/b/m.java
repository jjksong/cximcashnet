package cn.jpush.android.b;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.data.a;
import cn.jpush.android.data.b;
import cn.jpush.android.data.g;
import org.json.JSONException;
import org.json.JSONObject;

public final class m
{
  public static a a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    if (paramContext != null)
    {
      if (TextUtils.isEmpty(paramString1))
      {
        e.a("NO MSGID", 996, null, paramContext);
        return null;
      }
      JSONObject localJSONObject = a(paramContext, "NO_MSGID", paramString1);
      if (localJSONObject == null) {
        return null;
      }
      paramContext = localJSONObject.optString("msg_id", "");
      paramString1 = paramContext;
      if (TextUtils.isEmpty(paramContext)) {
        paramString1 = localJSONObject.optString("ad_id", "");
      }
      paramContext = paramString1;
      if (TextUtils.isEmpty(paramString1)) {
        paramContext = localJSONObject.optString("_jmsgid_", "");
      }
      paramString1 = paramContext;
      if (TextUtils.isEmpty(paramContext)) {
        paramString1 = paramString4;
      }
      int i = localJSONObject.optInt("n_only", 0);
      boolean bool = true;
      if (i != 1) {
        bool = false;
      }
      if (bool) {
        i = localJSONObject.optInt("n_builder_id", 0);
      } else {
        i = 0;
      }
      paramContext = new a();
      paramContext.e = paramString1;
      paramContext.a = localJSONObject;
      paramContext.d = localJSONObject.optInt("show_type", 3);
      paramContext.h = bool;
      paramContext.i = i;
      paramContext.j = localJSONObject.optInt("notificaion_type", 0);
      paramContext.l = localJSONObject.optString("message", "");
      paramContext.m = localJSONObject.optString("content_type", "");
      paramContext.o = localJSONObject.optString("title", "");
      paramContext.p = localJSONObject.optString("extras", "");
      paramContext.q = paramString2;
      paramContext.r = paramString3;
      paramContext.f = localJSONObject.optString("override_msg_id", "");
      return paramContext;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  private static JSONObject a(Context paramContext, String paramString1, String paramString2)
  {
    try
    {
      paramString2 = new JSONObject(paramString2);
      return paramString2;
    }
    catch (JSONException paramString2)
    {
      e.a(paramString1, 996, null, paramContext);
    }
    return null;
  }
  
  public static JSONObject a(Context paramContext, String paramString1, JSONObject paramJSONObject, String paramString2)
  {
    if (paramJSONObject == null)
    {
      e.a(paramString1, 996, null, paramContext);
      return null;
    }
    if (TextUtils.isEmpty(paramString2)) {
      return null;
    }
    try
    {
      if (paramJSONObject.isNull(paramString2)) {
        return null;
      }
      paramJSONObject = paramJSONObject.getJSONObject(paramString2);
      return paramJSONObject;
    }
    catch (JSONException paramJSONObject)
    {
      e.a(paramString1, 996, null, paramContext);
    }
    return null;
  }
  
  public static void a(Context paramContext, a parama)
  {
    if (paramContext != null)
    {
      int i = parama.d;
      Object localObject = parama.a;
      String str = parama.e;
      if ((i != 3) && (i != 4))
      {
        e.a(str, 996, null, paramContext);
        return;
      }
      JSONObject localJSONObject = a(paramContext, str, (JSONObject)localObject, "m_content");
      if (localJSONObject == null) {
        return;
      }
      int j = localJSONObject.optInt("ad_t", 0);
      if (j == 0)
      {
        localObject = new g();
        ((b)localObject).e = str;
        ((b)localObject).d = i;
        ((b)localObject).s = j;
        ((b)localObject).k = parama.k;
        ((b)localObject).h = parama.h;
        ((b)localObject).i = parama.i;
        ((b)localObject).q = parama.q;
        ((b)localObject).f = parama.f;
        ((b)localObject).j = parama.j;
        if (((b)localObject).a(paramContext, localJSONObject)) {
          ((b)localObject).a(paramContext);
        }
        return;
      }
      e.a(str, 996, null, paramContext);
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  public static void a(Context paramContext, String paramString)
  {
    if (paramContext != null)
    {
      if (TextUtils.isEmpty(paramString)) {
        return;
      }
      Object localObject2 = a(paramContext, "NO MSGID", paramString);
      if (localObject2 == null) {
        return;
      }
      Object localObject1 = ((JSONObject)localObject2).optString("msg_id", "");
      paramString = (String)localObject1;
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        paramString = ((JSONObject)localObject2).optString("ad_id", "");
      }
      int i = ((JSONObject)localObject2).optInt("show_type", -1);
      if (i == 2)
      {
        localObject1 = ((JSONObject)localObject2).optString("m_content", "").trim();
        if (a((String)localObject1))
        {
          if (paramContext != null)
          {
            JCoreInterface.asyncExecute(new n((String)localObject1, paramContext, paramString), new int[0]);
            return;
          }
          throw new IllegalArgumentException("NULL context");
        }
        e.a(paramString, 996, null, paramContext);
        return;
      }
      if (i == 1) {
        localObject1 = a(paramContext, paramString, (JSONObject)localObject2, "m_content");
      } else {
        localObject1 = null;
      }
      if (localObject1 == null) {
        return;
      }
      int j = ((JSONObject)localObject1).optInt("ad_t", -1);
      if (j != 0)
      {
        e.a(paramString, 996, null, paramContext);
        return;
      }
      localObject2 = new g();
      boolean bool = ((b)localObject2).a(paramContext, (JSONObject)localObject1);
      ((b)localObject2).e = paramString;
      ((b)localObject2).d = i;
      ((b)localObject2).s = j;
      if (bool) {
        ((b)localObject2).a(paramContext);
      }
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  public static boolean a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    return paramString.trim().matches("^[http|https]+://.*");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */