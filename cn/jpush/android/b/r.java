package cn.jpush.android.b;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.a.c;
import cn.jpush.android.api.TagAliasCallback;
import cn.jpush.android.api.b;
import cn.jpush.android.e.g;
import cn.jpush.android.e.i;
import cn.jpush.android.service.ServiceInterface;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONObject;

public final class r
{
  private static ConcurrentLinkedQueue<Long> a = new ConcurrentLinkedQueue();
  
  protected static long a(Context paramContext, String paramString, int paramInt, long paramLong)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      int i = localJSONObject.optInt("code", b.j);
      if (paramInt == 0) {
        paramLong = localJSONObject.optLong("sequence");
      }
      paramString = new android/content/Intent;
      paramString.<init>();
      paramString.addCategory(cn.jpush.android.a.c);
      paramString.putExtra("proto_type", paramInt);
      paramString.setPackage(paramContext.getPackageName());
      if (paramInt == 0)
      {
        paramString.setAction("cn.jpush.android.intent.TAG_ALIAS_CALLBACK");
      }
      else
      {
        paramString.setAction("cn.jpush.android.intent.RECEIVE_MESSAGE");
        if (paramInt == 1) {
          paramString.putExtra("message_type", 1);
        } else {
          paramString.putExtra("message_type", 2);
        }
      }
      paramString.putExtra("tagalias_errorcode", i);
      paramString.putExtra("tagalias_seqid", paramLong);
      paramString = s.a().a(paramContext, paramLong, i, localJSONObject, paramString);
      if (paramString != null) {
        paramContext.sendBroadcast(paramString);
      }
      return paramLong;
    }
    catch (Throwable paramContext) {}
    return -1L;
  }
  
  protected static c a(Context paramContext, String paramString, long paramLong, int paramInt)
  {
    int j = 0;
    int i;
    if ((paramInt != 2) && (paramInt != 3) && (paramInt != 5)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0) {}
    try
    {
      JSONObject localJSONObject = a(paramInt);
      i = j;
      if (paramInt == 2) {
        i = 1;
      }
      if (i != 0)
      {
        if (TextUtils.isEmpty(paramString))
        {
          g.d("TagAliasHelper", "alias was empty. Give up action.");
          a(paramContext, 2, b.b, paramLong);
          return null;
        }
        if (!a(paramContext, 2, paramString, paramLong)) {
          return null;
        }
        localJSONObject.put("alias", paramString);
      }
      paramString = localJSONObject.toString();
      if (TextUtils.isEmpty(paramString)) {
        break label149;
      }
      paramString = new c(1, 29, paramLong, JCoreInterface.getAppKey(), paramString);
      return paramString;
    }
    catch (Throwable paramString)
    {
      for (;;) {}
    }
    a(paramContext, 2, b.j, paramLong);
    label149:
    return null;
  }
  
  protected static c a(Context paramContext, List<String> paramList, long paramLong, int paramInt1, int paramInt2)
  {
    if (paramInt1 != 0) {}
    try
    {
      JSONObject localJSONObject = a(paramInt1);
      int i;
      if ((paramInt1 != 1) && (paramInt1 != 2) && (paramInt1 != 3) && (paramInt1 != 6)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i != 0) {
        if ((paramList != null) && (!paramList.isEmpty()))
        {
          Object localObject = new java/util/HashSet;
          ((HashSet)localObject).<init>(paramList);
          if (!a(paramContext, 1, (Set)localObject, paramLong)) {
            return null;
          }
          localObject = b((Set)localObject);
          if (!b(paramContext, (String)localObject, paramLong, 1)) {
            return null;
          }
          if (paramInt1 == 6)
          {
            if (TextUtils.isEmpty((CharSequence)localObject))
            {
              g.d("TagAliasHelper", "stags was empty. Give up action.");
              a(paramContext, 1, b.b, paramLong);
              return null;
            }
            localJSONObject.put("tags", localObject);
          }
          else
          {
            localObject = new org/json/JSONArray;
            ((JSONArray)localObject).<init>();
            paramList = paramList.iterator();
            while (paramList.hasNext()) {
              ((JSONArray)localObject).put((String)paramList.next());
            }
            localJSONObject.put("tags", localObject);
          }
        }
        else
        {
          g.d("TagAliasHelper", "tags was empty. Give up action.");
          a(paramContext, 1, b.b, paramLong);
          return null;
        }
      }
      if (paramInt1 == 5)
      {
        paramInt1 = paramInt2;
        if (paramInt2 == -1) {
          paramInt1 = 1;
        }
        localJSONObject.put("curr", paramInt1);
      }
      paramList = localJSONObject.toString();
      if (TextUtils.isEmpty(paramList)) {
        break label304;
      }
      paramList = new c(1, 28, paramLong, JCoreInterface.getAppKey(), paramList);
      return paramList;
    }
    catch (Throwable paramList)
    {
      label304:
      for (;;) {}
    }
    a(paramContext, 1, b.j, paramLong);
    return null;
  }
  
  private static c a(Context paramContext, List<String> paramList, String paramString, long paramLong)
  {
    if (paramList != null) {
      paramList = new HashSet(paramList);
    } else {
      paramList = null;
    }
    if ((paramString != null) && (!a(paramContext, 0, paramString, paramLong))) {
      return null;
    }
    if ((paramList != null) && (!a(paramContext, 0, paramList, paramLong))) {
      return null;
    }
    String str = b(paramList);
    if (!b(paramContext, str, paramLong, 0)) {
      return null;
    }
    if ((str == null) && (paramString == null))
    {
      g.d("TagAliasHelper", "NULL alias and tags. Give up action.");
      a(paramContext, 0, b.b, paramLong);
      return null;
    }
    Object localObject = new StringBuilder("action:setAliasAndTags - alias:");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(", tags:");
    ((StringBuilder)localObject).append(str);
    g.a("TagAliasHelper", ((StringBuilder)localObject).toString());
    try
    {
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      ((JSONObject)localObject).put("platform", "a");
      if (paramString != null) {
        ((JSONObject)localObject).put("alias", paramString);
      }
      if (paramList != null) {
        ((JSONObject)localObject).put("tags", str);
      }
      paramList = ((JSONObject)localObject).toString();
      if (!TextUtils.isEmpty(paramList))
      {
        paramList = new c(4, 10, paramLong, JCoreInterface.getAppKey(), paramList);
        return paramList;
      }
    }
    catch (Throwable paramList)
    {
      a(paramContext, 0, b.j, paramLong);
    }
    return null;
  }
  
  public static Set<String> a(Set<String> paramSet)
  {
    if (paramSet == null) {
      return null;
    }
    if (paramSet.isEmpty()) {
      return paramSet;
    }
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    int i = 0;
    paramSet = paramSet.iterator();
    while (paramSet.hasNext())
    {
      String str = (String)paramSet.next();
      if ((!TextUtils.isEmpty(str)) && (i.a(str)))
      {
        localLinkedHashSet.add(str);
        int j = i + 1;
        i = j;
        if (j >= 1000)
        {
          g.c("TagAliasHelper", "The lenght of tags maybe more than 1000.");
          return localLinkedHashSet;
        }
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder("Invalid tag : ");
        localStringBuilder.append(str);
        g.d("TagAliasHelper", localStringBuilder.toString());
      }
    }
    return localLinkedHashSet;
  }
  
  private static JSONObject a(int paramInt)
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("platform", "a");
    String str;
    switch (paramInt)
    {
    default: 
      str = null;
      break;
    case 6: 
      str = "valid";
      break;
    case 5: 
      str = "get";
      break;
    case 4: 
      str = "clean";
      break;
    case 3: 
      str = "del";
      break;
    case 2: 
      str = "set";
      break;
    case 1: 
      str = "add";
    }
    localJSONObject.put("op", str);
    return localJSONObject;
  }
  
  protected static void a(Context paramContext, int paramInt1, int paramInt2, long paramLong)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      localIntent.addCategory(paramContext.getPackageName());
      localIntent.setPackage(paramContext.getPackageName());
      if (paramInt1 == 0)
      {
        localIntent.setAction("cn.jpush.android.intent.TAG_ALIAS_CALLBACK");
      }
      else
      {
        localIntent.setAction("cn.jpush.android.intent.RECEIVE_MESSAGE");
        if (paramInt1 == 1) {
          localIntent.putExtra("message_type", 1);
        } else {
          localIntent.putExtra("message_type", 2);
        }
      }
      localIntent.putExtra("tagalias_errorcode", paramInt2);
      localIntent.putExtra("tagalias_seqid", paramLong);
      paramContext.sendBroadcast(localIntent);
      return;
    }
    catch (Throwable localThrowable)
    {
      paramContext = new StringBuilder("NotifyTagAliasError:");
      paramContext.append(localThrowable.getMessage());
      g.c("TagAliasHelper", paramContext.toString());
    }
  }
  
  public static void a(Context paramContext, int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    if (paramContext != null)
    {
      a(paramContext, paramString, null, new cn.jpush.android.api.a(paramInt1, paramString, System.currentTimeMillis(), 2, paramInt3));
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  public static void a(Context paramContext, int paramInt1, Set<String> paramSet, int paramInt2, int paramInt3)
  {
    if (paramContext != null)
    {
      a(paramContext, null, paramSet, new cn.jpush.android.api.a(paramInt1, paramSet, System.currentTimeMillis(), 1, paramInt3));
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  public static void a(Context paramContext, Bundle paramBundle)
  {
    String str = paramBundle.getString("alias");
    ArrayList localArrayList = paramBundle.getStringArrayList("tags");
    long l1 = paramBundle.getLong("seq_id", 0L);
    int i = 0;
    int j;
    try
    {
      j = Integer.parseInt(paramBundle.getString("proto_type"));
    }
    catch (Throwable localThrowable)
    {
      a(paramContext, 0, b.j, l1);
      j = 0;
    }
    int k;
    try
    {
      k = Integer.parseInt(paramBundle.getString("protoaction_type"));
    }
    catch (Throwable paramBundle)
    {
      a(paramContext, j, b.j, l1);
      k = 0;
    }
    long l2 = System.currentTimeMillis();
    if (a.size() < 10) {}
    for (;;)
    {
      a.offer(Long.valueOf(l2));
      break label182;
      long l3 = l2 - ((Long)a.element()).longValue();
      if (l3 < 0L)
      {
        a.clear();
        i = 2;
        break label182;
      }
      if (l3 <= 10000L) {
        break;
      }
      while (a.size() >= 10) {
        a.poll();
      }
    }
    i = 1;
    label182:
    if (i != 0)
    {
      if (i == 1) {
        i = b.l;
      } else {
        i = b.n;
      }
      a(paramContext, j, i, l1);
      return;
    }
    s.a();
    if (s.a(paramContext, j, l1))
    {
      a(paramContext, j, b.u, l1);
      return;
    }
    paramBundle = null;
    if (j == 0) {
      paramBundle = a(paramContext, localArrayList, str, l1);
    } else if (j == 1) {
      paramBundle = a(paramContext, localArrayList, l1, k, -1);
    } else if (j == 2) {
      paramBundle = a(paramContext, str, l1, k);
    }
    if ((paramBundle != null) && ((j == 1) || (j == 2))) {
      if (s.a().a(j))
      {
        s.a().a(j, k, l1, localArrayList, str);
      }
      else
      {
        if (j == 1) {
          i = b.v;
        } else {
          i = b.w;
        }
        a(paramContext, j, i, l1);
        return;
      }
    }
    a(paramContext, paramBundle);
  }
  
  protected static void a(Context paramContext, c paramc)
  {
    StringBuilder localStringBuilder = new StringBuilder("tagalias:");
    localStringBuilder.append(paramc);
    g.a("TagAliasHelper", localStringBuilder.toString());
    if (paramc != null) {
      f.a(paramContext).a(paramc, 20000);
    }
  }
  
  public static void a(Context paramContext, String paramString, Set<String> paramSet, TagAliasCallback paramTagAliasCallback, int paramInt1, int paramInt2)
  {
    if (paramContext != null)
    {
      a(paramContext, paramString, paramSet, new cn.jpush.android.api.a(paramString, paramSet, paramTagAliasCallback, System.currentTimeMillis(), 0, 0));
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  private static void a(Context paramContext, String paramString, Set<String> paramSet, cn.jpush.android.api.a parama)
  {
    long l = JCoreInterface.getNextRid();
    if (parama != null) {
      cn.jpush.android.service.f.a().a(paramContext, Long.valueOf(l), parama);
    }
    if (ServiceInterface.d(paramContext))
    {
      cn.jpush.android.service.f.a().a(paramContext, b.m, l, parama);
      return;
    }
    Context localContext = paramContext;
    if (!(paramContext instanceof Application)) {
      localContext = paramContext.getApplicationContext();
    }
    if (!cn.jpush.android.a.a(localContext))
    {
      cn.jpush.android.service.f.a().a(localContext, b.j, l, parama);
      return;
    }
    if (parama.e == 0) {
      cn.jpush.android.service.f.a().a(localContext);
    }
    ServiceInterface.a(localContext, paramString, paramSet, l, parama);
  }
  
  private static boolean a(Context paramContext, int paramInt, String paramString, long paramLong)
  {
    int i = i.b(paramString);
    if (i != 0)
    {
      StringBuilder localStringBuilder = new StringBuilder("Invalid alias: ");
      localStringBuilder.append(paramString);
      localStringBuilder.append(", will not set alias this time.");
      g.a("TagAliasHelper", localStringBuilder.toString());
      a(paramContext, paramInt, i, paramLong);
      return false;
    }
    return true;
  }
  
  private static boolean a(Context paramContext, int paramInt, Set<String> paramSet, long paramLong)
  {
    int i = i.a(paramSet);
    if (i != 0)
    {
      g.a("TagAliasHelper", "Invalid tags, will not set tags this time.");
      a(paramContext, paramInt, i, paramLong);
      return false;
    }
    return true;
  }
  
  public static String b(Set<String> paramSet)
  {
    String str = null;
    if (paramSet == null) {
      return null;
    }
    if (paramSet.isEmpty()) {
      return "";
    }
    int i = 0;
    Iterator localIterator = paramSet.iterator();
    paramSet = str;
    while (localIterator.hasNext())
    {
      str = (String)localIterator.next();
      StringBuilder localStringBuilder;
      if ((!TextUtils.isEmpty(str)) && (i.a(str)))
      {
        if (paramSet == null)
        {
          paramSet = str;
        }
        else
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(paramSet);
          localStringBuilder.append(",");
          localStringBuilder.append(str);
          paramSet = localStringBuilder.toString();
        }
        i++;
        if (i >= 1000) {
          return paramSet;
        }
      }
      else
      {
        localStringBuilder = new StringBuilder("Invalid tag: ");
        localStringBuilder.append(str);
        g.d("TagAliasHelper", localStringBuilder.toString());
      }
    }
    return paramSet;
  }
  
  private static boolean b(Context paramContext, String paramString, long paramLong, int paramInt)
  {
    if (paramString != null)
    {
      paramString = paramString.replaceAll(",", "");
      int i;
      if (paramInt != 0) {
        i = 1;
      } else {
        i = 0;
      }
      int j;
      if (!TextUtils.isEmpty(paramString)) {
        j = paramString.getBytes().length + 0;
      } else {
        j = 0;
      }
      int k = 5000;
      if (i != 0 ? j <= 5000 : j <= 7000) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0)
      {
        a(paramContext, paramInt, b.i, paramLong);
        paramContext = new StringBuilder("The length of tags should be less than ");
        if (paramInt != 0) {
          paramInt = k;
        } else {
          paramInt = 7000;
        }
        paramContext.append(paramInt);
        paramContext.append(" bytes.");
        g.c("TagAliasHelper", paramContext.toString());
        return false;
      }
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */