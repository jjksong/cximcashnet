package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.a.c;
import cn.jpush.android.api.b;
import cn.jpush.android.e.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public final class s
{
  private static volatile s a;
  private static final Object b = new Object();
  private ConcurrentHashMap<Long, t> c = new ConcurrentHashMap();
  
  public static s a()
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          s locals = new cn/jpush/android/b/s;
          locals.<init>();
          a = locals;
        }
      }
    }
    return a;
  }
  
  private static t a(JSONObject paramJSONObject, t paramt)
  {
    if (paramt == null) {
      return null;
    }
    if ((!TextUtils.equals(paramJSONObject.optString("op"), "get")) || (paramt.a == 1)) {}
    try
    {
      JSONArray localJSONArray = paramJSONObject.optJSONArray("tags");
      if ((localJSONArray == null) || (localJSONArray.length() == 0)) {
        break label115;
      }
      paramJSONObject = new java/util/ArrayList;
      paramJSONObject.<init>();
      for (int i = 0; i < localJSONArray.length(); i++) {
        paramJSONObject.add(localJSONArray.getString(i));
      }
      if (paramJSONObject.size() <= 0) {
        break label115;
      }
      paramt.d.addAll(paramJSONObject);
    }
    catch (Throwable paramJSONObject)
    {
      label115:
      for (;;) {}
    }
    paramJSONObject = paramJSONObject.optString("alias");
    if (paramJSONObject != null) {
      paramt.e = paramJSONObject;
    }
    return paramt;
  }
  
  protected static boolean a(Context paramContext, int paramInt, long paramLong)
  {
    if ((paramInt == 1) || (paramInt == 2))
    {
      long l2 = MultiSpHelper.getLong(paramContext, "TAFreezeEndTime", -1L);
      long l1 = l2;
      if (l2 > 1800L) {
        l1 = 0L;
      }
      l2 = MultiSpHelper.getLong(paramContext, "TAFreezeSetTime", -1L);
      int i;
      if ((l1 != -1L) && (l2 != -1L) && ((System.currentTimeMillis() - l2 < 0L) || (System.currentTimeMillis() - l2 > l1)))
      {
        MultiSpHelper.commitLong(paramContext, "TAFreezeSetTime", -1L);
        MultiSpHelper.commitLong(paramContext, "TAFreezeEndTime", -1L);
        i = 1;
      }
      else
      {
        i = 0;
      }
      if (i != 0)
      {
        r.a(paramContext, paramInt, b.u, paramLong);
        return true;
      }
    }
    return false;
  }
  
  private boolean a(Context paramContext, t paramt)
  {
    if (paramt == null) {
      return false;
    }
    c localc;
    if (paramt.a == 1)
    {
      localc = r.a(paramContext, paramt.d, paramt.c, paramt.b, paramt.g);
    }
    else
    {
      if (paramt.a != 2) {
        break label143;
      }
      localc = r.a(paramContext, paramt.e, paramt.c, paramt.a);
    }
    if (localc != null)
    {
      if (paramt.h > 200)
      {
        this.c.remove(Long.valueOf(paramt.c));
        r.a(paramContext, paramt.a, b.o, paramt.c);
      }
      else
      {
        r.a(paramContext, localc);
        paramt.h += 1;
        this.c.put(Long.valueOf(paramt.c), paramt);
      }
      return true;
    }
    label143:
    return false;
  }
  
  public final int a(long paramLong)
  {
    t localt = (t)this.c.remove(Long.valueOf(paramLong));
    if (localt != null) {
      return localt.a;
    }
    return 0;
  }
  
  public final Intent a(Context paramContext, long paramLong, int paramInt, JSONObject paramJSONObject, Intent paramIntent)
  {
    t localt = (t)this.c.get(Long.valueOf(paramLong));
    this.c.remove(Long.valueOf(paramLong));
    if (localt == null) {
      return paramIntent;
    }
    if ((paramInt == 1) && (localt.h == 0))
    {
      localt.h += 1;
      if (a(paramContext, localt.a, localt.c)) {}
      while (a(paramContext, localt))
      {
        i = 1;
        break;
      }
    }
    int i = 0;
    if (i != 0) {
      return null;
    }
    if (paramInt != 0)
    {
      if (paramInt == 100)
      {
        long l = paramJSONObject.optLong("wait", -1L);
        paramJSONObject = new StringBuilder("set tag/alias action will freeze ");
        paramJSONObject.append(l);
        paramJSONObject.append(" seconds");
        g.c("TagAliasNewProtoRetryHelper", paramJSONObject.toString());
        if ((l > 0L) && (l >= 0L))
        {
          paramLong = l;
          if (l > 1800L) {
            paramLong = 1800L;
          }
          MultiSpHelper.commitLong(paramContext, "TAFreezeSetTime", System.currentTimeMillis());
          MultiSpHelper.commitLong(paramContext, "TAFreezeEndTime", paramLong * 1000L);
        }
      }
      i = paramInt;
      if (localt.a != 0)
      {
        if (paramInt != 100) {
          i = paramInt;
        }
        switch (paramInt)
        {
        default: 
          i = paramInt;
          break;
        case 7: 
        case 8: 
          i = b.t;
          break;
        case 6: 
          i = b.s;
          break;
        case 5: 
          i = b.r;
          break;
        case 4: 
          i = b.q;
          break;
        case 3: 
          i = b.p;
          break;
        case 1: 
        case 2: 
          i = b.o;
          break;
          i = b.u;
        }
      }
      paramIntent.putExtra("tagalias_errorcode", i);
      return paramIntent;
    }
    localt.h = 0;
    if (localt.b == 5)
    {
      localt.f = paramJSONObject.optInt("total", -1);
      localt.g = paramJSONObject.optInt("curr", -1);
      a(paramJSONObject, localt);
    }
    if (localt == null) {}
    while (localt.g >= localt.f)
    {
      paramInt = 0;
      break;
    }
    paramInt = 1;
    if (paramInt != 0)
    {
      localt.g += 1;
      if (a(paramContext, localt.a, localt.c)) {
        return null;
      }
      if (a(paramContext, localt)) {
        return null;
      }
    }
    if (localt.b == 5)
    {
      if (localt.a == 1)
      {
        if (localt.d.size() > 0) {
          paramIntent.putStringArrayListExtra("tags", localt.d);
        }
      }
      else if ((localt.a == 2) && (localt.e != null)) {
        paramIntent.putExtra("alias", localt.e);
      }
    }
    else if ((localt.b == 6) && (localt.a == 1)) {
      paramIntent.putExtra("validated", paramJSONObject.optBoolean("validated", false));
    }
    return paramIntent;
  }
  
  public final void a(int paramInt1, int paramInt2, long paramLong, ArrayList<String> paramArrayList, String paramString)
  {
    paramArrayList = new t(this, paramInt1, paramInt2, paramLong, paramArrayList, paramString);
    this.c.put(Long.valueOf(paramLong), paramArrayList);
  }
  
  public final boolean a(int paramInt)
  {
    Object localObject = this.c;
    if ((localObject != null) && (!((ConcurrentHashMap)localObject).isEmpty()))
    {
      localObject = this.c.entrySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        t localt = (t)((Map.Entry)((Iterator)localObject).next()).getValue();
        if ((localt != null) && (localt.a == paramInt)) {
          return false;
        }
      }
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/s.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */