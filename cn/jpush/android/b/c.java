package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import cn.jiguang.api.JAction;
import cn.jiguang.api.JResponse;
import cn.jpush.android.service.ServiceInterface;
import cn.jpush.android.service.e;
import java.nio.ByteBuffer;

public class c
  implements JAction
{
  public long dispatchMessage(Context paramContext, long paramLong, int paramInt, Object paramObject, ByteBuffer paramByteBuffer)
  {
    if (!cn.jpush.android.a.a(paramContext)) {
      return -1L;
    }
    if (paramInt != 3)
    {
      if (paramInt != 10) {
        switch (paramInt)
        {
        default: 
          paramObject = null;
          break;
        case 26: 
        case 27: 
          paramObject = new cn.jpush.a.a(paramObject, paramByteBuffer);
          break;
        }
      } else {
        paramObject = new cn.jpush.a.d(paramObject, paramByteBuffer);
      }
    }
    else {
      paramObject = new cn.jpush.a.b(paramObject, paramByteBuffer);
    }
    if ((ServiceInterface.c(paramContext)) && (paramObject != null) && ((paramObject instanceof cn.jpush.a.b)) && (((cn.jpush.a.b)paramObject).a() != 20)) {
      return -1L;
    }
    if (paramObject != null)
    {
      paramByteBuffer = new StringBuilder("response:");
      paramByteBuffer.append(((JResponse)paramObject).toString());
      cn.jpush.android.e.g.a("JPushDataAction", paramByteBuffer.toString());
      paramInt = ((JResponse)paramObject).getCommand();
      if (paramInt != 3)
      {
        if (paramInt != 10) {
          switch (paramInt)
          {
          default: 
            break;
          case 28: 
          case 29: 
            paramByteBuffer = ((cn.jpush.a.d)paramObject).a();
            if (((JResponse)paramObject).getCommand() == 28) {
              paramInt = 1;
            } else {
              paramInt = 2;
            }
            return r.a(paramContext, paramByteBuffer, paramInt, ((JResponse)paramObject).getRid().longValue());
          case 27: 
            if (((JResponse)paramObject).code == 0) {
              cn.jpush.android.d.a.a().a(paramContext, ((JResponse)paramObject).getRid().longValue());
            } else {
              cn.jpush.android.d.a.a().a(paramContext, ((JResponse)paramObject).getRid().longValue(), ((JResponse)paramObject).code);
            }
            break;
          case 26: 
            g.a().a(paramContext, ((JResponse)paramObject).getRid().longValue(), ((JResponse)paramObject).code);
            break;
          }
        }
      }
      else {
        return o.a(cn.jpush.android.a.e, paramLong, (JResponse)paramObject);
      }
    }
    if (paramObject != null) {
      return ((JResponse)paramObject).getRid().longValue();
    }
    return -1L;
  }
  
  public void dispatchTimeOutMessage(Context paramContext, long paramLong1, long paramLong2, int paramInt)
  {
    if (!cn.jpush.android.a.a(paramContext)) {
      return;
    }
    if (paramInt != 10) {
      switch (paramInt)
      {
      default: 
        break;
      case 27: 
        cn.jpush.android.d.a.a().b(paramContext, paramLong2);
        return;
      case 26: 
        g.a().a(paramContext, paramLong2, cn.jpush.android.api.b.c);
        return;
      }
    }
    f.a(paramContext).a(paramLong2, cn.jpush.android.api.b.c);
  }
  
  public IBinder getBinderByType(String paramString)
  {
    return null;
  }
  
  public String getSdkVersion()
  {
    return "3.2.0";
  }
  
  public void handleMessage(Context paramContext, long paramLong, Object paramObject)
  {
    if (!cn.jpush.android.a.a(paramContext)) {
      return;
    }
    if (paramObject != null)
    {
      if ((paramObject instanceof Bundle))
      {
        e.a(paramContext);
        paramContext = (Bundle)paramObject;
        if (paramContext != null) {
          paramContext.getInt("what");
        }
        return;
      }
      if ((paramObject instanceof Intent))
      {
        cn.jpush.android.service.d.a();
        paramObject = (Intent)paramObject;
        if (paramObject == null) {
          paramContext = "Received null intent broadcast. Give up processing.";
        }
        for (;;)
        {
          cn.jpush.android.e.g.c("PushReceiverCore", paramContext);
          break;
          try
          {
            String str = ((Intent)paramObject).getAction();
            Object localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>("onReceive - ");
            ((StringBuilder)localObject).append(str);
            cn.jpush.android.e.g.a("PushReceiverCore", ((StringBuilder)localObject).toString());
            if ((!cn.jpush.android.a.a(paramContext.getApplicationContext())) || (str == null)) {
              break;
            }
            if ("intent.plugin.platform.REFRESSH_REGID".equals(str))
            {
              cn.jpush.android.d.d.a().b(paramContext, ((Intent)paramObject).getExtras());
              break;
            }
            if (str.startsWith("cn.jpush.android.intent.NOTIFICATION_RECEIVED_PROXY")) {
              try
              {
                if (ServiceInterface.d(paramContext)) {
                  break;
                }
                ((Intent)paramObject).getIntExtra("notificaion_type", 0);
                localObject = ((Intent)paramObject).getStringExtra("message");
                if (TextUtils.isEmpty((CharSequence)localObject))
                {
                  cn.jpush.android.e.g.c("PushReceiverCore", "Got an empty notification, don't show it!");
                }
                else
                {
                  str = ((Intent)paramObject).getStringExtra("senderId");
                  paramObject = m.a(paramContext, (String)localObject, ((Intent)paramObject).getStringExtra("appId"), str, ((Intent)paramObject).getStringExtra("msg_id"));
                  if (paramObject != null)
                  {
                    ((cn.jpush.android.data.a)paramObject).k = true;
                    m.a(paramContext, (cn.jpush.android.data.a)paramObject);
                  }
                }
              }
              catch (Exception paramContext)
              {
                paramContext.printStackTrace();
              }
            }
            if (!str.equals("android.intent.action.PACKAGE_ADDED"))
            {
              if (str.equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE"))
              {
                paramContext = (NetworkInfo)((Intent)paramObject).getParcelableExtra("networkInfo");
                if ((paramContext == null) || (2 == paramContext.getType()) || (3 == paramContext.getType()) || (((Intent)paramObject).getBooleanExtra("noConnectivity", false)) || (NetworkInfo.State.CONNECTED == paramContext.getState())) {
                  break;
                }
                paramObject = NetworkInfo.State.DISCONNECTED;
                paramContext.getState();
                break;
              }
              if (str.startsWith("cn.jpush.android.intent.NOTIFICATION_OPENED_PROXY"))
              {
                cn.jpush.android.service.d.a(paramContext, (Intent)paramObject);
                break;
              }
              if (str.startsWith("cn.jpush.android.intent.NOTIFICATION_CLICK_ACTION_PROXY")) {
                try
                {
                  localObject = new android/content/Intent;
                  ((Intent)localObject).<init>("cn.jpush.android.intent.NOTIFICATION_CLICK_ACTION");
                  ((Intent)localObject).putExtras(((Intent)paramObject).getExtras());
                  ((Intent)localObject).addCategory(paramContext.getPackageName());
                  ((Intent)localObject).setPackage(paramContext.getPackageName());
                  paramObject = new java/lang/StringBuilder;
                  ((StringBuilder)paramObject).<init>();
                  ((StringBuilder)paramObject).append(paramContext.getPackageName());
                  ((StringBuilder)paramObject).append(".permission.JPUSH_MESSAGE");
                  paramContext.sendBroadcast((Intent)localObject, ((StringBuilder)paramObject).toString());
                }
                catch (Throwable paramObject)
                {
                  paramContext = new StringBuilder("Click notification sendBroadcast :");
                  paramContext.append(((Throwable)paramObject).getMessage());
                  cn.jpush.android.e.g.c("PushReceiverCore", paramContext.toString());
                }
              }
            }
            return;
          }
          catch (NullPointerException paramContext)
          {
            paramContext = "Received no action intent broadcast. Give up processing.";
          }
        }
      }
      cn.jpush.android.e.g.a("JPushDataAction", "handleMessage unknown object ");
    }
  }
  
  public boolean isSupportedCMD(int paramInt)
  {
    return (paramInt == 3) || (paramInt == 10) || (paramInt == 27) || (paramInt == 28) || (paramInt == 29) || (paramInt == 26);
  }
  
  public void onActionRun(Context paramContext, long paramLong, Bundle paramBundle, Object paramObject)
  {
    cn.jpush.android.e.g.a("JPushDataAction", "Action - onActionRun");
    if (!cn.jpush.android.a.a(paramContext)) {
      return;
    }
    e.a(paramContext).a(paramBundle, (Handler)paramObject);
  }
  
  public void onEvent(Context paramContext, long paramLong, int paramInt)
  {
    if (!cn.jpush.android.a.a(paramContext)) {
      return;
    }
    if (paramInt != 19) {
      switch (paramInt)
      {
      default: 
        break;
      case 1: 
        cn.jpush.android.d.d.a().f(paramContext);
      case 0: 
        return;
      }
    } else {
      e.a(paramContext).a();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */