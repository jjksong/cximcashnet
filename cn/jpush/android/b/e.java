package cn.jpush.android.b;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class e
{
  public static void a(String paramString1, int paramInt, String paramString2, Context paramContext)
  {
    if (!JCoreInterface.isValidRegistered()) {
      return;
    }
    if (paramContext == null) {
      return;
    }
    Object localObject = new StringBuffer();
    StringBuilder localStringBuilder = new StringBuilder("action:reportActionResult - messageId: ");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(", code: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append("-");
    localStringBuilder.append(q.a(paramInt));
    ((StringBuffer)localObject).append(localStringBuilder.toString());
    if (!TextUtils.isEmpty(paramString2))
    {
      localStringBuilder = new StringBuilder(" report content: ");
      localStringBuilder.append(paramString2);
      ((StringBuffer)localObject).append(localStringBuilder.toString());
    }
    localObject = new JSONObject();
    try
    {
      ((JSONObject)localObject).put("msg_id", paramString1);
      ((JSONObject)localObject).put("result", paramInt);
      if (!TextUtils.isEmpty(paramString2)) {
        ((JSONObject)localObject).put("data", paramString2);
      }
      JCoreInterface.fillBaseReport((JSONObject)localObject, "msg_status");
      JCoreInterface.reportHttpData(paramContext, localObject, a.a);
      return;
    }
    catch (JSONException paramString1)
    {
      for (;;) {}
    }
  }
  
  public static void a(String paramString1, String paramString2, byte paramByte, int paramInt, Context paramContext)
  {
    if (!JCoreInterface.isValidRegistered()) {
      return;
    }
    if (paramContext == null) {
      return;
    }
    StringBuffer localStringBuffer = new StringBuffer();
    Object localObject = new StringBuilder("action:reportThirdSDKMsgActionResult - messageId: ");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(", code: ");
    ((StringBuilder)localObject).append(paramInt);
    localStringBuffer.append(((StringBuilder)localObject).toString());
    localObject = new JSONObject();
    try
    {
      ((JSONObject)localObject).put("msg_id", paramString1);
      ((JSONObject)localObject).put("tmsg_id", paramString2);
      ((JSONObject)localObject).put("result", paramInt);
      ((JSONObject)localObject).put("sdk_type", paramByte);
      JCoreInterface.fillBaseReport((JSONObject)localObject, "third_msg_status");
      JCoreInterface.reportHttpData(paramContext, localObject, a.a);
      return;
    }
    catch (JSONException paramString1)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */