package cn.jpush.android.b;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.JResponse;
import cn.jiguang.api.utils.OutputDataUtil;
import cn.jpush.a.b;
import cn.jpush.android.a.f;
import cn.jpush.android.data.c;
import cn.jpush.android.e.h;
import cn.jpush.android.e.l;
import cn.jpush.android.service.ServiceInterface;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import org.json.JSONObject;

public final class o
{
  public static long a(Context paramContext, long paramLong, JResponse paramJResponse)
  {
    Object localObject1 = (b)paramJResponse;
    int i = ((b)localObject1).a();
    long l2 = ((b)localObject1).b();
    long l1 = ((b)localObject1).getRid().longValue();
    paramLong = JCoreInterface.getUid();
    i = (byte)i;
    int j = JCoreInterface.getSid();
    Object localObject2 = new OutputDataUtil(20480);
    ((OutputDataUtil)localObject2).writeU16(0);
    ((OutputDataUtil)localObject2).writeU8(1);
    ((OutputDataUtil)localObject2).writeU8(4);
    ((OutputDataUtil)localObject2).writeU64(l1);
    ((OutputDataUtil)localObject2).writeU32(j);
    ((OutputDataUtil)localObject2).writeU64(paramLong);
    ((OutputDataUtil)localObject2).writeU16(0);
    ((OutputDataUtil)localObject2).writeU8(i);
    ((OutputDataUtil)localObject2).writeU64(l2);
    ((OutputDataUtil)localObject2).writeU16At(((OutputDataUtil)localObject2).current(), 0);
    localObject2 = ((OutputDataUtil)localObject2).toByteArray();
    JCoreInterface.sendData(cn.jpush.android.a.e, cn.jpush.android.a.a, 4, (byte[])localObject2);
    paramLong = ((b)localObject1).b();
    i = ((b)localObject1).a();
    localObject1 = ((b)localObject1).c();
    Object localObject3 = new LineNumberReader(new StringReader((String)localObject1));
    try
    {
      localObject2 = ((LineNumberReader)localObject3).readLine();
      if (localObject2 == null) {
        return -1L;
      }
      localObject3 = ((LineNumberReader)localObject3).readLine();
      if (localObject3 == null) {
        return -1L;
      }
      j = ((String)localObject2).length() + ((String)localObject3).length() + 2;
      if (((String)localObject1).length() > j + 1) {
        localObject1 = ((String)localObject1).substring(j);
      } else {
        localObject1 = "";
      }
      if ((i != 0) && (i != 2))
      {
        if (i == 20) {}
      }
      else {
        switch (i)
        {
        default: 
          JCoreInterface.processCtrlReport(i);
          break;
        case 100: 
        case 101: 
          break;
        }
      }
      return r.a(paramContext, (String)localObject1, 0, -1L);
    }
    catch (IOException paramContext)
    {
      l locall;
      label377:
      return -1L;
    }
    try
    {
      locall = new cn/jpush/android/e/l;
      locall.<init>("PushMessageProcessor", "Time to process received msg.");
      if ((!TextUtils.isEmpty((CharSequence)localObject2)) && (!TextUtils.isEmpty((CharSequence)localObject3)) && (!TextUtils.isEmpty((CharSequence)localObject1))) {
        a(paramContext, (String)localObject2, (String)localObject3, (String)localObject1, paramLong, (byte)0);
      }
      locall.a();
    }
    catch (Exception paramContext)
    {
      break label377;
    }
    return paramJResponse.getRid().longValue();
  }
  
  public static void a(Context paramContext, cn.jpush.android.data.a parama, int paramInt)
  {
    if (((paramInt & 0x2) != 0) && ((!TextUtils.isEmpty(parama.l)) || (!TextUtils.isEmpty(parama.p)))) {
      cn.jpush.android.e.a.a(paramContext, parama);
    }
    if ((paramInt & 0x1) != 0)
    {
      if (ServiceInterface.d(paramContext)) {
        return;
      }
      if (!cn.jpush.android.e.a.c(paramContext))
      {
        e.a(parama.e, 986, null, paramContext);
        return;
      }
      parama.k = true;
      m.a(paramContext, parama);
    }
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, String paramString3, long paramLong, byte paramByte)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramLong);
    paramString1 = m.a(paramContext, paramString3, paramString1, paramString2, localStringBuilder.toString());
    if (paramString1 == null) {
      return;
    }
    if (h.a(paramContext, new c(paramString1))) {
      return;
    }
    paramString1.g = paramByte;
    int i = 0;
    if ((paramString2 != null) && (paramString2.equalsIgnoreCase("7fef6a7d76c782b1f0eda446b2c6c40a")))
    {
      m.a(paramContext, paramString1);
    }
    else if (paramString1.h)
    {
      i = 1;
      if (paramString1.d == 4) {
        i = 3;
      }
    }
    else
    {
      i = 2;
    }
    if ((paramString1.a != null) && (paramString1.a.has("geofence")))
    {
      paramString2 = paramString1.a;
      paramString3 = paramString2.optJSONObject("geofence");
      if (paramString3 != null)
      {
        paramString1.K = paramString3.optString("geofenceid");
        paramString1.L = paramString3.optLong("radius");
        paramString1.M = paramString3.optString("status");
        paramString1.N = paramString3.optBoolean("repeat");
        paramString1.P = paramString3.optLong("expiration");
        paramString3 = paramString3.optJSONObject("center");
        if (paramString3 != null)
        {
          paramString1.Q = paramString3.optDouble("lon", 200.0D);
          paramString1.R = paramString3.optDouble("lat", 200.0D);
        }
        paramString1.b = paramString2.toString();
        paramString1.a = null;
        paramString1.c = i;
        f.a(paramContext).a(paramString1);
      }
      return;
    }
    new StringBuilder().append(paramLong);
    a(paramContext, paramString1, i);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */