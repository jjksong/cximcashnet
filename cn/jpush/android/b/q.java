package cn.jpush.android.b;

import android.util.SparseArray;

public final class q
{
  private static final SparseArray<String> a;
  
  static
  {
    SparseArray localSparseArray = new SparseArray();
    a = localSparseArray;
    localSparseArray.put(983, "wrong with resource file push_notification.xml");
    a.put(993, "target app uninstall,not found target app small icon");
    a.put(984, "Deep link source is not empty but PushReceiver and PopWinActvity invalid");
    a.put(985, "Deep link source is not empty but pkgname is empty");
    a.put(986, "Message is not in push time");
    a.put(987, "Deep link start failed");
    a.put(988, "Deep link target app uninstalled");
    a.put(991, "Link uri parse failed");
    a.put(992, "Deep link parse failed");
    a.put(995, "Message JSON parsing succeed");
    a.put(996, "Message JSON parsing failed");
    a.put(997, "Message already received, give up");
    a.put(998, "Message already received, still process");
    a.put(1000, "User clicked and opened the Message");
    a.put(1028, "User clicked and opened the Message from JPushInterface.reportNotificationOpened");
    a.put(1001, "Message download succeed");
    a.put(1002, "Message received succeed");
    a.put(1003, "Message silence download succeed");
    a.put(1004, "Video silence downlaod succeed");
    a.put(1005, "User clicked video and jumped to url Message (browser)");
    a.put(1008, "Video is force closed by user");
    a.put(1007, "User clicked 'OK'");
    a.put(1006, "User clicked 'Cancel'");
    a.put(1011, "Download failed");
    a.put(1012, "User clicked to download again");
    a.put(1013, "The file already exist and same size. Don't download again.");
    a.put(1100, "Invalid param or unexpected result.");
    a.put(1014, "Failed to preload required resource");
    a.put(1015, "User clicked install alert on status bar after downloading finished.");
    a.put(1016, "User clicked the webview's url");
    a.put(1017, "User clicked call action");
    a.put(1018, "The Message show in the status bar");
    a.put(1019, "Click applist and show the Message");
    a.put(1020, "Down image failed");
    a.put(1021, "Down html failed");
    a.put(1022, "Down Message failed");
    a.put(1030, "Discard the message because it is not in the push time");
    a.put(1031, "Stop push service");
    a.put(1032, "Resume push service");
  }
  
  public static String a(int paramInt)
  {
    if (a.get(paramInt) == null) {
      return "";
    }
    return (String)a.get(paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */