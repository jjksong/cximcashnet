package cn.jpush.android.b;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class p
{
  private static Queue<Integer> a = new ConcurrentLinkedQueue();
  
  public static int a()
  {
    if (a.size() > 0) {
      return ((Integer)a.poll()).intValue();
    }
    return 0;
  }
  
  public static boolean a(int paramInt)
  {
    return a.offer(Integer.valueOf(paramInt));
  }
  
  public static int b()
  {
    return a.size();
  }
  
  public static boolean b(int paramInt)
  {
    return a.contains(Integer.valueOf(paramInt));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */