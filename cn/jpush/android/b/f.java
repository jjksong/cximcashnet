package cn.jpush.android.b;

import android.content.Context;
import android.content.Intent;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.JRequest;
import cn.jiguang.api.utils.OutputDataUtil;
import cn.jpush.a.c;
import cn.jpush.android.e.g;

public final class f
{
  private static f b;
  private Context a = null;
  
  private f(Context paramContext)
  {
    this.a = paramContext;
  }
  
  public static f a(Context paramContext)
  {
    try
    {
      if (b == null)
      {
        f localf = new cn/jpush/android/b/f;
        localf.<init>(paramContext);
        b = localf;
      }
      paramContext = b;
      return paramContext;
    }
    finally {}
  }
  
  public final void a(long paramLong, int paramInt)
  {
    try
    {
      int i = s.a().a(paramLong);
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      ((Intent)localObject).addCategory(cn.jpush.android.a.c);
      ((Intent)localObject).setPackage(this.a.getPackageName());
      if (i == 0)
      {
        ((Intent)localObject).setAction("cn.jpush.android.intent.TAG_ALIAS_TIMEOUT");
      }
      else
      {
        ((Intent)localObject).setAction("cn.jpush.android.intent.RECEIVE_MESSAGE");
        if (i == 1) {
          ((Intent)localObject).putExtra("message_type", 1);
        } else {
          ((Intent)localObject).putExtra("message_type", 2);
        }
      }
      ((Intent)localObject).putExtra("tagalias_errorcode", paramInt);
      ((Intent)localObject).putExtra("tagalias_seqid", paramLong);
      this.a.sendBroadcast((Intent)localObject);
      return;
    }
    catch (Throwable localThrowable)
    {
      Object localObject = new StringBuilder("onTagaliasTimeout error:");
      ((StringBuilder)localObject).append(localThrowable.getMessage());
      g.c("JPushRequestHelper", ((StringBuilder)localObject).toString());
    }
  }
  
  public final void a(JRequest paramJRequest, int paramInt)
  {
    if (paramJRequest == null) {
      return;
    }
    Object localObject = new StringBuilder("Action - sendJPushRequest, timeout:");
    ((StringBuilder)localObject).append(20000);
    ((StringBuilder)localObject).append(", threadId:");
    ((StringBuilder)localObject).append(Thread.currentThread().getId());
    g.a("JPushRequestHelper", ((StringBuilder)localObject).toString());
    localObject = paramJRequest.getRid();
    int i = paramJRequest.getCommand();
    long l1 = JCoreInterface.getUid();
    paramInt = JCoreInterface.getSid();
    if (i != 10)
    {
      switch (i)
      {
      default: 
        break;
      }
    }
    else
    {
      long l2 = ((Long)localObject).longValue();
      localObject = JCoreInterface.getAppKey();
      int j = (short)paramJRequest.getVersion();
      i = (short)paramJRequest.getCommand();
      String str = ((c)paramJRequest).a();
      paramJRequest = new OutputDataUtil(20480);
      paramJRequest.writeU16(0);
      paramJRequest.writeU8(j);
      paramJRequest.writeU8(i);
      paramJRequest.writeU64(l2);
      paramJRequest.writeU32(paramInt);
      paramJRequest.writeU64(l1);
      if (i == 10) {
        paramJRequest.writeByteArrayincludeLength(cn.jpush.a.a.a.a((String)localObject));
      }
      paramJRequest.writeByteArrayincludeLength(cn.jpush.a.a.a.a(str));
      paramJRequest.writeU16At(paramJRequest.current(), 0);
      paramJRequest = paramJRequest.toByteArray();
      JCoreInterface.sendRequestData(this.a, cn.jpush.android.a.a, 20000, paramJRequest);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */