package cn.jpush.android.b;

import android.app.Notification;
import android.app.Notification.Builder;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RemoteViews;

public final class i
{
  private static int a = -1;
  private static int b = -1;
  private static float c = -1.0F;
  private static float d = -1.0F;
  
  public static int a()
  {
    return a;
  }
  
  @RequiresApi(api=11)
  public static void a(Context paramContext)
  {
    for (;;)
    {
      try
      {
        if (a != -1)
        {
          i = b;
          if (i != -1) {
            continue;
          }
        }
      }
      catch (Exception|Throwable paramContext)
      {
        int i;
        Object localObject2;
        Object localObject1;
        continue;
      }
      try
      {
        localObject2 = new android/app/Notification$Builder;
        ((Notification.Builder)localObject2).<init>(paramContext);
        ((Notification.Builder)localObject2).setContentTitle("title");
        ((Notification.Builder)localObject2).setContentText("content");
        if (Build.VERSION.SDK_INT >= 16) {
          ((Notification.Builder)localObject2).setSubText("subtext");
        }
        if (Build.VERSION.SDK_INT >= 16) {
          localObject1 = ((Notification.Builder)localObject2).build();
        } else {
          localObject1 = ((Notification.Builder)localObject2).getNotification();
        }
        if (Build.VERSION.SDK_INT >= 24) {
          localObject1 = ((Notification.Builder)localObject2).createContentView();
        } else {
          localObject1 = ((Notification)localObject1).contentView;
        }
        i = ((RemoteViews)localObject1).getLayoutId();
        localObject1 = (ViewGroup)LayoutInflater.from(paramContext).inflate(i, null);
        localObject2 = new cn/jpush/android/b/k;
        ((k)localObject2).<init>();
        a((View)localObject1, (l)localObject2);
      }
      catch (Throwable localThrowable) {}
    }
    if ((a == -1) || (b == -1))
    {
      localObject2 = new android/app/Notification$Builder;
      ((Notification.Builder)localObject2).<init>(paramContext);
      ((Notification.Builder)localObject2).setContentTitle("title");
      ((Notification.Builder)localObject2).setContentText("content");
      if (Build.VERSION.SDK_INT >= 16) {
        localObject1 = ((Notification.Builder)localObject2).build();
      } else {
        localObject1 = ((Notification.Builder)localObject2).getNotification();
      }
      if (Build.VERSION.SDK_INT >= 24) {
        localObject1 = ((Notification.Builder)localObject2).createContentView();
      } else {
        localObject1 = ((Notification)localObject1).contentView;
      }
      localObject2 = new android/widget/FrameLayout;
      ((FrameLayout)localObject2).<init>(paramContext);
      localObject1 = (ViewGroup)((RemoteViews)localObject1).apply(paramContext, (ViewGroup)localObject2);
      paramContext = new cn/jpush/android/b/j;
      paramContext.<init>();
      a((View)localObject1, paramContext);
    }
  }
  
  private static void a(View paramView, l paraml)
  {
    if ((paramView != null) && (paraml != null))
    {
      paraml.a(paramView);
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        for (int i = 0; i < paramView.getChildCount(); i++) {
          a(paramView.getChildAt(i), paraml);
        }
      }
    }
  }
  
  public static int b()
  {
    return b;
  }
  
  public static float c()
  {
    return d;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/b/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */