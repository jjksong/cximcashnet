package cn.jpush.android.api;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.b.c;
import cn.jpush.android.b.r;
import cn.jpush.android.data.JPushLocalNotification;
import cn.jpush.android.service.ServiceInterface;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JPushInterface
{
  public static final String ACTION_CONNECTION_CHANGE = "cn.jpush.android.intent.CONNECTION";
  public static final String ACTION_MESSAGE_RECEIVED = "cn.jpush.android.intent.MESSAGE_RECEIVED";
  public static final String ACTION_NOTIFICATION_CLICK_ACTION = "cn.jpush.android.intent.NOTIFICATION_CLICK_ACTION";
  public static final String ACTION_NOTIFICATION_OPENED = "cn.jpush.android.intent.NOTIFICATION_OPENED";
  public static final String ACTION_NOTIFICATION_RECEIVED = "cn.jpush.android.intent.NOTIFICATION_RECEIVED";
  public static final String ACTION_NOTIFICATION_RECEIVED_PROXY = "cn.jpush.android.intent.NOTIFICATION_RECEIVED_PROXY";
  public static final String ACTION_REGISTRATION_ID = "cn.jpush.android.intent.REGISTRATION";
  public static final String ACTION_RICHPUSH_CALLBACK = "cn.jpush.android.intent.ACTION_RICHPUSH_CALLBACK";
  public static final String EXTRA_ACTIVITY_PARAM = "cn.jpush.android.ACTIVITY_PARAM";
  public static final String EXTRA_ALERT = "cn.jpush.android.ALERT";
  public static final String EXTRA_ALERT_TYPE = "cn.jpush.android.ALERT_TYPE";
  public static final String EXTRA_APP_KEY = "cn.jpush.android.APPKEY";
  public static final String EXTRA_BIG_PIC_PATH = "cn.jpush.android.BIG_PIC_PATH";
  public static final String EXTRA_BIG_TEXT = "cn.jpush.android.BIG_TEXT";
  public static final String EXTRA_CONNECTION_CHANGE = "cn.jpush.android.CONNECTION_CHANGE";
  public static final String EXTRA_CONTENT_TYPE = "cn.jpush.android.CONTENT_TYPE";
  public static final String EXTRA_EXTRA = "cn.jpush.android.EXTRA";
  public static final String EXTRA_INBOX = "cn.jpush.android.INBOX";
  public static final String EXTRA_MESSAGE = "cn.jpush.android.MESSAGE";
  public static final String EXTRA_MSG_ID = "cn.jpush.android.MSG_ID";
  public static final String EXTRA_NOTIFICATION_ACTION_EXTRA = "cn.jpush.android.NOTIFIACATION_ACTION_EXTRA";
  public static final String EXTRA_NOTIFICATION_DEVELOPER_ARG0 = "cn.jpush.android.NOTIFICATION_DEVELOPER_ARG0";
  public static final String EXTRA_NOTIFICATION_ID = "cn.jpush.android.NOTIFICATION_ID";
  public static final String EXTRA_NOTIFICATION_LARGET_ICON = "cn.jpush.android.NOTIFICATION_LARGE_ICON";
  public static final String EXTRA_NOTIFICATION_TITLE = "cn.jpush.android.NOTIFICATION_CONTENT_TITLE";
  public static final String EXTRA_NOTIFICATION_URL = "cn.jpush.android.NOTIFICATION_URL";
  public static final String EXTRA_NOTI_CATEGORY = "cn.jpush.android.NOTI_CATEGORY";
  public static final String EXTRA_NOTI_PRIORITY = "cn.jpush.android.NOTI_PRIORITY";
  public static final String EXTRA_NOTI_TYPE = "cn.jpush.android.NOTIFICATION_TYPE";
  public static final String EXTRA_PUSH_ID = "cn.jpush.android.PUSH_ID";
  public static final String EXTRA_REGISTRATION_ID = "cn.jpush.android.REGISTRATION_ID";
  public static final String EXTRA_RICHPUSH_FILE_PATH = "cn.jpush.android.FILE_PATH";
  public static final String EXTRA_RICHPUSH_FILE_TYPE = "cn.jpush.android.FILE_TYPE";
  public static final String EXTRA_RICHPUSH_HTML_PATH = "cn.jpush.android.HTML_PATH";
  public static final String EXTRA_RICHPUSH_HTML_RES = "cn.jpush.android.HTML_RES";
  public static final String EXTRA_STATUS = "cn.jpush.android.STATUS";
  public static final String EXTRA_TITLE = "cn.jpush.android.TITLE";
  public static int a = 5;
  private static final Integer b = Integer.valueOf(0);
  
  static
  {
    JCoreInterface.initActionExtra(cn.jpush.android.a.a, cn.jpush.android.b.d.class);
    JCoreInterface.initAction(cn.jpush.android.a.a, c.class);
  }
  
  private static PushNotificationBuilder a(String paramString)
  {
    Context localContext = cn.jpush.android.a.e;
    StringBuilder localStringBuilder = new StringBuilder("jpush_save_custom_builder");
    localStringBuilder.append(paramString);
    paramString = MultiSpHelper.getString(localContext, localStringBuilder.toString(), "");
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    if ((!paramString.startsWith("basic")) && (!paramString.startsWith("custom"))) {
      return MultiActionsNotificationBuilder.parseFromPreference(paramString);
    }
    return BasicPushNotificationBuilder.a(paramString);
  }
  
  private static void a(Context paramContext)
  {
    if (paramContext != null) {
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  private static void a(Context paramContext, boolean paramBoolean, String paramString)
  {
    MultiSpHelper.commitBoolean(paramContext, "notification_enabled", paramBoolean);
    if (!paramBoolean)
    {
      cn.jpush.android.e.g.a("JPushInterface", "action:setPushTime - closed");
      return;
    }
    Object localObject = new StringBuilder("([0-6]{0,7})_((");
    ((StringBuilder)localObject).append("([0-9]|1[0-9]|2[0-3])\\^([0-9]|1[0-9]|2[0-3])");
    ((StringBuilder)localObject).append(")|(");
    ((StringBuilder)localObject).append("([0-9]|1[0-9]|2[0-3])\\^([0-9]|1[0-9]|2[0-3])");
    ((StringBuilder)localObject).append("-)+(");
    ((StringBuilder)localObject).append("([0-9]|1[0-9]|2[0-3])\\^([0-9]|1[0-9]|2[0-3])");
    ((StringBuilder)localObject).append("))");
    if (Pattern.compile(((StringBuilder)localObject).toString()).matcher(paramString).matches())
    {
      localObject = cn.jpush.android.b.b(paramContext);
      if (paramString.equals(localObject))
      {
        paramContext = new StringBuilder("Already SetPushTime, give up - ");
        paramContext.append((String)localObject);
        cn.jpush.android.e.g.a("JPushInterface", paramContext.toString());
        return;
      }
      localObject = new StringBuilder("action:setPushTime - enabled:");
      ((StringBuilder)localObject).append(paramBoolean);
      ((StringBuilder)localObject).append(", pushTime:");
      ((StringBuilder)localObject).append(paramString);
      cn.jpush.android.e.g.a("JPushInterface", ((StringBuilder)localObject).toString());
      cn.jpush.android.b.b(paramContext, paramString, false);
      return;
    }
    paramContext = new StringBuilder("Invalid time format - ");
    paramContext.append(paramString);
    cn.jpush.android.e.g.d("JPushInterface", paramContext.toString());
  }
  
  static boolean a(int paramInt)
  {
    if (paramInt <= 0) {
      return false;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramInt);
    if (a(localStringBuilder.toString()) == null)
    {
      localStringBuilder = new StringBuilder("The builder with id:");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" has not been set in your app, use default builder!");
      cn.jpush.android.e.g.c("JPushInterface", localStringBuilder.toString());
      return false;
    }
    return true;
  }
  
  public static void addLocalNotification(Context paramContext, JPushLocalNotification paramJPushLocalNotification)
  {
    a(paramContext);
    cn.jpush.android.service.a.a(paramContext).a(paramContext, paramJPushLocalNotification, false);
  }
  
  public static void addTags(Context paramContext, int paramInt, Set<String> paramSet)
  {
    a(paramContext);
    r.a(paramContext, paramInt, paramSet, 1, 1);
  }
  
  static PushNotificationBuilder b(int paramInt)
  {
    int i = paramInt;
    if (paramInt <= 0) {
      i = b.intValue();
    }
    Object localObject1 = null;
    try
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(i);
      localObject2 = a(((StringBuilder)localObject2).toString());
      localObject1 = localObject2;
    }
    catch (Exception localException)
    {
      Object localObject2;
      for (;;) {}
    }
    localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = new DefaultPushNotificationBuilder();
    }
    return (PushNotificationBuilder)localObject2;
  }
  
  public static void checkTagBindState(Context paramContext, int paramInt, String paramString)
  {
    a(paramContext);
    if (!TextUtils.isEmpty(paramString))
    {
      HashSet localHashSet = new HashSet();
      localHashSet.add(paramString);
      paramString = localHashSet;
    }
    else
    {
      paramString = null;
    }
    r.a(paramContext, paramInt, paramString, 1, 6);
  }
  
  public static void cleanTags(Context paramContext, int paramInt)
  {
    a(paramContext);
    r.a(paramContext, paramInt, new HashSet(), 1, 4);
  }
  
  public static void clearAllNotifications(Context paramContext)
  {
    a(paramContext);
    ServiceInterface.b(paramContext);
  }
  
  public static void clearLocalNotifications(Context paramContext)
  {
    a(paramContext);
    Bundle localBundle = new Bundle();
    cn.jpush.android.service.e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
    localBundle.putInt("multi_type", 8);
    JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
  }
  
  public static void clearNotificationById(Context paramContext, int paramInt)
  {
    a(paramContext);
    ((NotificationManager)paramContext.getSystemService("notification")).cancel(paramInt);
  }
  
  public static void deleteAlias(Context paramContext, int paramInt)
  {
    a(paramContext);
    r.a(paramContext, paramInt, null, 2, 3);
  }
  
  public static void deleteTags(Context paramContext, int paramInt, Set<String> paramSet)
  {
    a(paramContext);
    r.a(paramContext, paramInt, paramSet, 1, 3);
  }
  
  public static Set<String> filterValidTags(Set<String> paramSet)
  {
    return r.a(paramSet);
  }
  
  public static void getAlias(Context paramContext, int paramInt)
  {
    a(paramContext);
    r.a(paramContext, paramInt, null, 2, 5);
  }
  
  public static void getAllTags(Context paramContext, int paramInt)
  {
    a(paramContext);
    r.a(paramContext, paramInt, new HashSet(), 1, 5);
  }
  
  public static boolean getConnectionState(Context paramContext)
  {
    a(paramContext);
    return JCoreInterface.getConnectionState(paramContext);
  }
  
  public static String getRegistrationID(Context paramContext)
  {
    a(paramContext);
    return JCoreInterface.getRegistrationID(paramContext);
  }
  
  public static String getStringTags(Set<String> paramSet)
  {
    return r.b(paramSet);
  }
  
  public static String getUdid(Context paramContext)
  {
    a(paramContext);
    return JCoreInterface.getDeviceId(paramContext);
  }
  
  public static void init(Context paramContext)
  {
    StringBuilder localStringBuilder = new StringBuilder("action:init - sdkVersion:");
    localStringBuilder.append(ServiceInterface.a());
    localStringBuilder.append(", buildId:445");
    cn.jpush.android.e.g.a("JPushInterface", localStringBuilder.toString());
    a(paramContext);
    JCoreInterface.si(paramContext, 4098, null);
    if (!cn.jpush.android.a.a(paramContext)) {
      return;
    }
    if ((JCoreInterface.getDebugMode()) && (!cn.jpush.android.e.a.a(paramContext))) {
      cn.jpush.android.e.g.a("JPushInterface", "检测到当前没有网络。长连接将在有网络时自动继续建立。");
    }
    if (cn.jpush.android.b.a(paramContext) == -1) {
      setLatestNotificationNumber(paramContext, a);
    }
  }
  
  public static void initCrashHandler(Context paramContext)
  {
    a(paramContext);
    JCoreInterface.initCrashHandler(paramContext);
  }
  
  public static boolean isPushStopped(Context paramContext)
  {
    a(paramContext);
    return ServiceInterface.c(paramContext);
  }
  
  public static void onFragmentPause(Context paramContext, String paramString)
  {
    a(paramContext);
    JCoreInterface.onFragmentPause(paramContext, paramString);
  }
  
  public static void onFragmentResume(Context paramContext, String paramString)
  {
    a(paramContext);
    JCoreInterface.onFragmentResume(paramContext, paramString);
  }
  
  public static void onKillProcess(Context paramContext)
  {
    JCoreInterface.onKillProcess(paramContext);
  }
  
  public static void onPause(Context paramContext)
  {
    a(paramContext);
    JCoreInterface.onPause(paramContext);
  }
  
  public static void onResume(Context paramContext)
  {
    a(paramContext);
    JCoreInterface.onResume(paramContext);
  }
  
  public static void removeLocalNotification(Context paramContext, long paramLong)
  {
    a(paramContext);
    Bundle localBundle = new Bundle();
    cn.jpush.android.service.e.a(paramContext, localBundle, "intent.MULTI_PROCESS");
    localBundle.putInt("multi_type", 7);
    localBundle.putLong("local_notification_id", paramLong);
    JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
  }
  
  public static void reportNotificationOpened(Context paramContext, String paramString)
  {
    a(paramContext);
    if (TextUtils.isEmpty(paramString))
    {
      StringBuilder localStringBuilder = new StringBuilder("The msgId is not valid - ");
      localStringBuilder.append(paramString);
      cn.jpush.android.e.g.d("JPushInterface", localStringBuilder.toString());
    }
    cn.jpush.android.b.e.a(paramString, 1028, null, paramContext);
  }
  
  public static void reportNotificationOpened(Context paramContext, String paramString, byte paramByte)
  {
    a(paramContext);
    if (TextUtils.isEmpty(paramString))
    {
      StringBuilder localStringBuilder = new StringBuilder("The msgId is not valid - ");
      localStringBuilder.append(paramString);
      cn.jpush.android.e.g.d("JPushInterface", localStringBuilder.toString());
    }
    cn.jpush.android.b.e.a(paramString, "", paramByte, 1000, paramContext);
  }
  
  public static void requestPermission(Context paramContext)
  {
    if (paramContext == null)
    {
      cn.jpush.android.e.g.c("JPushInterface", "[requestPermission] unexcepted - context was null");
      return;
    }
    JCoreInterface.requestPermission(paramContext);
  }
  
  public static void resumePush(Context paramContext)
  {
    cn.jpush.android.e.g.a("JPushInterface", "action:resumePush");
    a(paramContext);
    ServiceInterface.b(paramContext, 1);
    cn.jpush.android.d.d.a().b(paramContext);
  }
  
  public static void setAlias(Context paramContext, int paramInt, String paramString)
  {
    a(paramContext);
    r.a(paramContext, paramInt, paramString, 2, 2);
  }
  
  @Deprecated
  public static void setAlias(Context paramContext, String paramString, TagAliasCallback paramTagAliasCallback)
  {
    a(paramContext);
    setAliasAndTags(paramContext, paramString, null, paramTagAliasCallback);
  }
  
  @Deprecated
  public static void setAliasAndTags(Context paramContext, String paramString, Set<String> paramSet, TagAliasCallback paramTagAliasCallback)
  {
    a(paramContext);
    r.a(paramContext, paramString, paramSet, paramTagAliasCallback, 0, 0);
  }
  
  public static void setChannel(Context paramContext, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("arg1", paramString);
    JCoreInterface.si(paramContext, 4096, localBundle);
  }
  
  public static void setDaemonAction(String paramString)
  {
    JCoreInterface.setDaemonAction(paramString);
  }
  
  public static void setDebugMode(boolean paramBoolean)
  {
    JCoreInterface.setDebugMode(paramBoolean);
  }
  
  public static void setDefaultPushNotificationBuilder(DefaultPushNotificationBuilder paramDefaultPushNotificationBuilder)
  {
    if (paramDefaultPushNotificationBuilder != null)
    {
      ServiceInterface.a(cn.jpush.android.a.e, b, paramDefaultPushNotificationBuilder);
      return;
    }
    throw new IllegalArgumentException("NULL notification");
  }
  
  public static void setGeofenceInterval(Context paramContext, long paramLong)
  {
    a(paramContext);
    if ((paramLong >= 180000L) && (paramLong <= 86400000L))
    {
      Bundle localBundle = new Bundle();
      cn.jpush.android.service.e.a(paramContext, localBundle, "intent.SET_GEOFENCE_INTERVAL");
      localBundle.putLong("interval", paramLong);
      JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
      return;
    }
    cn.jpush.android.e.g.d("JPushInterface", "Invalid interval, it should be a ms number between 3 mins and 1 day!");
  }
  
  public static void setLatestNotificationNumber(Context paramContext, int paramInt)
  {
    a(paramContext);
    StringBuilder localStringBuilder = new StringBuilder("action:setLatestNotificationNumber : ");
    localStringBuilder.append(paramInt);
    cn.jpush.android.e.g.a("JPushInterface", localStringBuilder.toString());
    if (paramInt <= 0)
    {
      cn.jpush.android.e.g.d("JPushInterface", "maxNum should > 0, Give up action..");
      return;
    }
    ServiceInterface.c(paramContext, paramInt);
  }
  
  public static void setMaxGeofenceNumber(Context paramContext, int paramInt)
  {
    a(paramContext);
    if ((paramInt > 0) && (paramInt <= 100))
    {
      Bundle localBundle = new Bundle();
      cn.jpush.android.service.e.a(paramContext, localBundle, "intent.SET_GEOFENCE_MAXNUM");
      localBundle.putInt("geofence_num", paramInt);
      JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, localBundle);
      return;
    }
    cn.jpush.android.e.g.d("JPushInterface", "Invalid maxNumber,it should be a number between 1 and 100!");
  }
  
  public static void setMobileNumber(Context paramContext, int paramInt, String paramString)
  {
    a(paramContext);
    cn.jpush.android.b.g.a();
    Object localObject = new StringBuilder("action - setMobileNubmer, sequence:");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(",mobileNumber:");
    ((StringBuilder)localObject).append(paramString);
    cn.jpush.android.e.g.a("MobileNumberHelper", ((StringBuilder)localObject).toString());
    if (ServiceInterface.d(paramContext))
    {
      cn.jpush.android.b.g.a(paramContext, paramInt, b.m, paramString);
      return;
    }
    localObject = paramContext;
    if (!(paramContext instanceof Application)) {
      localObject = paramContext.getApplicationContext();
    }
    if (!cn.jpush.android.a.a((Context)localObject))
    {
      cn.jpush.android.b.g.a((Context)localObject, paramInt, b.j, paramString);
      return;
    }
    paramContext = new Bundle();
    paramContext.putString("action", "intent.MOBILE_NUMBER");
    paramContext.putInt("sequence", paramInt);
    paramContext.putString("mobile_number", paramString);
    JCoreInterface.sendAction((Context)localObject, cn.jpush.android.a.a, paramContext);
  }
  
  public static void setPowerSaveMode(Context paramContext, boolean paramBoolean)
  {
    JCoreInterface.setPowerSaveMode(paramContext, paramBoolean);
  }
  
  public static void setPushNotificationBuilder(Integer paramInteger, DefaultPushNotificationBuilder paramDefaultPushNotificationBuilder)
  {
    if (paramDefaultPushNotificationBuilder != null)
    {
      if (paramInteger.intValue() <= 0)
      {
        cn.jpush.android.e.g.d("JPushInterface", "id should be larger than 0");
        return;
      }
      ServiceInterface.a(cn.jpush.android.a.e, paramInteger, paramDefaultPushNotificationBuilder);
      return;
    }
    throw new IllegalArgumentException("NULL pushNotificationBuilder");
  }
  
  public static void setPushTime(Context paramContext, Set<Integer> paramSet, int paramInt1, int paramInt2)
  {
    a(paramContext);
    if ((JCoreInterface.getDebugMode()) && (!cn.jpush.android.e.a.a(paramContext))) {
      cn.jpush.android.e.g.a("JPushInterface", "检测到当前没有网络。此动作将在有网络时自动继续执行。");
    }
    if (paramSet == null)
    {
      a(paramContext, true, "0123456_0^23");
      return;
    }
    if ((paramSet.size() != 0) && (!paramSet.isEmpty()))
    {
      if (paramInt1 > paramInt2)
      {
        cn.jpush.android.e.g.d("JPushInterface", "Invalid time format - startHour should less than endHour");
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = paramSet.iterator();
      while (localIterator.hasNext())
      {
        paramSet = (Integer)localIterator.next();
        if ((paramSet.intValue() <= 6) && (paramSet.intValue() >= 0))
        {
          localStringBuilder.append(paramSet);
        }
        else
        {
          paramContext = new StringBuilder("Invalid day format - ");
          paramContext.append(paramSet);
          cn.jpush.android.e.g.d("JPushInterface", paramContext.toString());
          return;
        }
      }
      localStringBuilder.append("_");
      localStringBuilder.append(paramInt1);
      localStringBuilder.append("^");
      localStringBuilder.append(paramInt2);
      a(paramContext, true, localStringBuilder.toString());
      return;
    }
    a(paramContext, false, "0123456_0^23");
  }
  
  public static void setSilenceTime(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a(paramContext);
    if ((paramInt1 >= 0) && (paramInt2 >= 0) && (paramInt3 >= 0) && (paramInt4 >= 0) && (paramInt2 <= 59) && (paramInt4 <= 59) && (paramInt3 <= 23) && (paramInt1 <= 23))
    {
      if ((paramInt1 == 0) && (paramInt2 == 0) && (paramInt3 == 0) && (paramInt4 == 0))
      {
        ServiceInterface.a(paramContext, "");
        cn.jpush.android.e.g.a("JPushInterface", "Remove the silence time!");
        return;
      }
      if (ServiceInterface.a(paramContext, paramInt1, paramInt2, paramInt3, paramInt4))
      {
        paramContext = new StringBuilder("Set Silence PushTime - ");
        paramContext.append(paramInt1);
        paramContext.append(" : ");
        paramContext.append(paramInt2);
        paramContext.append(" -- ");
        paramContext.append(paramInt3);
        paramContext.append(" : ");
        paramContext.append(paramInt4);
        cn.jpush.android.e.g.a("JPushInterface", paramContext.toString());
        return;
      }
      cn.jpush.android.e.g.d("JPushInterface", "Set Silence PushTime Failed");
      return;
    }
    cn.jpush.android.e.g.d("JPushInterface", "Invalid parameter format, startHour and endHour should between 0 ~ 23, startMins and endMins should between 0 ~ 59. ");
  }
  
  public static void setStatisticsEnable(boolean paramBoolean) {}
  
  public static void setStatisticsSessionTimeout(long paramLong)
  {
    if (paramLong < 10L)
    {
      cn.jpush.android.e.g.c("JPushInterface", "sesseion timeout less than 10s");
      return;
    }
    if (paramLong > 86400L) {
      cn.jpush.android.e.g.c("JPushInterface", "sesseion timeout larger than 1day");
    }
  }
  
  public static void setTags(Context paramContext, int paramInt, Set<String> paramSet)
  {
    a(paramContext);
    r.a(paramContext, paramInt, paramSet, 1, 2);
  }
  
  @Deprecated
  public static void setTags(Context paramContext, Set<String> paramSet, TagAliasCallback paramTagAliasCallback)
  {
    a(paramContext);
    setAliasAndTags(paramContext, null, paramSet, paramTagAliasCallback);
  }
  
  public static void stopCrashHandler(Context paramContext)
  {
    a(paramContext);
    JCoreInterface.stopCrashHandler(paramContext);
  }
  
  public static void stopPush(Context paramContext)
  {
    cn.jpush.android.e.g.a("JPushInterface", "action:stopPush");
    a(paramContext);
    ServiceInterface.a(paramContext, 1);
    cn.jpush.android.d.d.a().c(paramContext);
  }
  
  @Deprecated
  public void setAliasAndTags(Context paramContext, String paramString, Set<String> paramSet)
  {
    a(paramContext);
    r.a(paramContext, paramString, paramSet, null, 0, 0);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/JPushInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */