package cn.jpush.android.api;

import cn.jpush.android.e.g;

public class ThirdLogger
{
  public static void d(String paramString1, String paramString2) {}
  
  public static void d(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void dd(String paramString1, String paramString2)
  {
    g.a(paramString1, paramString2);
  }
  
  public static void dd(String paramString1, String paramString2, Throwable paramThrowable)
  {
    g.a(paramString1, paramString2, paramThrowable);
  }
  
  public static void e(String paramString1, String paramString2) {}
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void ee(String paramString1, String paramString2)
  {
    g.d(paramString1, paramString2);
  }
  
  public static void ee(String paramString1, String paramString2, Throwable paramThrowable)
  {
    g.d(paramString1, paramString2, paramThrowable);
  }
  
  public static void i(String paramString1, String paramString2) {}
  
  public static void i(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void ii(String paramString1, String paramString2)
  {
    g.b(paramString1, paramString2);
  }
  
  public static void ii(String paramString1, String paramString2, Throwable paramThrowable)
  {
    g.b(paramString1, paramString2, paramThrowable);
  }
  
  public static void v(String paramString1, String paramString2) {}
  
  public static void v(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void vv(String paramString1, String paramString2) {}
  
  public static void vv(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void w(String paramString1, String paramString2) {}
  
  public static void w(String paramString1, String paramString2, Throwable paramThrowable) {}
  
  public static void ww(String paramString1, String paramString2)
  {
    g.c(paramString1, paramString2);
  }
  
  public static void ww(String paramString1, String paramString2, Throwable paramThrowable)
  {
    g.c(paramString1, paramString2, paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/ThirdLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */