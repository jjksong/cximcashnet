package cn.jpush.android.api;

import java.io.Serializable;
import java.util.Set;

public class JPushMessage
  implements Serializable
{
  private String a;
  private Set<String> b;
  private String c;
  private int d;
  private boolean e;
  private boolean f;
  private int g;
  private String h;
  
  public String getAlias()
  {
    return this.a;
  }
  
  public String getCheckTag()
  {
    return this.c;
  }
  
  public int getErrorCode()
  {
    return this.d;
  }
  
  public String getMobileNumber()
  {
    return this.h;
  }
  
  public int getSequence()
  {
    return this.g;
  }
  
  public boolean getTagCheckStateResult()
  {
    return this.e;
  }
  
  public Set<String> getTags()
  {
    return this.b;
  }
  
  public boolean isTagCheckOperator()
  {
    return this.f;
  }
  
  public void setAlias(String paramString)
  {
    this.a = paramString;
  }
  
  public void setCheckTag(String paramString)
  {
    this.c = paramString;
  }
  
  public void setErrorCode(int paramInt)
  {
    this.d = paramInt;
  }
  
  public void setMobileNumber(String paramString)
  {
    this.h = paramString;
  }
  
  public void setSequence(int paramInt)
  {
    this.g = paramInt;
  }
  
  public void setTagCheckOperator(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  public void setTagCheckStateResult(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }
  
  public void setTags(Set<String> paramSet)
  {
    this.b = paramSet;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("JPushMessage{alias='");
    localStringBuilder.append(this.a);
    localStringBuilder.append('\'');
    localStringBuilder.append(", tags=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", checkTag='");
    localStringBuilder.append(this.c);
    localStringBuilder.append('\'');
    localStringBuilder.append(", errorCode=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", tagCheckStateResult=");
    localStringBuilder.append(this.e);
    localStringBuilder.append(", isTagCheckOperator=");
    localStringBuilder.append(this.f);
    localStringBuilder.append(", sequence=");
    localStringBuilder.append(this.g);
    localStringBuilder.append(", mobileNumber=");
    localStringBuilder.append(this.h);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/JPushMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */