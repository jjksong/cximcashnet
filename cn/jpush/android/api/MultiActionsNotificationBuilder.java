package cn.jpush.android.api;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.a;
import cn.jpush.android.e.g;
import cn.jpush.android.service.PushReceiver;
import cn.jpush.android.ui.PopWinActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MultiActionsNotificationBuilder
  extends DefaultPushNotificationBuilder
{
  private static final String NOTI_ACT_EXTRA_STR = "notification_action_extra_string";
  private static final String NOTI_ACT_RES_ID = "notification_action_res_id";
  private static final String NOTI_ACT_TEXT = "notification_action_text";
  private static final String TAG = "MultiActionsNotificationBuilder";
  private JSONArray actionJSONArray = new JSONArray();
  protected Context mContext;
  
  public MultiActionsNotificationBuilder(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  static PushNotificationBuilder parseFromPreference(String paramString)
  {
    MultiActionsNotificationBuilder localMultiActionsNotificationBuilder = new MultiActionsNotificationBuilder(a.e);
    try
    {
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>(paramString);
      localMultiActionsNotificationBuilder.actionJSONArray = localJSONArray;
    }
    catch (JSONException paramString)
    {
      g.c("MultiActionsNotificationBuilder", "Parse builder from preference failed!");
      paramString.printStackTrace();
    }
    return localMultiActionsNotificationBuilder;
  }
  
  public void addJPushAction(int paramInt, String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("notification_action_res_id", paramInt);
      localJSONObject.put("notification_action_text", paramString1);
      localJSONObject.put("notification_action_extra_string", paramString2);
      this.actionJSONArray.put(localJSONObject);
      return;
    }
    catch (JSONException paramString1)
    {
      g.c("MultiActionsNotificationBuilder", "Construct action failed!");
      paramString1.printStackTrace();
    }
  }
  
  @TargetApi(11)
  Notification getNotification(Notification.Builder paramBuilder)
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      for (int i = 0; i < this.actionJSONArray.length(); i++) {
        try
        {
          JSONObject localJSONObject = this.actionJSONArray.getJSONObject(i);
          Object localObject = new android/content/Intent;
          ((Intent)localObject).<init>("cn.jpush.android.intent.NOTIFICATION_CLICK_ACTION_PROXY");
          ((Intent)localObject).putExtra("cn.jpush.android.NOTIFIACATION_ACTION_EXTRA", localJSONObject.getString("notification_action_extra_string"));
          if (JCoreInterface.getRuningFlag())
          {
            ((Intent)localObject).setClass(this.mContext, PopWinActivity.class);
            ((Intent)localObject).putExtra("isNotification", true);
            localObject = PendingIntent.getActivity(this.mContext, i, (Intent)localObject, 134217728);
          }
          else
          {
            ((Intent)localObject).setClass(this.mContext, PushReceiver.class);
            localObject = PendingIntent.getBroadcast(this.mContext, i, (Intent)localObject, 134217728);
          }
          paramBuilder.addAction(localJSONObject.getInt("notification_action_res_id"), localJSONObject.getString("notification_action_text"), (PendingIntent)localObject).setAutoCancel(true);
        }
        catch (JSONException localJSONException)
        {
          g.c("MultiActionsNotificationBuilder", "Parse Action from preference preference failed!");
          localJSONException.printStackTrace();
        }
      }
      return paramBuilder.build();
    }
    return paramBuilder.getNotification();
  }
  
  public String toString()
  {
    return this.actionJSONArray.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/MultiActionsNotificationBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */