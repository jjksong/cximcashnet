package cn.jpush.android.api;

import android.app.ListActivity;

public class InstrumentedListActivity
  extends ListActivity
{
  protected void onPause()
  {
    super.onPause();
  }
  
  protected void onResume()
  {
    super.onResume();
  }
  
  public void onStart()
  {
    super.onStart();
  }
  
  public void onStop()
  {
    super.onStop();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/InstrumentedListActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */