package cn.jpush.android.api;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.Builder;
import android.content.Context;
import android.os.Build.VERSION;
import cn.jpush.android.a;

public class BasicPushNotificationBuilder
  extends DefaultPushNotificationBuilder
{
  protected Context a;
  public String developerArg0 = "developerArg0";
  public int notificationDefaults = -2;
  public int notificationFlags = 16;
  public int statusBarDrawable = a.b;
  
  public BasicPushNotificationBuilder(Context paramContext)
  {
    if (paramContext != null)
    {
      this.a = paramContext;
      return;
    }
    throw new IllegalArgumentException("NULL context");
  }
  
  static PushNotificationBuilder a(String paramString)
  {
    String[] arrayOfString = paramString.split("_____");
    paramString = arrayOfString[0];
    if ("basic".equals(paramString)) {
      paramString = new BasicPushNotificationBuilder(a.e);
    } else if ("custom".equals(paramString)) {
      paramString = new CustomPushNotificationBuilder(a.e);
    } else {
      paramString = new BasicPushNotificationBuilder(a.e);
    }
    paramString.a(arrayOfString);
    return paramString;
  }
  
  String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.notificationDefaults);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.notificationFlags);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.statusBarDrawable);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.developerArg0);
    return localStringBuilder.toString();
  }
  
  void a(String[] paramArrayOfString)
  {
    this.notificationDefaults = Integer.parseInt(paramArrayOfString[1]);
    this.notificationFlags = Integer.parseInt(paramArrayOfString[2]);
    this.statusBarDrawable = Integer.parseInt(paramArrayOfString[3]);
    if (5 == paramArrayOfString.length) {
      this.developerArg0 = paramArrayOfString[4];
    }
  }
  
  public String getDeveloperArg0()
  {
    return this.developerArg0;
  }
  
  @TargetApi(11)
  Notification getNotification(Notification.Builder paramBuilder)
  {
    int i = this.notificationDefaults;
    if (i != -2) {
      paramBuilder.setDefaults(i);
    }
    paramBuilder.setSmallIcon(this.statusBarDrawable);
    if (Build.VERSION.SDK_INT >= 16) {
      paramBuilder = paramBuilder.build();
    } else {
      paramBuilder = paramBuilder.getNotification();
    }
    paramBuilder.flags = (this.notificationFlags | 0x1);
    return paramBuilder;
  }
  
  void resetNotificationParams(Notification paramNotification)
  {
    paramNotification.defaults = this.notificationDefaults;
    paramNotification.flags = this.notificationFlags;
    paramNotification.icon = this.statusBarDrawable;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("basic_____");
    localStringBuilder.append(a());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/BasicPushNotificationBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */