package cn.jpush.android.api;

import android.app.Notification.Builder;
import android.content.Context;
import android.os.Bundle;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.d.d;
import cn.jpush.android.service.e;

public abstract class JThirdPlatFormInterface
{
  public static final String ACTION_NOTIFICATION_ARRIVED = "action_notification_arrived";
  public static final String ACTION_NOTIFICATION_CLCKED = "action_notification_clicked";
  public static final String ACTION_NOTIFICATION_SHOW = "action_notification_show";
  public static final String ACTION_PLUGIN_PALTFORM_ON_MESSAGING = "intent.plugin.platform.ON_MESSAGING";
  public static final String ACTION_PLUGIN_PALTFORM_REFRESSH_REGID = "intent.plugin.platform.REFRESSH_REGID";
  public static final String ACTION_PLUGIN_PALTFORM_REQUEST_REGID = "intent.plugin.platform.REQUEST_REGID";
  public static final String ACTION_REGISTER_TOKEN = "action_register_token";
  public static final String KEY_DATA = "data";
  public static final String KEY_MSG_ID = "msg_id";
  public static final String KEY_NOTI_ID = "noti_id";
  public static final String KEY_PLATFORM = "platform";
  public static final String KEY_TOKEN = "token";
  
  public static void doAction(Context paramContext, String paramString, Bundle paramBundle)
  {
    d.a().a(paramContext, paramString, paramBundle);
  }
  
  public static int getNofiticationID(String paramString, int paramInt)
  {
    return c.a(paramString, paramInt);
  }
  
  public static void sendActionByJCore(Context paramContext, Bundle paramBundle, String paramString)
  {
    e.a(paramContext, paramBundle, paramString);
    JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, paramBundle);
  }
  
  public static void setNotificationChannel(Context paramContext, Notification.Builder paramBuilder, String paramString, CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    c.a(paramContext, paramBuilder, paramInt1, paramInt2);
  }
  
  public static String toMD5(String paramString)
  {
    return cn.jpush.android.e.a.a(paramString);
  }
  
  public abstract String getAppId(Context paramContext);
  
  public abstract String getAppkey(Context paramContext);
  
  public abstract String getRomName();
  
  public abstract byte getRomType(Context paramContext);
  
  public abstract String getToken(Context paramContext);
  
  public abstract void init(Context paramContext);
  
  public abstract boolean isNeedClearToken(Context paramContext);
  
  public abstract boolean isSupport(Context paramContext);
  
  public boolean needSendToMainProcess()
  {
    return false;
  }
  
  public abstract void register(Context paramContext);
  
  public void resumePush(Context paramContext) {}
  
  public void stopPush(Context paramContext) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/JThirdPlatFormInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */