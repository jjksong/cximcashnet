package cn.jpush.android.api;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.BigPictureStyle;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.Notification.InboxStyle;
import android.app.Notification.Style;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.widget.RemoteViews;
import cn.jpush.android.a;
import cn.jpush.android.b.e;
import cn.jpush.android.b.i;
import cn.jpush.android.e.g;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONObject;

public class DefaultPushNotificationBuilder
  implements PushNotificationBuilder
{
  public static final int BIULDER_ID_CUSTOM_DEEPLINK = 1001;
  private static final String TAG = "DefaultPushNotificationBuilder";
  private static boolean hasCreateNotificationChannel = false;
  
  private RemoteViews buildDiyRemoteViews(String paramString1, String paramString2, String paramString3, Map<String, Object> paramMap)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      i.a(a.e);
    }
    Context localContext = a.e;
    int i2 = localContext.getResources().getIdentifier("push_notification", "layout", localContext.getPackageName());
    int i3 = localContext.getResources().getIdentifier("push_root_view", "id", localContext.getPackageName());
    int n = localContext.getResources().getIdentifier("push_notification_title", "id", localContext.getPackageName());
    int j = localContext.getResources().getIdentifier("push_notification_content", "id", localContext.getPackageName());
    int k = localContext.getResources().getIdentifier("push_notification_big_icon", "id", localContext.getPackageName());
    int m = localContext.getResources().getIdentifier("push_notification_small_icon", "id", localContext.getPackageName());
    int i = localContext.getResources().getIdentifier("push_notification_date", "id", localContext.getPackageName());
    int i1 = localContext.getResources().getIdentifier("push_notification_dot", "id", localContext.getPackageName());
    if ((i2 > 0) && (i3 > 0) && (n > 0) && (j > 0) && (k > 0) && (m > 0) && (i > 0) && (i1 > 0))
    {
      paramString1 = new RemoteViews(localContext.getPackageName(), i2);
      try
      {
        String str = Build.MANUFACTURER;
        i2 = i.a();
        if (i2 != -1)
        {
          paramString1.setTextColor(n, i2);
          paramString1.setTextColor(i, i2);
          paramString1.setTextColor(i1, i2);
        }
        else if ((str != null) && (str.toLowerCase().contains("oppo")))
        {
          paramString1.setTextColor(n, -1);
          paramString1.setTextColor(i, -1);
          paramString1.setTextColor(i1, -1);
        }
        i1 = i.b();
        if (i1 != -1) {
          paramString1.setTextColor(j, i1);
        } else if ((str != null) && (str.toLowerCase().contains("oppo"))) {
          paramString1.setTextColor(j, -1);
        }
      }
      catch (Throwable localThrowable) {}
      float f = i.c();
      if ((f != -1.0F) && (Build.VERSION.SDK_INT >= 16)) {
        paramString1.setTextViewTextSize(j, 0, f);
      }
      paramString1.setTextViewText(n, paramString2);
      paramString1.setTextViewText(j, paramString3);
      paramString1.setTextViewText(i, new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date(System.currentTimeMillis())));
      paramString2 = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_RESOURCE_ID");
      paramString3 = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_OBJ");
      Object localObject = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_BITMAP");
      j = 1;
      if (paramString3 != null)
      {
        if (Build.VERSION.SDK_INT >= 23)
        {
          paramString1.setImageViewIcon(m, (Icon)paramString3);
          i = j;
          break label582;
        }
      }
      else
      {
        if (localObject != null)
        {
          paramString1.setImageViewBitmap(m, (Bitmap)localObject);
          i = j;
          break label582;
        }
        if (paramString2 != null)
        {
          try
          {
            localContext.getResources().getDrawable(((Integer)paramString2).intValue());
            i = 1;
          }
          catch (Throwable paramString3)
          {
            i = 0;
          }
          if (i != 0)
          {
            paramString1.setImageViewResource(m, ((Integer)paramString2).intValue());
            i = j;
            break label582;
          }
        }
      }
      i = 0;
      label582:
      if (i == 0) {
        return null;
      }
      paramString2 = paramMap.get("cn.jpush.android.NOTIFICATION_LARGE_ICON_OBJ");
      paramString3 = paramMap.get("cn.jpush.android.NOTIFICATION_LARGE_ICON_BITMAP");
      if (paramString2 != null)
      {
        if (Build.VERSION.SDK_INT >= 23) {
          paramString1.setImageViewIcon(k, (Icon)paramString2);
        }
      }
      else if (paramString3 != null) {
        paramString1.setImageViewBitmap(k, (Bitmap)paramString3);
      }
      return paramString1;
    }
    g.c("DefaultPushNotificationBuilder", "not found valid push_notification in layout");
    e.a(paramString1, 983, null, localContext);
    return null;
  }
  
  @TargetApi(11)
  private boolean setNotifyIcons(Context paramContext, Notification.Builder paramBuilder, Map<String, Object> paramMap)
  {
    Object localObject1 = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_RESOURCE_ID");
    Object localObject2 = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_OBJ");
    Object localObject3 = paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON_BITMAP");
    if ((localObject2 != null) && (Build.VERSION.SDK_INT >= 23))
    {
      paramBuilder.setSmallIcon((Icon)localObject2);
      j = 1;
    }
    else
    {
      j = 0;
    }
    int i = j;
    if (j == 0)
    {
      i = j;
      if (localObject3 != null)
      {
        if (Build.VERSION.SDK_INT >= 23) {
          paramBuilder.setSmallIcon(Icon.createWithBitmap((Bitmap)localObject3));
        } else {
          paramBuilder.setSmallIcon(17170445);
        }
        i = 1;
      }
    }
    int j = i;
    if (i == 0)
    {
      j = i;
      if (localObject1 != null)
      {
        int k;
        try
        {
          paramContext.getResources().getDrawable(((Integer)localObject1).intValue());
          k = 1;
        }
        catch (Throwable paramContext)
        {
          k = 0;
        }
        j = i;
        if (k != 0)
        {
          paramBuilder.setSmallIcon(((Integer)localObject1).intValue());
          j = 1;
        }
      }
    }
    if (j == 0) {
      return false;
    }
    paramContext = paramMap.get("cn.jpush.android.NOTIFICATION_LARGE_ICON_OBJ");
    paramMap = paramMap.get("cn.jpush.android.NOTIFICATION_LARGE_ICON_BITMAP");
    if (paramContext != null)
    {
      if (Build.VERSION.SDK_INT >= 23) {
        paramBuilder.setLargeIcon((Icon)paramContext);
      }
    }
    else if (paramMap != null) {
      paramBuilder.setLargeIcon((Bitmap)paramMap);
    }
    return true;
  }
  
  RemoteViews buildContentView(String paramString1, String paramString2)
  {
    return null;
  }
  
  public Notification buildNotification(Map<String, String> paramMap)
  {
    if (a.e == null) {
      return null;
    }
    Object localObject1 = a.d;
    Object localObject2 = "";
    String str4 = "";
    String str3 = "";
    Object localObject3 = "";
    String str1 = "";
    String str2 = "";
    Object localObject6 = "";
    String str5 = "";
    Object localObject5 = "";
    Object localObject4 = "";
    if (paramMap.containsKey("cn.jpush.android.MSG_ID")) {
      localObject4 = (String)paramMap.get("cn.jpush.android.MSG_ID");
    }
    if (paramMap.containsKey("cn.jpush.android.ALERT")) {
      localObject2 = (String)paramMap.get("cn.jpush.android.ALERT");
    }
    if (TextUtils.isEmpty((CharSequence)localObject2))
    {
      g.c("DefaultPushNotificationBuilder", "No notification content to show. Give up.");
      return null;
    }
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_TARGET_PKGNAME")) {
      localObject5 = (String)paramMap.get("cn.jpush.android.NOTIFICATION_TARGET_PKGNAME");
    }
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_SMALL_ICON")) {
      str2 = (String)paramMap.get("cn.jpush.android.NOTIFICATION_SMALL_ICON");
    }
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_LARGE_ICON")) {
      localObject6 = (String)paramMap.get("cn.jpush.android.NOTIFICATION_LARGE_ICON");
    }
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_SOURCE")) {
      str5 = (String)paramMap.get("cn.jpush.android.NOTIFICATION_SOURCE");
    }
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_CONTENT_TITLE")) {
      localObject1 = (String)paramMap.get("cn.jpush.android.NOTIFICATION_CONTENT_TITLE");
    }
    if (paramMap.containsKey("cn.jpush.android.BIG_TEXT")) {
      str4 = (String)paramMap.get("cn.jpush.android.BIG_TEXT");
    }
    if (paramMap.containsKey("cn.jpush.android.INBOX")) {
      str3 = (String)paramMap.get("cn.jpush.android.INBOX");
    }
    int j;
    if (paramMap.containsKey("cn.jpush.android.NOTI_PRIORITY")) {
      j = Integer.parseInt((String)paramMap.get("cn.jpush.android.NOTI_PRIORITY"));
    } else {
      j = 0;
    }
    if (paramMap.containsKey("cn.jpush.android.NOTI_CATEGORY")) {
      localObject3 = (String)paramMap.get("cn.jpush.android.NOTI_CATEGORY");
    }
    if (paramMap.containsKey("cn.jpush.android.BIG_PIC_PATH")) {
      str1 = (String)paramMap.get("cn.jpush.android.BIG_PIC_PATH");
    }
    int i;
    if (paramMap.containsKey("cn.jpush.android.ALERT_TYPE")) {
      i = Integer.parseInt((String)paramMap.get("cn.jpush.android.ALERT_TYPE"));
    } else {
      i = -1;
    }
    int m;
    if (paramMap.containsKey("cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID")) {
      m = Integer.parseInt((String)paramMap.get("cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID"));
    } else {
      m = 0;
    }
    int k;
    if (i >= -1)
    {
      k = i;
      if (i <= 7) {}
    }
    else
    {
      k = -1;
    }
    HashMap localHashMap = new HashMap();
    if (!c.a(a.e, str5, (String)localObject5, str2, (String)localObject4, localHashMap)) {
      return null;
    }
    c.a(a.e, (String)localObject6, localHashMap);
    if (m == 1001) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      paramMap = buildDiyRemoteViews((String)localObject4, (String)localObject1, (String)localObject2, localHashMap);
    } else {
      paramMap = buildContentView((String)localObject2, (String)localObject1);
    }
    StringBuilder localStringBuilder;
    if (Build.VERSION.SDK_INT >= 11)
    {
      localObject4 = new Notification.Builder(a.e);
      ((Notification.Builder)localObject4).setContentTitle((CharSequence)localObject1).setContentText((CharSequence)localObject2).setTicker((CharSequence)localObject2);
      if ((i != 0) && (Build.VERSION.SDK_INT >= 20))
      {
        ((Notification.Builder)localObject4).setGroupSummary(false);
        ((Notification.Builder)localObject4).setGroup("group");
      }
      setNotifyIcons(a.e, (Notification.Builder)localObject4, localHashMap);
      if (Build.VERSION.SDK_INT >= 17) {
        ((Notification.Builder)localObject4).setShowWhen(true);
      }
      c.a(a.e, (Notification.Builder)localObject4, j, k);
      if (Build.VERSION.SDK_INT >= 16)
      {
        if (paramMap == null)
        {
          if (!TextUtils.isEmpty(str4))
          {
            localObject1 = new Notification.BigTextStyle();
            ((Notification.BigTextStyle)localObject1).bigText(str4);
            ((Notification.Builder)localObject4).setStyle((Notification.Style)localObject1);
          }
          if (!TextUtils.isEmpty(str3))
          {
            localObject1 = new Notification.InboxStyle();
            try
            {
              localObject5 = new java/util/TreeMap;
              ((TreeMap)localObject5).<init>();
              localObject2 = new org/json/JSONObject;
              ((JSONObject)localObject2).<init>(str3);
              localObject6 = ((JSONObject)localObject2).keys();
              while (((Iterator)localObject6).hasNext())
              {
                str2 = (String)((Iterator)localObject6).next();
                ((TreeMap)localObject5).put(str2, ((JSONObject)localObject2).optString(str2));
              }
              localObject5 = ((TreeMap)localObject5).values().iterator();
              while (((Iterator)localObject5).hasNext()) {
                ((Notification.InboxStyle)localObject1).addLine((String)((Iterator)localObject5).next());
              }
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>(" + ");
              ((StringBuilder)localObject5).append(((JSONObject)localObject2).length());
              ((StringBuilder)localObject5).append(" new messages");
              ((Notification.InboxStyle)localObject1).setSummaryText(((StringBuilder)localObject5).toString());
            }
            catch (Throwable localThrowable2)
            {
              localObject5 = new StringBuilder("Set inbox style error: ");
              ((StringBuilder)localObject5).append(localThrowable2.getMessage());
              g.d("DefaultPushNotificationBuilder", ((StringBuilder)localObject5).toString());
            }
            ((Notification.Builder)localObject4).setStyle((Notification.Style)localObject1);
          }
          if (!TextUtils.isEmpty(str1))
          {
            try
            {
              localObject1 = new android/app/Notification$BigPictureStyle;
              ((Notification.BigPictureStyle)localObject1).<init>();
              ((Notification.BigPictureStyle)localObject1).bigPicture(BitmapFactory.decodeFile(str1));
              ((Notification.Builder)localObject4).setStyle((Notification.Style)localObject1);
            }
            catch (Throwable localThrowable1)
            {
              localStringBuilder = new StringBuilder("Create big picture style failed. error:");
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
              localStringBuilder = new StringBuilder("Create bitmap failed caused by OutOfMemoryError.error:");
            }
            localStringBuilder.append(localOutOfMemoryError);
            g.c("DefaultPushNotificationBuilder", localStringBuilder.toString());
          }
        }
        if (j != 0) {
          if (j == 1)
          {
            ((Notification.Builder)localObject4).setPriority(1);
          }
          else
          {
            i = 2;
            if (j == 2)
            {
              ((Notification.Builder)localObject4).setPriority(i);
            }
            else
            {
              i = -1;
              if (j == -1) {}
              for (;;)
              {
                ((Notification.Builder)localObject4).setPriority(i);
                break label1082;
                i = -2;
                if (j == -2) {
                  break;
                }
                i = 0;
              }
            }
          }
        }
        label1082:
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {
          if (Build.VERSION.SDK_INT >= 21) {
            try
            {
              Class.forName("android.app.Notification$Builder").getDeclaredMethod("setCategory", new Class[] { String.class }).invoke(localObject4, new Object[] { localObject3 });
            }
            catch (Exception localException)
            {
              localException.printStackTrace();
            }
            catch (NoSuchMethodException localNoSuchMethodException)
            {
              localNoSuchMethodException.printStackTrace();
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
              localClassNotFoundException.printStackTrace();
            }
          } else {
            g.c("DefaultPushNotificationBuilder", "Device rom SDK < 21, can not set notification category!");
          }
        }
      }
      if (paramMap != null) {
        ((Notification.Builder)localObject4).setContent(paramMap);
      }
      ((Notification.Builder)localObject4).setDefaults(k);
      return getNotification((Notification.Builder)localObject4);
    }
    localObject3 = a.e;
    localObject4 = new Notification(c.a(), localStringBuilder, System.currentTimeMillis());
    resetNotificationParams((Notification)localObject4);
    ((Notification)localObject4).defaults = k;
    localObject3 = localClassNotFoundException;
    if (localClassNotFoundException == null) {
      localObject3 = a.d;
    }
    if (paramMap != null) {
      ((Notification)localObject4).contentView = paramMap;
    } else {
      c.a((Notification)localObject4, a.e, (String)localObject3, localStringBuilder, null);
    }
    ((Notification)localObject4).flags = 17;
    return (Notification)localObject4;
  }
  
  public String getDeveloperArg0()
  {
    return null;
  }
  
  Notification getNotification(Notification.Builder paramBuilder)
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 16) {
        paramBuilder = paramBuilder.build();
      } else {
        paramBuilder = paramBuilder.getNotification();
      }
      paramBuilder.flags = 17;
      return paramBuilder;
    }
    catch (Throwable paramBuilder)
    {
      g.c("DefaultPushNotificationBuilder", "Build notification error:", paramBuilder);
    }
    return null;
  }
  
  void resetNotificationParams(Notification paramNotification) {}
  
  public String toString()
  {
    return "";
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/DefaultPushNotificationBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */