package cn.jpush.android.api;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.b.e;
import cn.jpush.android.b.m;
import cn.jpush.android.b.p;
import cn.jpush.android.e.k;
import cn.jpush.android.service.PushReceiver;
import cn.jpush.android.ui.PopWinActivity;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.zip.Adler32;

public final class c
{
  private static boolean a = false;
  
  public static int a()
  {
    int j = cn.jpush.android.a.e.getResources().getIdentifier("jpush_notification_icon", "drawable", cn.jpush.android.a.e.getPackageName());
    int i = j;
    if (j == 0) {
      if (cn.jpush.android.a.b != 0) {
        i = cn.jpush.android.a.b;
      } else {
        try
        {
          i = cn.jpush.android.a.e.getPackageManager().getApplicationInfo(cn.jpush.android.a.e.getPackageName(), 0).icon;
        }
        catch (Throwable localThrowable)
        {
          cn.jpush.android.e.g.d("NotificationHelper", "failed to get application info and icon.", localThrowable);
          i = j;
        }
      }
    }
    return i;
  }
  
  public static int a(int paramInt)
  {
    int j = 17301618;
    int i = j;
    switch (paramInt)
    {
    case 1: 
    default: 
      i = 17301586;
      break;
    case 3: 
      i = 17301567;
      break;
    case 0: 
      i = 17301647;
      break;
    case -1: 
      HashMap localHashMap = a("R$drawable", new String[] { "jpush_notification_icon" });
      try
      {
        paramInt = ((Integer)localHashMap.get("jpush_notification_icon")).intValue();
      }
      catch (Exception localException)
      {
        paramInt = 0;
      }
      i = j;
      if (paramInt > 0) {
        i = paramInt;
      }
      break;
    }
    return i;
  }
  
  private static int a(cn.jpush.android.data.b paramb, int paramInt)
  {
    String str = paramb.e;
    if (!TextUtils.isEmpty(paramb.f)) {
      str = paramb.f;
    }
    return a(str, paramInt);
  }
  
  public static int a(String paramString, int paramInt)
  {
    if (TextUtils.isEmpty(paramString)) {
      return 0;
    }
    try
    {
      i = Integer.valueOf(paramString).intValue();
      return i;
    }
    catch (Exception localException)
    {
      Adler32 localAdler32 = new Adler32();
      localAdler32.update(paramString.getBytes());
      int j = (int)localAdler32.getValue();
      int i = j;
      if (j < 0) {
        i = Math.abs(j);
      }
      i += paramInt * 13889152;
      paramInt = i;
      if (i < 0) {
        paramInt = Math.abs(i);
      }
    }
    return paramInt;
  }
  
  @TargetApi(23)
  private static Icon a(String paramString)
  {
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(paramString);
      if (localFile.exists())
      {
        paramString = Icon.createWithFilePath(paramString);
        return paramString;
      }
    }
    catch (Throwable paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  private static String a(Context paramContext, String paramString)
  {
    if ((!TextUtils.isEmpty(paramString)) && ((paramString.startsWith("http://")) || (paramString.startsWith("https://"))))
    {
      if (cn.jpush.android.e.a.b(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE"))
      {
        if (!cn.jpush.android.e.a.a())
        {
          paramContext = "SDCard is not mounted,need not download pic";
        }
        else
        {
          if ((!paramString.endsWith(".jpg")) && (!paramString.endsWith(".png")) && (!paramString.endsWith(".jpeg"))) {}
          do
          {
            paramContext = "";
            break;
            Object localObject2 = cn.jpush.android.e.a.a(paramString);
            Object localObject1 = localObject2;
            if (TextUtils.isEmpty((CharSequence)localObject2)) {
              localObject1 = UUID.randomUUID().toString();
            }
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append((String)localObject1);
            ((StringBuilder)localObject2).append(paramString.substring(paramString.lastIndexOf(".")));
            localObject1 = ((StringBuilder)localObject2).toString();
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append(cn.jpush.android.e.c.b(paramContext, "noti_res"));
            ((StringBuilder)localObject2).append((String)localObject1);
            paramContext = ((StringBuilder)localObject2).toString();
            if (new File(paramContext).exists()) {
              break;
            }
            paramString = cn.jpush.android.c.a.a(paramString, 2);
          } while ((paramString == null) || (!cn.jpush.android.e.c.a(paramContext, paramString)));
          if (!TextUtils.isEmpty(paramContext)) {
            return paramContext;
          }
          paramContext = "Get network picture failed.";
        }
      }
      else {
        paramContext = "No permission to write resource to storage";
      }
      cn.jpush.android.e.g.c("NotificationHelper", paramContext);
    }
    return null;
  }
  
  private static HashMap<String, Integer> a(String paramString, String[] paramArrayOfString)
  {
    HashMap localHashMap;
    if (!TextUtils.isEmpty(paramString)) {
      localHashMap = new HashMap();
    }
    try
    {
      Object localObject = cn.jpush.android.a.e.getPackageName();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(".R");
      for (localStringBuilder : Class.forName(localStringBuilder.toString()).getDeclaredClasses()) {
        if (localStringBuilder.getName().contains(paramString))
        {
          for (??? = 0; ??? <= 0; ???++)
          {
            paramString = paramArrayOfString[0];
            localHashMap.put(paramString, Integer.valueOf(localStringBuilder.getDeclaredField(paramString).getInt(paramString)));
          }
          return localHashMap;
        }
      }
    }
    catch (Exception paramString)
    {
      for (;;) {}
    }
    return localHashMap;
    throw new NullPointerException("parameter resType or fieldNames error.");
  }
  
  public static Map<String, String> a(cn.jpush.android.data.b paramb)
  {
    HashMap localHashMap = new HashMap();
    if (paramb != null)
    {
      localHashMap.put("cn.jpush.android.MSG_ID", paramb.e);
      localHashMap.put("cn.jpush.android.ALERT", paramb.x);
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramb.n);
      localHashMap.put("cn.jpush.android.ALERT_TYPE", ((StringBuilder)localObject).toString());
      if (!TextUtils.isEmpty(paramb.w)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_CONTENT_TITLE", paramb.w);
      }
      if (!TextUtils.isEmpty(paramb.X)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_URL", paramb.X);
      }
      if (!TextUtils.isEmpty(paramb.p)) {
        localHashMap.put("cn.jpush.android.EXTRA", paramb.p);
      }
      String str;
      if ((paramb.y == 1) && (!TextUtils.isEmpty(paramb.z)))
      {
        localObject = "cn.jpush.android.BIG_TEXT";
        str = paramb.z;
      }
      for (;;)
      {
        localHashMap.put(localObject, str);
        break;
        if ((paramb.y == 2) && (!TextUtils.isEmpty(paramb.B)))
        {
          localObject = "cn.jpush.android.INBOX";
          str = paramb.B;
        }
        else
        {
          if ((paramb.y != 3) || (TextUtils.isEmpty(paramb.A))) {
            break;
          }
          localObject = "cn.jpush.android.BIG_PIC_PATH";
          str = paramb.A;
        }
      }
      if (paramb.C != 0)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(paramb.C);
        localHashMap.put("cn.jpush.android.NOTI_PRIORITY", ((StringBuilder)localObject).toString());
      }
      if (!TextUtils.isEmpty(paramb.D)) {
        localHashMap.put("cn.jpush.android.NOTI_CATEGORY", paramb.D);
      }
      if (!TextUtils.isEmpty(paramb.V)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_SMALL_ICON", paramb.V);
      }
      if (!TextUtils.isEmpty(paramb.W)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_LARGE_ICON", paramb.W);
      }
      if (!TextUtils.isEmpty(paramb.U)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_SOURCE", paramb.U);
      }
      if (!TextUtils.isEmpty(paramb.aa)) {
        localHashMap.put("cn.jpush.android.NOTIFICATION_TARGET_PKGNAME", paramb.aa);
      }
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramb.ab);
      localHashMap.put("cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID", ((StringBuilder)localObject).toString());
    }
    return localHashMap;
  }
  
  public static void a(Notification paramNotification, Context paramContext, String paramString1, String paramString2, PendingIntent paramPendingIntent)
  {
    try
    {
      Class.forName("android.app.Notification").getDeclaredMethod("setLatestEventInfo", new Class[] { Context.class, CharSequence.class, CharSequence.class, PendingIntent.class }).invoke(paramNotification, new Object[] { paramContext, paramString1, paramString2, null });
      return;
    }
    catch (Exception paramNotification)
    {
      paramNotification.printStackTrace();
    }
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    if (paramInt > 0) {
      for (int i = 0; i < paramInt; i++)
      {
        Integer localInteger = Integer.valueOf(p.a());
        if (localInteger.intValue() != 0) {
          b(paramContext, localInteger.intValue());
        }
      }
    }
  }
  
  public static void a(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if (!p.b(paramInt)) {
      p.a(paramInt);
    }
    if (p.b() > cn.jpush.android.b.a(paramContext))
    {
      paramInt = p.a();
      if (paramInt != 0) {
        b(paramContext, paramInt);
      }
    }
  }
  
  public static void a(Context paramContext, Notification.Builder paramBuilder, int paramInt1, int paramInt2)
  {
    if (paramContext == null) {
      return;
    }
    if (Build.VERSION.SDK_INT < 26) {
      return;
    }
    if (paramContext.getApplicationInfo().targetSdkVersion < 26) {
      return;
    }
    if (cn.jpush.android.e.a.d(paramContext)) {
      paramInt2 = 0;
    }
    int j;
    int i;
    if ((paramInt1 != -2) && (paramInt1 != -1))
    {
      j = paramInt1;
      i = paramInt2;
      if (paramInt1 >= 0)
      {
        j = paramInt1;
        i = paramInt2;
        if (paramInt2 == 0)
        {
          j = -1;
          i = paramInt2;
        }
      }
    }
    else
    {
      i = 0;
      j = paramInt1;
    }
    paramInt1 = b(j);
    paramContext = new StringBuilder("JPush_");
    paramContext.append(paramInt1);
    paramContext.append("_");
    paramContext.append(i);
    String str = paramContext.toString();
    paramContext = new StringBuilder("Notification_");
    paramContext.append(paramInt1);
    paramContext.append("_");
    paramContext.append(i);
    if (a(str, paramContext.toString(), j, i)) {
      try
      {
        k.a(paramBuilder, "setChannelId", new Class[] { String.class }, new String[] { str });
        return;
      }
      catch (Throwable paramBuilder)
      {
        paramContext = new StringBuilder("setChannelId error");
        paramContext.append(paramBuilder);
        cn.jpush.android.e.g.c("NotificationHelper", paramContext.toString());
      }
    }
  }
  
  public static void a(Context paramContext, cn.jpush.android.data.b paramb)
  {
    JCoreInterface.asyncExecute(new d(paramContext, paramb), new int[0]);
  }
  
  public static void a(Context paramContext, cn.jpush.android.data.b paramb, int paramInt)
  {
    Context localContext = paramContext;
    if (paramContext == null) {
      localContext = cn.jpush.android.a.e;
    }
    ((NotificationManager)localContext.getSystemService("notification")).cancel(a(paramb, 0));
  }
  
  /* Error */
  @TargetApi(11)
  public static void a(Context paramContext, String paramString, Map<String, Object> paramMap)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 108	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   4: ifeq +4 -> 8
    //   7: return
    //   8: ldc -68
    //   10: astore 6
    //   12: iconst_0
    //   13: istore 4
    //   15: aload 6
    //   17: astore 5
    //   19: iload 4
    //   21: istore_3
    //   22: aload_1
    //   23: invokestatic 108	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   26: ifne +48 -> 74
    //   29: aload_1
    //   30: invokestatic 484	cn/jpush/android/b/m:a	(Ljava/lang/String;)Z
    //   33: ifeq +18 -> 51
    //   36: getstatic 19	cn/jpush/android/a:e	Landroid/content/Context;
    //   39: aload_1
    //   40: invokestatic 486	cn/jpush/android/api/c:a	(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    //   43: astore 5
    //   45: iload 4
    //   47: istore_3
    //   48: goto +26 -> 74
    //   51: getstatic 19	cn/jpush/android/a:e	Landroid/content/Context;
    //   54: invokevirtual 25	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   57: aload_1
    //   58: ldc 29
    //   60: getstatic 19	cn/jpush/android/a:e	Landroid/content/Context;
    //   63: invokevirtual 33	android/content/Context:getPackageName	()Ljava/lang/String;
    //   66: invokevirtual 39	android/content/res/Resources:getIdentifier	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    //   69: istore_3
    //   70: aload 6
    //   72: astore 5
    //   74: getstatic 419	android/os/Build$VERSION:SDK_INT	I
    //   77: istore 4
    //   79: aconst_null
    //   80: astore_1
    //   81: aconst_null
    //   82: astore 6
    //   84: iload 4
    //   86: bipush 23
    //   88: if_icmplt +51 -> 139
    //   91: iload_3
    //   92: ifeq +12 -> 104
    //   95: aload_0
    //   96: iload_3
    //   97: invokestatic 490	android/graphics/drawable/Icon:createWithResource	(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;
    //   100: astore_0
    //   101: goto +20 -> 121
    //   104: aload 6
    //   106: astore_0
    //   107: aload 5
    //   109: invokestatic 108	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   112: ifne +9 -> 121
    //   115: aload 5
    //   117: invokestatic 492	cn/jpush/android/api/c:a	(Ljava/lang/String;)Landroid/graphics/drawable/Icon;
    //   120: astore_0
    //   121: aload_0
    //   122: ifnull +86 -> 208
    //   125: aload_2
    //   126: ldc_w 494
    //   129: aload_0
    //   130: invokeinterface 289 3 0
    //   135: pop
    //   136: goto +72 -> 208
    //   139: iload_3
    //   140: ifeq +15 -> 155
    //   143: aload_0
    //   144: invokevirtual 25	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   147: iload_3
    //   148: invokestatic 500	android/graphics/BitmapFactory:decodeResource	(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    //   151: astore_0
    //   152: goto +41 -> 193
    //   155: aload_1
    //   156: astore_0
    //   157: aload 5
    //   159: invokestatic 108	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   162: ifne +31 -> 193
    //   165: new 144	java/io/File
    //   168: astore 6
    //   170: aload 6
    //   172: aload 5
    //   174: invokespecial 147	java/io/File:<init>	(Ljava/lang/String;)V
    //   177: aload_1
    //   178: astore_0
    //   179: aload 6
    //   181: invokevirtual 151	java/io/File:exists	()Z
    //   184: ifeq +9 -> 193
    //   187: aload 5
    //   189: invokestatic 504	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;)Landroid/graphics/Bitmap;
    //   192: astore_0
    //   193: aload_0
    //   194: ifnull +14 -> 208
    //   197: aload_2
    //   198: ldc_w 506
    //   201: aload_0
    //   202: invokeinterface 289 3 0
    //   207: pop
    //   208: return
    //   209: astore_1
    //   210: aload 6
    //   212: astore 5
    //   214: iload 4
    //   216: istore_3
    //   217: goto -143 -> 74
    //   220: astore_0
    //   221: goto -13 -> 208
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	224	0	paramContext	Context
    //   0	224	1	paramString	String
    //   0	224	2	paramMap	Map<String, Object>
    //   21	196	3	i	int
    //   13	202	4	j	int
    //   17	196	5	localObject1	Object
    //   10	201	6	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   22	45	209	java/lang/Throwable
    //   51	70	209	java/lang/Throwable
    //   74	79	220	java/lang/Throwable
    //   95	101	220	java/lang/Throwable
    //   107	121	220	java/lang/Throwable
    //   125	136	220	java/lang/Throwable
    //   143	152	220	java/lang/Throwable
    //   157	177	220	java/lang/Throwable
    //   179	193	220	java/lang/Throwable
    //   197	208	220	java/lang/Throwable
  }
  
  public static void a(Context paramContext, Map<String, String> paramMap, int paramInt, String paramString1, String paramString2, cn.jpush.android.data.b paramb)
  {
    Intent localIntent = new Intent("cn.jpush.android.intent.NOTIFICATION_RECEIVED");
    try
    {
      cn.jpush.android.e.g.a("NotificationHelper", "Send push received broadcast to developer defined receiver");
      a(localIntent, paramMap, paramInt);
      if (!TextUtils.isEmpty(paramString1)) {
        localIntent.putExtra("cn.jpush.android.NOTIFICATION_DEVELOPER_ARG0", paramString1);
      }
      if ((paramb.a()) && ((paramb instanceof cn.jpush.android.data.g)))
      {
        paramMap = (cn.jpush.android.data.g)paramb;
        if ((paramMap.ac != 0) && (paramMap.ac != 4))
        {
          if ((paramMap.ah != null) && (paramMap.ah.startsWith("file://")))
          {
            paramMap.ah = paramMap.ah.replaceFirst("file://", "");
            localIntent.putExtra("cn.jpush.android.HTML_PATH", paramMap.ah);
          }
          if ((paramMap.ae != null) && (paramMap.ae.size() > 0))
          {
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            paramb = cn.jpush.android.e.c.b(paramContext, paramb.e);
            Iterator localIterator = paramMap.ae.iterator();
            if (localIterator.hasNext())
            {
              paramString1 = (String)localIterator.next();
              paramMap = paramString1;
              if (paramString1.startsWith("http://")) {
                paramMap = cn.jpush.android.e.c.a(paramString1);
              }
              if (TextUtils.isEmpty(localStringBuilder.toString())) {
                localStringBuilder.append(paramb);
              }
              for (;;)
              {
                localStringBuilder.append(paramMap);
                break;
                localStringBuilder.append(",");
                localStringBuilder.append(paramb);
              }
            }
            localIntent.putExtra("cn.jpush.android.HTML_RES", localStringBuilder.toString());
          }
        }
      }
      localIntent.addCategory(paramString2);
      localIntent.setPackage(paramContext.getPackageName());
      paramMap = new java/lang/StringBuilder;
      paramMap.<init>();
      paramMap.append(paramString2);
      paramMap.append(".permission.JPUSH_MESSAGE");
      paramContext.sendBroadcast(localIntent, paramMap.toString());
      return;
    }
    catch (Throwable paramString1)
    {
      paramMap = new StringBuilder("sendNotificationReceivedBroadcast error:");
      paramMap.append(paramString1.getMessage());
      cn.jpush.android.e.g.c("NotificationHelper", paramMap.toString());
      paramMap = new StringBuilder();
      paramMap.append(paramString2);
      paramMap.append(".permission.JPUSH_MESSAGE");
      cn.jpush.android.e.a.b(paramContext, localIntent, paramMap.toString());
    }
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      for (;;)
      {
        localObject = Integer.valueOf(p.a());
        if (((Integer)localObject).intValue() == 0) {
          break;
        }
        b(paramContext, ((Integer)localObject).intValue());
      }
      return;
    }
    Object localObject = new Bundle();
    ((Bundle)localObject).putString("action", "intent.MULTI_PROCESS");
    ((Bundle)localObject).putInt("multi_type", 10);
    JCoreInterface.sendAction(paramContext, cn.jpush.android.a.a, (Bundle)localObject);
  }
  
  public static void a(Intent paramIntent, Map<String, String> paramMap, int paramInt)
  {
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((!"cn.jpush.android.NOTIFICATION_SOURCE".equals(str)) && (!"cn.jpush.android.NOTIFICATION_SMALL_ICON".equals(str)) && (!"cn.jpush.android.NOTIFICATION_TARGET_PKGNAME".equals(str)) && (!"cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID".equals(str))) {
        paramIntent.putExtra(str, (String)paramMap.get(str));
      }
    }
    if (paramInt != 0) {
      paramIntent.putExtra("cn.jpush.android.NOTIFICATION_ID", paramInt);
    }
  }
  
  @TargetApi(11)
  public static boolean a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, Map<String, Object> paramMap)
  {
    str2 = "";
    str1 = str2;
    try
    {
      if (!TextUtils.isEmpty(paramString1))
      {
        str1 = str2;
        if (!TextUtils.isEmpty(paramString3)) {
          if (m.a(paramString3))
          {
            str1 = a(cn.jpush.android.a.e, paramString3);
          }
          else
          {
            i = cn.jpush.android.a.e.getResources().getIdentifier(paramString3, "drawable", cn.jpush.android.a.e.getPackageName());
            str1 = str2;
          }
        }
      }
    }
    catch (Throwable paramString1)
    {
      for (;;)
      {
        int i;
        str1 = str2;
      }
    }
    i = 0;
    if (!TextUtils.isEmpty(str1)) {
      try
      {
        if (Build.VERSION.SDK_INT >= 23)
        {
          paramString1 = a(str1);
          if (paramString1 != null)
          {
            paramMap.put("cn.jpush.android.NOTIFICATION_SMALL_ICON_OBJ", paramString1);
            return true;
          }
        }
        else
        {
          if (cn.jpush.android.e.a.b(str1)) {
            paramString1 = BitmapFactory.decodeFile(str1);
          } else {
            paramString1 = null;
          }
          if (paramString1 != null)
          {
            paramMap.put("cn.jpush.android.NOTIFICATION_SMALL_ICON_BITMAP", paramString1);
            return true;
          }
        }
      }
      catch (Throwable paramString1) {}
    }
    if (i != 0) {}
    for (;;)
    {
      paramMap.put("cn.jpush.android.NOTIFICATION_SMALL_ICON_RESOURCE_ID", Integer.valueOf(i));
      return true;
      int j = i;
      if (i == 0) {
        if ((!TextUtils.isEmpty(paramString2)) && (!paramString2.equals(paramContext.getPackageName())))
        {
          j = i;
          if (Build.VERSION.SDK_INT >= 23)
          {
            if (!cn.jpush.android.e.a.e(paramContext, paramString2))
            {
              e.a(paramString4, 993, null, paramContext);
              return false;
            }
            paramContext = b(paramContext, paramString2);
            if (paramContext == null) {
              return false;
            }
            try
            {
              paramContext = Icon.createWithBitmap(paramContext);
              if (paramContext == null) {
                return false;
              }
              paramMap.put("cn.jpush.android.NOTIFICATION_SMALL_ICON_OBJ", paramContext);
              return true;
            }
            catch (Throwable paramContext)
            {
              return false;
            }
          }
        }
        else
        {
          j = a();
        }
      }
      if (j == 0) {
        break;
      }
      i = j;
    }
    return false;
  }
  
  private static boolean a(String paramString, CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    if ((Build.VERSION.SDK_INT >= 26) && (cn.jpush.android.a.e.getApplicationInfo().targetSdkVersion >= 26))
    {
      NotificationManager localNotificationManager = (NotificationManager)cn.jpush.android.a.e.getSystemService("notification");
      if (localNotificationManager == null)
      {
        cn.jpush.android.e.g.d("NotificationHelper", "NotificationManager is null!");
        return false;
      }
      try
      {
        Object localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("prepare NotificationChannel, channelId=");
        ((StringBuilder)localObject1).append(paramString);
        cn.jpush.android.e.g.a("NotificationHelper", ((StringBuilder)localObject1).toString());
        localObject1 = Class.forName("android.app.NotificationChannel");
        Object localObject2 = ((Class)localObject1).getConstructor(new Class[] { String.class, CharSequence.class, Integer.TYPE });
        ((Constructor)localObject2).setAccessible(true);
        paramString = ((Constructor)localObject2).newInstance(new Object[] { paramString, paramCharSequence, Integer.valueOf(b(paramInt1)) });
        boolean bool;
        if ((paramInt2 & 0x4) != 0) {
          bool = true;
        } else {
          bool = false;
        }
        try
        {
          k.a(paramString, "enableLights", new Class[] { Boolean.TYPE }, new Boolean[] { Boolean.valueOf(bool) });
        }
        catch (Throwable paramCharSequence)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("enableLights fail:");
          ((StringBuilder)localObject2).append(paramCharSequence);
          cn.jpush.android.e.g.c("NotificationHelper", ((StringBuilder)localObject2).toString());
        }
        if ((paramInt2 & 0x2) != 0) {
          bool = true;
        } else {
          bool = false;
        }
        try
        {
          k.a(paramString, "enableVibration", new Class[] { Boolean.TYPE }, new Boolean[] { Boolean.valueOf(bool) });
        }
        catch (Throwable paramCharSequence)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("enableLights fail:");
          ((StringBuilder)localObject2).append(paramCharSequence);
          cn.jpush.android.e.g.c("NotificationHelper", ((StringBuilder)localObject2).toString());
        }
        if ((paramInt2 & 0x1) != 0) {
          paramInt1 = 1;
        } else {
          paramInt1 = 0;
        }
        if (paramInt1 == 0) {
          try
          {
            k.a(paramString, "setSound", new Class[] { Uri.class, Class.forName("android.media.AudioAttributes") }, new Object[][] { null, null });
          }
          catch (Throwable localThrowable)
          {
            paramCharSequence = new java/lang/StringBuilder;
            paramCharSequence.<init>("disableSounds fail:");
            paramCharSequence.append(localThrowable);
            cn.jpush.android.e.g.c("NotificationHelper", paramCharSequence.toString());
          }
        }
        try
        {
          k.a(localNotificationManager, "createNotificationChannel", new Class[] { localObject1 }, new Object[] { paramString });
        }
        catch (Throwable paramCharSequence)
        {
          paramString = new java/lang/StringBuilder;
          paramString.<init>("createNotificationChannel fail:");
          paramString.append(paramCharSequence);
          cn.jpush.android.e.g.d("NotificationHelper", paramString.toString());
        }
        return true;
      }
      catch (Throwable paramCharSequence)
      {
        paramString = new StringBuilder("new NotificationChannel fail:");
        paramString.append(paramCharSequence);
        cn.jpush.android.e.g.d("NotificationHelper", paramString.toString());
      }
    }
    return false;
  }
  
  private static int b(int paramInt)
  {
    switch (paramInt)
    {
    case 0: 
    default: 
      return 3;
    case 1: 
    case 2: 
      return 4;
    case -1: 
      return 2;
    }
    return 1;
  }
  
  /* Error */
  private static android.graphics.Bitmap b(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: aload_0
    //   4: invokevirtual 716	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   7: invokevirtual 47	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   10: astore_0
    //   11: aload_0
    //   12: aload_0
    //   13: aload_1
    //   14: iconst_0
    //   15: invokevirtual 53	android/content/pm/PackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   18: invokevirtual 720	android/content/pm/PackageManager:getApplicationIcon	(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    //   21: checkcast 722	android/graphics/drawable/BitmapDrawable
    //   24: invokevirtual 726	android/graphics/drawable/BitmapDrawable:getBitmap	()Landroid/graphics/Bitmap;
    //   27: astore_0
    //   28: ldc 2
    //   30: monitorexit
    //   31: aload_0
    //   32: areturn
    //   33: astore_0
    //   34: ldc 2
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: astore_0
    //   40: ldc 2
    //   42: monitorexit
    //   43: aconst_null
    //   44: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	45	0	paramContext	Context
    //   0	45	1	paramString	String
    // Exception table:
    //   from	to	target	type
    //   3	28	33	finally
    //   3	28	39	java/lang/Throwable
  }
  
  private static void b(Context paramContext, int paramInt)
  {
    Context localContext = paramContext;
    if (paramContext == null) {
      localContext = cn.jpush.android.a.e;
    }
    ((NotificationManager)localContext.getSystemService("notification")).cancel(paramInt);
  }
  
  public static void b(Context paramContext, cn.jpush.android.data.b paramb)
  {
    int j = a(paramb, 0);
    if ((paramb.k) && (paramb.h))
    {
      NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
      if ((paramb instanceof cn.jpush.android.data.g))
      {
        Map localMap = a(paramb);
        String str1;
        if (TextUtils.isEmpty(paramb.q)) {
          str1 = paramContext.getPackageName();
        } else {
          str1 = paramb.q;
        }
        if (TextUtils.isEmpty(paramb.x))
        {
          a(paramContext, localMap, 0, "", str1, paramb);
          return;
        }
        Object localObject1 = JPushInterface.b(paramb.i);
        if (!TextUtils.isEmpty(paramb.U))
        {
          if (TextUtils.isEmpty(paramb.aa))
          {
            e.a(paramb.e, 985, null, paramContext);
            return;
          }
          localObject1 = new DefaultPushNotificationBuilder();
        }
        String str2 = ((PushNotificationBuilder)localObject1).getDeveloperArg0();
        Notification localNotification = ((PushNotificationBuilder)localObject1).buildNotification(localMap);
        if ((localNotification != null) && (!TextUtils.isEmpty(paramb.x)))
        {
          if (!paramb.a())
          {
            if (cn.jpush.android.e.a.a(paramContext, PushReceiver.class))
            {
              localObject1 = new StringBuilder("cn.jpush.android.intent.NOTIFICATION_OPENED_PROXY.");
              ((StringBuilder)localObject1).append(UUID.randomUUID().toString());
              localObject1 = new Intent(((StringBuilder)localObject1).toString());
              localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append(paramb.j);
              ((Intent)localObject1).putExtra("cn.jpush.android.NOTIFICATION_TYPE", ((StringBuilder)localObject2).toString());
              localObject2 = new StringBuilder("running flag:");
              ((StringBuilder)localObject2).append(JCoreInterface.getRuningFlag());
              cn.jpush.android.e.g.a("NotificationHelper", ((StringBuilder)localObject2).toString());
              if (JCoreInterface.getRuningFlag()) {
                break label403;
              }
              ((Intent)localObject1).setClass(paramContext, PushReceiver.class);
            }
            for (;;)
            {
              i = 0;
              break;
              if (cn.jpush.android.e.a.b(paramContext, PopWinActivity.class))
              {
                localObject1 = new StringBuilder("cn.jpush.android.intent.NOTIFICATION_OPENED_PROXY.");
                ((StringBuilder)localObject1).append(UUID.randomUUID().toString());
                localObject1 = new Intent(((StringBuilder)localObject1).toString());
                localObject2 = new StringBuilder();
                ((StringBuilder)localObject2).append(paramb.j);
                ((Intent)localObject1).putExtra("cn.jpush.android.NOTIFICATION_TYPE", ((StringBuilder)localObject2).toString());
                label403:
                ((Intent)localObject1).setClass(paramContext, PopWinActivity.class);
                ((Intent)localObject1).putExtra("isNotification", true);
                i = 1;
                break;
              }
              if (!TextUtils.isEmpty(paramb.U)) {
                break label691;
              }
              localObject2 = new Intent("cn.jpush.android.intent.NOTIFICATION_OPENED");
              ((Intent)localObject2).addCategory(str1);
              if (Build.VERSION.SDK_INT < 25)
              {
                localObject1 = localObject2;
                if (Build.VERSION.SDK_INT >= 21) {}
              }
              else
              {
                localObject1 = localObject2;
                if ("cn.jpush.android.intent.NOTIFICATION_OPENED".equals(((Intent)localObject2).getAction()))
                {
                  List localList = cn.jpush.android.e.a.a(paramContext, (Intent)localObject2, null);
                  localObject1 = localObject2;
                  if (!localList.isEmpty())
                  {
                    ((Intent)localObject2).setComponent(new ComponentName(paramContext, (String)localList.get(0)));
                    localObject1 = localObject2;
                  }
                }
              }
            }
            ((Intent)localObject1).putExtra("sdktype", cn.jpush.android.a.a);
            ((Intent)localObject1).putExtra("platform", paramb.g);
            a((Intent)localObject1, localMap, j);
            ((Intent)localObject1).putExtra("key_show_entity", paramb);
            ((Intent)localObject1).putExtra("app", str1);
            if (!TextUtils.isEmpty(str2)) {
              ((Intent)localObject1).putExtra("cn.jpush.android.NOTIFICATION_DEVELOPER_ARG0", str2);
            }
            Object localObject2 = new StringBuilder("notification intent component=");
            ((StringBuilder)localObject2).append(((Intent)localObject1).getComponent());
            cn.jpush.android.e.g.a("NotificationHelper", ((StringBuilder)localObject2).toString());
            if (i != 0)
            {
              localObject1 = PendingIntent.getActivity(paramContext, 0, (Intent)localObject1, 1073741824);
            }
            else
            {
              localObject1 = PendingIntent.getBroadcast(paramContext, 0, (Intent)localObject1, 1073741824);
              break label728;
              label691:
              e.a(paramb.e, 984, null, paramContext);
            }
          }
          else
          {
            localObject1 = c(paramContext, paramb);
            if (localObject1 == null) {
              return;
            }
            localObject1 = PendingIntent.getActivity(paramContext, j, (Intent)localObject1, 134217728);
          }
          label728:
          localNotification.contentIntent = ((PendingIntent)localObject1);
          int i = -1;
          if (localMap.containsKey("cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID")) {
            i = Integer.parseInt((String)localMap.get("cn.jpush.android.NOTIFICATION_DEEPLINK_BUILDER_ID"));
          }
          if (i == 1001) {
            i = 17;
          }
          for (;;)
          {
            localNotification.flags = i;
            break;
            if (JPushInterface.a(paramb.i)) {
              break;
            }
            if (1 == paramb.j) {
              paramb.v = 1;
            }
            switch (paramb.v)
            {
            case 0: 
            default: 
              i = 1;
              break;
            case 2: 
              i = 32;
              break;
            case 1: 
              i = 16;
            }
            i |= 0x1;
          }
          if (cn.jpush.android.e.a.d(paramContext)) {
            localNotification.defaults = 0;
          }
          if (localNotification != null)
          {
            localObject1 = new StringBuilder("notification notify:");
            ((StringBuilder)localObject1).append(j);
            cn.jpush.android.e.g.a("NotificationHelper", ((StringBuilder)localObject1).toString());
            localNotificationManager.notify(j, localNotification);
          }
          if ((1 != paramb.j) && (paramb.g == 0))
          {
            a(paramContext, j, true);
            e.a(paramb.e, 1018, null, paramContext);
          }
          if ((paramb != null) && (paramb.g == 0)) {
            a(paramContext, localMap, j, str2, str1, paramb);
          }
        }
        else
        {
          cn.jpush.android.e.g.c("NotificationHelper", "Got NULL notification. Give up to show.");
        }
      }
    }
  }
  
  public static Intent c(Context paramContext, cn.jpush.android.data.b paramb)
  {
    if (paramContext == null)
    {
      cn.jpush.android.e.g.c("NotificationHelper", "context was null");
      return null;
    }
    if (paramb != null)
    {
      cn.jpush.android.data.g localg = (cn.jpush.android.data.g)paramb;
      if ((3 != localg.ac) && (4 != localg.ac) && (localg.ac != 0) && (2 == localg.ac))
      {
        paramContext = new Intent(paramContext, PopWinActivity.class);
        paramContext.putExtra("body", paramb);
        paramContext.addFlags(335544320);
        return paramContext;
      }
    }
    paramContext = cn.jpush.android.e.a.a(paramContext, paramb, false);
    return paramContext;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */