package cn.jpush.android.api;

import java.util.Set;

public final class a
{
  public String a;
  public Set<String> b;
  public TagAliasCallback c;
  public int d;
  public int e = 0;
  public int f = 0;
  private long g;
  
  public a(int paramInt1, String paramString, long paramLong, int paramInt2, int paramInt3)
  {
    this.d = paramInt1;
    this.a = paramString;
    this.g = paramLong;
    this.e = paramInt2;
    this.f = paramInt3;
  }
  
  public a(int paramInt1, Set<String> paramSet, long paramLong, int paramInt2, int paramInt3)
  {
    this.d = paramInt1;
    this.b = paramSet;
    this.g = paramLong;
    this.e = paramInt2;
    this.f = paramInt3;
  }
  
  public a(String paramString, Set<String> paramSet, TagAliasCallback paramTagAliasCallback, long paramLong, int paramInt1, int paramInt2)
  {
    this.a = paramString;
    this.b = paramSet;
    this.c = paramTagAliasCallback;
    this.g = paramLong;
    this.e = paramInt1;
    this.f = paramInt2;
  }
  
  public final boolean a(long paramLong)
  {
    return (this.e == 0) && (System.currentTimeMillis() - this.g > 30000L);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("CallBackParams{sendTime=");
    localStringBuilder.append(this.g);
    localStringBuilder.append(", alias='");
    localStringBuilder.append(this.a);
    localStringBuilder.append('\'');
    localStringBuilder.append(", tags=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", tagAliasCallBack=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", sequence=");
    localStringBuilder.append(this.d);
    localStringBuilder.append(", protoType=");
    localStringBuilder.append(this.e);
    localStringBuilder.append(", action=");
    localStringBuilder.append(this.f);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */