package cn.jpush.android.api;

import android.content.Context;
import android.widget.RemoteViews;
import cn.jpush.android.a;

public class CustomPushNotificationBuilder
  extends BasicPushNotificationBuilder
{
  public int layout;
  public int layoutContentId;
  public int layoutIconDrawable = a.b;
  public int layoutIconId;
  public int layoutTimeId;
  public int layoutTitleId;
  
  CustomPushNotificationBuilder(Context paramContext)
  {
    super(paramContext);
  }
  
  public CustomPushNotificationBuilder(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super(paramContext);
    this.layout = paramInt1;
    this.layoutIconId = paramInt2;
    this.layoutTitleId = paramInt3;
    this.layoutContentId = paramInt4;
  }
  
  public CustomPushNotificationBuilder(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    super(paramContext);
    this.layout = paramInt1;
    this.layoutIconId = paramInt2;
    this.layoutTitleId = paramInt3;
    this.layoutContentId = paramInt4;
    this.layoutTimeId = paramInt5;
  }
  
  final String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.a());
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layout);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layoutIconId);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layoutTitleId);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layoutContentId);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layoutIconDrawable);
    localStringBuilder.append("_____");
    localStringBuilder.append(this.layoutTimeId);
    return localStringBuilder.toString();
  }
  
  final void a(String[] paramArrayOfString)
  {
    super.a(paramArrayOfString);
    this.layout = Integer.parseInt(paramArrayOfString[5]);
    this.layoutIconId = Integer.parseInt(paramArrayOfString[6]);
    this.layoutTitleId = Integer.parseInt(paramArrayOfString[7]);
    this.layoutContentId = Integer.parseInt(paramArrayOfString[8]);
    this.layoutIconDrawable = Integer.parseInt(paramArrayOfString[9]);
    if (paramArrayOfString.length == 11) {
      this.layoutTimeId = Integer.parseInt(paramArrayOfString[10]);
    }
  }
  
  RemoteViews buildContentView(String paramString1, String paramString2)
  {
    RemoteViews localRemoteViews = new RemoteViews(this.a.getPackageName(), this.layout);
    localRemoteViews.setTextViewText(this.layoutTitleId, paramString2);
    localRemoteViews.setImageViewResource(this.layoutIconId, this.layoutIconDrawable);
    localRemoteViews.setTextViewText(this.layoutContentId, paramString1);
    int i = this.layoutTimeId;
    if (i != 0) {
      localRemoteViews.setLong(i, "setTime", System.currentTimeMillis());
    }
    return localRemoteViews;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("custom_____");
    localStringBuilder.append(a());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/CustomPushNotificationBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */