package cn.jpush.android.api;

import android.app.Notification;
import java.util.Map;

public abstract interface PushNotificationBuilder
{
  public abstract Notification buildNotification(Map<String, String> paramMap);
  
  public abstract String getDeveloperArg0();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/api/PushNotificationBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */