package cn.jpush.android.f.a;

import android.text.TextUtils;
import android.webkit.WebView;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public final class d
{
  private HashMap<String, Method> a;
  private String b;
  private String c;
  
  public d(String paramString, Class paramClass)
  {
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        this.b = paramString;
        paramString = new java/util/HashMap;
        paramString.<init>();
        this.a = paramString;
        Method[] arrayOfMethod = paramClass.getDeclaredMethods();
        paramClass = new java/lang/StringBuilder;
        paramClass.<init>("javascript:(function(b){console.log(\"");
        paramClass.append(this.b);
        paramClass.append(" initialization begin\");var a={queue:[],callback:function(){var d=Array.prototype.slice.call(arguments,0);var c=d.shift();var e=d.shift();this.queue[c].apply(this,d);if(!e){delete this.queue[c]}}};");
        int j = arrayOfMethod.length;
        for (int i = 0; i < j; i++)
        {
          Method localMethod = arrayOfMethod[i];
          if (localMethod.getModifiers() == 9)
          {
            paramString = a(localMethod);
            if (paramString != null)
            {
              this.a.put(paramString, localMethod);
              paramClass.append(String.format(Locale.ENGLISH, "a.%s=", new Object[] { localMethod.getName() }));
            }
          }
        }
        paramClass.append("function(){var f=Array.prototype.slice.call(arguments,0);if(f.length<1){throw\"");
        paramClass.append(this.b);
        paramClass.append(" call error, message:miss method name\"}var e=[];for(var h=1;h<f.length;h++){var c=f[h];var j=typeof c;e[e.length]=j;if(j==\"function\"){var d=a.queue.length;a.queue[d]=c;f[h]=d}}var g=JSON.parse(prompt(JSON.stringify({method:f.shift(),types:e,args:f})));if(g.code!=200){throw\"");
        paramClass.append(this.b);
        paramClass.append(" call error, code:\"+g.code+\", message:\"+g.result}return g.result};Object.getOwnPropertyNames(a).forEach(function(d){var c=a[d];if(typeof c===\"function\"&&d!==\"callback\"){a[d]=function(){return c.apply(a,[d].concat(Array.prototype.slice.call(arguments,0)))}}});b.");
        paramClass.append(this.b);
        paramClass.append("=a;console.log(\"");
        paramClass.append(this.b);
        paramClass.append(" initialization end\")})(window);");
        this.c = paramClass.toString();
        return;
      }
      paramString = new java/lang/Exception;
      paramString.<init>("injected name can not be null");
      throw paramString;
    }
    catch (Exception paramString) {}
  }
  
  private static String a(int paramInt, Object paramObject)
  {
    String str = "";
    if (paramObject == null)
    {
      str = "null";
    }
    else if ((paramObject instanceof String))
    {
      str = ((String)paramObject).replace("\"", "\\\"");
      paramObject = new StringBuilder("\"");
      ((StringBuilder)paramObject).append(str);
      ((StringBuilder)paramObject).append("\"");
      str = ((StringBuilder)paramObject).toString();
    }
    else if (((paramObject instanceof Integer)) || ((paramObject instanceof Long)) || ((paramObject instanceof Boolean)) || ((paramObject instanceof Float)) || ((paramObject instanceof Double)) || ((paramObject instanceof JSONObject)))
    {
      str = String.valueOf(paramObject);
    }
    return String.format(Locale.ENGLISH, "{\"code\": %d, \"result\": %s}", new Object[] { Integer.valueOf(paramInt), str });
  }
  
  private static String a(Method paramMethod)
  {
    Object localObject = paramMethod.getName();
    Class[] arrayOfClass = paramMethod.getParameterTypes();
    int j = arrayOfClass.length;
    if ((j > 0) && (arrayOfClass[0] == WebView.class))
    {
      int i = 1;
      paramMethod = (Method)localObject;
      while (i < j)
      {
        localObject = arrayOfClass[i];
        if (localObject == String.class)
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append(paramMethod);
          paramMethod = "_S";
        }
        for (;;)
        {
          ((StringBuilder)localObject).append(paramMethod);
          paramMethod = ((StringBuilder)localObject).toString();
          break;
          if ((localObject != Integer.TYPE) && (localObject != Long.TYPE) && (localObject != Float.TYPE) && (localObject != Double.TYPE))
          {
            if (localObject == Boolean.TYPE)
            {
              localObject = new StringBuilder();
              ((StringBuilder)localObject).append(paramMethod);
              paramMethod = "_B";
            }
            else if (localObject == JSONObject.class)
            {
              localObject = new StringBuilder();
              ((StringBuilder)localObject).append(paramMethod);
              paramMethod = "_O";
            }
            else
            {
              localObject = new StringBuilder();
              ((StringBuilder)localObject).append(paramMethod);
              paramMethod = "_P";
            }
          }
          else
          {
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append(paramMethod);
            paramMethod = "_N";
          }
        }
        i++;
      }
      return paramMethod;
    }
    return null;
  }
  
  public final String a()
  {
    return this.c;
  }
  
  public final String a(WebView paramWebView, String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      try
      {
        Object localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>(paramString);
        paramString = ((JSONObject)localObject).getString("method");
        JSONArray localJSONArray2 = ((JSONObject)localObject).getJSONArray("types");
        JSONArray localJSONArray1 = ((JSONObject)localObject).getJSONArray("args");
        int k = localJSONArray2.length();
        Object[] arrayOfObject = new Object[k + 1];
        int j = 0;
        arrayOfObject[0] = paramWebView;
        int i = 0;
        paramWebView = paramString;
        for (;;)
        {
          localObject = null;
          paramString = null;
          if (j >= k) {
            break;
          }
          String str = localJSONArray2.optString(j);
          if ("string".equals(str))
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append(paramWebView);
            ((StringBuilder)localObject).append("_S");
            localObject = ((StringBuilder)localObject).toString();
            if (localJSONArray1.isNull(j)) {
              paramWebView = paramString;
            } else {
              paramWebView = localJSONArray1.getString(j);
            }
            arrayOfObject[(j + 1)] = paramWebView;
            paramWebView = (WebView)localObject;
          }
          else if ("number".equals(str))
          {
            paramString = new java/lang/StringBuilder;
            paramString.<init>();
            paramString.append(paramWebView);
            paramString.append("_N");
            paramWebView = paramString.toString();
            i = i * 10 + j + 1;
          }
          else if ("boolean".equals(str))
          {
            paramString = new java/lang/StringBuilder;
            paramString.<init>();
            paramString.append(paramWebView);
            paramString.append("_B");
            paramWebView = paramString.toString();
            arrayOfObject[(j + 1)] = Boolean.valueOf(localJSONArray1.getBoolean(j));
          }
          else if ("object".equals(str))
          {
            paramString = new java/lang/StringBuilder;
            paramString.<init>();
            paramString.append(paramWebView);
            paramString.append("_O");
            paramString = paramString.toString();
            if (localJSONArray1.isNull(j)) {
              paramWebView = (WebView)localObject;
            } else {
              paramWebView = localJSONArray1.getJSONObject(j);
            }
            arrayOfObject[(j + 1)] = paramWebView;
            paramWebView = paramString;
          }
          else
          {
            paramString = new java/lang/StringBuilder;
            paramString.<init>();
            paramString.append(paramWebView);
            paramString.append("_P");
            paramWebView = paramString.toString();
          }
          j++;
        }
        paramString = (Method)this.a.get(paramWebView);
        if (paramString == null)
        {
          paramString = new java/lang/StringBuilder;
          paramString.<init>("not found method(");
          paramString.append(paramWebView);
          paramString.append(") with valid parameters");
          return a(500, paramString.toString());
        }
        if (i > 0)
        {
          paramWebView = paramString.getParameterTypes();
          while (i > 0)
          {
            j = i - i / 10 * 10;
            localObject = paramWebView[j];
            if (localObject == Integer.TYPE) {
              arrayOfObject[j] = Integer.valueOf(localJSONArray1.getInt(j - 1));
            } else if (localObject == Long.TYPE) {
              arrayOfObject[j] = Long.valueOf(Long.parseLong(localJSONArray1.getString(j - 1)));
            } else {
              arrayOfObject[j] = Double.valueOf(localJSONArray1.getDouble(j - 1));
            }
            i /= 10;
          }
        }
        paramWebView = a(200, paramString.invoke(null, arrayOfObject));
        return paramWebView;
      }
      catch (Exception paramWebView)
      {
        if (paramWebView.getCause() == null) {
          break label607;
        }
      }
      paramString = new StringBuilder("method execute error:");
      paramWebView = paramWebView.getCause().getMessage();
      paramString.append(paramWebView);
    }
    for (paramWebView = paramString.toString();; paramWebView = "call data empty")
    {
      return a(500, paramWebView);
      label607:
      paramString = new StringBuilder("method execute error:");
      paramWebView = paramWebView.getMessage();
      break;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/f/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */