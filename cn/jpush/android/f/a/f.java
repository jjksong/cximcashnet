package cn.jpush.android.f.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.api.c;
import cn.jpush.android.b.e;
import cn.jpush.android.b.m;
import cn.jpush.android.data.b;
import cn.jpush.android.e.a;
import cn.jpush.android.e.g;
import cn.jpush.android.ui.PopWinActivity;
import cn.jpush.android.ui.PushActivity;
import java.lang.ref.WeakReference;

public final class f
{
  private final WeakReference<Activity> a;
  private final b b;
  
  public f(Context paramContext, b paramb)
  {
    this.a = new WeakReference((Activity)paramContext);
    this.b = paramb;
  }
  
  @JavascriptInterface
  private void userClick(String paramString)
  {
    int i;
    try
    {
      i = Integer.parseInt(paramString);
    }
    catch (Exception paramString)
    {
      i = 1100;
    }
    e.a(this.b.e, i, null, (Context)this.a.get());
  }
  
  @JavascriptInterface
  public final void click(String paramString1, String paramString2, String paramString3)
  {
    if (this.a.get() != null)
    {
      userClick(paramString1);
      try
      {
        bool1 = Boolean.parseBoolean(paramString2);
        bool2 = false;
      }
      catch (Exception paramString1)
      {
        try
        {
          bool2 = Boolean.parseBoolean(paramString3);
        }
        catch (Exception paramString1)
        {
          boolean bool1;
          boolean bool2;
          for (;;) {}
        }
        paramString1 = paramString1;
        bool1 = false;
      }
      if (bool2) {
        c.a((Context)this.a.get(), this.b, 0);
      }
      if (bool1) {
        ((Activity)this.a.get()).finish();
      }
    }
  }
  
  @JavascriptInterface
  public final void close()
  {
    if (this.a.get() != null) {
      ((Activity)this.a.get()).finish();
    }
  }
  
  @JavascriptInterface
  public final void createShortcut(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      i = Integer.parseInt(paramString3);
    }
    catch (Exception paramString3)
    {
      i = 0;
    }
    if (this.a.get() == null) {
      return;
    }
    paramString3 = (Context)this.a.get();
    int i = c.a(i);
    paramString2 = Uri.parse(paramString2);
    if (paramString2 == null) {
      return;
    }
    Intent localIntent1 = new Intent("android.intent.action.VIEW", paramString2);
    localIntent1.setFlags(335544320);
    try
    {
      paramString2 = Intent.ShortcutIconResource.fromContext(paramString3, i);
      Intent localIntent2 = new android/content/Intent;
      localIntent2.<init>("com.android.launcher.action.INSTALL_SHORTCUT");
      localIntent2.putExtra("duplicate", false);
      localIntent2.putExtra("android.intent.extra.shortcut.NAME", paramString1);
      localIntent2.putExtra("android.intent.extra.shortcut.INTENT", localIntent1);
      localIntent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", paramString2);
      localIntent2.setPackage(paramString3.getPackageName());
      paramString3.sendBroadcast(localIntent2);
      return;
    }
    catch (Throwable paramString2)
    {
      paramString1 = new StringBuilder("createShortCut error:");
      paramString1.append(paramString2.getMessage());
      g.c("AndroidUtil", paramString1.toString());
    }
  }
  
  @JavascriptInterface
  public final void download(String paramString)
  {
    if (this.a.get() == null) {}
  }
  
  @JavascriptInterface
  public final void download(String paramString1, String paramString2)
  {
    if (this.a.get() == null) {
      return;
    }
    userClick(paramString1);
    download(paramString2);
    c.a((Context)this.a.get(), this.b, 0);
    ((Activity)this.a.get()).finish();
  }
  
  @JavascriptInterface
  public final void download(String paramString1, String paramString2, String paramString3)
  {
    download(paramString1, paramString2);
  }
  
  @JavascriptInterface
  public final void executeMsgMessage(String paramString)
  {
    if (JCoreInterface.getDebugMode())
    {
      if (this.a.get() == null) {
        return;
      }
      m.a((Context)this.a.get(), paramString);
    }
  }
  
  @JavascriptInterface
  public final void showTitleBar()
  {
    if ((this.a.get() != null) && ((this.a.get() instanceof PushActivity))) {
      ((PushActivity)this.a.get()).a();
    }
  }
  
  @JavascriptInterface
  public final void showToast(String paramString)
  {
    if (this.a.get() != null) {
      Toast.makeText((Context)this.a.get(), paramString, 0).show();
    }
  }
  
  @JavascriptInterface
  public final void startActivityByIntent(String paramString1, String paramString2)
  {
    Context localContext = (Context)this.a.get();
    if (localContext == null) {
      return;
    }
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramString1);
      localIntent.addCategory(localContext.getPackageName());
      localIntent.putExtra("cn.jpush.android.EXTRA", paramString2);
      localIntent.setFlags(268435456);
      localContext.startActivity(localIntent);
      return;
    }
    catch (Exception paramString2)
    {
      paramString2 = new StringBuilder("Unhandle intent : ");
      paramString2.append(paramString1);
      g.d("WebViewHelper", paramString2.toString());
    }
  }
  
  @JavascriptInterface
  public final void startActivityByName(String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString1)) {
      g.d("WebViewHelper", "The activity name is null or empty, Give up..");
    }
    Context localContext = (Context)this.a.get();
    if (localContext == null) {
      return;
    }
    try
    {
      paramString1 = Class.forName(paramString1);
      if (paramString1 != null)
      {
        paramString1 = new Intent(localContext, paramString1);
        paramString1.putExtra("cn.jpush.android.ACTIVITY_PARAM", paramString2);
        paramString1.setFlags(268435456);
        localContext.startActivity(paramString1);
      }
      return;
    }
    catch (Exception paramString1)
    {
      g.d("WebViewHelper", "The activity name is invalid, Give up..");
    }
  }
  
  @JavascriptInterface
  public final void startMainActivity(String paramString)
  {
    Context localContext = (Context)this.a.get();
    if (localContext == null) {
      return;
    }
    try
    {
      ((Activity)localContext).finish();
      a.d(localContext, paramString);
      return;
    }
    catch (Exception paramString)
    {
      g.d("WebViewHelper", "startMainActivity failed");
    }
  }
  
  @JavascriptInterface
  public final void startPushActivity(String paramString)
  {
    if ((this.a.get() != null) && ((this.a.get() instanceof PopWinActivity))) {
      ((PopWinActivity)this.a.get()).a(paramString);
    }
  }
  
  @JavascriptInterface
  public final void triggerNativeAction(String paramString)
  {
    Context localContext = (Context)this.a.get();
    if (localContext == null) {
      return;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("cn.jpush.android.EXTRA", paramString);
    a.a(localContext, "cn.jpush.android.intent.ACTION_RICHPUSH_CALLBACK", localBundle);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/f/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */