package cn.jpush.android.f.a;

import android.webkit.WebView;

public class b
{
  private static final String TAG = "HostJsScope";
  private static f mWebViewHelper;
  
  public static void click(WebView paramWebView, String paramString1, String paramString2, String paramString3)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.click(paramString1, paramString2, paramString3);
  }
  
  public static void close(WebView paramWebView)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.close();
  }
  
  public static void createShortcut(WebView paramWebView, String paramString1, String paramString2, String paramString3)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.createShortcut(paramString1, paramString2, paramString3);
  }
  
  public static void download(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.download(paramString);
  }
  
  public static void download(WebView paramWebView, String paramString1, String paramString2)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.download(paramString1, paramString2);
  }
  
  public static void download(WebView paramWebView, String paramString1, String paramString2, String paramString3)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.download(paramString1, paramString2, paramString3);
  }
  
  public static void executeMsgMessage(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.executeMsgMessage(paramString);
  }
  
  public static void setWebViewHelper(f paramf)
  {
    if (paramf == null) {
      return;
    }
    mWebViewHelper = paramf;
  }
  
  public static void showTitleBar(WebView paramWebView)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView != null) {
      paramWebView.showTitleBar();
    }
  }
  
  public static void showToast(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.showToast(paramString);
  }
  
  public static void startActivityByIntent(WebView paramWebView, String paramString1, String paramString2)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.startActivityByIntent(paramString1, paramString2);
  }
  
  public static void startActivityByName(WebView paramWebView, String paramString1, String paramString2)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.startActivityByName(paramString1, paramString2);
  }
  
  public static void startActivityByNameWithSystemAlert(WebView paramWebView, String paramString1, String paramString2)
  {
    if (cn.jpush.android.api.e.a == null) {
      return;
    }
    cn.jpush.android.api.e.a.a(paramString1, paramString2);
  }
  
  public static void startMainActivity(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.startMainActivity(paramString);
  }
  
  public static void startPushActivity(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView != null) {
      paramWebView.startPushActivity(paramString);
    }
  }
  
  public static void triggerNativeAction(WebView paramWebView, String paramString)
  {
    paramWebView = mWebViewHelper;
    if (paramWebView == null) {
      return;
    }
    paramWebView.triggerNativeAction(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/f/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */