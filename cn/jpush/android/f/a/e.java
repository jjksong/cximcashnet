package cn.jpush.android.f.a;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import cn.jpush.android.e.g;

public final class e
{
  private Context a;
  private WindowManager b;
  private WebView c;
  private ImageButton d;
  
  public final void a(String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString1)) {
      g.d("SystemAlertWebViewCallback", "The activity name is null or empty, Give up..");
    }
    if (this.a == null) {
      return;
    }
    try
    {
      paramString1 = Class.forName(paramString1);
      if (paramString1 != null)
      {
        paramString1 = new Intent(this.a, paramString1);
        paramString1.putExtra("cn.jpush.android.ACTIVITY_PARAM", paramString2);
        paramString1.setFlags(268435456);
        this.a.startActivity(paramString1);
        cn.jpush.android.api.e.a(this.b, this.c, this.d);
      }
      return;
    }
    catch (Exception paramString1)
    {
      g.d("SystemAlertWebViewCallback", "The activity name is invalid, Give up..");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/f/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */