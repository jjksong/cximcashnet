package cn.jpush.android.f.a;

import android.os.Build.VERSION;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import cn.jpush.android.e.g;

public class c
  extends WebChromeClient
{
  private final String a = "InjectedChromeClient";
  private d b;
  private boolean c;
  
  public c(String paramString, Class paramClass)
  {
    this.b = new d(paramString, paramClass);
  }
  
  public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    paramJsResult.confirm();
    return true;
  }
  
  public boolean onJsPrompt(WebView paramWebView, String paramString1, String paramString2, String paramString3, JsPromptResult paramJsPromptResult)
  {
    if (Build.VERSION.SDK_INT < 17) {
      paramJsPromptResult.confirm(this.b.a(paramWebView, paramString2));
    }
    return true;
  }
  
  public void onProgressChanged(WebView paramWebView, int paramInt)
  {
    paramWebView.getSettings().setSavePassword(false);
    if (Build.VERSION.SDK_INT < 17) {
      if (paramInt <= 25)
      {
        this.c = false;
      }
      else if (!this.c)
      {
        g.a("InjectedChromeClient", "Android sdk version lesser than 17, Java—Js interact by injection!");
        paramWebView.loadUrl(this.b.a());
        this.c = true;
      }
    }
    super.onProgressChanged(paramWebView, paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/f/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */