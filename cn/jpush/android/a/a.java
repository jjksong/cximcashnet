package cn.jpush.android.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.HandlerThread;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.e.g;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public final class a
  extends e
{
  private static volatile Object j = new Object();
  private long d;
  private LocationManager e;
  private Handler f;
  private long g = 900000L;
  private long h = 20000L;
  private String i;
  private boolean k = false;
  @SuppressLint({"MissingPermission"})
  private LocationListener l = new b(this);
  
  public a(Context paramContext)
  {
    super(paramContext);
    this.e = ((LocationManager)paramContext.getSystemService("location"));
    this.g = MultiSpHelper.getLong(paramContext, "geofence_interval", 900000L);
    this.d = MultiSpHelper.getLong(paramContext, "geofence_interval", -1L);
  }
  
  private void a(Location paramLocation)
  {
    Object localObject = new StringBuilder("current location:");
    ((StringBuilder)localObject).append(paramLocation);
    g.a("CustomGeofenAction", ((StringBuilder)localObject).toString());
    localObject = this.f;
    if ((localObject != null) && (((Handler)localObject).hasMessages(1001))) {
      this.f.removeMessages(1001);
    }
    if (paramLocation == null) {
      return;
    }
    double d4 = paramLocation.getLongitude();
    double d3 = paramLocation.getLatitude();
    int n = 0;
    localObject = this.b.entrySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      cn.jpush.android.data.a locala = (cn.jpush.android.data.a)((Map.Entry)((Iterator)localObject).next()).getValue();
      if (locala.P * 1000L <= System.currentTimeMillis())
      {
        paramLocation = new StringBuilder("Out of date geofence ");
        paramLocation.append(locala.K);
        g.a("CustomGeofenAction", paramLocation.toString());
        ((Iterator)localObject).remove();
      }
      else
      {
        double d2 = cn.jpush.android.e.e.a(d4, d3, locala.Q, locala.R);
        paramLocation = new StringBuilder();
        paramLocation.append(locala.K);
        paramLocation.append(" distance to center:");
        paramLocation.append(d2);
        g.a("CustomGeofenAction", paramLocation.toString());
        if (d2 <= locala.L) {
          paramLocation = "in";
        } else {
          paramLocation = "out";
        }
        double d1 = locala.L;
        Double.isNaN(d1);
        int m = n;
        if (Math.abs(d2 - d1) < 1000.0D) {
          m = 1;
        }
        n = m;
        if (!paramLocation.equals(locala.S))
        {
          if ((locala.S != null) && (paramLocation.equals(locala.M)) && (!paramLocation.equals(locala.S)) && ((locala.N) || ((!locala.N) && (!locala.O))))
          {
            locala.O = true;
            b(locala);
            if (!locala.N)
            {
              StringBuilder localStringBuilder = new StringBuilder("No repeat geofence ");
              localStringBuilder.append(locala.K);
              g.a("CustomGeofenAction", localStringBuilder.toString());
              this.b.remove(locala.K);
              c();
              c(locala);
            }
          }
          locala.S = paramLocation;
          n = m;
        }
      }
    }
    if (this.d == -1L)
    {
      long l1;
      if (n != 0) {
        l1 = 180000L;
      } else {
        l1 = 900000L;
      }
      this.g = l1;
    }
    c();
  }
  
  private void b(long paramLong)
  {
    Object localObject = new StringBuilder("Scan geofence after ");
    ((StringBuilder)localObject).append(paramLong);
    ((StringBuilder)localObject).append("ms");
    g.a("CustomGeofenAction", ((StringBuilder)localObject).toString());
    localObject = this.f;
    if (localObject == null) {
      return;
    }
    if (((Handler)localObject).hasMessages(1000)) {
      this.f.removeMessages(1000);
    }
    this.f.sendEmptyMessageDelayed(1000, paramLong);
  }
  
  private boolean d()
  {
    boolean bool = false;
    try
    {
      if (this.e != null)
      {
        if ((!this.e.isProviderEnabled("gps")) && (!this.e.isProviderEnabled("network")))
        {
          bool = this.e.isProviderEnabled("passive");
          if (!bool) {
            return false;
          }
        }
        bool = true;
      }
      return bool;
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder("Check GPS enable failed:");
      localStringBuilder.append(localThrowable);
      g.c("CustomGeofenAction", localStringBuilder.toString());
      return false;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      g.c("CustomGeofenAction", "The provider [gps] is illegal argument!");
      return false;
    }
    catch (SecurityException localSecurityException)
    {
      g.c("CustomGeofenAction", "No suitable permission is present when get GPS_PROVIDER!");
    }
    return false;
  }
  
  protected final void a()
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("geofence size:");
      localStringBuilder.append(this.b.size());
      g.a("CustomGeofenAction", localStringBuilder.toString());
      g.a("CustomGeofenAction", "stop listen geofence");
      if (this.k)
      {
        if (this.f != null) {
          this.f.removeMessages(1000);
        }
        this.k = false;
      }
      return;
    }
    finally {}
  }
  
  public final void a(long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder("Set geofence interval ");
    localStringBuilder.append(paramLong);
    g.a("CustomGeofenAction", localStringBuilder.toString());
    this.g = paramLong;
    this.d = paramLong;
    MultiSpHelper.commitLong(this.a, "geofence_interval", paramLong);
  }
  
  protected final void a(cn.jpush.android.data.b paramb)
  {
    StringBuilder localStringBuilder = new StringBuilder("Geofence create success ");
    localStringBuilder.append(paramb.K);
    g.a("CustomGeofenAction", localStringBuilder.toString());
    b(0L);
  }
  
  public final void b()
  {
    try
    {
      g.a("CustomGeofenAction", "start listen geofence");
      if (this.k)
      {
        g.b("CustomGeofenAction", "geofence is running!");
        return;
      }
      if (this.b.size() == 0)
      {
        g.a("CustomGeofenAction", "No geofence,not need listen");
        return;
      }
      Object localObject1 = this.f;
      if (localObject1 == null) {
        try
        {
          localObject1 = new cn/jpush/android/a/c;
          ((c)localObject1).<init>(this, "CustomGeofenAction");
          ((HandlerThread)localObject1).start();
          d locald = new cn/jpush/android/a/d;
          locald.<init>(this, ((HandlerThread)localObject1).getLooper());
          this.f = locald;
        }
        catch (Throwable localThrowable)
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("init geofence handler failed:");
          ((StringBuilder)localObject1).append(localThrowable);
          g.c("CustomGeofenAction", ((StringBuilder)localObject1).toString());
        }
      }
      this.f.sendEmptyMessage(1000);
      this.k = true;
      return;
    }
    finally {}
  }
  
  protected final void b(cn.jpush.android.data.b paramb)
  {
    StringBuilder localStringBuilder = new StringBuilder("Geofence update success ");
    localStringBuilder.append(paramb.K);
    g.a("CustomGeofenAction", localStringBuilder.toString());
    b(0L);
  }
  
  protected final void c(cn.jpush.android.data.b paramb)
  {
    StringBuilder localStringBuilder = new StringBuilder("Geofence delete success ");
    localStringBuilder.append(paramb.K);
    g.a("CustomGeofenAction", localStringBuilder.toString());
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */