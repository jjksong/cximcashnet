package cn.jpush.android.a;

import android.content.Context;
import android.text.TextUtils;
import cn.jiguang.api.MultiSpHelper;
import cn.jpush.android.b.o;
import cn.jpush.android.data.b;
import cn.jpush.android.e.g;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONObject;

public abstract class e
{
  protected Context a;
  protected LinkedHashMap<String, cn.jpush.android.data.a> b;
  protected int c = 100;
  
  public e(Context paramContext)
  {
    this.a = paramContext.getApplicationContext();
    this.c = MultiSpHelper.getInt(paramContext, "geofence_max_num", 10);
    try
    {
      localObject = new java/io/File;
      ((File)localObject).<init>(paramContext.getFilesDir(), "jpush_geofence");
      this.b = ((LinkedHashMap)cn.jpush.android.e.a.a((File)localObject));
    }
    catch (Throwable paramContext)
    {
      Object localObject = new StringBuilder("recover geofence failed:");
      ((StringBuilder)localObject).append(paramContext);
      g.a("GeofenceAction", ((StringBuilder)localObject).toString());
    }
    if (this.b == null) {
      this.b = new LinkedHashMap();
    }
    d();
    paramContext = new StringBuilder("Recover geofence size:");
    paramContext.append(this.b.size());
    g.a("GeofenceAction", paramContext.toString());
    e();
  }
  
  private void d()
  {
    Iterator localIterator = this.b.entrySet().iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      cn.jpush.android.data.a locala = (cn.jpush.android.data.a)((Map.Entry)localIterator.next()).getValue();
      if (locala.P * 1000L <= System.currentTimeMillis())
      {
        i = 1;
        StringBuilder localStringBuilder = new StringBuilder("Geofence ");
        localStringBuilder.append(locala.K);
        localStringBuilder.append(" is out of date!");
        g.a("GeofenceAction", localStringBuilder.toString());
        localIterator.remove();
        c(locala);
      }
    }
    if (i != 0) {
      c();
    }
  }
  
  private void e()
  {
    if (this.b.size() > this.c)
    {
      Object localObject = new StringBuilder("Geofence num more than max limit ");
      ((StringBuilder)localObject).append(this.c);
      ((StringBuilder)localObject).append(",we will remove them which create time is earlier!");
      g.a("GeofenceAction", ((StringBuilder)localObject).toString());
      localObject = this.b.entrySet().iterator();
      int i = 0;
      int k = this.b.size();
      int j = this.c;
      while ((((Iterator)localObject).hasNext()) && (i < k - j))
      {
        cn.jpush.android.data.a locala = (cn.jpush.android.data.a)((Map.Entry)((Iterator)localObject).next()).getValue();
        ((Iterator)localObject).remove();
        c(locala);
        i++;
      }
      c();
    }
  }
  
  protected void a() {}
  
  public final void a(int paramInt)
  {
    MultiSpHelper.commitInt(this.a, "geofence_max_num", paramInt);
    this.c = paramInt;
    e();
  }
  
  public void a(long paramLong) {}
  
  public final void a(cn.jpush.android.data.a parama)
  {
    Object localObject1 = new StringBuilder("Current geofence size:");
    ((StringBuilder)localObject1).append(this.b.size());
    g.a("GeofenceAction", ((StringBuilder)localObject1).toString());
    int i;
    if ((parama != null) && (!TextUtils.isEmpty(parama.K)) && (parama.L != -1L) && (parama.R >= -90.0D) && (parama.R <= 90.0D) && (parama.Q >= -180.0D) && (parama.Q <= 180.0D))
    {
      i = 1;
    }
    else
    {
      g.a("GeofenceAction", "The geofence is invalid,not need operate!");
      i = 0;
    }
    if (i == 0) {
      return;
    }
    localObject1 = (b)this.b.get(parama.K);
    if (localObject1 != null)
    {
      if (parama.P * 1000L <= System.currentTimeMillis())
      {
        this.b.remove(parama.K);
        c((b)localObject1);
      }
      else
      {
        this.b.put(parama.K, parama);
        b((b)localObject1);
      }
    }
    else
    {
      if (parama.P * 1000L <= System.currentTimeMillis())
      {
        localObject1 = new StringBuilder("The geofence");
        ((StringBuilder)localObject1).append(parama.K);
        ((StringBuilder)localObject1).append("is out of date, will not create!");
        g.a("GeofenceAction", ((StringBuilder)localObject1).toString());
        return;
      }
      if (this.b.size() == this.c)
      {
        localObject1 = "";
        Object localObject2 = this.b.keySet().iterator();
        if (((Iterator)localObject2).hasNext()) {
          localObject1 = (String)((Iterator)localObject2).next();
        }
        localObject2 = new StringBuilder("Geofence num limit,");
        ((StringBuilder)localObject2).append(this.c);
        ((StringBuilder)localObject2).append(" need remove earliest create one:");
        ((StringBuilder)localObject2).append((String)localObject1);
        g.a("GeofenceAction", ((StringBuilder)localObject2).toString());
        c((cn.jpush.android.data.a)this.b.remove(localObject1));
      }
      this.b.put(parama.K, parama);
      a(parama);
    }
    c();
    if (this.b.size() == 0)
    {
      a();
      return;
    }
    b();
  }
  
  protected abstract void a(b paramb);
  
  public void b() {}
  
  protected final void b(cn.jpush.android.data.a parama)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(parama.b);
      parama.a = localJSONObject;
      o.a(this.a, parama, parama.c);
      parama.a = null;
      return;
    }
    catch (Throwable localThrowable)
    {
      parama = new StringBuilder("process geofence error:");
      parama.append(localThrowable);
      g.c("GeofenceAction", parama.toString());
    }
  }
  
  protected abstract void b(b paramb);
  
  protected final void c()
  {
    g.a("GeofenceAction", "save geofence to file");
    cn.jpush.android.e.a.a(new File(this.a.getFilesDir(), "jpush_geofence"), this.b);
  }
  
  protected abstract void c(b paramb);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */