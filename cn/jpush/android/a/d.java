package cn.jpush.android.a;

import android.annotation.SuppressLint;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import cn.jpush.android.e.g;

final class d
  extends Handler
{
  d(a parama, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  @SuppressLint({"MissingPermission"})
  public final void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      break;
    case 1001: 
      a.d(this.a);
      try
      {
        if ((a.e(this.a) != null) && (a.e(this.a).equals("gps")))
        {
          g.a("CustomGeofenAction", "GPS provider time out!");
          a.a(this.a, "network");
          if (!a.a(this.a).isProviderEnabled(a.e(this.a)))
          {
            g.a("CustomGeofenAction", "Network provider is disabled");
            return;
          }
          a.a(this.a).requestLocationUpdates(a.e(this.a), 2000L, 0.0F, a.f(this.a));
          a.h(this.a).sendEmptyMessageDelayed(1001, a.g(this.a) / 2L);
        }
        else
        {
          g.a("CustomGeofenAction", "Network provider time out!");
          return;
        }
      }
      catch (Throwable paramMessage)
      {
        StringBuilder localStringBuilder = new StringBuilder("request location error#");
        localStringBuilder.append(paramMessage);
        g.a("CustomGeofenAction", localStringBuilder.toString());
      }
    case 1000: 
      paramMessage = this.a;
      a.a(paramMessage, a.b(paramMessage));
      a.c(this.a);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */