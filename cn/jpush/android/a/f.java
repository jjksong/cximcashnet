package cn.jpush.android.a;

import android.content.Context;
import cn.jpush.android.e.g;

public final class f
{
  private static f a;
  private static volatile Object b = new Object();
  private e c;
  
  private f(Context paramContext)
  {
    if (paramContext == null)
    {
      g.a("GeofenceManager", "context is null,init failed");
      return;
    }
    this.c = new a(paramContext);
  }
  
  public static f a(Context paramContext)
  {
    if (a == null) {
      synchronized (b)
      {
        if (a == null)
        {
          f localf = new cn/jpush/android/a/f;
          localf.<init>(paramContext);
          a = localf;
        }
      }
    }
    return a;
  }
  
  public final void a()
  {
    this.c.b();
  }
  
  public final void a(int paramInt)
  {
    this.c.a(paramInt);
  }
  
  public final void a(long paramLong)
  {
    this.c.a(paramLong);
  }
  
  public final void a(cn.jpush.android.data.a parama)
  {
    g.a("GeofenceManager", "recv geofence...");
    this.c.a(parama);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/android/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */