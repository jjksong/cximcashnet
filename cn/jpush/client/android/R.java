package cn.jpush.client.android;

public final class R
{
  public static final class drawable
  {
    public static final int jpush_ic_richpush_actionbar_back = 2131099850;
    public static final int jpush_ic_richpush_actionbar_divider = 2131099851;
    public static final int jpush_richpush_btn_selector = 2131099852;
    public static final int jpush_richpush_progressbar = 2131099853;
  }
  
  public static final class id
  {
    public static final int actionbarLayoutId = 2131165218;
    public static final int fullWebView = 2131165440;
    public static final int imgRichpushBtnBack = 2131165484;
    public static final int imgView = 2131165485;
    public static final int popLayoutId = 2131165659;
    public static final int pushPrograssBar = 2131165668;
    public static final int push_notification_bg = 2131165669;
    public static final int push_notification_big_icon = 2131165670;
    public static final int push_notification_content = 2131165671;
    public static final int push_notification_date = 2131165672;
    public static final int push_notification_dot = 2131165673;
    public static final int push_notification_layout_lefttop = 2131165674;
    public static final int push_notification_small_icon = 2131165675;
    public static final int push_notification_title = 2131165676;
    public static final int push_root_view = 2131165677;
    public static final int rlRichpushTitleBar = 2131165720;
    public static final int tvRichpushTitle = 2131165853;
    public static final int wvPopwin = 2131165914;
  }
  
  public static final class layout
  {
    public static final int jpush_popwin_layout = 2131296445;
    public static final int jpush_webview_layout = 2131296446;
    public static final int push_notification = 2131296479;
  }
  
  public static final class style
  {
    public static final int MyDialogStyle = 2131624114;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/client/android/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */