package cn.jpush.a;

import cn.jiguang.api.JResponse;
import java.nio.ByteBuffer;

public final class a
  extends JResponse
{
  public a(Object paramObject, ByteBuffer paramByteBuffer)
  {
    super(paramObject, paramByteBuffer);
  }
  
  public final String getName()
  {
    return "CommonResponse";
  }
  
  protected final boolean isNeedParseeErrorMsg()
  {
    return true;
  }
  
  public final void parseBody()
  {
    super.parseBody();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[CommonResponse] - ");
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }
  
  public final void writeBody()
  {
    super.writeBody();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */