package cn.jpush.a;

import cn.jiguang.api.JResponse;
import cn.jiguang.api.utils.ByteBufferUtils;
import cn.jiguang.api.utils.ProtocolUtil;
import java.nio.ByteBuffer;

public final class b
  extends JResponse
{
  int a;
  long b;
  String c;
  
  public b(Object paramObject, ByteBuffer paramByteBuffer)
  {
    super(paramObject, paramByteBuffer);
  }
  
  public final int a()
  {
    return this.a;
  }
  
  public final long b()
  {
    return this.b;
  }
  
  public final String c()
  {
    return this.c;
  }
  
  public final String getName()
  {
    return "MessagePush";
  }
  
  protected final boolean isNeedParseeErrorMsg()
  {
    return false;
  }
  
  public final void parseBody()
  {
    super.parseBody();
    ByteBuffer localByteBuffer = this.body;
    this.a = ByteBufferUtils.get(localByteBuffer, this).byteValue();
    this.b = ByteBufferUtils.getLong(localByteBuffer, this);
    this.c = ProtocolUtil.getTlv2(localByteBuffer, this);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[MessagePush] - msgType:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", msgId:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", msgContent:");
    localStringBuilder.append(this.c);
    localStringBuilder.append(" - ");
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }
  
  public final void writeBody()
  {
    super.writeBody();
    writeInt1(this.a);
    writeLong8(this.b);
    writeTlv2(this.c);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */