package cn.jpush.a;

import cn.jiguang.api.JRequest;
import cn.jiguang.api.utils.ProtocolUtil;
import java.nio.ByteBuffer;

public final class c
  extends JRequest
{
  String a;
  String b;
  
  public c(int paramInt1, int paramInt2, long paramLong, String paramString1, String paramString2)
  {
    super(paramInt1, paramInt2, paramLong);
    this.a = paramString1;
    this.b = paramString2;
  }
  
  public final String a()
  {
    return this.b;
  }
  
  public final String getName()
  {
    return "TagaliasRequest";
  }
  
  protected final boolean isNeedParseeErrorMsg()
  {
    return true;
  }
  
  public final void parseBody()
  {
    ByteBuffer localByteBuffer = this.body;
    this.a = ProtocolUtil.getTlv2(localByteBuffer);
    this.b = ProtocolUtil.getTlv2(localByteBuffer);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[TagaliasRequest] - appKey:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", action:");
    localStringBuilder.append(this.b);
    localStringBuilder.append(" - ");
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }
  
  public final void writeBody()
  {
    writeTlv2(this.a);
    writeTlv2(this.b);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */