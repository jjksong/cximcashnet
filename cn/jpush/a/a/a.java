package cn.jpush.a.a;

import cn.jpush.android.e.g;

public final class a
{
  public static byte[] a(String paramString)
  {
    try
    {
      byte[] arrayOfByte = paramString.getBytes("UTF-8");
      return arrayOfByte;
    }
    catch (Throwable localThrowable)
    {
      StringBuilder localStringBuilder = new StringBuilder("stringToUtf8Bytes error:");
      localStringBuilder.append(localThrowable.getMessage());
      g.d("PushPackage", localStringBuilder.toString());
    }
    return paramString.getBytes();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */