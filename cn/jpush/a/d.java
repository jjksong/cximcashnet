package cn.jpush.a;

import cn.jiguang.api.JResponse;
import cn.jiguang.api.utils.ProtocolUtil;
import java.nio.ByteBuffer;

public final class d
  extends JResponse
{
  String a;
  long b = -1L;
  
  public d(Object paramObject, ByteBuffer paramByteBuffer)
  {
    super(paramObject, paramByteBuffer);
  }
  
  public final String a()
  {
    return this.a;
  }
  
  public final String getName()
  {
    return "TagaliasResponse";
  }
  
  protected final boolean isNeedParseeErrorMsg()
  {
    return getCommand() == 10;
  }
  
  public final void parseBody()
  {
    super.parseBody();
    if (this.code > 0) {
      return;
    }
    this.a = ProtocolUtil.getTlv2(this.body, this);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[TagaliasResponse] - action:");
    localStringBuilder.append(this.a);
    localStringBuilder.append(" - ");
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }
  
  public final void writeBody()
  {
    super.writeBody();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/cn/jpush/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */