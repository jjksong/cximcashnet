package org.reactivestreams;

public abstract interface Publisher<T>
{
  public abstract void subscribe(Subscriber<? super T> paramSubscriber);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/reactivestreams/Publisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */