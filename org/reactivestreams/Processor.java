package org.reactivestreams;

public abstract interface Processor<T, R>
  extends Subscriber<T>, Publisher<R>
{}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/reactivestreams/Processor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */