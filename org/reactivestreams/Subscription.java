package org.reactivestreams;

public abstract interface Subscription
{
  public abstract void cancel();
  
  public abstract void request(long paramLong);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/reactivestreams/Subscription.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */