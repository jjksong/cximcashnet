package org.greenrobot.eventbus.util;

import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ExceptionToResourceMapping
{
  public final Map<Class<? extends Throwable>, Integer> throwableToMsgIdMap = new HashMap();
  
  public ExceptionToResourceMapping addMapping(Class<? extends Throwable> paramClass, int paramInt)
  {
    this.throwableToMsgIdMap.put(paramClass, Integer.valueOf(paramInt));
    return this;
  }
  
  public Integer mapThrowable(Throwable paramThrowable)
  {
    Object localObject1 = paramThrowable;
    int i = 20;
    Object localObject2;
    do
    {
      localObject2 = mapThrowableFlat((Throwable)localObject1);
      if (localObject2 != null) {
        return (Integer)localObject2;
      }
      localObject2 = ((Throwable)localObject1).getCause();
      i--;
      if ((i <= 0) || (localObject2 == paramThrowable)) {
        break;
      }
      localObject1 = localObject2;
    } while (localObject2 != null);
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("No specific message resource ID found for ");
    ((StringBuilder)localObject1).append(paramThrowable);
    Log.d("EventBus", ((StringBuilder)localObject1).toString());
    return null;
  }
  
  protected Integer mapThrowableFlat(Throwable paramThrowable)
  {
    Class localClass = paramThrowable.getClass();
    paramThrowable = (Integer)this.throwableToMsgIdMap.get(localClass);
    Object localObject2 = paramThrowable;
    if (paramThrowable == null)
    {
      Object localObject1 = null;
      Iterator localIterator = this.throwableToMsgIdMap.entrySet().iterator();
      for (;;)
      {
        localObject2 = paramThrowable;
        if (!localIterator.hasNext()) {
          break;
        }
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        localObject2 = (Class)localEntry.getKey();
        if ((((Class)localObject2).isAssignableFrom(localClass)) && ((localObject1 == null) || (((Class)localObject1).isAssignableFrom((Class)localObject2))))
        {
          paramThrowable = (Integer)localEntry.getValue();
          localObject1 = localObject2;
        }
      }
    }
    return (Integer)localObject2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/util/ExceptionToResourceMapping.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */