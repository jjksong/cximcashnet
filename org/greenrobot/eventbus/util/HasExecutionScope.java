package org.greenrobot.eventbus.util;

public abstract interface HasExecutionScope
{
  public abstract Object getExecutionScope();
  
  public abstract void setExecutionScope(Object paramObject);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/util/HasExecutionScope.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */