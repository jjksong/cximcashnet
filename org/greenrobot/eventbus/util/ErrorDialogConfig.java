package org.greenrobot.eventbus.util;

import android.content.res.Resources;
import android.util.Log;
import org.greenrobot.eventbus.EventBus;

public class ErrorDialogConfig
{
  int defaultDialogIconId;
  final int defaultErrorMsgId;
  Class<?> defaultEventTypeOnDialogClosed;
  final int defaultTitleId;
  EventBus eventBus;
  boolean logExceptions = true;
  final ExceptionToResourceMapping mapping;
  final Resources resources;
  String tagForLoggingExceptions;
  
  public ErrorDialogConfig(Resources paramResources, int paramInt1, int paramInt2)
  {
    this.resources = paramResources;
    this.defaultTitleId = paramInt1;
    this.defaultErrorMsgId = paramInt2;
    this.mapping = new ExceptionToResourceMapping();
  }
  
  public ErrorDialogConfig addMapping(Class<? extends Throwable> paramClass, int paramInt)
  {
    this.mapping.addMapping(paramClass, paramInt);
    return this;
  }
  
  public void disableExceptionLogging()
  {
    this.logExceptions = false;
  }
  
  EventBus getEventBus()
  {
    EventBus localEventBus = this.eventBus;
    if (localEventBus == null) {
      localEventBus = EventBus.getDefault();
    }
    return localEventBus;
  }
  
  public int getMessageIdForThrowable(Throwable paramThrowable)
  {
    Object localObject = this.mapping.mapThrowable(paramThrowable);
    if (localObject != null) {
      return ((Integer)localObject).intValue();
    }
    String str = EventBus.TAG;
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("No specific message ressource ID found for ");
    ((StringBuilder)localObject).append(paramThrowable);
    Log.d(str, ((StringBuilder)localObject).toString());
    return this.defaultErrorMsgId;
  }
  
  public void setDefaultDialogIconId(int paramInt)
  {
    this.defaultDialogIconId = paramInt;
  }
  
  public void setDefaultEventTypeOnDialogClosed(Class<?> paramClass)
  {
    this.defaultEventTypeOnDialogClosed = paramClass;
  }
  
  public void setEventBus(EventBus paramEventBus)
  {
    this.eventBus = paramEventBus;
  }
  
  public void setTagForLoggingExceptions(String paramString)
  {
    this.tagForLoggingExceptions = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/util/ErrorDialogConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */