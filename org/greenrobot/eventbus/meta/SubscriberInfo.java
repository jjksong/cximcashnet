package org.greenrobot.eventbus.meta;

import org.greenrobot.eventbus.SubscriberMethod;

public abstract interface SubscriberInfo
{
  public abstract Class<?> getSubscriberClass();
  
  public abstract SubscriberMethod[] getSubscriberMethods();
  
  public abstract SubscriberInfo getSuperSubscriberInfo();
  
  public abstract boolean shouldCheckSuperclass();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/meta/SubscriberInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */