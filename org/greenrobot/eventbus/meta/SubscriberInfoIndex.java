package org.greenrobot.eventbus.meta;

public abstract interface SubscriberInfoIndex
{
  public abstract SubscriberInfo getSubscriberInfo(Class<?> paramClass);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/meta/SubscriberInfoIndex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */