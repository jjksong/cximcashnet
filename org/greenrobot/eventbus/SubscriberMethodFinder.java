package org.greenrobot.eventbus;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.greenrobot.eventbus.meta.SubscriberInfo;
import org.greenrobot.eventbus.meta.SubscriberInfoIndex;

class SubscriberMethodFinder
{
  private static final int BRIDGE = 64;
  private static final FindState[] FIND_STATE_POOL = new FindState[4];
  private static final Map<Class<?>, List<SubscriberMethod>> METHOD_CACHE = new ConcurrentHashMap();
  private static final int MODIFIERS_IGNORE = 5192;
  private static final int POOL_SIZE = 4;
  private static final int SYNTHETIC = 4096;
  private final boolean ignoreGeneratedIndex;
  private final boolean strictMethodVerification;
  private List<SubscriberInfoIndex> subscriberInfoIndexes;
  
  SubscriberMethodFinder(List<SubscriberInfoIndex> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.subscriberInfoIndexes = paramList;
    this.strictMethodVerification = paramBoolean1;
    this.ignoreGeneratedIndex = paramBoolean2;
  }
  
  static void clearCaches()
  {
    METHOD_CACHE.clear();
  }
  
  private List<SubscriberMethod> findUsingInfo(Class<?> paramClass)
  {
    FindState localFindState = prepareFindState();
    localFindState.initForSubscriber(paramClass);
    while (localFindState.clazz != null)
    {
      localFindState.subscriberInfo = getSubscriberInfo(localFindState);
      if (localFindState.subscriberInfo != null) {
        for (Object localObject : localFindState.subscriberInfo.getSubscriberMethods()) {
          if (localFindState.checkAdd(((SubscriberMethod)localObject).method, ((SubscriberMethod)localObject).eventType)) {
            localFindState.subscriberMethods.add(localObject);
          }
        }
      }
      findUsingReflectionInSingleClass(localFindState);
      localFindState.moveToSuperclass();
    }
    return getMethodsAndRelease(localFindState);
  }
  
  private List<SubscriberMethod> findUsingReflection(Class<?> paramClass)
  {
    FindState localFindState = prepareFindState();
    localFindState.initForSubscriber(paramClass);
    while (localFindState.clazz != null)
    {
      findUsingReflectionInSingleClass(localFindState);
      localFindState.moveToSuperclass();
    }
    return getMethodsAndRelease(localFindState);
  }
  
  private void findUsingReflectionInSingleClass(FindState paramFindState)
  {
    Object localObject1;
    try
    {
      Method[] arrayOfMethod = paramFindState.clazz.getDeclaredMethods();
    }
    catch (Throwable localThrowable)
    {
      localObject1 = paramFindState.clazz.getMethods();
      paramFindState.skipSuperClasses = true;
    }
    int j = localObject1.length;
    for (int i = 0; i < j; i++)
    {
      Method localMethod = localObject1[i];
      int k = localMethod.getModifiers();
      if (((k & 0x1) != 0) && ((k & 0x1448) == 0))
      {
        Object localObject2 = localMethod.getParameterTypes();
        if (localObject2.length == 1)
        {
          Subscribe localSubscribe = (Subscribe)localMethod.getAnnotation(Subscribe.class);
          if (localSubscribe != null)
          {
            localObject2 = localObject2[0];
            if (paramFindState.checkAdd(localMethod, (Class)localObject2))
            {
              ThreadMode localThreadMode = localSubscribe.threadMode();
              paramFindState.subscriberMethods.add(new SubscriberMethod(localMethod, (Class)localObject2, localThreadMode, localSubscribe.priority(), localSubscribe.sticky()));
            }
          }
        }
        else if ((this.strictMethodVerification) && (localMethod.isAnnotationPresent(Subscribe.class)))
        {
          paramFindState = new StringBuilder();
          paramFindState.append(localMethod.getDeclaringClass().getName());
          paramFindState.append(".");
          paramFindState.append(localMethod.getName());
          paramFindState = paramFindState.toString();
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("@Subscribe method ");
          ((StringBuilder)localObject1).append(paramFindState);
          ((StringBuilder)localObject1).append("must have exactly 1 parameter but has ");
          ((StringBuilder)localObject1).append(localObject2.length);
          throw new EventBusException(((StringBuilder)localObject1).toString());
        }
      }
      else if ((this.strictMethodVerification) && (localMethod.isAnnotationPresent(Subscribe.class)))
      {
        paramFindState = new StringBuilder();
        paramFindState.append(localMethod.getDeclaringClass().getName());
        paramFindState.append(".");
        paramFindState.append(localMethod.getName());
        paramFindState = paramFindState.toString();
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(paramFindState);
        ((StringBuilder)localObject1).append(" is a illegal @Subscribe method: must be public, non-static, and non-abstract");
        throw new EventBusException(((StringBuilder)localObject1).toString());
      }
    }
  }
  
  private List<SubscriberMethod> getMethodsAndRelease(FindState paramFindState)
  {
    ArrayList localArrayList = new ArrayList(paramFindState.subscriberMethods);
    paramFindState.recycle();
    FindState[] arrayOfFindState = FIND_STATE_POOL;
    int i = 0;
    for (;;)
    {
      if (i < 4) {}
      try
      {
        if (FIND_STATE_POOL[i] == null)
        {
          FIND_STATE_POOL[i] = paramFindState;
        }
        else
        {
          i++;
          continue;
        }
        return localArrayList;
      }
      finally {}
    }
  }
  
  private SubscriberInfo getSubscriberInfo(FindState paramFindState)
  {
    if ((paramFindState.subscriberInfo != null) && (paramFindState.subscriberInfo.getSuperSubscriberInfo() != null))
    {
      localObject = paramFindState.subscriberInfo.getSuperSubscriberInfo();
      if (paramFindState.clazz == ((SubscriberInfo)localObject).getSubscriberClass()) {
        return (SubscriberInfo)localObject;
      }
    }
    Object localObject = this.subscriberInfoIndexes;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        SubscriberInfo localSubscriberInfo = ((SubscriberInfoIndex)((Iterator)localObject).next()).getSubscriberInfo(paramFindState.clazz);
        if (localSubscriberInfo != null) {
          return localSubscriberInfo;
        }
      }
    }
    return null;
  }
  
  private FindState prepareFindState()
  {
    arrayOfFindState = FIND_STATE_POOL;
    int i = 0;
    for (;;)
    {
      if (i < 4) {}
      try
      {
        FindState localFindState = FIND_STATE_POOL[i];
        if (localFindState != null)
        {
          FIND_STATE_POOL[i] = null;
          return localFindState;
        }
        i++;
      }
      finally {}
    }
    return new FindState();
  }
  
  List<SubscriberMethod> findSubscriberMethods(Class<?> paramClass)
  {
    Object localObject = (List)METHOD_CACHE.get(paramClass);
    if (localObject != null) {
      return (List<SubscriberMethod>)localObject;
    }
    if (this.ignoreGeneratedIndex) {
      localObject = findUsingReflection(paramClass);
    } else {
      localObject = findUsingInfo(paramClass);
    }
    if (!((List)localObject).isEmpty())
    {
      METHOD_CACHE.put(paramClass, localObject);
      return (List<SubscriberMethod>)localObject;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("Subscriber ");
    ((StringBuilder)localObject).append(paramClass);
    ((StringBuilder)localObject).append(" and its super classes have no public methods with the @Subscribe annotation");
    throw new EventBusException(((StringBuilder)localObject).toString());
  }
  
  static class FindState
  {
    final Map<Class, Object> anyMethodByEventType = new HashMap();
    Class<?> clazz;
    final StringBuilder methodKeyBuilder = new StringBuilder(128);
    boolean skipSuperClasses;
    Class<?> subscriberClass;
    final Map<String, Class> subscriberClassByMethodKey = new HashMap();
    SubscriberInfo subscriberInfo;
    final List<SubscriberMethod> subscriberMethods = new ArrayList();
    
    private boolean checkAddWithMethodSignature(Method paramMethod, Class<?> paramClass)
    {
      this.methodKeyBuilder.setLength(0);
      this.methodKeyBuilder.append(paramMethod.getName());
      Object localObject = this.methodKeyBuilder;
      ((StringBuilder)localObject).append('>');
      ((StringBuilder)localObject).append(paramClass.getName());
      paramClass = this.methodKeyBuilder.toString();
      localObject = paramMethod.getDeclaringClass();
      paramMethod = (Class)this.subscriberClassByMethodKey.put(paramClass, localObject);
      if ((paramMethod != null) && (!paramMethod.isAssignableFrom((Class)localObject)))
      {
        this.subscriberClassByMethodKey.put(paramClass, paramMethod);
        return false;
      }
      return true;
    }
    
    boolean checkAdd(Method paramMethod, Class<?> paramClass)
    {
      Object localObject = this.anyMethodByEventType.put(paramClass, paramMethod);
      if (localObject == null) {
        return true;
      }
      if ((localObject instanceof Method)) {
        if (checkAddWithMethodSignature((Method)localObject, paramClass)) {
          this.anyMethodByEventType.put(paramClass, this);
        } else {
          throw new IllegalStateException();
        }
      }
      return checkAddWithMethodSignature(paramMethod, paramClass);
    }
    
    void initForSubscriber(Class<?> paramClass)
    {
      this.clazz = paramClass;
      this.subscriberClass = paramClass;
      this.skipSuperClasses = false;
      this.subscriberInfo = null;
    }
    
    void moveToSuperclass()
    {
      if (this.skipSuperClasses)
      {
        this.clazz = null;
      }
      else
      {
        this.clazz = this.clazz.getSuperclass();
        String str = this.clazz.getName();
        if ((str.startsWith("java.")) || (str.startsWith("javax.")) || (str.startsWith("android."))) {
          this.clazz = null;
        }
      }
    }
    
    void recycle()
    {
      this.subscriberMethods.clear();
      this.anyMethodByEventType.clear();
      this.subscriberClassByMethodKey.clear();
      this.methodKeyBuilder.setLength(0);
      this.subscriberClass = null;
      this.clazz = null;
      this.skipSuperClasses = false;
      this.subscriberInfo = null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/SubscriberMethodFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */