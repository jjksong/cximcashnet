package org.greenrobot.eventbus;

import java.util.concurrent.ExecutorService;

final class BackgroundPoster
  implements Runnable, Poster
{
  private final EventBus eventBus;
  private volatile boolean executorRunning;
  private final PendingPostQueue queue;
  
  BackgroundPoster(EventBus paramEventBus)
  {
    this.eventBus = paramEventBus;
    this.queue = new PendingPostQueue();
  }
  
  public void enqueue(Subscription paramSubscription, Object paramObject)
  {
    paramSubscription = PendingPost.obtainPendingPost(paramSubscription, paramObject);
    try
    {
      this.queue.enqueue(paramSubscription);
      if (!this.executorRunning)
      {
        this.executorRunning = true;
        this.eventBus.getExecutorService().execute(this);
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 26	org/greenrobot/eventbus/BackgroundPoster:queue	Lorg/greenrobot/eventbus/PendingPostQueue;
    //   4: sipush 1000
    //   7: invokevirtual 59	org/greenrobot/eventbus/PendingPostQueue:poll	(I)Lorg/greenrobot/eventbus/PendingPost;
    //   10: astore_2
    //   11: aload_2
    //   12: astore_1
    //   13: aload_2
    //   14: ifnonnull +40 -> 54
    //   17: aload_0
    //   18: monitorenter
    //   19: aload_0
    //   20: getfield 26	org/greenrobot/eventbus/BackgroundPoster:queue	Lorg/greenrobot/eventbus/PendingPostQueue;
    //   23: invokevirtual 62	org/greenrobot/eventbus/PendingPostQueue:poll	()Lorg/greenrobot/eventbus/PendingPost;
    //   26: astore_1
    //   27: aload_1
    //   28: ifnonnull +16 -> 44
    //   31: aload_0
    //   32: iconst_0
    //   33: putfield 40	org/greenrobot/eventbus/BackgroundPoster:executorRunning	Z
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_0
    //   39: iconst_0
    //   40: putfield 40	org/greenrobot/eventbus/BackgroundPoster:executorRunning	Z
    //   43: return
    //   44: aload_0
    //   45: monitorexit
    //   46: goto +8 -> 54
    //   49: astore_1
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    //   54: aload_0
    //   55: getfield 21	org/greenrobot/eventbus/BackgroundPoster:eventBus	Lorg/greenrobot/eventbus/EventBus;
    //   58: aload_1
    //   59: invokevirtual 65	org/greenrobot/eventbus/EventBus:invokeSubscriber	(Lorg/greenrobot/eventbus/PendingPost;)V
    //   62: goto -62 -> 0
    //   65: astore_1
    //   66: goto +62 -> 128
    //   69: astore_3
    //   70: aload_0
    //   71: getfield 21	org/greenrobot/eventbus/BackgroundPoster:eventBus	Lorg/greenrobot/eventbus/EventBus;
    //   74: invokevirtual 69	org/greenrobot/eventbus/EventBus:getLogger	()Lorg/greenrobot/eventbus/Logger;
    //   77: astore_1
    //   78: getstatic 75	java/util/logging/Level:WARNING	Ljava/util/logging/Level;
    //   81: astore 4
    //   83: new 77	java/lang/StringBuilder
    //   86: astore_2
    //   87: aload_2
    //   88: invokespecial 78	java/lang/StringBuilder:<init>	()V
    //   91: aload_2
    //   92: invokestatic 84	java/lang/Thread:currentThread	()Ljava/lang/Thread;
    //   95: invokevirtual 88	java/lang/Thread:getName	()Ljava/lang/String;
    //   98: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload_2
    //   103: ldc 94
    //   105: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload_1
    //   110: aload 4
    //   112: aload_2
    //   113: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   116: aload_3
    //   117: invokeinterface 103 4 0
    //   122: aload_0
    //   123: iconst_0
    //   124: putfield 40	org/greenrobot/eventbus/BackgroundPoster:executorRunning	Z
    //   127: return
    //   128: aload_0
    //   129: iconst_0
    //   130: putfield 40	org/greenrobot/eventbus/BackgroundPoster:executorRunning	Z
    //   133: aload_1
    //   134: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	135	0	this	BackgroundPoster
    //   12	16	1	localObject1	Object
    //   49	10	1	localPendingPost	PendingPost
    //   65	1	1	localObject2	Object
    //   77	57	1	localLogger	Logger
    //   10	103	2	localObject3	Object
    //   69	48	3	localInterruptedException	InterruptedException
    //   81	30	4	localLevel	java.util.logging.Level
    // Exception table:
    //   from	to	target	type
    //   19	27	49	finally
    //   31	38	49	finally
    //   44	46	49	finally
    //   50	52	49	finally
    //   0	11	65	finally
    //   17	19	65	finally
    //   52	54	65	finally
    //   54	62	65	finally
    //   70	122	65	finally
    //   0	11	69	java/lang/InterruptedException
    //   17	19	69	java/lang/InterruptedException
    //   52	54	69	java/lang/InterruptedException
    //   54	62	69	java/lang/InterruptedException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/BackgroundPoster.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */