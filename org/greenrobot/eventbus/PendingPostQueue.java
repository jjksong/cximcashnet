package org.greenrobot.eventbus;

final class PendingPostQueue
{
  private PendingPost head;
  private PendingPost tail;
  
  void enqueue(PendingPost paramPendingPost)
  {
    if (paramPendingPost != null) {
      try
      {
        if (this.tail != null)
        {
          this.tail.next = paramPendingPost;
          this.tail = paramPendingPost;
        }
        else
        {
          if (this.head != null) {
            break label53;
          }
          this.tail = paramPendingPost;
          this.head = paramPendingPost;
        }
        notifyAll();
        return;
        label53:
        paramPendingPost = new java/lang/IllegalStateException;
        paramPendingPost.<init>("Head present, but no tail");
        throw paramPendingPost;
      }
      finally
      {
        break label81;
      }
    }
    paramPendingPost = new java/lang/NullPointerException;
    paramPendingPost.<init>("null cannot be enqueued");
    throw paramPendingPost;
    label81:
    throw paramPendingPost;
  }
  
  PendingPost poll()
  {
    try
    {
      PendingPost localPendingPost = this.head;
      if (this.head != null)
      {
        this.head = this.head.next;
        if (this.head == null) {
          this.tail = null;
        }
      }
      return localPendingPost;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  PendingPost poll(int paramInt)
    throws InterruptedException
  {
    try
    {
      if (this.head == null) {
        wait(paramInt);
      }
      PendingPost localPendingPost = poll();
      return localPendingPost;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/PendingPostQueue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */