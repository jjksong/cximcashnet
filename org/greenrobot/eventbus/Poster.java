package org.greenrobot.eventbus;

abstract interface Poster
{
  public abstract void enqueue(Subscription paramSubscription, Object paramObject);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/Poster.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */