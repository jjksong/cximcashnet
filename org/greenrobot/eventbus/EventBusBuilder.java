package org.greenrobot.eventbus;

import android.os.Looper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.greenrobot.eventbus.meta.SubscriberInfoIndex;

public class EventBusBuilder
{
  private static final ExecutorService DEFAULT_EXECUTOR_SERVICE = ;
  boolean eventInheritance = true;
  ExecutorService executorService = DEFAULT_EXECUTOR_SERVICE;
  boolean ignoreGeneratedIndex;
  boolean logNoSubscriberMessages = true;
  boolean logSubscriberExceptions = true;
  Logger logger;
  MainThreadSupport mainThreadSupport;
  boolean sendNoSubscriberEvent = true;
  boolean sendSubscriberExceptionEvent = true;
  List<Class<?>> skipMethodVerificationForClasses;
  boolean strictMethodVerification;
  List<SubscriberInfoIndex> subscriberInfoIndexes;
  boolean throwSubscriberException;
  
  public EventBusBuilder addIndex(SubscriberInfoIndex paramSubscriberInfoIndex)
  {
    if (this.subscriberInfoIndexes == null) {
      this.subscriberInfoIndexes = new ArrayList();
    }
    this.subscriberInfoIndexes.add(paramSubscriberInfoIndex);
    return this;
  }
  
  public EventBus build()
  {
    return new EventBus(this);
  }
  
  public EventBusBuilder eventInheritance(boolean paramBoolean)
  {
    this.eventInheritance = paramBoolean;
    return this;
  }
  
  public EventBusBuilder executorService(ExecutorService paramExecutorService)
  {
    this.executorService = paramExecutorService;
    return this;
  }
  
  Object getAndroidMainLooperOrNull()
  {
    try
    {
      Looper localLooper = Looper.getMainLooper();
      return localLooper;
    }
    catch (RuntimeException localRuntimeException) {}
    return null;
  }
  
  Logger getLogger()
  {
    Object localObject = this.logger;
    if (localObject != null) {
      return (Logger)localObject;
    }
    if ((Logger.AndroidLogger.isAndroidLogAvailable()) && (getAndroidMainLooperOrNull() != null)) {
      localObject = new Logger.AndroidLogger("EventBus");
    } else {
      localObject = new Logger.SystemOutLogger();
    }
    return (Logger)localObject;
  }
  
  MainThreadSupport getMainThreadSupport()
  {
    Object localObject1 = this.mainThreadSupport;
    if (localObject1 != null) {
      return (MainThreadSupport)localObject1;
    }
    boolean bool = Logger.AndroidLogger.isAndroidLogAvailable();
    localObject1 = null;
    if (bool)
    {
      Object localObject2 = getAndroidMainLooperOrNull();
      if (localObject2 != null) {
        localObject1 = new MainThreadSupport.AndroidHandlerMainThreadSupport((Looper)localObject2);
      }
      return (MainThreadSupport)localObject1;
    }
    return null;
  }
  
  public EventBusBuilder ignoreGeneratedIndex(boolean paramBoolean)
  {
    this.ignoreGeneratedIndex = paramBoolean;
    return this;
  }
  
  public EventBus installDefaultEventBus()
  {
    try
    {
      if (EventBus.defaultInstance == null)
      {
        EventBus.defaultInstance = build();
        localObject1 = EventBus.defaultInstance;
        return (EventBus)localObject1;
      }
      Object localObject1 = new org/greenrobot/eventbus/EventBusException;
      ((EventBusException)localObject1).<init>("Default instance already exists. It may be only set once before it's used the first time to ensure consistent behavior.");
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public EventBusBuilder logNoSubscriberMessages(boolean paramBoolean)
  {
    this.logNoSubscriberMessages = paramBoolean;
    return this;
  }
  
  public EventBusBuilder logSubscriberExceptions(boolean paramBoolean)
  {
    this.logSubscriberExceptions = paramBoolean;
    return this;
  }
  
  public EventBusBuilder logger(Logger paramLogger)
  {
    this.logger = paramLogger;
    return this;
  }
  
  public EventBusBuilder sendNoSubscriberEvent(boolean paramBoolean)
  {
    this.sendNoSubscriberEvent = paramBoolean;
    return this;
  }
  
  public EventBusBuilder sendSubscriberExceptionEvent(boolean paramBoolean)
  {
    this.sendSubscriberExceptionEvent = paramBoolean;
    return this;
  }
  
  public EventBusBuilder skipMethodVerificationFor(Class<?> paramClass)
  {
    if (this.skipMethodVerificationForClasses == null) {
      this.skipMethodVerificationForClasses = new ArrayList();
    }
    this.skipMethodVerificationForClasses.add(paramClass);
    return this;
  }
  
  public EventBusBuilder strictMethodVerification(boolean paramBoolean)
  {
    this.strictMethodVerification = paramBoolean;
    return this;
  }
  
  public EventBusBuilder throwSubscriberException(boolean paramBoolean)
  {
    this.throwSubscriberException = paramBoolean;
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/EventBusBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */