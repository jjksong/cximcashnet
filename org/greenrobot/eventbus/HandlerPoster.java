package org.greenrobot.eventbus;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

public class HandlerPoster
  extends Handler
  implements Poster
{
  private final EventBus eventBus;
  private boolean handlerActive;
  private final int maxMillisInsideHandleMessage;
  private final PendingPostQueue queue;
  
  protected HandlerPoster(EventBus paramEventBus, Looper paramLooper, int paramInt)
  {
    super(paramLooper);
    this.eventBus = paramEventBus;
    this.maxMillisInsideHandleMessage = paramInt;
    this.queue = new PendingPostQueue();
  }
  
  public void enqueue(Subscription paramSubscription, Object paramObject)
  {
    paramSubscription = PendingPost.obtainPendingPost(paramSubscription, paramObject);
    try
    {
      this.queue.enqueue(paramSubscription);
      if (!this.handlerActive)
      {
        this.handlerActive = true;
        if (!sendMessage(obtainMessage()))
        {
          paramSubscription = new org/greenrobot/eventbus/EventBusException;
          paramSubscription.<init>("Could not send handler message");
          throw paramSubscription;
        }
      }
      return;
    }
    finally {}
  }
  
  public void handleMessage(Message paramMessage)
  {
    try
    {
      long l = SystemClock.uptimeMillis();
      do
      {
        PendingPost localPendingPost = this.queue.poll();
        paramMessage = localPendingPost;
        if (localPendingPost == null) {
          try
          {
            paramMessage = this.queue.poll();
            if (paramMessage == null)
            {
              this.handlerActive = false;
              return;
            }
          }
          finally {}
        }
        this.eventBus.invokeSubscriber(paramMessage);
      } while (SystemClock.uptimeMillis() - l < this.maxMillisInsideHandleMessage);
      boolean bool = sendMessage(obtainMessage());
      if (bool)
      {
        this.handlerActive = true;
        return;
      }
      paramMessage = new org/greenrobot/eventbus/EventBusException;
      paramMessage.<init>("Could not send handler message");
      throw paramMessage;
    }
    finally
    {
      this.handlerActive = false;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/HandlerPoster.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */