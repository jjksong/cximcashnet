package org.greenrobot.eventbus;

import android.os.Looper;

public abstract interface MainThreadSupport
{
  public abstract Poster createPoster(EventBus paramEventBus);
  
  public abstract boolean isMainThread();
  
  public static class AndroidHandlerMainThreadSupport
    implements MainThreadSupport
  {
    private final Looper looper;
    
    public AndroidHandlerMainThreadSupport(Looper paramLooper)
    {
      this.looper = paramLooper;
    }
    
    public Poster createPoster(EventBus paramEventBus)
    {
      return new HandlerPoster(paramEventBus, this.looper, 10);
    }
    
    public boolean isMainThread()
    {
      boolean bool;
      if (this.looper == Looper.myLooper()) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/greenrobot/eventbus/MainThreadSupport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */