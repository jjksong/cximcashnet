package org.xutils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract interface ViewInjector
{
  public abstract View inject(Object paramObject, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup);
  
  public abstract void inject(Activity paramActivity);
  
  public abstract void inject(View paramView);
  
  public abstract void inject(Object paramObject, View paramView);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ViewInjector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */