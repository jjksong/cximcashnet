package org.xutils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.xutils.common.util.KeyValue;
import org.xutils.db.Selector;
import org.xutils.db.sqlite.SqlInfo;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.db.table.DbModel;
import org.xutils.db.table.TableEntity;
import org.xutils.ex.DbException;

public abstract interface DbManager
  extends Closeable
{
  public abstract void addColumn(Class<?> paramClass, String paramString)
    throws DbException;
  
  public abstract void close()
    throws IOException;
  
  public abstract int delete(Class<?> paramClass, WhereBuilder paramWhereBuilder)
    throws DbException;
  
  public abstract void delete(Class<?> paramClass)
    throws DbException;
  
  public abstract void delete(Object paramObject)
    throws DbException;
  
  public abstract void deleteById(Class<?> paramClass, Object paramObject)
    throws DbException;
  
  public abstract void dropDb()
    throws DbException;
  
  public abstract void dropTable(Class<?> paramClass)
    throws DbException;
  
  public abstract void execNonQuery(String paramString)
    throws DbException;
  
  public abstract void execNonQuery(SqlInfo paramSqlInfo)
    throws DbException;
  
  public abstract Cursor execQuery(String paramString)
    throws DbException;
  
  public abstract Cursor execQuery(SqlInfo paramSqlInfo)
    throws DbException;
  
  public abstract int executeUpdateDelete(String paramString)
    throws DbException;
  
  public abstract int executeUpdateDelete(SqlInfo paramSqlInfo)
    throws DbException;
  
  public abstract <T> List<T> findAll(Class<T> paramClass)
    throws DbException;
  
  public abstract <T> T findById(Class<T> paramClass, Object paramObject)
    throws DbException;
  
  public abstract List<DbModel> findDbModelAll(SqlInfo paramSqlInfo)
    throws DbException;
  
  public abstract DbModel findDbModelFirst(SqlInfo paramSqlInfo)
    throws DbException;
  
  public abstract <T> T findFirst(Class<T> paramClass)
    throws DbException;
  
  public abstract DaoConfig getDaoConfig();
  
  public abstract SQLiteDatabase getDatabase();
  
  public abstract <T> TableEntity<T> getTable(Class<T> paramClass)
    throws DbException;
  
  public abstract void replace(Object paramObject)
    throws DbException;
  
  public abstract void save(Object paramObject)
    throws DbException;
  
  public abstract boolean saveBindingId(Object paramObject)
    throws DbException;
  
  public abstract void saveOrUpdate(Object paramObject)
    throws DbException;
  
  public abstract <T> Selector<T> selector(Class<T> paramClass)
    throws DbException;
  
  public abstract int update(Class<?> paramClass, WhereBuilder paramWhereBuilder, KeyValue... paramVarArgs)
    throws DbException;
  
  public abstract void update(Object paramObject, String... paramVarArgs)
    throws DbException;
  
  public static class DaoConfig
  {
    private boolean allowTransaction = true;
    private File dbDir;
    private String dbName = "xUtils.db";
    private DbManager.DbOpenListener dbOpenListener;
    private DbManager.DbUpgradeListener dbUpgradeListener;
    private int dbVersion = 1;
    private DbManager.TableCreateListener tableCreateListener;
    
    public boolean equals(Object paramObject)
    {
      boolean bool = true;
      if (this == paramObject) {
        return true;
      }
      if ((paramObject != null) && (getClass() == paramObject.getClass()))
      {
        paramObject = (DaoConfig)paramObject;
        if (!this.dbName.equals(((DaoConfig)paramObject).dbName)) {
          return false;
        }
        File localFile = this.dbDir;
        if (localFile == null)
        {
          if (((DaoConfig)paramObject).dbDir != null) {
            bool = false;
          }
        }
        else {
          bool = localFile.equals(((DaoConfig)paramObject).dbDir);
        }
        return bool;
      }
      return false;
    }
    
    public File getDbDir()
    {
      return this.dbDir;
    }
    
    public String getDbName()
    {
      return this.dbName;
    }
    
    public DbManager.DbOpenListener getDbOpenListener()
    {
      return this.dbOpenListener;
    }
    
    public DbManager.DbUpgradeListener getDbUpgradeListener()
    {
      return this.dbUpgradeListener;
    }
    
    public int getDbVersion()
    {
      return this.dbVersion;
    }
    
    public DbManager.TableCreateListener getTableCreateListener()
    {
      return this.tableCreateListener;
    }
    
    public int hashCode()
    {
      int j = this.dbName.hashCode();
      File localFile = this.dbDir;
      int i;
      if (localFile != null) {
        i = localFile.hashCode();
      } else {
        i = 0;
      }
      return j * 31 + i;
    }
    
    public boolean isAllowTransaction()
    {
      return this.allowTransaction;
    }
    
    public DaoConfig setAllowTransaction(boolean paramBoolean)
    {
      this.allowTransaction = paramBoolean;
      return this;
    }
    
    public DaoConfig setDbDir(File paramFile)
    {
      this.dbDir = paramFile;
      return this;
    }
    
    public DaoConfig setDbName(String paramString)
    {
      if (!TextUtils.isEmpty(paramString)) {
        this.dbName = paramString;
      }
      return this;
    }
    
    public DaoConfig setDbOpenListener(DbManager.DbOpenListener paramDbOpenListener)
    {
      this.dbOpenListener = paramDbOpenListener;
      return this;
    }
    
    public DaoConfig setDbUpgradeListener(DbManager.DbUpgradeListener paramDbUpgradeListener)
    {
      this.dbUpgradeListener = paramDbUpgradeListener;
      return this;
    }
    
    public DaoConfig setDbVersion(int paramInt)
    {
      this.dbVersion = paramInt;
      return this;
    }
    
    public DaoConfig setTableCreateListener(DbManager.TableCreateListener paramTableCreateListener)
    {
      this.tableCreateListener = paramTableCreateListener;
      return this;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(String.valueOf(this.dbDir));
      localStringBuilder.append("/");
      localStringBuilder.append(this.dbName);
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface DbOpenListener
  {
    public abstract void onDbOpened(DbManager paramDbManager);
  }
  
  public static abstract interface DbUpgradeListener
  {
    public abstract void onUpgrade(DbManager paramDbManager, int paramInt1, int paramInt2);
  }
  
  public static abstract interface TableCreateListener
  {
    public abstract void onTableCreated(DbManager paramDbManager, TableEntity<?> paramTableEntity);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/DbManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */