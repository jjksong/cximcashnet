package org.xutils.common.util;

import android.database.Cursor;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class IOUtil
{
  public static void closeQuietly(Cursor paramCursor)
  {
    if (paramCursor != null) {
      try
      {
        paramCursor.close();
      }
      catch (Throwable paramCursor)
      {
        LogUtil.d(paramCursor.getMessage(), paramCursor);
      }
    }
  }
  
  public static void closeQuietly(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
      }
      catch (Throwable paramCloseable)
      {
        LogUtil.d(paramCloseable.getMessage(), paramCloseable);
      }
    }
  }
  
  public static void copy(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    Object localObject = paramInputStream;
    if (!(paramInputStream instanceof BufferedInputStream)) {
      localObject = new BufferedInputStream(paramInputStream);
    }
    paramInputStream = paramOutputStream;
    if (!(paramOutputStream instanceof BufferedOutputStream)) {
      paramInputStream = new BufferedOutputStream(paramOutputStream);
    }
    paramOutputStream = new byte['Ѐ'];
    for (;;)
    {
      int i = ((InputStream)localObject).read(paramOutputStream);
      if (i == -1) {
        break;
      }
      paramInputStream.write(paramOutputStream, 0, i);
    }
    paramInputStream.flush();
  }
  
  public static boolean deleteFileOrDir(File paramFile)
  {
    if ((paramFile != null) && (paramFile.exists()))
    {
      if (paramFile.isFile()) {
        return paramFile.delete();
      }
      File[] arrayOfFile = paramFile.listFiles();
      if (arrayOfFile != null)
      {
        int j = arrayOfFile.length;
        for (int i = 0; i < j; i++) {
          deleteFileOrDir(arrayOfFile[i]);
        }
      }
      return paramFile.delete();
    }
    return true;
  }
  
  /* Error */
  public static byte[] readBytes(InputStream paramInputStream)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_0
    //   3: instanceof 38
    //   6: ifne +12 -> 18
    //   9: new 38	java/io/BufferedInputStream
    //   12: dup
    //   13: aload_0
    //   14: invokespecial 41	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: astore_2
    //   18: new 86	java/io/ByteArrayOutputStream
    //   21: astore_0
    //   22: aload_0
    //   23: invokespecial 87	java/io/ByteArrayOutputStream:<init>	()V
    //   26: sipush 1024
    //   29: newarray <illegal type>
    //   31: astore_3
    //   32: aload_2
    //   33: aload_3
    //   34: invokevirtual 52	java/io/InputStream:read	([B)I
    //   37: istore_1
    //   38: iload_1
    //   39: iconst_m1
    //   40: if_icmpeq +13 -> 53
    //   43: aload_0
    //   44: aload_3
    //   45: iconst_0
    //   46: iload_1
    //   47: invokevirtual 88	java/io/ByteArrayOutputStream:write	([BII)V
    //   50: goto -18 -> 32
    //   53: aload_0
    //   54: invokevirtual 92	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   57: astore_2
    //   58: aload_0
    //   59: invokestatic 94	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   62: aload_2
    //   63: areturn
    //   64: astore_2
    //   65: goto +6 -> 71
    //   68: astore_2
    //   69: aconst_null
    //   70: astore_0
    //   71: aload_0
    //   72: invokestatic 94	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   75: aload_2
    //   76: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	77	0	paramInputStream	InputStream
    //   37	10	1	i	int
    //   1	62	2	localObject1	Object
    //   64	1	2	localObject2	Object
    //   68	8	2	localObject3	Object
    //   31	14	3	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   26	32	64	finally
    //   32	38	64	finally
    //   43	50	64	finally
    //   53	58	64	finally
    //   18	26	68	finally
  }
  
  public static byte[] readBytes(InputStream paramInputStream, long paramLong, int paramInt)
    throws IOException
  {
    if (paramLong > 0L) {
      while (paramLong > 0L)
      {
        long l = paramInputStream.skip(paramLong);
        if (l <= 0L) {
          break;
        }
        paramLong -= l;
      }
    }
    byte[] arrayOfByte = new byte[paramInt];
    for (int i = 0; i < paramInt; i++) {
      arrayOfByte[i] = ((byte)paramInputStream.read());
    }
    return arrayOfByte;
  }
  
  public static String readStr(InputStream paramInputStream)
    throws IOException
  {
    return readStr(paramInputStream, "UTF-8");
  }
  
  public static String readStr(InputStream paramInputStream, String paramString)
    throws IOException
  {
    Object localObject = paramString;
    if (TextUtils.isEmpty(paramString)) {
      localObject = "UTF-8";
    }
    paramString = paramInputStream;
    if (!(paramInputStream instanceof BufferedInputStream)) {
      paramString = new BufferedInputStream(paramInputStream);
    }
    localObject = new InputStreamReader(paramString, (String)localObject);
    paramString = new StringBuilder();
    paramInputStream = new char['Ѐ'];
    for (;;)
    {
      int i = ((Reader)localObject).read(paramInputStream);
      if (i < 0) {
        break;
      }
      paramString.append(paramInputStream, 0, i);
    }
    return paramString.toString();
  }
  
  public static void writeStr(OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    writeStr(paramOutputStream, paramString, "UTF-8");
  }
  
  public static void writeStr(OutputStream paramOutputStream, String paramString1, String paramString2)
    throws IOException
  {
    String str = paramString2;
    if (TextUtils.isEmpty(paramString2)) {
      str = "UTF-8";
    }
    paramOutputStream = new OutputStreamWriter(paramOutputStream, str);
    paramOutputStream.write(paramString1);
    paramOutputStream.flush();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/IOUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */