package org.xutils.common.util;

import android.text.TextUtils;
import android.util.Log;
import org.xutils.x;

public class LogUtil
{
  public static String customTagPrefix = "x_log";
  
  public static void d(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.d(generateTag(), paramString);
  }
  
  public static void d(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.d(generateTag(), paramString, paramThrowable);
  }
  
  public static void e(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.e(generateTag(), paramString);
  }
  
  public static void e(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.e(generateTag(), paramString, paramThrowable);
  }
  
  private static String generateTag()
  {
    Object localObject1 = new Throwable().getStackTrace()[2];
    Object localObject2 = ((StackTraceElement)localObject1).getClassName();
    localObject1 = String.format("%s.%s(L:%d)", new Object[] { ((String)localObject2).substring(((String)localObject2).lastIndexOf(".") + 1), ((StackTraceElement)localObject1).getMethodName(), Integer.valueOf(((StackTraceElement)localObject1).getLineNumber()) });
    if (!TextUtils.isEmpty(customTagPrefix))
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(customTagPrefix);
      ((StringBuilder)localObject2).append(":");
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    return (String)localObject1;
  }
  
  public static void i(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.i(generateTag(), paramString);
  }
  
  public static void i(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.i(generateTag(), paramString, paramThrowable);
  }
  
  public static void v(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.v(generateTag(), paramString);
  }
  
  public static void v(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.v(generateTag(), paramString, paramThrowable);
  }
  
  public static void w(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.w(generateTag(), paramString);
  }
  
  public static void w(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.w(generateTag(), paramString, paramThrowable);
  }
  
  public static void w(Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.w(generateTag(), paramThrowable);
  }
  
  public static void wtf(String paramString)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.wtf(generateTag(), paramString);
  }
  
  public static void wtf(String paramString, Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.wtf(generateTag(), paramString, paramThrowable);
  }
  
  public static void wtf(Throwable paramThrowable)
  {
    if (!x.isDebug()) {
      return;
    }
    Log.wtf(generateTag(), paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/LogUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */