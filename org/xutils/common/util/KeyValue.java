package org.xutils.common.util;

public class KeyValue
{
  public final String key;
  public final Object value;
  
  public KeyValue(String paramString, Object paramObject)
  {
    this.key = paramString;
    this.value = paramObject;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      KeyValue localKeyValue = (KeyValue)paramObject;
      paramObject = this.key;
      if (paramObject == null)
      {
        if (localKeyValue.key != null) {
          bool = false;
        }
      }
      else {
        bool = ((String)paramObject).equals(localKeyValue.key);
      }
      return bool;
    }
    return false;
  }
  
  public String getValueStr()
  {
    Object localObject = this.value;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = localObject.toString();
    }
    return (String)localObject;
  }
  
  public int hashCode()
  {
    String str = this.key;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    return i;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("KeyValue{key='");
    localStringBuilder.append(this.key);
    localStringBuilder.append('\'');
    localStringBuilder.append(", value=");
    localStringBuilder.append(this.value);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/KeyValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */