package org.xutils.common.util;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public class ParameterizedTypeUtil
{
  public static Type getParameterizedType(Type paramType, Class<?> paramClass, int paramInt)
  {
    boolean bool = paramType instanceof ParameterizedType;
    Type[] arrayOfType = null;
    Object localObject1;
    Class localClass;
    if (bool)
    {
      localObject1 = (ParameterizedType)paramType;
      localClass = (Class)((ParameterizedType)localObject1).getRawType();
      arrayOfType = ((ParameterizedType)localObject1).getActualTypeArguments();
      localObject1 = localClass.getTypeParameters();
    }
    else
    {
      localClass = (Class)paramType;
      localObject1 = null;
    }
    if (paramClass == localClass)
    {
      if (arrayOfType != null) {
        return arrayOfType[paramInt];
      }
      return Object.class;
    }
    Object localObject2 = localClass.getGenericInterfaces();
    if (localObject2 != null)
    {
      for (int i = 0; i < localObject2.length; i++)
      {
        Type localType = localObject2[i];
        if (((localType instanceof ParameterizedType)) && (paramClass.isAssignableFrom((Class)((ParameterizedType)localType).getRawType()))) {}
        try
        {
          localType = getTrueType(getParameterizedType(localType, paramClass, paramInt), (TypeVariable[])localObject1, arrayOfType);
          return localType;
        }
        catch (Throwable localThrowable)
        {
          for (;;) {}
        }
      }
    }
    else
    {
      localObject2 = localClass.getSuperclass();
      if ((localObject2 != null) && (paramClass.isAssignableFrom((Class)localObject2))) {
        return getTrueType(getParameterizedType(localClass.getGenericSuperclass(), paramClass, paramInt), (TypeVariable[])localObject1, arrayOfType);
      }
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("FindGenericType:");
      ((StringBuilder)localObject1).append(paramType);
      ((StringBuilder)localObject1).append(", declaredClass: ");
      ((StringBuilder)localObject1).append(paramClass);
      ((StringBuilder)localObject1).append(", index: ");
      ((StringBuilder)localObject1).append(paramInt);
      throw new IllegalArgumentException(((StringBuilder)localObject1).toString());
    }
  }
  
  private static Type getTrueType(Type paramType, TypeVariable<?>[] paramArrayOfTypeVariable, Type[] paramArrayOfType)
  {
    boolean bool = paramType instanceof TypeVariable;
    int i = 0;
    if (bool)
    {
      TypeVariable localTypeVariable = (TypeVariable)paramType;
      paramType = localTypeVariable.getName();
      if (paramArrayOfType != null) {
        while (i < paramArrayOfTypeVariable.length)
        {
          if (paramType.equals(paramArrayOfTypeVariable[i].getName())) {
            return paramArrayOfType[i];
          }
          i++;
        }
      }
      return localTypeVariable;
    }
    if ((paramType instanceof GenericArrayType))
    {
      paramArrayOfTypeVariable = ((GenericArrayType)paramType).getGenericComponentType();
      if ((paramArrayOfTypeVariable instanceof Class)) {
        return Array.newInstance((Class)paramArrayOfTypeVariable, 0).getClass();
      }
    }
    return paramType;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/ParameterizedTypeUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */