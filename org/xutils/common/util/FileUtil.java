package org.xutils.common.util;

import android.app.Application;
import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import org.xutils.x;

public class FileUtil
{
  /* Error */
  public static boolean copy(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: new 15	java/io/File
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 18	java/io/File:<init>	(Ljava/lang/String;)V
    //   8: astore_0
    //   9: aload_0
    //   10: invokevirtual 22	java/io/File:exists	()Z
    //   13: istore 4
    //   15: iconst_0
    //   16: istore_2
    //   17: iconst_0
    //   18: istore_3
    //   19: iload 4
    //   21: ifne +5 -> 26
    //   24: iconst_0
    //   25: ireturn
    //   26: new 15	java/io/File
    //   29: dup
    //   30: aload_1
    //   31: invokespecial 18	java/io/File:<init>	(Ljava/lang/String;)V
    //   34: astore 7
    //   36: aload 7
    //   38: invokestatic 28	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   41: pop
    //   42: aload 7
    //   44: invokevirtual 32	java/io/File:getParentFile	()Ljava/io/File;
    //   47: astore_1
    //   48: aload_1
    //   49: invokevirtual 22	java/io/File:exists	()Z
    //   52: ifne +10 -> 62
    //   55: aload_1
    //   56: invokevirtual 35	java/io/File:mkdirs	()Z
    //   59: ifeq +105 -> 164
    //   62: aconst_null
    //   63: astore 6
    //   65: aconst_null
    //   66: astore_1
    //   67: new 37	java/io/FileInputStream
    //   70: astore 5
    //   72: aload 5
    //   74: aload_0
    //   75: invokespecial 40	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   78: new 42	java/io/FileOutputStream
    //   81: astore_0
    //   82: aload_0
    //   83: aload 7
    //   85: invokespecial 43	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   88: aload 5
    //   90: aload_0
    //   91: invokestatic 46	org/xutils/common/util/IOUtil:copy	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   94: iconst_1
    //   95: istore_2
    //   96: aload 5
    //   98: invokestatic 50	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   101: goto +59 -> 160
    //   104: astore_1
    //   105: goto +10 -> 115
    //   108: astore_1
    //   109: goto +12 -> 121
    //   112: astore_1
    //   113: aconst_null
    //   114: astore_0
    //   115: goto +59 -> 174
    //   118: astore_1
    //   119: aconst_null
    //   120: astore_0
    //   121: aload_1
    //   122: astore 6
    //   124: aload 5
    //   126: astore_1
    //   127: goto +17 -> 144
    //   130: astore_1
    //   131: aconst_null
    //   132: astore_0
    //   133: aload 6
    //   135: astore 5
    //   137: goto +37 -> 174
    //   140: astore 6
    //   142: aconst_null
    //   143: astore_0
    //   144: aload 6
    //   146: invokevirtual 54	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   149: aload 6
    //   151: invokestatic 60	org/xutils/common/util/LogUtil:d	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   154: aload_1
    //   155: invokestatic 50	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   158: iload_3
    //   159: istore_2
    //   160: aload_0
    //   161: invokestatic 50	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   164: iload_2
    //   165: ireturn
    //   166: astore 6
    //   168: aload_1
    //   169: astore 5
    //   171: aload 6
    //   173: astore_1
    //   174: aload 5
    //   176: invokestatic 50	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   179: aload_0
    //   180: invokestatic 50	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   183: aload_1
    //   184: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	185	0	paramString1	String
    //   0	185	1	paramString2	String
    //   16	149	2	bool1	boolean
    //   18	141	3	bool2	boolean
    //   13	7	4	bool3	boolean
    //   70	105	5	localObject1	Object
    //   63	71	6	str	String
    //   140	10	6	localThrowable	Throwable
    //   166	6	6	localObject2	Object
    //   34	50	7	localFile	File
    // Exception table:
    //   from	to	target	type
    //   88	94	104	finally
    //   88	94	108	java/lang/Throwable
    //   78	88	112	finally
    //   78	88	118	java/lang/Throwable
    //   67	78	130	finally
    //   67	78	140	java/lang/Throwable
    //   144	154	166	finally
  }
  
  public static Boolean existsSdcard()
  {
    return Boolean.valueOf(Environment.getExternalStorageState().equals("mounted"));
  }
  
  public static File getCacheDir(String paramString)
  {
    if (existsSdcard().booleanValue())
    {
      File localFile = x.app().getExternalCacheDir();
      if (localFile == null)
      {
        localFile = Environment.getExternalStorageDirectory();
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Android/data/");
        localStringBuilder.append(x.app().getPackageName());
        localStringBuilder.append("/cache/");
        localStringBuilder.append(paramString);
        paramString = new File(localFile, localStringBuilder.toString());
      }
      else
      {
        paramString = new File(localFile, paramString);
      }
    }
    else
    {
      paramString = new File(x.app().getCacheDir(), paramString);
    }
    if ((!paramString.exists()) && (!paramString.mkdirs())) {
      return null;
    }
    return paramString;
  }
  
  public static long getDiskAvailableSize()
  {
    if (!existsSdcard().booleanValue()) {
      return 0L;
    }
    StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
    long l = localStatFs.getBlockSize();
    return localStatFs.getAvailableBlocks() * l;
  }
  
  public static long getFileOrDirSize(File paramFile)
  {
    boolean bool = paramFile.exists();
    long l1 = 0L;
    if (!bool) {
      return 0L;
    }
    if (!paramFile.isDirectory()) {
      return paramFile.length();
    }
    paramFile = paramFile.listFiles();
    long l2 = l1;
    if (paramFile != null)
    {
      int j = paramFile.length;
      for (int i = 0;; i++)
      {
        l2 = l1;
        if (i >= j) {
          break;
        }
        l1 += getFileOrDirSize(paramFile[i]);
      }
    }
    return l2;
  }
  
  public static boolean isDiskAvailable()
  {
    boolean bool;
    if (getDiskAvailableSize() > 10485760L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/FileUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */