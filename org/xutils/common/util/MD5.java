package org.xutils.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5
{
  private static final char[] hexDigits = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  
  /* Error */
  public static String md5(java.io.File paramFile)
    throws java.io.IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore_2
    //   5: aconst_null
    //   6: astore_1
    //   7: aconst_null
    //   8: astore 5
    //   10: ldc 38
    //   12: invokestatic 44	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   15: astore 6
    //   17: new 46	java/io/FileInputStream
    //   20: astore_3
    //   21: aload_3
    //   22: aload_0
    //   23: invokespecial 49	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   26: aload 5
    //   28: astore_1
    //   29: aload 4
    //   31: astore_2
    //   32: aload_3
    //   33: invokevirtual 53	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   36: astore 4
    //   38: aload 4
    //   40: astore_1
    //   41: aload 4
    //   43: astore_2
    //   44: aload 6
    //   46: aload 4
    //   48: getstatic 59	java/nio/channels/FileChannel$MapMode:READ_ONLY	Ljava/nio/channels/FileChannel$MapMode;
    //   51: lconst_0
    //   52: aload_0
    //   53: invokevirtual 65	java/io/File:length	()J
    //   56: invokevirtual 71	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   59: invokevirtual 75	java/security/MessageDigest:update	(Ljava/nio/ByteBuffer;)V
    //   62: aload 4
    //   64: astore_1
    //   65: aload 4
    //   67: astore_2
    //   68: aload 6
    //   70: invokevirtual 79	java/security/MessageDigest:digest	()[B
    //   73: astore_0
    //   74: aload_3
    //   75: invokestatic 85	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   78: aload 4
    //   80: invokestatic 85	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   83: aload_0
    //   84: invokestatic 89	org/xutils/common/util/MD5:toHexString	([B)Ljava/lang/String;
    //   87: areturn
    //   88: astore_2
    //   89: aload_3
    //   90: astore_0
    //   91: goto +41 -> 132
    //   94: astore 4
    //   96: aload_2
    //   97: astore_1
    //   98: aload_3
    //   99: astore_0
    //   100: goto +19 -> 119
    //   103: astore_2
    //   104: aconst_null
    //   105: astore_3
    //   106: aload_1
    //   107: astore_0
    //   108: aload_3
    //   109: astore_1
    //   110: goto +22 -> 132
    //   113: astore 4
    //   115: aconst_null
    //   116: astore_1
    //   117: aload_2
    //   118: astore_0
    //   119: new 91	java/lang/RuntimeException
    //   122: astore_2
    //   123: aload_2
    //   124: aload 4
    //   126: invokespecial 94	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   129: aload_2
    //   130: athrow
    //   131: astore_2
    //   132: aload_0
    //   133: invokestatic 85	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   136: aload_1
    //   137: invokestatic 85	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   140: aload_2
    //   141: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	142	0	paramFile	java.io.File
    //   6	131	1	localObject1	Object
    //   4	64	2	localFileChannel1	java.nio.channels.FileChannel
    //   88	9	2	localObject2	Object
    //   103	15	2	localObject3	Object
    //   122	8	2	localRuntimeException	RuntimeException
    //   131	10	2	localObject4	Object
    //   20	89	3	localFileInputStream	java.io.FileInputStream
    //   1	78	4	localFileChannel2	java.nio.channels.FileChannel
    //   94	1	4	localNoSuchAlgorithmException1	NoSuchAlgorithmException
    //   113	12	4	localNoSuchAlgorithmException2	NoSuchAlgorithmException
    //   8	19	5	localObject5	Object
    //   15	54	6	localMessageDigest	MessageDigest
    // Exception table:
    //   from	to	target	type
    //   32	38	88	finally
    //   44	62	88	finally
    //   68	74	88	finally
    //   32	38	94	java/security/NoSuchAlgorithmException
    //   44	62	94	java/security/NoSuchAlgorithmException
    //   68	74	94	java/security/NoSuchAlgorithmException
    //   10	26	103	finally
    //   10	26	113	java/security/NoSuchAlgorithmException
    //   119	131	131	finally
  }
  
  public static String md5(String paramString)
  {
    try
    {
      paramString = MessageDigest.getInstance("MD5").digest(paramString.getBytes("UTF-8"));
      return toHexString(paramString);
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new RuntimeException(paramString);
    }
    catch (NoSuchAlgorithmException paramString)
    {
      throw new RuntimeException(paramString);
    }
  }
  
  public static String toHexString(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder(paramArrayOfByte.length * 2);
    int j = paramArrayOfByte.length;
    for (int i = 0; i < j; i++)
    {
      int k = paramArrayOfByte[i];
      localStringBuilder.append(hexDigits[(k >> 4 & 0xF)]);
      localStringBuilder.append(hexDigits[(k & 0xF)]);
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/MD5.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */