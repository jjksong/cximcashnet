package org.xutils.common.util;

import android.app.Application;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.xutils.x;

public final class ProcessLock
  implements Closeable
{
  private static final DecimalFormat FORMAT = new DecimalFormat("0.##################");
  private static final String LOCK_FILE_DIR = "process_lock";
  private static final DoubleKeyValueMap<String, Integer, ProcessLock> LOCK_MAP = new DoubleKeyValueMap();
  private final File mFile;
  private final FileLock mFileLock;
  private final String mLockName;
  private final Closeable mStream;
  private final boolean mWriteMode;
  
  static
  {
    IOUtil.deleteFileOrDir(x.app().getDir("process_lock", 0));
  }
  
  private ProcessLock(String paramString, File paramFile, FileLock paramFileLock, Closeable paramCloseable, boolean paramBoolean)
  {
    this.mLockName = paramString;
    this.mFileLock = paramFileLock;
    this.mFile = paramFile;
    this.mStream = paramCloseable;
    this.mWriteMode = paramBoolean;
  }
  
  private static String customHash(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return "0";
    }
    double d1 = 0.0D;
    byte[] arrayOfByte = paramString.getBytes();
    for (int i = 0; i < paramString.length(); i++)
    {
      double d2 = arrayOfByte[i];
      Double.isNaN(d2);
      d1 = (d1 * 255.0D + d2) * 0.005D;
    }
    return FORMAT.format(d1);
  }
  
  private static boolean isValid(FileLock paramFileLock)
  {
    boolean bool;
    if ((paramFileLock != null) && (paramFileLock.isValid())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  /* Error */
  private static void release(String paramString, FileLock paramFileLock, File paramFile, Closeable paramCloseable)
  {
    // Byte code:
    //   0: getstatic 33	org/xutils/common/util/ProcessLock:LOCK_MAP	Lorg/xutils/common/util/DoubleKeyValueMap;
    //   3: astore 4
    //   5: aload 4
    //   7: monitorenter
    //   8: aload_1
    //   9: ifnull +97 -> 106
    //   12: getstatic 33	org/xutils/common/util/ProcessLock:LOCK_MAP	Lorg/xutils/common/util/DoubleKeyValueMap;
    //   15: aload_0
    //   16: aload_1
    //   17: invokevirtual 121	java/lang/Object:hashCode	()I
    //   20: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   23: invokevirtual 131	org/xutils/common/util/DoubleKeyValueMap:remove	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   26: getstatic 33	org/xutils/common/util/ProcessLock:LOCK_MAP	Lorg/xutils/common/util/DoubleKeyValueMap;
    //   29: aload_0
    //   30: invokevirtual 135	org/xutils/common/util/DoubleKeyValueMap:get	(Ljava/lang/Object;)Ljava/util/concurrent/ConcurrentHashMap;
    //   33: astore_0
    //   34: aload_0
    //   35: ifnull +10 -> 45
    //   38: aload_0
    //   39: invokevirtual 139	java/util/concurrent/ConcurrentHashMap:isEmpty	()Z
    //   42: ifeq +8 -> 50
    //   45: aload_2
    //   46: invokestatic 51	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   49: pop
    //   50: aload_1
    //   51: invokevirtual 143	java/nio/channels/FileLock:channel	()Ljava/nio/channels/FileChannel;
    //   54: invokevirtual 148	java/nio/channels/FileChannel:isOpen	()Z
    //   57: ifeq +7 -> 64
    //   60: aload_1
    //   61: invokevirtual 150	java/nio/channels/FileLock:release	()V
    //   64: aload_1
    //   65: invokevirtual 143	java/nio/channels/FileLock:channel	()Ljava/nio/channels/FileChannel;
    //   68: astore_0
    //   69: aload_0
    //   70: invokestatic 154	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   73: goto +33 -> 106
    //   76: astore_0
    //   77: goto +20 -> 97
    //   80: astore_0
    //   81: aload_0
    //   82: invokevirtual 158	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   85: aload_0
    //   86: invokestatic 164	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   89: aload_1
    //   90: invokevirtual 143	java/nio/channels/FileLock:channel	()Ljava/nio/channels/FileChannel;
    //   93: astore_0
    //   94: goto -25 -> 69
    //   97: aload_1
    //   98: invokevirtual 143	java/nio/channels/FileLock:channel	()Ljava/nio/channels/FileChannel;
    //   101: invokestatic 154	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   104: aload_0
    //   105: athrow
    //   106: aload_3
    //   107: invokestatic 154	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   110: aload 4
    //   112: monitorexit
    //   113: return
    //   114: astore_0
    //   115: aload 4
    //   117: monitorexit
    //   118: aload_0
    //   119: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	paramString	String
    //   0	120	1	paramFileLock	FileLock
    //   0	120	2	paramFile	File
    //   0	120	3	paramCloseable	Closeable
    //   3	113	4	localDoubleKeyValueMap	DoubleKeyValueMap
    // Exception table:
    //   from	to	target	type
    //   12	34	76	finally
    //   38	45	76	finally
    //   45	50	76	finally
    //   50	64	76	finally
    //   81	89	76	finally
    //   12	34	80	java/lang/Throwable
    //   38	45	80	java/lang/Throwable
    //   45	50	80	java/lang/Throwable
    //   50	64	80	java/lang/Throwable
    //   64	69	114	finally
    //   69	73	114	finally
    //   89	94	114	finally
    //   97	106	114	finally
    //   106	113	114	finally
    //   115	118	114	finally
  }
  
  public static ProcessLock tryLock(String paramString, boolean paramBoolean)
  {
    return tryLockInternal(paramString, customHash(paramString), paramBoolean);
  }
  
  public static ProcessLock tryLock(String paramString, boolean paramBoolean, long paramLong)
    throws InterruptedException
  {
    long l = System.currentTimeMillis();
    String str = customHash(paramString);
    ProcessLock localProcessLock = null;
    for (;;)
    {
      if (System.currentTimeMillis() < l + paramLong)
      {
        localProcessLock = tryLockInternal(paramString, str, paramBoolean);
        if (localProcessLock != null) {}
      }
      try
      {
        Thread.sleep(1L);
      }
      catch (InterruptedException paramString)
      {
        throw paramString;
        return localProcessLock;
      }
      catch (Throwable localThrowable) {}
    }
  }
  
  private static ProcessLock tryLockInternal(String paramString1, String paramString2, boolean paramBoolean)
  {
    synchronized (LOCK_MAP)
    {
      Object localObject1 = LOCK_MAP.get(paramString1);
      Object localObject2;
      if ((localObject1 != null) && (!((ConcurrentHashMap)localObject1).isEmpty()))
      {
        localObject1 = ((ConcurrentHashMap)localObject1).entrySet().iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (ProcessLock)((Map.Entry)((Iterator)localObject1).next()).getValue();
          if (localObject2 != null)
          {
            if (!((ProcessLock)localObject2).isValid())
            {
              ((Iterator)localObject1).remove();
            }
            else
            {
              if (paramBoolean) {
                return null;
              }
              if (((ProcessLock)localObject2).mWriteMode) {
                return null;
              }
            }
          }
          else {
            ((Iterator)localObject1).remove();
          }
        }
      }
      try
      {
        localObject2 = new java/io/File;
        ((File)localObject2).<init>(x.app().getDir("process_lock", 0), paramString2);
        if ((!((File)localObject2).exists()) && (!((File)localObject2).createNewFile())) {
          break label414;
        }
        if (paramBoolean)
        {
          paramString2 = new java/io/FileOutputStream;
          paramString2.<init>((File)localObject2, false);
          localObject1 = paramString2.getChannel();
        }
        else
        {
          paramString2 = new java/io/FileInputStream;
          paramString2.<init>((File)localObject2);
          localObject1 = paramString2.getChannel();
        }
        boolean bool;
        if (localObject1 != null) {
          if (!paramBoolean) {
            bool = true;
          } else {
            bool = false;
          }
        }
        try
        {
          localObject4 = ((FileChannel)localObject1).tryLock(0L, Long.MAX_VALUE, bool);
          if (isValid((FileLock)localObject4))
          {
            localObject3 = new org/xutils/common/util/ProcessLock;
            ((ProcessLock)localObject3).<init>(paramString1, (File)localObject2, (FileLock)localObject4, paramString2, paramBoolean);
            LOCK_MAP.put(paramString1, Integer.valueOf(localObject4.hashCode()), localObject3);
            return (ProcessLock)localObject3;
          }
          release(paramString1, (FileLock)localObject4, (File)localObject2, paramString2);
        }
        catch (Throwable localThrowable1)
        {
          Object localObject4;
          break label351;
        }
        localObject3 = new java/io/IOException;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        ((StringBuilder)localObject4).append("can not get file channel:");
        ((StringBuilder)localObject4).append(((File)localObject2).getAbsolutePath());
        ((IOException)localObject3).<init>(((StringBuilder)localObject4).toString());
        throw ((Throwable)localObject3);
      }
      catch (Throwable localThrowable2)
      {
        paramString2 = null;
        localObject1 = paramString2;
      }
      label351:
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      ((StringBuilder)localObject3).append("tryLock: ");
      ((StringBuilder)localObject3).append(paramString1);
      ((StringBuilder)localObject3).append(", ");
      ((StringBuilder)localObject3).append(localThrowable2.getMessage());
      LogUtil.d(((StringBuilder)localObject3).toString());
      IOUtil.closeQuietly(paramString2);
      IOUtil.closeQuietly((Closeable)localObject1);
      label414:
      return null;
    }
  }
  
  public void close()
    throws IOException
  {
    release();
  }
  
  protected void finalize()
    throws Throwable
  {
    super.finalize();
    release();
  }
  
  public boolean isValid()
  {
    return isValid(this.mFileLock);
  }
  
  public void release()
  {
    release(this.mLockName, this.mFileLock, this.mFile, this.mStream);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mLockName);
    localStringBuilder.append(": ");
    localStringBuilder.append(this.mFile.getName());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/ProcessLock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */