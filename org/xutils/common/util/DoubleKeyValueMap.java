package org.xutils.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DoubleKeyValueMap<K1, K2, V>
{
  private final ConcurrentHashMap<K1, ConcurrentHashMap<K2, V>> k1_k2V_map = new ConcurrentHashMap();
  
  public void clear()
  {
    if (this.k1_k2V_map.size() > 0)
    {
      Iterator localIterator = this.k1_k2V_map.values().iterator();
      while (localIterator.hasNext()) {
        ((ConcurrentHashMap)localIterator.next()).clear();
      }
      this.k1_k2V_map.clear();
    }
  }
  
  public boolean containsKey(K1 paramK1)
  {
    return this.k1_k2V_map.containsKey(paramK1);
  }
  
  public boolean containsKey(K1 paramK1, K2 paramK2)
  {
    if (this.k1_k2V_map.containsKey(paramK1)) {
      return ((ConcurrentHashMap)this.k1_k2V_map.get(paramK1)).containsKey(paramK2);
    }
    return false;
  }
  
  public V get(K1 paramK1, K2 paramK2)
  {
    paramK1 = (ConcurrentHashMap)this.k1_k2V_map.get(paramK1);
    if (paramK1 == null) {
      paramK1 = null;
    } else {
      paramK1 = paramK1.get(paramK2);
    }
    return paramK1;
  }
  
  public ConcurrentHashMap<K2, V> get(K1 paramK1)
  {
    return (ConcurrentHashMap)this.k1_k2V_map.get(paramK1);
  }
  
  public Collection<V> getAllValues()
  {
    Object localObject = this.k1_k2V_map.keySet();
    if (localObject != null)
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = ((Set)localObject).iterator();
      for (;;)
      {
        localObject = localArrayList;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject = localIterator.next();
        localObject = ((ConcurrentHashMap)this.k1_k2V_map.get(localObject)).values();
        if (localObject != null) {
          localArrayList.addAll((Collection)localObject);
        }
      }
    }
    localObject = null;
    return (Collection<V>)localObject;
  }
  
  public Collection<V> getAllValues(K1 paramK1)
  {
    paramK1 = (ConcurrentHashMap)this.k1_k2V_map.get(paramK1);
    if (paramK1 == null) {
      paramK1 = null;
    } else {
      paramK1 = paramK1.values();
    }
    return paramK1;
  }
  
  public Set<K1> getFirstKeys()
  {
    return this.k1_k2V_map.keySet();
  }
  
  public void put(K1 paramK1, K2 paramK2, V paramV)
  {
    if ((paramK1 != null) && (paramK2 != null) && (paramV != null))
    {
      ConcurrentHashMap localConcurrentHashMap;
      if (this.k1_k2V_map.containsKey(paramK1))
      {
        localConcurrentHashMap = (ConcurrentHashMap)this.k1_k2V_map.get(paramK1);
        if (localConcurrentHashMap != null)
        {
          localConcurrentHashMap.put(paramK2, paramV);
        }
        else
        {
          localConcurrentHashMap = new ConcurrentHashMap();
          localConcurrentHashMap.put(paramK2, paramV);
          this.k1_k2V_map.put(paramK1, localConcurrentHashMap);
        }
      }
      else
      {
        localConcurrentHashMap = new ConcurrentHashMap();
        localConcurrentHashMap.put(paramK2, paramV);
        this.k1_k2V_map.put(paramK1, localConcurrentHashMap);
      }
      return;
    }
  }
  
  public void remove(K1 paramK1)
  {
    this.k1_k2V_map.remove(paramK1);
  }
  
  public void remove(K1 paramK1, K2 paramK2)
  {
    ConcurrentHashMap localConcurrentHashMap = (ConcurrentHashMap)this.k1_k2V_map.get(paramK1);
    if (localConcurrentHashMap != null) {
      localConcurrentHashMap.remove(paramK2);
    }
    if ((localConcurrentHashMap == null) || (localConcurrentHashMap.isEmpty())) {
      this.k1_k2V_map.remove(paramK1);
    }
  }
  
  public int size()
  {
    int j = this.k1_k2V_map.size();
    int i = 0;
    if (j == 0) {
      return 0;
    }
    Iterator localIterator = this.k1_k2V_map.values().iterator();
    while (localIterator.hasNext()) {
      i += ((ConcurrentHashMap)localIterator.next()).size();
    }
    return i;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/util/DoubleKeyValueMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */