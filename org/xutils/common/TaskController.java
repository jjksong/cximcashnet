package org.xutils.common;

import org.xutils.common.task.AbsTask;

public abstract interface TaskController
{
  public abstract void autoPost(Runnable paramRunnable);
  
  public abstract void post(Runnable paramRunnable);
  
  public abstract void postDelayed(Runnable paramRunnable, long paramLong);
  
  public abstract void removeCallbacks(Runnable paramRunnable);
  
  public abstract void run(Runnable paramRunnable);
  
  public abstract <T> AbsTask<T> start(AbsTask<T> paramAbsTask);
  
  public abstract <T> T startSync(AbsTask<T> paramAbsTask)
    throws Throwable;
  
  public abstract <T extends AbsTask<?>> Callback.Cancelable startTasks(Callback.GroupCallback<T> paramGroupCallback, T... paramVarArgs);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/TaskController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */