package org.xutils.common;

import java.lang.reflect.Type;

public abstract interface Callback
{
  public static abstract interface CacheCallback<ResultType>
    extends Callback.CommonCallback<ResultType>
  {
    public abstract boolean onCache(ResultType paramResultType);
  }
  
  public static abstract interface Callable<ResultType>
  {
    public abstract void call(ResultType paramResultType);
  }
  
  public static abstract interface Cancelable
  {
    public abstract void cancel();
    
    public abstract boolean isCancelled();
  }
  
  public static class CancelledException
    extends RuntimeException
  {
    public CancelledException(String paramString)
    {
      super();
    }
  }
  
  public static abstract interface CommonCallback<ResultType>
    extends Callback
  {
    public abstract void onCancelled(Callback.CancelledException paramCancelledException);
    
    public abstract void onError(Throwable paramThrowable, boolean paramBoolean);
    
    public abstract void onFinished();
    
    public abstract void onSuccess(ResultType paramResultType);
  }
  
  public static abstract interface GroupCallback<ItemType>
    extends Callback
  {
    public abstract void onAllFinished();
    
    public abstract void onCancelled(ItemType paramItemType, Callback.CancelledException paramCancelledException);
    
    public abstract void onError(ItemType paramItemType, Throwable paramThrowable, boolean paramBoolean);
    
    public abstract void onFinished(ItemType paramItemType);
    
    public abstract void onSuccess(ItemType paramItemType);
  }
  
  public static abstract interface PrepareCallback<PrepareType, ResultType>
    extends Callback.CommonCallback<ResultType>
  {
    public abstract ResultType prepare(PrepareType paramPrepareType);
  }
  
  public static abstract interface ProgressCallback<ResultType>
    extends Callback.CommonCallback<ResultType>
  {
    public abstract void onLoading(long paramLong1, long paramLong2, boolean paramBoolean);
    
    public abstract void onStarted();
    
    public abstract void onWaiting();
  }
  
  public static abstract interface ProxyCacheCallback<ResultType>
    extends Callback.CacheCallback<ResultType>
  {
    public abstract boolean onlyCache();
  }
  
  public static abstract interface TypedCallback<ResultType>
    extends Callback.CommonCallback<ResultType>
  {
    public abstract Type getLoadType();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/Callback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */