package org.xutils.common.task;

import android.os.Looper;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.Callback.GroupCallback;
import org.xutils.common.TaskController;
import org.xutils.common.util.LogUtil;
import org.xutils.x.Ext;

public final class TaskControllerImpl
  implements TaskController
{
  private static volatile TaskController instance;
  
  public static void registerInstance()
  {
    if (instance == null) {
      try
      {
        if (instance == null)
        {
          TaskControllerImpl localTaskControllerImpl = new org/xutils/common/task/TaskControllerImpl;
          localTaskControllerImpl.<init>();
          instance = localTaskControllerImpl;
        }
      }
      finally {}
    }
    x.Ext.setTaskController(instance);
  }
  
  public void autoPost(Runnable paramRunnable)
  {
    if (paramRunnable == null) {
      return;
    }
    if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
      paramRunnable.run();
    } else {
      TaskProxy.sHandler.post(paramRunnable);
    }
  }
  
  public void post(Runnable paramRunnable)
  {
    if (paramRunnable == null) {
      return;
    }
    TaskProxy.sHandler.post(paramRunnable);
  }
  
  public void postDelayed(Runnable paramRunnable, long paramLong)
  {
    if (paramRunnable == null) {
      return;
    }
    TaskProxy.sHandler.postDelayed(paramRunnable, paramLong);
  }
  
  public void removeCallbacks(Runnable paramRunnable)
  {
    TaskProxy.sHandler.removeCallbacks(paramRunnable);
  }
  
  public void run(Runnable paramRunnable)
  {
    if (!TaskProxy.sDefaultExecutor.isBusy()) {
      TaskProxy.sDefaultExecutor.execute(paramRunnable);
    } else {
      new Thread(paramRunnable).start();
    }
  }
  
  public <T> AbsTask<T> start(AbsTask<T> paramAbsTask)
  {
    if ((paramAbsTask instanceof TaskProxy)) {
      paramAbsTask = (TaskProxy)paramAbsTask;
    } else {
      paramAbsTask = new TaskProxy(paramAbsTask);
    }
    try
    {
      paramAbsTask.doBackground();
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    return paramAbsTask;
  }
  
  /* Error */
  public <T> T startSync(AbsTask<T> paramAbsTask)
    throws Throwable
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_3
    //   3: astore_2
    //   4: aload_1
    //   5: invokevirtual 128	org/xutils/common/task/AbsTask:onWaiting	()V
    //   8: aload_3
    //   9: astore_2
    //   10: aload_1
    //   11: invokevirtual 131	org/xutils/common/task/AbsTask:onStarted	()V
    //   14: aload_3
    //   15: astore_2
    //   16: aload_1
    //   17: invokevirtual 132	org/xutils/common/task/AbsTask:doBackground	()Ljava/lang/Object;
    //   20: astore_3
    //   21: aload_3
    //   22: astore_2
    //   23: aload_1
    //   24: aload_3
    //   25: invokevirtual 136	org/xutils/common/task/AbsTask:onSuccess	(Ljava/lang/Object;)V
    //   28: aload_3
    //   29: astore_2
    //   30: goto +22 -> 52
    //   33: astore_2
    //   34: goto +24 -> 58
    //   37: astore_2
    //   38: aload_1
    //   39: aload_2
    //   40: iconst_0
    //   41: invokevirtual 140	org/xutils/common/task/AbsTask:onError	(Ljava/lang/Throwable;Z)V
    //   44: aload_2
    //   45: athrow
    //   46: astore_3
    //   47: aload_1
    //   48: aload_3
    //   49: invokevirtual 144	org/xutils/common/task/AbsTask:onCancelled	(Lorg/xutils/common/Callback$CancelledException;)V
    //   52: aload_1
    //   53: invokevirtual 147	org/xutils/common/task/AbsTask:onFinished	()V
    //   56: aload_2
    //   57: areturn
    //   58: aload_1
    //   59: invokevirtual 147	org/xutils/common/task/AbsTask:onFinished	()V
    //   62: aload_2
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	TaskControllerImpl
    //   0	64	1	paramAbsTask	AbsTask<T>
    //   3	27	2	localObject1	Object
    //   33	1	2	localObject2	Object
    //   37	26	2	localThrowable	Throwable
    //   1	28	3	localObject3	Object
    //   46	3	3	localCancelledException	Callback.CancelledException
    // Exception table:
    //   from	to	target	type
    //   4	8	33	finally
    //   10	14	33	finally
    //   16	21	33	finally
    //   23	28	33	finally
    //   38	46	33	finally
    //   47	52	33	finally
    //   4	8	37	java/lang/Throwable
    //   10	14	37	java/lang/Throwable
    //   16	21	37	java/lang/Throwable
    //   23	28	37	java/lang/Throwable
    //   4	8	46	org/xutils/common/Callback$CancelledException
    //   10	14	46	org/xutils/common/Callback$CancelledException
    //   16	21	46	org/xutils/common/Callback$CancelledException
    //   23	28	46	org/xutils/common/Callback$CancelledException
  }
  
  public <T extends AbsTask<?>> Callback.Cancelable startTasks(final Callback.GroupCallback<T> paramGroupCallback, final T... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      final Runnable local1 = new Runnable()
      {
        private final AtomicInteger count = new AtomicInteger(0);
        private final int total = paramVarArgs.length;
        
        public void run()
        {
          if (this.count.incrementAndGet() == this.total)
          {
            Callback.GroupCallback localGroupCallback = paramGroupCallback;
            if (localGroupCallback != null) {
              localGroupCallback.onAllFinished();
            }
          }
        }
      };
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++)
      {
        final T ? = paramVarArgs[i];
        start(new TaskProxy(?)
        {
          protected void onCancelled(final Callback.CancelledException paramAnonymousCancelledException)
          {
            super.onCancelled(paramAnonymousCancelledException);
            TaskControllerImpl.this.post(new Runnable()
            {
              public void run()
              {
                if (TaskControllerImpl.2.this.val$groupCallback != null) {
                  TaskControllerImpl.2.this.val$groupCallback.onCancelled(TaskControllerImpl.2.this.val$task, paramAnonymousCancelledException);
                }
              }
            });
          }
          
          protected void onError(final Throwable paramAnonymousThrowable, final boolean paramAnonymousBoolean)
          {
            super.onError(paramAnonymousThrowable, paramAnonymousBoolean);
            TaskControllerImpl.this.post(new Runnable()
            {
              public void run()
              {
                if (TaskControllerImpl.2.this.val$groupCallback != null) {
                  TaskControllerImpl.2.this.val$groupCallback.onError(TaskControllerImpl.2.this.val$task, paramAnonymousThrowable, paramAnonymousBoolean);
                }
              }
            });
          }
          
          protected void onFinished()
          {
            super.onFinished();
            TaskControllerImpl.this.post(new Runnable()
            {
              public void run()
              {
                if (TaskControllerImpl.2.this.val$groupCallback != null) {
                  TaskControllerImpl.2.this.val$groupCallback.onFinished(TaskControllerImpl.2.this.val$task);
                }
                TaskControllerImpl.2.this.val$callIfOnAllFinished.run();
              }
            });
          }
          
          protected void onSuccess(Object paramAnonymousObject)
          {
            super.onSuccess(paramAnonymousObject);
            TaskControllerImpl.this.post(new Runnable()
            {
              public void run()
              {
                if (TaskControllerImpl.2.this.val$groupCallback != null) {
                  TaskControllerImpl.2.this.val$groupCallback.onSuccess(TaskControllerImpl.2.this.val$task);
                }
              }
            });
          }
        });
      }
      new Callback.Cancelable()
      {
        public void cancel()
        {
          AbsTask[] arrayOfAbsTask = paramVarArgs;
          int j = arrayOfAbsTask.length;
          for (int i = 0; i < j; i++) {
            arrayOfAbsTask[i].cancel();
          }
        }
        
        public boolean isCancelled()
        {
          AbsTask[] arrayOfAbsTask = paramVarArgs;
          int j = arrayOfAbsTask.length;
          int i = 0;
          boolean bool = true;
          while (i < j)
          {
            if (!arrayOfAbsTask[i].isCancelled()) {
              bool = false;
            }
            i++;
          }
          return bool;
        }
      };
    }
    throw new IllegalArgumentException("task must not be null");
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/task/TaskControllerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */