package org.xutils.common.task;

class PriorityRunnable
  implements Runnable
{
  long SEQ;
  public final Priority priority;
  private final Runnable runnable;
  
  public PriorityRunnable(Priority paramPriority, Runnable paramRunnable)
  {
    Priority localPriority = paramPriority;
    if (paramPriority == null) {
      localPriority = Priority.DEFAULT;
    }
    this.priority = localPriority;
    this.runnable = paramRunnable;
  }
  
  public final void run()
  {
    this.runnable.run();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/task/PriorityRunnable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */