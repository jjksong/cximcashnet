package org.xutils.common.task;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.Executor;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.util.LogUtil;
import org.xutils.x;

class TaskProxy<ResultType>
  extends AbsTask<ResultType>
{
  private static final int MSG_WHAT_BASE = 1000000000;
  private static final int MSG_WHAT_ON_CANCEL = 1000000006;
  private static final int MSG_WHAT_ON_ERROR = 1000000004;
  private static final int MSG_WHAT_ON_FINISHED = 1000000007;
  private static final int MSG_WHAT_ON_START = 1000000002;
  private static final int MSG_WHAT_ON_SUCCESS = 1000000003;
  private static final int MSG_WHAT_ON_UPDATE = 1000000005;
  private static final int MSG_WHAT_ON_WAITING = 1000000001;
  static final PriorityExecutor sDefaultExecutor = new PriorityExecutor(true);
  static final InternalHandler sHandler = new InternalHandler(null);
  private volatile boolean callOnCanceled = false;
  private volatile boolean callOnFinished = false;
  private final Executor executor;
  private final AbsTask<ResultType> task;
  
  TaskProxy(AbsTask<ResultType> paramAbsTask)
  {
    super(paramAbsTask);
    this.task = paramAbsTask;
    this.task.setTaskProxy(this);
    setTaskProxy(null);
    Executor localExecutor = paramAbsTask.getExecutor();
    paramAbsTask = localExecutor;
    if (localExecutor == null) {
      paramAbsTask = sDefaultExecutor;
    }
    this.executor = paramAbsTask;
  }
  
  protected final ResultType doBackground()
    throws Throwable
  {
    onWaiting();
    PriorityRunnable localPriorityRunnable = new PriorityRunnable(this.task.getPriority(), new Runnable()
    {
      /* Error */
      public void run()
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   4: invokestatic 30	org/xutils/common/task/TaskProxy:access$100	(Lorg/xutils/common/task/TaskProxy;)Z
        //   7: ifne +121 -> 128
        //   10: aload_0
        //   11: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   14: invokevirtual 34	org/xutils/common/task/TaskProxy:isCancelled	()Z
        //   17: ifne +111 -> 128
        //   20: aload_0
        //   21: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   24: invokevirtual 37	org/xutils/common/task/TaskProxy:onStarted	()V
        //   27: aload_0
        //   28: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   31: invokevirtual 34	org/xutils/common/task/TaskProxy:isCancelled	()Z
        //   34: ifne +82 -> 116
        //   37: aload_0
        //   38: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   41: invokestatic 41	org/xutils/common/task/TaskProxy:access$200	(Lorg/xutils/common/task/TaskProxy;)Lorg/xutils/common/task/AbsTask;
        //   44: aload_0
        //   45: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   48: invokestatic 41	org/xutils/common/task/TaskProxy:access$200	(Lorg/xutils/common/task/TaskProxy;)Lorg/xutils/common/task/AbsTask;
        //   51: invokevirtual 44	org/xutils/common/task/AbsTask:doBackground	()Ljava/lang/Object;
        //   54: invokevirtual 48	org/xutils/common/task/AbsTask:setResult	(Ljava/lang/Object;)V
        //   57: aload_0
        //   58: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   61: aload_0
        //   62: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   65: invokestatic 41	org/xutils/common/task/TaskProxy:access$200	(Lorg/xutils/common/task/TaskProxy;)Lorg/xutils/common/task/AbsTask;
        //   68: invokevirtual 51	org/xutils/common/task/AbsTask:getResult	()Ljava/lang/Object;
        //   71: invokevirtual 52	org/xutils/common/task/TaskProxy:setResult	(Ljava/lang/Object;)V
        //   74: aload_0
        //   75: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   78: invokevirtual 34	org/xutils/common/task/TaskProxy:isCancelled	()Z
        //   81: ifne +23 -> 104
        //   84: aload_0
        //   85: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   88: aload_0
        //   89: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   92: invokestatic 41	org/xutils/common/task/TaskProxy:access$200	(Lorg/xutils/common/task/TaskProxy;)Lorg/xutils/common/task/AbsTask;
        //   95: invokevirtual 51	org/xutils/common/task/AbsTask:getResult	()Ljava/lang/Object;
        //   98: invokevirtual 55	org/xutils/common/task/TaskProxy:onSuccess	(Ljava/lang/Object;)V
        //   101: goto +65 -> 166
        //   104: new 24	org/xutils/common/Callback$CancelledException
        //   107: astore_1
        //   108: aload_1
        //   109: ldc 57
        //   111: invokespecial 60	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
        //   114: aload_1
        //   115: athrow
        //   116: new 24	org/xutils/common/Callback$CancelledException
        //   119: astore_1
        //   120: aload_1
        //   121: ldc 57
        //   123: invokespecial 60	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
        //   126: aload_1
        //   127: athrow
        //   128: new 24	org/xutils/common/Callback$CancelledException
        //   131: astore_1
        //   132: aload_1
        //   133: ldc 57
        //   135: invokespecial 60	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
        //   138: aload_1
        //   139: athrow
        //   140: astore_1
        //   141: goto +33 -> 174
        //   144: astore_1
        //   145: aload_0
        //   146: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   149: aload_1
        //   150: iconst_0
        //   151: invokevirtual 64	org/xutils/common/task/TaskProxy:onError	(Ljava/lang/Throwable;Z)V
        //   154: goto +12 -> 166
        //   157: astore_1
        //   158: aload_0
        //   159: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   162: aload_1
        //   163: invokevirtual 68	org/xutils/common/task/TaskProxy:onCancelled	(Lorg/xutils/common/Callback$CancelledException;)V
        //   166: aload_0
        //   167: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   170: invokevirtual 71	org/xutils/common/task/TaskProxy:onFinished	()V
        //   173: return
        //   174: aload_0
        //   175: getfield 17	org/xutils/common/task/TaskProxy$1:this$0	Lorg/xutils/common/task/TaskProxy;
        //   178: invokevirtual 71	org/xutils/common/task/TaskProxy:onFinished	()V
        //   181: aload_1
        //   182: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	183	0	this	1
        //   107	32	1	localCancelledException1	Callback.CancelledException
        //   140	1	1	localObject	Object
        //   144	6	1	localThrowable	Throwable
        //   157	25	1	localCancelledException2	Callback.CancelledException
        // Exception table:
        //   from	to	target	type
        //   0	101	140	finally
        //   104	116	140	finally
        //   116	128	140	finally
        //   128	140	140	finally
        //   145	154	140	finally
        //   158	166	140	finally
        //   0	101	144	java/lang/Throwable
        //   104	116	144	java/lang/Throwable
        //   116	128	144	java/lang/Throwable
        //   128	140	144	java/lang/Throwable
        //   0	101	157	org/xutils/common/Callback$CancelledException
        //   104	116	157	org/xutils/common/Callback$CancelledException
        //   116	128	157	org/xutils/common/Callback$CancelledException
        //   128	140	157	org/xutils/common/Callback$CancelledException
      }
    });
    this.executor.execute(localPriorityRunnable);
    return null;
  }
  
  public final Executor getExecutor()
  {
    return this.executor;
  }
  
  public final Priority getPriority()
  {
    return this.task.getPriority();
  }
  
  protected void onCancelled(Callback.CancelledException paramCancelledException)
  {
    setState(AbsTask.State.CANCELLED);
    sHandler.obtainMessage(1000000006, new ArgsObj(this, new Object[] { paramCancelledException })).sendToTarget();
  }
  
  protected void onError(Throwable paramThrowable, boolean paramBoolean)
  {
    setState(AbsTask.State.ERROR);
    sHandler.obtainMessage(1000000004, new ArgsObj(this, new Object[] { paramThrowable })).sendToTarget();
  }
  
  protected void onFinished()
  {
    sHandler.obtainMessage(1000000007, this).sendToTarget();
  }
  
  protected void onStarted()
  {
    setState(AbsTask.State.STARTED);
    sHandler.obtainMessage(1000000002, this).sendToTarget();
  }
  
  protected void onSuccess(ResultType paramResultType)
  {
    setState(AbsTask.State.SUCCESS);
    sHandler.obtainMessage(1000000003, this).sendToTarget();
  }
  
  protected void onUpdate(int paramInt, Object... paramVarArgs)
  {
    sHandler.obtainMessage(1000000005, paramInt, paramInt, new ArgsObj(this, paramVarArgs)).sendToTarget();
  }
  
  protected void onWaiting()
  {
    setState(AbsTask.State.WAITING);
    sHandler.obtainMessage(1000000001, this).sendToTarget();
  }
  
  final void setState(AbsTask.State paramState)
  {
    super.setState(paramState);
    this.task.setState(paramState);
  }
  
  private static class ArgsObj
  {
    final Object[] args;
    final TaskProxy taskProxy;
    
    public ArgsObj(TaskProxy paramTaskProxy, Object... paramVarArgs)
    {
      this.taskProxy = paramTaskProxy;
      this.args = paramVarArgs;
    }
  }
  
  static final class InternalHandler
    extends Handler
  {
    private InternalHandler()
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      if (paramMessage.obj != null)
      {
        boolean bool = paramMessage.obj instanceof TaskProxy;
        TaskProxy localTaskProxy = null;
        Object localObject;
        if (bool)
        {
          localTaskProxy = (TaskProxy)paramMessage.obj;
          localObject = null;
        }
        else if ((paramMessage.obj instanceof TaskProxy.ArgsObj))
        {
          localObject = (TaskProxy.ArgsObj)paramMessage.obj;
          localTaskProxy = ((TaskProxy.ArgsObj)localObject).taskProxy;
          localObject = ((TaskProxy.ArgsObj)localObject).args;
        }
        else
        {
          localObject = null;
        }
        if (localTaskProxy != null)
        {
          try
          {
            switch (paramMessage.what)
            {
            default: 
              break;
            case 1000000007: 
              if (localTaskProxy.callOnFinished) {
                return;
              }
              TaskProxy.access$302(localTaskProxy, true);
              localTaskProxy.task.onFinished();
              break;
            case 1000000006: 
              if (localTaskProxy.callOnCanceled) {
                return;
              }
              TaskProxy.access$102(localTaskProxy, true);
              localTaskProxy.task.onCancelled((Callback.CancelledException)localObject[0]);
              break;
            case 1000000005: 
              localTaskProxy.task.onUpdate(paramMessage.arg1, (Object[])localObject);
              break;
            case 1000000004: 
              localObject = (Throwable)localObject[0];
              LogUtil.d(((Throwable)localObject).getMessage(), (Throwable)localObject);
              localTaskProxy.task.onError((Throwable)localObject, false);
              break;
            case 1000000003: 
              localTaskProxy.task.onSuccess(localTaskProxy.getResult());
              break;
            case 1000000002: 
              localTaskProxy.task.onStarted();
              break;
            case 1000000001: 
              localTaskProxy.task.onWaiting();
            }
          }
          catch (Throwable localThrowable)
          {
            localTaskProxy.setState(AbsTask.State.ERROR);
            if (paramMessage.what != 1000000004) {
              localTaskProxy.task.onError(localThrowable, true);
            } else {
              if (x.isDebug()) {
                break label302;
              }
            }
          }
          return;
          label302:
          throw new RuntimeException(localThrowable);
        }
        throw new RuntimeException("msg.obj not instanceof TaskProxy");
      }
      throw new IllegalArgumentException("msg must not be null");
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/task/TaskProxy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */