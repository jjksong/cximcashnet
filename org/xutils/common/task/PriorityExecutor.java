package org.xutils.common.task;

import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class PriorityExecutor
  implements Executor
{
  private static final int CORE_POOL_SIZE = 5;
  private static final Comparator<Runnable> FIFO_CMP = new Comparator()
  {
    public int compare(Runnable paramAnonymousRunnable1, Runnable paramAnonymousRunnable2)
    {
      if (((paramAnonymousRunnable1 instanceof PriorityRunnable)) && ((paramAnonymousRunnable2 instanceof PriorityRunnable)))
      {
        paramAnonymousRunnable1 = (PriorityRunnable)paramAnonymousRunnable1;
        paramAnonymousRunnable2 = (PriorityRunnable)paramAnonymousRunnable2;
        int j = paramAnonymousRunnable1.priority.ordinal() - paramAnonymousRunnable2.priority.ordinal();
        int i = j;
        if (j == 0) {
          i = (int)(paramAnonymousRunnable1.SEQ - paramAnonymousRunnable2.SEQ);
        }
        return i;
      }
      return 0;
    }
  };
  private static final Comparator<Runnable> FILO_CMP = new Comparator()
  {
    public int compare(Runnable paramAnonymousRunnable1, Runnable paramAnonymousRunnable2)
    {
      if (((paramAnonymousRunnable1 instanceof PriorityRunnable)) && ((paramAnonymousRunnable2 instanceof PriorityRunnable)))
      {
        paramAnonymousRunnable1 = (PriorityRunnable)paramAnonymousRunnable1;
        paramAnonymousRunnable2 = (PriorityRunnable)paramAnonymousRunnable2;
        int j = paramAnonymousRunnable1.priority.ordinal() - paramAnonymousRunnable2.priority.ordinal();
        int i = j;
        if (j == 0) {
          i = (int)(paramAnonymousRunnable2.SEQ - paramAnonymousRunnable1.SEQ);
        }
        return i;
      }
      return 0;
    }
  };
  private static final int KEEP_ALIVE = 1;
  private static final int MAXIMUM_POOL_SIZE = 256;
  private static final AtomicLong SEQ_SEED = new AtomicLong(0L);
  private static final ThreadFactory sThreadFactory = new ThreadFactory()
  {
    private final AtomicInteger mCount = new AtomicInteger(1);
    
    public Thread newThread(Runnable paramAnonymousRunnable)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("xTID#");
      localStringBuilder.append(this.mCount.getAndIncrement());
      return new Thread(paramAnonymousRunnable, localStringBuilder.toString());
    }
  };
  private final ThreadPoolExecutor mThreadPoolExecutor;
  
  public PriorityExecutor(int paramInt, boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject = FIFO_CMP;
    } else {
      localObject = FILO_CMP;
    }
    Object localObject = new PriorityBlockingQueue(256, (Comparator)localObject);
    this.mThreadPoolExecutor = new ThreadPoolExecutor(paramInt, 256, 1L, TimeUnit.SECONDS, (BlockingQueue)localObject, sThreadFactory);
  }
  
  public PriorityExecutor(boolean paramBoolean)
  {
    this(5, paramBoolean);
  }
  
  public void execute(Runnable paramRunnable)
  {
    if ((paramRunnable instanceof PriorityRunnable)) {
      ((PriorityRunnable)paramRunnable).SEQ = SEQ_SEED.getAndIncrement();
    }
    this.mThreadPoolExecutor.execute(paramRunnable);
  }
  
  public int getPoolSize()
  {
    return this.mThreadPoolExecutor.getCorePoolSize();
  }
  
  public ThreadPoolExecutor getThreadPoolExecutor()
  {
    return this.mThreadPoolExecutor;
  }
  
  public boolean isBusy()
  {
    boolean bool;
    if (this.mThreadPoolExecutor.getActiveCount() >= this.mThreadPoolExecutor.getCorePoolSize()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void setPoolSize(int paramInt)
  {
    if (paramInt > 0) {
      this.mThreadPoolExecutor.setCorePoolSize(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/common/task/PriorityExecutor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */