package org.xutils;

import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.common.Callback.TypedCallback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;

public abstract interface HttpManager
{
  public abstract <T> Callback.Cancelable get(RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback);
  
  public abstract <T> T getSync(RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable;
  
  public abstract <T> Callback.Cancelable post(RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback);
  
  public abstract <T> T postSync(RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable;
  
  public abstract <T> Callback.Cancelable request(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback);
  
  public abstract <T> T requestSync(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable;
  
  public abstract <T> T requestSync(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Callback.TypedCallback<T> paramTypedCallback)
    throws Throwable;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/HttpManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */