package org.xutils.view;

import android.app.Activity;
import android.view.View;

final class ViewFinder
{
  private Activity activity;
  private View view;
  
  public ViewFinder(Activity paramActivity)
  {
    this.activity = paramActivity;
  }
  
  public ViewFinder(View paramView)
  {
    this.view = paramView;
  }
  
  public View findViewById(int paramInt)
  {
    Object localObject = this.view;
    if (localObject != null) {
      return ((View)localObject).findViewById(paramInt);
    }
    localObject = this.activity;
    if (localObject != null) {
      return ((Activity)localObject).findViewById(paramInt);
    }
    return null;
  }
  
  public View findViewById(int paramInt1, int paramInt2)
  {
    View localView;
    if (paramInt2 > 0) {
      localView = findViewById(paramInt2);
    } else {
      localView = null;
    }
    if (localView != null) {
      localView = localView.findViewById(paramInt1);
    } else {
      localView = findViewById(paramInt1);
    }
    return localView;
  }
  
  public View findViewByInfo(ViewInfo paramViewInfo)
  {
    return findViewById(paramViewInfo.value, paramViewInfo.parentId);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/view/ViewFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */