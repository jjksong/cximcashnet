package org.xutils.view.annotation;

import android.view.View.OnClickListener;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD})
public @interface Event
{
  String method() default "";
  
  int[] parentId() default {0};
  
  String setter() default "";
  
  Class<?> type() default View.OnClickListener.class;
  
  int[] value();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/view/annotation/Event.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */