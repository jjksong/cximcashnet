package org.xutils.view;

import android.text.TextUtils;
import android.view.View;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.xutils.common.util.DoubleKeyValueMap;
import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.Event;

final class EventListenerManager
{
  private static final HashSet<String> AVOID_QUICK_EVENT_SET = new HashSet(2);
  private static final long QUICK_EVENT_TIME_SPAN = 300L;
  private static final DoubleKeyValueMap<ViewInfo, Class<?>, Object> listenerCache = new DoubleKeyValueMap();
  
  static
  {
    AVOID_QUICK_EVENT_SET.add("onClick");
    AVOID_QUICK_EVENT_SET.add("onItemClick");
  }
  
  public static void addEventMethod(ViewFinder paramViewFinder, ViewInfo paramViewInfo, Event paramEvent, Object paramObject, Method paramMethod)
  {
    try
    {
      View localView = paramViewFinder.findViewByInfo(paramViewInfo);
      if (localView != null)
      {
        Class localClass = paramEvent.type();
        String str = paramEvent.setter();
        paramViewFinder = str;
        if (TextUtils.isEmpty(str))
        {
          paramViewFinder = new java/lang/StringBuilder;
          paramViewFinder.<init>();
          paramViewFinder.append("set");
          paramViewFinder.append(localClass.getSimpleName());
          paramViewFinder = paramViewFinder.toString();
        }
        str = paramEvent.method();
        paramEvent = listenerCache.get(paramViewInfo, localClass);
        boolean bool1;
        if (paramEvent != null)
        {
          DynamicHandler localDynamicHandler = (DynamicHandler)Proxy.getInvocationHandler(paramEvent);
          boolean bool2 = paramObject.equals(localDynamicHandler.getHandler());
          bool1 = bool2;
          if (bool2)
          {
            localDynamicHandler.addMethod(str, paramMethod);
            bool1 = bool2;
          }
        }
        else
        {
          bool1 = false;
        }
        if (!bool1)
        {
          paramEvent = new org/xutils/view/EventListenerManager$DynamicHandler;
          paramEvent.<init>(paramObject);
          paramEvent.addMethod(str, paramMethod);
          paramEvent = Proxy.newProxyInstance(localClass.getClassLoader(), new Class[] { localClass }, paramEvent);
          listenerCache.put(paramViewInfo, localClass, paramEvent);
        }
        localView.getClass().getMethod(paramViewFinder, new Class[] { localClass }).invoke(localView, new Object[] { paramEvent });
      }
    }
    catch (Throwable paramViewFinder)
    {
      LogUtil.e(paramViewFinder.getMessage(), paramViewFinder);
    }
  }
  
  public static class DynamicHandler
    implements InvocationHandler
  {
    private static long lastClickTime;
    private WeakReference<Object> handlerRef;
    private final HashMap<String, Method> methodMap = new HashMap(1);
    
    public DynamicHandler(Object paramObject)
    {
      this.handlerRef = new WeakReference(paramObject);
    }
    
    public void addMethod(String paramString, Method paramMethod)
    {
      this.methodMap.put(paramString, paramMethod);
    }
    
    public Object getHandler()
    {
      return this.handlerRef.get();
    }
    
    public Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
      throws Throwable
    {
      Object localObject1 = this.handlerRef.get();
      if (localObject1 != null)
      {
        String str = paramMethod.getName();
        if ("toString".equals(str)) {
          return DynamicHandler.class.getSimpleName();
        }
        paramMethod = (Method)this.methodMap.get(str);
        paramObject = paramMethod;
        if (paramMethod == null)
        {
          paramObject = paramMethod;
          if (this.methodMap.size() == 1)
          {
            Object localObject2 = this.methodMap.entrySet().iterator();
            paramObject = paramMethod;
            if (((Iterator)localObject2).hasNext())
            {
              localObject2 = (Map.Entry)((Iterator)localObject2).next();
              paramObject = paramMethod;
              if (TextUtils.isEmpty((CharSequence)((Map.Entry)localObject2).getKey())) {
                paramObject = (Method)((Map.Entry)localObject2).getValue();
              }
            }
          }
        }
        if (paramObject != null)
        {
          if (EventListenerManager.AVOID_QUICK_EVENT_SET.contains(str))
          {
            long l = System.currentTimeMillis() - lastClickTime;
            if (l < 300L)
            {
              paramObject = new StringBuilder();
              ((StringBuilder)paramObject).append("onClick cancelled: ");
              ((StringBuilder)paramObject).append(l);
              LogUtil.d(((StringBuilder)paramObject).toString());
              return null;
            }
            lastClickTime = System.currentTimeMillis();
          }
          try
          {
            paramMethod = ((Method)paramObject).invoke(localObject1, paramArrayOfObject);
            return paramMethod;
          }
          catch (Throwable paramArrayOfObject)
          {
            paramMethod = new StringBuilder();
            paramMethod.append("invoke method error:");
            paramMethod.append(localObject1.getClass().getName());
            paramMethod.append("#");
            paramMethod.append(((Method)paramObject).getName());
            throw new RuntimeException(paramMethod.toString(), paramArrayOfObject);
          }
        }
        paramObject = new StringBuilder();
        ((StringBuilder)paramObject).append("method not impl: ");
        ((StringBuilder)paramObject).append(str);
        ((StringBuilder)paramObject).append("(");
        ((StringBuilder)paramObject).append(localObject1.getClass().getSimpleName());
        ((StringBuilder)paramObject).append(")");
        LogUtil.w(((StringBuilder)paramObject).toString());
      }
      return null;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/view/EventListenerManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */