package org.xutils.view;

import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import org.xutils.ViewInjector;
import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x.Ext;

public final class ViewInjectorImpl
  implements ViewInjector
{
  private static final HashSet<Class<?>> IGNORED = new HashSet();
  private static volatile ViewInjectorImpl instance;
  private static final Object lock;
  
  static
  {
    IGNORED.add(Object.class);
    IGNORED.add(Activity.class);
    IGNORED.add(Fragment.class);
    try
    {
      IGNORED.add(Class.forName("android.support.v4.app.Fragment"));
      IGNORED.add(Class.forName("android.support.v4.app.FragmentActivity"));
      lock = new Object();
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  private static ContentView findContentView(Class<?> paramClass)
  {
    if ((paramClass != null) && (!IGNORED.contains(paramClass)))
    {
      ContentView localContentView = (ContentView)paramClass.getAnnotation(ContentView.class);
      if (localContentView == null) {
        return findContentView(paramClass.getSuperclass());
      }
      return localContentView;
    }
    return null;
  }
  
  private static void injectObject(Object paramObject, Class<?> paramClass, ViewFinder paramViewFinder)
  {
    if ((paramClass != null) && (!IGNORED.contains(paramClass)))
    {
      injectObject(paramObject, paramClass.getSuperclass(), paramViewFinder);
      Field[] arrayOfField = paramClass.getDeclaredFields();
      int j;
      int i;
      Object localObject1;
      Object localObject2;
      if ((arrayOfField != null) && (arrayOfField.length > 0))
      {
        j = arrayOfField.length;
        for (i = 0; i < j; i++)
        {
          Field localField = arrayOfField[i];
          localObject1 = localField.getType();
          if ((!Modifier.isStatic(localField.getModifiers())) && (!Modifier.isFinal(localField.getModifiers())) && (!((Class)localObject1).isPrimitive()) && (!((Class)localObject1).isArray()))
          {
            localObject1 = (ViewInject)localField.getAnnotation(ViewInject.class);
            if (localObject1 != null) {
              try
              {
                localObject1 = paramViewFinder.findViewById(((ViewInject)localObject1).value(), ((ViewInject)localObject1).parentId());
                if (localObject1 != null)
                {
                  localField.setAccessible(true);
                  localField.set(paramObject, localObject1);
                }
                else
                {
                  localObject2 = new java/lang/RuntimeException;
                  localObject1 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject1).<init>();
                  ((StringBuilder)localObject1).append("Invalid @ViewInject for ");
                  ((StringBuilder)localObject1).append(paramClass.getSimpleName());
                  ((StringBuilder)localObject1).append(".");
                  ((StringBuilder)localObject1).append(localField.getName());
                  ((RuntimeException)localObject2).<init>(((StringBuilder)localObject1).toString());
                  throw ((Throwable)localObject2);
                }
              }
              catch (Throwable localThrowable2)
              {
                LogUtil.e(localThrowable2.getMessage(), localThrowable2);
              }
            }
          }
        }
      }
      paramClass = paramClass.getDeclaredMethods();
      if ((paramClass != null) && (paramClass.length > 0))
      {
        int n = paramClass.length;
        i = 0;
        while (i < n)
        {
          arrayOfField = paramClass[i];
          if ((!Modifier.isStatic(arrayOfField.getModifiers())) && (Modifier.isPrivate(arrayOfField.getModifiers())))
          {
            localObject2 = (Event)arrayOfField.getAnnotation(Event.class);
            if (localObject2 != null) {
              try
              {
                int[] arrayOfInt1 = ((Event)localObject2).value();
                int[] arrayOfInt2 = ((Event)localObject2).parentId();
                if (arrayOfInt2 == null) {
                  j = 0;
                } else {
                  j = arrayOfInt2.length;
                }
                for (int k = 0; k < arrayOfInt1.length; k++)
                {
                  int m = arrayOfInt1[k];
                  if (m > 0)
                  {
                    localObject1 = new org/xutils/view/ViewInfo;
                    ((ViewInfo)localObject1).<init>();
                    ((ViewInfo)localObject1).value = m;
                    if (j > k) {
                      m = arrayOfInt2[k];
                    } else {
                      m = 0;
                    }
                    ((ViewInfo)localObject1).parentId = m;
                    arrayOfField.setAccessible(true);
                    EventListenerManager.addEventMethod(paramViewFinder, (ViewInfo)localObject1, (Event)localObject2, paramObject, arrayOfField);
                  }
                }
                i++;
              }
              catch (Throwable localThrowable1)
              {
                LogUtil.e(localThrowable1.getMessage(), localThrowable1);
              }
            }
          }
        }
      }
      return;
    }
  }
  
  public static void registerInstance()
  {
    if (instance == null) {
      synchronized (lock)
      {
        if (instance == null)
        {
          ViewInjectorImpl localViewInjectorImpl = new org/xutils/view/ViewInjectorImpl;
          localViewInjectorImpl.<init>();
          instance = localViewInjectorImpl;
        }
      }
    }
    x.Ext.setViewInjector(instance);
  }
  
  public View inject(Object paramObject, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup)
  {
    Class localClass = paramObject.getClass();
    Object localObject2 = null;
    Object localObject1;
    try
    {
      ContentView localContentView = findContentView(localClass);
      localObject1 = localObject2;
      if (localContentView != null)
      {
        int i = localContentView.value();
        localObject1 = localObject2;
        if (i > 0) {
          localObject1 = paramLayoutInflater.inflate(i, paramViewGroup, false);
        }
      }
    }
    catch (Throwable paramLayoutInflater)
    {
      LogUtil.e(paramLayoutInflater.getMessage(), paramLayoutInflater);
      localObject1 = localObject2;
    }
    injectObject(paramObject, localClass, new ViewFinder((View)localObject1));
    return (View)localObject1;
  }
  
  public void inject(Activity paramActivity)
  {
    Class localClass = paramActivity.getClass();
    try
    {
      ContentView localContentView = findContentView(localClass);
      if (localContentView != null)
      {
        int i = localContentView.value();
        if (i > 0) {
          localClass.getMethod("setContentView", new Class[] { Integer.TYPE }).invoke(paramActivity, new Object[] { Integer.valueOf(i) });
        }
      }
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    injectObject(paramActivity, localClass, new ViewFinder(paramActivity));
  }
  
  public void inject(View paramView)
  {
    injectObject(paramView, paramView.getClass(), new ViewFinder(paramView));
  }
  
  public void inject(Object paramObject, View paramView)
  {
    injectObject(paramObject, paramObject.getClass(), new ViewFinder(paramView));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/view/ViewInjectorImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */