package org.xutils.view;

final class ViewInfo
{
  public int parentId;
  public int value;
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (ViewInfo)paramObject;
      if (this.value != ((ViewInfo)paramObject).value) {
        return false;
      }
      if (this.parentId != ((ViewInfo)paramObject).parentId) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public int hashCode()
  {
    return this.value * 31 + this.parentId;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/view/ViewInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */