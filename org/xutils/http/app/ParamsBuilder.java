package org.xutils.http.app;

import javax.net.ssl.SSLSocketFactory;
import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpRequest;

public abstract interface ParamsBuilder
{
  public abstract String buildCacheKey(RequestParams paramRequestParams, String[] paramArrayOfString);
  
  public abstract void buildParams(RequestParams paramRequestParams)
    throws Throwable;
  
  public abstract void buildSign(RequestParams paramRequestParams, String[] paramArrayOfString)
    throws Throwable;
  
  public abstract String buildUri(RequestParams paramRequestParams, HttpRequest paramHttpRequest)
    throws Throwable;
  
  public abstract SSLSocketFactory getSSLSocketFactory()
    throws Throwable;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/ParamsBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */