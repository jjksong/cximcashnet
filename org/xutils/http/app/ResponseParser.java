package org.xutils.http.app;

import java.lang.reflect.Type;
import org.xutils.http.request.UriRequest;

public abstract interface ResponseParser
{
  public abstract void checkResponse(UriRequest paramUriRequest)
    throws Throwable;
  
  public abstract Object parse(Type paramType, Class<?> paramClass, String paramString)
    throws Throwable;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/ResponseParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */