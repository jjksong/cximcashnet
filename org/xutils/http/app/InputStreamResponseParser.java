package org.xutils.http.app;

import java.io.InputStream;
import java.lang.reflect.Type;

public abstract class InputStreamResponseParser
  implements ResponseParser
{
  public abstract Object parse(Type paramType, Class<?> paramClass, InputStream paramInputStream)
    throws Throwable;
  
  @Deprecated
  public final Object parse(Type paramType, Class<?> paramClass, String paramString)
    throws Throwable
  {
    return null;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/InputStreamResponseParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */