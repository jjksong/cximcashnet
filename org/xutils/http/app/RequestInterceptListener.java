package org.xutils.http.app;

import org.xutils.http.request.UriRequest;

public abstract interface RequestInterceptListener
{
  public abstract void afterRequest(UriRequest paramUriRequest)
    throws Throwable;
  
  public abstract void beforeRequest(UriRequest paramUriRequest)
    throws Throwable;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/RequestInterceptListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */