package org.xutils.http.app;

import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

public abstract interface RequestTracker
{
  public abstract void onCache(UriRequest paramUriRequest, Object paramObject);
  
  public abstract void onCancelled(UriRequest paramUriRequest);
  
  public abstract void onError(UriRequest paramUriRequest, Throwable paramThrowable, boolean paramBoolean);
  
  public abstract void onFinished(UriRequest paramUriRequest);
  
  public abstract void onRequestCreated(UriRequest paramUriRequest);
  
  public abstract void onStart(RequestParams paramRequestParams);
  
  public abstract void onSuccess(UriRequest paramUriRequest, Object paramObject);
  
  public abstract void onWaiting(RequestParams paramRequestParams);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/RequestTracker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */