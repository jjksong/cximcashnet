package org.xutils.http.app;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.PortUnreachableException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.HashSet;
import org.json.JSONException;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.util.LogUtil;
import org.xutils.ex.HttpException;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

public class HttpRetryHandler
{
  protected static HashSet<Class<?>> blackList = new HashSet();
  protected int maxRetryCount = 2;
  
  static
  {
    blackList.add(HttpException.class);
    blackList.add(Callback.CancelledException.class);
    blackList.add(MalformedURLException.class);
    blackList.add(URISyntaxException.class);
    blackList.add(NoRouteToHostException.class);
    blackList.add(PortUnreachableException.class);
    blackList.add(ProtocolException.class);
    blackList.add(NullPointerException.class);
    blackList.add(FileNotFoundException.class);
    blackList.add(JSONException.class);
    blackList.add(UnknownHostException.class);
    blackList.add(IllegalArgumentException.class);
  }
  
  public boolean canRetry(UriRequest paramUriRequest, Throwable paramThrowable, int paramInt)
  {
    LogUtil.w(paramThrowable.getMessage(), paramThrowable);
    if (paramInt > this.maxRetryCount)
    {
      LogUtil.w(paramUriRequest.toString());
      LogUtil.w("The Max Retry times has been reached!");
      return false;
    }
    if (!HttpMethod.permitsRetry(paramUriRequest.getParams().getMethod()))
    {
      LogUtil.w(paramUriRequest.toString());
      LogUtil.w("The Request Method can not be retried.");
      return false;
    }
    if (blackList.contains(paramThrowable.getClass()))
    {
      LogUtil.w(paramUriRequest.toString());
      LogUtil.w("The Exception can not be retried.");
      return false;
    }
    return true;
  }
  
  public void setMaxRetryCount(int paramInt)
  {
    this.maxRetryCount = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/HttpRetryHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */