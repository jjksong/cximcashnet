package org.xutils.http.app;

import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpRequest;

public class DefaultParamsBuilder
  implements ParamsBuilder
{
  private static SSLSocketFactory trustAllSSlSocketFactory;
  
  public static SSLSocketFactory getTrustAllSSLSocketFactory()
  {
    if (trustAllSSlSocketFactory == null) {
      try
      {
        if (trustAllSSlSocketFactory == null)
        {
          X509TrustManager local1 = new org/xutils/http/app/DefaultParamsBuilder$1;
          local1.<init>();
          try
          {
            SSLContext localSSLContext = SSLContext.getInstance("TLS");
            localSSLContext.init(null, new TrustManager[] { local1 }, null);
            trustAllSSlSocketFactory = localSSLContext.getSocketFactory();
          }
          catch (Throwable localThrowable)
          {
            LogUtil.e(localThrowable.getMessage(), localThrowable);
          }
        }
      }
      finally {}
    }
    return trustAllSSlSocketFactory;
  }
  
  public String buildCacheKey(RequestParams paramRequestParams, String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length > 0))
    {
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(paramRequestParams.getUri());
      ((StringBuilder)localObject1).append("?");
      localObject1 = ((StringBuilder)localObject1).toString();
      int j = paramArrayOfString.length;
      int i = 0;
      for (;;)
      {
        localObject2 = localObject1;
        if (i >= j) {
          break;
        }
        String str1 = paramArrayOfString[i];
        String str2 = paramRequestParams.getStringParameter(str1);
        localObject2 = localObject1;
        if (str2 != null)
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(str1);
          ((StringBuilder)localObject2).append("=");
          ((StringBuilder)localObject2).append(str2);
          ((StringBuilder)localObject2).append("&");
          localObject2 = ((StringBuilder)localObject2).toString();
        }
        i++;
        localObject1 = localObject2;
      }
    }
    Object localObject2 = null;
    return (String)localObject2;
  }
  
  public void buildParams(RequestParams paramRequestParams)
    throws Throwable
  {}
  
  public void buildSign(RequestParams paramRequestParams, String[] paramArrayOfString)
    throws Throwable
  {}
  
  public String buildUri(RequestParams paramRequestParams, HttpRequest paramHttpRequest)
    throws Throwable
  {
    paramRequestParams = new StringBuilder();
    paramRequestParams.append(paramHttpRequest.host());
    paramRequestParams.append("/");
    paramRequestParams.append(paramHttpRequest.path());
    return paramRequestParams.toString();
  }
  
  public SSLSocketFactory getSSLSocketFactory()
    throws Throwable
  {
    return getTrustAllSSLSocketFactory();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/app/DefaultParamsBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */