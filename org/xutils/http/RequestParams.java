package org.xutils.http;

import android.text.TextUtils;
import java.net.Proxy;
import java.util.concurrent.Executor;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import org.xutils.common.task.Priority;
import org.xutils.common.util.LogUtil;
import org.xutils.http.annotation.HttpRequest;
import org.xutils.http.app.DefaultParamsBuilder;
import org.xutils.http.app.HttpRetryHandler;
import org.xutils.http.app.ParamsBuilder;
import org.xutils.http.app.RedirectHandler;
import org.xutils.http.app.RequestTracker;

public class RequestParams
  extends BaseParams
{
  private boolean autoRename = false;
  private boolean autoResume = true;
  private String buildCacheKey;
  private String buildUri;
  private ParamsBuilder builder;
  private String cacheDirName;
  private final String[] cacheKeys;
  private long cacheMaxAge;
  private long cacheSize;
  private boolean cancelFast = false;
  private int connectTimeout = 15000;
  private Executor executor;
  private HostnameVerifier hostnameVerifier;
  private HttpRequest httpRequest;
  private HttpRetryHandler httpRetryHandler;
  private boolean invokedGetHttpRequest = false;
  private int loadingUpdateMaxTimeSpan = 300;
  private int maxRetryCount = 2;
  private Priority priority = Priority.DEFAULT;
  private Proxy proxy;
  private int readTimeout = 15000;
  private RedirectHandler redirectHandler;
  private RequestTracker requestTracker;
  private String saveFilePath;
  private final String[] signs;
  private SSLSocketFactory sslSocketFactory;
  private String uri;
  private boolean useCookie = true;
  
  public RequestParams()
  {
    this(null, null, null, null);
  }
  
  public RequestParams(String paramString)
  {
    this(paramString, null, null, null);
  }
  
  public RequestParams(String paramString, ParamsBuilder paramParamsBuilder, String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    Object localObject = paramParamsBuilder;
    if (paramString != null)
    {
      localObject = paramParamsBuilder;
      if (paramParamsBuilder == null) {
        localObject = new DefaultParamsBuilder();
      }
    }
    this.uri = paramString;
    this.signs = paramArrayOfString1;
    this.cacheKeys = paramArrayOfString2;
    this.builder = ((ParamsBuilder)localObject);
  }
  
  private HttpRequest getHttpRequest()
  {
    if ((this.httpRequest == null) && (!this.invokedGetHttpRequest))
    {
      this.invokedGetHttpRequest = true;
      Class localClass = getClass();
      if (localClass != RequestParams.class) {
        this.httpRequest = ((HttpRequest)localClass.getAnnotation(HttpRequest.class));
      }
    }
    return this.httpRequest;
  }
  
  private void initEntityParams()
  {
    RequestParamsHelper.parseKV(this, getClass(), new RequestParamsHelper.ParseKVListener()
    {
      public void onParseKV(String paramAnonymousString, Object paramAnonymousObject)
      {
        RequestParams.this.addParameter(paramAnonymousString, paramAnonymousObject);
      }
    });
  }
  
  public String getCacheDirName()
  {
    return this.cacheDirName;
  }
  
  public String getCacheKey()
  {
    if ((TextUtils.isEmpty(this.buildCacheKey)) && (this.builder != null))
    {
      HttpRequest localHttpRequest = getHttpRequest();
      if (localHttpRequest != null) {
        this.buildCacheKey = this.builder.buildCacheKey(this, localHttpRequest.cacheKeys());
      } else {
        this.buildCacheKey = this.builder.buildCacheKey(this, this.cacheKeys);
      }
    }
    return this.buildCacheKey;
  }
  
  public long getCacheMaxAge()
  {
    return this.cacheMaxAge;
  }
  
  public long getCacheSize()
  {
    return this.cacheSize;
  }
  
  public int getConnectTimeout()
  {
    return this.connectTimeout;
  }
  
  public Executor getExecutor()
  {
    return this.executor;
  }
  
  public HostnameVerifier getHostnameVerifier()
  {
    return this.hostnameVerifier;
  }
  
  public HttpRetryHandler getHttpRetryHandler()
  {
    return this.httpRetryHandler;
  }
  
  public int getLoadingUpdateMaxTimeSpan()
  {
    return this.loadingUpdateMaxTimeSpan;
  }
  
  public int getMaxRetryCount()
  {
    return this.maxRetryCount;
  }
  
  public Priority getPriority()
  {
    return this.priority;
  }
  
  public Proxy getProxy()
  {
    return this.proxy;
  }
  
  public int getReadTimeout()
  {
    return this.readTimeout;
  }
  
  public RedirectHandler getRedirectHandler()
  {
    return this.redirectHandler;
  }
  
  public RequestTracker getRequestTracker()
  {
    return this.requestTracker;
  }
  
  public String getSaveFilePath()
  {
    return this.saveFilePath;
  }
  
  public SSLSocketFactory getSslSocketFactory()
  {
    return this.sslSocketFactory;
  }
  
  public String getUri()
  {
    String str;
    if (TextUtils.isEmpty(this.buildUri)) {
      str = this.uri;
    } else {
      str = this.buildUri;
    }
    return str;
  }
  
  void init()
    throws Throwable
  {
    if (!TextUtils.isEmpty(this.buildUri)) {
      return;
    }
    if ((TextUtils.isEmpty(this.uri)) && (getHttpRequest() == null)) {
      throw new IllegalStateException("uri is empty && @HttpRequest == null");
    }
    initEntityParams();
    this.buildUri = this.uri;
    Object localObject = getHttpRequest();
    if (localObject != null)
    {
      this.builder = ((ParamsBuilder)((HttpRequest)localObject).builder().newInstance());
      this.buildUri = this.builder.buildUri(this, (HttpRequest)localObject);
      this.builder.buildParams(this);
      this.builder.buildSign(this, ((HttpRequest)localObject).signs());
      if (this.sslSocketFactory == null) {
        this.sslSocketFactory = this.builder.getSSLSocketFactory();
      }
    }
    else
    {
      localObject = this.builder;
      if (localObject != null)
      {
        ((ParamsBuilder)localObject).buildParams(this);
        this.builder.buildSign(this, this.signs);
        if (this.sslSocketFactory == null) {
          this.sslSocketFactory = this.builder.getSSLSocketFactory();
        }
      }
    }
  }
  
  public boolean isAutoRename()
  {
    return this.autoRename;
  }
  
  public boolean isAutoResume()
  {
    return this.autoResume;
  }
  
  public boolean isCancelFast()
  {
    return this.cancelFast;
  }
  
  public boolean isUseCookie()
  {
    return this.useCookie;
  }
  
  public void setAutoRename(boolean paramBoolean)
  {
    this.autoRename = paramBoolean;
  }
  
  public void setAutoResume(boolean paramBoolean)
  {
    this.autoResume = paramBoolean;
  }
  
  public void setCacheDirName(String paramString)
  {
    this.cacheDirName = paramString;
  }
  
  public void setCacheMaxAge(long paramLong)
  {
    this.cacheMaxAge = paramLong;
  }
  
  public void setCacheSize(long paramLong)
  {
    this.cacheSize = paramLong;
  }
  
  public void setCancelFast(boolean paramBoolean)
  {
    this.cancelFast = paramBoolean;
  }
  
  public void setConnectTimeout(int paramInt)
  {
    if (paramInt > 0) {
      this.connectTimeout = paramInt;
    }
  }
  
  public void setExecutor(Executor paramExecutor)
  {
    this.executor = paramExecutor;
  }
  
  public void setHostnameVerifier(HostnameVerifier paramHostnameVerifier)
  {
    this.hostnameVerifier = paramHostnameVerifier;
  }
  
  public void setHttpRetryHandler(HttpRetryHandler paramHttpRetryHandler)
  {
    this.httpRetryHandler = paramHttpRetryHandler;
  }
  
  public void setLoadingUpdateMaxTimeSpan(int paramInt)
  {
    this.loadingUpdateMaxTimeSpan = paramInt;
  }
  
  public void setMaxRetryCount(int paramInt)
  {
    this.maxRetryCount = paramInt;
  }
  
  public void setPriority(Priority paramPriority)
  {
    this.priority = paramPriority;
  }
  
  public void setProxy(Proxy paramProxy)
  {
    this.proxy = paramProxy;
  }
  
  public void setReadTimeout(int paramInt)
  {
    if (paramInt > 0) {
      this.readTimeout = paramInt;
    }
  }
  
  public void setRedirectHandler(RedirectHandler paramRedirectHandler)
  {
    this.redirectHandler = paramRedirectHandler;
  }
  
  public void setRequestTracker(RequestTracker paramRequestTracker)
  {
    this.requestTracker = paramRequestTracker;
  }
  
  public void setSaveFilePath(String paramString)
  {
    this.saveFilePath = paramString;
  }
  
  public void setSslSocketFactory(SSLSocketFactory paramSSLSocketFactory)
  {
    this.sslSocketFactory = paramSSLSocketFactory;
  }
  
  public void setUri(String paramString)
  {
    if (TextUtils.isEmpty(this.buildUri)) {
      this.uri = paramString;
    } else {
      this.buildUri = paramString;
    }
  }
  
  public void setUseCookie(boolean paramBoolean)
  {
    this.useCookie = paramBoolean;
  }
  
  public String toString()
  {
    try
    {
      init();
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    String str = getUri();
    if (TextUtils.isEmpty(str))
    {
      str = super.toString();
    }
    else
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(str);
      if (str.contains("?")) {
        str = "&";
      } else {
        str = "?";
      }
      localStringBuilder.append(str);
      localStringBuilder.append(super.toString());
      str = localStringBuilder.toString();
    }
    return str;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/RequestParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */