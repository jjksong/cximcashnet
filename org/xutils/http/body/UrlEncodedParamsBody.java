package org.xutils.http.body;

import android.net.Uri;
import android.text.TextUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import org.xutils.common.util.KeyValue;

public class UrlEncodedParamsBody
  implements RequestBody
{
  private String charset = "UTF-8";
  private byte[] content;
  
  public UrlEncodedParamsBody(List<KeyValue> paramList, String paramString)
    throws IOException
  {
    if (!TextUtils.isEmpty(paramString)) {
      this.charset = paramString;
    }
    paramString = new StringBuilder();
    if (paramList != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        Object localObject = (KeyValue)paramList.next();
        String str = ((KeyValue)localObject).key;
        localObject = ((KeyValue)localObject).getValueStr();
        if ((!TextUtils.isEmpty(str)) && (localObject != null))
        {
          if (paramString.length() > 0) {
            paramString.append("&");
          }
          paramString.append(Uri.encode(str, this.charset));
          paramString.append("=");
          paramString.append(Uri.encode((String)localObject, this.charset));
        }
      }
    }
    this.content = paramString.toString().getBytes(this.charset);
  }
  
  public long getContentLength()
  {
    return this.content.length;
  }
  
  public String getContentType()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("application/x-www-form-urlencoded;charset=");
    localStringBuilder.append(this.charset);
    return localStringBuilder.toString();
  }
  
  public void setContentType(String paramString) {}
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream.write(this.content);
    paramOutputStream.flush();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/UrlEncodedParamsBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */