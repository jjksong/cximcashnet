package org.xutils.http.body;

import java.io.IOException;
import java.io.OutputStream;

public abstract interface RequestBody
{
  public abstract long getContentLength();
  
  public abstract String getContentType();
  
  public abstract void setContentType(String paramString);
  
  public abstract void writeTo(OutputStream paramOutputStream)
    throws IOException;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/RequestBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */