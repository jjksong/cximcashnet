package org.xutils.http.body;

import android.text.TextUtils;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.util.IOUtil;
import org.xutils.http.ProgressHandler;

public class InputStreamBody
  implements ProgressBody
{
  private ProgressHandler callBackHandler;
  private InputStream content;
  private String contentType;
  private long current = 0L;
  private final long total;
  
  public InputStreamBody(InputStream paramInputStream)
  {
    this(paramInputStream, null);
  }
  
  public InputStreamBody(InputStream paramInputStream, String paramString)
  {
    this.content = paramInputStream;
    this.contentType = paramString;
    this.total = getInputStreamLength(paramInputStream);
  }
  
  public static long getInputStreamLength(InputStream paramInputStream)
  {
    try
    {
      if (((paramInputStream instanceof FileInputStream)) || ((paramInputStream instanceof ByteArrayInputStream)))
      {
        int i = paramInputStream.available();
        return i;
      }
    }
    catch (Throwable paramInputStream)
    {
      for (;;) {}
    }
    return -1L;
  }
  
  public long getContentLength()
  {
    return this.total;
  }
  
  public String getContentType()
  {
    String str;
    if (TextUtils.isEmpty(this.contentType)) {
      str = "application/octet-stream";
    } else {
      str = this.contentType;
    }
    return str;
  }
  
  public void setContentType(String paramString)
  {
    this.contentType = paramString;
  }
  
  public void setProgressHandler(ProgressHandler paramProgressHandler)
  {
    this.callBackHandler = paramProgressHandler;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Object localObject = this.callBackHandler;
    if ((localObject != null) && (!((ProgressHandler)localObject).updateProgress(this.total, this.current, true))) {
      throw new Callback.CancelledException("upload stopped!");
    }
    localObject = new byte['Ѐ'];
    try
    {
      do
      {
        int i = this.content.read((byte[])localObject);
        if (i == -1) {
          break;
        }
        paramOutputStream.write((byte[])localObject, 0, i);
        this.current += i;
      } while ((this.callBackHandler == null) || (this.callBackHandler.updateProgress(this.total, this.current, false)));
      paramOutputStream = new org/xutils/common/Callback$CancelledException;
      paramOutputStream.<init>("upload stopped!");
      throw paramOutputStream;
      paramOutputStream.flush();
      if (this.callBackHandler != null) {
        this.callBackHandler.updateProgress(this.total, this.total, true);
      }
      return;
    }
    finally
    {
      IOUtil.closeQuietly(this.content);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/InputStreamBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */