package org.xutils.http.body;

import org.xutils.http.ProgressHandler;

public abstract interface ProgressBody
  extends RequestBody
{
  public abstract void setProgressHandler(ProgressHandler paramProgressHandler);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/ProgressBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */