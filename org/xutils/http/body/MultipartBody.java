package org.xutils.http.body;

import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.KeyValue;
import org.xutils.http.ProgressHandler;

public class MultipartBody
  implements ProgressBody
{
  private static byte[] BOUNDARY_PREFIX_BYTES = "--------7da3d81520810".getBytes();
  private static byte[] END_BYTES = "\r\n".getBytes();
  private static byte[] TWO_DASHES_BYTES = "--".getBytes();
  private byte[] boundaryPostfixBytes;
  private ProgressHandler callBackHandler;
  private String charset = "UTF-8";
  private String contentType;
  private long current = 0L;
  private List<KeyValue> multipartParams;
  private long total = 0L;
  
  public MultipartBody(List<KeyValue> paramList, String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {
      this.charset = paramString;
    }
    this.multipartParams = paramList;
    generateContentType();
    paramList = new CounterOutputStream();
    try
    {
      writeTo(paramList);
      this.total = paramList.total.get();
    }
    catch (IOException paramList)
    {
      this.total = -1L;
    }
  }
  
  private static byte[] buildContentDisposition(String paramString1, String paramString2, String paramString3)
    throws UnsupportedEncodingException
  {
    StringBuilder localStringBuilder = new StringBuilder("Content-Disposition: form-data");
    localStringBuilder.append("; name=\"");
    localStringBuilder.append(paramString1.replace("\"", "\\\""));
    localStringBuilder.append("\"");
    if (!TextUtils.isEmpty(paramString2))
    {
      localStringBuilder.append("; filename=\"");
      localStringBuilder.append(paramString2.replace("\"", "\\\""));
      localStringBuilder.append("\"");
    }
    return localStringBuilder.toString().getBytes(paramString3);
  }
  
  private static byte[] buildContentType(Object paramObject, String paramString1, String paramString2)
    throws UnsupportedEncodingException
  {
    StringBuilder localStringBuilder = new StringBuilder("Content-Type: ");
    if (TextUtils.isEmpty(paramString1))
    {
      if ((paramObject instanceof String))
      {
        paramObject = new StringBuilder();
        ((StringBuilder)paramObject).append("text/plain; charset=");
        ((StringBuilder)paramObject).append(paramString2);
        paramObject = ((StringBuilder)paramObject).toString();
      }
      else
      {
        paramObject = "application/octet-stream";
      }
    }
    else {
      paramObject = paramString1.replaceFirst("\\/jpg$", "/jpeg");
    }
    localStringBuilder.append((String)paramObject);
    return localStringBuilder.toString().getBytes(paramString2);
  }
  
  private void generateContentType()
  {
    String str = Double.toHexString(Math.random() * 65535.0D);
    this.boundaryPostfixBytes = str.getBytes();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("multipart/form-data; boundary=");
    localStringBuilder.append(new String(BOUNDARY_PREFIX_BYTES));
    localStringBuilder.append(str);
    this.contentType = localStringBuilder.toString();
  }
  
  private void writeEntry(OutputStream paramOutputStream, String paramString, Object paramObject)
    throws IOException
  {
    writeLine(paramOutputStream, new byte[][] { TWO_DASHES_BYTES, BOUNDARY_PREFIX_BYTES, this.boundaryPostfixBytes });
    Object localObject1 = "";
    Object localObject2;
    Object localObject3;
    if ((paramObject instanceof BodyItemWrapper))
    {
      paramObject = (BodyItemWrapper)paramObject;
      localObject2 = ((BodyItemWrapper)paramObject).getValue();
      localObject1 = ((BodyItemWrapper)paramObject).getFileName();
      paramObject = ((BodyItemWrapper)paramObject).getContentType();
    }
    else
    {
      localObject3 = null;
      localObject2 = paramObject;
      paramObject = localObject3;
    }
    if ((localObject2 instanceof File))
    {
      File localFile = (File)localObject2;
      localObject3 = localObject1;
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        localObject3 = localFile.getName();
      }
      localObject1 = paramObject;
      if (TextUtils.isEmpty((CharSequence)paramObject)) {
        localObject1 = FileBody.getFileContentType(localFile);
      }
      writeLine(paramOutputStream, new byte[][] { buildContentDisposition(paramString, (String)localObject3, this.charset) });
      writeLine(paramOutputStream, new byte[][] { buildContentType(localObject2, (String)localObject1, this.charset) });
      writeLine(paramOutputStream, new byte[0][]);
      writeFile(paramOutputStream, localFile);
      writeLine(paramOutputStream, new byte[0][]);
    }
    else
    {
      writeLine(paramOutputStream, new byte[][] { buildContentDisposition(paramString, (String)localObject1, this.charset) });
      writeLine(paramOutputStream, new byte[][] { buildContentType(localObject2, (String)paramObject, this.charset) });
      writeLine(paramOutputStream, new byte[0][]);
      if ((localObject2 instanceof InputStream))
      {
        writeStreamAndCloseIn(paramOutputStream, (InputStream)localObject2);
        writeLine(paramOutputStream, new byte[0][]);
      }
      else
      {
        if ((localObject2 instanceof byte[])) {
          paramString = (byte[])localObject2;
        } else {
          paramString = String.valueOf(localObject2).getBytes(this.charset);
        }
        writeLine(paramOutputStream, new byte[][] { paramString });
        this.current += paramString.length;
        paramOutputStream = this.callBackHandler;
        if ((paramOutputStream != null) && (!paramOutputStream.updateProgress(this.total, this.current, false))) {
          throw new Callback.CancelledException("upload stopped!");
        }
      }
    }
  }
  
  private void writeFile(OutputStream paramOutputStream, File paramFile)
    throws IOException
  {
    if ((paramOutputStream instanceof CounterOutputStream)) {
      ((CounterOutputStream)paramOutputStream).addFile(paramFile);
    } else {
      writeStreamAndCloseIn(paramOutputStream, new FileInputStream(paramFile));
    }
  }
  
  private void writeLine(OutputStream paramOutputStream, byte[]... paramVarArgs)
    throws IOException
  {
    if (paramVarArgs != null)
    {
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++) {
        paramOutputStream.write(paramVarArgs[i]);
      }
    }
    paramOutputStream.write(END_BYTES);
  }
  
  private void writeStreamAndCloseIn(OutputStream paramOutputStream, InputStream paramInputStream)
    throws IOException
  {
    if ((paramOutputStream instanceof CounterOutputStream)) {
      ((CounterOutputStream)paramOutputStream).addStream(paramInputStream);
    }
    try
    {
      byte[] arrayOfByte = new byte['Ѐ'];
      do
      {
        int i = paramInputStream.read(arrayOfByte);
        if (i < 0) {
          break;
        }
        paramOutputStream.write(arrayOfByte, 0, i);
        this.current += i;
      } while ((this.callBackHandler == null) || (this.callBackHandler.updateProgress(this.total, this.current, false)));
      paramOutputStream = new org/xutils/common/Callback$CancelledException;
      paramOutputStream.<init>("upload stopped!");
      throw paramOutputStream;
      return;
    }
    finally
    {
      IOUtil.closeQuietly(paramInputStream);
    }
  }
  
  public long getContentLength()
  {
    return this.total;
  }
  
  public String getContentType()
  {
    return this.contentType;
  }
  
  public void setContentType(String paramString)
  {
    int i = this.contentType.indexOf(";");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("multipart/");
    localStringBuilder.append(paramString);
    localStringBuilder.append(this.contentType.substring(i));
    this.contentType = localStringBuilder.toString();
  }
  
  public void setProgressHandler(ProgressHandler paramProgressHandler)
  {
    this.callBackHandler = paramProgressHandler;
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Object localObject1 = this.callBackHandler;
    if ((localObject1 != null) && (!((ProgressHandler)localObject1).updateProgress(this.total, this.current, true))) {
      throw new Callback.CancelledException("upload stopped!");
    }
    localObject1 = this.multipartParams.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = (KeyValue)((Iterator)localObject1).next();
      String str = ((KeyValue)localObject2).key;
      localObject2 = ((KeyValue)localObject2).value;
      if ((!TextUtils.isEmpty(str)) && (localObject2 != null)) {
        writeEntry(paramOutputStream, str, localObject2);
      }
    }
    localObject1 = TWO_DASHES_BYTES;
    writeLine(paramOutputStream, new byte[][] { localObject1, BOUNDARY_PREFIX_BYTES, this.boundaryPostfixBytes, localObject1 });
    paramOutputStream.flush();
    paramOutputStream = this.callBackHandler;
    if (paramOutputStream != null)
    {
      long l = this.total;
      paramOutputStream.updateProgress(l, l, true);
    }
  }
  
  private class CounterOutputStream
    extends OutputStream
  {
    final AtomicLong total = new AtomicLong(0L);
    
    public CounterOutputStream() {}
    
    public void addFile(File paramFile)
    {
      if (this.total.get() == -1L) {
        return;
      }
      this.total.addAndGet(paramFile.length());
    }
    
    public void addStream(InputStream paramInputStream)
    {
      if (this.total.get() == -1L) {
        return;
      }
      long l = InputStreamBody.getInputStreamLength(paramInputStream);
      if (l > 0L) {
        this.total.addAndGet(l);
      } else {
        this.total.set(-1L);
      }
    }
    
    public void write(int paramInt)
      throws IOException
    {
      if (this.total.get() == -1L) {
        return;
      }
      this.total.incrementAndGet();
    }
    
    public void write(byte[] paramArrayOfByte)
      throws IOException
    {
      if (this.total.get() == -1L) {
        return;
      }
      this.total.addAndGet(paramArrayOfByte.length);
    }
    
    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if (this.total.get() == -1L) {
        return;
      }
      this.total.addAndGet(paramInt2);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/MultipartBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */