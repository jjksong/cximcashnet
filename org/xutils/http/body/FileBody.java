package org.xutils.http.body;

import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;

public class FileBody
  extends InputStreamBody
{
  private String contentType;
  private File file;
  
  public FileBody(File paramFile)
    throws IOException
  {
    this(paramFile, null);
  }
  
  public FileBody(File paramFile, String paramString)
    throws IOException
  {
    super(new FileInputStream(paramFile));
    this.file = paramFile;
    this.contentType = paramString;
  }
  
  public static String getFileContentType(File paramFile)
  {
    paramFile = HttpURLConnection.guessContentTypeFromName(paramFile.getName());
    if (TextUtils.isEmpty(paramFile)) {
      paramFile = "application/octet-stream";
    } else {
      paramFile = paramFile.replaceFirst("\\/jpg$", "/jpeg");
    }
    return paramFile;
  }
  
  public String getContentType()
  {
    if (TextUtils.isEmpty(this.contentType)) {
      this.contentType = getFileContentType(this.file);
    }
    return this.contentType;
  }
  
  public void setContentType(String paramString)
  {
    this.contentType = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/body/FileBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */