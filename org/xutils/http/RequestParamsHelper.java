package org.xutils.http;

import android.os.Parcelable.Creator;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.LogUtil;

final class RequestParamsHelper
{
  private static final ClassLoader BOOT_CL = String.class.getClassLoader();
  
  static Object parseJSONObject(Object paramObject)
    throws JSONException
  {
    if (paramObject == null) {
      return null;
    }
    Object localObject2 = paramObject.getClass();
    if (((Class)localObject2).isArray())
    {
      localObject2 = new JSONArray();
      int j = Array.getLength(paramObject);
      for (int i = 0;; i++)
      {
        localObject1 = localObject2;
        if (i >= j) {
          break;
        }
        ((JSONArray)localObject2).put(parseJSONObject(Array.get(paramObject, i)));
      }
    }
    if ((paramObject instanceof Iterable))
    {
      localObject2 = new JSONArray();
      paramObject = ((Iterable)paramObject).iterator();
      for (;;)
      {
        localObject1 = localObject2;
        if (!((Iterator)paramObject).hasNext()) {
          break;
        }
        ((JSONArray)localObject2).put(parseJSONObject(((Iterator)paramObject).next()));
      }
    }
    if ((paramObject instanceof Map))
    {
      localObject2 = new JSONObject();
      paramObject = ((Map)paramObject).entrySet().iterator();
      for (;;)
      {
        localObject1 = localObject2;
        if (!((Iterator)paramObject).hasNext()) {
          break;
        }
        Object localObject3 = (Map.Entry)((Iterator)paramObject).next();
        localObject1 = ((Map.Entry)localObject3).getKey();
        localObject3 = ((Map.Entry)localObject3).getValue();
        if ((localObject1 != null) && (localObject3 != null)) {
          ((JSONObject)localObject2).put(String.valueOf(localObject1), parseJSONObject(localObject3));
        }
      }
    }
    Object localObject1 = ((Class)localObject2).getClassLoader();
    if ((localObject1 != null) && (localObject1 != BOOT_CL))
    {
      localObject1 = new JSONObject();
      parseKV(paramObject, (Class)localObject2, new ParseKVListener()
      {
        public void onParseKV(String paramAnonymousString, Object paramAnonymousObject)
        {
          try
          {
            paramAnonymousObject = RequestParamsHelper.parseJSONObject(paramAnonymousObject);
            this.val$jo.put(paramAnonymousString, paramAnonymousObject);
            return;
          }
          catch (JSONException paramAnonymousString)
          {
            throw new IllegalArgumentException("parse RequestParams to json failed", paramAnonymousString);
          }
        }
      });
    }
    else
    {
      localObject1 = paramObject;
    }
    return localObject1;
  }
  
  static void parseKV(Object paramObject, Class<?> paramClass, ParseKVListener paramParseKVListener)
  {
    if ((paramObject != null) && (paramClass != null) && (paramClass != RequestParams.class) && (paramClass != Object.class))
    {
      Object localObject1 = paramClass.getClassLoader();
      if ((localObject1 != null) && (localObject1 != BOOT_CL))
      {
        localObject1 = paramClass.getDeclaredFields();
        if ((localObject1 != null) && (localObject1.length > 0))
        {
          int j = localObject1.length;
          for (int i = 0; i < j; i++)
          {
            Object localObject2 = localObject1[i];
            if ((!Modifier.isTransient(((Field)localObject2).getModifiers())) && (((Field)localObject2).getType() != Parcelable.Creator.class))
            {
              ((Field)localObject2).setAccessible(true);
              try
              {
                String str = ((Field)localObject2).getName();
                localObject2 = ((Field)localObject2).get(paramObject);
                if (localObject2 != null) {
                  paramParseKVListener.onParseKV(str, localObject2);
                }
              }
              catch (IllegalAccessException localIllegalAccessException)
              {
                LogUtil.e(localIllegalAccessException.getMessage(), localIllegalAccessException);
              }
            }
          }
        }
        parseKV(paramObject, paramClass.getSuperclass(), paramParseKVListener);
        return;
      }
      return;
    }
  }
  
  static abstract interface ParseKVListener
  {
    public abstract void onParseKV(String paramString, Object paramObject);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/RequestParamsHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */