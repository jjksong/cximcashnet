package org.xutils.http.request;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.loader.FileLoader;
import org.xutils.http.loader.Loader;

public class LocalFileRequest
  extends UriRequest
{
  private InputStream inputStream;
  
  LocalFileRequest(RequestParams paramRequestParams, Type paramType)
    throws Throwable
  {
    super(paramRequestParams, paramType);
  }
  
  private File getFile()
  {
    String str;
    if (this.queryUrl.startsWith("file:")) {
      str = this.queryUrl.substring(5);
    } else {
      str = this.queryUrl;
    }
    return new File(str);
  }
  
  public void clearCacheHeader() {}
  
  public void close()
    throws IOException
  {
    IOUtil.closeQuietly(this.inputStream);
    this.inputStream = null;
  }
  
  public String getCacheKey()
  {
    return null;
  }
  
  public long getContentLength()
  {
    return getFile().length();
  }
  
  public String getETag()
  {
    return null;
  }
  
  public long getExpiration()
  {
    return -1L;
  }
  
  public long getHeaderFieldDate(String paramString, long paramLong)
  {
    return paramLong;
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    if (this.inputStream == null) {
      this.inputStream = new FileInputStream(getFile());
    }
    return this.inputStream;
  }
  
  public long getLastModified()
  {
    return getFile().lastModified();
  }
  
  public int getResponseCode()
    throws IOException
  {
    int i;
    if (getFile().exists()) {
      i = 200;
    } else {
      i = 404;
    }
    return i;
  }
  
  public String getResponseHeader(String paramString)
  {
    return null;
  }
  
  public Map<String, List<String>> getResponseHeaders()
  {
    return null;
  }
  
  public String getResponseMessage()
    throws IOException
  {
    return null;
  }
  
  public boolean isLoading()
  {
    return true;
  }
  
  public Object loadResult()
    throws Throwable
  {
    if ((this.loader instanceof FileLoader)) {
      return getFile();
    }
    return this.loader.load(this);
  }
  
  public Object loadResultFromCache()
    throws Throwable
  {
    return null;
  }
  
  public void save2Cache() {}
  
  public void sendRequest()
    throws Throwable
  {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/request/LocalFileRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */