package org.xutils.http.request;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.loader.Loader;
import org.xutils.x;

public class AssetsRequest
  extends UriRequest
{
  private long contentLength = 0L;
  private InputStream inputStream;
  
  public AssetsRequest(RequestParams paramRequestParams, Type paramType)
    throws Throwable
  {
    super(paramRequestParams, paramType);
  }
  
  public void clearCacheHeader() {}
  
  public void close()
    throws IOException
  {
    IOUtil.closeQuietly(this.inputStream);
    this.inputStream = null;
  }
  
  protected long getAssetsLastModified()
  {
    return new File(x.app().getApplicationInfo().sourceDir).lastModified();
  }
  
  public String getCacheKey()
  {
    return this.queryUrl;
  }
  
  public long getContentLength()
  {
    try
    {
      getInputStream();
      long l = this.contentLength;
      return l;
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    return 0L;
  }
  
  public String getETag()
  {
    return null;
  }
  
  public long getExpiration()
  {
    return Long.MAX_VALUE;
  }
  
  public long getHeaderFieldDate(String paramString, long paramLong)
  {
    return paramLong;
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    if ((this.inputStream == null) && (this.callingClassLoader != null))
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("assets/");
      ((StringBuilder)localObject).append(this.queryUrl.substring(9));
      localObject = ((StringBuilder)localObject).toString();
      this.inputStream = this.callingClassLoader.getResourceAsStream((String)localObject);
      this.contentLength = this.inputStream.available();
    }
    return this.inputStream;
  }
  
  public long getLastModified()
  {
    return getAssetsLastModified();
  }
  
  public int getResponseCode()
    throws IOException
  {
    int i;
    if (getInputStream() != null) {
      i = 200;
    } else {
      i = 404;
    }
    return i;
  }
  
  public String getResponseHeader(String paramString)
  {
    return null;
  }
  
  public Map<String, List<String>> getResponseHeaders()
  {
    return null;
  }
  
  public String getResponseMessage()
    throws IOException
  {
    return null;
  }
  
  public boolean isLoading()
  {
    return true;
  }
  
  public Object loadResult()
    throws Throwable
  {
    return this.loader.load(this);
  }
  
  public Object loadResultFromCache()
    throws Throwable
  {
    DiskCacheEntity localDiskCacheEntity = LruDiskCache.getDiskCache(this.params.getCacheDirName()).setMaxSize(this.params.getCacheSize()).get(getCacheKey());
    if (localDiskCacheEntity != null)
    {
      Date localDate = localDiskCacheEntity.getLastModify();
      if ((localDate != null) && (localDate.getTime() >= getAssetsLastModified())) {
        return this.loader.loadFromCache(localDiskCacheEntity);
      }
      return null;
    }
    return null;
  }
  
  public void sendRequest()
    throws Throwable
  {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/request/AssetsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */