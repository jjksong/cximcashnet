package org.xutils.http.request;

import android.text.TextUtils;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.HashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.app.RequestTracker;

public final class UriRequestFactory
{
  private static final HashMap<String, Class<? extends UriRequest>> SCHEME_CLS_MAP = new HashMap();
  private static Class<? extends RequestTracker> defaultTrackerCls;
  
  public static RequestTracker getDefaultTracker()
  {
    RequestTracker localRequestTracker = null;
    try
    {
      if (defaultTrackerCls != null) {
        localRequestTracker = (RequestTracker)defaultTrackerCls.newInstance();
      }
      return localRequestTracker;
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    return null;
  }
  
  public static UriRequest getUriRequest(RequestParams paramRequestParams, Type paramType)
    throws Throwable
  {
    String str2 = paramRequestParams.getUri();
    int i = str2.indexOf(":");
    String str1;
    if (i > 0) {
      str1 = str2.substring(0, i);
    } else if (str2.startsWith("/")) {
      str1 = "file";
    } else {
      str1 = null;
    }
    if (!TextUtils.isEmpty(str1))
    {
      Class localClass = (Class)SCHEME_CLS_MAP.get(str1);
      if (localClass != null) {
        return (UriRequest)localClass.getConstructor(new Class[] { RequestParams.class, Class.class }).newInstance(new Object[] { paramRequestParams, paramType });
      }
      if (str1.startsWith("http")) {
        return new HttpRequest(paramRequestParams, paramType);
      }
      if (str1.equals("assets")) {
        return new AssetsRequest(paramRequestParams, paramType);
      }
      if (str1.equals("file")) {
        return new LocalFileRequest(paramRequestParams, paramType);
      }
      paramRequestParams = new StringBuilder();
      paramRequestParams.append("The url not be support: ");
      paramRequestParams.append(str2);
      throw new IllegalArgumentException(paramRequestParams.toString());
    }
    paramRequestParams = new StringBuilder();
    paramRequestParams.append("The url not be support: ");
    paramRequestParams.append(str2);
    throw new IllegalArgumentException(paramRequestParams.toString());
  }
  
  public static void registerDefaultTrackerClass(Class<? extends RequestTracker> paramClass)
  {
    defaultTrackerCls = paramClass;
  }
  
  public static void registerRequestClass(String paramString, Class<? extends UriRequest> paramClass)
  {
    SCHEME_CLS_MAP.put(paramString, paramClass);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/request/UriRequestFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */