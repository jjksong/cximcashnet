package org.xutils.http.request;

import android.net.Uri;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.http.cookie.DbCookieStore;
import org.xutils.http.loader.Loader;

public class HttpRequest
  extends UriRequest
{
  private static final CookieManager COOKIE_MANAGER = new CookieManager(DbCookieStore.INSTANCE, CookiePolicy.ACCEPT_ALL);
  private String cacheKey = null;
  private HttpURLConnection connection = null;
  private InputStream inputStream = null;
  private boolean isLoading = false;
  private int responseCode = 0;
  
  HttpRequest(RequestParams paramRequestParams, Type paramType)
    throws Throwable
  {
    super(paramRequestParams, paramType);
  }
  
  private static String toGMTString(Date paramDate)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("EEE, dd MMM y HH:mm:ss 'GMT'", Locale.US);
    TimeZone localTimeZone = TimeZone.getTimeZone("GMT");
    localSimpleDateFormat.setTimeZone(localTimeZone);
    new GregorianCalendar(localTimeZone).setTimeInMillis(paramDate.getTime());
    return localSimpleDateFormat.format(paramDate);
  }
  
  protected String buildQueryUrl(RequestParams paramRequestParams)
  {
    Object localObject1 = paramRequestParams.getUri();
    StringBuilder localStringBuilder = new StringBuilder((String)localObject1);
    if (!((String)localObject1).contains("?")) {
      localStringBuilder.append("?");
    } else if (!((String)localObject1).endsWith("?")) {
      localStringBuilder.append("&");
    }
    localObject1 = paramRequestParams.getQueryStringParams();
    if (localObject1 != null)
    {
      Iterator localIterator = ((List)localObject1).iterator();
      while (localIterator.hasNext())
      {
        Object localObject2 = (KeyValue)localIterator.next();
        localObject1 = ((KeyValue)localObject2).key;
        localObject2 = ((KeyValue)localObject2).getValueStr();
        if ((!TextUtils.isEmpty((CharSequence)localObject1)) && (localObject2 != null))
        {
          localStringBuilder.append(Uri.encode((String)localObject1, paramRequestParams.getCharset()));
          localStringBuilder.append("=");
          localStringBuilder.append(Uri.encode((String)localObject2, paramRequestParams.getCharset()));
          localStringBuilder.append("&");
        }
      }
    }
    if (localStringBuilder.charAt(localStringBuilder.length() - 1) == '&') {
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    if (localStringBuilder.charAt(localStringBuilder.length() - 1) == '?') {
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    return localStringBuilder.toString();
  }
  
  public void clearCacheHeader()
  {
    this.params.setHeader("If-Modified-Since", null);
    this.params.setHeader("If-None-Match", null);
  }
  
  public void close()
    throws IOException
  {
    Object localObject = this.inputStream;
    if (localObject != null)
    {
      IOUtil.closeQuietly((Closeable)localObject);
      this.inputStream = null;
    }
    localObject = this.connection;
    if (localObject != null) {
      ((HttpURLConnection)localObject).disconnect();
    }
  }
  
  public String getCacheKey()
  {
    if (this.cacheKey == null)
    {
      this.cacheKey = this.params.getCacheKey();
      if (TextUtils.isEmpty(this.cacheKey)) {
        this.cacheKey = this.params.toString();
      }
    }
    return this.cacheKey;
  }
  
  public long getContentLength()
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    long l1 = 0L;
    long l2;
    if (localHttpURLConnection != null)
    {
      try
      {
        i = localHttpURLConnection.getContentLength();
        l2 = i;
      }
      catch (Throwable localThrowable1)
      {
        LogUtil.e(localThrowable1.getMessage(), localThrowable1);
        l2 = l1;
      }
      l1 = l2;
      if (l2 >= 1L) {
        break label81;
      }
      l1 = l2;
    }
    try
    {
      l2 = getInputStream().available();
      l1 = l2;
    }
    catch (Throwable localThrowable2)
    {
      for (;;) {}
    }
    int i = getInputStream().available();
    l1 = i;
    label81:
    return l1;
  }
  
  public String getETag()
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    if (localHttpURLConnection == null) {
      return null;
    }
    return localHttpURLConnection.getHeaderField("ETag");
  }
  
  public long getExpiration()
  {
    Object localObject = this.connection;
    long l2 = -1L;
    if (localObject == null) {
      return -1L;
    }
    localObject = ((HttpURLConnection)localObject).getHeaderField("Cache-Control");
    long l1 = l2;
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      StringTokenizer localStringTokenizer = new StringTokenizer((String)localObject, ",");
      do
      {
        l1 = l2;
        if (!localStringTokenizer.hasMoreTokens()) {
          break;
        }
        localObject = localStringTokenizer.nextToken().trim().toLowerCase();
      } while (!((String)localObject).startsWith("max-age"));
      int i = ((String)localObject).indexOf('=');
      l1 = l2;
      if (i > 0) {
        try
        {
          long l3 = Long.parseLong(((String)localObject).substring(i + 1).trim());
          l1 = l2;
          if (l3 > 0L)
          {
            l1 = System.currentTimeMillis();
            l1 += l3 * 1000L;
          }
        }
        catch (Throwable localThrowable)
        {
          LogUtil.e(localThrowable.getMessage(), localThrowable);
          l1 = l2;
        }
      }
    }
    l2 = l1;
    if (l1 <= 0L) {
      l2 = this.connection.getExpiration();
    }
    if ((l2 <= 0L) && (this.params.getCacheMaxAge() > 0L)) {
      l1 = System.currentTimeMillis() + this.params.getCacheMaxAge();
    } else {
      l1 = l2;
    }
    l2 = l1;
    if (l1 <= 0L) {
      l2 = Long.MAX_VALUE;
    }
    return l2;
  }
  
  public long getHeaderFieldDate(String paramString, long paramLong)
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    if (localHttpURLConnection == null) {
      return paramLong;
    }
    return localHttpURLConnection.getHeaderFieldDate(paramString, paramLong);
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    Object localObject = this.connection;
    if ((localObject != null) && (this.inputStream == null))
    {
      if (((HttpURLConnection)localObject).getResponseCode() >= 400) {
        localObject = this.connection.getErrorStream();
      } else {
        localObject = this.connection.getInputStream();
      }
      this.inputStream = ((InputStream)localObject);
    }
    return this.inputStream;
  }
  
  public long getLastModified()
  {
    return getHeaderFieldDate("Last-Modified", System.currentTimeMillis());
  }
  
  public String getRequestUri()
  {
    String str2 = this.queryUrl;
    Object localObject = this.connection;
    String str1 = str2;
    if (localObject != null)
    {
      localObject = ((HttpURLConnection)localObject).getURL();
      str1 = str2;
      if (localObject != null) {
        str1 = ((URL)localObject).toString();
      }
    }
    return str1;
  }
  
  public int getResponseCode()
    throws IOException
  {
    if (this.connection != null) {
      return this.responseCode;
    }
    if (getInputStream() != null) {
      return 200;
    }
    return 404;
  }
  
  public String getResponseHeader(String paramString)
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    if (localHttpURLConnection == null) {
      return null;
    }
    return localHttpURLConnection.getHeaderField(paramString);
  }
  
  public Map<String, List<String>> getResponseHeaders()
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    if (localHttpURLConnection == null) {
      return null;
    }
    return localHttpURLConnection.getHeaderFields();
  }
  
  public String getResponseMessage()
    throws IOException
  {
    HttpURLConnection localHttpURLConnection = this.connection;
    if (localHttpURLConnection != null) {
      return URLDecoder.decode(localHttpURLConnection.getResponseMessage(), this.params.getCharset());
    }
    return null;
  }
  
  public boolean isLoading()
  {
    return this.isLoading;
  }
  
  public Object loadResult()
    throws Throwable
  {
    this.isLoading = true;
    return super.loadResult();
  }
  
  public Object loadResultFromCache()
    throws Throwable
  {
    this.isLoading = true;
    DiskCacheEntity localDiskCacheEntity = LruDiskCache.getDiskCache(this.params.getCacheDirName()).setMaxSize(this.params.getCacheSize()).get(getCacheKey());
    if (localDiskCacheEntity != null)
    {
      if (HttpMethod.permitsCache(this.params.getMethod()))
      {
        Object localObject = localDiskCacheEntity.getLastModify();
        if (((Date)localObject).getTime() > 0L) {
          this.params.setHeader("If-Modified-Since", toGMTString((Date)localObject));
        }
        localObject = localDiskCacheEntity.getEtag();
        if (!TextUtils.isEmpty((CharSequence)localObject)) {
          this.params.setHeader("If-None-Match", (String)localObject);
        }
      }
      return this.loader.loadFromCache(localDiskCacheEntity);
    }
    return null;
  }
  
  /* Error */
  @android.annotation.TargetApi(19)
  public void sendRequest()
    throws Throwable
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: putfield 48	org/xutils/http/request/HttpRequest:isLoading	Z
    //   5: aload_0
    //   6: iconst_0
    //   7: putfield 54	org/xutils/http/request/HttpRequest:responseCode	I
    //   10: new 329	java/net/URL
    //   13: dup
    //   14: aload_0
    //   15: getfield 323	org/xutils/http/request/HttpRequest:queryUrl	Ljava/lang/String;
    //   18: invokespecial 409	java/net/URL:<init>	(Ljava/lang/String;)V
    //   21: astore 5
    //   23: aload_0
    //   24: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   27: invokevirtual 413	org/xutils/http/RequestParams:getProxy	()Ljava/net/Proxy;
    //   30: astore 6
    //   32: aload 6
    //   34: ifnull +20 -> 54
    //   37: aload_0
    //   38: aload 5
    //   40: aload 6
    //   42: invokevirtual 417	java/net/URL:openConnection	(Ljava/net/Proxy;)Ljava/net/URLConnection;
    //   45: checkcast 213	java/net/HttpURLConnection
    //   48: putfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   51: goto +15 -> 66
    //   54: aload_0
    //   55: aload 5
    //   57: invokevirtual 420	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   60: checkcast 213	java/net/HttpURLConnection
    //   63: putfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   66: getstatic 425	android/os/Build$VERSION:SDK_INT	I
    //   69: bipush 19
    //   71: if_icmpge +16 -> 87
    //   74: aload_0
    //   75: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   78: ldc_w 427
    //   81: ldc_w 428
    //   84: invokevirtual 431	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   87: aload_0
    //   88: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   91: aload_0
    //   92: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   95: invokevirtual 434	org/xutils/http/RequestParams:getReadTimeout	()I
    //   98: invokevirtual 438	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   101: aload_0
    //   102: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   105: aload_0
    //   106: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   109: invokevirtual 441	org/xutils/http/RequestParams:getConnectTimeout	()I
    //   112: invokevirtual 444	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   115: aload_0
    //   116: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   119: astore 6
    //   121: aload_0
    //   122: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   125: invokevirtual 448	org/xutils/http/RequestParams:getRedirectHandler	()Lorg/xutils/http/app/RedirectHandler;
    //   128: ifnonnull +8 -> 136
    //   131: iconst_1
    //   132: istore_2
    //   133: goto +5 -> 138
    //   136: iconst_0
    //   137: istore_2
    //   138: aload 6
    //   140: iload_2
    //   141: invokevirtual 452	java/net/HttpURLConnection:setInstanceFollowRedirects	(Z)V
    //   144: aload_0
    //   145: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   148: instanceof 454
    //   151: ifeq +29 -> 180
    //   154: aload_0
    //   155: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   158: invokevirtual 458	org/xutils/http/RequestParams:getSslSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   161: astore 6
    //   163: aload 6
    //   165: ifnull +15 -> 180
    //   168: aload_0
    //   169: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   172: checkcast 454	javax/net/ssl/HttpsURLConnection
    //   175: aload 6
    //   177: invokevirtual 462	javax/net/ssl/HttpsURLConnection:setSSLSocketFactory	(Ljavax/net/ssl/SSLSocketFactory;)V
    //   180: aload_0
    //   181: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   184: invokevirtual 465	org/xutils/http/RequestParams:isUseCookie	()Z
    //   187: ifeq +86 -> 273
    //   190: getstatic 38	org/xutils/http/request/HttpRequest:COOKIE_MANAGER	Ljava/net/CookieManager;
    //   193: astore 7
    //   195: aload 5
    //   197: invokevirtual 469	java/net/URL:toURI	()Ljava/net/URI;
    //   200: astore 6
    //   202: new 471	java/util/HashMap
    //   205: astore 8
    //   207: aload 8
    //   209: iconst_0
    //   210: invokespecial 473	java/util/HashMap:<init>	(I)V
    //   213: aload 7
    //   215: aload 6
    //   217: aload 8
    //   219: invokevirtual 476	java/net/CookieManager:get	(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;
    //   222: ldc_w 478
    //   225: invokeinterface 483 2 0
    //   230: checkcast 136	java/util/List
    //   233: astore 6
    //   235: aload 6
    //   237: ifnull +36 -> 273
    //   240: aload_0
    //   241: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   244: ldc_w 478
    //   247: ldc_w 485
    //   250: aload 6
    //   252: invokestatic 489	android/text/TextUtils:join	(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   255: invokevirtual 431	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   258: goto +15 -> 273
    //   261: astore 6
    //   263: aload 6
    //   265: invokevirtual 226	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   268: aload 6
    //   270: invokestatic 232	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   273: aload_0
    //   274: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   277: invokevirtual 492	org/xutils/http/RequestParams:getHeaders	()Ljava/util/List;
    //   280: astore 6
    //   282: aload 6
    //   284: ifnull +100 -> 384
    //   287: aload 6
    //   289: invokeinterface 140 1 0
    //   294: astore 7
    //   296: aload 7
    //   298: invokeinterface 146 1 0
    //   303: ifeq +81 -> 384
    //   306: aload 7
    //   308: invokeinterface 150 1 0
    //   313: checkcast 494	org/xutils/http/BaseParams$Header
    //   316: astore 9
    //   318: aload 9
    //   320: getfield 495	org/xutils/http/BaseParams$Header:key	Ljava/lang/String;
    //   323: astore 8
    //   325: aload 9
    //   327: invokevirtual 496	org/xutils/http/BaseParams$Header:getValueStr	()Ljava/lang/String;
    //   330: astore 6
    //   332: aload 8
    //   334: invokestatic 163	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   337: ifne -41 -> 296
    //   340: aload 6
    //   342: invokestatic 163	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   345: ifne -49 -> 296
    //   348: aload 9
    //   350: getfield 498	org/xutils/http/BaseParams$Header:setHeader	Z
    //   353: ifeq +17 -> 370
    //   356: aload_0
    //   357: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   360: aload 8
    //   362: aload 6
    //   364: invokevirtual 431	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   367: goto -71 -> 296
    //   370: aload_0
    //   371: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   374: aload 8
    //   376: aload 6
    //   378: invokevirtual 501	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   381: goto -85 -> 296
    //   384: aload_0
    //   385: getfield 505	org/xutils/http/request/HttpRequest:requestInterceptListener	Lorg/xutils/http/app/RequestInterceptListener;
    //   388: ifnull +13 -> 401
    //   391: aload_0
    //   392: getfield 505	org/xutils/http/request/HttpRequest:requestInterceptListener	Lorg/xutils/http/app/RequestInterceptListener;
    //   395: aload_0
    //   396: invokeinterface 511 2 0
    //   401: aload_0
    //   402: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   405: invokevirtual 375	org/xutils/http/RequestParams:getMethod	()Lorg/xutils/http/HttpMethod;
    //   408: astore 8
    //   410: aload_0
    //   411: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   414: aload 8
    //   416: invokevirtual 512	org/xutils/http/HttpMethod:toString	()Ljava/lang/String;
    //   419: invokevirtual 515	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   422: goto +35 -> 457
    //   425: astore 6
    //   427: ldc -43
    //   429: ldc_w 517
    //   432: invokevirtual 523	java/lang/Class:getDeclaredField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   435: astore 7
    //   437: aload 7
    //   439: iconst_1
    //   440: invokevirtual 528	java/lang/reflect/Field:setAccessible	(Z)V
    //   443: aload 7
    //   445: aload_0
    //   446: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   449: aload 8
    //   451: invokevirtual 512	org/xutils/http/HttpMethod:toString	()Ljava/lang/String;
    //   454: invokevirtual 532	java/lang/reflect/Field:set	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   457: aload 8
    //   459: invokestatic 535	org/xutils/http/HttpMethod:permitsRequestBody	(Lorg/xutils/http/HttpMethod;)Z
    //   462: ifeq +180 -> 642
    //   465: aload_0
    //   466: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   469: invokevirtual 539	org/xutils/http/RequestParams:getRequestBody	()Lorg/xutils/http/body/RequestBody;
    //   472: astore 6
    //   474: aload 6
    //   476: ifnull +166 -> 642
    //   479: aload 6
    //   481: instanceof 541
    //   484: ifeq +17 -> 501
    //   487: aload 6
    //   489: checkcast 541	org/xutils/http/body/ProgressBody
    //   492: aload_0
    //   493: getfield 545	org/xutils/http/request/HttpRequest:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   496: invokeinterface 549 2 0
    //   501: aload 6
    //   503: invokeinterface 554 1 0
    //   508: astore 7
    //   510: aload 7
    //   512: invokestatic 163	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   515: ifne +15 -> 530
    //   518: aload_0
    //   519: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   522: ldc_w 556
    //   525: aload 7
    //   527: invokevirtual 431	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   530: aload 6
    //   532: invokeinterface 558 1 0
    //   537: lstore_3
    //   538: lload_3
    //   539: lconst_0
    //   540: lcmp
    //   541: ifge +16 -> 557
    //   544: aload_0
    //   545: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   548: ldc_w 559
    //   551: invokevirtual 562	java/net/HttpURLConnection:setChunkedStreamingMode	(I)V
    //   554: goto +52 -> 606
    //   557: lload_3
    //   558: ldc2_w 563
    //   561: lcmp
    //   562: ifge +15 -> 577
    //   565: aload_0
    //   566: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   569: lload_3
    //   570: l2i
    //   571: invokevirtual 567	java/net/HttpURLConnection:setFixedLengthStreamingMode	(I)V
    //   574: goto +32 -> 606
    //   577: getstatic 425	android/os/Build$VERSION:SDK_INT	I
    //   580: bipush 19
    //   582: if_icmplt +14 -> 596
    //   585: aload_0
    //   586: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   589: lload_3
    //   590: invokevirtual 569	java/net/HttpURLConnection:setFixedLengthStreamingMode	(J)V
    //   593: goto +13 -> 606
    //   596: aload_0
    //   597: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   600: ldc_w 559
    //   603: invokevirtual 562	java/net/HttpURLConnection:setChunkedStreamingMode	(I)V
    //   606: aload_0
    //   607: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   610: ldc_w 571
    //   613: lload_3
    //   614: invokestatic 575	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   617: invokevirtual 431	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   620: aload_0
    //   621: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   624: iconst_1
    //   625: invokevirtual 578	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   628: aload 6
    //   630: aload_0
    //   631: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   634: invokevirtual 582	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   637: invokeinterface 586 2 0
    //   642: aload_0
    //   643: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   646: invokevirtual 465	org/xutils/http/RequestParams:isUseCookie	()Z
    //   649: ifeq +45 -> 694
    //   652: aload_0
    //   653: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   656: invokevirtual 336	java/net/HttpURLConnection:getHeaderFields	()Ljava/util/Map;
    //   659: astore 6
    //   661: aload 6
    //   663: ifnull +31 -> 694
    //   666: getstatic 38	org/xutils/http/request/HttpRequest:COOKIE_MANAGER	Ljava/net/CookieManager;
    //   669: aload 5
    //   671: invokevirtual 469	java/net/URL:toURI	()Ljava/net/URI;
    //   674: aload 6
    //   676: invokevirtual 590	java/net/CookieManager:put	(Ljava/net/URI;Ljava/util/Map;)V
    //   679: goto +15 -> 694
    //   682: astore 5
    //   684: aload 5
    //   686: invokevirtual 226	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   689: aload 5
    //   691: invokestatic 232	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   694: aload_0
    //   695: aload_0
    //   696: getfield 52	org/xutils/http/request/HttpRequest:connection	Ljava/net/HttpURLConnection;
    //   699: invokevirtual 311	java/net/HttpURLConnection:getResponseCode	()I
    //   702: putfield 54	org/xutils/http/request/HttpRequest:responseCode	I
    //   705: aload_0
    //   706: getfield 505	org/xutils/http/request/HttpRequest:requestInterceptListener	Lorg/xutils/http/app/RequestInterceptListener;
    //   709: ifnull +13 -> 722
    //   712: aload_0
    //   713: getfield 505	org/xutils/http/request/HttpRequest:requestInterceptListener	Lorg/xutils/http/app/RequestInterceptListener;
    //   716: aload_0
    //   717: invokeinterface 593 2 0
    //   722: aload_0
    //   723: getfield 54	org/xutils/http/request/HttpRequest:responseCode	I
    //   726: istore_1
    //   727: iload_1
    //   728: sipush 204
    //   731: if_icmpeq +106 -> 837
    //   734: iload_1
    //   735: sipush 205
    //   738: if_icmpeq +99 -> 837
    //   741: iload_1
    //   742: sipush 300
    //   745: if_icmpge +9 -> 754
    //   748: aload_0
    //   749: iconst_1
    //   750: putfield 48	org/xutils/http/request/HttpRequest:isLoading	Z
    //   753: return
    //   754: new 595	org/xutils/ex/HttpException
    //   757: dup
    //   758: iload_1
    //   759: aload_0
    //   760: invokevirtual 596	org/xutils/http/request/HttpRequest:getResponseMessage	()Ljava/lang/String;
    //   763: invokespecial 599	org/xutils/ex/HttpException:<init>	(ILjava/lang/String;)V
    //   766: astore 5
    //   768: aload 5
    //   770: aload_0
    //   771: invokevirtual 236	org/xutils/http/request/HttpRequest:getInputStream	()Ljava/io/InputStream;
    //   774: aload_0
    //   775: getfield 194	org/xutils/http/request/HttpRequest:params	Lorg/xutils/http/RequestParams;
    //   778: invokevirtual 166	org/xutils/http/RequestParams:getCharset	()Ljava/lang/String;
    //   781: invokestatic 603	org/xutils/common/util/IOUtil:readStr	(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    //   784: invokevirtual 606	org/xutils/ex/HttpException:setResult	(Ljava/lang/String;)V
    //   787: new 109	java/lang/StringBuilder
    //   790: dup
    //   791: invokespecial 608	java/lang/StringBuilder:<init>	()V
    //   794: astore 6
    //   796: aload 6
    //   798: aload 5
    //   800: invokevirtual 609	org/xutils/ex/HttpException:toString	()Ljava/lang/String;
    //   803: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   806: pop
    //   807: aload 6
    //   809: ldc_w 611
    //   812: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   815: pop
    //   816: aload 6
    //   818: aload_0
    //   819: getfield 323	org/xutils/http/request/HttpRequest:queryUrl	Ljava/lang/String;
    //   822: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   825: pop
    //   826: aload 6
    //   828: invokevirtual 189	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   831: invokestatic 613	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;)V
    //   834: aload 5
    //   836: athrow
    //   837: new 595	org/xutils/ex/HttpException
    //   840: dup
    //   841: aload_0
    //   842: getfield 54	org/xutils/http/request/HttpRequest:responseCode	I
    //   845: aload_0
    //   846: invokevirtual 596	org/xutils/http/request/HttpRequest:getResponseMessage	()Ljava/lang/String;
    //   849: invokespecial 599	org/xutils/ex/HttpException:<init>	(ILjava/lang/String;)V
    //   852: athrow
    //   853: astore 5
    //   855: aload 6
    //   857: athrow
    //   858: astore 6
    //   860: goto -73 -> 787
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	863	0	this	HttpRequest
    //   726	33	1	i	int
    //   132	9	2	bool	boolean
    //   537	77	3	l	long
    //   21	649	5	localURL	URL
    //   682	8	5	localThrowable1	Throwable
    //   766	69	5	localHttpException	org.xutils.ex.HttpException
    //   853	1	5	localThrowable2	Throwable
    //   30	221	6	localObject1	Object
    //   261	8	6	localThrowable3	Throwable
    //   280	97	6	localObject2	Object
    //   425	1	6	localProtocolException	java.net.ProtocolException
    //   472	384	6	localObject3	Object
    //   858	1	6	localThrowable4	Throwable
    //   193	333	7	localObject4	Object
    //   205	253	8	localObject5	Object
    //   316	33	9	localHeader	org.xutils.http.BaseParams.Header
    // Exception table:
    //   from	to	target	type
    //   190	235	261	java/lang/Throwable
    //   240	258	261	java/lang/Throwable
    //   410	422	425	java/net/ProtocolException
    //   652	661	682	java/lang/Throwable
    //   666	679	682	java/lang/Throwable
    //   427	457	853	java/lang/Throwable
    //   768	787	858	java/lang/Throwable
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/request/HttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */