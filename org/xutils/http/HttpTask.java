package org.xutils.http;

import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.common.Callback.CacheCallback;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.common.Callback.PrepareCallback;
import org.xutils.common.Callback.ProgressCallback;
import org.xutils.common.Callback.TypedCallback;
import org.xutils.common.TaskController;
import org.xutils.common.task.AbsTask;
import org.xutils.common.task.Priority;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.ParameterizedTypeUtil;
import org.xutils.http.app.RequestInterceptListener;
import org.xutils.http.app.RequestTracker;
import org.xutils.http.request.UriRequest;
import org.xutils.http.request.UriRequestFactory;
import org.xutils.x;

public class HttpTask<ResultType>
  extends AbsTask<ResultType>
  implements ProgressHandler
{
  private static final PriorityExecutor CACHE_EXECUTOR = new PriorityExecutor(5, true);
  private static final HashMap<String, WeakReference<HttpTask<?>>> DOWNLOAD_TASK;
  private static final int FLAG_CACHE = 2;
  private static final int FLAG_PROGRESS = 3;
  private static final int FLAG_REQUEST_CREATED = 1;
  private static final PriorityExecutor HTTP_EXECUTOR;
  private static final int MAX_FILE_LOAD_WORKER = 3;
  private static final AtomicInteger sCurrFileLoadCount = new AtomicInteger(0);
  private Callback.CacheCallback<ResultType> cacheCallback;
  private final Object cacheLock = new Object();
  private final Callback.CommonCallback<ResultType> callback;
  private final Executor executor;
  private volatile boolean hasException = false;
  private long lastUpdateTime;
  private Type loadType;
  private long loadingUpdateMaxTimeSpan = 300L;
  private RequestParams params;
  private Callback.PrepareCallback prepareCallback;
  private Callback.ProgressCallback progressCallback;
  private Object rawResult = null;
  private UriRequest request;
  private RequestInterceptListener requestInterceptListener;
  private HttpTask<ResultType>.RequestWorker requestWorker;
  private RequestTracker tracker;
  private volatile Boolean trustCache = null;
  
  static
  {
    DOWNLOAD_TASK = new HashMap(1);
    HTTP_EXECUTOR = new PriorityExecutor(5, true);
  }
  
  public HttpTask(RequestParams paramRequestParams, Callback.Cancelable paramCancelable, Callback.CommonCallback<ResultType> paramCommonCallback)
  {
    super(paramCancelable);
    this.params = paramRequestParams;
    this.callback = paramCommonCallback;
    if ((paramCommonCallback instanceof Callback.CacheCallback)) {
      this.cacheCallback = ((Callback.CacheCallback)paramCommonCallback);
    }
    if ((paramCommonCallback instanceof Callback.PrepareCallback)) {
      this.prepareCallback = ((Callback.PrepareCallback)paramCommonCallback);
    }
    if ((paramCommonCallback instanceof Callback.ProgressCallback)) {
      this.progressCallback = ((Callback.ProgressCallback)paramCommonCallback);
    }
    if ((paramCommonCallback instanceof RequestInterceptListener)) {
      this.requestInterceptListener = ((RequestInterceptListener)paramCommonCallback);
    }
    RequestTracker localRequestTracker = paramRequestParams.getRequestTracker();
    paramCancelable = localRequestTracker;
    if (localRequestTracker == null) {
      if ((paramCommonCallback instanceof RequestTracker)) {
        paramCancelable = (RequestTracker)paramCommonCallback;
      } else {
        paramCancelable = UriRequestFactory.getDefaultTracker();
      }
    }
    if (paramCancelable != null) {
      this.tracker = new RequestTrackerWrapper(paramCancelable);
    }
    if (paramRequestParams.getExecutor() != null) {
      this.executor = paramRequestParams.getExecutor();
    } else if (this.cacheCallback != null) {
      this.executor = CACHE_EXECUTOR;
    } else {
      this.executor = HTTP_EXECUTOR;
    }
  }
  
  private void checkDownloadTask()
  {
    if (File.class == this.loadType) {
      synchronized (DOWNLOAD_TASK)
      {
        Object localObject1 = this.params.getSaveFilePath();
        Object localObject3;
        if (!TextUtils.isEmpty((CharSequence)localObject1))
        {
          localObject3 = (WeakReference)DOWNLOAD_TASK.get(localObject1);
          if (localObject3 != null)
          {
            localObject3 = (HttpTask)((WeakReference)localObject3).get();
            if (localObject3 != null)
            {
              ((HttpTask)localObject3).cancel();
              ((HttpTask)localObject3).closeRequestSync();
            }
            DOWNLOAD_TASK.remove(localObject1);
          }
          localObject3 = DOWNLOAD_TASK;
          WeakReference localWeakReference = new java/lang/ref/WeakReference;
          localWeakReference.<init>(this);
          ((HashMap)localObject3).put(localObject1, localWeakReference);
        }
        if (DOWNLOAD_TASK.size() > 3)
        {
          localObject1 = DOWNLOAD_TASK.entrySet().iterator();
          while (((Iterator)localObject1).hasNext())
          {
            localObject3 = (WeakReference)((Map.Entry)((Iterator)localObject1).next()).getValue();
            if ((localObject3 == null) || (((WeakReference)localObject3).get() == null)) {
              ((Iterator)localObject1).remove();
            }
          }
        }
      }
    }
  }
  
  private void clearRawResult()
  {
    Object localObject = this.rawResult;
    if ((localObject instanceof Closeable)) {
      IOUtil.closeQuietly((Closeable)localObject);
    }
    this.rawResult = null;
  }
  
  private void closeRequestSync()
  {
    clearRawResult();
    IOUtil.closeQuietly(this.request);
  }
  
  private UriRequest createNewRequest()
    throws Throwable
  {
    this.params.init();
    UriRequest localUriRequest = UriRequestFactory.getUriRequest(this.params, this.loadType);
    localUriRequest.setCallingClassLoader(this.callback.getClass().getClassLoader());
    localUriRequest.setProgressHandler(this);
    this.loadingUpdateMaxTimeSpan = this.params.getLoadingUpdateMaxTimeSpan();
    update(1, new Object[] { localUriRequest });
    return localUriRequest;
  }
  
  private void resolveLoadType()
  {
    Class localClass = this.callback.getClass();
    Callback.CommonCallback localCommonCallback = this.callback;
    if ((localCommonCallback instanceof Callback.TypedCallback)) {
      this.loadType = ((Callback.TypedCallback)localCommonCallback).getLoadType();
    } else if ((localCommonCallback instanceof Callback.PrepareCallback)) {
      this.loadType = ParameterizedTypeUtil.getParameterizedType(localClass, Callback.PrepareCallback.class, 0);
    } else {
      this.loadType = ParameterizedTypeUtil.getParameterizedType(localClass, Callback.CommonCallback.class, 0);
    }
  }
  
  protected void cancelWorks()
  {
    x.task().run(new Runnable()
    {
      public void run()
      {
        HttpTask.this.closeRequestSync();
      }
    });
  }
  
  /* Error */
  protected ResultType doBackground()
    throws Throwable
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   4: ifne +1205 -> 1209
    //   7: aload_0
    //   8: invokespecial 341	org/xutils/http/HttpTask:resolveLoadType	()V
    //   11: aload_0
    //   12: aload_0
    //   13: invokespecial 190	org/xutils/http/HttpTask:createNewRequest	()Lorg/xutils/http/request/UriRequest;
    //   16: putfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   19: aload_0
    //   20: invokespecial 343	org/xutils/http/HttpTask:checkDownloadTask	()V
    //   23: aload_0
    //   24: getfield 114	org/xutils/http/HttpTask:params	Lorg/xutils/http/RequestParams;
    //   27: invokevirtual 347	org/xutils/http/RequestParams:getHttpRetryHandler	()Lorg/xutils/http/app/HttpRetryHandler;
    //   30: astore 4
    //   32: aload 4
    //   34: astore 7
    //   36: aload 4
    //   38: ifnonnull +12 -> 50
    //   41: new 349	org/xutils/http/app/HttpRetryHandler
    //   44: dup
    //   45: invokespecial 350	org/xutils/http/app/HttpRetryHandler:<init>	()V
    //   48: astore 7
    //   50: aload 7
    //   52: aload_0
    //   53: getfield 114	org/xutils/http/HttpTask:params	Lorg/xutils/http/RequestParams;
    //   56: invokevirtual 353	org/xutils/http/RequestParams:getMaxRetryCount	()I
    //   59: invokevirtual 356	org/xutils/http/app/HttpRetryHandler:setMaxRetryCount	(I)V
    //   62: aload_0
    //   63: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   66: ifne +1132 -> 1198
    //   69: aload_0
    //   70: getfield 120	org/xutils/http/HttpTask:cacheCallback	Lorg/xutils/common/Callback$CacheCallback;
    //   73: ifnull +295 -> 368
    //   76: aload_0
    //   77: getfield 114	org/xutils/http/HttpTask:params	Lorg/xutils/http/RequestParams;
    //   80: invokevirtual 360	org/xutils/http/RequestParams:getMethod	()Lorg/xutils/http/HttpMethod;
    //   83: invokestatic 366	org/xutils/http/HttpMethod:permitsCache	(Lorg/xutils/http/HttpMethod;)Z
    //   86: ifeq +282 -> 368
    //   89: aload_0
    //   90: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   93: new 368	java/lang/StringBuilder
    //   96: astore 4
    //   98: aload 4
    //   100: invokespecial 369	java/lang/StringBuilder:<init>	()V
    //   103: aload 4
    //   105: ldc_w 371
    //   108: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload 4
    //   114: aload_0
    //   115: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   118: invokevirtual 378	org/xutils/http/request/UriRequest:getRequestUri	()Ljava/lang/String;
    //   121: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: pop
    //   125: aload 4
    //   127: invokevirtual 381	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   130: invokestatic 387	org/xutils/common/util/LogUtil:d	(Ljava/lang/String;)V
    //   133: aload_0
    //   134: aload_0
    //   135: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   138: invokevirtual 390	org/xutils/http/request/UriRequest:loadResultFromCache	()Ljava/lang/Object;
    //   141: putfield 100	org/xutils/http/HttpTask:rawResult	Ljava/lang/Object;
    //   144: goto +13 -> 157
    //   147: astore 4
    //   149: ldc_w 392
    //   152: aload 4
    //   154: invokestatic 396	org/xutils/common/util/LogUtil:w	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   157: aload_0
    //   158: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   161: ifne +192 -> 353
    //   164: aload_0
    //   165: getfield 100	org/xutils/http/HttpTask:rawResult	Ljava/lang/Object;
    //   168: astore 5
    //   170: aload 5
    //   172: ifnull +196 -> 368
    //   175: aload_0
    //   176: getfield 124	org/xutils/http/HttpTask:prepareCallback	Lorg/xutils/common/Callback$PrepareCallback;
    //   179: astore 6
    //   181: aload 5
    //   183: astore 4
    //   185: aload 6
    //   187: ifnull +50 -> 237
    //   190: aload 6
    //   192: aload 5
    //   194: invokeinterface 399 2 0
    //   199: astore 4
    //   201: aload_0
    //   202: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   205: goto +32 -> 237
    //   208: astore 4
    //   210: ldc_w 401
    //   213: aload 4
    //   215: invokestatic 396	org/xutils/common/util/LogUtil:w	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   218: aload_0
    //   219: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   222: aconst_null
    //   223: astore 4
    //   225: goto +12 -> 237
    //   228: astore 4
    //   230: aload_0
    //   231: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   234: aload 4
    //   236: athrow
    //   237: aload_0
    //   238: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   241: ifne +101 -> 342
    //   244: aload 4
    //   246: astore 5
    //   248: aload 4
    //   250: ifnull +121 -> 371
    //   253: aload_0
    //   254: iconst_2
    //   255: iconst_1
    //   256: anewarray 104	java/lang/Object
    //   259: dup
    //   260: iconst_0
    //   261: aload 4
    //   263: aastore
    //   264: invokevirtual 301	org/xutils/http/HttpTask:update	(I[Ljava/lang/Object;)V
    //   267: aload_0
    //   268: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   271: astore 5
    //   273: aload 5
    //   275: monitorenter
    //   276: aload_0
    //   277: getfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   280: astore 6
    //   282: aload 6
    //   284: ifnonnull +31 -> 315
    //   287: aload_0
    //   288: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   291: invokevirtual 404	java/lang/Object:wait	()V
    //   294: goto -18 -> 276
    //   297: astore 4
    //   299: new 406	org/xutils/common/Callback$CancelledException
    //   302: astore 4
    //   304: aload 4
    //   306: ldc_w 408
    //   309: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   312: aload 4
    //   314: athrow
    //   315: aload 5
    //   317: monitorexit
    //   318: aload 4
    //   320: astore 5
    //   322: aload_0
    //   323: getfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   326: invokevirtual 415	java/lang/Boolean:booleanValue	()Z
    //   329: ifeq +42 -> 371
    //   332: aconst_null
    //   333: areturn
    //   334: astore 4
    //   336: aload 5
    //   338: monitorexit
    //   339: aload 4
    //   341: athrow
    //   342: new 406	org/xutils/common/Callback$CancelledException
    //   345: dup
    //   346: ldc_w 408
    //   349: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   352: athrow
    //   353: aload_0
    //   354: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   357: new 406	org/xutils/common/Callback$CancelledException
    //   360: dup
    //   361: ldc_w 408
    //   364: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   367: athrow
    //   368: aconst_null
    //   369: astore 5
    //   371: aload_0
    //   372: getfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   375: ifnonnull +11 -> 386
    //   378: aload_0
    //   379: iconst_0
    //   380: invokestatic 419	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   383: putfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   386: aload 5
    //   388: ifnonnull +10 -> 398
    //   391: aload_0
    //   392: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   395: invokevirtual 422	org/xutils/http/request/UriRequest:clearCacheHeader	()V
    //   398: aload_0
    //   399: getfield 116	org/xutils/http/HttpTask:callback	Lorg/xutils/common/Callback$CommonCallback;
    //   402: astore 4
    //   404: aload 4
    //   406: instanceof 424
    //   409: ifeq +18 -> 427
    //   412: aload 4
    //   414: checkcast 424	org/xutils/common/Callback$ProxyCacheCallback
    //   417: invokeinterface 427 1 0
    //   422: ifeq +5 -> 427
    //   425: aconst_null
    //   426: areturn
    //   427: aconst_null
    //   428: astore 8
    //   430: aload 8
    //   432: astore 4
    //   434: iconst_1
    //   435: istore_3
    //   436: iconst_0
    //   437: istore_1
    //   438: iload_3
    //   439: ifeq +725 -> 1164
    //   442: aload 4
    //   444: astore 6
    //   446: aload 4
    //   448: astore 5
    //   450: aload_0
    //   451: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   454: ifne +514 -> 968
    //   457: aload 4
    //   459: astore 6
    //   461: aload 4
    //   463: astore 5
    //   465: aload_0
    //   466: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   469: invokevirtual 430	org/xutils/http/request/UriRequest:close	()V
    //   472: aload 4
    //   474: astore 5
    //   476: aload_0
    //   477: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   480: aload 4
    //   482: astore 5
    //   484: new 368	java/lang/StringBuilder
    //   487: astore 6
    //   489: aload 4
    //   491: astore 5
    //   493: aload 6
    //   495: invokespecial 369	java/lang/StringBuilder:<init>	()V
    //   498: aload 4
    //   500: astore 5
    //   502: aload 6
    //   504: ldc_w 432
    //   507: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   510: pop
    //   511: aload 4
    //   513: astore 5
    //   515: aload 6
    //   517: aload_0
    //   518: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   521: invokevirtual 378	org/xutils/http/request/UriRequest:getRequestUri	()Ljava/lang/String;
    //   524: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   527: pop
    //   528: aload 4
    //   530: astore 5
    //   532: aload 6
    //   534: invokevirtual 381	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   537: invokestatic 387	org/xutils/common/util/LogUtil:d	(Ljava/lang/String;)V
    //   540: aload 4
    //   542: astore 5
    //   544: new 13	org/xutils/http/HttpTask$RequestWorker
    //   547: astore 6
    //   549: aload 4
    //   551: astore 5
    //   553: aload 6
    //   555: aload_0
    //   556: aconst_null
    //   557: invokespecial 435	org/xutils/http/HttpTask$RequestWorker:<init>	(Lorg/xutils/http/HttpTask;Lorg/xutils/http/HttpTask$1;)V
    //   560: aload 4
    //   562: astore 5
    //   564: aload_0
    //   565: aload 6
    //   567: putfield 437	org/xutils/http/HttpTask:requestWorker	Lorg/xutils/http/HttpTask$RequestWorker;
    //   570: aload 4
    //   572: astore 5
    //   574: aload_0
    //   575: getfield 437	org/xutils/http/HttpTask:requestWorker	Lorg/xutils/http/HttpTask$RequestWorker;
    //   578: invokevirtual 439	org/xutils/http/HttpTask$RequestWorker:request	()V
    //   581: aload 4
    //   583: astore 5
    //   585: aload_0
    //   586: getfield 437	org/xutils/http/HttpTask:requestWorker	Lorg/xutils/http/HttpTask$RequestWorker;
    //   589: getfield 443	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
    //   592: ifnonnull +284 -> 876
    //   595: aload 4
    //   597: astore 5
    //   599: aload_0
    //   600: aload_0
    //   601: getfield 437	org/xutils/http/HttpTask:requestWorker	Lorg/xutils/http/HttpTask$RequestWorker;
    //   604: getfield 446	org/xutils/http/HttpTask$RequestWorker:result	Ljava/lang/Object;
    //   607: putfield 100	org/xutils/http/HttpTask:rawResult	Ljava/lang/Object;
    //   610: aload 4
    //   612: astore 6
    //   614: aload 4
    //   616: astore 5
    //   618: aload_0
    //   619: getfield 124	org/xutils/http/HttpTask:prepareCallback	Lorg/xutils/common/Callback$PrepareCallback;
    //   622: ifnull +129 -> 751
    //   625: aload 4
    //   627: astore 6
    //   629: aload 4
    //   631: astore 5
    //   633: aload_0
    //   634: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   637: istore_3
    //   638: iload_3
    //   639: ifne +72 -> 711
    //   642: aload_0
    //   643: getfield 124	org/xutils/http/HttpTask:prepareCallback	Lorg/xutils/common/Callback$PrepareCallback;
    //   646: aload_0
    //   647: getfield 100	org/xutils/http/HttpTask:rawResult	Ljava/lang/Object;
    //   650: invokeinterface 399 2 0
    //   655: astore 5
    //   657: aload_0
    //   658: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   661: aload 5
    //   663: astore 4
    //   665: goto +100 -> 765
    //   668: astore 6
    //   670: aload 5
    //   672: astore 4
    //   674: goto +344 -> 1018
    //   677: astore 4
    //   679: aload 5
    //   681: astore 4
    //   683: goto +437 -> 1120
    //   686: astore 9
    //   688: aload 4
    //   690: astore 6
    //   692: aload 4
    //   694: astore 5
    //   696: aload_0
    //   697: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   700: aload 4
    //   702: astore 6
    //   704: aload 4
    //   706: astore 5
    //   708: aload 9
    //   710: athrow
    //   711: aload 4
    //   713: astore 6
    //   715: aload 4
    //   717: astore 5
    //   719: new 406	org/xutils/common/Callback$CancelledException
    //   722: astore 9
    //   724: aload 4
    //   726: astore 6
    //   728: aload 4
    //   730: astore 5
    //   732: aload 9
    //   734: ldc_w 408
    //   737: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   740: aload 4
    //   742: astore 6
    //   744: aload 4
    //   746: astore 5
    //   748: aload 9
    //   750: athrow
    //   751: aload 4
    //   753: astore 6
    //   755: aload 4
    //   757: astore 5
    //   759: aload_0
    //   760: getfield 100	org/xutils/http/HttpTask:rawResult	Ljava/lang/Object;
    //   763: astore 4
    //   765: aload 4
    //   767: astore 6
    //   769: aload 4
    //   771: astore 5
    //   773: aload_0
    //   774: getfield 120	org/xutils/http/HttpTask:cacheCallback	Lorg/xutils/common/Callback$CacheCallback;
    //   777: ifnull +39 -> 816
    //   780: aload 4
    //   782: astore 6
    //   784: aload 4
    //   786: astore 5
    //   788: aload_0
    //   789: getfield 114	org/xutils/http/HttpTask:params	Lorg/xutils/http/RequestParams;
    //   792: invokevirtual 360	org/xutils/http/RequestParams:getMethod	()Lorg/xutils/http/HttpMethod;
    //   795: invokestatic 366	org/xutils/http/HttpMethod:permitsCache	(Lorg/xutils/http/HttpMethod;)Z
    //   798: ifeq +18 -> 816
    //   801: aload 4
    //   803: astore 6
    //   805: aload 4
    //   807: astore 5
    //   809: aload_0
    //   810: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   813: invokevirtual 449	org/xutils/http/request/UriRequest:save2Cache	()V
    //   816: aload 4
    //   818: astore 6
    //   820: aload 4
    //   822: astore 5
    //   824: aload_0
    //   825: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   828: ifne +8 -> 836
    //   831: iconst_0
    //   832: istore_3
    //   833: goto -395 -> 438
    //   836: aload 4
    //   838: astore 6
    //   840: aload 4
    //   842: astore 5
    //   844: new 406	org/xutils/common/Callback$CancelledException
    //   847: astore 9
    //   849: aload 4
    //   851: astore 6
    //   853: aload 4
    //   855: astore 5
    //   857: aload 9
    //   859: ldc_w 451
    //   862: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   865: aload 4
    //   867: astore 6
    //   869: aload 4
    //   871: astore 5
    //   873: aload 9
    //   875: athrow
    //   876: aload 4
    //   878: astore 5
    //   880: aload_0
    //   881: getfield 437	org/xutils/http/HttpTask:requestWorker	Lorg/xutils/http/HttpTask$RequestWorker;
    //   884: getfield 443	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
    //   887: athrow
    //   888: astore 9
    //   890: aload 4
    //   892: astore 6
    //   894: aload 4
    //   896: astore 5
    //   898: aload_0
    //   899: invokespecial 267	org/xutils/http/HttpTask:clearRawResult	()V
    //   902: aload 4
    //   904: astore 6
    //   906: aload 4
    //   908: astore 5
    //   910: aload_0
    //   911: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   914: ifeq +43 -> 957
    //   917: aload 4
    //   919: astore 6
    //   921: aload 4
    //   923: astore 5
    //   925: new 406	org/xutils/common/Callback$CancelledException
    //   928: astore 9
    //   930: aload 4
    //   932: astore 6
    //   934: aload 4
    //   936: astore 5
    //   938: aload 9
    //   940: ldc_w 453
    //   943: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   946: aload 4
    //   948: astore 6
    //   950: aload 4
    //   952: astore 5
    //   954: aload 9
    //   956: athrow
    //   957: aload 4
    //   959: astore 6
    //   961: aload 4
    //   963: astore 5
    //   965: aload 9
    //   967: athrow
    //   968: aload 4
    //   970: astore 6
    //   972: aload 4
    //   974: astore 5
    //   976: new 406	org/xutils/common/Callback$CancelledException
    //   979: astore 9
    //   981: aload 4
    //   983: astore 6
    //   985: aload 4
    //   987: astore 5
    //   989: aload 9
    //   991: ldc_w 408
    //   994: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   997: aload 4
    //   999: astore 6
    //   1001: aload 4
    //   1003: astore 5
    //   1005: aload 9
    //   1007: athrow
    //   1008: astore 5
    //   1010: aload 6
    //   1012: astore 4
    //   1014: aload 5
    //   1016: astore 6
    //   1018: aload_0
    //   1019: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   1022: invokevirtual 456	org/xutils/http/request/UriRequest:getResponseCode	()I
    //   1025: istore_2
    //   1026: iload_2
    //   1027: sipush 304
    //   1030: if_icmpeq +88 -> 1118
    //   1033: iload_2
    //   1034: tableswitch	default:+22->1056, 204:+84->1118, 205:+84->1118
    //   1056: aload 6
    //   1058: astore 5
    //   1060: aload_0
    //   1061: invokevirtual 339	org/xutils/http/HttpTask:isCancelled	()Z
    //   1064: ifeq +27 -> 1091
    //   1067: aload 6
    //   1069: astore 5
    //   1071: aload 6
    //   1073: instanceof 406
    //   1076: ifne +15 -> 1091
    //   1079: new 406	org/xutils/common/Callback$CancelledException
    //   1082: dup
    //   1083: ldc_w 458
    //   1086: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   1089: astore 5
    //   1091: aload_0
    //   1092: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   1095: astore 6
    //   1097: iinc 1 1
    //   1100: aload 7
    //   1102: aload 6
    //   1104: aload 5
    //   1106: iload_1
    //   1107: invokevirtual 462	org/xutils/http/app/HttpRetryHandler:canRetry	(Lorg/xutils/http/request/UriRequest;Ljava/lang/Throwable;I)Z
    //   1110: istore_3
    //   1111: aload 5
    //   1113: astore 8
    //   1115: goto -677 -> 438
    //   1118: aconst_null
    //   1119: areturn
    //   1120: new 368	java/lang/StringBuilder
    //   1123: dup
    //   1124: invokespecial 369	java/lang/StringBuilder:<init>	()V
    //   1127: astore 5
    //   1129: aload 5
    //   1131: ldc_w 464
    //   1134: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1137: pop
    //   1138: aload 5
    //   1140: aload_0
    //   1141: getfield 114	org/xutils/http/HttpTask:params	Lorg/xutils/http/RequestParams;
    //   1144: invokevirtual 467	org/xutils/http/RequestParams:getUri	()Ljava/lang/String;
    //   1147: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1150: pop
    //   1151: aload 5
    //   1153: invokevirtual 381	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1156: invokestatic 469	org/xutils/common/util/LogUtil:w	(Ljava/lang/String;)V
    //   1159: iconst_1
    //   1160: istore_3
    //   1161: goto -723 -> 438
    //   1164: aload 8
    //   1166: ifnull +29 -> 1195
    //   1169: aload 4
    //   1171: ifnonnull +24 -> 1195
    //   1174: aload_0
    //   1175: getfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   1178: invokevirtual 415	java/lang/Boolean:booleanValue	()Z
    //   1181: ifeq +6 -> 1187
    //   1184: goto +11 -> 1195
    //   1187: aload_0
    //   1188: iconst_1
    //   1189: putfield 98	org/xutils/http/HttpTask:hasException	Z
    //   1192: aload 8
    //   1194: athrow
    //   1195: aload 4
    //   1197: areturn
    //   1198: new 406	org/xutils/common/Callback$CancelledException
    //   1201: dup
    //   1202: ldc_w 408
    //   1205: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   1208: athrow
    //   1209: new 406	org/xutils/common/Callback$CancelledException
    //   1212: dup
    //   1213: ldc_w 408
    //   1216: invokespecial 410	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   1219: athrow
    //   1220: astore 6
    //   1222: goto -946 -> 276
    //   1225: astore 4
    //   1227: aload 5
    //   1229: astore 4
    //   1231: goto -111 -> 1120
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1234	0	this	HttpTask
    //   437	670	1	i	int
    //   1025	9	2	j	int
    //   435	726	3	bool	boolean
    //   30	96	4	localObject1	Object
    //   147	6	4	localThrowable1	Throwable
    //   183	17	4	localObject2	Object
    //   208	6	4	localThrowable2	Throwable
    //   223	1	4	localObject3	Object
    //   228	34	4	localObject4	Object
    //   297	1	4	localInterruptedException	InterruptedException
    //   302	17	4	localCancelledException1	Callback.CancelledException
    //   334	6	4	localObject5	Object
    //   402	271	4	localObject6	Object
    //   677	1	4	localHttpRedirectException1	org.xutils.ex.HttpRedirectException
    //   681	515	4	localObject7	Object
    //   1225	1	4	localHttpRedirectException2	org.xutils.ex.HttpRedirectException
    //   1229	1	4	localObject8	Object
    //   168	836	5	localObject9	Object
    //   1008	7	5	localThrowable3	Throwable
    //   1058	170	5	localObject10	Object
    //   179	449	6	localObject11	Object
    //   668	1	6	localThrowable4	Throwable
    //   690	413	6	localObject12	Object
    //   1220	1	6	localThrowable5	Throwable
    //   34	1067	7	localObject13	Object
    //   428	765	8	localObject14	Object
    //   686	23	9	localObject15	Object
    //   722	152	9	localCancelledException2	Callback.CancelledException
    //   888	1	9	localThrowable6	Throwable
    //   928	78	9	localCancelledException3	Callback.CancelledException
    // Exception table:
    //   from	to	target	type
    //   89	144	147	java/lang/Throwable
    //   190	201	208	java/lang/Throwable
    //   190	201	228	finally
    //   210	218	228	finally
    //   287	294	297	java/lang/InterruptedException
    //   276	282	334	finally
    //   287	294	334	finally
    //   299	315	334	finally
    //   315	318	334	finally
    //   336	339	334	finally
    //   657	661	668	java/lang/Throwable
    //   657	661	677	org/xutils/ex/HttpRedirectException
    //   642	657	686	finally
    //   476	480	888	java/lang/Throwable
    //   484	489	888	java/lang/Throwable
    //   493	498	888	java/lang/Throwable
    //   502	511	888	java/lang/Throwable
    //   515	528	888	java/lang/Throwable
    //   532	540	888	java/lang/Throwable
    //   544	549	888	java/lang/Throwable
    //   553	560	888	java/lang/Throwable
    //   564	570	888	java/lang/Throwable
    //   574	581	888	java/lang/Throwable
    //   585	595	888	java/lang/Throwable
    //   599	610	888	java/lang/Throwable
    //   880	888	888	java/lang/Throwable
    //   450	457	1008	java/lang/Throwable
    //   465	472	1008	java/lang/Throwable
    //   618	625	1008	java/lang/Throwable
    //   633	638	1008	java/lang/Throwable
    //   696	700	1008	java/lang/Throwable
    //   708	711	1008	java/lang/Throwable
    //   719	724	1008	java/lang/Throwable
    //   732	740	1008	java/lang/Throwable
    //   748	751	1008	java/lang/Throwable
    //   759	765	1008	java/lang/Throwable
    //   773	780	1008	java/lang/Throwable
    //   788	801	1008	java/lang/Throwable
    //   809	816	1008	java/lang/Throwable
    //   824	831	1008	java/lang/Throwable
    //   844	849	1008	java/lang/Throwable
    //   857	865	1008	java/lang/Throwable
    //   873	876	1008	java/lang/Throwable
    //   898	902	1008	java/lang/Throwable
    //   910	917	1008	java/lang/Throwable
    //   925	930	1008	java/lang/Throwable
    //   938	946	1008	java/lang/Throwable
    //   954	957	1008	java/lang/Throwable
    //   965	968	1008	java/lang/Throwable
    //   976	981	1008	java/lang/Throwable
    //   989	997	1008	java/lang/Throwable
    //   1005	1008	1008	java/lang/Throwable
    //   287	294	1220	java/lang/Throwable
    //   450	457	1225	org/xutils/ex/HttpRedirectException
    //   465	472	1225	org/xutils/ex/HttpRedirectException
    //   476	480	1225	org/xutils/ex/HttpRedirectException
    //   484	489	1225	org/xutils/ex/HttpRedirectException
    //   493	498	1225	org/xutils/ex/HttpRedirectException
    //   502	511	1225	org/xutils/ex/HttpRedirectException
    //   515	528	1225	org/xutils/ex/HttpRedirectException
    //   532	540	1225	org/xutils/ex/HttpRedirectException
    //   544	549	1225	org/xutils/ex/HttpRedirectException
    //   553	560	1225	org/xutils/ex/HttpRedirectException
    //   564	570	1225	org/xutils/ex/HttpRedirectException
    //   574	581	1225	org/xutils/ex/HttpRedirectException
    //   585	595	1225	org/xutils/ex/HttpRedirectException
    //   599	610	1225	org/xutils/ex/HttpRedirectException
    //   618	625	1225	org/xutils/ex/HttpRedirectException
    //   633	638	1225	org/xutils/ex/HttpRedirectException
    //   696	700	1225	org/xutils/ex/HttpRedirectException
    //   708	711	1225	org/xutils/ex/HttpRedirectException
    //   719	724	1225	org/xutils/ex/HttpRedirectException
    //   732	740	1225	org/xutils/ex/HttpRedirectException
    //   748	751	1225	org/xutils/ex/HttpRedirectException
    //   759	765	1225	org/xutils/ex/HttpRedirectException
    //   773	780	1225	org/xutils/ex/HttpRedirectException
    //   788	801	1225	org/xutils/ex/HttpRedirectException
    //   809	816	1225	org/xutils/ex/HttpRedirectException
    //   824	831	1225	org/xutils/ex/HttpRedirectException
    //   844	849	1225	org/xutils/ex/HttpRedirectException
    //   857	865	1225	org/xutils/ex/HttpRedirectException
    //   873	876	1225	org/xutils/ex/HttpRedirectException
    //   880	888	1225	org/xutils/ex/HttpRedirectException
    //   898	902	1225	org/xutils/ex/HttpRedirectException
    //   910	917	1225	org/xutils/ex/HttpRedirectException
    //   925	930	1225	org/xutils/ex/HttpRedirectException
    //   938	946	1225	org/xutils/ex/HttpRedirectException
    //   954	957	1225	org/xutils/ex/HttpRedirectException
    //   965	968	1225	org/xutils/ex/HttpRedirectException
    //   976	981	1225	org/xutils/ex/HttpRedirectException
    //   989	997	1225	org/xutils/ex/HttpRedirectException
    //   1005	1008	1225	org/xutils/ex/HttpRedirectException
  }
  
  public Executor getExecutor()
  {
    return this.executor;
  }
  
  public Priority getPriority()
  {
    return this.params.getPriority();
  }
  
  protected boolean isCancelFast()
  {
    return this.params.isCancelFast();
  }
  
  protected void onCancelled(Callback.CancelledException paramCancelledException)
  {
    RequestTracker localRequestTracker = this.tracker;
    if (localRequestTracker != null) {
      localRequestTracker.onCancelled(this.request);
    }
    this.callback.onCancelled(paramCancelledException);
  }
  
  protected void onError(Throwable paramThrowable, boolean paramBoolean)
  {
    RequestTracker localRequestTracker = this.tracker;
    if (localRequestTracker != null) {
      localRequestTracker.onError(this.request, paramThrowable, paramBoolean);
    }
    this.callback.onError(paramThrowable, paramBoolean);
  }
  
  protected void onFinished()
  {
    RequestTracker localRequestTracker = this.tracker;
    if (localRequestTracker != null) {
      localRequestTracker.onFinished(this.request);
    }
    x.task().run(new Runnable()
    {
      public void run()
      {
        HttpTask.this.closeRequestSync();
      }
    });
    this.callback.onFinished();
  }
  
  protected void onStarted()
  {
    Object localObject = this.tracker;
    if (localObject != null) {
      ((RequestTracker)localObject).onStart(this.params);
    }
    localObject = this.progressCallback;
    if (localObject != null) {
      ((Callback.ProgressCallback)localObject).onStarted();
    }
  }
  
  protected void onSuccess(ResultType paramResultType)
  {
    if (this.hasException) {
      return;
    }
    RequestTracker localRequestTracker = this.tracker;
    if (localRequestTracker != null) {
      localRequestTracker.onSuccess(this.request, paramResultType);
    }
    this.callback.onSuccess(paramResultType);
  }
  
  /* Error */
  protected void onUpdate(int paramInt, Object... paramVarArgs)
  {
    // Byte code:
    //   0: iload_1
    //   1: tableswitch	default:+27->28, 1:+208->209, 2:+96->97, 3:+30->31
    //   28: goto +202 -> 230
    //   31: aload_0
    //   32: getfield 128	org/xutils/http/HttpTask:progressCallback	Lorg/xutils/common/Callback$ProgressCallback;
    //   35: astore_3
    //   36: aload_3
    //   37: ifnull +193 -> 230
    //   40: aload_2
    //   41: arraylength
    //   42: iconst_3
    //   43: if_icmpne +187 -> 230
    //   46: aload_3
    //   47: aload_2
    //   48: iconst_0
    //   49: aaload
    //   50: checkcast 514	java/lang/Number
    //   53: invokevirtual 518	java/lang/Number:longValue	()J
    //   56: aload_2
    //   57: iconst_1
    //   58: aaload
    //   59: checkcast 514	java/lang/Number
    //   62: invokevirtual 518	java/lang/Number:longValue	()J
    //   65: aload_2
    //   66: iconst_2
    //   67: aaload
    //   68: checkcast 412	java/lang/Boolean
    //   71: invokevirtual 415	java/lang/Boolean:booleanValue	()Z
    //   74: invokeinterface 522 6 0
    //   79: goto +151 -> 230
    //   82: astore_2
    //   83: aload_0
    //   84: getfield 116	org/xutils/http/HttpTask:callback	Lorg/xutils/common/Callback$CommonCallback;
    //   87: aload_2
    //   88: iconst_1
    //   89: invokeinterface 491 3 0
    //   94: goto +136 -> 230
    //   97: aload_0
    //   98: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   101: astore_3
    //   102: aload_3
    //   103: monitorenter
    //   104: aload_2
    //   105: iconst_0
    //   106: aaload
    //   107: astore_2
    //   108: aload_0
    //   109: getfield 152	org/xutils/http/HttpTask:tracker	Lorg/xutils/http/app/RequestTracker;
    //   112: ifnull +17 -> 129
    //   115: aload_0
    //   116: getfield 152	org/xutils/http/HttpTask:tracker	Lorg/xutils/http/app/RequestTracker;
    //   119: aload_0
    //   120: getfield 177	org/xutils/http/HttpTask:request	Lorg/xutils/http/request/UriRequest;
    //   123: aload_2
    //   124: invokeinterface 525 3 0
    //   129: aload_0
    //   130: aload_0
    //   131: getfield 120	org/xutils/http/HttpTask:cacheCallback	Lorg/xutils/common/Callback$CacheCallback;
    //   134: aload_2
    //   135: invokeinterface 528 2 0
    //   140: invokestatic 419	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   143: putfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   146: aload_0
    //   147: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   150: astore_2
    //   151: aload_2
    //   152: invokevirtual 531	java/lang/Object:notifyAll	()V
    //   155: goto +35 -> 190
    //   158: astore_2
    //   159: goto +36 -> 195
    //   162: astore_2
    //   163: aload_0
    //   164: iconst_0
    //   165: invokestatic 419	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   168: putfield 102	org/xutils/http/HttpTask:trustCache	Ljava/lang/Boolean;
    //   171: aload_0
    //   172: getfield 116	org/xutils/http/HttpTask:callback	Lorg/xutils/common/Callback$CommonCallback;
    //   175: aload_2
    //   176: iconst_1
    //   177: invokeinterface 491 3 0
    //   182: aload_0
    //   183: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   186: astore_2
    //   187: goto -36 -> 151
    //   190: aload_3
    //   191: monitorexit
    //   192: goto +38 -> 230
    //   195: aload_0
    //   196: getfield 108	org/xutils/http/HttpTask:cacheLock	Ljava/lang/Object;
    //   199: invokevirtual 531	java/lang/Object:notifyAll	()V
    //   202: aload_2
    //   203: athrow
    //   204: astore_2
    //   205: aload_3
    //   206: monitorexit
    //   207: aload_2
    //   208: athrow
    //   209: aload_0
    //   210: getfield 152	org/xutils/http/HttpTask:tracker	Lorg/xutils/http/app/RequestTracker;
    //   213: astore_3
    //   214: aload_3
    //   215: ifnull +15 -> 230
    //   218: aload_3
    //   219: aload_2
    //   220: iconst_0
    //   221: aaload
    //   222: checkcast 286	org/xutils/http/request/UriRequest
    //   225: invokeinterface 534 2 0
    //   230: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	231	0	this	HttpTask
    //   0	231	1	paramInt	int
    //   0	231	2	paramVarArgs	Object[]
    //   35	184	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   46	79	82	java/lang/Throwable
    //   108	129	158	finally
    //   129	146	158	finally
    //   163	182	158	finally
    //   108	129	162	java/lang/Throwable
    //   129	146	162	java/lang/Throwable
    //   146	151	204	finally
    //   151	155	204	finally
    //   182	187	204	finally
    //   190	192	204	finally
    //   195	204	204	finally
    //   205	207	204	finally
  }
  
  protected void onWaiting()
  {
    Object localObject = this.tracker;
    if (localObject != null) {
      ((RequestTracker)localObject).onWaiting(this.params);
    }
    localObject = this.progressCallback;
    if (localObject != null) {
      ((Callback.ProgressCallback)localObject).onWaiting();
    }
  }
  
  public String toString()
  {
    return this.params.toString();
  }
  
  public boolean updateProgress(long paramLong1, long paramLong2, boolean paramBoolean)
  {
    boolean bool2 = isCancelled();
    boolean bool1 = false;
    if ((!bool2) && (!isFinished()))
    {
      if ((this.progressCallback != null) && (this.request != null) && (paramLong1 > 0L))
      {
        long l = paramLong1;
        if (paramLong1 < paramLong2) {
          l = paramLong2;
        }
        if (paramBoolean)
        {
          this.lastUpdateTime = System.currentTimeMillis();
          update(3, new Object[] { Long.valueOf(l), Long.valueOf(paramLong2), Boolean.valueOf(this.request.isLoading()) });
        }
        else
        {
          paramLong1 = System.currentTimeMillis();
          if (paramLong1 - this.lastUpdateTime >= this.loadingUpdateMaxTimeSpan)
          {
            this.lastUpdateTime = paramLong1;
            update(3, new Object[] { Long.valueOf(l), Long.valueOf(paramLong2), Boolean.valueOf(this.request.isLoading()) });
          }
        }
      }
      paramBoolean = bool1;
      if (!isCancelled())
      {
        paramBoolean = bool1;
        if (!isFinished()) {
          paramBoolean = true;
        }
      }
      return paramBoolean;
    }
    return false;
  }
  
  private final class RequestWorker
  {
    Throwable ex;
    Object result;
    
    private RequestWorker() {}
    
    /* Error */
    public void request()
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_1
      //   2: iconst_0
      //   3: istore_2
      //   4: ldc 31
      //   6: aload_0
      //   7: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   10: invokestatic 35	org/xutils/http/HttpTask:access$200	(Lorg/xutils/http/HttpTask;)Ljava/lang/reflect/Type;
      //   13: if_acmpne +74 -> 87
      //   16: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   19: astore 4
      //   21: aload 4
      //   23: monitorenter
      //   24: iload_2
      //   25: istore_1
      //   26: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   29: invokevirtual 45	java/util/concurrent/atomic/AtomicInteger:get	()I
      //   32: iconst_3
      //   33: if_icmplt +33 -> 66
      //   36: aload_0
      //   37: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   40: invokevirtual 49	org/xutils/http/HttpTask:isCancelled	()Z
      //   43: istore_3
      //   44: iload_2
      //   45: istore_1
      //   46: iload_3
      //   47: ifne +19 -> 66
      //   50: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   53: ldc2_w 50
      //   56: invokevirtual 55	java/lang/Object:wait	(J)V
      //   59: goto -35 -> 24
      //   62: astore 5
      //   64: iconst_1
      //   65: istore_1
      //   66: aload 4
      //   68: monitorexit
      //   69: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   72: invokevirtual 58	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
      //   75: pop
      //   76: goto +11 -> 87
      //   79: astore 5
      //   81: aload 4
      //   83: monitorexit
      //   84: aload 5
      //   86: athrow
      //   87: iload_1
      //   88: ifne +123 -> 211
      //   91: aload_0
      //   92: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   95: invokevirtual 49	org/xutils/http/HttpTask:isCancelled	()Z
      //   98: istore_3
      //   99: iload_3
      //   100: ifeq +6 -> 106
      //   103: goto +108 -> 211
      //   106: aload_0
      //   107: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   110: invokestatic 62	org/xutils/http/HttpTask:access$500	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/request/UriRequest;
      //   113: aload_0
      //   114: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   117: invokestatic 66	org/xutils/http/HttpTask:access$400	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/app/RequestInterceptListener;
      //   120: invokevirtual 72	org/xutils/http/request/UriRequest:setRequestInterceptListener	(Lorg/xutils/http/app/RequestInterceptListener;)V
      //   123: aload_0
      //   124: aload_0
      //   125: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   128: invokestatic 62	org/xutils/http/HttpTask:access$500	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/request/UriRequest;
      //   131: invokevirtual 76	org/xutils/http/request/UriRequest:loadResult	()Ljava/lang/Object;
      //   134: putfield 78	org/xutils/http/HttpTask$RequestWorker:result	Ljava/lang/Object;
      //   137: goto +11 -> 148
      //   140: astore 4
      //   142: aload_0
      //   143: aload 4
      //   145: putfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   148: aload_0
      //   149: getfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   152: astore 4
      //   154: aload 4
      //   156: ifnonnull +50 -> 206
      //   159: ldc 31
      //   161: aload_0
      //   162: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   165: invokestatic 35	org/xutils/http/HttpTask:access$200	(Lorg/xutils/http/HttpTask;)Ljava/lang/reflect/Type;
      //   168: if_acmpne +321 -> 489
      //   171: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   174: astore 5
      //   176: aload 5
      //   178: monitorenter
      //   179: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   182: invokevirtual 83	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   185: pop
      //   186: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   189: invokevirtual 86	java/lang/Object:notifyAll	()V
      //   192: aload 5
      //   194: monitorexit
      //   195: goto +294 -> 489
      //   198: astore 4
      //   200: aload 5
      //   202: monitorexit
      //   203: aload 4
      //   205: athrow
      //   206: aload_0
      //   207: getfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   210: athrow
      //   211: new 88	org/xutils/common/Callback$CancelledException
      //   214: astore 6
      //   216: new 90	java/lang/StringBuilder
      //   219: astore 5
      //   221: aload 5
      //   223: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   226: aload 5
      //   228: ldc 93
      //   230: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   233: pop
      //   234: iload_1
      //   235: ifeq +10 -> 245
      //   238: ldc 99
      //   240: astore 4
      //   242: goto +7 -> 249
      //   245: ldc 101
      //   247: astore 4
      //   249: aload 5
      //   251: aload 4
      //   253: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   256: pop
      //   257: aload 6
      //   259: aload 5
      //   261: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   264: invokespecial 108	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
      //   267: aload 6
      //   269: athrow
      //   270: astore 5
      //   272: goto +218 -> 490
      //   275: astore 4
      //   277: aload_0
      //   278: aload 4
      //   280: putfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   283: aload 4
      //   285: instanceof 110
      //   288: ifeq +154 -> 442
      //   291: aload 4
      //   293: checkcast 110	org/xutils/ex/HttpException
      //   296: astore 5
      //   298: aload 5
      //   300: invokevirtual 113	org/xutils/ex/HttpException:getCode	()I
      //   303: istore_1
      //   304: iload_1
      //   305: sipush 301
      //   308: if_icmpeq +10 -> 318
      //   311: iload_1
      //   312: sipush 302
      //   315: if_icmpne +127 -> 442
      //   318: aload_0
      //   319: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   322: invokestatic 117	org/xutils/http/HttpTask:access$600	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/RequestParams;
      //   325: invokevirtual 123	org/xutils/http/RequestParams:getRedirectHandler	()Lorg/xutils/http/app/RedirectHandler;
      //   328: astore 6
      //   330: aload 6
      //   332: ifnull +110 -> 442
      //   335: aload 6
      //   337: aload_0
      //   338: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   341: invokestatic 62	org/xutils/http/HttpTask:access$500	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/request/UriRequest;
      //   344: invokeinterface 129 2 0
      //   349: astore 6
      //   351: aload 6
      //   353: ifnull +89 -> 442
      //   356: aload 6
      //   358: invokevirtual 133	org/xutils/http/RequestParams:getMethod	()Lorg/xutils/http/HttpMethod;
      //   361: ifnonnull +18 -> 379
      //   364: aload 6
      //   366: aload_0
      //   367: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   370: invokestatic 117	org/xutils/http/HttpTask:access$600	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/RequestParams;
      //   373: invokevirtual 133	org/xutils/http/RequestParams:getMethod	()Lorg/xutils/http/HttpMethod;
      //   376: invokevirtual 137	org/xutils/http/RequestParams:setMethod	(Lorg/xutils/http/HttpMethod;)V
      //   379: aload_0
      //   380: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   383: aload 6
      //   385: invokestatic 141	org/xutils/http/HttpTask:access$602	(Lorg/xutils/http/HttpTask;Lorg/xutils/http/RequestParams;)Lorg/xutils/http/RequestParams;
      //   388: pop
      //   389: aload_0
      //   390: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   393: aload_0
      //   394: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   397: invokestatic 144	org/xutils/http/HttpTask:access$700	(Lorg/xutils/http/HttpTask;)Lorg/xutils/http/request/UriRequest;
      //   400: invokestatic 148	org/xutils/http/HttpTask:access$502	(Lorg/xutils/http/HttpTask;Lorg/xutils/http/request/UriRequest;)Lorg/xutils/http/request/UriRequest;
      //   403: pop
      //   404: new 150	org/xutils/ex/HttpRedirectException
      //   407: astore 6
      //   409: aload 6
      //   411: iload_1
      //   412: aload 5
      //   414: invokevirtual 153	org/xutils/ex/HttpException:getMessage	()Ljava/lang/String;
      //   417: aload 5
      //   419: invokevirtual 156	org/xutils/ex/HttpException:getResult	()Ljava/lang/String;
      //   422: invokespecial 159	org/xutils/ex/HttpRedirectException:<init>	(ILjava/lang/String;Ljava/lang/String;)V
      //   425: aload_0
      //   426: aload 6
      //   428: putfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   431: goto +11 -> 442
      //   434: astore 5
      //   436: aload_0
      //   437: aload 4
      //   439: putfield 80	org/xutils/http/HttpTask$RequestWorker:ex	Ljava/lang/Throwable;
      //   442: ldc 31
      //   444: aload_0
      //   445: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   448: invokestatic 35	org/xutils/http/HttpTask:access$200	(Lorg/xutils/http/HttpTask;)Ljava/lang/reflect/Type;
      //   451: if_acmpne +38 -> 489
      //   454: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   457: astore 5
      //   459: aload 5
      //   461: monitorenter
      //   462: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   465: invokevirtual 83	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   468: pop
      //   469: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   472: invokevirtual 86	java/lang/Object:notifyAll	()V
      //   475: aload 5
      //   477: monitorexit
      //   478: goto +11 -> 489
      //   481: astore 4
      //   483: aload 5
      //   485: monitorexit
      //   486: aload 4
      //   488: athrow
      //   489: return
      //   490: ldc 31
      //   492: aload_0
      //   493: getfield 17	org/xutils/http/HttpTask$RequestWorker:this$0	Lorg/xutils/http/HttpTask;
      //   496: invokestatic 35	org/xutils/http/HttpTask:access$200	(Lorg/xutils/http/HttpTask;)Ljava/lang/reflect/Type;
      //   499: if_acmpne +38 -> 537
      //   502: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   505: astore 4
      //   507: aload 4
      //   509: monitorenter
      //   510: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   513: invokevirtual 83	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   516: pop
      //   517: invokestatic 39	org/xutils/http/HttpTask:access$300	()Ljava/util/concurrent/atomic/AtomicInteger;
      //   520: invokevirtual 86	java/lang/Object:notifyAll	()V
      //   523: aload 4
      //   525: monitorexit
      //   526: goto +11 -> 537
      //   529: astore 5
      //   531: aload 4
      //   533: monitorexit
      //   534: aload 5
      //   536: athrow
      //   537: aload 5
      //   539: athrow
      //   540: astore 5
      //   542: goto -518 -> 24
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	545	0	this	RequestWorker
      //   1	411	1	i	int
      //   3	42	2	j	int
      //   43	57	3	bool	boolean
      //   19	63	4	localAtomicInteger1	AtomicInteger
      //   140	4	4	localThrowable1	Throwable
      //   152	3	4	localThrowable2	Throwable
      //   198	6	4	localObject1	Object
      //   240	12	4	str	String
      //   275	163	4	localThrowable3	Throwable
      //   481	6	4	localObject2	Object
      //   62	1	5	localInterruptedException	InterruptedException
      //   79	6	5	localObject3	Object
      //   270	1	5	localObject5	Object
      //   296	122	5	localHttpException	org.xutils.ex.HttpException
      //   434	1	5	localThrowable4	Throwable
      //   529	9	5	localObject6	Object
      //   540	1	5	localThrowable5	Throwable
      //   214	213	6	localObject7	Object
      // Exception table:
      //   from	to	target	type
      //   50	59	62	java/lang/InterruptedException
      //   26	44	79	finally
      //   50	59	79	finally
      //   66	69	79	finally
      //   81	84	79	finally
      //   106	137	140	java/lang/Throwable
      //   179	195	198	finally
      //   200	203	198	finally
      //   4	24	270	finally
      //   69	76	270	finally
      //   84	87	270	finally
      //   91	99	270	finally
      //   106	137	270	finally
      //   142	148	270	finally
      //   148	154	270	finally
      //   206	211	270	finally
      //   211	234	270	finally
      //   249	270	270	finally
      //   277	304	270	finally
      //   318	330	270	finally
      //   335	351	270	finally
      //   356	379	270	finally
      //   379	431	270	finally
      //   436	442	270	finally
      //   4	24	275	java/lang/Throwable
      //   69	76	275	java/lang/Throwable
      //   84	87	275	java/lang/Throwable
      //   91	99	275	java/lang/Throwable
      //   142	148	275	java/lang/Throwable
      //   148	154	275	java/lang/Throwable
      //   206	211	275	java/lang/Throwable
      //   211	234	275	java/lang/Throwable
      //   249	270	275	java/lang/Throwable
      //   335	351	434	java/lang/Throwable
      //   356	379	434	java/lang/Throwable
      //   379	431	434	java/lang/Throwable
      //   462	478	481	finally
      //   483	486	481	finally
      //   510	526	529	finally
      //   531	534	529	finally
      //   50	59	540	java/lang/Throwable
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/HttpTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */