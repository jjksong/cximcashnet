package org.xutils.http;

import android.text.TextUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.http.body.BodyItemWrapper;
import org.xutils.http.body.FileBody;
import org.xutils.http.body.InputStreamBody;
import org.xutils.http.body.MultipartBody;
import org.xutils.http.body.RequestBody;
import org.xutils.http.body.StringBody;
import org.xutils.http.body.UrlEncodedParamsBody;

abstract class BaseParams
{
  private boolean asJsonContent = false;
  private String bodyContent;
  private final List<KeyValue> bodyParams = new ArrayList();
  private String charset = "UTF-8";
  private final List<KeyValue> fileParams = new ArrayList();
  private final List<Header> headers = new ArrayList();
  private HttpMethod method;
  private boolean multipart = false;
  private final List<KeyValue> queryStringParams = new ArrayList();
  private RequestBody requestBody;
  
  private void checkBodyParams()
  {
    try
    {
      boolean bool = this.bodyParams.isEmpty();
      if (bool) {
        return;
      }
      if ((!HttpMethod.permitsRequestBody(this.method)) || (!TextUtils.isEmpty(this.bodyContent)) || (this.requestBody != null))
      {
        this.queryStringParams.addAll(this.bodyParams);
        this.bodyParams.clear();
      }
      if ((!this.bodyParams.isEmpty()) && ((this.multipart) || (this.fileParams.size() > 0)))
      {
        this.fileParams.addAll(this.bodyParams);
        this.bodyParams.clear();
      }
      if (this.asJsonContent)
      {
        bool = this.bodyParams.isEmpty();
        if (!bool) {
          try
          {
            if (!TextUtils.isEmpty(this.bodyContent))
            {
              localObject1 = new org/json/JSONObject;
              ((JSONObject)localObject1).<init>(this.bodyContent);
            }
            else
            {
              localObject1 = new JSONObject();
            }
            params2Json((JSONObject)localObject1, this.bodyParams);
            this.bodyContent = ((JSONObject)localObject1).toString();
            this.bodyParams.clear();
          }
          catch (JSONException localJSONException)
          {
            Object localObject1 = new java/lang/RuntimeException;
            ((RuntimeException)localObject1).<init>(localJSONException);
            throw ((Throwable)localObject1);
          }
        }
      }
      return;
    }
    finally {}
  }
  
  private void params2Json(JSONObject paramJSONObject, List<KeyValue> paramList)
    throws JSONException
  {
    HashSet localHashSet = new HashSet(paramList.size());
    LinkedHashMap localLinkedHashMap = new LinkedHashMap(paramList.size());
    Object localObject2;
    Object localObject1;
    for (int i = 0; i < paramList.size(); i++)
    {
      localObject2 = (KeyValue)paramList.get(i);
      String str = ((KeyValue)localObject2).key;
      if (!TextUtils.isEmpty(str))
      {
        if (localLinkedHashMap.containsKey(str))
        {
          localObject1 = (JSONArray)localLinkedHashMap.get(str);
        }
        else
        {
          localObject1 = new JSONArray();
          localLinkedHashMap.put(str, localObject1);
        }
        ((JSONArray)localObject1).put(RequestParamsHelper.parseJSONObject(((KeyValue)localObject2).value));
        if ((localObject2 instanceof ArrayItem)) {
          localHashSet.add(str);
        }
      }
    }
    paramList = localLinkedHashMap.entrySet().iterator();
    while (paramList.hasNext())
    {
      localObject2 = (Map.Entry)paramList.next();
      localObject1 = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (JSONArray)((Map.Entry)localObject2).getValue();
      if ((((JSONArray)localObject2).length() <= 1) && (!localHashSet.contains(localObject1))) {
        paramJSONObject.put((String)localObject1, ((JSONArray)localObject2).get(0));
      } else {
        paramJSONObject.put((String)localObject1, localObject2);
      }
    }
  }
  
  public void addBodyParameter(String paramString, File paramFile)
  {
    addBodyParameter(paramString, paramFile, null, null);
  }
  
  public void addBodyParameter(String paramString1, Object paramObject, String paramString2)
  {
    addBodyParameter(paramString1, paramObject, paramString2, null);
  }
  
  public void addBodyParameter(String paramString1, Object paramObject, String paramString2, String paramString3)
  {
    if ((TextUtils.isEmpty(paramString2)) && (TextUtils.isEmpty(paramString3))) {
      this.fileParams.add(new KeyValue(paramString1, paramObject));
    } else {
      this.fileParams.add(new KeyValue(paramString1, new BodyItemWrapper(paramObject, paramString2, paramString3)));
    }
  }
  
  public void addBodyParameter(String paramString1, String paramString2)
  {
    if (!TextUtils.isEmpty(paramString1)) {
      this.bodyParams.add(new KeyValue(paramString1, paramString2));
    } else {
      this.bodyContent = paramString2;
    }
  }
  
  public void addHeader(String paramString1, String paramString2)
  {
    this.headers.add(new Header(paramString1, paramString2, false));
  }
  
  public void addParameter(String paramString, Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    Object localObject = this.method;
    int j = 0;
    int k = 0;
    int i = 0;
    if ((localObject != null) && (!HttpMethod.permitsRequestBody((HttpMethod)localObject)))
    {
      if (!TextUtils.isEmpty(paramString))
      {
        if ((paramObject instanceof Iterable))
        {
          localObject = ((Iterable)paramObject).iterator();
          while (((Iterator)localObject).hasNext())
          {
            paramObject = ((Iterator)localObject).next();
            this.queryStringParams.add(new ArrayItem(paramString, paramObject));
          }
        }
        if (paramObject.getClass().isArray())
        {
          j = Array.getLength(paramObject);
          while (i < j)
          {
            this.queryStringParams.add(new ArrayItem(paramString, Array.get(paramObject, i)));
            i++;
          }
        }
        this.queryStringParams.add(new KeyValue(paramString, paramObject));
      }
    }
    else if (!TextUtils.isEmpty(paramString))
    {
      if ((!(paramObject instanceof File)) && (!(paramObject instanceof InputStream)) && (!(paramObject instanceof byte[])))
      {
        if ((paramObject instanceof Iterable))
        {
          localObject = ((Iterable)paramObject).iterator();
          while (((Iterator)localObject).hasNext())
          {
            paramObject = ((Iterator)localObject).next();
            this.bodyParams.add(new ArrayItem(paramString, paramObject));
          }
        }
        if ((paramObject instanceof JSONArray))
        {
          paramObject = (JSONArray)paramObject;
          k = ((JSONArray)paramObject).length();
          for (i = j; i < k; i++) {
            this.bodyParams.add(new ArrayItem(paramString, ((JSONArray)paramObject).opt(i)));
          }
        }
        if (paramObject.getClass().isArray())
        {
          j = Array.getLength(paramObject);
          for (i = k; i < j; i++) {
            this.bodyParams.add(new ArrayItem(paramString, Array.get(paramObject, i)));
          }
        }
        this.bodyParams.add(new KeyValue(paramString, paramObject));
      }
      else
      {
        this.fileParams.add(new KeyValue(paramString, paramObject));
      }
    }
    else {
      this.bodyContent = paramObject.toString();
    }
  }
  
  public void addQueryStringParameter(String paramString1, String paramString2)
  {
    if (!TextUtils.isEmpty(paramString1)) {
      this.queryStringParams.add(new KeyValue(paramString1, paramString2));
    }
  }
  
  public void clearParams()
  {
    this.queryStringParams.clear();
    this.bodyParams.clear();
    this.fileParams.clear();
    this.bodyContent = null;
    this.requestBody = null;
  }
  
  public String getBodyContent()
  {
    checkBodyParams();
    return this.bodyContent;
  }
  
  public List<KeyValue> getBodyParams()
  {
    checkBodyParams();
    return new ArrayList(this.bodyParams);
  }
  
  public String getCharset()
  {
    return this.charset;
  }
  
  public List<KeyValue> getFileParams()
  {
    checkBodyParams();
    return new ArrayList(this.fileParams);
  }
  
  public List<Header> getHeaders()
  {
    return new ArrayList(this.headers);
  }
  
  public HttpMethod getMethod()
  {
    return this.method;
  }
  
  public List<KeyValue> getParams(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject2 = this.queryStringParams.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (KeyValue)((Iterator)localObject2).next();
      if ((paramString == null) && (((KeyValue)localObject1).key == null)) {
        localArrayList.add(localObject1);
      } else if ((paramString != null) && (paramString.equals(((KeyValue)localObject1).key))) {
        localArrayList.add(localObject1);
      }
    }
    Object localObject1 = this.bodyParams.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (KeyValue)((Iterator)localObject1).next();
      if ((paramString == null) && (((KeyValue)localObject2).key == null)) {
        localArrayList.add(localObject2);
      } else if ((paramString != null) && (paramString.equals(((KeyValue)localObject2).key))) {
        localArrayList.add(localObject2);
      }
    }
    localObject2 = this.fileParams.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (KeyValue)((Iterator)localObject2).next();
      if ((paramString == null) && (((KeyValue)localObject1).key == null)) {
        localArrayList.add(localObject1);
      } else if ((paramString != null) && (paramString.equals(((KeyValue)localObject1).key))) {
        localArrayList.add(localObject1);
      }
    }
    return localArrayList;
  }
  
  public List<KeyValue> getQueryStringParams()
  {
    checkBodyParams();
    return new ArrayList(this.queryStringParams);
  }
  
  public RequestBody getRequestBody()
    throws IOException
  {
    checkBodyParams();
    Object localObject1 = this.requestBody;
    if (localObject1 != null) {
      return (RequestBody)localObject1;
    }
    boolean bool = TextUtils.isEmpty(this.bodyContent);
    Object localObject3 = null;
    if (!bool)
    {
      localObject1 = new StringBody(this.bodyContent, this.charset);
    }
    else if ((!this.multipart) && (this.fileParams.size() <= 0))
    {
      localObject1 = localObject3;
      if (this.bodyParams.size() > 0) {
        localObject1 = new UrlEncodedParamsBody(this.bodyParams, this.charset);
      }
    }
    else if ((!this.multipart) && (this.fileParams.size() == 1))
    {
      Object localObject2 = this.fileParams.iterator();
      localObject1 = localObject3;
      if (((Iterator)localObject2).hasNext())
      {
        localObject2 = ((KeyValue)((Iterator)localObject2).next()).value;
        if ((localObject2 instanceof BodyItemWrapper))
        {
          localObject1 = (BodyItemWrapper)localObject2;
          localObject2 = ((BodyItemWrapper)localObject1).getValue();
          localObject1 = ((BodyItemWrapper)localObject1).getContentType();
        }
        else
        {
          localObject1 = null;
        }
        if ((localObject2 instanceof File))
        {
          localObject1 = new FileBody((File)localObject2, (String)localObject1);
        }
        else if ((localObject2 instanceof InputStream))
        {
          localObject1 = new InputStreamBody((InputStream)localObject2, (String)localObject1);
        }
        else if ((localObject2 instanceof byte[]))
        {
          localObject1 = new InputStreamBody(new ByteArrayInputStream((byte[])localObject2), (String)localObject1);
        }
        else if ((localObject2 instanceof String))
        {
          localObject2 = new StringBody((String)localObject2, this.charset);
          ((RequestBody)localObject2).setContentType((String)localObject1);
          localObject1 = localObject2;
        }
        else
        {
          localObject1 = new StringBuilder();
          ((StringBuilder)localObject1).append("Some params will be ignored for: ");
          ((StringBuilder)localObject1).append(toString());
          LogUtil.w(((StringBuilder)localObject1).toString());
          localObject1 = localObject3;
        }
      }
    }
    else
    {
      this.multipart = true;
      localObject1 = new MultipartBody(this.fileParams, this.charset);
    }
    return (RequestBody)localObject1;
  }
  
  public String getStringParameter(String paramString)
  {
    Object localObject2 = this.queryStringParams.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (KeyValue)((Iterator)localObject2).next();
      if ((paramString == null) && (((KeyValue)localObject1).key == null)) {
        return ((KeyValue)localObject1).getValueStr();
      }
      if ((paramString != null) && (paramString.equals(((KeyValue)localObject1).key))) {
        return ((KeyValue)localObject1).getValueStr();
      }
    }
    Object localObject1 = this.bodyParams.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (KeyValue)((Iterator)localObject1).next();
      if ((paramString == null) && (((KeyValue)localObject2).key == null)) {
        return ((KeyValue)localObject2).getValueStr();
      }
      if ((paramString != null) && (paramString.equals(((KeyValue)localObject2).key))) {
        return ((KeyValue)localObject2).getValueStr();
      }
    }
    return null;
  }
  
  public List<KeyValue> getStringParams()
  {
    ArrayList localArrayList = new ArrayList(this.queryStringParams.size() + this.bodyParams.size());
    localArrayList.addAll(this.queryStringParams);
    localArrayList.addAll(this.bodyParams);
    return localArrayList;
  }
  
  public boolean isAsJsonContent()
  {
    return this.asJsonContent;
  }
  
  public boolean isMultipart()
  {
    return this.multipart;
  }
  
  public void removeParameter(String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      Iterator localIterator = this.queryStringParams.iterator();
      while (localIterator.hasNext()) {
        if (paramString.equals(((KeyValue)localIterator.next()).key)) {
          localIterator.remove();
        }
      }
      localIterator = this.bodyParams.iterator();
      while (localIterator.hasNext()) {
        if (paramString.equals(((KeyValue)localIterator.next()).key)) {
          localIterator.remove();
        }
      }
      localIterator = this.fileParams.iterator();
      while (localIterator.hasNext()) {
        if (paramString.equals(((KeyValue)localIterator.next()).key)) {
          localIterator.remove();
        }
      }
    }
    this.bodyContent = null;
  }
  
  public void setAsJsonContent(boolean paramBoolean)
  {
    this.asJsonContent = paramBoolean;
  }
  
  public void setBodyContent(String paramString)
  {
    this.bodyContent = paramString;
  }
  
  public void setCharset(String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {
      this.charset = paramString;
    }
  }
  
  public void setHeader(String paramString1, String paramString2)
  {
    Header localHeader = new Header(paramString1, paramString2, true);
    paramString2 = this.headers.iterator();
    while (paramString2.hasNext()) {
      if (paramString1.equals(((KeyValue)paramString2.next()).key)) {
        paramString2.remove();
      }
    }
    this.headers.add(localHeader);
  }
  
  public void setMethod(HttpMethod paramHttpMethod)
  {
    this.method = paramHttpMethod;
  }
  
  public void setMultipart(boolean paramBoolean)
  {
    this.multipart = paramBoolean;
  }
  
  public void setRequestBody(RequestBody paramRequestBody)
  {
    this.requestBody = paramRequestBody;
  }
  
  public String toJSONString()
  {
    ArrayList localArrayList = new ArrayList(this.queryStringParams.size() + this.bodyParams.size());
    localArrayList.addAll(this.queryStringParams);
    localArrayList.addAll(this.bodyParams);
    try
    {
      if (!TextUtils.isEmpty(this.bodyContent))
      {
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>(this.bodyContent);
      }
      else
      {
        localObject = new JSONObject();
      }
      params2Json((JSONObject)localObject, localArrayList);
      Object localObject = ((JSONObject)localObject).toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException)
    {
      throw new RuntimeException(localJSONException);
    }
  }
  
  public String toString()
  {
    checkBodyParams();
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject1;
    Object localObject2;
    if (!this.queryStringParams.isEmpty())
    {
      localObject1 = this.queryStringParams.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (KeyValue)((Iterator)localObject1).next();
        localStringBuilder.append(((KeyValue)localObject2).key);
        localStringBuilder.append("=");
        localStringBuilder.append(((KeyValue)localObject2).value);
        localStringBuilder.append("&");
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    if (HttpMethod.permitsRequestBody(this.method))
    {
      localStringBuilder.append("<");
      if (!TextUtils.isEmpty(this.bodyContent))
      {
        localStringBuilder.append(this.bodyContent);
      }
      else if (!this.bodyParams.isEmpty())
      {
        localObject2 = this.bodyParams.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject1 = (KeyValue)((Iterator)localObject2).next();
          localStringBuilder.append(((KeyValue)localObject1).key);
          localStringBuilder.append("=");
          localStringBuilder.append(((KeyValue)localObject1).value);
          localStringBuilder.append("&");
        }
        localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      }
      localStringBuilder.append(">");
    }
    return localStringBuilder.toString();
  }
  
  public static final class ArrayItem
    extends KeyValue
  {
    public ArrayItem(String paramString, Object paramObject)
    {
      super(paramObject);
    }
  }
  
  public static final class Header
    extends KeyValue
  {
    public final boolean setHeader;
    
    public Header(String paramString1, String paramString2, boolean paramBoolean)
    {
      super(paramString2);
      this.setHeader = paramBoolean;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/BaseParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */