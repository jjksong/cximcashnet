package org.xutils.http.cookie;

import android.text.TextUtils;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import org.xutils.DbManager;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.LogUtil;
import org.xutils.config.DbConfigs;
import org.xutils.db.DbModelSelector;
import org.xutils.db.Selector;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.db.table.DbModel;
import org.xutils.x;

public enum DbCookieStore
  implements CookieStore
{
  INSTANCE;
  
  private static final int LIMIT_COUNT = 5000;
  private static final long TRIM_TIME_SPAN = 1000L;
  private final DbManager db = x.getDb(DbConfigs.COOKIE.getConfig());
  private long lastTrimTime = 0L;
  private final Executor trimExecutor = new PriorityExecutor(1, true);
  
  private DbCookieStore()
  {
    try
    {
      this.db.delete(CookieEntity.class, WhereBuilder.b("expiry", "=", Long.valueOf(-1L)));
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
  }
  
  private URI getEffectiveURI(URI paramURI)
  {
    try
    {
      URI localURI = new java/net/URI;
      localURI.<init>("http", paramURI.getHost(), paramURI.getPath(), null, null);
      paramURI = localURI;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    return paramURI;
  }
  
  private void trimSize()
  {
    this.trimExecutor.execute(new Runnable()
    {
      public void run()
      {
        long l = System.currentTimeMillis();
        if (l - DbCookieStore.this.lastTrimTime < 1000L) {
          return;
        }
        DbCookieStore.access$002(DbCookieStore.this, l);
        try
        {
          DbCookieStore.this.db.delete(CookieEntity.class, WhereBuilder.b("expiry", "<", Long.valueOf(System.currentTimeMillis())).and("expiry", "!=", Long.valueOf(-1L)));
        }
        catch (Throwable localThrowable1)
        {
          LogUtil.e(localThrowable1.getMessage(), localThrowable1);
        }
        try
        {
          int i = (int)DbCookieStore.this.db.selector(CookieEntity.class).count();
          if (i > 5010)
          {
            List localList = DbCookieStore.this.db.selector(CookieEntity.class).where("expiry", "!=", Long.valueOf(-1L)).orderBy("expiry", false).limit(i - 5000).findAll();
            if (localList != null) {
              DbCookieStore.this.db.delete(localList);
            }
          }
        }
        catch (Throwable localThrowable2)
        {
          LogUtil.e(localThrowable2.getMessage(), localThrowable2);
        }
      }
    });
  }
  
  public void add(URI paramURI, HttpCookie paramHttpCookie)
  {
    if (paramHttpCookie == null) {
      return;
    }
    URI localURI = getEffectiveURI(paramURI);
    try
    {
      paramURI = this.db;
      CookieEntity localCookieEntity = new org/xutils/http/cookie/CookieEntity;
      localCookieEntity.<init>(localURI, paramHttpCookie);
      paramURI.replace(localCookieEntity);
    }
    catch (Throwable paramURI)
    {
      LogUtil.e(paramURI.getMessage(), paramURI);
    }
    trimSize();
  }
  
  public List<HttpCookie> get(URI paramURI)
  {
    ArrayList localArrayList;
    if (paramURI != null)
    {
      URI localURI = getEffectiveURI(paramURI);
      localArrayList = new ArrayList();
      try
      {
        Object localObject1 = this.db.selector(CookieEntity.class);
        WhereBuilder localWhereBuilder1 = WhereBuilder.b();
        paramURI = localURI.getHost();
        Object localObject2;
        int i;
        if (!TextUtils.isEmpty(paramURI))
        {
          WhereBuilder localWhereBuilder2 = WhereBuilder.b("domain", "=", paramURI);
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(".");
          ((StringBuilder)localObject2).append(paramURI);
          localObject2 = localWhereBuilder2.or("domain", "=", ((StringBuilder)localObject2).toString());
          i = paramURI.indexOf(".");
          int j = paramURI.lastIndexOf(".");
          if ((i > 0) && (j > i))
          {
            paramURI = paramURI.substring(i, paramURI.length());
            if (!TextUtils.isEmpty(paramURI)) {
              ((WhereBuilder)localObject2).or("domain", "=", paramURI);
            }
          }
          localWhereBuilder1.and((WhereBuilder)localObject2);
        }
        paramURI = localURI.getPath();
        if (!TextUtils.isEmpty(paramURI))
        {
          localObject2 = WhereBuilder.b("path", "=", paramURI).or("path", "=", "/").or("path", "=", null);
          for (i = paramURI.lastIndexOf("/"); i > 0; i = paramURI.lastIndexOf("/"))
          {
            paramURI = paramURI.substring(0, i);
            ((WhereBuilder)localObject2).or("path", "=", paramURI);
          }
          localWhereBuilder1.and((WhereBuilder)localObject2);
        }
        localWhereBuilder1.or("uri", "=", localURI.toString());
        paramURI = ((Selector)localObject1).where(localWhereBuilder1).findAll();
        if (paramURI != null)
        {
          localObject1 = paramURI.iterator();
          while (((Iterator)localObject1).hasNext())
          {
            paramURI = (CookieEntity)((Iterator)localObject1).next();
            if (!paramURI.isExpired()) {
              localArrayList.add(paramURI.toHttpCookie());
            }
          }
        }
        return localArrayList;
      }
      catch (Throwable paramURI)
      {
        LogUtil.e(paramURI.getMessage(), paramURI);
      }
    }
    throw new NullPointerException("uri is null");
  }
  
  public List<HttpCookie> getCookies()
  {
    localArrayList = new ArrayList();
    try
    {
      Object localObject = this.db.findAll(CookieEntity.class);
      if (localObject != null)
      {
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext())
        {
          CookieEntity localCookieEntity = (CookieEntity)((Iterator)localObject).next();
          if (!localCookieEntity.isExpired()) {
            localArrayList.add(localCookieEntity.toHttpCookie());
          }
        }
      }
      return localArrayList;
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
  }
  
  public List<URI> getURIs()
  {
    localArrayList = new ArrayList();
    try
    {
      Object localObject = this.db.selector(CookieEntity.class).select(new String[] { "uri" }).findAll();
      if (localObject != null)
      {
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext())
        {
          String str = ((DbModel)((Iterator)localObject).next()).getString("uri");
          boolean bool = TextUtils.isEmpty(str);
          if (!bool) {
            try
            {
              URI localURI = new java/net/URI;
              localURI.<init>(str);
              localArrayList.add(localURI);
            }
            catch (Throwable localThrowable3)
            {
              LogUtil.e(localThrowable3.getMessage(), localThrowable3);
              try
              {
                this.db.delete(CookieEntity.class, WhereBuilder.b("uri", "=", str));
              }
              catch (Throwable localThrowable2)
              {
                LogUtil.e(localThrowable2.getMessage(), localThrowable2);
              }
            }
          }
        }
      }
      return localArrayList;
    }
    catch (Throwable localThrowable1)
    {
      LogUtil.e(localThrowable1.getMessage(), localThrowable1);
    }
  }
  
  public boolean remove(URI paramURI, HttpCookie paramHttpCookie)
  {
    boolean bool = true;
    if (paramHttpCookie == null) {
      return true;
    }
    try
    {
      WhereBuilder localWhereBuilder = WhereBuilder.b("name", "=", paramHttpCookie.getName());
      paramURI = paramHttpCookie.getDomain();
      if (!TextUtils.isEmpty(paramURI)) {
        localWhereBuilder.and("domain", "=", paramURI);
      }
      paramHttpCookie = paramHttpCookie.getPath();
      if (!TextUtils.isEmpty(paramHttpCookie))
      {
        paramURI = paramHttpCookie;
        if (paramHttpCookie.length() > 1)
        {
          paramURI = paramHttpCookie;
          if (paramHttpCookie.endsWith("/")) {
            paramURI = paramHttpCookie.substring(0, paramHttpCookie.length() - 1);
          }
        }
        localWhereBuilder.and("path", "=", paramURI);
      }
      this.db.delete(CookieEntity.class, localWhereBuilder);
    }
    catch (Throwable paramURI)
    {
      LogUtil.e(paramURI.getMessage(), paramURI);
      bool = false;
    }
    return bool;
  }
  
  public boolean removeAll()
  {
    try
    {
      this.db.delete(CookieEntity.class);
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/cookie/DbCookieStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */