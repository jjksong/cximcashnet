package org.xutils.http;

public abstract interface ProgressHandler
{
  public abstract boolean updateProgress(long paramLong1, long paramLong2, boolean paramBoolean);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/ProgressHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */