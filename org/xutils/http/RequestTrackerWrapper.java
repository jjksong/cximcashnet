package org.xutils.http;

import org.xutils.common.util.LogUtil;
import org.xutils.http.app.RequestTracker;
import org.xutils.http.request.UriRequest;

final class RequestTrackerWrapper
  implements RequestTracker
{
  private final RequestTracker base;
  
  public RequestTrackerWrapper(RequestTracker paramRequestTracker)
  {
    this.base = paramRequestTracker;
  }
  
  public void onCache(UriRequest paramUriRequest, Object paramObject)
  {
    try
    {
      this.base.onCache(paramUriRequest, paramObject);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onCancelled(UriRequest paramUriRequest)
  {
    try
    {
      this.base.onCancelled(paramUriRequest);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onError(UriRequest paramUriRequest, Throwable paramThrowable, boolean paramBoolean)
  {
    try
    {
      this.base.onError(paramUriRequest, paramThrowable, paramBoolean);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onFinished(UriRequest paramUriRequest)
  {
    try
    {
      this.base.onFinished(paramUriRequest);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onRequestCreated(UriRequest paramUriRequest)
  {
    try
    {
      this.base.onRequestCreated(paramUriRequest);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onStart(RequestParams paramRequestParams)
  {
    try
    {
      this.base.onStart(paramRequestParams);
    }
    catch (Throwable paramRequestParams)
    {
      LogUtil.e(paramRequestParams.getMessage(), paramRequestParams);
    }
  }
  
  public void onSuccess(UriRequest paramUriRequest, Object paramObject)
  {
    try
    {
      this.base.onSuccess(paramUriRequest, paramObject);
    }
    catch (Throwable paramUriRequest)
    {
      LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
    }
  }
  
  public void onWaiting(RequestParams paramRequestParams)
  {
    try
    {
      this.base.onWaiting(paramRequestParams);
    }
    catch (Throwable paramRequestParams)
    {
      LogUtil.e(paramRequestParams.getMessage(), paramRequestParams);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/RequestTrackerWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */