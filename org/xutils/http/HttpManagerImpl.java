package org.xutils.http;

import java.lang.reflect.Type;
import org.xutils.HttpManager;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.common.Callback.TypedCallback;
import org.xutils.common.TaskController;
import org.xutils.x;
import org.xutils.x.Ext;

public final class HttpManagerImpl
  implements HttpManager
{
  private static volatile HttpManagerImpl instance;
  private static final Object lock = new Object();
  
  public static void registerInstance()
  {
    if (instance == null) {
      synchronized (lock)
      {
        if (instance == null)
        {
          HttpManagerImpl localHttpManagerImpl = new org/xutils/http/HttpManagerImpl;
          localHttpManagerImpl.<init>();
          instance = localHttpManagerImpl;
        }
      }
    }
    x.Ext.setHttpManager(instance);
  }
  
  public <T> Callback.Cancelable get(RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback)
  {
    return request(HttpMethod.GET, paramRequestParams, paramCommonCallback);
  }
  
  public <T> T getSync(RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable
  {
    return (T)requestSync(HttpMethod.GET, paramRequestParams, paramClass);
  }
  
  public <T> Callback.Cancelable post(RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback)
  {
    return request(HttpMethod.POST, paramRequestParams, paramCommonCallback);
  }
  
  public <T> T postSync(RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable
  {
    return (T)requestSync(HttpMethod.POST, paramRequestParams, paramClass);
  }
  
  public <T> Callback.Cancelable request(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Callback.CommonCallback<T> paramCommonCallback)
  {
    paramRequestParams.setMethod(paramHttpMethod);
    if ((paramCommonCallback instanceof Callback.Cancelable)) {
      paramHttpMethod = (Callback.Cancelable)paramCommonCallback;
    } else {
      paramHttpMethod = null;
    }
    paramHttpMethod = new HttpTask(paramRequestParams, paramHttpMethod, paramCommonCallback);
    return x.task().start(paramHttpMethod);
  }
  
  public <T> T requestSync(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Class<T> paramClass)
    throws Throwable
  {
    return (T)requestSync(paramHttpMethod, paramRequestParams, new DefaultSyncCallback(paramClass));
  }
  
  public <T> T requestSync(HttpMethod paramHttpMethod, RequestParams paramRequestParams, Callback.TypedCallback<T> paramTypedCallback)
    throws Throwable
  {
    paramRequestParams.setMethod(paramHttpMethod);
    paramHttpMethod = new HttpTask(paramRequestParams, null, paramTypedCallback);
    return (T)x.task().startSync(paramHttpMethod);
  }
  
  private class DefaultSyncCallback<T>
    implements Callback.TypedCallback<T>
  {
    private final Class<T> resultType;
    
    public DefaultSyncCallback()
    {
      Class localClass;
      this.resultType = localClass;
    }
    
    public Type getLoadType()
    {
      return this.resultType;
    }
    
    public void onCancelled(Callback.CancelledException paramCancelledException) {}
    
    public void onError(Throwable paramThrowable, boolean paramBoolean) {}
    
    public void onFinished() {}
    
    public void onSuccess(T paramT) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/HttpManagerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */