package org.xutils.http.loader;

import java.io.InputStream;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.http.request.UriRequest;

class BooleanLoader
  extends Loader<Boolean>
{
  public Boolean load(InputStream paramInputStream)
    throws Throwable
  {
    return Boolean.valueOf(false);
  }
  
  public Boolean load(UriRequest paramUriRequest)
    throws Throwable
  {
    paramUriRequest.sendRequest();
    boolean bool;
    if (paramUriRequest.getResponseCode() < 300) {
      bool = true;
    } else {
      bool = false;
    }
    return Boolean.valueOf(bool);
  }
  
  public Boolean loadFromCache(DiskCacheEntity paramDiskCacheEntity)
    throws Throwable
  {
    return null;
  }
  
  public Loader<Boolean> newInstance()
  {
    return new BooleanLoader();
  }
  
  public void save2Cache(UriRequest paramUriRequest) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/loader/BooleanLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */