package org.xutils.http.loader;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

public final class LoaderFactory
{
  private static final HashMap<Type, Loader> converterHashMap = new HashMap();
  
  static
  {
    converterHashMap.put(JSONObject.class, new JSONObjectLoader());
    converterHashMap.put(JSONArray.class, new JSONArrayLoader());
    converterHashMap.put(String.class, new StringLoader());
    converterHashMap.put(File.class, new FileLoader());
    converterHashMap.put(byte[].class, new ByteArrayLoader());
    Object localObject = new BooleanLoader();
    converterHashMap.put(Boolean.TYPE, localObject);
    converterHashMap.put(Boolean.class, localObject);
    localObject = new IntegerLoader();
    converterHashMap.put(Integer.TYPE, localObject);
    converterHashMap.put(Integer.class, localObject);
  }
  
  public static Loader<?> getLoader(Type paramType, RequestParams paramRequestParams)
  {
    Loader localLoader = (Loader)converterHashMap.get(paramType);
    if (localLoader == null) {
      paramType = new ObjectLoader(paramType);
    } else {
      paramType = localLoader.newInstance();
    }
    paramType.setParams(paramRequestParams);
    return paramType;
  }
  
  public static <T> void registerLoader(Type paramType, Loader<T> paramLoader)
  {
    converterHashMap.put(paramType, paramLoader);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/loader/LoaderFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */