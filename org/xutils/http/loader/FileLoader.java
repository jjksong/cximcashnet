package org.xutils.http.loader;

import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.cache.DiskCacheFile;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.ProgressHandler;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

public class FileLoader
  extends Loader<File>
{
  private static final int CHECK_SIZE = 512;
  private long contentLength;
  private DiskCacheFile diskCacheFile;
  private boolean isAutoRename;
  private boolean isAutoResume;
  private String responseFileName;
  private String saveFilePath;
  private String tempSaveFilePath;
  
  private File autoRename(File paramFile)
  {
    Object localObject1;
    Object localObject2;
    if ((this.isAutoRename) && (paramFile.exists()) && (!TextUtils.isEmpty(this.responseFileName)))
    {
      for (localObject1 = new File(paramFile.getParent(), this.responseFileName); ((File)localObject1).exists(); localObject1 = new File((String)localObject2, ((StringBuilder)localObject1).toString()))
      {
        localObject2 = paramFile.getParent();
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(System.currentTimeMillis());
        ((StringBuilder)localObject1).append(this.responseFileName);
      }
      localObject2 = paramFile;
      if (paramFile.renameTo((File)localObject1)) {
        localObject2 = localObject1;
      }
      return (File)localObject2;
    }
    if (!this.saveFilePath.equals(this.tempSaveFilePath))
    {
      localObject2 = new File(this.saveFilePath);
      localObject1 = paramFile;
      if (paramFile.renameTo((File)localObject2)) {
        localObject1 = localObject2;
      }
      return (File)localObject1;
    }
    return paramFile;
  }
  
  private static String getResponseFileName(UriRequest paramUriRequest)
  {
    if (paramUriRequest == null) {
      return null;
    }
    String str = paramUriRequest.getResponseHeader("Content-Disposition");
    if (!TextUtils.isEmpty(str))
    {
      int i = str.indexOf("filename=");
      if (i > 0)
      {
        int k = i + 9;
        int j = str.indexOf(";", k);
        i = j;
        if (j < 0) {
          i = str.length();
        }
        if (i > k) {
          try
          {
            str = URLDecoder.decode(str.substring(k, i), paramUriRequest.getParams().getCharset());
            paramUriRequest = str;
            if (str.startsWith("\""))
            {
              paramUriRequest = str;
              if (str.endsWith("\"")) {
                paramUriRequest = str.substring(1, str.length() - 1);
              }
            }
            return paramUriRequest;
          }
          catch (UnsupportedEncodingException paramUriRequest)
          {
            LogUtil.e(paramUriRequest.getMessage(), paramUriRequest);
          }
        }
      }
    }
    return null;
  }
  
  private void initDiskCacheFile(UriRequest paramUriRequest)
    throws Throwable
  {
    Object localObject = new DiskCacheEntity();
    ((DiskCacheEntity)localObject).setKey(paramUriRequest.getCacheKey());
    this.diskCacheFile = LruDiskCache.getDiskCache(this.params.getCacheDirName()).createDiskCacheFile((DiskCacheEntity)localObject);
    localObject = this.diskCacheFile;
    if (localObject != null)
    {
      this.saveFilePath = ((DiskCacheFile)localObject).getAbsolutePath();
      this.tempSaveFilePath = this.saveFilePath;
      this.isAutoRename = false;
      return;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("create cache file error:");
    ((StringBuilder)localObject).append(paramUriRequest.getCacheKey());
    throw new IOException(((StringBuilder)localObject).toString());
  }
  
  private static boolean isSupportRange(UriRequest paramUriRequest)
  {
    boolean bool2 = false;
    if (paramUriRequest == null) {
      return false;
    }
    String str = paramUriRequest.getResponseHeader("Accept-Ranges");
    if (str != null) {
      return str.contains("bytes");
    }
    paramUriRequest = paramUriRequest.getResponseHeader("Content-Range");
    boolean bool1 = bool2;
    if (paramUriRequest != null)
    {
      bool1 = bool2;
      if (paramUriRequest.contains("bytes")) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public File load(InputStream paramInputStream)
    throws Throwable
  {
    ProgressHandler localProgressHandler = null;
    Object localObject10 = null;
    try
    {
      File localFile = new java/io/File;
      localFile.<init>(this.tempSaveFilePath);
      if (localFile.isDirectory()) {
        IOUtil.deleteFileOrDir(localFile);
      }
      Object localObject1;
      Object localObject7;
      if (!localFile.exists())
      {
        localObject1 = localFile.getParentFile();
        if ((!((File)localObject1).exists()) && (!((File)localObject1).mkdirs()))
        {
          localObject7 = new java/io/IOException;
          paramInputStream = new java/lang/StringBuilder;
          paramInputStream.<init>();
          paramInputStream.append("can not create dir: ");
          paramInputStream.append(((File)localObject1).getAbsolutePath());
          ((IOException)localObject7).<init>(paramInputStream.toString());
          throw ((Throwable)localObject7);
        }
      }
      long l1 = localFile.length();
      boolean bool = this.isAutoResume;
      if ((bool) && (l1 > 0L))
      {
        l2 = l1 - 512L;
        if (l2 > 0L) {}
        try
        {
          localObject7 = new java/io/FileInputStream;
          ((FileInputStream)localObject7).<init>(localFile);
          try
          {
            localObject1 = IOUtil.readBytes((InputStream)localObject7, l2, 512);
            if (Arrays.equals(IOUtil.readBytes(paramInputStream, 0L, 512), (byte[])localObject1))
            {
              this.contentLength -= 512L;
              IOUtil.closeQuietly((Closeable)localObject7);
              break label267;
            }
            IOUtil.closeQuietly((Closeable)localObject7);
            IOUtil.deleteFileOrDir(localFile);
            paramInputStream = new java/lang/RuntimeException;
            paramInputStream.<init>("need retry");
            throw paramInputStream;
          }
          finally
          {
            paramInputStream = (InputStream)localObject7;
            break label260;
          }
          IOUtil.deleteFileOrDir(localFile);
          paramInputStream = new java/lang/RuntimeException;
          paramInputStream.<init>("need retry");
          throw paramInputStream;
        }
        finally
        {
          paramInputStream = null;
          label260:
          IOUtil.closeQuietly(paramInputStream);
        }
      }
      label267:
      if (this.isAutoResume)
      {
        localObject7 = new java/io/FileOutputStream;
        ((FileOutputStream)localObject7).<init>(localFile, true);
      }
      else
      {
        localObject7 = new FileOutputStream(localFile);
        l1 = 0L;
      }
      long l2 = this.contentLength + l1;
      BufferedInputStream localBufferedInputStream = new java/io/BufferedInputStream;
      localBufferedInputStream.<init>(paramInputStream);
      paramInputStream = (InputStream)localObject10;
      label564:
      Object localObject9;
      try
      {
        Object localObject4 = new java/io/BufferedOutputStream;
        paramInputStream = (InputStream)localObject10;
        ((BufferedOutputStream)localObject4).<init>((OutputStream)localObject7);
        try
        {
          if (this.progressHandler != null)
          {
            localProgressHandler = this.progressHandler;
            localObject7 = localObject4;
            paramInputStream = (InputStream)localObject7;
            if (!localProgressHandler.updateProgress(l2, l1, true))
            {
              paramInputStream = (InputStream)localObject7;
              localObject4 = new org/xutils/common/Callback$CancelledException;
              paramInputStream = (InputStream)localObject7;
              ((Callback.CancelledException)localObject4).<init>("download stopped!");
              paramInputStream = (InputStream)localObject7;
              throw ((Throwable)localObject4);
            }
          }
          paramInputStream = (InputStream)localObject4;
          localObject7 = new byte['က'];
          for (;;)
          {
            paramInputStream = (InputStream)localObject4;
            int i = localBufferedInputStream.read((byte[])localObject7);
            if (i == -1) {
              break label564;
            }
            paramInputStream = (InputStream)localObject4;
            if (!localFile.getParentFile().exists()) {
              break;
            }
            paramInputStream = (InputStream)localObject4;
            ((BufferedOutputStream)localObject4).write((byte[])localObject7, 0, i);
            l1 = i + l1;
            paramInputStream = (InputStream)localObject4;
            if (this.progressHandler != null)
            {
              paramInputStream = (InputStream)localObject4;
              if (!this.progressHandler.updateProgress(l2, l1, false))
              {
                paramInputStream = (InputStream)localObject4;
                ((BufferedOutputStream)localObject4).flush();
                paramInputStream = (InputStream)localObject4;
                localObject7 = new org/xutils/common/Callback$CancelledException;
                paramInputStream = (InputStream)localObject4;
                ((Callback.CancelledException)localObject7).<init>("download stopped!");
                paramInputStream = (InputStream)localObject4;
                throw ((Throwable)localObject7);
              }
            }
          }
          paramInputStream = (InputStream)localObject4;
          localFile.getParentFile().mkdirs();
          paramInputStream = (InputStream)localObject4;
          localObject7 = new java/io/IOException;
          paramInputStream = (InputStream)localObject4;
          ((IOException)localObject7).<init>("parent be deleted!");
          paramInputStream = (InputStream)localObject4;
          throw ((Throwable)localObject7);
          paramInputStream = (InputStream)localObject4;
          ((BufferedOutputStream)localObject4).flush();
          localObject7 = localFile;
          paramInputStream = (InputStream)localObject4;
          if (this.diskCacheFile != null)
          {
            paramInputStream = (InputStream)localObject4;
            localObject7 = this.diskCacheFile.commit();
          }
          paramInputStream = (InputStream)localObject4;
          if (this.progressHandler != null)
          {
            paramInputStream = (InputStream)localObject4;
            this.progressHandler.updateProgress(l2, l1, true);
          }
          IOUtil.closeQuietly(localBufferedInputStream);
          IOUtil.closeQuietly((Closeable)localObject4);
          return autoRename((File)localObject7);
        }
        finally
        {
          paramInputStream = (InputStream)localObject4;
        }
        localObject6 = finally;
      }
      finally
      {
        localObject9 = localBufferedInputStream;
      }
      IOUtil.closeQuietly((Closeable)localObject9);
    }
    finally
    {
      paramInputStream = localProgressHandler;
    }
    IOUtil.closeQuietly(paramInputStream);
    throw ((Throwable)localObject6);
  }
  
  /* Error */
  public File load(UriRequest paramUriRequest)
    throws Throwable
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 10
    //   3: aconst_null
    //   4: astore 9
    //   6: aload 9
    //   8: astore 8
    //   10: aload 10
    //   12: astore 7
    //   14: aload_0
    //   15: aload_0
    //   16: getfield 166	org/xutils/http/loader/FileLoader:params	Lorg/xutils/http/RequestParams;
    //   19: invokevirtual 310	org/xutils/http/RequestParams:getSaveFilePath	()Ljava/lang/String;
    //   22: putfield 74	org/xutils/http/loader/FileLoader:saveFilePath	Ljava/lang/String;
    //   25: aload 9
    //   27: astore 8
    //   29: aload 10
    //   31: astore 7
    //   33: aload_0
    //   34: aconst_null
    //   35: putfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   38: aload 9
    //   40: astore 8
    //   42: aload 10
    //   44: astore 7
    //   46: aload_0
    //   47: getfield 74	org/xutils/http/loader/FileLoader:saveFilePath	Ljava/lang/String;
    //   50: invokestatic 42	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   53: ifeq +100 -> 153
    //   56: aload 9
    //   58: astore 8
    //   60: aload 10
    //   62: astore 7
    //   64: aload_0
    //   65: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   68: ifnull +69 -> 137
    //   71: aload 9
    //   73: astore 8
    //   75: aload 10
    //   77: astore 7
    //   79: aload_0
    //   80: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   83: lconst_0
    //   84: lconst_0
    //   85: iconst_0
    //   86: invokeinterface 280 6 0
    //   91: ifeq +6 -> 97
    //   94: goto +43 -> 137
    //   97: aload 9
    //   99: astore 8
    //   101: aload 10
    //   103: astore 7
    //   105: new 282	org/xutils/common/Callback$CancelledException
    //   108: astore 11
    //   110: aload 9
    //   112: astore 8
    //   114: aload 10
    //   116: astore 7
    //   118: aload 11
    //   120: ldc_w 284
    //   123: invokespecial 285	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   126: aload 9
    //   128: astore 8
    //   130: aload 10
    //   132: astore 7
    //   134: aload 11
    //   136: athrow
    //   137: aload 9
    //   139: astore 8
    //   141: aload 10
    //   143: astore 7
    //   145: aload_0
    //   146: aload_1
    //   147: invokespecial 312	org/xutils/http/loader/FileLoader:initDiskCacheFile	(Lorg/xutils/http/request/UriRequest;)V
    //   150: goto +81 -> 231
    //   153: aload 9
    //   155: astore 8
    //   157: aload 10
    //   159: astore 7
    //   161: new 51	java/lang/StringBuilder
    //   164: astore 11
    //   166: aload 9
    //   168: astore 8
    //   170: aload 10
    //   172: astore 7
    //   174: aload 11
    //   176: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   179: aload 9
    //   181: astore 8
    //   183: aload 10
    //   185: astore 7
    //   187: aload 11
    //   189: aload_0
    //   190: getfield 74	org/xutils/http/loader/FileLoader:saveFilePath	Ljava/lang/String;
    //   193: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload 9
    //   199: astore 8
    //   201: aload 10
    //   203: astore 7
    //   205: aload 11
    //   207: ldc_w 314
    //   210: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload 9
    //   216: astore 8
    //   218: aload 10
    //   220: astore 7
    //   222: aload_0
    //   223: aload 11
    //   225: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   228: putfield 76	org/xutils/http/loader/FileLoader:tempSaveFilePath	Ljava/lang/String;
    //   231: aload 9
    //   233: astore 8
    //   235: aload 10
    //   237: astore 7
    //   239: aload_0
    //   240: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   243: ifnull +69 -> 312
    //   246: aload 9
    //   248: astore 8
    //   250: aload 10
    //   252: astore 7
    //   254: aload_0
    //   255: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   258: lconst_0
    //   259: lconst_0
    //   260: iconst_0
    //   261: invokeinterface 280 6 0
    //   266: ifeq +6 -> 272
    //   269: goto +43 -> 312
    //   272: aload 9
    //   274: astore 8
    //   276: aload 10
    //   278: astore 7
    //   280: new 282	org/xutils/common/Callback$CancelledException
    //   283: astore 11
    //   285: aload 9
    //   287: astore 8
    //   289: aload 10
    //   291: astore 7
    //   293: aload 11
    //   295: ldc_w 284
    //   298: invokespecial 285	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   301: aload 9
    //   303: astore 8
    //   305: aload 10
    //   307: astore 7
    //   309: aload 11
    //   311: athrow
    //   312: aload 9
    //   314: astore 8
    //   316: aload 10
    //   318: astore 7
    //   320: new 51	java/lang/StringBuilder
    //   323: astore 11
    //   325: aload 9
    //   327: astore 8
    //   329: aload 10
    //   331: astore 7
    //   333: aload 11
    //   335: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   338: aload 9
    //   340: astore 8
    //   342: aload 10
    //   344: astore 7
    //   346: aload 11
    //   348: aload_0
    //   349: getfield 74	org/xutils/http/loader/FileLoader:saveFilePath	Ljava/lang/String;
    //   352: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   355: pop
    //   356: aload 9
    //   358: astore 8
    //   360: aload 10
    //   362: astore 7
    //   364: aload 11
    //   366: ldc_w 316
    //   369: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: pop
    //   373: aload 9
    //   375: astore 8
    //   377: aload 10
    //   379: astore 7
    //   381: aload 11
    //   383: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   386: iconst_1
    //   387: invokestatic 322	org/xutils/common/util/ProcessLock:tryLock	(Ljava/lang/String;Z)Lorg/xutils/common/util/ProcessLock;
    //   390: astore 9
    //   392: aload 9
    //   394: ifnull +655 -> 1049
    //   397: aload 9
    //   399: astore 8
    //   401: aload 9
    //   403: astore 7
    //   405: aload 9
    //   407: invokevirtual 325	org/xutils/common/util/ProcessLock:isValid	()Z
    //   410: ifeq +639 -> 1049
    //   413: aload 9
    //   415: astore 8
    //   417: aload 9
    //   419: astore 7
    //   421: aload_0
    //   422: aload_1
    //   423: invokevirtual 120	org/xutils/http/request/UriRequest:getParams	()Lorg/xutils/http/RequestParams;
    //   426: putfield 166	org/xutils/http/loader/FileLoader:params	Lorg/xutils/http/RequestParams;
    //   429: aload 9
    //   431: astore 8
    //   433: aload 9
    //   435: astore 7
    //   437: aload_0
    //   438: getfield 227	org/xutils/http/loader/FileLoader:isAutoResume	Z
    //   441: istore 6
    //   443: lconst_0
    //   444: lstore 4
    //   446: lload 4
    //   448: lstore_2
    //   449: iload 6
    //   451: ifeq +81 -> 532
    //   454: aload 9
    //   456: astore 8
    //   458: aload 9
    //   460: astore 7
    //   462: new 30	java/io/File
    //   465: astore 10
    //   467: aload 9
    //   469: astore 8
    //   471: aload 9
    //   473: astore 7
    //   475: aload 10
    //   477: aload_0
    //   478: getfield 76	org/xutils/http/loader/FileLoader:tempSaveFilePath	Ljava/lang/String;
    //   481: invokespecial 85	java/io/File:<init>	(Ljava/lang/String;)V
    //   484: aload 9
    //   486: astore 8
    //   488: aload 9
    //   490: astore 7
    //   492: aload 10
    //   494: invokevirtual 225	java/io/File:length	()J
    //   497: lstore_2
    //   498: lload_2
    //   499: ldc2_w 228
    //   502: lcmp
    //   503: ifgt +23 -> 526
    //   506: aload 9
    //   508: astore 8
    //   510: aload 9
    //   512: astore 7
    //   514: aload 10
    //   516: invokestatic 213	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   519: pop
    //   520: lload 4
    //   522: lstore_2
    //   523: goto +9 -> 532
    //   526: lload_2
    //   527: ldc2_w 228
    //   530: lsub
    //   531: lstore_2
    //   532: aload 9
    //   534: astore 8
    //   536: aload 9
    //   538: astore 7
    //   540: aload_0
    //   541: getfield 166	org/xutils/http/loader/FileLoader:params	Lorg/xutils/http/RequestParams;
    //   544: astore 11
    //   546: aload 9
    //   548: astore 8
    //   550: aload 9
    //   552: astore 7
    //   554: new 51	java/lang/StringBuilder
    //   557: astore 10
    //   559: aload 9
    //   561: astore 8
    //   563: aload 9
    //   565: astore 7
    //   567: aload 10
    //   569: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   572: aload 9
    //   574: astore 8
    //   576: aload 9
    //   578: astore 7
    //   580: aload 10
    //   582: ldc_w 327
    //   585: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   588: pop
    //   589: aload 9
    //   591: astore 8
    //   593: aload 9
    //   595: astore 7
    //   597: aload 10
    //   599: lload_2
    //   600: invokevirtual 62	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   603: pop
    //   604: aload 9
    //   606: astore 8
    //   608: aload 9
    //   610: astore 7
    //   612: aload 10
    //   614: ldc_w 329
    //   617: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   620: pop
    //   621: aload 9
    //   623: astore 8
    //   625: aload 9
    //   627: astore 7
    //   629: aload 11
    //   631: ldc_w 331
    //   634: aload 10
    //   636: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   639: invokevirtual 334	org/xutils/http/RequestParams:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
    //   642: aload 9
    //   644: astore 8
    //   646: aload 9
    //   648: astore 7
    //   650: aload_0
    //   651: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   654: ifnull +69 -> 723
    //   657: aload 9
    //   659: astore 8
    //   661: aload 9
    //   663: astore 7
    //   665: aload_0
    //   666: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   669: lconst_0
    //   670: lconst_0
    //   671: iconst_0
    //   672: invokeinterface 280 6 0
    //   677: ifeq +6 -> 683
    //   680: goto +43 -> 723
    //   683: aload 9
    //   685: astore 8
    //   687: aload 9
    //   689: astore 7
    //   691: new 282	org/xutils/common/Callback$CancelledException
    //   694: astore 10
    //   696: aload 9
    //   698: astore 8
    //   700: aload 9
    //   702: astore 7
    //   704: aload 10
    //   706: ldc_w 284
    //   709: invokespecial 285	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   712: aload 9
    //   714: astore 8
    //   716: aload 9
    //   718: astore 7
    //   720: aload 10
    //   722: athrow
    //   723: aload 9
    //   725: astore 8
    //   727: aload 9
    //   729: astore 7
    //   731: aload_1
    //   732: invokevirtual 337	org/xutils/http/request/UriRequest:sendRequest	()V
    //   735: aload 9
    //   737: astore 8
    //   739: aload 9
    //   741: astore 7
    //   743: aload_0
    //   744: aload_1
    //   745: invokevirtual 340	org/xutils/http/request/UriRequest:getContentLength	()J
    //   748: putfield 245	org/xutils/http/loader/FileLoader:contentLength	J
    //   751: aload 9
    //   753: astore 8
    //   755: aload 9
    //   757: astore 7
    //   759: aload_0
    //   760: getfield 28	org/xutils/http/loader/FileLoader:isAutoRename	Z
    //   763: ifeq +19 -> 782
    //   766: aload 9
    //   768: astore 8
    //   770: aload 9
    //   772: astore 7
    //   774: aload_0
    //   775: aload_1
    //   776: invokestatic 342	org/xutils/http/loader/FileLoader:getResponseFileName	(Lorg/xutils/http/request/UriRequest;)Ljava/lang/String;
    //   779: putfield 36	org/xutils/http/loader/FileLoader:responseFileName	Ljava/lang/String;
    //   782: aload 9
    //   784: astore 8
    //   786: aload 9
    //   788: astore 7
    //   790: aload_0
    //   791: getfield 227	org/xutils/http/loader/FileLoader:isAutoResume	Z
    //   794: ifeq +19 -> 813
    //   797: aload 9
    //   799: astore 8
    //   801: aload 9
    //   803: astore 7
    //   805: aload_0
    //   806: aload_1
    //   807: invokestatic 344	org/xutils/http/loader/FileLoader:isSupportRange	(Lorg/xutils/http/request/UriRequest;)Z
    //   810: putfield 227	org/xutils/http/loader/FileLoader:isAutoResume	Z
    //   813: aload 9
    //   815: astore 8
    //   817: aload 9
    //   819: astore 7
    //   821: aload_0
    //   822: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   825: ifnull +69 -> 894
    //   828: aload 9
    //   830: astore 8
    //   832: aload 9
    //   834: astore 7
    //   836: aload_0
    //   837: getfield 274	org/xutils/http/loader/FileLoader:progressHandler	Lorg/xutils/http/ProgressHandler;
    //   840: lconst_0
    //   841: lconst_0
    //   842: iconst_0
    //   843: invokeinterface 280 6 0
    //   848: ifeq +6 -> 854
    //   851: goto +43 -> 894
    //   854: aload 9
    //   856: astore 8
    //   858: aload 9
    //   860: astore 7
    //   862: new 282	org/xutils/common/Callback$CancelledException
    //   865: astore 10
    //   867: aload 9
    //   869: astore 8
    //   871: aload 9
    //   873: astore 7
    //   875: aload 10
    //   877: ldc_w 284
    //   880: invokespecial 285	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   883: aload 9
    //   885: astore 8
    //   887: aload 9
    //   889: astore 7
    //   891: aload 10
    //   893: athrow
    //   894: aload 9
    //   896: astore 8
    //   898: aload 9
    //   900: astore 7
    //   902: aload_0
    //   903: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   906: ifnull +115 -> 1021
    //   909: aload 9
    //   911: astore 8
    //   913: aload 9
    //   915: astore 7
    //   917: aload_0
    //   918: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   921: invokevirtual 348	org/xutils/cache/DiskCacheFile:getCacheEntity	()Lorg/xutils/cache/DiskCacheEntity;
    //   924: astore 10
    //   926: aload 9
    //   928: astore 8
    //   930: aload 9
    //   932: astore 7
    //   934: aload 10
    //   936: invokestatic 58	java/lang/System:currentTimeMillis	()J
    //   939: invokevirtual 352	org/xutils/cache/DiskCacheEntity:setLastAccess	(J)V
    //   942: aload 9
    //   944: astore 8
    //   946: aload 9
    //   948: astore 7
    //   950: aload 10
    //   952: aload_1
    //   953: invokevirtual 355	org/xutils/http/request/UriRequest:getETag	()Ljava/lang/String;
    //   956: invokevirtual 358	org/xutils/cache/DiskCacheEntity:setEtag	(Ljava/lang/String;)V
    //   959: aload 9
    //   961: astore 8
    //   963: aload 9
    //   965: astore 7
    //   967: aload 10
    //   969: aload_1
    //   970: invokevirtual 361	org/xutils/http/request/UriRequest:getExpiration	()J
    //   973: invokevirtual 364	org/xutils/cache/DiskCacheEntity:setExpires	(J)V
    //   976: aload 9
    //   978: astore 8
    //   980: aload 9
    //   982: astore 7
    //   984: new 366	java/util/Date
    //   987: astore 11
    //   989: aload 9
    //   991: astore 8
    //   993: aload 9
    //   995: astore 7
    //   997: aload 11
    //   999: aload_1
    //   1000: invokevirtual 369	org/xutils/http/request/UriRequest:getLastModified	()J
    //   1003: invokespecial 371	java/util/Date:<init>	(J)V
    //   1006: aload 9
    //   1008: astore 8
    //   1010: aload 9
    //   1012: astore 7
    //   1014: aload 10
    //   1016: aload 11
    //   1018: invokevirtual 375	org/xutils/cache/DiskCacheEntity:setLastModify	(Ljava/util/Date;)V
    //   1021: aload 9
    //   1023: astore 8
    //   1025: aload 9
    //   1027: astore 7
    //   1029: aload_0
    //   1030: aload_1
    //   1031: invokevirtual 379	org/xutils/http/request/UriRequest:getInputStream	()Ljava/io/InputStream;
    //   1034: invokevirtual 381	org/xutils/http/loader/FileLoader:load	(Ljava/io/InputStream;)Ljava/io/File;
    //   1037: astore 10
    //   1039: aload 9
    //   1041: astore 7
    //   1043: aload 10
    //   1045: astore_1
    //   1046: goto +222 -> 1268
    //   1049: aload 9
    //   1051: astore 8
    //   1053: aload 9
    //   1055: astore 7
    //   1057: new 383	org/xutils/ex/FileLockedException
    //   1060: astore 10
    //   1062: aload 9
    //   1064: astore 8
    //   1066: aload 9
    //   1068: astore 7
    //   1070: new 51	java/lang/StringBuilder
    //   1073: astore 11
    //   1075: aload 9
    //   1077: astore 8
    //   1079: aload 9
    //   1081: astore 7
    //   1083: aload 11
    //   1085: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   1088: aload 9
    //   1090: astore 8
    //   1092: aload 9
    //   1094: astore 7
    //   1096: aload 11
    //   1098: ldc_w 385
    //   1101: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1104: pop
    //   1105: aload 9
    //   1107: astore 8
    //   1109: aload 9
    //   1111: astore 7
    //   1113: aload 11
    //   1115: aload_0
    //   1116: getfield 74	org/xutils/http/loader/FileLoader:saveFilePath	Ljava/lang/String;
    //   1119: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1122: pop
    //   1123: aload 9
    //   1125: astore 8
    //   1127: aload 9
    //   1129: astore 7
    //   1131: aload 10
    //   1133: aload 11
    //   1135: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1138: invokespecial 386	org/xutils/ex/FileLockedException:<init>	(Ljava/lang/String;)V
    //   1141: aload 9
    //   1143: astore 8
    //   1145: aload 9
    //   1147: astore 7
    //   1149: aload 10
    //   1151: athrow
    //   1152: astore_1
    //   1153: goto +221 -> 1374
    //   1156: astore 9
    //   1158: aload 7
    //   1160: astore 8
    //   1162: aload 9
    //   1164: invokevirtual 389	org/xutils/ex/HttpException:getCode	()I
    //   1167: sipush 416
    //   1170: if_icmpne +197 -> 1367
    //   1173: aload 7
    //   1175: astore 8
    //   1177: aload_0
    //   1178: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   1181: ifnull +19 -> 1200
    //   1184: aload 7
    //   1186: astore 8
    //   1188: aload_0
    //   1189: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   1192: invokevirtual 302	org/xutils/cache/DiskCacheFile:commit	()Lorg/xutils/cache/DiskCacheFile;
    //   1195: astore 9
    //   1197: goto +20 -> 1217
    //   1200: aload 7
    //   1202: astore 8
    //   1204: new 30	java/io/File
    //   1207: dup
    //   1208: aload_0
    //   1209: getfield 76	org/xutils/http/loader/FileLoader:tempSaveFilePath	Ljava/lang/String;
    //   1212: invokespecial 85	java/io/File:<init>	(Ljava/lang/String;)V
    //   1215: astore 9
    //   1217: aload 9
    //   1219: ifnull +63 -> 1282
    //   1222: aload 7
    //   1224: astore 8
    //   1226: aload 9
    //   1228: invokevirtual 34	java/io/File:exists	()Z
    //   1231: ifeq +51 -> 1282
    //   1234: aload 7
    //   1236: astore 8
    //   1238: aload_0
    //   1239: getfield 28	org/xutils/http/loader/FileLoader:isAutoRename	Z
    //   1242: ifeq +15 -> 1257
    //   1245: aload 7
    //   1247: astore 8
    //   1249: aload_0
    //   1250: aload_1
    //   1251: invokestatic 342	org/xutils/http/loader/FileLoader:getResponseFileName	(Lorg/xutils/http/request/UriRequest;)Ljava/lang/String;
    //   1254: putfield 36	org/xutils/http/loader/FileLoader:responseFileName	Ljava/lang/String;
    //   1257: aload 7
    //   1259: astore 8
    //   1261: aload_0
    //   1262: aload 9
    //   1264: invokespecial 304	org/xutils/http/loader/FileLoader:autoRename	(Ljava/io/File;)Ljava/io/File;
    //   1267: astore_1
    //   1268: aload 7
    //   1270: invokestatic 249	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   1273: aload_0
    //   1274: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   1277: invokestatic 249	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   1280: aload_1
    //   1281: areturn
    //   1282: aload 7
    //   1284: astore 8
    //   1286: aload 9
    //   1288: invokestatic 213	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   1291: pop
    //   1292: aload 7
    //   1294: astore 8
    //   1296: new 391	java/lang/IllegalStateException
    //   1299: astore 10
    //   1301: aload 7
    //   1303: astore 8
    //   1305: new 51	java/lang/StringBuilder
    //   1308: astore 9
    //   1310: aload 7
    //   1312: astore 8
    //   1314: aload 9
    //   1316: invokespecial 52	java/lang/StringBuilder:<init>	()V
    //   1319: aload 7
    //   1321: astore 8
    //   1323: aload 9
    //   1325: ldc_w 393
    //   1328: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1331: pop
    //   1332: aload 7
    //   1334: astore 8
    //   1336: aload 9
    //   1338: aload_1
    //   1339: invokevirtual 159	org/xutils/http/request/UriRequest:getCacheKey	()Ljava/lang/String;
    //   1342: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1345: pop
    //   1346: aload 7
    //   1348: astore 8
    //   1350: aload 10
    //   1352: aload 9
    //   1354: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1357: invokespecial 394	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   1360: aload 7
    //   1362: astore 8
    //   1364: aload 10
    //   1366: athrow
    //   1367: aload 7
    //   1369: astore 8
    //   1371: aload 9
    //   1373: athrow
    //   1374: aload 8
    //   1376: invokestatic 249	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   1379: aload_0
    //   1380: getfield 181	org/xutils/http/loader/FileLoader:diskCacheFile	Lorg/xutils/cache/DiskCacheFile;
    //   1383: invokestatic 249	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   1386: aload_1
    //   1387: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1388	0	this	FileLoader
    //   0	1388	1	paramUriRequest	UriRequest
    //   448	152	2	l1	long
    //   444	77	4	l2	long
    //   441	9	6	bool	boolean
    //   12	1356	7	localObject1	Object
    //   8	1367	8	localObject2	Object
    //   4	1142	9	localProcessLock	org.xutils.common.util.ProcessLock
    //   1156	7	9	localHttpException	org.xutils.ex.HttpException
    //   1195	177	9	localObject3	Object
    //   1	1364	10	localObject4	Object
    //   108	1026	11	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   14	25	1152	finally
    //   33	38	1152	finally
    //   46	56	1152	finally
    //   64	71	1152	finally
    //   79	94	1152	finally
    //   105	110	1152	finally
    //   118	126	1152	finally
    //   134	137	1152	finally
    //   145	150	1152	finally
    //   161	166	1152	finally
    //   174	179	1152	finally
    //   187	197	1152	finally
    //   205	214	1152	finally
    //   222	231	1152	finally
    //   239	246	1152	finally
    //   254	269	1152	finally
    //   280	285	1152	finally
    //   293	301	1152	finally
    //   309	312	1152	finally
    //   320	325	1152	finally
    //   333	338	1152	finally
    //   346	356	1152	finally
    //   364	373	1152	finally
    //   381	392	1152	finally
    //   405	413	1152	finally
    //   421	429	1152	finally
    //   437	443	1152	finally
    //   462	467	1152	finally
    //   475	484	1152	finally
    //   492	498	1152	finally
    //   514	520	1152	finally
    //   540	546	1152	finally
    //   554	559	1152	finally
    //   567	572	1152	finally
    //   580	589	1152	finally
    //   597	604	1152	finally
    //   612	621	1152	finally
    //   629	642	1152	finally
    //   650	657	1152	finally
    //   665	680	1152	finally
    //   691	696	1152	finally
    //   704	712	1152	finally
    //   720	723	1152	finally
    //   731	735	1152	finally
    //   743	751	1152	finally
    //   759	766	1152	finally
    //   774	782	1152	finally
    //   790	797	1152	finally
    //   805	813	1152	finally
    //   821	828	1152	finally
    //   836	851	1152	finally
    //   862	867	1152	finally
    //   875	883	1152	finally
    //   891	894	1152	finally
    //   902	909	1152	finally
    //   917	926	1152	finally
    //   934	942	1152	finally
    //   950	959	1152	finally
    //   967	976	1152	finally
    //   984	989	1152	finally
    //   997	1006	1152	finally
    //   1014	1021	1152	finally
    //   1029	1039	1152	finally
    //   1057	1062	1152	finally
    //   1070	1075	1152	finally
    //   1083	1088	1152	finally
    //   1096	1105	1152	finally
    //   1113	1123	1152	finally
    //   1131	1141	1152	finally
    //   1149	1152	1152	finally
    //   1162	1173	1152	finally
    //   1177	1184	1152	finally
    //   1188	1197	1152	finally
    //   1204	1217	1152	finally
    //   1226	1234	1152	finally
    //   1238	1245	1152	finally
    //   1249	1257	1152	finally
    //   1261	1268	1152	finally
    //   1286	1292	1152	finally
    //   1296	1301	1152	finally
    //   1305	1310	1152	finally
    //   1314	1319	1152	finally
    //   1323	1332	1152	finally
    //   1336	1346	1152	finally
    //   1350	1360	1152	finally
    //   1364	1367	1152	finally
    //   1371	1374	1152	finally
    //   14	25	1156	org/xutils/ex/HttpException
    //   33	38	1156	org/xutils/ex/HttpException
    //   46	56	1156	org/xutils/ex/HttpException
    //   64	71	1156	org/xutils/ex/HttpException
    //   79	94	1156	org/xutils/ex/HttpException
    //   105	110	1156	org/xutils/ex/HttpException
    //   118	126	1156	org/xutils/ex/HttpException
    //   134	137	1156	org/xutils/ex/HttpException
    //   145	150	1156	org/xutils/ex/HttpException
    //   161	166	1156	org/xutils/ex/HttpException
    //   174	179	1156	org/xutils/ex/HttpException
    //   187	197	1156	org/xutils/ex/HttpException
    //   205	214	1156	org/xutils/ex/HttpException
    //   222	231	1156	org/xutils/ex/HttpException
    //   239	246	1156	org/xutils/ex/HttpException
    //   254	269	1156	org/xutils/ex/HttpException
    //   280	285	1156	org/xutils/ex/HttpException
    //   293	301	1156	org/xutils/ex/HttpException
    //   309	312	1156	org/xutils/ex/HttpException
    //   320	325	1156	org/xutils/ex/HttpException
    //   333	338	1156	org/xutils/ex/HttpException
    //   346	356	1156	org/xutils/ex/HttpException
    //   364	373	1156	org/xutils/ex/HttpException
    //   381	392	1156	org/xutils/ex/HttpException
    //   405	413	1156	org/xutils/ex/HttpException
    //   421	429	1156	org/xutils/ex/HttpException
    //   437	443	1156	org/xutils/ex/HttpException
    //   462	467	1156	org/xutils/ex/HttpException
    //   475	484	1156	org/xutils/ex/HttpException
    //   492	498	1156	org/xutils/ex/HttpException
    //   514	520	1156	org/xutils/ex/HttpException
    //   540	546	1156	org/xutils/ex/HttpException
    //   554	559	1156	org/xutils/ex/HttpException
    //   567	572	1156	org/xutils/ex/HttpException
    //   580	589	1156	org/xutils/ex/HttpException
    //   597	604	1156	org/xutils/ex/HttpException
    //   612	621	1156	org/xutils/ex/HttpException
    //   629	642	1156	org/xutils/ex/HttpException
    //   650	657	1156	org/xutils/ex/HttpException
    //   665	680	1156	org/xutils/ex/HttpException
    //   691	696	1156	org/xutils/ex/HttpException
    //   704	712	1156	org/xutils/ex/HttpException
    //   720	723	1156	org/xutils/ex/HttpException
    //   731	735	1156	org/xutils/ex/HttpException
    //   743	751	1156	org/xutils/ex/HttpException
    //   759	766	1156	org/xutils/ex/HttpException
    //   774	782	1156	org/xutils/ex/HttpException
    //   790	797	1156	org/xutils/ex/HttpException
    //   805	813	1156	org/xutils/ex/HttpException
    //   821	828	1156	org/xutils/ex/HttpException
    //   836	851	1156	org/xutils/ex/HttpException
    //   862	867	1156	org/xutils/ex/HttpException
    //   875	883	1156	org/xutils/ex/HttpException
    //   891	894	1156	org/xutils/ex/HttpException
    //   902	909	1156	org/xutils/ex/HttpException
    //   917	926	1156	org/xutils/ex/HttpException
    //   934	942	1156	org/xutils/ex/HttpException
    //   950	959	1156	org/xutils/ex/HttpException
    //   967	976	1156	org/xutils/ex/HttpException
    //   984	989	1156	org/xutils/ex/HttpException
    //   997	1006	1156	org/xutils/ex/HttpException
    //   1014	1021	1156	org/xutils/ex/HttpException
    //   1029	1039	1156	org/xutils/ex/HttpException
    //   1057	1062	1156	org/xutils/ex/HttpException
    //   1070	1075	1156	org/xutils/ex/HttpException
    //   1083	1088	1156	org/xutils/ex/HttpException
    //   1096	1105	1156	org/xutils/ex/HttpException
    //   1113	1123	1156	org/xutils/ex/HttpException
    //   1131	1141	1156	org/xutils/ex/HttpException
    //   1149	1152	1156	org/xutils/ex/HttpException
  }
  
  public File loadFromCache(DiskCacheEntity paramDiskCacheEntity)
    throws Throwable
  {
    return LruDiskCache.getDiskCache(this.params.getCacheDirName()).getDiskCacheFile(paramDiskCacheEntity.getKey());
  }
  
  public Loader<File> newInstance()
  {
    return new FileLoader();
  }
  
  public void save2Cache(UriRequest paramUriRequest) {}
  
  public void setParams(RequestParams paramRequestParams)
  {
    if (paramRequestParams != null)
    {
      this.params = paramRequestParams;
      this.isAutoResume = paramRequestParams.isAutoResume();
      this.isAutoRename = paramRequestParams.isAutoRename();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/loader/FileLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */