package org.xutils.http.loader;

import android.text.TextUtils;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.ParameterizedTypeUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpResponse;
import org.xutils.http.app.InputStreamResponseParser;
import org.xutils.http.app.ResponseParser;
import org.xutils.http.request.UriRequest;

class ObjectLoader
  extends Loader<Object>
{
  private String charset = "UTF-8";
  private final Class<?> objectClass;
  private final Type objectType;
  private final ResponseParser parser;
  private String resultStr = null;
  
  public ObjectLoader(Type paramType)
  {
    this.objectType = paramType;
    if ((paramType instanceof ParameterizedType))
    {
      this.objectClass = ((Class)((ParameterizedType)paramType).getRawType());
    }
    else
    {
      if ((paramType instanceof TypeVariable)) {
        break label315;
      }
      this.objectClass = ((Class)paramType);
    }
    if (List.class.equals(this.objectClass))
    {
      localObject = ParameterizedTypeUtil.getParameterizedType(this.objectType, List.class, 0);
      if ((localObject instanceof ParameterizedType))
      {
        paramType = (Class)((ParameterizedType)localObject).getRawType();
      }
      else
      {
        if ((localObject instanceof TypeVariable)) {
          break label197;
        }
        paramType = (Class)localObject;
      }
      paramType = (HttpResponse)paramType.getAnnotation(HttpResponse.class);
      if (paramType != null)
      {
        try
        {
          this.parser = ((ResponseParser)paramType.parser().newInstance());
        }
        catch (Throwable paramType)
        {
          throw new RuntimeException("create parser error", paramType);
        }
      }
      else
      {
        paramType = new StringBuilder();
        paramType.append("not found @HttpResponse from ");
        paramType.append(localObject);
        throw new IllegalArgumentException(paramType.toString());
        label197:
        paramType = new StringBuilder();
        paramType.append("not support callback type ");
        paramType.append(localObject.toString());
        throw new IllegalArgumentException(paramType.toString());
      }
    }
    else
    {
      paramType = (HttpResponse)this.objectClass.getAnnotation(HttpResponse.class);
      if (paramType == null) {
        break label279;
      }
    }
    try
    {
      this.parser = ((ResponseParser)paramType.parser().newInstance());
      return;
    }
    catch (Throwable paramType)
    {
      throw new RuntimeException("create parser error", paramType);
    }
    label279:
    paramType = new StringBuilder();
    paramType.append("not found @HttpResponse from ");
    paramType.append(this.objectType);
    throw new IllegalArgumentException(paramType.toString());
    label315:
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("not support callback type ");
    ((StringBuilder)localObject).append(paramType.toString());
    throw new IllegalArgumentException(((StringBuilder)localObject).toString());
  }
  
  public Object load(InputStream paramInputStream)
    throws Throwable
  {
    ResponseParser localResponseParser = this.parser;
    if ((localResponseParser instanceof InputStreamResponseParser))
    {
      paramInputStream = ((InputStreamResponseParser)localResponseParser).parse(this.objectType, this.objectClass, paramInputStream);
    }
    else
    {
      this.resultStr = IOUtil.readStr(paramInputStream, this.charset);
      paramInputStream = this.parser.parse(this.objectType, this.objectClass, this.resultStr);
    }
    return paramInputStream;
  }
  
  public Object load(UriRequest paramUriRequest)
    throws Throwable
  {
    try
    {
      paramUriRequest.sendRequest();
      return load(paramUriRequest.getInputStream());
    }
    finally
    {
      this.parser.checkResponse(paramUriRequest);
    }
  }
  
  public Object loadFromCache(DiskCacheEntity paramDiskCacheEntity)
    throws Throwable
  {
    if (paramDiskCacheEntity != null)
    {
      paramDiskCacheEntity = paramDiskCacheEntity.getTextContent();
      if (!TextUtils.isEmpty(paramDiskCacheEntity)) {
        return this.parser.parse(this.objectType, this.objectClass, paramDiskCacheEntity);
      }
    }
    return null;
  }
  
  public Loader<Object> newInstance()
  {
    throw new IllegalAccessError("use constructor create ObjectLoader.");
  }
  
  public void save2Cache(UriRequest paramUriRequest)
  {
    saveStringCache(paramUriRequest, this.resultStr);
  }
  
  public void setParams(RequestParams paramRequestParams)
  {
    if (paramRequestParams != null)
    {
      paramRequestParams = paramRequestParams.getCharset();
      if (!TextUtils.isEmpty(paramRequestParams)) {
        this.charset = paramRequestParams;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/loader/ObjectLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */