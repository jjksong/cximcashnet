package org.xutils.http.loader;

import android.text.TextUtils;
import java.io.InputStream;
import org.json.JSONObject;
import org.xutils.cache.DiskCacheEntity;
import org.xutils.common.util.IOUtil;
import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

class JSONObjectLoader
  extends Loader<JSONObject>
{
  private String charset = "UTF-8";
  private String resultStr = null;
  
  public JSONObject load(InputStream paramInputStream)
    throws Throwable
  {
    this.resultStr = IOUtil.readStr(paramInputStream, this.charset);
    return new JSONObject(this.resultStr);
  }
  
  public JSONObject load(UriRequest paramUriRequest)
    throws Throwable
  {
    paramUriRequest.sendRequest();
    return load(paramUriRequest.getInputStream());
  }
  
  public JSONObject loadFromCache(DiskCacheEntity paramDiskCacheEntity)
    throws Throwable
  {
    if (paramDiskCacheEntity != null)
    {
      paramDiskCacheEntity = paramDiskCacheEntity.getTextContent();
      if (!TextUtils.isEmpty(paramDiskCacheEntity)) {
        return new JSONObject(paramDiskCacheEntity);
      }
    }
    return null;
  }
  
  public Loader<JSONObject> newInstance()
  {
    return new JSONObjectLoader();
  }
  
  public void save2Cache(UriRequest paramUriRequest)
  {
    saveStringCache(paramUriRequest, this.resultStr);
  }
  
  public void setParams(RequestParams paramRequestParams)
  {
    if (paramRequestParams != null)
    {
      paramRequestParams = paramRequestParams.getCharset();
      if (!TextUtils.isEmpty(paramRequestParams)) {
        this.charset = paramRequestParams;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/http/loader/JSONObjectLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */