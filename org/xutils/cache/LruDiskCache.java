package org.xutils.cache;

import android.text.TextUtils;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import org.xutils.DbManager;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.FileUtil;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.common.util.MD5;
import org.xutils.common.util.ProcessLock;
import org.xutils.config.DbConfigs;
import org.xutils.db.Selector;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.ex.DbException;
import org.xutils.ex.FileLockedException;
import org.xutils.x;

public final class LruDiskCache
{
  private static final String CACHE_DIR_NAME = "xUtils_cache";
  private static final HashMap<String, LruDiskCache> DISK_CACHE_MAP = new HashMap(5);
  private static final int LIMIT_COUNT = 5000;
  private static final long LIMIT_SIZE = 104857600L;
  private static final int LOCK_WAIT = 3000;
  private static final String TEMP_FILE_SUFFIX = ".tmp";
  private static final long TRIM_TIME_SPAN = 1000L;
  private boolean available = false;
  private final DbManager cacheDb = x.getDb(DbConfigs.HTTP.getConfig());
  private File cacheDir;
  private long diskCacheSize = 104857600L;
  private long lastTrimTime = 0L;
  private final Executor trimExecutor = new PriorityExecutor(1, true);
  
  private LruDiskCache(String paramString)
  {
    this.cacheDir = FileUtil.getCacheDir(paramString);
    paramString = this.cacheDir;
    if ((paramString != null) && ((paramString.exists()) || (this.cacheDir.mkdirs()))) {
      this.available = true;
    }
    deleteNoIndexFiles();
  }
  
  private void deleteExpiry()
  {
    try
    {
      Object localObject1 = WhereBuilder.b("expires", "<", Long.valueOf(System.currentTimeMillis()));
      Object localObject2 = this.cacheDb.selector(DiskCacheEntity.class).where((WhereBuilder)localObject1).findAll();
      this.cacheDb.delete(DiskCacheEntity.class, (WhereBuilder)localObject1);
      if ((localObject2 != null) && (((List)localObject2).size() > 0))
      {
        localObject1 = ((List)localObject2).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = ((DiskCacheEntity)((Iterator)localObject1).next()).getPath();
          if (!TextUtils.isEmpty((CharSequence)localObject2)) {
            deleteFileWithLock((String)localObject2);
          }
        }
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      LogUtil.e(localThrowable.getMessage(), localThrowable);
    }
  }
  
  private boolean deleteFileWithLock(String paramString)
  {
    try
    {
      localProcessLock = ProcessLock.tryLock(paramString, true);
      if (localProcessLock != null) {
        try
        {
          if (localProcessLock.isValid())
          {
            File localFile = new java/io/File;
            localFile.<init>(paramString);
            boolean bool = IOUtil.deleteFileOrDir(localFile);
            IOUtil.closeQuietly(localProcessLock);
            return bool;
          }
        }
        finally
        {
          break label53;
        }
      }
      IOUtil.closeQuietly(localProcessLock);
      return false;
    }
    finally
    {
      ProcessLock localProcessLock = null;
      label53:
      IOUtil.closeQuietly(localProcessLock);
    }
  }
  
  private void deleteNoIndexFiles()
  {
    this.trimExecutor.execute(new Runnable()
    {
      public void run()
      {
        if (LruDiskCache.this.available) {
          try
          {
            File[] arrayOfFile = LruDiskCache.this.cacheDir.listFiles();
            if (arrayOfFile != null)
            {
              int j = arrayOfFile.length;
              for (int i = 0; i < j; i++)
              {
                File localFile = arrayOfFile[i];
                try
                {
                  if (LruDiskCache.this.cacheDb.selector(DiskCacheEntity.class).where("path", "=", localFile.getAbsolutePath()).count() < 1L) {
                    IOUtil.deleteFileOrDir(localFile);
                  }
                }
                catch (Throwable localThrowable2)
                {
                  LogUtil.e(localThrowable2.getMessage(), localThrowable2);
                }
              }
            }
            return;
          }
          catch (Throwable localThrowable1)
          {
            LogUtil.e(localThrowable1.getMessage(), localThrowable1);
          }
        }
      }
    });
  }
  
  public static LruDiskCache getDiskCache(String paramString)
  {
    String str = paramString;
    try
    {
      if (TextUtils.isEmpty(paramString)) {
        str = "xUtils_cache";
      }
      LruDiskCache localLruDiskCache = (LruDiskCache)DISK_CACHE_MAP.get(str);
      paramString = localLruDiskCache;
      if (localLruDiskCache == null)
      {
        paramString = new org/xutils/cache/LruDiskCache;
        paramString.<init>(str);
        DISK_CACHE_MAP.put(str, paramString);
      }
      return paramString;
    }
    finally {}
  }
  
  private void trimSize()
  {
    this.trimExecutor.execute(new Runnable()
    {
      /* Error */
      public void run()
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   4: invokestatic 27	org/xutils/cache/LruDiskCache:access$100	(Lorg/xutils/cache/LruDiskCache;)Z
        //   7: ifeq +482 -> 489
        //   10: invokestatic 33	java/lang/System:currentTimeMillis	()J
        //   13: lstore_2
        //   14: lload_2
        //   15: aload_0
        //   16: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   19: invokestatic 37	org/xutils/cache/LruDiskCache:access$200	(Lorg/xutils/cache/LruDiskCache;)J
        //   22: lsub
        //   23: ldc2_w 38
        //   26: lcmp
        //   27: ifge +4 -> 31
        //   30: return
        //   31: aload_0
        //   32: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   35: lload_2
        //   36: invokestatic 43	org/xutils/cache/LruDiskCache:access$202	(Lorg/xutils/cache/LruDiskCache;J)J
        //   39: pop2
        //   40: aload_0
        //   41: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   44: invokestatic 46	org/xutils/cache/LruDiskCache:access$300	(Lorg/xutils/cache/LruDiskCache;)V
        //   47: aload_0
        //   48: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   51: invokestatic 50	org/xutils/cache/LruDiskCache:access$000	(Lorg/xutils/cache/LruDiskCache;)Lorg/xutils/DbManager;
        //   54: ldc 52
        //   56: invokeinterface 58 2 0
        //   61: invokevirtual 63	org/xutils/db/Selector:count	()J
        //   64: l2i
        //   65: istore_1
        //   66: iload_1
        //   67: sipush 5010
        //   70: if_icmple +202 -> 272
        //   73: aload_0
        //   74: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   77: invokestatic 50	org/xutils/cache/LruDiskCache:access$000	(Lorg/xutils/cache/LruDiskCache;)Lorg/xutils/DbManager;
        //   80: ldc 52
        //   82: invokeinterface 58 2 0
        //   87: ldc 65
        //   89: invokevirtual 69	org/xutils/db/Selector:orderBy	(Ljava/lang/String;)Lorg/xutils/db/Selector;
        //   92: ldc 71
        //   94: invokevirtual 69	org/xutils/db/Selector:orderBy	(Ljava/lang/String;)Lorg/xutils/db/Selector;
        //   97: iload_1
        //   98: sipush 5000
        //   101: isub
        //   102: invokevirtual 75	org/xutils/db/Selector:limit	(I)Lorg/xutils/db/Selector;
        //   105: iconst_0
        //   106: invokevirtual 78	org/xutils/db/Selector:offset	(I)Lorg/xutils/db/Selector;
        //   109: invokevirtual 82	org/xutils/db/Selector:findAll	()Ljava/util/List;
        //   112: astore 4
        //   114: aload 4
        //   116: ifnull +156 -> 272
        //   119: aload 4
        //   121: invokeinterface 88 1 0
        //   126: ifle +146 -> 272
        //   129: aload 4
        //   131: invokeinterface 92 1 0
        //   136: astore 4
        //   138: aload 4
        //   140: invokeinterface 98 1 0
        //   145: ifeq +127 -> 272
        //   148: aload 4
        //   150: invokeinterface 102 1 0
        //   155: checkcast 52	org/xutils/cache/DiskCacheEntity
        //   158: astore 5
        //   160: aload_0
        //   161: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   164: invokestatic 50	org/xutils/cache/LruDiskCache:access$000	(Lorg/xutils/cache/LruDiskCache;)Lorg/xutils/DbManager;
        //   167: aload 5
        //   169: invokeinterface 106 2 0
        //   174: aload 5
        //   176: invokevirtual 110	org/xutils/cache/DiskCacheEntity:getPath	()Ljava/lang/String;
        //   179: astore 6
        //   181: aload 6
        //   183: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //   186: ifne -48 -> 138
        //   189: aload_0
        //   190: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   193: aload 6
        //   195: invokestatic 120	org/xutils/cache/LruDiskCache:access$400	(Lorg/xutils/cache/LruDiskCache;Ljava/lang/String;)Z
        //   198: pop
        //   199: aload_0
        //   200: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   203: astore 5
        //   205: new 122	java/lang/StringBuilder
        //   208: astore 7
        //   210: aload 7
        //   212: invokespecial 123	java/lang/StringBuilder:<init>	()V
        //   215: aload 7
        //   217: aload 6
        //   219: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   222: pop
        //   223: aload 7
        //   225: ldc -127
        //   227: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   230: pop
        //   231: aload 5
        //   233: aload 7
        //   235: invokevirtual 132	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   238: invokestatic 120	org/xutils/cache/LruDiskCache:access$400	(Lorg/xutils/cache/LruDiskCache;Ljava/lang/String;)Z
        //   241: pop
        //   242: goto -104 -> 138
        //   245: astore 5
        //   247: aload 5
        //   249: invokevirtual 135	org/xutils/ex/DbException:getMessage	()Ljava/lang/String;
        //   252: aload 5
        //   254: invokestatic 141	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   257: goto -119 -> 138
        //   260: astore 4
        //   262: aload 4
        //   264: invokevirtual 135	org/xutils/ex/DbException:getMessage	()Ljava/lang/String;
        //   267: aload 4
        //   269: invokestatic 141	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   272: aload_0
        //   273: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   276: invokestatic 145	org/xutils/cache/LruDiskCache:access$500	(Lorg/xutils/cache/LruDiskCache;)Ljava/io/File;
        //   279: invokestatic 151	org/xutils/common/util/FileUtil:getFileOrDirSize	(Ljava/io/File;)J
        //   282: aload_0
        //   283: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   286: invokestatic 154	org/xutils/cache/LruDiskCache:access$600	(Lorg/xutils/cache/LruDiskCache;)J
        //   289: lcmp
        //   290: ifle +199 -> 489
        //   293: aload_0
        //   294: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   297: invokestatic 50	org/xutils/cache/LruDiskCache:access$000	(Lorg/xutils/cache/LruDiskCache;)Lorg/xutils/DbManager;
        //   300: ldc 52
        //   302: invokeinterface 58 2 0
        //   307: ldc 65
        //   309: invokevirtual 69	org/xutils/db/Selector:orderBy	(Ljava/lang/String;)Lorg/xutils/db/Selector;
        //   312: ldc 71
        //   314: invokevirtual 69	org/xutils/db/Selector:orderBy	(Ljava/lang/String;)Lorg/xutils/db/Selector;
        //   317: bipush 10
        //   319: invokevirtual 75	org/xutils/db/Selector:limit	(I)Lorg/xutils/db/Selector;
        //   322: iconst_0
        //   323: invokevirtual 78	org/xutils/db/Selector:offset	(I)Lorg/xutils/db/Selector;
        //   326: invokevirtual 82	org/xutils/db/Selector:findAll	()Ljava/util/List;
        //   329: astore 4
        //   331: aload 4
        //   333: ifnull -61 -> 272
        //   336: aload 4
        //   338: invokeinterface 88 1 0
        //   343: ifle -71 -> 272
        //   346: aload 4
        //   348: invokeinterface 92 1 0
        //   353: astore 4
        //   355: aload 4
        //   357: invokeinterface 98 1 0
        //   362: ifeq -90 -> 272
        //   365: aload 4
        //   367: invokeinterface 102 1 0
        //   372: checkcast 52	org/xutils/cache/DiskCacheEntity
        //   375: astore 5
        //   377: aload_0
        //   378: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   381: invokestatic 50	org/xutils/cache/LruDiskCache:access$000	(Lorg/xutils/cache/LruDiskCache;)Lorg/xutils/DbManager;
        //   384: aload 5
        //   386: invokeinterface 106 2 0
        //   391: aload 5
        //   393: invokevirtual 110	org/xutils/cache/DiskCacheEntity:getPath	()Ljava/lang/String;
        //   396: astore 5
        //   398: aload 5
        //   400: invokestatic 116	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //   403: ifne -48 -> 355
        //   406: aload_0
        //   407: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   410: aload 5
        //   412: invokestatic 120	org/xutils/cache/LruDiskCache:access$400	(Lorg/xutils/cache/LruDiskCache;Ljava/lang/String;)Z
        //   415: pop
        //   416: aload_0
        //   417: getfield 17	org/xutils/cache/LruDiskCache$2:this$0	Lorg/xutils/cache/LruDiskCache;
        //   420: astore 7
        //   422: new 122	java/lang/StringBuilder
        //   425: astore 6
        //   427: aload 6
        //   429: invokespecial 123	java/lang/StringBuilder:<init>	()V
        //   432: aload 6
        //   434: aload 5
        //   436: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   439: pop
        //   440: aload 6
        //   442: ldc -127
        //   444: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   447: pop
        //   448: aload 7
        //   450: aload 6
        //   452: invokevirtual 132	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   455: invokestatic 120	org/xutils/cache/LruDiskCache:access$400	(Lorg/xutils/cache/LruDiskCache;Ljava/lang/String;)Z
        //   458: pop
        //   459: goto -104 -> 355
        //   462: astore 5
        //   464: aload 5
        //   466: invokevirtual 135	org/xutils/ex/DbException:getMessage	()Ljava/lang/String;
        //   469: aload 5
        //   471: invokestatic 141	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   474: goto -119 -> 355
        //   477: astore 4
        //   479: aload 4
        //   481: invokevirtual 135	org/xutils/ex/DbException:getMessage	()Ljava/lang/String;
        //   484: aload 4
        //   486: invokestatic 141	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   489: return
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	490	0	this	2
        //   65	37	1	i	int
        //   13	23	2	l	long
        //   112	37	4	localObject1	Object
        //   260	8	4	localDbException1	DbException
        //   329	37	4	localObject2	Object
        //   477	8	4	localDbException2	DbException
        //   158	74	5	localObject3	Object
        //   245	8	5	localDbException3	DbException
        //   375	60	5	localObject4	Object
        //   462	8	5	localDbException4	DbException
        //   179	272	6	localObject5	Object
        //   208	241	7	localObject6	Object
        // Exception table:
        //   from	to	target	type
        //   160	242	245	org/xutils/ex/DbException
        //   47	66	260	org/xutils/ex/DbException
        //   73	114	260	org/xutils/ex/DbException
        //   119	138	260	org/xutils/ex/DbException
        //   138	160	260	org/xutils/ex/DbException
        //   247	257	260	org/xutils/ex/DbException
        //   377	459	462	org/xutils/ex/DbException
        //   272	331	477	org/xutils/ex/DbException
        //   336	355	477	org/xutils/ex/DbException
        //   355	377	477	org/xutils/ex/DbException
        //   464	474	477	org/xutils/ex/DbException
      }
    });
  }
  
  public void clearCacheFiles()
  {
    IOUtil.deleteFileOrDir(this.cacheDir);
  }
  
  /* Error */
  DiskCacheFile commitDiskCacheFile(DiskCacheFile paramDiskCacheFile)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_1
    //   4: ifnull +18 -> 22
    //   7: aload_1
    //   8: invokevirtual 269	org/xutils/cache/DiskCacheFile:length	()J
    //   11: lconst_1
    //   12: lcmp
    //   13: ifge +9 -> 22
    //   16: aload_1
    //   17: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   20: aconst_null
    //   21: areturn
    //   22: aload_0
    //   23: getfield 58	org/xutils/cache/LruDiskCache:available	Z
    //   26: ifeq +316 -> 342
    //   29: aload_1
    //   30: ifnonnull +6 -> 36
    //   33: goto +309 -> 342
    //   36: aload_1
    //   37: getfield 273	org/xutils/cache/DiskCacheFile:cacheEntity	Lorg/xutils/cache/DiskCacheEntity;
    //   40: astore 7
    //   42: aload_1
    //   43: astore_3
    //   44: aload_1
    //   45: invokevirtual 276	org/xutils/cache/DiskCacheFile:getName	()Ljava/lang/String;
    //   48: ldc 29
    //   50: invokevirtual 281	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   53: ifeq +287 -> 340
    //   56: aload 7
    //   58: invokevirtual 198	org/xutils/cache/DiskCacheEntity:getPath	()Ljava/lang/String;
    //   61: astore_3
    //   62: aload_3
    //   63: iconst_1
    //   64: ldc2_w 282
    //   67: invokestatic 286	org/xutils/common/util/ProcessLock:tryLock	(Ljava/lang/String;ZJ)Lorg/xutils/common/util/ProcessLock;
    //   70: astore 5
    //   72: aload 5
    //   74: ifnull +137 -> 211
    //   77: aload 5
    //   79: invokevirtual 222	org/xutils/common/util/ProcessLock:isValid	()Z
    //   82: ifeq +129 -> 211
    //   85: new 266	org/xutils/cache/DiskCacheFile
    //   88: astore 4
    //   90: aload 4
    //   92: aload 7
    //   94: aload_3
    //   95: aload 5
    //   97: invokespecial 289	org/xutils/cache/DiskCacheFile:<init>	(Lorg/xutils/cache/DiskCacheEntity;Ljava/lang/String;Lorg/xutils/common/util/ProcessLock;)V
    //   100: aload_1
    //   101: aload 4
    //   103: invokevirtual 292	org/xutils/cache/DiskCacheFile:renameTo	(Ljava/io/File;)Z
    //   106: istore_2
    //   107: iload_2
    //   108: ifeq +53 -> 161
    //   111: aload_0
    //   112: getfield 87	org/xutils/cache/LruDiskCache:cacheDb	Lorg/xutils/DbManager;
    //   115: aload 7
    //   117: invokeinterface 296 2 0
    //   122: goto +20 -> 142
    //   125: astore_3
    //   126: aload 4
    //   128: astore 6
    //   130: goto +175 -> 305
    //   133: astore_3
    //   134: aload_3
    //   135: invokevirtual 297	org/xutils/ex/DbException:getMessage	()Ljava/lang/String;
    //   138: aload_3
    //   139: invokestatic 213	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   142: aload_0
    //   143: invokespecial 299	org/xutils/cache/LruDiskCache:trimSize	()V
    //   146: aload_1
    //   147: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   150: aload_1
    //   151: invokestatic 230	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   154: pop
    //   155: aload 4
    //   157: astore_3
    //   158: goto +182 -> 340
    //   161: new 260	java/io/IOException
    //   164: astore 7
    //   166: new 301	java/lang/StringBuilder
    //   169: astore_3
    //   170: aload_3
    //   171: invokespecial 302	java/lang/StringBuilder:<init>	()V
    //   174: aload_3
    //   175: ldc_w 304
    //   178: invokevirtual 308	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload_3
    //   183: aload_1
    //   184: invokevirtual 311	org/xutils/cache/DiskCacheFile:getAbsolutePath	()Ljava/lang/String;
    //   187: invokevirtual 308	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload 7
    //   193: aload_3
    //   194: invokevirtual 314	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   197: invokespecial 315	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   200: aload 7
    //   202: athrow
    //   203: astore_3
    //   204: goto +101 -> 305
    //   207: astore_3
    //   208: goto +46 -> 254
    //   211: new 317	org/xutils/ex/FileLockedException
    //   214: astore 4
    //   216: aload 4
    //   218: aload_3
    //   219: invokespecial 318	org/xutils/ex/FileLockedException:<init>	(Ljava/lang/String;)V
    //   222: aload 4
    //   224: athrow
    //   225: astore_3
    //   226: aconst_null
    //   227: astore 4
    //   229: goto +76 -> 305
    //   232: astore_3
    //   233: goto +18 -> 251
    //   236: astore_3
    //   237: aconst_null
    //   238: astore 5
    //   240: aload 5
    //   242: astore 4
    //   244: goto +61 -> 305
    //   247: astore_3
    //   248: aconst_null
    //   249: astore 5
    //   251: aconst_null
    //   252: astore 4
    //   254: aload_3
    //   255: invokevirtual 319	java/lang/InterruptedException:getMessage	()Ljava/lang/String;
    //   258: aload_3
    //   259: invokestatic 213	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   262: aload_1
    //   263: ifnonnull +24 -> 287
    //   266: aload 4
    //   268: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   271: aload 5
    //   273: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   276: aload 4
    //   278: invokestatic 230	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   281: pop
    //   282: aload_1
    //   283: astore_3
    //   284: goto +56 -> 340
    //   287: aload_1
    //   288: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   291: aload_1
    //   292: invokestatic 230	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   295: pop
    //   296: aload_1
    //   297: astore_3
    //   298: goto +42 -> 340
    //   301: astore_3
    //   302: aload_1
    //   303: astore 6
    //   305: aload 6
    //   307: ifnonnull +22 -> 329
    //   310: aload 4
    //   312: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   315: aload 5
    //   317: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   320: aload 4
    //   322: invokestatic 230	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   325: pop
    //   326: goto +12 -> 338
    //   329: aload_1
    //   330: invokestatic 234	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   333: aload_1
    //   334: invokestatic 230	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   337: pop
    //   338: aload_3
    //   339: athrow
    //   340: aload_3
    //   341: areturn
    //   342: aconst_null
    //   343: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	344	0	this	LruDiskCache
    //   0	344	1	paramDiskCacheFile	DiskCacheFile
    //   106	2	2	bool	boolean
    //   43	52	3	localObject1	Object
    //   125	1	3	localObject2	Object
    //   133	6	3	localDbException	DbException
    //   157	37	3	localObject3	Object
    //   203	1	3	localObject4	Object
    //   207	12	3	localInterruptedException1	InterruptedException
    //   225	1	3	localObject5	Object
    //   232	1	3	localInterruptedException2	InterruptedException
    //   236	1	3	localObject6	Object
    //   247	12	3	localInterruptedException3	InterruptedException
    //   283	15	3	localDiskCacheFile1	DiskCacheFile
    //   301	40	3	localDiskCacheFile2	DiskCacheFile
    //   88	233	4	localObject7	Object
    //   70	246	5	localProcessLock	ProcessLock
    //   1	305	6	localObject8	Object
    //   40	161	7	localObject9	Object
    // Exception table:
    //   from	to	target	type
    //   111	122	125	finally
    //   134	142	125	finally
    //   142	146	125	finally
    //   111	122	133	org/xutils/ex/DbException
    //   100	107	203	finally
    //   161	203	203	finally
    //   100	107	207	java/lang/InterruptedException
    //   111	122	207	java/lang/InterruptedException
    //   134	142	207	java/lang/InterruptedException
    //   142	146	207	java/lang/InterruptedException
    //   161	203	207	java/lang/InterruptedException
    //   77	100	225	finally
    //   211	225	225	finally
    //   77	100	232	java/lang/InterruptedException
    //   211	225	232	java/lang/InterruptedException
    //   56	72	236	finally
    //   56	72	247	java/lang/InterruptedException
    //   254	262	301	finally
  }
  
  public DiskCacheFile createDiskCacheFile(DiskCacheEntity paramDiskCacheEntity)
    throws IOException
  {
    if ((this.available) && (paramDiskCacheEntity != null))
    {
      paramDiskCacheEntity.setPath(new File(this.cacheDir, MD5.md5(paramDiskCacheEntity.getKey())).getAbsolutePath());
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append(paramDiskCacheEntity.getPath());
      ((StringBuilder)localObject).append(".tmp");
      localObject = ((StringBuilder)localObject).toString();
      ProcessLock localProcessLock = ProcessLock.tryLock((String)localObject, true);
      if ((localProcessLock != null) && (localProcessLock.isValid()))
      {
        paramDiskCacheEntity = new DiskCacheFile(paramDiskCacheEntity, (String)localObject, localProcessLock);
        if (!paramDiskCacheEntity.getParentFile().exists()) {
          paramDiskCacheEntity.mkdirs();
        }
        return paramDiskCacheEntity;
      }
      throw new FileLockedException(paramDiskCacheEntity.getPath());
    }
    return null;
  }
  
  public DiskCacheEntity get(final String paramString)
  {
    if ((this.available) && (!TextUtils.isEmpty(paramString)))
    {
      try
      {
        paramString = (DiskCacheEntity)this.cacheDb.selector(DiskCacheEntity.class).where("key", "=", paramString).findFirst();
      }
      catch (Throwable paramString)
      {
        LogUtil.e(paramString.getMessage(), paramString);
        paramString = null;
      }
      if (paramString != null)
      {
        if (paramString.getExpires() < System.currentTimeMillis()) {
          return null;
        }
        this.trimExecutor.execute(new Runnable()
        {
          public void run()
          {
            DiskCacheEntity localDiskCacheEntity = paramString;
            localDiskCacheEntity.setHits(localDiskCacheEntity.getHits() + 1L);
            paramString.setLastAccess(System.currentTimeMillis());
            try
            {
              LruDiskCache.this.cacheDb.update(paramString, new String[] { "hits", "lastAccess" });
            }
            catch (Throwable localThrowable)
            {
              LogUtil.e(localThrowable.getMessage(), localThrowable);
            }
          }
        });
      }
      return paramString;
    }
    return null;
  }
  
  public DiskCacheFile getDiskCacheFile(String paramString)
    throws InterruptedException
  {
    boolean bool = this.available;
    Object localObject = null;
    if ((bool) && (!TextUtils.isEmpty(paramString)))
    {
      DiskCacheEntity localDiskCacheEntity = get(paramString);
      paramString = (String)localObject;
      if (localDiskCacheEntity != null)
      {
        paramString = (String)localObject;
        if (new File(localDiskCacheEntity.getPath()).exists())
        {
          ProcessLock localProcessLock = ProcessLock.tryLock(localDiskCacheEntity.getPath(), false, 3000L);
          paramString = (String)localObject;
          if (localProcessLock != null)
          {
            paramString = (String)localObject;
            if (localProcessLock.isValid())
            {
              paramString = new DiskCacheFile(localDiskCacheEntity, localDiskCacheEntity.getPath(), localProcessLock);
              if (!paramString.exists()) {
                try
                {
                  this.cacheDb.delete(localDiskCacheEntity);
                  paramString = (String)localObject;
                }
                catch (DbException paramString)
                {
                  LogUtil.e(paramString.getMessage(), paramString);
                  paramString = (String)localObject;
                }
              }
            }
          }
        }
      }
      return paramString;
    }
    return null;
  }
  
  public void put(DiskCacheEntity paramDiskCacheEntity)
  {
    if ((this.available) && (paramDiskCacheEntity != null) && (!TextUtils.isEmpty(paramDiskCacheEntity.getTextContent())) && (paramDiskCacheEntity.getExpires() >= System.currentTimeMillis()))
    {
      try
      {
        this.cacheDb.replace(paramDiskCacheEntity);
      }
      catch (DbException paramDiskCacheEntity)
      {
        LogUtil.e(paramDiskCacheEntity.getMessage(), paramDiskCacheEntity);
      }
      trimSize();
      return;
    }
  }
  
  public LruDiskCache setMaxSize(long paramLong)
  {
    if (paramLong > 0L)
    {
      long l = FileUtil.getDiskAvailableSize();
      if (l > paramLong) {
        this.diskCacheSize = paramLong;
      } else {
        this.diskCacheSize = l;
      }
    }
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/cache/LruDiskCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */