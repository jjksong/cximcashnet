package org.xutils.image;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import org.xutils.HttpManager;
import org.xutils.cache.LruCache;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback.CacheCallback;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.common.Callback.PrepareCallback;
import org.xutils.common.Callback.ProgressCallback;
import org.xutils.common.Callback.TypedCallback;
import org.xutils.common.TaskController;
import org.xutils.common.task.Priority;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.IOUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.ex.FileLockedException;
import org.xutils.http.RequestParams;
import org.xutils.x;

final class ImageLoader
  implements Callback.PrepareCallback<File, Drawable>, Callback.CacheCallback<Drawable>, Callback.ProgressCallback<Drawable>, Callback.TypedCallback<Drawable>, Callback.Cancelable
{
  private static final String DISK_CACHE_DIR_NAME = "xUtils_img";
  private static final Executor EXECUTOR;
  private static final HashMap<String, FakeImageView> FAKE_IMG_MAP = new HashMap();
  private static final LruCache<MemCacheKey, Drawable> MEM_CACHE;
  private static final int MEM_CACHE_MIN_SIZE = 4194304;
  private static final AtomicLong SEQ_SEEK = new AtomicLong(0L);
  private static final Type loadType = File.class;
  private Callback.CacheCallback<Drawable> cacheCallback;
  private Callback.CommonCallback<Drawable> callback;
  private Callback.Cancelable cancelable;
  private volatile boolean cancelled = false;
  private boolean hasCache = false;
  private MemCacheKey key;
  private ImageOptions options;
  private Callback.PrepareCallback<File, Drawable> prepareCallback;
  private Callback.ProgressCallback<Drawable> progressCallback;
  private final long seq = SEQ_SEEK.incrementAndGet();
  private volatile boolean stopped = false;
  private WeakReference<ImageView> viewRef;
  
  static
  {
    EXECUTOR = new PriorityExecutor(10, false);
    MEM_CACHE = new LruCache(4194304)
    {
      private boolean deepClear = false;
      
      protected void entryRemoved(boolean paramAnonymousBoolean, MemCacheKey paramAnonymousMemCacheKey, Drawable paramAnonymousDrawable1, Drawable paramAnonymousDrawable2)
      {
        super.entryRemoved(paramAnonymousBoolean, paramAnonymousMemCacheKey, paramAnonymousDrawable1, paramAnonymousDrawable2);
        if ((paramAnonymousBoolean) && (this.deepClear) && ((paramAnonymousDrawable1 instanceof ReusableDrawable))) {
          ((ReusableDrawable)paramAnonymousDrawable1).setMemCacheKey(null);
        }
      }
      
      protected int sizeOf(MemCacheKey paramAnonymousMemCacheKey, Drawable paramAnonymousDrawable)
      {
        if ((paramAnonymousDrawable instanceof BitmapDrawable))
        {
          paramAnonymousMemCacheKey = ((BitmapDrawable)paramAnonymousDrawable).getBitmap();
          int i;
          if (paramAnonymousMemCacheKey == null) {
            i = 0;
          } else {
            i = paramAnonymousMemCacheKey.getByteCount();
          }
          return i;
        }
        if ((paramAnonymousDrawable instanceof GifDrawable)) {
          return ((GifDrawable)paramAnonymousDrawable).getByteCount();
        }
        return super.sizeOf(paramAnonymousMemCacheKey, paramAnonymousDrawable);
      }
      
      public void trimToSize(int paramAnonymousInt)
      {
        if (paramAnonymousInt < 0) {
          this.deepClear = true;
        }
        super.trimToSize(paramAnonymousInt);
        this.deepClear = false;
      }
    };
    int j = ((ActivityManager)x.app().getSystemService("activity")).getMemoryClass() * 1048576 / 8;
    int i = j;
    if (j < 4194304) {
      i = 4194304;
    }
    MEM_CACHE.resize(i);
  }
  
  static void clearCacheFiles()
  {
    LruDiskCache.getDiskCache("xUtils_img").clearCacheFiles();
  }
  
  static void clearMemCache()
  {
    MEM_CACHE.evictAll();
  }
  
  private static RequestParams createRequestParams(String paramString, ImageOptions paramImageOptions)
  {
    RequestParams localRequestParams = new RequestParams(paramString);
    localRequestParams.setCacheDirName("xUtils_img");
    localRequestParams.setConnectTimeout(8000);
    localRequestParams.setPriority(Priority.BG_LOW);
    localRequestParams.setExecutor(EXECUTOR);
    localRequestParams.setCancelFast(true);
    localRequestParams.setUseCookie(false);
    paramString = localRequestParams;
    if (paramImageOptions != null)
    {
      ImageOptions.ParamsBuilder localParamsBuilder = paramImageOptions.getParamsBuilder();
      paramString = localRequestParams;
      if (localParamsBuilder != null) {
        paramString = localParamsBuilder.buildParams(localRequestParams, paramImageOptions);
      }
    }
    return paramString;
  }
  
  /* Error */
  static Callback.Cancelable doBind(ImageView paramImageView, String paramString, ImageOptions paramImageOptions, Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +13 -> 14
    //   4: aconst_null
    //   5: aload_2
    //   6: ldc -33
    //   8: aload_3
    //   9: invokestatic 227	org/xutils/image/ImageLoader:postArgsException	(Landroid/widget/ImageView;Lorg/xutils/image/ImageOptions;Ljava/lang/String;Lorg/xutils/common/Callback$CommonCallback;)V
    //   12: aconst_null
    //   13: areturn
    //   14: aload_1
    //   15: invokestatic 233	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   18: ifeq +13 -> 31
    //   21: aload_0
    //   22: aload_2
    //   23: ldc -21
    //   25: aload_3
    //   26: invokestatic 227	org/xutils/image/ImageLoader:postArgsException	(Landroid/widget/ImageView;Lorg/xutils/image/ImageOptions;Ljava/lang/String;Lorg/xutils/common/Callback$CommonCallback;)V
    //   29: aconst_null
    //   30: areturn
    //   31: aload_2
    //   32: astore 7
    //   34: aload_2
    //   35: ifnonnull +8 -> 43
    //   38: getstatic 238	org/xutils/image/ImageOptions:DEFAULT	Lorg/xutils/image/ImageOptions;
    //   41: astore 7
    //   43: aload 7
    //   45: aload_0
    //   46: invokevirtual 242	org/xutils/image/ImageOptions:optimizeMaxSize	(Landroid/widget/ImageView;)V
    //   49: new 244	org/xutils/image/MemCacheKey
    //   52: dup
    //   53: aload_1
    //   54: aload 7
    //   56: invokespecial 247	org/xutils/image/MemCacheKey:<init>	(Ljava/lang/String;Lorg/xutils/image/ImageOptions;)V
    //   59: astore_2
    //   60: aload_0
    //   61: invokevirtual 253	android/widget/ImageView:getDrawable	()Landroid/graphics/drawable/Drawable;
    //   64: astore 9
    //   66: aload 9
    //   68: instanceof 255
    //   71: ifeq +48 -> 119
    //   74: aload 9
    //   76: checkcast 255	org/xutils/image/AsyncDrawable
    //   79: invokevirtual 259	org/xutils/image/AsyncDrawable:getImageLoader	()Lorg/xutils/image/ImageLoader;
    //   82: astore 8
    //   84: aload 8
    //   86: ifnull +77 -> 163
    //   89: aload 8
    //   91: getfield 138	org/xutils/image/ImageLoader:stopped	Z
    //   94: ifne +69 -> 163
    //   97: aload_2
    //   98: aload 8
    //   100: getfield 150	org/xutils/image/ImageLoader:key	Lorg/xutils/image/MemCacheKey;
    //   103: invokevirtual 263	org/xutils/image/MemCacheKey:equals	(Ljava/lang/Object;)Z
    //   106: ifeq +5 -> 111
    //   109: aconst_null
    //   110: areturn
    //   111: aload 8
    //   113: invokevirtual 266	org/xutils/image/ImageLoader:cancel	()V
    //   116: goto +47 -> 163
    //   119: aload 9
    //   121: instanceof 268
    //   124: ifeq +39 -> 163
    //   127: aload 9
    //   129: checkcast 268	org/xutils/image/ReusableDrawable
    //   132: invokeinterface 272 1 0
    //   137: astore 8
    //   139: aload 8
    //   141: ifnull +22 -> 163
    //   144: aload 8
    //   146: aload_2
    //   147: invokevirtual 263	org/xutils/image/MemCacheKey:equals	(Ljava/lang/Object;)Z
    //   150: ifeq +13 -> 163
    //   153: getstatic 92	org/xutils/image/ImageLoader:MEM_CACHE	Lorg/xutils/cache/LruCache;
    //   156: aload_2
    //   157: aload 9
    //   159: invokevirtual 276	org/xutils/cache/LruCache:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   162: pop
    //   163: aload 7
    //   165: invokevirtual 280	org/xutils/image/ImageOptions:isUseMemCache	()Z
    //   168: ifeq +57 -> 225
    //   171: getstatic 92	org/xutils/image/ImageLoader:MEM_CACHE	Lorg/xutils/cache/LruCache;
    //   174: aload_2
    //   175: invokevirtual 284	org/xutils/cache/LruCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   178: checkcast 286	android/graphics/drawable/Drawable
    //   181: astore 8
    //   183: aload 8
    //   185: astore_2
    //   186: aload 8
    //   188: instanceof 288
    //   191: ifeq +36 -> 227
    //   194: aload 8
    //   196: checkcast 288	android/graphics/drawable/BitmapDrawable
    //   199: invokevirtual 292	android/graphics/drawable/BitmapDrawable:getBitmap	()Landroid/graphics/Bitmap;
    //   202: astore 9
    //   204: aload 9
    //   206: ifnull +14 -> 220
    //   209: aload 8
    //   211: astore_2
    //   212: aload 9
    //   214: invokevirtual 297	android/graphics/Bitmap:isRecycled	()Z
    //   217: ifeq +10 -> 227
    //   220: aconst_null
    //   221: astore_2
    //   222: goto +5 -> 227
    //   225: aconst_null
    //   226: astore_2
    //   227: aload_2
    //   228: ifnull +250 -> 478
    //   231: iconst_0
    //   232: istore 6
    //   234: aload_3
    //   235: instanceof 11
    //   238: ifeq +12 -> 250
    //   241: aload_3
    //   242: checkcast 11	org/xutils/common/Callback$ProgressCallback
    //   245: invokeinterface 300 1 0
    //   250: aload_0
    //   251: aload 7
    //   253: invokevirtual 304	org/xutils/image/ImageOptions:getImageScaleType	()Landroid/widget/ImageView$ScaleType;
    //   256: invokevirtual 308	android/widget/ImageView:setScaleType	(Landroid/widget/ImageView$ScaleType;)V
    //   259: aload_0
    //   260: aload_2
    //   261: invokevirtual 312	android/widget/ImageView:setImageDrawable	(Landroid/graphics/drawable/Drawable;)V
    //   264: iconst_1
    //   265: istore 5
    //   267: aload_3
    //   268: instanceof 9
    //   271: ifeq +79 -> 350
    //   274: aload_3
    //   275: checkcast 9	org/xutils/common/Callback$CacheCallback
    //   278: aload_2
    //   279: invokeinterface 315 2 0
    //   284: istore 4
    //   286: iload 4
    //   288: istore 5
    //   290: iload 4
    //   292: ifne +72 -> 364
    //   295: new 2	org/xutils/image/ImageLoader
    //   298: astore_2
    //   299: aload_2
    //   300: invokespecial 316	org/xutils/image/ImageLoader:<init>	()V
    //   303: aload_2
    //   304: aload_0
    //   305: aload_1
    //   306: aload 7
    //   308: aload_3
    //   309: invokespecial 319	org/xutils/image/ImageLoader:doLoad	(Landroid/widget/ImageView;Ljava/lang/String;Lorg/xutils/image/ImageOptions;Lorg/xutils/common/Callback$CommonCallback;)Lorg/xutils/common/Callback$Cancelable;
    //   312: astore_2
    //   313: iload 4
    //   315: ifeq +25 -> 340
    //   318: aload_3
    //   319: ifnull +21 -> 340
    //   322: aload_3
    //   323: invokeinterface 324 1 0
    //   328: goto +12 -> 340
    //   331: astore_0
    //   332: aload_0
    //   333: invokevirtual 328	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   336: aload_0
    //   337: invokestatic 334	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   340: aload_2
    //   341: areturn
    //   342: astore_0
    //   343: goto +106 -> 449
    //   346: astore_2
    //   347: goto +73 -> 420
    //   350: aload_3
    //   351: ifnull +10 -> 361
    //   354: aload_3
    //   355: aload_2
    //   356: invokeinterface 338 2 0
    //   361: iconst_1
    //   362: istore 5
    //   364: iload 5
    //   366: ifeq +25 -> 391
    //   369: aload_3
    //   370: ifnull +21 -> 391
    //   373: aload_3
    //   374: invokeinterface 324 1 0
    //   379: goto +12 -> 391
    //   382: astore_0
    //   383: aload_0
    //   384: invokevirtual 328	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   387: aload_0
    //   388: invokestatic 334	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   391: aconst_null
    //   392: areturn
    //   393: astore_0
    //   394: iconst_1
    //   395: istore 4
    //   397: goto +52 -> 449
    //   400: astore_2
    //   401: iload 5
    //   403: istore 4
    //   405: goto +15 -> 420
    //   408: astore_0
    //   409: iload 6
    //   411: istore 4
    //   413: goto +36 -> 449
    //   416: astore_2
    //   417: iconst_0
    //   418: istore 4
    //   420: aload_2
    //   421: invokevirtual 328	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   424: aload_2
    //   425: invokestatic 334	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   428: new 2	org/xutils/image/ImageLoader
    //   431: astore_2
    //   432: aload_2
    //   433: invokespecial 316	org/xutils/image/ImageLoader:<init>	()V
    //   436: aload_2
    //   437: aload_0
    //   438: aload_1
    //   439: aload 7
    //   441: aload_3
    //   442: invokespecial 319	org/xutils/image/ImageLoader:doLoad	(Landroid/widget/ImageView;Ljava/lang/String;Lorg/xutils/image/ImageOptions;Lorg/xutils/common/Callback$CommonCallback;)Lorg/xutils/common/Callback$Cancelable;
    //   445: astore_0
    //   446: aload_0
    //   447: areturn
    //   448: astore_0
    //   449: iload 4
    //   451: ifeq +25 -> 476
    //   454: aload_3
    //   455: ifnull +21 -> 476
    //   458: aload_3
    //   459: invokeinterface 324 1 0
    //   464: goto +12 -> 476
    //   467: astore_1
    //   468: aload_1
    //   469: invokevirtual 328	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   472: aload_1
    //   473: invokestatic 334	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   476: aload_0
    //   477: athrow
    //   478: new 2	org/xutils/image/ImageLoader
    //   481: dup
    //   482: invokespecial 316	org/xutils/image/ImageLoader:<init>	()V
    //   485: aload_0
    //   486: aload_1
    //   487: aload 7
    //   489: aload_3
    //   490: invokespecial 319	org/xutils/image/ImageLoader:doLoad	(Landroid/widget/ImageView;Ljava/lang/String;Lorg/xutils/image/ImageOptions;Lorg/xutils/common/Callback$CommonCallback;)Lorg/xutils/common/Callback$Cancelable;
    //   493: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	494	0	paramImageView	ImageView
    //   0	494	1	paramString	String
    //   0	494	2	paramImageOptions	ImageOptions
    //   0	494	3	paramCommonCallback	Callback.CommonCallback<Drawable>
    //   284	166	4	bool1	boolean
    //   265	137	5	bool2	boolean
    //   232	178	6	bool3	boolean
    //   32	456	7	localImageOptions	ImageOptions
    //   82	128	8	localObject1	Object
    //   64	149	9	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   322	328	331	java/lang/Throwable
    //   295	313	342	finally
    //   295	313	346	java/lang/Throwable
    //   373	379	382	java/lang/Throwable
    //   267	286	393	finally
    //   354	361	393	finally
    //   267	286	400	java/lang/Throwable
    //   354	361	400	java/lang/Throwable
    //   234	250	408	finally
    //   250	264	408	finally
    //   428	446	408	finally
    //   234	250	416	java/lang/Throwable
    //   250	264	416	java/lang/Throwable
    //   420	428	448	finally
    //   458	464	467	java/lang/Throwable
  }
  
  private Callback.Cancelable doLoad(ImageView paramImageView, String paramString, ImageOptions arg3, Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    this.viewRef = new WeakReference(paramImageView);
    this.options = ???;
    this.key = new MemCacheKey(paramString, ???);
    this.callback = paramCommonCallback;
    if ((paramCommonCallback instanceof Callback.ProgressCallback)) {
      this.progressCallback = ((Callback.ProgressCallback)paramCommonCallback);
    }
    if ((paramCommonCallback instanceof Callback.PrepareCallback)) {
      this.prepareCallback = ((Callback.PrepareCallback)paramCommonCallback);
    }
    if ((paramCommonCallback instanceof Callback.CacheCallback)) {
      this.cacheCallback = ((Callback.CacheCallback)paramCommonCallback);
    }
    if (???.isForceLoadingDrawable())
    {
      paramCommonCallback = ???.getLoadingDrawable(paramImageView);
      paramImageView.setScaleType(???.getPlaceholderScaleType());
      paramImageView.setImageDrawable(new AsyncDrawable(this, paramCommonCallback));
    }
    else
    {
      paramImageView.setImageDrawable(new AsyncDrawable(this, paramImageView.getDrawable()));
    }
    paramCommonCallback = createRequestParams(paramString, ???);
    if ((paramImageView instanceof FakeImageView)) {
      synchronized (FAKE_IMG_MAP)
      {
        FAKE_IMG_MAP.put(paramString, (FakeImageView)paramImageView);
      }
    }
    paramImageView = x.http().get(paramCommonCallback, this);
    this.cancelable = paramImageView;
    return paramImageView;
  }
  
  static Callback.Cancelable doLoadDrawable(String paramString, ImageOptions paramImageOptions, Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    if (TextUtils.isEmpty(paramString))
    {
      postArgsException(null, paramImageOptions, "url is null", paramCommonCallback);
      return null;
    }
    synchronized (FAKE_IMG_MAP)
    {
      FakeImageView localFakeImageView2 = (FakeImageView)FAKE_IMG_MAP.get(paramString);
      FakeImageView localFakeImageView1 = localFakeImageView2;
      if (localFakeImageView2 == null)
      {
        localFakeImageView1 = new org/xutils/image/ImageLoader$FakeImageView;
        localFakeImageView1.<init>();
      }
      return doBind(localFakeImageView1, paramString, paramImageOptions, paramCommonCallback);
    }
  }
  
  static Callback.Cancelable doLoadFile(String paramString, ImageOptions paramImageOptions, Callback.CacheCallback<File> paramCacheCallback)
  {
    if (TextUtils.isEmpty(paramString))
    {
      postArgsException(null, paramImageOptions, "url is null", paramCacheCallback);
      return null;
    }
    paramString = createRequestParams(paramString, paramImageOptions);
    return x.http().get(paramString, paramCacheCallback);
  }
  
  private static void postArgsException(final ImageView paramImageView, final ImageOptions paramImageOptions, final String paramString, Callback.CommonCallback<?> paramCommonCallback)
  {
    x.task().autoPost(new Runnable()
    {
      /* Error */
      public void run()
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   4: instanceof 38
        //   7: ifeq +15 -> 22
        //   10: aload_0
        //   11: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   14: checkcast 38	org/xutils/common/Callback$ProgressCallback
        //   17: invokeinterface 41 1 0
        //   22: aload_0
        //   23: getfield 25	org/xutils/image/ImageLoader$3:val$view	Landroid/widget/ImageView;
        //   26: ifnull +42 -> 68
        //   29: aload_0
        //   30: getfield 27	org/xutils/image/ImageLoader$3:val$options	Lorg/xutils/image/ImageOptions;
        //   33: ifnull +35 -> 68
        //   36: aload_0
        //   37: getfield 25	org/xutils/image/ImageLoader$3:val$view	Landroid/widget/ImageView;
        //   40: aload_0
        //   41: getfield 27	org/xutils/image/ImageLoader$3:val$options	Lorg/xutils/image/ImageOptions;
        //   44: invokevirtual 47	org/xutils/image/ImageOptions:getPlaceholderScaleType	()Landroid/widget/ImageView$ScaleType;
        //   47: invokevirtual 53	android/widget/ImageView:setScaleType	(Landroid/widget/ImageView$ScaleType;)V
        //   50: aload_0
        //   51: getfield 25	org/xutils/image/ImageLoader$3:val$view	Landroid/widget/ImageView;
        //   54: aload_0
        //   55: getfield 27	org/xutils/image/ImageLoader$3:val$options	Lorg/xutils/image/ImageOptions;
        //   58: aload_0
        //   59: getfield 25	org/xutils/image/ImageLoader$3:val$view	Landroid/widget/ImageView;
        //   62: invokevirtual 57	org/xutils/image/ImageOptions:getFailureDrawable	(Landroid/widget/ImageView;)Landroid/graphics/drawable/Drawable;
        //   65: invokevirtual 61	android/widget/ImageView:setImageDrawable	(Landroid/graphics/drawable/Drawable;)V
        //   68: aload_0
        //   69: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   72: ifnull +28 -> 100
        //   75: aload_0
        //   76: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   79: astore_2
        //   80: new 63	java/lang/IllegalArgumentException
        //   83: astore_1
        //   84: aload_1
        //   85: aload_0
        //   86: getfield 29	org/xutils/image/ImageLoader$3:val$exMsg	Ljava/lang/String;
        //   89: invokespecial 66	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //   92: aload_2
        //   93: aload_1
        //   94: iconst_0
        //   95: invokeinterface 72 3 0
        //   100: aload_0
        //   101: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   104: astore_1
        //   105: aload_1
        //   106: ifnull +76 -> 182
        //   109: aload_1
        //   110: invokeinterface 75 1 0
        //   115: goto +67 -> 182
        //   118: astore_1
        //   119: goto +64 -> 183
        //   122: astore_2
        //   123: aload_0
        //   124: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   127: astore_1
        //   128: aload_1
        //   129: ifnull +26 -> 155
        //   132: aload_0
        //   133: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   136: aload_2
        //   137: iconst_1
        //   138: invokeinterface 72 3 0
        //   143: goto +12 -> 155
        //   146: astore_1
        //   147: aload_1
        //   148: invokevirtual 79	java/lang/Throwable:getMessage	()Ljava/lang/String;
        //   151: aload_1
        //   152: invokestatic 85	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   155: aload_0
        //   156: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   159: astore_1
        //   160: aload_1
        //   161: ifnull +21 -> 182
        //   164: aload_1
        //   165: invokeinterface 75 1 0
        //   170: goto +12 -> 182
        //   173: astore_1
        //   174: aload_1
        //   175: invokevirtual 79	java/lang/Throwable:getMessage	()Ljava/lang/String;
        //   178: aload_1
        //   179: invokestatic 85	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   182: return
        //   183: aload_0
        //   184: getfield 23	org/xutils/image/ImageLoader$3:val$callback	Lorg/xutils/common/Callback$CommonCallback;
        //   187: astore_2
        //   188: aload_2
        //   189: ifnull +21 -> 210
        //   192: aload_2
        //   193: invokeinterface 75 1 0
        //   198: goto +12 -> 210
        //   201: astore_2
        //   202: aload_2
        //   203: invokevirtual 79	java/lang/Throwable:getMessage	()Ljava/lang/String;
        //   206: aload_2
        //   207: invokestatic 85	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   210: aload_1
        //   211: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	212	0	this	3
        //   83	27	1	localObject1	Object
        //   118	1	1	localObject2	Object
        //   127	2	1	localCommonCallback1	Callback.CommonCallback
        //   146	6	1	localThrowable1	Throwable
        //   159	6	1	localCommonCallback2	Callback.CommonCallback
        //   173	38	1	localThrowable2	Throwable
        //   79	14	2	localCommonCallback3	Callback.CommonCallback
        //   122	15	2	localThrowable3	Throwable
        //   187	6	2	localCommonCallback4	Callback.CommonCallback
        //   201	6	2	localThrowable4	Throwable
        // Exception table:
        //   from	to	target	type
        //   0	22	118	finally
        //   22	68	118	finally
        //   68	100	118	finally
        //   123	128	118	finally
        //   132	143	118	finally
        //   147	155	118	finally
        //   0	22	122	java/lang/Throwable
        //   22	68	122	java/lang/Throwable
        //   68	100	122	java/lang/Throwable
        //   132	143	146	java/lang/Throwable
        //   109	115	173	java/lang/Throwable
        //   164	170	173	java/lang/Throwable
        //   192	198	201	java/lang/Throwable
      }
    });
  }
  
  private void setErrorDrawable4Callback()
  {
    ImageView localImageView = (ImageView)this.viewRef.get();
    if (localImageView != null)
    {
      Drawable localDrawable = this.options.getFailureDrawable(localImageView);
      localImageView.setScaleType(this.options.getPlaceholderScaleType());
      localImageView.setImageDrawable(localDrawable);
    }
  }
  
  private void setSuccessDrawable4Callback(Drawable paramDrawable)
  {
    ImageView localImageView = (ImageView)this.viewRef.get();
    if (localImageView != null)
    {
      localImageView.setScaleType(this.options.getImageScaleType());
      if ((paramDrawable instanceof GifDrawable))
      {
        if (localImageView.getScaleType() == ImageView.ScaleType.CENTER) {
          localImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        localImageView.setLayerType(1, null);
      }
      if (this.options.getAnimation() != null) {
        ImageAnimationHelper.animationDisplay(localImageView, paramDrawable, this.options.getAnimation());
      } else if (this.options.isFadeIn()) {
        ImageAnimationHelper.fadeInDisplay(localImageView, paramDrawable);
      } else {
        localImageView.setImageDrawable(paramDrawable);
      }
    }
  }
  
  private boolean validView4Callback(boolean paramBoolean)
  {
    ImageView localImageView = (ImageView)this.viewRef.get();
    if (localImageView != null)
    {
      Object localObject = localImageView.getDrawable();
      if ((localObject instanceof AsyncDrawable))
      {
        localObject = ((AsyncDrawable)localObject).getImageLoader();
        if (localObject != null)
        {
          if (localObject == this)
          {
            if (localImageView.getVisibility() != 0)
            {
              ((ImageLoader)localObject).cancel();
              return false;
            }
            return true;
          }
          if (this.seq > ((ImageLoader)localObject).seq)
          {
            ((ImageLoader)localObject).cancel();
            return true;
          }
          cancel();
          return false;
        }
      }
      else if (paramBoolean)
      {
        cancel();
        return false;
      }
      return true;
    }
    return false;
  }
  
  public void cancel()
  {
    this.stopped = true;
    this.cancelled = true;
    Callback.Cancelable localCancelable = this.cancelable;
    if (localCancelable != null) {
      localCancelable.cancel();
    }
  }
  
  public Type getLoadType()
  {
    return loadType;
  }
  
  public boolean isCancelled()
  {
    boolean bool2 = this.cancelled;
    boolean bool1 = false;
    if ((bool2) || (!validView4Callback(false))) {
      bool1 = true;
    }
    return bool1;
  }
  
  public boolean onCache(Drawable paramDrawable)
  {
    if (!validView4Callback(true)) {
      return false;
    }
    if (paramDrawable != null)
    {
      this.hasCache = true;
      setSuccessDrawable4Callback(paramDrawable);
      Object localObject = this.cacheCallback;
      if (localObject != null) {
        return ((Callback.CacheCallback)localObject).onCache(paramDrawable);
      }
      localObject = this.callback;
      if (localObject != null)
      {
        ((Callback.CommonCallback)localObject).onSuccess(paramDrawable);
        return true;
      }
      return true;
    }
    return false;
  }
  
  public void onCancelled(Callback.CancelledException paramCancelledException)
  {
    this.stopped = true;
    if (!validView4Callback(false)) {
      return;
    }
    Callback.CommonCallback localCommonCallback = this.callback;
    if (localCommonCallback != null) {
      localCommonCallback.onCancelled(paramCancelledException);
    }
  }
  
  public void onError(Throwable paramThrowable, boolean paramBoolean)
  {
    this.stopped = true;
    if (!validView4Callback(false)) {
      return;
    }
    if ((paramThrowable instanceof FileLockedException))
    {
      paramThrowable = new StringBuilder();
      paramThrowable.append("ImageFileLocked: ");
      paramThrowable.append(this.key.url);
      LogUtil.d(paramThrowable.toString());
      x.task().postDelayed(new Runnable()
      {
        public void run()
        {
          ImageLoader.doBind((ImageView)ImageLoader.this.viewRef.get(), ImageLoader.this.key.url, ImageLoader.this.options, ImageLoader.this.callback);
        }
      }, 10L);
      return;
    }
    LogUtil.e(this.key.url, paramThrowable);
    setErrorDrawable4Callback();
    Callback.CommonCallback localCommonCallback = this.callback;
    if (localCommonCallback != null) {
      localCommonCallback.onError(paramThrowable, paramBoolean);
    }
  }
  
  public void onFinished()
  {
    this.stopped = true;
    if (((ImageView)this.viewRef.get() instanceof FakeImageView)) {
      synchronized (FAKE_IMG_MAP)
      {
        FAKE_IMG_MAP.remove(this.key.url);
      }
    }
    if (!validView4Callback(false)) {
      return;
    }
    ??? = this.callback;
    if (??? != null) {
      ((Callback.CommonCallback)???).onFinished();
    }
  }
  
  public void onLoading(long paramLong1, long paramLong2, boolean paramBoolean)
  {
    if (validView4Callback(true))
    {
      Callback.ProgressCallback localProgressCallback = this.progressCallback;
      if (localProgressCallback != null) {
        localProgressCallback.onLoading(paramLong1, paramLong2, paramBoolean);
      }
    }
  }
  
  public void onStarted()
  {
    if (validView4Callback(true))
    {
      Callback.ProgressCallback localProgressCallback = this.progressCallback;
      if (localProgressCallback != null) {
        localProgressCallback.onStarted();
      }
    }
  }
  
  public void onSuccess(Drawable paramDrawable)
  {
    if (!validView4Callback(this.hasCache ^ true)) {
      return;
    }
    if (paramDrawable != null)
    {
      setSuccessDrawable4Callback(paramDrawable);
      Callback.CommonCallback localCommonCallback = this.callback;
      if (localCommonCallback != null) {
        localCommonCallback.onSuccess(paramDrawable);
      }
    }
  }
  
  public void onWaiting()
  {
    Callback.ProgressCallback localProgressCallback = this.progressCallback;
    if (localProgressCallback != null) {
      localProgressCallback.onWaiting();
    }
  }
  
  public Drawable prepare(File paramFile)
  {
    if (!validView4Callback(true)) {
      return null;
    }
    try
    {
      Drawable localDrawable1;
      if (this.prepareCallback != null) {
        localDrawable1 = (Drawable)this.prepareCallback.prepare(paramFile);
      } else {
        localDrawable1 = null;
      }
      Drawable localDrawable2 = localDrawable1;
      if (localDrawable1 == null) {
        localDrawable2 = ImageDecoder.decodeFileWithLock(paramFile, this.options, this);
      }
      if ((localDrawable2 != null) && ((localDrawable2 instanceof ReusableDrawable)))
      {
        ((ReusableDrawable)localDrawable2).setMemCacheKey(this.key);
        MEM_CACHE.put(this.key, localDrawable2);
      }
      return localDrawable2;
    }
    catch (IOException localIOException)
    {
      IOUtil.deleteFileOrDir(paramFile);
      LogUtil.w(localIOException.getMessage(), localIOException);
    }
    return null;
  }
  
  @SuppressLint({"ViewConstructor"})
  private static final class FakeImageView
    extends ImageView
  {
    private Drawable drawable;
    
    public FakeImageView()
    {
      super();
    }
    
    public Drawable getDrawable()
    {
      return this.drawable;
    }
    
    public void setImageDrawable(Drawable paramDrawable)
    {
      this.drawable = paramDrawable;
    }
    
    public void setLayerType(int paramInt, Paint paramPaint) {}
    
    public void setScaleType(ImageView.ScaleType paramScaleType) {}
    
    public void startAnimation(Animation paramAnimation) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ImageLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */