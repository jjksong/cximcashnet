package org.xutils.image;

final class MemCacheKey
{
  public final ImageOptions options;
  public final String url;
  
  public MemCacheKey(String paramString, ImageOptions paramImageOptions)
  {
    this.url = paramString;
    this.options = paramImageOptions;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (MemCacheKey)paramObject;
      if (!this.url.equals(((MemCacheKey)paramObject).url)) {
        return false;
      }
      return this.options.equals(((MemCacheKey)paramObject).options);
    }
    return false;
  }
  
  public int hashCode()
  {
    return this.url.hashCode() * 31 + this.options.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.url);
    localStringBuilder.append(this.options.toString());
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/MemCacheKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */