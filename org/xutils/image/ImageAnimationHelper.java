package org.xutils.image;

import android.graphics.drawable.Drawable;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import java.lang.reflect.Method;
import org.xutils.common.util.LogUtil;

public final class ImageAnimationHelper
{
  private static final Method cloneMethod;
  
  static
  {
    Method localMethod2;
    try
    {
      Method localMethod1 = Animation.class.getDeclaredMethod("clone", new Class[0]);
      localMethod1.setAccessible(true);
    }
    catch (Throwable localThrowable)
    {
      LogUtil.w(localThrowable.getMessage(), localThrowable);
      localMethod2 = null;
    }
    cloneMethod = localMethod2;
  }
  
  public static void animationDisplay(ImageView paramImageView, Drawable paramDrawable, Animation paramAnimation)
  {
    paramImageView.setImageDrawable(paramDrawable);
    paramDrawable = cloneMethod;
    if ((paramDrawable != null) && (paramAnimation != null)) {
      try
      {
        paramImageView.startAnimation((Animation)paramDrawable.invoke(paramAnimation, new Object[0]));
      }
      catch (Throwable paramDrawable)
      {
        paramImageView.startAnimation(paramAnimation);
      }
    } else {
      paramImageView.startAnimation(paramAnimation);
    }
  }
  
  public static void fadeInDisplay(ImageView paramImageView, Drawable paramDrawable)
  {
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(0.0F, 1.0F);
    localAlphaAnimation.setDuration(300L);
    localAlphaAnimation.setInterpolator(new DecelerateInterpolator());
    paramImageView.setImageDrawable(paramDrawable);
    paramImageView.startAnimation(localAlphaAnimation);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ImageAnimationHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */