package org.xutils.image;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import org.xutils.cache.LruDiskCache;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CancelledException;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.LogUtil;

public final class ImageDecoder
{
  private static final int BITMAP_DECODE_MAX_WORKER;
  private static final byte[] GIF_HEADER;
  private static final LruDiskCache THUMB_CACHE;
  private static final Executor THUMB_CACHE_EXECUTOR;
  private static final Object bitmapDecodeLock;
  private static final AtomicInteger bitmapDecodeWorker;
  private static final Object gifDecodeLock;
  private static final boolean supportWebP;
  
  static
  {
    boolean bool = false;
    bitmapDecodeWorker = new AtomicInteger(0);
    bitmapDecodeLock = new Object();
    gifDecodeLock = new Object();
    GIF_HEADER = new byte[] { 71, 73, 70 };
    int i = 1;
    THUMB_CACHE_EXECUTOR = new PriorityExecutor(1, true);
    THUMB_CACHE = LruDiskCache.getDiskCache("xUtils_img_thumb");
    if (Build.VERSION.SDK_INT > 16) {
      bool = true;
    }
    supportWebP = bool;
    if (Runtime.getRuntime().availableProcessors() > 4) {
      i = 2;
    }
    BITMAP_DECODE_MAX_WORKER = i;
  }
  
  public static int calculateSampleSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int j = 1;
    int k = 1;
    if ((paramInt1 > paramInt3) || (paramInt2 > paramInt4))
    {
      int i;
      if (paramInt1 > paramInt2) {
        i = Math.round(paramInt2 / paramInt4);
      } else {
        i = Math.round(paramInt1 / paramInt3);
      }
      if (i < 1) {
        i = k;
      }
      float f2 = paramInt1 * paramInt2;
      float f1 = paramInt3 * paramInt4 * 2;
      for (;;)
      {
        j = i;
        if (f2 / (i * i) <= f1) {
          break;
        }
        i++;
      }
    }
    return j;
  }
  
  static void clearCacheFiles()
  {
    THUMB_CACHE.clearCacheFiles();
  }
  
  public static Bitmap cut2Circular(Bitmap paramBitmap, boolean paramBoolean)
  {
    int i = paramBitmap.getWidth();
    int k = paramBitmap.getHeight();
    int j = Math.min(i, k);
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    Bitmap localBitmap = Bitmap.createBitmap(j, j, Bitmap.Config.ARGB_8888);
    Object localObject = paramBitmap;
    if (localBitmap != null)
    {
      localObject = new Canvas(localBitmap);
      float f = j / 2;
      ((Canvas)localObject).drawCircle(f, f, f, localPaint);
      localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
      ((Canvas)localObject).drawBitmap(paramBitmap, (j - i) / 2, (j - k) / 2, localPaint);
      if (paramBoolean) {
        paramBitmap.recycle();
      }
      localObject = localBitmap;
    }
    return (Bitmap)localObject;
  }
  
  public static Bitmap cut2RoundCorner(Bitmap paramBitmap, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramInt <= 0) {
      return paramBitmap;
    }
    int k = paramBitmap.getWidth();
    int m = paramBitmap.getHeight();
    int i;
    int j;
    if (paramBoolean1)
    {
      i = Math.min(k, m);
      j = i;
    }
    else
    {
      i = k;
      j = m;
    }
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    Object localObject = paramBitmap;
    if (localBitmap != null)
    {
      localObject = new Canvas(localBitmap);
      RectF localRectF = new RectF(0.0F, 0.0F, i, j);
      float f = paramInt;
      ((Canvas)localObject).drawRoundRect(localRectF, f, f, localPaint);
      localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
      ((Canvas)localObject).drawBitmap(paramBitmap, (i - k) / 2, (j - m) / 2, localPaint);
      if (paramBoolean2) {
        paramBitmap.recycle();
      }
      localObject = localBitmap;
    }
    return (Bitmap)localObject;
  }
  
  public static Bitmap cut2ScaleSize(Bitmap paramBitmap, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int j = paramBitmap.getWidth();
    int i = paramBitmap.getHeight();
    if ((j == paramInt1) && (i == paramInt2)) {
      return paramBitmap;
    }
    Object localObject = new Matrix();
    float f6 = paramInt1;
    float f3 = j;
    float f1 = f6 / f3;
    float f5 = paramInt2;
    float f4 = i;
    float f2 = f5 / f4;
    if (f1 > f2)
    {
      f2 = f5 / f1;
      paramInt1 = (int)((f4 - f2) / 2.0F);
      i = (int)((f4 + f2) / 2.0F);
      int k = 0;
      paramInt2 = j;
      j = k;
    }
    else
    {
      f1 = f6 / f2;
      j = (int)((f3 - f1) / 2.0F);
      paramInt2 = (int)((f3 + f1) / 2.0F);
      f1 = f2;
      paramInt1 = 0;
    }
    ((Matrix)localObject).setScale(f1, f1);
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap, j, paramInt1, paramInt2 - j, i - paramInt1, (Matrix)localObject, true);
    localObject = paramBitmap;
    if (localBitmap != null)
    {
      if ((paramBoolean) && (localBitmap != paramBitmap)) {
        paramBitmap.recycle();
      }
      localObject = localBitmap;
    }
    return (Bitmap)localObject;
  }
  
  public static Bitmap cut2Square(Bitmap paramBitmap, boolean paramBoolean)
  {
    int k = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    if (k == j) {
      return paramBitmap;
    }
    int i = Math.min(k, j);
    Bitmap localBitmap2 = Bitmap.createBitmap(paramBitmap, (k - i) / 2, (j - i) / 2, i, i);
    Bitmap localBitmap1 = paramBitmap;
    if (localBitmap2 != null)
    {
      if ((paramBoolean) && (localBitmap2 != paramBitmap)) {
        paramBitmap.recycle();
      }
      localBitmap1 = localBitmap2;
    }
    return localBitmap1;
  }
  
  public static Bitmap decodeBitmap(File paramFile, ImageOptions paramImageOptions, Callback.Cancelable paramCancelable)
    throws IOException
  {
    if ((paramFile != null) && (paramFile.exists()) && (paramFile.length() >= 1L))
    {
      ImageOptions localImageOptions = paramImageOptions;
      if (paramImageOptions == null) {
        localImageOptions = ImageOptions.DEFAULT;
      }
      if ((localImageOptions.getMaxWidth() <= 0) || (localImageOptions.getMaxHeight() <= 0)) {
        localImageOptions.optimizeMaxSize(null);
      }
      if (paramCancelable != null) {}
      try
      {
        if (paramCancelable.isCancelled())
        {
          paramFile = new org/xutils/common/Callback$CancelledException;
          paramFile.<init>("cancelled during decode image");
          throw paramFile;
        }
        paramImageOptions = new android/graphics/BitmapFactory$Options;
        paramImageOptions.<init>();
        paramImageOptions.inJustDecodeBounds = true;
        paramImageOptions.inPurgeable = true;
        paramImageOptions.inInputShareable = true;
        BitmapFactory.decodeFile(paramFile.getAbsolutePath(), paramImageOptions);
        int j = 0;
        paramImageOptions.inJustDecodeBounds = false;
        paramImageOptions.inPreferredConfig = localImageOptions.getConfig();
        int i1 = paramImageOptions.outWidth;
        int n = paramImageOptions.outHeight;
        int i2 = localImageOptions.getWidth();
        int i3 = localImageOptions.getHeight();
        int k = i1;
        int i = n;
        if (localImageOptions.isAutoRotate())
        {
          int m = getRotateAngle(paramFile.getAbsolutePath());
          j = m;
          k = i1;
          i = n;
          if (m / 90 % 2 == 1)
          {
            k = paramImageOptions.outHeight;
            i = paramImageOptions.outWidth;
            j = m;
          }
        }
        if ((!localImageOptions.isCrop()) && (i2 > 0) && (i3 > 0)) {
          if (j / 90 % 2 == 1)
          {
            paramImageOptions.outWidth = i3;
            paramImageOptions.outHeight = i2;
          }
          else
          {
            paramImageOptions.outWidth = i2;
            paramImageOptions.outHeight = i3;
          }
        }
        paramImageOptions.inSampleSize = calculateSampleSize(k, i, localImageOptions.getMaxWidth(), localImageOptions.getMaxHeight());
        if ((paramCancelable != null) && (paramCancelable.isCancelled()))
        {
          paramFile = new org/xutils/common/Callback$CancelledException;
          paramFile.<init>("cancelled during decode image");
          throw paramFile;
        }
        paramImageOptions = BitmapFactory.decodeFile(paramFile.getAbsolutePath(), paramImageOptions);
        if (paramImageOptions != null)
        {
          if ((paramCancelable != null) && (paramCancelable.isCancelled()))
          {
            paramFile = new org/xutils/common/Callback$CancelledException;
            paramFile.<init>("cancelled during decode image");
            throw paramFile;
          }
          paramFile = paramImageOptions;
          if (j != 0) {
            paramFile = rotate(paramImageOptions, j, true);
          }
          if ((paramCancelable != null) && (paramCancelable.isCancelled()))
          {
            paramFile = new org/xutils/common/Callback$CancelledException;
            paramFile.<init>("cancelled during decode image");
            throw paramFile;
          }
          paramImageOptions = paramFile;
          if (localImageOptions.isCrop())
          {
            paramImageOptions = paramFile;
            if (i2 > 0)
            {
              paramImageOptions = paramFile;
              if (i3 > 0) {
                paramImageOptions = cut2ScaleSize(paramFile, i2, i3, true);
              }
            }
          }
          if (paramImageOptions != null)
          {
            if ((paramCancelable != null) && (paramCancelable.isCancelled()))
            {
              paramFile = new org/xutils/common/Callback$CancelledException;
              paramFile.<init>("cancelled during decode image");
              throw paramFile;
            }
            if (localImageOptions.isCircular())
            {
              paramFile = cut2Circular(paramImageOptions, true);
            }
            else if (localImageOptions.getRadius() > 0)
            {
              paramFile = cut2RoundCorner(paramImageOptions, localImageOptions.getRadius(), localImageOptions.isSquare(), true);
            }
            else
            {
              paramFile = paramImageOptions;
              if (localImageOptions.isSquare()) {
                paramFile = cut2Square(paramImageOptions, true);
              }
            }
            if (paramFile == null)
            {
              paramFile = new java/io/IOException;
              paramFile.<init>("decode image error");
              throw paramFile;
            }
          }
          else
          {
            paramFile = new java/io/IOException;
            paramFile.<init>("decode image error");
            throw paramFile;
          }
        }
        else
        {
          paramFile = new java/io/IOException;
          paramFile.<init>("decode image error");
          throw paramFile;
        }
      }
      catch (Throwable paramFile)
      {
        LogUtil.e(paramFile.getMessage(), paramFile);
        paramFile = null;
        return paramFile;
      }
      catch (IOException paramFile)
      {
        throw paramFile;
      }
    }
    return null;
  }
  
  /* Error */
  static android.graphics.drawable.Drawable decodeFileWithLock(File arg0, ImageOptions paramImageOptions, Callback.Cancelable paramCancelable)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_0
    //   4: ifnull +375 -> 379
    //   7: aload_0
    //   8: invokevirtual 196	java/io/File:exists	()Z
    //   11: ifeq +368 -> 379
    //   14: aload_0
    //   15: invokevirtual 200	java/io/File:length	()J
    //   18: lconst_1
    //   19: lcmp
    //   20: ifge +6 -> 26
    //   23: goto +356 -> 379
    //   26: aload_2
    //   27: ifnull +25 -> 52
    //   30: aload_2
    //   31: invokeinterface 221 1 0
    //   36: ifne +6 -> 42
    //   39: goto +13 -> 52
    //   42: new 223	org/xutils/common/Callback$CancelledException
    //   45: dup
    //   46: ldc -31
    //   48: invokespecial 228	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   51: athrow
    //   52: aload_1
    //   53: invokevirtual 321	org/xutils/image/ImageOptions:isIgnoreGif	()Z
    //   56: ifne +58 -> 114
    //   59: aload_0
    //   60: invokestatic 325	org/xutils/image/ImageDecoder:isGif	(Ljava/io/File;)Z
    //   63: ifeq +51 -> 114
    //   66: getstatic 37	org/xutils/image/ImageDecoder:gifDecodeLock	Ljava/lang/Object;
    //   69: astore 4
    //   71: aload 4
    //   73: monitorenter
    //   74: aload_0
    //   75: aload_1
    //   76: aload_2
    //   77: invokestatic 329	org/xutils/image/ImageDecoder:decodeGif	(Ljava/io/File;Lorg/xutils/image/ImageOptions;Lorg/xutils/common/Callback$Cancelable;)Landroid/graphics/Movie;
    //   80: astore_2
    //   81: aload 4
    //   83: monitorexit
    //   84: aload 6
    //   86: astore_1
    //   87: aload_2
    //   88: ifnull +249 -> 337
    //   91: new 331	org/xutils/image/GifDrawable
    //   94: dup
    //   95: aload_2
    //   96: aload_0
    //   97: invokevirtual 200	java/io/File:length	()J
    //   100: l2i
    //   101: invokespecial 334	org/xutils/image/GifDrawable:<init>	(Landroid/graphics/Movie;I)V
    //   104: astore_1
    //   105: goto +232 -> 337
    //   108: astore_0
    //   109: aload 4
    //   111: monitorexit
    //   112: aload_0
    //   113: athrow
    //   114: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   117: astore 4
    //   119: aload 4
    //   121: monitorenter
    //   122: getstatic 31	org/xutils/image/ImageDecoder:bitmapDecodeWorker	Ljava/util/concurrent/atomic/AtomicInteger;
    //   125: invokevirtual 337	java/util/concurrent/atomic/AtomicInteger:get	()I
    //   128: getstatic 78	org/xutils/image/ImageDecoder:BITMAP_DECODE_MAX_WORKER	I
    //   131: if_icmplt +40 -> 171
    //   134: aload_2
    //   135: ifnull +14 -> 149
    //   138: aload_2
    //   139: invokeinterface 221 1 0
    //   144: istore_3
    //   145: iload_3
    //   146: ifne +25 -> 171
    //   149: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   152: invokevirtual 340	java/lang/Object:wait	()V
    //   155: goto -33 -> 122
    //   158: astore_0
    //   159: new 223	org/xutils/common/Callback$CancelledException
    //   162: astore_0
    //   163: aload_0
    //   164: ldc -31
    //   166: invokespecial 228	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   169: aload_0
    //   170: athrow
    //   171: aload 4
    //   173: monitorexit
    //   174: aload_2
    //   175: ifnull +27 -> 202
    //   178: aload_2
    //   179: invokeinterface 221 1 0
    //   184: ifne +6 -> 190
    //   187: goto +15 -> 202
    //   190: new 223	org/xutils/common/Callback$CancelledException
    //   193: astore_0
    //   194: aload_0
    //   195: ldc -31
    //   197: invokespecial 228	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   200: aload_0
    //   201: athrow
    //   202: getstatic 31	org/xutils/image/ImageDecoder:bitmapDecodeWorker	Ljava/util/concurrent/atomic/AtomicInteger;
    //   205: invokevirtual 343	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
    //   208: pop
    //   209: aload_1
    //   210: invokevirtual 346	org/xutils/image/ImageOptions:isCompress	()Z
    //   213: ifeq +13 -> 226
    //   216: aload_0
    //   217: aload_1
    //   218: invokestatic 350	org/xutils/image/ImageDecoder:getThumbCache	(Ljava/io/File;Lorg/xutils/image/ImageOptions;)Landroid/graphics/Bitmap;
    //   221: astore 4
    //   223: goto +6 -> 229
    //   226: aconst_null
    //   227: astore 4
    //   229: aload 4
    //   231: astore 5
    //   233: aload 4
    //   235: ifnonnull +57 -> 292
    //   238: aload_0
    //   239: aload_1
    //   240: aload_2
    //   241: invokestatic 352	org/xutils/image/ImageDecoder:decodeBitmap	(Ljava/io/File;Lorg/xutils/image/ImageOptions;Lorg/xutils/common/Callback$Cancelable;)Landroid/graphics/Bitmap;
    //   244: astore_2
    //   245: aload_2
    //   246: astore 5
    //   248: aload_2
    //   249: ifnull +43 -> 292
    //   252: aload_2
    //   253: astore 5
    //   255: aload_1
    //   256: invokevirtual 346	org/xutils/image/ImageOptions:isCompress	()Z
    //   259: ifeq +33 -> 292
    //   262: getstatic 49	org/xutils/image/ImageDecoder:THUMB_CACHE_EXECUTOR	Ljava/util/concurrent/Executor;
    //   265: astore 4
    //   267: new 6	org/xutils/image/ImageDecoder$1
    //   270: astore 5
    //   272: aload 5
    //   274: aload_0
    //   275: aload_1
    //   276: aload_2
    //   277: invokespecial 354	org/xutils/image/ImageDecoder$1:<init>	(Ljava/io/File;Lorg/xutils/image/ImageOptions;Landroid/graphics/Bitmap;)V
    //   280: aload 4
    //   282: aload 5
    //   284: invokeinterface 360 2 0
    //   289: aload_2
    //   290: astore 5
    //   292: getstatic 31	org/xutils/image/ImageDecoder:bitmapDecodeWorker	Ljava/util/concurrent/atomic/AtomicInteger;
    //   295: invokevirtual 363	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
    //   298: pop
    //   299: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   302: astore_0
    //   303: aload_0
    //   304: monitorenter
    //   305: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   308: invokevirtual 366	java/lang/Object:notifyAll	()V
    //   311: aload_0
    //   312: monitorexit
    //   313: aload 6
    //   315: astore_1
    //   316: aload 5
    //   318: ifnull +19 -> 337
    //   321: new 368	org/xutils/image/ReusableBitmapDrawable
    //   324: dup
    //   325: invokestatic 374	org/xutils/x:app	()Landroid/app/Application;
    //   328: invokevirtual 380	android/app/Application:getResources	()Landroid/content/res/Resources;
    //   331: aload 5
    //   333: invokespecial 383	org/xutils/image/ReusableBitmapDrawable:<init>	(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    //   336: astore_1
    //   337: aload_1
    //   338: areturn
    //   339: astore_1
    //   340: aload_0
    //   341: monitorexit
    //   342: aload_1
    //   343: athrow
    //   344: astore_0
    //   345: aload 4
    //   347: monitorexit
    //   348: aload_0
    //   349: athrow
    //   350: astore_1
    //   351: getstatic 31	org/xutils/image/ImageDecoder:bitmapDecodeWorker	Ljava/util/concurrent/atomic/AtomicInteger;
    //   354: invokevirtual 363	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
    //   357: pop
    //   358: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   361: astore_0
    //   362: aload_0
    //   363: monitorenter
    //   364: getstatic 35	org/xutils/image/ImageDecoder:bitmapDecodeLock	Ljava/lang/Object;
    //   367: invokevirtual 366	java/lang/Object:notifyAll	()V
    //   370: aload_0
    //   371: monitorexit
    //   372: aload_1
    //   373: athrow
    //   374: astore_1
    //   375: aload_0
    //   376: monitorexit
    //   377: aload_1
    //   378: athrow
    //   379: aconst_null
    //   380: areturn
    //   381: astore 5
    //   383: goto -261 -> 122
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	386	1	paramImageOptions	ImageOptions
    //   0	386	2	paramCancelable	Callback.Cancelable
    //   144	2	3	bool	boolean
    //   69	277	4	localObject1	Object
    //   231	101	5	localObject2	Object
    //   381	1	5	localThrowable	Throwable
    //   1	313	6	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   74	84	108	finally
    //   109	112	108	finally
    //   149	155	158	java/lang/InterruptedException
    //   305	313	339	finally
    //   340	342	339	finally
    //   122	134	344	finally
    //   138	145	344	finally
    //   149	155	344	finally
    //   159	171	344	finally
    //   171	174	344	finally
    //   345	348	344	finally
    //   114	122	350	finally
    //   178	187	350	finally
    //   190	202	350	finally
    //   202	223	350	finally
    //   238	245	350	finally
    //   255	289	350	finally
    //   348	350	350	finally
    //   364	372	374	finally
    //   375	377	374	finally
    //   149	155	381	java/lang/Throwable
  }
  
  /* Error */
  public static android.graphics.Movie decodeGif(File paramFile, ImageOptions paramImageOptions, Callback.Cancelable paramCancelable)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: aload_0
    //   7: ifnull +195 -> 202
    //   10: aload_0
    //   11: invokevirtual 196	java/io/File:exists	()Z
    //   14: ifeq +188 -> 202
    //   17: aload_0
    //   18: invokevirtual 200	java/io/File:length	()J
    //   21: lconst_1
    //   22: lcmp
    //   23: ifge +6 -> 29
    //   26: goto +176 -> 202
    //   29: aload_2
    //   30: ifnull +39 -> 69
    //   33: aload 5
    //   35: astore_3
    //   36: aload_2
    //   37: invokeinterface 221 1 0
    //   42: ifne +6 -> 48
    //   45: goto +24 -> 69
    //   48: aload 5
    //   50: astore_3
    //   51: new 223	org/xutils/common/Callback$CancelledException
    //   54: astore_0
    //   55: aload 5
    //   57: astore_3
    //   58: aload_0
    //   59: ldc -31
    //   61: invokespecial 228	org/xutils/common/Callback$CancelledException:<init>	(Ljava/lang/String;)V
    //   64: aload 5
    //   66: astore_3
    //   67: aload_0
    //   68: athrow
    //   69: aload 5
    //   71: astore_3
    //   72: new 385	java/io/BufferedInputStream
    //   75: astore_1
    //   76: aload 5
    //   78: astore_3
    //   79: new 387	java/io/FileInputStream
    //   82: astore_2
    //   83: aload 5
    //   85: astore_3
    //   86: aload_2
    //   87: aload_0
    //   88: invokespecial 390	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   91: aload 5
    //   93: astore_3
    //   94: aload_1
    //   95: aload_2
    //   96: sipush 16384
    //   99: invokespecial 393	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;I)V
    //   102: aload_1
    //   103: astore_0
    //   104: aload_1
    //   105: sipush 16384
    //   108: invokevirtual 398	java/io/InputStream:mark	(I)V
    //   111: aload_1
    //   112: astore_0
    //   113: aload_1
    //   114: invokestatic 404	android/graphics/Movie:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Movie;
    //   117: astore_2
    //   118: aload_2
    //   119: ifnull +9 -> 128
    //   122: aload_1
    //   123: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   126: aload_2
    //   127: areturn
    //   128: aload_1
    //   129: astore_0
    //   130: new 188	java/io/IOException
    //   133: astore_2
    //   134: aload_1
    //   135: astore_0
    //   136: aload_2
    //   137: ldc_w 303
    //   140: invokespecial 304	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   143: aload_1
    //   144: astore_0
    //   145: aload_2
    //   146: athrow
    //   147: astore_2
    //   148: goto +18 -> 166
    //   151: astore_0
    //   152: aload_1
    //   153: astore_3
    //   154: goto +40 -> 194
    //   157: astore_0
    //   158: aload_3
    //   159: astore_1
    //   160: goto +36 -> 196
    //   163: astore_2
    //   164: aconst_null
    //   165: astore_1
    //   166: aload_1
    //   167: astore_0
    //   168: aload_2
    //   169: invokevirtual 307	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   172: aload_2
    //   173: invokestatic 313	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   176: aload_1
    //   177: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   180: aconst_null
    //   181: areturn
    //   182: astore_2
    //   183: aload_0
    //   184: astore_1
    //   185: aload_2
    //   186: astore_0
    //   187: goto +9 -> 196
    //   190: astore_0
    //   191: aload 4
    //   193: astore_3
    //   194: aload_0
    //   195: athrow
    //   196: aload_1
    //   197: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   200: aload_0
    //   201: athrow
    //   202: aconst_null
    //   203: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	paramFile	File
    //   0	204	1	paramImageOptions	ImageOptions
    //   0	204	2	paramCancelable	Callback.Cancelable
    //   35	159	3	localObject1	Object
    //   1	191	4	localObject2	Object
    //   4	88	5	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   104	111	147	java/lang/Throwable
    //   113	118	147	java/lang/Throwable
    //   130	134	147	java/lang/Throwable
    //   136	143	147	java/lang/Throwable
    //   145	147	147	java/lang/Throwable
    //   104	111	151	java/io/IOException
    //   113	118	151	java/io/IOException
    //   130	134	151	java/io/IOException
    //   136	143	151	java/io/IOException
    //   145	147	151	java/io/IOException
    //   36	45	157	finally
    //   51	55	157	finally
    //   58	64	157	finally
    //   67	69	157	finally
    //   72	76	157	finally
    //   79	83	157	finally
    //   86	91	157	finally
    //   94	102	157	finally
    //   194	196	157	finally
    //   36	45	163	java/lang/Throwable
    //   51	55	163	java/lang/Throwable
    //   58	64	163	java/lang/Throwable
    //   67	69	163	java/lang/Throwable
    //   72	76	163	java/lang/Throwable
    //   79	83	163	java/lang/Throwable
    //   86	91	163	java/lang/Throwable
    //   94	102	163	java/lang/Throwable
    //   104	111	182	finally
    //   113	118	182	finally
    //   130	134	182	finally
    //   136	143	182	finally
    //   145	147	182	finally
    //   168	176	182	finally
    //   36	45	190	java/io/IOException
    //   51	55	190	java/io/IOException
    //   58	64	190	java/io/IOException
    //   67	69	190	java/io/IOException
    //   72	76	190	java/io/IOException
    //   79	83	190	java/io/IOException
    //   86	91	190	java/io/IOException
    //   94	102	190	java/io/IOException
  }
  
  public static int getRotateAngle(String paramString)
  {
    int i = 0;
    try
    {
      ExifInterface localExifInterface = new android/media/ExifInterface;
      localExifInterface.<init>(paramString);
      int j = localExifInterface.getAttributeInt("Orientation", 0);
      if (j != 3)
      {
        if (j != 6)
        {
          if (j == 8) {
            i = 270;
          }
        }
        else {
          i = 90;
        }
      }
      else {
        i = 180;
      }
    }
    catch (Throwable paramString)
    {
      LogUtil.e(paramString.getMessage(), paramString);
    }
    return i;
  }
  
  /* Error */
  private static Bitmap getThumbCache(File paramFile, ImageOptions paramImageOptions)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: getstatic 59	org/xutils/image/ImageDecoder:THUMB_CACHE	Lorg/xutils/cache/LruDiskCache;
    //   5: astore 4
    //   7: new 421	java/lang/StringBuilder
    //   10: astore_3
    //   11: aload_3
    //   12: invokespecial 422	java/lang/StringBuilder:<init>	()V
    //   15: aload_3
    //   16: aload_0
    //   17: invokevirtual 244	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   20: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload_3
    //   25: ldc_w 428
    //   28: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_3
    //   33: aload_0
    //   34: invokevirtual 431	java/io/File:lastModified	()J
    //   37: invokevirtual 434	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload_3
    //   42: aload_1
    //   43: invokevirtual 437	org/xutils/image/ImageOptions:toString	()Ljava/lang/String;
    //   46: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload 4
    //   52: aload_3
    //   53: invokevirtual 438	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   56: invokevirtual 442	org/xutils/cache/LruDiskCache:getDiskCacheFile	(Ljava/lang/String;)Lorg/xutils/cache/DiskCacheFile;
    //   59: astore_0
    //   60: aload_0
    //   61: astore_2
    //   62: aload_0
    //   63: ifnull +98 -> 161
    //   66: aload_0
    //   67: astore_2
    //   68: aload_0
    //   69: astore_1
    //   70: aload_0
    //   71: invokevirtual 445	org/xutils/cache/DiskCacheFile:exists	()Z
    //   74: ifeq +87 -> 161
    //   77: aload_0
    //   78: astore_1
    //   79: new 230	android/graphics/BitmapFactory$Options
    //   82: astore_2
    //   83: aload_0
    //   84: astore_1
    //   85: aload_2
    //   86: invokespecial 231	android/graphics/BitmapFactory$Options:<init>	()V
    //   89: aload_0
    //   90: astore_1
    //   91: aload_2
    //   92: iconst_0
    //   93: putfield 234	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   96: aload_0
    //   97: astore_1
    //   98: aload_2
    //   99: iconst_1
    //   100: putfield 237	android/graphics/BitmapFactory$Options:inPurgeable	Z
    //   103: aload_0
    //   104: astore_1
    //   105: aload_2
    //   106: iconst_1
    //   107: putfield 240	android/graphics/BitmapFactory$Options:inInputShareable	Z
    //   110: aload_0
    //   111: astore_1
    //   112: aload_2
    //   113: getstatic 122	android/graphics/Bitmap$Config:ARGB_8888	Landroid/graphics/Bitmap$Config;
    //   116: putfield 257	android/graphics/BitmapFactory$Options:inPreferredConfig	Landroid/graphics/Bitmap$Config;
    //   119: aload_0
    //   120: astore_1
    //   121: aload_0
    //   122: invokevirtual 446	org/xutils/cache/DiskCacheFile:getAbsolutePath	()Ljava/lang/String;
    //   125: aload_2
    //   126: invokestatic 250	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   129: astore_2
    //   130: aload_0
    //   131: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   134: aload_2
    //   135: areturn
    //   136: astore_2
    //   137: goto +12 -> 149
    //   140: astore_0
    //   141: aload_2
    //   142: astore_1
    //   143: goto +25 -> 168
    //   146: astore_2
    //   147: aconst_null
    //   148: astore_0
    //   149: aload_0
    //   150: astore_1
    //   151: aload_2
    //   152: invokevirtual 307	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   155: aload_2
    //   156: invokestatic 449	org/xutils/common/util/LogUtil:w	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   159: aload_0
    //   160: astore_2
    //   161: aload_2
    //   162: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   165: aconst_null
    //   166: areturn
    //   167: astore_0
    //   168: aload_1
    //   169: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   172: aload_0
    //   173: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	174	0	paramFile	File
    //   0	174	1	paramImageOptions	ImageOptions
    //   1	134	2	localObject	Object
    //   136	6	2	localThrowable1	Throwable
    //   146	10	2	localThrowable2	Throwable
    //   160	2	2	localFile	File
    //   10	43	3	localStringBuilder	StringBuilder
    //   5	46	4	localLruDiskCache	LruDiskCache
    // Exception table:
    //   from	to	target	type
    //   70	77	136	java/lang/Throwable
    //   79	83	136	java/lang/Throwable
    //   85	89	136	java/lang/Throwable
    //   91	96	136	java/lang/Throwable
    //   98	103	136	java/lang/Throwable
    //   105	110	136	java/lang/Throwable
    //   112	119	136	java/lang/Throwable
    //   121	130	136	java/lang/Throwable
    //   2	60	140	finally
    //   2	60	146	java/lang/Throwable
    //   70	77	167	finally
    //   79	83	167	finally
    //   85	89	167	finally
    //   91	96	167	finally
    //   98	103	167	finally
    //   105	110	167	finally
    //   112	119	167	finally
    //   121	130	167	finally
    //   151	159	167	finally
  }
  
  /* Error */
  public static boolean isGif(File paramFile)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: aload 5
    //   8: astore_2
    //   9: new 387	java/io/FileInputStream
    //   12: astore_3
    //   13: aload 5
    //   15: astore_2
    //   16: aload_3
    //   17: aload_0
    //   18: invokespecial 390	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   21: aload_3
    //   22: lconst_0
    //   23: iconst_3
    //   24: invokestatic 453	org/xutils/common/util/IOUtil:readBytes	(Ljava/io/InputStream;JI)[B
    //   27: astore_0
    //   28: getstatic 42	org/xutils/image/ImageDecoder:GIF_HEADER	[B
    //   31: aload_0
    //   32: invokestatic 459	java/util/Arrays:equals	([B[B)Z
    //   35: istore_1
    //   36: aload_3
    //   37: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   40: iload_1
    //   41: ireturn
    //   42: astore_0
    //   43: aload_3
    //   44: astore_2
    //   45: goto +35 -> 80
    //   48: astore_2
    //   49: aload_3
    //   50: astore_0
    //   51: aload_2
    //   52: astore_3
    //   53: goto +11 -> 64
    //   56: astore_0
    //   57: goto +23 -> 80
    //   60: astore_3
    //   61: aload 4
    //   63: astore_0
    //   64: aload_0
    //   65: astore_2
    //   66: aload_3
    //   67: invokevirtual 307	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   70: aload_3
    //   71: invokestatic 313	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   74: aload_0
    //   75: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   78: iconst_0
    //   79: ireturn
    //   80: aload_2
    //   81: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   84: aload_0
    //   85: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	86	0	paramFile	File
    //   35	6	1	bool	boolean
    //   8	37	2	localObject1	Object
    //   48	4	2	localThrowable1	Throwable
    //   65	16	2	localFile	File
    //   12	41	3	localObject2	Object
    //   60	11	3	localThrowable2	Throwable
    //   1	61	4	localObject3	Object
    //   4	10	5	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   21	36	42	finally
    //   21	36	48	java/lang/Throwable
    //   9	13	56	finally
    //   16	21	56	finally
    //   66	74	56	finally
    //   9	13	60	java/lang/Throwable
    //   16	21	60	java/lang/Throwable
  }
  
  public static Bitmap rotate(Bitmap paramBitmap, int paramInt, boolean paramBoolean)
  {
    Object localObject2;
    if (paramInt != 0)
    {
      Object localObject1 = new Matrix();
      ((Matrix)localObject1).setRotate(paramInt);
      try
      {
        localObject1 = Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), (Matrix)localObject1, true);
      }
      catch (Throwable localThrowable)
      {
        LogUtil.e(localThrowable.getMessage(), localThrowable);
      }
    }
    else
    {
      localObject2 = null;
    }
    Object localObject3 = paramBitmap;
    if (localObject2 != null)
    {
      if ((paramBoolean) && (localObject2 != paramBitmap)) {
        paramBitmap.recycle();
      }
      localObject3 = localObject2;
    }
    return (Bitmap)localObject3;
  }
  
  /* Error */
  private static void saveThumbCache(File paramFile, ImageOptions paramImageOptions, Bitmap paramBitmap)
  {
    // Byte code:
    //   0: new 465	org/xutils/cache/DiskCacheEntity
    //   3: dup
    //   4: invokespecial 466	org/xutils/cache/DiskCacheEntity:<init>	()V
    //   7: astore_3
    //   8: new 421	java/lang/StringBuilder
    //   11: dup
    //   12: invokespecial 422	java/lang/StringBuilder:<init>	()V
    //   15: astore 4
    //   17: aload 4
    //   19: aload_0
    //   20: invokevirtual 244	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   23: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   26: pop
    //   27: aload 4
    //   29: ldc_w 428
    //   32: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload 4
    //   38: aload_0
    //   39: invokevirtual 431	java/io/File:lastModified	()J
    //   42: invokevirtual 434	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 4
    //   48: aload_1
    //   49: invokevirtual 437	org/xutils/image/ImageOptions:toString	()Ljava/lang/String;
    //   52: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_3
    //   57: aload 4
    //   59: invokevirtual 438	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   62: invokevirtual 469	org/xutils/cache/DiskCacheEntity:setKey	(Ljava/lang/String;)V
    //   65: aconst_null
    //   66: astore_1
    //   67: getstatic 59	org/xutils/image/ImageDecoder:THUMB_CACHE	Lorg/xutils/cache/LruDiskCache;
    //   70: aload_3
    //   71: invokevirtual 473	org/xutils/cache/LruDiskCache:createDiskCacheFile	(Lorg/xutils/cache/DiskCacheEntity;)Lorg/xutils/cache/DiskCacheFile;
    //   74: astore_0
    //   75: aload_0
    //   76: astore_3
    //   77: aload_0
    //   78: ifnull +101 -> 179
    //   81: new 475	java/io/FileOutputStream
    //   84: astore_1
    //   85: aload_1
    //   86: aload_0
    //   87: invokespecial 476	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   90: aload_1
    //   91: astore_3
    //   92: aload_0
    //   93: astore 4
    //   95: getstatic 66	org/xutils/image/ImageDecoder:supportWebP	Z
    //   98: ifeq +16 -> 114
    //   101: aload_1
    //   102: astore_3
    //   103: aload_0
    //   104: astore 4
    //   106: getstatic 482	android/graphics/Bitmap$CompressFormat:WEBP	Landroid/graphics/Bitmap$CompressFormat;
    //   109: astore 5
    //   111: goto +13 -> 124
    //   114: aload_1
    //   115: astore_3
    //   116: aload_0
    //   117: astore 4
    //   119: getstatic 485	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   122: astore 5
    //   124: aload_1
    //   125: astore_3
    //   126: aload_0
    //   127: astore 4
    //   129: aload_2
    //   130: aload 5
    //   132: bipush 80
    //   134: aload_1
    //   135: invokevirtual 489	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   138: pop
    //   139: aload_1
    //   140: astore_3
    //   141: aload_0
    //   142: astore 4
    //   144: aload_1
    //   145: invokevirtual 494	java/io/OutputStream:flush	()V
    //   148: aload_1
    //   149: astore_3
    //   150: aload_0
    //   151: astore 4
    //   153: aload_0
    //   154: invokevirtual 498	org/xutils/cache/DiskCacheFile:commit	()Lorg/xutils/cache/DiskCacheFile;
    //   157: astore_2
    //   158: aload_2
    //   159: astore_3
    //   160: goto +19 -> 179
    //   163: astore_2
    //   164: goto +39 -> 203
    //   167: astore_1
    //   168: aconst_null
    //   169: astore_3
    //   170: goto +69 -> 239
    //   173: astore_2
    //   174: aconst_null
    //   175: astore_1
    //   176: goto +27 -> 203
    //   179: aload_3
    //   180: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   183: aload_1
    //   184: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   187: goto +47 -> 234
    //   190: astore_1
    //   191: aconst_null
    //   192: astore_3
    //   193: aload_3
    //   194: astore_0
    //   195: goto +44 -> 239
    //   198: astore_2
    //   199: aconst_null
    //   200: astore_1
    //   201: aload_1
    //   202: astore_0
    //   203: aload_1
    //   204: astore_3
    //   205: aload_0
    //   206: astore 4
    //   208: aload_0
    //   209: invokestatic 501	org/xutils/common/util/IOUtil:deleteFileOrDir	(Ljava/io/File;)Z
    //   212: pop
    //   213: aload_1
    //   214: astore_3
    //   215: aload_0
    //   216: astore 4
    //   218: aload_2
    //   219: invokevirtual 307	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   222: aload_2
    //   223: invokestatic 449	org/xutils/common/util/LogUtil:w	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   226: aload_0
    //   227: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   230: aload_1
    //   231: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   234: return
    //   235: astore_1
    //   236: aload 4
    //   238: astore_0
    //   239: aload_0
    //   240: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   243: aload_3
    //   244: invokestatic 410	org/xutils/common/util/IOUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   247: aload_1
    //   248: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	249	0	paramFile	File
    //   0	249	1	paramImageOptions	ImageOptions
    //   0	249	2	paramBitmap	Bitmap
    //   7	237	3	localObject1	Object
    //   15	222	4	localObject2	Object
    //   109	22	5	localCompressFormat	android.graphics.Bitmap.CompressFormat
    // Exception table:
    //   from	to	target	type
    //   95	101	163	java/lang/Throwable
    //   106	111	163	java/lang/Throwable
    //   119	124	163	java/lang/Throwable
    //   129	139	163	java/lang/Throwable
    //   144	148	163	java/lang/Throwable
    //   153	158	163	java/lang/Throwable
    //   81	90	167	finally
    //   81	90	173	java/lang/Throwable
    //   67	75	190	finally
    //   67	75	198	java/lang/Throwable
    //   95	101	235	finally
    //   106	111	235	finally
    //   119	124	235	finally
    //   129	139	235	finally
    //   144	148	235	finally
    //   153	158	235	finally
    //   208	213	235	finally
    //   218	226	235	finally
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ImageDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */