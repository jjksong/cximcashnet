package org.xutils.image;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import java.lang.ref.WeakReference;

public final class AsyncDrawable
  extends Drawable
{
  private Drawable baseDrawable;
  private final WeakReference<ImageLoader> imageLoaderReference;
  
  public AsyncDrawable(ImageLoader paramImageLoader, Drawable paramDrawable)
  {
    if (paramImageLoader != null)
    {
      for (this.baseDrawable = paramDrawable;; this.baseDrawable = ((AsyncDrawable)paramDrawable).baseDrawable)
      {
        paramDrawable = this.baseDrawable;
        if (!(paramDrawable instanceof AsyncDrawable)) {
          break;
        }
      }
      this.imageLoaderReference = new WeakReference(paramImageLoader);
      return;
    }
    throw new IllegalArgumentException("imageLoader may not be null");
  }
  
  public void clearColorFilter()
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.clearColorFilter();
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.draw(paramCanvas);
    }
  }
  
  protected void finalize()
    throws Throwable
  {
    super.finalize();
    ImageLoader localImageLoader = getImageLoader();
    if (localImageLoader != null) {
      localImageLoader.cancel();
    }
  }
  
  public Drawable getBaseDrawable()
  {
    return this.baseDrawable;
  }
  
  public int getChangingConfigurations()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = 0;
    } else {
      i = localDrawable.getChangingConfigurations();
    }
    return i;
  }
  
  public Drawable.ConstantState getConstantState()
  {
    Object localObject = this.baseDrawable;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((Drawable)localObject).getConstantState();
    }
    return (Drawable.ConstantState)localObject;
  }
  
  public Drawable getCurrent()
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable == null) {
      localDrawable = null;
    } else {
      localDrawable = localDrawable.getCurrent();
    }
    return localDrawable;
  }
  
  public ImageLoader getImageLoader()
  {
    return (ImageLoader)this.imageLoaderReference.get();
  }
  
  public int getIntrinsicHeight()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = 0;
    } else {
      i = localDrawable.getIntrinsicHeight();
    }
    return i;
  }
  
  public int getIntrinsicWidth()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = 0;
    } else {
      i = localDrawable.getIntrinsicWidth();
    }
    return i;
  }
  
  public int getMinimumHeight()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = 0;
    } else {
      i = localDrawable.getMinimumHeight();
    }
    return i;
  }
  
  public int getMinimumWidth()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = 0;
    } else {
      i = localDrawable.getMinimumWidth();
    }
    return i;
  }
  
  public int getOpacity()
  {
    Drawable localDrawable = this.baseDrawable;
    int i;
    if (localDrawable == null) {
      i = -3;
    } else {
      i = localDrawable.getOpacity();
    }
    return i;
  }
  
  public boolean getPadding(Rect paramRect)
  {
    Drawable localDrawable = this.baseDrawable;
    boolean bool;
    if ((localDrawable != null) && (localDrawable.getPadding(paramRect))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int[] getState()
  {
    Object localObject = this.baseDrawable;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((Drawable)localObject).getState();
    }
    return (int[])localObject;
  }
  
  public Region getTransparentRegion()
  {
    Object localObject = this.baseDrawable;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((Drawable)localObject).getTransparentRegion();
    }
    return (Region)localObject;
  }
  
  public void invalidateSelf()
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.invalidateSelf();
    }
  }
  
  public boolean isStateful()
  {
    Drawable localDrawable = this.baseDrawable;
    boolean bool;
    if ((localDrawable != null) && (localDrawable.isStateful())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public Drawable mutate()
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable == null) {
      localDrawable = null;
    } else {
      localDrawable = localDrawable.mutate();
    }
    return localDrawable;
  }
  
  public void scheduleSelf(Runnable paramRunnable, long paramLong)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.scheduleSelf(paramRunnable, paramLong);
    }
  }
  
  public void setAlpha(int paramInt)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setAlpha(paramInt);
    }
  }
  
  public void setBaseDrawable(Drawable paramDrawable)
  {
    this.baseDrawable = paramDrawable;
  }
  
  public void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  public void setBounds(Rect paramRect)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setBounds(paramRect);
    }
  }
  
  public void setChangingConfigurations(int paramInt)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setChangingConfigurations(paramInt);
    }
  }
  
  public void setColorFilter(int paramInt, PorterDuff.Mode paramMode)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setColorFilter(paramInt, paramMode);
    }
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setColorFilter(paramColorFilter);
    }
  }
  
  public void setDither(boolean paramBoolean)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setDither(paramBoolean);
    }
  }
  
  public void setFilterBitmap(boolean paramBoolean)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.setFilterBitmap(paramBoolean);
    }
  }
  
  public boolean setState(int[] paramArrayOfInt)
  {
    Drawable localDrawable = this.baseDrawable;
    boolean bool;
    if ((localDrawable != null) && (localDrawable.setState(paramArrayOfInt))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    Drawable localDrawable = this.baseDrawable;
    if ((localDrawable != null) && (localDrawable.setVisible(paramBoolean1, paramBoolean2))) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    }
    return paramBoolean1;
  }
  
  public void unscheduleSelf(Runnable paramRunnable)
  {
    Drawable localDrawable = this.baseDrawable;
    if (localDrawable != null) {
      localDrawable.unscheduleSelf(paramRunnable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/AsyncDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */