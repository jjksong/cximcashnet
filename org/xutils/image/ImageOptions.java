package org.xutils.image;

import android.content.res.Resources;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import java.lang.reflect.Field;
import org.xutils.common.util.DensityUtil;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;

public class ImageOptions
{
  public static final ImageOptions DEFAULT = new ImageOptions();
  private Animation animation = null;
  private boolean autoRotate = false;
  private boolean circular = false;
  private boolean compress = true;
  private Bitmap.Config config = Bitmap.Config.RGB_565;
  private boolean crop = false;
  private boolean fadeIn = false;
  private Drawable failureDrawable = null;
  private int failureDrawableId = 0;
  private boolean forceLoadingDrawable = true;
  private int height = 0;
  private boolean ignoreGif = true;
  private ImageView.ScaleType imageScaleType = ImageView.ScaleType.CENTER_CROP;
  private Drawable loadingDrawable = null;
  private int loadingDrawableId = 0;
  private int maxHeight = 0;
  private int maxWidth = 0;
  private ParamsBuilder paramsBuilder;
  private ImageView.ScaleType placeholderScaleType = ImageView.ScaleType.CENTER_INSIDE;
  private int radius = 0;
  private boolean square = false;
  private boolean useMemCache = true;
  private int width = 0;
  
  private static int getImageViewFieldValue(ImageView paramImageView, String paramString)
  {
    j = 0;
    try
    {
      paramString = ImageView.class.getDeclaredField(paramString);
      paramString.setAccessible(true);
      int k = ((Integer)paramString.get(paramImageView)).intValue();
      i = j;
      if (k > 0)
      {
        i = j;
        if (k < Integer.MAX_VALUE) {
          i = k;
        }
      }
    }
    catch (Throwable paramImageView)
    {
      for (;;)
      {
        int i = j;
      }
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (ImageOptions)paramObject;
      if (this.maxWidth != ((ImageOptions)paramObject).maxWidth) {
        return false;
      }
      if (this.maxHeight != ((ImageOptions)paramObject).maxHeight) {
        return false;
      }
      if (this.width != ((ImageOptions)paramObject).width) {
        return false;
      }
      if (this.height != ((ImageOptions)paramObject).height) {
        return false;
      }
      if (this.crop != ((ImageOptions)paramObject).crop) {
        return false;
      }
      if (this.radius != ((ImageOptions)paramObject).radius) {
        return false;
      }
      if (this.square != ((ImageOptions)paramObject).square) {
        return false;
      }
      if (this.circular != ((ImageOptions)paramObject).circular) {
        return false;
      }
      if (this.autoRotate != ((ImageOptions)paramObject).autoRotate) {
        return false;
      }
      if (this.compress != ((ImageOptions)paramObject).compress) {
        return false;
      }
      if (this.config != ((ImageOptions)paramObject).config) {
        bool = false;
      }
      return bool;
    }
    return false;
  }
  
  public Animation getAnimation()
  {
    return this.animation;
  }
  
  public Bitmap.Config getConfig()
  {
    return this.config;
  }
  
  public Drawable getFailureDrawable(ImageView paramImageView)
  {
    if ((this.failureDrawable == null) && (this.failureDrawableId > 0) && (paramImageView != null)) {
      try
      {
        this.failureDrawable = paramImageView.getResources().getDrawable(this.failureDrawableId);
      }
      catch (Throwable paramImageView)
      {
        LogUtil.e(paramImageView.getMessage(), paramImageView);
      }
    }
    return this.failureDrawable;
  }
  
  public int getHeight()
  {
    return this.height;
  }
  
  public ImageView.ScaleType getImageScaleType()
  {
    return this.imageScaleType;
  }
  
  public Drawable getLoadingDrawable(ImageView paramImageView)
  {
    if ((this.loadingDrawable == null) && (this.loadingDrawableId > 0) && (paramImageView != null)) {
      try
      {
        this.loadingDrawable = paramImageView.getResources().getDrawable(this.loadingDrawableId);
      }
      catch (Throwable paramImageView)
      {
        LogUtil.e(paramImageView.getMessage(), paramImageView);
      }
    }
    return this.loadingDrawable;
  }
  
  public int getMaxHeight()
  {
    return this.maxHeight;
  }
  
  public int getMaxWidth()
  {
    return this.maxWidth;
  }
  
  public ParamsBuilder getParamsBuilder()
  {
    return this.paramsBuilder;
  }
  
  public ImageView.ScaleType getPlaceholderScaleType()
  {
    return this.placeholderScaleType;
  }
  
  public int getRadius()
  {
    return this.radius;
  }
  
  public int getWidth()
  {
    return this.width;
  }
  
  public int hashCode()
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public boolean isAutoRotate()
  {
    return this.autoRotate;
  }
  
  public boolean isCircular()
  {
    return this.circular;
  }
  
  public boolean isCompress()
  {
    return this.compress;
  }
  
  public boolean isCrop()
  {
    return this.crop;
  }
  
  public boolean isFadeIn()
  {
    return this.fadeIn;
  }
  
  public boolean isForceLoadingDrawable()
  {
    return this.forceLoadingDrawable;
  }
  
  public boolean isIgnoreGif()
  {
    return this.ignoreGif;
  }
  
  public boolean isSquare()
  {
    return this.square;
  }
  
  public boolean isUseMemCache()
  {
    return this.useMemCache;
  }
  
  final void optimizeMaxSize(ImageView paramImageView)
  {
    int i = this.width;
    int j;
    if (i > 0)
    {
      j = this.height;
      if (j > 0)
      {
        this.maxWidth = i;
        this.maxHeight = j;
        return;
      }
    }
    int i2 = DensityUtil.getScreenWidth();
    int i1 = DensityUtil.getScreenHeight();
    if (this.width < 0)
    {
      this.maxWidth = (i2 * 3 / 2);
      this.compress = false;
    }
    if (this.height < 0)
    {
      this.maxHeight = (i1 * 3 / 2);
      this.compress = false;
    }
    if ((paramImageView == null) && (this.maxWidth <= 0) && (this.maxHeight <= 0))
    {
      this.maxWidth = i2;
      this.maxHeight = i1;
    }
    else
    {
      int k = this.maxWidth;
      int n = this.maxHeight;
      int m = k;
      j = n;
      if (paramImageView != null)
      {
        ViewGroup.LayoutParams localLayoutParams = paramImageView.getLayoutParams();
        m = k;
        i = n;
        if (localLayoutParams != null)
        {
          j = k;
          if (k <= 0) {
            if (localLayoutParams.width > 0)
            {
              i = localLayoutParams.width;
              j = i;
              if (this.width <= 0)
              {
                this.width = i;
                j = i;
              }
            }
            else
            {
              j = k;
              if (localLayoutParams.width != -2) {
                j = paramImageView.getWidth();
              }
            }
          }
          m = j;
          i = n;
          if (n <= 0) {
            if (localLayoutParams.height > 0)
            {
              k = localLayoutParams.height;
              m = j;
              i = k;
              if (this.height <= 0)
              {
                this.height = k;
                m = j;
                i = k;
              }
            }
            else
            {
              m = j;
              i = n;
              if (localLayoutParams.height != -2)
              {
                i = paramImageView.getHeight();
                m = j;
              }
            }
          }
        }
        k = m;
        if (m <= 0) {
          k = getImageViewFieldValue(paramImageView, "mMaxWidth");
        }
        m = k;
        j = i;
        if (i <= 0)
        {
          j = getImageViewFieldValue(paramImageView, "mMaxHeight");
          m = k;
        }
      }
      if (m <= 0) {
        i = i2;
      } else {
        i = m;
      }
      if (j <= 0) {
        j = i1;
      }
      this.maxWidth = i;
      this.maxHeight = j;
    }
  }
  
  public String toString()
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:783)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:706)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:701)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:722)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:706)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:813)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:843)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public static class Builder
  {
    protected ImageOptions options;
    
    public Builder()
    {
      newImageOptions();
    }
    
    public ImageOptions build()
    {
      return this.options;
    }
    
    protected void newImageOptions()
    {
      this.options = new ImageOptions();
    }
    
    public Builder setAnimation(Animation paramAnimation)
    {
      ImageOptions.access$1402(this.options, paramAnimation);
      return this;
    }
    
    public Builder setAutoRotate(boolean paramBoolean)
    {
      ImageOptions.access$602(this.options, paramBoolean);
      return this;
    }
    
    public Builder setCircular(boolean paramBoolean)
    {
      ImageOptions.access$502(this.options, paramBoolean);
      return this;
    }
    
    public Builder setConfig(Bitmap.Config paramConfig)
    {
      ImageOptions.access$702(this.options, paramConfig);
      return this;
    }
    
    public Builder setCrop(boolean paramBoolean)
    {
      ImageOptions.access$202(this.options, paramBoolean);
      return this;
    }
    
    public Builder setFadeIn(boolean paramBoolean)
    {
      ImageOptions.access$1302(this.options, paramBoolean);
      return this;
    }
    
    public Builder setFailureDrawable(Drawable paramDrawable)
    {
      ImageOptions.access$1202(this.options, paramDrawable);
      return this;
    }
    
    public Builder setFailureDrawableId(int paramInt)
    {
      ImageOptions.access$1102(this.options, paramInt);
      return this;
    }
    
    public Builder setForceLoadingDrawable(boolean paramBoolean)
    {
      ImageOptions.access$1702(this.options, paramBoolean);
      return this;
    }
    
    public Builder setIgnoreGif(boolean paramBoolean)
    {
      ImageOptions.access$802(this.options, paramBoolean);
      return this;
    }
    
    public Builder setImageScaleType(ImageView.ScaleType paramScaleType)
    {
      ImageOptions.access$1602(this.options, paramScaleType);
      return this;
    }
    
    public Builder setLoadingDrawable(Drawable paramDrawable)
    {
      ImageOptions.access$1002(this.options, paramDrawable);
      return this;
    }
    
    public Builder setLoadingDrawableId(int paramInt)
    {
      ImageOptions.access$902(this.options, paramInt);
      return this;
    }
    
    public Builder setParamsBuilder(ImageOptions.ParamsBuilder paramParamsBuilder)
    {
      ImageOptions.access$1902(this.options, paramParamsBuilder);
      return this;
    }
    
    public Builder setPlaceholderScaleType(ImageView.ScaleType paramScaleType)
    {
      ImageOptions.access$1502(this.options, paramScaleType);
      return this;
    }
    
    public Builder setRadius(int paramInt)
    {
      ImageOptions.access$302(this.options, paramInt);
      return this;
    }
    
    public Builder setSize(int paramInt1, int paramInt2)
    {
      ImageOptions.access$002(this.options, paramInt1);
      ImageOptions.access$102(this.options, paramInt2);
      return this;
    }
    
    public Builder setSquare(boolean paramBoolean)
    {
      ImageOptions.access$402(this.options, paramBoolean);
      return this;
    }
    
    public Builder setUseMemCache(boolean paramBoolean)
    {
      ImageOptions.access$1802(this.options, paramBoolean);
      return this;
    }
  }
  
  public static abstract interface ParamsBuilder
  {
    public abstract RequestParams buildParams(RequestParams paramRequestParams, ImageOptions paramImageOptions);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ImageOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */