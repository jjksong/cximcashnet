package org.xutils.image;

abstract interface ReusableDrawable
{
  public abstract MemCacheKey getMemCacheKey();
  
  public abstract void setMemCacheKey(MemCacheKey paramMemCacheKey);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ReusableDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */