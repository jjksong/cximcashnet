package org.xutils.image;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.io.File;
import org.xutils.ImageManager;
import org.xutils.common.Callback.CacheCallback;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.common.TaskController;
import org.xutils.x;
import org.xutils.x.Ext;

public final class ImageManagerImpl
  implements ImageManager
{
  private static volatile ImageManagerImpl instance;
  private static final Object lock = new Object();
  
  public static void registerInstance()
  {
    if (instance == null) {
      synchronized (lock)
      {
        if (instance == null)
        {
          ImageManagerImpl localImageManagerImpl = new org/xutils/image/ImageManagerImpl;
          localImageManagerImpl.<init>();
          instance = localImageManagerImpl;
        }
      }
    }
    x.Ext.setImageManager(instance);
  }
  
  public void bind(final ImageView paramImageView, final String paramString)
  {
    x.task().autoPost(new Runnable()
    {
      public void run()
      {
        ImageLoader.doBind(paramImageView, paramString, null, null);
      }
    });
  }
  
  public void bind(final ImageView paramImageView, final String paramString, final Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    x.task().autoPost(new Runnable()
    {
      public void run()
      {
        ImageLoader.doBind(paramImageView, paramString, null, paramCommonCallback);
      }
    });
  }
  
  public void bind(final ImageView paramImageView, final String paramString, final ImageOptions paramImageOptions)
  {
    x.task().autoPost(new Runnable()
    {
      public void run()
      {
        ImageLoader.doBind(paramImageView, paramString, paramImageOptions, null);
      }
    });
  }
  
  public void bind(final ImageView paramImageView, final String paramString, final ImageOptions paramImageOptions, final Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    x.task().autoPost(new Runnable()
    {
      public void run()
      {
        ImageLoader.doBind(paramImageView, paramString, paramImageOptions, paramCommonCallback);
      }
    });
  }
  
  public void clearCacheFiles()
  {
    ImageLoader.clearCacheFiles();
    ImageDecoder.clearCacheFiles();
  }
  
  public void clearMemCache() {}
  
  public Callback.Cancelable loadDrawable(String paramString, ImageOptions paramImageOptions, Callback.CommonCallback<Drawable> paramCommonCallback)
  {
    return ImageLoader.doLoadDrawable(paramString, paramImageOptions, paramCommonCallback);
  }
  
  public Callback.Cancelable loadFile(String paramString, ImageOptions paramImageOptions, Callback.CacheCallback<File> paramCacheCallback)
  {
    return ImageLoader.doLoadFile(paramString, paramImageOptions, paramCacheCallback);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/image/ImageManagerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */