package org.xutils;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "org.xutils";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 20170405;
  public static final String VERSION_NAME = "3.5.0";
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */