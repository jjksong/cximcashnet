package org.xutils;

import android.app.Application;
import android.content.Context;
import java.lang.reflect.Method;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import org.xutils.common.TaskController;
import org.xutils.common.task.TaskControllerImpl;
import org.xutils.db.DbManagerImpl;
import org.xutils.http.HttpManagerImpl;
import org.xutils.image.ImageManagerImpl;
import org.xutils.view.ViewInjectorImpl;

public final class x
{
  public static Application app()
  {
    if (Ext.app == null) {
      try
      {
        Context localContext = (Context)Class.forName("com.android.layoutlib.bridge.impl.RenderAction").getDeclaredMethod("getCurrentContext", new Class[0]).invoke(null, new Object[0]);
        MockApplication localMockApplication = new org/xutils/x$MockApplication;
        localMockApplication.<init>(localContext);
        Ext.access$102(localMockApplication);
      }
      catch (Throwable localThrowable)
      {
        throw new RuntimeException("please invoke x.Ext.init(app) on Application#onCreate() and register your Application in manifest.");
      }
    }
    return Ext.app;
  }
  
  public static DbManager getDb(DbManager.DaoConfig paramDaoConfig)
  {
    return DbManagerImpl.getInstance(paramDaoConfig);
  }
  
  public static HttpManager http()
  {
    if (Ext.httpManager == null) {
      HttpManagerImpl.registerInstance();
    }
    return Ext.httpManager;
  }
  
  public static ImageManager image()
  {
    if (Ext.imageManager == null) {
      ImageManagerImpl.registerInstance();
    }
    return Ext.imageManager;
  }
  
  public static boolean isDebug()
  {
    return Ext.debug;
  }
  
  public static TaskController task()
  {
    return Ext.taskController;
  }
  
  public static ViewInjector view()
  {
    if (Ext.viewInjector == null) {
      ViewInjectorImpl.registerInstance();
    }
    return Ext.viewInjector;
  }
  
  public static class Ext
  {
    private static Application app;
    private static boolean debug;
    private static HttpManager httpManager;
    private static ImageManager imageManager;
    private static TaskController taskController;
    private static ViewInjector viewInjector;
    
    public static void init(Application paramApplication)
    {
      
      if (app == null) {
        app = paramApplication;
      }
    }
    
    public static void setDebug(boolean paramBoolean)
    {
      debug = paramBoolean;
    }
    
    public static void setDefaultHostnameVerifier(HostnameVerifier paramHostnameVerifier)
    {
      HttpsURLConnection.setDefaultHostnameVerifier(paramHostnameVerifier);
    }
    
    public static void setHttpManager(HttpManager paramHttpManager)
    {
      httpManager = paramHttpManager;
    }
    
    public static void setImageManager(ImageManager paramImageManager)
    {
      imageManager = paramImageManager;
    }
    
    public static void setTaskController(TaskController paramTaskController)
    {
      if (taskController == null) {
        taskController = paramTaskController;
      }
    }
    
    public static void setViewInjector(ViewInjector paramViewInjector)
    {
      viewInjector = paramViewInjector;
    }
  }
  
  private static class MockApplication
    extends Application
  {
    public MockApplication(Context paramContext)
    {
      attachBaseContext(paramContext);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/x.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */