package org.xutils;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.io.File;
import org.xutils.common.Callback.CacheCallback;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.image.ImageOptions;

public abstract interface ImageManager
{
  public abstract void bind(ImageView paramImageView, String paramString);
  
  public abstract void bind(ImageView paramImageView, String paramString, Callback.CommonCallback<Drawable> paramCommonCallback);
  
  public abstract void bind(ImageView paramImageView, String paramString, ImageOptions paramImageOptions);
  
  public abstract void bind(ImageView paramImageView, String paramString, ImageOptions paramImageOptions, Callback.CommonCallback<Drawable> paramCommonCallback);
  
  public abstract void clearCacheFiles();
  
  public abstract void clearMemCache();
  
  public abstract Callback.Cancelable loadDrawable(String paramString, ImageOptions paramImageOptions, Callback.CommonCallback<Drawable> paramCommonCallback);
  
  public abstract Callback.Cancelable loadFile(String paramString, ImageOptions paramImageOptions, Callback.CacheCallback<File> paramCacheCallback);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ImageManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */