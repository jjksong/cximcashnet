package org.xutils.ex;

public class HttpRedirectException
  extends HttpException
{
  private static final long serialVersionUID = 1L;
  
  public HttpRedirectException(int paramInt, String paramString1, String paramString2)
  {
    super(paramInt, paramString1);
    setResult(paramString2);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ex/HttpRedirectException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */