package org.xutils.ex;

public class DbException
  extends BaseException
{
  private static final long serialVersionUID = 1L;
  
  public DbException() {}
  
  public DbException(String paramString)
  {
    super(paramString);
  }
  
  public DbException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
  
  public DbException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ex/DbException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */