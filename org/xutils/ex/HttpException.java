package org.xutils.ex;

import android.text.TextUtils;

public class HttpException
  extends BaseException
{
  private static final long serialVersionUID = 1L;
  private int code;
  private String customMessage;
  private String errorCode;
  private String result;
  
  public HttpException(int paramInt, String paramString)
  {
    super(paramString);
    this.code = paramInt;
  }
  
  public int getCode()
  {
    return this.code;
  }
  
  public String getErrorCode()
  {
    String str2 = this.errorCode;
    String str1 = str2;
    if (str2 == null) {
      str1 = String.valueOf(this.code);
    }
    return str1;
  }
  
  public String getMessage()
  {
    if (!TextUtils.isEmpty(this.customMessage)) {
      return this.customMessage;
    }
    return super.getMessage();
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setCode(int paramInt)
  {
    this.code = paramInt;
  }
  
  public void setErrorCode(String paramString)
  {
    this.errorCode = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.customMessage = paramString;
  }
  
  public void setResult(String paramString)
  {
    this.result = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("errorCode: ");
    localStringBuilder.append(getErrorCode());
    localStringBuilder.append(", msg: ");
    localStringBuilder.append(getMessage());
    localStringBuilder.append(", result: ");
    localStringBuilder.append(this.result);
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ex/HttpException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */