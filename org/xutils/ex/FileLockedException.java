package org.xutils.ex;

public class FileLockedException
  extends BaseException
{
  private static final long serialVersionUID = 1L;
  
  public FileLockedException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/ex/FileLockedException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */