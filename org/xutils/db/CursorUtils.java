package org.xutils.db;

import android.database.Cursor;
import java.util.HashMap;
import org.xutils.db.table.ColumnEntity;
import org.xutils.db.table.DbModel;
import org.xutils.db.table.TableEntity;

final class CursorUtils
{
  public static DbModel getDbModel(Cursor paramCursor)
  {
    DbModel localDbModel = new DbModel();
    int j = paramCursor.getColumnCount();
    for (int i = 0; i < j; i++) {
      localDbModel.add(paramCursor.getColumnName(i), paramCursor.getString(i));
    }
    return localDbModel;
  }
  
  public static <T> T getEntity(TableEntity<T> paramTableEntity, Cursor paramCursor)
    throws Throwable
  {
    Object localObject = paramTableEntity.createEntity();
    paramTableEntity = paramTableEntity.getColumnMap();
    int j = paramCursor.getColumnCount();
    for (int i = 0; i < j; i++)
    {
      ColumnEntity localColumnEntity = (ColumnEntity)paramTableEntity.get(paramCursor.getColumnName(i));
      if (localColumnEntity != null) {
        localColumnEntity.setValueFromCursor(localObject, paramCursor, i);
      }
    }
    return (T)localObject;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/CursorUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */