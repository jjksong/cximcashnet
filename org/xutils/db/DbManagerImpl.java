package org.xutils.db;

import android.app.Application;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.xutils.DbManager;
import org.xutils.DbManager.DaoConfig;
import org.xutils.DbManager.DbOpenListener;
import org.xutils.DbManager.DbUpgradeListener;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.db.sqlite.SqlInfo;
import org.xutils.db.sqlite.SqlInfoBuilder;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.db.table.ColumnEntity;
import org.xutils.db.table.DbBase;
import org.xutils.db.table.TableEntity;
import org.xutils.ex.DbException;
import org.xutils.x;

public final class DbManagerImpl
  extends DbBase
{
  private static final HashMap<DbManager.DaoConfig, DbManagerImpl> DAO_MAP = new HashMap();
  private boolean allowTransaction;
  private DbManager.DaoConfig daoConfig;
  private SQLiteDatabase database;
  
  private DbManagerImpl(DbManager.DaoConfig paramDaoConfig)
  {
    if (paramDaoConfig != null)
    {
      this.daoConfig = paramDaoConfig;
      this.allowTransaction = paramDaoConfig.isAllowTransaction();
      this.database = openOrCreateDatabase(paramDaoConfig);
      paramDaoConfig = paramDaoConfig.getDbOpenListener();
      if (paramDaoConfig != null) {
        paramDaoConfig.onDbOpened(this);
      }
      return;
    }
    throw new IllegalArgumentException("daoConfig may not be null");
  }
  
  private void beginTransaction()
  {
    if (this.allowTransaction) {
      if ((Build.VERSION.SDK_INT >= 16) && (this.database.isWriteAheadLoggingEnabled())) {
        this.database.beginTransactionNonExclusive();
      } else {
        this.database.beginTransaction();
      }
    }
  }
  
  private void endTransaction()
  {
    if (this.allowTransaction) {
      this.database.endTransaction();
    }
  }
  
  public static DbManager getInstance(DbManager.DaoConfig paramDaoConfig)
  {
    Object localObject = paramDaoConfig;
    if (paramDaoConfig == null) {}
    try
    {
      localObject = new org/xutils/DbManager$DaoConfig;
      ((DbManager.DaoConfig)localObject).<init>();
      paramDaoConfig = (DbManagerImpl)DAO_MAP.get(localObject);
      if (paramDaoConfig == null)
      {
        paramDaoConfig = new org/xutils/db/DbManagerImpl;
        paramDaoConfig.<init>((DbManager.DaoConfig)localObject);
        DAO_MAP.put(localObject, paramDaoConfig);
      }
      else
      {
        paramDaoConfig.daoConfig = ((DbManager.DaoConfig)localObject);
      }
      SQLiteDatabase localSQLiteDatabase = paramDaoConfig.database;
      int j = localSQLiteDatabase.getVersion();
      int i = ((DbManager.DaoConfig)localObject).getDbVersion();
      if (j != i)
      {
        if (j != 0)
        {
          localObject = ((DbManager.DaoConfig)localObject).getDbUpgradeListener();
          if (localObject != null) {
            ((DbManager.DbUpgradeListener)localObject).onUpgrade(paramDaoConfig, j, i);
          } else {
            try
            {
              paramDaoConfig.dropDb();
            }
            catch (DbException localDbException)
            {
              LogUtil.e(localDbException.getMessage(), localDbException);
            }
          }
        }
        localSQLiteDatabase.setVersion(i);
      }
      return paramDaoConfig;
    }
    finally {}
  }
  
  /* Error */
  private long getLastAutoIncrementId(String paramString)
    throws DbException
  {
    // Byte code:
    //   0: new 133	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 134	java/lang/StringBuilder:<init>	()V
    //   7: astore 6
    //   9: aload 6
    //   11: ldc -120
    //   13: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   16: pop
    //   17: aload 6
    //   19: aload_1
    //   20: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload 6
    //   26: ldc -114
    //   28: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_0
    //   33: aload 6
    //   35: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   38: invokevirtual 149	org/xutils/db/DbManagerImpl:execQuery	(Ljava/lang/String;)Landroid/database/Cursor;
    //   41: astore_1
    //   42: ldc2_w 150
    //   45: lstore 4
    //   47: lload 4
    //   49: lstore_2
    //   50: aload_1
    //   51: ifnull +59 -> 110
    //   54: lload 4
    //   56: lstore_2
    //   57: aload_1
    //   58: invokeinterface 156 1 0
    //   63: ifeq +11 -> 74
    //   66: aload_1
    //   67: iconst_0
    //   68: invokeinterface 160 2 0
    //   73: lstore_2
    //   74: aload_1
    //   75: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   78: goto +32 -> 110
    //   81: astore 6
    //   83: goto +20 -> 103
    //   86: astore 6
    //   88: new 82	org/xutils/ex/DbException
    //   91: astore 7
    //   93: aload 7
    //   95: aload 6
    //   97: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   100: aload 7
    //   102: athrow
    //   103: aload_1
    //   104: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   107: aload 6
    //   109: athrow
    //   110: lload_2
    //   111: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	DbManagerImpl
    //   0	112	1	paramString	String
    //   49	62	2	l1	long
    //   45	10	4	l2	long
    //   7	27	6	localStringBuilder	StringBuilder
    //   81	1	6	localObject	Object
    //   86	22	6	localThrowable	Throwable
    //   91	10	7	localDbException	DbException
    // Exception table:
    //   from	to	target	type
    //   57	74	81	finally
    //   88	103	81	finally
    //   57	74	86	java/lang/Throwable
  }
  
  private SQLiteDatabase openOrCreateDatabase(DbManager.DaoConfig paramDaoConfig)
  {
    File localFile = paramDaoConfig.getDbDir();
    if ((localFile != null) && ((localFile.exists()) || (localFile.mkdirs()))) {
      paramDaoConfig = SQLiteDatabase.openOrCreateDatabase(new File(localFile, paramDaoConfig.getDbName()), null);
    } else {
      paramDaoConfig = x.app().openOrCreateDatabase(paramDaoConfig.getDbName(), 0, null);
    }
    return paramDaoConfig;
  }
  
  private boolean saveBindingIdWithoutTransaction(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    ColumnEntity localColumnEntity = paramTableEntity.getId();
    if (localColumnEntity.isAutoId())
    {
      execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(paramTableEntity, paramObject));
      long l = getLastAutoIncrementId(paramTableEntity.getName());
      if (l == -1L) {
        return false;
      }
      localColumnEntity.setAutoIdValue(paramObject, l);
      return true;
    }
    execNonQuery(SqlInfoBuilder.buildInsertSqlInfo(paramTableEntity, paramObject));
    return true;
  }
  
  private void saveOrUpdateWithoutTransaction(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    ColumnEntity localColumnEntity = paramTableEntity.getId();
    if (localColumnEntity.isAutoId())
    {
      if (localColumnEntity.getColumnValue(paramObject) != null) {
        execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo(paramTableEntity, paramObject, new String[0]));
      } else {
        saveBindingIdWithoutTransaction(paramTableEntity, paramObject);
      }
    }
    else {
      execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo(paramTableEntity, paramObject));
    }
  }
  
  private void setTransactionSuccessful()
  {
    if (this.allowTransaction) {
      this.database.setTransactionSuccessful();
    }
  }
  
  public void close()
    throws IOException
  {
    if (DAO_MAP.containsKey(this.daoConfig))
    {
      DAO_MAP.remove(this.daoConfig);
      this.database.close();
    }
  }
  
  public int delete(Class<?> paramClass, WhereBuilder paramWhereBuilder)
    throws DbException
  {
    paramClass = getTable(paramClass);
    if (!paramClass.tableIsExist()) {
      return 0;
    }
    try
    {
      beginTransaction();
      int i = executeUpdateDelete(SqlInfoBuilder.buildDeleteSqlInfo(paramClass, paramWhereBuilder));
      setTransactionSuccessful();
      return i;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void delete(Class<?> paramClass)
    throws DbException
  {
    delete(paramClass, null);
  }
  
  public void delete(Object paramObject)
    throws DbException
  {
    try
    {
      beginTransaction();
      if ((paramObject instanceof List))
      {
        localObject = (List)paramObject;
        bool = ((List)localObject).isEmpty();
        if (bool) {
          return;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        bool = ((TableEntity)paramObject).tableIsExist();
        if (!bool) {
          return;
        }
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          execNonQuery(SqlInfoBuilder.buildDeleteSqlInfo((TableEntity)paramObject, ((Iterator)localObject).next()));
        }
      }
      Object localObject = getTable(paramObject.getClass());
      boolean bool = ((TableEntity)localObject).tableIsExist();
      if (!bool) {
        return;
      }
      execNonQuery(SqlInfoBuilder.buildDeleteSqlInfo((TableEntity)localObject, paramObject));
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void deleteById(Class<?> paramClass, Object paramObject)
    throws DbException
  {
    paramClass = getTable(paramClass);
    if (!paramClass.tableIsExist()) {
      return;
    }
    try
    {
      beginTransaction();
      execNonQuery(SqlInfoBuilder.buildDeleteSqlInfoById(paramClass, paramObject));
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void execNonQuery(String paramString)
    throws DbException
  {
    try
    {
      this.database.execSQL(paramString);
      return;
    }
    catch (Throwable paramString)
    {
      throw new DbException(paramString);
    }
  }
  
  /* Error */
  public void execNonQuery(SqlInfo paramSqlInfo)
    throws DbException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore_2
    //   4: aload_1
    //   5: aload_0
    //   6: getfield 41	org/xutils/db/DbManagerImpl:database	Landroid/database/sqlite/SQLiteDatabase;
    //   9: invokevirtual 338	org/xutils/db/sqlite/SqlInfo:buildStatement	(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    //   12: astore_1
    //   13: aload_1
    //   14: astore_2
    //   15: aload_1
    //   16: astore_3
    //   17: aload_1
    //   18: invokevirtual 343	android/database/sqlite/SQLiteStatement:execute	()V
    //   21: aload_1
    //   22: ifnull +19 -> 41
    //   25: aload_1
    //   26: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   29: goto +12 -> 41
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   37: aload_1
    //   38: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   41: return
    //   42: astore_1
    //   43: goto +23 -> 66
    //   46: astore 4
    //   48: aload_3
    //   49: astore_2
    //   50: new 82	org/xutils/ex/DbException
    //   53: astore_1
    //   54: aload_3
    //   55: astore_2
    //   56: aload_1
    //   57: aload 4
    //   59: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   62: aload_3
    //   63: astore_2
    //   64: aload_1
    //   65: athrow
    //   66: aload_2
    //   67: ifnull +19 -> 86
    //   70: aload_2
    //   71: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   74: goto +12 -> 86
    //   77: astore_2
    //   78: aload_2
    //   79: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   82: aload_2
    //   83: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   86: aload_1
    //   87: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	88	0	this	DbManagerImpl
    //   0	88	1	paramSqlInfo	SqlInfo
    //   3	68	2	localSqlInfo1	SqlInfo
    //   77	6	2	localThrowable1	Throwable
    //   1	62	3	localSqlInfo2	SqlInfo
    //   46	12	4	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   25	29	32	java/lang/Throwable
    //   4	13	42	finally
    //   17	21	42	finally
    //   50	54	42	finally
    //   56	62	42	finally
    //   64	66	42	finally
    //   4	13	46	java/lang/Throwable
    //   17	21	46	java/lang/Throwable
    //   70	74	77	java/lang/Throwable
  }
  
  public Cursor execQuery(String paramString)
    throws DbException
  {
    try
    {
      paramString = this.database.rawQuery(paramString, null);
      return paramString;
    }
    catch (Throwable paramString)
    {
      throw new DbException(paramString);
    }
  }
  
  public Cursor execQuery(SqlInfo paramSqlInfo)
    throws DbException
  {
    try
    {
      paramSqlInfo = this.database.rawQuery(paramSqlInfo.getSql(), paramSqlInfo.getBindArgsAsStrArray());
      return paramSqlInfo;
    }
    catch (Throwable paramSqlInfo)
    {
      throw new DbException(paramSqlInfo);
    }
  }
  
  /* Error */
  public int executeUpdateDelete(String paramString)
    throws DbException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore_3
    //   5: aload_0
    //   6: getfield 41	org/xutils/db/DbManagerImpl:database	Landroid/database/sqlite/SQLiteDatabase;
    //   9: aload_1
    //   10: invokevirtual 364	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    //   13: astore_1
    //   14: aload_1
    //   15: astore_3
    //   16: aload_1
    //   17: astore 4
    //   19: aload_1
    //   20: invokevirtual 366	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
    //   23: istore_2
    //   24: aload_1
    //   25: ifnull +19 -> 44
    //   28: aload_1
    //   29: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   32: goto +12 -> 44
    //   35: astore_1
    //   36: aload_1
    //   37: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   40: aload_1
    //   41: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   44: iload_2
    //   45: ireturn
    //   46: astore_1
    //   47: goto +26 -> 73
    //   50: astore 5
    //   52: aload 4
    //   54: astore_3
    //   55: new 82	org/xutils/ex/DbException
    //   58: astore_1
    //   59: aload 4
    //   61: astore_3
    //   62: aload_1
    //   63: aload 5
    //   65: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   68: aload 4
    //   70: astore_3
    //   71: aload_1
    //   72: athrow
    //   73: aload_3
    //   74: ifnull +19 -> 93
    //   77: aload_3
    //   78: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   81: goto +12 -> 93
    //   84: astore_3
    //   85: aload_3
    //   86: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   89: aload_3
    //   90: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   93: aload_1
    //   94: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	this	DbManagerImpl
    //   0	95	1	paramString	String
    //   23	22	2	i	int
    //   4	74	3	str1	String
    //   84	6	3	localThrowable1	Throwable
    //   1	68	4	str2	String
    //   50	14	5	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   28	32	35	java/lang/Throwable
    //   5	14	46	finally
    //   19	24	46	finally
    //   55	59	46	finally
    //   62	68	46	finally
    //   71	73	46	finally
    //   5	14	50	java/lang/Throwable
    //   19	24	50	java/lang/Throwable
    //   77	81	84	java/lang/Throwable
  }
  
  /* Error */
  public int executeUpdateDelete(SqlInfo paramSqlInfo)
    throws DbException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore_3
    //   5: aload_1
    //   6: aload_0
    //   7: getfield 41	org/xutils/db/DbManagerImpl:database	Landroid/database/sqlite/SQLiteDatabase;
    //   10: invokevirtual 338	org/xutils/db/sqlite/SqlInfo:buildStatement	(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    //   13: astore_1
    //   14: aload_1
    //   15: astore_3
    //   16: aload_1
    //   17: astore 4
    //   19: aload_1
    //   20: invokevirtual 366	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
    //   23: istore_2
    //   24: aload_1
    //   25: ifnull +19 -> 44
    //   28: aload_1
    //   29: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   32: goto +12 -> 44
    //   35: astore_1
    //   36: aload_1
    //   37: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   40: aload_1
    //   41: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   44: iload_2
    //   45: ireturn
    //   46: astore_1
    //   47: goto +26 -> 73
    //   50: astore 5
    //   52: aload 4
    //   54: astore_3
    //   55: new 82	org/xutils/ex/DbException
    //   58: astore_1
    //   59: aload 4
    //   61: astore_3
    //   62: aload_1
    //   63: aload 5
    //   65: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   68: aload 4
    //   70: astore_3
    //   71: aload_1
    //   72: athrow
    //   73: aload_3
    //   74: ifnull +19 -> 93
    //   77: aload_3
    //   78: invokevirtual 346	android/database/sqlite/SQLiteStatement:releaseReference	()V
    //   81: goto +12 -> 93
    //   84: astore_3
    //   85: aload_3
    //   86: invokevirtual 347	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   89: aload_3
    //   90: invokestatic 123	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   93: aload_1
    //   94: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	this	DbManagerImpl
    //   0	95	1	paramSqlInfo	SqlInfo
    //   23	22	2	i	int
    //   4	74	3	localSqlInfo1	SqlInfo
    //   84	6	3	localThrowable1	Throwable
    //   1	68	4	localSqlInfo2	SqlInfo
    //   50	14	5	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   28	32	35	java/lang/Throwable
    //   5	14	46	finally
    //   19	24	46	finally
    //   55	59	46	finally
    //   62	68	46	finally
    //   71	73	46	finally
    //   5	14	50	java/lang/Throwable
    //   19	24	50	java/lang/Throwable
    //   77	81	84	java/lang/Throwable
  }
  
  public <T> List<T> findAll(Class<T> paramClass)
    throws DbException
  {
    return selector(paramClass).findAll();
  }
  
  /* Error */
  public <T> T findById(Class<T> paramClass, Object paramObject)
    throws DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 274	org/xutils/db/DbManagerImpl:getTable	(Ljava/lang/Class;)Lorg/xutils/db/table/TableEntity;
    //   5: astore_3
    //   6: aload_3
    //   7: invokevirtual 277	org/xutils/db/table/TableEntity:tableIsExist	()Z
    //   10: ifne +5 -> 15
    //   13: aconst_null
    //   14: areturn
    //   15: aload_0
    //   16: aload_3
    //   17: invokestatic 384	org/xutils/db/Selector:from	(Lorg/xutils/db/table/TableEntity;)Lorg/xutils/db/Selector;
    //   20: aload_3
    //   21: invokevirtual 210	org/xutils/db/table/TableEntity:getId	()Lorg/xutils/db/table/ColumnEntity;
    //   24: invokevirtual 385	org/xutils/db/table/ColumnEntity:getName	()Ljava/lang/String;
    //   27: ldc_w 387
    //   30: aload_2
    //   31: invokevirtual 391	org/xutils/db/Selector:where	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lorg/xutils/db/Selector;
    //   34: iconst_1
    //   35: invokevirtual 395	org/xutils/db/Selector:limit	(I)Lorg/xutils/db/Selector;
    //   38: invokevirtual 396	org/xutils/db/Selector:toString	()Ljava/lang/String;
    //   41: invokevirtual 149	org/xutils/db/DbManagerImpl:execQuery	(Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_1
    //   45: aload_1
    //   46: ifnull +53 -> 99
    //   49: aload_1
    //   50: invokeinterface 156 1 0
    //   55: ifeq +15 -> 70
    //   58: aload_3
    //   59: aload_1
    //   60: invokestatic 402	org/xutils/db/CursorUtils:getEntity	(Lorg/xutils/db/table/TableEntity;Landroid/database/Cursor;)Ljava/lang/Object;
    //   63: astore_2
    //   64: aload_1
    //   65: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   68: aload_2
    //   69: areturn
    //   70: aload_1
    //   71: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   74: goto +25 -> 99
    //   77: astore_2
    //   78: goto +15 -> 93
    //   81: astore_3
    //   82: new 82	org/xutils/ex/DbException
    //   85: astore_2
    //   86: aload_2
    //   87: aload_3
    //   88: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   91: aload_2
    //   92: athrow
    //   93: aload_1
    //   94: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   97: aload_2
    //   98: athrow
    //   99: aconst_null
    //   100: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	101	0	this	DbManagerImpl
    //   0	101	1	paramClass	Class<T>
    //   0	101	2	paramObject	Object
    //   5	54	3	localTableEntity	TableEntity
    //   81	7	3	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   49	64	77	finally
    //   82	93	77	finally
    //   49	64	81	java/lang/Throwable
  }
  
  /* Error */
  public List<org.xutils.db.table.DbModel> findDbModelAll(SqlInfo paramSqlInfo)
    throws DbException
  {
    // Byte code:
    //   0: new 407	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 408	java/util/ArrayList:<init>	()V
    //   7: astore_2
    //   8: aload_0
    //   9: aload_1
    //   10: invokevirtual 410	org/xutils/db/DbManagerImpl:execQuery	(Lorg/xutils/db/sqlite/SqlInfo;)Landroid/database/Cursor;
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull +55 -> 70
    //   18: aload_1
    //   19: invokeinterface 156 1 0
    //   24: ifeq +17 -> 41
    //   27: aload_2
    //   28: aload_1
    //   29: invokestatic 414	org/xutils/db/CursorUtils:getDbModel	(Landroid/database/Cursor;)Lorg/xutils/db/table/DbModel;
    //   32: invokeinterface 417 2 0
    //   37: pop
    //   38: goto -20 -> 18
    //   41: aload_1
    //   42: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   45: goto +25 -> 70
    //   48: astore_2
    //   49: goto +15 -> 64
    //   52: astore_2
    //   53: new 82	org/xutils/ex/DbException
    //   56: astore_3
    //   57: aload_3
    //   58: aload_2
    //   59: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   62: aload_3
    //   63: athrow
    //   64: aload_1
    //   65: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   68: aload_2
    //   69: athrow
    //   70: aload_2
    //   71: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	72	0	this	DbManagerImpl
    //   0	72	1	paramSqlInfo	SqlInfo
    //   7	21	2	localArrayList	java.util.ArrayList
    //   48	1	2	localObject	Object
    //   52	19	2	localThrowable	Throwable
    //   56	7	3	localDbException	DbException
    // Exception table:
    //   from	to	target	type
    //   18	38	48	finally
    //   53	64	48	finally
    //   18	38	52	java/lang/Throwable
  }
  
  /* Error */
  public org.xutils.db.table.DbModel findDbModelFirst(SqlInfo paramSqlInfo)
    throws DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 410	org/xutils/db/DbManagerImpl:execQuery	(Lorg/xutils/db/sqlite/SqlInfo;)Landroid/database/Cursor;
    //   5: astore_1
    //   6: aload_1
    //   7: ifnull +52 -> 59
    //   10: aload_1
    //   11: invokeinterface 156 1 0
    //   16: ifeq +14 -> 30
    //   19: aload_1
    //   20: invokestatic 414	org/xutils/db/CursorUtils:getDbModel	(Landroid/database/Cursor;)Lorg/xutils/db/table/DbModel;
    //   23: astore_2
    //   24: aload_1
    //   25: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   28: aload_2
    //   29: areturn
    //   30: aload_1
    //   31: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   34: goto +25 -> 59
    //   37: astore_2
    //   38: goto +15 -> 53
    //   41: astore_3
    //   42: new 82	org/xutils/ex/DbException
    //   45: astore_2
    //   46: aload_2
    //   47: aload_3
    //   48: invokespecial 169	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   51: aload_2
    //   52: athrow
    //   53: aload_1
    //   54: invokestatic 166	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   57: aload_2
    //   58: athrow
    //   59: aconst_null
    //   60: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	61	0	this	DbManagerImpl
    //   0	61	1	paramSqlInfo	SqlInfo
    //   23	6	2	localDbModel	org.xutils.db.table.DbModel
    //   37	1	2	localObject	Object
    //   45	13	2	localDbException	DbException
    //   41	7	3	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   10	24	37	finally
    //   42	53	37	finally
    //   10	24	41	java/lang/Throwable
  }
  
  public <T> T findFirst(Class<T> paramClass)
    throws DbException
  {
    return (T)selector(paramClass).findFirst();
  }
  
  public DbManager.DaoConfig getDaoConfig()
  {
    return this.daoConfig;
  }
  
  public SQLiteDatabase getDatabase()
  {
    return this.database;
  }
  
  public void replace(Object paramObject)
    throws DbException
  {
    try
    {
      beginTransaction();
      if ((paramObject instanceof List))
      {
        localObject = (List)paramObject;
        boolean bool = ((List)localObject).isEmpty();
        if (bool) {
          return;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        createTableIfNotExist((TableEntity)paramObject);
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo((TableEntity)paramObject, ((Iterator)localObject).next()));
        }
      }
      Object localObject = getTable(paramObject.getClass());
      createTableIfNotExist((TableEntity)localObject);
      execNonQuery(SqlInfoBuilder.buildReplaceSqlInfo((TableEntity)localObject, paramObject));
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void save(Object paramObject)
    throws DbException
  {
    try
    {
      beginTransaction();
      if ((paramObject instanceof List))
      {
        localObject = (List)paramObject;
        boolean bool = ((List)localObject).isEmpty();
        if (bool) {
          return;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        createTableIfNotExist((TableEntity)paramObject);
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          execNonQuery(SqlInfoBuilder.buildInsertSqlInfo((TableEntity)paramObject, ((Iterator)localObject).next()));
        }
      }
      Object localObject = getTable(paramObject.getClass());
      createTableIfNotExist((TableEntity)localObject);
      execNonQuery(SqlInfoBuilder.buildInsertSqlInfo((TableEntity)localObject, paramObject));
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public boolean saveBindingId(Object paramObject)
    throws DbException
  {
    try
    {
      beginTransaction();
      boolean bool1 = paramObject instanceof List;
      boolean bool2 = false;
      if (bool1)
      {
        localObject = (List)paramObject;
        bool1 = ((List)localObject).isEmpty();
        if (bool1) {
          return false;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        createTableIfNotExist((TableEntity)paramObject);
        localObject = ((List)localObject).iterator();
        do
        {
          bool1 = bool2;
          if (!((Iterator)localObject).hasNext()) {
            break;
          }
        } while (saveBindingIdWithoutTransaction((TableEntity)paramObject, ((Iterator)localObject).next()));
        paramObject = new org/xutils/ex/DbException;
        ((DbException)paramObject).<init>("saveBindingId error, transaction will not commit!");
        throw ((Throwable)paramObject);
      }
      Object localObject = getTable(paramObject.getClass());
      createTableIfNotExist((TableEntity)localObject);
      bool1 = saveBindingIdWithoutTransaction((TableEntity)localObject, paramObject);
      setTransactionSuccessful();
      return bool1;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void saveOrUpdate(Object paramObject)
    throws DbException
  {
    try
    {
      beginTransaction();
      if ((paramObject instanceof List))
      {
        localObject = (List)paramObject;
        boolean bool = ((List)localObject).isEmpty();
        if (bool) {
          return;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        createTableIfNotExist((TableEntity)paramObject);
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          saveOrUpdateWithoutTransaction((TableEntity)paramObject, ((Iterator)localObject).next());
        }
      }
      Object localObject = getTable(paramObject.getClass());
      createTableIfNotExist((TableEntity)localObject);
      saveOrUpdateWithoutTransaction((TableEntity)localObject, paramObject);
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public <T> Selector<T> selector(Class<T> paramClass)
    throws DbException
  {
    return Selector.from(getTable(paramClass));
  }
  
  public int update(Class<?> paramClass, WhereBuilder paramWhereBuilder, KeyValue... paramVarArgs)
    throws DbException
  {
    paramClass = getTable(paramClass);
    if (!paramClass.tableIsExist()) {
      return 0;
    }
    try
    {
      beginTransaction();
      int i = executeUpdateDelete(SqlInfoBuilder.buildUpdateSqlInfo(paramClass, paramWhereBuilder, paramVarArgs));
      setTransactionSuccessful();
      return i;
    }
    finally
    {
      endTransaction();
    }
  }
  
  public void update(Object paramObject, String... paramVarArgs)
    throws DbException
  {
    try
    {
      beginTransaction();
      if ((paramObject instanceof List))
      {
        localObject = (List)paramObject;
        bool = ((List)localObject).isEmpty();
        if (bool) {
          return;
        }
        paramObject = getTable(((List)localObject).get(0).getClass());
        bool = ((TableEntity)paramObject).tableIsExist();
        if (!bool) {
          return;
        }
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo((TableEntity)paramObject, ((Iterator)localObject).next(), paramVarArgs));
        }
      }
      Object localObject = getTable(paramObject.getClass());
      boolean bool = ((TableEntity)localObject).tableIsExist();
      if (!bool) {
        return;
      }
      execNonQuery(SqlInfoBuilder.buildUpdateSqlInfo((TableEntity)localObject, paramObject, paramVarArgs));
      setTransactionSuccessful();
      return;
    }
    finally
    {
      endTransaction();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/DbManagerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */