package org.xutils.db;

import android.text.TextUtils;
import java.util.List;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.db.table.TableEntity;

public final class DbModelSelector
{
  private String[] columnExpressions;
  private String groupByColumnName;
  private WhereBuilder having;
  private Selector<?> selector;
  
  protected DbModelSelector(Selector<?> paramSelector, String paramString)
  {
    this.selector = paramSelector;
    this.groupByColumnName = paramString;
  }
  
  protected DbModelSelector(Selector<?> paramSelector, String[] paramArrayOfString)
  {
    this.selector = paramSelector;
    this.columnExpressions = paramArrayOfString;
  }
  
  private DbModelSelector(TableEntity<?> paramTableEntity)
  {
    this.selector = Selector.from(paramTableEntity);
  }
  
  static DbModelSelector from(TableEntity<?> paramTableEntity)
  {
    return new DbModelSelector(paramTableEntity);
  }
  
  public DbModelSelector and(String paramString1, String paramString2, Object paramObject)
  {
    this.selector.and(paramString1, paramString2, paramObject);
    return this;
  }
  
  public DbModelSelector and(WhereBuilder paramWhereBuilder)
  {
    this.selector.and(paramWhereBuilder);
    return this;
  }
  
  public DbModelSelector expr(String paramString)
  {
    this.selector.expr(paramString);
    return this;
  }
  
  /* Error */
  public List<org.xutils.db.table.DbModel> findAll()
    throws org.xutils.ex.DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 20	org/xutils/db/DbModelSelector:selector	Lorg/xutils/db/Selector;
    //   4: invokevirtual 65	org/xutils/db/Selector:getTable	()Lorg/xutils/db/table/TableEntity;
    //   7: astore_3
    //   8: aload_3
    //   9: invokevirtual 71	org/xutils/db/table/TableEntity:tableIsExist	()Z
    //   12: istore_1
    //   13: aconst_null
    //   14: astore_2
    //   15: iload_1
    //   16: ifne +5 -> 21
    //   19: aconst_null
    //   20: areturn
    //   21: aload_3
    //   22: invokevirtual 75	org/xutils/db/table/TableEntity:getDb	()Lorg/xutils/DbManager;
    //   25: aload_0
    //   26: invokevirtual 79	org/xutils/db/DbModelSelector:toString	()Ljava/lang/String;
    //   29: invokeinterface 85 2 0
    //   34: astore_3
    //   35: aload_3
    //   36: ifnull +65 -> 101
    //   39: new 87	java/util/ArrayList
    //   42: astore_2
    //   43: aload_2
    //   44: invokespecial 88	java/util/ArrayList:<init>	()V
    //   47: aload_3
    //   48: invokeinterface 93 1 0
    //   53: ifeq +17 -> 70
    //   56: aload_2
    //   57: aload_3
    //   58: invokestatic 99	org/xutils/db/CursorUtils:getDbModel	(Landroid/database/Cursor;)Lorg/xutils/db/table/DbModel;
    //   61: invokeinterface 105 2 0
    //   66: pop
    //   67: goto -20 -> 47
    //   70: aload_3
    //   71: invokestatic 111	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   74: goto +27 -> 101
    //   77: astore_2
    //   78: goto +17 -> 95
    //   81: astore 4
    //   83: new 59	org/xutils/ex/DbException
    //   86: astore_2
    //   87: aload_2
    //   88: aload 4
    //   90: invokespecial 114	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   93: aload_2
    //   94: athrow
    //   95: aload_3
    //   96: invokestatic 111	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   99: aload_2
    //   100: athrow
    //   101: aload_2
    //   102: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	this	DbModelSelector
    //   12	4	1	bool	boolean
    //   14	43	2	localArrayList	java.util.ArrayList
    //   77	1	2	localObject1	Object
    //   86	16	2	localDbException	org.xutils.ex.DbException
    //   7	89	3	localObject2	Object
    //   81	8	4	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   39	47	77	finally
    //   47	67	77	finally
    //   83	95	77	finally
    //   39	47	81	java/lang/Throwable
    //   47	67	81	java/lang/Throwable
  }
  
  /* Error */
  public org.xutils.db.table.DbModel findFirst()
    throws org.xutils.ex.DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 20	org/xutils/db/DbModelSelector:selector	Lorg/xutils/db/Selector;
    //   4: invokevirtual 65	org/xutils/db/Selector:getTable	()Lorg/xutils/db/table/TableEntity;
    //   7: astore_1
    //   8: aload_1
    //   9: invokevirtual 71	org/xutils/db/table/TableEntity:tableIsExist	()Z
    //   12: ifne +5 -> 17
    //   15: aconst_null
    //   16: areturn
    //   17: aload_0
    //   18: iconst_1
    //   19: invokevirtual 122	org/xutils/db/DbModelSelector:limit	(I)Lorg/xutils/db/DbModelSelector;
    //   22: pop
    //   23: aload_1
    //   24: invokevirtual 75	org/xutils/db/table/TableEntity:getDb	()Lorg/xutils/DbManager;
    //   27: aload_0
    //   28: invokevirtual 79	org/xutils/db/DbModelSelector:toString	()Ljava/lang/String;
    //   31: invokeinterface 85 2 0
    //   36: astore_1
    //   37: aload_1
    //   38: ifnull +52 -> 90
    //   41: aload_1
    //   42: invokeinterface 93 1 0
    //   47: ifeq +14 -> 61
    //   50: aload_1
    //   51: invokestatic 99	org/xutils/db/CursorUtils:getDbModel	(Landroid/database/Cursor;)Lorg/xutils/db/table/DbModel;
    //   54: astore_2
    //   55: aload_1
    //   56: invokestatic 111	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   59: aload_2
    //   60: areturn
    //   61: aload_1
    //   62: invokestatic 111	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   65: goto +25 -> 90
    //   68: astore_2
    //   69: goto +15 -> 84
    //   72: astore_3
    //   73: new 59	org/xutils/ex/DbException
    //   76: astore_2
    //   77: aload_2
    //   78: aload_3
    //   79: invokespecial 114	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   82: aload_2
    //   83: athrow
    //   84: aload_1
    //   85: invokestatic 111	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   88: aload_2
    //   89: athrow
    //   90: aconst_null
    //   91: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	92	0	this	DbModelSelector
    //   7	78	1	localObject1	Object
    //   54	6	2	localDbModel	org.xutils.db.table.DbModel
    //   68	1	2	localObject2	Object
    //   76	13	2	localDbException	org.xutils.ex.DbException
    //   72	7	3	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   41	55	68	finally
    //   73	84	68	finally
    //   41	55	72	java/lang/Throwable
  }
  
  public TableEntity<?> getTable()
  {
    return this.selector.getTable();
  }
  
  public DbModelSelector groupBy(String paramString)
  {
    this.groupByColumnName = paramString;
    return this;
  }
  
  public DbModelSelector having(WhereBuilder paramWhereBuilder)
  {
    this.having = paramWhereBuilder;
    return this;
  }
  
  public DbModelSelector limit(int paramInt)
  {
    this.selector.limit(paramInt);
    return this;
  }
  
  public DbModelSelector offset(int paramInt)
  {
    this.selector.offset(paramInt);
    return this;
  }
  
  public DbModelSelector or(String paramString1, String paramString2, Object paramObject)
  {
    this.selector.or(paramString1, paramString2, paramObject);
    return this;
  }
  
  public DbModelSelector or(WhereBuilder paramWhereBuilder)
  {
    this.selector.or(paramWhereBuilder);
    return this;
  }
  
  public DbModelSelector orderBy(String paramString)
  {
    this.selector.orderBy(paramString);
    return this;
  }
  
  public DbModelSelector orderBy(String paramString, boolean paramBoolean)
  {
    this.selector.orderBy(paramString, paramBoolean);
    return this;
  }
  
  public DbModelSelector select(String... paramVarArgs)
  {
    this.columnExpressions = paramVarArgs;
    return this;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("SELECT ");
    Object localObject = this.columnExpressions;
    int j = 0;
    int i;
    if ((localObject != null) && (localObject.length > 0))
    {
      int k = localObject.length;
      for (i = 0; i < k; i++)
      {
        localStringBuilder.append(localObject[i]);
        localStringBuilder.append(",");
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    else if (!TextUtils.isEmpty(this.groupByColumnName))
    {
      localStringBuilder.append(this.groupByColumnName);
    }
    else
    {
      localStringBuilder.append("*");
    }
    localStringBuilder.append(" FROM ");
    localStringBuilder.append("\"");
    localStringBuilder.append(this.selector.getTable().getName());
    localStringBuilder.append("\"");
    localObject = this.selector.getWhereBuilder();
    if ((localObject != null) && (((WhereBuilder)localObject).getWhereItemSize() > 0))
    {
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(((WhereBuilder)localObject).toString());
    }
    if (!TextUtils.isEmpty(this.groupByColumnName))
    {
      localStringBuilder.append(" GROUP BY ");
      localStringBuilder.append("\"");
      localStringBuilder.append(this.groupByColumnName);
      localStringBuilder.append("\"");
      localObject = this.having;
      if ((localObject != null) && (((WhereBuilder)localObject).getWhereItemSize() > 0))
      {
        localStringBuilder.append(" HAVING ");
        localStringBuilder.append(this.having.toString());
      }
    }
    localObject = this.selector.getOrderByList();
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      for (i = j; i < ((List)localObject).size(); i++)
      {
        localStringBuilder.append(" ORDER BY ");
        localStringBuilder.append(((Selector.OrderBy)((List)localObject).get(i)).toString());
        localStringBuilder.append(',');
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    if (this.selector.getLimit() > 0)
    {
      localStringBuilder.append(" LIMIT ");
      localStringBuilder.append(this.selector.getLimit());
      localStringBuilder.append(" OFFSET ");
      localStringBuilder.append(this.selector.getOffset());
    }
    return localStringBuilder.toString();
  }
  
  public DbModelSelector where(String paramString1, String paramString2, Object paramObject)
  {
    this.selector.where(paramString1, paramString2, paramObject);
    return this;
  }
  
  public DbModelSelector where(WhereBuilder paramWhereBuilder)
  {
    this.selector.where(paramWhereBuilder);
    return this;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/DbModelSelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */