package org.xutils.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.db.table.ColumnEntity;
import org.xutils.db.table.DbModel;
import org.xutils.db.table.TableEntity;
import org.xutils.ex.DbException;

public final class Selector<T>
{
  private int limit = 0;
  private int offset = 0;
  private List<OrderBy> orderByList;
  private final TableEntity<T> table;
  private WhereBuilder whereBuilder;
  
  private Selector(TableEntity<T> paramTableEntity)
  {
    this.table = paramTableEntity;
  }
  
  static <T> Selector<T> from(TableEntity<T> paramTableEntity)
  {
    return new Selector(paramTableEntity);
  }
  
  public Selector<T> and(String paramString1, String paramString2, Object paramObject)
  {
    this.whereBuilder.and(paramString1, paramString2, paramObject);
    return this;
  }
  
  public Selector<T> and(WhereBuilder paramWhereBuilder)
  {
    this.whereBuilder.and(paramWhereBuilder);
    return this;
  }
  
  public long count()
    throws DbException
  {
    if (!this.table.tableIsExist()) {
      return 0L;
    }
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append("count(\"");
    ((StringBuilder)localObject).append(this.table.getId().getName());
    ((StringBuilder)localObject).append("\") as count");
    localObject = select(new String[] { ((StringBuilder)localObject).toString() }).findFirst();
    if (localObject != null) {
      return ((DbModel)localObject).getLong("count");
    }
    return 0L;
  }
  
  public Selector<T> expr(String paramString)
  {
    if (this.whereBuilder == null) {
      this.whereBuilder = WhereBuilder.b();
    }
    this.whereBuilder.expr(paramString);
    return this;
  }
  
  /* Error */
  public List<T> findAll()
    throws DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   4: invokevirtual 63	org/xutils/db/table/TableEntity:tableIsExist	()Z
    //   7: istore_1
    //   8: aconst_null
    //   9: astore_2
    //   10: iload_1
    //   11: ifne +5 -> 16
    //   14: aconst_null
    //   15: areturn
    //   16: aload_0
    //   17: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   20: invokevirtual 125	org/xutils/db/table/TableEntity:getDb	()Lorg/xutils/DbManager;
    //   23: aload_0
    //   24: invokevirtual 126	org/xutils/db/Selector:toString	()Ljava/lang/String;
    //   27: invokeinterface 132 2 0
    //   32: astore_3
    //   33: aload_3
    //   34: ifnull +70 -> 104
    //   37: new 134	java/util/ArrayList
    //   40: astore_2
    //   41: aload_2
    //   42: invokespecial 135	java/util/ArrayList:<init>	()V
    //   45: aload_3
    //   46: invokeinterface 140 1 0
    //   51: ifeq +21 -> 72
    //   54: aload_2
    //   55: aload_0
    //   56: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   59: aload_3
    //   60: invokestatic 146	org/xutils/db/CursorUtils:getEntity	(Lorg/xutils/db/table/TableEntity;Landroid/database/Cursor;)Ljava/lang/Object;
    //   63: invokeinterface 152 2 0
    //   68: pop
    //   69: goto -24 -> 45
    //   72: aload_3
    //   73: invokestatic 158	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   76: goto +28 -> 104
    //   79: astore_2
    //   80: goto +18 -> 98
    //   83: astore_2
    //   84: new 57	org/xutils/ex/DbException
    //   87: astore 4
    //   89: aload 4
    //   91: aload_2
    //   92: invokespecial 161	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   95: aload 4
    //   97: athrow
    //   98: aload_3
    //   99: invokestatic 158	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   102: aload_2
    //   103: athrow
    //   104: aload_2
    //   105: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	this	Selector
    //   7	4	1	bool	boolean
    //   9	46	2	localArrayList	ArrayList
    //   79	1	2	localObject	Object
    //   83	22	2	localThrowable	Throwable
    //   32	67	3	localCursor	android.database.Cursor
    //   87	9	4	localDbException	DbException
    // Exception table:
    //   from	to	target	type
    //   37	45	79	finally
    //   45	69	79	finally
    //   84	98	79	finally
    //   37	45	83	java/lang/Throwable
    //   45	69	83	java/lang/Throwable
  }
  
  /* Error */
  public T findFirst()
    throws DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   4: invokevirtual 63	org/xutils/db/table/TableEntity:tableIsExist	()Z
    //   7: ifne +5 -> 12
    //   10: aconst_null
    //   11: areturn
    //   12: aload_0
    //   13: iconst_1
    //   14: invokevirtual 166	org/xutils/db/Selector:limit	(I)Lorg/xutils/db/Selector;
    //   17: pop
    //   18: aload_0
    //   19: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   22: invokevirtual 125	org/xutils/db/table/TableEntity:getDb	()Lorg/xutils/DbManager;
    //   25: aload_0
    //   26: invokevirtual 126	org/xutils/db/Selector:toString	()Ljava/lang/String;
    //   29: invokeinterface 132 2 0
    //   34: astore_1
    //   35: aload_1
    //   36: ifnull +56 -> 92
    //   39: aload_1
    //   40: invokeinterface 140 1 0
    //   45: ifeq +18 -> 63
    //   48: aload_0
    //   49: getfield 30	org/xutils/db/Selector:table	Lorg/xutils/db/table/TableEntity;
    //   52: aload_1
    //   53: invokestatic 146	org/xutils/db/CursorUtils:getEntity	(Lorg/xutils/db/table/TableEntity;Landroid/database/Cursor;)Ljava/lang/Object;
    //   56: astore_2
    //   57: aload_1
    //   58: invokestatic 158	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   61: aload_2
    //   62: areturn
    //   63: aload_1
    //   64: invokestatic 158	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   67: goto +25 -> 92
    //   70: astore_2
    //   71: goto +15 -> 86
    //   74: astore_2
    //   75: new 57	org/xutils/ex/DbException
    //   78: astore_3
    //   79: aload_3
    //   80: aload_2
    //   81: invokespecial 161	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   84: aload_3
    //   85: athrow
    //   86: aload_1
    //   87: invokestatic 158	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   90: aload_2
    //   91: athrow
    //   92: aconst_null
    //   93: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	94	0	this	Selector
    //   34	53	1	localCursor	android.database.Cursor
    //   56	6	2	localObject1	Object
    //   70	1	2	localObject2	Object
    //   74	17	2	localThrowable	Throwable
    //   78	7	3	localDbException	DbException
    // Exception table:
    //   from	to	target	type
    //   39	57	70	finally
    //   75	86	70	finally
    //   39	57	74	java/lang/Throwable
  }
  
  public int getLimit()
  {
    return this.limit;
  }
  
  public int getOffset()
  {
    return this.offset;
  }
  
  public List<OrderBy> getOrderByList()
  {
    return this.orderByList;
  }
  
  public TableEntity<T> getTable()
  {
    return this.table;
  }
  
  public WhereBuilder getWhereBuilder()
  {
    return this.whereBuilder;
  }
  
  public DbModelSelector groupBy(String paramString)
  {
    return new DbModelSelector(this, paramString);
  }
  
  public Selector<T> limit(int paramInt)
  {
    this.limit = paramInt;
    return this;
  }
  
  public Selector<T> offset(int paramInt)
  {
    this.offset = paramInt;
    return this;
  }
  
  public Selector<T> or(String paramString1, String paramString2, Object paramObject)
  {
    this.whereBuilder.or(paramString1, paramString2, paramObject);
    return this;
  }
  
  public Selector or(WhereBuilder paramWhereBuilder)
  {
    this.whereBuilder.or(paramWhereBuilder);
    return this;
  }
  
  public Selector<T> orderBy(String paramString)
  {
    if (this.orderByList == null) {
      this.orderByList = new ArrayList(5);
    }
    this.orderByList.add(new OrderBy(paramString));
    return this;
  }
  
  public Selector<T> orderBy(String paramString, boolean paramBoolean)
  {
    if (this.orderByList == null) {
      this.orderByList = new ArrayList(5);
    }
    this.orderByList.add(new OrderBy(paramString, paramBoolean));
    return this;
  }
  
  public DbModelSelector select(String... paramVarArgs)
  {
    return new DbModelSelector(this, paramVarArgs);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("SELECT ");
    localStringBuilder.append("*");
    localStringBuilder.append(" FROM ");
    localStringBuilder.append("\"");
    localStringBuilder.append(this.table.getName());
    localStringBuilder.append("\"");
    Object localObject = this.whereBuilder;
    if ((localObject != null) && (((WhereBuilder)localObject).getWhereItemSize() > 0))
    {
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(this.whereBuilder.toString());
    }
    localObject = this.orderByList;
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      localStringBuilder.append(" ORDER BY ");
      localObject = this.orderByList.iterator();
      while (((Iterator)localObject).hasNext())
      {
        localStringBuilder.append(((OrderBy)((Iterator)localObject).next()).toString());
        localStringBuilder.append(',');
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    }
    if (this.limit > 0)
    {
      localStringBuilder.append(" LIMIT ");
      localStringBuilder.append(this.limit);
      localStringBuilder.append(" OFFSET ");
      localStringBuilder.append(this.offset);
    }
    return localStringBuilder.toString();
  }
  
  public Selector<T> where(String paramString1, String paramString2, Object paramObject)
  {
    this.whereBuilder = WhereBuilder.b(paramString1, paramString2, paramObject);
    return this;
  }
  
  public Selector<T> where(WhereBuilder paramWhereBuilder)
  {
    this.whereBuilder = paramWhereBuilder;
    return this;
  }
  
  public static class OrderBy
  {
    private String columnName;
    private boolean desc;
    
    public OrderBy(String paramString)
    {
      this.columnName = paramString;
    }
    
    public OrderBy(String paramString, boolean paramBoolean)
    {
      this.columnName = paramString;
      this.desc = paramBoolean;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("\"");
      localStringBuilder.append(this.columnName);
      localStringBuilder.append("\"");
      String str;
      if (this.desc) {
        str = " DESC";
      } else {
        str = " ASC";
      }
      localStringBuilder.append(str);
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/Selector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */