package org.xutils.db.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import java.util.ArrayList;
import java.util.List;
import org.xutils.common.util.KeyValue;
import org.xutils.db.converter.ColumnConverter;
import org.xutils.db.converter.ColumnConverterFactory;
import org.xutils.db.table.ColumnUtils;

public final class SqlInfo
{
  private List<KeyValue> bindArgs;
  private String sql;
  
  public SqlInfo() {}
  
  public SqlInfo(String paramString)
  {
    this.sql = paramString;
  }
  
  public void addBindArg(KeyValue paramKeyValue)
  {
    if (this.bindArgs == null) {
      this.bindArgs = new ArrayList();
    }
    this.bindArgs.add(paramKeyValue);
  }
  
  public void addBindArgs(List<KeyValue> paramList)
  {
    List localList = this.bindArgs;
    if (localList == null) {
      this.bindArgs = paramList;
    } else {
      localList.addAll(paramList);
    }
  }
  
  public SQLiteStatement buildStatement(SQLiteDatabase paramSQLiteDatabase)
  {
    SQLiteStatement localSQLiteStatement = paramSQLiteDatabase.compileStatement(this.sql);
    if (this.bindArgs != null) {
      for (int i = 1; i < this.bindArgs.size() + 1; i++)
      {
        paramSQLiteDatabase = ColumnUtils.convert2DbValueIfNeeded(((KeyValue)this.bindArgs.get(i - 1)).value);
        if (paramSQLiteDatabase == null)
        {
          localSQLiteStatement.bindNull(i);
        }
        else
        {
          ColumnDbType localColumnDbType = ColumnConverterFactory.getColumnConverter(paramSQLiteDatabase.getClass()).getColumnDbType();
          switch (localColumnDbType)
          {
          default: 
            localSQLiteStatement.bindNull(i);
            break;
          case ???: 
            localSQLiteStatement.bindBlob(i, (byte[])paramSQLiteDatabase);
            break;
          case ???: 
            localSQLiteStatement.bindString(i, paramSQLiteDatabase.toString());
            break;
          case ???: 
            localSQLiteStatement.bindDouble(i, ((Number)paramSQLiteDatabase).doubleValue());
            break;
          case ???: 
            localSQLiteStatement.bindLong(i, ((Number)paramSQLiteDatabase).longValue());
          }
        }
      }
    }
    return localSQLiteStatement;
  }
  
  public Object[] getBindArgs()
  {
    Object localObject = this.bindArgs;
    if (localObject != null)
    {
      Object[] arrayOfObject = new Object[((List)localObject).size()];
      for (int i = 0;; i++)
      {
        localObject = arrayOfObject;
        if (i >= this.bindArgs.size()) {
          break;
        }
        arrayOfObject[i] = ColumnUtils.convert2DbValueIfNeeded(((KeyValue)this.bindArgs.get(i)).value);
      }
    }
    localObject = null;
    return (Object[])localObject;
  }
  
  public String[] getBindArgsAsStrArray()
  {
    Object localObject = this.bindArgs;
    if (localObject != null)
    {
      String[] arrayOfString = new String[((List)localObject).size()];
      for (int i = 0;; i++)
      {
        localObject = arrayOfString;
        if (i >= this.bindArgs.size()) {
          break;
        }
        localObject = ColumnUtils.convert2DbValueIfNeeded(((KeyValue)this.bindArgs.get(i)).value);
        if (localObject == null) {
          localObject = null;
        } else {
          localObject = localObject.toString();
        }
        arrayOfString[i] = localObject;
      }
    }
    localObject = null;
    return (String[])localObject;
  }
  
  public String getSql()
  {
    return this.sql;
  }
  
  public void setSql(String paramString)
  {
    this.sql = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/sqlite/SqlInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */