package org.xutils.db.sqlite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.xutils.common.util.KeyValue;
import org.xutils.db.table.ColumnEntity;
import org.xutils.db.table.TableEntity;
import org.xutils.ex.DbException;

public final class SqlInfoBuilder
{
  private static final ConcurrentHashMap<TableEntity<?>, String> INSERT_SQL_CACHE = new ConcurrentHashMap();
  private static final ConcurrentHashMap<TableEntity<?>, String> REPLACE_SQL_CACHE = new ConcurrentHashMap();
  
  public static SqlInfo buildCreateTableSqlInfo(TableEntity<?> paramTableEntity)
    throws DbException
  {
    ColumnEntity localColumnEntity = paramTableEntity.getId();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("CREATE TABLE IF NOT EXISTS ");
    localStringBuilder.append("\"");
    localStringBuilder.append(paramTableEntity.getName());
    localStringBuilder.append("\"");
    localStringBuilder.append(" ( ");
    if (localColumnEntity.isAutoId())
    {
      localStringBuilder.append("\"");
      localStringBuilder.append(localColumnEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" INTEGER PRIMARY KEY AUTOINCREMENT, ");
    }
    else
    {
      localStringBuilder.append("\"");
      localStringBuilder.append(localColumnEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(localColumnEntity.getColumnDbType());
      localStringBuilder.append(" PRIMARY KEY, ");
    }
    paramTableEntity = paramTableEntity.getColumnMap().values().iterator();
    while (paramTableEntity.hasNext())
    {
      localColumnEntity = (ColumnEntity)paramTableEntity.next();
      if (!localColumnEntity.isId())
      {
        localStringBuilder.append("\"");
        localStringBuilder.append(localColumnEntity.getName());
        localStringBuilder.append("\"");
        localStringBuilder.append(' ');
        localStringBuilder.append(localColumnEntity.getColumnDbType());
        localStringBuilder.append(' ');
        localStringBuilder.append(localColumnEntity.getProperty());
        localStringBuilder.append(',');
      }
    }
    localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
    localStringBuilder.append(" )");
    return new SqlInfo(localStringBuilder.toString());
  }
  
  public static SqlInfo buildDeleteSqlInfo(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    SqlInfo localSqlInfo = new SqlInfo();
    ColumnEntity localColumnEntity = paramTableEntity.getId();
    paramObject = localColumnEntity.getColumnValue(paramObject);
    if (paramObject != null)
    {
      StringBuilder localStringBuilder = new StringBuilder("DELETE FROM ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramTableEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(WhereBuilder.b(localColumnEntity.getName(), "=", paramObject));
      localSqlInfo.setSql(localStringBuilder.toString());
      return localSqlInfo;
    }
    paramObject = new StringBuilder();
    ((StringBuilder)paramObject).append("this entity[");
    ((StringBuilder)paramObject).append(paramTableEntity.getEntityType());
    ((StringBuilder)paramObject).append("]'s id value is null");
    throw new DbException(((StringBuilder)paramObject).toString());
  }
  
  public static SqlInfo buildDeleteSqlInfo(TableEntity<?> paramTableEntity, WhereBuilder paramWhereBuilder)
    throws DbException
  {
    StringBuilder localStringBuilder = new StringBuilder("DELETE FROM ");
    localStringBuilder.append("\"");
    localStringBuilder.append(paramTableEntity.getName());
    localStringBuilder.append("\"");
    if ((paramWhereBuilder != null) && (paramWhereBuilder.getWhereItemSize() > 0))
    {
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(paramWhereBuilder.toString());
    }
    return new SqlInfo(localStringBuilder.toString());
  }
  
  public static SqlInfo buildDeleteSqlInfoById(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    SqlInfo localSqlInfo = new SqlInfo();
    ColumnEntity localColumnEntity = paramTableEntity.getId();
    if (paramObject != null)
    {
      StringBuilder localStringBuilder = new StringBuilder("DELETE FROM ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramTableEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(WhereBuilder.b(localColumnEntity.getName(), "=", paramObject));
      localSqlInfo.setSql(localStringBuilder.toString());
      return localSqlInfo;
    }
    paramObject = new StringBuilder();
    ((StringBuilder)paramObject).append("this entity[");
    ((StringBuilder)paramObject).append(paramTableEntity.getEntityType());
    ((StringBuilder)paramObject).append("]'s id value is null");
    throw new DbException(((StringBuilder)paramObject).toString());
  }
  
  public static SqlInfo buildInsertSqlInfo(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    paramObject = entity2KeyValueList(paramTableEntity, paramObject);
    if (((List)paramObject).size() == 0) {
      return null;
    }
    SqlInfo localSqlInfo = new SqlInfo();
    Object localObject = (String)INSERT_SQL_CACHE.get(paramTableEntity);
    if (localObject == null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("INSERT INTO ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramTableEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" (");
      localObject = ((List)paramObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        KeyValue localKeyValue = (KeyValue)((Iterator)localObject).next();
        localStringBuilder.append("\"");
        localStringBuilder.append(localKeyValue.key);
        localStringBuilder.append("\"");
        localStringBuilder.append(',');
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      localStringBuilder.append(") VALUES (");
      int j = ((List)paramObject).size();
      for (int i = 0; i < j; i++) {
        localStringBuilder.append("?,");
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      localStringBuilder.append(")");
      localObject = localStringBuilder.toString();
      localSqlInfo.setSql((String)localObject);
      localSqlInfo.addBindArgs((List)paramObject);
      INSERT_SQL_CACHE.put(paramTableEntity, localObject);
    }
    else
    {
      localSqlInfo.setSql((String)localObject);
      localSqlInfo.addBindArgs((List)paramObject);
    }
    return localSqlInfo;
  }
  
  public static SqlInfo buildReplaceSqlInfo(TableEntity<?> paramTableEntity, Object paramObject)
    throws DbException
  {
    List localList = entity2KeyValueList(paramTableEntity, paramObject);
    if (localList.size() == 0) {
      return null;
    }
    paramObject = new SqlInfo();
    Object localObject = (String)REPLACE_SQL_CACHE.get(paramTableEntity);
    if (localObject == null)
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("REPLACE INTO ");
      ((StringBuilder)localObject).append("\"");
      ((StringBuilder)localObject).append(paramTableEntity.getName());
      ((StringBuilder)localObject).append("\"");
      ((StringBuilder)localObject).append(" (");
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        KeyValue localKeyValue = (KeyValue)localIterator.next();
        ((StringBuilder)localObject).append("\"");
        ((StringBuilder)localObject).append(localKeyValue.key);
        ((StringBuilder)localObject).append("\"");
        ((StringBuilder)localObject).append(',');
      }
      ((StringBuilder)localObject).deleteCharAt(((StringBuilder)localObject).length() - 1);
      ((StringBuilder)localObject).append(") VALUES (");
      int j = localList.size();
      for (int i = 0; i < j; i++) {
        ((StringBuilder)localObject).append("?,");
      }
      ((StringBuilder)localObject).deleteCharAt(((StringBuilder)localObject).length() - 1);
      ((StringBuilder)localObject).append(")");
      localObject = ((StringBuilder)localObject).toString();
      ((SqlInfo)paramObject).setSql((String)localObject);
      ((SqlInfo)paramObject).addBindArgs(localList);
      REPLACE_SQL_CACHE.put(paramTableEntity, localObject);
    }
    else
    {
      ((SqlInfo)paramObject).setSql((String)localObject);
      ((SqlInfo)paramObject).addBindArgs(localList);
    }
    return (SqlInfo)paramObject;
  }
  
  public static SqlInfo buildUpdateSqlInfo(TableEntity<?> paramTableEntity, Object paramObject, String... paramVarArgs)
    throws DbException
  {
    Object localObject2 = entity2KeyValueList(paramTableEntity, paramObject);
    int i = ((List)localObject2).size();
    StringBuilder localStringBuilder = null;
    if (i == 0) {
      return null;
    }
    Object localObject1 = localStringBuilder;
    if (paramVarArgs != null)
    {
      localObject1 = localStringBuilder;
      if (paramVarArgs.length > 0)
      {
        localObject1 = new HashSet(paramVarArgs.length);
        Collections.addAll((Collection)localObject1, paramVarArgs);
      }
    }
    paramVarArgs = paramTableEntity.getId();
    paramObject = paramVarArgs.getColumnValue(paramObject);
    if (paramObject != null)
    {
      SqlInfo localSqlInfo = new SqlInfo();
      localStringBuilder = new StringBuilder("UPDATE ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramTableEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" SET ");
      paramTableEntity = ((List)localObject2).iterator();
      while (paramTableEntity.hasNext())
      {
        localObject2 = (KeyValue)paramTableEntity.next();
        if ((localObject1 == null) || (((HashSet)localObject1).contains(((KeyValue)localObject2).key)))
        {
          localStringBuilder.append("\"");
          localStringBuilder.append(((KeyValue)localObject2).key);
          localStringBuilder.append("\"");
          localStringBuilder.append("=?,");
          localSqlInfo.addBindArg((KeyValue)localObject2);
        }
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      localStringBuilder.append(" WHERE ");
      localStringBuilder.append(WhereBuilder.b(paramVarArgs.getName(), "=", paramObject));
      localSqlInfo.setSql(localStringBuilder.toString());
      return localSqlInfo;
    }
    paramObject = new StringBuilder();
    ((StringBuilder)paramObject).append("this entity[");
    ((StringBuilder)paramObject).append(paramTableEntity.getEntityType());
    ((StringBuilder)paramObject).append("]'s id value is null");
    throw new DbException(((StringBuilder)paramObject).toString());
  }
  
  public static SqlInfo buildUpdateSqlInfo(TableEntity<?> paramTableEntity, WhereBuilder paramWhereBuilder, KeyValue... paramVarArgs)
    throws DbException
  {
    if ((paramVarArgs != null) && (paramVarArgs.length != 0))
    {
      SqlInfo localSqlInfo = new SqlInfo();
      StringBuilder localStringBuilder = new StringBuilder("UPDATE ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramTableEntity.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" SET ");
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++)
      {
        paramTableEntity = paramVarArgs[i];
        localStringBuilder.append("\"");
        localStringBuilder.append(paramTableEntity.key);
        localStringBuilder.append("\"");
        localStringBuilder.append("=?,");
        localSqlInfo.addBindArg(paramTableEntity);
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1);
      if ((paramWhereBuilder != null) && (paramWhereBuilder.getWhereItemSize() > 0))
      {
        localStringBuilder.append(" WHERE ");
        localStringBuilder.append(paramWhereBuilder.toString());
      }
      localSqlInfo.setSql(localStringBuilder.toString());
      return localSqlInfo;
    }
    return null;
  }
  
  private static KeyValue column2KeyValue(Object paramObject, ColumnEntity paramColumnEntity)
  {
    if (paramColumnEntity.isAutoId()) {
      return null;
    }
    return new KeyValue(paramColumnEntity.getName(), paramColumnEntity.getFieldValue(paramObject));
  }
  
  public static List<KeyValue> entity2KeyValueList(TableEntity<?> paramTableEntity, Object paramObject)
  {
    Object localObject = paramTableEntity.getColumnMap().values();
    paramTableEntity = new ArrayList(((Collection)localObject).size());
    localObject = ((Collection)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      KeyValue localKeyValue = column2KeyValue(paramObject, (ColumnEntity)((Iterator)localObject).next());
      if (localKeyValue != null) {
        paramTableEntity.add(localKeyValue);
      }
    }
    return paramTableEntity;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/sqlite/SqlInfoBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */