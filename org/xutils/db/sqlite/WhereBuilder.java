package org.xutils.db.sqlite;

import android.text.TextUtils;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xutils.db.converter.ColumnConverterFactory;
import org.xutils.db.table.ColumnUtils;

public class WhereBuilder
{
  private final List<String> whereItems = new ArrayList();
  
  private void appendCondition(String paramString1, String paramString2, String paramString3, Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.whereItems.size() > 0) {
      localStringBuilder.append(" ");
    }
    if (!TextUtils.isEmpty(paramString1))
    {
      localStringBuilder.append(paramString1);
      localStringBuilder.append(" ");
    }
    localStringBuilder.append("\"");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("\"");
    if ("!=".equals(paramString3))
    {
      paramString1 = "<>";
    }
    else
    {
      paramString1 = paramString3;
      if ("==".equals(paramString3)) {
        paramString1 = "=";
      }
    }
    if (paramObject == null)
    {
      if ("=".equals(paramString1))
      {
        localStringBuilder.append(" IS NULL");
      }
      else if ("<>".equals(paramString1))
      {
        localStringBuilder.append(" IS NOT NULL");
      }
      else
      {
        localStringBuilder.append(" ");
        localStringBuilder.append(paramString1);
        localStringBuilder.append(" NULL");
      }
    }
    else
    {
      localStringBuilder.append(" ");
      localStringBuilder.append(paramString1);
      localStringBuilder.append(" ");
      boolean bool = "IN".equalsIgnoreCase(paramString1);
      int j = 0;
      int i = 0;
      paramString3 = null;
      paramString2 = null;
      if (bool)
      {
        if ((paramObject instanceof Iterable))
        {
          paramString1 = (Iterable)paramObject;
        }
        else
        {
          paramString1 = paramString2;
          if (paramObject.getClass().isArray())
          {
            j = Array.getLength(paramObject);
            paramString2 = new ArrayList(j);
            for (;;)
            {
              paramString1 = paramString2;
              if (i >= j) {
                break;
              }
              paramString2.add(Array.get(paramObject, i));
              i++;
            }
          }
        }
        if (paramString1 != null)
        {
          paramString3 = new StringBuilder("(");
          paramObject = paramString1.iterator();
          while (((Iterator)paramObject).hasNext())
          {
            paramString1 = ColumnUtils.convert2DbValueIfNeeded(((Iterator)paramObject).next());
            if (ColumnDbType.TEXT.equals(ColumnConverterFactory.getDbColumnType(paramString1.getClass())))
            {
              paramString2 = paramString1.toString();
              paramString1 = paramString2;
              if (paramString2.indexOf('\'') != -1) {
                paramString1 = paramString2.replace("'", "''");
              }
              paramString3.append("'");
              paramString3.append(paramString1);
              paramString3.append("'");
            }
            else
            {
              paramString3.append(paramString1);
            }
            paramString3.append(",");
          }
          paramString3.deleteCharAt(paramString3.length() - 1);
          paramString3.append(")");
          localStringBuilder.append(paramString3.toString());
        }
        else
        {
          throw new IllegalArgumentException("value must be an Array or an Iterable.");
        }
      }
      else if ("BETWEEN".equalsIgnoreCase(paramString1))
      {
        if ((paramObject instanceof Iterable))
        {
          paramString1 = (Iterable)paramObject;
        }
        else
        {
          paramString1 = paramString3;
          if (paramObject.getClass().isArray())
          {
            int k = Array.getLength(paramObject);
            paramString2 = new ArrayList(k);
            for (i = j;; i++)
            {
              paramString1 = paramString2;
              if (i >= k) {
                break;
              }
              paramString2.add(Array.get(paramObject, i));
            }
          }
        }
        if (paramString1 != null)
        {
          paramString2 = paramString1.iterator();
          if (paramString2.hasNext())
          {
            paramString1 = paramString2.next();
            if (paramString2.hasNext())
            {
              paramString2 = paramString2.next();
              paramString1 = ColumnUtils.convert2DbValueIfNeeded(paramString1);
              paramString3 = ColumnUtils.convert2DbValueIfNeeded(paramString2);
              if (ColumnDbType.TEXT.equals(ColumnConverterFactory.getDbColumnType(paramString1.getClass())))
              {
                paramString2 = paramString1.toString();
                paramString1 = paramString2;
                if (paramString2.indexOf('\'') != -1) {
                  paramString1 = paramString2.replace("'", "''");
                }
                paramString3 = paramString3.toString();
                paramString2 = paramString3;
                if (paramString3.indexOf('\'') != -1) {
                  paramString2 = paramString3.replace("'", "''");
                }
                localStringBuilder.append("'");
                localStringBuilder.append(paramString1);
                localStringBuilder.append("'");
                localStringBuilder.append(" AND ");
                localStringBuilder.append("'");
                localStringBuilder.append(paramString2);
                localStringBuilder.append("'");
              }
              else
              {
                localStringBuilder.append(paramString1);
                localStringBuilder.append(" AND ");
                localStringBuilder.append(paramString3);
              }
            }
            else
            {
              throw new IllegalArgumentException("value must have tow items.");
            }
          }
          else
          {
            throw new IllegalArgumentException("value must have tow items.");
          }
        }
        else
        {
          throw new IllegalArgumentException("value must be an Array or an Iterable.");
        }
      }
      else
      {
        paramString1 = ColumnUtils.convert2DbValueIfNeeded(paramObject);
        if (ColumnDbType.TEXT.equals(ColumnConverterFactory.getDbColumnType(paramString1.getClass())))
        {
          paramString2 = paramString1.toString();
          paramString1 = paramString2;
          if (paramString2.indexOf('\'') != -1) {
            paramString1 = paramString2.replace("'", "''");
          }
          localStringBuilder.append("'");
          localStringBuilder.append(paramString1);
          localStringBuilder.append("'");
        }
        else
        {
          localStringBuilder.append(paramString1);
        }
      }
    }
    this.whereItems.add(localStringBuilder.toString());
  }
  
  public static WhereBuilder b()
  {
    return new WhereBuilder();
  }
  
  public static WhereBuilder b(String paramString1, String paramString2, Object paramObject)
  {
    WhereBuilder localWhereBuilder = new WhereBuilder();
    localWhereBuilder.appendCondition(null, paramString1, paramString2, paramObject);
    return localWhereBuilder;
  }
  
  public WhereBuilder and(String paramString1, String paramString2, Object paramObject)
  {
    String str;
    if (this.whereItems.size() == 0) {
      str = null;
    } else {
      str = "AND";
    }
    appendCondition(str, paramString1, paramString2, paramObject);
    return this;
  }
  
  public WhereBuilder and(WhereBuilder paramWhereBuilder)
  {
    String str;
    if (this.whereItems.size() == 0) {
      str = " ";
    } else {
      str = "AND ";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append("(");
    localStringBuilder.append(paramWhereBuilder.toString());
    localStringBuilder.append(")");
    return expr(localStringBuilder.toString());
  }
  
  public WhereBuilder expr(String paramString)
  {
    List localList = this.whereItems;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(" ");
    localStringBuilder.append(paramString);
    localList.add(localStringBuilder.toString());
    return this;
  }
  
  public int getWhereItemSize()
  {
    return this.whereItems.size();
  }
  
  public WhereBuilder or(String paramString1, String paramString2, Object paramObject)
  {
    String str;
    if (this.whereItems.size() == 0) {
      str = null;
    } else {
      str = "OR";
    }
    appendCondition(str, paramString1, paramString2, paramObject);
    return this;
  }
  
  public WhereBuilder or(WhereBuilder paramWhereBuilder)
  {
    String str;
    if (this.whereItems.size() == 0) {
      str = " ";
    } else {
      str = "OR ";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append("(");
    localStringBuilder.append(paramWhereBuilder.toString());
    localStringBuilder.append(")");
    return expr(localStringBuilder.toString());
  }
  
  public String toString()
  {
    if (this.whereItems.size() == 0) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.whereItems.iterator();
    while (localIterator.hasNext()) {
      localStringBuilder.append((String)localIterator.next());
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/sqlite/WhereBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */