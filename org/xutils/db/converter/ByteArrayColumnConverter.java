package org.xutils.db.converter;

import android.database.Cursor;
import org.xutils.db.sqlite.ColumnDbType;

public class ByteArrayColumnConverter
  implements ColumnConverter<byte[]>
{
  public Object fieldValue2DbValue(byte[] paramArrayOfByte)
  {
    return paramArrayOfByte;
  }
  
  public ColumnDbType getColumnDbType()
  {
    return ColumnDbType.BLOB;
  }
  
  public byte[] getFieldValue(Cursor paramCursor, int paramInt)
  {
    if (paramCursor.isNull(paramInt)) {
      paramCursor = null;
    } else {
      paramCursor = paramCursor.getBlob(paramInt);
    }
    return paramCursor;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/converter/ByteArrayColumnConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */