package org.xutils.db.converter;

import java.util.concurrent.ConcurrentHashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.db.sqlite.ColumnDbType;

public final class ColumnConverterFactory
{
  private static final ConcurrentHashMap<String, ColumnConverter> columnType_columnConverter_map = new ConcurrentHashMap();
  
  static
  {
    Object localObject = new BooleanColumnConverter();
    columnType_columnConverter_map.put(Boolean.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Boolean.class.getName(), localObject);
    localObject = new ByteArrayColumnConverter();
    columnType_columnConverter_map.put(byte[].class.getName(), localObject);
    localObject = new ByteColumnConverter();
    columnType_columnConverter_map.put(Byte.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Byte.class.getName(), localObject);
    localObject = new CharColumnConverter();
    columnType_columnConverter_map.put(Character.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Character.class.getName(), localObject);
    localObject = new DateColumnConverter();
    columnType_columnConverter_map.put(java.util.Date.class.getName(), localObject);
    localObject = new DoubleColumnConverter();
    columnType_columnConverter_map.put(Double.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Double.class.getName(), localObject);
    localObject = new FloatColumnConverter();
    columnType_columnConverter_map.put(Float.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Float.class.getName(), localObject);
    localObject = new IntegerColumnConverter();
    columnType_columnConverter_map.put(Integer.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Integer.class.getName(), localObject);
    localObject = new LongColumnConverter();
    columnType_columnConverter_map.put(Long.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Long.class.getName(), localObject);
    localObject = new ShortColumnConverter();
    columnType_columnConverter_map.put(Short.TYPE.getName(), localObject);
    columnType_columnConverter_map.put(Short.class.getName(), localObject);
    localObject = new SqlDateColumnConverter();
    columnType_columnConverter_map.put(java.sql.Date.class.getName(), localObject);
    localObject = new StringColumnConverter();
    columnType_columnConverter_map.put(String.class.getName(), localObject);
  }
  
  public static ColumnConverter getColumnConverter(Class paramClass)
  {
    Object localObject1;
    if (columnType_columnConverter_map.containsKey(paramClass.getName())) {
      localObject1 = (ColumnConverter)columnType_columnConverter_map.get(paramClass.getName());
    } else if (ColumnConverter.class.isAssignableFrom(paramClass)) {
      try
      {
        ColumnConverter localColumnConverter = (ColumnConverter)paramClass.newInstance();
        localObject1 = localColumnConverter;
        if (localColumnConverter == null) {
          break label81;
        }
        columnType_columnConverter_map.put(paramClass.getName(), localColumnConverter);
        localObject1 = localColumnConverter;
      }
      catch (Throwable localThrowable)
      {
        LogUtil.e(localThrowable.getMessage(), localThrowable);
      }
    } else {
      localObject2 = null;
    }
    label81:
    if (localObject2 != null) {
      return (ColumnConverter)localObject2;
    }
    Object localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("Database Column Not Support: ");
    ((StringBuilder)localObject2).append(paramClass.getName());
    ((StringBuilder)localObject2).append(", please impl ColumnConverter or use ColumnConverterFactory#registerColumnConverter(...)");
    throw new RuntimeException(((StringBuilder)localObject2).toString());
  }
  
  public static ColumnDbType getDbColumnType(Class paramClass)
  {
    return getColumnConverter(paramClass).getColumnDbType();
  }
  
  public static boolean isSupportColumnConverter(Class paramClass)
  {
    boolean bool2 = columnType_columnConverter_map.containsKey(paramClass.getName());
    boolean bool1 = true;
    if (bool2) {
      return true;
    }
    if (ColumnConverter.class.isAssignableFrom(paramClass)) {}
    try
    {
      ColumnConverter localColumnConverter = (ColumnConverter)paramClass.newInstance();
      if (localColumnConverter != null) {
        columnType_columnConverter_map.put(paramClass.getName(), localColumnConverter);
      }
      if (localColumnConverter != null) {
        bool1 = false;
      }
      return bool1;
    }
    catch (Throwable paramClass)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static void registerColumnConverter(Class paramClass, ColumnConverter paramColumnConverter)
  {
    columnType_columnConverter_map.put(paramClass.getName(), paramColumnConverter);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/converter/ColumnConverterFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */