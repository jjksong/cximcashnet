package org.xutils.db.converter;

import android.database.Cursor;
import java.sql.Date;
import org.xutils.db.sqlite.ColumnDbType;

public class SqlDateColumnConverter
  implements ColumnConverter<Date>
{
  public Object fieldValue2DbValue(Date paramDate)
  {
    if (paramDate == null) {
      return null;
    }
    return Long.valueOf(paramDate.getTime());
  }
  
  public ColumnDbType getColumnDbType()
  {
    return ColumnDbType.INTEGER;
  }
  
  public Date getFieldValue(Cursor paramCursor, int paramInt)
  {
    if (paramCursor.isNull(paramInt)) {
      paramCursor = null;
    } else {
      paramCursor = new Date(paramCursor.getLong(paramInt));
    }
    return paramCursor;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/converter/SqlDateColumnConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */