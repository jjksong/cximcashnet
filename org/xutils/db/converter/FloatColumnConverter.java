package org.xutils.db.converter;

import android.database.Cursor;
import org.xutils.db.sqlite.ColumnDbType;

public class FloatColumnConverter
  implements ColumnConverter<Float>
{
  public Object fieldValue2DbValue(Float paramFloat)
  {
    return paramFloat;
  }
  
  public ColumnDbType getColumnDbType()
  {
    return ColumnDbType.REAL;
  }
  
  public Float getFieldValue(Cursor paramCursor, int paramInt)
  {
    if (paramCursor.isNull(paramInt)) {
      paramCursor = null;
    } else {
      paramCursor = Float.valueOf(paramCursor.getFloat(paramInt));
    }
    return paramCursor;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/converter/FloatColumnConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */