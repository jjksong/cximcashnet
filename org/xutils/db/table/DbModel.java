package org.xutils.db.table;

import android.text.TextUtils;
import java.util.HashMap;

public final class DbModel
{
  private HashMap<String, String> dataMap = new HashMap();
  
  public void add(String paramString1, String paramString2)
  {
    this.dataMap.put(paramString1, paramString2);
  }
  
  public boolean getBoolean(String paramString)
  {
    paramString = (String)this.dataMap.get(paramString);
    if (paramString != null)
    {
      boolean bool;
      if (paramString.length() == 1) {
        bool = "1".equals(paramString);
      } else {
        bool = Boolean.valueOf(paramString).booleanValue();
      }
      return bool;
    }
    return false;
  }
  
  public HashMap<String, String> getDataMap()
  {
    return this.dataMap;
  }
  
  public java.util.Date getDate(String paramString)
  {
    return new java.util.Date(Long.valueOf((String)this.dataMap.get(paramString)).longValue());
  }
  
  public double getDouble(String paramString)
  {
    return Double.valueOf((String)this.dataMap.get(paramString)).doubleValue();
  }
  
  public float getFloat(String paramString)
  {
    return Float.valueOf((String)this.dataMap.get(paramString)).floatValue();
  }
  
  public int getInt(String paramString)
  {
    return Integer.valueOf((String)this.dataMap.get(paramString)).intValue();
  }
  
  public long getLong(String paramString)
  {
    return Long.valueOf((String)this.dataMap.get(paramString)).longValue();
  }
  
  public java.sql.Date getSqlDate(String paramString)
  {
    return new java.sql.Date(Long.valueOf((String)this.dataMap.get(paramString)).longValue());
  }
  
  public String getString(String paramString)
  {
    return (String)this.dataMap.get(paramString);
  }
  
  public boolean isEmpty(String paramString)
  {
    return TextUtils.isEmpty((CharSequence)this.dataMap.get(paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/DbModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */