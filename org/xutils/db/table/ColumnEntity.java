package org.xutils.db.table;

import android.database.Cursor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.xutils.common.util.LogUtil;
import org.xutils.db.annotation.Column;
import org.xutils.db.converter.ColumnConverter;
import org.xutils.db.converter.ColumnConverterFactory;
import org.xutils.db.sqlite.ColumnDbType;

public final class ColumnEntity
{
  protected final ColumnConverter columnConverter;
  protected final Field columnField;
  protected final Method getMethod;
  private final boolean isAutoId;
  private final boolean isId;
  protected final String name;
  private final String property;
  protected final Method setMethod;
  
  ColumnEntity(Class<?> paramClass, Field paramField, Column paramColumn)
  {
    paramField.setAccessible(true);
    this.columnField = paramField;
    this.name = paramColumn.name();
    this.property = paramColumn.property();
    this.isId = paramColumn.isId();
    Class localClass = paramField.getType();
    boolean bool;
    if ((this.isId) && (paramColumn.autoGen()) && (ColumnUtils.isAutoIdType(localClass))) {
      bool = true;
    } else {
      bool = false;
    }
    this.isAutoId = bool;
    this.columnConverter = ColumnConverterFactory.getColumnConverter(localClass);
    this.getMethod = ColumnUtils.findGetMethod(paramClass, paramField);
    paramColumn = this.getMethod;
    if ((paramColumn != null) && (!paramColumn.isAccessible())) {
      this.getMethod.setAccessible(true);
    }
    this.setMethod = ColumnUtils.findSetMethod(paramClass, paramField);
    paramClass = this.setMethod;
    if ((paramClass != null) && (!paramClass.isAccessible())) {
      this.setMethod.setAccessible(true);
    }
  }
  
  public ColumnConverter getColumnConverter()
  {
    return this.columnConverter;
  }
  
  public ColumnDbType getColumnDbType()
  {
    return this.columnConverter.getColumnDbType();
  }
  
  public Field getColumnField()
  {
    return this.columnField;
  }
  
  public Object getColumnValue(Object paramObject)
  {
    paramObject = getFieldValue(paramObject);
    if ((this.isAutoId) && ((paramObject.equals(Long.valueOf(0L))) || (paramObject.equals(Integer.valueOf(0))))) {
      return null;
    }
    return this.columnConverter.fieldValue2DbValue(paramObject);
  }
  
  public Object getFieldValue(Object paramObject)
  {
    if (paramObject != null)
    {
      Method localMethod = this.getMethod;
      if (localMethod != null) {
        try
        {
          paramObject = localMethod.invoke(paramObject, new Object[0]);
        }
        catch (Throwable paramObject)
        {
          LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
          break label59;
        }
      } else {
        try
        {
          paramObject = this.columnField.get(paramObject);
        }
        catch (Throwable paramObject)
        {
          LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
        }
      }
    }
    label59:
    paramObject = null;
    return paramObject;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getProperty()
  {
    return this.property;
  }
  
  public boolean isAutoId()
  {
    return this.isAutoId;
  }
  
  public boolean isId()
  {
    return this.isId;
  }
  
  public void setAutoIdValue(Object paramObject, long paramLong)
  {
    Object localObject = Long.valueOf(paramLong);
    if (ColumnUtils.isInteger(this.columnField.getType())) {
      localObject = Integer.valueOf((int)paramLong);
    }
    Method localMethod = this.setMethod;
    if (localMethod != null) {
      try
      {
        localMethod.invoke(paramObject, new Object[] { localObject });
      }
      catch (Throwable paramObject)
      {
        LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
      }
    } else {
      try
      {
        this.columnField.set(paramObject, localObject);
      }
      catch (Throwable paramObject)
      {
        LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
      }
    }
  }
  
  public void setValueFromCursor(Object paramObject, Cursor paramCursor, int paramInt)
  {
    paramCursor = this.columnConverter.getFieldValue(paramCursor, paramInt);
    if (paramCursor == null) {
      return;
    }
    Method localMethod = this.setMethod;
    if (localMethod != null) {
      try
      {
        localMethod.invoke(paramObject, new Object[] { paramCursor });
      }
      catch (Throwable paramObject)
      {
        LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
      }
    } else {
      try
      {
        this.columnField.set(paramObject, paramCursor);
      }
      catch (Throwable paramObject)
      {
        LogUtil.e(((Throwable)paramObject).getMessage(), (Throwable)paramObject);
      }
    }
  }
  
  public String toString()
  {
    return this.name;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/ColumnEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */