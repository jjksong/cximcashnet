package org.xutils.db.table;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import org.xutils.common.util.LogUtil;
import org.xutils.db.converter.ColumnConverter;
import org.xutils.db.converter.ColumnConverterFactory;

public final class ColumnUtils
{
  private static final HashSet<Class<?>> AUTO_INCREMENT_TYPES;
  private static final HashSet<Class<?>> BOOLEAN_TYPES = new HashSet(2);
  private static final HashSet<Class<?>> INTEGER_TYPES = new HashSet(2);
  
  static
  {
    AUTO_INCREMENT_TYPES = new HashSet(4);
    BOOLEAN_TYPES.add(Boolean.TYPE);
    BOOLEAN_TYPES.add(Boolean.class);
    INTEGER_TYPES.add(Integer.TYPE);
    INTEGER_TYPES.add(Integer.class);
    AUTO_INCREMENT_TYPES.addAll(INTEGER_TYPES);
    AUTO_INCREMENT_TYPES.add(Long.TYPE);
    AUTO_INCREMENT_TYPES.add(Long.class);
  }
  
  public static Object convert2DbValueIfNeeded(Object paramObject)
  {
    Object localObject = paramObject;
    if (paramObject != null) {
      localObject = ColumnConverterFactory.getColumnConverter(paramObject.getClass()).fieldValue2DbValue(paramObject);
    }
    return localObject;
  }
  
  private static Method findBooleanGetMethod(Class<?> paramClass, String paramString)
  {
    Object localObject;
    if (!paramString.startsWith("is"))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("is");
      ((StringBuilder)localObject).append(paramString.substring(0, 1).toUpperCase());
      ((StringBuilder)localObject).append(paramString.substring(1));
      paramString = ((StringBuilder)localObject).toString();
    }
    try
    {
      localObject = paramClass.getDeclaredMethod(paramString, new Class[0]);
      return (Method)localObject;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramClass.getName());
      localStringBuilder.append("#");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" not exist");
      LogUtil.d(localStringBuilder.toString());
    }
    return null;
  }
  
  private static Method findBooleanSetMethod(Class<?> paramClass1, String paramString, Class<?> paramClass2)
  {
    StringBuilder localStringBuilder;
    if (paramString.startsWith("is"))
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("set");
      localStringBuilder.append(paramString.substring(2, 3).toUpperCase());
      localStringBuilder.append(paramString.substring(3));
      paramString = localStringBuilder.toString();
    }
    else
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("set");
      localStringBuilder.append(paramString.substring(0, 1).toUpperCase());
      localStringBuilder.append(paramString.substring(1));
      paramString = localStringBuilder.toString();
    }
    try
    {
      paramClass2 = paramClass1.getDeclaredMethod(paramString, new Class[] { paramClass2 });
      return paramClass2;
    }
    catch (NoSuchMethodException paramClass2)
    {
      paramClass2 = new StringBuilder();
      paramClass2.append(paramClass1.getName());
      paramClass2.append("#");
      paramClass2.append(paramString);
      paramClass2.append(" not exist");
      LogUtil.d(paramClass2.toString());
    }
    return null;
  }
  
  static Method findGetMethod(Class<?> paramClass, Field paramField)
  {
    boolean bool = Object.class.equals(paramClass);
    Method localMethod = null;
    if (bool) {
      return null;
    }
    String str = paramField.getName();
    if (isBoolean(paramField.getType())) {
      localMethod = findBooleanGetMethod(paramClass, str);
    }
    Object localObject1 = localMethod;
    Object localObject2;
    if (localMethod == null)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("get");
      ((StringBuilder)localObject1).append(str.substring(0, 1).toUpperCase());
      ((StringBuilder)localObject1).append(str.substring(1));
      str = ((StringBuilder)localObject1).toString();
      try
      {
        localObject1 = paramClass.getDeclaredMethod(str, new Class[0]);
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append(paramClass.getName());
        ((StringBuilder)localObject2).append("#");
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(" not exist");
        LogUtil.d(((StringBuilder)localObject2).toString());
        localObject2 = localMethod;
      }
    }
    if (localObject2 == null) {
      return findGetMethod(paramClass.getSuperclass(), paramField);
    }
    return (Method)localObject2;
  }
  
  static Method findSetMethod(Class<?> paramClass, Field paramField)
  {
    boolean bool = Object.class.equals(paramClass);
    Method localMethod = null;
    if (bool) {
      return null;
    }
    String str = paramField.getName();
    Class localClass = paramField.getType();
    if (isBoolean(localClass)) {
      localMethod = findBooleanSetMethod(paramClass, str, localClass);
    }
    Object localObject1 = localMethod;
    Object localObject2;
    if (localMethod == null)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append("set");
      ((StringBuilder)localObject1).append(str.substring(0, 1).toUpperCase());
      ((StringBuilder)localObject1).append(str.substring(1));
      str = ((StringBuilder)localObject1).toString();
      try
      {
        localObject1 = paramClass.getDeclaredMethod(str, new Class[] { localClass });
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append(paramClass.getName());
        ((StringBuilder)localObject2).append("#");
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(" not exist");
        LogUtil.d(((StringBuilder)localObject2).toString());
        localObject2 = localMethod;
      }
    }
    if (localObject2 == null) {
      return findSetMethod(paramClass.getSuperclass(), paramField);
    }
    return (Method)localObject2;
  }
  
  public static boolean isAutoIdType(Class<?> paramClass)
  {
    return AUTO_INCREMENT_TYPES.contains(paramClass);
  }
  
  public static boolean isBoolean(Class<?> paramClass)
  {
    return BOOLEAN_TYPES.contains(paramClass);
  }
  
  public static boolean isInteger(Class<?> paramClass)
  {
    return INTEGER_TYPES.contains(paramClass);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/ColumnUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */