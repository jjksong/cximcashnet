package org.xutils.db.table;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.xutils.DbManager;
import org.xutils.db.annotation.Table;

public final class TableEntity<T>
{
  private volatile boolean checkedDatabase;
  private final LinkedHashMap<String, ColumnEntity> columnMap;
  private Constructor<T> constructor;
  private final DbManager db;
  private Class<T> entityType;
  private ColumnEntity id;
  private final String name;
  private final String onCreated;
  
  TableEntity(DbManager paramDbManager, Class<T> paramClass)
    throws Throwable
  {
    this.db = paramDbManager;
    this.entityType = paramClass;
    this.constructor = paramClass.getConstructor(new Class[0]);
    this.constructor.setAccessible(true);
    paramDbManager = (Table)paramClass.getAnnotation(Table.class);
    this.name = paramDbManager.name();
    this.onCreated = paramDbManager.onCreated();
    this.columnMap = TableUtils.findColumnMap(paramClass);
    paramClass = this.columnMap.values().iterator();
    while (paramClass.hasNext())
    {
      paramDbManager = (ColumnEntity)paramClass.next();
      if (paramDbManager.isId()) {
        this.id = paramDbManager;
      }
    }
  }
  
  public T createEntity()
    throws Throwable
  {
    return (T)this.constructor.newInstance(new Object[0]);
  }
  
  public LinkedHashMap<String, ColumnEntity> getColumnMap()
  {
    return this.columnMap;
  }
  
  public DbManager getDb()
  {
    return this.db;
  }
  
  public Class<T> getEntityType()
  {
    return this.entityType;
  }
  
  public ColumnEntity getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getOnCreated()
  {
    return this.onCreated;
  }
  
  boolean isCheckedDatabase()
  {
    return this.checkedDatabase;
  }
  
  void setCheckedDatabase(boolean paramBoolean)
  {
    this.checkedDatabase = paramBoolean;
  }
  
  /* Error */
  public boolean tableIsExist()
    throws org.xutils.ex.DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 131	org/xutils/db/table/TableEntity:isCheckedDatabase	()Z
    //   4: ifeq +5 -> 9
    //   7: iconst_1
    //   8: ireturn
    //   9: aload_0
    //   10: getfield 32	org/xutils/db/table/TableEntity:db	Lorg/xutils/DbManager;
    //   13: astore_1
    //   14: new 133	java/lang/StringBuilder
    //   17: dup
    //   18: invokespecial 134	java/lang/StringBuilder:<init>	()V
    //   21: astore_2
    //   22: aload_2
    //   23: ldc -120
    //   25: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: aload_2
    //   30: aload_0
    //   31: getfield 59	org/xutils/db/table/TableEntity:name	Ljava/lang/String;
    //   34: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_2
    //   39: ldc -114
    //   41: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: pop
    //   45: aload_1
    //   46: aload_2
    //   47: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   50: invokeinterface 151 2 0
    //   55: astore_1
    //   56: aload_1
    //   57: ifnull +62 -> 119
    //   60: aload_1
    //   61: invokeinterface 156 1 0
    //   66: ifeq +24 -> 90
    //   69: aload_1
    //   70: iconst_0
    //   71: invokeinterface 160 2 0
    //   76: ifle +14 -> 90
    //   79: aload_0
    //   80: iconst_1
    //   81: invokevirtual 162	org/xutils/db/table/TableEntity:setCheckedDatabase	(Z)V
    //   84: aload_1
    //   85: invokestatic 168	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   88: iconst_1
    //   89: ireturn
    //   90: aload_1
    //   91: invokestatic 168	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   94: goto +25 -> 119
    //   97: astore_2
    //   98: goto +15 -> 113
    //   101: astore_3
    //   102: new 129	org/xutils/ex/DbException
    //   105: astore_2
    //   106: aload_2
    //   107: aload_3
    //   108: invokespecial 171	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   111: aload_2
    //   112: athrow
    //   113: aload_1
    //   114: invokestatic 168	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   117: aload_2
    //   118: athrow
    //   119: iconst_0
    //   120: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	121	0	this	TableEntity
    //   13	101	1	localObject1	Object
    //   21	26	2	localStringBuilder	StringBuilder
    //   97	1	2	localObject2	Object
    //   105	13	2	localDbException	org.xutils.ex.DbException
    //   101	7	3	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   60	84	97	finally
    //   102	113	97	finally
    //   60	84	101	java/lang/Throwable
  }
  
  public String toString()
  {
    return this.name;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/TableEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */