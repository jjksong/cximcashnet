package org.xutils.db.table;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.xutils.DbManager;
import org.xutils.DbManager.DaoConfig;
import org.xutils.DbManager.TableCreateListener;
import org.xutils.db.sqlite.SqlInfoBuilder;
import org.xutils.ex.DbException;

public abstract class DbBase
  implements DbManager
{
  private final HashMap<Class<?>, TableEntity<?>> tableMap = new HashMap();
  
  public void addColumn(Class<?> paramClass, String paramString)
    throws DbException
  {
    paramClass = getTable(paramClass);
    paramString = (ColumnEntity)paramClass.getColumnMap().get(paramString);
    if (paramString != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("ALTER TABLE ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramClass.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" ADD COLUMN ");
      localStringBuilder.append("\"");
      localStringBuilder.append(paramString.getName());
      localStringBuilder.append("\"");
      localStringBuilder.append(" ");
      localStringBuilder.append(paramString.getColumnDbType());
      localStringBuilder.append(" ");
      localStringBuilder.append(paramString.getProperty());
      execNonQuery(localStringBuilder.toString());
    }
  }
  
  protected void createTableIfNotExist(TableEntity<?> paramTableEntity)
    throws DbException
  {
    if (!paramTableEntity.tableIsExist()) {
      synchronized (paramTableEntity.getClass())
      {
        if (!paramTableEntity.tableIsExist())
        {
          execNonQuery(SqlInfoBuilder.buildCreateTableSqlInfo(paramTableEntity));
          Object localObject = paramTableEntity.getOnCreated();
          if (!TextUtils.isEmpty((CharSequence)localObject)) {
            execNonQuery((String)localObject);
          }
          paramTableEntity.setCheckedDatabase(true);
          localObject = getDaoConfig().getTableCreateListener();
          if (localObject != null) {
            ((DbManager.TableCreateListener)localObject).onTableCreated(this, paramTableEntity);
          }
        }
      }
    }
  }
  
  /* Error */
  public void dropDb()
    throws DbException
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc -121
    //   3: invokevirtual 139	org/xutils/db/table/DbBase:execQuery	(Ljava/lang/String;)Landroid/database/Cursor;
    //   6: astore_2
    //   7: aload_2
    //   8: ifnull +166 -> 174
    //   11: aload_2
    //   12: invokeinterface 144 1 0
    //   17: istore_1
    //   18: iload_1
    //   19: ifeq +60 -> 79
    //   22: aload_2
    //   23: iconst_0
    //   24: invokeinterface 148 2 0
    //   29: astore_3
    //   30: new 43	java/lang/StringBuilder
    //   33: astore 4
    //   35: aload 4
    //   37: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   40: aload 4
    //   42: ldc -106
    //   44: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload 4
    //   50: aload_3
    //   51: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload_0
    //   56: aload 4
    //   58: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   61: invokevirtual 78	org/xutils/db/table/DbBase:execNonQuery	(Ljava/lang/String;)V
    //   64: goto -53 -> 11
    //   67: astore_3
    //   68: aload_3
    //   69: invokevirtual 153	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   72: aload_3
    //   73: invokestatic 159	org/xutils/common/util/LogUtil:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   76: goto -65 -> 11
    //   79: aload_0
    //   80: getfield 18	org/xutils/db/table/DbBase:tableMap	Ljava/util/HashMap;
    //   83: astore_3
    //   84: aload_3
    //   85: monitorenter
    //   86: aload_0
    //   87: getfield 18	org/xutils/db/table/DbBase:tableMap	Ljava/util/HashMap;
    //   90: invokevirtual 163	java/util/HashMap:values	()Ljava/util/Collection;
    //   93: invokeinterface 169 1 0
    //   98: astore 4
    //   100: aload 4
    //   102: invokeinterface 174 1 0
    //   107: ifeq +20 -> 127
    //   110: aload 4
    //   112: invokeinterface 178 1 0
    //   117: checkcast 29	org/xutils/db/table/TableEntity
    //   120: iconst_0
    //   121: invokevirtual 113	org/xutils/db/table/TableEntity:setCheckedDatabase	(Z)V
    //   124: goto -24 -> 100
    //   127: aload_0
    //   128: getfield 18	org/xutils/db/table/DbBase:tableMap	Ljava/util/HashMap;
    //   131: invokevirtual 181	java/util/HashMap:clear	()V
    //   134: aload_3
    //   135: monitorexit
    //   136: aload_2
    //   137: invokestatic 187	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   140: goto +34 -> 174
    //   143: astore 4
    //   145: aload_3
    //   146: monitorexit
    //   147: aload 4
    //   149: athrow
    //   150: astore_3
    //   151: goto +17 -> 168
    //   154: astore 4
    //   156: new 23	org/xutils/ex/DbException
    //   159: astore_3
    //   160: aload_3
    //   161: aload 4
    //   163: invokespecial 190	org/xutils/ex/DbException:<init>	(Ljava/lang/Throwable;)V
    //   166: aload_3
    //   167: athrow
    //   168: aload_2
    //   169: invokestatic 187	org/xutils/common/util/IOUtil:closeQuietly	(Landroid/database/Cursor;)V
    //   172: aload_3
    //   173: athrow
    //   174: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	175	0	this	DbBase
    //   17	2	1	bool	boolean
    //   6	163	2	localCursor	android.database.Cursor
    //   29	22	3	str	String
    //   67	6	3	localThrowable1	Throwable
    //   150	1	3	localObject1	Object
    //   159	14	3	localDbException	DbException
    //   33	78	4	localObject2	Object
    //   143	5	4	localObject3	Object
    //   154	8	4	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   22	64	67	java/lang/Throwable
    //   86	100	143	finally
    //   100	124	143	finally
    //   127	136	143	finally
    //   145	147	143	finally
    //   11	18	150	finally
    //   22	64	150	finally
    //   68	76	150	finally
    //   79	86	150	finally
    //   147	150	150	finally
    //   156	168	150	finally
    //   11	18	154	java/lang/Throwable
    //   68	76	154	java/lang/Throwable
    //   79	86	154	java/lang/Throwable
    //   147	150	154	java/lang/Throwable
  }
  
  public void dropTable(Class<?> paramClass)
    throws DbException
  {
    TableEntity localTableEntity = getTable(paramClass);
    if (!localTableEntity.tableIsExist()) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DROP TABLE \"");
    localStringBuilder.append(localTableEntity.getName());
    localStringBuilder.append("\"");
    execNonQuery(localStringBuilder.toString());
    localTableEntity.setCheckedDatabase(false);
    removeTable(paramClass);
  }
  
  public <T> TableEntity<T> getTable(Class<T> paramClass)
    throws DbException
  {
    synchronized (this.tableMap)
    {
      TableEntity localTableEntity = (TableEntity)this.tableMap.get(paramClass);
      Object localObject = localTableEntity;
      if (localTableEntity == null) {
        try
        {
          localObject = new org/xutils/db/table/TableEntity;
          ((TableEntity)localObject).<init>(this, paramClass);
          this.tableMap.put(paramClass, localObject);
        }
        catch (Throwable paramClass)
        {
          localObject = new org/xutils/ex/DbException;
          ((DbException)localObject).<init>(paramClass);
          throw ((Throwable)localObject);
        }
      }
      return (TableEntity<T>)localObject;
    }
  }
  
  protected void removeTable(Class<?> paramClass)
  {
    synchronized (this.tableMap)
    {
      this.tableMap.remove(paramClass);
      return;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/DbBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */