package org.xutils.db.table;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.xutils.common.util.LogUtil;
import org.xutils.db.annotation.Column;
import org.xutils.db.converter.ColumnConverterFactory;

final class TableUtils
{
  private static void addColumns2Map(Class<?> paramClass, HashMap<String, ColumnEntity> paramHashMap)
  {
    if (Object.class.equals(paramClass)) {
      return;
    }
    try
    {
      for (Field localField : paramClass.getDeclaredFields())
      {
        int k = localField.getModifiers();
        if ((!Modifier.isStatic(k)) && (!Modifier.isTransient(k)))
        {
          Column localColumn = (Column)localField.getAnnotation(Column.class);
          if ((localColumn != null) && (ColumnConverterFactory.isSupportColumnConverter(localField.getType())))
          {
            ColumnEntity localColumnEntity = new org/xutils/db/table/ColumnEntity;
            localColumnEntity.<init>(paramClass, localField, localColumn);
            if (!paramHashMap.containsKey(localColumnEntity.getName())) {
              paramHashMap.put(localColumnEntity.getName(), localColumnEntity);
            }
          }
        }
      }
      addColumns2Map(paramClass.getSuperclass(), paramHashMap);
    }
    catch (Throwable paramClass)
    {
      LogUtil.e(paramClass.getMessage(), paramClass);
    }
  }
  
  static LinkedHashMap<String, ColumnEntity> findColumnMap(Class<?> paramClass)
  {
    try
    {
      LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
      localLinkedHashMap.<init>();
      addColumns2Map(paramClass, localLinkedHashMap);
      return localLinkedHashMap;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/org/xutils/db/table/TableUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */