package top.zibin.luban;

import android.text.TextUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

class Checker
{
  private static final String GIF = "gif";
  private static final String JPEG = "jpeg";
  private static final String JPG = "jpg";
  private static final String PNG = "png";
  private static final String WEBP = "webp";
  private static List<String> format = new ArrayList();
  
  static
  {
    format.add("jpg");
    format.add("jpeg");
    format.add("png");
    format.add("webp");
    format.add("gif");
  }
  
  static String checkSuffix(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return ".jpg";
    }
    return paramString.substring(paramString.lastIndexOf("."), paramString.length());
  }
  
  static boolean isImage(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    paramString = paramString.substring(paramString.lastIndexOf(".") + 1, paramString.length());
    return format.contains(paramString.toLowerCase());
  }
  
  static boolean isJPG(String paramString)
  {
    boolean bool2 = TextUtils.isEmpty(paramString);
    boolean bool1 = false;
    if (bool2) {
      return false;
    }
    paramString = paramString.substring(paramString.lastIndexOf("."), paramString.length()).toLowerCase();
    if ((paramString.contains("jpg")) || (paramString.contains("jpeg"))) {
      bool1 = true;
    }
    return bool1;
  }
  
  static boolean isNeedCompress(int paramInt, String paramString)
  {
    if (paramInt > 0)
    {
      paramString = new File(paramString);
      if (!paramString.exists()) {
        return false;
      }
      if (paramString.length() <= paramInt << 10) {
        return false;
      }
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/top/zibin/luban/Checker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */