package top.zibin.luban;

import java.io.File;

public abstract interface OnCompressListener
{
  public abstract void onError(Throwable paramThrowable);
  
  public abstract void onStart();
  
  public abstract void onSuccess(File paramFile);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/top/zibin/luban/OnCompressListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */