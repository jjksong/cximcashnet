package top.zibin.luban;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class Engine
{
  private ExifInterface srcExif;
  private int srcHeight;
  private String srcImg;
  private int srcWidth;
  private File tagImg;
  
  Engine(String paramString, File paramFile)
    throws IOException
  {
    if (Checker.isJPG(paramString)) {
      this.srcExif = new ExifInterface(paramString);
    }
    this.tagImg = paramFile;
    this.srcImg = paramString;
    paramFile = new BitmapFactory.Options();
    paramFile.inJustDecodeBounds = true;
    paramFile.inSampleSize = 1;
    BitmapFactory.decodeFile(paramString, paramFile);
    this.srcWidth = paramFile.outWidth;
    this.srcHeight = paramFile.outHeight;
  }
  
  private int computeSize()
  {
    int j = this.srcWidth;
    int i = j;
    if (j % 2 == 1) {
      i = j + 1;
    }
    this.srcWidth = i;
    j = this.srcHeight;
    i = j;
    if (j % 2 == 1) {
      i = j + 1;
    }
    this.srcHeight = i;
    i = Math.max(this.srcWidth, this.srcHeight);
    float f = Math.min(this.srcWidth, this.srcHeight) / i;
    if ((f <= 1.0F) && (f > 0.5625D))
    {
      if (i < 1664) {
        return 1;
      }
      if ((i >= 1664) && (i < 4990)) {
        return 2;
      }
      if ((i > 4990) && (i < 10240)) {
        return 4;
      }
      j = i / 1280;
      i = j;
      if (j == 0) {
        i = 1;
      }
      return i;
    }
    double d2 = f;
    if ((d2 <= 0.5625D) && (d2 > 0.5D))
    {
      j = i / 1280;
      i = j;
      if (j == 0) {
        i = 1;
      }
      return i;
    }
    double d1 = i;
    Double.isNaN(d2);
    d2 = 1280.0D / d2;
    Double.isNaN(d1);
    return (int)Math.ceil(d1 / d2);
  }
  
  private Bitmap rotatingImage(Bitmap paramBitmap)
  {
    if (this.srcExif == null) {
      return paramBitmap;
    }
    Matrix localMatrix = new Matrix();
    int i = 0;
    int j = this.srcExif.getAttributeInt("Orientation", 1);
    if (j != 3)
    {
      if (j != 6)
      {
        if (j == 8) {
          i = 270;
        }
      }
      else {
        i = 90;
      }
    }
    else {
      i = 180;
    }
    localMatrix.postRotate(i);
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, true);
  }
  
  File compress()
    throws IOException
  {
    Object localObject1 = new BitmapFactory.Options();
    ((BitmapFactory.Options)localObject1).inSampleSize = computeSize();
    Object localObject2 = BitmapFactory.decodeFile(this.srcImg, (BitmapFactory.Options)localObject1);
    localObject1 = new ByteArrayOutputStream();
    localObject2 = rotatingImage((Bitmap)localObject2);
    ((Bitmap)localObject2).compress(Bitmap.CompressFormat.JPEG, 60, (OutputStream)localObject1);
    ((Bitmap)localObject2).recycle();
    localObject2 = new FileOutputStream(this.tagImg);
    ((FileOutputStream)localObject2).write(((ByteArrayOutputStream)localObject1).toByteArray());
    ((FileOutputStream)localObject2).flush();
    ((FileOutputStream)localObject2).close();
    ((ByteArrayOutputStream)localObject1).close();
    return this.tagImg;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/top/zibin/luban/Engine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */