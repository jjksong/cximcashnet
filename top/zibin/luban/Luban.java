package top.zibin.luban;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;

public class Luban
  implements Handler.Callback
{
  private static final String DEFAULT_DISK_CACHE_DIR = "luban_disk_cache";
  private static final int MSG_COMPRESS_ERROR = 2;
  private static final int MSG_COMPRESS_START = 1;
  private static final int MSG_COMPRESS_SUCCESS = 0;
  private static final String TAG = "Luban";
  private OnCompressListener mCompressListener;
  private Handler mHandler;
  private int mLeastCompressSize;
  private List<String> mPaths;
  private String mTargetDir;
  
  private Luban(Builder paramBuilder)
  {
    this.mPaths = paramBuilder.mPaths;
    this.mTargetDir = paramBuilder.mTargetDir;
    this.mCompressListener = paramBuilder.mCompressListener;
    this.mLeastCompressSize = paramBuilder.mLeastCompressSize;
    this.mHandler = new Handler(Looper.getMainLooper(), this);
  }
  
  @WorkerThread
  private File get(String paramString, Context paramContext)
    throws IOException
  {
    return new Engine(paramString, getImageCacheFile(paramContext, Checker.checkSuffix(paramString))).compress();
  }
  
  @WorkerThread
  private List<File> get(Context paramContext)
    throws IOException
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mPaths.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (Checker.isImage(str)) {
        localArrayList.add(new Engine(str, getImageCacheFile(paramContext, Checker.checkSuffix(str))).compress());
      }
      localIterator.remove();
    }
    return localArrayList;
  }
  
  @Nullable
  private File getImageCacheDir(Context paramContext)
  {
    return getImageCacheDir(paramContext, "luban_disk_cache");
  }
  
  @Nullable
  private File getImageCacheDir(Context paramContext, String paramString)
  {
    paramContext = paramContext.getExternalCacheDir();
    if (paramContext != null)
    {
      paramContext = new File(paramContext, paramString);
      if ((!paramContext.mkdirs()) && ((!paramContext.exists()) || (!paramContext.isDirectory()))) {
        return null;
      }
      return paramContext;
    }
    if (Log.isLoggable("Luban", 6)) {
      Log.e("Luban", "default disk cache dir is null");
    }
    return null;
  }
  
  private File getImageCacheFile(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(this.mTargetDir)) {
      this.mTargetDir = getImageCacheDir(paramContext).getAbsolutePath();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mTargetDir);
    localStringBuilder.append("/");
    localStringBuilder.append(System.currentTimeMillis());
    localStringBuilder.append((int)(Math.random() * 1000.0D));
    paramContext = paramString;
    if (TextUtils.isEmpty(paramString)) {
      paramContext = ".jpg";
    }
    localStringBuilder.append(paramContext);
    return new File(localStringBuilder.toString());
  }
  
  @UiThread
  private void launch(final Context paramContext)
  {
    Object localObject = this.mPaths;
    if ((localObject == null) || ((((List)localObject).size() == 0) && (this.mCompressListener != null))) {
      this.mCompressListener.onError(new NullPointerException("image file cannot be null"));
    }
    localObject = this.mPaths.iterator();
    while (((Iterator)localObject).hasNext())
    {
      final String str = (String)((Iterator)localObject).next();
      if (Checker.isImage(str))
      {
        AsyncTask.SERIAL_EXECUTOR.execute(new Runnable()
        {
          public void run()
          {
            try
            {
              Luban.this.mHandler.sendMessage(Luban.this.mHandler.obtainMessage(1));
              Object localObject;
              if (Checker.isNeedCompress(Luban.this.mLeastCompressSize, str))
              {
                localObject = new top/zibin/luban/Engine;
                ((Engine)localObject).<init>(str, Luban.this.getImageCacheFile(paramContext, Checker.checkSuffix(str)));
                localObject = ((Engine)localObject).compress();
              }
              else
              {
                localObject = new File(str);
              }
              Luban.this.mHandler.sendMessage(Luban.this.mHandler.obtainMessage(0, localObject));
            }
            catch (IOException localIOException)
            {
              Luban.this.mHandler.sendMessage(Luban.this.mHandler.obtainMessage(2, localIOException));
            }
          }
        });
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("can not read the path : ");
        localStringBuilder.append(str);
        Log.e("Luban", localStringBuilder.toString());
      }
      ((Iterator)localObject).remove();
    }
  }
  
  public static Builder with(Context paramContext)
  {
    return new Builder(paramContext);
  }
  
  public boolean handleMessage(Message paramMessage)
  {
    if (this.mCompressListener == null) {
      return false;
    }
    switch (paramMessage.what)
    {
    default: 
      break;
    case 2: 
      this.mCompressListener.onError((Throwable)paramMessage.obj);
      break;
    case 1: 
      this.mCompressListener.onStart();
      break;
    case 0: 
      this.mCompressListener.onSuccess((File)paramMessage.obj);
    }
    return false;
  }
  
  public static class Builder
  {
    private Context context;
    private OnCompressListener mCompressListener;
    private int mLeastCompressSize = 100;
    private List<String> mPaths;
    private String mTargetDir;
    
    Builder(Context paramContext)
    {
      this.context = paramContext;
      this.mPaths = new ArrayList();
    }
    
    private Luban build()
    {
      return new Luban(this, null);
    }
    
    public File get(String paramString)
      throws IOException
    {
      return build().get(paramString, this.context);
    }
    
    public List<File> get()
      throws IOException
    {
      return build().get(this.context);
    }
    
    public Builder ignoreBy(int paramInt)
    {
      this.mLeastCompressSize = paramInt;
      return this;
    }
    
    public void launch()
    {
      build().launch(this.context);
    }
    
    public Builder load(File paramFile)
    {
      this.mPaths.add(paramFile.getAbsolutePath());
      return this;
    }
    
    public Builder load(String paramString)
    {
      this.mPaths.add(paramString);
      return this;
    }
    
    public Builder load(List<String> paramList)
    {
      this.mPaths.addAll(paramList);
      return this;
    }
    
    public Builder putGear(int paramInt)
    {
      return this;
    }
    
    public Builder setCompressListener(OnCompressListener paramOnCompressListener)
    {
      this.mCompressListener = paramOnCompressListener;
      return this;
    }
    
    public Builder setTargetDir(String paramString)
    {
      this.mTargetDir = paramString;
      return this;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/top/zibin/luban/Luban.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */