package io.reactivex;

import io.reactivex.disposables.Disposable;

public abstract interface Observer<T>
{
  public abstract void onComplete();
  
  public abstract void onError(Throwable paramThrowable);
  
  public abstract void onNext(T paramT);
  
  public abstract void onSubscribe(Disposable paramDisposable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/Observer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */