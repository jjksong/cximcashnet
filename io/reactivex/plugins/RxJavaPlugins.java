package io.reactivex.plugins;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.Experimental;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.schedulers.ComputationScheduler;
import io.reactivex.internal.schedulers.IoScheduler;
import io.reactivex.internal.schedulers.NewThreadScheduler;
import io.reactivex.internal.schedulers.SingleScheduler;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observables.ConnectableObservable;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadFactory;
import org.reactivestreams.Subscriber;

public final class RxJavaPlugins
{
  static volatile Consumer<Throwable> errorHandler;
  static volatile boolean failNonBlockingScheduler;
  static volatile boolean lockdown;
  static volatile BooleanSupplier onBeforeBlocking;
  static volatile Function<Completable, Completable> onCompletableAssembly;
  static volatile BiFunction<Completable, CompletableObserver, CompletableObserver> onCompletableSubscribe;
  static volatile Function<Scheduler, Scheduler> onComputationHandler;
  static volatile Function<ConnectableFlowable, ConnectableFlowable> onConnectableFlowableAssembly;
  static volatile Function<ConnectableObservable, ConnectableObservable> onConnectableObservableAssembly;
  static volatile Function<Flowable, Flowable> onFlowableAssembly;
  static volatile BiFunction<Flowable, Subscriber, Subscriber> onFlowableSubscribe;
  static volatile Function<Callable<Scheduler>, Scheduler> onInitComputationHandler;
  static volatile Function<Callable<Scheduler>, Scheduler> onInitIoHandler;
  static volatile Function<Callable<Scheduler>, Scheduler> onInitNewThreadHandler;
  static volatile Function<Callable<Scheduler>, Scheduler> onInitSingleHandler;
  static volatile Function<Scheduler, Scheduler> onIoHandler;
  static volatile Function<Maybe, Maybe> onMaybeAssembly;
  static volatile BiFunction<Maybe, MaybeObserver, MaybeObserver> onMaybeSubscribe;
  static volatile Function<Scheduler, Scheduler> onNewThreadHandler;
  static volatile Function<Observable, Observable> onObservableAssembly;
  static volatile BiFunction<Observable, Observer, Observer> onObservableSubscribe;
  static volatile Function<Runnable, Runnable> onScheduleHandler;
  static volatile Function<Single, Single> onSingleAssembly;
  static volatile Function<Scheduler, Scheduler> onSingleHandler;
  static volatile BiFunction<Single, SingleObserver, SingleObserver> onSingleSubscribe;
  
  private RxJavaPlugins()
  {
    throw new IllegalStateException("No instances!");
  }
  
  static <T, U, R> R apply(BiFunction<T, U, R> paramBiFunction, T paramT, U paramU)
  {
    try
    {
      paramBiFunction = paramBiFunction.apply(paramT, paramU);
      return paramBiFunction;
    }
    catch (Throwable paramBiFunction)
    {
      throw ExceptionHelper.wrapOrThrow(paramBiFunction);
    }
  }
  
  static <T, R> R apply(Function<T, R> paramFunction, T paramT)
  {
    try
    {
      paramFunction = paramFunction.apply(paramT);
      return paramFunction;
    }
    catch (Throwable paramFunction)
    {
      throw ExceptionHelper.wrapOrThrow(paramFunction);
    }
  }
  
  static Scheduler applyRequireNonNull(Function<Callable<Scheduler>, Scheduler> paramFunction, Callable<Scheduler> paramCallable)
  {
    return (Scheduler)ObjectHelper.requireNonNull(apply(paramFunction, paramCallable), "Scheduler Callable result can't be null");
  }
  
  static Scheduler callRequireNonNull(Callable<Scheduler> paramCallable)
  {
    try
    {
      paramCallable = (Scheduler)ObjectHelper.requireNonNull(paramCallable.call(), "Scheduler Callable result can't be null");
      return paramCallable;
    }
    catch (Throwable paramCallable)
    {
      throw ExceptionHelper.wrapOrThrow(paramCallable);
    }
  }
  
  @Experimental
  public static Scheduler createComputationScheduler(ThreadFactory paramThreadFactory)
  {
    return new ComputationScheduler((ThreadFactory)ObjectHelper.requireNonNull(paramThreadFactory, "threadFactory is null"));
  }
  
  @Experimental
  public static Scheduler createIoScheduler(ThreadFactory paramThreadFactory)
  {
    return new IoScheduler((ThreadFactory)ObjectHelper.requireNonNull(paramThreadFactory, "threadFactory is null"));
  }
  
  @Experimental
  public static Scheduler createNewThreadScheduler(ThreadFactory paramThreadFactory)
  {
    return new NewThreadScheduler((ThreadFactory)ObjectHelper.requireNonNull(paramThreadFactory, "threadFactory is null"));
  }
  
  @Experimental
  public static Scheduler createSingleScheduler(ThreadFactory paramThreadFactory)
  {
    return new SingleScheduler((ThreadFactory)ObjectHelper.requireNonNull(paramThreadFactory, "threadFactory is null"));
  }
  
  public static Function<Scheduler, Scheduler> getComputationSchedulerHandler()
  {
    return onComputationHandler;
  }
  
  public static Consumer<Throwable> getErrorHandler()
  {
    return errorHandler;
  }
  
  public static Function<Callable<Scheduler>, Scheduler> getInitComputationSchedulerHandler()
  {
    return onInitComputationHandler;
  }
  
  public static Function<Callable<Scheduler>, Scheduler> getInitIoSchedulerHandler()
  {
    return onInitIoHandler;
  }
  
  public static Function<Callable<Scheduler>, Scheduler> getInitNewThreadSchedulerHandler()
  {
    return onInitNewThreadHandler;
  }
  
  public static Function<Callable<Scheduler>, Scheduler> getInitSingleSchedulerHandler()
  {
    return onInitSingleHandler;
  }
  
  public static Function<Scheduler, Scheduler> getIoSchedulerHandler()
  {
    return onIoHandler;
  }
  
  public static Function<Scheduler, Scheduler> getNewThreadSchedulerHandler()
  {
    return onNewThreadHandler;
  }
  
  @Experimental
  public static BooleanSupplier getOnBeforeBlocking()
  {
    return onBeforeBlocking;
  }
  
  public static Function<Completable, Completable> getOnCompletableAssembly()
  {
    return onCompletableAssembly;
  }
  
  public static BiFunction<Completable, CompletableObserver, CompletableObserver> getOnCompletableSubscribe()
  {
    return onCompletableSubscribe;
  }
  
  public static Function<ConnectableFlowable, ConnectableFlowable> getOnConnectableFlowableAssembly()
  {
    return onConnectableFlowableAssembly;
  }
  
  public static Function<ConnectableObservable, ConnectableObservable> getOnConnectableObservableAssembly()
  {
    return onConnectableObservableAssembly;
  }
  
  public static Function<Flowable, Flowable> getOnFlowableAssembly()
  {
    return onFlowableAssembly;
  }
  
  public static BiFunction<Flowable, Subscriber, Subscriber> getOnFlowableSubscribe()
  {
    return onFlowableSubscribe;
  }
  
  public static Function<Maybe, Maybe> getOnMaybeAssembly()
  {
    return onMaybeAssembly;
  }
  
  public static BiFunction<Maybe, MaybeObserver, MaybeObserver> getOnMaybeSubscribe()
  {
    return onMaybeSubscribe;
  }
  
  public static Function<Observable, Observable> getOnObservableAssembly()
  {
    return onObservableAssembly;
  }
  
  public static BiFunction<Observable, Observer, Observer> getOnObservableSubscribe()
  {
    return onObservableSubscribe;
  }
  
  public static Function<Single, Single> getOnSingleAssembly()
  {
    return onSingleAssembly;
  }
  
  public static BiFunction<Single, SingleObserver, SingleObserver> getOnSingleSubscribe()
  {
    return onSingleSubscribe;
  }
  
  public static Function<Runnable, Runnable> getScheduleHandler()
  {
    return onScheduleHandler;
  }
  
  public static Function<Scheduler, Scheduler> getSingleSchedulerHandler()
  {
    return onSingleHandler;
  }
  
  public static Scheduler initComputationScheduler(Callable<Scheduler> paramCallable)
  {
    ObjectHelper.requireNonNull(paramCallable, "Scheduler Callable can't be null");
    Function localFunction = onInitComputationHandler;
    if (localFunction == null) {
      return callRequireNonNull(paramCallable);
    }
    return applyRequireNonNull(localFunction, paramCallable);
  }
  
  public static Scheduler initIoScheduler(Callable<Scheduler> paramCallable)
  {
    ObjectHelper.requireNonNull(paramCallable, "Scheduler Callable can't be null");
    Function localFunction = onInitIoHandler;
    if (localFunction == null) {
      return callRequireNonNull(paramCallable);
    }
    return applyRequireNonNull(localFunction, paramCallable);
  }
  
  public static Scheduler initNewThreadScheduler(Callable<Scheduler> paramCallable)
  {
    ObjectHelper.requireNonNull(paramCallable, "Scheduler Callable can't be null");
    Function localFunction = onInitNewThreadHandler;
    if (localFunction == null) {
      return callRequireNonNull(paramCallable);
    }
    return applyRequireNonNull(localFunction, paramCallable);
  }
  
  public static Scheduler initSingleScheduler(Callable<Scheduler> paramCallable)
  {
    ObjectHelper.requireNonNull(paramCallable, "Scheduler Callable can't be null");
    Function localFunction = onInitSingleHandler;
    if (localFunction == null) {
      return callRequireNonNull(paramCallable);
    }
    return applyRequireNonNull(localFunction, paramCallable);
  }
  
  @Experimental
  public static boolean isFailOnNonBlockingScheduler()
  {
    return failNonBlockingScheduler;
  }
  
  public static boolean isLockdown()
  {
    return lockdown;
  }
  
  public static void lockdown()
  {
    lockdown = true;
  }
  
  public static Completable onAssembly(Completable paramCompletable)
  {
    Function localFunction = onCompletableAssembly;
    if (localFunction != null) {
      return (Completable)apply(localFunction, paramCompletable);
    }
    return paramCompletable;
  }
  
  public static <T> Flowable<T> onAssembly(Flowable<T> paramFlowable)
  {
    Function localFunction = onFlowableAssembly;
    if (localFunction != null) {
      return (Flowable)apply(localFunction, paramFlowable);
    }
    return paramFlowable;
  }
  
  public static <T> Maybe<T> onAssembly(Maybe<T> paramMaybe)
  {
    Function localFunction = onMaybeAssembly;
    if (localFunction != null) {
      return (Maybe)apply(localFunction, paramMaybe);
    }
    return paramMaybe;
  }
  
  public static <T> Observable<T> onAssembly(Observable<T> paramObservable)
  {
    Function localFunction = onObservableAssembly;
    if (localFunction != null) {
      return (Observable)apply(localFunction, paramObservable);
    }
    return paramObservable;
  }
  
  public static <T> Single<T> onAssembly(Single<T> paramSingle)
  {
    Function localFunction = onSingleAssembly;
    if (localFunction != null) {
      return (Single)apply(localFunction, paramSingle);
    }
    return paramSingle;
  }
  
  public static <T> ConnectableFlowable<T> onAssembly(ConnectableFlowable<T> paramConnectableFlowable)
  {
    Function localFunction = onConnectableFlowableAssembly;
    if (localFunction != null) {
      return (ConnectableFlowable)apply(localFunction, paramConnectableFlowable);
    }
    return paramConnectableFlowable;
  }
  
  public static <T> ConnectableObservable<T> onAssembly(ConnectableObservable<T> paramConnectableObservable)
  {
    Function localFunction = onConnectableObservableAssembly;
    if (localFunction != null) {
      return (ConnectableObservable)apply(localFunction, paramConnectableObservable);
    }
    return paramConnectableObservable;
  }
  
  @Experimental
  public static boolean onBeforeBlocking()
  {
    BooleanSupplier localBooleanSupplier = onBeforeBlocking;
    if (localBooleanSupplier != null) {
      try
      {
        boolean bool = localBooleanSupplier.getAsBoolean();
        return bool;
      }
      catch (Throwable localThrowable)
      {
        throw ExceptionHelper.wrapOrThrow(localThrowable);
      }
    }
    return false;
  }
  
  public static Scheduler onComputationScheduler(Scheduler paramScheduler)
  {
    Function localFunction = onComputationHandler;
    if (localFunction == null) {
      return paramScheduler;
    }
    return (Scheduler)apply(localFunction, paramScheduler);
  }
  
  public static void onError(Throwable paramThrowable)
  {
    Consumer localConsumer = errorHandler;
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    if (localConsumer != null) {
      try
      {
        localConsumer.accept(localObject);
        return;
      }
      catch (Throwable paramThrowable)
      {
        paramThrowable.printStackTrace();
        uncaught(paramThrowable);
      }
    }
    ((Throwable)localObject).printStackTrace();
    uncaught((Throwable)localObject);
  }
  
  public static Scheduler onIoScheduler(Scheduler paramScheduler)
  {
    Function localFunction = onIoHandler;
    if (localFunction == null) {
      return paramScheduler;
    }
    return (Scheduler)apply(localFunction, paramScheduler);
  }
  
  public static Scheduler onNewThreadScheduler(Scheduler paramScheduler)
  {
    Function localFunction = onNewThreadHandler;
    if (localFunction == null) {
      return paramScheduler;
    }
    return (Scheduler)apply(localFunction, paramScheduler);
  }
  
  public static Runnable onSchedule(Runnable paramRunnable)
  {
    Function localFunction = onScheduleHandler;
    if (localFunction == null) {
      return paramRunnable;
    }
    return (Runnable)apply(localFunction, paramRunnable);
  }
  
  public static Scheduler onSingleScheduler(Scheduler paramScheduler)
  {
    Function localFunction = onSingleHandler;
    if (localFunction == null) {
      return paramScheduler;
    }
    return (Scheduler)apply(localFunction, paramScheduler);
  }
  
  public static CompletableObserver onSubscribe(Completable paramCompletable, CompletableObserver paramCompletableObserver)
  {
    BiFunction localBiFunction = onCompletableSubscribe;
    if (localBiFunction != null) {
      return (CompletableObserver)apply(localBiFunction, paramCompletable, paramCompletableObserver);
    }
    return paramCompletableObserver;
  }
  
  public static <T> MaybeObserver<? super T> onSubscribe(Maybe<T> paramMaybe, MaybeObserver<? super T> paramMaybeObserver)
  {
    BiFunction localBiFunction = onMaybeSubscribe;
    if (localBiFunction != null) {
      return (MaybeObserver)apply(localBiFunction, paramMaybe, paramMaybeObserver);
    }
    return paramMaybeObserver;
  }
  
  public static <T> Observer<? super T> onSubscribe(Observable<T> paramObservable, Observer<? super T> paramObserver)
  {
    BiFunction localBiFunction = onObservableSubscribe;
    if (localBiFunction != null) {
      return (Observer)apply(localBiFunction, paramObservable, paramObserver);
    }
    return paramObserver;
  }
  
  public static <T> SingleObserver<? super T> onSubscribe(Single<T> paramSingle, SingleObserver<? super T> paramSingleObserver)
  {
    BiFunction localBiFunction = onSingleSubscribe;
    if (localBiFunction != null) {
      return (SingleObserver)apply(localBiFunction, paramSingle, paramSingleObserver);
    }
    return paramSingleObserver;
  }
  
  public static <T> Subscriber<? super T> onSubscribe(Flowable<T> paramFlowable, Subscriber<? super T> paramSubscriber)
  {
    BiFunction localBiFunction = onFlowableSubscribe;
    if (localBiFunction != null) {
      return (Subscriber)apply(localBiFunction, paramFlowable, paramSubscriber);
    }
    return paramSubscriber;
  }
  
  public static void reset()
  {
    setErrorHandler(null);
    setScheduleHandler(null);
    setComputationSchedulerHandler(null);
    setInitComputationSchedulerHandler(null);
    setIoSchedulerHandler(null);
    setInitIoSchedulerHandler(null);
    setSingleSchedulerHandler(null);
    setInitSingleSchedulerHandler(null);
    setNewThreadSchedulerHandler(null);
    setInitNewThreadSchedulerHandler(null);
    setOnFlowableAssembly(null);
    setOnFlowableSubscribe(null);
    setOnObservableAssembly(null);
    setOnObservableSubscribe(null);
    setOnSingleAssembly(null);
    setOnSingleSubscribe(null);
    setOnCompletableAssembly(null);
    setOnCompletableSubscribe(null);
    setOnConnectableFlowableAssembly(null);
    setOnConnectableObservableAssembly(null);
    setOnMaybeAssembly(null);
    setOnMaybeSubscribe(null);
    setFailOnNonBlockingScheduler(false);
    setOnBeforeBlocking(null);
  }
  
  public static void setComputationSchedulerHandler(Function<Scheduler, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onComputationHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setErrorHandler(Consumer<Throwable> paramConsumer)
  {
    if (!lockdown)
    {
      errorHandler = paramConsumer;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  @Experimental
  public static void setFailOnNonBlockingScheduler(boolean paramBoolean)
  {
    if (!lockdown)
    {
      failNonBlockingScheduler = paramBoolean;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setInitComputationSchedulerHandler(Function<Callable<Scheduler>, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onInitComputationHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setInitIoSchedulerHandler(Function<Callable<Scheduler>, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onInitIoHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setInitNewThreadSchedulerHandler(Function<Callable<Scheduler>, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onInitNewThreadHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setInitSingleSchedulerHandler(Function<Callable<Scheduler>, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onInitSingleHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setIoSchedulerHandler(Function<Scheduler, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onIoHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setNewThreadSchedulerHandler(Function<Scheduler, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onNewThreadHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  @Experimental
  public static void setOnBeforeBlocking(BooleanSupplier paramBooleanSupplier)
  {
    if (!lockdown)
    {
      onBeforeBlocking = paramBooleanSupplier;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnCompletableAssembly(Function<Completable, Completable> paramFunction)
  {
    if (!lockdown)
    {
      onCompletableAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnCompletableSubscribe(BiFunction<Completable, CompletableObserver, CompletableObserver> paramBiFunction)
  {
    if (!lockdown)
    {
      onCompletableSubscribe = paramBiFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnConnectableFlowableAssembly(Function<ConnectableFlowable, ConnectableFlowable> paramFunction)
  {
    if (!lockdown)
    {
      onConnectableFlowableAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnConnectableObservableAssembly(Function<ConnectableObservable, ConnectableObservable> paramFunction)
  {
    if (!lockdown)
    {
      onConnectableObservableAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnFlowableAssembly(Function<Flowable, Flowable> paramFunction)
  {
    if (!lockdown)
    {
      onFlowableAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnFlowableSubscribe(BiFunction<Flowable, Subscriber, Subscriber> paramBiFunction)
  {
    if (!lockdown)
    {
      onFlowableSubscribe = paramBiFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnMaybeAssembly(Function<Maybe, Maybe> paramFunction)
  {
    if (!lockdown)
    {
      onMaybeAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnMaybeSubscribe(BiFunction<Maybe, MaybeObserver, MaybeObserver> paramBiFunction)
  {
    if (!lockdown)
    {
      onMaybeSubscribe = paramBiFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnObservableAssembly(Function<Observable, Observable> paramFunction)
  {
    if (!lockdown)
    {
      onObservableAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnObservableSubscribe(BiFunction<Observable, Observer, Observer> paramBiFunction)
  {
    if (!lockdown)
    {
      onObservableSubscribe = paramBiFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnSingleAssembly(Function<Single, Single> paramFunction)
  {
    if (!lockdown)
    {
      onSingleAssembly = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setOnSingleSubscribe(BiFunction<Single, SingleObserver, SingleObserver> paramBiFunction)
  {
    if (!lockdown)
    {
      onSingleSubscribe = paramBiFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setScheduleHandler(Function<Runnable, Runnable> paramFunction)
  {
    if (!lockdown)
    {
      onScheduleHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  public static void setSingleSchedulerHandler(Function<Scheduler, Scheduler> paramFunction)
  {
    if (!lockdown)
    {
      onSingleHandler = paramFunction;
      return;
    }
    throw new IllegalStateException("Plugins can't be changed anymore");
  }
  
  static void uncaught(Throwable paramThrowable)
  {
    Thread localThread = Thread.currentThread();
    localThread.getUncaughtExceptionHandler().uncaughtException(localThread, paramThrowable);
  }
  
  static void unlock()
  {
    lockdown = false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/plugins/RxJavaPlugins.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */