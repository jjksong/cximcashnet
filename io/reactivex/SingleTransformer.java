package io.reactivex;

public abstract interface SingleTransformer<Upstream, Downstream>
{
  public abstract SingleSource<Downstream> apply(Single<Upstream> paramSingle);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/SingleTransformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */