package io.reactivex;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Cancellable;

public abstract interface CompletableEmitter
{
  public abstract boolean isDisposed();
  
  public abstract void onComplete();
  
  public abstract void onError(Throwable paramThrowable);
  
  public abstract void setCancellable(Cancellable paramCancellable);
  
  public abstract void setDisposable(Disposable paramDisposable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/CompletableEmitter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */