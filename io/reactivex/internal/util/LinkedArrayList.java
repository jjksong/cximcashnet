package io.reactivex.internal.util;

import java.util.ArrayList;
import java.util.List;

public class LinkedArrayList
{
  final int capacityHint;
  Object[] head;
  int indexInTail;
  volatile int size;
  Object[] tail;
  
  public LinkedArrayList(int paramInt)
  {
    this.capacityHint = paramInt;
  }
  
  public void add(Object paramObject)
  {
    Object[] arrayOfObject;
    if (this.size == 0)
    {
      this.head = new Object[this.capacityHint + 1];
      arrayOfObject = this.head;
      this.tail = arrayOfObject;
      arrayOfObject[0] = paramObject;
      this.indexInTail = 1;
      this.size = 1;
    }
    else
    {
      int i = this.indexInTail;
      int j = this.capacityHint;
      if (i == j)
      {
        arrayOfObject = new Object[j + 1];
        arrayOfObject[0] = paramObject;
        this.tail[j] = arrayOfObject;
        this.tail = arrayOfObject;
        this.indexInTail = 1;
        this.size += 1;
      }
      else
      {
        this.tail[i] = paramObject;
        this.indexInTail = (i + 1);
        this.size += 1;
      }
    }
  }
  
  public Object[] head()
  {
    return this.head;
  }
  
  public int size()
  {
    return this.size;
  }
  
  public String toString()
  {
    int n = this.capacityHint;
    int i1 = this.size;
    ArrayList localArrayList = new ArrayList(i1 + 1);
    Object[] arrayOfObject = head();
    int j = 0;
    int i = 0;
    while (j < i1)
    {
      localArrayList.add(arrayOfObject[i]);
      int k = j + 1;
      int m = i + 1;
      j = k;
      i = m;
      if (m == n)
      {
        arrayOfObject = (Object[])arrayOfObject[n];
        i = 0;
        j = k;
      }
    }
    return localArrayList.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/LinkedArrayList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */