package io.reactivex.internal.util;

import io.reactivex.Observer;
import io.reactivex.functions.BiPredicate;
import io.reactivex.functions.Predicate;
import org.reactivestreams.Subscriber;

public class AppendOnlyLinkedArrayList<T>
{
  final int capacity;
  final Object[] head;
  int offset;
  Object[] tail;
  
  public AppendOnlyLinkedArrayList(int paramInt)
  {
    this.capacity = paramInt;
    this.head = new Object[paramInt + 1];
    this.tail = this.head;
  }
  
  public <U> boolean accept(Observer<? super U> paramObserver)
  {
    Object[] arrayOfObject = this.head;
    int j = this.capacity;
    for (;;)
    {
      int i = 0;
      if (arrayOfObject == null) {
        break;
      }
      while (i < j)
      {
        Object localObject = arrayOfObject[i];
        if ((localObject == null) || (NotificationLite.acceptFull(localObject, paramObserver))) {
          break;
        }
        i++;
      }
      arrayOfObject = (Object[])arrayOfObject[j];
    }
    return false;
  }
  
  public <U> boolean accept(Subscriber<? super U> paramSubscriber)
  {
    Object[] arrayOfObject = this.head;
    int j = this.capacity;
    for (;;)
    {
      int i = 0;
      if (arrayOfObject == null) {
        break;
      }
      while (i < j)
      {
        Object localObject = arrayOfObject[i];
        if ((localObject == null) || (NotificationLite.acceptFull(localObject, paramSubscriber))) {
          break;
        }
        i++;
      }
      arrayOfObject = (Object[])arrayOfObject[j];
    }
    return false;
  }
  
  public void add(T paramT)
  {
    int k = this.capacity;
    int j = this.offset;
    int i = j;
    if (j == k)
    {
      Object[] arrayOfObject = new Object[k + 1];
      this.tail[k] = arrayOfObject;
      this.tail = arrayOfObject;
      i = 0;
    }
    this.tail[i] = paramT;
    this.offset = (i + 1);
  }
  
  public void forEachWhile(NonThrowingPredicate<? super T> paramNonThrowingPredicate)
  {
    Object[] arrayOfObject = this.head;
    int j = this.capacity;
    while (arrayOfObject != null)
    {
      for (int i = 0; i < j; i++)
      {
        Object localObject = arrayOfObject[i];
        if ((localObject == null) || (paramNonThrowingPredicate.test(localObject))) {
          break;
        }
      }
      arrayOfObject = (Object[])arrayOfObject[j];
    }
  }
  
  public <S> void forEachWhile(S paramS, BiPredicate<? super S, ? super T> paramBiPredicate)
    throws Exception
  {
    Object[] arrayOfObject = this.head;
    int j = this.capacity;
    for (;;)
    {
      for (int i = 0; i < j; i++)
      {
        Object localObject = arrayOfObject[i];
        if (localObject == null) {
          return;
        }
        if (paramBiPredicate.test(paramS, localObject)) {
          return;
        }
      }
      arrayOfObject = (Object[])arrayOfObject[j];
    }
  }
  
  public void setFirst(T paramT)
  {
    this.head[0] = paramT;
  }
  
  public static abstract interface NonThrowingPredicate<T>
    extends Predicate<T>
  {
    public abstract boolean test(T paramT);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/AppendOnlyLinkedArrayList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */