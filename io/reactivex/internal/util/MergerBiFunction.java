package io.reactivex.internal.util;

import io.reactivex.functions.BiFunction;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public final class MergerBiFunction<T>
  implements BiFunction<List<T>, List<T>, List<T>>
{
  Comparator<? super T> comparator;
  
  public MergerBiFunction(Comparator<? super T> paramComparator)
  {
    this.comparator = paramComparator;
  }
  
  public List<T> apply(List<T> paramList1, List<T> paramList2)
    throws Exception
  {
    int i = paramList1.size() + paramList2.size();
    if (i == 0) {
      return new ArrayList();
    }
    ArrayList localArrayList = new ArrayList(i);
    Iterator localIterator1 = paramList1.iterator();
    Iterator localIterator2 = paramList2.iterator();
    if (localIterator1.hasNext()) {
      paramList2 = localIterator1.next();
    } else {
      paramList2 = null;
    }
    if (localIterator2.hasNext()) {
      paramList1 = localIterator2.next();
    } else {
      paramList1 = null;
    }
    while ((paramList2 != null) && (paramList1 != null)) {
      if (this.comparator.compare(paramList2, paramList1) < 0)
      {
        localArrayList.add(paramList2);
        if (localIterator1.hasNext()) {
          paramList2 = localIterator1.next();
        } else {
          paramList2 = null;
        }
      }
      else
      {
        localArrayList.add(paramList1);
        if (localIterator2.hasNext()) {
          paramList1 = localIterator2.next();
        } else {
          paramList1 = null;
        }
      }
    }
    if (paramList2 != null)
    {
      localArrayList.add(paramList2);
      while (localIterator1.hasNext()) {
        localArrayList.add(localIterator1.next());
      }
    }
    if (paramList1 != null)
    {
      localArrayList.add(paramList1);
      while (localIterator2.hasNext()) {
        localArrayList.add(localIterator2.next());
      }
    }
    return localArrayList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/MergerBiFunction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */