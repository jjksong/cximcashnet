package io.reactivex.internal.util;

import io.reactivex.Observer;

public abstract interface ObservableQueueDrain<T, U>
{
  public abstract void accept(Observer<? super U> paramObserver, T paramT);
  
  public abstract boolean cancelled();
  
  public abstract boolean done();
  
  public abstract boolean enter();
  
  public abstract Throwable error();
  
  public abstract int leave(int paramInt);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/ObservableQueueDrain.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */