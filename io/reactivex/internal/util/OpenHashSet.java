package io.reactivex.internal.util;

public final class OpenHashSet<T>
{
  private static final int INT_PHI = -1640531527;
  T[] keys;
  final float loadFactor;
  int mask;
  int maxSize;
  int size;
  
  public OpenHashSet()
  {
    this(16, 0.75F);
  }
  
  public OpenHashSet(int paramInt)
  {
    this(paramInt, 0.75F);
  }
  
  public OpenHashSet(int paramInt, float paramFloat)
  {
    this.loadFactor = paramFloat;
    paramInt = Pow2.roundToPowerOfTwo(paramInt);
    this.mask = (paramInt - 1);
    this.maxSize = ((int)(paramFloat * paramInt));
    this.keys = ((Object[])new Object[paramInt]);
  }
  
  static int mix(int paramInt)
  {
    paramInt *= -1640531527;
    return paramInt ^ paramInt >>> 16;
  }
  
  public boolean add(T paramT)
  {
    Object[] arrayOfObject = this.keys;
    int k = this.mask;
    int j = mix(paramT.hashCode()) & k;
    Object localObject = arrayOfObject[j];
    int i = j;
    if (localObject != null)
    {
      if (localObject.equals(paramT)) {
        return false;
      }
      do
      {
        i = j + 1 & k;
        localObject = arrayOfObject[i];
        if (localObject == null) {
          break;
        }
        j = i;
      } while (!localObject.equals(paramT));
      return false;
    }
    arrayOfObject[i] = paramT;
    i = this.size + 1;
    this.size = i;
    if (i >= this.maxSize) {
      rehash();
    }
    return true;
  }
  
  public Object[] keys()
  {
    return this.keys;
  }
  
  void rehash()
  {
    Object[] arrayOfObject2 = this.keys;
    int i = arrayOfObject2.length;
    int i1 = i << 1;
    int n = i1 - 1;
    Object[] arrayOfObject1 = (Object[])new Object[i1];
    for (int j = this.size; j != 0; j--)
    {
      do
      {
        i--;
      } while (arrayOfObject2[i] == null);
      int m = mix(arrayOfObject2[i].hashCode()) & n;
      int k = m;
      if (arrayOfObject1[m] != null)
      {
        k = m;
        do
        {
          m = k + 1 & n;
          k = m;
        } while (arrayOfObject1[m] != null);
        k = m;
      }
      arrayOfObject1[k] = arrayOfObject2[i];
    }
    this.mask = n;
    this.maxSize = ((int)(i1 * this.loadFactor));
    this.keys = arrayOfObject1;
  }
  
  public boolean remove(T paramT)
  {
    Object[] arrayOfObject = this.keys;
    int k = this.mask;
    int j = mix(paramT.hashCode()) & k;
    Object localObject = arrayOfObject[j];
    if (localObject == null) {
      return false;
    }
    int i = j;
    if (localObject.equals(paramT)) {
      return removeEntry(j, arrayOfObject, k);
    }
    do
    {
      j = i + 1 & k;
      localObject = arrayOfObject[j];
      if (localObject == null) {
        return false;
      }
      i = j;
    } while (!localObject.equals(paramT));
    return removeEntry(j, arrayOfObject, k);
  }
  
  boolean removeEntry(int paramInt1, T[] paramArrayOfT, int paramInt2)
  {
    this.size -= 1;
    int i = paramInt1;
    for (paramInt1 = i + 1 & paramInt2;; paramInt1 = paramInt1 + 1 & paramInt2)
    {
      T ? = paramArrayOfT[paramInt1];
      if (? == null)
      {
        paramArrayOfT[i] = null;
        return true;
      }
      int j = mix(?.hashCode()) & paramInt2;
      if (i <= paramInt1 ? (i >= j) || (j > paramInt1) : (i >= j) && (j > paramInt1))
      {
        paramArrayOfT[i] = ?;
        i = paramInt1;
        break;
      }
    }
  }
  
  public int size()
  {
    return this.size;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/OpenHashSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */