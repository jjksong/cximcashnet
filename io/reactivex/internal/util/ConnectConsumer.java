package io.reactivex.internal.util;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public final class ConnectConsumer
  implements Consumer<Disposable>
{
  public Disposable disposable;
  
  public void accept(Disposable paramDisposable)
    throws Exception
  {
    this.disposable = paramDisposable;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/ConnectConsumer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */