package io.reactivex.internal.util;

import io.reactivex.functions.Function;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class SorterFunction<T>
  implements Function<List<T>, List<T>>
{
  Comparator<? super T> comparator;
  
  public SorterFunction(Comparator<? super T> paramComparator)
  {
    this.comparator = paramComparator;
  }
  
  public List<T> apply(List<T> paramList)
    throws Exception
  {
    Collections.sort(paramList, this.comparator);
    return paramList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/SorterFunction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */