package io.reactivex.internal.util;

import io.reactivex.functions.BiFunction;
import java.util.List;

public enum ListAddBiConsumer
  implements BiFunction<List, Object, List>
{
  INSTANCE;
  
  private ListAddBiConsumer() {}
  
  public static <T> BiFunction<List<T>, T, List<T>> instance()
  {
    return INSTANCE;
  }
  
  public List apply(List paramList, Object paramObject)
    throws Exception
  {
    paramList.add(paramObject);
    return paramList;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/ListAddBiConsumer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */