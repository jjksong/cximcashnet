package io.reactivex.internal.util;

import io.reactivex.exceptions.CompositeException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class ExceptionHelper
{
  public static final Throwable TERMINATED = new Throwable("No further exceptions")
  {
    private static final long serialVersionUID = -4649703670690200604L;
    
    public Throwable fillInStackTrace()
    {
      return this;
    }
  };
  
  private ExceptionHelper()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T> boolean addThrowable(AtomicReference<Throwable> paramAtomicReference, Throwable paramThrowable)
  {
    Throwable localThrowable;
    Object localObject;
    do
    {
      localThrowable = (Throwable)paramAtomicReference.get();
      if (localThrowable == TERMINATED) {
        return false;
      }
      if (localThrowable == null) {
        localObject = paramThrowable;
      } else {
        localObject = new CompositeException(new Throwable[] { localThrowable, paramThrowable });
      }
    } while (!paramAtomicReference.compareAndSet(localThrowable, localObject));
    return true;
  }
  
  public static List<Throwable> flatten(Throwable paramThrowable)
  {
    ArrayList localArrayList = new ArrayList();
    ArrayDeque localArrayDeque = new ArrayDeque();
    localArrayDeque.offer(paramThrowable);
    while (!localArrayDeque.isEmpty())
    {
      paramThrowable = (Throwable)localArrayDeque.removeFirst();
      if ((paramThrowable instanceof CompositeException))
      {
        paramThrowable = ((CompositeException)paramThrowable).getExceptions();
        for (int i = paramThrowable.size() - 1; i >= 0; i--) {
          localArrayDeque.offerFirst(paramThrowable.get(i));
        }
      }
      else
      {
        localArrayList.add(paramThrowable);
      }
    }
    return localArrayList;
  }
  
  public static <T> Throwable terminate(AtomicReference<Throwable> paramAtomicReference)
  {
    Throwable localThrowable2 = (Throwable)paramAtomicReference.get();
    Throwable localThrowable3 = TERMINATED;
    Throwable localThrowable1 = localThrowable2;
    if (localThrowable2 != localThrowable3) {
      localThrowable1 = (Throwable)paramAtomicReference.getAndSet(localThrowable3);
    }
    return localThrowable1;
  }
  
  public static RuntimeException wrapOrThrow(Throwable paramThrowable)
  {
    if (!(paramThrowable instanceof Error))
    {
      if ((paramThrowable instanceof RuntimeException)) {
        return (RuntimeException)paramThrowable;
      }
      return new RuntimeException(paramThrowable);
    }
    throw ((Error)paramThrowable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/ExceptionHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */