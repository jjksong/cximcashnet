package io.reactivex.internal.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public enum HashMapSupplier
  implements Callable<Map<Object, Object>>
{
  INSTANCE;
  
  private HashMapSupplier() {}
  
  public static <K, V> Callable<Map<K, V>> asCallable()
  {
    return INSTANCE;
  }
  
  public Map<Object, Object> call()
    throws Exception
  {
    return new HashMap();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/util/HashMapSupplier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */