package io.reactivex.internal.subscriptions;

import java.util.concurrent.atomic.AtomicLong;

class FullArbiterMissed
  extends FullArbiterPad1
{
  final AtomicLong missedRequested = new AtomicLong();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscriptions/FullArbiterMissed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */