package io.reactivex.internal.subscriptions;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FullArbiter<T>
  extends FullArbiterPad2
  implements Subscription
{
  static final Subscription INITIAL = new Subscription()
  {
    public void cancel() {}
    
    public void request(long paramAnonymousLong) {}
  };
  static final Object REQUEST = new Object();
  final Subscriber<? super T> actual;
  volatile boolean cancelled;
  final SpscLinkedArrayQueue<Object> queue;
  long requested;
  Disposable resource;
  volatile Subscription s;
  
  public FullArbiter(Subscriber<? super T> paramSubscriber, Disposable paramDisposable, int paramInt)
  {
    this.actual = paramSubscriber;
    this.resource = paramDisposable;
    this.queue = new SpscLinkedArrayQueue(paramInt);
    this.s = INITIAL;
  }
  
  public void cancel()
  {
    if (!this.cancelled)
    {
      this.cancelled = true;
      dispose();
    }
  }
  
  void dispose()
  {
    Disposable localDisposable = this.resource;
    this.resource = null;
    if (localDisposable != null) {
      localDisposable.dispose();
    }
  }
  
  void drain()
  {
    if (this.wip.getAndIncrement() != 0) {
      return;
    }
    SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
    Subscriber localSubscriber = this.actual;
    int i = 1;
    for (;;)
    {
      Object localObject2 = localSpscLinkedArrayQueue.poll();
      if (localObject2 == null)
      {
        int j = this.wip.addAndGet(-i);
        i = j;
        if (j != 0) {}
      }
      else
      {
        Object localObject1 = localSpscLinkedArrayQueue.poll();
        long l;
        if (localObject2 == REQUEST)
        {
          l = this.missedRequested.getAndSet(0L);
          if (l != 0L)
          {
            this.requested = BackpressureHelper.addCap(this.requested, l);
            this.s.request(l);
          }
        }
        else if (localObject2 == this.s)
        {
          if (NotificationLite.isSubscription(localObject1))
          {
            localObject1 = NotificationLite.getSubscription(localObject1);
            if (!this.cancelled)
            {
              this.s = ((Subscription)localObject1);
              l = this.requested;
              if (l != 0L) {
                ((Subscription)localObject1).request(l);
              }
            }
            else
            {
              ((Subscription)localObject1).cancel();
            }
          }
          else if (NotificationLite.isError(localObject1))
          {
            localSpscLinkedArrayQueue.clear();
            dispose();
            localObject1 = NotificationLite.getError(localObject1);
            if (!this.cancelled)
            {
              this.cancelled = true;
              localSubscriber.onError((Throwable)localObject1);
            }
            else
            {
              RxJavaPlugins.onError((Throwable)localObject1);
            }
          }
          else if (NotificationLite.isComplete(localObject1))
          {
            localSpscLinkedArrayQueue.clear();
            dispose();
            if (!this.cancelled)
            {
              this.cancelled = true;
              localSubscriber.onComplete();
            }
          }
          else
          {
            l = this.requested;
            if (l != 0L)
            {
              localSubscriber.onNext(NotificationLite.getValue(localObject1));
              this.requested = (l - 1L);
            }
          }
        }
      }
    }
  }
  
  public void onComplete(Subscription paramSubscription)
  {
    this.queue.offer(paramSubscription, NotificationLite.complete());
    drain();
  }
  
  public void onError(Throwable paramThrowable, Subscription paramSubscription)
  {
    if (this.cancelled)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    this.queue.offer(paramSubscription, NotificationLite.error(paramThrowable));
    drain();
  }
  
  public boolean onNext(T paramT, Subscription paramSubscription)
  {
    if (this.cancelled) {
      return false;
    }
    this.queue.offer(paramSubscription, NotificationLite.next(paramT));
    drain();
    return true;
  }
  
  public void request(long paramLong)
  {
    if (SubscriptionHelper.validate(paramLong))
    {
      BackpressureHelper.add(this.missedRequested, paramLong);
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Object localObject = REQUEST;
      localSpscLinkedArrayQueue.offer(localObject, localObject);
      drain();
    }
  }
  
  public boolean setSubscription(Subscription paramSubscription)
  {
    if (this.cancelled)
    {
      if (paramSubscription != null) {
        paramSubscription.cancel();
      }
      return false;
    }
    ObjectHelper.requireNonNull(paramSubscription, "s is null");
    this.queue.offer(this.s, NotificationLite.subscription(paramSubscription));
    drain();
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscriptions/FullArbiter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */