package io.reactivex.internal.subscriptions;

import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscription;

public class SubscriptionArbiter
  extends AtomicInteger
  implements Subscription
{
  private static final long serialVersionUID = -2189523197179400958L;
  Subscription actual;
  volatile boolean cancelled;
  final AtomicLong missedProduced = new AtomicLong();
  final AtomicLong missedRequested = new AtomicLong();
  final AtomicReference<Subscription> missedSubscription = new AtomicReference();
  long requested;
  protected boolean unbounded;
  
  public void cancel()
  {
    if (!this.cancelled)
    {
      this.cancelled = true;
      drain();
    }
  }
  
  final void drain()
  {
    if (getAndIncrement() != 0) {
      return;
    }
    drainLoop();
  }
  
  final void drainLoop()
  {
    Object localObject3 = null;
    long l2 = 0L;
    int i = 1;
    Object localObject2;
    long l1;
    int j;
    do
    {
      localObject2 = (Subscription)this.missedSubscription.get();
      Object localObject1 = localObject2;
      if (localObject2 != null) {
        localObject1 = (Subscription)this.missedSubscription.getAndSet(null);
      }
      l1 = this.missedRequested.get();
      long l3 = l1;
      if (l1 != 0L) {
        l3 = this.missedRequested.getAndSet(0L);
      }
      l1 = this.missedProduced.get();
      long l4 = l1;
      if (l1 != 0L) {
        l4 = this.missedProduced.getAndSet(0L);
      }
      Subscription localSubscription = this.actual;
      if (this.cancelled)
      {
        if (localSubscription != null)
        {
          localSubscription.cancel();
          this.actual = null;
        }
        l1 = l2;
        localObject2 = localObject3;
        if (localObject1 != null)
        {
          ((Subscription)localObject1).cancel();
          l1 = l2;
          localObject2 = localObject3;
        }
      }
      else
      {
        l1 = this.requested;
        long l5 = l1;
        if (l1 != Long.MAX_VALUE)
        {
          l5 = BackpressureHelper.addCap(l1, l3);
          l1 = l5;
          if (l5 != Long.MAX_VALUE)
          {
            l4 = l5 - l4;
            l1 = l4;
            if (l4 < 0L)
            {
              SubscriptionHelper.reportMoreProduced(l4);
              l1 = 0L;
            }
          }
          this.requested = l1;
          l5 = l1;
        }
        if (localObject1 != null)
        {
          if (localSubscription != null) {
            localSubscription.cancel();
          }
          this.actual = ((Subscription)localObject1);
          l1 = l2;
          localObject2 = localObject3;
          if (l5 != 0L)
          {
            l1 = BackpressureHelper.addCap(l2, l5);
            localObject2 = localObject1;
          }
        }
        else
        {
          l1 = l2;
          localObject2 = localObject3;
          if (localSubscription != null)
          {
            l1 = l2;
            localObject2 = localObject3;
            if (l3 != 0L)
            {
              l1 = BackpressureHelper.addCap(l2, l3);
              localObject2 = localSubscription;
            }
          }
        }
      }
      j = addAndGet(-i);
      l2 = l1;
      i = j;
      localObject3 = localObject2;
    } while (j != 0);
    if (l1 != 0L) {
      ((Subscription)localObject2).request(l1);
    }
  }
  
  public final boolean isCancelled()
  {
    return this.cancelled;
  }
  
  public final boolean isUnbounded()
  {
    return this.unbounded;
  }
  
  public final void produced(long paramLong)
  {
    if (this.unbounded) {
      return;
    }
    if ((get() == 0) && (compareAndSet(0, 1)))
    {
      long l = this.requested;
      if (l != Long.MAX_VALUE)
      {
        l -= paramLong;
        paramLong = 0L;
        if (l < 0L) {
          SubscriptionHelper.reportMoreProduced(l);
        } else {
          paramLong = l;
        }
        this.requested = paramLong;
      }
      if (decrementAndGet() == 0) {
        return;
      }
      drainLoop();
      return;
    }
    BackpressureHelper.add(this.missedProduced, paramLong);
    drain();
  }
  
  public final void request(long paramLong)
  {
    if (SubscriptionHelper.validate(paramLong))
    {
      if (this.unbounded) {
        return;
      }
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        long l = this.requested;
        if (l != Long.MAX_VALUE)
        {
          l = BackpressureHelper.addCap(l, paramLong);
          this.requested = l;
          if (l == Long.MAX_VALUE) {
            this.unbounded = true;
          }
        }
        Subscription localSubscription = this.actual;
        if (decrementAndGet() != 0) {
          drainLoop();
        }
        if (localSubscription != null) {
          localSubscription.request(paramLong);
        }
        return;
      }
      BackpressureHelper.add(this.missedRequested, paramLong);
      drain();
    }
  }
  
  public final void setSubscription(Subscription paramSubscription)
  {
    if (this.cancelled)
    {
      paramSubscription.cancel();
      return;
    }
    ObjectHelper.requireNonNull(paramSubscription, "s is null");
    if ((get() == 0) && (compareAndSet(0, 1)))
    {
      Subscription localSubscription = this.actual;
      if (localSubscription != null) {
        localSubscription.cancel();
      }
      this.actual = paramSubscription;
      long l = this.requested;
      if (decrementAndGet() != 0) {
        drainLoop();
      }
      if (l != 0L) {
        paramSubscription.request(l);
      }
      return;
    }
    paramSubscription = (Subscription)this.missedSubscription.getAndSet(paramSubscription);
    if (paramSubscription != null) {
      paramSubscription.cancel();
    }
    drain();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscriptions/SubscriptionArbiter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */