package io.reactivex.internal.fuseable;

import io.reactivex.ObservableSource;

public abstract interface HasUpstreamObservableSource<T>
{
  public abstract ObservableSource<T> source();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/HasUpstreamObservableSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */