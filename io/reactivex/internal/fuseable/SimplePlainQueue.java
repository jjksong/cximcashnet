package io.reactivex.internal.fuseable;

public abstract interface SimplePlainQueue<T>
  extends SimpleQueue<T>
{
  public abstract T poll();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/SimplePlainQueue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */