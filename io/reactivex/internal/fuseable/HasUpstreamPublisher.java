package io.reactivex.internal.fuseable;

import org.reactivestreams.Publisher;

public abstract interface HasUpstreamPublisher<T>
{
  public abstract Publisher<T> source();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/HasUpstreamPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */