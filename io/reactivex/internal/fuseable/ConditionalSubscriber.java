package io.reactivex.internal.fuseable;

import org.reactivestreams.Subscriber;

public abstract interface ConditionalSubscriber<T>
  extends Subscriber<T>
{
  public abstract boolean tryOnNext(T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/ConditionalSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */