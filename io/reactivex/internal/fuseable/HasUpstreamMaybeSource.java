package io.reactivex.internal.fuseable;

import io.reactivex.MaybeSource;

public abstract interface HasUpstreamMaybeSource<T>
{
  public abstract MaybeSource<T> source();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/HasUpstreamMaybeSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */