package io.reactivex.internal.fuseable;

import java.util.concurrent.Callable;

public abstract interface ScalarCallable<T>
  extends Callable<T>
{
  public abstract T call();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/ScalarCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */