package io.reactivex.internal.fuseable;

import io.reactivex.SingleSource;

public abstract interface HasUpstreamSingleSource<T>
{
  public abstract SingleSource<T> source();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/HasUpstreamSingleSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */