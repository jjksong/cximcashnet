package io.reactivex.internal.fuseable;

import io.reactivex.Maybe;

public abstract interface FuseToMaybe<T>
{
  public abstract Maybe<T> fuseToMaybe();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/FuseToMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */