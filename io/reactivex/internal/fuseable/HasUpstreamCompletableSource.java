package io.reactivex.internal.fuseable;

import io.reactivex.CompletableSource;

public abstract interface HasUpstreamCompletableSource
{
  public abstract CompletableSource source();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/HasUpstreamCompletableSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */