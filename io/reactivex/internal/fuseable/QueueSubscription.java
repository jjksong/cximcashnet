package io.reactivex.internal.fuseable;

import org.reactivestreams.Subscription;

public abstract interface QueueSubscription<T>
  extends QueueFuseable<T>, Subscription
{}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/QueueSubscription.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */