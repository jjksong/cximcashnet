package io.reactivex.internal.fuseable;

import io.reactivex.Flowable;

public abstract interface FuseToFlowable<T>
{
  public abstract Flowable<T> fuseToFlowable();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/FuseToFlowable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */