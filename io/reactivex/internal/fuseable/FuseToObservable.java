package io.reactivex.internal.fuseable;

import io.reactivex.Observable;

public abstract interface FuseToObservable<T>
{
  public abstract Observable<T> fuseToObservable();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/fuseable/FuseToObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */