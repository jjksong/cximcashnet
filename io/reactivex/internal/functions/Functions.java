package io.reactivex.internal.functions;

import io.reactivex.Notification;
import io.reactivex.Scheduler;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Function4;
import io.reactivex.functions.Function5;
import io.reactivex.functions.Function6;
import io.reactivex.functions.Function7;
import io.reactivex.functions.Function8;
import io.reactivex.functions.Function9;
import io.reactivex.functions.LongConsumer;
import io.reactivex.functions.Predicate;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Timed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Subscription;

public final class Functions
{
  static final Predicate<Object> ALWAYS_FALSE;
  static final Predicate<Object> ALWAYS_TRUE;
  public static final Action EMPTY_ACTION;
  static final Consumer<Object> EMPTY_CONSUMER;
  public static final LongConsumer EMPTY_LONG_CONSUMER;
  public static final Runnable EMPTY_RUNNABLE;
  public static final Consumer<Throwable> ERROR_CONSUMER;
  static final Function<Object, Object> IDENTITY = new Function()
  {
    public Object apply(Object paramAnonymousObject)
    {
      return paramAnonymousObject;
    }
    
    public String toString()
    {
      return "IdentityFunction";
    }
  };
  static final Comparator<Object> NATURAL_COMPARATOR = new Comparator()
  {
    public int compare(Object paramAnonymousObject1, Object paramAnonymousObject2)
    {
      return ((Comparable)paramAnonymousObject1).compareTo(paramAnonymousObject2);
    }
  };
  static final Callable<Object> NULL_SUPPLIER;
  public static final Consumer<Subscription> REQUEST_MAX = new Consumer()
  {
    public void accept(Subscription paramAnonymousSubscription)
      throws Exception
    {
      paramAnonymousSubscription.request(Long.MAX_VALUE);
    }
  };
  
  static
  {
    EMPTY_RUNNABLE = new Runnable()
    {
      public void run() {}
      
      public String toString()
      {
        return "EmptyRunnable";
      }
    };
    EMPTY_ACTION = new Action()
    {
      public void run() {}
      
      public String toString()
      {
        return "EmptyAction";
      }
    };
    EMPTY_CONSUMER = new Consumer()
    {
      public void accept(Object paramAnonymousObject) {}
      
      public String toString()
      {
        return "EmptyConsumer";
      }
    };
    ERROR_CONSUMER = new Consumer()
    {
      public void accept(Throwable paramAnonymousThrowable)
      {
        RxJavaPlugins.onError(paramAnonymousThrowable);
      }
    };
    EMPTY_LONG_CONSUMER = new LongConsumer()
    {
      public void accept(long paramAnonymousLong) {}
    };
    ALWAYS_TRUE = new Predicate()
    {
      public boolean test(Object paramAnonymousObject)
      {
        return true;
      }
    };
    ALWAYS_FALSE = new Predicate()
    {
      public boolean test(Object paramAnonymousObject)
      {
        return false;
      }
    };
    NULL_SUPPLIER = new Callable()
    {
      public Object call()
      {
        return null;
      }
    };
  }
  
  private Functions()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T> Consumer<T> actionConsumer(Action paramAction)
  {
    return new ActionConsumer(paramAction);
  }
  
  public static <T> Predicate<T> alwaysFalse()
  {
    return ALWAYS_FALSE;
  }
  
  public static <T> Predicate<T> alwaysTrue()
  {
    return ALWAYS_TRUE;
  }
  
  public static <T, U> Function<T, U> castFunction(Class<U> paramClass)
  {
    return new CastToClass(paramClass);
  }
  
  public static <T> Callable<List<T>> createArrayList(int paramInt)
  {
    return new ArrayListCapacityCallable(paramInt);
  }
  
  public static <T> Callable<Set<T>> createHashSet()
  {
    return HashSetCallable.INSTANCE;
  }
  
  public static <T> Consumer<T> emptyConsumer()
  {
    return EMPTY_CONSUMER;
  }
  
  public static <T> Predicate<T> equalsWith(T paramT)
  {
    return new EqualsPredicate(paramT);
  }
  
  public static Action futureAction(Future<?> paramFuture)
  {
    return new FutureAction(paramFuture);
  }
  
  public static <T> Function<T, T> identity()
  {
    return IDENTITY;
  }
  
  public static <T, U> Predicate<T> isInstanceOf(Class<U> paramClass)
  {
    return new ClassFilter(paramClass);
  }
  
  public static <T> Callable<T> justCallable(T paramT)
  {
    return new JustValue(paramT);
  }
  
  public static <T, U> Function<T, U> justFunction(U paramU)
  {
    return new JustValue(paramU);
  }
  
  public static <T> Function<List<T>, List<T>> listSorter(Comparator<? super T> paramComparator)
  {
    return new ListSorter(paramComparator);
  }
  
  public static <T> Comparator<T> naturalComparator()
  {
    return NaturalComparator.INSTANCE;
  }
  
  public static <T> Comparator<T> naturalOrder()
  {
    return NATURAL_COMPARATOR;
  }
  
  public static <T> Action notificationOnComplete(Consumer<? super Notification<T>> paramConsumer)
  {
    return new NotificationOnComplete(paramConsumer);
  }
  
  public static <T> Consumer<Throwable> notificationOnError(Consumer<? super Notification<T>> paramConsumer)
  {
    return new NotificationOnError(paramConsumer);
  }
  
  public static <T> Consumer<T> notificationOnNext(Consumer<? super Notification<T>> paramConsumer)
  {
    return new NotificationOnNext(paramConsumer);
  }
  
  public static <T> Callable<T> nullSupplier()
  {
    return NULL_SUPPLIER;
  }
  
  public static <T> Predicate<T> predicateReverseFor(BooleanSupplier paramBooleanSupplier)
  {
    return new BooleanSupplierPredicateReverse(paramBooleanSupplier);
  }
  
  public static <T> Function<T, Timed<T>> timestampWith(TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    return new TimestampFunction(paramTimeUnit, paramScheduler);
  }
  
  public static <T1, T2, R> Function<Object[], R> toFunction(BiFunction<? super T1, ? super T2, ? extends R> paramBiFunction)
  {
    ObjectHelper.requireNonNull(paramBiFunction, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 2) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 2 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, R> Function<Object[], R> toFunction(Function3<T1, T2, T3, R> paramFunction3)
  {
    ObjectHelper.requireNonNull(paramFunction3, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 3) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 3 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, R> Function<Object[], R> toFunction(Function4<T1, T2, T3, T4, R> paramFunction4)
  {
    ObjectHelper.requireNonNull(paramFunction4, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 4) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 4 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, T5, R> Function<Object[], R> toFunction(Function5<T1, T2, T3, T4, T5, R> paramFunction5)
  {
    ObjectHelper.requireNonNull(paramFunction5, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 5) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3], paramAnonymousArrayOfObject[4]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 5 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, T5, T6, R> Function<Object[], R> toFunction(Function6<T1, T2, T3, T4, T5, T6, R> paramFunction6)
  {
    ObjectHelper.requireNonNull(paramFunction6, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 6) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3], paramAnonymousArrayOfObject[4], paramAnonymousArrayOfObject[5]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 6 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, T5, T6, T7, R> Function<Object[], R> toFunction(Function7<T1, T2, T3, T4, T5, T6, T7, R> paramFunction7)
  {
    ObjectHelper.requireNonNull(paramFunction7, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 7) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3], paramAnonymousArrayOfObject[4], paramAnonymousArrayOfObject[5], paramAnonymousArrayOfObject[6]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 7 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, T5, T6, T7, T8, R> Function<Object[], R> toFunction(Function8<T1, T2, T3, T4, T5, T6, T7, T8, R> paramFunction8)
  {
    ObjectHelper.requireNonNull(paramFunction8, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 8) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3], paramAnonymousArrayOfObject[4], paramAnonymousArrayOfObject[5], paramAnonymousArrayOfObject[6], paramAnonymousArrayOfObject[7]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 8 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T1, T2, T3, T4, T5, T6, T7, T8, T9, R> Function<Object[], R> toFunction(Function9<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> paramFunction9)
  {
    ObjectHelper.requireNonNull(paramFunction9, "f is null");
    new Function()
    {
      public R apply(Object[] paramAnonymousArrayOfObject)
        throws Exception
      {
        if (paramAnonymousArrayOfObject.length == 9) {
          return (R)this.val$f.apply(paramAnonymousArrayOfObject[0], paramAnonymousArrayOfObject[1], paramAnonymousArrayOfObject[2], paramAnonymousArrayOfObject[3], paramAnonymousArrayOfObject[4], paramAnonymousArrayOfObject[5], paramAnonymousArrayOfObject[6], paramAnonymousArrayOfObject[7], paramAnonymousArrayOfObject[8]);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Array of size 9 expected but got ");
        localStringBuilder.append(paramAnonymousArrayOfObject.length);
        throw new IllegalArgumentException(localStringBuilder.toString());
      }
    };
  }
  
  public static <T, K> BiConsumer<Map<K, T>, T> toMapKeySelector(Function<? super T, ? extends K> paramFunction)
  {
    return new ToMapKeySelector(paramFunction);
  }
  
  public static <T, K, V> BiConsumer<Map<K, V>, T> toMapKeyValueSelector(Function<? super T, ? extends K> paramFunction, Function<? super T, ? extends V> paramFunction1)
  {
    return new ToMapKeyValueSelector(paramFunction1, paramFunction);
  }
  
  public static <T, K, V> BiConsumer<Map<K, Collection<V>>, T> toMultimapKeyValueSelector(Function<? super T, ? extends K> paramFunction, Function<? super T, ? extends V> paramFunction1, Function<? super K, ? extends Collection<? super V>> paramFunction2)
  {
    return new ToMultimapKeyValueSelector(paramFunction2, paramFunction1, paramFunction);
  }
  
  static final class ActionConsumer<T>
    implements Consumer<T>
  {
    final Action action;
    
    ActionConsumer(Action paramAction)
    {
      this.action = paramAction;
    }
    
    public void accept(T paramT)
      throws Exception
    {
      this.action.run();
    }
  }
  
  static final class ArrayListCapacityCallable<T>
    implements Callable<List<T>>
  {
    final int capacity;
    
    ArrayListCapacityCallable(int paramInt)
    {
      this.capacity = paramInt;
    }
    
    public List<T> call()
      throws Exception
    {
      return new ArrayList(this.capacity);
    }
  }
  
  static final class BooleanSupplierPredicateReverse<T>
    implements Predicate<T>
  {
    final BooleanSupplier supplier;
    
    BooleanSupplierPredicateReverse(BooleanSupplier paramBooleanSupplier)
    {
      this.supplier = paramBooleanSupplier;
    }
    
    public boolean test(T paramT)
      throws Exception
    {
      return this.supplier.getAsBoolean() ^ true;
    }
  }
  
  static final class CastToClass<T, U>
    implements Function<T, U>
  {
    final Class<U> clazz;
    
    CastToClass(Class<U> paramClass)
    {
      this.clazz = paramClass;
    }
    
    public U apply(T paramT)
      throws Exception
    {
      return (U)this.clazz.cast(paramT);
    }
  }
  
  static final class ClassFilter<T, U>
    implements Predicate<T>
  {
    final Class<U> clazz;
    
    ClassFilter(Class<U> paramClass)
    {
      this.clazz = paramClass;
    }
    
    public boolean test(T paramT)
      throws Exception
    {
      return this.clazz.isInstance(paramT);
    }
  }
  
  static final class EqualsPredicate<T>
    implements Predicate<T>
  {
    final T value;
    
    EqualsPredicate(T paramT)
    {
      this.value = paramT;
    }
    
    public boolean test(T paramT)
      throws Exception
    {
      return ObjectHelper.equals(paramT, this.value);
    }
  }
  
  static final class FutureAction
    implements Action
  {
    final Future<?> future;
    
    FutureAction(Future<?> paramFuture)
    {
      this.future = paramFuture;
    }
    
    public void run()
      throws Exception
    {
      this.future.get();
    }
  }
  
  static enum HashSetCallable
    implements Callable<Set<Object>>
  {
    INSTANCE;
    
    private HashSetCallable() {}
    
    public Set<Object> call()
      throws Exception
    {
      return new HashSet();
    }
  }
  
  static final class JustValue<T, U>
    implements Callable<U>, Function<T, U>
  {
    final U value;
    
    JustValue(U paramU)
    {
      this.value = paramU;
    }
    
    public U apply(T paramT)
      throws Exception
    {
      return (U)this.value;
    }
    
    public U call()
      throws Exception
    {
      return (U)this.value;
    }
  }
  
  static final class ListSorter<T>
    implements Function<List<T>, List<T>>
  {
    private final Comparator<? super T> comparator;
    
    ListSorter(Comparator<? super T> paramComparator)
    {
      this.comparator = paramComparator;
    }
    
    public List<T> apply(List<T> paramList)
    {
      Collections.sort(paramList, this.comparator);
      return paramList;
    }
  }
  
  static enum NaturalComparator
    implements Comparator<Object>
  {
    INSTANCE;
    
    private NaturalComparator() {}
    
    public int compare(Object paramObject1, Object paramObject2)
    {
      return ((Comparable)paramObject1).compareTo(paramObject2);
    }
  }
  
  static final class NotificationOnComplete<T>
    implements Action
  {
    final Consumer<? super Notification<T>> onNotification;
    
    NotificationOnComplete(Consumer<? super Notification<T>> paramConsumer)
    {
      this.onNotification = paramConsumer;
    }
    
    public void run()
      throws Exception
    {
      this.onNotification.accept(Notification.createOnComplete());
    }
  }
  
  static final class NotificationOnError<T>
    implements Consumer<Throwable>
  {
    final Consumer<? super Notification<T>> onNotification;
    
    NotificationOnError(Consumer<? super Notification<T>> paramConsumer)
    {
      this.onNotification = paramConsumer;
    }
    
    public void accept(Throwable paramThrowable)
      throws Exception
    {
      this.onNotification.accept(Notification.createOnError(paramThrowable));
    }
  }
  
  static final class NotificationOnNext<T>
    implements Consumer<T>
  {
    final Consumer<? super Notification<T>> onNotification;
    
    NotificationOnNext(Consumer<? super Notification<T>> paramConsumer)
    {
      this.onNotification = paramConsumer;
    }
    
    public void accept(T paramT)
      throws Exception
    {
      this.onNotification.accept(Notification.createOnNext(paramT));
    }
  }
  
  static final class TimestampFunction<T>
    implements Function<T, Timed<T>>
  {
    final Scheduler scheduler;
    final TimeUnit unit;
    
    TimestampFunction(TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
    }
    
    public Timed<T> apply(T paramT)
      throws Exception
    {
      return new Timed(paramT, this.scheduler.now(this.unit), this.unit);
    }
  }
  
  static final class ToMapKeySelector<K, T>
    implements BiConsumer<Map<K, T>, T>
  {
    private final Function<? super T, ? extends K> keySelector;
    
    ToMapKeySelector(Function<? super T, ? extends K> paramFunction)
    {
      this.keySelector = paramFunction;
    }
    
    public void accept(Map<K, T> paramMap, T paramT)
      throws Exception
    {
      paramMap.put(this.keySelector.apply(paramT), paramT);
    }
  }
  
  static final class ToMapKeyValueSelector<K, V, T>
    implements BiConsumer<Map<K, V>, T>
  {
    private final Function<? super T, ? extends K> keySelector;
    private final Function<? super T, ? extends V> valueSelector;
    
    ToMapKeyValueSelector(Function<? super T, ? extends V> paramFunction, Function<? super T, ? extends K> paramFunction1)
    {
      this.valueSelector = paramFunction;
      this.keySelector = paramFunction1;
    }
    
    public void accept(Map<K, V> paramMap, T paramT)
      throws Exception
    {
      paramMap.put(this.keySelector.apply(paramT), this.valueSelector.apply(paramT));
    }
  }
  
  static final class ToMultimapKeyValueSelector<K, V, T>
    implements BiConsumer<Map<K, Collection<V>>, T>
  {
    private final Function<? super K, ? extends Collection<? super V>> collectionFactory;
    private final Function<? super T, ? extends K> keySelector;
    private final Function<? super T, ? extends V> valueSelector;
    
    ToMultimapKeyValueSelector(Function<? super K, ? extends Collection<? super V>> paramFunction, Function<? super T, ? extends V> paramFunction1, Function<? super T, ? extends K> paramFunction2)
    {
      this.collectionFactory = paramFunction;
      this.valueSelector = paramFunction1;
      this.keySelector = paramFunction2;
    }
    
    public void accept(Map<K, Collection<V>> paramMap, T paramT)
      throws Exception
    {
      Object localObject = this.keySelector.apply(paramT);
      Collection localCollection2 = (Collection)paramMap.get(localObject);
      Collection localCollection1 = localCollection2;
      if (localCollection2 == null)
      {
        localCollection1 = (Collection)this.collectionFactory.apply(localObject);
        paramMap.put(localObject, localCollection1);
      }
      localCollection1.add(this.valueSelector.apply(paramT));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/functions/Functions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */