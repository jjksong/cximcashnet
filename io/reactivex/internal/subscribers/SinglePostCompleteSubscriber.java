package io.reactivex.internal.subscribers;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract class SinglePostCompleteSubscriber<T, R>
  extends AtomicLong
  implements Subscriber<T>, Subscription
{
  static final long COMPLETE_MASK = Long.MIN_VALUE;
  static final long REQUEST_MASK = Long.MAX_VALUE;
  private static final long serialVersionUID = 7917814472626990048L;
  protected final Subscriber<? super R> actual;
  protected long produced;
  protected Subscription s;
  protected R value;
  
  public SinglePostCompleteSubscriber(Subscriber<? super R> paramSubscriber)
  {
    this.actual = paramSubscriber;
  }
  
  public void cancel()
  {
    this.s.cancel();
  }
  
  protected final void complete(R paramR)
  {
    long l = this.produced;
    if (l != 0L) {
      BackpressureHelper.produced(this, l);
    }
    for (;;)
    {
      l = get();
      if ((l & 0x8000000000000000) != 0L)
      {
        onDrop(paramR);
        return;
      }
      if ((l & 0x7FFFFFFFFFFFFFFF) != 0L)
      {
        lazySet(-9223372036854775807L);
        this.actual.onNext(paramR);
        this.actual.onComplete();
        return;
      }
      this.value = paramR;
      if (compareAndSet(0L, Long.MIN_VALUE)) {
        return;
      }
      this.value = null;
    }
  }
  
  protected void onDrop(R paramR) {}
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.s, paramSubscription))
    {
      this.s = paramSubscription;
      this.actual.onSubscribe(this);
    }
  }
  
  public final void request(long paramLong)
  {
    if (SubscriptionHelper.validate(paramLong))
    {
      long l;
      do
      {
        l = get();
        if ((l & 0x8000000000000000) != 0L)
        {
          if (!compareAndSet(Long.MIN_VALUE, -9223372036854775807L)) {
            break;
          }
          this.actual.onNext(this.value);
          this.actual.onComplete();
          break;
        }
      } while (!compareAndSet(l, BackpressureHelper.addCap(l, paramLong)));
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/SinglePostCompleteSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */