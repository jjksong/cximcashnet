package io.reactivex.internal.subscribers;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.concurrent.CountDownLatch;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract class BlockingBaseSubscriber<T>
  extends CountDownLatch
  implements Subscriber<T>
{
  volatile boolean cancelled;
  Throwable error;
  Subscription s;
  T value;
  
  public BlockingBaseSubscriber()
  {
    super(1);
  }
  
  public final T blockingGet()
  {
    if (getCount() != 0L) {
      try
      {
        BlockingHelper.verifyNonBlocking();
        await();
      }
      catch (InterruptedException localInterruptedException)
      {
        localObject = this.s;
        this.s = SubscriptionHelper.CANCELLED;
        if (localObject != null) {
          ((Subscription)localObject).cancel();
        }
        throw ExceptionHelper.wrapOrThrow(localInterruptedException);
      }
    }
    Object localObject = this.error;
    if (localObject == null) {
      return (T)this.value;
    }
    throw ExceptionHelper.wrapOrThrow((Throwable)localObject);
  }
  
  public final void onComplete()
  {
    countDown();
  }
  
  public final void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.s, paramSubscription))
    {
      this.s = paramSubscription;
      if (!this.cancelled)
      {
        paramSubscription.request(Long.MAX_VALUE);
        if (this.cancelled)
        {
          this.s = SubscriptionHelper.CANCELLED;
          paramSubscription.cancel();
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/BlockingBaseSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */