package io.reactivex.internal.subscribers;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class LambdaSubscriber<T>
  extends AtomicReference<Subscription>
  implements Subscriber<T>, Subscription, Disposable
{
  private static final long serialVersionUID = -7251123623727029452L;
  final Action onComplete;
  final Consumer<? super Throwable> onError;
  final Consumer<? super T> onNext;
  final Consumer<? super Subscription> onSubscribe;
  
  public LambdaSubscriber(Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction, Consumer<? super Subscription> paramConsumer2)
  {
    this.onNext = paramConsumer;
    this.onError = paramConsumer1;
    this.onComplete = paramAction;
    this.onSubscribe = paramConsumer2;
  }
  
  public void cancel()
  {
    SubscriptionHelper.cancel(this);
  }
  
  public void dispose()
  {
    cancel();
  }
  
  public boolean isDisposed()
  {
    boolean bool;
    if (get() == SubscriptionHelper.CANCELLED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if (get() != SubscriptionHelper.CANCELLED)
    {
      lazySet(SubscriptionHelper.CANCELLED);
      try
      {
        this.onComplete.run();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (get() != SubscriptionHelper.CANCELLED)
    {
      lazySet(SubscriptionHelper.CANCELLED);
      try
      {
        this.onError.accept(paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    else
    {
      RxJavaPlugins.onError(paramThrowable);
    }
  }
  
  public void onNext(T paramT)
  {
    if (!isDisposed()) {
      try
      {
        this.onNext.accept(paramT);
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        ((Subscription)get()).cancel();
        onError(paramT);
      }
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.setOnce(this, paramSubscription)) {
      try
      {
        this.onSubscribe.accept(this);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        paramSubscription.cancel();
        onError(localThrowable);
      }
    }
  }
  
  public void request(long paramLong)
  {
    ((Subscription)get()).request(paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/LambdaSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */