package io.reactivex.internal.subscribers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SubscriberResourceWrapper<T>
  extends AtomicReference<Disposable>
  implements Subscriber<T>, Disposable, Subscription
{
  private static final long serialVersionUID = -8612022020200669122L;
  final Subscriber<? super T> actual;
  final AtomicReference<Subscription> subscription = new AtomicReference();
  
  public SubscriberResourceWrapper(Subscriber<? super T> paramSubscriber)
  {
    this.actual = paramSubscriber;
  }
  
  public void cancel()
  {
    dispose();
  }
  
  public void dispose()
  {
    SubscriptionHelper.cancel(this.subscription);
    DisposableHelper.dispose(this);
  }
  
  public boolean isDisposed()
  {
    boolean bool;
    if (this.subscription.get() == SubscriptionHelper.CANCELLED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    dispose();
    this.actual.onComplete();
  }
  
  public void onError(Throwable paramThrowable)
  {
    dispose();
    this.actual.onError(paramThrowable);
  }
  
  public void onNext(T paramT)
  {
    this.actual.onNext(paramT);
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    do
    {
      Subscription localSubscription = (Subscription)this.subscription.get();
      if (localSubscription == SubscriptionHelper.CANCELLED)
      {
        paramSubscription.cancel();
        return;
      }
      if (localSubscription != null)
      {
        paramSubscription.cancel();
        SubscriptionHelper.reportSubscriptionSet();
        return;
      }
    } while (!this.subscription.compareAndSet(null, paramSubscription));
    this.actual.onSubscribe(this);
  }
  
  public void request(long paramLong)
  {
    if (SubscriptionHelper.validate(paramLong)) {
      ((Subscription)this.subscription.get()).request(paramLong);
    }
  }
  
  public void setResource(Disposable paramDisposable)
  {
    DisposableHelper.set(this, paramDisposable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/SubscriberResourceWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */