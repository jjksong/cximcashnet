package io.reactivex.internal.subscribers;

public abstract interface InnerQueuedSubscriberSupport<T>
{
  public abstract void drain();
  
  public abstract void innerComplete(InnerQueuedSubscriber<T> paramInnerQueuedSubscriber);
  
  public abstract void innerError(InnerQueuedSubscriber<T> paramInnerQueuedSubscriber, Throwable paramThrowable);
  
  public abstract void innerNext(InnerQueuedSubscriber<T> paramInnerQueuedSubscriber, T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/InnerQueuedSubscriberSupport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */