package io.reactivex.internal.subscribers;

import io.reactivex.internal.subscriptions.FullArbiter;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FullArbiterSubscriber<T>
  implements Subscriber<T>
{
  final FullArbiter<T> arbiter;
  Subscription s;
  
  public FullArbiterSubscriber(FullArbiter<T> paramFullArbiter)
  {
    this.arbiter = paramFullArbiter;
  }
  
  public void onComplete()
  {
    this.arbiter.onComplete(this.s);
  }
  
  public void onError(Throwable paramThrowable)
  {
    this.arbiter.onError(paramThrowable, this.s);
  }
  
  public void onNext(T paramT)
  {
    this.arbiter.onNext(paramT, this.s);
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.s, paramSubscription))
    {
      this.s = paramSubscription;
      this.arbiter.setSubscription(paramSubscription);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/subscribers/FullArbiterSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */