package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.concurrent.CountDownLatch;

public abstract class BlockingBaseObserver<T>
  extends CountDownLatch
  implements Observer<T>, Disposable
{
  volatile boolean cancelled;
  Disposable d;
  Throwable error;
  T value;
  
  public BlockingBaseObserver()
  {
    super(1);
  }
  
  public final T blockingGet()
  {
    if (getCount() != 0L) {
      try
      {
        BlockingHelper.verifyNonBlocking();
        await();
      }
      catch (InterruptedException localInterruptedException)
      {
        dispose();
        throw ExceptionHelper.wrapOrThrow(localInterruptedException);
      }
    }
    Throwable localThrowable = this.error;
    if (localThrowable == null) {
      return (T)this.value;
    }
    throw ExceptionHelper.wrapOrThrow(localThrowable);
  }
  
  public final void dispose()
  {
    this.cancelled = true;
    Disposable localDisposable = this.d;
    if (localDisposable != null) {
      localDisposable.dispose();
    }
  }
  
  public final boolean isDisposed()
  {
    return this.cancelled;
  }
  
  public final void onComplete()
  {
    countDown();
  }
  
  public final void onSubscribe(Disposable paramDisposable)
  {
    this.d = paramDisposable;
    if (this.cancelled) {
      paramDisposable.dispose();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/BlockingBaseObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */