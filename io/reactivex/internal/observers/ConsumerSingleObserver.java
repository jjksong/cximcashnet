package io.reactivex.internal.observers;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class ConsumerSingleObserver<T>
  extends AtomicReference<Disposable>
  implements SingleObserver<T>, Disposable
{
  private static final long serialVersionUID = -7012088219455310787L;
  final Consumer<? super Throwable> onError;
  final Consumer<? super T> onSuccess;
  
  public ConsumerSingleObserver(Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1)
  {
    this.onSuccess = paramConsumer;
    this.onError = paramConsumer1;
  }
  
  public void dispose()
  {
    DisposableHelper.dispose(this);
  }
  
  public boolean isDisposed()
  {
    boolean bool;
    if (get() == DisposableHelper.DISPOSED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onError(Throwable paramThrowable)
  {
    try
    {
      this.onError.accept(paramThrowable);
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    DisposableHelper.setOnce(this, paramDisposable);
  }
  
  public void onSuccess(T paramT)
  {
    try
    {
      this.onSuccess.accept(paramT);
    }
    catch (Throwable paramT)
    {
      Exceptions.throwIfFatal(paramT);
      RxJavaPlugins.onError(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/ConsumerSingleObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */