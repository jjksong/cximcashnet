package io.reactivex.internal.observers;

public abstract interface InnerQueuedObserverSupport<T>
{
  public abstract void drain();
  
  public abstract void innerComplete(InnerQueuedObserver<T> paramInnerQueuedObserver);
  
  public abstract void innerError(InnerQueuedObserver<T> paramInnerQueuedObserver, Throwable paramThrowable);
  
  public abstract void innerNext(InnerQueuedObserver<T> paramInnerQueuedObserver, T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/InnerQueuedObserverSupport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */