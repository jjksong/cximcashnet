package io.reactivex.internal.observers;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class FutureSingleObserver<T>
  extends CountDownLatch
  implements SingleObserver<T>, Future<T>, Disposable
{
  Throwable error;
  final AtomicReference<Disposable> s = new AtomicReference();
  T value;
  
  public FutureSingleObserver()
  {
    super(1);
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    Disposable localDisposable;
    do
    {
      localDisposable = (Disposable)this.s.get();
      if ((localDisposable == this) || (localDisposable == DisposableHelper.DISPOSED)) {
        break;
      }
    } while (!this.s.compareAndSet(localDisposable, DisposableHelper.DISPOSED));
    if (localDisposable != null) {
      localDisposable.dispose();
    }
    countDown();
    return true;
    return false;
  }
  
  public void dispose() {}
  
  public T get()
    throws InterruptedException, ExecutionException
  {
    if (getCount() != 0L)
    {
      BlockingHelper.verifyNonBlocking();
      await();
    }
    if (!isCancelled())
    {
      Throwable localThrowable = this.error;
      if (localThrowable == null) {
        return (T)this.value;
      }
      throw new ExecutionException(localThrowable);
    }
    throw new CancellationException();
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ExecutionException, TimeoutException
  {
    if (getCount() != 0L)
    {
      BlockingHelper.verifyNonBlocking();
      if (!await(paramLong, paramTimeUnit)) {
        throw new TimeoutException();
      }
    }
    if (!isCancelled())
    {
      paramTimeUnit = this.error;
      if (paramTimeUnit == null) {
        return (T)this.value;
      }
      throw new ExecutionException(paramTimeUnit);
    }
    throw new CancellationException();
  }
  
  public boolean isCancelled()
  {
    return DisposableHelper.isDisposed((Disposable)this.s.get());
  }
  
  public boolean isDisposed()
  {
    return isDone();
  }
  
  public boolean isDone()
  {
    boolean bool;
    if (getCount() == 0L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onError(Throwable paramThrowable)
  {
    Disposable localDisposable;
    do
    {
      localDisposable = (Disposable)this.s.get();
      if (localDisposable == DisposableHelper.DISPOSED)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
    } while (!this.s.compareAndSet(localDisposable, this));
    countDown();
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    DisposableHelper.setOnce(this.s, paramDisposable);
  }
  
  public void onSuccess(T paramT)
  {
    Disposable localDisposable = (Disposable)this.s.get();
    if (localDisposable == DisposableHelper.DISPOSED) {
      return;
    }
    this.value = paramT;
    this.s.compareAndSet(localDisposable, this);
    countDown();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/FutureSingleObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */