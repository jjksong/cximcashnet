package io.reactivex.internal.observers;

import io.reactivex.disposables.Disposable;

public final class BlockingFirstObserver<T>
  extends BlockingBaseObserver<T>
{
  public void onError(Throwable paramThrowable)
  {
    if (this.value == null) {
      this.error = paramThrowable;
    }
    countDown();
  }
  
  public void onNext(T paramT)
  {
    if (this.value == null)
    {
      this.value = paramT;
      this.d.dispose();
      countDown();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/BlockingFirstObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */