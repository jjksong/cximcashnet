package io.reactivex.internal.observers;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class CallbackCompletableObserver
  extends AtomicReference<Disposable>
  implements CompletableObserver, Disposable, Consumer<Throwable>
{
  private static final long serialVersionUID = -4361286194466301354L;
  final Action onComplete;
  final Consumer<? super Throwable> onError;
  
  public CallbackCompletableObserver(Action paramAction)
  {
    this.onError = this;
    this.onComplete = paramAction;
  }
  
  public CallbackCompletableObserver(Consumer<? super Throwable> paramConsumer, Action paramAction)
  {
    this.onError = paramConsumer;
    this.onComplete = paramAction;
  }
  
  public void accept(Throwable paramThrowable)
  {
    RxJavaPlugins.onError(paramThrowable);
  }
  
  public void dispose()
  {
    DisposableHelper.dispose(this);
  }
  
  public boolean isDisposed()
  {
    boolean bool;
    if (get() == DisposableHelper.DISPOSED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    try
    {
      this.onComplete.run();
      lazySet(DisposableHelper.DISPOSED);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      onError(localThrowable);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    try
    {
      this.onError.accept(paramThrowable);
    }
    catch (Throwable paramThrowable)
    {
      Exceptions.throwIfFatal(paramThrowable);
      RxJavaPlugins.onError(paramThrowable);
    }
    lazySet(DisposableHelper.DISPOSED);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    DisposableHelper.setOnce(this, paramDisposable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/CallbackCompletableObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */