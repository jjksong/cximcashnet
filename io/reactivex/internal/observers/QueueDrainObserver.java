package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.util.ObservableQueueDrain;
import io.reactivex.internal.util.QueueDrainHelper;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class QueueDrainObserver<T, U, V>
  extends QueueDrainSubscriberPad2
  implements Observer<T>, ObservableQueueDrain<U, V>
{
  protected final Observer<? super V> actual;
  protected volatile boolean cancelled;
  protected volatile boolean done;
  protected Throwable error;
  protected final SimpleQueue<U> queue;
  
  public QueueDrainObserver(Observer<? super V> paramObserver, SimpleQueue<U> paramSimpleQueue)
  {
    this.actual = paramObserver;
    this.queue = paramSimpleQueue;
  }
  
  public void accept(Observer<? super V> paramObserver, U paramU) {}
  
  public final boolean cancelled()
  {
    return this.cancelled;
  }
  
  public final boolean done()
  {
    return this.done;
  }
  
  public void drain(boolean paramBoolean, Disposable paramDisposable)
  {
    if (enter()) {
      QueueDrainHelper.drainLoop(this.queue, this.actual, paramBoolean, paramDisposable, this);
    }
  }
  
  public final boolean enter()
  {
    boolean bool;
    if (this.wip.getAndIncrement() == 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final Throwable error()
  {
    return this.error;
  }
  
  public final boolean fastEnter()
  {
    int i = this.wip.get();
    boolean bool = true;
    if ((i != 0) || (!this.wip.compareAndSet(0, 1))) {
      bool = false;
    }
    return bool;
  }
  
  protected final void fastPathEmit(U paramU, boolean paramBoolean, Disposable paramDisposable)
  {
    Observer localObserver = this.actual;
    SimpleQueue localSimpleQueue = this.queue;
    if ((this.wip.get() == 0) && (this.wip.compareAndSet(0, 1)))
    {
      accept(localObserver, paramU);
      if (leave(-1) != 0) {}
    }
    else
    {
      localSimpleQueue.offer(paramU);
      if (!enter()) {
        return;
      }
    }
    QueueDrainHelper.drainLoop(localSimpleQueue, localObserver, paramBoolean, paramDisposable, this);
  }
  
  protected final void fastPathOrderedEmit(U paramU, boolean paramBoolean, Disposable paramDisposable)
  {
    Observer localObserver = this.actual;
    SimpleQueue localSimpleQueue = this.queue;
    if ((this.wip.get() == 0) && (this.wip.compareAndSet(0, 1)))
    {
      if (localSimpleQueue.isEmpty())
      {
        accept(localObserver, paramU);
        if (leave(-1) != 0) {}
      }
      else
      {
        localSimpleQueue.offer(paramU);
      }
    }
    else
    {
      localSimpleQueue.offer(paramU);
      if (!enter()) {
        return;
      }
    }
    QueueDrainHelper.drainLoop(localSimpleQueue, localObserver, paramBoolean, paramDisposable, this);
  }
  
  public final int leave(int paramInt)
  {
    return this.wip.addAndGet(paramInt);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/QueueDrainObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */