package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.ObserverFullArbiter;

public final class FullArbiterObserver<T>
  implements Observer<T>
{
  final ObserverFullArbiter<T> arbiter;
  Disposable s;
  
  public FullArbiterObserver(ObserverFullArbiter<T> paramObserverFullArbiter)
  {
    this.arbiter = paramObserverFullArbiter;
  }
  
  public void onComplete()
  {
    this.arbiter.onComplete(this.s);
  }
  
  public void onError(Throwable paramThrowable)
  {
    this.arbiter.onError(paramThrowable, this.s);
  }
  
  public void onNext(T paramT)
  {
    this.arbiter.onNext(paramT, this.s);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.validate(this.s, paramDisposable))
    {
      this.s = paramDisposable;
      this.arbiter.setDisposable(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/observers/FullArbiterObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */