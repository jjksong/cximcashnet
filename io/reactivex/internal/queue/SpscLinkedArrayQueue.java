package io.reactivex.internal.queue;

import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.util.Pow2;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class SpscLinkedArrayQueue<T>
  implements SimplePlainQueue<T>
{
  private static final Object HAS_NEXT = new Object();
  static final int MAX_LOOK_AHEAD_STEP = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
  AtomicReferenceArray<Object> consumerBuffer;
  final AtomicLong consumerIndex = new AtomicLong();
  final int consumerMask;
  AtomicReferenceArray<Object> producerBuffer;
  final AtomicLong producerIndex = new AtomicLong();
  long producerLookAhead;
  int producerLookAheadStep;
  final int producerMask;
  
  public SpscLinkedArrayQueue(int paramInt)
  {
    paramInt = Pow2.roundToPowerOfTwo(Math.max(8, paramInt));
    int i = paramInt - 1;
    AtomicReferenceArray localAtomicReferenceArray = new AtomicReferenceArray(paramInt + 1);
    this.producerBuffer = localAtomicReferenceArray;
    this.producerMask = i;
    adjustLookAheadStep(paramInt);
    this.consumerBuffer = localAtomicReferenceArray;
    this.consumerMask = i;
    this.producerLookAhead = (i - 1);
    soProducerIndex(0L);
  }
  
  private void adjustLookAheadStep(int paramInt)
  {
    this.producerLookAheadStep = Math.min(paramInt / 4, MAX_LOOK_AHEAD_STEP);
  }
  
  private static int calcDirectOffset(int paramInt)
  {
    return paramInt;
  }
  
  private static int calcWrappedOffset(long paramLong, int paramInt)
  {
    return calcDirectOffset((int)paramLong & paramInt);
  }
  
  private long lpConsumerIndex()
  {
    return this.consumerIndex.get();
  }
  
  private long lpProducerIndex()
  {
    return this.producerIndex.get();
  }
  
  private long lvConsumerIndex()
  {
    return this.consumerIndex.get();
  }
  
  private static <E> Object lvElement(AtomicReferenceArray<Object> paramAtomicReferenceArray, int paramInt)
  {
    return paramAtomicReferenceArray.get(paramInt);
  }
  
  private AtomicReferenceArray<Object> lvNext(AtomicReferenceArray<Object> paramAtomicReferenceArray)
  {
    return (AtomicReferenceArray)lvElement(paramAtomicReferenceArray, calcDirectOffset(paramAtomicReferenceArray.length() - 1));
  }
  
  private long lvProducerIndex()
  {
    return this.producerIndex.get();
  }
  
  private T newBufferPeek(AtomicReferenceArray<Object> paramAtomicReferenceArray, long paramLong, int paramInt)
  {
    this.consumerBuffer = paramAtomicReferenceArray;
    return (T)lvElement(paramAtomicReferenceArray, calcWrappedOffset(paramLong, paramInt));
  }
  
  private T newBufferPoll(AtomicReferenceArray<Object> paramAtomicReferenceArray, long paramLong, int paramInt)
  {
    this.consumerBuffer = paramAtomicReferenceArray;
    paramInt = calcWrappedOffset(paramLong, paramInt);
    Object localObject = lvElement(paramAtomicReferenceArray, paramInt);
    if (localObject != null)
    {
      soElement(paramAtomicReferenceArray, paramInt, null);
      soConsumerIndex(paramLong + 1L);
    }
    return (T)localObject;
  }
  
  private void resize(AtomicReferenceArray<Object> paramAtomicReferenceArray, long paramLong1, int paramInt, T paramT, long paramLong2)
  {
    AtomicReferenceArray localAtomicReferenceArray = new AtomicReferenceArray(paramAtomicReferenceArray.length());
    this.producerBuffer = localAtomicReferenceArray;
    this.producerLookAhead = (paramLong2 + paramLong1 - 1L);
    soElement(localAtomicReferenceArray, paramInt, paramT);
    soNext(paramAtomicReferenceArray, localAtomicReferenceArray);
    soElement(paramAtomicReferenceArray, paramInt, HAS_NEXT);
    soProducerIndex(paramLong1 + 1L);
  }
  
  private void soConsumerIndex(long paramLong)
  {
    this.consumerIndex.lazySet(paramLong);
  }
  
  private static void soElement(AtomicReferenceArray<Object> paramAtomicReferenceArray, int paramInt, Object paramObject)
  {
    paramAtomicReferenceArray.lazySet(paramInt, paramObject);
  }
  
  private void soNext(AtomicReferenceArray<Object> paramAtomicReferenceArray1, AtomicReferenceArray<Object> paramAtomicReferenceArray2)
  {
    soElement(paramAtomicReferenceArray1, calcDirectOffset(paramAtomicReferenceArray1.length() - 1), paramAtomicReferenceArray2);
  }
  
  private void soProducerIndex(long paramLong)
  {
    this.producerIndex.lazySet(paramLong);
  }
  
  private boolean writeToQueue(AtomicReferenceArray<Object> paramAtomicReferenceArray, T paramT, long paramLong, int paramInt)
  {
    soElement(paramAtomicReferenceArray, paramInt, paramT);
    soProducerIndex(paramLong + 1L);
    return true;
  }
  
  public void clear()
  {
    while ((poll() != null) || (!isEmpty())) {}
  }
  
  public boolean isEmpty()
  {
    boolean bool;
    if (lvProducerIndex() == lvConsumerIndex()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean offer(T paramT)
  {
    if (paramT != null)
    {
      AtomicReferenceArray localAtomicReferenceArray = this.producerBuffer;
      long l2 = lpProducerIndex();
      int j = this.producerMask;
      int i = calcWrappedOffset(l2, j);
      if (l2 < this.producerLookAhead) {
        return writeToQueue(localAtomicReferenceArray, paramT, l2, i);
      }
      long l1 = this.producerLookAheadStep + l2;
      if (lvElement(localAtomicReferenceArray, calcWrappedOffset(l1, j)) == null)
      {
        this.producerLookAhead = (l1 - 1L);
        return writeToQueue(localAtomicReferenceArray, paramT, l2, i);
      }
      if (lvElement(localAtomicReferenceArray, calcWrappedOffset(1L + l2, j)) == null) {
        return writeToQueue(localAtomicReferenceArray, paramT, l2, i);
      }
      resize(localAtomicReferenceArray, l2, i, paramT, j);
      return true;
    }
    throw new NullPointerException("Null is not a valid element");
  }
  
  public boolean offer(T paramT1, T paramT2)
  {
    AtomicReferenceArray localAtomicReferenceArray1 = this.producerBuffer;
    long l2 = lvProducerIndex();
    int i = this.producerMask;
    long l1 = 2L + l2;
    if (lvElement(localAtomicReferenceArray1, calcWrappedOffset(l1, i)) == null)
    {
      i = calcWrappedOffset(l2, i);
      soElement(localAtomicReferenceArray1, i + 1, paramT2);
      soElement(localAtomicReferenceArray1, i, paramT1);
      soProducerIndex(l1);
    }
    else
    {
      AtomicReferenceArray localAtomicReferenceArray2 = new AtomicReferenceArray(localAtomicReferenceArray1.length());
      this.producerBuffer = localAtomicReferenceArray2;
      i = calcWrappedOffset(l2, i);
      soElement(localAtomicReferenceArray2, i + 1, paramT2);
      soElement(localAtomicReferenceArray2, i, paramT1);
      soNext(localAtomicReferenceArray1, localAtomicReferenceArray2);
      soElement(localAtomicReferenceArray1, i, HAS_NEXT);
      soProducerIndex(l1);
    }
    return true;
  }
  
  public T peek()
  {
    AtomicReferenceArray localAtomicReferenceArray = this.consumerBuffer;
    long l = lpConsumerIndex();
    int i = this.consumerMask;
    Object localObject = lvElement(localAtomicReferenceArray, calcWrappedOffset(l, i));
    if (localObject == HAS_NEXT) {
      return (T)newBufferPeek(lvNext(localAtomicReferenceArray), l, i);
    }
    return (T)localObject;
  }
  
  public T poll()
  {
    AtomicReferenceArray localAtomicReferenceArray = this.consumerBuffer;
    long l = lpConsumerIndex();
    int j = this.consumerMask;
    int k = calcWrappedOffset(l, j);
    Object localObject = lvElement(localAtomicReferenceArray, k);
    int i;
    if (localObject == HAS_NEXT) {
      i = 1;
    } else {
      i = 0;
    }
    if ((localObject != null) && (i == 0))
    {
      soElement(localAtomicReferenceArray, k, null);
      soConsumerIndex(l + 1L);
      return (T)localObject;
    }
    if (i != 0) {
      return (T)newBufferPoll(lvNext(localAtomicReferenceArray), l, j);
    }
    return null;
  }
  
  public int size()
  {
    long l2;
    for (long l1 = lvConsumerIndex();; l1 = l2)
    {
      long l3 = lvProducerIndex();
      l2 = lvConsumerIndex();
      if (l1 == l2) {
        return (int)(l3 - l2);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/queue/SpscLinkedArrayQueue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */