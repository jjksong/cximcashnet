package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableSwitchMap<T, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final int bufferSize;
  final boolean delayErrors;
  final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
  
  public ObservableSwitchMap(ObservableSource<T> paramObservableSource, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.mapper = paramFunction;
    this.bufferSize = paramInt;
    this.delayErrors = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    if (ObservableScalarXMap.tryScalarXMapSubscribe(this.source, paramObserver, this.mapper)) {
      return;
    }
    this.source.subscribe(new SwitchMapObserver(paramObserver, this.mapper, this.bufferSize, this.delayErrors));
  }
  
  static final class SwitchMapInnerObserver<T, R>
    extends AtomicReference<Disposable>
    implements Observer<R>
  {
    private static final long serialVersionUID = 3837284832786408377L;
    volatile boolean done;
    final long index;
    final ObservableSwitchMap.SwitchMapObserver<T, R> parent;
    final SpscLinkedArrayQueue<R> queue;
    
    SwitchMapInnerObserver(ObservableSwitchMap.SwitchMapObserver<T, R> paramSwitchMapObserver, long paramLong, int paramInt)
    {
      this.parent = paramSwitchMapObserver;
      this.index = paramLong;
      this.queue = new SpscLinkedArrayQueue(paramInt);
    }
    
    public void cancel()
    {
      DisposableHelper.dispose(this);
    }
    
    public void onComplete()
    {
      if (this.index == this.parent.unique)
      {
        this.done = true;
        this.parent.drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(this, paramThrowable);
    }
    
    public void onNext(R paramR)
    {
      if (this.index == this.parent.unique)
      {
        this.queue.offer(paramR);
        this.parent.drain();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
  
  static final class SwitchMapObserver<T, R>
    extends AtomicInteger
    implements Observer<T>, Disposable
  {
    static final ObservableSwitchMap.SwitchMapInnerObserver<Object, Object> CANCELLED = new ObservableSwitchMap.SwitchMapInnerObserver(null, -1L, 1);
    private static final long serialVersionUID = -3491074160481096299L;
    final AtomicReference<ObservableSwitchMap.SwitchMapInnerObserver<T, R>> active = new AtomicReference();
    final Observer<? super R> actual;
    final int bufferSize;
    volatile boolean cancelled;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicThrowable errors;
    final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
    Disposable s;
    volatile long unique;
    
    static
    {
      CANCELLED.cancel();
    }
    
    SwitchMapObserver(Observer<? super R> paramObserver, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
      this.bufferSize = paramInt;
      this.delayErrors = paramBoolean;
      this.errors = new AtomicThrowable();
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.dispose();
        disposeInner();
      }
    }
    
    void disposeInner()
    {
      ObservableSwitchMap.SwitchMapInnerObserver localSwitchMapInnerObserver1 = (ObservableSwitchMap.SwitchMapInnerObserver)this.active.get();
      ObservableSwitchMap.SwitchMapInnerObserver localSwitchMapInnerObserver2 = CANCELLED;
      if (localSwitchMapInnerObserver1 != localSwitchMapInnerObserver2)
      {
        localSwitchMapInnerObserver1 = (ObservableSwitchMap.SwitchMapInnerObserver)this.active.getAndSet(localSwitchMapInnerObserver2);
        if ((localSwitchMapInnerObserver1 != CANCELLED) && (localSwitchMapInnerObserver1 != null)) {
          localSwitchMapInnerObserver1.cancel();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = this.actual;
      int j = 1;
      int i;
      label378:
      label390:
      do
      {
        boolean bool;
        int k;
        Object localObject1;
        ObservableSwitchMap.SwitchMapInnerObserver localSwitchMapInnerObserver;
        for (;;)
        {
          if (this.cancelled) {
            return;
          }
          bool = this.done;
          k = 0;
          if (bool)
          {
            if (this.active.get() == null) {
              i = 1;
            } else {
              i = 0;
            }
            if (this.delayErrors)
            {
              if (i != 0)
              {
                localObject1 = (Throwable)this.errors.get();
                if (localObject1 != null) {
                  localObserver.onError((Throwable)localObject1);
                } else {
                  localObserver.onComplete();
                }
              }
            }
            else
            {
              if ((Throwable)this.errors.get() != null)
              {
                localObserver.onError(this.errors.terminate());
                return;
              }
              if (i != 0)
              {
                localObserver.onComplete();
                return;
              }
            }
          }
          localSwitchMapInnerObserver = (ObservableSwitchMap.SwitchMapInnerObserver)this.active.get();
          if (localSwitchMapInnerObserver == null) {
            break label390;
          }
          localObject1 = localSwitchMapInnerObserver.queue;
          if (!localSwitchMapInnerObserver.done) {
            break;
          }
          bool = ((SpscLinkedArrayQueue)localObject1).isEmpty();
          if (this.delayErrors)
          {
            if (!bool) {
              break;
            }
            this.active.compareAndSet(localSwitchMapInnerObserver, null);
          }
          else
          {
            if ((Throwable)this.errors.get() != null)
            {
              localObserver.onError(this.errors.terminate());
              return;
            }
            if (!bool) {
              break;
            }
            this.active.compareAndSet(localSwitchMapInnerObserver, null);
          }
        }
        for (;;)
        {
          if (this.cancelled) {
            return;
          }
          Object localObject2;
          if (localSwitchMapInnerObserver != this.active.get())
          {
            i = 1;
          }
          else
          {
            if ((!this.delayErrors) && ((Throwable)this.errors.get() != null))
            {
              localObserver.onError(this.errors.terminate());
              return;
            }
            bool = localSwitchMapInnerObserver.done;
            localObject2 = ((SpscLinkedArrayQueue)localObject1).poll();
            if (localObject2 == null) {
              i = 1;
            } else {
              i = 0;
            }
            if ((bool) && (i != 0))
            {
              this.active.compareAndSet(localSwitchMapInnerObserver, null);
              i = 1;
            }
            else
            {
              if (i == 0) {
                break label378;
              }
              i = k;
            }
          }
          if (i == 0) {
            break label390;
          }
          break;
          localObserver.onNext(localObject2);
        }
        i = addAndGet(-j);
        j = i;
      } while (i != 0);
    }
    
    void innerError(ObservableSwitchMap.SwitchMapInnerObserver<T, R> paramSwitchMapInnerObserver, Throwable paramThrowable)
    {
      if ((paramSwitchMapInnerObserver.index == this.unique) && (this.errors.addThrowable(paramThrowable)))
      {
        if (!this.delayErrors) {
          this.s.dispose();
        }
        paramSwitchMapInnerObserver.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.done) && (this.errors.addThrowable(paramThrowable)))
      {
        this.done = true;
        drain();
        return;
      }
      if (!this.delayErrors) {
        disposeInner();
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.unique + 1L;
      this.unique = l;
      ObservableSwitchMap.SwitchMapInnerObserver localSwitchMapInnerObserver = (ObservableSwitchMap.SwitchMapInnerObserver)this.active.get();
      if (localSwitchMapInnerObserver != null) {
        localSwitchMapInnerObserver.cancel();
      }
      try
      {
        ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The ObservableSource returned is null");
        localSwitchMapInnerObserver = new ObservableSwitchMap.SwitchMapInnerObserver(this, l, this.bufferSize);
        do
        {
          paramT = (ObservableSwitchMap.SwitchMapInnerObserver)this.active.get();
          if (paramT == CANCELLED) {
            break;
          }
        } while (!this.active.compareAndSet(paramT, localSwitchMapInnerObserver));
        localObservableSource.subscribe(localSwitchMapInnerObserver);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSwitchMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */