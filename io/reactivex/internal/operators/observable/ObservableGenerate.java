package io.reactivex.internal.operators.observable;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableGenerate<T, S>
  extends Observable<T>
{
  final Consumer<? super S> disposeState;
  final BiFunction<S, Emitter<T>, S> generator;
  final Callable<S> stateSupplier;
  
  public ObservableGenerate(Callable<S> paramCallable, BiFunction<S, Emitter<T>, S> paramBiFunction, Consumer<? super S> paramConsumer)
  {
    this.stateSupplier = paramCallable;
    this.generator = paramBiFunction;
    this.disposeState = paramConsumer;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    try
    {
      Object localObject = this.stateSupplier.call();
      localObject = new GeneratorDisposable(paramObserver, this.generator, this.disposeState, localObject);
      paramObserver.onSubscribe((Disposable)localObject);
      ((GeneratorDisposable)localObject).run();
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
  
  static final class GeneratorDisposable<T, S>
    implements Emitter<T>, Disposable
  {
    final Observer<? super T> actual;
    volatile boolean cancelled;
    final Consumer<? super S> disposeState;
    final BiFunction<S, ? super Emitter<T>, S> generator;
    boolean hasNext;
    S state;
    boolean terminate;
    
    GeneratorDisposable(Observer<? super T> paramObserver, BiFunction<S, ? super Emitter<T>, S> paramBiFunction, Consumer<? super S> paramConsumer, S paramS)
    {
      this.actual = paramObserver;
      this.generator = paramBiFunction;
      this.disposeState = paramConsumer;
      this.state = paramS;
    }
    
    private void dispose(S paramS)
    {
      try
      {
        this.disposeState.accept(paramS);
      }
      catch (Throwable paramS)
      {
        Exceptions.throwIfFatal(paramS);
        RxJavaPlugins.onError(paramS);
      }
    }
    
    public void dispose()
    {
      this.cancelled = true;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (!this.terminate)
      {
        this.terminate = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.terminate)
      {
        RxJavaPlugins.onError(paramThrowable);
      }
      else
      {
        Object localObject = paramThrowable;
        if (paramThrowable == null) {
          localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        this.terminate = true;
        this.actual.onError((Throwable)localObject);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.terminate) {
        if (this.hasNext)
        {
          onError(new IllegalStateException("onNext already called in this generate turn"));
        }
        else if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        }
        else
        {
          this.hasNext = true;
          this.actual.onNext(paramT);
        }
      }
    }
    
    public void run()
    {
      Object localObject1 = this.state;
      if (this.cancelled)
      {
        this.state = null;
        dispose(localObject1);
        return;
      }
      BiFunction localBiFunction = this.generator;
      for (;;)
      {
        if (this.cancelled)
        {
          this.state = null;
          dispose(localObject1);
          return;
        }
        this.hasNext = false;
        try
        {
          Object localObject2 = localBiFunction.apply(localObject1, this);
          localObject1 = localObject2;
          if (this.terminate)
          {
            this.cancelled = true;
            this.state = null;
            dispose(localObject2);
            return;
          }
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.state = null;
          this.cancelled = true;
          onError(localThrowable);
          dispose(localObject1);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableGenerate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */