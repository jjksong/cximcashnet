package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableOperator;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableLift<R, T>
  extends AbstractObservableWithUpstream<T, R>
{
  final ObservableOperator<? extends R, ? super T> operator;
  
  public ObservableLift(ObservableSource<T> paramObservableSource, ObservableOperator<? extends R, ? super T> paramObservableOperator)
  {
    super(paramObservableSource);
    this.operator = paramObservableOperator;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    try
    {
      paramObserver = this.operator.apply(paramObserver);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("Operator ");
      localStringBuilder.append(this.operator);
      localStringBuilder.append(" returned a null Observer");
      paramObserver = (Observer)ObjectHelper.requireNonNull(paramObserver, localStringBuilder.toString());
      this.source.subscribe(paramObserver);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
      paramObserver = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
      paramObserver.initCause(localThrowable);
      throw paramObserver;
    }
    catch (NullPointerException paramObserver)
    {
      throw paramObserver;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableLift.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */