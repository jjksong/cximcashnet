package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ObservableUsing<T, D>
  extends Observable<T>
{
  final Consumer<? super D> disposer;
  final boolean eager;
  final Callable<? extends D> resourceSupplier;
  final Function<? super D, ? extends ObservableSource<? extends T>> sourceSupplier;
  
  public ObservableUsing(Callable<? extends D> paramCallable, Function<? super D, ? extends ObservableSource<? extends T>> paramFunction, Consumer<? super D> paramConsumer, boolean paramBoolean)
  {
    this.resourceSupplier = paramCallable;
    this.sourceSupplier = paramFunction;
    this.disposer = paramConsumer;
    this.eager = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    try
    {
      Object localObject = this.resourceSupplier.call();
      try
      {
        ObservableSource localObservableSource = (ObservableSource)this.sourceSupplier.apply(localObject);
        localObservableSource.subscribe(new UsingObserver(paramObserver, localObject, this.disposer, this.eager));
        return;
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        try
        {
          this.disposer.accept(localObject);
          EmptyDisposable.error(localThrowable1, paramObserver);
          return;
        }
        catch (Throwable localThrowable3)
        {
          Exceptions.throwIfFatal(localThrowable3);
          EmptyDisposable.error(new CompositeException(new Throwable[] { localThrowable1, localThrowable3 }), paramObserver);
          return;
        }
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptyDisposable.error(localThrowable2, paramObserver);
    }
  }
  
  static final class UsingObserver<T, D>
    extends AtomicBoolean
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 5904473792286235046L;
    final Observer<? super T> actual;
    final Consumer<? super D> disposer;
    final boolean eager;
    final D resource;
    Disposable s;
    
    UsingObserver(Observer<? super T> paramObserver, D paramD, Consumer<? super D> paramConsumer, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.resource = paramD;
      this.disposer = paramConsumer;
      this.eager = paramBoolean;
    }
    
    public void dispose()
    {
      disposeAfter();
      this.s.dispose();
    }
    
    void disposeAfter()
    {
      if (compareAndSet(false, true)) {
        try
        {
          this.disposer.accept(this.resource);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return get();
    }
    
    public void onComplete()
    {
      if (this.eager)
      {
        if (compareAndSet(false, true)) {
          try
          {
            this.disposer.accept(this.resource);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.actual.onError(localThrowable);
            return;
          }
        }
        this.s.dispose();
        this.actual.onComplete();
      }
      else
      {
        this.actual.onComplete();
        this.s.dispose();
        disposeAfter();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.eager)
      {
        Throwable localThrowable1 = paramThrowable;
        CompositeException localCompositeException;
        if (compareAndSet(false, true)) {
          try
          {
            this.disposer.accept(this.resource);
            localThrowable1 = paramThrowable;
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            localCompositeException = new CompositeException(new Throwable[] { paramThrowable, localThrowable2 });
          }
        }
        this.s.dispose();
        this.actual.onError(localCompositeException);
      }
      else
      {
        this.actual.onError(paramThrowable);
        this.s.dispose();
        disposeAfter();
      }
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableUsing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */