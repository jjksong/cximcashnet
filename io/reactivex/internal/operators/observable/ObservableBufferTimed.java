package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableBufferTimed<T, U extends Collection<? super T>>
  extends AbstractObservableWithUpstream<T, U>
{
  final Callable<U> bufferSupplier;
  final int maxSize;
  final boolean restartTimerOnMaxSize;
  final Scheduler scheduler;
  final long timeskip;
  final long timespan;
  final TimeUnit unit;
  
  public ObservableBufferTimed(ObservableSource<T> paramObservableSource, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler, Callable<U> paramCallable, int paramInt, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.timespan = paramLong1;
    this.timeskip = paramLong2;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.bufferSupplier = paramCallable;
    this.maxSize = paramInt;
    this.restartTimerOnMaxSize = paramBoolean;
  }
  
  protected void subscribeActual(Observer<? super U> paramObserver)
  {
    if ((this.timespan == this.timeskip) && (this.maxSize == Integer.MAX_VALUE))
    {
      this.source.subscribe(new BufferExactUnboundedObserver(new SerializedObserver(paramObserver), this.bufferSupplier, this.timespan, this.unit, this.scheduler));
      return;
    }
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    if (this.timespan == this.timeskip)
    {
      this.source.subscribe(new BufferExactBoundedObserver(new SerializedObserver(paramObserver), this.bufferSupplier, this.timespan, this.unit, this.maxSize, this.restartTimerOnMaxSize, localWorker));
      return;
    }
    this.source.subscribe(new BufferSkipBoundedObserver(new SerializedObserver(paramObserver), this.bufferSupplier, this.timespan, this.timeskip, this.unit, localWorker));
  }
  
  static final class BufferExactBoundedObserver<T, U extends Collection<? super T>>
    extends QueueDrainObserver<T, U, U>
    implements Runnable, Disposable
  {
    U buffer;
    final Callable<U> bufferSupplier;
    long consumerIndex;
    final int maxSize;
    long producerIndex;
    final boolean restartTimerOnMaxSize;
    Disposable s;
    Disposable timer;
    final long timespan;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    BufferExactBoundedObserver(Observer<? super U> paramObserver, Callable<U> paramCallable, long paramLong, TimeUnit paramTimeUnit, int paramInt, boolean paramBoolean, Scheduler.Worker paramWorker)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong;
      this.unit = paramTimeUnit;
      this.maxSize = paramInt;
      this.restartTimerOnMaxSize = paramBoolean;
      this.w = paramWorker;
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      paramObserver.onNext(paramU);
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.w.dispose();
        try
        {
          this.buffer = null;
          this.s.dispose();
        }
        finally {}
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      this.w.dispose();
      try
      {
        Collection localCollection = this.buffer;
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.w.dispose();
      try
      {
        this.buffer = null;
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    /* Error */
    public void onNext(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 82	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:buffer	Ljava/util/Collection;
      //   6: astore 4
      //   8: aload 4
      //   10: ifnonnull +6 -> 16
      //   13: aload_0
      //   14: monitorexit
      //   15: return
      //   16: aload 4
      //   18: aload_1
      //   19: invokeinterface 123 2 0
      //   24: pop
      //   25: aload 4
      //   27: invokeinterface 127 1 0
      //   32: aload_0
      //   33: getfield 51	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:maxSize	I
      //   36: if_icmpge +6 -> 42
      //   39: aload_0
      //   40: monitorexit
      //   41: return
      //   42: aload_0
      //   43: monitorexit
      //   44: aload_0
      //   45: getfield 53	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:restartTimerOnMaxSize	Z
      //   48: ifeq +27 -> 75
      //   51: aload_0
      //   52: aconst_null
      //   53: putfield 82	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:buffer	Ljava/util/Collection;
      //   56: aload_0
      //   57: aload_0
      //   58: getfield 129	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:producerIndex	J
      //   61: lconst_1
      //   62: ladd
      //   63: putfield 129	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:producerIndex	J
      //   66: aload_0
      //   67: getfield 131	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:timer	Lio/reactivex/disposables/Disposable;
      //   70: invokeinterface 85 1 0
      //   75: aload_0
      //   76: aload 4
      //   78: iconst_0
      //   79: aload_0
      //   80: invokevirtual 135	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:fastPathOrderedEmit	(Ljava/lang/Object;ZLio/reactivex/disposables/Disposable;)V
      //   83: aload_0
      //   84: getfield 45	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:bufferSupplier	Ljava/util/concurrent/Callable;
      //   87: invokeinterface 141 1 0
      //   92: ldc -113
      //   94: invokestatic 149	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   97: checkcast 62	java/util/Collection
      //   100: astore_1
      //   101: aload_0
      //   102: getfield 53	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:restartTimerOnMaxSize	Z
      //   105: ifeq +55 -> 160
      //   108: aload_0
      //   109: monitorenter
      //   110: aload_0
      //   111: aload_1
      //   112: putfield 82	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:buffer	Ljava/util/Collection;
      //   115: aload_0
      //   116: aload_0
      //   117: getfield 151	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:consumerIndex	J
      //   120: lconst_1
      //   121: ladd
      //   122: putfield 151	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:consumerIndex	J
      //   125: aload_0
      //   126: monitorexit
      //   127: aload_0
      //   128: getfield 55	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:w	Lio/reactivex/Scheduler$Worker;
      //   131: astore_1
      //   132: aload_0
      //   133: getfield 47	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:timespan	J
      //   136: lstore_2
      //   137: aload_0
      //   138: aload_1
      //   139: aload_0
      //   140: lload_2
      //   141: lload_2
      //   142: aload_0
      //   143: getfield 49	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:unit	Ljava/util/concurrent/TimeUnit;
      //   146: invokevirtual 155	io/reactivex/Scheduler$Worker:schedulePeriodically	(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
      //   149: putfield 131	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:timer	Lio/reactivex/disposables/Disposable;
      //   152: goto +17 -> 169
      //   155: astore_1
      //   156: aload_0
      //   157: monitorexit
      //   158: aload_1
      //   159: athrow
      //   160: aload_0
      //   161: monitorenter
      //   162: aload_0
      //   163: aload_1
      //   164: putfield 82	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:buffer	Ljava/util/Collection;
      //   167: aload_0
      //   168: monitorexit
      //   169: return
      //   170: astore_1
      //   171: aload_0
      //   172: monitorexit
      //   173: aload_1
      //   174: athrow
      //   175: astore_1
      //   176: aload_1
      //   177: invokestatic 160	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   180: aload_0
      //   181: invokevirtual 161	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:dispose	()V
      //   184: aload_0
      //   185: getfield 108	io/reactivex/internal/operators/observable/ObservableBufferTimed$BufferExactBoundedObserver:actual	Lio/reactivex/Observer;
      //   188: aload_1
      //   189: invokeinterface 118 2 0
      //   194: return
      //   195: astore_1
      //   196: aload_0
      //   197: monitorexit
      //   198: aload_1
      //   199: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	200	0	this	BufferExactBoundedObserver
      //   0	200	1	paramT	T
      //   136	6	2	l	long
      //   6	71	4	localCollection	Collection
      // Exception table:
      //   from	to	target	type
      //   110	127	155	finally
      //   156	158	155	finally
      //   162	169	170	finally
      //   171	173	170	finally
      //   83	101	175	java/lang/Throwable
      //   2	8	195	finally
      //   13	15	195	finally
      //   16	41	195	finally
      //   42	44	195	finally
      //   196	198	195	finally
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        try
        {
          Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
          this.buffer = localCollection;
          this.actual.onSubscribe(this);
          paramDisposable = this.w;
          long l = this.timespan;
          this.timer = paramDisposable.schedulePeriodically(this, l, l, this.unit);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.w.dispose();
          paramDisposable.dispose();
          EmptyDisposable.error(localThrowable, this.actual);
          return;
        }
      }
    }
    
    public void run()
    {
      try
      {
        Collection localCollection1 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
        try
        {
          Collection localCollection2 = this.buffer;
          if ((localCollection2 != null) && (this.producerIndex == this.consumerIndex))
          {
            this.buffer = localCollection1;
            fastPathOrderedEmit(localCollection2, false, this);
            return;
          }
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        dispose();
        this.actual.onError(localThrowable);
      }
    }
  }
  
  static final class BufferExactUnboundedObserver<T, U extends Collection<? super T>>
    extends QueueDrainObserver<T, U, U>
    implements Runnable, Disposable
  {
    U buffer;
    final Callable<U> bufferSupplier;
    Disposable s;
    final Scheduler scheduler;
    final AtomicReference<Disposable> timer = new AtomicReference();
    final long timespan;
    final TimeUnit unit;
    
    BufferExactUnboundedObserver(Observer<? super U> paramObserver, Callable<U> paramCallable, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      this.actual.onNext(paramU);
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.timer);
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.timer.get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this.timer);
      try
      {
        Collection localCollection = this.buffer;
        this.buffer = null;
        if (localCollection != null)
        {
          this.queue.offer(localCollection);
          this.done = true;
          if (enter()) {
            QueueDrainHelper.drainLoop(this.queue, this.actual, false, this, this);
          }
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.timer);
      try
      {
        this.buffer = null;
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        localCollection.add(paramT);
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        try
        {
          paramDisposable = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
          this.buffer = paramDisposable;
          this.actual.onSubscribe(this);
          if (!this.cancelled)
          {
            paramDisposable = this.scheduler;
            long l = this.timespan;
            paramDisposable = paramDisposable.schedulePeriodicallyDirect(this, l, l, this.unit);
            if (!this.timer.compareAndSet(null, paramDisposable)) {
              paramDisposable.dispose();
            }
          }
        }
        catch (Throwable paramDisposable)
        {
          Exceptions.throwIfFatal(paramDisposable);
          dispose();
          EmptyDisposable.error(paramDisposable, this.actual);
          return;
        }
      }
    }
    
    public void run()
    {
      try
      {
        Collection localCollection1 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
        try
        {
          Collection localCollection2 = this.buffer;
          if (localCollection2 != null) {
            this.buffer = localCollection1;
          }
          if (localCollection2 == null)
          {
            DisposableHelper.dispose(this.timer);
            return;
          }
          fastPathEmit(localCollection2, false, this);
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        dispose();
        this.actual.onError(localThrowable);
      }
    }
  }
  
  static final class BufferSkipBoundedObserver<T, U extends Collection<? super T>>
    extends QueueDrainObserver<T, U, U>
    implements Runnable, Disposable
  {
    final Callable<U> bufferSupplier;
    final List<U> buffers;
    Disposable s;
    final long timeskip;
    final long timespan;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    BufferSkipBoundedObserver(Observer<? super U> paramObserver, Callable<U> paramCallable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong1;
      this.timeskip = paramLong2;
      this.unit = paramTimeUnit;
      this.w = paramWorker;
      this.buffers = new LinkedList();
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      paramObserver.onNext(paramU);
    }
    
    void clear()
    {
      try
      {
        this.buffers.clear();
        return;
      }
      finally {}
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.w.dispose();
        clear();
        this.s.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      try
      {
        Object localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>(this.buffers);
        this.buffers.clear();
        Iterator localIterator = ((List)localObject1).iterator();
        while (localIterator.hasNext())
        {
          localObject1 = (Collection)localIterator.next();
          this.queue.offer(localObject1);
        }
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainLoop(this.queue, this.actual, false, this.w, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.done = true;
      this.w.dispose();
      clear();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Iterator localIterator = this.buffers.iterator();
        while (localIterator.hasNext()) {
          ((Collection)localIterator.next()).add(paramT);
        }
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        try
        {
          final Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
          this.buffers.add(localCollection);
          this.actual.onSubscribe(this);
          paramDisposable = this.w;
          long l = this.timeskip;
          paramDisposable.schedulePeriodically(this, l, l, this.unit);
          this.w.schedule(new Runnable()
          {
            public void run()
            {
              synchronized (ObservableBufferTimed.BufferSkipBoundedObserver.this)
              {
                ObservableBufferTimed.BufferSkipBoundedObserver.this.buffers.remove(localCollection);
                ??? = ObservableBufferTimed.BufferSkipBoundedObserver.this;
                ???.fastPathOrderedEmit(localCollection, false, ???.w);
                return;
              }
            }
          }, this.timespan, this.unit);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.w.dispose();
          paramDisposable.dispose();
          EmptyDisposable.error(localThrowable, this.actual);
          return;
        }
      }
    }
    
    public void run()
    {
      if (this.cancelled) {
        return;
      }
      try
      {
        final Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
        try
        {
          if (this.cancelled) {
            return;
          }
          this.buffers.add(localCollection);
          this.w.schedule(new Runnable()
          {
            public void run()
            {
              synchronized (ObservableBufferTimed.BufferSkipBoundedObserver.this)
              {
                ObservableBufferTimed.BufferSkipBoundedObserver.this.buffers.remove(localCollection);
                ??? = ObservableBufferTimed.BufferSkipBoundedObserver.this;
                ???.fastPathOrderedEmit(localCollection, false, ???.w);
                return;
              }
            }
          }, this.timespan, this.unit);
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        dispose();
        this.actual.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableBufferTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */