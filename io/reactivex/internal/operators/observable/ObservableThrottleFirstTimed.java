package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableThrottleFirstTimed<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Scheduler scheduler;
  final long timeout;
  final TimeUnit unit;
  
  public ObservableThrottleFirstTimed(ObservableSource<T> paramObservableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramObservableSource);
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new DebounceTimedObserver(new SerializedObserver(paramObserver), this.timeout, this.unit, this.scheduler.createWorker()));
  }
  
  static final class DebounceTimedObserver<T>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable, Runnable
  {
    private static final long serialVersionUID = 786994795061867455L;
    final Observer<? super T> actual;
    boolean done;
    volatile boolean gate;
    Disposable s;
    final long timeout;
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    DebounceTimedObserver(Observer<? super T> paramObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      this.actual = paramObserver;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      this.worker.dispose();
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        DisposableHelper.dispose(this);
        this.worker.dispose();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
      }
      else
      {
        this.done = true;
        DisposableHelper.dispose(this);
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if ((!this.gate) && (!this.done))
      {
        this.gate = true;
        this.actual.onNext(paramT);
        paramT = (Disposable)get();
        if (paramT != null) {
          paramT.dispose();
        }
        DisposableHelper.replace(this, this.worker.schedule(this, this.timeout, this.unit));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void run()
    {
      this.gate = false;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableThrottleFirstTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */