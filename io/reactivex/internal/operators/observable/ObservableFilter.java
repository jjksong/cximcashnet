package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.observers.BasicFuseableObserver;

public final class ObservableFilter<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Predicate<? super T> predicate;
  
  public ObservableFilter(ObservableSource<T> paramObservableSource, Predicate<? super T> paramPredicate)
  {
    super(paramObservableSource);
    this.predicate = paramPredicate;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new FilterObserver(paramObserver, this.predicate));
  }
  
  static final class FilterObserver<T>
    extends BasicFuseableObserver<T, T>
  {
    final Predicate<? super T> filter;
    
    FilterObserver(Observer<? super T> paramObserver, Predicate<? super T> paramPredicate)
    {
      super();
      this.filter = paramPredicate;
    }
    
    public void onNext(T paramT)
    {
      if (this.sourceMode == 0) {
        try
        {
          boolean bool = this.filter.test(paramT);
          if (!bool) {
            return;
          }
          this.actual.onNext(paramT);
        }
        catch (Throwable paramT)
        {
          fail(paramT);
          return;
        }
      } else {
        this.actual.onNext(null);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject;
      do
      {
        localObject = this.qs.poll();
      } while ((localObject != null) && (!this.filter.test(localObject)));
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */