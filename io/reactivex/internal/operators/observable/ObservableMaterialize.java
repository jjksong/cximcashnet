package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableMaterialize<T>
  extends AbstractObservableWithUpstream<T, Notification<T>>
{
  public ObservableMaterialize(ObservableSource<T> paramObservableSource)
  {
    super(paramObservableSource);
  }
  
  public void subscribeActual(Observer<? super Notification<T>> paramObserver)
  {
    this.source.subscribe(new MaterializeObserver(paramObserver));
  }
  
  static final class MaterializeObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super Notification<T>> actual;
    Disposable s;
    
    MaterializeObserver(Observer<? super Notification<T>> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      Notification localNotification = Notification.createOnComplete();
      this.actual.onNext(localNotification);
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      paramThrowable = Notification.createOnError(paramThrowable);
      this.actual.onNext(paramThrowable);
      this.actual.onComplete();
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(Notification.createOnNext(paramT));
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableMaterialize.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */