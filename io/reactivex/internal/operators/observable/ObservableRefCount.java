package io.reactivex.internal.operators.observable;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observables.ConnectableObservable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

public final class ObservableRefCount<T>
  extends AbstractObservableWithUpstream<T, T>
{
  volatile CompositeDisposable baseDisposable = new CompositeDisposable();
  final ReentrantLock lock = new ReentrantLock();
  final ConnectableObservable<? extends T> source;
  final AtomicInteger subscriptionCount = new AtomicInteger();
  
  public ObservableRefCount(ConnectableObservable<T> paramConnectableObservable)
  {
    super(paramConnectableObservable);
    this.source = paramConnectableObservable;
  }
  
  private Disposable disconnect(final CompositeDisposable paramCompositeDisposable)
  {
    Disposables.fromRunnable(new Runnable()
    {
      public void run()
      {
        ObservableRefCount.this.lock.lock();
        try
        {
          if ((ObservableRefCount.this.baseDisposable == paramCompositeDisposable) && (ObservableRefCount.this.subscriptionCount.decrementAndGet() == 0))
          {
            ObservableRefCount.this.baseDisposable.dispose();
            ObservableRefCount localObservableRefCount = ObservableRefCount.this;
            CompositeDisposable localCompositeDisposable = new io/reactivex/disposables/CompositeDisposable;
            localCompositeDisposable.<init>();
            localObservableRefCount.baseDisposable = localCompositeDisposable;
          }
          return;
        }
        finally
        {
          ObservableRefCount.this.lock.unlock();
        }
      }
    });
  }
  
  private Consumer<Disposable> onSubscribe(final Observer<? super T> paramObserver, final AtomicBoolean paramAtomicBoolean)
  {
    new Consumer()
    {
      public void accept(Disposable paramAnonymousDisposable)
      {
        try
        {
          ObservableRefCount.this.baseDisposable.add(paramAnonymousDisposable);
          ObservableRefCount.this.doSubscribe(paramObserver, ObservableRefCount.this.baseDisposable);
          return;
        }
        finally
        {
          ObservableRefCount.this.lock.unlock();
          paramAtomicBoolean.set(false);
        }
      }
    };
  }
  
  void doSubscribe(Observer<? super T> paramObserver, CompositeDisposable paramCompositeDisposable)
  {
    paramCompositeDisposable = new ConnectionObserver(paramObserver, paramCompositeDisposable, disconnect(paramCompositeDisposable));
    paramObserver.onSubscribe(paramCompositeDisposable);
    this.source.subscribe(paramCompositeDisposable);
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.lock.lock();
    if (this.subscriptionCount.incrementAndGet() == 1)
    {
      AtomicBoolean localAtomicBoolean = new AtomicBoolean(true);
      try
      {
        this.source.connect(onSubscribe(paramObserver, localAtomicBoolean));
        if (localAtomicBoolean.get()) {
          this.lock.unlock();
        }
      }
      finally
      {
        if (localAtomicBoolean.get()) {
          this.lock.unlock();
        }
      }
    }
    try
    {
      doSubscribe(paramObserver, this.baseDisposable);
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  final class ConnectionObserver
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 3813126992133394324L;
    final CompositeDisposable currentBase;
    final Disposable resource;
    final Observer<? super T> subscriber;
    
    ConnectionObserver(CompositeDisposable paramCompositeDisposable, Disposable paramDisposable)
    {
      this.subscriber = paramCompositeDisposable;
      this.currentBase = paramDisposable;
      Disposable localDisposable;
      this.resource = localDisposable;
    }
    
    void cleanup()
    {
      ObservableRefCount.this.lock.lock();
      try
      {
        if (ObservableRefCount.this.baseDisposable == this.currentBase)
        {
          ObservableRefCount.this.baseDisposable.dispose();
          ObservableRefCount localObservableRefCount = ObservableRefCount.this;
          CompositeDisposable localCompositeDisposable = new io/reactivex/disposables/CompositeDisposable;
          localCompositeDisposable.<init>();
          localObservableRefCount.baseDisposable = localCompositeDisposable;
          ObservableRefCount.this.subscriptionCount.set(0);
        }
        return;
      }
      finally
      {
        ObservableRefCount.this.lock.unlock();
      }
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      this.resource.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      cleanup();
      this.subscriber.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      cleanup();
      this.subscriber.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.subscriber.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableRefCount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */