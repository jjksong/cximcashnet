package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.NoSuchElementException;

public final class ObservableLastSingle<T>
  extends Single<T>
{
  final T defaultItem;
  final ObservableSource<T> source;
  
  public ObservableLastSingle(ObservableSource<T> paramObservableSource, T paramT)
  {
    this.source = paramObservableSource;
    this.defaultItem = paramT;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new LastObserver(paramSingleObserver, this.defaultItem));
  }
  
  static final class LastObserver<T>
    implements Observer<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    final T defaultItem;
    T item;
    Disposable s;
    
    LastObserver(SingleObserver<? super T> paramSingleObserver, T paramT)
    {
      this.actual = paramSingleObserver;
      this.defaultItem = paramT;
    }
    
    public void dispose()
    {
      this.s.dispose();
      this.s = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = DisposableHelper.DISPOSED;
      Object localObject = this.item;
      if (localObject != null)
      {
        this.item = null;
        this.actual.onSuccess(localObject);
      }
      else
      {
        localObject = this.defaultItem;
        if (localObject != null) {
          this.actual.onSuccess(localObject);
        } else {
          this.actual.onError(new NoSuchElementException());
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = DisposableHelper.DISPOSED;
      this.item = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.item = paramT;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableLastSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */