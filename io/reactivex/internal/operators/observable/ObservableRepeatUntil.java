package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.internal.disposables.SequentialDisposable;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableRepeatUntil<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final BooleanSupplier until;
  
  public ObservableRepeatUntil(Observable<T> paramObservable, BooleanSupplier paramBooleanSupplier)
  {
    super(paramObservable);
    this.until = paramBooleanSupplier;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    SequentialDisposable localSequentialDisposable = new SequentialDisposable();
    paramObserver.onSubscribe(localSequentialDisposable);
    new RepeatUntilObserver(paramObserver, this.until, localSequentialDisposable, this.source).subscribeNext();
  }
  
  static final class RepeatUntilObserver<T>
    extends AtomicInteger
    implements Observer<T>
  {
    private static final long serialVersionUID = -7098360935104053232L;
    final Observer<? super T> actual;
    final SequentialDisposable sd;
    final ObservableSource<? extends T> source;
    final BooleanSupplier stop;
    
    RepeatUntilObserver(Observer<? super T> paramObserver, BooleanSupplier paramBooleanSupplier, SequentialDisposable paramSequentialDisposable, ObservableSource<? extends T> paramObservableSource)
    {
      this.actual = paramObserver;
      this.sd = paramSequentialDisposable;
      this.source = paramObservableSource;
      this.stop = paramBooleanSupplier;
    }
    
    public void onComplete()
    {
      try
      {
        boolean bool = this.stop.getAsBoolean();
        if (bool) {
          this.actual.onComplete();
        } else {
          subscribeNext();
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.sd.replace(paramDisposable);
    }
    
    void subscribeNext()
    {
      if (getAndIncrement() == 0)
      {
        int i = 1;
        int j;
        do
        {
          this.source.subscribe(this);
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableRepeatUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */