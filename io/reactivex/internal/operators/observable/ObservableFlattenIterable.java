package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;

public final class ObservableFlattenIterable<T, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final Function<? super T, ? extends Iterable<? extends R>> mapper;
  
  public ObservableFlattenIterable(ObservableSource<T> paramObservableSource, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
  {
    super(paramObservableSource);
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    this.source.subscribe(new FlattenIterableObserver(paramObserver, this.mapper));
  }
  
  static final class FlattenIterableObserver<T, R>
    implements Observer<T>, Disposable
  {
    final Observer<? super R> actual;
    Disposable d;
    final Function<? super T, ? extends Iterable<? extends R>> mapper;
    
    FlattenIterableObserver(Observer<? super R> paramObserver, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.d == DisposableHelper.DISPOSED) {
        return;
      }
      this.d = DisposableHelper.DISPOSED;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.d == DisposableHelper.DISPOSED)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.d == DisposableHelper.DISPOSED) {
        return;
      }
      try
      {
        Iterator localIterator = ((Iterable)this.mapper.apply(paramT)).iterator();
        Observer localObserver = this.actual;
        try
        {
          for (;;)
          {
            boolean bool = localIterator.hasNext();
            if (bool) {
              try
              {
                paramT = ObjectHelper.requireNonNull(localIterator.next(), "The iterator returned a null value");
                localObserver.onNext(paramT);
              }
              catch (Throwable paramT)
              {
                Exceptions.throwIfFatal(paramT);
                this.d.dispose();
                onError(paramT);
                return;
              }
            }
          }
          return;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.d.dispose();
          onError(paramT);
          return;
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.d.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableFlattenIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */