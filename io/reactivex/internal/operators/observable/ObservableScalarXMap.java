package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableScalarXMap
{
  private ObservableScalarXMap()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T, U> Observable<U> scalarXMap(T paramT, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction)
  {
    return RxJavaPlugins.onAssembly(new ScalarXMapObservable(paramT, paramFunction));
  }
  
  public static <T, R> boolean tryScalarXMapSubscribe(ObservableSource<T> paramObservableSource, Observer<? super R> paramObserver, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction)
  {
    if ((paramObservableSource instanceof Callable)) {
      try
      {
        paramObservableSource = ((Callable)paramObservableSource).call();
        if (paramObservableSource == null)
        {
          EmptyDisposable.complete(paramObserver);
          return true;
        }
        try
        {
          paramObservableSource = (ObservableSource)ObjectHelper.requireNonNull(paramFunction.apply(paramObservableSource), "The mapper returned a null ObservableSource");
          if ((paramObservableSource instanceof Callable)) {
            try
            {
              paramObservableSource = ((Callable)paramObservableSource).call();
              if (paramObservableSource == null)
              {
                EmptyDisposable.complete(paramObserver);
                return true;
              }
              paramObservableSource = new ScalarDisposable(paramObserver, paramObservableSource);
              paramObserver.onSubscribe(paramObservableSource);
              paramObservableSource.run();
            }
            catch (Throwable paramObservableSource)
            {
              Exceptions.throwIfFatal(paramObservableSource);
              EmptyDisposable.error(paramObservableSource, paramObserver);
              return true;
            }
          }
          paramObservableSource.subscribe(paramObserver);
          return true;
        }
        catch (Throwable paramObservableSource)
        {
          Exceptions.throwIfFatal(paramObservableSource);
          EmptyDisposable.error(paramObservableSource, paramObserver);
          return true;
        }
        return false;
      }
      catch (Throwable paramObservableSource)
      {
        Exceptions.throwIfFatal(paramObservableSource);
        EmptyDisposable.error(paramObservableSource, paramObserver);
        return true;
      }
    }
  }
  
  public static final class ScalarDisposable<T>
    extends AtomicInteger
    implements QueueDisposable<T>, Runnable
  {
    static final int FUSED = 1;
    static final int ON_COMPLETE = 3;
    static final int ON_NEXT = 2;
    static final int START = 0;
    private static final long serialVersionUID = 3880992722410194083L;
    final Observer<? super T> observer;
    final T value;
    
    public ScalarDisposable(Observer<? super T> paramObserver, T paramT)
    {
      this.observer = paramObserver;
      this.value = paramT;
    }
    
    public void clear()
    {
      lazySet(3);
    }
    
    public void dispose()
    {
      set(3);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == 3) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public boolean isEmpty()
    {
      int i = get();
      boolean bool = true;
      if (i == 1) {
        bool = false;
      }
      return bool;
    }
    
    public boolean offer(T paramT)
    {
      throw new UnsupportedOperationException("Should not be called!");
    }
    
    public boolean offer(T paramT1, T paramT2)
    {
      throw new UnsupportedOperationException("Should not be called!");
    }
    
    public T poll()
      throws Exception
    {
      if (get() == 1)
      {
        lazySet(3);
        return (T)this.value;
      }
      return null;
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x1) != 0)
      {
        lazySet(1);
        return 1;
      }
      return 0;
    }
    
    public void run()
    {
      if ((get() == 0) && (compareAndSet(0, 2)))
      {
        this.observer.onNext(this.value);
        if (get() == 2)
        {
          lazySet(3);
          this.observer.onComplete();
        }
      }
    }
  }
  
  static final class ScalarXMapObservable<T, R>
    extends Observable<R>
  {
    final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
    final T value;
    
    ScalarXMapObservable(T paramT, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction)
    {
      this.value = paramT;
      this.mapper = paramFunction;
    }
    
    public void subscribeActual(Observer<? super R> paramObserver)
    {
      try
      {
        Object localObject = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(this.value), "The mapper returned a null ObservableSource");
        if ((localObject instanceof Callable)) {
          try
          {
            localObject = ((Callable)localObject).call();
            if (localObject == null)
            {
              EmptyDisposable.complete(paramObserver);
              return;
            }
            localObject = new ObservableScalarXMap.ScalarDisposable(paramObserver, localObject);
            paramObserver.onSubscribe((Disposable)localObject);
            ((ObservableScalarXMap.ScalarDisposable)localObject).run();
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            EmptyDisposable.error(localThrowable1, paramObserver);
            return;
          }
        }
        localThrowable1.subscribe(paramObserver);
        return;
      }
      catch (Throwable localThrowable2)
      {
        EmptyDisposable.error(localThrowable2, paramObserver);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableScalarXMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */