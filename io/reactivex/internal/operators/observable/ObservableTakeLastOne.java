package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableTakeLastOne<T>
  extends AbstractObservableWithUpstream<T, T>
{
  public ObservableTakeLastOne(ObservableSource<T> paramObservableSource)
  {
    super(paramObservableSource);
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new TakeLastOneObserver(paramObserver));
  }
  
  static final class TakeLastOneObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    Disposable s;
    T value;
    
    TakeLastOneObserver(Observer<? super T> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.value = null;
      this.s.dispose();
    }
    
    void emit()
    {
      Object localObject = this.value;
      if (localObject != null)
      {
        this.value = null;
        this.actual.onNext(localObject);
      }
      this.actual.onComplete();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      emit();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.value = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.value = paramT;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTakeLastOne.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */