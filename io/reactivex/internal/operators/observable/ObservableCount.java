package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableCount<T>
  extends AbstractObservableWithUpstream<T, Long>
{
  public ObservableCount(ObservableSource<T> paramObservableSource)
  {
    super(paramObservableSource);
  }
  
  public void subscribeActual(Observer<? super Long> paramObserver)
  {
    this.source.subscribe(new CountObserver(paramObserver));
  }
  
  static final class CountObserver
    implements Observer<Object>, Disposable
  {
    final Observer<? super Long> actual;
    long count;
    Disposable s;
    
    CountObserver(Observer<? super Long> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onNext(Long.valueOf(this.count));
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.count += 1L;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */