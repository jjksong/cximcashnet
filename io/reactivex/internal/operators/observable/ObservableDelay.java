package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.TimeUnit;

public final class ObservableDelay<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final long delay;
  final boolean delayError;
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public ObservableDelay(ObservableSource<T> paramObservableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.delay = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.delayError = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    if (!this.delayError) {
      paramObserver = new SerializedObserver(paramObserver);
    }
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    this.source.subscribe(new DelayObserver(paramObserver, this.delay, this.unit, localWorker, this.delayError));
  }
  
  static final class DelayObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    final long delay;
    final boolean delayError;
    Disposable s;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    DelayObserver(Observer<? super T> paramObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.delay = paramLong;
      this.unit = paramTimeUnit;
      this.w = paramWorker;
      this.delayError = paramBoolean;
    }
    
    public void dispose()
    {
      this.w.dispose();
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.w.isDisposed();
    }
    
    public void onComplete()
    {
      this.w.schedule(new Runnable()
      {
        public void run()
        {
          try
          {
            ObservableDelay.DelayObserver.this.actual.onComplete();
            return;
          }
          finally
          {
            ObservableDelay.DelayObserver.this.w.dispose();
          }
        }
      }, this.delay, this.unit);
    }
    
    public void onError(final Throwable paramThrowable)
    {
      Scheduler.Worker localWorker = this.w;
      paramThrowable = new Runnable()
      {
        public void run()
        {
          try
          {
            ObservableDelay.DelayObserver.this.actual.onError(paramThrowable);
            return;
          }
          finally
          {
            ObservableDelay.DelayObserver.this.w.dispose();
          }
        }
      };
      long l;
      if (this.delayError) {
        l = this.delay;
      } else {
        l = 0L;
      }
      localWorker.schedule(paramThrowable, l, this.unit);
    }
    
    public void onNext(final T paramT)
    {
      this.w.schedule(new Runnable()
      {
        public void run()
        {
          ObservableDelay.DelayObserver.this.actual.onNext(paramT);
        }
      }, this.delay, this.unit);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDelay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */