package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableJoin<TLeft, TRight, TLeftEnd, TRightEnd, R>
  extends AbstractObservableWithUpstream<TLeft, R>
{
  final Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> leftEnd;
  final ObservableSource<? extends TRight> other;
  final BiFunction<? super TLeft, ? super TRight, ? extends R> resultSelector;
  final Function<? super TRight, ? extends ObservableSource<TRightEnd>> rightEnd;
  
  public ObservableJoin(ObservableSource<TLeft> paramObservableSource, ObservableSource<? extends TRight> paramObservableSource1, Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> paramFunction, Function<? super TRight, ? extends ObservableSource<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super TRight, ? extends R> paramBiFunction)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
    this.leftEnd = paramFunction;
    this.rightEnd = paramFunction1;
    this.resultSelector = paramBiFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    JoinDisposable localJoinDisposable = new JoinDisposable(paramObserver, this.leftEnd, this.rightEnd, this.resultSelector);
    paramObserver.onSubscribe(localJoinDisposable);
    paramObserver = new ObservableGroupJoin.LeftRightObserver(localJoinDisposable, true);
    localJoinDisposable.disposables.add(paramObserver);
    ObservableGroupJoin.LeftRightObserver localLeftRightObserver = new ObservableGroupJoin.LeftRightObserver(localJoinDisposable, false);
    localJoinDisposable.disposables.add(localLeftRightObserver);
    this.source.subscribe(paramObserver);
    this.other.subscribe(localLeftRightObserver);
  }
  
  static final class JoinDisposable<TLeft, TRight, TLeftEnd, TRightEnd, R>
    extends AtomicInteger
    implements Disposable, ObservableGroupJoin.JoinSupport
  {
    static final Integer LEFT_CLOSE = Integer.valueOf(3);
    static final Integer LEFT_VALUE = Integer.valueOf(1);
    static final Integer RIGHT_CLOSE = Integer.valueOf(4);
    static final Integer RIGHT_VALUE = Integer.valueOf(2);
    private static final long serialVersionUID = -6071216598687999801L;
    final AtomicInteger active;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    final CompositeDisposable disposables;
    final AtomicReference<Throwable> error;
    final Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> leftEnd;
    int leftIndex;
    final Map<Integer, TLeft> lefts;
    final SpscLinkedArrayQueue<Object> queue;
    final BiFunction<? super TLeft, ? super TRight, ? extends R> resultSelector;
    final Function<? super TRight, ? extends ObservableSource<TRightEnd>> rightEnd;
    int rightIndex;
    final Map<Integer, TRight> rights;
    
    JoinDisposable(Observer<? super R> paramObserver, Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> paramFunction, Function<? super TRight, ? extends ObservableSource<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super TRight, ? extends R> paramBiFunction)
    {
      this.actual = paramObserver;
      this.disposables = new CompositeDisposable();
      this.queue = new SpscLinkedArrayQueue(Observable.bufferSize());
      this.lefts = new LinkedHashMap();
      this.rights = new LinkedHashMap();
      this.error = new AtomicReference();
      this.leftEnd = paramFunction;
      this.rightEnd = paramFunction1;
      this.resultSelector = paramBiFunction;
      this.active = new AtomicInteger(2);
    }
    
    void cancelAll()
    {
      this.disposables.dispose();
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        cancelAll();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Observer localObserver = this.actual;
      int i = 1;
      for (;;)
      {
        if (this.cancelled)
        {
          localSpscLinkedArrayQueue.clear();
          return;
        }
        if ((Throwable)this.error.get() != null)
        {
          localSpscLinkedArrayQueue.clear();
          cancelAll();
          errorAll(localObserver);
          return;
        }
        int j;
        if (this.active.get() == 0) {
          j = 1;
        } else {
          j = 0;
        }
        Object localObject2 = (Integer)localSpscLinkedArrayQueue.poll();
        int k;
        if (localObject2 == null) {
          k = 1;
        } else {
          k = 0;
        }
        if ((j != 0) && (k != 0))
        {
          this.lefts.clear();
          this.rights.clear();
          this.disposables.dispose();
          localObserver.onComplete();
          return;
        }
        if (k != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          Object localObject1 = localSpscLinkedArrayQueue.poll();
          Object localObject3;
          if (localObject2 == LEFT_VALUE)
          {
            j = this.leftIndex;
            this.leftIndex = (j + 1);
            this.lefts.put(Integer.valueOf(j), localObject1);
            try
            {
              localObject2 = (ObservableSource)ObjectHelper.requireNonNull(this.leftEnd.apply(localObject1), "The leftEnd returned a null ObservableSource");
              localObject3 = new ObservableGroupJoin.LeftRightEndObserver(this, true, j);
              this.disposables.add((Disposable)localObject3);
              ((ObservableSource)localObject2).subscribe((Observer)localObject3);
              if ((Throwable)this.error.get() != null)
              {
                localSpscLinkedArrayQueue.clear();
                cancelAll();
                errorAll(localObserver);
                return;
              }
              localObject2 = this.rights.values().iterator();
              while (((Iterator)localObject2).hasNext())
              {
                localObject3 = ((Iterator)localObject2).next();
                try
                {
                  localObject3 = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject1, localObject3), "The resultSelector returned a null value");
                  localObserver.onNext(localObject3);
                }
                catch (Throwable localThrowable1)
                {
                  fail(localThrowable1, localObserver, localSpscLinkedArrayQueue);
                  return;
                }
              }
              if (localObject2 != RIGHT_VALUE) {
                break label589;
              }
            }
            catch (Throwable localThrowable2)
            {
              fail(localThrowable2, localObserver, localSpscLinkedArrayQueue);
              return;
            }
          }
          else
          {
            j = this.rightIndex;
            this.rightIndex = (j + 1);
            this.rights.put(Integer.valueOf(j), localThrowable2);
            try
            {
              localObject3 = (ObservableSource)ObjectHelper.requireNonNull(this.rightEnd.apply(localThrowable2), "The rightEnd returned a null ObservableSource");
              localObject2 = new ObservableGroupJoin.LeftRightEndObserver(this, false, j);
              this.disposables.add((Disposable)localObject2);
              ((ObservableSource)localObject3).subscribe((Observer)localObject2);
              if ((Throwable)this.error.get() != null)
              {
                localSpscLinkedArrayQueue.clear();
                cancelAll();
                errorAll(localObserver);
                return;
              }
              localObject2 = this.lefts.values().iterator();
              while (((Iterator)localObject2).hasNext())
              {
                localObject3 = ((Iterator)localObject2).next();
                try
                {
                  localObject3 = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject3, localThrowable2), "The resultSelector returned a null value");
                  localObserver.onNext(localObject3);
                }
                catch (Throwable localThrowable3)
                {
                  fail(localThrowable3, localObserver, localSpscLinkedArrayQueue);
                  return;
                }
              }
              if (localObject2 != LEFT_CLOSE) {
                break label635;
              }
            }
            catch (Throwable localThrowable4)
            {
              fail(localThrowable4, localObserver, localSpscLinkedArrayQueue);
              return;
            }
            label589:
            ObservableGroupJoin.LeftRightEndObserver localLeftRightEndObserver = (ObservableGroupJoin.LeftRightEndObserver)localThrowable4;
            this.lefts.remove(Integer.valueOf(localLeftRightEndObserver.index));
            this.disposables.remove(localLeftRightEndObserver);
            continue;
            label635:
            localLeftRightEndObserver = (ObservableGroupJoin.LeftRightEndObserver)localLeftRightEndObserver;
            this.rights.remove(Integer.valueOf(localLeftRightEndObserver.index));
            this.disposables.remove(localLeftRightEndObserver);
          }
        }
      }
    }
    
    void errorAll(Observer<?> paramObserver)
    {
      Throwable localThrowable = ExceptionHelper.terminate(this.error);
      this.lefts.clear();
      this.rights.clear();
      paramObserver.onError(localThrowable);
    }
    
    void fail(Throwable paramThrowable, Observer<?> paramObserver, SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      Exceptions.throwIfFatal(paramThrowable);
      ExceptionHelper.addThrowable(this.error, paramThrowable);
      paramSpscLinkedArrayQueue.clear();
      cancelAll();
      errorAll(paramObserver);
    }
    
    public void innerClose(boolean paramBoolean, ObservableGroupJoin.LeftRightEndObserver paramLeftRightEndObserver)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_CLOSE;
        } else {
          localInteger = RIGHT_CLOSE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramLeftRightEndObserver);
        drain();
        return;
      }
      finally {}
    }
    
    public void innerCloseError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerComplete(ObservableGroupJoin.LeftRightObserver paramLeftRightObserver)
    {
      this.disposables.delete(paramLeftRightObserver);
      this.active.decrementAndGet();
      drain();
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable))
      {
        this.active.decrementAndGet();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerValue(boolean paramBoolean, Object paramObject)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_VALUE;
        } else {
          localInteger = RIGHT_VALUE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramObject);
        drain();
        return;
      }
      finally {}
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */