package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowBoundarySelector<T, B, V>
  extends AbstractObservableWithUpstream<T, Observable<T>>
{
  final int bufferSize;
  final Function<? super B, ? extends ObservableSource<V>> close;
  final ObservableSource<B> open;
  
  public ObservableWindowBoundarySelector(ObservableSource<T> paramObservableSource, ObservableSource<B> paramObservableSource1, Function<? super B, ? extends ObservableSource<V>> paramFunction, int paramInt)
  {
    super(paramObservableSource);
    this.open = paramObservableSource1;
    this.close = paramFunction;
    this.bufferSize = paramInt;
  }
  
  public void subscribeActual(Observer<? super Observable<T>> paramObserver)
  {
    this.source.subscribe(new WindowBoundaryMainObserver(new SerializedObserver(paramObserver), this.open, this.close, this.bufferSize));
  }
  
  static final class OperatorWindowBoundaryCloseObserver<T, V>
    extends DisposableObserver<V>
  {
    boolean done;
    final ObservableWindowBoundarySelector.WindowBoundaryMainObserver<T, ?, V> parent;
    final UnicastSubject<T> w;
    
    OperatorWindowBoundaryCloseObserver(ObservableWindowBoundarySelector.WindowBoundaryMainObserver<T, ?, V> paramWindowBoundaryMainObserver, UnicastSubject<T> paramUnicastSubject)
    {
      this.parent = paramWindowBoundaryMainObserver;
      this.w = paramUnicastSubject;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.close(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.error(paramThrowable);
    }
    
    public void onNext(V paramV)
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.parent.close(this);
    }
  }
  
  static final class OperatorWindowBoundaryOpenObserver<T, B>
    extends DisposableObserver<B>
  {
    final ObservableWindowBoundarySelector.WindowBoundaryMainObserver<T, B, ?> parent;
    
    OperatorWindowBoundaryOpenObserver(ObservableWindowBoundarySelector.WindowBoundaryMainObserver<T, B, ?> paramWindowBoundaryMainObserver)
    {
      this.parent = paramWindowBoundaryMainObserver;
    }
    
    public void onComplete()
    {
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.error(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      this.parent.open(paramB);
    }
  }
  
  static final class WindowBoundaryMainObserver<T, B, V>
    extends QueueDrainObserver<T, Object, Observable<T>>
    implements Disposable
  {
    final AtomicReference<Disposable> boundary = new AtomicReference();
    final int bufferSize;
    final Function<? super B, ? extends ObservableSource<V>> close;
    final ObservableSource<B> open;
    final CompositeDisposable resources;
    Disposable s;
    final AtomicLong windows = new AtomicLong();
    final List<UnicastSubject<T>> ws;
    
    WindowBoundaryMainObserver(Observer<? super Observable<T>> paramObserver, ObservableSource<B> paramObservableSource, Function<? super B, ? extends ObservableSource<V>> paramFunction, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.open = paramObservableSource;
      this.close = paramFunction;
      this.bufferSize = paramInt;
      this.resources = new CompositeDisposable();
      this.ws = new ArrayList();
      this.windows.lazySet(1L);
    }
    
    public void accept(Observer<? super Observable<T>> paramObserver, Object paramObject) {}
    
    void close(ObservableWindowBoundarySelector.OperatorWindowBoundaryCloseObserver<T, V> paramOperatorWindowBoundaryCloseObserver)
    {
      this.resources.delete(paramOperatorWindowBoundaryCloseObserver);
      this.queue.offer(new ObservableWindowBoundarySelector.WindowOperation(paramOperatorWindowBoundaryCloseObserver.w, null));
      if (enter()) {
        drainLoop();
      }
    }
    
    public void dispose()
    {
      this.cancelled = true;
    }
    
    void disposeBoundary()
    {
      this.resources.dispose();
      DisposableHelper.dispose(this.boundary);
    }
    
    void drainLoop()
    {
      Object localObject2 = (MpscLinkedQueue)this.queue;
      Object localObject1 = this.actual;
      List localList = this.ws;
      int i = 1;
      for (;;)
      {
        boolean bool = this.done;
        Object localObject4 = ((MpscLinkedQueue)localObject2).poll();
        int j;
        if (localObject4 == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          disposeBoundary();
          localObject2 = this.error;
          if (localObject2 != null)
          {
            localObject1 = localList.iterator();
            while (((Iterator)localObject1).hasNext()) {
              ((UnicastSubject)((Iterator)localObject1).next()).onError((Throwable)localObject2);
            }
          }
          localObject1 = localList.iterator();
          while (((Iterator)localObject1).hasNext()) {
            ((UnicastSubject)((Iterator)localObject1).next()).onComplete();
          }
          localList.clear();
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if ((localObject4 instanceof ObservableWindowBoundarySelector.WindowOperation))
        {
          Object localObject3 = (ObservableWindowBoundarySelector.WindowOperation)localObject4;
          if (((ObservableWindowBoundarySelector.WindowOperation)localObject3).w != null)
          {
            if (localList.remove(((ObservableWindowBoundarySelector.WindowOperation)localObject3).w))
            {
              ((ObservableWindowBoundarySelector.WindowOperation)localObject3).w.onComplete();
              if (this.windows.decrementAndGet() == 0L) {
                disposeBoundary();
              }
            }
          }
          else if (!this.cancelled)
          {
            localObject4 = UnicastSubject.create(this.bufferSize);
            localList.add(localObject4);
            ((Observer)localObject1).onNext(localObject4);
            try
            {
              localObject3 = (ObservableSource)ObjectHelper.requireNonNull(this.close.apply(((ObservableWindowBoundarySelector.WindowOperation)localObject3).open), "The ObservableSource supplied is null");
              localObject4 = new ObservableWindowBoundarySelector.OperatorWindowBoundaryCloseObserver(this, (UnicastSubject)localObject4);
              if (!this.resources.add((Disposable)localObject4)) {
                continue;
              }
              this.windows.getAndIncrement();
              ((ObservableSource)localObject3).subscribe((Observer)localObject4);
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              this.cancelled = true;
              ((Observer)localObject1).onError(localThrowable);
            }
          }
        }
        else
        {
          Iterator localIterator = localList.iterator();
          while (localIterator.hasNext()) {
            ((UnicastSubject)localIterator.next()).onNext(NotificationLite.getValue(localObject4));
          }
        }
      }
    }
    
    void error(Throwable paramThrowable)
    {
      this.s.dispose();
      this.resources.dispose();
      onError(paramThrowable);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        this.resources.dispose();
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        this.resources.dispose();
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (fastEnter())
      {
        Iterator localIterator = this.ws.iterator();
        while (localIterator.hasNext()) {
          ((UnicastSubject)localIterator.next()).onNext(paramT);
        }
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        paramDisposable = new ObservableWindowBoundarySelector.OperatorWindowBoundaryOpenObserver(this);
        if (this.boundary.compareAndSet(null, paramDisposable))
        {
          this.windows.getAndIncrement();
          this.open.subscribe(paramDisposable);
        }
      }
    }
    
    void open(B paramB)
    {
      this.queue.offer(new ObservableWindowBoundarySelector.WindowOperation(null, paramB));
      if (enter()) {
        drainLoop();
      }
    }
  }
  
  static final class WindowOperation<T, B>
  {
    final B open;
    final UnicastSubject<T> w;
    
    WindowOperation(UnicastSubject<T> paramUnicastSubject, B paramB)
    {
      this.w = paramUnicastSubject;
      this.open = paramB;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableWindowBoundarySelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */