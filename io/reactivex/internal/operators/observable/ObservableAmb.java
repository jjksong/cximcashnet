package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableAmb<T>
  extends Observable<T>
{
  final ObservableSource<? extends T>[] sources;
  final Iterable<? extends ObservableSource<? extends T>> sourcesIterable;
  
  public ObservableAmb(ObservableSource<? extends T>[] paramArrayOfObservableSource, Iterable<? extends ObservableSource<? extends T>> paramIterable)
  {
    this.sources = paramArrayOfObservableSource;
    this.sourcesIterable = paramIterable;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    Object localObject2 = this.sources;
    int j;
    if (localObject2 == null)
    {
      Object localObject1 = new Observable[8];
      try
      {
        Iterator localIterator = this.sourcesIterable.iterator();
        int i = 0;
        for (;;)
        {
          localObject2 = localObject1;
          j = i;
          if (!localIterator.hasNext()) {
            break;
          }
          ObservableSource localObservableSource = (ObservableSource)localIterator.next();
          if (localObservableSource == null)
          {
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("One of the sources is null");
            EmptyDisposable.error((Throwable)localObject1, paramObserver);
            return;
          }
          localObject2 = localObject1;
          if (i == localObject1.length)
          {
            localObject2 = new ObservableSource[(i >> 2) + i];
            System.arraycopy(localObject1, 0, localObject2, 0, i);
          }
          localObject2[i] = localObservableSource;
          i++;
          localObject1 = localObject2;
        }
        j = localObject2.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptyDisposable.error(localThrowable, paramObserver);
        return;
      }
    }
    if (j == 0)
    {
      EmptyDisposable.complete(paramObserver);
      return;
    }
    if (j == 1)
    {
      localObject2[0].subscribe(paramObserver);
      return;
    }
    new AmbCoordinator(paramObserver, j).subscribe((ObservableSource[])localObject2);
  }
  
  static final class AmbCoordinator<T>
    implements Disposable
  {
    final Observer<? super T> actual;
    final ObservableAmb.AmbInnerObserver<T>[] observers;
    final AtomicInteger winner = new AtomicInteger();
    
    AmbCoordinator(Observer<? super T> paramObserver, int paramInt)
    {
      this.actual = paramObserver;
      this.observers = new ObservableAmb.AmbInnerObserver[paramInt];
    }
    
    public void dispose()
    {
      if (this.winner.get() != -1)
      {
        this.winner.lazySet(-1);
        ObservableAmb.AmbInnerObserver[] arrayOfAmbInnerObserver = this.observers;
        int j = arrayOfAmbInnerObserver.length;
        for (int i = 0; i < j; i++) {
          arrayOfAmbInnerObserver[i].dispose();
        }
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.winner.get() == -1) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void subscribe(ObservableSource<? extends T>[] paramArrayOfObservableSource)
    {
      ObservableAmb.AmbInnerObserver[] arrayOfAmbInnerObserver = this.observers;
      int m = arrayOfAmbInnerObserver.length;
      int j = 0;
      int k;
      for (int i = 0; i < m; i = k)
      {
        k = i + 1;
        arrayOfAmbInnerObserver[i] = new ObservableAmb.AmbInnerObserver(this, k, this.actual);
      }
      this.winner.lazySet(0);
      this.actual.onSubscribe(this);
      for (i = j; i < m; i++)
      {
        if (this.winner.get() != 0) {
          return;
        }
        paramArrayOfObservableSource[i].subscribe(arrayOfAmbInnerObserver[i]);
      }
    }
    
    public boolean win(int paramInt)
    {
      int j = this.winner.get();
      boolean bool = true;
      int i = 0;
      if (j == 0)
      {
        if (this.winner.compareAndSet(0, paramInt))
        {
          ObservableAmb.AmbInnerObserver[] arrayOfAmbInnerObserver = this.observers;
          int k = arrayOfAmbInnerObserver.length;
          while (i < k)
          {
            j = i + 1;
            if (j != paramInt) {
              arrayOfAmbInnerObserver[i].dispose();
            }
            i = j;
          }
          return true;
        }
        return false;
      }
      if (j != paramInt) {
        bool = false;
      }
      return bool;
    }
  }
  
  static final class AmbInnerObserver<T>
    extends AtomicReference<Disposable>
    implements Observer<T>
  {
    private static final long serialVersionUID = -1185974347409665484L;
    final Observer<? super T> actual;
    final int index;
    final ObservableAmb.AmbCoordinator<T> parent;
    boolean won;
    
    AmbInnerObserver(ObservableAmb.AmbCoordinator<T> paramAmbCoordinator, int paramInt, Observer<? super T> paramObserver)
    {
      this.parent = paramAmbCoordinator;
      this.index = paramInt;
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public void onComplete()
    {
      if (this.won)
      {
        this.actual.onComplete();
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.won)
      {
        this.actual.onError(paramThrowable);
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.won)
      {
        this.actual.onNext(paramT);
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onNext(paramT);
      }
      else
      {
        ((Disposable)get()).dispose();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableAmb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */