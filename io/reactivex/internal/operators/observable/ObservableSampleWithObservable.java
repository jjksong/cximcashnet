package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableSampleWithObservable<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final boolean emitLast;
  final ObservableSource<?> other;
  
  public ObservableSampleWithObservable(ObservableSource<T> paramObservableSource, ObservableSource<?> paramObservableSource1, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
    this.emitLast = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    paramObserver = new SerializedObserver(paramObserver);
    if (this.emitLast) {
      this.source.subscribe(new SampleMainEmitLast(paramObserver, this.other));
    } else {
      this.source.subscribe(new SampleMainNoLast(paramObserver, this.other));
    }
  }
  
  static final class SampleMainEmitLast<T>
    extends ObservableSampleWithObservable.SampleMainObserver<T>
  {
    private static final long serialVersionUID = -3029755663834015785L;
    volatile boolean done;
    final AtomicInteger wip = new AtomicInteger();
    
    SampleMainEmitLast(Observer<? super T> paramObserver, ObservableSource<?> paramObservableSource)
    {
      super(paramObservableSource);
    }
    
    void completeMain()
    {
      this.done = true;
      if (this.wip.getAndIncrement() == 0)
      {
        emit();
        this.actual.onComplete();
      }
    }
    
    void completeOther()
    {
      this.done = true;
      if (this.wip.getAndIncrement() == 0)
      {
        emit();
        this.actual.onComplete();
      }
    }
    
    void run()
    {
      if (this.wip.getAndIncrement() == 0) {
        do
        {
          boolean bool = this.done;
          emit();
          if (bool)
          {
            this.actual.onComplete();
            return;
          }
        } while (this.wip.decrementAndGet() != 0);
      }
    }
  }
  
  static final class SampleMainNoLast<T>
    extends ObservableSampleWithObservable.SampleMainObserver<T>
  {
    private static final long serialVersionUID = -3029755663834015785L;
    
    SampleMainNoLast(Observer<? super T> paramObserver, ObservableSource<?> paramObservableSource)
    {
      super(paramObservableSource);
    }
    
    void completeMain()
    {
      this.actual.onComplete();
    }
    
    void completeOther()
    {
      this.actual.onComplete();
    }
    
    void run()
    {
      emit();
    }
  }
  
  static abstract class SampleMainObserver<T>
    extends AtomicReference<T>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -3517602651313910099L;
    final Observer<? super T> actual;
    final AtomicReference<Disposable> other = new AtomicReference();
    Disposable s;
    final ObservableSource<?> sampler;
    
    SampleMainObserver(Observer<? super T> paramObserver, ObservableSource<?> paramObservableSource)
    {
      this.actual = paramObserver;
      this.sampler = paramObservableSource;
    }
    
    public void complete()
    {
      this.s.dispose();
      completeOther();
    }
    
    abstract void completeMain();
    
    abstract void completeOther();
    
    public void dispose()
    {
      DisposableHelper.dispose(this.other);
      this.s.dispose();
    }
    
    void emit()
    {
      Object localObject = getAndSet(null);
      if (localObject != null) {
        this.actual.onNext(localObject);
      }
    }
    
    public void error(Throwable paramThrowable)
    {
      this.s.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.other.get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this.other);
      completeMain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.other);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      lazySet(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
        if (this.other.get() == null) {
          this.sampler.subscribe(new ObservableSampleWithObservable.SamplerObserver(this));
        }
      }
    }
    
    abstract void run();
    
    boolean setOther(Disposable paramDisposable)
    {
      return DisposableHelper.setOnce(this.other, paramDisposable);
    }
  }
  
  static final class SamplerObserver<T>
    implements Observer<Object>
  {
    final ObservableSampleWithObservable.SampleMainObserver<T> parent;
    
    SamplerObserver(ObservableSampleWithObservable.SampleMainObserver<T> paramSampleMainObserver)
    {
      this.parent = paramSampleMainObserver;
    }
    
    public void onComplete()
    {
      this.parent.complete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.error(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.parent.run();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.parent.setOther(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSampleWithObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */