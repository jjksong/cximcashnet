package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.fuseable.HasUpstreamObservableSource;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Timed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableReplay<T>
  extends ConnectableObservable<T>
  implements HasUpstreamObservableSource<T>
{
  static final BufferSupplier DEFAULT_UNBOUNDED_FACTORY = new BufferSupplier()
  {
    public ObservableReplay.ReplayBuffer call()
    {
      return new ObservableReplay.UnboundedReplayBuffer(16);
    }
  };
  final BufferSupplier<T> bufferFactory;
  final AtomicReference<ReplayObserver<T>> current;
  final ObservableSource<T> onSubscribe;
  final ObservableSource<T> source;
  
  private ObservableReplay(ObservableSource<T> paramObservableSource1, ObservableSource<T> paramObservableSource2, AtomicReference<ReplayObserver<T>> paramAtomicReference, BufferSupplier<T> paramBufferSupplier)
  {
    this.onSubscribe = paramObservableSource1;
    this.source = paramObservableSource2;
    this.current = paramAtomicReference;
    this.bufferFactory = paramBufferSupplier;
  }
  
  public static <T> ConnectableObservable<T> create(ObservableSource<T> paramObservableSource, int paramInt)
  {
    if (paramInt == Integer.MAX_VALUE) {
      return createFrom(paramObservableSource);
    }
    create(paramObservableSource, new BufferSupplier()
    {
      public ObservableReplay.ReplayBuffer<T> call()
      {
        return new ObservableReplay.SizeBoundReplayBuffer(this.val$bufferSize);
      }
    });
  }
  
  public static <T> ConnectableObservable<T> create(ObservableSource<T> paramObservableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    return create(paramObservableSource, paramLong, paramTimeUnit, paramScheduler, Integer.MAX_VALUE);
  }
  
  public static <T> ConnectableObservable<T> create(ObservableSource<T> paramObservableSource, final long paramLong, TimeUnit paramTimeUnit, final Scheduler paramScheduler, int paramInt)
  {
    create(paramObservableSource, new BufferSupplier()
    {
      public ObservableReplay.ReplayBuffer<T> call()
      {
        return new ObservableReplay.SizeAndTimeBoundReplayBuffer(this.val$bufferSize, paramLong, paramScheduler, this.val$scheduler);
      }
    });
  }
  
  static <T> ConnectableObservable<T> create(ObservableSource<T> paramObservableSource, final BufferSupplier<T> paramBufferSupplier)
  {
    AtomicReference localAtomicReference = new AtomicReference();
    RxJavaPlugins.onAssembly(new ObservableReplay(new ObservableSource()
    {
      public void subscribe(Observer<? super T> paramAnonymousObserver)
      {
        Object localObject1;
        do
        {
          localObject2 = (ObservableReplay.ReplayObserver)this.val$curr.get();
          localObject1 = localObject2;
          if (localObject2 != null) {
            break;
          }
          localObject1 = new ObservableReplay.ReplayObserver(paramBufferSupplier.call());
        } while (!this.val$curr.compareAndSet(null, localObject1));
        Object localObject2 = new ObservableReplay.InnerDisposable((ObservableReplay.ReplayObserver)localObject1, paramAnonymousObserver);
        paramAnonymousObserver.onSubscribe((Disposable)localObject2);
        ((ObservableReplay.ReplayObserver)localObject1).add((ObservableReplay.InnerDisposable)localObject2);
        if (((ObservableReplay.InnerDisposable)localObject2).isDisposed())
        {
          ((ObservableReplay.ReplayObserver)localObject1).remove((ObservableReplay.InnerDisposable)localObject2);
          return;
        }
        ((ObservableReplay.ReplayObserver)localObject1).buffer.replay((ObservableReplay.InnerDisposable)localObject2);
      }
    }, paramObservableSource, localAtomicReference, paramBufferSupplier));
  }
  
  public static <T> ConnectableObservable<T> createFrom(ObservableSource<? extends T> paramObservableSource)
  {
    return create(paramObservableSource, DEFAULT_UNBOUNDED_FACTORY);
  }
  
  public static <U, R> Observable<R> multicastSelector(Callable<? extends ConnectableObservable<U>> paramCallable, final Function<? super Observable<U>, ? extends ObservableSource<R>> paramFunction)
  {
    RxJavaPlugins.onAssembly(new Observable()
    {
      protected void subscribeActual(final Observer<? super R> paramAnonymousObserver)
      {
        try
        {
          ConnectableObservable localConnectableObservable = (ConnectableObservable)this.val$connectableFactory.call();
          ObservableSource localObservableSource = (ObservableSource)paramFunction.apply(localConnectableObservable);
          paramAnonymousObserver = new ObserverResourceWrapper(paramAnonymousObserver);
          localObservableSource.subscribe(paramAnonymousObserver);
          localConnectableObservable.connect(new Consumer()
          {
            public void accept(Disposable paramAnonymous2Disposable)
            {
              paramAnonymousObserver.setResource(paramAnonymous2Disposable);
            }
          });
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          EmptyDisposable.error(localThrowable, paramAnonymousObserver);
        }
      }
    });
  }
  
  public static <T> ConnectableObservable<T> observeOn(ConnectableObservable<T> paramConnectableObservable, Scheduler paramScheduler)
  {
    RxJavaPlugins.onAssembly(new ConnectableObservable()
    {
      public void connect(Consumer<? super Disposable> paramAnonymousConsumer)
      {
        this.val$co.connect(paramAnonymousConsumer);
      }
      
      protected void subscribeActual(Observer<? super T> paramAnonymousObserver)
      {
        this.val$observable.subscribe(paramAnonymousObserver);
      }
    });
  }
  
  public void connect(Consumer<? super Disposable> paramConsumer)
  {
    ReplayObserver localReplayObserver2;
    ReplayObserver localReplayObserver1;
    do
    {
      localReplayObserver2 = (ReplayObserver)this.current.get();
      if (localReplayObserver2 != null)
      {
        localReplayObserver1 = localReplayObserver2;
        if (!localReplayObserver2.isDisposed()) {
          break;
        }
      }
      localReplayObserver1 = new ReplayObserver(this.bufferFactory.call());
    } while (!this.current.compareAndSet(localReplayObserver2, localReplayObserver1));
    int i;
    if ((!localReplayObserver1.shouldConnect.get()) && (localReplayObserver1.shouldConnect.compareAndSet(false, true))) {
      i = 1;
    } else {
      i = 0;
    }
    try
    {
      paramConsumer.accept(localReplayObserver1);
      if (i != 0) {
        this.source.subscribe(localReplayObserver1);
      }
      return;
    }
    catch (Throwable paramConsumer)
    {
      if (i != 0) {
        localReplayObserver1.shouldConnect.compareAndSet(true, false);
      }
      Exceptions.throwIfFatal(paramConsumer);
      throw ExceptionHelper.wrapOrThrow(paramConsumer);
    }
  }
  
  public ObservableSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.onSubscribe.subscribe(paramObserver);
  }
  
  static abstract class BoundedReplayBuffer<T>
    extends AtomicReference<ObservableReplay.Node>
    implements ObservableReplay.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 2346567790059478686L;
    int size;
    ObservableReplay.Node tail;
    
    BoundedReplayBuffer()
    {
      ObservableReplay.Node localNode = new ObservableReplay.Node(null);
      this.tail = localNode;
      set(localNode);
    }
    
    final void addLast(ObservableReplay.Node paramNode)
    {
      this.tail.set(paramNode);
      this.tail = paramNode;
      this.size += 1;
    }
    
    final void collect(Collection<? super T> paramCollection)
    {
      ObservableReplay.Node localNode = (ObservableReplay.Node)get();
      for (;;)
      {
        localNode = (ObservableReplay.Node)localNode.get();
        if (localNode == null) {
          break;
        }
        Object localObject = leaveTransform(localNode.value);
        if ((NotificationLite.isComplete(localObject)) || (NotificationLite.isError(localObject))) {
          break;
        }
        paramCollection.add(NotificationLite.getValue(localObject));
      }
    }
    
    public final void complete()
    {
      addLast(new ObservableReplay.Node(enterTransform(NotificationLite.complete())));
      truncateFinal();
    }
    
    Object enterTransform(Object paramObject)
    {
      return paramObject;
    }
    
    public final void error(Throwable paramThrowable)
    {
      addLast(new ObservableReplay.Node(enterTransform(NotificationLite.error(paramThrowable))));
      truncateFinal();
    }
    
    boolean hasCompleted()
    {
      boolean bool;
      if ((this.tail.value != null) && (NotificationLite.isComplete(leaveTransform(this.tail.value)))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    boolean hasError()
    {
      boolean bool;
      if ((this.tail.value != null) && (NotificationLite.isError(leaveTransform(this.tail.value)))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    Object leaveTransform(Object paramObject)
    {
      return paramObject;
    }
    
    public final void next(T paramT)
    {
      addLast(new ObservableReplay.Node(enterTransform(NotificationLite.next(paramT))));
      truncate();
    }
    
    final void removeFirst()
    {
      ObservableReplay.Node localNode = (ObservableReplay.Node)((ObservableReplay.Node)get()).get();
      this.size -= 1;
      setFirst(localNode);
    }
    
    final void removeSome(int paramInt)
    {
      ObservableReplay.Node localNode = (ObservableReplay.Node)get();
      while (paramInt > 0)
      {
        localNode = (ObservableReplay.Node)localNode.get();
        paramInt--;
        this.size -= 1;
      }
      setFirst(localNode);
    }
    
    public final void replay(ObservableReplay.InnerDisposable<T> paramInnerDisposable)
    {
      if (paramInnerDisposable.getAndIncrement() != 0) {
        return;
      }
      int i = 1;
      int j;
      do
      {
        ObservableReplay.Node localNode2 = (ObservableReplay.Node)paramInnerDisposable.index();
        ObservableReplay.Node localNode1 = localNode2;
        if (localNode2 == null)
        {
          localNode1 = (ObservableReplay.Node)get();
          paramInnerDisposable.index = localNode1;
        }
        for (;;)
        {
          if (paramInnerDisposable.isDisposed()) {
            return;
          }
          localNode2 = (ObservableReplay.Node)localNode1.get();
          if (localNode2 == null) {
            break;
          }
          if (NotificationLite.accept(leaveTransform(localNode2.value), paramInnerDisposable.child))
          {
            paramInnerDisposable.index = null;
            return;
          }
          localNode1 = localNode2;
        }
        paramInnerDisposable.index = localNode1;
        j = paramInnerDisposable.addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    final void setFirst(ObservableReplay.Node paramNode)
    {
      set(paramNode);
    }
    
    abstract void truncate();
    
    void truncateFinal() {}
  }
  
  static abstract interface BufferSupplier<T>
  {
    public abstract ObservableReplay.ReplayBuffer<T> call();
  }
  
  static final class InnerDisposable<T>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = 2728361546769921047L;
    volatile boolean cancelled;
    final Observer<? super T> child;
    Object index;
    final ObservableReplay.ReplayObserver<T> parent;
    
    InnerDisposable(ObservableReplay.ReplayObserver<T> paramReplayObserver, Observer<? super T> paramObserver)
    {
      this.parent = paramReplayObserver;
      this.child = paramObserver;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.parent.remove(this);
      }
    }
    
    <U> U index()
    {
      return (U)this.index;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
  }
  
  static final class Node
    extends AtomicReference<Node>
  {
    private static final long serialVersionUID = 245354315435971818L;
    final Object value;
    
    Node(Object paramObject)
    {
      this.value = paramObject;
    }
  }
  
  static abstract interface ReplayBuffer<T>
  {
    public abstract void complete();
    
    public abstract void error(Throwable paramThrowable);
    
    public abstract void next(T paramT);
    
    public abstract void replay(ObservableReplay.InnerDisposable<T> paramInnerDisposable);
  }
  
  static final class ReplayObserver<T>
    implements Observer<T>, Disposable
  {
    static final ObservableReplay.InnerDisposable[] EMPTY = new ObservableReplay.InnerDisposable[0];
    static final ObservableReplay.InnerDisposable[] TERMINATED = new ObservableReplay.InnerDisposable[0];
    final ObservableReplay.ReplayBuffer<T> buffer;
    boolean done;
    final AtomicReference<ObservableReplay.InnerDisposable[]> observers;
    final AtomicBoolean shouldConnect;
    volatile Disposable subscription;
    
    ReplayObserver(ObservableReplay.ReplayBuffer<T> paramReplayBuffer)
    {
      this.buffer = paramReplayBuffer;
      this.observers = new AtomicReference(EMPTY);
      this.shouldConnect = new AtomicBoolean();
    }
    
    boolean add(ObservableReplay.InnerDisposable<T> paramInnerDisposable)
    {
      ObservableReplay.InnerDisposable[] arrayOfInnerDisposable2;
      ObservableReplay.InnerDisposable[] arrayOfInnerDisposable1;
      do
      {
        arrayOfInnerDisposable2 = (ObservableReplay.InnerDisposable[])this.observers.get();
        if (arrayOfInnerDisposable2 == TERMINATED) {
          return false;
        }
        int i = arrayOfInnerDisposable2.length;
        arrayOfInnerDisposable1 = new ObservableReplay.InnerDisposable[i + 1];
        System.arraycopy(arrayOfInnerDisposable2, 0, arrayOfInnerDisposable1, 0, i);
        arrayOfInnerDisposable1[i] = paramInnerDisposable;
      } while (!this.observers.compareAndSet(arrayOfInnerDisposable2, arrayOfInnerDisposable1));
      return true;
    }
    
    public void dispose()
    {
      this.observers.set(TERMINATED);
      this.subscription.dispose();
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.observers.get() == TERMINATED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.buffer.complete();
        replayFinal();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.done)
      {
        this.done = true;
        this.buffer.error(paramThrowable);
        replayFinal();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        this.buffer.next(paramT);
        replay();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.subscription, paramDisposable))
      {
        this.subscription = paramDisposable;
        replay();
      }
    }
    
    void remove(ObservableReplay.InnerDisposable<T> paramInnerDisposable)
    {
      ObservableReplay.InnerDisposable[] arrayOfInnerDisposable2;
      ObservableReplay.InnerDisposable[] arrayOfInnerDisposable1;
      do
      {
        arrayOfInnerDisposable2 = (ObservableReplay.InnerDisposable[])this.observers.get();
        int m = arrayOfInnerDisposable2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerDisposable2[i].equals(paramInnerDisposable))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerDisposable1 = EMPTY;
        }
        else
        {
          arrayOfInnerDisposable1 = new ObservableReplay.InnerDisposable[m - 1];
          System.arraycopy(arrayOfInnerDisposable2, 0, arrayOfInnerDisposable1, 0, j);
          System.arraycopy(arrayOfInnerDisposable2, j + 1, arrayOfInnerDisposable1, j, m - j - 1);
        }
      } while (!this.observers.compareAndSet(arrayOfInnerDisposable2, arrayOfInnerDisposable1));
    }
    
    void replay()
    {
      for (ObservableReplay.InnerDisposable localInnerDisposable : (ObservableReplay.InnerDisposable[])this.observers.get()) {
        this.buffer.replay(localInnerDisposable);
      }
    }
    
    void replayFinal()
    {
      for (ObservableReplay.InnerDisposable localInnerDisposable : (ObservableReplay.InnerDisposable[])this.observers.getAndSet(TERMINATED)) {
        this.buffer.replay(localInnerDisposable);
      }
    }
  }
  
  static final class SizeAndTimeBoundReplayBuffer<T>
    extends ObservableReplay.BoundedReplayBuffer<T>
  {
    private static final long serialVersionUID = 3457957419649567404L;
    final int limit;
    final long maxAge;
    final Scheduler scheduler;
    final TimeUnit unit;
    
    SizeAndTimeBoundReplayBuffer(int paramInt, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.scheduler = paramScheduler;
      this.limit = paramInt;
      this.maxAge = paramLong;
      this.unit = paramTimeUnit;
    }
    
    Object enterTransform(Object paramObject)
    {
      return new Timed(paramObject, this.scheduler.now(this.unit), this.unit);
    }
    
    Object leaveTransform(Object paramObject)
    {
      return ((Timed)paramObject).value();
    }
    
    void truncate()
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.maxAge;
      Object localObject2 = (ObservableReplay.Node)get();
      Object localObject1 = (ObservableReplay.Node)((ObservableReplay.Node)localObject2).get();
      int i = 0;
      while (localObject1 != null)
      {
        ObservableReplay.Node localNode;
        if (this.size > this.limit)
        {
          i++;
          this.size -= 1;
          localNode = (ObservableReplay.Node)((ObservableReplay.Node)localObject1).get();
          localObject2 = localObject1;
          localObject1 = localNode;
        }
        else
        {
          if (((Timed)((ObservableReplay.Node)localObject1).value).time() > l1 - l2) {
            break;
          }
          i++;
          this.size -= 1;
          localNode = (ObservableReplay.Node)((ObservableReplay.Node)localObject1).get();
          localObject2 = localObject1;
          localObject1 = localNode;
        }
      }
      if (i != 0) {
        setFirst((ObservableReplay.Node)localObject2);
      }
    }
    
    void truncateFinal()
    {
      long l2 = this.scheduler.now(this.unit);
      long l1 = this.maxAge;
      Object localObject2 = (ObservableReplay.Node)get();
      Object localObject1 = (ObservableReplay.Node)((ObservableReplay.Node)localObject2).get();
      int i = 0;
      while ((localObject1 != null) && (this.size > 1) && (((Timed)((ObservableReplay.Node)localObject1).value).time() <= l2 - l1))
      {
        i++;
        this.size -= 1;
        ObservableReplay.Node localNode = (ObservableReplay.Node)((ObservableReplay.Node)localObject1).get();
        localObject2 = localObject1;
        localObject1 = localNode;
      }
      if (i != 0) {
        setFirst((ObservableReplay.Node)localObject2);
      }
    }
  }
  
  static final class SizeBoundReplayBuffer<T>
    extends ObservableReplay.BoundedReplayBuffer<T>
  {
    private static final long serialVersionUID = -5898283885385201806L;
    final int limit;
    
    SizeBoundReplayBuffer(int paramInt)
    {
      this.limit = paramInt;
    }
    
    void truncate()
    {
      if (this.size > this.limit) {
        removeFirst();
      }
    }
  }
  
  static final class UnboundedReplayBuffer<T>
    extends ArrayList<Object>
    implements ObservableReplay.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 7063189396499112664L;
    volatile int size;
    
    UnboundedReplayBuffer(int paramInt)
    {
      super();
    }
    
    public void complete()
    {
      add(NotificationLite.complete());
      this.size += 1;
    }
    
    public void error(Throwable paramThrowable)
    {
      add(NotificationLite.error(paramThrowable));
      this.size += 1;
    }
    
    public void next(T paramT)
    {
      add(NotificationLite.next(paramT));
      this.size += 1;
    }
    
    public void replay(ObservableReplay.InnerDisposable<T> paramInnerDisposable)
    {
      if (paramInnerDisposable.getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = paramInnerDisposable.child;
      int j = 1;
      int i;
      do
      {
        if (paramInnerDisposable.isDisposed()) {
          return;
        }
        int k = this.size;
        Integer localInteger = (Integer)paramInnerDisposable.index();
        if (localInteger != null) {
          i = localInteger.intValue();
        }
        for (i = 0; i < k; i++)
        {
          if (NotificationLite.accept(get(i), localObserver)) {
            return;
          }
          if (paramInnerDisposable.isDisposed()) {
            return;
          }
        }
        paramInnerDisposable.index = Integer.valueOf(i);
        i = paramInnerDisposable.addAndGet(-j);
        j = i;
      } while (i != 0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableReplay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */