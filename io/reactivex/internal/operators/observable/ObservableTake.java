package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableTake<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final long limit;
  
  public ObservableTake(ObservableSource<T> paramObservableSource, long paramLong)
  {
    super(paramObservableSource);
    this.limit = paramLong;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new TakeObserver(paramObserver, this.limit));
  }
  
  static final class TakeObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    boolean done;
    long remaining;
    Disposable subscription;
    
    TakeObserver(Observer<? super T> paramObserver, long paramLong)
    {
      this.actual = paramObserver;
      this.remaining = paramLong;
    }
    
    public void dispose()
    {
      this.subscription.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.subscription.isDisposed();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.subscription.dispose();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.subscription.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        long l = this.remaining;
        this.remaining = (l - 1L);
        if (l > 0L)
        {
          int i;
          if (this.remaining == 0L) {
            i = 1;
          } else {
            i = 0;
          }
          this.actual.onNext(paramT);
          if (i != 0) {
            onComplete();
          }
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.subscription, paramDisposable))
      {
        this.subscription = paramDisposable;
        if (this.remaining == 0L)
        {
          this.done = true;
          paramDisposable.dispose();
          EmptyDisposable.complete(this.actual);
        }
        else
        {
          this.actual.onSubscribe(this);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTake.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */