package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableSubscribeOn<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Scheduler scheduler;
  
  public ObservableSubscribeOn(ObservableSource<T> paramObservableSource, Scheduler paramScheduler)
  {
    super(paramObservableSource);
    this.scheduler = paramScheduler;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    final SubscribeOnObserver localSubscribeOnObserver = new SubscribeOnObserver(paramObserver);
    paramObserver.onSubscribe(localSubscribeOnObserver);
    localSubscribeOnObserver.setDisposable(this.scheduler.scheduleDirect(new Runnable()
    {
      public void run()
      {
        ObservableSubscribeOn.this.source.subscribe(localSubscribeOnObserver);
      }
    }));
  }
  
  static final class SubscribeOnObserver<T>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 8094547886072529208L;
    final Observer<? super T> actual;
    final AtomicReference<Disposable> s;
    
    SubscribeOnObserver(Observer<? super T> paramObserver)
    {
      this.actual = paramObserver;
      this.s = new AtomicReference();
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.s);
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.s, paramDisposable);
    }
    
    void setDisposable(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSubscribeOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */