package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.EmptyComponent;

public final class ObservableDetach<T>
  extends AbstractObservableWithUpstream<T, T>
{
  public ObservableDetach(ObservableSource<T> paramObservableSource)
  {
    super(paramObservableSource);
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new DetachObserver(paramObserver));
  }
  
  static final class DetachObserver<T>
    implements Observer<T>, Disposable
  {
    Observer<? super T> actual;
    Disposable s;
    
    DetachObserver(Observer<? super T> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      Disposable localDisposable = this.s;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asObserver();
      localDisposable.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      Observer localObserver = this.actual;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asObserver();
      localObserver.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      Observer localObserver = this.actual;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asObserver();
      localObserver.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDetach.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */