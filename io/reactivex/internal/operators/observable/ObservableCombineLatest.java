package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableCombineLatest<T, R>
  extends Observable<R>
{
  final int bufferSize;
  final Function<? super Object[], ? extends R> combiner;
  final boolean delayError;
  final ObservableSource<? extends T>[] sources;
  final Iterable<? extends ObservableSource<? extends T>> sourcesIterable;
  
  public ObservableCombineLatest(ObservableSource<? extends T>[] paramArrayOfObservableSource, Iterable<? extends ObservableSource<? extends T>> paramIterable, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
  {
    this.sources = paramArrayOfObservableSource;
    this.sourcesIterable = paramIterable;
    this.combiner = paramFunction;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    Object localObject1 = this.sources;
    int i;
    if (localObject1 == null)
    {
      localObject1 = new Observable[8];
      Iterator localIterator = this.sourcesIterable.iterator();
      i = 0;
      while (localIterator.hasNext())
      {
        ObservableSource localObservableSource = (ObservableSource)localIterator.next();
        Object localObject2 = localObject1;
        if (i == localObject1.length)
        {
          localObject2 = new ObservableSource[(i >> 2) + i];
          System.arraycopy(localObject1, 0, localObject2, 0, i);
        }
        localObject2[i] = localObservableSource;
        i++;
        localObject1 = localObject2;
      }
    }
    else
    {
      i = localObject1.length;
    }
    if (i == 0)
    {
      EmptyDisposable.complete(paramObserver);
      return;
    }
    new LatestCoordinator(paramObserver, this.combiner, i, this.bufferSize, this.delayError).subscribe((ObservableSource[])localObject1);
  }
  
  static final class CombinerObserver<T, R>
    implements Observer<T>
  {
    final int index;
    final ObservableCombineLatest.LatestCoordinator<T, R> parent;
    final AtomicReference<Disposable> s = new AtomicReference();
    
    CombinerObserver(ObservableCombineLatest.LatestCoordinator<T, R> paramLatestCoordinator, int paramInt)
    {
      this.parent = paramLatestCoordinator;
      this.index = paramInt;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.s);
    }
    
    public void onComplete()
    {
      this.parent.combine(null, this.index);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.onError(paramThrowable);
      this.parent.combine(null, this.index);
    }
    
    public void onNext(T paramT)
    {
      this.parent.combine(paramT, this.index);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.s, paramDisposable);
    }
  }
  
  static final class LatestCoordinator<T, R>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = 8567835998786448817L;
    int active;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    final Function<? super Object[], ? extends R> combiner;
    int complete;
    final boolean delayError;
    volatile boolean done;
    final AtomicThrowable errors = new AtomicThrowable();
    final T[] latest;
    final ObservableCombineLatest.CombinerObserver<T, R>[] observers;
    final SpscLinkedArrayQueue<Object> queue;
    
    LatestCoordinator(Observer<? super R> paramObserver, Function<? super Object[], ? extends R> paramFunction, int paramInt1, int paramInt2, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.combiner = paramFunction;
      this.delayError = paramBoolean;
      this.latest = ((Object[])new Object[paramInt1]);
      this.observers = new ObservableCombineLatest.CombinerObserver[paramInt1];
      this.queue = new SpscLinkedArrayQueue(paramInt2);
    }
    
    void cancel(SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      clear(paramSpscLinkedArrayQueue);
      paramSpscLinkedArrayQueue = this.observers;
      int j = paramSpscLinkedArrayQueue.length;
      for (int i = 0; i < j; i++) {
        paramSpscLinkedArrayQueue[i].dispose();
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Observer<?> paramObserver, SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue, boolean paramBoolean3)
    {
      if (this.cancelled)
      {
        cancel(paramSpscLinkedArrayQueue);
        return true;
      }
      if (paramBoolean1) {
        if (paramBoolean3)
        {
          if (paramBoolean2)
          {
            cancel(paramSpscLinkedArrayQueue);
            paramSpscLinkedArrayQueue = this.errors.terminate();
            if (paramSpscLinkedArrayQueue != null) {
              paramObserver.onError(paramSpscLinkedArrayQueue);
            } else {
              paramObserver.onComplete();
            }
            return true;
          }
        }
        else
        {
          if ((Throwable)this.errors.get() != null)
          {
            cancel(paramSpscLinkedArrayQueue);
            paramObserver.onError(this.errors.terminate());
            return true;
          }
          if (paramBoolean2)
          {
            clear(this.queue);
            paramObserver.onComplete();
            return true;
          }
        }
      }
      return false;
    }
    
    void clear(SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      try
      {
        Arrays.fill(this.latest, null);
        paramSpscLinkedArrayQueue.clear();
        return;
      }
      finally {}
    }
    
    void combine(T paramT, int paramInt)
    {
      ObservableCombineLatest.CombinerObserver localCombinerObserver = this.observers[paramInt];
      try
      {
        if (this.cancelled) {
          return;
        }
        int k = this.latest.length;
        Object localObject = this.latest[paramInt];
        int j = this.active;
        int i = j;
        if (localObject == null)
        {
          i = j + 1;
          this.active = i;
        }
        j = this.complete;
        if (paramT == null)
        {
          paramInt = j + 1;
          this.complete = paramInt;
        }
        else
        {
          this.latest[paramInt] = paramT;
          paramInt = j;
        }
        j = 0;
        if (i == k) {
          i = 1;
        } else {
          i = 0;
        }
        if (paramInt != k)
        {
          paramInt = j;
          if (paramT == null)
          {
            paramInt = j;
            if (localObject != null) {}
          }
        }
        else
        {
          paramInt = 1;
        }
        if (paramInt == 0)
        {
          if ((paramT != null) && (i != 0)) {
            this.queue.offer(localCombinerObserver, this.latest.clone());
          } else if ((paramT == null) && (this.errors.get() != null)) {
            this.done = true;
          }
        }
        else {
          this.done = true;
        }
        if ((i == 0) && (paramT != null)) {
          return;
        }
        drain();
        return;
      }
      finally {}
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        if (getAndIncrement() == 0) {
          cancel(this.queue);
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Observer localObserver = this.actual;
      boolean bool3 = this.delayError;
      int i = 1;
      if (checkTerminated(this.done, localSpscLinkedArrayQueue.isEmpty(), localObserver, localSpscLinkedArrayQueue, bool3)) {
        return;
      }
      for (;;)
      {
        boolean bool2 = this.done;
        boolean bool1;
        if ((ObservableCombineLatest.CombinerObserver)localSpscLinkedArrayQueue.poll() == null) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        if (checkTerminated(bool2, bool1, localObserver, localSpscLinkedArrayQueue, bool3)) {
          return;
        }
        if (bool1)
        {
          int j = addAndGet(-i);
          i = j;
          if (j != 0) {
            break;
          }
          return;
        }
        Object localObject = (Object[])localSpscLinkedArrayQueue.poll();
        try
        {
          localObject = ObjectHelper.requireNonNull(this.combiner.apply(localObject), "The combiner returned a null");
          localObserver.onNext(localObject);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.cancelled = true;
          cancel(localSpscLinkedArrayQueue);
          localObserver.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    void onError(Throwable paramThrowable)
    {
      if (!this.errors.addThrowable(paramThrowable)) {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void subscribe(ObservableSource<? extends T>[] paramArrayOfObservableSource)
    {
      ObservableCombineLatest.CombinerObserver[] arrayOfCombinerObserver = this.observers;
      int k = arrayOfCombinerObserver.length;
      int j = 0;
      for (int i = 0; i < k; i++) {
        arrayOfCombinerObserver[i] = new ObservableCombineLatest.CombinerObserver(this, i);
      }
      lazySet(0);
      this.actual.onSubscribe(this);
      i = j;
      while (i < k) {
        if ((!this.done) && (!this.cancelled))
        {
          paramArrayOfObservableSource[i].subscribe(arrayOfCombinerObserver[i]);
          i++;
        }
        else {}
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCombineLatest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */