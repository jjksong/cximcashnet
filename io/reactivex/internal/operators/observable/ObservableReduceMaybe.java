package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableReduceMaybe<T>
  extends Maybe<T>
{
  final BiFunction<T, T, T> reducer;
  final ObservableSource<T> source;
  
  public ObservableReduceMaybe(ObservableSource<T> paramObservableSource, BiFunction<T, T, T> paramBiFunction)
  {
    this.source = paramObservableSource;
    this.reducer = paramBiFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new ReduceObserver(paramMaybeObserver, this.reducer));
  }
  
  static final class ReduceObserver<T>
    implements Observer<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    boolean done;
    final BiFunction<T, T, T> reducer;
    T value;
    
    ReduceObserver(MaybeObserver<? super T> paramMaybeObserver, BiFunction<T, T, T> paramBiFunction)
    {
      this.actual = paramMaybeObserver;
      this.reducer = paramBiFunction;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject = this.value;
      this.value = null;
      if (localObject != null) {
        this.actual.onSuccess(localObject);
      } else {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.value = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        Object localObject = this.value;
        if (localObject == null) {
          this.value = paramT;
        } else {
          try
          {
            this.value = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            this.d.dispose();
            onError(paramT);
          }
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableReduceMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */