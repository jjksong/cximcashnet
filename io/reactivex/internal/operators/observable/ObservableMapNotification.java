package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class ObservableMapNotification<T, R>
  extends AbstractObservableWithUpstream<T, ObservableSource<? extends R>>
{
  final Callable<? extends ObservableSource<? extends R>> onCompleteSupplier;
  final Function<? super Throwable, ? extends ObservableSource<? extends R>> onErrorMapper;
  final Function<? super T, ? extends ObservableSource<? extends R>> onNextMapper;
  
  public ObservableMapNotification(ObservableSource<T> paramObservableSource, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, Function<? super Throwable, ? extends ObservableSource<? extends R>> paramFunction1, Callable<? extends ObservableSource<? extends R>> paramCallable)
  {
    super(paramObservableSource);
    this.onNextMapper = paramFunction;
    this.onErrorMapper = paramFunction1;
    this.onCompleteSupplier = paramCallable;
  }
  
  public void subscribeActual(Observer<? super ObservableSource<? extends R>> paramObserver)
  {
    this.source.subscribe(new MapNotificationObserver(paramObserver, this.onNextMapper, this.onErrorMapper, this.onCompleteSupplier));
  }
  
  static final class MapNotificationObserver<T, R>
    implements Observer<T>, Disposable
  {
    final Observer<? super ObservableSource<? extends R>> actual;
    final Callable<? extends ObservableSource<? extends R>> onCompleteSupplier;
    final Function<? super Throwable, ? extends ObservableSource<? extends R>> onErrorMapper;
    final Function<? super T, ? extends ObservableSource<? extends R>> onNextMapper;
    Disposable s;
    
    MapNotificationObserver(Observer<? super ObservableSource<? extends R>> paramObserver, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, Function<? super Throwable, ? extends ObservableSource<? extends R>> paramFunction1, Callable<? extends ObservableSource<? extends R>> paramCallable)
    {
      this.actual = paramObserver;
      this.onNextMapper = paramFunction;
      this.onErrorMapper = paramFunction1;
      this.onCompleteSupplier = paramCallable;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      try
      {
        ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.onCompleteSupplier.call(), "The onComplete publisher returned is null");
        this.actual.onNext(localObservableSource);
        this.actual.onComplete();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        paramThrowable = (ObservableSource)ObjectHelper.requireNonNull(this.onErrorMapper.apply(paramThrowable), "The onError publisher returned is null");
        this.actual.onNext(paramThrowable);
        this.actual.onComplete();
        return;
      }
      catch (Throwable paramThrowable)
      {
        Exceptions.throwIfFatal(paramThrowable);
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      try
      {
        paramT = (ObservableSource)ObjectHelper.requireNonNull(this.onNextMapper.apply(paramT), "The onNext publisher returned is null");
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableMapNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */