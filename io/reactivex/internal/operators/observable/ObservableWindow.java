package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.subjects.UnicastSubject;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableWindow<T>
  extends AbstractObservableWithUpstream<T, Observable<T>>
{
  final int capacityHint;
  final long count;
  final long skip;
  
  public ObservableWindow(ObservableSource<T> paramObservableSource, long paramLong1, long paramLong2, int paramInt)
  {
    super(paramObservableSource);
    this.count = paramLong1;
    this.skip = paramLong2;
    this.capacityHint = paramInt;
  }
  
  public void subscribeActual(Observer<? super Observable<T>> paramObserver)
  {
    if (this.count == this.skip) {
      this.source.subscribe(new WindowExactObserver(paramObserver, this.count, this.capacityHint));
    } else {
      this.source.subscribe(new WindowSkipObserver(paramObserver, this.count, this.skip, this.capacityHint));
    }
  }
  
  static final class WindowExactObserver<T>
    extends AtomicInteger
    implements Observer<T>, Disposable, Runnable
  {
    private static final long serialVersionUID = -7481782523886138128L;
    final Observer<? super Observable<T>> actual;
    volatile boolean cancelled;
    final int capacityHint;
    final long count;
    Disposable s;
    long size;
    UnicastSubject<T> window;
    
    WindowExactObserver(Observer<? super Observable<T>> paramObserver, long paramLong, int paramInt)
    {
      this.actual = paramObserver;
      this.count = paramLong;
      this.capacityHint = paramInt;
    }
    
    public void dispose()
    {
      this.cancelled = true;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      UnicastSubject localUnicastSubject = this.window;
      if (localUnicastSubject != null)
      {
        this.window = null;
        localUnicastSubject.onComplete();
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      UnicastSubject localUnicastSubject = this.window;
      if (localUnicastSubject != null)
      {
        this.window = null;
        localUnicastSubject.onError(paramThrowable);
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      UnicastSubject localUnicastSubject2 = this.window;
      UnicastSubject localUnicastSubject1 = localUnicastSubject2;
      if (localUnicastSubject2 == null)
      {
        localUnicastSubject1 = localUnicastSubject2;
        if (!this.cancelled)
        {
          localUnicastSubject1 = UnicastSubject.create(this.capacityHint, this);
          this.window = localUnicastSubject1;
          this.actual.onNext(localUnicastSubject1);
        }
      }
      if (localUnicastSubject1 != null)
      {
        localUnicastSubject1.onNext(paramT);
        long l = this.size + 1L;
        this.size = l;
        if (l >= this.count)
        {
          this.size = 0L;
          this.window = null;
          localUnicastSubject1.onComplete();
          if (this.cancelled) {
            this.s.dispose();
          }
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void run()
    {
      if (this.cancelled) {
        this.s.dispose();
      }
    }
  }
  
  static final class WindowSkipObserver<T>
    extends AtomicBoolean
    implements Observer<T>, Disposable, Runnable
  {
    private static final long serialVersionUID = 3366976432059579510L;
    final Observer<? super Observable<T>> actual;
    volatile boolean cancelled;
    final int capacityHint;
    final long count;
    long firstEmission;
    long index;
    Disposable s;
    final long skip;
    final ArrayDeque<UnicastSubject<T>> windows;
    final AtomicInteger wip = new AtomicInteger();
    
    WindowSkipObserver(Observer<? super Observable<T>> paramObserver, long paramLong1, long paramLong2, int paramInt)
    {
      this.actual = paramObserver;
      this.count = paramLong1;
      this.skip = paramLong2;
      this.capacityHint = paramInt;
      this.windows = new ArrayDeque();
    }
    
    public void dispose()
    {
      this.cancelled = true;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      ArrayDeque localArrayDeque = this.windows;
      while (!localArrayDeque.isEmpty()) {
        ((UnicastSubject)localArrayDeque.poll()).onComplete();
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      ArrayDeque localArrayDeque = this.windows;
      while (!localArrayDeque.isEmpty()) {
        ((UnicastSubject)localArrayDeque.poll()).onError(paramThrowable);
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      ArrayDeque localArrayDeque = this.windows;
      long l3 = this.index;
      long l1 = this.skip;
      if ((l3 % l1 == 0L) && (!this.cancelled))
      {
        this.wip.getAndIncrement();
        localObject = UnicastSubject.create(this.capacityHint, this);
        localArrayDeque.offer(localObject);
        this.actual.onNext(localObject);
      }
      long l2 = this.firstEmission + 1L;
      Object localObject = localArrayDeque.iterator();
      while (((Iterator)localObject).hasNext()) {
        ((UnicastSubject)((Iterator)localObject).next()).onNext(paramT);
      }
      if (l2 >= this.count)
      {
        ((UnicastSubject)localArrayDeque.poll()).onComplete();
        if ((localArrayDeque.isEmpty()) && (this.cancelled))
        {
          this.s.dispose();
          return;
        }
        this.firstEmission = (l2 - l1);
      }
      else
      {
        this.firstEmission = l2;
      }
      this.index = (l3 + 1L);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void run()
    {
      if ((this.wip.decrementAndGet() == 0) && (this.cancelled)) {
        this.s.dispose();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableWindow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */