package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class ObservableDefer<T>
  extends Observable<T>
{
  final Callable<? extends ObservableSource<? extends T>> supplier;
  
  public ObservableDefer(Callable<? extends ObservableSource<? extends T>> paramCallable)
  {
    this.supplier = paramCallable;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    try
    {
      ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.supplier.call(), "null publisher supplied");
      localObservableSource.subscribe(paramObserver);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDefer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */