package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableConcatMap<T, U>
  extends AbstractObservableWithUpstream<T, U>
{
  final int bufferSize;
  final ErrorMode delayErrors;
  final Function<? super T, ? extends ObservableSource<? extends U>> mapper;
  
  public ObservableConcatMap(ObservableSource<T> paramObservableSource, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction, int paramInt, ErrorMode paramErrorMode)
  {
    super(paramObservableSource);
    this.mapper = paramFunction;
    this.delayErrors = paramErrorMode;
    this.bufferSize = Math.max(8, paramInt);
  }
  
  public void subscribeActual(Observer<? super U> paramObserver)
  {
    if (ObservableScalarXMap.tryScalarXMapSubscribe(this.source, paramObserver, this.mapper)) {
      return;
    }
    if (this.delayErrors == ErrorMode.IMMEDIATE)
    {
      paramObserver = new SerializedObserver(paramObserver);
      this.source.subscribe(new SourceObserver(paramObserver, this.mapper, this.bufferSize));
    }
    else
    {
      ObservableSource localObservableSource = this.source;
      Function localFunction = this.mapper;
      int i = this.bufferSize;
      boolean bool;
      if (this.delayErrors == ErrorMode.END) {
        bool = true;
      } else {
        bool = false;
      }
      localObservableSource.subscribe(new ConcatMapDelayErrorObserver(paramObserver, localFunction, i, bool));
    }
  }
  
  static final class ConcatMapDelayErrorObserver<T, R>
    extends AtomicInteger
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -6951100001833242599L;
    volatile boolean active;
    final Observer<? super R> actual;
    final SequentialDisposable arbiter;
    final int bufferSize;
    volatile boolean cancelled;
    Disposable d;
    volatile boolean done;
    final AtomicThrowable error;
    final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
    final DelayErrorInnerObserver<R> observer;
    SimpleQueue<T> queue;
    int sourceMode;
    final boolean tillTheEnd;
    
    ConcatMapDelayErrorObserver(Observer<? super R> paramObserver, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
      this.bufferSize = paramInt;
      this.tillTheEnd = paramBoolean;
      this.error = new AtomicThrowable();
      this.observer = new DelayErrorInnerObserver(paramObserver, this);
      this.arbiter = new SequentialDisposable();
    }
    
    public void dispose()
    {
      this.cancelled = true;
      this.d.dispose();
      this.arbiter.dispose();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      Object localObject1 = this.error;
      while (!this.active)
      {
        if (this.cancelled)
        {
          localSimpleQueue.clear();
          return;
        }
        if ((!this.tillTheEnd) && ((Throwable)((AtomicThrowable)localObject1).get() != null))
        {
          localSimpleQueue.clear();
          localObserver.onError(((AtomicThrowable)localObject1).terminate());
          return;
        }
        boolean bool = this.done;
        try
        {
          Object localObject2 = localSimpleQueue.poll();
          int i;
          if (localObject2 == null) {
            i = 1;
          } else {
            i = 0;
          }
          if ((bool) && (i != 0))
          {
            localObject1 = ((AtomicThrowable)localObject1).terminate();
            if (localObject1 != null) {
              localObserver.onError((Throwable)localObject1);
            } else {
              localObserver.onComplete();
            }
            return;
          }
          if (i == 0) {
            try
            {
              localObject2 = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(localObject2), "The mapper returned a null ObservableSource");
              if ((localObject2 instanceof Callable))
              {
                try
                {
                  localObject2 = ((Callable)localObject2).call();
                  if ((localObject2 == null) || (this.cancelled)) {
                    continue;
                  }
                  localObserver.onNext(localObject2);
                }
                catch (Throwable localThrowable2)
                {
                  Exceptions.throwIfFatal(localThrowable2);
                  ((AtomicThrowable)localObject1).addThrowable(localThrowable2);
                }
                continue;
              }
              this.active = true;
              localThrowable2.subscribe(this.observer);
            }
            catch (Throwable localThrowable3)
            {
              Exceptions.throwIfFatal(localThrowable3);
              this.d.dispose();
              localSimpleQueue.clear();
              ((AtomicThrowable)localObject1).addThrowable(localThrowable3);
              localObserver.onError(((AtomicThrowable)localObject1).terminate());
              return;
            }
          } else if (decrementAndGet() != 0) {}
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          this.d.dispose();
          ((AtomicThrowable)localObject1).addThrowable(localThrowable1);
          localObserver.onError(((AtomicThrowable)localObject1).terminate());
          return;
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.sourceMode == 0) {
        this.queue.offer(paramT);
      }
      drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        if ((paramDisposable instanceof QueueDisposable))
        {
          paramDisposable = (QueueDisposable)paramDisposable;
          int i = paramDisposable.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.done = true;
            this.actual.onSubscribe(this);
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.actual.onSubscribe(this);
            return;
          }
        }
        this.queue = new SpscLinkedArrayQueue(this.bufferSize);
        this.actual.onSubscribe(this);
      }
    }
    
    static final class DelayErrorInnerObserver<R>
      implements Observer<R>
    {
      final Observer<? super R> actual;
      final ObservableConcatMap.ConcatMapDelayErrorObserver<?, R> parent;
      
      DelayErrorInnerObserver(Observer<? super R> paramObserver, ObservableConcatMap.ConcatMapDelayErrorObserver<?, R> paramConcatMapDelayErrorObserver)
      {
        this.actual = paramObserver;
        this.parent = paramConcatMapDelayErrorObserver;
      }
      
      public void onComplete()
      {
        ObservableConcatMap.ConcatMapDelayErrorObserver localConcatMapDelayErrorObserver = this.parent;
        localConcatMapDelayErrorObserver.active = false;
        localConcatMapDelayErrorObserver.drain();
      }
      
      public void onError(Throwable paramThrowable)
      {
        ObservableConcatMap.ConcatMapDelayErrorObserver localConcatMapDelayErrorObserver = this.parent;
        if (localConcatMapDelayErrorObserver.error.addThrowable(paramThrowable))
        {
          if (!localConcatMapDelayErrorObserver.tillTheEnd) {
            localConcatMapDelayErrorObserver.d.dispose();
          }
          localConcatMapDelayErrorObserver.active = false;
          localConcatMapDelayErrorObserver.drain();
        }
        else
        {
          RxJavaPlugins.onError(paramThrowable);
        }
      }
      
      public void onNext(R paramR)
      {
        this.actual.onNext(paramR);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        this.parent.arbiter.replace(paramDisposable);
      }
    }
  }
  
  static final class SourceObserver<T, U>
    extends AtomicInteger
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 8828587559905699186L;
    volatile boolean active;
    final Observer<? super U> actual;
    final int bufferSize;
    volatile boolean disposed;
    volatile boolean done;
    int fusionMode;
    final Observer<U> inner;
    final Function<? super T, ? extends ObservableSource<? extends U>> mapper;
    SimpleQueue<T> queue;
    Disposable s;
    final SequentialDisposable sa;
    
    SourceObserver(Observer<? super U> paramObserver, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction, int paramInt)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
      this.bufferSize = paramInt;
      this.inner = new InnerObserver(paramObserver, this);
      this.sa = new SequentialDisposable();
    }
    
    public void dispose()
    {
      this.disposed = true;
      this.sa.dispose();
      this.s.dispose();
      if (getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      for (;;)
      {
        if (this.disposed)
        {
          this.queue.clear();
          return;
        }
        if (!this.active)
        {
          boolean bool = this.done;
          try
          {
            Object localObject = this.queue.poll();
            int i;
            if (localObject == null) {
              i = 1;
            } else {
              i = 0;
            }
            if ((bool) && (i != 0))
            {
              this.actual.onComplete();
              return;
            }
            if (i == 0) {
              try
              {
                localObject = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(localObject), "The mapper returned a null ObservableSource");
                this.active = true;
                ((ObservableSource)localObject).subscribe(this.inner);
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                dispose();
                this.queue.clear();
                this.actual.onError(localThrowable1);
                return;
              }
            }
            if (decrementAndGet() != 0) {}
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            dispose();
            this.queue.clear();
            this.actual.onError(localThrowable2);
            return;
          }
        }
      }
    }
    
    void innerComplete()
    {
      this.active = false;
      drain();
    }
    
    void innerSubscribe(Disposable paramDisposable)
    {
      this.sa.update(paramDisposable);
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.fusionMode == 0) {
        this.queue.offer(paramT);
      }
      drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        if ((paramDisposable instanceof QueueDisposable))
        {
          paramDisposable = (QueueDisposable)paramDisposable;
          int i = paramDisposable.requestFusion(3);
          if (i == 1)
          {
            this.fusionMode = i;
            this.queue = paramDisposable;
            this.done = true;
            this.actual.onSubscribe(this);
            drain();
            return;
          }
          if (i == 2)
          {
            this.fusionMode = i;
            this.queue = paramDisposable;
            this.actual.onSubscribe(this);
            return;
          }
        }
        this.queue = new SpscLinkedArrayQueue(this.bufferSize);
        this.actual.onSubscribe(this);
      }
    }
    
    static final class InnerObserver<U>
      implements Observer<U>
    {
      final Observer<? super U> actual;
      final ObservableConcatMap.SourceObserver<?, ?> parent;
      
      InnerObserver(Observer<? super U> paramObserver, ObservableConcatMap.SourceObserver<?, ?> paramSourceObserver)
      {
        this.actual = paramObserver;
        this.parent = paramSourceObserver;
      }
      
      public void onComplete()
      {
        this.parent.innerComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.parent.dispose();
        this.actual.onError(paramThrowable);
      }
      
      public void onNext(U paramU)
      {
        this.actual.onNext(paramU);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        this.parent.innerSubscribe(paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableConcatMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */