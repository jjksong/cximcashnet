package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.BasicIntQueueDisposable;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.schedulers.TrampolineScheduler;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableObserveOn<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final int bufferSize;
  final boolean delayError;
  final Scheduler scheduler;
  
  public ObservableObserveOn(ObservableSource<T> paramObservableSource, Scheduler paramScheduler, boolean paramBoolean, int paramInt)
  {
    super(paramObservableSource);
    this.scheduler = paramScheduler;
    this.delayError = paramBoolean;
    this.bufferSize = paramInt;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    Object localObject = this.scheduler;
    if ((localObject instanceof TrampolineScheduler))
    {
      this.source.subscribe(paramObserver);
    }
    else
    {
      localObject = ((Scheduler)localObject).createWorker();
      this.source.subscribe(new ObserveOnObserver(paramObserver, (Scheduler.Worker)localObject, this.delayError, this.bufferSize));
    }
  }
  
  static final class ObserveOnObserver<T>
    extends BasicIntQueueDisposable<T>
    implements Observer<T>, Runnable
  {
    private static final long serialVersionUID = 6576896619930983584L;
    final Observer<? super T> actual;
    final int bufferSize;
    volatile boolean cancelled;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    boolean outputFused;
    SimpleQueue<T> queue;
    Disposable s;
    int sourceMode;
    final Scheduler.Worker worker;
    
    ObserveOnObserver(Observer<? super T> paramObserver, Scheduler.Worker paramWorker, boolean paramBoolean, int paramInt)
    {
      this.actual = paramObserver;
      this.worker = paramWorker;
      this.delayError = paramBoolean;
      this.bufferSize = paramInt;
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Observer<? super T> paramObserver)
    {
      if (this.cancelled)
      {
        this.queue.clear();
        return true;
      }
      if (paramBoolean1)
      {
        Throwable localThrowable = this.error;
        if (this.delayError)
        {
          if (paramBoolean2)
          {
            if (localThrowable != null) {
              paramObserver.onError(localThrowable);
            } else {
              paramObserver.onComplete();
            }
            this.worker.dispose();
            return true;
          }
        }
        else
        {
          if (localThrowable != null)
          {
            this.queue.clear();
            paramObserver.onError(localThrowable);
            this.worker.dispose();
            return true;
          }
          if (paramBoolean2)
          {
            paramObserver.onComplete();
            this.worker.dispose();
            return true;
          }
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.dispose();
        this.worker.dispose();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    void drainFused()
    {
      int i = 1;
      int j;
      do
      {
        if (this.cancelled) {
          return;
        }
        boolean bool = this.done;
        Throwable localThrowable = this.error;
        if ((!this.delayError) && (bool) && (localThrowable != null))
        {
          this.actual.onError(localThrowable);
          this.worker.dispose();
          return;
        }
        this.actual.onNext(null);
        if (bool)
        {
          localThrowable = this.error;
          if (localThrowable != null) {
            this.actual.onError(localThrowable);
          } else {
            this.actual.onComplete();
          }
          this.worker.dispose();
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void drainNormal()
    {
      SimpleQueue localSimpleQueue = this.queue;
      Observer localObserver = this.actual;
      int i = 1;
      for (;;)
      {
        if (checkTerminated(this.done, localSimpleQueue.isEmpty(), localObserver)) {
          return;
        }
        boolean bool2 = this.done;
        try
        {
          Object localObject = localSimpleQueue.poll();
          boolean bool1;
          if (localObject == null) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if (checkTerminated(bool2, bool1, localObserver)) {
            return;
          }
          if (bool1)
          {
            int j = addAndGet(-i);
            i = j;
            if (j != 0) {}
          }
          else
          {
            localObserver.onNext(localObject);
          }
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.s.dispose();
          localSimpleQueue.clear();
          localObserver.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      schedule();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      schedule();
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode != 2) {
        this.queue.offer(paramT);
      }
      schedule();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        if ((paramDisposable instanceof QueueDisposable))
        {
          paramDisposable = (QueueDisposable)paramDisposable;
          int i = paramDisposable.requestFusion(7);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.done = true;
            this.actual.onSubscribe(this);
            schedule();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.actual.onSubscribe(this);
            return;
          }
        }
        this.queue = new SpscLinkedArrayQueue(this.bufferSize);
        this.actual.onSubscribe(this);
      }
    }
    
    public T poll()
      throws Exception
    {
      return (T)this.queue.poll();
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
    
    public void run()
    {
      if (this.outputFused) {
        drainFused();
      } else {
        drainNormal();
      }
    }
    
    void schedule()
    {
      if (getAndIncrement() == 0) {
        this.worker.schedule(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableObserveOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */