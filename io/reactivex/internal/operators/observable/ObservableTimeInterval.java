package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.schedulers.Timed;
import java.util.concurrent.TimeUnit;

public final class ObservableTimeInterval<T>
  extends AbstractObservableWithUpstream<T, Timed<T>>
{
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public ObservableTimeInterval(ObservableSource<T> paramObservableSource, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramObservableSource);
    this.scheduler = paramScheduler;
    this.unit = paramTimeUnit;
  }
  
  public void subscribeActual(Observer<? super Timed<T>> paramObserver)
  {
    this.source.subscribe(new TimeIntervalObserver(paramObserver, this.unit, this.scheduler));
  }
  
  static final class TimeIntervalObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super Timed<T>> actual;
    long lastTime;
    Disposable s;
    final Scheduler scheduler;
    final TimeUnit unit;
    
    TimeIntervalObserver(Observer<? super Timed<T>> paramObserver, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.actual = paramObserver;
      this.scheduler = paramScheduler;
      this.unit = paramTimeUnit;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.lastTime;
      this.lastTime = l1;
      this.actual.onNext(new Timed(paramT, l1 - l2, this.unit));
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.lastTime = this.scheduler.now(this.unit);
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTimeInterval.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */