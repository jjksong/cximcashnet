package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableReduceSeedSingle<T, R>
  extends Single<R>
{
  final BiFunction<R, ? super T, R> reducer;
  final R seed;
  final ObservableSource<T> source;
  
  public ObservableReduceSeedSingle(ObservableSource<T> paramObservableSource, R paramR, BiFunction<R, ? super T, R> paramBiFunction)
  {
    this.source = paramObservableSource;
    this.seed = paramR;
    this.reducer = paramBiFunction;
  }
  
  protected void subscribeActual(SingleObserver<? super R> paramSingleObserver)
  {
    this.source.subscribe(new ReduceSeedObserver(paramSingleObserver, this.reducer, this.seed));
  }
  
  static final class ReduceSeedObserver<T, R>
    implements Observer<T>, Disposable
  {
    final SingleObserver<? super R> actual;
    Disposable d;
    final BiFunction<R, ? super T, R> reducer;
    R value;
    
    ReduceSeedObserver(SingleObserver<? super R> paramSingleObserver, BiFunction<R, ? super T, R> paramBiFunction, R paramR)
    {
      this.actual = paramSingleObserver;
      this.value = paramR;
      this.reducer = paramBiFunction;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      Object localObject = this.value;
      this.value = null;
      if (localObject != null) {
        this.actual.onSuccess(localObject);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = this.value;
      this.value = null;
      if (localObject != null) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      Object localObject = this.value;
      if (localObject != null) {
        try
        {
          this.value = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.d.dispose();
          onError(paramT);
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableReduceSeedSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */