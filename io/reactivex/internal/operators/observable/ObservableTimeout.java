package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.ObserverFullArbiter;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.FullArbiterObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableTimeout<T, U, V>
  extends AbstractObservableWithUpstream<T, T>
{
  final ObservableSource<U> firstTimeoutIndicator;
  final Function<? super T, ? extends ObservableSource<V>> itemTimeoutIndicator;
  final ObservableSource<? extends T> other;
  
  public ObservableTimeout(ObservableSource<T> paramObservableSource, ObservableSource<U> paramObservableSource1, Function<? super T, ? extends ObservableSource<V>> paramFunction, ObservableSource<? extends T> paramObservableSource2)
  {
    super(paramObservableSource);
    this.firstTimeoutIndicator = paramObservableSource1;
    this.itemTimeoutIndicator = paramFunction;
    this.other = paramObservableSource2;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    if (this.other == null) {
      this.source.subscribe(new TimeoutObserver(new SerializedObserver(paramObserver), this.firstTimeoutIndicator, this.itemTimeoutIndicator));
    } else {
      this.source.subscribe(new TimeoutOtherObserver(paramObserver, this.firstTimeoutIndicator, this.itemTimeoutIndicator, this.other));
    }
  }
  
  static abstract interface OnTimeout
  {
    public abstract void innerError(Throwable paramThrowable);
    
    public abstract void timeout(long paramLong);
  }
  
  static final class TimeoutInnerObserver<T, U, V>
    extends DisposableObserver<Object>
  {
    boolean done;
    final long index;
    final ObservableTimeout.OnTimeout parent;
    
    TimeoutInnerObserver(ObservableTimeout.OnTimeout paramOnTimeout, long paramLong)
    {
      this.parent = paramOnTimeout;
      this.index = paramLong;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.timeout(this.index);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.parent.timeout(this.index);
    }
  }
  
  static final class TimeoutObserver<T, U, V>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable, ObservableTimeout.OnTimeout
  {
    private static final long serialVersionUID = 2672739326310051084L;
    final Observer<? super T> actual;
    final ObservableSource<U> firstTimeoutIndicator;
    volatile long index;
    final Function<? super T, ? extends ObservableSource<V>> itemTimeoutIndicator;
    Disposable s;
    
    TimeoutObserver(Observer<? super T> paramObserver, ObservableSource<U> paramObservableSource, Function<? super T, ? extends ObservableSource<V>> paramFunction)
    {
      this.actual = paramObserver;
      this.firstTimeoutIndicator = paramObservableSource;
      this.itemTimeoutIndicator = paramFunction;
    }
    
    public void dispose()
    {
      if (DisposableHelper.dispose(this)) {
        this.s.dispose();
      }
    }
    
    public void innerError(Throwable paramThrowable)
    {
      this.s.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this);
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.index + 1L;
      this.index = l;
      this.actual.onNext(paramT);
      Disposable localDisposable = (Disposable)get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      try
      {
        ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.itemTimeoutIndicator.apply(paramT), "The ObservableSource returned is null");
        paramT = new ObservableTimeout.TimeoutInnerObserver(this, l);
        if (compareAndSet(localDisposable, paramT)) {
          localObservableSource.subscribe(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        dispose();
        this.actual.onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        Observer localObserver = this.actual;
        ObservableSource localObservableSource = this.firstTimeoutIndicator;
        if (localObservableSource != null)
        {
          paramDisposable = new ObservableTimeout.TimeoutInnerObserver(this, 0L);
          if (compareAndSet(null, paramDisposable))
          {
            localObserver.onSubscribe(this);
            localObservableSource.subscribe(paramDisposable);
          }
        }
        else
        {
          localObserver.onSubscribe(this);
        }
      }
    }
    
    public void timeout(long paramLong)
    {
      if (paramLong == this.index)
      {
        dispose();
        this.actual.onError(new TimeoutException());
      }
    }
  }
  
  static final class TimeoutOtherObserver<T, U, V>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable, ObservableTimeout.OnTimeout
  {
    private static final long serialVersionUID = -1957813281749686898L;
    final Observer<? super T> actual;
    final ObserverFullArbiter<T> arbiter;
    boolean done;
    final ObservableSource<U> firstTimeoutIndicator;
    volatile long index;
    final Function<? super T, ? extends ObservableSource<V>> itemTimeoutIndicator;
    final ObservableSource<? extends T> other;
    Disposable s;
    
    TimeoutOtherObserver(Observer<? super T> paramObserver, ObservableSource<U> paramObservableSource, Function<? super T, ? extends ObservableSource<V>> paramFunction, ObservableSource<? extends T> paramObservableSource1)
    {
      this.actual = paramObserver;
      this.firstTimeoutIndicator = paramObservableSource;
      this.itemTimeoutIndicator = paramFunction;
      this.other = paramObservableSource1;
      this.arbiter = new ObserverFullArbiter(paramObserver, this, 8);
    }
    
    public void dispose()
    {
      if (DisposableHelper.dispose(this)) {
        this.s.dispose();
      }
    }
    
    public void innerError(Throwable paramThrowable)
    {
      this.s.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.arbiter.onComplete(this.s);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      dispose();
      this.arbiter.onError(paramThrowable, this.s);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      if (!this.arbiter.onNext(paramT, this.s)) {
        return;
      }
      Disposable localDisposable = (Disposable)get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      try
      {
        ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.itemTimeoutIndicator.apply(paramT), "The ObservableSource returned is null");
        paramT = new ObservableTimeout.TimeoutInnerObserver(this, l);
        if (compareAndSet(localDisposable, paramT)) {
          localObservableSource.subscribe(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.arbiter.setDisposable(paramDisposable);
        Observer localObserver = this.actual;
        paramDisposable = this.firstTimeoutIndicator;
        if (paramDisposable != null)
        {
          ObservableTimeout.TimeoutInnerObserver localTimeoutInnerObserver = new ObservableTimeout.TimeoutInnerObserver(this, 0L);
          if (compareAndSet(null, localTimeoutInnerObserver))
          {
            localObserver.onSubscribe(this.arbiter);
            paramDisposable.subscribe(localTimeoutInnerObserver);
          }
        }
        else
        {
          localObserver.onSubscribe(this.arbiter);
        }
      }
    }
    
    public void timeout(long paramLong)
    {
      if (paramLong == this.index)
      {
        dispose();
        this.other.subscribe(new FullArbiterObserver(this.arbiter));
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTimeout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */