package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableIntervalRange
  extends Observable<Long>
{
  final long end;
  final long initialDelay;
  final long period;
  final Scheduler scheduler;
  final long start;
  final TimeUnit unit;
  
  public ObservableIntervalRange(long paramLong1, long paramLong2, long paramLong3, long paramLong4, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    this.initialDelay = paramLong3;
    this.period = paramLong4;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.start = paramLong1;
    this.end = paramLong2;
  }
  
  public void subscribeActual(Observer<? super Long> paramObserver)
  {
    IntervalRangeObserver localIntervalRangeObserver = new IntervalRangeObserver(paramObserver, this.start, this.end);
    paramObserver.onSubscribe(localIntervalRangeObserver);
    localIntervalRangeObserver.setResource(this.scheduler.schedulePeriodicallyDirect(localIntervalRangeObserver, this.initialDelay, this.period, this.unit));
  }
  
  static final class IntervalRangeObserver
    extends AtomicReference<Disposable>
    implements Disposable, Runnable
  {
    private static final long serialVersionUID = 1891866368734007884L;
    final Observer<? super Long> actual;
    long count;
    final long end;
    
    IntervalRangeObserver(Observer<? super Long> paramObserver, long paramLong1, long paramLong2)
    {
      this.actual = paramObserver;
      this.count = paramLong1;
      this.end = paramLong2;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void run()
    {
      if (!isDisposed())
      {
        long l = this.count;
        this.actual.onNext(Long.valueOf(l));
        if (l == this.end)
        {
          DisposableHelper.dispose(this);
          this.actual.onComplete();
          return;
        }
        this.count = (l + 1L);
      }
    }
    
    public void setResource(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableIntervalRange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */