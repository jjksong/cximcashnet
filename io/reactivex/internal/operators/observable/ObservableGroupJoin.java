package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableGroupJoin<TLeft, TRight, TLeftEnd, TRightEnd, R>
  extends AbstractObservableWithUpstream<TLeft, R>
{
  final Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> leftEnd;
  final ObservableSource<? extends TRight> other;
  final BiFunction<? super TLeft, ? super Observable<TRight>, ? extends R> resultSelector;
  final Function<? super TRight, ? extends ObservableSource<TRightEnd>> rightEnd;
  
  public ObservableGroupJoin(ObservableSource<TLeft> paramObservableSource, ObservableSource<? extends TRight> paramObservableSource1, Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> paramFunction, Function<? super TRight, ? extends ObservableSource<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super Observable<TRight>, ? extends R> paramBiFunction)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
    this.leftEnd = paramFunction;
    this.rightEnd = paramFunction1;
    this.resultSelector = paramBiFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    GroupJoinDisposable localGroupJoinDisposable = new GroupJoinDisposable(paramObserver, this.leftEnd, this.rightEnd, this.resultSelector);
    paramObserver.onSubscribe(localGroupJoinDisposable);
    paramObserver = new LeftRightObserver(localGroupJoinDisposable, true);
    localGroupJoinDisposable.disposables.add(paramObserver);
    LeftRightObserver localLeftRightObserver = new LeftRightObserver(localGroupJoinDisposable, false);
    localGroupJoinDisposable.disposables.add(localLeftRightObserver);
    this.source.subscribe(paramObserver);
    this.other.subscribe(localLeftRightObserver);
  }
  
  static final class GroupJoinDisposable<TLeft, TRight, TLeftEnd, TRightEnd, R>
    extends AtomicInteger
    implements Disposable, ObservableGroupJoin.JoinSupport
  {
    static final Integer LEFT_CLOSE = Integer.valueOf(3);
    static final Integer LEFT_VALUE = Integer.valueOf(1);
    static final Integer RIGHT_CLOSE = Integer.valueOf(4);
    static final Integer RIGHT_VALUE = Integer.valueOf(2);
    private static final long serialVersionUID = -6071216598687999801L;
    final AtomicInteger active;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    final CompositeDisposable disposables;
    final AtomicReference<Throwable> error;
    final Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> leftEnd;
    int leftIndex;
    final Map<Integer, UnicastSubject<TRight>> lefts;
    final SpscLinkedArrayQueue<Object> queue;
    final BiFunction<? super TLeft, ? super Observable<TRight>, ? extends R> resultSelector;
    final Function<? super TRight, ? extends ObservableSource<TRightEnd>> rightEnd;
    int rightIndex;
    final Map<Integer, TRight> rights;
    
    GroupJoinDisposable(Observer<? super R> paramObserver, Function<? super TLeft, ? extends ObservableSource<TLeftEnd>> paramFunction, Function<? super TRight, ? extends ObservableSource<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super Observable<TRight>, ? extends R> paramBiFunction)
    {
      this.actual = paramObserver;
      this.disposables = new CompositeDisposable();
      this.queue = new SpscLinkedArrayQueue(Observable.bufferSize());
      this.lefts = new LinkedHashMap();
      this.rights = new LinkedHashMap();
      this.error = new AtomicReference();
      this.leftEnd = paramFunction;
      this.rightEnd = paramFunction1;
      this.resultSelector = paramBiFunction;
      this.active = new AtomicInteger(2);
    }
    
    void cancelAll()
    {
      this.disposables.dispose();
    }
    
    public void dispose()
    {
      if (this.cancelled) {
        return;
      }
      this.cancelled = true;
      cancelAll();
      if (getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
    
    /* Error */
    void drain()
    {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual 127	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:getAndIncrement	()I
      //   4: ifeq +4 -> 8
      //   7: return
      //   8: aload_0
      //   9: getfield 93	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:queue	Lio/reactivex/internal/queue/SpscLinkedArrayQueue;
      //   12: astore 5
      //   14: aload_0
      //   15: getfield 75	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:actual	Lio/reactivex/Observer;
      //   18: astore 4
      //   20: iconst_1
      //   21: istore_1
      //   22: aload_0
      //   23: getfield 122	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:cancelled	Z
      //   26: ifeq +9 -> 35
      //   29: aload 5
      //   31: invokevirtual 130	io/reactivex/internal/queue/SpscLinkedArrayQueue:clear	()V
      //   34: return
      //   35: aload_0
      //   36: getfield 105	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:error	Ljava/util/concurrent/atomic/AtomicReference;
      //   39: invokevirtual 137	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
      //   42: checkcast 133	java/lang/Throwable
      //   45: ifnull +19 -> 64
      //   48: aload 5
      //   50: invokevirtual 130	io/reactivex/internal/queue/SpscLinkedArrayQueue:clear	()V
      //   53: aload_0
      //   54: invokevirtual 124	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:cancelAll	()V
      //   57: aload_0
      //   58: aload 4
      //   60: invokevirtual 141	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:errorAll	(Lio/reactivex/Observer;)V
      //   63: return
      //   64: aload_0
      //   65: getfield 114	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:active	Ljava/util/concurrent/atomic/AtomicInteger;
      //   68: invokevirtual 143	java/util/concurrent/atomic/AtomicInteger:get	()I
      //   71: ifne +8 -> 79
      //   74: iconst_1
      //   75: istore_2
      //   76: goto +5 -> 81
      //   79: iconst_0
      //   80: istore_2
      //   81: aload 5
      //   83: invokevirtual 146	io/reactivex/internal/queue/SpscLinkedArrayQueue:poll	()Ljava/lang/Object;
      //   86: checkcast 56	java/lang/Integer
      //   89: astore 7
      //   91: aload 7
      //   93: ifnonnull +8 -> 101
      //   96: iconst_1
      //   97: istore_3
      //   98: goto +5 -> 103
      //   101: iconst_0
      //   102: istore_3
      //   103: iload_2
      //   104: ifeq +82 -> 186
      //   107: iload_3
      //   108: ifeq +78 -> 186
      //   111: aload_0
      //   112: getfield 98	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:lefts	Ljava/util/Map;
      //   115: invokeinterface 152 1 0
      //   120: invokeinterface 158 1 0
      //   125: astore 5
      //   127: aload 5
      //   129: invokeinterface 164 1 0
      //   134: ifeq +19 -> 153
      //   137: aload 5
      //   139: invokeinterface 167 1 0
      //   144: checkcast 169	io/reactivex/subjects/UnicastSubject
      //   147: invokevirtual 172	io/reactivex/subjects/UnicastSubject:onComplete	()V
      //   150: goto -23 -> 127
      //   153: aload_0
      //   154: getfield 98	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:lefts	Ljava/util/Map;
      //   157: invokeinterface 173 1 0
      //   162: aload_0
      //   163: getfield 100	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rights	Ljava/util/Map;
      //   166: invokeinterface 173 1 0
      //   171: aload_0
      //   172: getfield 80	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:disposables	Lio/reactivex/disposables/CompositeDisposable;
      //   175: invokevirtual 120	io/reactivex/disposables/CompositeDisposable:dispose	()V
      //   178: aload 4
      //   180: invokeinterface 176 1 0
      //   185: return
      //   186: iload_3
      //   187: ifeq +17 -> 204
      //   190: aload_0
      //   191: iload_1
      //   192: ineg
      //   193: invokevirtual 180	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:addAndGet	(I)I
      //   196: istore_2
      //   197: iload_2
      //   198: istore_1
      //   199: iload_2
      //   200: ifne -178 -> 22
      //   203: return
      //   204: aload 5
      //   206: invokevirtual 146	io/reactivex/internal/queue/SpscLinkedArrayQueue:poll	()Ljava/lang/Object;
      //   209: astore 6
      //   211: aload 7
      //   213: getstatic 62	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:LEFT_VALUE	Ljava/lang/Integer;
      //   216: if_acmpne +213 -> 429
      //   219: invokestatic 184	io/reactivex/subjects/UnicastSubject:create	()Lio/reactivex/subjects/UnicastSubject;
      //   222: astore 7
      //   224: aload_0
      //   225: getfield 186	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:leftIndex	I
      //   228: istore_2
      //   229: aload_0
      //   230: iload_2
      //   231: iconst_1
      //   232: iadd
      //   233: putfield 186	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:leftIndex	I
      //   236: aload_0
      //   237: getfield 98	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:lefts	Ljava/util/Map;
      //   240: iload_2
      //   241: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   244: aload 7
      //   246: invokeinterface 190 3 0
      //   251: pop
      //   252: aload_0
      //   253: getfield 107	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:leftEnd	Lio/reactivex/functions/Function;
      //   256: aload 6
      //   258: invokeinterface 196 2 0
      //   263: ldc -58
      //   265: invokestatic 204	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   268: checkcast 206	io/reactivex/ObservableSource
      //   271: astore 9
      //   273: new 208	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver
      //   276: dup
      //   277: aload_0
      //   278: iconst_1
      //   279: iload_2
      //   280: invokespecial 211	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver:<init>	(Lio/reactivex/internal/operators/observable/ObservableGroupJoin$JoinSupport;ZI)V
      //   283: astore 8
      //   285: aload_0
      //   286: getfield 80	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:disposables	Lio/reactivex/disposables/CompositeDisposable;
      //   289: aload 8
      //   291: invokevirtual 215	io/reactivex/disposables/CompositeDisposable:add	(Lio/reactivex/disposables/Disposable;)Z
      //   294: pop
      //   295: aload 9
      //   297: aload 8
      //   299: invokeinterface 218 2 0
      //   304: aload_0
      //   305: getfield 105	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:error	Ljava/util/concurrent/atomic/AtomicReference;
      //   308: invokevirtual 137	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
      //   311: checkcast 133	java/lang/Throwable
      //   314: ifnull +19 -> 333
      //   317: aload 5
      //   319: invokevirtual 130	io/reactivex/internal/queue/SpscLinkedArrayQueue:clear	()V
      //   322: aload_0
      //   323: invokevirtual 124	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:cancelAll	()V
      //   326: aload_0
      //   327: aload 4
      //   329: invokevirtual 141	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:errorAll	(Lio/reactivex/Observer;)V
      //   332: return
      //   333: aload_0
      //   334: getfield 111	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:resultSelector	Lio/reactivex/functions/BiFunction;
      //   337: aload 6
      //   339: aload 7
      //   341: invokeinterface 222 3 0
      //   346: ldc -32
      //   348: invokestatic 204	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   351: astore 6
      //   353: aload 4
      //   355: aload 6
      //   357: invokeinterface 228 2 0
      //   362: aload_0
      //   363: getfield 100	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rights	Ljava/util/Map;
      //   366: invokeinterface 152 1 0
      //   371: invokeinterface 158 1 0
      //   376: astore 6
      //   378: aload 6
      //   380: invokeinterface 164 1 0
      //   385: ifeq -363 -> 22
      //   388: aload 7
      //   390: aload 6
      //   392: invokeinterface 167 1 0
      //   397: invokevirtual 229	io/reactivex/subjects/UnicastSubject:onNext	(Ljava/lang/Object;)V
      //   400: goto -22 -> 378
      //   403: astore 6
      //   405: aload_0
      //   406: aload 6
      //   408: aload 4
      //   410: aload 5
      //   412: invokevirtual 233	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:fail	(Ljava/lang/Throwable;Lio/reactivex/Observer;Lio/reactivex/internal/queue/SpscLinkedArrayQueue;)V
      //   415: return
      //   416: astore 6
      //   418: aload_0
      //   419: aload 6
      //   421: aload 4
      //   423: aload 5
      //   425: invokevirtual 233	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:fail	(Ljava/lang/Throwable;Lio/reactivex/Observer;Lio/reactivex/internal/queue/SpscLinkedArrayQueue;)V
      //   428: return
      //   429: aload 7
      //   431: getstatic 64	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:RIGHT_VALUE	Ljava/lang/Integer;
      //   434: if_acmpne +169 -> 603
      //   437: aload_0
      //   438: getfield 235	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rightIndex	I
      //   441: istore_2
      //   442: aload_0
      //   443: iload_2
      //   444: iconst_1
      //   445: iadd
      //   446: putfield 235	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rightIndex	I
      //   449: aload_0
      //   450: getfield 100	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rights	Ljava/util/Map;
      //   453: iload_2
      //   454: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   457: aload 6
      //   459: invokeinterface 190 3 0
      //   464: pop
      //   465: aload_0
      //   466: getfield 109	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rightEnd	Lio/reactivex/functions/Function;
      //   469: aload 6
      //   471: invokeinterface 196 2 0
      //   476: ldc -19
      //   478: invokestatic 204	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   481: checkcast 206	io/reactivex/ObservableSource
      //   484: astore 7
      //   486: new 208	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver
      //   489: dup
      //   490: aload_0
      //   491: iconst_0
      //   492: iload_2
      //   493: invokespecial 211	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver:<init>	(Lio/reactivex/internal/operators/observable/ObservableGroupJoin$JoinSupport;ZI)V
      //   496: astore 8
      //   498: aload_0
      //   499: getfield 80	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:disposables	Lio/reactivex/disposables/CompositeDisposable;
      //   502: aload 8
      //   504: invokevirtual 215	io/reactivex/disposables/CompositeDisposable:add	(Lio/reactivex/disposables/Disposable;)Z
      //   507: pop
      //   508: aload 7
      //   510: aload 8
      //   512: invokeinterface 218 2 0
      //   517: aload_0
      //   518: getfield 105	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:error	Ljava/util/concurrent/atomic/AtomicReference;
      //   521: invokevirtual 137	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
      //   524: checkcast 133	java/lang/Throwable
      //   527: ifnull +19 -> 546
      //   530: aload 5
      //   532: invokevirtual 130	io/reactivex/internal/queue/SpscLinkedArrayQueue:clear	()V
      //   535: aload_0
      //   536: invokevirtual 124	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:cancelAll	()V
      //   539: aload_0
      //   540: aload 4
      //   542: invokevirtual 141	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:errorAll	(Lio/reactivex/Observer;)V
      //   545: return
      //   546: aload_0
      //   547: getfield 98	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:lefts	Ljava/util/Map;
      //   550: invokeinterface 152 1 0
      //   555: invokeinterface 158 1 0
      //   560: astore 7
      //   562: aload 7
      //   564: invokeinterface 164 1 0
      //   569: ifeq -547 -> 22
      //   572: aload 7
      //   574: invokeinterface 167 1 0
      //   579: checkcast 169	io/reactivex/subjects/UnicastSubject
      //   582: aload 6
      //   584: invokevirtual 229	io/reactivex/subjects/UnicastSubject:onNext	(Ljava/lang/Object;)V
      //   587: goto -25 -> 562
      //   590: astore 6
      //   592: aload_0
      //   593: aload 6
      //   595: aload 4
      //   597: aload 5
      //   599: invokevirtual 233	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:fail	(Ljava/lang/Throwable;Lio/reactivex/Observer;Lio/reactivex/internal/queue/SpscLinkedArrayQueue;)V
      //   602: return
      //   603: aload 7
      //   605: getstatic 66	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:LEFT_CLOSE	Ljava/lang/Integer;
      //   608: if_acmpne +55 -> 663
      //   611: aload 6
      //   613: checkcast 208	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver
      //   616: astore 6
      //   618: aload_0
      //   619: getfield 98	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:lefts	Ljava/util/Map;
      //   622: aload 6
      //   624: getfield 240	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver:index	I
      //   627: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   630: invokeinterface 243 2 0
      //   635: checkcast 169	io/reactivex/subjects/UnicastSubject
      //   638: astore 7
      //   640: aload_0
      //   641: getfield 80	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:disposables	Lio/reactivex/disposables/CompositeDisposable;
      //   644: aload 6
      //   646: invokevirtual 245	io/reactivex/disposables/CompositeDisposable:remove	(Lio/reactivex/disposables/Disposable;)Z
      //   649: pop
      //   650: aload 7
      //   652: ifnull -630 -> 22
      //   655: aload 7
      //   657: invokevirtual 172	io/reactivex/subjects/UnicastSubject:onComplete	()V
      //   660: goto -638 -> 22
      //   663: aload 7
      //   665: getstatic 68	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:RIGHT_CLOSE	Ljava/lang/Integer;
      //   668: if_acmpne -646 -> 22
      //   671: aload 6
      //   673: checkcast 208	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver
      //   676: astore 6
      //   678: aload_0
      //   679: getfield 100	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:rights	Ljava/util/Map;
      //   682: aload 6
      //   684: getfield 240	io/reactivex/internal/operators/observable/ObservableGroupJoin$LeftRightEndObserver:index	I
      //   687: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   690: invokeinterface 243 2 0
      //   695: pop
      //   696: aload_0
      //   697: getfield 80	io/reactivex/internal/operators/observable/ObservableGroupJoin$GroupJoinDisposable:disposables	Lio/reactivex/disposables/CompositeDisposable;
      //   700: aload 6
      //   702: invokevirtual 245	io/reactivex/disposables/CompositeDisposable:remove	(Lio/reactivex/disposables/Disposable;)Z
      //   705: pop
      //   706: goto -684 -> 22
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	709	0	this	GroupJoinDisposable
      //   21	178	1	i	int
      //   75	418	2	j	int
      //   97	90	3	k	int
      //   18	578	4	localObserver	Observer
      //   12	586	5	localObject1	Object
      //   209	182	6	localObject2	Object
      //   403	4	6	localThrowable1	Throwable
      //   416	167	6	localThrowable2	Throwable
      //   590	22	6	localThrowable3	Throwable
      //   616	85	6	localLeftRightEndObserver1	ObservableGroupJoin.LeftRightEndObserver
      //   89	575	7	localObject3	Object
      //   283	228	8	localLeftRightEndObserver2	ObservableGroupJoin.LeftRightEndObserver
      //   271	25	9	localObservableSource	ObservableSource
      // Exception table:
      //   from	to	target	type
      //   333	353	403	java/lang/Throwable
      //   252	273	416	java/lang/Throwable
      //   465	486	590	java/lang/Throwable
    }
    
    void errorAll(Observer<?> paramObserver)
    {
      Throwable localThrowable = ExceptionHelper.terminate(this.error);
      Iterator localIterator = this.lefts.values().iterator();
      while (localIterator.hasNext()) {
        ((UnicastSubject)localIterator.next()).onError(localThrowable);
      }
      this.lefts.clear();
      this.rights.clear();
      paramObserver.onError(localThrowable);
    }
    
    void fail(Throwable paramThrowable, Observer<?> paramObserver, SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      Exceptions.throwIfFatal(paramThrowable);
      ExceptionHelper.addThrowable(this.error, paramThrowable);
      paramSpscLinkedArrayQueue.clear();
      cancelAll();
      errorAll(paramObserver);
    }
    
    public void innerClose(boolean paramBoolean, ObservableGroupJoin.LeftRightEndObserver paramLeftRightEndObserver)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_CLOSE;
        } else {
          localInteger = RIGHT_CLOSE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramLeftRightEndObserver);
        drain();
        return;
      }
      finally {}
    }
    
    public void innerCloseError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerComplete(ObservableGroupJoin.LeftRightObserver paramLeftRightObserver)
    {
      this.disposables.delete(paramLeftRightObserver);
      this.active.decrementAndGet();
      drain();
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable))
      {
        this.active.decrementAndGet();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerValue(boolean paramBoolean, Object paramObject)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_VALUE;
        } else {
          localInteger = RIGHT_VALUE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramObject);
        drain();
        return;
      }
      finally {}
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
  }
  
  static abstract interface JoinSupport
  {
    public abstract void innerClose(boolean paramBoolean, ObservableGroupJoin.LeftRightEndObserver paramLeftRightEndObserver);
    
    public abstract void innerCloseError(Throwable paramThrowable);
    
    public abstract void innerComplete(ObservableGroupJoin.LeftRightObserver paramLeftRightObserver);
    
    public abstract void innerError(Throwable paramThrowable);
    
    public abstract void innerValue(boolean paramBoolean, Object paramObject);
  }
  
  static final class LeftRightEndObserver
    extends AtomicReference<Disposable>
    implements Observer<Object>, Disposable
  {
    private static final long serialVersionUID = 1883890389173668373L;
    final int index;
    final boolean isLeft;
    final ObservableGroupJoin.JoinSupport parent;
    
    LeftRightEndObserver(ObservableGroupJoin.JoinSupport paramJoinSupport, boolean paramBoolean, int paramInt)
    {
      this.parent = paramJoinSupport;
      this.isLeft = paramBoolean;
      this.index = paramInt;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.parent.innerClose(this.isLeft, this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerCloseError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (DisposableHelper.dispose(this)) {
        this.parent.innerClose(this.isLeft, this);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
  
  static final class LeftRightObserver
    extends AtomicReference<Disposable>
    implements Observer<Object>, Disposable
  {
    private static final long serialVersionUID = 1883890389173668373L;
    final boolean isLeft;
    final ObservableGroupJoin.JoinSupport parent;
    
    LeftRightObserver(ObservableGroupJoin.JoinSupport paramJoinSupport, boolean paramBoolean)
    {
      this.parent = paramJoinSupport;
      this.isLeft = paramBoolean;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.parent.innerComplete(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.parent.innerValue(this.isLeft, paramObject);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableGroupJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */