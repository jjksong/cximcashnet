package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;

public final class ObservableZipIterable<T, U, V>
  extends Observable<V>
{
  final Iterable<U> other;
  final Observable<? extends T> source;
  final BiFunction<? super T, ? super U, ? extends V> zipper;
  
  public ObservableZipIterable(Observable<? extends T> paramObservable, Iterable<U> paramIterable, BiFunction<? super T, ? super U, ? extends V> paramBiFunction)
  {
    this.source = paramObservable;
    this.other = paramIterable;
    this.zipper = paramBiFunction;
  }
  
  public void subscribeActual(Observer<? super V> paramObserver)
  {
    try
    {
      Iterator localIterator = (Iterator)ObjectHelper.requireNonNull(this.other.iterator(), "The iterator returned by other is null");
      try
      {
        boolean bool = localIterator.hasNext();
        if (!bool)
        {
          EmptyDisposable.complete(paramObserver);
          return;
        }
        this.source.subscribe(new ZipIterableObserver(paramObserver, localIterator, this.zipper));
        return;
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        EmptyDisposable.error(localThrowable1, paramObserver);
        return;
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptyDisposable.error(localThrowable2, paramObserver);
    }
  }
  
  static final class ZipIterableObserver<T, U, V>
    implements Observer<T>, Disposable
  {
    final Observer<? super V> actual;
    boolean done;
    final Iterator<U> iterator;
    Disposable s;
    final BiFunction<? super T, ? super U, ? extends V> zipper;
    
    ZipIterableObserver(Observer<? super V> paramObserver, Iterator<U> paramIterator, BiFunction<? super T, ? super U, ? extends V> paramBiFunction)
    {
      this.actual = paramObserver;
      this.iterator = paramIterator;
      this.zipper = paramBiFunction;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    void error(Throwable paramThrowable)
    {
      this.done = true;
      this.s.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    /* Error */
    public void onNext(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 48	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:done	Z
      //   4: ifeq +4 -> 8
      //   7: return
      //   8: aload_0
      //   9: getfield 34	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:iterator	Ljava/util/Iterator;
      //   12: invokeinterface 71 1 0
      //   17: ldc 73
      //   19: invokestatic 79	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   22: astore_3
      //   23: aload_0
      //   24: getfield 36	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:zipper	Lio/reactivex/functions/BiFunction;
      //   27: aload_1
      //   28: aload_3
      //   29: invokeinterface 85 3 0
      //   34: ldc 87
      //   36: invokestatic 79	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   39: astore_1
      //   40: aload_0
      //   41: getfield 32	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:actual	Lio/reactivex/Observer;
      //   44: aload_1
      //   45: invokeinterface 89 2 0
      //   50: aload_0
      //   51: getfield 34	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:iterator	Ljava/util/Iterator;
      //   54: invokeinterface 92 1 0
      //   59: istore_2
      //   60: iload_2
      //   61: ifne +26 -> 87
      //   64: aload_0
      //   65: iconst_1
      //   66: putfield 48	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:done	Z
      //   69: aload_0
      //   70: getfield 42	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:s	Lio/reactivex/disposables/Disposable;
      //   73: invokeinterface 44 1 0
      //   78: aload_0
      //   79: getfield 32	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:actual	Lio/reactivex/Observer;
      //   82: invokeinterface 58 1 0
      //   87: return
      //   88: astore_1
      //   89: aload_1
      //   90: invokestatic 97	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   93: aload_0
      //   94: aload_1
      //   95: invokevirtual 99	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:error	(Ljava/lang/Throwable;)V
      //   98: return
      //   99: astore_1
      //   100: aload_1
      //   101: invokestatic 97	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   104: aload_0
      //   105: aload_1
      //   106: invokevirtual 99	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:error	(Ljava/lang/Throwable;)V
      //   109: return
      //   110: astore_1
      //   111: aload_1
      //   112: invokestatic 97	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   115: aload_0
      //   116: aload_1
      //   117: invokevirtual 99	io/reactivex/internal/operators/observable/ObservableZipIterable$ZipIterableObserver:error	(Ljava/lang/Throwable;)V
      //   120: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	121	0	this	ZipIterableObserver
      //   0	121	1	paramT	T
      //   59	2	2	bool	boolean
      //   22	7	3	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   50	60	88	java/lang/Throwable
      //   23	40	99	java/lang/Throwable
      //   8	23	110	java/lang/Throwable
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableZipIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */