package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableLastMaybe<T>
  extends Maybe<T>
{
  final ObservableSource<T> source;
  
  public ObservableLastMaybe(ObservableSource<T> paramObservableSource)
  {
    this.source = paramObservableSource;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new LastObserver(paramMaybeObserver));
  }
  
  static final class LastObserver<T>
    implements Observer<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    T item;
    Disposable s;
    
    LastObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      this.s.dispose();
      this.s = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = DisposableHelper.DISPOSED;
      Object localObject = this.item;
      if (localObject != null)
      {
        this.item = null;
        this.actual.onSuccess(localObject);
      }
      else
      {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = DisposableHelper.DISPOSED;
      this.item = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.item = paramT;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableLastMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */