package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableOnErrorReturn<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Function<? super Throwable, ? extends T> valueSupplier;
  
  public ObservableOnErrorReturn(ObservableSource<T> paramObservableSource, Function<? super Throwable, ? extends T> paramFunction)
  {
    super(paramObservableSource);
    this.valueSupplier = paramFunction;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new OnErrorReturnObserver(paramObserver, this.valueSupplier));
  }
  
  static final class OnErrorReturnObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    Disposable s;
    final Function<? super Throwable, ? extends T> valueSupplier;
    
    OnErrorReturnObserver(Observer<? super T> paramObserver, Function<? super Throwable, ? extends T> paramFunction)
    {
      this.actual = paramObserver;
      this.valueSupplier = paramFunction;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        Object localObject = this.valueSupplier.apply(paramThrowable);
        if (localObject == null)
        {
          localObject = new NullPointerException("The supplied value is null");
          ((NullPointerException)localObject).initCause(paramThrowable);
          this.actual.onError((Throwable)localObject);
          return;
        }
        this.actual.onNext(localObject);
        this.actual.onComplete();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableOnErrorReturn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */