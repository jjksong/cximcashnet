package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableRepeatWhen<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Function<? super Observable<Object>, ? extends ObservableSource<?>> handler;
  
  public ObservableRepeatWhen(ObservableSource<T> paramObservableSource, Function<? super Observable<Object>, ? extends ObservableSource<?>> paramFunction)
  {
    super(paramObservableSource);
    this.handler = paramFunction;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    Object localObject = PublishSubject.create().toSerialized();
    try
    {
      ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.handler.apply(localObject), "The handler returned a null ObservableSource");
      localObject = new RepeatWhenObserver(paramObserver, (Subject)localObject, this.source);
      paramObserver.onSubscribe((Disposable)localObject);
      localObservableSource.subscribe(((RepeatWhenObserver)localObject).inner);
      ((RepeatWhenObserver)localObject).subscribeNext();
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
  
  static final class RepeatWhenObserver<T>
    extends AtomicInteger
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 802743776666017014L;
    volatile boolean active;
    final Observer<? super T> actual;
    final AtomicReference<Disposable> d;
    final AtomicThrowable error;
    final RepeatWhenObserver<T>.InnerRepeatObserver inner;
    final Subject<Object> signaller;
    final ObservableSource<T> source;
    final AtomicInteger wip;
    
    RepeatWhenObserver(Observer<? super T> paramObserver, Subject<Object> paramSubject, ObservableSource<T> paramObservableSource)
    {
      this.actual = paramObserver;
      this.signaller = paramSubject;
      this.source = paramObservableSource;
      this.wip = new AtomicInteger();
      this.error = new AtomicThrowable();
      this.inner = new InnerRepeatObserver();
      this.d = new AtomicReference();
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.d);
      DisposableHelper.dispose(this.inner);
    }
    
    void innerComplete()
    {
      DisposableHelper.dispose(this.d);
      HalfSerializer.onComplete(this.actual, this, this.error);
    }
    
    void innerError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.d);
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    void innerNext()
    {
      subscribeNext();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.d.get());
    }
    
    public void onComplete()
    {
      this.active = false;
      this.signaller.onNext(Integer.valueOf(0));
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.inner);
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    public void onNext(T paramT)
    {
      HalfSerializer.onNext(this.actual, paramT, this, this.error);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.replace(this.d, paramDisposable);
    }
    
    void subscribeNext()
    {
      if (this.wip.getAndIncrement() == 0) {
        do
        {
          if (isDisposed()) {
            return;
          }
          if (!this.active)
          {
            this.active = true;
            this.source.subscribe(this);
          }
        } while (this.wip.decrementAndGet() != 0);
      }
    }
    
    final class InnerRepeatObserver
      extends AtomicReference<Disposable>
      implements Observer<Object>
    {
      private static final long serialVersionUID = 3254781284376480842L;
      
      InnerRepeatObserver() {}
      
      public void onComplete()
      {
        ObservableRepeatWhen.RepeatWhenObserver.this.innerComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        ObservableRepeatWhen.RepeatWhenObserver.this.innerError(paramThrowable);
      }
      
      public void onNext(Object paramObject)
      {
        ObservableRepeatWhen.RepeatWhenObserver.this.innerNext();
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this, paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableRepeatWhen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */