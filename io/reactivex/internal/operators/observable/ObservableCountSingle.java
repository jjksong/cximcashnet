package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableCountSingle<T>
  extends Single<Long>
  implements FuseToObservable<Long>
{
  final ObservableSource<T> source;
  
  public ObservableCountSingle(ObservableSource<T> paramObservableSource)
  {
    this.source = paramObservableSource;
  }
  
  public Observable<Long> fuseToObservable()
  {
    return RxJavaPlugins.onAssembly(new ObservableCount(this.source));
  }
  
  public void subscribeActual(SingleObserver<? super Long> paramSingleObserver)
  {
    this.source.subscribe(new CountObserver(paramSingleObserver));
  }
  
  static final class CountObserver
    implements Observer<Object>, Disposable
  {
    final SingleObserver<? super Long> actual;
    long count;
    Disposable d;
    
    CountObserver(SingleObserver<? super Long> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Long.valueOf(this.count));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.count += 1L;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCountSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */