package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.ArrayCompositeDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;

public final class ObservableSkipUntil<T, U>
  extends AbstractObservableWithUpstream<T, T>
{
  final ObservableSource<U> other;
  
  public ObservableSkipUntil(ObservableSource<T> paramObservableSource, ObservableSource<U> paramObservableSource1)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
  }
  
  public void subscribeActual(final Observer<? super T> paramObserver)
  {
    final SerializedObserver localSerializedObserver = new SerializedObserver(paramObserver);
    final ArrayCompositeDisposable localArrayCompositeDisposable = new ArrayCompositeDisposable(2);
    localSerializedObserver.onSubscribe(localArrayCompositeDisposable);
    paramObserver = new SkipUntilObserver(localSerializedObserver, localArrayCompositeDisposable);
    this.other.subscribe(new Observer()
    {
      Disposable s;
      
      public void onComplete()
      {
        paramObserver.notSkipping = true;
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        localArrayCompositeDisposable.dispose();
        localSerializedObserver.onError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        this.s.dispose();
        paramObserver.notSkipping = true;
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        if (DisposableHelper.validate(this.s, paramAnonymousDisposable))
        {
          this.s = paramAnonymousDisposable;
          localArrayCompositeDisposable.setResource(1, paramAnonymousDisposable);
        }
      }
    });
    this.source.subscribe(paramObserver);
  }
  
  static final class SkipUntilObserver<T>
    implements Observer<T>
  {
    final Observer<? super T> actual;
    final ArrayCompositeDisposable frc;
    volatile boolean notSkipping;
    boolean notSkippingLocal;
    Disposable s;
    
    SkipUntilObserver(Observer<? super T> paramObserver, ArrayCompositeDisposable paramArrayCompositeDisposable)
    {
      this.actual = paramObserver;
      this.frc = paramArrayCompositeDisposable;
    }
    
    public void onComplete()
    {
      this.frc.dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.frc.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.notSkippingLocal)
      {
        this.actual.onNext(paramT);
      }
      else if (this.notSkipping)
      {
        this.notSkippingLocal = true;
        this.actual.onNext(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.frc.setResource(0, paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSkipUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */