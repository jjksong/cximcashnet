package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiPredicate;
import io.reactivex.internal.disposables.ArrayCompositeDisposable;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableSequenceEqual<T>
  extends Observable<Boolean>
{
  final int bufferSize;
  final BiPredicate<? super T, ? super T> comparer;
  final ObservableSource<? extends T> first;
  final ObservableSource<? extends T> second;
  
  public ObservableSequenceEqual(ObservableSource<? extends T> paramObservableSource1, ObservableSource<? extends T> paramObservableSource2, BiPredicate<? super T, ? super T> paramBiPredicate, int paramInt)
  {
    this.first = paramObservableSource1;
    this.second = paramObservableSource2;
    this.comparer = paramBiPredicate;
    this.bufferSize = paramInt;
  }
  
  public void subscribeActual(Observer<? super Boolean> paramObserver)
  {
    EqualCoordinator localEqualCoordinator = new EqualCoordinator(paramObserver, this.bufferSize, this.first, this.second, this.comparer);
    paramObserver.onSubscribe(localEqualCoordinator);
    localEqualCoordinator.subscribe();
  }
  
  static final class EqualCoordinator<T>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = -6178010334400373240L;
    final Observer<? super Boolean> actual;
    volatile boolean cancelled;
    final BiPredicate<? super T, ? super T> comparer;
    final ObservableSource<? extends T> first;
    final ObservableSequenceEqual.EqualObserver<T>[] observers;
    final ArrayCompositeDisposable resources;
    final ObservableSource<? extends T> second;
    T v1;
    T v2;
    
    EqualCoordinator(Observer<? super Boolean> paramObserver, int paramInt, ObservableSource<? extends T> paramObservableSource1, ObservableSource<? extends T> paramObservableSource2, BiPredicate<? super T, ? super T> paramBiPredicate)
    {
      this.actual = paramObserver;
      this.first = paramObservableSource1;
      this.second = paramObservableSource2;
      this.comparer = paramBiPredicate;
      paramObserver = new ObservableSequenceEqual.EqualObserver[2];
      this.observers = paramObserver;
      paramObserver[0] = new ObservableSequenceEqual.EqualObserver(this, 0, paramInt);
      paramObserver[1] = new ObservableSequenceEqual.EqualObserver(this, 1, paramInt);
      this.resources = new ArrayCompositeDisposable(2);
    }
    
    void cancel(SpscLinkedArrayQueue<T> paramSpscLinkedArrayQueue1, SpscLinkedArrayQueue<T> paramSpscLinkedArrayQueue2)
    {
      this.cancelled = true;
      paramSpscLinkedArrayQueue1.clear();
      paramSpscLinkedArrayQueue2.clear();
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.resources.dispose();
        if (getAndIncrement() == 0)
        {
          ObservableSequenceEqual.EqualObserver[] arrayOfEqualObserver = this.observers;
          arrayOfEqualObserver[0].queue.clear();
          arrayOfEqualObserver[1].queue.clear();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Object localObject1 = this.observers;
      Object localObject2 = localObject1[0];
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = ((ObservableSequenceEqual.EqualObserver)localObject2).queue;
      Object localObject3 = localObject1[1];
      localObject1 = ((ObservableSequenceEqual.EqualObserver)localObject3).queue;
      int i = 1;
      int j;
      do
      {
        int k;
        do
        {
          if (this.cancelled)
          {
            localSpscLinkedArrayQueue.clear();
            ((SpscLinkedArrayQueue)localObject1).clear();
            return;
          }
          boolean bool1 = ((ObservableSequenceEqual.EqualObserver)localObject2).done;
          Throwable localThrowable2;
          if (bool1)
          {
            localThrowable2 = ((ObservableSequenceEqual.EqualObserver)localObject2).error;
            if (localThrowable2 != null)
            {
              cancel(localSpscLinkedArrayQueue, (SpscLinkedArrayQueue)localObject1);
              this.actual.onError(localThrowable2);
              return;
            }
          }
          boolean bool2 = ((ObservableSequenceEqual.EqualObserver)localObject3).done;
          if (bool2)
          {
            localThrowable2 = ((ObservableSequenceEqual.EqualObserver)localObject3).error;
            if (localThrowable2 != null)
            {
              cancel(localSpscLinkedArrayQueue, (SpscLinkedArrayQueue)localObject1);
              this.actual.onError(localThrowable2);
              return;
            }
          }
          if (this.v1 == null) {
            this.v1 = localSpscLinkedArrayQueue.poll();
          }
          if (this.v1 == null) {
            j = 1;
          } else {
            j = 0;
          }
          if (this.v2 == null) {
            this.v2 = ((SpscLinkedArrayQueue)localObject1).poll();
          }
          if (this.v2 == null) {
            k = 1;
          } else {
            k = 0;
          }
          if ((bool1) && (bool2) && (j != 0) && (k != 0))
          {
            this.actual.onNext(Boolean.valueOf(true));
            this.actual.onComplete();
            return;
          }
          if ((bool1) && (bool2) && (j != k))
          {
            cancel(localSpscLinkedArrayQueue, (SpscLinkedArrayQueue)localObject1);
            this.actual.onNext(Boolean.valueOf(false));
            this.actual.onComplete();
            return;
          }
          if ((j == 0) && (k == 0)) {
            try
            {
              bool1 = this.comparer.test(this.v1, this.v2);
              if (!bool1)
              {
                cancel(localSpscLinkedArrayQueue, (SpscLinkedArrayQueue)localObject1);
                this.actual.onNext(Boolean.valueOf(false));
                this.actual.onComplete();
                return;
              }
              this.v1 = null;
              this.v2 = null;
            }
            catch (Throwable localThrowable1)
            {
              Exceptions.throwIfFatal(localThrowable1);
              cancel(localSpscLinkedArrayQueue, (SpscLinkedArrayQueue)localObject1);
              this.actual.onError(localThrowable1);
              return;
            }
          }
        } while ((j == 0) && (k == 0));
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    boolean setDisposable(Disposable paramDisposable, int paramInt)
    {
      return this.resources.setResource(paramInt, paramDisposable);
    }
    
    void subscribe()
    {
      ObservableSequenceEqual.EqualObserver[] arrayOfEqualObserver = this.observers;
      this.first.subscribe(arrayOfEqualObserver[0]);
      this.second.subscribe(arrayOfEqualObserver[1]);
    }
  }
  
  static final class EqualObserver<T>
    implements Observer<T>
  {
    volatile boolean done;
    Throwable error;
    final int index;
    final ObservableSequenceEqual.EqualCoordinator<T> parent;
    final SpscLinkedArrayQueue<T> queue;
    
    EqualObserver(ObservableSequenceEqual.EqualCoordinator<T> paramEqualCoordinator, int paramInt1, int paramInt2)
    {
      this.parent = paramEqualCoordinator;
      this.index = paramInt1;
      this.queue = new SpscLinkedArrayQueue(paramInt2);
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      this.parent.drain();
    }
    
    public void onNext(T paramT)
    {
      this.queue.offer(paramT);
      this.parent.drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.parent.setDisposable(paramDisposable, this.index);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSequenceEqual.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */