package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableBufferBoundary<T, U extends Collection<? super T>, Open, Close>
  extends AbstractObservableWithUpstream<T, U>
{
  final Function<? super Open, ? extends ObservableSource<? extends Close>> bufferClose;
  final ObservableSource<? extends Open> bufferOpen;
  final Callable<U> bufferSupplier;
  
  public ObservableBufferBoundary(ObservableSource<T> paramObservableSource, ObservableSource<? extends Open> paramObservableSource1, Function<? super Open, ? extends ObservableSource<? extends Close>> paramFunction, Callable<U> paramCallable)
  {
    super(paramObservableSource);
    this.bufferOpen = paramObservableSource1;
    this.bufferClose = paramFunction;
    this.bufferSupplier = paramCallable;
  }
  
  protected void subscribeActual(Observer<? super U> paramObserver)
  {
    this.source.subscribe(new BufferBoundaryObserver(new SerializedObserver(paramObserver), this.bufferOpen, this.bufferClose, this.bufferSupplier));
  }
  
  static final class BufferBoundaryObserver<T, U extends Collection<? super T>, Open, Close>
    extends QueueDrainObserver<T, U, U>
    implements Disposable
  {
    final Function<? super Open, ? extends ObservableSource<? extends Close>> bufferClose;
    final ObservableSource<? extends Open> bufferOpen;
    final Callable<U> bufferSupplier;
    final List<U> buffers;
    final CompositeDisposable resources;
    Disposable s;
    final AtomicInteger windows = new AtomicInteger();
    
    BufferBoundaryObserver(Observer<? super U> paramObserver, ObservableSource<? extends Open> paramObservableSource, Function<? super Open, ? extends ObservableSource<? extends Close>> paramFunction, Callable<U> paramCallable)
    {
      super(new MpscLinkedQueue());
      this.bufferOpen = paramObservableSource;
      this.bufferClose = paramFunction;
      this.bufferSupplier = paramCallable;
      this.buffers = new LinkedList();
      this.resources = new CompositeDisposable();
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      paramObserver.onNext(paramU);
    }
    
    void close(U paramU, Disposable paramDisposable)
    {
      try
      {
        boolean bool = this.buffers.remove(paramU);
        if (bool) {
          fastPathOrderedEmit(paramU, false, this);
        }
        if ((this.resources.remove(paramDisposable)) && (this.windows.decrementAndGet() == 0)) {
          complete();
        }
        return;
      }
      finally {}
    }
    
    void complete()
    {
      try
      {
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>(this.buffers);
        this.buffers.clear();
        SimpleQueue localSimpleQueue = this.queue;
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext()) {
          localSimpleQueue.offer((Collection)((Iterator)localObject2).next());
        }
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainLoop(localSimpleQueue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.resources.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (this.windows.decrementAndGet() == 0) {
        complete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      dispose();
      this.cancelled = true;
      try
      {
        this.buffers.clear();
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Iterator localIterator = this.buffers.iterator();
        while (localIterator.hasNext()) {
          ((Collection)localIterator.next()).add(paramT);
        }
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        paramDisposable = new ObservableBufferBoundary.BufferOpenObserver(this);
        this.resources.add(paramDisposable);
        this.actual.onSubscribe(this);
        this.windows.lazySet(1);
        this.bufferOpen.subscribe(paramDisposable);
      }
    }
    
    /* Error */
    void open(Open paramOpen)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 151	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:cancelled	Z
      //   4: ifeq +4 -> 8
      //   7: return
      //   8: aload_0
      //   9: getfield 49	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:bufferSupplier	Ljava/util/concurrent/Callable;
      //   12: invokeinterface 201 1 0
      //   17: ldc -53
      //   19: invokestatic 209	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   22: checkcast 66	java/util/Collection
      //   25: astore_2
      //   26: aload_0
      //   27: getfield 47	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:bufferClose	Lio/reactivex/functions/Function;
      //   30: aload_1
      //   31: invokeinterface 215 2 0
      //   36: ldc -39
      //   38: invokestatic 209	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   41: checkcast 189	io/reactivex/ObservableSource
      //   44: astore_1
      //   45: aload_0
      //   46: getfield 151	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:cancelled	Z
      //   49: ifeq +4 -> 53
      //   52: return
      //   53: aload_0
      //   54: monitorenter
      //   55: aload_0
      //   56: getfield 151	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:cancelled	Z
      //   59: ifeq +6 -> 65
      //   62: aload_0
      //   63: monitorexit
      //   64: return
      //   65: aload_0
      //   66: getfield 54	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:buffers	Ljava/util/List;
      //   69: aload_2
      //   70: invokeinterface 218 2 0
      //   75: pop
      //   76: aload_0
      //   77: monitorexit
      //   78: new 220	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferCloseObserver
      //   81: dup
      //   82: aload_2
      //   83: aload_0
      //   84: invokespecial 223	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferCloseObserver:<init>	(Ljava/util/Collection;Lio/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver;)V
      //   87: astore_2
      //   88: aload_0
      //   89: getfield 59	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:resources	Lio/reactivex/disposables/CompositeDisposable;
      //   92: aload_2
      //   93: invokevirtual 181	io/reactivex/disposables/CompositeDisposable:add	(Lio/reactivex/disposables/Disposable;)Z
      //   96: pop
      //   97: aload_0
      //   98: getfield 43	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:windows	Ljava/util/concurrent/atomic/AtomicInteger;
      //   101: invokevirtual 226	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
      //   104: pop
      //   105: aload_1
      //   106: aload_2
      //   107: invokeinterface 193 2 0
      //   112: return
      //   113: astore_1
      //   114: aload_0
      //   115: monitorexit
      //   116: aload_1
      //   117: athrow
      //   118: astore_1
      //   119: aload_1
      //   120: invokestatic 231	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   123: aload_0
      //   124: aload_1
      //   125: invokevirtual 232	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:onError	(Ljava/lang/Throwable;)V
      //   128: return
      //   129: astore_1
      //   130: aload_1
      //   131: invokestatic 231	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   134: aload_0
      //   135: aload_1
      //   136: invokevirtual 232	io/reactivex/internal/operators/observable/ObservableBufferBoundary$BufferBoundaryObserver:onError	(Ljava/lang/Throwable;)V
      //   139: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	140	0	this	BufferBoundaryObserver
      //   0	140	1	paramOpen	Open
      //   25	82	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   55	64	113	finally
      //   65	78	113	finally
      //   114	116	113	finally
      //   26	45	118	java/lang/Throwable
      //   8	26	129	java/lang/Throwable
    }
    
    void openFinished(Disposable paramDisposable)
    {
      if ((this.resources.remove(paramDisposable)) && (this.windows.decrementAndGet() == 0)) {
        complete();
      }
    }
  }
  
  static final class BufferCloseObserver<T, U extends Collection<? super T>, Open, Close>
    extends DisposableObserver<Close>
  {
    boolean done;
    final ObservableBufferBoundary.BufferBoundaryObserver<T, U, Open, Close> parent;
    final U value;
    
    BufferCloseObserver(U paramU, ObservableBufferBoundary.BufferBoundaryObserver<T, U, Open, Close> paramBufferBoundaryObserver)
    {
      this.parent = paramBufferBoundaryObserver;
      this.value = paramU;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.close(this.value, this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(Close paramClose)
    {
      onComplete();
    }
  }
  
  static final class BufferOpenObserver<T, U extends Collection<? super T>, Open, Close>
    extends DisposableObserver<Open>
  {
    boolean done;
    final ObservableBufferBoundary.BufferBoundaryObserver<T, U, Open, Close> parent;
    
    BufferOpenObserver(ObservableBufferBoundary.BufferBoundaryObserver<T, U, Open, Close> paramBufferBoundaryObserver)
    {
      this.parent = paramBufferBoundaryObserver;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.openFinished(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(Open paramOpen)
    {
      if (this.done) {
        return;
      }
      this.parent.open(paramOpen);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableBufferBoundary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */