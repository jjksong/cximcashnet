package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableAllSingle<T>
  extends Single<Boolean>
  implements FuseToObservable<Boolean>
{
  final Predicate<? super T> predicate;
  final ObservableSource<T> source;
  
  public ObservableAllSingle(ObservableSource<T> paramObservableSource, Predicate<? super T> paramPredicate)
  {
    this.source = paramObservableSource;
    this.predicate = paramPredicate;
  }
  
  public Observable<Boolean> fuseToObservable()
  {
    return RxJavaPlugins.onAssembly(new ObservableAll(this.source, this.predicate));
  }
  
  protected void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    this.source.subscribe(new AllObserver(paramSingleObserver, this.predicate));
  }
  
  static final class AllObserver<T>
    implements Observer<T>, Disposable
  {
    final SingleObserver<? super Boolean> actual;
    boolean done;
    final Predicate<? super T> predicate;
    Disposable s;
    
    AllObserver(SingleObserver<? super Boolean> paramSingleObserver, Predicate<? super T> paramPredicate)
    {
      this.actual = paramSingleObserver;
      this.predicate = paramPredicate;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onSuccess(Boolean.valueOf(true));
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        boolean bool = this.predicate.test(paramT);
        if (!bool)
        {
          this.done = true;
          this.s.dispose();
          this.actual.onSuccess(Boolean.valueOf(false));
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableAllSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */