package io.reactivex.internal.operators.observable;

import io.reactivex.Emitter;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.functions.Functions;
import io.reactivex.observables.ConnectableObservable;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public final class ObservableInternalHelper
{
  private ObservableInternalHelper()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T, U> Function<T, ObservableSource<U>> flatMapIntoIterable(Function<? super T, ? extends Iterable<? extends U>> paramFunction)
  {
    return new FlatMapIntoIterable(paramFunction);
  }
  
  public static <T, U, R> Function<T, ObservableSource<R>> flatMapWithCombiner(Function<? super T, ? extends ObservableSource<? extends U>> paramFunction, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
  {
    return new FlatMapWithCombinerOuter(paramBiFunction, paramFunction);
  }
  
  public static <T, U> Function<T, ObservableSource<T>> itemDelay(Function<? super T, ? extends ObservableSource<U>> paramFunction)
  {
    return new ItemDelayFunction(paramFunction);
  }
  
  public static <T> Action observerOnComplete(Observer<T> paramObserver)
  {
    return new ObserverOnComplete(paramObserver);
  }
  
  public static <T> Consumer<Throwable> observerOnError(Observer<T> paramObserver)
  {
    return new ObserverOnError(paramObserver);
  }
  
  public static <T> Consumer<T> observerOnNext(Observer<T> paramObserver)
  {
    return new ObserverOnNext(paramObserver);
  }
  
  public static Function<Observable<Notification<Object>>, ObservableSource<?>> repeatWhenHandler(Function<? super Observable<Object>, ? extends ObservableSource<?>> paramFunction)
  {
    return new RepeatWhenOuterHandler(paramFunction);
  }
  
  public static <T> Callable<ConnectableObservable<T>> replayCallable(Observable<T> paramObservable)
  {
    new Callable()
    {
      public ConnectableObservable<T> call()
      {
        return this.val$parent.replay();
      }
    };
  }
  
  public static <T> Callable<ConnectableObservable<T>> replayCallable(Observable<T> paramObservable, final int paramInt)
  {
    new Callable()
    {
      public ConnectableObservable<T> call()
      {
        return this.val$parent.replay(paramInt);
      }
    };
  }
  
  public static <T> Callable<ConnectableObservable<T>> replayCallable(Observable<T> paramObservable, final int paramInt, final long paramLong, TimeUnit paramTimeUnit, final Scheduler paramScheduler)
  {
    new Callable()
    {
      public ConnectableObservable<T> call()
      {
        return this.val$parent.replay(paramInt, paramLong, paramScheduler, this.val$scheduler);
      }
    };
  }
  
  public static <T> Callable<ConnectableObservable<T>> replayCallable(Observable<T> paramObservable, final long paramLong, TimeUnit paramTimeUnit, final Scheduler paramScheduler)
  {
    new Callable()
    {
      public ConnectableObservable<T> call()
      {
        return this.val$parent.replay(paramLong, paramScheduler, this.val$scheduler);
      }
    };
  }
  
  public static <T, R> Function<Observable<T>, ObservableSource<R>> replayFunction(Function<? super Observable<T>, ? extends ObservableSource<R>> paramFunction, final Scheduler paramScheduler)
  {
    new Function()
    {
      public ObservableSource<R> apply(Observable<T> paramAnonymousObservable)
        throws Exception
      {
        return Observable.wrap((ObservableSource)this.val$selector.apply(paramAnonymousObservable)).observeOn(paramScheduler);
      }
    };
  }
  
  public static <T> Function<Observable<Notification<Object>>, ObservableSource<?>> retryWhenHandler(Function<? super Observable<Throwable>, ? extends ObservableSource<?>> paramFunction)
  {
    return new RetryWhenInner(paramFunction);
  }
  
  public static <T, S> BiFunction<S, Emitter<T>, S> simpleBiGenerator(BiConsumer<S, Emitter<T>> paramBiConsumer)
  {
    return new SimpleBiGenerator(paramBiConsumer);
  }
  
  public static <T, S> BiFunction<S, Emitter<T>, S> simpleGenerator(Consumer<Emitter<T>> paramConsumer)
  {
    return new SimpleGenerator(paramConsumer);
  }
  
  public static <T, R> Function<List<ObservableSource<? extends T>>, ObservableSource<? extends R>> zipIterable(Function<? super Object[], ? extends R> paramFunction)
  {
    return new ZipIterableFunction(paramFunction);
  }
  
  static enum ErrorMapperFilter
    implements Function<Notification<Object>, Throwable>, Predicate<Notification<Object>>
  {
    INSTANCE;
    
    private ErrorMapperFilter() {}
    
    public Throwable apply(Notification<Object> paramNotification)
      throws Exception
    {
      return paramNotification.getError();
    }
    
    public boolean test(Notification<Object> paramNotification)
      throws Exception
    {
      return paramNotification.isOnError();
    }
  }
  
  static final class FlatMapIntoIterable<T, U>
    implements Function<T, ObservableSource<U>>
  {
    private final Function<? super T, ? extends Iterable<? extends U>> mapper;
    
    FlatMapIntoIterable(Function<? super T, ? extends Iterable<? extends U>> paramFunction)
    {
      this.mapper = paramFunction;
    }
    
    public ObservableSource<U> apply(T paramT)
      throws Exception
    {
      return new ObservableFromIterable((Iterable)this.mapper.apply(paramT));
    }
  }
  
  static final class FlatMapWithCombinerInner<U, R, T>
    implements Function<U, R>
  {
    private final BiFunction<? super T, ? super U, ? extends R> combiner;
    private final T t;
    
    FlatMapWithCombinerInner(BiFunction<? super T, ? super U, ? extends R> paramBiFunction, T paramT)
    {
      this.combiner = paramBiFunction;
      this.t = paramT;
    }
    
    public R apply(U paramU)
      throws Exception
    {
      return (R)this.combiner.apply(this.t, paramU);
    }
  }
  
  static final class FlatMapWithCombinerOuter<T, R, U>
    implements Function<T, ObservableSource<R>>
  {
    private final BiFunction<? super T, ? super U, ? extends R> combiner;
    private final Function<? super T, ? extends ObservableSource<? extends U>> mapper;
    
    FlatMapWithCombinerOuter(BiFunction<? super T, ? super U, ? extends R> paramBiFunction, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction)
    {
      this.combiner = paramBiFunction;
      this.mapper = paramFunction;
    }
    
    public ObservableSource<R> apply(T paramT)
      throws Exception
    {
      return new ObservableMap((ObservableSource)this.mapper.apply(paramT), new ObservableInternalHelper.FlatMapWithCombinerInner(this.combiner, paramT));
    }
  }
  
  static final class ItemDelayFunction<T, U>
    implements Function<T, ObservableSource<T>>
  {
    final Function<? super T, ? extends ObservableSource<U>> itemDelay;
    
    ItemDelayFunction(Function<? super T, ? extends ObservableSource<U>> paramFunction)
    {
      this.itemDelay = paramFunction;
    }
    
    public ObservableSource<T> apply(T paramT)
      throws Exception
    {
      return new ObservableTake((ObservableSource)this.itemDelay.apply(paramT), 1L).map(Functions.justFunction(paramT)).defaultIfEmpty(paramT);
    }
  }
  
  static enum MapToInt
    implements Function<Object, Object>
  {
    INSTANCE;
    
    private MapToInt() {}
    
    public Object apply(Object paramObject)
      throws Exception
    {
      return Integer.valueOf(0);
    }
  }
  
  static final class ObserverOnComplete<T>
    implements Action
  {
    final Observer<T> observer;
    
    ObserverOnComplete(Observer<T> paramObserver)
    {
      this.observer = paramObserver;
    }
    
    public void run()
      throws Exception
    {
      this.observer.onComplete();
    }
  }
  
  static final class ObserverOnError<T>
    implements Consumer<Throwable>
  {
    final Observer<T> observer;
    
    ObserverOnError(Observer<T> paramObserver)
    {
      this.observer = paramObserver;
    }
    
    public void accept(Throwable paramThrowable)
      throws Exception
    {
      this.observer.onError(paramThrowable);
    }
  }
  
  static final class ObserverOnNext<T>
    implements Consumer<T>
  {
    final Observer<T> observer;
    
    ObserverOnNext(Observer<T> paramObserver)
    {
      this.observer = paramObserver;
    }
    
    public void accept(T paramT)
      throws Exception
    {
      this.observer.onNext(paramT);
    }
  }
  
  static final class RepeatWhenOuterHandler
    implements Function<Observable<Notification<Object>>, ObservableSource<?>>
  {
    private final Function<? super Observable<Object>, ? extends ObservableSource<?>> handler;
    
    RepeatWhenOuterHandler(Function<? super Observable<Object>, ? extends ObservableSource<?>> paramFunction)
    {
      this.handler = paramFunction;
    }
    
    public ObservableSource<?> apply(Observable<Notification<Object>> paramObservable)
      throws Exception
    {
      return (ObservableSource)this.handler.apply(paramObservable.map(ObservableInternalHelper.MapToInt.INSTANCE));
    }
  }
  
  static final class RetryWhenInner
    implements Function<Observable<Notification<Object>>, ObservableSource<?>>
  {
    private final Function<? super Observable<Throwable>, ? extends ObservableSource<?>> handler;
    
    RetryWhenInner(Function<? super Observable<Throwable>, ? extends ObservableSource<?>> paramFunction)
    {
      this.handler = paramFunction;
    }
    
    public ObservableSource<?> apply(Observable<Notification<Object>> paramObservable)
      throws Exception
    {
      paramObservable = paramObservable.takeWhile(ObservableInternalHelper.ErrorMapperFilter.INSTANCE).map(ObservableInternalHelper.ErrorMapperFilter.INSTANCE);
      return (ObservableSource)this.handler.apply(paramObservable);
    }
  }
  
  static final class SimpleBiGenerator<T, S>
    implements BiFunction<S, Emitter<T>, S>
  {
    final BiConsumer<S, Emitter<T>> consumer;
    
    SimpleBiGenerator(BiConsumer<S, Emitter<T>> paramBiConsumer)
    {
      this.consumer = paramBiConsumer;
    }
    
    public S apply(S paramS, Emitter<T> paramEmitter)
      throws Exception
    {
      this.consumer.accept(paramS, paramEmitter);
      return paramS;
    }
  }
  
  static final class SimpleGenerator<T, S>
    implements BiFunction<S, Emitter<T>, S>
  {
    final Consumer<Emitter<T>> consumer;
    
    SimpleGenerator(Consumer<Emitter<T>> paramConsumer)
    {
      this.consumer = paramConsumer;
    }
    
    public S apply(S paramS, Emitter<T> paramEmitter)
      throws Exception
    {
      this.consumer.accept(paramEmitter);
      return paramS;
    }
  }
  
  static final class ZipIterableFunction<T, R>
    implements Function<List<ObservableSource<? extends T>>, ObservableSource<? extends R>>
  {
    private final Function<? super Object[], ? extends R> zipper;
    
    ZipIterableFunction(Function<? super Object[], ? extends R> paramFunction)
    {
      this.zipper = paramFunction;
    }
    
    public ObservableSource<? extends R> apply(List<ObservableSource<? extends T>> paramList)
    {
      return Observable.zipIterable(paramList, this.zipper, false, Observable.bufferSize());
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableInternalHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */