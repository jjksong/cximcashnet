package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.LinkedArrayList;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableCache<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final AtomicBoolean once;
  final CacheState<T> state;
  
  private ObservableCache(Observable<T> paramObservable, CacheState<T> paramCacheState)
  {
    super(paramObservable);
    this.state = paramCacheState;
    this.once = new AtomicBoolean();
  }
  
  public static <T> Observable<T> from(Observable<T> paramObservable)
  {
    return from(paramObservable, 16);
  }
  
  public static <T> Observable<T> from(Observable<T> paramObservable, int paramInt)
  {
    ObjectHelper.verifyPositive(paramInt, "capacityHint");
    return RxJavaPlugins.onAssembly(new ObservableCache(paramObservable, new CacheState(paramObservable, paramInt)));
  }
  
  int cachedEventCount()
  {
    return this.state.size();
  }
  
  boolean hasObservers()
  {
    boolean bool;
    if (((ReplayDisposable[])this.state.observers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  boolean isConnected()
  {
    return this.state.isConnected;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    ReplayDisposable localReplayDisposable = new ReplayDisposable(paramObserver, this.state);
    paramObserver.onSubscribe(localReplayDisposable);
    this.state.addChild(localReplayDisposable);
    if ((!this.once.get()) && (this.once.compareAndSet(false, true))) {
      this.state.connect();
    }
    localReplayDisposable.replay();
  }
  
  static final class CacheState<T>
    extends LinkedArrayList
    implements Observer<T>
  {
    static final ObservableCache.ReplayDisposable[] EMPTY = new ObservableCache.ReplayDisposable[0];
    static final ObservableCache.ReplayDisposable[] TERMINATED = new ObservableCache.ReplayDisposable[0];
    final SequentialDisposable connection;
    volatile boolean isConnected;
    final AtomicReference<ObservableCache.ReplayDisposable<T>[]> observers;
    final Observable<? extends T> source;
    boolean sourceDone;
    
    CacheState(Observable<? extends T> paramObservable, int paramInt)
    {
      super();
      this.source = paramObservable;
      this.observers = new AtomicReference(EMPTY);
      this.connection = new SequentialDisposable();
    }
    
    public boolean addChild(ObservableCache.ReplayDisposable<T> paramReplayDisposable)
    {
      ObservableCache.ReplayDisposable[] arrayOfReplayDisposable2;
      ObservableCache.ReplayDisposable[] arrayOfReplayDisposable1;
      do
      {
        arrayOfReplayDisposable2 = (ObservableCache.ReplayDisposable[])this.observers.get();
        if (arrayOfReplayDisposable2 == TERMINATED) {
          return false;
        }
        int i = arrayOfReplayDisposable2.length;
        arrayOfReplayDisposable1 = new ObservableCache.ReplayDisposable[i + 1];
        System.arraycopy(arrayOfReplayDisposable2, 0, arrayOfReplayDisposable1, 0, i);
        arrayOfReplayDisposable1[i] = paramReplayDisposable;
      } while (!this.observers.compareAndSet(arrayOfReplayDisposable2, arrayOfReplayDisposable1));
      return true;
    }
    
    public void connect()
    {
      this.source.subscribe(this);
      this.isConnected = true;
    }
    
    public void onComplete()
    {
      if (!this.sourceDone)
      {
        this.sourceDone = true;
        add(NotificationLite.complete());
        this.connection.dispose();
        ObservableCache.ReplayDisposable[] arrayOfReplayDisposable = (ObservableCache.ReplayDisposable[])this.observers.getAndSet(TERMINATED);
        int j = arrayOfReplayDisposable.length;
        for (int i = 0; i < j; i++) {
          arrayOfReplayDisposable[i].replay();
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.sourceDone)
      {
        this.sourceDone = true;
        add(NotificationLite.error(paramThrowable));
        this.connection.dispose();
        paramThrowable = (ObservableCache.ReplayDisposable[])this.observers.getAndSet(TERMINATED);
        int j = paramThrowable.length;
        for (int i = 0; i < j; i++) {
          paramThrowable[i].replay();
        }
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.sourceDone)
      {
        add(NotificationLite.next(paramT));
        paramT = (ObservableCache.ReplayDisposable[])this.observers.get();
        int j = paramT.length;
        for (int i = 0; i < j; i++) {
          paramT[i].replay();
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.connection.update(paramDisposable);
    }
    
    public void removeChild(ObservableCache.ReplayDisposable<T> paramReplayDisposable)
    {
      ObservableCache.ReplayDisposable[] arrayOfReplayDisposable2;
      ObservableCache.ReplayDisposable[] arrayOfReplayDisposable1;
      do
      {
        arrayOfReplayDisposable2 = (ObservableCache.ReplayDisposable[])this.observers.get();
        int m = arrayOfReplayDisposable2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfReplayDisposable2[i].equals(paramReplayDisposable))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfReplayDisposable1 = EMPTY;
        }
        else
        {
          arrayOfReplayDisposable1 = new ObservableCache.ReplayDisposable[m - 1];
          System.arraycopy(arrayOfReplayDisposable2, 0, arrayOfReplayDisposable1, 0, j);
          System.arraycopy(arrayOfReplayDisposable2, j + 1, arrayOfReplayDisposable1, j, m - j - 1);
        }
      } while (!this.observers.compareAndSet(arrayOfReplayDisposable2, arrayOfReplayDisposable1));
    }
  }
  
  static final class ReplayDisposable<T>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = 7058506693698832024L;
    volatile boolean cancelled;
    final Observer<? super T> child;
    Object[] currentBuffer;
    int currentIndexInBuffer;
    int index;
    final ObservableCache.CacheState<T> state;
    
    ReplayDisposable(Observer<? super T> paramObserver, ObservableCache.CacheState<T> paramCacheState)
    {
      this.child = paramObserver;
      this.state = paramCacheState;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.state.removeChild(this);
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void replay()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = this.child;
      int j = 1;
      int i;
      do
      {
        if (this.cancelled) {
          return;
        }
        int i1 = this.state.size();
        if (i1 != 0)
        {
          Object localObject2 = this.currentBuffer;
          Object localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = this.state.head();
            this.currentBuffer = ((Object[])localObject1);
          }
          int n = localObject1.length - 1;
          int k = this.index;
          i = this.currentIndexInBuffer;
          while (k < i1)
          {
            if (this.cancelled) {
              return;
            }
            localObject2 = localObject1;
            int m = i;
            if (i == n)
            {
              localObject2 = (Object[])localObject1[n];
              m = 0;
            }
            if (NotificationLite.accept(localObject2[m], localObserver)) {
              return;
            }
            i = m + 1;
            k++;
            localObject1 = localObject2;
          }
          if (this.cancelled) {
            return;
          }
          this.index = k;
          this.currentIndexInBuffer = i;
          this.currentBuffer = ((Object[])localObject1);
        }
        i = addAndGet(-j);
        j = i;
      } while (i != 0);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */