package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.DeferredScalarDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableFromCallable<T>
  extends Observable<T>
  implements Callable<T>
{
  final Callable<? extends T> callable;
  
  public ObservableFromCallable(Callable<? extends T> paramCallable)
  {
    this.callable = paramCallable;
  }
  
  public T call()
    throws Exception
  {
    return (T)this.callable.call();
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    DeferredScalarDisposable localDeferredScalarDisposable = new DeferredScalarDisposable(paramObserver);
    paramObserver.onSubscribe(localDeferredScalarDisposable);
    if (localDeferredScalarDisposable.isDisposed()) {
      return;
    }
    try
    {
      Object localObject = ObjectHelper.requireNonNull(this.callable.call(), "Callable returned null");
      localDeferredScalarDisposable.complete(localObject);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      if (!localDeferredScalarDisposable.isDisposed()) {
        paramObserver.onError(localThrowable);
      } else {
        RxJavaPlugins.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableFromCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */