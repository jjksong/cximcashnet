package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowBoundary<T, B>
  extends AbstractObservableWithUpstream<T, Observable<T>>
{
  final int bufferSize;
  final ObservableSource<B> other;
  
  public ObservableWindowBoundary(ObservableSource<T> paramObservableSource, ObservableSource<B> paramObservableSource1, int paramInt)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
    this.bufferSize = paramInt;
  }
  
  public void subscribeActual(Observer<? super Observable<T>> paramObserver)
  {
    this.source.subscribe(new WindowBoundaryMainObserver(new SerializedObserver(paramObserver), this.other, this.bufferSize));
  }
  
  static final class WindowBoundaryInnerObserver<T, B>
    extends DisposableObserver<B>
  {
    boolean done;
    final ObservableWindowBoundary.WindowBoundaryMainObserver<T, B> parent;
    
    WindowBoundaryInnerObserver(ObservableWindowBoundary.WindowBoundaryMainObserver<T, B> paramWindowBoundaryMainObserver)
    {
      this.parent = paramWindowBoundaryMainObserver;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      if (this.done) {
        return;
      }
      this.parent.next();
    }
  }
  
  static final class WindowBoundaryMainObserver<T, B>
    extends QueueDrainObserver<T, Object, Observable<T>>
    implements Disposable
  {
    static final Object NEXT = new Object();
    final AtomicReference<Disposable> boundary = new AtomicReference();
    final int bufferSize;
    final ObservableSource<B> other;
    Disposable s;
    UnicastSubject<T> window;
    final AtomicLong windows = new AtomicLong();
    
    WindowBoundaryMainObserver(Observer<? super Observable<T>> paramObserver, ObservableSource<B> paramObservableSource, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.other = paramObservableSource;
      this.bufferSize = paramInt;
      this.windows.lazySet(1L);
    }
    
    public void dispose()
    {
      this.cancelled = true;
    }
    
    void drainLoop()
    {
      Object localObject1 = (MpscLinkedQueue)this.queue;
      Observer localObserver = this.actual;
      UnicastSubject localUnicastSubject = this.window;
      int i = 1;
      for (;;)
      {
        boolean bool = this.done;
        Object localObject2 = ((MpscLinkedQueue)localObject1).poll();
        int j;
        if (localObject2 == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          DisposableHelper.dispose(this.boundary);
          localObject1 = this.error;
          if (localObject1 != null) {
            localUnicastSubject.onError((Throwable)localObject1);
          } else {
            localUnicastSubject.onComplete();
          }
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if (localObject2 == NEXT)
        {
          localUnicastSubject.onComplete();
          if (this.windows.decrementAndGet() == 0L)
          {
            DisposableHelper.dispose(this.boundary);
            return;
          }
          if (!this.cancelled)
          {
            localUnicastSubject = UnicastSubject.create(this.bufferSize);
            this.windows.getAndIncrement();
            this.window = localUnicastSubject;
            localObserver.onNext(localUnicastSubject);
          }
        }
        else
        {
          localUnicastSubject.onNext(NotificationLite.getValue(localObject2));
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    void next()
    {
      this.queue.offer(NEXT);
      if (enter()) {
        drainLoop();
      }
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        DisposableHelper.dispose(this.boundary);
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        DisposableHelper.dispose(this.boundary);
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (fastEnter())
      {
        this.window.onNext(paramT);
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        paramDisposable = this.actual;
        paramDisposable.onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        UnicastSubject localUnicastSubject = UnicastSubject.create(this.bufferSize);
        this.window = localUnicastSubject;
        paramDisposable.onNext(localUnicastSubject);
        paramDisposable = new ObservableWindowBoundary.WindowBoundaryInnerObserver(this);
        if (this.boundary.compareAndSet(null, paramDisposable))
        {
          this.windows.getAndIncrement();
          this.other.subscribe(paramDisposable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableWindowBoundary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */