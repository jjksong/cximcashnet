package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.subjects.PublishSubject;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservablePublishSelector<T, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final Function<? super Observable<T>, ? extends ObservableSource<R>> selector;
  
  public ObservablePublishSelector(ObservableSource<T> paramObservableSource, Function<? super Observable<T>, ? extends ObservableSource<R>> paramFunction)
  {
    super(paramObservableSource);
    this.selector = paramFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    PublishSubject localPublishSubject = PublishSubject.create();
    try
    {
      ObservableSource localObservableSource = (ObservableSource)ObjectHelper.requireNonNull(this.selector.apply(localPublishSubject), "The selector returned a null ObservableSource");
      paramObserver = new TargetObserver(paramObserver);
      localObservableSource.subscribe(paramObserver);
      this.source.subscribe(new SourceObserver(localPublishSubject, paramObserver));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
  
  static final class SourceObserver<T, R>
    implements Observer<T>
  {
    final PublishSubject<T> subject;
    final AtomicReference<Disposable> target;
    
    SourceObserver(PublishSubject<T> paramPublishSubject, AtomicReference<Disposable> paramAtomicReference)
    {
      this.subject = paramPublishSubject;
      this.target = paramAtomicReference;
    }
    
    public void onComplete()
    {
      this.subject.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.subject.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.subject.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.target, paramDisposable);
    }
  }
  
  static final class TargetObserver<T, R>
    extends AtomicReference<Disposable>
    implements Observer<R>, Disposable
  {
    private static final long serialVersionUID = 854110278590336484L;
    final Observer<? super R> actual;
    Disposable d;
    
    TargetObserver(Observer<? super R> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this);
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(R paramR)
    {
      this.actual.onNext(paramR);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservablePublishSelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */