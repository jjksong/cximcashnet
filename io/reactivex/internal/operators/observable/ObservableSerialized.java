package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.observers.SerializedObserver;

public final class ObservableSerialized<T>
  extends AbstractObservableWithUpstream<T, T>
{
  public ObservableSerialized(Observable<T> paramObservable)
  {
    super(paramObservable);
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new SerializedObserver(paramObserver));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSerialized.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */