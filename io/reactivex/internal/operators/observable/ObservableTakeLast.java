package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.ArrayDeque;

public final class ObservableTakeLast<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final int count;
  
  public ObservableTakeLast(ObservableSource<T> paramObservableSource, int paramInt)
  {
    super(paramObservableSource);
    this.count = paramInt;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new TakeLastObserver(paramObserver, this.count));
  }
  
  static final class TakeLastObserver<T>
    extends ArrayDeque<T>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = 7240042530241604978L;
    final Observer<? super T> actual;
    volatile boolean cancelled;
    final int count;
    Disposable s;
    
    TakeLastObserver(Observer<? super T> paramObserver, int paramInt)
    {
      this.actual = paramObserver;
      this.count = paramInt;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      Observer localObserver = this.actual;
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        Object localObject = poll();
        if (localObject == null)
        {
          if (!this.cancelled) {
            localObserver.onComplete();
          }
          return;
        }
        localObserver.onNext(localObject);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.count == size()) {
        poll();
      }
      offer(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTakeLast.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */