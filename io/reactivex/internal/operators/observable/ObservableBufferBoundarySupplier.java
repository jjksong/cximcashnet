package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableBufferBoundarySupplier<T, U extends Collection<? super T>, B>
  extends AbstractObservableWithUpstream<T, U>
{
  final Callable<? extends ObservableSource<B>> boundarySupplier;
  final Callable<U> bufferSupplier;
  
  public ObservableBufferBoundarySupplier(ObservableSource<T> paramObservableSource, Callable<? extends ObservableSource<B>> paramCallable, Callable<U> paramCallable1)
  {
    super(paramObservableSource);
    this.boundarySupplier = paramCallable;
    this.bufferSupplier = paramCallable1;
  }
  
  protected void subscribeActual(Observer<? super U> paramObserver)
  {
    this.source.subscribe(new BufferBoundarySupplierObserver(new SerializedObserver(paramObserver), this.bufferSupplier, this.boundarySupplier));
  }
  
  static final class BufferBoundaryObserver<T, U extends Collection<? super T>, B>
    extends DisposableObserver<B>
  {
    boolean once;
    final ObservableBufferBoundarySupplier.BufferBoundarySupplierObserver<T, U, B> parent;
    
    BufferBoundaryObserver(ObservableBufferBoundarySupplier.BufferBoundarySupplierObserver<T, U, B> paramBufferBoundarySupplierObserver)
    {
      this.parent = paramBufferBoundarySupplierObserver;
    }
    
    public void onComplete()
    {
      if (this.once) {
        return;
      }
      this.once = true;
      this.parent.next();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.once)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.once = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      if (this.once) {
        return;
      }
      this.once = true;
      dispose();
      this.parent.next();
    }
  }
  
  static final class BufferBoundarySupplierObserver<T, U extends Collection<? super T>, B>
    extends QueueDrainObserver<T, U, U>
    implements Observer<T>, Disposable
  {
    final Callable<? extends ObservableSource<B>> boundarySupplier;
    U buffer;
    final Callable<U> bufferSupplier;
    final AtomicReference<Disposable> other = new AtomicReference();
    Disposable s;
    
    BufferBoundarySupplierObserver(Observer<? super U> paramObserver, Callable<U> paramCallable, Callable<? extends ObservableSource<B>> paramCallable1)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.boundarySupplier = paramCallable1;
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      this.actual.onNext(paramU);
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.dispose();
        disposeOther();
        if (enter()) {
          this.queue.clear();
        }
      }
    }
    
    void disposeOther()
    {
      DisposableHelper.dispose(this.other);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    /* Error */
    void next()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 42	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:bufferSupplier	Ljava/util/concurrent/Callable;
      //   4: invokeinterface 103 1 0
      //   9: ldc 105
      //   11: invokestatic 111	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   14: checkcast 51	java/util/Collection
      //   17: astore_2
      //   18: aload_0
      //   19: getfield 44	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:boundarySupplier	Ljava/util/concurrent/Callable;
      //   22: invokeinterface 103 1 0
      //   27: ldc 113
      //   29: invokestatic 111	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   32: checkcast 115	io/reactivex/ObservableSource
      //   35: astore_1
      //   36: new 117	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundaryObserver
      //   39: dup
      //   40: aload_0
      //   41: invokespecial 120	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundaryObserver:<init>	(Lio/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver;)V
      //   44: astore_3
      //   45: aload_0
      //   46: getfield 40	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:other	Ljava/util/concurrent/atomic/AtomicReference;
      //   49: invokevirtual 123	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
      //   52: checkcast 9	io/reactivex/disposables/Disposable
      //   55: astore 4
      //   57: aload_0
      //   58: getfield 40	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:other	Ljava/util/concurrent/atomic/AtomicReference;
      //   61: aload 4
      //   63: aload_3
      //   64: invokevirtual 127	java/util/concurrent/atomic/AtomicReference:compareAndSet	(Ljava/lang/Object;Ljava/lang/Object;)Z
      //   67: ifne +4 -> 71
      //   70: return
      //   71: aload_0
      //   72: monitorenter
      //   73: aload_0
      //   74: getfield 129	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:buffer	Ljava/util/Collection;
      //   77: astore 4
      //   79: aload 4
      //   81: ifnonnull +6 -> 87
      //   84: aload_0
      //   85: monitorexit
      //   86: return
      //   87: aload_0
      //   88: aload_2
      //   89: putfield 129	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:buffer	Ljava/util/Collection;
      //   92: aload_0
      //   93: monitorexit
      //   94: aload_1
      //   95: aload_3
      //   96: invokeinterface 133 2 0
      //   101: aload_0
      //   102: aload 4
      //   104: iconst_0
      //   105: aload_0
      //   106: invokevirtual 137	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:fastPathEmit	(Ljava/lang/Object;ZLio/reactivex/disposables/Disposable;)V
      //   109: return
      //   110: astore_1
      //   111: aload_0
      //   112: monitorexit
      //   113: aload_1
      //   114: athrow
      //   115: astore_1
      //   116: aload_1
      //   117: invokestatic 143	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   120: aload_0
      //   121: iconst_1
      //   122: putfield 68	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:cancelled	Z
      //   125: aload_0
      //   126: getfield 70	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:s	Lio/reactivex/disposables/Disposable;
      //   129: invokeinterface 72 1 0
      //   134: aload_0
      //   135: getfield 58	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:actual	Lio/reactivex/Observer;
      //   138: aload_1
      //   139: invokeinterface 146 2 0
      //   144: return
      //   145: astore_1
      //   146: aload_1
      //   147: invokestatic 143	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   150: aload_0
      //   151: invokevirtual 147	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:dispose	()V
      //   154: aload_0
      //   155: getfield 58	io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier$BufferBoundarySupplierObserver:actual	Lio/reactivex/Observer;
      //   158: aload_1
      //   159: invokeinterface 146 2 0
      //   164: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	165	0	this	BufferBoundarySupplierObserver
      //   35	60	1	localObservableSource	ObservableSource
      //   110	4	1	localObject1	Object
      //   115	24	1	localThrowable1	Throwable
      //   145	14	1	localThrowable2	Throwable
      //   17	72	2	localCollection	Collection
      //   44	52	3	localBufferBoundaryObserver	ObservableBufferBoundarySupplier.BufferBoundaryObserver
      //   55	48	4	localObject2	Object
      // Exception table:
      //   from	to	target	type
      //   73	79	110	finally
      //   84	86	110	finally
      //   87	94	110	finally
      //   111	113	110	finally
      //   18	36	115	java/lang/Throwable
      //   0	18	145	java/lang/Throwable
    }
    
    public void onComplete()
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        localCollection.add(paramT);
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        Observer localObserver = this.actual;
        try
        {
          Object localObject = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
          this.buffer = ((Collection)localObject);
          try
          {
            localObject = (ObservableSource)ObjectHelper.requireNonNull(this.boundarySupplier.call(), "The boundary publisher supplied is null");
            paramDisposable = new ObservableBufferBoundarySupplier.BufferBoundaryObserver(this);
            this.other.set(paramDisposable);
            localObserver.onSubscribe(this);
            if (!this.cancelled) {
              ((ObservableSource)localObject).subscribe(paramDisposable);
            }
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            this.cancelled = true;
            paramDisposable.dispose();
            EmptyDisposable.error(localThrowable1, localObserver);
            return;
          }
          return;
        }
        catch (Throwable localThrowable2)
        {
          Exceptions.throwIfFatal(localThrowable2);
          this.cancelled = true;
          paramDisposable.dispose();
          EmptyDisposable.error(localThrowable2, localObserver);
          return;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableBufferBoundarySupplier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */