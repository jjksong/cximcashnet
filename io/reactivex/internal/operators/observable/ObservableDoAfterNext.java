package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.annotations.Experimental;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.observers.BasicFuseableObserver;

@Experimental
public final class ObservableDoAfterNext<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Consumer<? super T> onAfterNext;
  
  public ObservableDoAfterNext(ObservableSource<T> paramObservableSource, Consumer<? super T> paramConsumer)
  {
    super(paramObservableSource);
    this.onAfterNext = paramConsumer;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new DoAfterObserver(paramObserver, this.onAfterNext));
  }
  
  static final class DoAfterObserver<T>
    extends BasicFuseableObserver<T, T>
  {
    final Consumer<? super T> onAfterNext;
    
    DoAfterObserver(Observer<? super T> paramObserver, Consumer<? super T> paramConsumer)
    {
      super();
      this.onAfterNext = paramConsumer;
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      if (this.sourceMode == 0) {
        try
        {
          this.onAfterNext.accept(paramT);
        }
        catch (Throwable paramT)
        {
          fail(paramT);
        }
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {
        this.onAfterNext.accept(localObject);
      }
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDoAfterNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */