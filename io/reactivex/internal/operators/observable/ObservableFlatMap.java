package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableFlatMap<T, U>
  extends AbstractObservableWithUpstream<T, U>
{
  final int bufferSize;
  final boolean delayErrors;
  final Function<? super T, ? extends ObservableSource<? extends U>> mapper;
  final int maxConcurrency;
  
  public ObservableFlatMap(ObservableSource<T> paramObservableSource, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    super(paramObservableSource);
    this.mapper = paramFunction;
    this.delayErrors = paramBoolean;
    this.maxConcurrency = paramInt1;
    this.bufferSize = paramInt2;
  }
  
  public void subscribeActual(Observer<? super U> paramObserver)
  {
    if (ObservableScalarXMap.tryScalarXMapSubscribe(this.source, paramObserver, this.mapper)) {
      return;
    }
    this.source.subscribe(new MergeObserver(paramObserver, this.mapper, this.delayErrors, this.maxConcurrency, this.bufferSize));
  }
  
  static final class InnerObserver<T, U>
    extends AtomicReference<Disposable>
    implements Observer<U>
  {
    private static final long serialVersionUID = -4606175640614850599L;
    volatile boolean done;
    int fusionMode;
    final long id;
    final ObservableFlatMap.MergeObserver<T, U> parent;
    volatile SimpleQueue<U> queue;
    
    InnerObserver(ObservableFlatMap.MergeObserver<T, U> paramMergeObserver, long paramLong)
    {
      this.id = paramLong;
      this.parent = paramMergeObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.parent.errors.addThrowable(paramThrowable))
      {
        if (!this.parent.delayErrors) {
          this.parent.disposeAll();
        }
        this.done = true;
        this.parent.drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(U paramU)
    {
      if (this.fusionMode == 0) {
        this.parent.tryEmit(paramU, this);
      } else {
        this.parent.drain();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if ((DisposableHelper.setOnce(this, paramDisposable)) && ((paramDisposable instanceof QueueDisposable)))
      {
        paramDisposable = (QueueDisposable)paramDisposable;
        int i = paramDisposable.requestFusion(7);
        if (i == 1)
        {
          this.fusionMode = i;
          this.queue = paramDisposable;
          this.done = true;
          this.parent.drain();
          return;
        }
        if (i == 2)
        {
          this.fusionMode = i;
          this.queue = paramDisposable;
        }
      }
    }
  }
  
  static final class MergeObserver<T, U>
    extends AtomicInteger
    implements Disposable, Observer<T>
  {
    static final ObservableFlatMap.InnerObserver<?, ?>[] CANCELLED = new ObservableFlatMap.InnerObserver[0];
    static final ObservableFlatMap.InnerObserver<?, ?>[] EMPTY = new ObservableFlatMap.InnerObserver[0];
    private static final long serialVersionUID = -2117620485640801370L;
    final Observer<? super U> actual;
    final int bufferSize;
    volatile boolean cancelled;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicThrowable errors = new AtomicThrowable();
    long lastId;
    int lastIndex;
    final Function<? super T, ? extends ObservableSource<? extends U>> mapper;
    final int maxConcurrency;
    final AtomicReference<ObservableFlatMap.InnerObserver<?, ?>[]> observers;
    volatile SimplePlainQueue<U> queue;
    Disposable s;
    Queue<ObservableSource<? extends U>> sources;
    long uniqueId;
    int wip;
    
    MergeObserver(Observer<? super U> paramObserver, Function<? super T, ? extends ObservableSource<? extends U>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
      this.delayErrors = paramBoolean;
      this.maxConcurrency = paramInt1;
      this.bufferSize = paramInt2;
      if (paramInt1 != Integer.MAX_VALUE) {
        this.sources = new ArrayDeque(paramInt1);
      }
      this.observers = new AtomicReference(EMPTY);
    }
    
    boolean addInner(ObservableFlatMap.InnerObserver<T, U> paramInnerObserver)
    {
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver2;
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver1;
      do
      {
        arrayOfInnerObserver2 = (ObservableFlatMap.InnerObserver[])this.observers.get();
        if (arrayOfInnerObserver2 == CANCELLED)
        {
          paramInnerObserver.dispose();
          return false;
        }
        int i = arrayOfInnerObserver2.length;
        arrayOfInnerObserver1 = new ObservableFlatMap.InnerObserver[i + 1];
        System.arraycopy(arrayOfInnerObserver2, 0, arrayOfInnerObserver1, 0, i);
        arrayOfInnerObserver1[i] = paramInnerObserver;
      } while (!this.observers.compareAndSet(arrayOfInnerObserver2, arrayOfInnerObserver1));
      return true;
    }
    
    boolean checkTerminate()
    {
      if (this.cancelled) {
        return true;
      }
      Throwable localThrowable = (Throwable)this.errors.get();
      if ((!this.delayErrors) && (localThrowable != null))
      {
        disposeAll();
        this.actual.onError(this.errors.terminate());
        return true;
      }
      return false;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        if (disposeAll())
        {
          Throwable localThrowable = this.errors.terminate();
          if ((localThrowable != null) && (localThrowable != ExceptionHelper.TERMINATED)) {
            RxJavaPlugins.onError(localThrowable);
          }
        }
      }
    }
    
    boolean disposeAll()
    {
      this.s.dispose();
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver2 = (ObservableFlatMap.InnerObserver[])this.observers.get();
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver1 = CANCELLED;
      int i = 0;
      if (arrayOfInnerObserver2 != arrayOfInnerObserver1)
      {
        arrayOfInnerObserver1 = (ObservableFlatMap.InnerObserver[])this.observers.getAndSet(arrayOfInnerObserver1);
        if (arrayOfInnerObserver1 != CANCELLED)
        {
          int j = arrayOfInnerObserver1.length;
          while (i < j)
          {
            arrayOfInnerObserver1[i].dispose();
            i++;
          }
          return true;
        }
      }
      return false;
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        drainLoop();
      }
    }
    
    void drainLoop()
    {
      Observer localObserver = this.actual;
      int n = 1;
      int i;
      label488:
      do
      {
        for (;;)
        {
          if (checkTerminate()) {
            return;
          }
          Object localObject2 = this.queue;
          if (localObject2 != null) {
            for (;;)
            {
              if (checkTerminate()) {
                return;
              }
              localObject3 = ((SimplePlainQueue)localObject2).poll();
              if (localObject3 == null)
              {
                if (localObject3 == null) {
                  break;
                }
              }
              else {
                localObserver.onNext(localObject3);
              }
            }
          }
          boolean bool = this.done;
          Object localObject3 = this.queue;
          localObject2 = (ObservableFlatMap.InnerObserver[])this.observers.get();
          int i3 = localObject2.length;
          if ((bool) && ((localObject3 == null) || (((SimplePlainQueue)localObject3).isEmpty())) && (i3 == 0))
          {
            if ((Throwable)this.errors.get() == null) {
              localObserver.onComplete();
            } else {
              localObserver.onError(this.errors.terminate());
            }
            return;
          }
          i = 0;
          if (i3 != 0)
          {
            long l = this.lastId;
            int j = this.lastIndex;
            int k;
            if (i3 > j)
            {
              i = j;
              if (localObject2[j].id == l) {}
            }
            else
            {
              i = j;
              if (i3 <= j) {
                i = 0;
              }
              for (j = 0; (j < i3) && (localObject2[i].id != l); j++)
              {
                k = i + 1;
                i = k;
                if (k == i3) {
                  i = 0;
                }
              }
              this.lastIndex = i;
              this.lastId = localObject2[i].id;
            }
            int m = i;
            j = 0;
            i = 0;
            while (j < i3)
            {
              if (checkTerminate()) {
                return;
              }
              localObject3 = localObject2[m];
              for (;;)
              {
                if (checkTerminate()) {
                  return;
                }
                SimpleQueue localSimpleQueue = ((ObservableFlatMap.InnerObserver)localObject3).queue;
                if (localSimpleQueue != null) {}
                try
                {
                  do
                  {
                    Object localObject4 = localSimpleQueue.poll();
                    if (localObject4 == null)
                    {
                      if (localObject4 != null) {
                        break;
                      }
                      bool = ((ObservableFlatMap.InnerObserver)localObject3).done;
                      localSimpleQueue = ((ObservableFlatMap.InnerObserver)localObject3).queue;
                      k = i;
                      if (bool) {
                        if (localSimpleQueue != null)
                        {
                          k = i;
                          if (!localSimpleQueue.isEmpty()) {}
                        }
                        else
                        {
                          removeInner((ObservableFlatMap.InnerObserver)localObject3);
                          if (checkTerminate()) {
                            return;
                          }
                          k = 1;
                        }
                      }
                      int i2 = m + 1;
                      i1 = j;
                      m = i2;
                      i = k;
                      if (i2 != i3) {
                        break label488;
                      }
                      m = 0;
                      i1 = j;
                      i = k;
                      break label488;
                    }
                    localObserver.onNext(localObject4);
                  } while (!checkTerminate());
                  return;
                }
                catch (Throwable localThrowable)
                {
                  Exceptions.throwIfFatal(localThrowable);
                  ((ObservableFlatMap.InnerObserver)localObject3).dispose();
                  this.errors.addThrowable(localThrowable);
                  if (checkTerminate()) {
                    return;
                  }
                  removeInner((ObservableFlatMap.InnerObserver)localObject3);
                  int i1 = j + 1;
                  i = 1;
                  j = i1 + 1;
                }
              }
            }
            this.lastIndex = m;
            this.lastId = localObject2[m].id;
          }
          if (i != 0)
          {
            if (this.maxConcurrency == Integer.MAX_VALUE) {
              continue;
            }
            try
            {
              localObject2 = (ObservableSource)this.sources.poll();
              if (localObject2 == null) {
                this.wip -= 1;
              } else {
                subscribeInner((ObservableSource)localObject2);
              }
            }
            finally {}
          }
        }
        i = addAndGet(-n);
        n = i;
      } while (i != 0);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      if (this.errors.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        paramT = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null ObservableSource");
        if (this.maxConcurrency != Integer.MAX_VALUE) {
          try
          {
            if (this.wip == this.maxConcurrency)
            {
              this.sources.offer(paramT);
              return;
            }
            this.wip += 1;
          }
          finally {}
        }
        subscribeInner(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    void removeInner(ObservableFlatMap.InnerObserver<T, U> paramInnerObserver)
    {
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver2;
      ObservableFlatMap.InnerObserver[] arrayOfInnerObserver1;
      do
      {
        arrayOfInnerObserver2 = (ObservableFlatMap.InnerObserver[])this.observers.get();
        int m = arrayOfInnerObserver2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerObserver2[i] == paramInnerObserver)
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerObserver1 = EMPTY;
        }
        else
        {
          arrayOfInnerObserver1 = new ObservableFlatMap.InnerObserver[m - 1];
          System.arraycopy(arrayOfInnerObserver2, 0, arrayOfInnerObserver1, 0, j);
          System.arraycopy(arrayOfInnerObserver2, j + 1, arrayOfInnerObserver1, j, m - j - 1);
        }
      } while (!this.observers.compareAndSet(arrayOfInnerObserver2, arrayOfInnerObserver1));
    }
    
    void subscribeInner(ObservableSource<? extends U> paramObservableSource)
    {
      while ((paramObservableSource instanceof Callable))
      {
        tryEmitScalar((Callable)paramObservableSource);
        if (this.maxConcurrency == Integer.MAX_VALUE) {
          return;
        }
        try
        {
          paramObservableSource = (ObservableSource)this.sources.poll();
          if (paramObservableSource == null)
          {
            this.wip -= 1;
            return;
          }
        }
        finally {}
      }
      long l = this.uniqueId;
      this.uniqueId = (1L + l);
      ObservableFlatMap.InnerObserver localInnerObserver = new ObservableFlatMap.InnerObserver(this, l);
      if (addInner(localInnerObserver)) {
        paramObservableSource.subscribe(localInnerObserver);
      }
    }
    
    void tryEmit(U paramU, ObservableFlatMap.InnerObserver<T, U> paramInnerObserver)
    {
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        this.actual.onNext(paramU);
        if (decrementAndGet() != 0) {}
      }
      else
      {
        SimpleQueue localSimpleQueue = paramInnerObserver.queue;
        Object localObject = localSimpleQueue;
        if (localSimpleQueue == null)
        {
          localObject = new SpscLinkedArrayQueue(this.bufferSize);
          paramInnerObserver.queue = ((SimpleQueue)localObject);
        }
        ((SimpleQueue)localObject).offer(paramU);
        if (getAndIncrement() != 0) {
          return;
        }
      }
      drainLoop();
    }
    
    void tryEmitScalar(Callable<? extends U> paramCallable)
    {
      try
      {
        Object localObject = paramCallable.call();
        if (localObject == null) {
          return;
        }
        if ((get() == 0) && (compareAndSet(0, 1)))
        {
          this.actual.onNext(localObject);
          if (decrementAndGet() != 0) {}
        }
        else
        {
          SimplePlainQueue localSimplePlainQueue = this.queue;
          paramCallable = localSimplePlainQueue;
          if (localSimplePlainQueue == null)
          {
            int i = this.maxConcurrency;
            if (i == Integer.MAX_VALUE) {
              paramCallable = new SpscLinkedArrayQueue(this.bufferSize);
            } else {
              paramCallable = new SpscArrayQueue(i);
            }
            this.queue = paramCallable;
          }
          if (!paramCallable.offer(localObject))
          {
            onError(new IllegalStateException("Scalar queue full?!"));
            return;
          }
          if (getAndIncrement() != 0) {
            return;
          }
        }
        drainLoop();
        return;
      }
      catch (Throwable paramCallable)
      {
        Exceptions.throwIfFatal(paramCallable);
        this.errors.addThrowable(paramCallable);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableFlatMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */