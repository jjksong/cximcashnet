package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.ObserverFullArbiter;
import io.reactivex.internal.observers.FullArbiterObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableTimeoutTimed<T>
  extends AbstractObservableWithUpstream<T, T>
{
  static final Disposable NEW_TIMER = new Disposable()
  {
    public void dispose() {}
    
    public boolean isDisposed()
    {
      return true;
    }
  };
  final ObservableSource<? extends T> other;
  final Scheduler scheduler;
  final long timeout;
  final TimeUnit unit;
  
  public ObservableTimeoutTimed(ObservableSource<T> paramObservableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, ObservableSource<? extends T> paramObservableSource1)
  {
    super(paramObservableSource);
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.other = paramObservableSource1;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    if (this.other == null) {
      this.source.subscribe(new TimeoutTimedObserver(new SerializedObserver(paramObserver), this.timeout, this.unit, this.scheduler.createWorker()));
    } else {
      this.source.subscribe(new TimeoutTimedOtherObserver(paramObserver, this.timeout, this.unit, this.scheduler.createWorker(), this.other));
    }
  }
  
  static final class TimeoutTimedObserver<T>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -8387234228317808253L;
    final Observer<? super T> actual;
    volatile boolean done;
    volatile long index;
    Disposable s;
    final long timeout;
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    TimeoutTimedObserver(Observer<? super T> paramObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      this.actual = paramObserver;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
    }
    
    public void dispose()
    {
      this.worker.dispose();
      DisposableHelper.dispose(this);
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      this.actual.onNext(paramT);
      scheduleTimeout(l);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
        scheduleTimeout(0L);
      }
    }
    
    void scheduleTimeout(final long paramLong)
    {
      Disposable localDisposable = (Disposable)get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      if (compareAndSet(localDisposable, ObservableTimeoutTimed.NEW_TIMER)) {
        DisposableHelper.replace(this, this.worker.schedule(new Runnable()
        {
          public void run()
          {
            if (paramLong == ObservableTimeoutTimed.TimeoutTimedObserver.this.index)
            {
              ObservableTimeoutTimed.TimeoutTimedObserver localTimeoutTimedObserver = ObservableTimeoutTimed.TimeoutTimedObserver.this;
              localTimeoutTimedObserver.done = true;
              DisposableHelper.dispose(localTimeoutTimedObserver);
              ObservableTimeoutTimed.TimeoutTimedObserver.this.s.dispose();
              ObservableTimeoutTimed.TimeoutTimedObserver.this.actual.onError(new TimeoutException());
              ObservableTimeoutTimed.TimeoutTimedObserver.this.worker.dispose();
            }
          }
        }, this.timeout, this.unit));
      }
    }
  }
  
  static final class TimeoutTimedOtherObserver<T>
    extends AtomicReference<Disposable>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -4619702551964128179L;
    final Observer<? super T> actual;
    final ObserverFullArbiter<T> arbiter;
    volatile boolean done;
    volatile long index;
    final ObservableSource<? extends T> other;
    Disposable s;
    final long timeout;
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    TimeoutTimedOtherObserver(Observer<? super T> paramObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker, ObservableSource<? extends T> paramObservableSource)
    {
      this.actual = paramObserver;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
      this.other = paramObservableSource;
      this.arbiter = new ObserverFullArbiter(paramObserver, this, 8);
    }
    
    public void dispose()
    {
      this.worker.dispose();
      DisposableHelper.dispose(this);
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.worker.dispose();
      DisposableHelper.dispose(this);
      this.arbiter.onComplete(this.s);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.worker.dispose();
      DisposableHelper.dispose(this);
      this.arbiter.onError(paramThrowable, this.s);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      if (this.arbiter.onNext(paramT, this.s)) {
        scheduleTimeout(l);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        if (this.arbiter.setDisposable(paramDisposable))
        {
          this.actual.onSubscribe(this.arbiter);
          scheduleTimeout(0L);
        }
      }
    }
    
    void scheduleTimeout(final long paramLong)
    {
      Disposable localDisposable = (Disposable)get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      if (compareAndSet(localDisposable, ObservableTimeoutTimed.NEW_TIMER)) {
        DisposableHelper.replace(this, this.worker.schedule(new Runnable()
        {
          public void run()
          {
            if (paramLong == ObservableTimeoutTimed.TimeoutTimedOtherObserver.this.index)
            {
              ObservableTimeoutTimed.TimeoutTimedOtherObserver localTimeoutTimedOtherObserver = ObservableTimeoutTimed.TimeoutTimedOtherObserver.this;
              localTimeoutTimedOtherObserver.done = true;
              localTimeoutTimedOtherObserver.s.dispose();
              DisposableHelper.dispose(ObservableTimeoutTimed.TimeoutTimedOtherObserver.this);
              ObservableTimeoutTimed.TimeoutTimedOtherObserver.this.subscribeNext();
              ObservableTimeoutTimed.TimeoutTimedOtherObserver.this.worker.dispose();
            }
          }
        }, this.timeout, this.unit));
      }
    }
    
    void subscribeNext()
    {
      this.other.subscribe(new FullArbiterObserver(this.arbiter));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTimeoutTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */