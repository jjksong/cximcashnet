package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableZip<T, R>
  extends Observable<R>
{
  final int bufferSize;
  final boolean delayError;
  final ObservableSource<? extends T>[] sources;
  final Iterable<? extends ObservableSource<? extends T>> sourcesIterable;
  final Function<? super Object[], ? extends R> zipper;
  
  public ObservableZip(ObservableSource<? extends T>[] paramArrayOfObservableSource, Iterable<? extends ObservableSource<? extends T>> paramIterable, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
  {
    this.sources = paramArrayOfObservableSource;
    this.sourcesIterable = paramIterable;
    this.zipper = paramFunction;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    Object localObject2 = this.sources;
    if (localObject2 == null)
    {
      Object localObject1 = new Observable[8];
      Iterator localIterator = this.sourcesIterable.iterator();
      int i = 0;
      for (;;)
      {
        localObject2 = localObject1;
        j = i;
        if (!localIterator.hasNext()) {
          break;
        }
        ObservableSource localObservableSource = (ObservableSource)localIterator.next();
        localObject2 = localObject1;
        if (i == localObject1.length)
        {
          localObject2 = new ObservableSource[(i >> 2) + i];
          System.arraycopy(localObject1, 0, localObject2, 0, i);
        }
        localObject2[i] = localObservableSource;
        i++;
        localObject1 = localObject2;
      }
    }
    int j = localObject2.length;
    if (j == 0)
    {
      EmptyDisposable.complete(paramObserver);
      return;
    }
    new ZipCoordinator(paramObserver, this.zipper, j, this.delayError).subscribe((ObservableSource[])localObject2, this.bufferSize);
  }
  
  static final class ZipCoordinator<T, R>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = 2983708048395377667L;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    final boolean delayError;
    final ObservableZip.ZipObserver<T, R>[] observers;
    final T[] row;
    final Function<? super Object[], ? extends R> zipper;
    
    ZipCoordinator(Observer<? super R> paramObserver, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.zipper = paramFunction;
      this.observers = new ObservableZip.ZipObserver[paramInt];
      this.row = ((Object[])new Object[paramInt]);
      this.delayError = paramBoolean;
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Observer<? super R> paramObserver, boolean paramBoolean3, ObservableZip.ZipObserver<?, ?> paramZipObserver)
    {
      if (this.cancelled)
      {
        clear();
        return true;
      }
      if (paramBoolean1) {
        if (paramBoolean3)
        {
          if (paramBoolean2)
          {
            paramZipObserver = paramZipObserver.error;
            clear();
            if (paramZipObserver != null) {
              paramObserver.onError(paramZipObserver);
            } else {
              paramObserver.onComplete();
            }
            return true;
          }
        }
        else
        {
          paramZipObserver = paramZipObserver.error;
          if (paramZipObserver != null)
          {
            clear();
            paramObserver.onError(paramZipObserver);
            return true;
          }
          if (paramBoolean2)
          {
            clear();
            paramObserver.onComplete();
            return true;
          }
        }
      }
      return false;
    }
    
    void clear()
    {
      for (ObservableZip.ZipObserver localZipObserver : this.observers)
      {
        localZipObserver.dispose();
        localZipObserver.queue.clear();
      }
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        if (getAndIncrement() == 0) {
          clear();
        }
      }
    }
    
    public void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      ObservableZip.ZipObserver[] arrayOfZipObserver = this.observers;
      Observer localObserver = this.actual;
      Object[] arrayOfObject = this.row;
      boolean bool2 = this.delayError;
      int i = 1;
      for (;;)
      {
        int i1 = arrayOfZipObserver.length;
        int m = 0;
        int n = 0;
        int j = 0;
        Object localObject1;
        while (m < i1)
        {
          localObject1 = arrayOfZipObserver[m];
          int k;
          if (arrayOfObject[j] == null)
          {
            boolean bool3 = ((ObservableZip.ZipObserver)localObject1).done;
            Object localObject2 = ((ObservableZip.ZipObserver)localObject1).queue.poll();
            boolean bool1;
            if (localObject2 == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool3, bool1, localObserver, bool2, (ObservableZip.ZipObserver)localObject1)) {
              return;
            }
            if (!bool1)
            {
              arrayOfObject[j] = localObject2;
              k = n;
            }
            else
            {
              k = n + 1;
            }
          }
          else
          {
            k = n;
            if (((ObservableZip.ZipObserver)localObject1).done)
            {
              k = n;
              if (!bool2)
              {
                localObject1 = ((ObservableZip.ZipObserver)localObject1).error;
                k = n;
                if (localObject1 != null)
                {
                  clear();
                  localObserver.onError((Throwable)localObject1);
                  return;
                }
              }
            }
          }
          j++;
          m++;
          n = k;
        }
        if (n != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          try
          {
            localObject1 = ObjectHelper.requireNonNull(this.zipper.apply(arrayOfObject.clone()), "The zipper returned a null value");
            localObserver.onNext(localObject1);
            Arrays.fill(arrayOfObject, null);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            clear();
            localObserver.onError(localThrowable);
          }
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void subscribe(ObservableSource<? extends T>[] paramArrayOfObservableSource, int paramInt)
    {
      ObservableZip.ZipObserver[] arrayOfZipObserver = this.observers;
      int k = arrayOfZipObserver.length;
      int j = 0;
      for (int i = 0; i < k; i++) {
        arrayOfZipObserver[i] = new ObservableZip.ZipObserver(this, paramInt);
      }
      lazySet(0);
      this.actual.onSubscribe(this);
      for (paramInt = j; paramInt < k; paramInt++)
      {
        if (this.cancelled) {
          return;
        }
        paramArrayOfObservableSource[paramInt].subscribe(arrayOfZipObserver[paramInt]);
      }
    }
  }
  
  static final class ZipObserver<T, R>
    implements Observer<T>
  {
    volatile boolean done;
    Throwable error;
    final ObservableZip.ZipCoordinator<T, R> parent;
    final SpscLinkedArrayQueue<T> queue;
    final AtomicReference<Disposable> s = new AtomicReference();
    
    ZipObserver(ObservableZip.ZipCoordinator<T, R> paramZipCoordinator, int paramInt)
    {
      this.parent = paramZipCoordinator;
      this.queue = new SpscLinkedArrayQueue(paramInt);
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.s);
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      this.parent.drain();
    }
    
    public void onNext(T paramT)
    {
      this.queue.offer(paramT);
      this.parent.drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.s, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableZip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */