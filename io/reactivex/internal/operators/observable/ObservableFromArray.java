package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicQueueDisposable;

public final class ObservableFromArray<T>
  extends Observable<T>
{
  final T[] array;
  
  public ObservableFromArray(T[] paramArrayOfT)
  {
    this.array = paramArrayOfT;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    FromArrayDisposable localFromArrayDisposable = new FromArrayDisposable(paramObserver, this.array);
    paramObserver.onSubscribe(localFromArrayDisposable);
    if (localFromArrayDisposable.fusionMode) {
      return;
    }
    localFromArrayDisposable.run();
  }
  
  static final class FromArrayDisposable<T>
    extends BasicQueueDisposable<T>
  {
    final Observer<? super T> actual;
    final T[] array;
    volatile boolean disposed;
    boolean fusionMode;
    int index;
    
    FromArrayDisposable(Observer<? super T> paramObserver, T[] paramArrayOfT)
    {
      this.actual = paramObserver;
      this.array = paramArrayOfT;
    }
    
    public void clear()
    {
      this.index = this.array.length;
    }
    
    public void dispose()
    {
      this.disposed = true;
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.index == this.array.length) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public T poll()
    {
      int i = this.index;
      Object[] arrayOfObject = this.array;
      if (i != arrayOfObject.length)
      {
        this.index = (i + 1);
        return (T)ObjectHelper.requireNonNull(arrayOfObject[i], "The array element is null");
      }
      return null;
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x1) != 0)
      {
        this.fusionMode = true;
        return 1;
      }
      return 0;
    }
    
    void run()
    {
      Object localObject = this.array;
      int j = localObject.length;
      for (int i = 0; (i < j) && (!isDisposed()); i++)
      {
        Observer localObserver = localObject[i];
        if (localObserver == null)
        {
          localObserver = this.actual;
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append("The ");
          ((StringBuilder)localObject).append(i);
          ((StringBuilder)localObject).append("th element is null");
          localObserver.onError(new NullPointerException(((StringBuilder)localObject).toString()));
          return;
        }
        this.actual.onNext(localObserver);
      }
      if (!isDisposed()) {
        this.actual.onComplete();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableFromArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */