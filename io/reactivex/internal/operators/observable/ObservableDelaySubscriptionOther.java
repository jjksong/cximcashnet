package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableDelaySubscriptionOther<T, U>
  extends Observable<T>
{
  final ObservableSource<? extends T> main;
  final ObservableSource<U> other;
  
  public ObservableDelaySubscriptionOther(ObservableSource<? extends T> paramObservableSource, ObservableSource<U> paramObservableSource1)
  {
    this.main = paramObservableSource;
    this.other = paramObservableSource1;
  }
  
  public void subscribeActual(final Observer<? super T> paramObserver)
  {
    final SequentialDisposable localSequentialDisposable = new SequentialDisposable();
    paramObserver.onSubscribe(localSequentialDisposable);
    paramObserver = new Observer()
    {
      boolean done;
      
      public void onComplete()
      {
        if (this.done) {
          return;
        }
        this.done = true;
        ObservableDelaySubscriptionOther.this.main.subscribe(new Observer()
        {
          public void onComplete()
          {
            ObservableDelaySubscriptionOther.1.this.val$child.onComplete();
          }
          
          public void onError(Throwable paramAnonymous2Throwable)
          {
            ObservableDelaySubscriptionOther.1.this.val$child.onError(paramAnonymous2Throwable);
          }
          
          public void onNext(T paramAnonymous2T)
          {
            ObservableDelaySubscriptionOther.1.this.val$child.onNext(paramAnonymous2T);
          }
          
          public void onSubscribe(Disposable paramAnonymous2Disposable)
          {
            ObservableDelaySubscriptionOther.1.this.val$serial.update(paramAnonymous2Disposable);
          }
        });
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        if (this.done)
        {
          RxJavaPlugins.onError(paramAnonymousThrowable);
          return;
        }
        this.done = true;
        paramObserver.onError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        onComplete();
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localSequentialDisposable.update(paramAnonymousDisposable);
      }
    };
    this.other.subscribe(paramObserver);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDelaySubscriptionOther.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */