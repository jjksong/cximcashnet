package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public final class ObservableSkip<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final long n;
  
  public ObservableSkip(ObservableSource<T> paramObservableSource, long paramLong)
  {
    super(paramObservableSource);
    this.n = paramLong;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new SkipObserver(paramObserver, this.n));
  }
  
  static final class SkipObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    Disposable d;
    long remaining;
    
    SkipObserver(Observer<? super T> paramObserver, long paramLong)
    {
      this.actual = paramObserver;
      this.remaining = paramLong;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.remaining;
      if (l != 0L) {
        this.remaining = (l - 1L);
      } else {
        this.actual.onNext(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.d = paramDisposable;
      this.actual.onSubscribe(this);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSkip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */