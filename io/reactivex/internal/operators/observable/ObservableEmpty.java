package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.fuseable.ScalarCallable;

public final class ObservableEmpty
  extends Observable<Object>
  implements ScalarCallable<Object>
{
  public static final Observable<Object> INSTANCE = new ObservableEmpty();
  
  public Object call()
  {
    return null;
  }
  
  protected void subscribeActual(Observer<? super Object> paramObserver)
  {
    EmptyDisposable.complete(paramObserver);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableEmpty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */