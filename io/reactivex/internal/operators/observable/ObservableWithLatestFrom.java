package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWithLatestFrom<T, U, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final BiFunction<? super T, ? super U, ? extends R> combiner;
  final ObservableSource<? extends U> other;
  
  public ObservableWithLatestFrom(ObservableSource<T> paramObservableSource, BiFunction<? super T, ? super U, ? extends R> paramBiFunction, ObservableSource<? extends U> paramObservableSource1)
  {
    super(paramObservableSource);
    this.combiner = paramBiFunction;
    this.other = paramObservableSource1;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    paramObserver = new SerializedObserver(paramObserver);
    final WithLatestFromObserver localWithLatestFromObserver = new WithLatestFromObserver(paramObserver, this.combiner);
    paramObserver.onSubscribe(localWithLatestFromObserver);
    this.other.subscribe(new Observer()
    {
      public void onComplete() {}
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        localWithLatestFromObserver.otherError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        localWithLatestFromObserver.lazySet(paramAnonymousU);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localWithLatestFromObserver.setOther(paramAnonymousDisposable);
      }
    });
    this.source.subscribe(localWithLatestFromObserver);
  }
  
  static final class WithLatestFromObserver<T, U, R>
    extends AtomicReference<U>
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -312246233408980075L;
    final Observer<? super R> actual;
    final BiFunction<? super T, ? super U, ? extends R> combiner;
    final AtomicReference<Disposable> other = new AtomicReference();
    final AtomicReference<Disposable> s = new AtomicReference();
    
    WithLatestFromObserver(Observer<? super R> paramObserver, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
    {
      this.actual = paramObserver;
      this.combiner = paramBiFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.s);
      DisposableHelper.dispose(this.other);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.s.get());
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this.other);
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.other);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      Object localObject = get();
      if (localObject != null) {
        try
        {
          paramT = ObjectHelper.requireNonNull(this.combiner.apply(paramT, localObject), "The combiner returned a null value");
          this.actual.onNext(paramT);
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          dispose();
          this.actual.onError(paramT);
          return;
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.s, paramDisposable);
    }
    
    public void otherError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.s);
      this.actual.onError(paramThrowable);
    }
    
    public boolean setOther(Disposable paramDisposable)
    {
      return DisposableHelper.setOnce(this.other, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableWithLatestFrom.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */