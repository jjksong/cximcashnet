package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Cancellable;
import io.reactivex.internal.disposables.CancellableDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableCreate<T>
  extends Observable<T>
{
  final ObservableOnSubscribe<T> source;
  
  public ObservableCreate(ObservableOnSubscribe<T> paramObservableOnSubscribe)
  {
    this.source = paramObservableOnSubscribe;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    CreateEmitter localCreateEmitter = new CreateEmitter(paramObserver);
    paramObserver.onSubscribe(localCreateEmitter);
    try
    {
      this.source.subscribe(localCreateEmitter);
    }
    catch (Throwable paramObserver)
    {
      Exceptions.throwIfFatal(paramObserver);
      localCreateEmitter.onError(paramObserver);
    }
  }
  
  static final class CreateEmitter<T>
    extends AtomicReference<Disposable>
    implements ObservableEmitter<T>, Disposable
  {
    private static final long serialVersionUID = -3434801548987643227L;
    final Observer<? super T> observer;
    
    CreateEmitter(Observer<? super T> paramObserver)
    {
      this.observer = paramObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    /* Error */
    public void onComplete()
    {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual 47	io/reactivex/internal/operators/observable/ObservableCreate$CreateEmitter:isDisposed	()Z
      //   4: ifne +26 -> 30
      //   7: aload_0
      //   8: getfield 26	io/reactivex/internal/operators/observable/ObservableCreate$CreateEmitter:observer	Lio/reactivex/Observer;
      //   11: invokeinterface 51 1 0
      //   16: aload_0
      //   17: invokevirtual 53	io/reactivex/internal/operators/observable/ObservableCreate$CreateEmitter:dispose	()V
      //   20: goto +10 -> 30
      //   23: astore_1
      //   24: aload_0
      //   25: invokevirtual 53	io/reactivex/internal/operators/observable/ObservableCreate$CreateEmitter:dispose	()V
      //   28: aload_1
      //   29: athrow
      //   30: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	31	0	this	CreateEmitter
      //   23	6	1	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   7	16	23	finally
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      if (!isDisposed()) {}
      try
      {
        this.observer.onError((Throwable)localObject);
        dispose();
      }
      finally
      {
        dispose();
      }
    }
    
    public void onNext(T paramT)
    {
      if (paramT == null)
      {
        onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        return;
      }
      if (!isDisposed()) {
        this.observer.onNext(paramT);
      }
    }
    
    public ObservableEmitter<T> serialize()
    {
      return new ObservableCreate.SerializedEmitter(this);
    }
    
    public void setCancellable(Cancellable paramCancellable)
    {
      setDisposable(new CancellableDisposable(paramCancellable));
    }
    
    public void setDisposable(Disposable paramDisposable)
    {
      DisposableHelper.set(this, paramDisposable);
    }
  }
  
  static final class SerializedEmitter<T>
    extends AtomicInteger
    implements ObservableEmitter<T>
  {
    private static final long serialVersionUID = 4883307006032401862L;
    volatile boolean done;
    final ObservableEmitter<T> emitter;
    final AtomicThrowable error;
    final SpscLinkedArrayQueue<T> queue;
    
    SerializedEmitter(ObservableEmitter<T> paramObservableEmitter)
    {
      this.emitter = paramObservableEmitter;
      this.error = new AtomicThrowable();
      this.queue = new SpscLinkedArrayQueue(16);
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        drainLoop();
      }
    }
    
    void drainLoop()
    {
      ObservableEmitter localObservableEmitter = this.emitter;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      AtomicThrowable localAtomicThrowable = this.error;
      int i = 1;
      for (;;)
      {
        if (localObservableEmitter.isDisposed())
        {
          localSpscLinkedArrayQueue.clear();
          return;
        }
        if (localAtomicThrowable.get() != null)
        {
          localSpscLinkedArrayQueue.clear();
          localObservableEmitter.onError(localAtomicThrowable.terminate());
          return;
        }
        boolean bool = this.done;
        Object localObject = localSpscLinkedArrayQueue.poll();
        int j;
        if (localObject == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          localObservableEmitter.onComplete();
          return;
        }
        if (j != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          localObservableEmitter.onNext(localObject);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.emitter.isDisposed();
    }
    
    public void onComplete()
    {
      if ((!this.emitter.isDisposed()) && (!this.done))
      {
        this.done = true;
        drain();
        return;
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.emitter.isDisposed()) && (!this.done))
      {
        Object localObject = paramThrowable;
        if (paramThrowable == null) {
          localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        if (this.error.addThrowable((Throwable)localObject))
        {
          this.done = true;
          drain();
        }
        else
        {
          RxJavaPlugins.onError((Throwable)localObject);
        }
        return;
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if ((!this.emitter.isDisposed()) && (!this.done))
      {
        if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
          return;
        }
        if ((get() == 0) && (compareAndSet(0, 1)))
        {
          this.emitter.onNext(paramT);
          if (decrementAndGet() != 0) {
            break label99;
          }
        }
        synchronized (this.queue)
        {
          ???.offer(paramT);
          if (getAndIncrement() != 0) {
            return;
          }
          label99:
          drainLoop();
          return;
        }
      }
    }
    
    public ObservableEmitter<T> serialize()
    {
      return this;
    }
    
    public void setCancellable(Cancellable paramCancellable)
    {
      this.emitter.setCancellable(paramCancellable);
    }
    
    public void setDisposable(Disposable paramDisposable)
    {
      this.emitter.setDisposable(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCreate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */