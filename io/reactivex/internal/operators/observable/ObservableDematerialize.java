package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableDematerialize<T>
  extends AbstractObservableWithUpstream<Notification<T>, T>
{
  public ObservableDematerialize(ObservableSource<Notification<T>> paramObservableSource)
  {
    super(paramObservableSource);
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new DematerializeObserver(paramObserver));
  }
  
  static final class DematerializeObserver<T>
    implements Observer<Notification<T>>, Disposable
  {
    final Observer<? super T> actual;
    boolean done;
    Disposable s;
    
    DematerializeObserver(Observer<? super T> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Notification<T> paramNotification)
    {
      if (this.done)
      {
        if (paramNotification.isOnError()) {
          RxJavaPlugins.onError(paramNotification.getError());
        }
        return;
      }
      if (paramNotification.isOnError())
      {
        this.s.dispose();
        onError(paramNotification.getError());
      }
      else if (paramNotification.isOnComplete())
      {
        this.s.dispose();
        onComplete();
      }
      else
      {
        this.actual.onNext(paramNotification.getValue());
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDematerialize.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */