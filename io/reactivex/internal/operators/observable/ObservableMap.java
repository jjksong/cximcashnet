package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.observers.BasicFuseableObserver;

public final class ObservableMap<T, U>
  extends AbstractObservableWithUpstream<T, U>
{
  final Function<? super T, ? extends U> function;
  
  public ObservableMap(ObservableSource<T> paramObservableSource, Function<? super T, ? extends U> paramFunction)
  {
    super(paramObservableSource);
    this.function = paramFunction;
  }
  
  public void subscribeActual(Observer<? super U> paramObserver)
  {
    this.source.subscribe(new MapObserver(paramObserver, this.function));
  }
  
  static final class MapObserver<T, U>
    extends BasicFuseableObserver<T, U>
  {
    final Function<? super T, ? extends U> mapper;
    
    MapObserver(Observer<? super U> paramObserver, Function<? super T, ? extends U> paramFunction)
    {
      super();
      this.mapper = paramFunction;
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode != 0)
      {
        this.actual.onNext(null);
        return;
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper function returned a null value.");
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
    }
    
    public U poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {
        localObject = ObjectHelper.requireNonNull(this.mapper.apply(localObject), "The mapper function returned a null value.");
      } else {
        localObject = null;
      }
      return (U)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */