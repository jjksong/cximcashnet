package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableScanSeed<T, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final BiFunction<R, ? super T, R> accumulator;
  final Callable<R> seedSupplier;
  
  public ObservableScanSeed(ObservableSource<T> paramObservableSource, Callable<R> paramCallable, BiFunction<R, ? super T, R> paramBiFunction)
  {
    super(paramObservableSource);
    this.accumulator = paramBiFunction;
    this.seedSupplier = paramCallable;
  }
  
  public void subscribeActual(Observer<? super R> paramObserver)
  {
    try
    {
      Object localObject = ObjectHelper.requireNonNull(this.seedSupplier.call(), "The seed supplied is null");
      this.source.subscribe(new ScanSeedObserver(paramObserver, this.accumulator, localObject));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
  
  static final class ScanSeedObserver<T, R>
    implements Observer<T>, Disposable
  {
    final BiFunction<R, ? super T, R> accumulator;
    final Observer<? super R> actual;
    boolean done;
    Disposable s;
    R value;
    
    ScanSeedObserver(Observer<? super R> paramObserver, BiFunction<R, ? super T, R> paramBiFunction, R paramR)
    {
      this.actual = paramObserver;
      this.accumulator = paramBiFunction;
      this.value = paramR;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Object localObject = this.value;
      try
      {
        paramT = ObjectHelper.requireNonNull(this.accumulator.apply(localObject, paramT), "The accumulator returned a null value");
        this.value = paramT;
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
        this.actual.onNext(this.value);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableScanSeed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */