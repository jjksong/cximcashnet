package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.observers.BasicIntQueueDisposable;

public final class ObservableRange
  extends Observable<Integer>
{
  private final long end;
  private final int start;
  
  public ObservableRange(int paramInt1, int paramInt2)
  {
    this.start = paramInt1;
    this.end = (paramInt1 + paramInt2);
  }
  
  protected void subscribeActual(Observer<? super Integer> paramObserver)
  {
    RangeDisposable localRangeDisposable = new RangeDisposable(paramObserver, this.start, this.end);
    paramObserver.onSubscribe(localRangeDisposable);
    localRangeDisposable.run();
  }
  
  static final class RangeDisposable
    extends BasicIntQueueDisposable<Integer>
  {
    private static final long serialVersionUID = 396518478098735504L;
    final Observer<? super Integer> actual;
    final long end;
    boolean fused;
    long index;
    
    RangeDisposable(Observer<? super Integer> paramObserver, long paramLong1, long paramLong2)
    {
      this.actual = paramObserver;
      this.index = paramLong1;
      this.end = paramLong2;
    }
    
    public void clear()
    {
      this.index = this.end;
      lazySet(1);
    }
    
    public void dispose()
    {
      set(1);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() != 0) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.index == this.end) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public Integer poll()
      throws Exception
    {
      long l = this.index;
      if (l != this.end)
      {
        this.index = (1L + l);
        return Integer.valueOf((int)l);
      }
      lazySet(1);
      return null;
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x1) != 0)
      {
        this.fused = true;
        return 1;
      }
      return 0;
    }
    
    void run()
    {
      if (this.fused) {
        return;
      }
      Observer localObserver = this.actual;
      long l2 = this.end;
      for (long l1 = this.index; (l1 != l2) && (get() == 0); l1 += 1L) {
        localObserver.onNext(Integer.valueOf((int)l1));
      }
      if (get() == 0)
      {
        lazySet(1);
        localObserver.onComplete();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableRange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */