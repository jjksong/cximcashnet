package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableBufferExactBoundary<T, U extends Collection<? super T>, B>
  extends AbstractObservableWithUpstream<T, U>
{
  final ObservableSource<B> boundary;
  final Callable<U> bufferSupplier;
  
  public ObservableBufferExactBoundary(ObservableSource<T> paramObservableSource, ObservableSource<B> paramObservableSource1, Callable<U> paramCallable)
  {
    super(paramObservableSource);
    this.boundary = paramObservableSource1;
    this.bufferSupplier = paramCallable;
  }
  
  protected void subscribeActual(Observer<? super U> paramObserver)
  {
    this.source.subscribe(new BufferExactBoundaryObserver(new SerializedObserver(paramObserver), this.bufferSupplier, this.boundary));
  }
  
  static final class BufferBoundaryObserver<T, U extends Collection<? super T>, B>
    extends DisposableObserver<B>
  {
    final ObservableBufferExactBoundary.BufferExactBoundaryObserver<T, U, B> parent;
    
    BufferBoundaryObserver(ObservableBufferExactBoundary.BufferExactBoundaryObserver<T, U, B> paramBufferExactBoundaryObserver)
    {
      this.parent = paramBufferExactBoundaryObserver;
    }
    
    public void onComplete()
    {
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      this.parent.next();
    }
  }
  
  static final class BufferExactBoundaryObserver<T, U extends Collection<? super T>, B>
    extends QueueDrainObserver<T, U, U>
    implements Observer<T>, Disposable
  {
    final ObservableSource<B> boundary;
    U buffer;
    final Callable<U> bufferSupplier;
    Disposable other;
    Disposable s;
    
    BufferExactBoundaryObserver(Observer<? super U> paramObserver, Callable<U> paramCallable, ObservableSource<B> paramObservableSource)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.boundary = paramObservableSource;
    }
    
    public void accept(Observer<? super U> paramObserver, U paramU)
    {
      this.actual.onNext(paramU);
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.other.dispose();
        this.s.dispose();
        if (enter()) {
          this.queue.clear();
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    void next()
    {
      try
      {
        Collection localCollection2 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
        try
        {
          Collection localCollection1 = this.buffer;
          if (localCollection1 == null) {
            return;
          }
          this.buffer = localCollection2;
          fastPathEmit(localCollection1, false, this);
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        dispose();
        this.actual.onError(localThrowable);
      }
    }
    
    public void onComplete()
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        localCollection.add(paramT);
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        try
        {
          Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
          this.buffer = localCollection;
          paramDisposable = new ObservableBufferExactBoundary.BufferBoundaryObserver(this);
          this.other = paramDisposable;
          this.actual.onSubscribe(this);
          if (!this.cancelled) {
            this.boundary.subscribe(paramDisposable);
          }
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.cancelled = true;
          paramDisposable.dispose();
          EmptyDisposable.error(localThrowable, this.actual);
          return;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableBufferExactBoundary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */