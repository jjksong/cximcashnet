package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ObservableBuffer<T, U extends Collection<? super T>>
  extends AbstractObservableWithUpstream<T, U>
{
  final Callable<U> bufferSupplier;
  final int count;
  final int skip;
  
  public ObservableBuffer(ObservableSource<T> paramObservableSource, int paramInt1, int paramInt2, Callable<U> paramCallable)
  {
    super(paramObservableSource);
    this.count = paramInt1;
    this.skip = paramInt2;
    this.bufferSupplier = paramCallable;
  }
  
  protected void subscribeActual(Observer<? super U> paramObserver)
  {
    int i = this.skip;
    int j = this.count;
    if (i == j)
    {
      paramObserver = new BufferExactObserver(paramObserver, j, this.bufferSupplier);
      if (paramObserver.createBuffer()) {
        this.source.subscribe(paramObserver);
      }
    }
    else
    {
      this.source.subscribe(new BufferSkipObserver(paramObserver, this.count, this.skip, this.bufferSupplier));
    }
  }
  
  static final class BufferExactObserver<T, U extends Collection<? super T>>
    implements Observer<T>, Disposable
  {
    final Observer<? super U> actual;
    U buffer;
    final Callable<U> bufferSupplier;
    final int count;
    Disposable s;
    int size;
    
    BufferExactObserver(Observer<? super U> paramObserver, int paramInt, Callable<U> paramCallable)
    {
      this.actual = paramObserver;
      this.count = paramInt;
      this.bufferSupplier = paramCallable;
    }
    
    boolean createBuffer()
    {
      try
      {
        Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "Empty buffer supplied");
        this.buffer = localCollection;
        return true;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.buffer = null;
        Disposable localDisposable = this.s;
        if (localDisposable == null)
        {
          EmptyDisposable.error(localThrowable, this.actual);
        }
        else
        {
          localDisposable.dispose();
          this.actual.onError(localThrowable);
        }
      }
      return false;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      Collection localCollection = this.buffer;
      this.buffer = null;
      if ((localCollection != null) && (!localCollection.isEmpty())) {
        this.actual.onNext(localCollection);
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.buffer = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      Collection localCollection = this.buffer;
      if (localCollection != null)
      {
        localCollection.add(paramT);
        int i = this.size + 1;
        this.size = i;
        if (i >= this.count)
        {
          this.actual.onNext(localCollection);
          this.size = 0;
          createBuffer();
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
  
  static final class BufferSkipObserver<T, U extends Collection<? super T>>
    extends AtomicBoolean
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -8223395059921494546L;
    final Observer<? super U> actual;
    final Callable<U> bufferSupplier;
    final ArrayDeque<U> buffers;
    final int count;
    long index;
    Disposable s;
    final int skip;
    
    BufferSkipObserver(Observer<? super U> paramObserver, int paramInt1, int paramInt2, Callable<U> paramCallable)
    {
      this.actual = paramObserver;
      this.count = paramInt1;
      this.skip = paramInt2;
      this.bufferSupplier = paramCallable;
      this.buffers = new ArrayDeque();
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      while (!this.buffers.isEmpty()) {
        this.actual.onNext(this.buffers.poll());
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.buffers.clear();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.index;
      this.index = (1L + l);
      if (l % this.skip == 0L) {
        try
        {
          localObject = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
          this.buffers.offer(localObject);
        }
        catch (Throwable paramT)
        {
          this.buffers.clear();
          this.s.dispose();
          this.actual.onError(paramT);
          return;
        }
      }
      Object localObject = this.buffers.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Collection localCollection = (Collection)((Iterator)localObject).next();
        localCollection.add(paramT);
        if (this.count <= localCollection.size())
        {
          ((Iterator)localObject).remove();
          this.actual.onNext(localCollection);
        }
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */