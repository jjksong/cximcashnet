package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableSkipLastTimed<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final int bufferSize;
  final boolean delayError;
  final Scheduler scheduler;
  final long time;
  final TimeUnit unit;
  
  public ObservableSkipLastTimed(ObservableSource<T> paramObservableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.time = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new SkipLastTimedObserver(paramObserver, this.time, this.unit, this.scheduler, this.bufferSize, this.delayError));
  }
  
  static final class SkipLastTimedObserver<T>
    extends AtomicInteger
    implements Observer<T>, Disposable
  {
    private static final long serialVersionUID = -5677354903406201275L;
    final Observer<? super T> actual;
    volatile boolean cancelled;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final SpscLinkedArrayQueue<Object> queue;
    Disposable s;
    final Scheduler scheduler;
    final long time;
    final TimeUnit unit;
    
    SkipLastTimedObserver(Observer<? super T> paramObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.time = paramLong;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
      this.queue = new SpscLinkedArrayQueue(paramInt);
      this.delayError = paramBoolean;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.dispose();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = this.actual;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      boolean bool2 = this.delayError;
      TimeUnit localTimeUnit = this.unit;
      Object localObject1 = this.scheduler;
      long l2 = this.time;
      int j = 1;
      for (;;)
      {
        if (this.cancelled)
        {
          this.queue.clear();
          return;
        }
        boolean bool1 = this.done;
        Object localObject2 = (Long)localSpscLinkedArrayQueue.peek();
        int i;
        if (localObject2 == null) {
          i = 1;
        } else {
          i = 0;
        }
        long l1 = ((Scheduler)localObject1).now(localTimeUnit);
        int k = i;
        if (i == 0)
        {
          k = i;
          if (((Long)localObject2).longValue() > l1 - l2) {
            k = 1;
          }
        }
        if (bool1) {
          if (bool2)
          {
            if (k != 0)
            {
              localObject1 = this.error;
              if (localObject1 != null) {
                localObserver.onError((Throwable)localObject1);
              } else {
                localObserver.onComplete();
              }
            }
          }
          else
          {
            localObject2 = this.error;
            if (localObject2 != null)
            {
              this.queue.clear();
              localObserver.onError((Throwable)localObject2);
              return;
            }
            if (k != 0)
            {
              localObserver.onComplete();
              return;
            }
          }
        }
        if (k != 0)
        {
          i = addAndGet(-j);
          j = i;
          if (i != 0) {}
        }
        else
        {
          localSpscLinkedArrayQueue.poll();
          localObserver.onNext(localSpscLinkedArrayQueue.poll());
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      this.queue.offer(Long.valueOf(this.scheduler.now(this.unit)), paramT);
      drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSkipLastTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */