package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.observers.BasicIntQueueDisposable;
import io.reactivex.plugins.RxJavaPlugins;

@Experimental
public final class ObservableDoFinally<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final Action onFinally;
  
  public ObservableDoFinally(ObservableSource<T> paramObservableSource, Action paramAction)
  {
    super(paramObservableSource);
    this.onFinally = paramAction;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new DoFinallyObserver(paramObserver, this.onFinally));
  }
  
  static final class DoFinallyObserver<T>
    extends BasicIntQueueDisposable<T>
    implements Observer<T>
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final Observer<? super T> actual;
    Disposable d;
    final Action onFinally;
    QueueDisposable<T> qd;
    boolean syncFused;
    
    DoFinallyObserver(Observer<? super T> paramObserver, Action paramAction)
    {
      this.actual = paramObserver;
      this.onFinally = paramAction;
    }
    
    public void clear()
    {
      this.qd.clear();
    }
    
    public void dispose()
    {
      this.d.dispose();
      runFinally();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public boolean isEmpty()
    {
      return this.qd.isEmpty();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      runFinally();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        if ((paramDisposable instanceof QueueDisposable)) {
          this.qd = ((QueueDisposable)paramDisposable);
        }
        this.actual.onSubscribe(this);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qd.poll();
      if ((localObject == null) && (this.syncFused)) {
        runFinally();
      }
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      QueueDisposable localQueueDisposable = this.qd;
      if ((localQueueDisposable != null) && ((paramInt & 0x4) == 0))
      {
        paramInt = localQueueDisposable.requestFusion(paramInt);
        if (paramInt != 0)
        {
          boolean bool = true;
          if (paramInt != 1) {
            bool = false;
          }
          this.syncFused = bool;
        }
        return paramInt;
      }
      return 0;
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDoFinally.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */