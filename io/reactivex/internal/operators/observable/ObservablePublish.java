package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.HasUpstreamObservableSource;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservablePublish<T>
  extends ConnectableObservable<T>
  implements HasUpstreamObservableSource<T>
{
  final AtomicReference<PublishObserver<T>> current;
  final ObservableSource<T> onSubscribe;
  final ObservableSource<T> source;
  
  private ObservablePublish(ObservableSource<T> paramObservableSource1, ObservableSource<T> paramObservableSource2, AtomicReference<PublishObserver<T>> paramAtomicReference)
  {
    this.onSubscribe = paramObservableSource1;
    this.source = paramObservableSource2;
    this.current = paramAtomicReference;
  }
  
  public static <T> ConnectableObservable<T> create(ObservableSource<T> paramObservableSource)
  {
    AtomicReference localAtomicReference = new AtomicReference();
    RxJavaPlugins.onAssembly(new ObservablePublish(new ObservableSource()
    {
      public void subscribe(Observer<? super T> paramAnonymousObserver)
      {
        ObservablePublish.InnerDisposable localInnerDisposable = new ObservablePublish.InnerDisposable(paramAnonymousObserver);
        paramAnonymousObserver.onSubscribe(localInnerDisposable);
        do
        {
          ObservablePublish.PublishObserver localPublishObserver;
          do
          {
            localPublishObserver = (ObservablePublish.PublishObserver)this.val$curr.get();
            if (localPublishObserver != null)
            {
              paramAnonymousObserver = localPublishObserver;
              if (!localPublishObserver.isDisposed()) {
                break;
              }
            }
            paramAnonymousObserver = new ObservablePublish.PublishObserver(this.val$curr);
          } while (!this.val$curr.compareAndSet(localPublishObserver, paramAnonymousObserver));
        } while (!paramAnonymousObserver.add(localInnerDisposable));
        localInnerDisposable.setParent(paramAnonymousObserver);
      }
    }, paramObservableSource, localAtomicReference));
  }
  
  public void connect(Consumer<? super Disposable> paramConsumer)
  {
    PublishObserver localPublishObserver2;
    PublishObserver localPublishObserver1;
    do
    {
      localPublishObserver2 = (PublishObserver)this.current.get();
      if (localPublishObserver2 != null)
      {
        localPublishObserver1 = localPublishObserver2;
        if (!localPublishObserver2.isDisposed()) {
          break;
        }
      }
      localPublishObserver1 = new PublishObserver(this.current);
    } while (!this.current.compareAndSet(localPublishObserver2, localPublishObserver1));
    boolean bool = localPublishObserver1.shouldConnect.get();
    int i = 1;
    if ((bool) || (!localPublishObserver1.shouldConnect.compareAndSet(false, true))) {
      i = 0;
    }
    try
    {
      paramConsumer.accept(localPublishObserver1);
      if (i != 0) {
        this.source.subscribe(localPublishObserver1);
      }
      return;
    }
    catch (Throwable paramConsumer)
    {
      Exceptions.throwIfFatal(paramConsumer);
      throw ExceptionHelper.wrapOrThrow(paramConsumer);
    }
  }
  
  public ObservableSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.onSubscribe.subscribe(paramObserver);
  }
  
  static final class InnerDisposable<T>
    extends AtomicReference<Object>
    implements Disposable
  {
    private static final long serialVersionUID = -1100270633763673112L;
    final Observer<? super T> child;
    
    InnerDisposable(Observer<? super T> paramObserver)
    {
      this.child = paramObserver;
    }
    
    public void dispose()
    {
      Object localObject = getAndSet(this);
      if ((localObject != null) && (localObject != this)) {
        ((ObservablePublish.PublishObserver)localObject).remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == this) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    void setParent(ObservablePublish.PublishObserver<T> paramPublishObserver)
    {
      if (!compareAndSet(null, paramPublishObserver)) {
        paramPublishObserver.remove(this);
      }
    }
  }
  
  static final class PublishObserver<T>
    implements Observer<T>, Disposable
  {
    static final ObservablePublish.InnerDisposable[] EMPTY = new ObservablePublish.InnerDisposable[0];
    static final ObservablePublish.InnerDisposable[] TERMINATED = new ObservablePublish.InnerDisposable[0];
    final AtomicReference<PublishObserver<T>> current;
    final AtomicReference<ObservablePublish.InnerDisposable<T>[]> observers = new AtomicReference(EMPTY);
    final AtomicReference<Disposable> s = new AtomicReference();
    final AtomicBoolean shouldConnect;
    
    PublishObserver(AtomicReference<PublishObserver<T>> paramAtomicReference)
    {
      this.current = paramAtomicReference;
      this.shouldConnect = new AtomicBoolean();
    }
    
    boolean add(ObservablePublish.InnerDisposable<T> paramInnerDisposable)
    {
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable1;
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable2;
      do
      {
        arrayOfInnerDisposable1 = (ObservablePublish.InnerDisposable[])this.observers.get();
        if (arrayOfInnerDisposable1 == TERMINATED) {
          return false;
        }
        int i = arrayOfInnerDisposable1.length;
        arrayOfInnerDisposable2 = new ObservablePublish.InnerDisposable[i + 1];
        System.arraycopy(arrayOfInnerDisposable1, 0, arrayOfInnerDisposable2, 0, i);
        arrayOfInnerDisposable2[i] = paramInnerDisposable;
      } while (!this.observers.compareAndSet(arrayOfInnerDisposable1, arrayOfInnerDisposable2));
      return true;
    }
    
    public void dispose()
    {
      Object localObject = this.observers.get();
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable = TERMINATED;
      if ((localObject != arrayOfInnerDisposable) && ((ObservablePublish.InnerDisposable[])this.observers.getAndSet(arrayOfInnerDisposable) != TERMINATED))
      {
        this.current.compareAndSet(this, null);
        DisposableHelper.dispose(this.s);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.observers.get() == TERMINATED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.current.compareAndSet(this, null);
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable = (ObservablePublish.InnerDisposable[])this.observers.getAndSet(TERMINATED);
      int j = arrayOfInnerDisposable.length;
      for (int i = 0; i < j; i++) {
        arrayOfInnerDisposable[i].child.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.current.compareAndSet(this, null);
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable = (ObservablePublish.InnerDisposable[])this.observers.getAndSet(TERMINATED);
      if (arrayOfInnerDisposable.length != 0)
      {
        int j = arrayOfInnerDisposable.length;
        for (int i = 0; i < j; i++) {
          arrayOfInnerDisposable[i].child.onError(paramThrowable);
        }
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable = (ObservablePublish.InnerDisposable[])this.observers.get();
      int j = arrayOfInnerDisposable.length;
      for (int i = 0; i < j; i++) {
        arrayOfInnerDisposable[i].child.onNext(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this.s, paramDisposable);
    }
    
    void remove(ObservablePublish.InnerDisposable<T> paramInnerDisposable)
    {
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable2;
      ObservablePublish.InnerDisposable[] arrayOfInnerDisposable1;
      do
      {
        arrayOfInnerDisposable2 = (ObservablePublish.InnerDisposable[])this.observers.get();
        int m = arrayOfInnerDisposable2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerDisposable2[i].equals(paramInnerDisposable))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerDisposable1 = EMPTY;
        }
        else
        {
          arrayOfInnerDisposable1 = new ObservablePublish.InnerDisposable[m - 1];
          System.arraycopy(arrayOfInnerDisposable2, 0, arrayOfInnerDisposable1, 0, j);
          System.arraycopy(arrayOfInnerDisposable2, j + 1, arrayOfInnerDisposable1, j, m - j - 1);
        }
      } while (!this.observers.compareAndSet(arrayOfInnerDisposable2, arrayOfInnerDisposable1));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservablePublish.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */