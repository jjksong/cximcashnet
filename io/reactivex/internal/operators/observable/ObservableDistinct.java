package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.observers.BasicFuseableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableDistinct<T, K>
  extends AbstractObservableWithUpstream<T, T>
{
  final Callable<? extends Collection<? super K>> collectionSupplier;
  final Function<? super T, K> keySelector;
  
  public ObservableDistinct(ObservableSource<T> paramObservableSource, Function<? super T, K> paramFunction, Callable<? extends Collection<? super K>> paramCallable)
  {
    super(paramObservableSource);
    this.keySelector = paramFunction;
    this.collectionSupplier = paramCallable;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    try
    {
      Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.collectionSupplier.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      this.source.subscribe(new DistinctObserver(paramObserver, this.keySelector, localCollection));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramObserver);
    }
  }
  
  static final class DistinctObserver<T, K>
    extends BasicFuseableObserver<T, T>
  {
    final Collection<? super K> collection;
    final Function<? super T, K> keySelector;
    
    DistinctObserver(Observer<? super T> paramObserver, Function<? super T, K> paramFunction, Collection<? super K> paramCollection)
    {
      super();
      this.keySelector = paramFunction;
      this.collection = paramCollection;
    }
    
    public void clear()
    {
      this.collection.clear();
      super.clear();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.collection.clear();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
      }
      else
      {
        this.done = true;
        this.collection.clear();
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode == 0) {
        try
        {
          Object localObject = ObjectHelper.requireNonNull(this.keySelector.apply(paramT), "The keySelector returned a null key");
          boolean bool = this.collection.add(localObject);
          if (!bool) {
            return;
          }
          this.actual.onNext(paramT);
        }
        catch (Throwable paramT)
        {
          fail(paramT);
          return;
        }
      } else {
        this.actual.onNext(null);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject;
      do
      {
        localObject = this.qs.poll();
      } while ((localObject != null) && (!this.collection.add(ObjectHelper.requireNonNull(this.keySelector.apply(localObject), "The keySelector returned a null key"))));
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableDistinct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */