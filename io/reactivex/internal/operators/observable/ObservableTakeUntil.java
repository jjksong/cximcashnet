package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.ArrayCompositeDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ObservableTakeUntil<T, U>
  extends AbstractObservableWithUpstream<T, T>
{
  final ObservableSource<? extends U> other;
  
  public ObservableTakeUntil(ObservableSource<T> paramObservableSource, ObservableSource<? extends U> paramObservableSource1)
  {
    super(paramObservableSource);
    this.other = paramObservableSource1;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    final SerializedObserver localSerializedObserver = new SerializedObserver(paramObserver);
    final ArrayCompositeDisposable localArrayCompositeDisposable = new ArrayCompositeDisposable(2);
    TakeUntilObserver localTakeUntilObserver = new TakeUntilObserver(localSerializedObserver, localArrayCompositeDisposable);
    paramObserver.onSubscribe(localArrayCompositeDisposable);
    this.other.subscribe(new Observer()
    {
      public void onComplete()
      {
        localArrayCompositeDisposable.dispose();
        localSerializedObserver.onComplete();
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        localArrayCompositeDisposable.dispose();
        localSerializedObserver.onError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        localArrayCompositeDisposable.dispose();
        localSerializedObserver.onComplete();
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localArrayCompositeDisposable.setResource(1, paramAnonymousDisposable);
      }
    });
    this.source.subscribe(localTakeUntilObserver);
  }
  
  static final class TakeUntilObserver<T>
    extends AtomicBoolean
    implements Observer<T>
  {
    private static final long serialVersionUID = 3451719290311127173L;
    final Observer<? super T> actual;
    final ArrayCompositeDisposable frc;
    Disposable s;
    
    TakeUntilObserver(Observer<? super T> paramObserver, ArrayCompositeDisposable paramArrayCompositeDisposable)
    {
      this.actual = paramObserver;
      this.frc = paramArrayCompositeDisposable;
    }
    
    public void onComplete()
    {
      this.frc.dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.frc.dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.frc.setResource(0, paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableTakeUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */