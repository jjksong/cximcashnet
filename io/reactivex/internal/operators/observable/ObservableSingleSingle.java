package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;

public final class ObservableSingleSingle<T>
  extends Single<T>
{
  final T defaultValue;
  final ObservableSource<? extends T> source;
  
  public ObservableSingleSingle(ObservableSource<? extends T> paramObservableSource, T paramT)
  {
    this.source = paramObservableSource;
    this.defaultValue = paramT;
  }
  
  public void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleElementObserver(paramSingleObserver, this.defaultValue));
  }
  
  static final class SingleElementObserver<T>
    implements Observer<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    final T defaultValue;
    boolean done;
    Disposable s;
    T value;
    
    SingleElementObserver(SingleObserver<? super T> paramSingleObserver, T paramT)
    {
      this.actual = paramSingleObserver;
      this.defaultValue = paramT;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject2 = this.value;
      this.value = null;
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = this.defaultValue;
      }
      if (localObject1 != null) {
        this.actual.onSuccess(localObject1);
      } else {
        this.actual.onError(new NoSuchElementException());
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.value != null)
      {
        this.done = true;
        this.s.dispose();
        this.actual.onError(new IllegalArgumentException("Sequence contains more than one element!"));
        return;
      }
      this.value = paramT;
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableSingleSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */