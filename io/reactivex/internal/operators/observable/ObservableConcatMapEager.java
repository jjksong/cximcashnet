package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.InnerQueuedObserver;
import io.reactivex.internal.observers.InnerQueuedObserverSupport;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayDeque;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObservableConcatMapEager<T, R>
  extends AbstractObservableWithUpstream<T, R>
{
  final ErrorMode errorMode;
  final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
  final int maxConcurrency;
  final int prefetch;
  
  public ObservableConcatMapEager(ObservableSource<T> paramObservableSource, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, ErrorMode paramErrorMode, int paramInt1, int paramInt2)
  {
    super(paramObservableSource);
    this.mapper = paramFunction;
    this.errorMode = paramErrorMode;
    this.maxConcurrency = paramInt1;
    this.prefetch = paramInt2;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    this.source.subscribe(new ConcatMapEagerMainObserver(paramObserver, this.mapper, this.maxConcurrency, this.prefetch, this.errorMode));
  }
  
  static final class ConcatMapEagerMainObserver<T, R>
    extends AtomicInteger
    implements Observer<T>, Disposable, InnerQueuedObserverSupport<R>
  {
    private static final long serialVersionUID = 8080567949447303262L;
    int activeCount;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    InnerQueuedObserver<R> current;
    Disposable d;
    volatile boolean done;
    final AtomicThrowable error;
    final ErrorMode errorMode;
    final Function<? super T, ? extends ObservableSource<? extends R>> mapper;
    final int maxConcurrency;
    final ArrayDeque<InnerQueuedObserver<R>> observers;
    final int prefetch;
    SimpleQueue<T> queue;
    int sourceMode;
    
    ConcatMapEagerMainObserver(Observer<? super R> paramObserver, Function<? super T, ? extends ObservableSource<? extends R>> paramFunction, int paramInt1, int paramInt2, ErrorMode paramErrorMode)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
      this.maxConcurrency = paramInt1;
      this.prefetch = paramInt2;
      this.errorMode = paramErrorMode;
      this.error = new AtomicThrowable();
      this.observers = new ArrayDeque();
    }
    
    public void dispose()
    {
      this.cancelled = true;
      if (getAndIncrement() == 0)
      {
        this.queue.clear();
        disposeAll();
      }
    }
    
    void disposeAll()
    {
      InnerQueuedObserver localInnerQueuedObserver = this.current;
      if (localInnerQueuedObserver != null) {
        localInnerQueuedObserver.dispose();
      }
      for (;;)
      {
        localInnerQueuedObserver = (InnerQueuedObserver)this.observers.poll();
        if (localInnerQueuedObserver == null) {
          return;
        }
        localInnerQueuedObserver.dispose();
      }
    }
    
    public void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      SimpleQueue localSimpleQueue = this.queue;
      ArrayDeque localArrayDeque = this.observers;
      Observer localObserver = this.actual;
      ErrorMode localErrorMode = this.errorMode;
      int i = 1;
      int j;
      do
      {
        for (;;)
        {
          j = this.activeCount;
          while (j != this.maxConcurrency)
          {
            if (this.cancelled)
            {
              localSimpleQueue.clear();
              disposeAll();
              return;
            }
            if ((localErrorMode == ErrorMode.IMMEDIATE) && ((Throwable)this.error.get() != null))
            {
              localSimpleQueue.clear();
              disposeAll();
              localObserver.onError(this.error.terminate());
              return;
            }
            try
            {
              Object localObject1 = localSimpleQueue.poll();
              if (localObject1 != null)
              {
                localObject1 = (ObservableSource)ObjectHelper.requireNonNull(this.mapper.apply(localObject1), "The mapper returned a null ObservableSource");
                localObject3 = new InnerQueuedObserver(this, this.prefetch);
                localArrayDeque.offer(localObject3);
                ((ObservableSource)localObject1).subscribe((Observer)localObject3);
                j++;
              }
            }
            catch (Throwable localThrowable1)
            {
              Exceptions.throwIfFatal(localThrowable1);
              this.d.dispose();
              localSimpleQueue.clear();
              disposeAll();
              this.error.addThrowable(localThrowable1);
              localObserver.onError(this.error.terminate());
              return;
            }
          }
          this.activeCount = j;
          if (this.cancelled)
          {
            localSimpleQueue.clear();
            disposeAll();
            return;
          }
          if ((localErrorMode == ErrorMode.IMMEDIATE) && ((Throwable)this.error.get() != null))
          {
            localSimpleQueue.clear();
            disposeAll();
            localObserver.onError(this.error.terminate());
            return;
          }
          Object localObject3 = this.current;
          Object localObject2 = localObject3;
          if (localObject3 == null)
          {
            if ((localErrorMode == ErrorMode.BOUNDARY) && ((Throwable)this.error.get() != null))
            {
              localSimpleQueue.clear();
              disposeAll();
              localObserver.onError(this.error.terminate());
              return;
            }
            bool = this.done;
            localObject2 = (InnerQueuedObserver)localArrayDeque.poll();
            if (localObject2 == null) {
              j = 1;
            } else {
              j = 0;
            }
            if ((bool) && (j != 0))
            {
              if ((Throwable)this.error.get() != null)
              {
                localSimpleQueue.clear();
                disposeAll();
                localObserver.onError(this.error.terminate());
              }
              else
              {
                localObserver.onComplete();
              }
              return;
            }
            if (j == 0) {
              this.current = ((InnerQueuedObserver)localObject2);
            }
          }
          if (localObject2 == null) {
            break;
          }
          localObject3 = ((InnerQueuedObserver)localObject2).queue();
          if (this.cancelled)
          {
            localSimpleQueue.clear();
            disposeAll();
            return;
          }
          boolean bool = ((InnerQueuedObserver)localObject2).isDone();
          if ((localErrorMode == ErrorMode.IMMEDIATE) && ((Throwable)this.error.get() != null))
          {
            localSimpleQueue.clear();
            disposeAll();
            localObserver.onError(this.error.terminate());
            return;
          }
          try
          {
            Object localObject4 = ((SimpleQueue)localObject3).poll();
            if (localObject4 == null) {
              j = 1;
            } else {
              j = 0;
            }
            if ((bool) && (j != 0))
            {
              this.current = null;
              this.activeCount -= 1;
            }
            else
            {
              if (j != 0) {
                break;
              }
              localObserver.onNext(localObject4);
            }
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            this.error.addThrowable(localThrowable2);
            this.current = null;
            this.activeCount -= 1;
          }
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void innerComplete(InnerQueuedObserver<R> paramInnerQueuedObserver)
    {
      paramInnerQueuedObserver.setDone();
      drain();
    }
    
    public void innerError(InnerQueuedObserver<R> paramInnerQueuedObserver, Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable))
      {
        if (this.errorMode == ErrorMode.IMMEDIATE) {
          this.d.dispose();
        }
        paramInnerQueuedObserver.setDone();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerNext(InnerQueuedObserver<R> paramInnerQueuedObserver, R paramR)
    {
      paramInnerQueuedObserver.queue().offer(paramR);
      drain();
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.sourceMode == 0) {
        this.queue.offer(paramT);
      }
      drain();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        if ((paramDisposable instanceof QueueDisposable))
        {
          paramDisposable = (QueueDisposable)paramDisposable;
          int i = paramDisposable.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.done = true;
            this.actual.onSubscribe(this);
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = paramDisposable;
            this.actual.onSubscribe(this);
            return;
          }
        }
        this.queue = QueueDrainHelper.createQueue(this.prefetch);
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableConcatMapEager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */