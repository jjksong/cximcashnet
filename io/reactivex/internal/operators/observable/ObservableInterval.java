package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableInterval
  extends Observable<Long>
{
  final long initialDelay;
  final long period;
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public ObservableInterval(long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    this.initialDelay = paramLong1;
    this.period = paramLong2;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  public void subscribeActual(Observer<? super Long> paramObserver)
  {
    IntervalObserver localIntervalObserver = new IntervalObserver(paramObserver);
    paramObserver.onSubscribe(localIntervalObserver);
    localIntervalObserver.setResource(this.scheduler.schedulePeriodicallyDirect(localIntervalObserver, this.initialDelay, this.period, this.unit));
  }
  
  static final class IntervalObserver
    extends AtomicReference<Disposable>
    implements Disposable, Runnable
  {
    private static final long serialVersionUID = 346773832286157679L;
    final Observer<? super Long> actual;
    long count;
    
    IntervalObserver(Observer<? super Long> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void run()
    {
      if (get() != DisposableHelper.DISPOSED)
      {
        Observer localObserver = this.actual;
        long l = this.count;
        this.count = (1L + l);
        localObserver.onNext(Long.valueOf(l));
      }
    }
    
    public void setResource(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableInterval.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */