package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;

public final class ObservableElementAt<T>
  extends AbstractObservableWithUpstream<T, T>
{
  final T defaultValue;
  final boolean errorOnFewer;
  final long index;
  
  public ObservableElementAt(ObservableSource<T> paramObservableSource, long paramLong, T paramT, boolean paramBoolean)
  {
    super(paramObservableSource);
    this.index = paramLong;
    this.defaultValue = paramT;
    this.errorOnFewer = paramBoolean;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new ElementAtObserver(paramObserver, this.index, this.defaultValue, this.errorOnFewer));
  }
  
  static final class ElementAtObserver<T>
    implements Observer<T>, Disposable
  {
    final Observer<? super T> actual;
    long count;
    final T defaultValue;
    boolean done;
    final boolean errorOnFewer;
    final long index;
    Disposable s;
    
    ElementAtObserver(Observer<? super T> paramObserver, long paramLong, T paramT, boolean paramBoolean)
    {
      this.actual = paramObserver;
      this.index = paramLong;
      this.defaultValue = paramT;
      this.errorOnFewer = paramBoolean;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        Object localObject = this.defaultValue;
        if ((localObject == null) && (this.errorOnFewer))
        {
          this.actual.onError(new NoSuchElementException());
        }
        else
        {
          if (localObject != null) {
            this.actual.onNext(localObject);
          }
          this.actual.onComplete();
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.count;
      if (l == this.index)
      {
        this.done = true;
        this.s.dispose();
        this.actual.onNext(paramT);
        this.actual.onComplete();
        return;
      }
      this.count = (l + 1L);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableElementAt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */