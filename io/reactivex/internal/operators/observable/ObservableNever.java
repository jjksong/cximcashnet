package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.disposables.EmptyDisposable;

public final class ObservableNever
  extends Observable<Object>
{
  public static final Observable<Object> INSTANCE = new ObservableNever();
  
  protected void subscribeActual(Observer<? super Object> paramObserver)
  {
    paramObserver.onSubscribe(EmptyDisposable.NEVER);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableNever.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */