package io.reactivex.internal.operators.observable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableIgnoreElementsCompletable<T>
  extends Completable
  implements FuseToObservable<T>
{
  final ObservableSource<T> source;
  
  public ObservableIgnoreElementsCompletable(ObservableSource<T> paramObservableSource)
  {
    this.source = paramObservableSource;
  }
  
  public Observable<T> fuseToObservable()
  {
    return RxJavaPlugins.onAssembly(new ObservableIgnoreElements(this.source));
  }
  
  public void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new IgnoreObservable(paramCompletableObserver));
  }
  
  static final class IgnoreObservable<T>
    implements Observer<T>, Disposable
  {
    final CompletableObserver actual;
    Disposable d;
    
    IgnoreObservable(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT) {}
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.d = paramDisposable;
      this.actual.onSubscribe(this);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableIgnoreElementsCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */