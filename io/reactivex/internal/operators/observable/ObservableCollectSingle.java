package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableCollectSingle<T, U>
  extends Single<U>
  implements FuseToObservable<U>
{
  final BiConsumer<? super U, ? super T> collector;
  final Callable<? extends U> initialSupplier;
  final ObservableSource<T> source;
  
  public ObservableCollectSingle(ObservableSource<T> paramObservableSource, Callable<? extends U> paramCallable, BiConsumer<? super U, ? super T> paramBiConsumer)
  {
    this.source = paramObservableSource;
    this.initialSupplier = paramCallable;
    this.collector = paramBiConsumer;
  }
  
  public Observable<U> fuseToObservable()
  {
    return RxJavaPlugins.onAssembly(new ObservableCollect(this.source, this.initialSupplier, this.collector));
  }
  
  protected void subscribeActual(SingleObserver<? super U> paramSingleObserver)
  {
    try
    {
      Object localObject = ObjectHelper.requireNonNull(this.initialSupplier.call(), "The initialSupplier returned a null value");
      this.source.subscribe(new CollectObserver(paramSingleObserver, localObject, this.collector));
      return;
    }
    catch (Throwable localThrowable)
    {
      EmptyDisposable.error(localThrowable, paramSingleObserver);
    }
  }
  
  static final class CollectObserver<T, U>
    implements Observer<T>, Disposable
  {
    final SingleObserver<? super U> actual;
    final BiConsumer<? super U, ? super T> collector;
    boolean done;
    Disposable s;
    final U u;
    
    CollectObserver(SingleObserver<? super U> paramSingleObserver, U paramU, BiConsumer<? super U, ? super T> paramBiConsumer)
    {
      this.actual = paramSingleObserver;
      this.collector = paramBiConsumer;
      this.u = paramU;
    }
    
    public void dispose()
    {
      this.s.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.s.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onSuccess(this.u);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        this.collector.accept(this.u, paramT);
      }
      catch (Throwable paramT)
      {
        this.s.dispose();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.s, paramDisposable))
      {
        this.s = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/observable/ObservableCollectSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */