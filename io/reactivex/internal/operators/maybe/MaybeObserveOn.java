package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeObserveOn<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Scheduler scheduler;
  
  public MaybeObserveOn(MaybeSource<T> paramMaybeSource, Scheduler paramScheduler)
  {
    super(paramMaybeSource);
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new ObserveOnMaybeObserver(paramMaybeObserver, this.scheduler));
  }
  
  static final class ObserveOnMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable, Runnable
  {
    private static final long serialVersionUID = 8571289934935992137L;
    final MaybeObserver<? super T> actual;
    Throwable error;
    final Scheduler scheduler;
    T value;
    
    ObserveOnMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Scheduler paramScheduler)
    {
      this.actual = paramMaybeObserver;
      this.scheduler = paramScheduler;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      DisposableHelper.replace(this, this.scheduler.scheduleDirect(this));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      DisposableHelper.replace(this, this.scheduler.scheduleDirect(this));
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.value = paramT;
      DisposableHelper.replace(this, this.scheduler.scheduleDirect(this));
    }
    
    public void run()
    {
      Object localObject = this.error;
      if (localObject != null)
      {
        this.error = null;
        this.actual.onError((Throwable)localObject);
      }
      else
      {
        localObject = this.value;
        if (localObject != null)
        {
          this.value = null;
          this.actual.onSuccess(localObject);
        }
        else
        {
          this.actual.onComplete();
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeObserveOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */