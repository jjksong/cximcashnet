package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

@Experimental
public final class MaybeDoAfterSuccess<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Consumer<? super T> onAfterSuccess;
  
  public MaybeDoAfterSuccess(MaybeSource<T> paramMaybeSource, Consumer<? super T> paramConsumer)
  {
    super(paramMaybeSource);
    this.onAfterSuccess = paramConsumer;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DoAfterObserver(paramMaybeObserver, this.onAfterSuccess));
  }
  
  static final class DoAfterObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Consumer<? super T> onAfterSuccess;
    
    DoAfterObserver(MaybeObserver<? super T> paramMaybeObserver, Consumer<? super T> paramConsumer)
    {
      this.actual = paramMaybeObserver;
      this.onAfterSuccess = paramConsumer;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
      try
      {
        this.onAfterSuccess.accept(paramT);
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        RxJavaPlugins.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDoAfterSuccess.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */