package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeDelaySubscriptionOtherPublisher<T, U>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Publisher<U> other;
  
  public MaybeDelaySubscriptionOtherPublisher(MaybeSource<T> paramMaybeSource, Publisher<U> paramPublisher)
  {
    super(paramMaybeSource);
    this.other = paramPublisher;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.other.subscribe(new OtherSubscriber(paramMaybeObserver, this.source));
  }
  
  static final class DelayMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = 706635022205076709L;
    final MaybeObserver<? super T> actual;
    
    DelayMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
  
  static final class OtherSubscriber<T>
    implements Subscriber<Object>, Disposable
  {
    final MaybeDelaySubscriptionOtherPublisher.DelayMaybeObserver<T> main;
    Subscription s;
    MaybeSource<T> source;
    
    OtherSubscriber(MaybeObserver<? super T> paramMaybeObserver, MaybeSource<T> paramMaybeSource)
    {
      this.main = new MaybeDelaySubscriptionOtherPublisher.DelayMaybeObserver(paramMaybeObserver);
      this.source = paramMaybeSource;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
      DisposableHelper.dispose(this.main);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.main.get());
    }
    
    public void onComplete()
    {
      if (this.s != SubscriptionHelper.CANCELLED)
      {
        this.s = SubscriptionHelper.CANCELLED;
        subscribeNext();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.s != SubscriptionHelper.CANCELLED)
      {
        this.s = SubscriptionHelper.CANCELLED;
        this.main.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(Object paramObject)
    {
      if (this.s != SubscriptionHelper.CANCELLED)
      {
        this.s.cancel();
        this.s = SubscriptionHelper.CANCELLED;
        subscribeNext();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.main.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    void subscribeNext()
    {
      MaybeSource localMaybeSource = this.source;
      this.source = null;
      localMaybeSource.subscribe(this.main);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDelaySubscriptionOtherPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */