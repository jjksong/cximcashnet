package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public final class MaybeFlatMapSingleElement<T, R>
  extends Maybe<R>
{
  final Function<? super T, ? extends SingleSource<? extends R>> mapper;
  final MaybeSource<T> source;
  
  public MaybeFlatMapSingleElement(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends SingleSource<? extends R>> paramFunction)
  {
    this.source = paramMaybeSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    this.source.subscribe(new FlatMapMaybeObserver(paramMaybeObserver, this.mapper));
  }
  
  static final class FlatMapMaybeObserver<T, R>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 4827726964688405508L;
    final MaybeObserver<? super R> actual;
    final Function<? super T, ? extends SingleSource<? extends R>> mapper;
    
    FlatMapMaybeObserver(MaybeObserver<? super R> paramMaybeObserver, Function<? super T, ? extends SingleSource<? extends R>> paramFunction)
    {
      this.actual = paramMaybeObserver;
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = (SingleSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null SingleSource");
        paramT.subscribe(new MaybeFlatMapSingleElement.FlatMapSingleObserver(this, this.actual));
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        onError(paramT);
      }
    }
  }
  
  static final class FlatMapSingleObserver<R>
    implements SingleObserver<R>
  {
    final MaybeObserver<? super R> actual;
    final AtomicReference<Disposable> parent;
    
    FlatMapSingleObserver(AtomicReference<Disposable> paramAtomicReference, MaybeObserver<? super R> paramMaybeObserver)
    {
      this.parent = paramAtomicReference;
      this.actual = paramMaybeObserver;
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.replace(this.parent, paramDisposable);
    }
    
    public void onSuccess(R paramR)
    {
      this.actual.onSuccess(paramR);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapSingleElement.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */