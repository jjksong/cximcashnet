package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeUsing<T, D>
  extends Maybe<T>
{
  final boolean eager;
  final Consumer<? super D> resourceDisposer;
  final Callable<? extends D> resourceSupplier;
  final Function<? super D, ? extends MaybeSource<? extends T>> sourceSupplier;
  
  public MaybeUsing(Callable<? extends D> paramCallable, Function<? super D, ? extends MaybeSource<? extends T>> paramFunction, Consumer<? super D> paramConsumer, boolean paramBoolean)
  {
    this.resourceSupplier = paramCallable;
    this.sourceSupplier = paramFunction;
    this.resourceDisposer = paramConsumer;
    this.eager = paramBoolean;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    try
    {
      Object localObject = this.resourceSupplier.call();
      try
      {
        MaybeSource localMaybeSource = (MaybeSource)ObjectHelper.requireNonNull(this.sourceSupplier.apply(localObject), "The sourceSupplier returned a null MaybeSource");
        localMaybeSource.subscribe(new UsingObserver(paramMaybeObserver, localObject, this.resourceDisposer, this.eager));
        return;
      }
      catch (Throwable localThrowable3)
      {
        Exceptions.throwIfFatal(localThrowable3);
        if (this.eager) {
          try
          {
            this.resourceDisposer.accept(localObject);
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            EmptyDisposable.error(new CompositeException(new Throwable[] { localThrowable3, localThrowable1 }), paramMaybeObserver);
            return;
          }
        }
        EmptyDisposable.error(localThrowable3, paramMaybeObserver);
        if (!this.eager) {
          try
          {
            this.resourceDisposer.accept(localThrowable1);
          }
          catch (Throwable paramMaybeObserver)
          {
            Exceptions.throwIfFatal(paramMaybeObserver);
            RxJavaPlugins.onError(paramMaybeObserver);
          }
        }
        return;
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptyDisposable.error(localThrowable2, paramMaybeObserver);
    }
  }
  
  static final class UsingObserver<T, D>
    extends AtomicReference<Object>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = -674404550052917487L;
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Consumer<? super D> disposer;
    final boolean eager;
    
    UsingObserver(MaybeObserver<? super T> paramMaybeObserver, D paramD, Consumer<? super D> paramConsumer, boolean paramBoolean)
    {
      super();
      this.actual = paramMaybeObserver;
      this.disposer = paramConsumer;
      this.eager = paramBoolean;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
      disposeResourceAfter();
    }
    
    void disposeResourceAfter()
    {
      Object localObject = getAndSet(this);
      if (localObject != this) {
        try
        {
          this.disposer.accept(localObject);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      if (this.eager)
      {
        Object localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.actual.onError(localThrowable);
            return;
          }
        } else {
          return;
        }
      }
      this.actual.onComplete();
      if (!this.eager) {
        disposeResourceAfter();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      Object localObject = paramThrowable;
      CompositeException localCompositeException;
      if (this.eager)
      {
        localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
            localObject = paramThrowable;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            localCompositeException = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
          }
        } else {
          return;
        }
      }
      this.actual.onError(localCompositeException);
      if (!this.eager) {
        disposeResourceAfter();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      if (this.eager)
      {
        Object localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            this.actual.onError(paramT);
            return;
          }
        } else {
          return;
        }
      }
      this.actual.onSuccess(paramT);
      if (!this.eager) {
        disposeResourceAfter();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeUsing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */