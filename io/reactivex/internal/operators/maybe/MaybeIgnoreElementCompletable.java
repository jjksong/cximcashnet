package io.reactivex.internal.operators.maybe;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToMaybe;
import io.reactivex.plugins.RxJavaPlugins;

public final class MaybeIgnoreElementCompletable<T>
  extends Completable
  implements FuseToMaybe<T>
{
  final MaybeSource<T> source;
  
  public MaybeIgnoreElementCompletable(MaybeSource<T> paramMaybeSource)
  {
    this.source = paramMaybeSource;
  }
  
  public Maybe<T> fuseToMaybe()
  {
    return RxJavaPlugins.onAssembly(new MaybeIgnoreElement(this.source));
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new IgnoreMaybeObserver(paramCompletableObserver));
  }
  
  static final class IgnoreMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final CompletableObserver actual;
    Disposable d;
    
    IgnoreMaybeObserver(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onComplete();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeIgnoreElementCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */