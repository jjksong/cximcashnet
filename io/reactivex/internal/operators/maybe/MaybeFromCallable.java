package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class MaybeFromCallable<T>
  extends Maybe<T>
  implements Callable<T>
{
  final Callable<? extends T> callable;
  
  public MaybeFromCallable(Callable<? extends T> paramCallable)
  {
    this.callable = paramCallable;
  }
  
  public T call()
    throws Exception
  {
    return (T)this.callable.call();
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Disposable localDisposable = Disposables.empty();
    paramMaybeObserver.onSubscribe(localDisposable);
    if (!localDisposable.isDisposed()) {
      try
      {
        Object localObject = this.callable.call();
        if (!localDisposable.isDisposed()) {
          if (localObject == null) {
            paramMaybeObserver.onComplete();
          } else {
            paramMaybeObserver.onSuccess(localObject);
          }
        }
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onError(localThrowable);
        } else {
          RxJavaPlugins.onError(localThrowable);
        }
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFromCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */