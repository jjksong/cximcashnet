package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.internal.disposables.EmptyDisposable;

public final class MaybeNever
  extends Maybe<Object>
{
  public static final MaybeNever INSTANCE = new MaybeNever();
  
  protected void subscribeActual(MaybeObserver<? super Object> paramMaybeObserver)
  {
    paramMaybeObserver.onSubscribe(EmptyDisposable.NEVER);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeNever.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */