package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.HasUpstreamMaybeSource;

public final class MaybeContains<T>
  extends Single<Boolean>
  implements HasUpstreamMaybeSource<T>
{
  final MaybeSource<T> source;
  final Object value;
  
  public MaybeContains(MaybeSource<T> paramMaybeSource, Object paramObject)
  {
    this.source = paramMaybeSource;
    this.value = paramObject;
  }
  
  public MaybeSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    this.source.subscribe(new ContainsMaybeObserver(paramSingleObserver, this.value));
  }
  
  static final class ContainsMaybeObserver
    implements MaybeObserver<Object>, Disposable
  {
    final SingleObserver<? super Boolean> actual;
    Disposable d;
    final Object value;
    
    ContainsMaybeObserver(SingleObserver<? super Boolean> paramSingleObserver, Object paramObject)
    {
      this.actual = paramSingleObserver;
      this.value = paramObject;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Boolean.valueOf(false));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(Object paramObject)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Boolean.valueOf(ObjectHelper.equals(paramObject, this.value)));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeContains.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */