package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeFlatMapNotification<T, R>
  extends AbstractMaybeWithUpstream<T, R>
{
  final Callable<? extends MaybeSource<? extends R>> onCompleteSupplier;
  final Function<? super Throwable, ? extends MaybeSource<? extends R>> onErrorMapper;
  final Function<? super T, ? extends MaybeSource<? extends R>> onSuccessMapper;
  
  public MaybeFlatMapNotification(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction, Function<? super Throwable, ? extends MaybeSource<? extends R>> paramFunction1, Callable<? extends MaybeSource<? extends R>> paramCallable)
  {
    super(paramMaybeSource);
    this.onSuccessMapper = paramFunction;
    this.onErrorMapper = paramFunction1;
    this.onCompleteSupplier = paramCallable;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    this.source.subscribe(new FlatMapMaybeObserver(paramMaybeObserver, this.onSuccessMapper, this.onErrorMapper, this.onCompleteSupplier));
  }
  
  static final class FlatMapMaybeObserver<T, R>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 4375739915521278546L;
    final MaybeObserver<? super R> actual;
    Disposable d;
    final Callable<? extends MaybeSource<? extends R>> onCompleteSupplier;
    final Function<? super Throwable, ? extends MaybeSource<? extends R>> onErrorMapper;
    final Function<? super T, ? extends MaybeSource<? extends R>> onSuccessMapper;
    
    FlatMapMaybeObserver(MaybeObserver<? super R> paramMaybeObserver, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction, Function<? super Throwable, ? extends MaybeSource<? extends R>> paramFunction1, Callable<? extends MaybeSource<? extends R>> paramCallable)
    {
      this.actual = paramMaybeObserver;
      this.onSuccessMapper = paramFunction;
      this.onErrorMapper = paramFunction1;
      this.onCompleteSupplier = paramCallable;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      try
      {
        MaybeSource localMaybeSource = (MaybeSource)ObjectHelper.requireNonNull(this.onCompleteSupplier.call(), "The onCompleteSupplier returned a null MaybeSource");
        localMaybeSource.subscribe(new InnerObserver());
        return;
      }
      catch (Exception localException)
      {
        Exceptions.throwIfFatal(localException);
        this.actual.onError(localException);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        MaybeSource localMaybeSource = (MaybeSource)ObjectHelper.requireNonNull(this.onErrorMapper.apply(paramThrowable), "The onErrorMapper returned a null MaybeSource");
        localMaybeSource.subscribe(new InnerObserver());
        return;
      }
      catch (Exception localException)
      {
        Exceptions.throwIfFatal(localException);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localException }));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = (MaybeSource)ObjectHelper.requireNonNull(this.onSuccessMapper.apply(paramT), "The onSuccessMapper returned a null MaybeSource");
        paramT.subscribe(new InnerObserver());
        return;
      }
      catch (Exception paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    final class InnerObserver
      implements MaybeObserver<R>
    {
      InnerObserver() {}
      
      public void onComplete()
      {
        MaybeFlatMapNotification.FlatMapMaybeObserver.this.actual.onComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        MaybeFlatMapNotification.FlatMapMaybeObserver.this.actual.onError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(MaybeFlatMapNotification.FlatMapMaybeObserver.this, paramDisposable);
      }
      
      public void onSuccess(R paramR)
      {
        MaybeFlatMapNotification.FlatMapMaybeObserver.this.actual.onSuccess(paramR);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */