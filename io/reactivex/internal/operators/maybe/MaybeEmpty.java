package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.fuseable.ScalarCallable;

public final class MaybeEmpty
  extends Maybe<Object>
  implements ScalarCallable<Object>
{
  public static final MaybeEmpty INSTANCE = new MaybeEmpty();
  
  public Object call()
  {
    return null;
  }
  
  protected void subscribeActual(MaybeObserver<? super Object> paramMaybeObserver)
  {
    EmptyDisposable.complete(paramMaybeObserver);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeEmpty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */