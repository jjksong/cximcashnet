package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.HasUpstreamMaybeSource;

public final class MaybeCount<T>
  extends Single<Long>
  implements HasUpstreamMaybeSource<T>
{
  final MaybeSource<T> source;
  
  public MaybeCount(MaybeSource<T> paramMaybeSource)
  {
    this.source = paramMaybeSource;
  }
  
  public MaybeSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(SingleObserver<? super Long> paramSingleObserver)
  {
    this.source.subscribe(new CountMaybeObserver(paramSingleObserver));
  }
  
  static final class CountMaybeObserver
    implements MaybeObserver<Object>, Disposable
  {
    final SingleObserver<? super Long> actual;
    Disposable d;
    
    CountMaybeObserver(SingleObserver<? super Long> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Long.valueOf(0L));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(Object paramObject)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Long.valueOf(1L));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeCount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */