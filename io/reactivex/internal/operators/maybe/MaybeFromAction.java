package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class MaybeFromAction<T>
  extends Maybe<T>
  implements Callable<T>
{
  final Action action;
  
  public MaybeFromAction(Action paramAction)
  {
    this.action = paramAction;
  }
  
  public T call()
    throws Exception
  {
    this.action.run();
    return null;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Disposable localDisposable = Disposables.empty();
    paramMaybeObserver.onSubscribe(localDisposable);
    if (!localDisposable.isDisposed()) {
      try
      {
        this.action.run();
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onComplete();
        }
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onError(localThrowable);
        } else {
          RxJavaPlugins.onError(localThrowable);
        }
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFromAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */