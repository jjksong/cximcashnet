package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class MaybeFromFuture<T>
  extends Maybe<T>
{
  final Future<? extends T> future;
  final long timeout;
  final TimeUnit unit;
  
  public MaybeFromFuture(Future<? extends T> paramFuture, long paramLong, TimeUnit paramTimeUnit)
  {
    this.future = paramFuture;
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Disposable localDisposable = Disposables.empty();
    paramMaybeObserver.onSubscribe(localDisposable);
    if (!localDisposable.isDisposed()) {
      try
      {
        Object localObject;
        if (this.timeout <= 0L) {
          localObject = this.future.get();
        } else {
          localObject = this.future.get(this.timeout, this.unit);
        }
        if (!localDisposable.isDisposed()) {
          if (localObject == null) {
            paramMaybeObserver.onComplete();
          } else {
            paramMaybeObserver.onSuccess(localObject);
          }
        }
      }
      catch (TimeoutException localTimeoutException)
      {
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onError(localTimeoutException);
        }
        return;
      }
      catch (ExecutionException localExecutionException)
      {
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onError(localExecutionException.getCause());
        }
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        if (!localDisposable.isDisposed()) {
          paramMaybeObserver.onError(localInterruptedException);
        }
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFromFuture.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */