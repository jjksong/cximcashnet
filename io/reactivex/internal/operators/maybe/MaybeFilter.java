package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeFilter<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Predicate<? super T> predicate;
  
  public MaybeFilter(MaybeSource<T> paramMaybeSource, Predicate<? super T> paramPredicate)
  {
    super(paramMaybeSource);
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new FilterMaybeObserver(paramMaybeObserver, this.predicate));
  }
  
  static final class FilterMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Predicate<? super T> predicate;
    
    FilterMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Predicate<? super T> paramPredicate)
    {
      this.actual = paramMaybeObserver;
      this.predicate = paramPredicate;
    }
    
    public void dispose()
    {
      Disposable localDisposable = this.d;
      this.d = DisposableHelper.DISPOSED;
      localDisposable.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        boolean bool = this.predicate.test(paramT);
        if (bool) {
          this.actual.onSuccess(paramT);
        } else {
          this.actual.onComplete();
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */