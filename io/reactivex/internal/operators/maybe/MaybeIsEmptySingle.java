package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToMaybe;
import io.reactivex.internal.fuseable.HasUpstreamMaybeSource;
import io.reactivex.plugins.RxJavaPlugins;

public final class MaybeIsEmptySingle<T>
  extends Single<Boolean>
  implements HasUpstreamMaybeSource<T>, FuseToMaybe<Boolean>
{
  final MaybeSource<T> source;
  
  public MaybeIsEmptySingle(MaybeSource<T> paramMaybeSource)
  {
    this.source = paramMaybeSource;
  }
  
  public Maybe<Boolean> fuseToMaybe()
  {
    return RxJavaPlugins.onAssembly(new MaybeIsEmpty(this.source));
  }
  
  public MaybeSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    this.source.subscribe(new IsEmptyMaybeObserver(paramSingleObserver));
  }
  
  static final class IsEmptyMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final SingleObserver<? super Boolean> actual;
    Disposable d;
    
    IsEmptyMaybeObserver(SingleObserver<? super Boolean> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Boolean.valueOf(true));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(Boolean.valueOf(false));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeIsEmptySingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */