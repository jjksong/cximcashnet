package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;

public final class MaybeMap<T, R>
  extends AbstractMaybeWithUpstream<T, R>
{
  final Function<? super T, ? extends R> mapper;
  
  public MaybeMap(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends R> paramFunction)
  {
    super(paramMaybeSource);
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    this.source.subscribe(new MapMaybeObserver(paramMaybeObserver, this.mapper));
  }
  
  static final class MapMaybeObserver<T, R>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super R> actual;
    Disposable d;
    final Function<? super T, ? extends R> mapper;
    
    MapMaybeObserver(MaybeObserver<? super R> paramMaybeObserver, Function<? super T, ? extends R> paramFunction)
    {
      this.actual = paramMaybeObserver;
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      Disposable localDisposable = this.d;
      this.d = DisposableHelper.DISPOSED;
      localDisposable.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null item");
        this.actual.onSuccess(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */