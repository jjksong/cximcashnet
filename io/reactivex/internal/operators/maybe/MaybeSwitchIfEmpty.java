package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeSwitchIfEmpty<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final MaybeSource<? extends T> other;
  
  public MaybeSwitchIfEmpty(MaybeSource<T> paramMaybeSource, MaybeSource<? extends T> paramMaybeSource1)
  {
    super(paramMaybeSource);
    this.other = paramMaybeSource1;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new SwitchIfEmptyMaybeObserver(paramMaybeObserver, this.other));
  }
  
  static final class SwitchIfEmptyMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = -2223459372976438024L;
    final MaybeObserver<? super T> actual;
    final MaybeSource<? extends T> other;
    
    SwitchIfEmptyMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, MaybeSource<? extends T> paramMaybeSource)
    {
      this.actual = paramMaybeObserver;
      this.other = paramMaybeSource;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      Disposable localDisposable = (Disposable)get();
      if ((localDisposable != DisposableHelper.DISPOSED) && (compareAndSet(localDisposable, null))) {
        this.other.subscribe(new OtherMaybeObserver(this.actual, this));
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
    
    static final class OtherMaybeObserver<T>
      implements MaybeObserver<T>
    {
      final MaybeObserver<? super T> actual;
      final AtomicReference<Disposable> parent;
      
      OtherMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, AtomicReference<Disposable> paramAtomicReference)
      {
        this.actual = paramMaybeObserver;
        this.parent = paramAtomicReference;
      }
      
      public void onComplete()
      {
        this.actual.onComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.actual.onError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this.parent, paramDisposable);
      }
      
      public void onSuccess(T paramT)
      {
        this.actual.onSuccess(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeSwitchIfEmpty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */