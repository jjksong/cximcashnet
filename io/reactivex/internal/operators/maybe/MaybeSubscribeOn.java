package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeSubscribeOn<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Scheduler scheduler;
  
  public MaybeSubscribeOn(MaybeSource<T> paramMaybeSource, Scheduler paramScheduler)
  {
    super(paramMaybeSource);
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    SubscribeOnMaybeObserver localSubscribeOnMaybeObserver = new SubscribeOnMaybeObserver(paramMaybeObserver);
    paramMaybeObserver.onSubscribe(localSubscribeOnMaybeObserver);
    localSubscribeOnMaybeObserver.task.replace(this.scheduler.scheduleDirect(new SubscribeTask(localSubscribeOnMaybeObserver, this.source)));
  }
  
  static final class SubscribeOnMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 8571289934935992137L;
    final MaybeObserver<? super T> actual;
    final SequentialDisposable task;
    
    SubscribeOnMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
      this.task = new SequentialDisposable();
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      this.task.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
  
  static final class SubscribeTask<T>
    implements Runnable
  {
    final MaybeObserver<? super T> observer;
    final MaybeSource<T> source;
    
    SubscribeTask(MaybeObserver<? super T> paramMaybeObserver, MaybeSource<T> paramMaybeSource)
    {
      this.observer = paramMaybeObserver;
      this.source = paramMaybeSource;
    }
    
    public void run()
    {
      this.source.subscribe(this.observer);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeSubscribeOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */