package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeIsEmpty<T>
  extends AbstractMaybeWithUpstream<T, Boolean>
{
  public MaybeIsEmpty(MaybeSource<T> paramMaybeSource)
  {
    super(paramMaybeSource);
  }
  
  protected void subscribeActual(MaybeObserver<? super Boolean> paramMaybeObserver)
  {
    this.source.subscribe(new IsEmptyMaybeObserver(paramMaybeObserver));
  }
  
  static final class IsEmptyMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super Boolean> actual;
    Disposable d;
    
    IsEmptyMaybeObserver(MaybeObserver<? super Boolean> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onSuccess(Boolean.valueOf(true));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(Boolean.valueOf(false));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeIsEmpty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */