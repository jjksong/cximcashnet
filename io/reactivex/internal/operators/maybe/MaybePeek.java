package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class MaybePeek<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Action onAfterTerminate;
  final Action onCompleteCall;
  final Action onDisposeCall;
  final Consumer<? super Throwable> onErrorCall;
  final Consumer<? super Disposable> onSubscribeCall;
  final Consumer<? super T> onSuccessCall;
  
  public MaybePeek(MaybeSource<T> paramMaybeSource, Consumer<? super Disposable> paramConsumer, Consumer<? super T> paramConsumer1, Consumer<? super Throwable> paramConsumer2, Action paramAction1, Action paramAction2, Action paramAction3)
  {
    super(paramMaybeSource);
    this.onSubscribeCall = paramConsumer;
    this.onSuccessCall = paramConsumer1;
    this.onErrorCall = paramConsumer2;
    this.onCompleteCall = paramAction1;
    this.onAfterTerminate = paramAction2;
    this.onDisposeCall = paramAction3;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new MaybePeekObserver(paramMaybeObserver, this));
  }
  
  static final class MaybePeekObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final MaybePeek<T> parent;
    
    MaybePeekObserver(MaybeObserver<? super T> paramMaybeObserver, MaybePeek<T> paramMaybePeek)
    {
      this.actual = paramMaybeObserver;
      this.parent = paramMaybePeek;
    }
    
    public void dispose()
    {
      try
      {
        this.parent.onDisposeCall.run();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    void onAfterTerminate()
    {
      try
      {
        this.parent.onAfterTerminate.run();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
    }
    
    public void onComplete()
    {
      if (this.d == DisposableHelper.DISPOSED) {
        return;
      }
      try
      {
        this.parent.onCompleteCall.run();
        this.d = DisposableHelper.DISPOSED;
        this.actual.onComplete();
        onAfterTerminate();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        onErrorInner(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.d == DisposableHelper.DISPOSED)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      onErrorInner(paramThrowable);
    }
    
    void onErrorInner(Throwable paramThrowable)
    {
      try
      {
        this.parent.onErrorCall.accept(paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        paramThrowable = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
      }
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
      onAfterTerminate();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable)) {
        try
        {
          this.parent.onSubscribeCall.accept(paramDisposable);
          this.d = paramDisposable;
          this.actual.onSubscribe(this);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramDisposable.dispose();
          this.d = DisposableHelper.DISPOSED;
          EmptyDisposable.error(localThrowable, this.actual);
          return;
        }
      }
    }
    
    public void onSuccess(T paramT)
    {
      if (this.d == DisposableHelper.DISPOSED) {
        return;
      }
      try
      {
        this.parent.onSuccessCall.accept(paramT);
        this.d = DisposableHelper.DISPOSED;
        this.actual.onSuccess(paramT);
        onAfterTerminate();
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        onErrorInner(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybePeek.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */