package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeCallbackObserver<T>
  extends AtomicReference<Disposable>
  implements MaybeObserver<T>, Disposable
{
  private static final long serialVersionUID = -6076952298809384986L;
  final Action onComplete;
  final Consumer<? super Throwable> onError;
  final Consumer<? super T> onSuccess;
  
  public MaybeCallbackObserver(Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction)
  {
    this.onSuccess = paramConsumer;
    this.onError = paramConsumer1;
    this.onComplete = paramAction;
  }
  
  public void dispose()
  {
    DisposableHelper.dispose(this);
  }
  
  public boolean isDisposed()
  {
    return DisposableHelper.isDisposed((Disposable)get());
  }
  
  public void onComplete()
  {
    lazySet(DisposableHelper.DISPOSED);
    try
    {
      this.onComplete.run();
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    lazySet(DisposableHelper.DISPOSED);
    try
    {
      this.onError.accept(paramThrowable);
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    DisposableHelper.setOnce(this, paramDisposable);
  }
  
  public void onSuccess(T paramT)
  {
    lazySet(DisposableHelper.DISPOSED);
    try
    {
      this.onSuccess.accept(paramT);
    }
    catch (Throwable paramT)
    {
      Exceptions.throwIfFatal(paramT);
      RxJavaPlugins.onError(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeCallbackObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */