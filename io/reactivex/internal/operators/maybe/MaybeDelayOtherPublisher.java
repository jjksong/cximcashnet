package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeDelayOtherPublisher<T, U>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Publisher<U> other;
  
  public MaybeDelayOtherPublisher(MaybeSource<T> paramMaybeSource, Publisher<U> paramPublisher)
  {
    super(paramMaybeSource);
    this.other = paramPublisher;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DelayMaybeObserver(paramMaybeObserver, this.other));
  }
  
  static final class DelayMaybeObserver<T, U>
    implements MaybeObserver<T>, Disposable
  {
    Disposable d;
    final MaybeDelayOtherPublisher.OtherSubscriber<T> other;
    final Publisher<U> otherSource;
    
    DelayMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Publisher<U> paramPublisher)
    {
      this.other = new MaybeDelayOtherPublisher.OtherSubscriber(paramMaybeObserver);
      this.otherSource = paramPublisher;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
      SubscriptionHelper.cancel(this.other);
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)this.other.get());
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      subscribeNext();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.other.error = paramThrowable;
      subscribeNext();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.other.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      this.other.value = paramT;
      subscribeNext();
    }
    
    void subscribeNext()
    {
      this.otherSource.subscribe(this.other);
    }
  }
  
  static final class OtherSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<Object>
  {
    private static final long serialVersionUID = -1215060610805418006L;
    final MaybeObserver<? super T> actual;
    Throwable error;
    T value;
    
    OtherSubscriber(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void onComplete()
    {
      Object localObject = this.error;
      if (localObject != null)
      {
        this.actual.onError((Throwable)localObject);
      }
      else
      {
        localObject = this.value;
        if (localObject != null) {
          this.actual.onSuccess(localObject);
        } else {
          this.actual.onComplete();
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      Throwable localThrowable = this.error;
      if (localThrowable == null) {
        this.actual.onError(paramThrowable);
      } else {
        this.actual.onError(new CompositeException(new Throwable[] { localThrowable, paramThrowable }));
      }
    }
    
    public void onNext(Object paramObject)
    {
      paramObject = (Subscription)get();
      if (paramObject != SubscriptionHelper.CANCELLED)
      {
        lazySet(SubscriptionHelper.CANCELLED);
        ((Subscription)paramObject).cancel();
        onComplete();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDelayOtherPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */