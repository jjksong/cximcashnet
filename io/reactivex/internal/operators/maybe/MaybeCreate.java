package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeEmitter;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Cancellable;
import io.reactivex.internal.disposables.CancellableDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeCreate<T>
  extends Maybe<T>
{
  final MaybeOnSubscribe<T> source;
  
  public MaybeCreate(MaybeOnSubscribe<T> paramMaybeOnSubscribe)
  {
    this.source = paramMaybeOnSubscribe;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Emitter localEmitter = new Emitter(paramMaybeObserver);
    paramMaybeObserver.onSubscribe(localEmitter);
    try
    {
      this.source.subscribe(localEmitter);
    }
    catch (Throwable paramMaybeObserver)
    {
      Exceptions.throwIfFatal(paramMaybeObserver);
      localEmitter.onError(paramMaybeObserver);
    }
  }
  
  static final class Emitter<T>
    extends AtomicReference<Disposable>
    implements MaybeEmitter<T>, Disposable
  {
    private static final long serialVersionUID = -2467358622224974244L;
    final MaybeObserver<? super T> actual;
    
    Emitter(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    /* Error */
    public void onComplete()
    {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual 41	io/reactivex/internal/operators/maybe/MaybeCreate$Emitter:get	()Ljava/lang/Object;
      //   4: getstatic 49	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   7: if_acmpeq +56 -> 63
      //   10: aload_0
      //   11: getstatic 49	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   14: invokevirtual 53	io/reactivex/internal/operators/maybe/MaybeCreate$Emitter:getAndSet	(Ljava/lang/Object;)Ljava/lang/Object;
      //   17: checkcast 9	io/reactivex/disposables/Disposable
      //   20: astore_2
      //   21: aload_2
      //   22: getstatic 49	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   25: if_acmpeq +38 -> 63
      //   28: aload_0
      //   29: getfield 26	io/reactivex/internal/operators/maybe/MaybeCreate$Emitter:actual	Lio/reactivex/MaybeObserver;
      //   32: invokeinterface 57 1 0
      //   37: aload_2
      //   38: ifnull +25 -> 63
      //   41: aload_2
      //   42: invokeinterface 59 1 0
      //   47: goto +16 -> 63
      //   50: astore_1
      //   51: aload_2
      //   52: ifnull +9 -> 61
      //   55: aload_2
      //   56: invokeinterface 59 1 0
      //   61: aload_1
      //   62: athrow
      //   63: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	64	0	this	Emitter
      //   50	12	1	localObject	Object
      //   20	36	2	localDisposable	Disposable
      // Exception table:
      //   from	to	target	type
      //   28	37	50	finally
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      if (get() != DisposableHelper.DISPOSED)
      {
        paramThrowable = (Disposable)getAndSet(DisposableHelper.DISPOSED);
        if (paramThrowable != DisposableHelper.DISPOSED) {
          try
          {
            this.actual.onError((Throwable)localObject);
            return;
          }
          finally
          {
            if (paramThrowable != null) {
              paramThrowable.dispose();
            }
          }
        }
      }
      RxJavaPlugins.onError(localThrowable);
    }
    
    public void onSuccess(T paramT)
    {
      if (get() != DisposableHelper.DISPOSED)
      {
        Disposable localDisposable = (Disposable)getAndSet(DisposableHelper.DISPOSED);
        if (localDisposable != DisposableHelper.DISPOSED)
        {
          if (paramT == null) {}
          try
          {
            MaybeObserver localMaybeObserver = this.actual;
            paramT = new java/lang/NullPointerException;
            paramT.<init>("onSuccess called with null. Null values are generally not allowed in 2.x operators and sources.");
            localMaybeObserver.onError(paramT);
            break label67;
            this.actual.onSuccess(paramT);
          }
          finally
          {
            label67:
            if (localDisposable != null) {
              localDisposable.dispose();
            }
          }
        }
      }
    }
    
    public void setCancellable(Cancellable paramCancellable)
    {
      setDisposable(new CancellableDisposable(paramCancellable));
    }
    
    public void setDisposable(Disposable paramDisposable)
    {
      DisposableHelper.set(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeCreate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */