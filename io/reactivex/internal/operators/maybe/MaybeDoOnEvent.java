package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeDoOnEvent<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final BiConsumer<? super T, ? super Throwable> onEvent;
  
  public MaybeDoOnEvent(MaybeSource<T> paramMaybeSource, BiConsumer<? super T, ? super Throwable> paramBiConsumer)
  {
    super(paramMaybeSource);
    this.onEvent = paramBiConsumer;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DoOnEventMaybeObserver(paramMaybeObserver, this.onEvent));
  }
  
  static final class DoOnEventMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final BiConsumer<? super T, ? super Throwable> onEvent;
    
    DoOnEventMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, BiConsumer<? super T, ? super Throwable> paramBiConsumer)
    {
      this.actual = paramMaybeObserver;
      this.onEvent = paramBiConsumer;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      try
      {
        this.onEvent.accept(null, null);
        this.actual.onComplete();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      try
      {
        this.onEvent.accept(null, paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        paramThrowable = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      try
      {
        this.onEvent.accept(paramT, null);
        this.actual.onSuccess(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDoOnEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */