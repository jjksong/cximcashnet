package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeFilterSingle<T>
  extends Maybe<T>
{
  final Predicate<? super T> predicate;
  final SingleSource<T> source;
  
  public MaybeFilterSingle(SingleSource<T> paramSingleSource, Predicate<? super T> paramPredicate)
  {
    this.source = paramSingleSource;
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new FilterMaybeObserver(paramMaybeObserver, this.predicate));
  }
  
  static final class FilterMaybeObserver<T>
    implements SingleObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Predicate<? super T> predicate;
    
    FilterMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Predicate<? super T> paramPredicate)
    {
      this.actual = paramMaybeObserver;
      this.predicate = paramPredicate;
    }
    
    public void dispose()
    {
      Disposable localDisposable = this.d;
      this.d = DisposableHelper.DISPOSED;
      localDisposable.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        boolean bool = this.predicate.test(paramT);
        if (bool) {
          this.actual.onSuccess(paramT);
        } else {
          this.actual.onComplete();
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFilterSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */