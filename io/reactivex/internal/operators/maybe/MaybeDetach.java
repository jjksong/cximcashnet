package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeDetach<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  public MaybeDetach(MaybeSource<T> paramMaybeSource)
  {
    super(paramMaybeSource);
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DetachMaybeObserver(paramMaybeObserver));
  }
  
  static final class DetachMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    MaybeObserver<? super T> actual;
    Disposable d;
    
    DetachMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      this.actual = null;
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      MaybeObserver localMaybeObserver = this.actual;
      if (localMaybeObserver != null) {
        localMaybeObserver.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      MaybeObserver localMaybeObserver = this.actual;
      if (localMaybeObserver != null) {
        localMaybeObserver.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      MaybeObserver localMaybeObserver = this.actual;
      if (localMaybeObserver != null) {
        localMaybeObserver.onSuccess(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDetach.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */