package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;

@Experimental
public final class MaybeDoFinally<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Action onFinally;
  
  public MaybeDoFinally(MaybeSource<T> paramMaybeSource, Action paramAction)
  {
    super(paramMaybeSource);
    this.onFinally = paramAction;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DoFinallyObserver(paramMaybeObserver, this.onFinally));
  }
  
  static final class DoFinallyObserver<T>
    extends AtomicInteger
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Action onFinally;
    
    DoFinallyObserver(MaybeObserver<? super T> paramMaybeObserver, Action paramAction)
    {
      this.actual = paramMaybeObserver;
      this.onFinally = paramAction;
    }
    
    public void dispose()
    {
      this.d.dispose();
      runFinally();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      runFinally();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
      runFinally();
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDoFinally.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */