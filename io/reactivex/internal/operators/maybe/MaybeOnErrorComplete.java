package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;

public final class MaybeOnErrorComplete<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Predicate<? super Throwable> predicate;
  
  public MaybeOnErrorComplete(MaybeSource<T> paramMaybeSource, Predicate<? super Throwable> paramPredicate)
  {
    super(paramMaybeSource);
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new OnErrorCompleteMaybeObserver(paramMaybeObserver, this.predicate));
  }
  
  static final class OnErrorCompleteMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Predicate<? super Throwable> predicate;
    
    OnErrorCompleteMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Predicate<? super Throwable> paramPredicate)
    {
      this.actual = paramMaybeObserver;
      this.predicate = paramPredicate;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        boolean bool = this.predicate.test(paramThrowable);
        if (bool) {
          this.actual.onComplete();
        } else {
          this.actual.onError(paramThrowable);
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeOnErrorComplete.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */