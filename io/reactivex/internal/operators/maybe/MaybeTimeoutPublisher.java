package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeTimeoutPublisher<T, U>
  extends AbstractMaybeWithUpstream<T, T>
{
  final MaybeSource<? extends T> fallback;
  final Publisher<U> other;
  
  public MaybeTimeoutPublisher(MaybeSource<T> paramMaybeSource, Publisher<U> paramPublisher, MaybeSource<? extends T> paramMaybeSource1)
  {
    super(paramMaybeSource);
    this.other = paramPublisher;
    this.fallback = paramMaybeSource1;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    TimeoutMainMaybeObserver localTimeoutMainMaybeObserver = new TimeoutMainMaybeObserver(paramMaybeObserver, this.fallback);
    paramMaybeObserver.onSubscribe(localTimeoutMainMaybeObserver);
    this.other.subscribe(localTimeoutMainMaybeObserver.other);
    this.source.subscribe(localTimeoutMainMaybeObserver);
  }
  
  static final class TimeoutFallbackMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = 8663801314800248617L;
    final MaybeObserver<? super T> actual;
    
    TimeoutFallbackMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
  
  static final class TimeoutMainMaybeObserver<T, U>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = -5955289211445418871L;
    final MaybeObserver<? super T> actual;
    final MaybeSource<? extends T> fallback;
    final MaybeTimeoutPublisher.TimeoutOtherMaybeObserver<T, U> other;
    final MaybeTimeoutPublisher.TimeoutFallbackMaybeObserver<T> otherObserver;
    
    TimeoutMainMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, MaybeSource<? extends T> paramMaybeSource)
    {
      this.actual = paramMaybeObserver;
      this.other = new MaybeTimeoutPublisher.TimeoutOtherMaybeObserver(this);
      this.fallback = paramMaybeSource;
      if (paramMaybeSource != null) {
        paramMaybeObserver = new MaybeTimeoutPublisher.TimeoutFallbackMaybeObserver(paramMaybeObserver);
      } else {
        paramMaybeObserver = null;
      }
      this.otherObserver = paramMaybeObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      SubscriptionHelper.cancel(this.other);
      MaybeTimeoutPublisher.TimeoutFallbackMaybeObserver localTimeoutFallbackMaybeObserver = this.otherObserver;
      if (localTimeoutFallbackMaybeObserver != null) {
        DisposableHelper.dispose(localTimeoutFallbackMaybeObserver);
      }
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onSuccess(paramT);
      }
    }
    
    public void otherComplete()
    {
      if (DisposableHelper.dispose(this))
      {
        MaybeSource localMaybeSource = this.fallback;
        if (localMaybeSource == null) {
          this.actual.onError(new TimeoutException());
        } else {
          localMaybeSource.subscribe(this.otherObserver);
        }
      }
    }
    
    public void otherError(Throwable paramThrowable)
    {
      if (DisposableHelper.dispose(this)) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
  }
  
  static final class TimeoutOtherMaybeObserver<T, U>
    extends AtomicReference<Subscription>
    implements Subscriber<Object>
  {
    private static final long serialVersionUID = 8663801314800248617L;
    final MaybeTimeoutPublisher.TimeoutMainMaybeObserver<T, U> parent;
    
    TimeoutOtherMaybeObserver(MaybeTimeoutPublisher.TimeoutMainMaybeObserver<T, U> paramTimeoutMainMaybeObserver)
    {
      this.parent = paramTimeoutMainMaybeObserver;
    }
    
    public void onComplete()
    {
      this.parent.otherComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.otherError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      ((Subscription)get()).cancel();
      this.parent.otherComplete();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeTimeoutPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */