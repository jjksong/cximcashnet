package io.reactivex.internal.operators.maybe;

import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeConcatIterable<T>
  extends Flowable<T>
{
  final Iterable<? extends MaybeSource<? extends T>> sources;
  
  public MaybeConcatIterable(Iterable<? extends MaybeSource<? extends T>> paramIterable)
  {
    this.sources = paramIterable;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    try
    {
      Object localObject = (Iterator)ObjectHelper.requireNonNull(this.sources.iterator(), "The sources Iterable returned a null Iterator");
      localObject = new ConcatMaybeObserver(paramSubscriber, (Iterator)localObject);
      paramSubscriber.onSubscribe((Subscription)localObject);
      ((ConcatMaybeObserver)localObject).drain();
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class ConcatMaybeObserver<T>
    extends AtomicInteger
    implements MaybeObserver<T>, Subscription
  {
    private static final long serialVersionUID = 3520831347801429610L;
    final Subscriber<? super T> actual;
    final AtomicReference<Object> current;
    final SequentialDisposable disposables;
    long produced;
    final AtomicLong requested;
    final Iterator<? extends MaybeSource<? extends T>> sources;
    
    ConcatMaybeObserver(Subscriber<? super T> paramSubscriber, Iterator<? extends MaybeSource<? extends T>> paramIterator)
    {
      this.actual = paramSubscriber;
      this.sources = paramIterator;
      this.requested = new AtomicLong();
      this.disposables = new SequentialDisposable();
      this.current = new AtomicReference(NotificationLite.COMPLETE);
    }
    
    public void cancel()
    {
      this.disposables.dispose();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      AtomicReference localAtomicReference = this.current;
      Subscriber localSubscriber = this.actual;
      do
      {
        if (this.disposables.isDisposed())
        {
          localAtomicReference.lazySet(null);
          return;
        }
        Object localObject = localAtomicReference.get();
        if (localObject != null)
        {
          NotificationLite localNotificationLite = NotificationLite.COMPLETE;
          int i = 1;
          if (localObject != localNotificationLite)
          {
            long l = this.produced;
            if (l != this.requested.get())
            {
              this.produced = (l + 1L);
              localAtomicReference.lazySet(null);
              localSubscriber.onNext(localObject);
            }
            else
            {
              i = 0;
            }
          }
          else
          {
            localAtomicReference.lazySet(null);
          }
          if (i != 0) {
            try
            {
              boolean bool = this.sources.hasNext();
              if (bool) {
                try
                {
                  localObject = (MaybeSource)ObjectHelper.requireNonNull(this.sources.next(), "The source Iterator returned a null MaybeSource");
                  ((MaybeSource)localObject).subscribe(this);
                }
                catch (Throwable localThrowable1)
                {
                  Exceptions.throwIfFatal(localThrowable1);
                  localSubscriber.onError(localThrowable1);
                  return;
                }
              } else {
                localSubscriber.onComplete();
              }
            }
            catch (Throwable localThrowable2)
            {
              Exceptions.throwIfFatal(localThrowable2);
              localSubscriber.onError(localThrowable2);
              return;
            }
          }
        }
      } while (decrementAndGet() != 0);
    }
    
    public void onComplete()
    {
      this.current.lazySet(NotificationLite.COMPLETE);
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.disposables.replace(paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.current.lazySet(paramT);
      drain();
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeConcatIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */