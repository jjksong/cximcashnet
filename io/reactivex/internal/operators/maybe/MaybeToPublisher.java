package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeSource;
import io.reactivex.functions.Function;
import org.reactivestreams.Publisher;

public enum MaybeToPublisher
  implements Function<MaybeSource<Object>, Publisher<Object>>
{
  INSTANCE;
  
  private MaybeToPublisher() {}
  
  public static <T> Function<MaybeSource<T>, Publisher<T>> instance()
  {
    return INSTANCE;
  }
  
  public Publisher<Object> apply(MaybeSource<Object> paramMaybeSource)
    throws Exception
  {
    return new MaybeToFlowable(paramMaybeSource);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeToPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */