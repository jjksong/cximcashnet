package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.Arrays;
import java.util.Iterator;

public final class MaybeZipIterable<T, R>
  extends Maybe<R>
{
  final Iterable<? extends MaybeSource<? extends T>> sources;
  final Function<? super Object[], ? extends R> zipper;
  
  public MaybeZipIterable(Iterable<? extends MaybeSource<? extends T>> paramIterable, Function<? super Object[], ? extends R> paramFunction)
  {
    this.sources = paramIterable;
    this.zipper = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    Object localObject1 = new MaybeSource[8];
    try
    {
      Iterator localIterator = this.sources.iterator();
      int j = 0;
      int i = 0;
      while (localIterator.hasNext())
      {
        MaybeSource localMaybeSource = (MaybeSource)localIterator.next();
        localObject2 = localObject1;
        if (i == localObject1.length) {
          localObject2 = (MaybeSource[])Arrays.copyOf((Object[])localObject1, (i >> 2) + i);
        }
        localObject2[i] = localMaybeSource;
        i++;
        localObject1 = localObject2;
      }
      if (i == 0)
      {
        EmptyDisposable.complete(paramMaybeObserver);
        return;
      }
      if (i == 1)
      {
        localObject1[0].subscribe(new MaybeMap.MapMaybeObserver(paramMaybeObserver, new Function()
        {
          public R apply(T paramAnonymousT)
            throws Exception
          {
            return (R)MaybeZipIterable.this.zipper.apply(new Object[] { paramAnonymousT });
          }
        }));
        return;
      }
      Object localObject2 = new MaybeZipArray.ZipCoordinator(paramMaybeObserver, i, this.zipper);
      paramMaybeObserver.onSubscribe((Disposable)localObject2);
      while (j < i)
      {
        if (((MaybeZipArray.ZipCoordinator)localObject2).isDisposed()) {
          return;
        }
        localObject1[j].subscribe(localObject2.observers[j]);
        j++;
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramMaybeObserver);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeZipIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */