package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;

public final class MaybeOnErrorReturn<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Function<? super Throwable, ? extends T> valueSupplier;
  
  public MaybeOnErrorReturn(MaybeSource<T> paramMaybeSource, Function<? super Throwable, ? extends T> paramFunction)
  {
    super(paramMaybeSource);
    this.valueSupplier = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new OnErrorReturnMaybeObserver(paramMaybeObserver, this.valueSupplier));
  }
  
  static final class OnErrorReturnMaybeObserver<T>
    implements MaybeObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    final Function<? super Throwable, ? extends T> valueSupplier;
    
    OnErrorReturnMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Function<? super Throwable, ? extends T> paramFunction)
    {
      this.actual = paramMaybeObserver;
      this.valueSupplier = paramFunction;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        Object localObject = ObjectHelper.requireNonNull(this.valueSupplier.apply(paramThrowable), "The valueSupplier returned a null value");
        this.actual.onSuccess(localObject);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeOnErrorReturn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */