package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeZipArray<T, R>
  extends Maybe<R>
{
  final MaybeSource<? extends T>[] sources;
  final Function<? super Object[], ? extends R> zipper;
  
  public MaybeZipArray(MaybeSource<? extends T>[] paramArrayOfMaybeSource, Function<? super Object[], ? extends R> paramFunction)
  {
    this.sources = paramArrayOfMaybeSource;
    this.zipper = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    MaybeSource[] arrayOfMaybeSource = this.sources;
    int j = arrayOfMaybeSource.length;
    int i = 0;
    if (j == 1)
    {
      arrayOfMaybeSource[0].subscribe(new MaybeMap.MapMaybeObserver(paramMaybeObserver, new Function()
      {
        public R apply(T paramAnonymousT)
          throws Exception
        {
          return (R)MaybeZipArray.this.zipper.apply(new Object[] { paramAnonymousT });
        }
      }));
      return;
    }
    ZipCoordinator localZipCoordinator = new ZipCoordinator(paramMaybeObserver, j, this.zipper);
    paramMaybeObserver.onSubscribe(localZipCoordinator);
    while (i < j)
    {
      if (localZipCoordinator.isDisposed()) {
        return;
      }
      arrayOfMaybeSource[i].subscribe(localZipCoordinator.observers[i]);
      i++;
    }
  }
  
  static final class ZipCoordinator<T, R>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = -5556924161382950569L;
    final MaybeObserver<? super R> actual;
    final MaybeZipArray.ZipMaybeObserver<T>[] observers;
    final Object[] values;
    final Function<? super Object[], ? extends R> zipper;
    
    ZipCoordinator(MaybeObserver<? super R> paramMaybeObserver, int paramInt, Function<? super Object[], ? extends R> paramFunction)
    {
      super();
      this.actual = paramMaybeObserver;
      this.zipper = paramFunction;
      paramMaybeObserver = new MaybeZipArray.ZipMaybeObserver[paramInt];
      for (int i = 0; i < paramInt; i++) {
        paramMaybeObserver[i] = new MaybeZipArray.ZipMaybeObserver(this, i);
      }
      this.observers = paramMaybeObserver;
      this.values = new Object[paramInt];
    }
    
    public void dispose()
    {
      int i = 0;
      if (getAndSet(0) > 0)
      {
        MaybeZipArray.ZipMaybeObserver[] arrayOfZipMaybeObserver = this.observers;
        int j = arrayOfZipMaybeObserver.length;
        while (i < j)
        {
          arrayOfZipMaybeObserver[i].dispose();
          i++;
        }
      }
    }
    
    void disposeExcept(int paramInt)
    {
      MaybeZipArray.ZipMaybeObserver[] arrayOfZipMaybeObserver = this.observers;
      int k = arrayOfZipMaybeObserver.length;
      int j;
      for (int i = 0;; i++)
      {
        j = paramInt;
        if (i >= paramInt) {
          break;
        }
        arrayOfZipMaybeObserver[i].dispose();
      }
      for (;;)
      {
        j++;
        if (j >= k) {
          break;
        }
        arrayOfZipMaybeObserver[j].dispose();
      }
    }
    
    void innerComplete(int paramInt)
    {
      if (getAndSet(0) > 0)
      {
        disposeExcept(paramInt);
        this.actual.onComplete();
      }
    }
    
    void innerError(Throwable paramThrowable, int paramInt)
    {
      if (getAndSet(0) > 0)
      {
        disposeExcept(paramInt);
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void innerSuccess(T paramT, int paramInt)
    {
      this.values[paramInt] = paramT;
      if (decrementAndGet() == 0) {
        try
        {
          paramT = ObjectHelper.requireNonNull(this.zipper.apply(this.values), "The zipper returned a null value");
          this.actual.onSuccess(paramT);
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.actual.onError(paramT);
          return;
        }
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() <= 0) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
  
  static final class ZipMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = 3323743579927613702L;
    final int index;
    final MaybeZipArray.ZipCoordinator<T, ?> parent;
    
    ZipMaybeObserver(MaybeZipArray.ZipCoordinator<T, ?> paramZipCoordinator, int paramInt)
    {
      this.parent = paramZipCoordinator;
      this.index = paramInt;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public void onComplete()
    {
      this.parent.innerComplete(this.index);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(paramThrowable, this.index);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.parent.innerSuccess(paramT, this.index);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeZipArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */