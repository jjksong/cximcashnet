package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeDelay<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final long delay;
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public MaybeDelay(MaybeSource<T> paramMaybeSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramMaybeSource);
    this.delay = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new DelayMaybeObserver(paramMaybeObserver, this.delay, this.unit, this.scheduler));
  }
  
  static final class DelayMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable, Runnable
  {
    private static final long serialVersionUID = 5566860102500855068L;
    final MaybeObserver<? super T> actual;
    final long delay;
    Throwable error;
    final Scheduler scheduler;
    final TimeUnit unit;
    T value;
    
    DelayMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.actual = paramMaybeObserver;
      this.delay = paramLong;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      schedule();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      schedule();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.value = paramT;
      schedule();
    }
    
    public void run()
    {
      Object localObject = this.error;
      if (localObject != null)
      {
        this.actual.onError((Throwable)localObject);
      }
      else
      {
        localObject = this.value;
        if (localObject != null) {
          this.actual.onSuccess(localObject);
        } else {
          this.actual.onComplete();
        }
      }
    }
    
    void schedule()
    {
      DisposableHelper.replace(this, this.scheduler.scheduleDirect(this, this.delay, this.unit));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDelay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */