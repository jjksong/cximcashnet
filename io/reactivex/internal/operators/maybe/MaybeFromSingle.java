package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.HasUpstreamSingleSource;

public final class MaybeFromSingle<T>
  extends Maybe<T>
  implements HasUpstreamSingleSource<T>
{
  final SingleSource<T> source;
  
  public MaybeFromSingle(SingleSource<T> paramSingleSource)
  {
    this.source = paramSingleSource;
  }
  
  public SingleSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new FromSingleObserver(paramMaybeObserver));
  }
  
  static final class FromSingleObserver<T>
    implements SingleObserver<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    Disposable d;
    
    FromSingleObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFromSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */