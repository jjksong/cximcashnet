package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeTakeUntilPublisher<T, U>
  extends AbstractMaybeWithUpstream<T, T>
{
  final Publisher<U> other;
  
  public MaybeTakeUntilPublisher(MaybeSource<T> paramMaybeSource, Publisher<U> paramPublisher)
  {
    super(paramMaybeSource);
    this.other = paramPublisher;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    TakeUntilMainMaybeObserver localTakeUntilMainMaybeObserver = new TakeUntilMainMaybeObserver(paramMaybeObserver);
    paramMaybeObserver.onSubscribe(localTakeUntilMainMaybeObserver);
    this.other.subscribe(localTakeUntilMainMaybeObserver.other);
    this.source.subscribe(localTakeUntilMainMaybeObserver);
  }
  
  static final class TakeUntilMainMaybeObserver<T, U>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = -2187421758664251153L;
    final MaybeObserver<? super T> actual;
    final TakeUntilOtherMaybeObserver<U> other;
    
    TakeUntilMainMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
      this.other = new TakeUntilOtherMaybeObserver(this);
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      SubscriptionHelper.cancel(this.other);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      SubscriptionHelper.cancel(this.other);
      if (getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED) {
        this.actual.onSuccess(paramT);
      }
    }
    
    void otherComplete()
    {
      if (DisposableHelper.dispose(this)) {
        this.actual.onComplete();
      }
    }
    
    void otherError(Throwable paramThrowable)
    {
      if (DisposableHelper.dispose(this)) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    static final class TakeUntilOtherMaybeObserver<U>
      extends AtomicReference<Subscription>
      implements Subscriber<U>
    {
      private static final long serialVersionUID = -1266041316834525931L;
      final MaybeTakeUntilPublisher.TakeUntilMainMaybeObserver<?, U> parent;
      
      TakeUntilOtherMaybeObserver(MaybeTakeUntilPublisher.TakeUntilMainMaybeObserver<?, U> paramTakeUntilMainMaybeObserver)
      {
        this.parent = paramTakeUntilMainMaybeObserver;
      }
      
      public void onComplete()
      {
        this.parent.otherComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.parent.otherError(paramThrowable);
      }
      
      public void onNext(Object paramObject)
      {
        this.parent.otherComplete();
      }
      
      public void onSubscribe(Subscription paramSubscription)
      {
        if (SubscriptionHelper.setOnce(this, paramSubscription)) {
          paramSubscription.request(Long.MAX_VALUE);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeTakeUntilPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */