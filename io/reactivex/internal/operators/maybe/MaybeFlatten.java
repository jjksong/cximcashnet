package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeFlatten<T, R>
  extends AbstractMaybeWithUpstream<T, R>
{
  final Function<? super T, ? extends MaybeSource<? extends R>> mapper;
  
  public MaybeFlatten(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction)
  {
    super(paramMaybeSource);
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    this.source.subscribe(new FlatMapMaybeObserver(paramMaybeObserver, this.mapper));
  }
  
  static final class FlatMapMaybeObserver<T, R>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 4375739915521278546L;
    final MaybeObserver<? super R> actual;
    Disposable d;
    final Function<? super T, ? extends MaybeSource<? extends R>> mapper;
    
    FlatMapMaybeObserver(MaybeObserver<? super R> paramMaybeObserver, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction)
    {
      this.actual = paramMaybeObserver;
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = (MaybeSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null MaybeSource");
        if (!isDisposed()) {
          paramT.subscribe(new InnerObserver());
        }
        return;
      }
      catch (Exception paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    final class InnerObserver
      implements MaybeObserver<R>
    {
      InnerObserver() {}
      
      public void onComplete()
      {
        MaybeFlatten.FlatMapMaybeObserver.this.actual.onComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        MaybeFlatten.FlatMapMaybeObserver.this.actual.onError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(MaybeFlatten.FlatMapMaybeObserver.this, paramDisposable);
      }
      
      public void onSuccess(R paramR)
      {
        MaybeFlatten.FlatMapMaybeObserver.this.actual.onSuccess(paramR);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatten.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */