package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeFlatMapBiSelector<T, U, R>
  extends AbstractMaybeWithUpstream<T, R>
{
  final Function<? super T, ? extends MaybeSource<? extends U>> mapper;
  final BiFunction<? super T, ? super U, ? extends R> resultSelector;
  
  public MaybeFlatMapBiSelector(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends MaybeSource<? extends U>> paramFunction, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
  {
    super(paramMaybeSource);
    this.mapper = paramFunction;
    this.resultSelector = paramBiFunction;
  }
  
  protected void subscribeActual(MaybeObserver<? super R> paramMaybeObserver)
  {
    this.source.subscribe(new FlatMapBiMainObserver(paramMaybeObserver, this.mapper, this.resultSelector));
  }
  
  static final class FlatMapBiMainObserver<T, U, R>
    implements MaybeObserver<T>, Disposable
  {
    final InnerObserver<T, U, R> inner;
    final Function<? super T, ? extends MaybeSource<? extends U>> mapper;
    
    FlatMapBiMainObserver(MaybeObserver<? super R> paramMaybeObserver, Function<? super T, ? extends MaybeSource<? extends U>> paramFunction, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
    {
      this.inner = new InnerObserver(paramMaybeObserver, paramBiFunction);
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.inner);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.inner.get());
    }
    
    public void onComplete()
    {
      this.inner.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.inner.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this.inner, paramDisposable)) {
        this.inner.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        MaybeSource localMaybeSource = (MaybeSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null MaybeSource");
        if (DisposableHelper.replace(this.inner, null))
        {
          InnerObserver localInnerObserver = this.inner;
          localInnerObserver.value = paramT;
          localMaybeSource.subscribe(localInnerObserver);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.inner.actual.onError(paramT);
      }
    }
    
    static final class InnerObserver<T, U, R>
      extends AtomicReference<Disposable>
      implements MaybeObserver<U>
    {
      private static final long serialVersionUID = -2897979525538174559L;
      final MaybeObserver<? super R> actual;
      final BiFunction<? super T, ? super U, ? extends R> resultSelector;
      T value;
      
      InnerObserver(MaybeObserver<? super R> paramMaybeObserver, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
      {
        this.actual = paramMaybeObserver;
        this.resultSelector = paramBiFunction;
      }
      
      public void onComplete()
      {
        this.actual.onComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.actual.onError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this, paramDisposable);
      }
      
      public void onSuccess(U paramU)
      {
        Object localObject = this.value;
        this.value = null;
        try
        {
          paramU = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject, paramU), "The resultSelector returned a null value");
          this.actual.onSuccess(paramU);
          return;
        }
        catch (Throwable paramU)
        {
          Exceptions.throwIfFatal(paramU);
          this.actual.onError(paramU);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapBiSelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */