package io.reactivex.internal.operators.maybe;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeDelayWithCompletable<T>
  extends Maybe<T>
{
  final CompletableSource other;
  final MaybeSource<T> source;
  
  public MaybeDelayWithCompletable(MaybeSource<T> paramMaybeSource, CompletableSource paramCompletableSource)
  {
    this.source = paramMaybeSource;
    this.other = paramCompletableSource;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.other.subscribe(new OtherObserver(paramMaybeObserver, this.source));
  }
  
  static final class DelayWithMainObserver<T>
    implements MaybeObserver<T>
  {
    final MaybeObserver<? super T> actual;
    final AtomicReference<Disposable> parent;
    
    DelayWithMainObserver(AtomicReference<Disposable> paramAtomicReference, MaybeObserver<? super T> paramMaybeObserver)
    {
      this.parent = paramAtomicReference;
      this.actual = paramMaybeObserver;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.replace(this.parent, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
  
  static final class OtherObserver<T>
    extends AtomicReference<Disposable>
    implements CompletableObserver, Disposable
  {
    private static final long serialVersionUID = 703409937383992161L;
    final MaybeObserver<? super T> actual;
    final MaybeSource<T> source;
    
    OtherObserver(MaybeObserver<? super T> paramMaybeObserver, MaybeSource<T> paramMaybeSource)
    {
      this.actual = paramMaybeObserver;
      this.source = paramMaybeSource;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.source.subscribe(new MaybeDelayWithCompletable.DelayWithMainObserver(this, this.actual));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeDelayWithCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */