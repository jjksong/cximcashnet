package io.reactivex.internal.operators.maybe;

import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeMergeArray<T>
  extends Flowable<T>
{
  final MaybeSource<? extends T>[] sources;
  
  public MaybeMergeArray(MaybeSource<? extends T>[] paramArrayOfMaybeSource)
  {
    this.sources = paramArrayOfMaybeSource;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    MaybeSource[] arrayOfMaybeSource = this.sources;
    int i = arrayOfMaybeSource.length;
    if (i <= bufferSize()) {
      localObject = new MpscFillOnceSimpleQueue(i);
    } else {
      localObject = new ClqSimpleQueue();
    }
    Object localObject = new MergeMaybeObserver(paramSubscriber, i, (SimpleQueueWithConsumerIndex)localObject);
    paramSubscriber.onSubscribe((Subscription)localObject);
    paramSubscriber = ((MergeMaybeObserver)localObject).error;
    int j = arrayOfMaybeSource.length;
    i = 0;
    while (i < j)
    {
      MaybeSource localMaybeSource = arrayOfMaybeSource[i];
      if ((!((MergeMaybeObserver)localObject).isCancelled()) && (paramSubscriber.get() == null))
      {
        localMaybeSource.subscribe((MaybeObserver)localObject);
        i++;
      }
      else {}
    }
  }
  
  static final class ClqSimpleQueue<T>
    extends ConcurrentLinkedQueue<T>
    implements MaybeMergeArray.SimpleQueueWithConsumerIndex<T>
  {
    private static final long serialVersionUID = -4025173261791142821L;
    int consumerIndex;
    final AtomicInteger producerIndex = new AtomicInteger();
    
    public int consumerIndex()
    {
      return this.consumerIndex;
    }
    
    public void drop()
    {
      poll();
    }
    
    public boolean offer(T paramT)
    {
      this.producerIndex.getAndIncrement();
      return super.offer(paramT);
    }
    
    public boolean offer(T paramT1, T paramT2)
    {
      throw new UnsupportedOperationException();
    }
    
    public T poll()
    {
      Object localObject = super.poll();
      if (localObject != null) {
        this.consumerIndex += 1;
      }
      return (T)localObject;
    }
    
    public int producerIndex()
    {
      return this.producerIndex.get();
    }
  }
  
  static final class MergeMaybeObserver<T>
    extends BasicIntQueueSubscription<T>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = -660395290758764731L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    long consumed;
    final AtomicThrowable error;
    boolean outputFused;
    final MaybeMergeArray.SimpleQueueWithConsumerIndex<Object> queue;
    final AtomicLong requested;
    final CompositeDisposable set;
    final int sourceCount;
    
    MergeMaybeObserver(Subscriber<? super T> paramSubscriber, int paramInt, MaybeMergeArray.SimpleQueueWithConsumerIndex<Object> paramSimpleQueueWithConsumerIndex)
    {
      this.actual = paramSubscriber;
      this.sourceCount = paramInt;
      this.set = new CompositeDisposable();
      this.requested = new AtomicLong();
      this.error = new AtomicThrowable();
      this.queue = paramSimpleQueueWithConsumerIndex;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.set.dispose();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      if (this.outputFused) {
        drainFused();
      } else {
        drainNormal();
      }
    }
    
    void drainFused()
    {
      Subscriber localSubscriber = this.actual;
      MaybeMergeArray.SimpleQueueWithConsumerIndex localSimpleQueueWithConsumerIndex = this.queue;
      int i = 1;
      int j;
      do
      {
        if (this.cancelled)
        {
          localSimpleQueueWithConsumerIndex.clear();
          return;
        }
        Throwable localThrowable = (Throwable)this.error.get();
        if (localThrowable != null)
        {
          localSimpleQueueWithConsumerIndex.clear();
          localSubscriber.onError(localThrowable);
          return;
        }
        if (localSimpleQueueWithConsumerIndex.producerIndex() == this.sourceCount) {
          j = 1;
        } else {
          j = 0;
        }
        if (!localSimpleQueueWithConsumerIndex.isEmpty()) {
          localSubscriber.onNext(null);
        }
        if (j != 0)
        {
          localSubscriber.onComplete();
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void drainNormal()
    {
      Subscriber localSubscriber = this.actual;
      MaybeMergeArray.SimpleQueueWithConsumerIndex localSimpleQueueWithConsumerIndex = this.queue;
      long l1 = this.consumed;
      int i = 1;
      int j;
      do
      {
        long l2 = this.requested.get();
        while (l1 != l2)
        {
          if (this.cancelled)
          {
            localSimpleQueueWithConsumerIndex.clear();
            return;
          }
          if ((Throwable)this.error.get() != null)
          {
            localSimpleQueueWithConsumerIndex.clear();
            localSubscriber.onError(this.error.terminate());
            return;
          }
          if (localSimpleQueueWithConsumerIndex.consumerIndex() == this.sourceCount)
          {
            localSubscriber.onComplete();
            return;
          }
          Object localObject = localSimpleQueueWithConsumerIndex.poll();
          if (localObject == null) {
            break;
          }
          if (localObject != NotificationLite.COMPLETE)
          {
            localSubscriber.onNext(localObject);
            l1 += 1L;
          }
        }
        if (l1 == l2)
        {
          if ((Throwable)this.error.get() != null)
          {
            localSimpleQueueWithConsumerIndex.clear();
            localSubscriber.onError(this.error.terminate());
            return;
          }
          while (localSimpleQueueWithConsumerIndex.peek() == NotificationLite.COMPLETE) {
            localSimpleQueueWithConsumerIndex.drop();
          }
          if (localSimpleQueueWithConsumerIndex.consumerIndex() == this.sourceCount)
          {
            localSubscriber.onComplete();
            return;
          }
        }
        this.consumed = l1;
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    boolean isCancelled()
    {
      return this.cancelled;
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public void onComplete()
    {
      this.queue.offer(NotificationLite.COMPLETE);
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable))
      {
        this.set.dispose();
        this.queue.offer(NotificationLite.COMPLETE);
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.set.add(paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.queue.offer(paramT);
      drain();
    }
    
    public T poll()
      throws Exception
    {
      Object localObject;
      do
      {
        localObject = this.queue.poll();
      } while (localObject == NotificationLite.COMPLETE);
      return (T)localObject;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
  
  static final class MpscFillOnceSimpleQueue<T>
    extends AtomicReferenceArray<T>
    implements MaybeMergeArray.SimpleQueueWithConsumerIndex<T>
  {
    private static final long serialVersionUID = -7969063454040569579L;
    int consumerIndex;
    final AtomicInteger producerIndex = new AtomicInteger();
    
    MpscFillOnceSimpleQueue(int paramInt)
    {
      super();
    }
    
    public void clear()
    {
      while ((poll() != null) && (!isEmpty())) {}
    }
    
    public int consumerIndex()
    {
      return this.consumerIndex;
    }
    
    public void drop()
    {
      int i = this.consumerIndex;
      lazySet(i, null);
      this.consumerIndex = (i + 1);
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.consumerIndex == producerIndex()) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public boolean offer(T paramT)
    {
      ObjectHelper.requireNonNull(paramT, "value is null");
      int i = this.producerIndex.getAndIncrement();
      if (i < length())
      {
        lazySet(i, paramT);
        return true;
      }
      return false;
    }
    
    public boolean offer(T paramT1, T paramT2)
    {
      throw new UnsupportedOperationException();
    }
    
    public T peek()
    {
      int i = this.consumerIndex;
      if (i == length()) {
        return null;
      }
      return (T)get(i);
    }
    
    public T poll()
    {
      int i = this.consumerIndex;
      if (i == length()) {
        return null;
      }
      AtomicInteger localAtomicInteger = this.producerIndex;
      do
      {
        Object localObject = get(i);
        if (localObject != null)
        {
          this.consumerIndex = (i + 1);
          lazySet(i, null);
          return (T)localObject;
        }
      } while (localAtomicInteger.get() != i);
      return null;
    }
    
    public int producerIndex()
    {
      return this.producerIndex.get();
    }
  }
  
  static abstract interface SimpleQueueWithConsumerIndex<T>
    extends SimpleQueue<T>
  {
    public abstract int consumerIndex();
    
    public abstract void drop();
    
    public abstract T peek();
    
    public abstract T poll();
    
    public abstract int producerIndex();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeMergeArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */