package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiPredicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeEqualSingle<T>
  extends Single<Boolean>
{
  final BiPredicate<? super T, ? super T> isEqual;
  final MaybeSource<? extends T> source1;
  final MaybeSource<? extends T> source2;
  
  public MaybeEqualSingle(MaybeSource<? extends T> paramMaybeSource1, MaybeSource<? extends T> paramMaybeSource2, BiPredicate<? super T, ? super T> paramBiPredicate)
  {
    this.source1 = paramMaybeSource1;
    this.source2 = paramMaybeSource2;
    this.isEqual = paramBiPredicate;
  }
  
  protected void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    EqualCoordinator localEqualCoordinator = new EqualCoordinator(paramSingleObserver, this.isEqual);
    paramSingleObserver.onSubscribe(localEqualCoordinator);
    localEqualCoordinator.subscribe(this.source1, this.source2);
  }
  
  static final class EqualCoordinator<T>
    extends AtomicInteger
    implements Disposable
  {
    final SingleObserver<? super Boolean> actual;
    final BiPredicate<? super T, ? super T> isEqual;
    final MaybeEqualSingle.EqualObserver<T> observer1;
    final MaybeEqualSingle.EqualObserver<T> observer2;
    
    EqualCoordinator(SingleObserver<? super Boolean> paramSingleObserver, BiPredicate<? super T, ? super T> paramBiPredicate)
    {
      super();
      this.actual = paramSingleObserver;
      this.isEqual = paramBiPredicate;
      this.observer1 = new MaybeEqualSingle.EqualObserver(this);
      this.observer2 = new MaybeEqualSingle.EqualObserver(this);
    }
    
    public void dispose()
    {
      this.observer1.dispose();
      this.observer2.dispose();
    }
    
    void done()
    {
      if (decrementAndGet() == 0)
      {
        Object localObject2 = this.observer1.value;
        Object localObject1 = this.observer2.value;
        boolean bool;
        if ((localObject2 != null) && (localObject1 != null))
        {
          try
          {
            bool = this.isEqual.test(localObject2, localObject1);
            this.actual.onSuccess(Boolean.valueOf(bool));
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.actual.onError(localThrowable);
            return;
          }
        }
        else
        {
          SingleObserver localSingleObserver = this.actual;
          if ((localObject2 == null) && (localThrowable == null)) {
            bool = true;
          } else {
            bool = false;
          }
          localSingleObserver.onSuccess(Boolean.valueOf(bool));
        }
      }
    }
    
    void error(MaybeEqualSingle.EqualObserver<T> paramEqualObserver, Throwable paramThrowable)
    {
      if (getAndSet(0) > 0)
      {
        MaybeEqualSingle.EqualObserver localEqualObserver = this.observer1;
        if (paramEqualObserver == localEqualObserver) {
          this.observer2.dispose();
        } else {
          localEqualObserver.dispose();
        }
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.observer1.get());
    }
    
    void subscribe(MaybeSource<? extends T> paramMaybeSource1, MaybeSource<? extends T> paramMaybeSource2)
    {
      paramMaybeSource1.subscribe(this.observer1);
      paramMaybeSource2.subscribe(this.observer2);
    }
  }
  
  static final class EqualObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = -3031974433025990931L;
    final MaybeEqualSingle.EqualCoordinator<T> parent;
    Object value;
    
    EqualObserver(MaybeEqualSingle.EqualCoordinator<T> paramEqualCoordinator)
    {
      this.parent = paramEqualCoordinator;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public void onComplete()
    {
      this.parent.done();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.error(this, paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.value = paramT;
      this.parent.done();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeEqualSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */