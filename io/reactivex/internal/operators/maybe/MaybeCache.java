package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeCache<T>
  extends Maybe<T>
  implements MaybeObserver<T>
{
  static final CacheDisposable[] EMPTY = new CacheDisposable[0];
  static final CacheDisposable[] TERMINATED = new CacheDisposable[0];
  Throwable error;
  final AtomicReference<CacheDisposable<T>[]> observers;
  final AtomicReference<MaybeSource<T>> source;
  T value;
  
  public MaybeCache(MaybeSource<T> paramMaybeSource)
  {
    this.source = new AtomicReference(paramMaybeSource);
    this.observers = new AtomicReference(EMPTY);
  }
  
  boolean add(CacheDisposable<T> paramCacheDisposable)
  {
    CacheDisposable[] arrayOfCacheDisposable1;
    CacheDisposable[] arrayOfCacheDisposable2;
    do
    {
      arrayOfCacheDisposable1 = (CacheDisposable[])this.observers.get();
      if (arrayOfCacheDisposable1 == TERMINATED) {
        return false;
      }
      int i = arrayOfCacheDisposable1.length;
      arrayOfCacheDisposable2 = new CacheDisposable[i + 1];
      System.arraycopy(arrayOfCacheDisposable1, 0, arrayOfCacheDisposable2, 0, i);
      arrayOfCacheDisposable2[i] = paramCacheDisposable;
    } while (!this.observers.compareAndSet(arrayOfCacheDisposable1, arrayOfCacheDisposable2));
    return true;
  }
  
  public void onComplete()
  {
    for (CacheDisposable localCacheDisposable : (CacheDisposable[])this.observers.getAndSet(TERMINATED)) {
      if (!localCacheDisposable.isDisposed()) {
        localCacheDisposable.actual.onComplete();
      }
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    this.error = paramThrowable;
    for (CacheDisposable localCacheDisposable : (CacheDisposable[])this.observers.getAndSet(TERMINATED)) {
      if (!localCacheDisposable.isDisposed()) {
        localCacheDisposable.actual.onError(paramThrowable);
      }
    }
  }
  
  public void onSubscribe(Disposable paramDisposable) {}
  
  public void onSuccess(T paramT)
  {
    this.value = paramT;
    for (CacheDisposable localCacheDisposable : (CacheDisposable[])this.observers.getAndSet(TERMINATED)) {
      if (!localCacheDisposable.isDisposed()) {
        localCacheDisposable.actual.onSuccess(paramT);
      }
    }
  }
  
  void remove(CacheDisposable<T> paramCacheDisposable)
  {
    CacheDisposable[] arrayOfCacheDisposable2;
    CacheDisposable[] arrayOfCacheDisposable1;
    do
    {
      arrayOfCacheDisposable2 = (CacheDisposable[])this.observers.get();
      int m = arrayOfCacheDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfCacheDisposable2[i] == paramCacheDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfCacheDisposable1 = EMPTY;
      }
      else
      {
        arrayOfCacheDisposable1 = new CacheDisposable[m - 1];
        System.arraycopy(arrayOfCacheDisposable2, 0, arrayOfCacheDisposable1, 0, j);
        System.arraycopy(arrayOfCacheDisposable2, j + 1, arrayOfCacheDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfCacheDisposable2, arrayOfCacheDisposable1));
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Object localObject = new CacheDisposable(paramMaybeObserver, this);
    paramMaybeObserver.onSubscribe((Disposable)localObject);
    if (add((CacheDisposable)localObject))
    {
      if (((CacheDisposable)localObject).isDisposed())
      {
        remove((CacheDisposable)localObject);
        return;
      }
      paramMaybeObserver = (MaybeSource)this.source.getAndSet(null);
      if (paramMaybeObserver != null) {
        paramMaybeObserver.subscribe(this);
      }
      return;
    }
    if (!((CacheDisposable)localObject).isDisposed())
    {
      localObject = this.error;
      if (localObject != null)
      {
        paramMaybeObserver.onError((Throwable)localObject);
      }
      else
      {
        localObject = this.value;
        if (localObject != null) {
          paramMaybeObserver.onSuccess(localObject);
        } else {
          paramMaybeObserver.onComplete();
        }
      }
    }
  }
  
  static final class CacheDisposable<T>
    extends AtomicReference<MaybeCache<T>>
    implements Disposable
  {
    private static final long serialVersionUID = -5791853038359966195L;
    final MaybeObserver<? super T> actual;
    
    CacheDisposable(MaybeObserver<? super T> paramMaybeObserver, MaybeCache<T> paramMaybeCache)
    {
      super();
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      MaybeCache localMaybeCache = (MaybeCache)getAndSet(null);
      if (localMaybeCache != null) {
        localMaybeCache.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */