package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeOnErrorNext<T>
  extends AbstractMaybeWithUpstream<T, T>
{
  final boolean allowFatal;
  final Function<? super Throwable, ? extends MaybeSource<? extends T>> resumeFunction;
  
  public MaybeOnErrorNext(MaybeSource<T> paramMaybeSource, Function<? super Throwable, ? extends MaybeSource<? extends T>> paramFunction, boolean paramBoolean)
  {
    super(paramMaybeSource);
    this.resumeFunction = paramFunction;
    this.allowFatal = paramBoolean;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new OnErrorNextMaybeObserver(paramMaybeObserver, this.resumeFunction, this.allowFatal));
  }
  
  static final class OnErrorNextMaybeObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = 2026620218879969836L;
    final MaybeObserver<? super T> actual;
    final boolean allowFatal;
    final Function<? super Throwable, ? extends MaybeSource<? extends T>> resumeFunction;
    
    OnErrorNextMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, Function<? super Throwable, ? extends MaybeSource<? extends T>> paramFunction, boolean paramBoolean)
    {
      this.actual = paramMaybeObserver;
      this.resumeFunction = paramFunction;
      this.allowFatal = paramBoolean;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.allowFatal) && (!(paramThrowable instanceof Exception)))
      {
        this.actual.onError(paramThrowable);
        return;
      }
      try
      {
        MaybeSource localMaybeSource = (MaybeSource)ObjectHelper.requireNonNull(this.resumeFunction.apply(paramThrowable), "The resumeFunction returned a null MaybeSource");
        DisposableHelper.replace(this, null);
        localMaybeSource.subscribe(new NextMaybeObserver(this.actual, this));
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
    
    static final class NextMaybeObserver<T>
      implements MaybeObserver<T>
    {
      final MaybeObserver<? super T> actual;
      final AtomicReference<Disposable> d;
      
      NextMaybeObserver(MaybeObserver<? super T> paramMaybeObserver, AtomicReference<Disposable> paramAtomicReference)
      {
        this.actual = paramMaybeObserver;
        this.d = paramAtomicReference;
      }
      
      public void onComplete()
      {
        this.actual.onComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.actual.onError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this.d, paramDisposable);
      }
      
      public void onSuccess(T paramT)
      {
        this.actual.onSuccess(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeOnErrorNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */