package io.reactivex.internal.operators.maybe;

import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;

public final class MaybeFlatMapIterableFlowable<T, R>
  extends Flowable<R>
{
  final Function<? super T, ? extends Iterable<? extends R>> mapper;
  final MaybeSource<T> source;
  
  public MaybeFlatMapIterableFlowable(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
  {
    this.source = paramMaybeSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    this.source.subscribe(new FlatMapIterableObserver(paramSubscriber, this.mapper));
  }
  
  static final class FlatMapIterableObserver<T, R>
    extends BasicIntQueueSubscription<R>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = -8938804753851907758L;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    Disposable d;
    volatile Iterator<? extends R> it;
    final Function<? super T, ? extends Iterable<? extends R>> mapper;
    boolean outputFused;
    final AtomicLong requested;
    
    FlatMapIterableObserver(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      this.cancelled = true;
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public void clear()
    {
      this.it = null;
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      Iterator localIterator1 = this.it;
      if ((this.outputFused) && (localIterator1 != null))
      {
        localSubscriber.onNext(null);
        localSubscriber.onComplete();
        return;
      }
      int i = 1;
      for (;;)
      {
        if (localIterator1 != null)
        {
          long l2 = this.requested.get();
          if (l2 == Long.MAX_VALUE)
          {
            fastPath(localSubscriber, localIterator1);
            return;
          }
          long l1 = 0L;
          while (l1 != l2)
          {
            if (this.cancelled) {
              return;
            }
            try
            {
              Object localObject = ObjectHelper.requireNonNull(localIterator1.next(), "The iterator returned a null value");
              localSubscriber.onNext(localObject);
              if (this.cancelled) {
                return;
              }
              l1 += 1L;
              try
              {
                boolean bool = localIterator1.hasNext();
                if (bool) {
                  continue;
                }
                localSubscriber.onComplete();
                return;
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                localSubscriber.onError(localThrowable1);
                return;
              }
              if (l1 == 0L) {
                break label205;
              }
            }
            catch (Throwable localThrowable2)
            {
              Exceptions.throwIfFatal(localThrowable2);
              localSubscriber.onError(localThrowable2);
              return;
            }
          }
          BackpressureHelper.produced(this.requested, l1);
        }
        label205:
        int j = addAndGet(-i);
        if (j == 0) {
          return;
        }
        i = j;
        if (localThrowable2 == null)
        {
          Iterator localIterator2 = this.it;
          i = j;
        }
      }
    }
    
    void fastPath(Subscriber<? super R> paramSubscriber, Iterator<? extends R> paramIterator)
    {
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        try
        {
          Object localObject = paramIterator.next();
          paramSubscriber.onNext(localObject);
          if (this.cancelled) {
            return;
          }
          try
          {
            boolean bool = paramIterator.hasNext();
            if (bool) {
              continue;
            }
            paramSubscriber.onComplete();
            return;
          }
          catch (Throwable paramIterator)
          {
            Exceptions.throwIfFatal(paramIterator);
            paramSubscriber.onError(paramIterator);
            return;
          }
          return;
        }
        catch (Throwable paramIterator)
        {
          Exceptions.throwIfFatal(paramIterator);
          paramSubscriber.onError(paramIterator);
        }
      }
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.it == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = ((Iterable)this.mapper.apply(paramT)).iterator();
        boolean bool = paramT.hasNext();
        if (!bool)
        {
          this.actual.onComplete();
          return;
        }
        this.it = paramT;
        drain();
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    public R poll()
      throws Exception
    {
      Iterator localIterator = this.it;
      if (localIterator != null)
      {
        Object localObject = ObjectHelper.requireNonNull(localIterator.next(), "The iterator returned a null value");
        if (!localIterator.hasNext()) {
          this.it = null;
        }
        return (R)localObject;
      }
      return null;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapIterableFlowable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */