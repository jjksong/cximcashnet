package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicQueueDisposable;
import java.util.Iterator;

public final class MaybeFlatMapIterableObservable<T, R>
  extends Observable<R>
{
  final Function<? super T, ? extends Iterable<? extends R>> mapper;
  final MaybeSource<T> source;
  
  public MaybeFlatMapIterableObservable(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
  {
    this.source = paramMaybeSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    this.source.subscribe(new FlatMapIterableObserver(paramObserver, this.mapper));
  }
  
  static final class FlatMapIterableObserver<T, R>
    extends BasicQueueDisposable<R>
    implements MaybeObserver<T>
  {
    final Observer<? super R> actual;
    volatile boolean cancelled;
    Disposable d;
    volatile Iterator<? extends R> it;
    final Function<? super T, ? extends Iterable<? extends R>> mapper;
    boolean outputFused;
    
    FlatMapIterableObserver(Observer<? super R> paramObserver, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
    }
    
    public void clear()
    {
      this.it = null;
    }
    
    public void dispose()
    {
      this.cancelled = true;
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.it == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    /* Error */
    public void onSuccess(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 31	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:actual	Lio/reactivex/Observer;
      //   4: astore_3
      //   5: aload_0
      //   6: getfield 33	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:mapper	Lio/reactivex/functions/Function;
      //   9: aload_1
      //   10: invokeinterface 84 2 0
      //   15: checkcast 86	java/lang/Iterable
      //   18: invokeinterface 90 1 0
      //   23: astore_1
      //   24: aload_1
      //   25: invokeinterface 95 1 0
      //   30: istore_2
      //   31: iload_2
      //   32: ifne +10 -> 42
      //   35: aload_3
      //   36: invokeinterface 62 1 0
      //   41: return
      //   42: aload_0
      //   43: aload_1
      //   44: putfield 39	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:it	Ljava/util/Iterator;
      //   47: aload_0
      //   48: getfield 97	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:outputFused	Z
      //   51: ifeq +21 -> 72
      //   54: aload_1
      //   55: ifnull +17 -> 72
      //   58: aload_3
      //   59: aconst_null
      //   60: invokeinterface 100 2 0
      //   65: aload_3
      //   66: invokeinterface 62 1 0
      //   71: return
      //   72: aload_0
      //   73: getfield 42	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:cancelled	Z
      //   76: ifeq +4 -> 80
      //   79: return
      //   80: aload_1
      //   81: invokeinterface 104 1 0
      //   86: astore 4
      //   88: aload_3
      //   89: aload 4
      //   91: invokeinterface 100 2 0
      //   96: aload_0
      //   97: getfield 42	io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable$FlatMapIterableObserver:cancelled	Z
      //   100: ifeq +4 -> 104
      //   103: return
      //   104: aload_1
      //   105: invokeinterface 95 1 0
      //   110: istore_2
      //   111: iload_2
      //   112: ifne -40 -> 72
      //   115: aload_3
      //   116: invokeinterface 62 1 0
      //   121: return
      //   122: astore_1
      //   123: aload_1
      //   124: invokestatic 109	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   127: aload_3
      //   128: aload_1
      //   129: invokeinterface 66 2 0
      //   134: return
      //   135: astore_1
      //   136: aload_1
      //   137: invokestatic 109	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   140: aload_3
      //   141: aload_1
      //   142: invokeinterface 66 2 0
      //   147: return
      //   148: astore_1
      //   149: aload_1
      //   150: invokestatic 109	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   153: aload_3
      //   154: aload_1
      //   155: invokeinterface 66 2 0
      //   160: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	161	0	this	FlatMapIterableObserver
      //   0	161	1	paramT	T
      //   30	82	2	bool	boolean
      //   4	150	3	localObserver	Observer
      //   86	4	4	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   104	111	122	java/lang/Throwable
      //   80	88	135	java/lang/Throwable
      //   5	31	148	java/lang/Throwable
    }
    
    public R poll()
      throws Exception
    {
      Iterator localIterator = this.it;
      if (localIterator != null)
      {
        Object localObject = ObjectHelper.requireNonNull(localIterator.next(), "The iterator returned a null value");
        if (!localIterator.hasNext()) {
          this.it = null;
        }
        return (R)localObject;
      }
      return null;
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapIterableObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */