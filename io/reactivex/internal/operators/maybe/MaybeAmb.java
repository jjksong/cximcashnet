package io.reactivex.internal.operators.maybe;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class MaybeAmb<T>
  extends Maybe<T>
{
  private final MaybeSource<? extends T>[] sources;
  private final Iterable<? extends MaybeSource<? extends T>> sourcesIterable;
  
  public MaybeAmb(MaybeSource<? extends T>[] paramArrayOfMaybeSource, Iterable<? extends MaybeSource<? extends T>> paramIterable)
  {
    this.sources = paramArrayOfMaybeSource;
    this.sourcesIterable = paramIterable;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Object localObject3 = this.sources;
    int k = 0;
    int j;
    if (localObject3 == null)
    {
      Object localObject1 = new MaybeSource[8];
      try
      {
        localObject4 = this.sourcesIterable.iterator();
        i = 0;
        for (;;)
        {
          localObject3 = localObject1;
          j = i;
          if (!((Iterator)localObject4).hasNext()) {
            break;
          }
          MaybeSource localMaybeSource = (MaybeSource)((Iterator)localObject4).next();
          if (localMaybeSource == null)
          {
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("One of the sources is null");
            EmptyDisposable.error((Throwable)localObject1, paramMaybeObserver);
            return;
          }
          localObject3 = localObject1;
          if (i == localObject1.length)
          {
            localObject3 = new MaybeSource[(i >> 2) + i];
            System.arraycopy(localObject1, 0, localObject3, 0, i);
          }
          localObject3[i] = localMaybeSource;
          i++;
          localObject1 = localObject3;
        }
        j = localObject3.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptyDisposable.error(localThrowable, paramMaybeObserver);
        return;
      }
    }
    Object localObject4 = new AmbMaybeObserver(paramMaybeObserver);
    paramMaybeObserver.onSubscribe((Disposable)localObject4);
    for (int i = k; i < j; i++)
    {
      Object localObject2 = localObject3[i];
      if (((AmbMaybeObserver)localObject4).isDisposed()) {
        return;
      }
      if (localObject2 == null)
      {
        ((AmbMaybeObserver)localObject4).onError(new NullPointerException("One of the MaybeSources is null"));
        return;
      }
      ((MaybeSource)localObject2).subscribe((MaybeObserver)localObject4);
    }
    if (j == 0) {
      paramMaybeObserver.onComplete();
    }
  }
  
  static final class AmbMaybeObserver<T>
    extends AtomicBoolean
    implements MaybeObserver<T>, Disposable
  {
    private static final long serialVersionUID = -7044685185359438206L;
    final MaybeObserver<? super T> actual;
    final CompositeDisposable set;
    
    AmbMaybeObserver(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
      this.set = new CompositeDisposable();
    }
    
    public void dispose()
    {
      if (compareAndSet(false, true)) {
        this.set.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return get();
    }
    
    public void onComplete()
    {
      if (compareAndSet(false, true))
      {
        this.set.dispose();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (compareAndSet(false, true))
      {
        this.set.dispose();
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.set.add(paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      if (compareAndSet(false, true))
      {
        this.set.dispose();
        this.actual.onSuccess(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeAmb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */