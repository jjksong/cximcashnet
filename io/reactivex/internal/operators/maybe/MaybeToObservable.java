package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.HasUpstreamMaybeSource;
import io.reactivex.internal.observers.DeferredScalarDisposable;

public final class MaybeToObservable<T>
  extends Observable<T>
  implements HasUpstreamMaybeSource<T>
{
  final MaybeSource<T> source;
  
  public MaybeToObservable(MaybeSource<T> paramMaybeSource)
  {
    this.source = paramMaybeSource;
  }
  
  public MaybeSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new MaybeToFlowableSubscriber(paramObserver));
  }
  
  static final class MaybeToFlowableSubscriber<T>
    extends DeferredScalarDisposable<T>
    implements MaybeObserver<T>
  {
    private static final long serialVersionUID = 7603343402964826922L;
    Disposable d;
    
    MaybeToFlowableSubscriber(Observer<? super T> paramObserver)
    {
      super();
    }
    
    public void dispose()
    {
      super.dispose();
      this.d.dispose();
    }
    
    public void onComplete()
    {
      complete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      error(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      complete(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeToObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */