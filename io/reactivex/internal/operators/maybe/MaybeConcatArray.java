package io.reactivex.internal.operators.maybe;

import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class MaybeConcatArray<T>
  extends Flowable<T>
{
  final MaybeSource<? extends T>[] sources;
  
  public MaybeConcatArray(MaybeSource<? extends T>[] paramArrayOfMaybeSource)
  {
    this.sources = paramArrayOfMaybeSource;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    ConcatMaybeObserver localConcatMaybeObserver = new ConcatMaybeObserver(paramSubscriber, this.sources);
    paramSubscriber.onSubscribe(localConcatMaybeObserver);
    localConcatMaybeObserver.drain();
  }
  
  static final class ConcatMaybeObserver<T>
    extends AtomicInteger
    implements MaybeObserver<T>, Subscription
  {
    private static final long serialVersionUID = 3520831347801429610L;
    final Subscriber<? super T> actual;
    final AtomicReference<Object> current;
    final SequentialDisposable disposables;
    int index;
    long produced;
    final AtomicLong requested;
    final MaybeSource<? extends T>[] sources;
    
    ConcatMaybeObserver(Subscriber<? super T> paramSubscriber, MaybeSource<? extends T>[] paramArrayOfMaybeSource)
    {
      this.actual = paramSubscriber;
      this.sources = paramArrayOfMaybeSource;
      this.requested = new AtomicLong();
      this.disposables = new SequentialDisposable();
      this.current = new AtomicReference(NotificationLite.COMPLETE);
    }
    
    public void cancel()
    {
      this.disposables.dispose();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      AtomicReference localAtomicReference = this.current;
      Subscriber localSubscriber = this.actual;
      do
      {
        if (this.disposables.isDisposed())
        {
          localAtomicReference.lazySet(null);
          return;
        }
        Object localObject2 = localAtomicReference.get();
        if (localObject2 != null)
        {
          Object localObject1 = NotificationLite.COMPLETE;
          int i = 1;
          if (localObject2 != localObject1)
          {
            long l = this.produced;
            if (l != this.requested.get())
            {
              this.produced = (l + 1L);
              localAtomicReference.lazySet(null);
              localSubscriber.onNext(localObject2);
            }
            else
            {
              i = 0;
            }
          }
          else
          {
            localAtomicReference.lazySet(null);
          }
          if (i != 0)
          {
            i = this.index;
            localObject1 = this.sources;
            if (i == localObject1.length)
            {
              localSubscriber.onComplete();
              return;
            }
            this.index = (i + 1);
            localObject1[i].subscribe(this);
          }
        }
      } while (decrementAndGet() != 0);
    }
    
    public void onComplete()
    {
      this.current.lazySet(NotificationLite.COMPLETE);
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.disposables.replace(paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.current.lazySet(paramT);
      drain();
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeConcatArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */