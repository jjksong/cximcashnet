package io.reactivex.internal.operators.maybe;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.HasUpstreamMaybeSource;
import java.util.NoSuchElementException;

public final class MaybeToSingle<T>
  extends Single<T>
  implements HasUpstreamMaybeSource<T>
{
  final T defaultValue;
  final MaybeSource<T> source;
  
  public MaybeToSingle(MaybeSource<T> paramMaybeSource, T paramT)
  {
    this.source = paramMaybeSource;
    this.defaultValue = paramT;
  }
  
  public MaybeSource<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new ToSingleMaybeSubscriber(paramSingleObserver, this.defaultValue));
  }
  
  static final class ToSingleMaybeSubscriber<T>
    implements MaybeObserver<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    Disposable d;
    final T defaultValue;
    
    ToSingleMaybeSubscriber(SingleObserver<? super T> paramSingleObserver, T paramT)
    {
      this.actual = paramSingleObserver;
      this.defaultValue = paramT;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      Object localObject = this.defaultValue;
      if (localObject != null) {
        this.actual.onSuccess(localObject);
      } else {
        this.actual.onError(new NoSuchElementException("The MaybeSource is empty"));
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeToSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */