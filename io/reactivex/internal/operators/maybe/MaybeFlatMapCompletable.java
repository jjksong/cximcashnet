package io.reactivex.internal.operators.maybe;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class MaybeFlatMapCompletable<T>
  extends Completable
{
  final Function<? super T, ? extends CompletableSource> mapper;
  final MaybeSource<T> source;
  
  public MaybeFlatMapCompletable(MaybeSource<T> paramMaybeSource, Function<? super T, ? extends CompletableSource> paramFunction)
  {
    this.source = paramMaybeSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    FlatMapCompletableObserver localFlatMapCompletableObserver = new FlatMapCompletableObserver(paramCompletableObserver, this.mapper);
    paramCompletableObserver.onSubscribe(localFlatMapCompletableObserver);
    this.source.subscribe(localFlatMapCompletableObserver);
  }
  
  static final class FlatMapCompletableObserver<T>
    extends AtomicReference<Disposable>
    implements MaybeObserver<T>, CompletableObserver, Disposable
  {
    private static final long serialVersionUID = -2177128922851101253L;
    final CompletableObserver actual;
    final Function<? super T, ? extends CompletableSource> mapper;
    
    FlatMapCompletableObserver(CompletableObserver paramCompletableObserver, Function<? super T, ? extends CompletableSource> paramFunction)
    {
      this.actual = paramCompletableObserver;
      this.mapper = paramFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.replace(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      try
      {
        paramT = (CompletableSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null CompletableSource");
        if (!isDisposed()) {
          paramT.subscribe(this);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/maybe/MaybeFlatMapCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */