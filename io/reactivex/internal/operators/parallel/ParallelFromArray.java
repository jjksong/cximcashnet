package io.reactivex.internal.operators.parallel;

import io.reactivex.parallel.ParallelFlowable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class ParallelFromArray<T>
  extends ParallelFlowable<T>
{
  final Publisher<T>[] sources;
  
  public ParallelFromArray(Publisher<T>[] paramArrayOfPublisher)
  {
    this.sources = paramArrayOfPublisher;
  }
  
  public int parallelism()
  {
    return this.sources.length;
  }
  
  public void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    for (int i = 0; i < j; i++) {
      this.sources[i].subscribe(paramArrayOfSubscriber[i]);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelFromArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */