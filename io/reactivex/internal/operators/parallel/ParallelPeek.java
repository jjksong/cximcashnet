package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.LongConsumer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelPeek<T>
  extends ParallelFlowable<T>
{
  final Consumer<? super T> onAfterNext;
  final Action onAfterTerminated;
  final Action onCancel;
  final Action onComplete;
  final Consumer<? super Throwable> onError;
  final Consumer<? super T> onNext;
  final LongConsumer onRequest;
  final Consumer<? super Subscription> onSubscribe;
  final ParallelFlowable<T> source;
  
  public ParallelPeek(ParallelFlowable<T> paramParallelFlowable, Consumer<? super T> paramConsumer1, Consumer<? super T> paramConsumer2, Consumer<? super Throwable> paramConsumer, Action paramAction1, Action paramAction2, Consumer<? super Subscription> paramConsumer3, LongConsumer paramLongConsumer, Action paramAction3)
  {
    this.source = paramParallelFlowable;
    this.onNext = ((Consumer)ObjectHelper.requireNonNull(paramConsumer1, "onNext is null"));
    this.onAfterNext = ((Consumer)ObjectHelper.requireNonNull(paramConsumer2, "onAfterNext is null"));
    this.onError = ((Consumer)ObjectHelper.requireNonNull(paramConsumer, "onError is null"));
    this.onComplete = ((Action)ObjectHelper.requireNonNull(paramAction1, "onComplete is null"));
    this.onAfterTerminated = ((Action)ObjectHelper.requireNonNull(paramAction2, "onAfterTerminated is null"));
    this.onSubscribe = ((Consumer)ObjectHelper.requireNonNull(paramConsumer3, "onSubscribe is null"));
    this.onRequest = ((LongConsumer)ObjectHelper.requireNonNull(paramLongConsumer, "onRequest is null"));
    this.onCancel = ((Action)ObjectHelper.requireNonNull(paramAction3, "onCancel is null"));
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  public void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[j];
    for (int i = 0; i < j; i++) {
      arrayOfSubscriber[i] = new ParallelPeekSubscriber(paramArrayOfSubscriber[i], this);
    }
    this.source.subscribe(arrayOfSubscriber);
  }
  
  static final class ParallelPeekSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super T> actual;
    boolean done;
    final ParallelPeek<T> parent;
    Subscription s;
    
    ParallelPeekSubscriber(Subscriber<? super T> paramSubscriber, ParallelPeek<T> paramParallelPeek)
    {
      this.actual = paramSubscriber;
      this.parent = paramParallelPeek;
    }
    
    public void cancel()
    {
      try
      {
        this.parent.onCancel.run();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        try
        {
          this.parent.onComplete.run();
          this.actual.onComplete();
          try
          {
            this.parent.onAfterTerminated.run();
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            RxJavaPlugins.onError(localThrowable1);
          }
          return;
        }
        catch (Throwable localThrowable2)
        {
          Exceptions.throwIfFatal(localThrowable2);
          this.actual.onError(localThrowable2);
          return;
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      try
      {
        this.parent.onError.accept(paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        paramThrowable = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
      }
      this.actual.onError(paramThrowable);
      try
      {
        this.parent.onAfterTerminated.run();
      }
      catch (Throwable paramThrowable)
      {
        Exceptions.throwIfFatal(paramThrowable);
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.done) {
        try
        {
          this.parent.onNext.accept(paramT);
          this.actual.onNext(paramT);
          try
          {
            this.parent.onAfterNext.accept(paramT);
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            onError(paramT);
            return;
          }
          return;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          onError(paramT);
          return;
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        try
        {
          this.parent.onSubscribe.accept(paramSubscription);
          this.actual.onSubscribe(this);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramSubscription.cancel();
          this.actual.onSubscribe(EmptySubscription.INSTANCE);
          onError(localThrowable);
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      try
      {
        this.parent.onRequest.accept(paramLong);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelPeek.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */