package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelMap<T, R>
  extends ParallelFlowable<R>
{
  final Function<? super T, ? extends R> mapper;
  final ParallelFlowable<T> source;
  
  public ParallelMap(ParallelFlowable<T> paramParallelFlowable, Function<? super T, ? extends R> paramFunction)
  {
    this.source = paramParallelFlowable;
    this.mapper = paramFunction;
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  public void subscribe(Subscriber<? super R>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[j];
    for (int i = 0; i < j; i++)
    {
      Subscriber<? super R> localSubscriber = paramArrayOfSubscriber[i];
      if ((localSubscriber instanceof ConditionalSubscriber)) {
        arrayOfSubscriber[i] = new ParallelMapConditionalSubscriber((ConditionalSubscriber)localSubscriber, this.mapper);
      } else {
        arrayOfSubscriber[i] = new ParallelMapSubscriber(localSubscriber, this.mapper);
      }
    }
    this.source.subscribe(arrayOfSubscriber);
  }
  
  static final class ParallelMapConditionalSubscriber<T, R>
    implements ConditionalSubscriber<T>, Subscription
  {
    final ConditionalSubscriber<? super R> actual;
    boolean done;
    final Function<? super T, ? extends R> mapper;
    Subscription s;
    
    ParallelMapConditionalSubscriber(ConditionalSubscriber<? super R> paramConditionalSubscriber, Function<? super T, ? extends R> paramFunction)
    {
      this.actual = paramConditionalSubscriber;
      this.mapper = paramFunction;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null value");
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null value");
        return this.actual.tryOnNext(paramT);
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        onError(paramT);
      }
      return false;
    }
  }
  
  static final class ParallelMapSubscriber<T, R>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super R> actual;
    boolean done;
    final Function<? super T, ? extends R> mapper;
    Subscription s;
    
    ParallelMapSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends R> paramFunction)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null value");
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */