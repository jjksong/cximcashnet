package io.reactivex.internal.operators.parallel;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelReduceFull<T>
  extends Flowable<T>
{
  final BiFunction<T, T, T> reducer;
  final ParallelFlowable<? extends T> source;
  
  public ParallelReduceFull(ParallelFlowable<? extends T> paramParallelFlowable, BiFunction<T, T, T> paramBiFunction)
  {
    this.source = paramParallelFlowable;
    this.reducer = paramBiFunction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    ParallelReduceFullMainSubscriber localParallelReduceFullMainSubscriber = new ParallelReduceFullMainSubscriber(paramSubscriber, this.source.parallelism(), this.reducer);
    paramSubscriber.onSubscribe(localParallelReduceFullMainSubscriber);
    this.source.subscribe(localParallelReduceFullMainSubscriber.subscribers);
  }
  
  static final class ParallelReduceFullInnerSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -7954444275102466525L;
    boolean done;
    final ParallelReduceFull.ParallelReduceFullMainSubscriber<T> parent;
    final BiFunction<T, T, T> reducer;
    T value;
    
    ParallelReduceFullInnerSubscriber(ParallelReduceFull.ParallelReduceFullMainSubscriber<T> paramParallelReduceFullMainSubscriber, BiFunction<T, T, T> paramBiFunction)
    {
      this.parent = paramParallelReduceFullMainSubscriber;
      this.reducer = paramBiFunction;
    }
    
    void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.parent.innerComplete(this.value);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        Object localObject = this.value;
        if (localObject == null) {
          this.value = paramT;
        } else {
          try
          {
            paramT = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
            this.value = paramT;
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            ((Subscription)get()).cancel();
            onError(paramT);
            return;
          }
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
  
  static final class ParallelReduceFullMainSubscriber<T>
    extends DeferredScalarSubscription<T>
  {
    private static final long serialVersionUID = -5370107872170712765L;
    final AtomicReference<ParallelReduceFull.SlotPair<T>> current = new AtomicReference();
    final AtomicReference<Throwable> error = new AtomicReference();
    final BiFunction<T, T, T> reducer;
    final AtomicInteger remaining = new AtomicInteger();
    final ParallelReduceFull.ParallelReduceFullInnerSubscriber<T>[] subscribers;
    
    ParallelReduceFullMainSubscriber(Subscriber<? super T> paramSubscriber, int paramInt, BiFunction<T, T, T> paramBiFunction)
    {
      super();
      paramSubscriber = new ParallelReduceFull.ParallelReduceFullInnerSubscriber[paramInt];
      for (int i = 0; i < paramInt; i++) {
        paramSubscriber[i] = new ParallelReduceFull.ParallelReduceFullInnerSubscriber(this, paramBiFunction);
      }
      this.subscribers = paramSubscriber;
      this.reducer = paramBiFunction;
      this.remaining.lazySet(paramInt);
    }
    
    ParallelReduceFull.SlotPair<T> addValue(T paramT)
    {
      ParallelReduceFull.SlotPair localSlotPair1;
      int i;
      for (;;)
      {
        ParallelReduceFull.SlotPair localSlotPair2 = (ParallelReduceFull.SlotPair)this.current.get();
        localSlotPair1 = localSlotPair2;
        if (localSlotPair2 == null)
        {
          localSlotPair2 = new ParallelReduceFull.SlotPair();
          localSlotPair1 = localSlotPair2;
          if (!this.current.compareAndSet(null, localSlotPair2)) {}
        }
        else
        {
          i = localSlotPair1.tryAcquireSlot();
          if (i >= 0) {
            break;
          }
          this.current.compareAndSet(localSlotPair1, null);
        }
      }
      if (i == 0) {
        localSlotPair1.first = paramT;
      } else {
        localSlotPair1.second = paramT;
      }
      if (localSlotPair1.releaseSlot())
      {
        this.current.compareAndSet(localSlotPair1, null);
        return localSlotPair1;
      }
      return null;
    }
    
    public void cancel()
    {
      ParallelReduceFull.ParallelReduceFullInnerSubscriber[] arrayOfParallelReduceFullInnerSubscriber = this.subscribers;
      int j = arrayOfParallelReduceFullInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfParallelReduceFullInnerSubscriber[i].cancel();
      }
    }
    
    void innerComplete(T paramT)
    {
      if (paramT != null) {
        for (;;)
        {
          paramT = addValue(paramT);
          if (paramT != null) {
            try
            {
              paramT = ObjectHelper.requireNonNull(this.reducer.apply(paramT.first, paramT.second), "The reducer returned a null value");
            }
            catch (Throwable paramT)
            {
              Exceptions.throwIfFatal(paramT);
              innerError(paramT);
              return;
            }
          }
        }
      }
      if (this.remaining.decrementAndGet() == 0)
      {
        paramT = (ParallelReduceFull.SlotPair)this.current.get();
        this.current.lazySet(null);
        if (paramT != null) {
          complete(paramT.first);
        } else {
          this.actual.onComplete();
        }
      }
    }
    
    void innerError(Throwable paramThrowable)
    {
      if (this.error.compareAndSet(null, paramThrowable))
      {
        cancel();
        this.actual.onError(paramThrowable);
      }
      else if (paramThrowable != this.error.get())
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
  }
  
  static final class SlotPair<T>
    extends AtomicInteger
  {
    private static final long serialVersionUID = 473971317683868662L;
    T first;
    final AtomicInteger releaseIndex = new AtomicInteger();
    T second;
    
    boolean releaseSlot()
    {
      boolean bool;
      if (this.releaseIndex.incrementAndGet() == 2) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    int tryAcquireSlot()
    {
      int i;
      do
      {
        i = get();
        if (i >= 2) {
          return -1;
        }
      } while (!compareAndSet(i, i + 1));
      return i;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelReduceFull.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */