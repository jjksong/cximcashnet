package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscribers.DeferredScalarSubscriber;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelReduce<T, R>
  extends ParallelFlowable<R>
{
  final Callable<R> initialSupplier;
  final BiFunction<R, ? super T, R> reducer;
  final ParallelFlowable<? extends T> source;
  
  public ParallelReduce(ParallelFlowable<? extends T> paramParallelFlowable, Callable<R> paramCallable, BiFunction<R, ? super T, R> paramBiFunction)
  {
    this.source = paramParallelFlowable;
    this.initialSupplier = paramCallable;
    this.reducer = paramBiFunction;
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  void reportError(Subscriber<?>[] paramArrayOfSubscriber, Throwable paramThrowable)
  {
    int j = paramArrayOfSubscriber.length;
    for (int i = 0; i < j; i++) {
      EmptySubscription.error(paramThrowable, paramArrayOfSubscriber[i]);
    }
  }
  
  public void subscribe(Subscriber<? super R>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[j];
    int i = 0;
    while (i < j) {
      try
      {
        Object localObject = ObjectHelper.requireNonNull(this.initialSupplier.call(), "The initialSupplier returned a null value");
        arrayOfSubscriber[i] = new ParallelReduceSubscriber(paramArrayOfSubscriber[i], localObject, this.reducer);
        i++;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        reportError(paramArrayOfSubscriber, localThrowable);
        return;
      }
    }
    this.source.subscribe(localThrowable);
  }
  
  static final class ParallelReduceSubscriber<T, R>
    extends DeferredScalarSubscriber<T, R>
  {
    private static final long serialVersionUID = 8200530050639449080L;
    R accumulator;
    boolean done;
    final BiFunction<R, ? super T, R> reducer;
    
    ParallelReduceSubscriber(Subscriber<? super R> paramSubscriber, R paramR, BiFunction<R, ? super T, R> paramBiFunction)
    {
      super();
      this.accumulator = paramR;
      this.reducer = paramBiFunction;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        Object localObject = this.accumulator;
        this.accumulator = null;
        complete(localObject);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.accumulator = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.done) {
        try
        {
          paramT = ObjectHelper.requireNonNull(this.reducer.apply(this.accumulator, paramT), "The reducer returned a null value");
          this.accumulator = paramT;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return;
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelReduce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */