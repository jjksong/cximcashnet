package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscribers.DeferredScalarSubscriber;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelCollect<T, C>
  extends ParallelFlowable<C>
{
  final BiConsumer<? super C, ? super T> collector;
  final Callable<? extends C> initialCollection;
  final ParallelFlowable<? extends T> source;
  
  public ParallelCollect(ParallelFlowable<? extends T> paramParallelFlowable, Callable<? extends C> paramCallable, BiConsumer<? super C, ? super T> paramBiConsumer)
  {
    this.source = paramParallelFlowable;
    this.initialCollection = paramCallable;
    this.collector = paramBiConsumer;
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  void reportError(Subscriber<?>[] paramArrayOfSubscriber, Throwable paramThrowable)
  {
    int j = paramArrayOfSubscriber.length;
    for (int i = 0; i < j; i++) {
      EmptySubscription.error(paramThrowable, paramArrayOfSubscriber[i]);
    }
  }
  
  public void subscribe(Subscriber<? super C>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[j];
    int i = 0;
    while (i < j) {
      try
      {
        Object localObject = ObjectHelper.requireNonNull(this.initialCollection.call(), "The initialSupplier returned a null value");
        arrayOfSubscriber[i] = new ParallelCollectSubscriber(paramArrayOfSubscriber[i], localObject, this.collector);
        i++;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        reportError(paramArrayOfSubscriber, localThrowable);
        return;
      }
    }
    this.source.subscribe(localThrowable);
  }
  
  static final class ParallelCollectSubscriber<T, C>
    extends DeferredScalarSubscriber<T, C>
  {
    private static final long serialVersionUID = -4767392946044436228L;
    C collection;
    final BiConsumer<? super C, ? super T> collector;
    boolean done;
    
    ParallelCollectSubscriber(Subscriber<? super C> paramSubscriber, C paramC, BiConsumer<? super C, ? super T> paramBiConsumer)
    {
      super();
      this.collection = paramC;
      this.collector = paramBiConsumer;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject = this.collection;
      this.collection = null;
      complete(localObject);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.collection = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        this.collector.accept(this.collection, paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelCollect.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */