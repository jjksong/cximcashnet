package io.reactivex.internal.operators.parallel;

import io.reactivex.Flowable;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelJoin<T>
  extends Flowable<T>
{
  final int prefetch;
  final ParallelFlowable<? extends T> source;
  
  public ParallelJoin(ParallelFlowable<? extends T> paramParallelFlowable, int paramInt)
  {
    this.source = paramParallelFlowable;
    this.prefetch = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    JoinSubscription localJoinSubscription = new JoinSubscription(paramSubscriber, this.source.parallelism(), this.prefetch);
    paramSubscriber.onSubscribe(localJoinSubscription);
    this.source.subscribe(localJoinSubscription.subscribers);
  }
  
  static final class JoinInnerSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = 8410034718427740355L;
    volatile boolean done;
    final int limit;
    final ParallelJoin.JoinSubscription<T> parent;
    final int prefetch;
    long produced;
    volatile SimplePlainQueue<T> queue;
    
    JoinInnerSubscriber(ParallelJoin.JoinSubscription<T> paramJoinSubscription, int paramInt)
    {
      this.parent = paramJoinSubscription;
      this.prefetch = paramInt;
      this.limit = (paramInt - (paramInt >> 2));
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    SimplePlainQueue<T> getQueue()
    {
      SimplePlainQueue localSimplePlainQueue = this.queue;
      Object localObject = localSimplePlainQueue;
      if (localSimplePlainQueue == null)
      {
        localObject = new SpscArrayQueue(this.prefetch);
        this.queue = ((SimplePlainQueue)localObject);
      }
      return (SimplePlainQueue<T>)localObject;
    }
    
    public void onComplete()
    {
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.parent.onNext(this, paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void request(long paramLong)
    {
      paramLong = this.produced + paramLong;
      if (paramLong >= this.limit)
      {
        this.produced = 0L;
        ((Subscription)get()).request(paramLong);
      }
      else
      {
        this.produced = paramLong;
      }
    }
    
    public void requestOne()
    {
      long l = this.produced + 1L;
      if (l == this.limit)
      {
        this.produced = 0L;
        ((Subscription)get()).request(l);
      }
      else
      {
        this.produced = l;
      }
    }
  }
  
  static final class JoinSubscription<T>
    extends AtomicInteger
    implements Subscription
  {
    private static final long serialVersionUID = 3100232009247827843L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final AtomicInteger done = new AtomicInteger();
    final AtomicReference<Throwable> error = new AtomicReference();
    final AtomicLong requested = new AtomicLong();
    final ParallelJoin.JoinInnerSubscriber<T>[] subscribers;
    
    JoinSubscription(Subscriber<? super T> paramSubscriber, int paramInt1, int paramInt2)
    {
      this.actual = paramSubscriber;
      paramSubscriber = new ParallelJoin.JoinInnerSubscriber[paramInt1];
      for (int i = 0; i < paramInt1; i++) {
        paramSubscriber[i] = new ParallelJoin.JoinInnerSubscriber(this, paramInt2);
      }
      this.subscribers = paramSubscriber;
      this.done.lazySet(paramInt1);
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        cancelAll();
        if (getAndIncrement() == 0) {
          cleanup();
        }
      }
    }
    
    void cancelAll()
    {
      ParallelJoin.JoinInnerSubscriber[] arrayOfJoinInnerSubscriber = this.subscribers;
      int j = arrayOfJoinInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfJoinInnerSubscriber[i].cancel();
      }
    }
    
    void cleanup()
    {
      ParallelJoin.JoinInnerSubscriber[] arrayOfJoinInnerSubscriber = this.subscribers;
      int j = arrayOfJoinInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfJoinInnerSubscriber[i].queue = null;
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      drainLoop();
    }
    
    void drainLoop()
    {
      ParallelJoin.JoinInnerSubscriber[] arrayOfJoinInnerSubscriber = this.subscribers;
      int i1 = arrayOfJoinInnerSubscriber.length;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      for (;;)
      {
        long l3 = this.requested.get();
        long l1 = 0L;
        long l2;
        Object localObject1;
        int k;
        for (;;)
        {
          l2 = l1;
          if (l1 == l3) {
            break;
          }
          if (this.cancelled)
          {
            cleanup();
            return;
          }
          localObject1 = (Throwable)this.error.get();
          if (localObject1 != null)
          {
            cleanup();
            localSubscriber.onError((Throwable)localObject1);
            return;
          }
          if (this.done.get() == 0) {
            j = 1;
          } else {
            j = 0;
          }
          int n = 0;
          k = 1;
          while (n < i1)
          {
            localObject1 = arrayOfJoinInnerSubscriber[n];
            Object localObject2 = ((ParallelJoin.JoinInnerSubscriber)localObject1).queue;
            int m = k;
            l2 = l1;
            if (localObject2 != null)
            {
              localObject2 = ((SimplePlainQueue)localObject2).poll();
              m = k;
              l2 = l1;
              if (localObject2 != null)
              {
                localSubscriber.onNext(localObject2);
                ((ParallelJoin.JoinInnerSubscriber)localObject1).requestOne();
                l1 = 1L + l1;
                if (l1 == l3)
                {
                  l2 = l1;
                  break label247;
                }
                m = 0;
                l2 = l1;
              }
            }
            n++;
            k = m;
            l1 = l2;
          }
          if ((j != 0) && (k != 0))
          {
            localSubscriber.onComplete();
            return;
          }
          if (k != 0)
          {
            l2 = l1;
            break;
          }
        }
        label247:
        if (l2 == l3)
        {
          if (this.cancelled)
          {
            cleanup();
            return;
          }
          localObject1 = (Throwable)this.error.get();
          if (localObject1 != null)
          {
            cleanup();
            localSubscriber.onError((Throwable)localObject1);
            return;
          }
          if (this.done.get() == 0) {
            j = 1;
          } else {
            j = 0;
          }
          for (k = 0; k < i1; k++)
          {
            localObject1 = arrayOfJoinInnerSubscriber[k].queue;
            if ((localObject1 != null) && (!((SimpleQueue)localObject1).isEmpty()))
            {
              k = 0;
              break label360;
            }
          }
          k = 1;
          label360:
          if ((j != 0) && (k != 0))
          {
            localSubscriber.onComplete();
            return;
          }
        }
        if ((l2 != 0L) && (l3 != Long.MAX_VALUE)) {
          this.requested.addAndGet(-l2);
        }
        int j = get();
        if (j == i)
        {
          i = addAndGet(-i);
          if (i == 0) {
            return;
          }
        }
        else
        {
          i = j;
        }
      }
    }
    
    void onComplete()
    {
      this.done.decrementAndGet();
      drain();
    }
    
    void onError(Throwable paramThrowable)
    {
      if (this.error.compareAndSet(null, paramThrowable))
      {
        cancelAll();
        drain();
      }
      else if (paramThrowable != this.error.get())
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void onNext(ParallelJoin.JoinInnerSubscriber<T> paramJoinInnerSubscriber, T paramT)
    {
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        if (this.requested.get() != 0L)
        {
          this.actual.onNext(paramT);
          if (this.requested.get() != Long.MAX_VALUE) {
            this.requested.decrementAndGet();
          }
          paramJoinInnerSubscriber.request(1L);
        }
        else if (!paramJoinInnerSubscriber.getQueue().offer(paramT))
        {
          cancelAll();
          paramJoinInnerSubscriber = new MissingBackpressureException("Queue full?!");
          if (this.error.compareAndSet(null, paramJoinInnerSubscriber)) {
            this.actual.onError(paramJoinInnerSubscriber);
          } else {
            RxJavaPlugins.onError(paramJoinInnerSubscriber);
          }
          return;
        }
        if (decrementAndGet() != 0) {}
      }
      else
      {
        if (!paramJoinInnerSubscriber.getQueue().offer(paramT))
        {
          cancelAll();
          onError(new MissingBackpressureException("Queue full?!"));
          return;
        }
        if (getAndIncrement() != 0) {
          return;
        }
      }
      drainLoop();
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */