package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.parallel.ParallelFlowable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLongArray;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelFromPublisher<T>
  extends ParallelFlowable<T>
{
  final int parallelism;
  final int prefetch;
  final Publisher<? extends T> source;
  
  public ParallelFromPublisher(Publisher<? extends T> paramPublisher, int paramInt1, int paramInt2)
  {
    this.source = paramPublisher;
    this.parallelism = paramInt1;
    this.prefetch = paramInt2;
  }
  
  public int parallelism()
  {
    return this.parallelism;
  }
  
  public void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    this.source.subscribe(new ParallelDispatcher(paramArrayOfSubscriber, this.prefetch));
  }
  
  static final class ParallelDispatcher<T>
    extends AtomicInteger
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -4470634016609963609L;
    volatile boolean cancelled;
    volatile boolean done;
    final long[] emissions;
    Throwable error;
    int index;
    final int limit;
    final int prefetch;
    int produced;
    SimpleQueue<T> queue;
    final AtomicLongArray requests;
    Subscription s;
    int sourceMode;
    final AtomicInteger subscriberCount = new AtomicInteger();
    final Subscriber<? super T>[] subscribers;
    
    ParallelDispatcher(Subscriber<? super T>[] paramArrayOfSubscriber, int paramInt)
    {
      this.subscribers = paramArrayOfSubscriber;
      this.prefetch = paramInt;
      this.limit = (paramInt - (paramInt >> 2));
      this.requests = new AtomicLongArray(paramArrayOfSubscriber.length);
      this.emissions = new long[paramArrayOfSubscriber.length];
    }
    
    void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      if (this.sourceMode == 1) {
        drainSync();
      } else {
        drainAsync();
      }
    }
    
    void drainAsync()
    {
      SimpleQueue localSimpleQueue = this.queue;
      Subscriber[] arrayOfSubscriber = this.subscribers;
      AtomicLongArray localAtomicLongArray = this.requests;
      long[] arrayOfLong = this.emissions;
      int i6 = arrayOfLong.length;
      int k = this.index;
      int i = this.produced;
      int j = 1;
      for (;;)
      {
        int i4 = 0;
        int i5 = 0;
        int i3 = 0;
        int i2 = 0;
        int n = i;
        int i1;
        do
        {
          if (this.cancelled)
          {
            localSimpleQueue.clear();
            return;
          }
          boolean bool1 = this.done;
          Object localObject;
          if (bool1)
          {
            localObject = this.error;
            if (localObject != null)
            {
              localSimpleQueue.clear();
              j = arrayOfSubscriber.length;
              for (i = i3; i < j; i++) {
                arrayOfSubscriber[i].onError((Throwable)localObject);
              }
              return;
            }
          }
          boolean bool2 = localSimpleQueue.isEmpty();
          if ((bool1) && (bool2))
          {
            j = arrayOfSubscriber.length;
            for (i = i4; i < j; i++) {
              arrayOfSubscriber[i].onComplete();
            }
            return;
          }
          long l2;
          if (!bool2)
          {
            long l1 = localAtomicLongArray.get(k);
            l2 = arrayOfLong[k];
            if (l1 == l2) {}
          }
          else
          {
            try
            {
              localObject = localSimpleQueue.poll();
              if (localObject == null)
              {
                m = n;
                i = k;
                break;
              }
              arrayOfSubscriber[k].onNext(localObject);
              arrayOfLong[k] = (l2 + 1L);
              m = n + 1;
              i = m;
              if (m == this.limit)
              {
                this.s.request(m);
                i = 0;
              }
              m = 0;
              i1 = i;
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              this.s.cancel();
              j = arrayOfSubscriber.length;
              for (i = i5; i < j; i++) {
                arrayOfSubscriber[i].onError(localThrowable);
              }
              return;
            }
          }
          m = i2 + 1;
          i1 = n;
          k++;
          i = k;
          if (k == i6) {
            i = 0;
          }
          k = i;
          i2 = m;
          n = i1;
        } while (m != i6);
        int m = i1;
        k = get();
        if (k == j)
        {
          this.index = i;
          this.produced = m;
          n = addAndGet(-j);
          k = i;
          i = m;
          j = n;
          if (n != 0) {}
        }
        else
        {
          j = k;
          k = i;
          i = m;
        }
      }
    }
    
    void drainSync()
    {
      SimpleQueue localSimpleQueue = this.queue;
      Subscriber[] arrayOfSubscriber = this.subscribers;
      AtomicLongArray localAtomicLongArray = this.requests;
      long[] arrayOfLong = this.emissions;
      int i4 = arrayOfLong.length;
      int i = this.index;
      int j = 1;
      for (;;)
      {
        int i2 = 0;
        int i3 = 0;
        int i1 = 0;
        int n = 0;
        int m = i;
        do
        {
          if (this.cancelled)
          {
            localSimpleQueue.clear();
            return;
          }
          if (localSimpleQueue.isEmpty())
          {
            j = arrayOfSubscriber.length;
            for (i = i1; i < j; i++) {
              arrayOfSubscriber[i].onComplete();
            }
            return;
          }
          long l1 = localAtomicLongArray.get(m);
          long l2 = arrayOfLong[m];
          if (l1 != l2) {
            try
            {
              Object localObject = localSimpleQueue.poll();
              if (localObject == null)
              {
                j = arrayOfSubscriber.length;
                for (i = i2; i < j; i++) {
                  arrayOfSubscriber[i].onComplete();
                }
                return;
              }
              arrayOfSubscriber[m].onNext(localObject);
              arrayOfLong[m] = (l2 + 1L);
              k = 0;
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              this.s.cancel();
              j = arrayOfSubscriber.length;
              for (i = i3; i < j; i++) {
                arrayOfSubscriber[i].onError(localThrowable);
              }
              return;
            }
          }
          k = n + 1;
          m++;
          i = m;
          if (m == i4) {
            i = 0;
          }
          m = i;
          n = k;
        } while (k != i4);
        int k = get();
        if (k == j)
        {
          this.index = i;
          k = addAndGet(-j);
          j = k;
          if (k != 0) {}
        }
        else
        {
          j = k;
        }
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      if ((this.sourceMode == 0) && (!this.queue.offer(paramT)))
      {
        this.s.cancel();
        onError(new MissingBackpressureException("Queue is full?"));
        return;
      }
      drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            setupSubscribers();
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            setupSubscribers();
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        setupSubscribers();
        paramSubscription.request(this.prefetch);
      }
    }
    
    void setupSubscribers()
    {
      final int k = this.subscribers.length;
      int j;
      for (final int i = 0; i < k; i = j)
      {
        if (this.cancelled) {
          return;
        }
        AtomicInteger localAtomicInteger = this.subscriberCount;
        j = i + 1;
        localAtomicInteger.lazySet(j);
        this.subscribers[i].onSubscribe(new Subscription()
        {
          public void cancel()
          {
            ParallelFromPublisher.ParallelDispatcher.this.cancel();
          }
          
          public void request(long paramAnonymousLong)
          {
            if (SubscriptionHelper.validate(paramAnonymousLong))
            {
              AtomicLongArray localAtomicLongArray = ParallelFromPublisher.ParallelDispatcher.this.requests;
              long l1;
              long l2;
              do
              {
                l1 = localAtomicLongArray.get(i);
                if (l1 == Long.MAX_VALUE) {
                  return;
                }
                l2 = BackpressureHelper.addCap(l1, paramAnonymousLong);
              } while (!localAtomicLongArray.compareAndSet(i, l1, l2));
              if (ParallelFromPublisher.ParallelDispatcher.this.subscriberCount.get() == k) {
                ParallelFromPublisher.ParallelDispatcher.this.drain();
              }
            }
          }
        });
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelFromPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */