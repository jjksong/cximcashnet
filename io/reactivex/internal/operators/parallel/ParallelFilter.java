package io.reactivex.internal.operators.parallel;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelFilter<T>
  extends ParallelFlowable<T>
{
  final Predicate<? super T> predicate;
  final ParallelFlowable<T> source;
  
  public ParallelFilter(ParallelFlowable<T> paramParallelFlowable, Predicate<? super T> paramPredicate)
  {
    this.source = paramParallelFlowable;
    this.predicate = paramPredicate;
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  public void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int j = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[j];
    for (int i = 0; i < j; i++)
    {
      Subscriber<? super T> localSubscriber = paramArrayOfSubscriber[i];
      if ((localSubscriber instanceof ConditionalSubscriber)) {
        arrayOfSubscriber[i] = new ParallelFilterConditionalSubscriber((ConditionalSubscriber)localSubscriber, this.predicate);
      } else {
        arrayOfSubscriber[i] = new ParallelFilterSubscriber(localSubscriber, this.predicate);
      }
    }
    this.source.subscribe(arrayOfSubscriber);
  }
  
  static abstract class BaseFilterSubscriber<T>
    implements ConditionalSubscriber<T>, Subscription
  {
    boolean done;
    final Predicate<? super T> predicate;
    Subscription s;
    
    BaseFilterSubscriber(Predicate<? super T> paramPredicate)
    {
      this.predicate = paramPredicate;
    }
    
    public final void cancel()
    {
      this.s.cancel();
    }
    
    public final void onNext(T paramT)
    {
      if (!tryOnNext(paramT)) {
        this.s.request(1L);
      }
    }
    
    public final void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
  
  static final class ParallelFilterConditionalSubscriber<T>
    extends ParallelFilter.BaseFilterSubscriber<T>
  {
    final ConditionalSubscriber<? super T> actual;
    
    ParallelFilterConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Predicate<? super T> paramPredicate)
    {
      super();
      this.actual = paramConditionalSubscriber;
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (!this.done) {
        try
        {
          boolean bool = this.predicate.test(paramT);
          if (bool) {
            return this.actual.tryOnNext(paramT);
          }
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return false;
        }
      }
      return false;
    }
  }
  
  static final class ParallelFilterSubscriber<T>
    extends ParallelFilter.BaseFilterSubscriber<T>
  {
    final Subscriber<? super T> actual;
    
    ParallelFilterSubscriber(Subscriber<? super T> paramSubscriber, Predicate<? super T> paramPredicate)
    {
      super();
      this.actual = paramSubscriber;
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (!this.done) {
        try
        {
          boolean bool = this.predicate.test(paramT);
          if (bool)
          {
            this.actual.onNext(paramT);
            return true;
          }
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return false;
        }
      }
      return false;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */