package io.reactivex.internal.operators.parallel;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelSortedJoin<T>
  extends Flowable<T>
{
  final Comparator<? super T> comparator;
  final ParallelFlowable<List<T>> source;
  
  public ParallelSortedJoin(ParallelFlowable<List<T>> paramParallelFlowable, Comparator<? super T> paramComparator)
  {
    this.source = paramParallelFlowable;
    this.comparator = paramComparator;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SortedJoinSubscription localSortedJoinSubscription = new SortedJoinSubscription(paramSubscriber, this.source.parallelism(), this.comparator);
    paramSubscriber.onSubscribe(localSortedJoinSubscription);
    this.source.subscribe(localSortedJoinSubscription.subscribers);
  }
  
  static final class SortedJoinInnerSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<List<T>>
  {
    private static final long serialVersionUID = 6751017204873808094L;
    final int index;
    final ParallelSortedJoin.SortedJoinSubscription<T> parent;
    
    SortedJoinInnerSubscriber(ParallelSortedJoin.SortedJoinSubscription<T> paramSortedJoinSubscription, int paramInt)
    {
      this.parent = paramSortedJoinSubscription;
      this.index = paramInt;
    }
    
    void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete() {}
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(List<T> paramList)
    {
      this.parent.innerNext(paramList, this.index);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
  
  static final class SortedJoinSubscription<T>
    extends AtomicInteger
    implements Subscription
  {
    private static final long serialVersionUID = 3481980673745556697L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final Comparator<? super T> comparator;
    final AtomicReference<Throwable> error = new AtomicReference();
    final int[] indexes;
    final List<T>[] lists;
    final AtomicInteger remaining = new AtomicInteger();
    final AtomicLong requested = new AtomicLong();
    final ParallelSortedJoin.SortedJoinInnerSubscriber<T>[] subscribers;
    
    SortedJoinSubscription(Subscriber<? super T> paramSubscriber, int paramInt, Comparator<? super T> paramComparator)
    {
      this.actual = paramSubscriber;
      this.comparator = paramComparator;
      paramSubscriber = new ParallelSortedJoin.SortedJoinInnerSubscriber[paramInt];
      for (int i = 0; i < paramInt; i++) {
        paramSubscriber[i] = new ParallelSortedJoin.SortedJoinInnerSubscriber(this, i);
      }
      this.subscribers = paramSubscriber;
      this.lists = new List[paramInt];
      this.indexes = new int[paramInt];
      this.remaining.lazySet(paramInt);
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        cancelAll();
        if (getAndIncrement() == 0) {
          Arrays.fill(this.lists, null);
        }
      }
    }
    
    void cancelAll()
    {
      ParallelSortedJoin.SortedJoinInnerSubscriber[] arrayOfSortedJoinInnerSubscriber = this.subscribers;
      int j = arrayOfSortedJoinInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfSortedJoinInnerSubscriber[i].cancel();
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      List[] arrayOfList = this.lists;
      int[] arrayOfInt = this.indexes;
      int i1 = arrayOfInt.length;
      int j = 1;
      for (;;)
      {
        long l2 = this.requested.get();
        for (long l1 = 0L; l1 != l2; l1 += 1L)
        {
          if (this.cancelled)
          {
            Arrays.fill(arrayOfList, null);
            return;
          }
          Object localObject1 = (Throwable)this.error.get();
          if (localObject1 != null)
          {
            cancelAll();
            Arrays.fill(arrayOfList, null);
            localSubscriber.onError((Throwable)localObject1);
            return;
          }
          Object localObject2 = null;
          i = 0;
          int k;
          for (int m = -1; i < i1; m = k)
          {
            Object localObject3 = arrayOfList[i];
            int n = arrayOfInt[i];
            localObject1 = localObject2;
            k = m;
            if (((List)localObject3).size() != n) {
              if (localObject2 == null)
              {
                localObject1 = ((List)localObject3).get(n);
                k = i;
              }
              else
              {
                localObject3 = ((List)localObject3).get(n);
                try
                {
                  k = this.comparator.compare(localObject2, localObject3);
                  if (k > 0) {
                    n = 1;
                  } else {
                    n = 0;
                  }
                  localObject1 = localObject2;
                  k = m;
                  if (n != 0)
                  {
                    localObject1 = localObject3;
                    k = i;
                  }
                }
                catch (Throwable localThrowable1)
                {
                  Exceptions.throwIfFatal(localThrowable1);
                  cancelAll();
                  Arrays.fill(arrayOfList, null);
                  if (!this.error.compareAndSet(null, localThrowable1)) {
                    RxJavaPlugins.onError(localThrowable1);
                  }
                  localSubscriber.onError((Throwable)this.error.get());
                  return;
                }
              }
            }
            i++;
            localObject2 = localThrowable1;
          }
          if (localObject2 == null)
          {
            Arrays.fill(arrayOfList, null);
            localSubscriber.onComplete();
            return;
          }
          localSubscriber.onNext(localObject2);
          arrayOfInt[m] += 1;
        }
        if (l1 == l2)
        {
          if (this.cancelled)
          {
            Arrays.fill(arrayOfList, null);
            return;
          }
          Throwable localThrowable2 = (Throwable)this.error.get();
          if (localThrowable2 != null)
          {
            cancelAll();
            Arrays.fill(arrayOfList, null);
            localSubscriber.onError(localThrowable2);
            return;
          }
          for (i = 0; i < i1; i++) {
            if (arrayOfInt[i] != arrayOfList[i].size())
            {
              i = 0;
              break label440;
            }
          }
          i = 1;
          label440:
          if (i != 0)
          {
            Arrays.fill(arrayOfList, null);
            localSubscriber.onComplete();
            return;
          }
        }
        if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
          this.requested.addAndGet(-l1);
        }
        int i = get();
        if (i == j)
        {
          j = addAndGet(-j);
          if (j == 0) {
            return;
          }
        }
        else
        {
          j = i;
        }
      }
    }
    
    void innerError(Throwable paramThrowable)
    {
      if (this.error.compareAndSet(null, paramThrowable)) {
        drain();
      } else if (paramThrowable != this.error.get()) {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void innerNext(List<T> paramList, int paramInt)
    {
      this.lists[paramInt] = paramList;
      if (this.remaining.decrementAndGet() == 0) {
        drain();
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        if (this.remaining.get() == 0) {
          drain();
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelSortedJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */