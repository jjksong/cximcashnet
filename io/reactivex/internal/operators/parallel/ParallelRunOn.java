package io.reactivex.internal.operators.parallel;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ParallelRunOn<T>
  extends ParallelFlowable<T>
{
  final int prefetch;
  final Scheduler scheduler;
  final ParallelFlowable<? extends T> source;
  
  public ParallelRunOn(ParallelFlowable<? extends T> paramParallelFlowable, Scheduler paramScheduler, int paramInt)
  {
    this.source = paramParallelFlowable;
    this.scheduler = paramScheduler;
    this.prefetch = paramInt;
  }
  
  public int parallelism()
  {
    return this.source.parallelism();
  }
  
  public void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber)
  {
    if (!validate(paramArrayOfSubscriber)) {
      return;
    }
    int k = paramArrayOfSubscriber.length;
    Subscriber[] arrayOfSubscriber = new Subscriber[k];
    int j = this.prefetch;
    for (int i = 0; i < k; i++)
    {
      Subscriber<? super T> localSubscriber = paramArrayOfSubscriber[i];
      Scheduler.Worker localWorker = this.scheduler.createWorker();
      SpscArrayQueue localSpscArrayQueue = new SpscArrayQueue(j);
      if ((localSubscriber instanceof ConditionalSubscriber)) {
        arrayOfSubscriber[i] = new RunOnConditionalSubscriber((ConditionalSubscriber)localSubscriber, j, localSpscArrayQueue, localWorker);
      } else {
        arrayOfSubscriber[i] = new RunOnSubscriber(localSubscriber, j, localSpscArrayQueue, localWorker);
      }
    }
    this.source.subscribe(arrayOfSubscriber);
  }
  
  static abstract class BaseRunOnSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription, Runnable
  {
    private static final long serialVersionUID = 9222303586456402150L;
    volatile boolean cancelled;
    int consumed;
    volatile boolean done;
    Throwable error;
    final int limit;
    final int prefetch;
    final SpscArrayQueue<T> queue;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    final Scheduler.Worker worker;
    
    BaseRunOnSubscriber(int paramInt, SpscArrayQueue<T> paramSpscArrayQueue, Scheduler.Worker paramWorker)
    {
      this.prefetch = paramInt;
      this.queue = paramSpscArrayQueue;
      this.limit = (paramInt - (paramInt >> 2));
      this.worker = paramWorker;
    }
    
    public final void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        this.worker.dispose();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    public final void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      schedule();
    }
    
    public final void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      schedule();
    }
    
    public final void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (!this.queue.offer(paramT))
      {
        this.s.cancel();
        onError(new MissingBackpressureException("Queue is full?!"));
        return;
      }
      schedule();
    }
    
    public final void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        schedule();
      }
    }
    
    final void schedule()
    {
      if (getAndIncrement() == 0) {
        this.worker.schedule(this);
      }
    }
  }
  
  static final class RunOnConditionalSubscriber<T>
    extends ParallelRunOn.BaseRunOnSubscriber<T>
  {
    private static final long serialVersionUID = 1075119423897941642L;
    final ConditionalSubscriber<? super T> actual;
    
    RunOnConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, int paramInt, SpscArrayQueue<T> paramSpscArrayQueue, Scheduler.Worker paramWorker)
    {
      super(paramSpscArrayQueue, paramWorker);
      this.actual = paramConditionalSubscriber;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void run()
    {
      int j = this.consumed;
      SpscArrayQueue localSpscArrayQueue = this.queue;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      int m = this.limit;
      int i = 1;
      for (;;)
      {
        long l3 = this.requested.get();
        Object localObject;
        long l1;
        for (long l2 = 0L; l2 != l3; l2 = l1)
        {
          if (this.cancelled)
          {
            localSpscArrayQueue.clear();
            return;
          }
          boolean bool = this.done;
          if (bool)
          {
            localObject = this.error;
            if (localObject != null)
            {
              localSpscArrayQueue.clear();
              localConditionalSubscriber.onError((Throwable)localObject);
              this.worker.dispose();
              return;
            }
          }
          localObject = localSpscArrayQueue.poll();
          if (localObject == null) {
            k = 1;
          } else {
            k = 0;
          }
          if ((bool) && (k != 0))
          {
            localConditionalSubscriber.onComplete();
            this.worker.dispose();
            return;
          }
          if (k != 0) {
            break;
          }
          l1 = l2;
          if (localConditionalSubscriber.tryOnNext(localObject)) {
            l1 = l2 + 1L;
          }
          j++;
          if (j == m)
          {
            this.s.request(j);
            j = 0;
          }
        }
        int k = i;
        if (l2 == l3)
        {
          if (this.cancelled)
          {
            localSpscArrayQueue.clear();
            return;
          }
          if (this.done)
          {
            localObject = this.error;
            if (localObject != null)
            {
              localSpscArrayQueue.clear();
              localConditionalSubscriber.onError((Throwable)localObject);
              this.worker.dispose();
              return;
            }
            if (localSpscArrayQueue.isEmpty())
            {
              localConditionalSubscriber.onComplete();
              this.worker.dispose();
              return;
            }
          }
        }
        if ((l2 != 0L) && (l3 != Long.MAX_VALUE)) {
          this.requested.addAndGet(-l2);
        }
        i = get();
        if (i == k)
        {
          this.consumed = j;
          i = addAndGet(-k);
          if (i == 0) {
            return;
          }
        }
      }
    }
  }
  
  static final class RunOnSubscriber<T>
    extends ParallelRunOn.BaseRunOnSubscriber<T>
  {
    private static final long serialVersionUID = 1075119423897941642L;
    final Subscriber<? super T> actual;
    
    RunOnSubscriber(Subscriber<? super T> paramSubscriber, int paramInt, SpscArrayQueue<T> paramSpscArrayQueue, Scheduler.Worker paramWorker)
    {
      super(paramSpscArrayQueue, paramWorker);
      this.actual = paramSubscriber;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void run()
    {
      int j = this.consumed;
      SpscArrayQueue localSpscArrayQueue = this.queue;
      Subscriber localSubscriber = this.actual;
      int m = this.limit;
      int i = 1;
      for (;;)
      {
        long l2 = this.requested.get();
        long l1 = 0L;
        Object localObject;
        while (l1 != l2)
        {
          if (this.cancelled)
          {
            localSpscArrayQueue.clear();
            return;
          }
          boolean bool = this.done;
          if (bool)
          {
            localObject = this.error;
            if (localObject != null)
            {
              localSpscArrayQueue.clear();
              localSubscriber.onError((Throwable)localObject);
              this.worker.dispose();
              return;
            }
          }
          localObject = localSpscArrayQueue.poll();
          if (localObject == null) {
            k = 1;
          } else {
            k = 0;
          }
          if ((bool) && (k != 0))
          {
            localSubscriber.onComplete();
            this.worker.dispose();
            return;
          }
          if (k != 0) {
            break;
          }
          localSubscriber.onNext(localObject);
          l1 += 1L;
          j++;
          if (j == m)
          {
            this.s.request(j);
            j = 0;
          }
        }
        if (l1 == l2)
        {
          if (this.cancelled)
          {
            localSpscArrayQueue.clear();
            return;
          }
          if (this.done)
          {
            localObject = this.error;
            if (localObject != null)
            {
              localSpscArrayQueue.clear();
              localSubscriber.onError((Throwable)localObject);
              this.worker.dispose();
              return;
            }
            if (localSpscArrayQueue.isEmpty())
            {
              localSubscriber.onComplete();
              this.worker.dispose();
              return;
            }
          }
        }
        if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
          this.requested.addAndGet(-l1);
        }
        int k = get();
        if (k == i)
        {
          this.consumed = j;
          i = addAndGet(-i);
          if (i == 0) {
            return;
          }
        }
        else
        {
          i = k;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/parallel/ParallelRunOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */