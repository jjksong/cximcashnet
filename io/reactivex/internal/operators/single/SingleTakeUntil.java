package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SingleTakeUntil<T, U>
  extends Single<T>
{
  final Publisher<U> other;
  final SingleSource<T> source;
  
  public SingleTakeUntil(SingleSource<T> paramSingleSource, Publisher<U> paramPublisher)
  {
    this.source = paramSingleSource;
    this.other = paramPublisher;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    TakeUntilMainObserver localTakeUntilMainObserver = new TakeUntilMainObserver(paramSingleObserver);
    paramSingleObserver.onSubscribe(localTakeUntilMainObserver);
    this.other.subscribe(localTakeUntilMainObserver.other);
    this.source.subscribe(localTakeUntilMainObserver);
  }
  
  static final class TakeUntilMainObserver<T>
    extends AtomicReference<Disposable>
    implements SingleObserver<T>, Disposable
  {
    private static final long serialVersionUID = -622603812305745221L;
    final SingleObserver<? super T> actual;
    final SingleTakeUntil.TakeUntilOtherSubscriber other;
    
    TakeUntilMainObserver(SingleObserver<? super T> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
      this.other = new SingleTakeUntil.TakeUntilOtherSubscriber(this);
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.other.dispose();
      if (((Disposable)get() != DisposableHelper.DISPOSED) && ((Disposable)getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED))
      {
        this.actual.onError(paramThrowable);
        return;
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      DisposableHelper.setOnce(this, paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      this.other.dispose();
      if (((Disposable)get() != DisposableHelper.DISPOSED) && ((Disposable)getAndSet(DisposableHelper.DISPOSED) != DisposableHelper.DISPOSED)) {
        this.actual.onSuccess(paramT);
      }
    }
    
    void otherError(Throwable paramThrowable)
    {
      if ((Disposable)get() != DisposableHelper.DISPOSED)
      {
        Disposable localDisposable = (Disposable)getAndSet(DisposableHelper.DISPOSED);
        if (localDisposable != DisposableHelper.DISPOSED)
        {
          if (localDisposable != null) {
            localDisposable.dispose();
          }
          this.actual.onError(paramThrowable);
          return;
        }
      }
      RxJavaPlugins.onError(paramThrowable);
    }
  }
  
  static final class TakeUntilOtherSubscriber
    extends AtomicReference<Subscription>
    implements Subscriber<Object>
  {
    private static final long serialVersionUID = 5170026210238877381L;
    final SingleTakeUntil.TakeUntilMainObserver<?> parent;
    
    TakeUntilOtherSubscriber(SingleTakeUntil.TakeUntilMainObserver<?> paramTakeUntilMainObserver)
    {
      this.parent = paramTakeUntilMainObserver;
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      if (get() != SubscriptionHelper.CANCELLED)
      {
        lazySet(SubscriptionHelper.CANCELLED);
        this.parent.otherError(new CancellationException());
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.otherError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (SubscriptionHelper.cancel(this)) {
        this.parent.otherError(new CancellationException());
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleTakeUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */