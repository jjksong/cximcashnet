package io.reactivex.internal.operators.single;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;

public final class SingleInternalHelper
{
  private SingleInternalHelper()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T> Callable<NoSuchElementException> emptyThrower()
  {
    return NoSuchElementCallable.INSTANCE;
  }
  
  public static <T> Iterable<? extends Flowable<T>> iterableToFlowable(Iterable<? extends SingleSource<? extends T>> paramIterable)
  {
    return new ToFlowableIterable(paramIterable);
  }
  
  public static <T> Function<SingleSource<? extends T>, Publisher<? extends T>> toFlowable()
  {
    return ToFlowable.INSTANCE;
  }
  
  public static <T> Function<SingleSource<? extends T>, Observable<? extends T>> toObservable()
  {
    return ToObservable.INSTANCE;
  }
  
  static enum NoSuchElementCallable
    implements Callable<NoSuchElementException>
  {
    INSTANCE;
    
    private NoSuchElementCallable() {}
    
    public NoSuchElementException call()
      throws Exception
    {
      return new NoSuchElementException();
    }
  }
  
  static enum ToFlowable
    implements Function<SingleSource, Publisher>
  {
    INSTANCE;
    
    private ToFlowable() {}
    
    public Publisher apply(SingleSource paramSingleSource)
    {
      return new SingleToFlowable(paramSingleSource);
    }
  }
  
  static final class ToFlowableIterable<T>
    implements Iterable<Flowable<T>>
  {
    private final Iterable<? extends SingleSource<? extends T>> sources;
    
    ToFlowableIterable(Iterable<? extends SingleSource<? extends T>> paramIterable)
    {
      this.sources = paramIterable;
    }
    
    public Iterator<Flowable<T>> iterator()
    {
      return new SingleInternalHelper.ToFlowableIterator(this.sources.iterator());
    }
  }
  
  static final class ToFlowableIterator<T>
    implements Iterator<Flowable<T>>
  {
    private final Iterator<? extends SingleSource<? extends T>> sit;
    
    ToFlowableIterator(Iterator<? extends SingleSource<? extends T>> paramIterator)
    {
      this.sit = paramIterator;
    }
    
    public boolean hasNext()
    {
      return this.sit.hasNext();
    }
    
    public Flowable<T> next()
    {
      return new SingleToFlowable((SingleSource)this.sit.next());
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException();
    }
  }
  
  static enum ToObservable
    implements Function<SingleSource, Observable>
  {
    INSTANCE;
    
    private ToObservable() {}
    
    public Observable apply(SingleSource paramSingleSource)
    {
      return new SingleToObservable(paramSingleSource);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleInternalHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */