package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;

@Experimental
public final class SingleDoFinally<T>
  extends Single<T>
{
  final Action onFinally;
  final SingleSource<T> source;
  
  public SingleDoFinally(SingleSource<T> paramSingleSource, Action paramAction)
  {
    this.source = paramSingleSource;
    this.onFinally = paramAction;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new DoFinallyObserver(paramSingleObserver, this.onFinally));
  }
  
  static final class DoFinallyObserver<T>
    extends AtomicInteger
    implements SingleObserver<T>, Disposable
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final SingleObserver<? super T> actual;
    Disposable d;
    final Action onFinally;
    
    DoFinallyObserver(SingleObserver<? super T> paramSingleObserver, Action paramAction)
    {
      this.actual = paramSingleObserver;
      this.onFinally = paramAction;
    }
    
    public void dispose()
    {
      this.d.dispose();
      runFinally();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
      runFinally();
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoFinally.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */