package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.concurrent.Callable;

public final class SingleFromCallable<T>
  extends Single<T>
{
  final Callable<? extends T> callable;
  
  public SingleFromCallable(Callable<? extends T> paramCallable)
  {
    this.callable = paramCallable;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    paramSingleObserver.onSubscribe(EmptyDisposable.INSTANCE);
    try
    {
      Object localObject = this.callable.call();
      if (localObject != null)
      {
        paramSingleObserver.onSuccess(localObject);
      }
      else
      {
        localObject = new java/lang/NullPointerException;
        ((NullPointerException)localObject).<init>("The callable returned a null value");
        paramSingleObserver.onError((Throwable)localObject);
      }
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      paramSingleObserver.onError(localThrowable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleFromCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */