package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.observers.ResumeSingleObserver;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SingleDelayWithPublisher<T, U>
  extends Single<T>
{
  final Publisher<U> other;
  final SingleSource<T> source;
  
  public SingleDelayWithPublisher(SingleSource<T> paramSingleSource, Publisher<U> paramPublisher)
  {
    this.source = paramSingleSource;
    this.other = paramPublisher;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.other.subscribe(new OtherSubscriber(paramSingleObserver, this.source));
  }
  
  static final class OtherSubscriber<T, U>
    extends AtomicReference<Disposable>
    implements Subscriber<U>, Disposable
  {
    private static final long serialVersionUID = -8565274649390031272L;
    final SingleObserver<? super T> actual;
    boolean done;
    Subscription s;
    final SingleSource<T> source;
    
    OtherSubscriber(SingleObserver<? super T> paramSingleObserver, SingleSource<T> paramSingleSource)
    {
      this.actual = paramSingleObserver;
      this.source = paramSingleSource;
    }
    
    public void dispose()
    {
      this.s.cancel();
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.source.subscribe(new ResumeSingleObserver(this, this.actual));
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(U paramU)
    {
      this.s.cancel();
      onComplete();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDelayWithPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */