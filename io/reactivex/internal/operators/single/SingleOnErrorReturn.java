package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;

public final class SingleOnErrorReturn<T>
  extends Single<T>
{
  final SingleSource<? extends T> source;
  final T value;
  final Function<? super Throwable, ? extends T> valueSupplier;
  
  public SingleOnErrorReturn(SingleSource<? extends T> paramSingleSource, Function<? super Throwable, ? extends T> paramFunction, T paramT)
  {
    this.source = paramSingleSource;
    this.valueSupplier = paramFunction;
    this.value = paramT;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        Object localObject2;
        if (SingleOnErrorReturn.this.valueSupplier != null) {
          try
          {
            Object localObject1 = SingleOnErrorReturn.this.valueSupplier.apply(paramAnonymousThrowable);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            paramSingleObserver.onError(new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable }));
            return;
          }
        } else {
          localObject2 = SingleOnErrorReturn.this.value;
        }
        if (localObject2 == null)
        {
          localObject2 = new NullPointerException("Value supplied was null");
          ((NullPointerException)localObject2).initCause(paramAnonymousThrowable);
          paramSingleObserver.onError((Throwable)localObject2);
          return;
        }
        paramSingleObserver.onSuccess(localObject2);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        paramSingleObserver.onSuccess(paramAnonymousT);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleOnErrorReturn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */