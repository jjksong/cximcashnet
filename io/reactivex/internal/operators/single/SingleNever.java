package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.internal.disposables.EmptyDisposable;

public final class SingleNever
  extends Single<Object>
{
  public static final Single<Object> INSTANCE = new SingleNever();
  
  protected void subscribeActual(SingleObserver<? super Object> paramSingleObserver)
  {
    paramSingleObserver.onSubscribe(EmptyDisposable.NEVER);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleNever.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */