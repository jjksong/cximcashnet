package io.reactivex.internal.operators.single;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicIntQueueDisposable;
import java.util.Iterator;

public final class SingleFlatMapIterableObservable<T, R>
  extends Observable<R>
{
  final Function<? super T, ? extends Iterable<? extends R>> mapper;
  final SingleSource<T> source;
  
  public SingleFlatMapIterableObservable(SingleSource<T> paramSingleSource, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
  {
    this.source = paramSingleSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(Observer<? super R> paramObserver)
  {
    this.source.subscribe(new FlatMapIterableObserver(paramObserver, this.mapper));
  }
  
  static final class FlatMapIterableObserver<T, R>
    extends BasicIntQueueDisposable<R>
    implements SingleObserver<T>
  {
    private static final long serialVersionUID = -8938804753851907758L;
    final Observer<? super R> actual;
    volatile boolean cancelled;
    Disposable d;
    volatile Iterator<? extends R> it;
    final Function<? super T, ? extends Iterable<? extends R>> mapper;
    boolean outputFused;
    
    FlatMapIterableObserver(Observer<? super R> paramObserver, Function<? super T, ? extends Iterable<? extends R>> paramFunction)
    {
      this.actual = paramObserver;
      this.mapper = paramFunction;
    }
    
    public void clear()
    {
      this.it = null;
    }
    
    public void dispose()
    {
      this.cancelled = true;
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public boolean isEmpty()
    {
      boolean bool;
      if (this.it == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    /* Error */
    public void onSuccess(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 35	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:actual	Lio/reactivex/Observer;
      //   4: astore_3
      //   5: aload_0
      //   6: getfield 37	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:mapper	Lio/reactivex/functions/Function;
      //   9: aload_1
      //   10: invokeinterface 85 2 0
      //   15: checkcast 87	java/lang/Iterable
      //   18: invokeinterface 91 1 0
      //   23: astore 4
      //   25: aload 4
      //   27: invokeinterface 96 1 0
      //   32: istore_2
      //   33: iload_2
      //   34: ifne +10 -> 44
      //   37: aload_3
      //   38: invokeinterface 99 1 0
      //   43: return
      //   44: aload_0
      //   45: getfield 101	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:outputFused	Z
      //   48: ifeq +23 -> 71
      //   51: aload_0
      //   52: aload 4
      //   54: putfield 43	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:it	Ljava/util/Iterator;
      //   57: aload_3
      //   58: aconst_null
      //   59: invokeinterface 104 2 0
      //   64: aload_3
      //   65: invokeinterface 99 1 0
      //   70: return
      //   71: aload_0
      //   72: getfield 46	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:cancelled	Z
      //   75: ifeq +4 -> 79
      //   78: return
      //   79: aload 4
      //   81: invokeinterface 108 1 0
      //   86: astore_1
      //   87: aload_3
      //   88: aload_1
      //   89: invokeinterface 104 2 0
      //   94: aload_0
      //   95: getfield 46	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:cancelled	Z
      //   98: ifeq +4 -> 102
      //   101: return
      //   102: aload 4
      //   104: invokeinterface 96 1 0
      //   109: istore_2
      //   110: iload_2
      //   111: ifne -40 -> 71
      //   114: aload_3
      //   115: invokeinterface 99 1 0
      //   120: return
      //   121: astore_1
      //   122: aload_1
      //   123: invokestatic 113	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   126: aload_3
      //   127: aload_1
      //   128: invokeinterface 67 2 0
      //   133: return
      //   134: astore_1
      //   135: aload_1
      //   136: invokestatic 113	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   139: aload_3
      //   140: aload_1
      //   141: invokeinterface 67 2 0
      //   146: return
      //   147: astore_1
      //   148: aload_1
      //   149: invokestatic 113	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   152: aload_0
      //   153: getfield 35	io/reactivex/internal/operators/single/SingleFlatMapIterableObservable$FlatMapIterableObserver:actual	Lio/reactivex/Observer;
      //   156: aload_1
      //   157: invokeinterface 67 2 0
      //   162: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	163	0	this	FlatMapIterableObserver
      //   0	163	1	paramT	T
      //   32	79	2	bool	boolean
      //   4	136	3	localObserver	Observer
      //   23	80	4	localIterator	Iterator
      // Exception table:
      //   from	to	target	type
      //   102	110	121	java/lang/Throwable
      //   79	87	134	java/lang/Throwable
      //   5	33	147	java/lang/Throwable
    }
    
    public R poll()
      throws Exception
    {
      Iterator localIterator = this.it;
      if (localIterator != null)
      {
        Object localObject = ObjectHelper.requireNonNull(localIterator.next(), "The iterator returned a null value");
        if (!localIterator.hasNext()) {
          this.it = null;
        }
        return (R)localObject;
      }
      return null;
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleFlatMapIterableObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */