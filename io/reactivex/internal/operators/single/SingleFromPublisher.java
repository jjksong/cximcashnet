package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SingleFromPublisher<T>
  extends Single<T>
{
  final Publisher<? extends T> publisher;
  
  public SingleFromPublisher(Publisher<? extends T> paramPublisher)
  {
    this.publisher = paramPublisher;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.publisher.subscribe(new ToSingleObserver(paramSingleObserver));
  }
  
  static final class ToSingleObserver<T>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    volatile boolean disposed;
    boolean done;
    Subscription s;
    T value;
    
    ToSingleObserver(SingleObserver<? super T> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      this.disposed = true;
      this.s.cancel();
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject = this.value;
      this.value = null;
      if (localObject == null) {
        this.actual.onError(new NoSuchElementException("The source Publisher is empty"));
      } else {
        this.actual.onSuccess(localObject);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.value = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.value != null)
      {
        this.s.cancel();
        this.done = true;
        this.value = null;
        this.actual.onError(new IndexOutOfBoundsException("Too many elements in the Publisher"));
      }
      else
      {
        this.value = paramT;
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleFromPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */