package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiPredicate;

public final class SingleContains<T>
  extends Single<Boolean>
{
  final BiPredicate<Object, Object> comparer;
  final SingleSource<T> source;
  final Object value;
  
  public SingleContains(SingleSource<T> paramSingleSource, Object paramObject, BiPredicate<Object, Object> paramBiPredicate)
  {
    this.source = paramSingleSource;
    this.value = paramObject;
    this.comparer = paramBiPredicate;
  }
  
  protected void subscribeActual(final SingleObserver<? super Boolean> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        try
        {
          boolean bool = SingleContains.this.comparer.test(paramAnonymousT, SingleContains.this.value);
          paramSingleObserver.onSuccess(Boolean.valueOf(bool));
          return;
        }
        catch (Throwable paramAnonymousT)
        {
          Exceptions.throwIfFatal(paramAnonymousT);
          paramSingleObserver.onError(paramAnonymousT);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleContains.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */