package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiConsumer;

public final class SingleDoOnEvent<T>
  extends Single<T>
{
  final BiConsumer<? super T, ? super Throwable> onEvent;
  final SingleSource<T> source;
  
  public SingleDoOnEvent(SingleSource<T> paramSingleSource, BiConsumer<? super T, ? super Throwable> paramBiConsumer)
  {
    this.source = paramSingleSource;
    this.onEvent = paramBiConsumer;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          SingleDoOnEvent.this.onEvent.accept(null, paramAnonymousThrowable);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramAnonymousThrowable = new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable });
        }
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        try
        {
          SingleDoOnEvent.this.onEvent.accept(paramAnonymousT, null);
          paramSingleObserver.onSuccess(paramAnonymousT);
          return;
        }
        catch (Throwable paramAnonymousT)
        {
          Exceptions.throwIfFatal(paramAnonymousT);
          paramSingleObserver.onError(paramAnonymousT);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoOnEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */