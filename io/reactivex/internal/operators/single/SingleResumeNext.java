package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.ResumeSingleObserver;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleResumeNext<T>
  extends Single<T>
{
  final Function<? super Throwable, ? extends SingleSource<? extends T>> nextFunction;
  final SingleSource<? extends T> source;
  
  public SingleResumeNext(SingleSource<? extends T> paramSingleSource, Function<? super Throwable, ? extends SingleSource<? extends T>> paramFunction)
  {
    this.source = paramSingleSource;
    this.nextFunction = paramFunction;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new ResumeMainSingleObserver(paramSingleObserver, this.nextFunction));
  }
  
  static final class ResumeMainSingleObserver<T>
    extends AtomicReference<Disposable>
    implements SingleObserver<T>, Disposable
  {
    private static final long serialVersionUID = -5314538511045349925L;
    final SingleObserver<? super T> actual;
    final Function<? super Throwable, ? extends SingleSource<? extends T>> nextFunction;
    
    ResumeMainSingleObserver(SingleObserver<? super T> paramSingleObserver, Function<? super Throwable, ? extends SingleSource<? extends T>> paramFunction)
    {
      this.actual = paramSingleObserver;
      this.nextFunction = paramFunction;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        SingleSource localSingleSource = (SingleSource)ObjectHelper.requireNonNull(this.nextFunction.apply(paramThrowable), "The nextFunction returned a null SingleSource.");
        localSingleSource.subscribe(new ResumeSingleObserver(this, this.actual));
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.setOnce(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleResumeNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */