package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;

public final class SingleMap<T, R>
  extends Single<R>
{
  final Function<? super T, ? extends R> mapper;
  final SingleSource<? extends T> source;
  
  public SingleMap(SingleSource<? extends T> paramSingleSource, Function<? super T, ? extends R> paramFunction)
  {
    this.source = paramSingleSource;
    this.mapper = paramFunction;
  }
  
  protected void subscribeActual(final SingleObserver<? super R> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        try
        {
          paramAnonymousT = SingleMap.this.mapper.apply(paramAnonymousT);
          paramSingleObserver.onSuccess(paramAnonymousT);
          return;
        }
        catch (Throwable paramAnonymousT)
        {
          Exceptions.throwIfFatal(paramAnonymousT);
          onError(paramAnonymousT);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */