package io.reactivex.internal.operators.single;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class SingleToObservable<T>
  extends Observable<T>
{
  final SingleSource<? extends T> source;
  
  public SingleToObservable(SingleSource<? extends T> paramSingleSource)
  {
    this.source = paramSingleSource;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new SingleToObservableObserver(paramObserver));
  }
  
  static final class SingleToObservableObserver<T>
    implements SingleObserver<T>, Disposable
  {
    final Observer<? super T> actual;
    Disposable d;
    
    SingleToObservableObserver(Observer<? super T> paramObserver)
    {
      this.actual = paramObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onNext(paramT);
      this.actual.onComplete();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleToObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */