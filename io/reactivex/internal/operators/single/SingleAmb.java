package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class SingleAmb<T>
  extends Single<T>
{
  private final SingleSource<? extends T>[] sources;
  private final Iterable<? extends SingleSource<? extends T>> sourcesIterable;
  
  public SingleAmb(SingleSource<? extends T>[] paramArrayOfSingleSource, Iterable<? extends SingleSource<? extends T>> paramIterable)
  {
    this.sources = paramArrayOfSingleSource;
    this.sourcesIterable = paramIterable;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    Object localObject2 = this.sources;
    Iterator localIterator;
    int j;
    if (localObject2 == null)
    {
      Object localObject1 = new SingleSource[8];
      try
      {
        localIterator = this.sourcesIterable.iterator();
        i = 0;
        for (;;)
        {
          localObject2 = localObject1;
          j = i;
          if (!localIterator.hasNext()) {
            break;
          }
          localObject3 = (SingleSource)localIterator.next();
          if (localObject3 == null)
          {
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("One of the sources is null");
            EmptyDisposable.error((Throwable)localObject1, paramSingleObserver);
            return;
          }
          localObject2 = localObject1;
          if (i == localObject1.length)
          {
            localObject2 = new SingleSource[(i >> 2) + i];
            System.arraycopy(localObject1, 0, localObject2, 0, i);
          }
          localObject2[i] = localObject3;
          i++;
          localObject1 = localObject2;
        }
        j = localObject2.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptyDisposable.error(localThrowable, paramSingleObserver);
        return;
      }
    }
    Object localObject3 = new CompositeDisposable();
    AmbSingleObserver localAmbSingleObserver = new AmbSingleObserver(paramSingleObserver, (CompositeDisposable)localObject3);
    paramSingleObserver.onSubscribe((Disposable)localObject3);
    for (int i = 0; i < j; i++)
    {
      localIterator = localObject2[i];
      if (localAmbSingleObserver.get()) {
        return;
      }
      if (localIterator == null)
      {
        ((CompositeDisposable)localObject3).dispose();
        localObject2 = new NullPointerException("One of the sources is null");
        if (localAmbSingleObserver.compareAndSet(false, true)) {
          paramSingleObserver.onError((Throwable)localObject2);
        } else {
          RxJavaPlugins.onError((Throwable)localObject2);
        }
        return;
      }
      localIterator.subscribe(localAmbSingleObserver);
    }
  }
  
  static final class AmbSingleObserver<T>
    extends AtomicBoolean
    implements SingleObserver<T>
  {
    private static final long serialVersionUID = -1944085461036028108L;
    final SingleObserver<? super T> s;
    final CompositeDisposable set;
    
    AmbSingleObserver(SingleObserver<? super T> paramSingleObserver, CompositeDisposable paramCompositeDisposable)
    {
      this.s = paramSingleObserver;
      this.set = paramCompositeDisposable;
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (compareAndSet(false, true))
      {
        this.set.dispose();
        this.s.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.set.add(paramDisposable);
    }
    
    public void onSuccess(T paramT)
    {
      if (compareAndSet(false, true))
      {
        this.set.dispose();
        this.s.onSuccess(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleAmb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */