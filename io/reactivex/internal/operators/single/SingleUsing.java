package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleUsing<T, U>
  extends Single<T>
{
  final Consumer<? super U> disposer;
  final boolean eager;
  final Callable<U> resourceSupplier;
  final Function<? super U, ? extends SingleSource<? extends T>> singleFunction;
  
  public SingleUsing(Callable<U> paramCallable, Function<? super U, ? extends SingleSource<? extends T>> paramFunction, Consumer<? super U> paramConsumer, boolean paramBoolean)
  {
    this.resourceSupplier = paramCallable;
    this.singleFunction = paramFunction;
    this.disposer = paramConsumer;
    this.eager = paramBoolean;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    try
    {
      Object localObject = this.resourceSupplier.call();
      try
      {
        SingleSource localSingleSource = (SingleSource)ObjectHelper.requireNonNull(this.singleFunction.apply(localObject), "The singleFunction returned a null SingleSource");
        localSingleSource.subscribe(new UsingSingleObserver(paramSingleObserver, localObject, this.eager, this.disposer));
        return;
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        Throwable localThrowable3 = localThrowable1;
        CompositeException localCompositeException;
        if (this.eager) {
          try
          {
            this.disposer.accept(localObject);
            localThrowable3 = localThrowable1;
          }
          catch (Throwable localThrowable4)
          {
            Exceptions.throwIfFatal(localThrowable4);
            localCompositeException = new CompositeException(new Throwable[] { localThrowable1, localThrowable4 });
          }
        }
        EmptyDisposable.error(localCompositeException, paramSingleObserver);
        if (!this.eager) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable paramSingleObserver)
          {
            Exceptions.throwIfFatal(paramSingleObserver);
            RxJavaPlugins.onError(paramSingleObserver);
          }
        }
        return;
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptyDisposable.error(localThrowable2, paramSingleObserver);
    }
  }
  
  static final class UsingSingleObserver<T, U>
    extends AtomicReference<Object>
    implements SingleObserver<T>, Disposable
  {
    private static final long serialVersionUID = -5331524057054083935L;
    final SingleObserver<? super T> actual;
    Disposable d;
    final Consumer<? super U> disposer;
    final boolean eager;
    
    UsingSingleObserver(SingleObserver<? super T> paramSingleObserver, U paramU, boolean paramBoolean, Consumer<? super U> paramConsumer)
    {
      super();
      this.actual = paramSingleObserver;
      this.eager = paramBoolean;
      this.disposer = paramConsumer;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
      disposeAfter();
    }
    
    void disposeAfter()
    {
      Object localObject = getAndSet(this);
      if (localObject != this) {
        try
        {
          this.disposer.accept(localObject);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      Object localObject = paramThrowable;
      CompositeException localCompositeException;
      if (this.eager)
      {
        localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
            localObject = paramThrowable;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            localCompositeException = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
          }
        } else {
          return;
        }
      }
      this.actual.onError(localCompositeException);
      if (!this.eager) {
        disposeAfter();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.d = DisposableHelper.DISPOSED;
      if (this.eager)
      {
        Object localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            this.actual.onError(paramT);
            return;
          }
        } else {
          return;
        }
      }
      this.actual.onSuccess(paramT);
      if (!this.eager) {
        disposeAfter();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleUsing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */