package io.reactivex.internal.operators.single;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class SingleTimeout<T>
  extends Single<T>
{
  final SingleSource<? extends T> other;
  final Scheduler scheduler;
  final SingleSource<T> source;
  final long timeout;
  final TimeUnit unit;
  
  public SingleTimeout(SingleSource<T> paramSingleSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, SingleSource<? extends T> paramSingleSource1)
  {
    this.source = paramSingleSource;
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.other = paramSingleSource1;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    final CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    paramSingleObserver.onSubscribe(localCompositeDisposable);
    final AtomicBoolean localAtomicBoolean = new AtomicBoolean();
    localCompositeDisposable.add(this.scheduler.scheduleDirect(new Runnable()
    {
      public void run()
      {
        if (localAtomicBoolean.compareAndSet(false, true)) {
          if (SingleTimeout.this.other != null)
          {
            localCompositeDisposable.clear();
            SingleTimeout.this.other.subscribe(new SingleObserver()
            {
              public void onError(Throwable paramAnonymous2Throwable)
              {
                SingleTimeout.1.this.val$set.dispose();
                SingleTimeout.1.this.val$s.onError(paramAnonymous2Throwable);
              }
              
              public void onSubscribe(Disposable paramAnonymous2Disposable)
              {
                SingleTimeout.1.this.val$set.add(paramAnonymous2Disposable);
              }
              
              public void onSuccess(T paramAnonymous2T)
              {
                SingleTimeout.1.this.val$set.dispose();
                SingleTimeout.1.this.val$s.onSuccess(paramAnonymous2T);
              }
            });
          }
          else
          {
            localCompositeDisposable.dispose();
            paramSingleObserver.onError(new TimeoutException());
          }
        }
      }
    }, this.timeout, this.unit));
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        if (localAtomicBoolean.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramSingleObserver.onError(paramAnonymousThrowable);
        }
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        if (localAtomicBoolean.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramSingleObserver.onSuccess(paramAnonymousT);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleTimeout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */