package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposables;

public final class SingleJust<T>
  extends Single<T>
{
  final T value;
  
  public SingleJust(T paramT)
  {
    this.value = paramT;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    paramSingleObserver.onSubscribe(Disposables.disposed());
    paramSingleObserver.onSuccess(this.value);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleJust.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */