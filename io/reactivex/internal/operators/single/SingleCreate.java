package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Cancellable;
import io.reactivex.internal.disposables.CancellableDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleCreate<T>
  extends Single<T>
{
  final SingleOnSubscribe<T> source;
  
  public SingleCreate(SingleOnSubscribe<T> paramSingleOnSubscribe)
  {
    this.source = paramSingleOnSubscribe;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    Emitter localEmitter = new Emitter(paramSingleObserver);
    paramSingleObserver.onSubscribe(localEmitter);
    try
    {
      this.source.subscribe(localEmitter);
    }
    catch (Throwable paramSingleObserver)
    {
      Exceptions.throwIfFatal(paramSingleObserver);
      localEmitter.onError(paramSingleObserver);
    }
  }
  
  static final class Emitter<T>
    extends AtomicReference<Disposable>
    implements SingleEmitter<T>, Disposable
  {
    private static final long serialVersionUID = -2467358622224974244L;
    final SingleObserver<? super T> actual;
    
    Emitter(SingleObserver<? super T> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      if (get() != DisposableHelper.DISPOSED)
      {
        paramThrowable = (Disposable)getAndSet(DisposableHelper.DISPOSED);
        if (paramThrowable != DisposableHelper.DISPOSED) {
          try
          {
            this.actual.onError((Throwable)localObject);
            return;
          }
          finally
          {
            if (paramThrowable != null) {
              paramThrowable.dispose();
            }
          }
        }
      }
      RxJavaPlugins.onError(localThrowable);
    }
    
    public void onSuccess(T paramT)
    {
      if (get() != DisposableHelper.DISPOSED)
      {
        Disposable localDisposable = (Disposable)getAndSet(DisposableHelper.DISPOSED);
        if (localDisposable != DisposableHelper.DISPOSED)
        {
          if (paramT == null) {}
          try
          {
            paramT = this.actual;
            NullPointerException localNullPointerException = new java/lang/NullPointerException;
            localNullPointerException.<init>("onSuccess called with null. Null values are generally not allowed in 2.x operators and sources.");
            paramT.onError(localNullPointerException);
            break label67;
            this.actual.onSuccess(paramT);
          }
          finally
          {
            label67:
            if (localDisposable != null) {
              localDisposable.dispose();
            }
          }
        }
      }
    }
    
    public void setCancellable(Cancellable paramCancellable)
    {
      setDisposable(new CancellableDisposable(paramCancellable));
    }
    
    public void setDisposable(Disposable paramDisposable)
    {
      DisposableHelper.set(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleCreate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */