package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;

public final class SingleDoOnSuccess<T>
  extends Single<T>
{
  final Consumer<? super T> onSuccess;
  final SingleSource<T> source;
  
  public SingleDoOnSuccess(SingleSource<T> paramSingleSource, Consumer<? super T> paramConsumer)
  {
    this.source = paramSingleSource;
    this.onSuccess = paramConsumer;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        try
        {
          SingleDoOnSuccess.this.onSuccess.accept(paramAnonymousT);
          paramSingleObserver.onSuccess(paramAnonymousT);
          return;
        }
        catch (Throwable paramAnonymousT)
        {
          Exceptions.throwIfFatal(paramAnonymousT);
          paramSingleObserver.onError(paramAnonymousT);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoOnSuccess.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */