package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class SingleDoOnSubscribe<T>
  extends Single<T>
{
  final Consumer<? super Disposable> onSubscribe;
  final SingleSource<T> source;
  
  public SingleDoOnSubscribe(SingleSource<T> paramSingleSource, Consumer<? super Disposable> paramConsumer)
  {
    this.source = paramSingleSource;
    this.onSubscribe = paramConsumer;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new DoOnSubscribeSingleObserver(paramSingleObserver, this.onSubscribe));
  }
  
  static final class DoOnSubscribeSingleObserver<T>
    implements SingleObserver<T>
  {
    final SingleObserver<? super T> actual;
    boolean done;
    final Consumer<? super Disposable> onSubscribe;
    
    DoOnSubscribeSingleObserver(SingleObserver<? super T> paramSingleObserver, Consumer<? super Disposable> paramConsumer)
    {
      this.actual = paramSingleObserver;
      this.onSubscribe = paramConsumer;
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      try
      {
        this.onSubscribe.accept(paramDisposable);
        this.actual.onSubscribe(paramDisposable);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.done = true;
        paramDisposable.dispose();
        EmptyDisposable.error(localThrowable, this.actual);
      }
    }
    
    public void onSuccess(T paramT)
    {
      if (this.done) {
        return;
      }
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoOnSubscribe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */