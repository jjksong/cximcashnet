package io.reactivex.internal.operators.single;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.observers.ResumeSingleObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleDelayWithObservable<T, U>
  extends Single<T>
{
  final ObservableSource<U> other;
  final SingleSource<T> source;
  
  public SingleDelayWithObservable(SingleSource<T> paramSingleSource, ObservableSource<U> paramObservableSource)
  {
    this.source = paramSingleSource;
    this.other = paramObservableSource;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.other.subscribe(new OtherSubscriber(paramSingleObserver, this.source));
  }
  
  static final class OtherSubscriber<T, U>
    extends AtomicReference<Disposable>
    implements Observer<U>, Disposable
  {
    private static final long serialVersionUID = -8565274649390031272L;
    final SingleObserver<? super T> actual;
    boolean done;
    final SingleSource<T> source;
    
    OtherSubscriber(SingleObserver<? super T> paramSingleObserver, SingleSource<T> paramSingleSource)
    {
      this.actual = paramSingleObserver;
      this.source = paramSingleSource;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.source.subscribe(new ResumeSingleObserver(this, this.actual));
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(U paramU)
    {
      ((Disposable)get()).dispose();
      onComplete();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.set(this, paramDisposable)) {
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDelayWithObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */