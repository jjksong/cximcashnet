package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

@Experimental
public final class SingleDoAfterSuccess<T>
  extends Single<T>
{
  final Consumer<? super T> onAfterSuccess;
  final SingleSource<T> source;
  
  public SingleDoAfterSuccess(SingleSource<T> paramSingleSource, Consumer<? super T> paramConsumer)
  {
    this.source = paramSingleSource;
    this.onAfterSuccess = paramConsumer;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new DoAfterObserver(paramSingleObserver, this.onAfterSuccess));
  }
  
  static final class DoAfterObserver<T>
    implements SingleObserver<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    Disposable d;
    final Consumer<? super T> onAfterSuccess;
    
    DoAfterObserver(SingleObserver<? super T> paramSingleObserver, Consumer<? super T> paramConsumer)
    {
      this.actual = paramSingleObserver;
      this.onAfterSuccess = paramConsumer;
    }
    
    public void dispose()
    {
      this.d.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
      try
      {
        this.onAfterSuccess.accept(paramT);
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        RxJavaPlugins.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoAfterSuccess.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */