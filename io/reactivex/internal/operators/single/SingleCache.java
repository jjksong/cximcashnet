package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleCache<T>
  extends Single<T>
  implements SingleObserver<T>
{
  static final CacheDisposable[] EMPTY = new CacheDisposable[0];
  static final CacheDisposable[] TERMINATED = new CacheDisposable[0];
  Throwable error;
  final AtomicReference<CacheDisposable<T>[]> observers;
  final SingleSource<? extends T> source;
  T value;
  final AtomicInteger wip;
  
  public SingleCache(SingleSource<? extends T> paramSingleSource)
  {
    this.source = paramSingleSource;
    this.wip = new AtomicInteger();
    this.observers = new AtomicReference(EMPTY);
  }
  
  boolean add(CacheDisposable<T> paramCacheDisposable)
  {
    CacheDisposable[] arrayOfCacheDisposable2;
    CacheDisposable[] arrayOfCacheDisposable1;
    do
    {
      arrayOfCacheDisposable2 = (CacheDisposable[])this.observers.get();
      if (arrayOfCacheDisposable2 == TERMINATED) {
        return false;
      }
      int i = arrayOfCacheDisposable2.length;
      arrayOfCacheDisposable1 = new CacheDisposable[i + 1];
      System.arraycopy(arrayOfCacheDisposable2, 0, arrayOfCacheDisposable1, 0, i);
      arrayOfCacheDisposable1[i] = paramCacheDisposable;
    } while (!this.observers.compareAndSet(arrayOfCacheDisposable2, arrayOfCacheDisposable1));
    return true;
  }
  
  public void onError(Throwable paramThrowable)
  {
    this.error = paramThrowable;
    for (CacheDisposable localCacheDisposable : (CacheDisposable[])this.observers.getAndSet(TERMINATED)) {
      if (!localCacheDisposable.isDisposed()) {
        localCacheDisposable.actual.onError(paramThrowable);
      }
    }
  }
  
  public void onSubscribe(Disposable paramDisposable) {}
  
  public void onSuccess(T paramT)
  {
    this.value = paramT;
    for (CacheDisposable localCacheDisposable : (CacheDisposable[])this.observers.getAndSet(TERMINATED)) {
      if (!localCacheDisposable.isDisposed()) {
        localCacheDisposable.actual.onSuccess(paramT);
      }
    }
  }
  
  void remove(CacheDisposable<T> paramCacheDisposable)
  {
    CacheDisposable[] arrayOfCacheDisposable2;
    CacheDisposable[] arrayOfCacheDisposable1;
    do
    {
      arrayOfCacheDisposable2 = (CacheDisposable[])this.observers.get();
      int m = arrayOfCacheDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfCacheDisposable2[i] == paramCacheDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfCacheDisposable1 = EMPTY;
      }
      else
      {
        arrayOfCacheDisposable1 = new CacheDisposable[m - 1];
        System.arraycopy(arrayOfCacheDisposable2, 0, arrayOfCacheDisposable1, 0, j);
        System.arraycopy(arrayOfCacheDisposable2, j + 1, arrayOfCacheDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfCacheDisposable2, arrayOfCacheDisposable1));
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    Object localObject = new CacheDisposable(paramSingleObserver, this);
    paramSingleObserver.onSubscribe((Disposable)localObject);
    if (add((CacheDisposable)localObject))
    {
      if (((CacheDisposable)localObject).isDisposed()) {
        remove((CacheDisposable)localObject);
      }
      if (this.wip.getAndIncrement() == 0) {
        this.source.subscribe(this);
      }
      return;
    }
    localObject = this.error;
    if (localObject != null) {
      paramSingleObserver.onError((Throwable)localObject);
    } else {
      paramSingleObserver.onSuccess(this.value);
    }
  }
  
  static final class CacheDisposable<T>
    extends AtomicBoolean
    implements Disposable
  {
    private static final long serialVersionUID = 7514387411091976596L;
    final SingleObserver<? super T> actual;
    final SingleCache<T> parent;
    
    CacheDisposable(SingleObserver<? super T> paramSingleObserver, SingleCache<T> paramSingleCache)
    {
      this.actual = paramSingleObserver;
      this.parent = paramSingleCache;
    }
    
    public void dispose()
    {
      if (compareAndSet(false, true)) {
        this.parent.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      return get();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */