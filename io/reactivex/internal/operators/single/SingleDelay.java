package io.reactivex.internal.operators.single;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import java.util.concurrent.TimeUnit;

public final class SingleDelay<T>
  extends Single<T>
{
  final Scheduler scheduler;
  final SingleSource<? extends T> source;
  final long time;
  final TimeUnit unit;
  
  public SingleDelay(SingleSource<? extends T> paramSingleSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    this.source = paramSingleSource;
    this.time = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    final SequentialDisposable localSequentialDisposable = new SequentialDisposable();
    paramSingleObserver.onSubscribe(localSequentialDisposable);
    this.source.subscribe(new SingleObserver()
    {
      public void onError(final Throwable paramAnonymousThrowable)
      {
        localSequentialDisposable.replace(SingleDelay.this.scheduler.scheduleDirect(new Runnable()
        {
          public void run()
          {
            SingleDelay.1.this.val$s.onError(paramAnonymousThrowable);
          }
        }, 0L, SingleDelay.this.unit));
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localSequentialDisposable.replace(paramAnonymousDisposable);
      }
      
      public void onSuccess(final T paramAnonymousT)
      {
        localSequentialDisposable.replace(SingleDelay.this.scheduler.scheduleDirect(new Runnable()
        {
          public void run()
          {
            SingleDelay.1.this.val$s.onSuccess(paramAnonymousT);
          }
        }, SingleDelay.this.time, SingleDelay.this.unit));
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDelay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */