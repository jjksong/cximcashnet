package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;

public final class SingleDoOnError<T>
  extends Single<T>
{
  final Consumer<? super Throwable> onError;
  final SingleSource<T> source;
  
  public SingleDoOnError(SingleSource<T> paramSingleSource, Consumer<? super Throwable> paramConsumer)
  {
    this.source = paramSingleSource;
    this.onError = paramConsumer;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleObserver()
    {
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          SingleDoOnError.this.onError.accept(paramAnonymousThrowable);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramAnonymousThrowable = new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable });
        }
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        paramSingleObserver.onSuccess(paramAnonymousT);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoOnError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */