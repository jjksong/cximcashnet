package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleDoOnDispose<T>
  extends Single<T>
{
  final Action onDispose;
  final SingleSource<T> source;
  
  public SingleDoOnDispose(SingleSource<T> paramSingleSource, Action paramAction)
  {
    this.source = paramSingleSource;
    this.onDispose = paramAction;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new DoOnDisposeObserver(paramSingleObserver, this.onDispose));
  }
  
  static final class DoOnDisposeObserver<T>
    extends AtomicReference<Action>
    implements SingleObserver<T>, Disposable
  {
    private static final long serialVersionUID = -8583764624474935784L;
    final SingleObserver<? super T> actual;
    Disposable d;
    
    DoOnDisposeObserver(SingleObserver<? super T> paramSingleObserver, Action paramAction)
    {
      this.actual = paramSingleObserver;
      lazySet(paramAction);
    }
    
    public void dispose()
    {
      Action localAction = (Action)getAndSet(null);
      if (localAction != null)
      {
        try
        {
          localAction.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
        this.d.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    public void onSuccess(T paramT)
    {
      this.actual.onSuccess(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleDoOnDispose.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */