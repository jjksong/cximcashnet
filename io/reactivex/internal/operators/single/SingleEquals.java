package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;

public final class SingleEquals<T>
  extends Single<Boolean>
{
  final SingleSource<? extends T> first;
  final SingleSource<? extends T> second;
  
  public SingleEquals(SingleSource<? extends T> paramSingleSource1, SingleSource<? extends T> paramSingleSource2)
  {
    this.first = paramSingleSource1;
    this.second = paramSingleSource2;
  }
  
  protected void subscribeActual(final SingleObserver<? super Boolean> paramSingleObserver)
  {
    final AtomicInteger localAtomicInteger = new AtomicInteger();
    final Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = null;
    arrayOfObject[1] = null;
    final CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    paramSingleObserver.onSubscribe(localCompositeDisposable);
    this.first.subscribe(new SingleObserver()
    {
      final int index;
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        int i;
        do
        {
          i = localAtomicInteger.get();
          if (i >= 2)
          {
            RxJavaPlugins.onError(paramAnonymousThrowable);
            return;
          }
        } while (!localAtomicInteger.compareAndSet(i, 2));
        localCompositeDisposable.dispose();
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        arrayOfObject[this.index] = paramAnonymousT;
        if (localAtomicInteger.incrementAndGet() == 2)
        {
          paramAnonymousT = paramSingleObserver;
          Object[] arrayOfObject = arrayOfObject;
          paramAnonymousT.onSuccess(Boolean.valueOf(ObjectHelper.equals(arrayOfObject[0], arrayOfObject[1])));
        }
      }
    });
    this.second.subscribe(new SingleObserver()
    {
      final int index;
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        int i;
        do
        {
          i = localAtomicInteger.get();
          if (i >= 2)
          {
            RxJavaPlugins.onError(paramAnonymousThrowable);
            return;
          }
        } while (!localAtomicInteger.compareAndSet(i, 2));
        localCompositeDisposable.dispose();
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
      }
      
      public void onSuccess(T paramAnonymousT)
      {
        arrayOfObject[this.index] = paramAnonymousT;
        if (localAtomicInteger.incrementAndGet() == 2)
        {
          paramAnonymousT = paramSingleObserver;
          Object[] arrayOfObject = arrayOfObject;
          paramAnonymousT.onSuccess(Boolean.valueOf(ObjectHelper.equals(arrayOfObject[0], arrayOfObject[1])));
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/single/SingleEquals.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */