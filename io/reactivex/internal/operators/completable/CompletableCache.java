package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public final class CompletableCache
  extends Completable
  implements CompletableObserver
{
  static final InnerCompletableCache[] EMPTY = new InnerCompletableCache[0];
  static final InnerCompletableCache[] TERMINATED = new InnerCompletableCache[0];
  Throwable error;
  final AtomicReference<InnerCompletableCache[]> observers;
  final AtomicBoolean once;
  final CompletableSource source;
  
  public CompletableCache(CompletableSource paramCompletableSource)
  {
    this.source = paramCompletableSource;
    this.observers = new AtomicReference(EMPTY);
    this.once = new AtomicBoolean();
  }
  
  boolean add(InnerCompletableCache paramInnerCompletableCache)
  {
    InnerCompletableCache[] arrayOfInnerCompletableCache1;
    InnerCompletableCache[] arrayOfInnerCompletableCache2;
    do
    {
      arrayOfInnerCompletableCache1 = (InnerCompletableCache[])this.observers.get();
      if (arrayOfInnerCompletableCache1 == TERMINATED) {
        return false;
      }
      int i = arrayOfInnerCompletableCache1.length;
      arrayOfInnerCompletableCache2 = new InnerCompletableCache[i + 1];
      System.arraycopy(arrayOfInnerCompletableCache1, 0, arrayOfInnerCompletableCache2, 0, i);
      arrayOfInnerCompletableCache2[i] = paramInnerCompletableCache;
    } while (!this.observers.compareAndSet(arrayOfInnerCompletableCache1, arrayOfInnerCompletableCache2));
    return true;
  }
  
  public void onComplete()
  {
    for (InnerCompletableCache localInnerCompletableCache : (InnerCompletableCache[])this.observers.getAndSet(TERMINATED)) {
      if (!localInnerCompletableCache.get()) {
        localInnerCompletableCache.actual.onComplete();
      }
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    this.error = paramThrowable;
    for (InnerCompletableCache localInnerCompletableCache : (InnerCompletableCache[])this.observers.getAndSet(TERMINATED)) {
      if (!localInnerCompletableCache.get()) {
        localInnerCompletableCache.actual.onError(paramThrowable);
      }
    }
  }
  
  public void onSubscribe(Disposable paramDisposable) {}
  
  void remove(InnerCompletableCache paramInnerCompletableCache)
  {
    InnerCompletableCache[] arrayOfInnerCompletableCache2;
    InnerCompletableCache[] arrayOfInnerCompletableCache1;
    do
    {
      arrayOfInnerCompletableCache2 = (InnerCompletableCache[])this.observers.get();
      int m = arrayOfInnerCompletableCache2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfInnerCompletableCache2[i] == paramInnerCompletableCache)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfInnerCompletableCache1 = EMPTY;
      }
      else
      {
        arrayOfInnerCompletableCache1 = new InnerCompletableCache[m - 1];
        System.arraycopy(arrayOfInnerCompletableCache2, 0, arrayOfInnerCompletableCache1, 0, j);
        System.arraycopy(arrayOfInnerCompletableCache2, j + 1, arrayOfInnerCompletableCache1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfInnerCompletableCache2, arrayOfInnerCompletableCache1));
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    Object localObject = new InnerCompletableCache(paramCompletableObserver);
    paramCompletableObserver.onSubscribe((Disposable)localObject);
    if (add((InnerCompletableCache)localObject))
    {
      if (((InnerCompletableCache)localObject).isDisposed()) {
        remove((InnerCompletableCache)localObject);
      }
      if (this.once.compareAndSet(false, true)) {
        this.source.subscribe(this);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null) {
        paramCompletableObserver.onError((Throwable)localObject);
      } else {
        paramCompletableObserver.onComplete();
      }
    }
  }
  
  final class InnerCompletableCache
    extends AtomicBoolean
    implements Disposable
  {
    private static final long serialVersionUID = 8943152917179642732L;
    final CompletableObserver actual;
    
    InnerCompletableCache(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      if (compareAndSet(false, true)) {
        CompletableCache.this.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      return get();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */