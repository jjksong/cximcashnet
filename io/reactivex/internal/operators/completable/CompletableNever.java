package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.internal.disposables.EmptyDisposable;

public final class CompletableNever
  extends Completable
{
  public static final Completable INSTANCE = new CompletableNever();
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    paramCompletableObserver.onSubscribe(EmptyDisposable.NEVER);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableNever.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */