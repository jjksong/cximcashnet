package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class CompletableUsing<R>
  extends Completable
{
  final Function<? super R, ? extends CompletableSource> completableFunction;
  final Consumer<? super R> disposer;
  final boolean eager;
  final Callable<R> resourceSupplier;
  
  public CompletableUsing(Callable<R> paramCallable, Function<? super R, ? extends CompletableSource> paramFunction, Consumer<? super R> paramConsumer, boolean paramBoolean)
  {
    this.resourceSupplier = paramCallable;
    this.completableFunction = paramFunction;
    this.disposer = paramConsumer;
    this.eager = paramBoolean;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    try
    {
      Object localObject = this.resourceSupplier.call();
      try
      {
        CompletableSource localCompletableSource = (CompletableSource)ObjectHelper.requireNonNull(this.completableFunction.apply(localObject), "The completableFunction returned a null CompletableSource");
        localCompletableSource.subscribe(new UsingObserver(paramCompletableObserver, localObject, this.disposer, this.eager));
        return;
      }
      catch (Throwable localThrowable3)
      {
        Exceptions.throwIfFatal(localThrowable3);
        if (this.eager) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            EmptyDisposable.error(new CompositeException(new Throwable[] { localThrowable3, localThrowable1 }), paramCompletableObserver);
            return;
          }
        }
        EmptyDisposable.error(localThrowable3, paramCompletableObserver);
        if (!this.eager) {
          try
          {
            this.disposer.accept(localThrowable1);
          }
          catch (Throwable paramCompletableObserver)
          {
            Exceptions.throwIfFatal(paramCompletableObserver);
            RxJavaPlugins.onError(paramCompletableObserver);
          }
        }
        return;
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptyDisposable.error(localThrowable2, paramCompletableObserver);
    }
  }
  
  static final class UsingObserver<R>
    extends AtomicReference<Object>
    implements CompletableObserver, Disposable
  {
    private static final long serialVersionUID = -674404550052917487L;
    final CompletableObserver actual;
    Disposable d;
    final Consumer<? super R> disposer;
    final boolean eager;
    
    UsingObserver(CompletableObserver paramCompletableObserver, R paramR, Consumer<? super R> paramConsumer, boolean paramBoolean)
    {
      super();
      this.actual = paramCompletableObserver;
      this.disposer = paramConsumer;
      this.eager = paramBoolean;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
      disposeResourceAfter();
    }
    
    void disposeResourceAfter()
    {
      Object localObject = getAndSet(this);
      if (localObject != this) {
        try
        {
          this.disposer.accept(localObject);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.d = DisposableHelper.DISPOSED;
      if (this.eager)
      {
        Object localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.actual.onError(localThrowable);
            return;
          }
        } else {
          return;
        }
      }
      this.actual.onComplete();
      if (!this.eager) {
        disposeResourceAfter();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.d = DisposableHelper.DISPOSED;
      Object localObject = paramThrowable;
      CompositeException localCompositeException;
      if (this.eager)
      {
        localObject = getAndSet(this);
        if (localObject != this) {
          try
          {
            this.disposer.accept(localObject);
            localObject = paramThrowable;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            localCompositeException = new CompositeException(new Throwable[] { paramThrowable, localThrowable });
          }
        } else {
          return;
        }
      }
      this.actual.onError(localCompositeException);
      if (!this.eager) {
        disposeResourceAfter();
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableUsing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */