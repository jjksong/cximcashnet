package io.reactivex.internal.operators.completable;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public final class CompletableToObservable<T>
  extends Observable<T>
{
  final CompletableSource source;
  
  public CompletableToObservable(CompletableSource paramCompletableSource)
  {
    this.source = paramCompletableSource;
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.source.subscribe(new ObserverCompletableObserver(paramObserver));
  }
  
  static final class ObserverCompletableObserver
    implements CompletableObserver
  {
    private final Observer<?> observer;
    
    ObserverCompletableObserver(Observer<?> paramObserver)
    {
      this.observer = paramObserver;
    }
    
    public void onComplete()
    {
      this.observer.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.observer.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.observer.onSubscribe(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableToObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */