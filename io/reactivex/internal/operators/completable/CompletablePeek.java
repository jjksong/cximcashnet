package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class CompletablePeek
  extends Completable
{
  final Action onAfterTerminate;
  final Action onComplete;
  final Action onDispose;
  final Consumer<? super Throwable> onError;
  final Consumer<? super Disposable> onSubscribe;
  final Action onTerminate;
  final CompletableSource source;
  
  public CompletablePeek(CompletableSource paramCompletableSource, Consumer<? super Disposable> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction1, Action paramAction2, Action paramAction3, Action paramAction4)
  {
    this.source = paramCompletableSource;
    this.onSubscribe = paramConsumer;
    this.onError = paramConsumer1;
    this.onComplete = paramAction1;
    this.onTerminate = paramAction2;
    this.onAfterTerminate = paramAction3;
    this.onDispose = paramAction4;
  }
  
  protected void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new CompletableObserver()
    {
      void doAfter()
      {
        try
        {
          CompletablePeek.this.onAfterTerminate.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
      
      public void onComplete()
      {
        try
        {
          CompletablePeek.this.onComplete.run();
          CompletablePeek.this.onTerminate.run();
          paramCompletableObserver.onComplete();
          doAfter();
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramCompletableObserver.onError(localThrowable);
        }
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          CompletablePeek.this.onError.accept(paramAnonymousThrowable);
          CompletablePeek.this.onTerminate.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramAnonymousThrowable = new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable });
        }
        paramCompletableObserver.onError(paramAnonymousThrowable);
        doAfter();
      }
      
      public void onSubscribe(final Disposable paramAnonymousDisposable)
      {
        try
        {
          CompletablePeek.this.onSubscribe.accept(paramAnonymousDisposable);
          paramCompletableObserver.onSubscribe(Disposables.fromRunnable(new Runnable()
          {
            public void run()
            {
              try
              {
                CompletablePeek.this.onDispose.run();
              }
              catch (Throwable localThrowable)
              {
                Exceptions.throwIfFatal(localThrowable);
                RxJavaPlugins.onError(localThrowable);
              }
              paramAnonymousDisposable.dispose();
            }
          }));
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramAnonymousDisposable.dispose();
          EmptyDisposable.error(localThrowable, paramCompletableObserver);
        }
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletablePeek.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */