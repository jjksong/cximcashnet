package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class CompletableFromPublisher<T>
  extends Completable
{
  final Publisher<T> flowable;
  
  public CompletableFromPublisher(Publisher<T> paramPublisher)
  {
    this.flowable = paramPublisher;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.flowable.subscribe(new FromPublisherSubscriber(paramCompletableObserver));
  }
  
  static final class FromPublisherSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final CompletableObserver cs;
    Subscription s;
    
    FromPublisherSubscriber(CompletableObserver paramCompletableObserver)
    {
      this.cs = paramCompletableObserver;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.cs.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.cs.onError(paramThrowable);
    }
    
    public void onNext(T paramT) {}
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.cs.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableFromPublisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */