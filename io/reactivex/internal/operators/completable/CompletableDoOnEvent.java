package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;

public final class CompletableDoOnEvent
  extends Completable
{
  final Consumer<? super Throwable> onEvent;
  final CompletableSource source;
  
  public CompletableDoOnEvent(CompletableSource paramCompletableSource, Consumer<? super Throwable> paramConsumer)
  {
    this.source = paramCompletableSource;
    this.onEvent = paramConsumer;
  }
  
  protected void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        try
        {
          CompletableDoOnEvent.this.onEvent.accept(null);
          paramCompletableObserver.onComplete();
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramCompletableObserver.onError(localThrowable);
        }
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          CompletableDoOnEvent.this.onEvent.accept(paramAnonymousThrowable);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramAnonymousThrowable = new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable });
        }
        paramCompletableObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramCompletableObserver.onSubscribe(paramAnonymousDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableDoOnEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */