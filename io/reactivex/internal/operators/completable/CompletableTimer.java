package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class CompletableTimer
  extends Completable
{
  final long delay;
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public CompletableTimer(long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    this.delay = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    TimerDisposable localTimerDisposable = new TimerDisposable(paramCompletableObserver);
    paramCompletableObserver.onSubscribe(localTimerDisposable);
    localTimerDisposable.setFuture(this.scheduler.scheduleDirect(localTimerDisposable, this.delay, this.unit));
  }
  
  static final class TimerDisposable
    extends AtomicReference<Disposable>
    implements Disposable, Runnable
  {
    private static final long serialVersionUID = 3167244060586201109L;
    final CompletableObserver actual;
    
    TimerDisposable(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)get());
    }
    
    public void run()
    {
      this.actual.onComplete();
    }
    
    void setFuture(Disposable paramDisposable)
    {
      DisposableHelper.replace(this, paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableTimer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */