package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.AtomicThrowable;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public final class CompletableMergeDelayErrorIterable
  extends Completable
{
  final Iterable<? extends CompletableSource> sources;
  
  public CompletableMergeDelayErrorIterable(Iterable<? extends CompletableSource> paramIterable)
  {
    this.sources = paramIterable;
  }
  
  public void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    paramCompletableObserver.onSubscribe(localCompositeDisposable);
    try
    {
      Iterator localIterator = (Iterator)ObjectHelper.requireNonNull(this.sources.iterator(), "The source iterator returned is null");
      Object localObject = new AtomicInteger(1);
      AtomicThrowable localAtomicThrowable = new AtomicThrowable();
      for (;;)
      {
        if (localCompositeDisposable.isDisposed()) {
          return;
        }
        try
        {
          boolean bool = localIterator.hasNext();
          if (bool)
          {
            if (localCompositeDisposable.isDisposed()) {
              return;
            }
            try
            {
              CompletableSource localCompletableSource = (CompletableSource)ObjectHelper.requireNonNull(localIterator.next(), "The iterator returned a null CompletableSource");
              if (localCompositeDisposable.isDisposed()) {
                return;
              }
              ((AtomicInteger)localObject).getAndIncrement();
              localCompletableSource.subscribe(new CompletableMergeDelayErrorArray.MergeInnerCompletableObserver(paramCompletableObserver, localCompositeDisposable, localAtomicThrowable, (AtomicInteger)localObject));
            }
            catch (Throwable localThrowable2)
            {
              Exceptions.throwIfFatal(localThrowable2);
              localAtomicThrowable.addThrowable(localThrowable2);
            }
          }
          else
          {
            if (((AtomicInteger)localObject).decrementAndGet() != 0) {
              break label207;
            }
          }
        }
        catch (Throwable localThrowable3)
        {
          Exceptions.throwIfFatal(localThrowable3);
          localAtomicThrowable.addThrowable(localThrowable3);
        }
      }
      localObject = localAtomicThrowable.terminate();
      if (localObject == null) {
        paramCompletableObserver.onComplete();
      } else {
        paramCompletableObserver.onError((Throwable)localObject);
      }
      label207:
      return;
    }
    catch (Throwable localThrowable1)
    {
      Exceptions.throwIfFatal(localThrowable1);
      paramCompletableObserver.onError(localThrowable1);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableMergeDelayErrorIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */