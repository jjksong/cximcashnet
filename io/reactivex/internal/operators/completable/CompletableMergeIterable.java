package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class CompletableMergeIterable
  extends Completable
{
  final Iterable<? extends CompletableSource> sources;
  
  public CompletableMergeIterable(Iterable<? extends CompletableSource> paramIterable)
  {
    this.sources = paramIterable;
  }
  
  /* Error */
  public void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    // Byte code:
    //   0: new 26	io/reactivex/disposables/CompositeDisposable
    //   3: dup
    //   4: invokespecial 27	io/reactivex/disposables/CompositeDisposable:<init>	()V
    //   7: astore_3
    //   8: aload_1
    //   9: aload_3
    //   10: invokeinterface 33 2 0
    //   15: aload_0
    //   16: getfield 17	io/reactivex/internal/operators/completable/CompletableMergeIterable:sources	Ljava/lang/Iterable;
    //   19: invokeinterface 39 1 0
    //   24: ldc 41
    //   26: invokestatic 47	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   29: checkcast 49	java/util/Iterator
    //   32: astore 4
    //   34: new 51	java/util/concurrent/atomic/AtomicInteger
    //   37: dup
    //   38: iconst_1
    //   39: invokespecial 54	java/util/concurrent/atomic/AtomicInteger:<init>	(I)V
    //   42: astore 5
    //   44: new 6	io/reactivex/internal/operators/completable/CompletableMergeIterable$MergeCompletableObserver
    //   47: dup
    //   48: aload_1
    //   49: aload_3
    //   50: aload 5
    //   52: invokespecial 57	io/reactivex/internal/operators/completable/CompletableMergeIterable$MergeCompletableObserver:<init>	(Lio/reactivex/CompletableObserver;Lio/reactivex/disposables/CompositeDisposable;Ljava/util/concurrent/atomic/AtomicInteger;)V
    //   55: astore_1
    //   56: aload_3
    //   57: invokevirtual 61	io/reactivex/disposables/CompositeDisposable:isDisposed	()Z
    //   60: ifeq +4 -> 64
    //   63: return
    //   64: aload 4
    //   66: invokeinterface 64 1 0
    //   71: istore_2
    //   72: iload_2
    //   73: ifne +8 -> 81
    //   76: aload_1
    //   77: invokevirtual 67	io/reactivex/internal/operators/completable/CompletableMergeIterable$MergeCompletableObserver:onComplete	()V
    //   80: return
    //   81: aload_3
    //   82: invokevirtual 61	io/reactivex/disposables/CompositeDisposable:isDisposed	()Z
    //   85: ifeq +4 -> 89
    //   88: return
    //   89: aload 4
    //   91: invokeinterface 71 1 0
    //   96: ldc 73
    //   98: invokestatic 47	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   101: checkcast 75	io/reactivex/CompletableSource
    //   104: astore 6
    //   106: aload_3
    //   107: invokevirtual 61	io/reactivex/disposables/CompositeDisposable:isDisposed	()Z
    //   110: ifeq +4 -> 114
    //   113: return
    //   114: aload 5
    //   116: invokevirtual 79	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
    //   119: pop
    //   120: aload 6
    //   122: aload_1
    //   123: invokeinterface 82 2 0
    //   128: goto -72 -> 56
    //   131: astore 4
    //   133: aload 4
    //   135: invokestatic 88	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   138: aload_3
    //   139: invokevirtual 91	io/reactivex/disposables/CompositeDisposable:dispose	()V
    //   142: aload_1
    //   143: aload 4
    //   145: invokevirtual 94	io/reactivex/internal/operators/completable/CompletableMergeIterable$MergeCompletableObserver:onError	(Ljava/lang/Throwable;)V
    //   148: return
    //   149: astore 4
    //   151: aload 4
    //   153: invokestatic 88	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   156: aload_3
    //   157: invokevirtual 91	io/reactivex/disposables/CompositeDisposable:dispose	()V
    //   160: aload_1
    //   161: aload 4
    //   163: invokevirtual 94	io/reactivex/internal/operators/completable/CompletableMergeIterable$MergeCompletableObserver:onError	(Ljava/lang/Throwable;)V
    //   166: return
    //   167: astore_3
    //   168: aload_3
    //   169: invokestatic 88	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   172: aload_1
    //   173: aload_3
    //   174: invokeinterface 95 2 0
    //   179: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	180	0	this	CompletableMergeIterable
    //   0	180	1	paramCompletableObserver	CompletableObserver
    //   71	2	2	bool	boolean
    //   7	150	3	localCompositeDisposable	CompositeDisposable
    //   167	7	3	localThrowable1	Throwable
    //   32	58	4	localIterator	java.util.Iterator
    //   131	13	4	localThrowable2	Throwable
    //   149	13	4	localThrowable3	Throwable
    //   42	73	5	localAtomicInteger	AtomicInteger
    //   104	17	6	localCompletableSource	CompletableSource
    // Exception table:
    //   from	to	target	type
    //   89	106	131	java/lang/Throwable
    //   64	72	149	java/lang/Throwable
    //   15	34	167	java/lang/Throwable
  }
  
  static final class MergeCompletableObserver
    extends AtomicBoolean
    implements CompletableObserver
  {
    private static final long serialVersionUID = -7730517613164279224L;
    final CompletableObserver actual;
    final CompositeDisposable set;
    final AtomicInteger wip;
    
    MergeCompletableObserver(CompletableObserver paramCompletableObserver, CompositeDisposable paramCompositeDisposable, AtomicInteger paramAtomicInteger)
    {
      this.actual = paramCompletableObserver;
      this.set = paramCompositeDisposable;
      this.wip = paramAtomicInteger;
    }
    
    public void onComplete()
    {
      if ((this.wip.decrementAndGet() == 0) && (compareAndSet(false, true))) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.set.dispose();
      if (compareAndSet(false, true)) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.set.add(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableMergeIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */