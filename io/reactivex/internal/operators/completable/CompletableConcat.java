package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class CompletableConcat
  extends Completable
{
  final int prefetch;
  final Publisher<? extends CompletableSource> sources;
  
  public CompletableConcat(Publisher<? extends CompletableSource> paramPublisher, int paramInt)
  {
    this.sources = paramPublisher;
    this.prefetch = paramInt;
  }
  
  public void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.sources.subscribe(new CompletableConcatSubscriber(paramCompletableObserver, this.prefetch));
  }
  
  static final class CompletableConcatSubscriber
    extends AtomicInteger
    implements Subscriber<CompletableSource>, Disposable
  {
    private static final long serialVersionUID = 9032184911934499404L;
    volatile boolean active;
    final CompletableObserver actual;
    int consumed;
    volatile boolean done;
    final ConcatInnerObserver inner;
    final int limit;
    final AtomicBoolean once;
    final int prefetch;
    SimpleQueue<CompletableSource> queue;
    Subscription s;
    int sourceFused;
    
    CompletableConcatSubscriber(CompletableObserver paramCompletableObserver, int paramInt)
    {
      this.actual = paramCompletableObserver;
      this.prefetch = paramInt;
      this.inner = new ConcatInnerObserver(this);
      this.once = new AtomicBoolean();
      this.limit = (paramInt - (paramInt >> 2));
    }
    
    public void dispose()
    {
      this.s.cancel();
      DisposableHelper.dispose(this.inner);
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      do
      {
        if (isDisposed()) {
          return;
        }
        if (!this.active)
        {
          boolean bool = this.done;
          try
          {
            CompletableSource localCompletableSource = (CompletableSource)this.queue.poll();
            int i;
            if (localCompletableSource == null) {
              i = 1;
            } else {
              i = 0;
            }
            if ((bool) && (i != 0))
            {
              if (this.once.compareAndSet(false, true)) {
                this.actual.onComplete();
              }
              return;
            }
            if (i == 0)
            {
              this.active = true;
              localCompletableSource.subscribe(this.inner);
              request();
            }
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            innerError(localThrowable);
            return;
          }
        }
      } while (decrementAndGet() != 0);
    }
    
    void innerComplete()
    {
      this.active = false;
      drain();
    }
    
    void innerError(Throwable paramThrowable)
    {
      if (this.once.compareAndSet(false, true))
      {
        this.s.cancel();
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public boolean isDisposed()
    {
      return DisposableHelper.isDisposed((Disposable)this.inner.get());
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.once.compareAndSet(false, true))
      {
        DisposableHelper.dispose(this.inner);
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(CompletableSource paramCompletableSource)
    {
      if ((this.sourceFused == 0) && (!this.queue.offer(paramCompletableSource)))
      {
        onError(new MissingBackpressureException());
        return;
      }
      drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        int i = this.prefetch;
        long l;
        if (i == Integer.MAX_VALUE) {
          l = Long.MAX_VALUE;
        } else {
          l = i;
        }
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceFused = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.actual.onSubscribe(this);
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceFused = i;
            this.queue = localQueueSubscription;
            this.actual.onSubscribe(this);
            paramSubscription.request(l);
            return;
          }
        }
        i = this.prefetch;
        if (i == Integer.MAX_VALUE) {
          this.queue = new SpscLinkedArrayQueue(Flowable.bufferSize());
        } else {
          this.queue = new SpscArrayQueue(i);
        }
        this.actual.onSubscribe(this);
        paramSubscription.request(l);
      }
    }
    
    void request()
    {
      if (this.sourceFused != 1)
      {
        int i = this.consumed + 1;
        if (i == this.limit)
        {
          this.consumed = 0;
          this.s.request(i);
        }
        else
        {
          this.consumed = i;
        }
      }
    }
    
    static final class ConcatInnerObserver
      extends AtomicReference<Disposable>
      implements CompletableObserver
    {
      private static final long serialVersionUID = -5454794857847146511L;
      final CompletableConcat.CompletableConcatSubscriber parent;
      
      ConcatInnerObserver(CompletableConcat.CompletableConcatSubscriber paramCompletableConcatSubscriber)
      {
        this.parent = paramCompletableConcatSubscriber;
      }
      
      public void onComplete()
      {
        this.parent.innerComplete();
      }
      
      public void onError(Throwable paramThrowable)
      {
        this.parent.innerError(paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.replace(this, paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableConcat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */