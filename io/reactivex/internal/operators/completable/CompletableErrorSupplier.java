package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class CompletableErrorSupplier
  extends Completable
{
  final Callable<? extends Throwable> errorSupplier;
  
  public CompletableErrorSupplier(Callable<? extends Throwable> paramCallable)
  {
    this.errorSupplier = paramCallable;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    try
    {
      Throwable localThrowable1 = (Throwable)ObjectHelper.requireNonNull(this.errorSupplier.call(), "The error returned is null");
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
    }
    EmptyDisposable.error(localThrowable2, paramCompletableObserver);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableErrorSupplier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */