package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.SequentialDisposable;

public final class CompletableResumeNext
  extends Completable
{
  final Function<? super Throwable, ? extends CompletableSource> errorMapper;
  final CompletableSource source;
  
  public CompletableResumeNext(CompletableSource paramCompletableSource, Function<? super Throwable, ? extends CompletableSource> paramFunction)
  {
    this.source = paramCompletableSource;
    this.errorMapper = paramFunction;
  }
  
  protected void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    final SequentialDisposable localSequentialDisposable = new SequentialDisposable();
    paramCompletableObserver.onSubscribe(localSequentialDisposable);
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        paramCompletableObserver.onComplete();
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          Object localObject = (CompletableSource)CompletableResumeNext.this.errorMapper.apply(paramAnonymousThrowable);
          if (localObject == null)
          {
            localObject = new NullPointerException("The CompletableConsumable returned is null");
            ((NullPointerException)localObject).initCause(paramAnonymousThrowable);
            paramCompletableObserver.onError((Throwable)localObject);
            return;
          }
          ((CompletableSource)localObject).subscribe(new CompletableObserver()
          {
            public void onComplete()
            {
              CompletableResumeNext.1.this.val$s.onComplete();
            }
            
            public void onError(Throwable paramAnonymous2Throwable)
            {
              CompletableResumeNext.1.this.val$s.onError(paramAnonymous2Throwable);
            }
            
            public void onSubscribe(Disposable paramAnonymous2Disposable)
            {
              CompletableResumeNext.1.this.val$sd.update(paramAnonymous2Disposable);
            }
          });
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramCompletableObserver.onError(new CompositeException(new Throwable[] { localThrowable, paramAnonymousThrowable }));
        }
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localSequentialDisposable.update(paramAnonymousDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableResumeNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */