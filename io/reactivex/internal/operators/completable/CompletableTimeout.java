package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CompletableTimeout
  extends Completable
{
  final CompletableSource other;
  final Scheduler scheduler;
  final CompletableSource source;
  final long timeout;
  final TimeUnit unit;
  
  public CompletableTimeout(CompletableSource paramCompletableSource1, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, CompletableSource paramCompletableSource2)
  {
    this.source = paramCompletableSource1;
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.other = paramCompletableSource2;
  }
  
  public void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    final CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    paramCompletableObserver.onSubscribe(localCompositeDisposable);
    final AtomicBoolean localAtomicBoolean = new AtomicBoolean();
    localCompositeDisposable.add(this.scheduler.scheduleDirect(new Runnable()
    {
      public void run()
      {
        if (localAtomicBoolean.compareAndSet(false, true))
        {
          localCompositeDisposable.clear();
          if (CompletableTimeout.this.other == null) {
            paramCompletableObserver.onError(new TimeoutException());
          } else {
            CompletableTimeout.this.other.subscribe(new CompletableObserver()
            {
              public void onComplete()
              {
                CompletableTimeout.1.this.val$set.dispose();
                CompletableTimeout.1.this.val$s.onComplete();
              }
              
              public void onError(Throwable paramAnonymous2Throwable)
              {
                CompletableTimeout.1.this.val$set.dispose();
                CompletableTimeout.1.this.val$s.onError(paramAnonymous2Throwable);
              }
              
              public void onSubscribe(Disposable paramAnonymous2Disposable)
              {
                CompletableTimeout.1.this.val$set.add(paramAnonymous2Disposable);
              }
            });
          }
        }
      }
    }, this.timeout, this.unit));
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        if (localAtomicBoolean.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramCompletableObserver.onComplete();
        }
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        if (localAtomicBoolean.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramCompletableObserver.onError(paramAnonymousThrowable);
        }
        else
        {
          RxJavaPlugins.onError(paramAnonymousThrowable);
        }
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableTimeout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */