package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableOperator;
import io.reactivex.CompletableSource;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.plugins.RxJavaPlugins;

public final class CompletableLift
  extends Completable
{
  final CompletableOperator onLift;
  final CompletableSource source;
  
  public CompletableLift(CompletableSource paramCompletableSource, CompletableOperator paramCompletableOperator)
  {
    this.source = paramCompletableSource;
    this.onLift = paramCompletableOperator;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    try
    {
      try
      {
        paramCompletableObserver = this.onLift.apply(paramCompletableObserver);
        this.source.subscribe(paramCompletableObserver);
      }
      catch (Throwable paramCompletableObserver)
      {
        Exceptions.throwIfFatal(paramCompletableObserver);
        RxJavaPlugins.onError(paramCompletableObserver);
      }
      return;
    }
    catch (NullPointerException paramCompletableObserver)
    {
      throw paramCompletableObserver;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableLift.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */