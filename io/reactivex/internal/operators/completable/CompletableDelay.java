package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.TimeUnit;

public final class CompletableDelay
  extends Completable
{
  final long delay;
  final boolean delayError;
  final Scheduler scheduler;
  final CompletableSource source;
  final TimeUnit unit;
  
  public CompletableDelay(CompletableSource paramCompletableSource, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, boolean paramBoolean)
  {
    this.source = paramCompletableSource;
    this.delay = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    final CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        localCompositeDisposable.add(CompletableDelay.this.scheduler.scheduleDirect(new Runnable()
        {
          public void run()
          {
            CompletableDelay.1.this.val$s.onComplete();
          }
        }, CompletableDelay.this.delay, CompletableDelay.this.unit));
      }
      
      public void onError(final Throwable paramAnonymousThrowable)
      {
        CompositeDisposable localCompositeDisposable = localCompositeDisposable;
        Scheduler localScheduler = CompletableDelay.this.scheduler;
        paramAnonymousThrowable = new Runnable()
        {
          public void run()
          {
            CompletableDelay.1.this.val$s.onError(paramAnonymousThrowable);
          }
        };
        long l;
        if (CompletableDelay.this.delayError) {
          l = CompletableDelay.this.delay;
        } else {
          l = 0L;
        }
        localCompositeDisposable.add(localScheduler.scheduleDirect(paramAnonymousThrowable, l, CompletableDelay.this.unit));
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
        paramCompletableObserver.onSubscribe(localCompositeDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableDelay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */