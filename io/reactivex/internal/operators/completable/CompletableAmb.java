package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CompletableAmb
  extends Completable
{
  private final CompletableSource[] sources;
  private final Iterable<? extends CompletableSource> sourcesIterable;
  
  public CompletableAmb(CompletableSource[] paramArrayOfCompletableSource, Iterable<? extends CompletableSource> paramIterable)
  {
    this.sources = paramArrayOfCompletableSource;
    this.sourcesIterable = paramIterable;
  }
  
  public void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    Object localObject2 = this.sources;
    int j;
    CompletableSource localCompletableSource;
    if (localObject2 == null)
    {
      Object localObject1 = new CompletableSource[8];
      try
      {
        localObject3 = this.sourcesIterable.iterator();
        i = 0;
        for (;;)
        {
          localObject2 = localObject1;
          j = i;
          if (!((Iterator)localObject3).hasNext()) {
            break;
          }
          localCompletableSource = (CompletableSource)((Iterator)localObject3).next();
          if (localCompletableSource == null)
          {
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("One of the sources is null");
            EmptyDisposable.error((Throwable)localObject1, paramCompletableObserver);
            return;
          }
          localObject2 = localObject1;
          if (i == localObject1.length)
          {
            localObject2 = new CompletableSource[(i >> 2) + i];
            System.arraycopy(localObject1, 0, localObject2, 0, i);
          }
          localObject2[i] = localCompletableSource;
          i++;
          localObject1 = localObject2;
        }
        j = localObject2.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptyDisposable.error(localThrowable, paramCompletableObserver);
        return;
      }
    }
    final CompositeDisposable localCompositeDisposable = new CompositeDisposable();
    paramCompletableObserver.onSubscribe(localCompositeDisposable);
    final Object localObject3 = new AtomicBoolean();
    CompletableObserver local1 = new CompletableObserver()
    {
      public void onComplete()
      {
        if (localObject3.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramCompletableObserver.onComplete();
        }
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        if (localObject3.compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramCompletableObserver.onError(paramAnonymousThrowable);
        }
        else
        {
          RxJavaPlugins.onError(paramAnonymousThrowable);
        }
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        localCompositeDisposable.add(paramAnonymousDisposable);
      }
    };
    for (int i = 0; i < j; i++)
    {
      localCompletableSource = localObject2[i];
      if (localCompositeDisposable.isDisposed()) {
        return;
      }
      if (localCompletableSource == null)
      {
        localObject2 = new NullPointerException("One of the sources is null");
        if (((AtomicBoolean)localObject3).compareAndSet(false, true))
        {
          localCompositeDisposable.dispose();
          paramCompletableObserver.onError((Throwable)localObject2);
        }
        else
        {
          RxJavaPlugins.onError((Throwable)localObject2);
        }
        return;
      }
      localCompletableSource.subscribe(local1);
    }
    if (j == 0) {
      paramCompletableObserver.onComplete();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableAmb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */