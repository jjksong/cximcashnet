package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;

@Experimental
public final class CompletableDoFinally
  extends Completable
{
  final Action onFinally;
  final CompletableSource source;
  
  public CompletableDoFinally(CompletableSource paramCompletableSource, Action paramAction)
  {
    this.source = paramCompletableSource;
    this.onFinally = paramAction;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new DoFinallyObserver(paramCompletableObserver, this.onFinally));
  }
  
  static final class DoFinallyObserver
    extends AtomicInteger
    implements CompletableObserver, Disposable
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final CompletableObserver actual;
    Disposable d;
    final Action onFinally;
    
    DoFinallyObserver(CompletableObserver paramCompletableObserver, Action paramAction)
    {
      this.actual = paramCompletableObserver;
      this.onFinally = paramAction;
    }
    
    public void dispose()
    {
      this.d.dispose();
      runFinally();
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      runFinally();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableDoFinally.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */