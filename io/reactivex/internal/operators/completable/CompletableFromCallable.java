package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.Exceptions;
import java.util.concurrent.Callable;

public final class CompletableFromCallable
  extends Completable
{
  final Callable<?> callable;
  
  public CompletableFromCallable(Callable<?> paramCallable)
  {
    this.callable = paramCallable;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    Disposable localDisposable = Disposables.empty();
    paramCompletableObserver.onSubscribe(localDisposable);
    try
    {
      this.callable.call();
      if (!localDisposable.isDisposed()) {
        paramCompletableObserver.onComplete();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      if (!localDisposable.isDisposed()) {
        paramCompletableObserver.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableFromCallable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */