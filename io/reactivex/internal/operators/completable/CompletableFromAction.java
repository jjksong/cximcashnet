package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;

public final class CompletableFromAction
  extends Completable
{
  final Action run;
  
  public CompletableFromAction(Action paramAction)
  {
    this.run = paramAction;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    Disposable localDisposable = Disposables.empty();
    paramCompletableObserver.onSubscribe(localDisposable);
    try
    {
      this.run.run();
      if (!localDisposable.isDisposed()) {
        paramCompletableObserver.onComplete();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      if (!localDisposable.isDisposed()) {
        paramCompletableObserver.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableFromAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */