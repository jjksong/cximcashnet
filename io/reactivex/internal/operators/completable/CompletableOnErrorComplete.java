package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;

public final class CompletableOnErrorComplete
  extends Completable
{
  final Predicate<? super Throwable> predicate;
  final CompletableSource source;
  
  public CompletableOnErrorComplete(CompletableSource paramCompletableSource, Predicate<? super Throwable> paramPredicate)
  {
    this.source = paramCompletableSource;
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(final CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        paramCompletableObserver.onComplete();
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        try
        {
          boolean bool = CompletableOnErrorComplete.this.predicate.test(paramAnonymousThrowable);
          if (bool) {
            paramCompletableObserver.onComplete();
          } else {
            paramCompletableObserver.onError(paramAnonymousThrowable);
          }
          return;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramCompletableObserver.onError(new CompositeException(new Throwable[] { paramAnonymousThrowable, localThrowable }));
        }
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramCompletableObserver.onSubscribe(paramAnonymousDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableOnErrorComplete.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */