package io.reactivex.internal.operators.completable;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import java.util.concurrent.Callable;

public final class CompletableToSingle<T>
  extends Single<T>
{
  final T completionValue;
  final Callable<? extends T> completionValueSupplier;
  final CompletableSource source;
  
  public CompletableToSingle(CompletableSource paramCompletableSource, Callable<? extends T> paramCallable, T paramT)
  {
    this.source = paramCompletableSource;
    this.completionValue = paramT;
    this.completionValueSupplier = paramCallable;
  }
  
  protected void subscribeActual(final SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new CompletableObserver()
    {
      public void onComplete()
      {
        Object localObject2;
        if (CompletableToSingle.this.completionValueSupplier != null) {
          try
          {
            Object localObject1 = CompletableToSingle.this.completionValueSupplier.call();
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            paramSingleObserver.onError(localThrowable);
            return;
          }
        } else {
          localObject2 = CompletableToSingle.this.completionValue;
        }
        if (localObject2 == null) {
          paramSingleObserver.onError(new NullPointerException("The value supplied is null"));
        } else {
          paramSingleObserver.onSuccess(localObject2);
        }
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        paramSingleObserver.onError(paramAnonymousThrowable);
      }
      
      public void onSubscribe(Disposable paramAnonymousDisposable)
      {
        paramSingleObserver.onSubscribe(paramAnonymousDisposable);
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableToSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */