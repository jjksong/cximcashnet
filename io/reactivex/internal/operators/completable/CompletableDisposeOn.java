package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class CompletableDisposeOn
  extends Completable
{
  final Scheduler scheduler;
  final CompletableSource source;
  
  public CompletableDisposeOn(CompletableSource paramCompletableSource, Scheduler paramScheduler)
  {
    this.source = paramCompletableSource;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new CompletableObserverImplementation(paramCompletableObserver, this.scheduler));
  }
  
  static final class CompletableObserverImplementation
    implements CompletableObserver, Disposable, Runnable
  {
    Disposable d;
    volatile boolean disposed;
    final CompletableObserver s;
    final Scheduler scheduler;
    
    CompletableObserverImplementation(CompletableObserver paramCompletableObserver, Scheduler paramScheduler)
    {
      this.s = paramCompletableObserver;
      this.scheduler = paramScheduler;
    }
    
    public void dispose()
    {
      this.disposed = true;
      this.scheduler.scheduleDirect(this);
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public void onComplete()
    {
      if (this.disposed) {
        return;
      }
      this.s.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.disposed)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.s.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.s.onSubscribe(this);
      }
    }
    
    public void run()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableDisposeOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */