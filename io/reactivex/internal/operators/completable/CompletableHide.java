package io.reactivex.internal.operators.completable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class CompletableHide
  extends Completable
{
  final CompletableSource source;
  
  public CompletableHide(CompletableSource paramCompletableSource)
  {
    this.source = paramCompletableSource;
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new HideCompletableObserver(paramCompletableObserver));
  }
  
  static final class HideCompletableObserver
    implements CompletableObserver, Disposable
  {
    final CompletableObserver actual;
    Disposable d;
    
    HideCompletableObserver(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      this.d.dispose();
      this.d = DisposableHelper.DISPOSED;
    }
    
    public boolean isDisposed()
    {
      return this.d.isDisposed();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      if (DisposableHelper.validate(this.d, paramDisposable))
      {
        this.d = paramDisposable;
        this.actual.onSubscribe(this);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/completable/CompletableHide.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */