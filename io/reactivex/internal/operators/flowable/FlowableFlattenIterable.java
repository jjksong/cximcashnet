package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFlattenIterable<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final Function<? super T, ? extends Iterable<? extends R>> mapper;
  final int prefetch;
  
  public FlowableFlattenIterable(Publisher<T> paramPublisher, Function<? super T, ? extends Iterable<? extends R>> paramFunction, int paramInt)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.prefetch = paramInt;
  }
  
  public void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    if ((this.source instanceof Callable)) {
      try
      {
        Object localObject = ((Callable)this.source).call();
        if (localObject == null)
        {
          EmptySubscription.complete(paramSubscriber);
          return;
        }
        try
        {
          localObject = ((Iterable)this.mapper.apply(localObject)).iterator();
          FlowableFromIterable.subscribe(paramSubscriber, (Iterator)localObject);
          return;
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          EmptySubscription.error(localThrowable1, paramSubscriber);
          return;
        }
        this.source.subscribe(new FlattenIterableSubscriber(paramSubscriber, this.mapper, this.prefetch));
      }
      catch (Throwable localThrowable2)
      {
        Exceptions.throwIfFatal(localThrowable2);
        EmptySubscription.error(localThrowable2, paramSubscriber);
        return;
      }
    }
  }
  
  static final class FlattenIterableSubscriber<T, R>
    extends BasicIntQueueSubscription<R>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -3096000382929934955L;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    int consumed;
    Iterator<? extends R> current;
    volatile boolean done;
    final AtomicReference<Throwable> error;
    int fusionMode;
    final int limit;
    final Function<? super T, ? extends Iterable<? extends R>> mapper;
    final int prefetch;
    SimpleQueue<T> queue;
    final AtomicLong requested;
    Subscription s;
    
    FlattenIterableSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Iterable<? extends R>> paramFunction, int paramInt)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.prefetch = paramInt;
      this.limit = (paramInt - (paramInt >> 2));
      this.error = new AtomicReference();
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<?> paramSubscriber, SimpleQueue<?> paramSimpleQueue)
    {
      if (this.cancelled)
      {
        this.current = null;
        paramSimpleQueue.clear();
        return true;
      }
      if (paramBoolean1)
      {
        if ((Throwable)this.error.get() != null)
        {
          Throwable localThrowable = ExceptionHelper.terminate(this.error);
          this.current = null;
          paramSimpleQueue.clear();
          paramSubscriber.onError(localThrowable);
          return true;
        }
        if (paramBoolean2)
        {
          paramSubscriber.onComplete();
          return true;
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.current = null;
      this.queue.clear();
    }
    
    void consumedOne(boolean paramBoolean)
    {
      if (paramBoolean)
      {
        int i = this.consumed + 1;
        if (i == this.limit)
        {
          this.consumed = 0;
          this.s.request(i);
        }
        else
        {
          this.consumed = i;
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      boolean bool1;
      if (this.fusionMode != 1) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      Iterator localIterator1 = this.current;
      int i = 1;
      for (;;)
      {
        boolean bool3 = false;
        Iterator localIterator2 = localIterator1;
        boolean bool4;
        Object localObject3;
        boolean bool2;
        Object localObject1;
        if (localIterator1 == null)
        {
          bool4 = this.done;
          try
          {
            localObject3 = localSimpleQueue.poll();
            if (localObject3 == null) {
              bool2 = true;
            } else {
              bool2 = false;
            }
            if (checkTerminated(bool4, bool2, localSubscriber, localSimpleQueue)) {
              return;
            }
            localIterator2 = localIterator1;
            if (localObject3 != null) {
              try
              {
                localIterator2 = ((Iterable)this.mapper.apply(localObject3)).iterator();
                bool2 = localIterator2.hasNext();
                if (!bool2)
                {
                  consumedOne(bool1);
                  localIterator1 = null;
                  continue;
                }
                this.current = localIterator2;
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                this.s.cancel();
                ExceptionHelper.addThrowable(this.error, localThrowable1);
                localSubscriber.onError(ExceptionHelper.terminate(this.error));
                return;
              }
            }
            localObject3 = localIterator2;
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            this.s.cancel();
            ExceptionHelper.addThrowable(this.error, localThrowable2);
            localObject1 = ExceptionHelper.terminate(this.error);
            this.current = null;
            localSimpleQueue.clear();
            localSubscriber.onError((Throwable)localObject1);
            return;
          }
        }
        else if (localIterator2 != null)
        {
          long l3 = this.requested.get();
          long l1 = 0L;
          long l2;
          for (;;)
          {
            localObject1 = localIterator2;
            l2 = l1;
            if (l1 != l3)
            {
              if (checkTerminated(this.done, false, localSubscriber, localSimpleQueue)) {
                return;
              }
              try
              {
                localObject1 = localIterator2.next();
                localSubscriber.onNext(localObject1);
                if (checkTerminated(this.done, false, localSubscriber, localSimpleQueue)) {
                  return;
                }
                l2 = l1 + 1L;
                try
                {
                  bool2 = localIterator2.hasNext();
                  l1 = l2;
                  if (bool2) {
                    continue;
                  }
                  consumedOne(bool1);
                  this.current = null;
                  localObject1 = null;
                }
                catch (Throwable localThrowable3)
                {
                  Exceptions.throwIfFatal(localThrowable3);
                  this.current = null;
                  this.s.cancel();
                  ExceptionHelper.addThrowable(this.error, localThrowable3);
                  localSubscriber.onError(ExceptionHelper.terminate(this.error));
                  return;
                }
                if (l2 != l3) {
                  break label538;
                }
              }
              catch (Throwable localThrowable4)
              {
                Exceptions.throwIfFatal(localThrowable4);
                this.current = null;
                this.s.cancel();
                ExceptionHelper.addThrowable(this.error, localThrowable4);
                localSubscriber.onError(ExceptionHelper.terminate(this.error));
                return;
              }
            }
          }
          bool4 = this.done;
          bool2 = bool3;
          if (localSimpleQueue.isEmpty())
          {
            bool2 = bool3;
            if (localThrowable4 == null) {
              bool2 = true;
            }
          }
          if (checkTerminated(bool4, bool2, localSubscriber, localSimpleQueue)) {
            return;
          }
          label538:
          if ((l2 != 0L) && (l3 != Long.MAX_VALUE)) {
            this.requested.addAndGet(-l2);
          }
          localObject3 = localThrowable4;
          if (localThrowable4 == null) {}
        }
        else
        {
          i = addAndGet(-i);
          if (i == 0) {
            return;
          }
          Object localObject2 = localObject3;
        }
      }
    }
    
    public boolean isEmpty()
    {
      Iterator localIterator = this.current;
      boolean bool;
      if (((localIterator != null) && (!localIterator.hasNext())) || (this.queue.isEmpty())) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.done) && (ExceptionHelper.addThrowable(this.error, paramThrowable)))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if ((this.fusionMode == 0) && (!this.queue.offer(paramT)))
      {
        onError(new MissingBackpressureException("Queue is full?!"));
        return;
      }
      drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.actual.onSubscribe(this);
            return;
          }
          if (i == 2)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
            this.actual.onSubscribe(this);
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        this.actual.onSubscribe(this);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public R poll()
      throws Exception
    {
      Object localObject2;
      for (Object localObject1 = this.current;; localObject1 = null)
      {
        localObject2 = localObject1;
        if (localObject1 != null) {
          break label65;
        }
        localObject1 = this.queue.poll();
        if (localObject1 == null) {
          return null;
        }
        localObject2 = ((Iterable)this.mapper.apply(localObject1)).iterator();
        if (((Iterator)localObject2).hasNext()) {
          break;
        }
      }
      this.current = ((Iterator)localObject2);
      label65:
      localObject1 = ((Iterator)localObject2).next();
      if (!((Iterator)localObject2).hasNext()) {
        this.current = null;
      }
      return (R)localObject1;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if (((paramInt & 0x1) != 0) && (this.fusionMode == 1)) {
        return 1;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFlattenIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */