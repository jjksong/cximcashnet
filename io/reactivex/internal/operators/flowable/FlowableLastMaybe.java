package io.reactivex.internal.operators.flowable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableLastMaybe<T>
  extends Maybe<T>
{
  final Publisher<T> source;
  
  public FlowableLastMaybe(Publisher<T> paramPublisher)
  {
    this.source = paramPublisher;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new LastSubscriber(paramMaybeObserver));
  }
  
  static final class LastSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    T item;
    Subscription s;
    
    LastSubscriber(MaybeObserver<? super T> paramMaybeObserver)
    {
      this.actual = paramMaybeObserver;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      Object localObject = this.item;
      if (localObject != null)
      {
        this.item = null;
        this.actual.onSuccess(localObject);
      }
      else
      {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.item = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.item = paramT;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableLastMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */