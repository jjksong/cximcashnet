package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.Collection;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableToList<T, U extends Collection<? super T>>
  extends AbstractFlowableWithUpstream<T, U>
{
  final Callable<U> collectionSupplier;
  
  public FlowableToList(Publisher<T> paramPublisher, Callable<U> paramCallable)
  {
    super(paramPublisher);
    this.collectionSupplier = paramCallable;
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    try
    {
      Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.collectionSupplier.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      this.source.subscribe(new ToListSubscriber(paramSubscriber, localCollection));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class ToListSubscriber<T, U extends Collection<? super T>>
    extends DeferredScalarSubscription<U>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -8134157938864266736L;
    Subscription s;
    
    ToListSubscriber(Subscriber<? super U> paramSubscriber, U paramU)
    {
      super();
      this.value = paramU;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      complete(this.value);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.value = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      ((Collection)this.value).add(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableToList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */