package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSkip<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long n;
  
  public FlowableSkip(Publisher<T> paramPublisher, long paramLong)
  {
    super(paramPublisher);
    this.n = paramLong;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new SkipSubscriber(paramSubscriber, this.n));
  }
  
  static final class SkipSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super T> actual;
    long remaining;
    Subscription s;
    
    SkipSubscriber(Subscriber<? super T> paramSubscriber, long paramLong)
    {
      this.actual = paramSubscriber;
      this.remaining = paramLong;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.remaining;
      if (l != 0L) {
        this.remaining = (l - 1L);
      } else {
        this.actual.onNext(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        long l = this.remaining;
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(l);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSkip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */