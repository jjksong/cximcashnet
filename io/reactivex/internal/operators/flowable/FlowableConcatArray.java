package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableConcatArray<T>
  extends Flowable<T>
{
  final boolean delayError;
  final Publisher<? extends T>[] sources;
  
  public FlowableConcatArray(Publisher<? extends T>[] paramArrayOfPublisher, boolean paramBoolean)
  {
    this.sources = paramArrayOfPublisher;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    ConcatArraySubscriber localConcatArraySubscriber = new ConcatArraySubscriber(this.sources, this.delayError, paramSubscriber);
    paramSubscriber.onSubscribe(localConcatArraySubscriber);
    localConcatArraySubscriber.onComplete();
  }
  
  static final class ConcatArraySubscriber<T>
    extends SubscriptionArbiter
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -8158322871608889516L;
    final Subscriber<? super T> actual;
    final boolean delayError;
    List<Throwable> errors;
    int index;
    long produced;
    final Publisher<? extends T>[] sources;
    final AtomicInteger wip;
    
    ConcatArraySubscriber(Publisher<? extends T>[] paramArrayOfPublisher, boolean paramBoolean, Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
      this.sources = paramArrayOfPublisher;
      this.delayError = paramBoolean;
      this.wip = new AtomicInteger();
    }
    
    public void onComplete()
    {
      if (this.wip.getAndIncrement() == 0)
      {
        Publisher[] arrayOfPublisher = this.sources;
        int j = arrayOfPublisher.length;
        int i = this.index;
        label198:
        do
        {
          Object localObject;
          NullPointerException localNullPointerException;
          for (;;)
          {
            if (i == j)
            {
              localObject = this.errors;
              if (localObject != null)
              {
                if (((List)localObject).size() == 1) {
                  this.actual.onError((Throwable)((List)localObject).get(0));
                } else {
                  this.actual.onError(new CompositeException((Iterable)localObject));
                }
              }
              else {
                this.actual.onComplete();
              }
              return;
            }
            localObject = arrayOfPublisher[i];
            if (localObject != null) {
              break label198;
            }
            localNullPointerException = new NullPointerException("A Publisher entry is null");
            if (!this.delayError) {
              break;
            }
            List localList = this.errors;
            localObject = localList;
            if (localList == null)
            {
              localObject = new ArrayList(j - i + 1);
              this.errors = ((List)localObject);
            }
            ((List)localObject).add(localNullPointerException);
            i++;
          }
          this.actual.onError(localNullPointerException);
          return;
          long l = this.produced;
          if (l != 0L)
          {
            this.produced = 0L;
            produced(l);
          }
          ((Publisher)localObject).subscribe(this);
          i++;
          this.index = i;
        } while (this.wip.decrementAndGet() != 0);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.delayError)
      {
        List localList = this.errors;
        Object localObject = localList;
        if (localList == null)
        {
          localObject = new ArrayList(this.sources.length - this.index + 1);
          this.errors = ((List)localObject);
        }
        ((List)localObject).add(paramThrowable);
        onComplete();
      }
      else
      {
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      this.produced += 1L;
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      setSubscription(paramSubscription);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableConcatArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */