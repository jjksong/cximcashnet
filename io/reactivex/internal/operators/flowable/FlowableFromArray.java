package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.subscriptions.BasicQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import org.reactivestreams.Subscriber;

public final class FlowableFromArray<T>
  extends Flowable<T>
{
  final T[] array;
  
  public FlowableFromArray(T[] paramArrayOfT)
  {
    this.array = paramArrayOfT;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      paramSubscriber.onSubscribe(new ArrayConditionalSubscription((ConditionalSubscriber)paramSubscriber, this.array));
    } else {
      paramSubscriber.onSubscribe(new ArraySubscription(paramSubscriber, this.array));
    }
  }
  
  static final class ArrayConditionalSubscription<T>
    extends FlowableFromArray.BaseArraySubscription<T>
  {
    private static final long serialVersionUID = 2587302975077663557L;
    final ConditionalSubscriber<? super T> actual;
    
    ArrayConditionalSubscription(ConditionalSubscriber<? super T> paramConditionalSubscriber, T[] paramArrayOfT)
    {
      super();
      this.actual = paramConditionalSubscriber;
    }
    
    void fastPath()
    {
      Object[] arrayOfObject = this.array;
      int j = arrayOfObject.length;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      for (int i = this.index; i != j; i++)
      {
        if (this.cancelled) {
          return;
        }
        Object localObject = arrayOfObject[i];
        if (localObject == null)
        {
          localConditionalSubscriber.onError(new NullPointerException("array element is null"));
          return;
        }
        localConditionalSubscriber.tryOnNext(localObject);
      }
      if (this.cancelled) {
        return;
      }
      localConditionalSubscriber.onComplete();
    }
    
    void slowPath(long paramLong)
    {
      Object[] arrayOfObject = this.array;
      int j = arrayOfObject.length;
      int i = this.index;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        long l2;
        if ((l1 != paramLong) && (i != j))
        {
          if (this.cancelled) {
            return;
          }
          Object localObject = arrayOfObject[i];
          if (localObject == null)
          {
            localConditionalSubscriber.onError(new NullPointerException("array element is null"));
            return;
          }
          l2 = l1;
          if (localConditionalSubscriber.tryOnNext(localObject)) {
            l2 = l1 + 1L;
          }
          i++;
          l1 = l2;
        }
        else
        {
          if (i == j)
          {
            if (!this.cancelled) {
              localConditionalSubscriber.onComplete();
            }
            return;
          }
          l2 = get();
          paramLong = l2;
          if (l1 == l2)
          {
            this.index = i;
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
  
  static final class ArraySubscription<T>
    extends FlowableFromArray.BaseArraySubscription<T>
  {
    private static final long serialVersionUID = 2587302975077663557L;
    final Subscriber<? super T> actual;
    
    ArraySubscription(Subscriber<? super T> paramSubscriber, T[] paramArrayOfT)
    {
      super();
      this.actual = paramSubscriber;
    }
    
    void fastPath()
    {
      Object[] arrayOfObject = this.array;
      int j = arrayOfObject.length;
      Subscriber localSubscriber = this.actual;
      for (int i = this.index; i != j; i++)
      {
        if (this.cancelled) {
          return;
        }
        Object localObject = arrayOfObject[i];
        if (localObject == null)
        {
          localSubscriber.onError(new NullPointerException("array element is null"));
          return;
        }
        localSubscriber.onNext(localObject);
      }
      if (this.cancelled) {
        return;
      }
      localSubscriber.onComplete();
    }
    
    void slowPath(long paramLong)
    {
      Object[] arrayOfObject = this.array;
      int j = arrayOfObject.length;
      int i = this.index;
      Subscriber localSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        if ((l1 != paramLong) && (i != j))
        {
          if (this.cancelled) {
            return;
          }
          Object localObject = arrayOfObject[i];
          if (localObject == null)
          {
            localSubscriber.onError(new NullPointerException("array element is null"));
            return;
          }
          localSubscriber.onNext(localObject);
          l1 += 1L;
          i++;
        }
        else
        {
          if (i == j)
          {
            if (!this.cancelled) {
              localSubscriber.onComplete();
            }
            return;
          }
          long l2 = get();
          paramLong = l2;
          if (l1 == l2)
          {
            this.index = i;
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
  
  static abstract class BaseArraySubscription<T>
    extends BasicQueueSubscription<T>
  {
    private static final long serialVersionUID = -2252972430506210021L;
    final T[] array;
    volatile boolean cancelled;
    int index;
    
    BaseArraySubscription(T[] paramArrayOfT)
    {
      this.array = paramArrayOfT;
    }
    
    public final void cancel()
    {
      this.cancelled = true;
    }
    
    public final void clear()
    {
      this.index = this.array.length;
    }
    
    abstract void fastPath();
    
    public final boolean isEmpty()
    {
      boolean bool;
      if (this.index == this.array.length) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public final T poll()
    {
      int i = this.index;
      Object[] arrayOfObject = this.array;
      if (i == arrayOfObject.length) {
        return null;
      }
      this.index = (i + 1);
      return (T)ObjectHelper.requireNonNull(arrayOfObject[i], "array element is null");
    }
    
    public final void request(long paramLong)
    {
      if ((SubscriptionHelper.validate(paramLong)) && (BackpressureHelper.add(this, paramLong) == 0L)) {
        if (paramLong == Long.MAX_VALUE) {
          fastPath();
        } else {
          slowPath(paramLong);
        }
      }
    }
    
    public final int requestFusion(int paramInt)
    {
      return paramInt & 0x1;
    }
    
    abstract void slowPath(long paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFromArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */