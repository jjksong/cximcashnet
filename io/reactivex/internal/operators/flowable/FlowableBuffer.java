package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableBuffer<T, C extends Collection<? super T>>
  extends AbstractFlowableWithUpstream<T, C>
{
  final Callable<C> bufferSupplier;
  final int size;
  final int skip;
  
  public FlowableBuffer(Publisher<T> paramPublisher, int paramInt1, int paramInt2, Callable<C> paramCallable)
  {
    super(paramPublisher);
    this.size = paramInt1;
    this.skip = paramInt2;
    this.bufferSupplier = paramCallable;
  }
  
  public void subscribeActual(Subscriber<? super C> paramSubscriber)
  {
    int i = this.size;
    int j = this.skip;
    if (i == j) {
      this.source.subscribe(new PublisherBufferExactSubscriber(paramSubscriber, this.size, this.bufferSupplier));
    } else if (j > i) {
      this.source.subscribe(new PublisherBufferSkipSubscriber(paramSubscriber, this.size, this.skip, this.bufferSupplier));
    } else {
      this.source.subscribe(new PublisherBufferOverlappingSubscriber(paramSubscriber, this.size, this.skip, this.bufferSupplier));
    }
  }
  
  static final class PublisherBufferExactSubscriber<T, C extends Collection<? super T>>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super C> actual;
    C buffer;
    final Callable<C> bufferSupplier;
    boolean done;
    int index;
    Subscription s;
    final int size;
    
    PublisherBufferExactSubscriber(Subscriber<? super C> paramSubscriber, int paramInt, Callable<C> paramCallable)
    {
      this.actual = paramSubscriber;
      this.size = paramInt;
      this.bufferSupplier = paramCallable;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Collection localCollection = this.buffer;
      if ((localCollection != null) && (!localCollection.isEmpty())) {
        this.actual.onNext(localCollection);
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Collection localCollection2 = this.buffer;
      Collection localCollection1 = localCollection2;
      if (localCollection2 == null) {
        try
        {
          localCollection1 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
          this.buffer = localCollection1;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return;
        }
      }
      localCollection1.add(paramT);
      int i = this.index + 1;
      if (i == this.size)
      {
        this.index = 0;
        this.buffer = null;
        this.actual.onNext(localCollection1);
      }
      else
      {
        this.index = i;
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        this.s.request(BackpressureHelper.multiplyCap(paramLong, this.size));
      }
    }
  }
  
  static final class PublisherBufferOverlappingSubscriber<T, C extends Collection<? super T>>
    extends AtomicLong
    implements Subscriber<T>, Subscription, BooleanSupplier
  {
    private static final long serialVersionUID = -7370244972039324525L;
    final Subscriber<? super C> actual;
    final Callable<C> bufferSupplier;
    final ArrayDeque<C> buffers;
    volatile boolean cancelled;
    boolean done;
    int index;
    final AtomicBoolean once;
    long produced;
    Subscription s;
    final int size;
    final int skip;
    
    PublisherBufferOverlappingSubscriber(Subscriber<? super C> paramSubscriber, int paramInt1, int paramInt2, Callable<C> paramCallable)
    {
      this.actual = paramSubscriber;
      this.size = paramInt1;
      this.skip = paramInt2;
      this.bufferSupplier = paramCallable;
      this.once = new AtomicBoolean();
      this.buffers = new ArrayDeque();
    }
    
    public void cancel()
    {
      this.cancelled = true;
      this.s.cancel();
    }
    
    public boolean getAsBoolean()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      long l = this.produced;
      if (l != 0L) {
        BackpressureHelper.produced(this, l);
      }
      QueueDrainHelper.postComplete(this.actual, this.buffers, this, this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.buffers.clear();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Object localObject = this.buffers;
      int i = this.index;
      int j = i + 1;
      if (i == 0) {
        try
        {
          localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
          ((ArrayDeque)localObject).offer(localCollection);
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return;
        }
      }
      Collection localCollection = (Collection)((ArrayDeque)localObject).peek();
      if ((localCollection != null) && (localCollection.size() + 1 == this.size))
      {
        ((ArrayDeque)localObject).poll();
        localCollection.add(paramT);
        this.produced += 1L;
        this.actual.onNext(localCollection);
      }
      localObject = ((ArrayDeque)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        ((Collection)((Iterator)localObject).next()).add(paramT);
      }
      i = j;
      if (j == this.skip) {
        i = 0;
      }
      this.index = i;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        if (QueueDrainHelper.postCompleteRequest(paramLong, this.actual, this.buffers, this, this)) {
          return;
        }
        if ((!this.once.get()) && (this.once.compareAndSet(false, true)))
        {
          paramLong = BackpressureHelper.multiplyCap(this.skip, paramLong - 1L);
          paramLong = BackpressureHelper.addCap(this.size, paramLong);
          this.s.request(paramLong);
        }
        else
        {
          paramLong = BackpressureHelper.multiplyCap(this.skip, paramLong);
          this.s.request(paramLong);
        }
      }
    }
  }
  
  static final class PublisherBufferSkipSubscriber<T, C extends Collection<? super T>>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -5616169793639412593L;
    final Subscriber<? super C> actual;
    C buffer;
    final Callable<C> bufferSupplier;
    boolean done;
    int index;
    Subscription s;
    final int size;
    final int skip;
    
    PublisherBufferSkipSubscriber(Subscriber<? super C> paramSubscriber, int paramInt1, int paramInt2, Callable<C> paramCallable)
    {
      this.actual = paramSubscriber;
      this.size = paramInt1;
      this.skip = paramInt2;
      this.bufferSupplier = paramCallable;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Collection localCollection = this.buffer;
      this.buffer = null;
      if (localCollection != null) {
        this.actual.onNext(localCollection);
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.buffer = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Collection localCollection = this.buffer;
      int i = this.index;
      int j = i + 1;
      if (i == 0) {
        try
        {
          localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The bufferSupplier returned a null buffer");
          this.buffer = localCollection;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
          return;
        }
      }
      if (localCollection != null)
      {
        localCollection.add(paramT);
        if (localCollection.size() == this.size)
        {
          this.buffer = null;
          this.actual.onNext(localCollection);
        }
      }
      i = j;
      if (j == this.skip) {
        i = 0;
      }
      this.index = i;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        if ((get() == 0) && (compareAndSet(0, 1)))
        {
          long l = BackpressureHelper.multiplyCap(paramLong, this.size);
          paramLong = BackpressureHelper.multiplyCap(this.skip - this.size, paramLong - 1L);
          this.s.request(BackpressureHelper.addCap(l, paramLong));
        }
        else
        {
          this.s.request(BackpressureHelper.multiplyCap(this.skip, paramLong));
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */