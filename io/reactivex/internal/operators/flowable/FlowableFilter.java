package io.reactivex.internal.operators.flowable;

import io.reactivex.functions.Predicate;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscribers.BasicFuseableConditionalSubscriber;
import io.reactivex.internal.subscribers.BasicFuseableSubscriber;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFilter<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Predicate<? super T> predicate;
  
  public FlowableFilter(Publisher<T> paramPublisher, Predicate<? super T> paramPredicate)
  {
    super(paramPublisher);
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      this.source.subscribe(new FilterConditionalSubscriber((ConditionalSubscriber)paramSubscriber, this.predicate));
    } else {
      this.source.subscribe(new FilterSubscriber(paramSubscriber, this.predicate));
    }
  }
  
  static final class FilterConditionalSubscriber<T>
    extends BasicFuseableConditionalSubscriber<T, T>
  {
    final Predicate<? super T> filter;
    
    FilterConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Predicate<? super T> paramPredicate)
    {
      super();
      this.filter = paramPredicate;
    }
    
    public void onNext(T paramT)
    {
      if (!tryOnNext(paramT)) {
        this.s.request(1L);
      }
    }
    
    public T poll()
      throws Exception
    {
      QueueSubscription localQueueSubscription = this.qs;
      Predicate localPredicate = this.filter;
      for (;;)
      {
        Object localObject = localQueueSubscription.poll();
        if (localObject == null) {
          return null;
        }
        if (localPredicate.test(localObject)) {
          return (T)localObject;
        }
        if (this.sourceMode == 2) {
          localQueueSubscription.request(1L);
        }
      }
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      if (this.sourceMode != 0) {
        return this.actual.tryOnNext(null);
      }
      boolean bool1 = true;
      try
      {
        boolean bool2 = this.filter.test(paramT);
        if ((!bool2) || (!this.actual.tryOnNext(paramT))) {
          bool1 = false;
        }
        return bool1;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return true;
    }
  }
  
  static final class FilterSubscriber<T>
    extends BasicFuseableSubscriber<T, T>
    implements ConditionalSubscriber<T>
  {
    final Predicate<? super T> filter;
    
    FilterSubscriber(Subscriber<? super T> paramSubscriber, Predicate<? super T> paramPredicate)
    {
      super();
      this.filter = paramPredicate;
    }
    
    public void onNext(T paramT)
    {
      if (!tryOnNext(paramT)) {
        this.s.request(1L);
      }
    }
    
    public T poll()
      throws Exception
    {
      QueueSubscription localQueueSubscription = this.qs;
      Predicate localPredicate = this.filter;
      for (;;)
      {
        Object localObject = localQueueSubscription.poll();
        if (localObject == null) {
          return null;
        }
        if (localPredicate.test(localObject)) {
          return (T)localObject;
        }
        if (this.sourceMode == 2) {
          localQueueSubscription.request(1L);
        }
      }
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      if (this.sourceMode != 0)
      {
        this.actual.onNext(null);
        return true;
      }
      try
      {
        boolean bool = this.filter.test(paramT);
        if (bool) {
          this.actual.onNext(paramT);
        }
        return bool;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return true;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */