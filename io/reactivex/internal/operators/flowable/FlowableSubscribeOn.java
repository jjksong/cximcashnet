package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSubscribeOn<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final boolean nonScheduledRequests;
  final Scheduler scheduler;
  
  public FlowableSubscribeOn(Publisher<T> paramPublisher, Scheduler paramScheduler, boolean paramBoolean)
  {
    super(paramPublisher);
    this.scheduler = paramScheduler;
    this.nonScheduledRequests = paramBoolean;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    SubscribeOnSubscriber localSubscribeOnSubscriber = new SubscribeOnSubscriber(paramSubscriber, localWorker, this.source, this.nonScheduledRequests);
    paramSubscriber.onSubscribe(localSubscribeOnSubscriber);
    localWorker.schedule(localSubscribeOnSubscriber);
  }
  
  static final class SubscribeOnSubscriber<T>
    extends AtomicReference<Thread>
    implements Subscriber<T>, Subscription, Runnable
  {
    private static final long serialVersionUID = 8094547886072529208L;
    final Subscriber<? super T> actual;
    final boolean nonScheduledRequests;
    final AtomicLong requested;
    final AtomicReference<Subscription> s;
    Publisher<T> source;
    final Scheduler.Worker worker;
    
    SubscribeOnSubscriber(Subscriber<? super T> paramSubscriber, Scheduler.Worker paramWorker, Publisher<T> paramPublisher, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.worker = paramWorker;
      this.source = paramPublisher;
      this.s = new AtomicReference();
      this.requested = new AtomicLong();
      this.nonScheduledRequests = paramBoolean;
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this.s);
      this.worker.dispose();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      this.worker.dispose();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      this.worker.dispose();
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this.s, paramSubscription))
      {
        long l = this.requested.getAndSet(0L);
        if (l != 0L) {
          requestUpstream(l, paramSubscription);
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        Subscription localSubscription = (Subscription)this.s.get();
        if (localSubscription != null)
        {
          requestUpstream(paramLong, localSubscription);
        }
        else
        {
          BackpressureHelper.add(this.requested, paramLong);
          localSubscription = (Subscription)this.s.get();
          if (localSubscription != null)
          {
            paramLong = this.requested.getAndSet(0L);
            if (paramLong != 0L) {
              requestUpstream(paramLong, localSubscription);
            }
          }
        }
      }
    }
    
    void requestUpstream(final long paramLong, final Subscription paramSubscription)
    {
      if ((!this.nonScheduledRequests) && (Thread.currentThread() != get())) {
        this.worker.schedule(new Runnable()
        {
          public void run()
          {
            paramSubscription.request(paramLong);
          }
        });
      } else {
        paramSubscription.request(paramLong);
      }
    }
    
    public void run()
    {
      lazySet(Thread.currentThread());
      Publisher localPublisher = this.source;
      this.source = null;
      localPublisher.subscribe(this);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSubscribeOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */