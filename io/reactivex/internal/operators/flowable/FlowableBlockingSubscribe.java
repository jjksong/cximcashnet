package io.reactivex.internal.operators.flowable;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.subscribers.BlockingSubscriber;
import io.reactivex.internal.subscribers.LambdaSubscriber;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.BlockingIgnoringReceiver;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableBlockingSubscribe
{
  private FlowableBlockingSubscribe()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T> void subscribe(Publisher<? extends T> paramPublisher)
  {
    BlockingIgnoringReceiver localBlockingIgnoringReceiver = new BlockingIgnoringReceiver();
    LambdaSubscriber localLambdaSubscriber = new LambdaSubscriber(Functions.emptyConsumer(), localBlockingIgnoringReceiver, localBlockingIgnoringReceiver, Functions.REQUEST_MAX);
    paramPublisher.subscribe(localLambdaSubscriber);
    BlockingHelper.awaitForComplete(localBlockingIgnoringReceiver, localLambdaSubscriber);
    paramPublisher = localBlockingIgnoringReceiver.error;
    if (paramPublisher == null) {
      return;
    }
    throw ExceptionHelper.wrapOrThrow(paramPublisher);
  }
  
  public static <T> void subscribe(Publisher<? extends T> paramPublisher, Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction)
  {
    subscribe(paramPublisher, new LambdaSubscriber(paramConsumer, paramConsumer1, paramAction, Functions.REQUEST_MAX));
  }
  
  public static <T> void subscribe(Publisher<? extends T> paramPublisher, Subscriber<? super T> paramSubscriber)
  {
    LinkedBlockingQueue localLinkedBlockingQueue = new LinkedBlockingQueue();
    BlockingSubscriber localBlockingSubscriber = new BlockingSubscriber(localLinkedBlockingQueue);
    paramPublisher.subscribe(localBlockingSubscriber);
    try
    {
      boolean bool;
      do
      {
        if (localBlockingSubscriber.isCancelled()) {
          break;
        }
        Object localObject2 = localLinkedBlockingQueue.poll();
        Object localObject1 = localObject2;
        if (localObject2 == null)
        {
          if (localBlockingSubscriber.isCancelled()) {
            break;
          }
          BlockingHelper.verifyNonBlocking();
          localObject1 = localLinkedBlockingQueue.take();
        }
        if ((localBlockingSubscriber.isCancelled()) || (paramPublisher == BlockingSubscriber.TERMINATED)) {
          break;
        }
        bool = NotificationLite.acceptFull(localObject1, paramSubscriber);
      } while (!bool);
    }
    catch (InterruptedException paramPublisher)
    {
      localBlockingSubscriber.cancel();
      paramSubscriber.onError(paramPublisher);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableBlockingSubscribe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */