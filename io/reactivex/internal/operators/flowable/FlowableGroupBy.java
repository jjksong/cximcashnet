package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.flowables.GroupedFlowable;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableGroupBy<T, K, V>
  extends AbstractFlowableWithUpstream<T, GroupedFlowable<K, V>>
{
  final int bufferSize;
  final boolean delayError;
  final Function<? super T, ? extends K> keySelector;
  final Function<? super T, ? extends V> valueSelector;
  
  public FlowableGroupBy(Publisher<T> paramPublisher, Function<? super T, ? extends K> paramFunction, Function<? super T, ? extends V> paramFunction1, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.keySelector = paramFunction;
    this.valueSelector = paramFunction1;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super GroupedFlowable<K, V>> paramSubscriber)
  {
    this.source.subscribe(new GroupBySubscriber(paramSubscriber, this.keySelector, this.valueSelector, this.bufferSize, this.delayError));
  }
  
  public static final class GroupBySubscriber<T, K, V>
    extends BasicIntQueueSubscription<GroupedFlowable<K, V>>
    implements Subscriber<T>
  {
    static final Object NULL_KEY = new Object();
    private static final long serialVersionUID = -3688291656102519502L;
    final Subscriber<? super GroupedFlowable<K, V>> actual;
    final int bufferSize;
    final AtomicBoolean cancelled = new AtomicBoolean();
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final AtomicInteger groupCount = new AtomicInteger(1);
    final Map<Object, FlowableGroupBy.GroupedUnicast<K, V>> groups;
    final Function<? super T, ? extends K> keySelector;
    boolean outputFused;
    final SpscLinkedArrayQueue<GroupedFlowable<K, V>> queue;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    final Function<? super T, ? extends V> valueSelector;
    
    public GroupBySubscriber(Subscriber<? super GroupedFlowable<K, V>> paramSubscriber, Function<? super T, ? extends K> paramFunction, Function<? super T, ? extends V> paramFunction1, int paramInt, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.keySelector = paramFunction;
      this.valueSelector = paramFunction1;
      this.bufferSize = paramInt;
      this.delayError = paramBoolean;
      this.groups = new ConcurrentHashMap();
      this.queue = new SpscLinkedArrayQueue(paramInt);
    }
    
    public void cancel()
    {
      if ((this.cancelled.compareAndSet(false, true)) && (this.groupCount.decrementAndGet() == 0)) {
        this.s.cancel();
      }
    }
    
    public void cancel(K paramK)
    {
      if (paramK == null) {
        paramK = NULL_KEY;
      }
      this.groups.remove(paramK);
      if (this.groupCount.decrementAndGet() == 0)
      {
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<?> paramSubscriber, SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      if (this.cancelled.get())
      {
        paramSpscLinkedArrayQueue.clear();
        return true;
      }
      if (this.delayError)
      {
        if ((paramBoolean1) && (paramBoolean2))
        {
          paramSpscLinkedArrayQueue = this.error;
          if (paramSpscLinkedArrayQueue != null) {
            paramSubscriber.onError(paramSpscLinkedArrayQueue);
          } else {
            paramSubscriber.onComplete();
          }
          return true;
        }
      }
      else if (paramBoolean1)
      {
        Throwable localThrowable = this.error;
        if (localThrowable != null)
        {
          paramSpscLinkedArrayQueue.clear();
          paramSubscriber.onError(localThrowable);
          return true;
        }
        if (paramBoolean2)
        {
          paramSubscriber.onComplete();
          return true;
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      if (this.outputFused) {
        drainFused();
      } else {
        drainNormal();
      }
    }
    
    void drainFused()
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      int j;
      do
      {
        if (this.cancelled.get())
        {
          localSpscLinkedArrayQueue.clear();
          return;
        }
        boolean bool = this.done;
        Throwable localThrowable;
        if ((bool) && (!this.delayError))
        {
          localThrowable = this.error;
          if (localThrowable != null)
          {
            localSpscLinkedArrayQueue.clear();
            localSubscriber.onError(localThrowable);
            return;
          }
        }
        localSubscriber.onNext(null);
        if (bool)
        {
          localThrowable = this.error;
          if (localThrowable != null) {
            localSubscriber.onError(localThrowable);
          } else {
            localSubscriber.onComplete();
          }
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void drainNormal()
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      int j;
      do
      {
        long l2 = this.requested.get();
        for (long l1 = 0L; l1 != l2; l1 += 1L)
        {
          boolean bool2 = this.done;
          GroupedFlowable localGroupedFlowable = (GroupedFlowable)localSpscLinkedArrayQueue.poll();
          boolean bool1;
          if (localGroupedFlowable == null) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if (checkTerminated(bool2, bool1, localSubscriber, localSpscLinkedArrayQueue)) {
            return;
          }
          if (bool1) {
            break;
          }
          localSubscriber.onNext(localGroupedFlowable);
        }
        if ((l1 == l2) && (checkTerminated(this.done, localSpscLinkedArrayQueue.isEmpty(), localSubscriber, localSpscLinkedArrayQueue))) {
          return;
        }
        if (l1 != 0L)
        {
          if (l2 != Long.MAX_VALUE) {
            this.requested.addAndGet(-l1);
          }
          this.s.request(l1);
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        Iterator localIterator = this.groups.values().iterator();
        while (localIterator.hasNext()) {
          ((FlowableGroupBy.GroupedUnicast)localIterator.next()).onComplete();
        }
        this.groups.clear();
        this.done = true;
        drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      Iterator localIterator = this.groups.values().iterator();
      while (localIterator.hasNext()) {
        ((FlowableGroupBy.GroupedUnicast)localIterator.next()).onError(paramThrowable);
      }
      this.groups.clear();
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      try
      {
        Object localObject2 = this.keySelector.apply(paramT);
        int i = 0;
        Object localObject1;
        if (localObject2 != null) {
          localObject1 = localObject2;
        } else {
          localObject1 = NULL_KEY;
        }
        FlowableGroupBy.GroupedUnicast localGroupedUnicast2 = (FlowableGroupBy.GroupedUnicast)this.groups.get(localObject1);
        FlowableGroupBy.GroupedUnicast localGroupedUnicast1 = localGroupedUnicast2;
        if (localGroupedUnicast2 == null)
        {
          if (this.cancelled.get()) {
            return;
          }
          localGroupedUnicast1 = FlowableGroupBy.GroupedUnicast.createWith(localObject2, this.bufferSize, this, this.delayError);
          this.groups.put(localObject1, localGroupedUnicast1);
          this.groupCount.getAndIncrement();
          i = 1;
        }
        try
        {
          paramT = ObjectHelper.requireNonNull(this.valueSelector.apply(paramT), "The valueSelector returned null");
          localGroupedUnicast1.onNext(paramT);
          if (i != 0)
          {
            localSpscLinkedArrayQueue.offer(localGroupedUnicast1);
            drain();
          }
          return;
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.s.cancel();
          onError(paramT);
          return;
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(this.bufferSize);
      }
    }
    
    public GroupedFlowable<K, V> poll()
    {
      return (GroupedFlowable)this.queue.poll();
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
  
  static final class GroupedUnicast<K, T>
    extends GroupedFlowable<K, T>
  {
    final FlowableGroupBy.State<T, K> state;
    
    protected GroupedUnicast(K paramK, FlowableGroupBy.State<T, K> paramState)
    {
      super();
      this.state = paramState;
    }
    
    public static <T, K> GroupedUnicast<K, T> createWith(K paramK, int paramInt, FlowableGroupBy.GroupBySubscriber<?, K, T> paramGroupBySubscriber, boolean paramBoolean)
    {
      return new GroupedUnicast(paramK, new FlowableGroupBy.State(paramInt, paramGroupBySubscriber, paramK, paramBoolean));
    }
    
    public void onComplete()
    {
      this.state.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.state.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.state.onNext(paramT);
    }
    
    protected void subscribeActual(Subscriber<? super T> paramSubscriber)
    {
      this.state.subscribe(paramSubscriber);
    }
  }
  
  static final class State<T, K>
    extends BasicIntQueueSubscription<T>
    implements Publisher<T>
  {
    private static final long serialVersionUID = -3852313036005250360L;
    final AtomicReference<Subscriber<? super T>> actual = new AtomicReference();
    final AtomicBoolean cancelled = new AtomicBoolean();
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final K key;
    final AtomicBoolean once = new AtomicBoolean();
    boolean outputFused;
    final FlowableGroupBy.GroupBySubscriber<?, K, T> parent;
    int produced;
    final SpscLinkedArrayQueue<T> queue;
    final AtomicLong requested = new AtomicLong();
    
    State(int paramInt, FlowableGroupBy.GroupBySubscriber<?, K, T> paramGroupBySubscriber, K paramK, boolean paramBoolean)
    {
      this.queue = new SpscLinkedArrayQueue(paramInt);
      this.parent = paramGroupBySubscriber;
      this.key = paramK;
      this.delayError = paramBoolean;
    }
    
    public void cancel()
    {
      if (this.cancelled.compareAndSet(false, true)) {
        this.parent.cancel(this.key);
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<? super T> paramSubscriber, boolean paramBoolean3)
    {
      if (this.cancelled.get())
      {
        this.queue.clear();
        return true;
      }
      if (paramBoolean1)
      {
        Throwable localThrowable;
        if (paramBoolean3)
        {
          if (paramBoolean2)
          {
            localThrowable = this.error;
            if (localThrowable != null) {
              paramSubscriber.onError(localThrowable);
            } else {
              paramSubscriber.onComplete();
            }
            return true;
          }
        }
        else
        {
          localThrowable = this.error;
          if (localThrowable != null)
          {
            this.queue.clear();
            paramSubscriber.onError(localThrowable);
            return true;
          }
          if (paramBoolean2)
          {
            paramSubscriber.onComplete();
            return true;
          }
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      if (this.outputFused) {
        drainFused();
      } else {
        drainNormal();
      }
    }
    
    void drainFused()
    {
      Object localObject = this.queue;
      Subscriber localSubscriber = (Subscriber)this.actual.get();
      int i = 1;
      for (;;)
      {
        if (localSubscriber != null)
        {
          if (this.cancelled.get())
          {
            ((SpscLinkedArrayQueue)localObject).clear();
            return;
          }
          boolean bool = this.done;
          if ((bool) && (!this.delayError))
          {
            Throwable localThrowable = this.error;
            if (localThrowable != null)
            {
              ((SpscLinkedArrayQueue)localObject).clear();
              localSubscriber.onError(localThrowable);
              return;
            }
          }
          localSubscriber.onNext(null);
          if (bool)
          {
            localObject = this.error;
            if (localObject != null) {
              localSubscriber.onError((Throwable)localObject);
            } else {
              localSubscriber.onComplete();
            }
            return;
          }
        }
        int j = addAndGet(-i);
        if (j == 0) {
          return;
        }
        i = j;
        if (localSubscriber == null)
        {
          localSubscriber = (Subscriber)this.actual.get();
          i = j;
        }
      }
    }
    
    void drainNormal()
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      boolean bool2 = this.delayError;
      Subscriber localSubscriber = (Subscriber)this.actual.get();
      int i = 1;
      for (;;)
      {
        if (localSubscriber != null)
        {
          long l2 = this.requested.get();
          for (long l1 = 0L; l1 != l2; l1 += 1L)
          {
            boolean bool3 = this.done;
            Object localObject = localSpscLinkedArrayQueue.poll();
            boolean bool1;
            if (localObject == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool3, bool1, localSubscriber, bool2)) {
              return;
            }
            if (bool1) {
              break;
            }
            localSubscriber.onNext(localObject);
          }
          if ((l1 == l2) && (checkTerminated(this.done, localSpscLinkedArrayQueue.isEmpty(), localSubscriber, bool2))) {
            return;
          }
          if (l1 != 0L)
          {
            if (l2 != Long.MAX_VALUE) {
              this.requested.addAndGet(-l1);
            }
            this.parent.s.request(l1);
          }
        }
        int j = addAndGet(-i);
        if (j == 0) {
          return;
        }
        i = j;
        if (localSubscriber == null)
        {
          localSubscriber = (Subscriber)this.actual.get();
          i = j;
        }
      }
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      this.queue.offer(paramT);
      drain();
    }
    
    public T poll()
    {
      Object localObject = this.queue.poll();
      if (localObject != null)
      {
        this.produced += 1;
        return (T)localObject;
      }
      int i = this.produced;
      if (i != 0)
      {
        this.produced = 0;
        this.parent.s.request(i);
      }
      return null;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
    
    public void subscribe(Subscriber<? super T> paramSubscriber)
    {
      if (this.once.compareAndSet(false, true))
      {
        paramSubscriber.onSubscribe(this);
        this.actual.lazySet(paramSubscriber);
        drain();
      }
      else
      {
        EmptySubscription.error(new IllegalStateException("Only one Subscriber allowed!"), paramSubscriber);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableGroupBy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */