package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableZip<T, R>
  extends Flowable<R>
{
  final int bufferSize;
  final boolean delayError;
  final Publisher<? extends T>[] sources;
  final Iterable<? extends Publisher<? extends T>> sourcesIterable;
  final Function<? super Object[], ? extends R> zipper;
  
  public FlowableZip(Publisher<? extends T>[] paramArrayOfPublisher, Iterable<? extends Publisher<? extends T>> paramIterable, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
  {
    this.sources = paramArrayOfPublisher;
    this.sourcesIterable = paramIterable;
    this.zipper = paramFunction;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  public void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    Object localObject2 = this.sources;
    if (localObject2 == null)
    {
      localObject1 = new Publisher[8];
      Iterator localIterator = this.sourcesIterable.iterator();
      int i = 0;
      for (;;)
      {
        localObject2 = localObject1;
        j = i;
        if (!localIterator.hasNext()) {
          break;
        }
        Publisher localPublisher = (Publisher)localIterator.next();
        localObject2 = localObject1;
        if (i == localObject1.length)
        {
          localObject2 = new Publisher[(i >> 2) + i];
          System.arraycopy(localObject1, 0, localObject2, 0, i);
        }
        localObject2[i] = localPublisher;
        i++;
        localObject1 = localObject2;
      }
    }
    int j = localObject2.length;
    if (j == 0)
    {
      EmptySubscription.complete(paramSubscriber);
      return;
    }
    Object localObject1 = new ZipCoordinator(paramSubscriber, this.zipper, j, this.bufferSize, this.delayError);
    paramSubscriber.onSubscribe((Subscription)localObject1);
    ((ZipCoordinator)localObject1).subscribe((Publisher[])localObject2, j);
  }
  
  static final class ZipCoordinator<T, R>
    extends AtomicInteger
    implements Subscription
  {
    private static final long serialVersionUID = -2434867452883857743L;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    final Object[] current;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicThrowable errors;
    final AtomicLong requested;
    final FlowableZip.ZipSubscriber<T, R>[] subscribers;
    final Function<? super Object[], ? extends R> zipper;
    
    ZipCoordinator(Subscriber<? super R> paramSubscriber, Function<? super Object[], ? extends R> paramFunction, int paramInt1, int paramInt2, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.zipper = paramFunction;
      this.delayErrors = paramBoolean;
      paramSubscriber = new FlowableZip.ZipSubscriber[paramInt1];
      for (int i = 0; i < paramInt1; i++) {
        paramSubscriber[i] = new FlowableZip.ZipSubscriber(this, paramInt2, i);
      }
      this.current = new Object[paramInt1];
      this.subscribers = paramSubscriber;
      this.requested = new AtomicLong();
      this.errors = new AtomicThrowable();
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        cancelAll();
      }
    }
    
    void cancelAll()
    {
      FlowableZip.ZipSubscriber[] arrayOfZipSubscriber = this.subscribers;
      int j = arrayOfZipSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfZipSubscriber[i].cancel();
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      FlowableZip.ZipSubscriber[] arrayOfZipSubscriber = this.subscribers;
      int n = arrayOfZipSubscriber.length;
      Object[] arrayOfObject = this.current;
      int j = 1;
      int i;
      do
      {
        long l2 = this.requested.get();
        long l1 = 0L;
        int k;
        boolean bool;
        while (l2 != l1)
        {
          if (this.cancelled) {
            return;
          }
          if ((!this.delayErrors) && (this.errors.get() != null))
          {
            cancelAll();
            localSubscriber.onError(this.errors.terminate());
            return;
          }
          int m = 0;
          k = 0;
          while (k < n)
          {
            Object localObject1 = arrayOfZipSubscriber[k];
            i = m;
            if (arrayOfObject[k] == null) {
              try
              {
                bool = ((FlowableZip.ZipSubscriber)localObject1).done;
                localObject1 = ((FlowableZip.ZipSubscriber)localObject1).queue;
                if (localObject1 != null) {
                  localObject1 = ((SimpleQueue)localObject1).poll();
                } else {
                  localObject1 = null;
                }
                if (localObject1 == null) {
                  i = 1;
                } else {
                  i = 0;
                }
                if ((bool) && (i != 0))
                {
                  cancelAll();
                  if ((Throwable)this.errors.get() != null) {
                    localSubscriber.onError(this.errors.terminate());
                  } else {
                    localSubscriber.onComplete();
                  }
                  return;
                }
                if (i == 0)
                {
                  arrayOfObject[k] = localObject1;
                  i = m;
                }
                else
                {
                  i = 1;
                }
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                this.errors.addThrowable(localThrowable1);
                if (!this.delayErrors)
                {
                  cancelAll();
                  localSubscriber.onError(this.errors.terminate());
                  return;
                }
                i = 1;
              }
            }
            k++;
            m = i;
          }
          if (m == 0) {
            try
            {
              Object localObject2 = ObjectHelper.requireNonNull(this.zipper.apply(arrayOfObject.clone()), "The zipper returned a null value");
              localSubscriber.onNext(localObject2);
              l1 += 1L;
              Arrays.fill(arrayOfObject, null);
            }
            catch (Throwable localThrowable2)
            {
              Exceptions.throwIfFatal(localThrowable2);
              cancelAll();
              this.errors.addThrowable(localThrowable2);
              localSubscriber.onError(this.errors.terminate());
              return;
            }
          }
        }
        if (l2 == l1)
        {
          if (this.cancelled) {
            return;
          }
          if ((!this.delayErrors) && (this.errors.get() != null))
          {
            cancelAll();
            localSubscriber.onError(this.errors.terminate());
            return;
          }
          for (i = 0; i < n; i++)
          {
            Object localObject3 = arrayOfZipSubscriber[i];
            if (arrayOfObject[i] == null) {
              try
              {
                bool = ((FlowableZip.ZipSubscriber)localObject3).done;
                localObject3 = ((FlowableZip.ZipSubscriber)localObject3).queue;
                if (localObject3 != null) {
                  localObject3 = ((SimpleQueue)localObject3).poll();
                } else {
                  localObject3 = null;
                }
                if (localObject3 == null) {
                  k = 1;
                } else {
                  k = 0;
                }
                if ((bool) && (k != 0))
                {
                  cancelAll();
                  if ((Throwable)this.errors.get() != null) {
                    localSubscriber.onError(this.errors.terminate());
                  } else {
                    localSubscriber.onComplete();
                  }
                  return;
                }
                if (k == 0) {
                  arrayOfObject[i] = localObject3;
                }
              }
              catch (Throwable localThrowable3)
              {
                Exceptions.throwIfFatal(localThrowable3);
                this.errors.addThrowable(localThrowable3);
                if (!this.delayErrors)
                {
                  cancelAll();
                  localSubscriber.onError(this.errors.terminate());
                  return;
                }
              }
            }
          }
        }
        if (l1 != 0L)
        {
          k = arrayOfZipSubscriber.length;
          for (i = 0; i < k; i++) {
            arrayOfZipSubscriber[i].request(l1);
          }
          if (l2 != Long.MAX_VALUE) {
            this.requested.addAndGet(-l1);
          }
        }
        i = addAndGet(-j);
        j = i;
      } while (i != 0);
    }
    
    void error(FlowableZip.ZipSubscriber<T, R> paramZipSubscriber, Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        paramZipSubscriber.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    void subscribe(Publisher<? extends T>[] paramArrayOfPublisher, int paramInt)
    {
      FlowableZip.ZipSubscriber[] arrayOfZipSubscriber = this.subscribers;
      int i = 0;
      while (i < paramInt) {
        if ((!this.done) && (!this.cancelled) && ((this.delayErrors) || (this.errors.get() == null)))
        {
          paramArrayOfPublisher[i].subscribe(arrayOfZipSubscriber[i]);
          i++;
        }
        else {}
      }
    }
  }
  
  static final class ZipSubscriber<T, R>
    extends AtomicReference<Subscription>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -4627193790118206028L;
    volatile boolean done;
    final int index;
    final int limit;
    final FlowableZip.ZipCoordinator<T, R> parent;
    final int prefetch;
    long produced;
    SimpleQueue<T> queue;
    int sourceMode;
    
    ZipSubscriber(FlowableZip.ZipCoordinator<T, R> paramZipCoordinator, int paramInt1, int paramInt2)
    {
      this.parent = paramZipCoordinator;
      this.prefetch = paramInt1;
      this.index = paramInt2;
      this.limit = (paramInt1 - (paramInt1 >> 2));
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.error(this, paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.sourceMode != 2) {
        this.queue.offer(paramT);
      }
      this.parent.drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(7);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.parent.drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void request(long paramLong)
    {
      if (this.sourceMode != 1)
      {
        paramLong = this.produced + paramLong;
        if (paramLong >= this.limit)
        {
          this.produced = 0L;
          ((Subscription)get()).request(paramLong);
        }
        else
        {
          this.produced = paramLong;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableZip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */