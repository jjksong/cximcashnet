package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTakeUntilPredicate<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Predicate<? super T> predicate;
  
  public FlowableTakeUntilPredicate(Publisher<T> paramPublisher, Predicate<? super T> paramPredicate)
  {
    super(paramPublisher);
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new InnerSubscriber(paramSubscriber, this.predicate));
  }
  
  static final class InnerSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super T> actual;
    boolean done;
    final Predicate<? super T> predicate;
    Subscription s;
    
    InnerSubscriber(Subscriber<? super T> paramSubscriber, Predicate<? super T> paramPredicate)
    {
      this.actual = paramSubscriber;
      this.predicate = paramPredicate;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.done)
      {
        this.done = true;
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        this.actual.onNext(paramT);
        try
        {
          boolean bool = this.predicate.test(paramT);
          if (bool)
          {
            this.done = true;
            this.s.cancel();
            this.actual.onComplete();
          }
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.s.cancel();
          onError(paramT);
          return;
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTakeUntilPredicate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */