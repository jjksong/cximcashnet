package io.reactivex.internal.operators.flowable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableIgnoreElementsCompletable<T>
  extends Completable
  implements FuseToFlowable<T>
{
  final Publisher<T> source;
  
  public FlowableIgnoreElementsCompletable(Publisher<T> paramPublisher)
  {
    this.source = paramPublisher;
  }
  
  public Flowable<T> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableIgnoreElements(this.source));
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    this.source.subscribe(new IgnoreElementsSubscriber(paramCompletableObserver));
  }
  
  static final class IgnoreElementsSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final CompletableObserver actual;
    Subscription s;
    
    IgnoreElementsSubscriber(CompletableObserver paramCompletableObserver)
    {
      this.actual = paramCompletableObserver;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT) {}
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableIgnoreElementsCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */