package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableUsing<T, D>
  extends Flowable<T>
{
  final Consumer<? super D> disposer;
  final boolean eager;
  final Callable<? extends D> resourceSupplier;
  final Function<? super D, ? extends Publisher<? extends T>> sourceSupplier;
  
  public FlowableUsing(Callable<? extends D> paramCallable, Function<? super D, ? extends Publisher<? extends T>> paramFunction, Consumer<? super D> paramConsumer, boolean paramBoolean)
  {
    this.resourceSupplier = paramCallable;
    this.sourceSupplier = paramFunction;
    this.disposer = paramConsumer;
    this.eager = paramBoolean;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    try
    {
      Object localObject = this.resourceSupplier.call();
      try
      {
        Publisher localPublisher = (Publisher)this.sourceSupplier.apply(localObject);
        localPublisher.subscribe(new UsingSubscriber(paramSubscriber, localObject, this.disposer, this.eager));
        return;
      }
      catch (Throwable localThrowable3)
      {
        Exceptions.throwIfFatal(localThrowable3);
        try
        {
          this.disposer.accept(localObject);
          EmptySubscription.error(localThrowable3, paramSubscriber);
          return;
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          EmptySubscription.error(new CompositeException(new Throwable[] { localThrowable3, localThrowable1 }), paramSubscriber);
          return;
        }
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptySubscription.error(localThrowable2, paramSubscriber);
    }
  }
  
  static final class UsingSubscriber<T, D>
    extends AtomicBoolean
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 5904473792286235046L;
    final Subscriber<? super T> actual;
    final Consumer<? super D> disposer;
    final boolean eager;
    final D resource;
    Subscription s;
    
    UsingSubscriber(Subscriber<? super T> paramSubscriber, D paramD, Consumer<? super D> paramConsumer, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.resource = paramD;
      this.disposer = paramConsumer;
      this.eager = paramBoolean;
    }
    
    public void cancel()
    {
      disposeAfter();
      this.s.cancel();
    }
    
    void disposeAfter()
    {
      if (compareAndSet(false, true)) {
        try
        {
          this.disposer.accept(this.resource);
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public void onComplete()
    {
      if (this.eager)
      {
        if (compareAndSet(false, true)) {
          try
          {
            this.disposer.accept(this.resource);
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.actual.onError(localThrowable);
            return;
          }
        }
        this.s.cancel();
        this.actual.onComplete();
      }
      else
      {
        this.actual.onComplete();
        this.s.cancel();
        disposeAfter();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.eager)
      {
        Object localObject2 = null;
        Object localObject1 = localObject2;
        if (compareAndSet(false, true)) {
          try
          {
            this.disposer.accept(this.resource);
            localObject1 = localObject2;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
          }
        }
        this.s.cancel();
        if (localThrowable != null) {
          this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
        } else {
          this.actual.onError(paramThrowable);
        }
      }
      else
      {
        this.actual.onError(paramThrowable);
        this.s.cancel();
        disposeAfter();
      }
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableUsing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */