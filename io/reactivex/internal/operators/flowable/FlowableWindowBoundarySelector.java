package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.processors.UnicastProcessor;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableWindowBoundarySelector<T, B, V>
  extends AbstractFlowableWithUpstream<T, Flowable<T>>
{
  final int bufferSize;
  final Function<? super B, ? extends Publisher<V>> close;
  final Publisher<B> open;
  
  public FlowableWindowBoundarySelector(Publisher<T> paramPublisher, Publisher<B> paramPublisher1, Function<? super B, ? extends Publisher<V>> paramFunction, int paramInt)
  {
    super(paramPublisher);
    this.open = paramPublisher1;
    this.close = paramFunction;
    this.bufferSize = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super Flowable<T>> paramSubscriber)
  {
    this.source.subscribe(new WindowBoundaryMainSubscriber(new SerializedSubscriber(paramSubscriber), this.open, this.close, this.bufferSize));
  }
  
  static final class OperatorWindowBoundaryCloseSubscriber<T, V>
    extends DisposableSubscriber<V>
  {
    boolean done;
    final FlowableWindowBoundarySelector.WindowBoundaryMainSubscriber<T, ?, V> parent;
    final UnicastProcessor<T> w;
    
    OperatorWindowBoundaryCloseSubscriber(FlowableWindowBoundarySelector.WindowBoundaryMainSubscriber<T, ?, V> paramWindowBoundaryMainSubscriber, UnicastProcessor<T> paramUnicastProcessor)
    {
      this.parent = paramWindowBoundaryMainSubscriber;
      this.w = paramUnicastProcessor;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.close(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.error(paramThrowable);
    }
    
    public void onNext(V paramV)
    {
      if (this.done) {
        return;
      }
      this.done = true;
      cancel();
      this.parent.close(this);
    }
  }
  
  static final class OperatorWindowBoundaryOpenSubscriber<T, B>
    extends DisposableSubscriber<B>
  {
    boolean done;
    final FlowableWindowBoundarySelector.WindowBoundaryMainSubscriber<T, B, ?> parent;
    
    OperatorWindowBoundaryOpenSubscriber(FlowableWindowBoundarySelector.WindowBoundaryMainSubscriber<T, B, ?> paramWindowBoundaryMainSubscriber)
    {
      this.parent = paramWindowBoundaryMainSubscriber;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.error(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      if (this.done) {
        return;
      }
      this.parent.open(paramB);
    }
  }
  
  static final class WindowBoundaryMainSubscriber<T, B, V>
    extends QueueDrainSubscriber<T, Object, Flowable<T>>
    implements Subscription
  {
    final AtomicReference<Disposable> boundary = new AtomicReference();
    final int bufferSize;
    final Function<? super B, ? extends Publisher<V>> close;
    final Publisher<B> open;
    final CompositeDisposable resources;
    Subscription s;
    final AtomicLong windows = new AtomicLong();
    final List<UnicastProcessor<T>> ws;
    
    WindowBoundaryMainSubscriber(Subscriber<? super Flowable<T>> paramSubscriber, Publisher<B> paramPublisher, Function<? super B, ? extends Publisher<V>> paramFunction, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.open = paramPublisher;
      this.close = paramFunction;
      this.bufferSize = paramInt;
      this.resources = new CompositeDisposable();
      this.ws = new ArrayList();
      this.windows.lazySet(1L);
    }
    
    public boolean accept(Subscriber<? super Flowable<T>> paramSubscriber, Object paramObject)
    {
      return false;
    }
    
    public void cancel()
    {
      this.cancelled = true;
    }
    
    void close(FlowableWindowBoundarySelector.OperatorWindowBoundaryCloseSubscriber<T, V> paramOperatorWindowBoundaryCloseSubscriber)
    {
      this.resources.delete(paramOperatorWindowBoundaryCloseSubscriber);
      this.queue.offer(new FlowableWindowBoundarySelector.WindowOperation(paramOperatorWindowBoundaryCloseSubscriber.w, null));
      if (enter()) {
        drainLoop();
      }
    }
    
    void dispose()
    {
      this.resources.dispose();
      DisposableHelper.dispose(this.boundary);
    }
    
    void drainLoop()
    {
      Object localObject1 = this.queue;
      Object localObject2 = this.actual;
      List localList = this.ws;
      int i = 1;
      for (;;)
      {
        boolean bool = this.done;
        Object localObject4 = ((SimplePlainQueue)localObject1).poll();
        int j;
        if (localObject4 == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          dispose();
          localObject1 = this.error;
          if (localObject1 != null)
          {
            localObject2 = localList.iterator();
            while (((Iterator)localObject2).hasNext()) {
              ((UnicastProcessor)((Iterator)localObject2).next()).onError((Throwable)localObject1);
            }
          }
          localObject1 = localList.iterator();
          while (((Iterator)localObject1).hasNext()) {
            ((UnicastProcessor)((Iterator)localObject1).next()).onComplete();
          }
          localList.clear();
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if ((localObject4 instanceof FlowableWindowBoundarySelector.WindowOperation))
        {
          localObject4 = (FlowableWindowBoundarySelector.WindowOperation)localObject4;
          if (((FlowableWindowBoundarySelector.WindowOperation)localObject4).w != null)
          {
            if (localList.remove(((FlowableWindowBoundarySelector.WindowOperation)localObject4).w))
            {
              ((FlowableWindowBoundarySelector.WindowOperation)localObject4).w.onComplete();
              if (this.windows.decrementAndGet() == 0L) {
                dispose();
              }
            }
          }
          else if (!this.cancelled)
          {
            Object localObject3 = UnicastProcessor.create(this.bufferSize);
            long l = requested();
            if (l != 0L)
            {
              localList.add(localObject3);
              ((Subscriber)localObject2).onNext(localObject3);
              if (l != Long.MAX_VALUE) {
                produced(1L);
              }
              try
              {
                localObject4 = (Publisher)ObjectHelper.requireNonNull(this.close.apply(((FlowableWindowBoundarySelector.WindowOperation)localObject4).open), "The publisher supplied is null");
                localObject3 = new FlowableWindowBoundarySelector.OperatorWindowBoundaryCloseSubscriber(this, (UnicastProcessor)localObject3);
                if (!this.resources.add((Disposable)localObject3)) {
                  continue;
                }
                this.windows.getAndIncrement();
                ((Publisher)localObject4).subscribe((Subscriber)localObject3);
              }
              catch (Throwable localThrowable)
              {
                this.cancelled = true;
                ((Subscriber)localObject2).onError(localThrowable);
              }
            }
            else
            {
              this.cancelled = true;
              ((Subscriber)localObject2).onError(new MissingBackpressureException("Could not deliver new window due to lack of requests"));
            }
          }
        }
        else
        {
          Iterator localIterator = localList.iterator();
          while (localIterator.hasNext()) {
            ((UnicastProcessor)localIterator.next()).onNext(NotificationLite.getValue(localObject4));
          }
        }
      }
    }
    
    void error(Throwable paramThrowable)
    {
      this.s.cancel();
      this.resources.dispose();
      DisposableHelper.dispose(this.boundary);
      this.actual.onError(paramThrowable);
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        this.resources.dispose();
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        this.resources.dispose();
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (fastEnter())
      {
        Iterator localIterator = this.ws.iterator();
        while (localIterator.hasNext()) {
          ((UnicastProcessor)localIterator.next()).onNext(paramT);
        }
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        FlowableWindowBoundarySelector.OperatorWindowBoundaryOpenSubscriber localOperatorWindowBoundaryOpenSubscriber = new FlowableWindowBoundarySelector.OperatorWindowBoundaryOpenSubscriber(this);
        if (this.boundary.compareAndSet(null, localOperatorWindowBoundaryOpenSubscriber))
        {
          this.windows.getAndIncrement();
          paramSubscription.request(Long.MAX_VALUE);
          this.open.subscribe(localOperatorWindowBoundaryOpenSubscriber);
        }
      }
    }
    
    void open(B paramB)
    {
      this.queue.offer(new FlowableWindowBoundarySelector.WindowOperation(null, paramB));
      if (enter()) {
        drainLoop();
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
  }
  
  static final class WindowOperation<T, B>
  {
    final B open;
    final UnicastProcessor<T> w;
    
    WindowOperation(UnicastProcessor<T> paramUnicastProcessor, B paramB)
    {
      this.w = paramUnicastProcessor;
      this.open = paramB;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableWindowBoundarySelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */