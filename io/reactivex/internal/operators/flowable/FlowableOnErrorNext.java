package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnErrorNext<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final boolean allowFatal;
  final Function<? super Throwable, ? extends Publisher<? extends T>> nextSupplier;
  
  public FlowableOnErrorNext(Publisher<T> paramPublisher, Function<? super Throwable, ? extends Publisher<? extends T>> paramFunction, boolean paramBoolean)
  {
    super(paramPublisher);
    this.nextSupplier = paramFunction;
    this.allowFatal = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    OnErrorNextSubscriber localOnErrorNextSubscriber = new OnErrorNextSubscriber(paramSubscriber, this.nextSupplier, this.allowFatal);
    paramSubscriber.onSubscribe(localOnErrorNextSubscriber.arbiter);
    this.source.subscribe(localOnErrorNextSubscriber);
  }
  
  static final class OnErrorNextSubscriber<T>
    implements Subscriber<T>
  {
    final Subscriber<? super T> actual;
    final boolean allowFatal;
    final SubscriptionArbiter arbiter;
    boolean done;
    final Function<? super Throwable, ? extends Publisher<? extends T>> nextSupplier;
    boolean once;
    
    OnErrorNextSubscriber(Subscriber<? super T> paramSubscriber, Function<? super Throwable, ? extends Publisher<? extends T>> paramFunction, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.nextSupplier = paramFunction;
      this.allowFatal = paramBoolean;
      this.arbiter = new SubscriptionArbiter();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.once = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.once)
      {
        if (this.done)
        {
          RxJavaPlugins.onError(paramThrowable);
          return;
        }
        this.actual.onError(paramThrowable);
        return;
      }
      this.once = true;
      if ((this.allowFatal) && (!(paramThrowable instanceof Exception)))
      {
        this.actual.onError(paramThrowable);
        return;
      }
      try
      {
        Object localObject = (Publisher)this.nextSupplier.apply(paramThrowable);
        if (localObject == null)
        {
          localObject = new NullPointerException("Publisher is null");
          ((NullPointerException)localObject).initCause(paramThrowable);
          this.actual.onError((Throwable)localObject);
          return;
        }
        ((Publisher)localObject).subscribe(this);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      this.actual.onNext(paramT);
      if (!this.once) {
        this.arbiter.produced(1L);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.arbiter.setSubscription(paramSubscription);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnErrorNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */