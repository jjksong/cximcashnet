package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnBackpressureLatest<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableOnBackpressureLatest(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new BackpressureLatestSubscriber(paramSubscriber));
  }
  
  static final class BackpressureLatestSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 163080509307634843L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final AtomicReference<T> current = new AtomicReference();
    volatile boolean done;
    Throwable error;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    
    BackpressureLatestSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.current.lazySet(null);
        }
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<?> paramSubscriber, AtomicReference<T> paramAtomicReference)
    {
      if (this.cancelled)
      {
        paramAtomicReference.lazySet(null);
        return true;
      }
      if (paramBoolean1)
      {
        Throwable localThrowable = this.error;
        if (localThrowable != null)
        {
          paramAtomicReference.lazySet(null);
          paramSubscriber.onError(localThrowable);
          return true;
        }
        if (paramBoolean2)
        {
          paramSubscriber.onComplete();
          return true;
        }
      }
      return false;
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      AtomicLong localAtomicLong = this.requested;
      AtomicReference localAtomicReference = this.current;
      int i = 1;
      int j;
      do
      {
        boolean bool2;
        boolean bool3;
        boolean bool1;
        for (long l1 = 0L;; l1 += 1L)
        {
          long l2 = localAtomicLong.get();
          bool2 = false;
          if (l1 == l2) {
            break;
          }
          bool3 = this.done;
          Object localObject = localAtomicReference.getAndSet(null);
          if (localObject == null) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if (checkTerminated(bool3, bool1, localSubscriber, localAtomicReference)) {
            return;
          }
          if (bool1) {
            break;
          }
          localSubscriber.onNext(localObject);
        }
        if (l1 == localAtomicLong.get())
        {
          bool3 = this.done;
          bool1 = bool2;
          if (localAtomicReference.get() == null) {
            bool1 = true;
          }
          if (checkTerminated(bool3, bool1, localSubscriber, localAtomicReference)) {
            return;
          }
        }
        if (l1 != 0L) {
          BackpressureHelper.produced(localAtomicLong, l1);
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      this.current.lazySet(paramT);
      drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnBackpressureLatest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */