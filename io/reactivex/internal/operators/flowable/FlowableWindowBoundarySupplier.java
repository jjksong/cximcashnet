package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.processors.UnicastProcessor;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableWindowBoundarySupplier<T, B>
  extends AbstractFlowableWithUpstream<T, Flowable<T>>
{
  final int bufferSize;
  final Callable<? extends Publisher<B>> other;
  
  public FlowableWindowBoundarySupplier(Publisher<T> paramPublisher, Callable<? extends Publisher<B>> paramCallable, int paramInt)
  {
    super(paramPublisher);
    this.other = paramCallable;
    this.bufferSize = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super Flowable<T>> paramSubscriber)
  {
    this.source.subscribe(new WindowBoundaryMainSubscriber(new SerializedSubscriber(paramSubscriber), this.other, this.bufferSize));
  }
  
  static final class WindowBoundaryInnerSubscriber<T, B>
    extends DisposableSubscriber<B>
  {
    boolean done;
    final FlowableWindowBoundarySupplier.WindowBoundaryMainSubscriber<T, B> parent;
    
    WindowBoundaryInnerSubscriber(FlowableWindowBoundarySupplier.WindowBoundaryMainSubscriber<T, B> paramWindowBoundaryMainSubscriber)
    {
      this.parent = paramWindowBoundaryMainSubscriber;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      if (this.done) {
        return;
      }
      this.done = true;
      cancel();
      this.parent.next();
    }
  }
  
  static final class WindowBoundaryMainSubscriber<T, B>
    extends QueueDrainSubscriber<T, Object, Flowable<T>>
    implements Subscription
  {
    static final Object NEXT = new Object();
    final AtomicReference<Disposable> boundary = new AtomicReference();
    final int bufferSize;
    final Callable<? extends Publisher<B>> other;
    Subscription s;
    UnicastProcessor<T> window;
    final AtomicLong windows = new AtomicLong();
    
    WindowBoundaryMainSubscriber(Subscriber<? super Flowable<T>> paramSubscriber, Callable<? extends Publisher<B>> paramCallable, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.other = paramCallable;
      this.bufferSize = paramInt;
      this.windows.lazySet(1L);
    }
    
    public void cancel()
    {
      this.cancelled = true;
    }
    
    void drainLoop()
    {
      SimplePlainQueue localSimplePlainQueue = this.queue;
      Object localObject1 = this.actual;
      UnicastProcessor localUnicastProcessor = this.window;
      int i = 1;
      for (;;)
      {
        boolean bool = this.done;
        Object localObject2 = localSimplePlainQueue.poll();
        int j;
        if (localObject2 == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          DisposableHelper.dispose(this.boundary);
          localObject1 = this.error;
          if (localObject1 != null) {
            localUnicastProcessor.onError((Throwable)localObject1);
          } else {
            localUnicastProcessor.onComplete();
          }
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if (localObject2 == NEXT)
        {
          localUnicastProcessor.onComplete();
          if (this.windows.decrementAndGet() == 0L)
          {
            DisposableHelper.dispose(this.boundary);
            return;
          }
          if (!this.cancelled) {
            try
            {
              localObject2 = (Publisher)ObjectHelper.requireNonNull(this.other.call(), "The publisher supplied is null");
              localUnicastProcessor = UnicastProcessor.create(this.bufferSize);
              long l = requested();
              if (l != 0L)
              {
                this.windows.getAndIncrement();
                ((Subscriber)localObject1).onNext(localUnicastProcessor);
                if (l != Long.MAX_VALUE) {
                  produced(1L);
                }
                this.window = localUnicastProcessor;
                FlowableWindowBoundarySupplier.WindowBoundaryInnerSubscriber localWindowBoundaryInnerSubscriber = new FlowableWindowBoundarySupplier.WindowBoundaryInnerSubscriber(this);
                AtomicReference localAtomicReference = this.boundary;
                if (localAtomicReference.compareAndSet(localAtomicReference.get(), localWindowBoundaryInnerSubscriber)) {
                  ((Publisher)localObject2).subscribe(localWindowBoundaryInnerSubscriber);
                }
              }
              else
              {
                this.cancelled = true;
                ((Subscriber)localObject1).onError(new MissingBackpressureException("Could not deliver new window due to lack of requests"));
              }
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              DisposableHelper.dispose(this.boundary);
              ((Subscriber)localObject1).onError(localThrowable);
              return;
            }
          }
        }
        else
        {
          localThrowable.onNext(NotificationLite.getValue(localObject2));
        }
      }
    }
    
    void next()
    {
      this.queue.offer(NEXT);
      if (enter()) {
        drainLoop();
      }
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        DisposableHelper.dispose(this.boundary);
      }
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      if (this.windows.decrementAndGet() == 0L) {
        DisposableHelper.dispose(this.boundary);
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (fastEnter())
      {
        this.window.onNext(paramT);
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        Object localObject = this.actual;
        ((Subscriber)localObject).onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        try
        {
          Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.other.call(), "The first window publisher supplied is null");
          UnicastProcessor localUnicastProcessor = UnicastProcessor.create(this.bufferSize);
          long l = requested();
          if (l != 0L)
          {
            ((Subscriber)localObject).onNext(localUnicastProcessor);
            if (l != Long.MAX_VALUE) {
              produced(1L);
            }
            this.window = localUnicastProcessor;
            localObject = new FlowableWindowBoundarySupplier.WindowBoundaryInnerSubscriber(this);
            if (this.boundary.compareAndSet(null, localObject))
            {
              this.windows.getAndIncrement();
              paramSubscription.request(Long.MAX_VALUE);
              localPublisher.subscribe((Subscriber)localObject);
            }
          }
          else
          {
            paramSubscription.cancel();
            ((Subscriber)localObject).onError(new MissingBackpressureException("Could not deliver first window due to lack of requests"));
            return;
          }
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          paramSubscription.cancel();
          ((Subscriber)localObject).onError(localThrowable);
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */