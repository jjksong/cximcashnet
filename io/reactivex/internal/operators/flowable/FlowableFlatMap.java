package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFlatMap<T, U>
  extends AbstractFlowableWithUpstream<T, U>
{
  final int bufferSize;
  final boolean delayErrors;
  final Function<? super T, ? extends Publisher<? extends U>> mapper;
  final int maxConcurrency;
  
  public FlowableFlatMap(Publisher<T> paramPublisher, Function<? super T, ? extends Publisher<? extends U>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.delayErrors = paramBoolean;
    this.maxConcurrency = paramInt1;
    this.bufferSize = paramInt2;
  }
  
  public static <T, U> Subscriber<T> subscribe(Subscriber<? super U> paramSubscriber, Function<? super T, ? extends Publisher<? extends U>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    return new MergeSubscriber(paramSubscriber, paramFunction, paramBoolean, paramInt1, paramInt2);
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    if (FlowableScalarXMap.tryScalarXMapSubscribe(this.source, paramSubscriber, this.mapper)) {
      return;
    }
    this.source.subscribe(subscribe(paramSubscriber, this.mapper, this.delayErrors, this.maxConcurrency, this.bufferSize));
  }
  
  static final class InnerSubscriber<T, U>
    extends AtomicReference<Subscription>
    implements Subscriber<U>, Disposable
  {
    private static final long serialVersionUID = -4606175640614850599L;
    final int bufferSize;
    volatile boolean done;
    int fusionMode;
    final long id;
    final int limit;
    final FlowableFlatMap.MergeSubscriber<T, U> parent;
    long produced;
    volatile SimpleQueue<U> queue;
    
    InnerSubscriber(FlowableFlatMap.MergeSubscriber<T, U> paramMergeSubscriber, long paramLong)
    {
      this.id = paramLong;
      this.parent = paramMergeSubscriber;
      this.bufferSize = paramMergeSubscriber.bufferSize;
      this.limit = (this.bufferSize >> 2);
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.parent.errs.addThrowable(paramThrowable))
      {
        this.done = true;
        this.parent.drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(U paramU)
    {
      if (this.fusionMode != 2) {
        this.parent.tryEmit(paramU, this);
      } else {
        this.parent.drain();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(7);
          if (i == 1)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.parent.drain();
            return;
          }
          if (i == 2)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
          }
        }
        paramSubscription.request(this.bufferSize);
      }
    }
    
    void requestMore(long paramLong)
    {
      if (this.fusionMode != 1)
      {
        paramLong = this.produced + paramLong;
        if (paramLong >= this.limit)
        {
          this.produced = 0L;
          ((Subscription)get()).request(paramLong);
        }
        else
        {
          this.produced = paramLong;
        }
      }
    }
  }
  
  static final class MergeSubscriber<T, U>
    extends AtomicInteger
    implements Subscription, Subscriber<T>
  {
    static final FlowableFlatMap.InnerSubscriber<?, ?>[] CANCELLED = new FlowableFlatMap.InnerSubscriber[0];
    static final FlowableFlatMap.InnerSubscriber<?, ?>[] EMPTY = new FlowableFlatMap.InnerSubscriber[0];
    private static final long serialVersionUID = -2117620485640801370L;
    final Subscriber<? super U> actual;
    final int bufferSize;
    volatile boolean cancelled;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicThrowable errs = new AtomicThrowable();
    long lastId;
    int lastIndex;
    final Function<? super T, ? extends Publisher<? extends U>> mapper;
    final int maxConcurrency;
    volatile SimplePlainQueue<U> queue;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    int scalarEmitted;
    final int scalarLimit;
    final AtomicReference<FlowableFlatMap.InnerSubscriber<?, ?>[]> subscribers = new AtomicReference();
    long uniqueId;
    
    MergeSubscriber(Subscriber<? super U> paramSubscriber, Function<? super T, ? extends Publisher<? extends U>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.delayErrors = paramBoolean;
      this.maxConcurrency = paramInt1;
      this.bufferSize = paramInt2;
      this.scalarLimit = Math.max(1, paramInt1 >> 1);
      this.subscribers.lazySet(EMPTY);
    }
    
    boolean addInner(FlowableFlatMap.InnerSubscriber<T, U> paramInnerSubscriber)
    {
      FlowableFlatMap.InnerSubscriber[] arrayOfInnerSubscriber1;
      FlowableFlatMap.InnerSubscriber[] arrayOfInnerSubscriber2;
      do
      {
        arrayOfInnerSubscriber1 = (FlowableFlatMap.InnerSubscriber[])this.subscribers.get();
        if (arrayOfInnerSubscriber1 == CANCELLED)
        {
          paramInnerSubscriber.dispose();
          return false;
        }
        int i = arrayOfInnerSubscriber1.length;
        arrayOfInnerSubscriber2 = new FlowableFlatMap.InnerSubscriber[i + 1];
        System.arraycopy(arrayOfInnerSubscriber1, 0, arrayOfInnerSubscriber2, 0, i);
        arrayOfInnerSubscriber2[i] = paramInnerSubscriber;
      } while (!this.subscribers.compareAndSet(arrayOfInnerSubscriber1, arrayOfInnerSubscriber2));
      return true;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        disposeAll();
        if (getAndIncrement() == 0)
        {
          SimplePlainQueue localSimplePlainQueue = this.queue;
          if (localSimplePlainQueue != null) {
            localSimplePlainQueue.clear();
          }
        }
      }
    }
    
    boolean checkTerminate()
    {
      if (this.cancelled)
      {
        SimplePlainQueue localSimplePlainQueue = this.queue;
        if (localSimplePlainQueue != null) {
          localSimplePlainQueue.clear();
        }
        return true;
      }
      if ((!this.delayErrors) && (this.errs.get() != null))
      {
        this.actual.onError(this.errs.terminate());
        return true;
      }
      return false;
    }
    
    void disposeAll()
    {
      FlowableFlatMap.InnerSubscriber[] arrayOfInnerSubscriber = (FlowableFlatMap.InnerSubscriber[])this.subscribers.get();
      Object localObject = CANCELLED;
      if (arrayOfInnerSubscriber != localObject)
      {
        localObject = (FlowableFlatMap.InnerSubscriber[])this.subscribers.getAndSet(localObject);
        if (localObject != CANCELLED)
        {
          int j = localObject.length;
          for (int i = 0; i < j; i++) {
            localObject[i].dispose();
          }
          localObject = this.errs.terminate();
          if ((localObject != null) && (localObject != ExceptionHelper.TERMINATED)) {
            RxJavaPlugins.onError((Throwable)localObject);
          }
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        drainLoop();
      }
    }
    
    void drainLoop()
    {
      Subscriber localSubscriber = this.actual;
      int i = 1;
      int j;
      label752:
      do
      {
        do
        {
          if (checkTerminate()) {
            return;
          }
          Object localObject2 = this.queue;
          long l1 = this.requested.get();
          int i1;
          if (l1 == Long.MAX_VALUE) {
            i1 = 1;
          } else {
            i1 = 0;
          }
          long l3;
          if (localObject2 != null)
          {
            l3 = 0L;
            for (;;)
            {
              l2 = 0L;
              localObject1 = null;
              while (l1 != 0L)
              {
                localObject1 = ((SimplePlainQueue)localObject2).poll();
                if (checkTerminate()) {
                  return;
                }
                if (localObject1 == null) {
                  break;
                }
                localSubscriber.onNext(localObject1);
                l3 += 1L;
                l2 += 1L;
                l1 -= 1L;
              }
              if (l2 != 0L) {
                if (i1 != 0) {
                  l1 = Long.MAX_VALUE;
                } else {
                  l1 = this.requested.addAndGet(-l2);
                }
              }
              l4 = l1;
              l2 = l3;
              if (l1 == 0L) {
                break;
              }
              if (localObject1 == null)
              {
                l4 = l1;
                l2 = l3;
                break;
              }
            }
          }
          long l2 = 0L;
          long l4 = l1;
          boolean bool = this.done;
          localObject2 = this.queue;
          Object localObject1 = (FlowableFlatMap.InnerSubscriber[])this.subscribers.get();
          int m = localObject1.length;
          if ((bool) && ((localObject2 == null) || (((SimplePlainQueue)localObject2).isEmpty())) && (m == 0))
          {
            localObject1 = this.errs.terminate();
            if (localObject1 == null) {
              localSubscriber.onComplete();
            } else {
              localSubscriber.onError((Throwable)localObject1);
            }
            return;
          }
          if (m != 0)
          {
            l1 = this.lastId;
            int k = this.lastIndex;
            if (m > k)
            {
              j = k;
              if (localObject1[k].id == l1) {}
            }
            else
            {
              j = k;
              if (m <= k) {
                j = 0;
              }
              for (k = 0; (k < m) && (localObject1[j].id != l1); k++)
              {
                n = j + 1;
                j = n;
                if (n == m) {
                  j = 0;
                }
              }
              this.lastIndex = j;
              this.lastId = localObject1[j].id;
            }
            int i2 = 0;
            int i3 = 0;
            l1 = l2;
            k = m;
            l2 = l4;
            int n = j;
            m = i3;
            j = i2;
            while (m < k)
            {
              if (checkTerminate()) {
                return;
              }
              FlowableFlatMap.InnerSubscriber localInnerSubscriber = localObject1[n];
              localObject2 = null;
              for (;;)
              {
                if (checkTerminate()) {
                  return;
                }
                localSimpleQueue = localInnerSubscriber.queue;
                if (localSimpleQueue == null)
                {
                  l3 = l2;
                  break;
                }
                l3 = 0L;
                for (;;)
                {
                  if (l2 != 0L) {
                    try
                    {
                      localObject2 = localSimpleQueue.poll();
                      if (localObject2 != null)
                      {
                        localSubscriber.onNext(localObject2);
                        if (checkTerminate()) {
                          return;
                        }
                        l2 -= 1L;
                        l3 += 1L;
                      }
                    }
                    catch (Throwable localThrowable)
                    {
                      Exceptions.throwIfFatal(localThrowable);
                      localInnerSubscriber.dispose();
                      this.errs.addThrowable(localThrowable);
                      if (checkTerminate()) {
                        return;
                      }
                      removeInner(localInnerSubscriber);
                      m++;
                      j = 1;
                      break label752;
                    }
                  }
                }
                if (l3 != 0L)
                {
                  if (i1 == 0) {
                    l2 = this.requested.addAndGet(-l3);
                  } else {
                    l2 = Long.MAX_VALUE;
                  }
                  localInnerSubscriber.requestMore(l3);
                  l4 = 0L;
                }
                else
                {
                  l4 = 0L;
                }
                l3 = l2;
                if (l2 == l4) {
                  break;
                }
                if (localThrowable == null)
                {
                  l3 = l2;
                  break;
                }
              }
              Object localObject3 = localObject1;
              bool = localInnerSubscriber.done;
              SimpleQueue localSimpleQueue = localInnerSubscriber.queue;
              if ((bool) && ((localSimpleQueue == null) || (localSimpleQueue.isEmpty())))
              {
                removeInner(localInnerSubscriber);
                if (checkTerminate()) {
                  return;
                }
                l1 += 1L;
                j = 1;
              }
              if (l3 == 0L)
              {
                localObject1 = localObject3;
                break;
              }
              n++;
              if (n == k)
              {
                n = 0;
                l2 = l3;
              }
              else
              {
                l2 = l3;
              }
              m++;
            }
            this.lastIndex = n;
            this.lastId = localObject1[n].id;
            l2 = l1;
          }
          else
          {
            j = 0;
          }
          if ((l2 != 0L) && (!this.cancelled)) {
            this.s.request(l2);
          }
        } while (j != 0);
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    SimpleQueue<U> getInnerQueue(FlowableFlatMap.InnerSubscriber<T, U> paramInnerSubscriber)
    {
      SimpleQueue localSimpleQueue = paramInnerSubscriber.queue;
      Object localObject = localSimpleQueue;
      if (localSimpleQueue == null)
      {
        localObject = new SpscArrayQueue(this.bufferSize);
        paramInnerSubscriber.queue = ((SimpleQueue)localObject);
      }
      return (SimpleQueue<U>)localObject;
    }
    
    SimpleQueue<U> getMainQueue()
    {
      SimplePlainQueue localSimplePlainQueue = this.queue;
      Object localObject = localSimplePlainQueue;
      if (localSimplePlainQueue == null)
      {
        int i = this.maxConcurrency;
        if (i == Integer.MAX_VALUE) {
          localObject = new SpscLinkedArrayQueue(this.bufferSize);
        } else {
          localObject = new SpscArrayQueue(i);
        }
        this.queue = ((SimplePlainQueue)localObject);
      }
      return (SimpleQueue<U>)localObject;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      if (this.errs.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null Publisher");
        if ((localPublisher instanceof Callable)) {
          try
          {
            paramT = ((Callable)localPublisher).call();
            if (paramT != null)
            {
              tryEmitScalar(paramT);
            }
            else if ((this.maxConcurrency != Integer.MAX_VALUE) && (!this.cancelled))
            {
              int i = this.scalarEmitted + 1;
              this.scalarEmitted = i;
              int j = this.scalarLimit;
              if (i == j)
              {
                this.scalarEmitted = 0;
                this.s.request(j);
              }
            }
          }
          catch (Throwable paramT)
          {
            Exceptions.throwIfFatal(paramT);
            this.errs.addThrowable(paramT);
            drain();
            return;
          }
        }
        long l = this.uniqueId;
        this.uniqueId = (1L + l);
        paramT = new FlowableFlatMap.InnerSubscriber(this, l);
        if (addInner(paramT)) {
          localPublisher.subscribe(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        if (!this.cancelled)
        {
          int i = this.maxConcurrency;
          if (i == Integer.MAX_VALUE) {
            paramSubscription.request(Long.MAX_VALUE);
          } else {
            paramSubscription.request(i);
          }
        }
      }
    }
    
    void removeInner(FlowableFlatMap.InnerSubscriber<T, U> paramInnerSubscriber)
    {
      FlowableFlatMap.InnerSubscriber[] arrayOfInnerSubscriber2;
      FlowableFlatMap.InnerSubscriber[] arrayOfInnerSubscriber1;
      do
      {
        arrayOfInnerSubscriber2 = (FlowableFlatMap.InnerSubscriber[])this.subscribers.get();
        if ((arrayOfInnerSubscriber2 == CANCELLED) || (arrayOfInnerSubscriber2 == EMPTY)) {
          break;
        }
        int m = arrayOfInnerSubscriber2.length;
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerSubscriber2[i] == paramInnerSubscriber)
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerSubscriber1 = EMPTY;
        }
        else
        {
          arrayOfInnerSubscriber1 = new FlowableFlatMap.InnerSubscriber[m - 1];
          System.arraycopy(arrayOfInnerSubscriber2, 0, arrayOfInnerSubscriber1, 0, j);
          System.arraycopy(arrayOfInnerSubscriber2, j + 1, arrayOfInnerSubscriber1, j, m - j - 1);
        }
      } while (!this.subscribers.compareAndSet(arrayOfInnerSubscriber2, arrayOfInnerSubscriber1));
      return;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    void tryEmit(U paramU, FlowableFlatMap.InnerSubscriber<T, U> paramInnerSubscriber)
    {
      SimpleQueue localSimpleQueue;
      Object localObject;
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        long l = this.requested.get();
        localSimpleQueue = paramInnerSubscriber.queue;
        if ((l != 0L) && ((localSimpleQueue == null) || (localSimpleQueue.isEmpty())))
        {
          this.actual.onNext(paramU);
          if (l != Long.MAX_VALUE) {
            this.requested.decrementAndGet();
          }
          paramInnerSubscriber.requestMore(1L);
        }
        else
        {
          localObject = localSimpleQueue;
          if (localSimpleQueue == null) {
            localObject = getInnerQueue(paramInnerSubscriber);
          }
          if (!((SimpleQueue)localObject).offer(paramU))
          {
            onError(new MissingBackpressureException("Inner queue full?!"));
            return;
          }
        }
        if (decrementAndGet() != 0) {}
      }
      else
      {
        localSimpleQueue = paramInnerSubscriber.queue;
        localObject = localSimpleQueue;
        if (localSimpleQueue == null)
        {
          localObject = new SpscArrayQueue(this.bufferSize);
          paramInnerSubscriber.queue = ((SimpleQueue)localObject);
        }
        if (!((SimpleQueue)localObject).offer(paramU))
        {
          onError(new MissingBackpressureException("Inner queue full?!"));
          return;
        }
        if (getAndIncrement() != 0) {
          return;
        }
      }
      drainLoop();
    }
    
    void tryEmitScalar(U paramU)
    {
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        long l = this.requested.get();
        SimplePlainQueue localSimplePlainQueue = this.queue;
        if ((l != 0L) && ((localSimplePlainQueue == null) || (localSimplePlainQueue.isEmpty())))
        {
          this.actual.onNext(paramU);
          if (l != Long.MAX_VALUE) {
            this.requested.decrementAndGet();
          }
          if ((this.maxConcurrency != Integer.MAX_VALUE) && (!this.cancelled))
          {
            int i = this.scalarEmitted + 1;
            this.scalarEmitted = i;
            int j = this.scalarLimit;
            if (i == j)
            {
              this.scalarEmitted = 0;
              this.s.request(j);
            }
          }
        }
        else
        {
          Object localObject = localSimplePlainQueue;
          if (localSimplePlainQueue == null) {
            localObject = getMainQueue();
          }
          if (!((SimpleQueue)localObject).offer(paramU))
          {
            onError(new IllegalStateException("Scalar queue full?!"));
            return;
          }
        }
        if (decrementAndGet() != 0) {}
      }
      else
      {
        if (!getMainQueue().offer(paramU))
        {
          onError(new IllegalStateException("Scalar queue full?!"));
          return;
        }
        if (getAndIncrement() != 0) {
          return;
        }
      }
      drainLoop();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFlatMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */