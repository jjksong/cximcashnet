package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.HasUpstreamPublisher;
import org.reactivestreams.Publisher;

abstract class AbstractFlowableWithUpstream<T, R>
  extends Flowable<R>
  implements HasUpstreamPublisher<T>
{
  protected final Publisher<T> source;
  
  AbstractFlowableWithUpstream(Publisher<T> paramPublisher)
  {
    this.source = ((Publisher)ObjectHelper.requireNonNull(paramPublisher, "source is null"));
  }
  
  public final Publisher<T> source()
  {
    return this.source;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */