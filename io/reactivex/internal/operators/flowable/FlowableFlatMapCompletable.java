package io.reactivex.internal.operators.flowable;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFlatMapCompletable<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final boolean delayErrors;
  final Function<? super T, ? extends CompletableSource> mapper;
  final int maxConcurrency;
  
  public FlowableFlatMapCompletable(Publisher<T> paramPublisher, Function<? super T, ? extends CompletableSource> paramFunction, boolean paramBoolean, int paramInt)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.delayErrors = paramBoolean;
    this.maxConcurrency = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new FlatMapCompletableMainSubscriber(paramSubscriber, this.mapper, this.delayErrors, this.maxConcurrency));
  }
  
  static final class FlatMapCompletableMainSubscriber<T>
    extends BasicIntQueueSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = 8443155186132538303L;
    final Subscriber<? super T> actual;
    final boolean delayErrors;
    final AtomicThrowable errors;
    final Function<? super T, ? extends CompletableSource> mapper;
    final int maxConcurrency;
    Subscription s;
    final CompositeDisposable set;
    
    FlatMapCompletableMainSubscriber(Subscriber<? super T> paramSubscriber, Function<? super T, ? extends CompletableSource> paramFunction, boolean paramBoolean, int paramInt)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.delayErrors = paramBoolean;
      this.errors = new AtomicThrowable();
      this.set = new CompositeDisposable();
      this.maxConcurrency = paramInt;
      lazySet(1);
    }
    
    public void cancel()
    {
      this.s.cancel();
      this.set.dispose();
    }
    
    public void clear() {}
    
    void innerComplete(FlatMapCompletableMainSubscriber<T>.InnerConsumer paramFlatMapCompletableMainSubscriber)
    {
      this.set.delete(paramFlatMapCompletableMainSubscriber);
      onComplete();
    }
    
    void innerError(FlatMapCompletableMainSubscriber<T>.InnerConsumer paramFlatMapCompletableMainSubscriber, Throwable paramThrowable)
    {
      this.set.delete(paramFlatMapCompletableMainSubscriber);
      onError(paramThrowable);
    }
    
    public boolean isEmpty()
    {
      return true;
    }
    
    public void onComplete()
    {
      if (decrementAndGet() == 0)
      {
        Throwable localThrowable = this.errors.terminate();
        if (localThrowable != null) {
          this.actual.onError(localThrowable);
        } else {
          this.actual.onComplete();
        }
      }
      else if (this.maxConcurrency != Integer.MAX_VALUE)
      {
        this.s.request(1L);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        if (this.delayErrors)
        {
          if (decrementAndGet() == 0)
          {
            paramThrowable = this.errors.terminate();
            this.actual.onError(paramThrowable);
            return;
          }
          if (this.maxConcurrency != Integer.MAX_VALUE) {
            this.s.request(1L);
          }
        }
        else
        {
          cancel();
          if (getAndSet(0) > 0)
          {
            paramThrowable = this.errors.terminate();
            this.actual.onError(paramThrowable);
          }
        }
      }
      else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      try
      {
        CompletableSource localCompletableSource = (CompletableSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null CompletableSource");
        getAndIncrement();
        paramT = new InnerConsumer();
        if (this.set.add(paramT)) {
          localCompletableSource.subscribe(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        int i = this.maxConcurrency;
        if (i == Integer.MAX_VALUE) {
          paramSubscription.request(Long.MAX_VALUE);
        } else {
          paramSubscription.request(i);
        }
      }
    }
    
    public T poll()
      throws Exception
    {
      return null;
    }
    
    public void request(long paramLong) {}
    
    public int requestFusion(int paramInt)
    {
      return paramInt & 0x2;
    }
    
    final class InnerConsumer
      extends AtomicReference<Disposable>
      implements CompletableObserver, Disposable
    {
      private static final long serialVersionUID = 8606673141535671828L;
      
      InnerConsumer() {}
      
      public void dispose()
      {
        DisposableHelper.dispose(this);
      }
      
      public boolean isDisposed()
      {
        return DisposableHelper.isDisposed((Disposable)get());
      }
      
      public void onComplete()
      {
        FlowableFlatMapCompletable.FlatMapCompletableMainSubscriber.this.innerComplete(this);
      }
      
      public void onError(Throwable paramThrowable)
      {
        FlowableFlatMapCompletable.FlatMapCompletableMainSubscriber.this.innerError(this, paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this, paramDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFlatMapCompletable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */