package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.schedulers.Timed;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTimeInterval<T>
  extends AbstractFlowableWithUpstream<T, Timed<T>>
{
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public FlowableTimeInterval(Publisher<T> paramPublisher, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramPublisher);
    this.scheduler = paramScheduler;
    this.unit = paramTimeUnit;
  }
  
  protected void subscribeActual(Subscriber<? super Timed<T>> paramSubscriber)
  {
    this.source.subscribe(new TimeIntervalSubscriber(paramSubscriber, this.unit, this.scheduler));
  }
  
  static final class TimeIntervalSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super Timed<T>> actual;
    long lastTime;
    Subscription s;
    final Scheduler scheduler;
    final TimeUnit unit;
    
    TimeIntervalSubscriber(Subscriber<? super Timed<T>> paramSubscriber, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.actual = paramSubscriber;
      this.scheduler = paramScheduler;
      this.unit = paramTimeUnit;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.lastTime;
      this.lastTime = l1;
      this.actual.onNext(new Timed(paramT, l1 - l2, this.unit));
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.lastTime = this.scheduler.now(this.unit);
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTimeInterval.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */