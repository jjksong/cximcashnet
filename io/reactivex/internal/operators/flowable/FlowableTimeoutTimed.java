package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.subscribers.FullArbiterSubscriber;
import io.reactivex.internal.subscriptions.FullArbiter;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTimeoutTimed<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  static final Disposable NEW_TIMER = new Disposable()
  {
    public void dispose() {}
    
    public boolean isDisposed()
    {
      return true;
    }
  };
  final Publisher<? extends T> other;
  final Scheduler scheduler;
  final long timeout;
  final TimeUnit unit;
  
  public FlowableTimeoutTimed(Publisher<T> paramPublisher, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, Publisher<? extends T> paramPublisher1)
  {
    super(paramPublisher);
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.other = paramPublisher1;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if (this.other == null) {
      this.source.subscribe(new TimeoutTimedSubscriber(new SerializedSubscriber(paramSubscriber), this.timeout, this.unit, this.scheduler.createWorker()));
    } else {
      this.source.subscribe(new TimeoutTimedOtherSubscriber(paramSubscriber, this.timeout, this.unit, this.scheduler.createWorker(), this.other));
    }
  }
  
  static final class TimeoutTimedOtherSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final Subscriber<? super T> actual;
    final FullArbiter<T> arbiter;
    volatile boolean done;
    volatile long index;
    final Publisher<? extends T> other;
    Subscription s;
    final long timeout;
    final AtomicReference<Disposable> timer = new AtomicReference();
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    TimeoutTimedOtherSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
      this.other = paramPublisher;
      this.arbiter = new FullArbiter(paramSubscriber, this, 8);
    }
    
    public void dispose()
    {
      this.worker.dispose();
      DisposableHelper.dispose(this.timer);
      this.s.cancel();
    }
    
    public boolean isDisposed()
    {
      return this.worker.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.worker.dispose();
      DisposableHelper.dispose(this.timer);
      this.arbiter.onComplete(this.s);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.worker.dispose();
      DisposableHelper.dispose(this.timer);
      this.arbiter.onError(paramThrowable, this.s);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      if (this.arbiter.onNext(paramT, this.s)) {
        scheduleTimeout(l);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if (this.arbiter.setSubscription(paramSubscription))
        {
          this.actual.onSubscribe(this.arbiter);
          scheduleTimeout(0L);
        }
      }
    }
    
    void scheduleTimeout(final long paramLong)
    {
      Disposable localDisposable = (Disposable)this.timer.get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      if (this.timer.compareAndSet(localDisposable, FlowableTimeoutTimed.NEW_TIMER))
      {
        localDisposable = this.worker.schedule(new Runnable()
        {
          public void run()
          {
            if (paramLong == FlowableTimeoutTimed.TimeoutTimedOtherSubscriber.this.index)
            {
              FlowableTimeoutTimed.TimeoutTimedOtherSubscriber localTimeoutTimedOtherSubscriber = FlowableTimeoutTimed.TimeoutTimedOtherSubscriber.this;
              localTimeoutTimedOtherSubscriber.done = true;
              localTimeoutTimedOtherSubscriber.s.cancel();
              DisposableHelper.dispose(FlowableTimeoutTimed.TimeoutTimedOtherSubscriber.this.timer);
              FlowableTimeoutTimed.TimeoutTimedOtherSubscriber.this.subscribeNext();
              FlowableTimeoutTimed.TimeoutTimedOtherSubscriber.this.worker.dispose();
            }
          }
        }, this.timeout, this.unit);
        DisposableHelper.replace(this.timer, localDisposable);
      }
    }
    
    void subscribeNext()
    {
      this.other.subscribe(new FullArbiterSubscriber(this.arbiter));
    }
  }
  
  static final class TimeoutTimedSubscriber<T>
    implements Subscriber<T>, Disposable, Subscription
  {
    final Subscriber<? super T> actual;
    volatile boolean done;
    volatile long index;
    Subscription s;
    final long timeout;
    final AtomicReference<Disposable> timer = new AtomicReference();
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    TimeoutTimedSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      this.actual = paramSubscriber;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
    }
    
    public void cancel()
    {
      dispose();
    }
    
    public void dispose()
    {
      this.worker.dispose();
      DisposableHelper.dispose(this.timer);
      this.s.cancel();
    }
    
    public boolean isDisposed()
    {
      return this.worker.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      this.actual.onNext(paramT);
      scheduleTimeout(l);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        scheduleTimeout(0L);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
    
    void scheduleTimeout(final long paramLong)
    {
      Disposable localDisposable = (Disposable)this.timer.get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      if (this.timer.compareAndSet(localDisposable, FlowableTimeoutTimed.NEW_TIMER))
      {
        localDisposable = this.worker.schedule(new Runnable()
        {
          public void run()
          {
            if (paramLong == FlowableTimeoutTimed.TimeoutTimedSubscriber.this.index)
            {
              FlowableTimeoutTimed.TimeoutTimedSubscriber localTimeoutTimedSubscriber = FlowableTimeoutTimed.TimeoutTimedSubscriber.this;
              localTimeoutTimedSubscriber.done = true;
              localTimeoutTimedSubscriber.dispose();
              FlowableTimeoutTimed.TimeoutTimedSubscriber.this.actual.onError(new TimeoutException());
            }
          }
        }, this.timeout, this.unit);
        DisposableHelper.replace(this.timer, localDisposable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTimeoutTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */