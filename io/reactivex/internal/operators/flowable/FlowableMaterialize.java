package io.reactivex.internal.operators.flowable;

import io.reactivex.Notification;
import io.reactivex.internal.subscribers.SinglePostCompleteSubscriber;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableMaterialize<T>
  extends AbstractFlowableWithUpstream<T, Notification<T>>
{
  public FlowableMaterialize(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super Notification<T>> paramSubscriber)
  {
    this.source.subscribe(new MaterializeSubscriber(paramSubscriber));
  }
  
  static final class MaterializeSubscriber<T>
    extends SinglePostCompleteSubscriber<T, Notification<T>>
  {
    private static final long serialVersionUID = -3740826063558713822L;
    
    MaterializeSubscriber(Subscriber<? super Notification<T>> paramSubscriber)
    {
      super();
    }
    
    public void onComplete()
    {
      complete(Notification.createOnComplete());
    }
    
    protected void onDrop(Notification<T> paramNotification)
    {
      if (paramNotification.isOnError()) {
        RxJavaPlugins.onError(paramNotification.getError());
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      complete(Notification.createOnError(paramThrowable));
    }
    
    public void onNext(T paramT)
    {
      this.produced += 1L;
      this.actual.onNext(Notification.createOnNext(paramT));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableMaterialize.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */