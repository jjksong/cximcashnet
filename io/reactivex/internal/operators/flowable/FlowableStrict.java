package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableStrict<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableStrict(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new StrictSubscriber(paramSubscriber));
  }
  
  static final class StrictSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -4945028590049415624L;
    final Subscriber<? super T> actual;
    volatile boolean done;
    final AtomicThrowable error;
    final AtomicBoolean once;
    final AtomicLong requested;
    final AtomicReference<Subscription> s;
    
    StrictSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
      this.error = new AtomicThrowable();
      this.requested = new AtomicLong();
      this.s = new AtomicReference();
      this.once = new AtomicBoolean();
    }
    
    public void cancel()
    {
      if (!this.done) {
        SubscriptionHelper.cancel(this.s);
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      HalfSerializer.onComplete(this.actual, this, this.error);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.done = true;
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    public void onNext(T paramT)
    {
      HalfSerializer.onNext(this.actual, paramT, this, this.error);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (this.once.compareAndSet(false, true))
      {
        this.actual.onSubscribe(this);
        SubscriptionHelper.deferredSetOnce(this.s, this.requested, paramSubscription);
      }
      else
      {
        paramSubscription.cancel();
        cancel();
        onError(new IllegalStateException("§2.12 violated: onSubscribe must be called at most once"));
      }
    }
    
    public void request(long paramLong)
    {
      if (paramLong <= 0L)
      {
        cancel();
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("§3.9 violated: positive request amount required but it was ");
        localStringBuilder.append(paramLong);
        onError(new IllegalArgumentException(localStringBuilder.toString()));
      }
      else
      {
        SubscriptionHelper.deferredRequest(this.s, this.requested, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableStrict.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */