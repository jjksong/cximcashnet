package io.reactivex.internal.operators.flowable;

import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableGenerate<T, S>
  extends Flowable<T>
{
  final Consumer<? super S> disposeState;
  final BiFunction<S, Emitter<T>, S> generator;
  final Callable<S> stateSupplier;
  
  public FlowableGenerate(Callable<S> paramCallable, BiFunction<S, Emitter<T>, S> paramBiFunction, Consumer<? super S> paramConsumer)
  {
    this.stateSupplier = paramCallable;
    this.generator = paramBiFunction;
    this.disposeState = paramConsumer;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    try
    {
      Object localObject = this.stateSupplier.call();
      paramSubscriber.onSubscribe(new GeneratorSubscription(paramSubscriber, this.generator, this.disposeState, localObject));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class GeneratorSubscription<T, S>
    extends AtomicLong
    implements Emitter<T>, Subscription
  {
    private static final long serialVersionUID = 7565982551505011832L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final Consumer<? super S> disposeState;
    final BiFunction<S, ? super Emitter<T>, S> generator;
    boolean hasNext;
    S state;
    boolean terminate;
    
    GeneratorSubscription(Subscriber<? super T> paramSubscriber, BiFunction<S, ? super Emitter<T>, S> paramBiFunction, Consumer<? super S> paramConsumer, S paramS)
    {
      this.actual = paramSubscriber;
      this.generator = paramBiFunction;
      this.disposeState = paramConsumer;
      this.state = paramS;
    }
    
    private void dispose(S paramS)
    {
      try
      {
        this.disposeState.accept(paramS);
      }
      catch (Throwable paramS)
      {
        Exceptions.throwIfFatal(paramS);
        RxJavaPlugins.onError(paramS);
      }
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        if (BackpressureHelper.add(this, 1L) == 0L)
        {
          Object localObject = this.state;
          this.state = null;
          dispose(localObject);
        }
      }
    }
    
    public void onComplete()
    {
      if (!this.terminate)
      {
        this.terminate = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.terminate)
      {
        RxJavaPlugins.onError(paramThrowable);
      }
      else
      {
        Object localObject = paramThrowable;
        if (paramThrowable == null) {
          localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        this.terminate = true;
        this.actual.onError((Throwable)localObject);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.terminate) {
        if (this.hasNext)
        {
          onError(new IllegalStateException("onNext already called in this generate turn"));
        }
        else if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        }
        else
        {
          this.hasNext = true;
          this.actual.onNext(paramT);
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (!SubscriptionHelper.validate(paramLong)) {
        return;
      }
      if (BackpressureHelper.add(this, paramLong) != 0L) {
        return;
      }
      Object localObject1 = this.state;
      BiFunction localBiFunction = this.generator;
      long l1 = 0L;
      for (;;)
      {
        Object localObject3 = localObject1;
        if (l1 != paramLong)
        {
          if (this.cancelled)
          {
            this.state = null;
            dispose(localObject3);
            return;
          }
          this.hasNext = false;
          try
          {
            localObject1 = localBiFunction.apply(localObject3, this);
            if (this.terminate)
            {
              this.cancelled = true;
              this.state = null;
              dispose(localObject1);
              return;
            }
            l1 += 1L;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.cancelled = true;
            this.state = null;
            onError(localThrowable);
            dispose(localObject3);
            return;
          }
        }
        else
        {
          long l2 = get();
          Object localObject2 = localObject3;
          paramLong = l2;
          if (l1 == l2)
          {
            this.state = localObject3;
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
            localObject2 = localObject3;
          }
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableGenerate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */