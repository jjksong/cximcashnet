package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.ArrayListSupplier;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableToListSingle<T, U extends Collection<? super T>>
  extends Single<U>
  implements FuseToFlowable<U>
{
  final Callable<U> collectionSupplier;
  final Publisher<T> source;
  
  public FlowableToListSingle(Publisher<T> paramPublisher)
  {
    this(paramPublisher, ArrayListSupplier.asCallable());
  }
  
  public FlowableToListSingle(Publisher<T> paramPublisher, Callable<U> paramCallable)
  {
    this.source = paramPublisher;
    this.collectionSupplier = paramCallable;
  }
  
  public Flowable<U> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableToList(this.source, this.collectionSupplier));
  }
  
  protected void subscribeActual(SingleObserver<? super U> paramSingleObserver)
  {
    try
    {
      Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.collectionSupplier.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      this.source.subscribe(new ToListSubscriber(paramSingleObserver, localCollection));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptyDisposable.error(localThrowable, paramSingleObserver);
    }
  }
  
  static final class ToListSubscriber<T, U extends Collection<? super T>>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super U> actual;
    Subscription s;
    U value;
    
    ToListSubscriber(SingleObserver<? super U> paramSingleObserver, U paramU)
    {
      this.actual = paramSingleObserver;
      this.value = paramU;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onSuccess(this.value);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.value = null;
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.value.add(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableToListSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */