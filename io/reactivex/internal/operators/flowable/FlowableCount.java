package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCount<T>
  extends AbstractFlowableWithUpstream<T, Long>
{
  public FlowableCount(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super Long> paramSubscriber)
  {
    this.source.subscribe(new CountSubscriber(paramSubscriber));
  }
  
  static final class CountSubscriber
    extends DeferredScalarSubscription<Long>
    implements Subscriber<Object>
  {
    private static final long serialVersionUID = 4973004223787171406L;
    long count;
    Subscription s;
    
    CountSubscriber(Subscriber<? super Long> paramSubscriber)
    {
      super();
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      complete(Long.valueOf(this.count));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.count += 1L;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */