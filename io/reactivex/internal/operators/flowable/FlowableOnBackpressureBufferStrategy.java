package io.reactivex.internal.operators.flowable;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Action;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnBackpressureBufferStrategy<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long bufferSize;
  final Action onOverflow;
  final BackpressureOverflowStrategy strategy;
  
  public FlowableOnBackpressureBufferStrategy(Publisher<T> paramPublisher, long paramLong, Action paramAction, BackpressureOverflowStrategy paramBackpressureOverflowStrategy)
  {
    super(paramPublisher);
    this.bufferSize = paramLong;
    this.onOverflow = paramAction;
    this.strategy = paramBackpressureOverflowStrategy;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new OnBackpressureBufferStrategySubscriber(paramSubscriber, this.onOverflow, this.strategy, this.bufferSize));
  }
  
  static final class OnBackpressureBufferStrategySubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 3240706908776709697L;
    final Subscriber<? super T> actual;
    final long bufferSize;
    volatile boolean cancelled;
    final Deque<T> deque;
    volatile boolean done;
    Throwable error;
    final Action onOverflow;
    final AtomicLong requested;
    Subscription s;
    final BackpressureOverflowStrategy strategy;
    
    OnBackpressureBufferStrategySubscriber(Subscriber<? super T> paramSubscriber, Action paramAction, BackpressureOverflowStrategy paramBackpressureOverflowStrategy, long paramLong)
    {
      this.actual = paramSubscriber;
      this.onOverflow = paramAction;
      this.strategy = paramBackpressureOverflowStrategy;
      this.bufferSize = paramLong;
      this.requested = new AtomicLong();
      this.deque = new ArrayDeque();
    }
    
    public void cancel()
    {
      this.cancelled = true;
      this.s.cancel();
      if (getAndIncrement() == 0) {
        clear(this.deque);
      }
    }
    
    void clear(Deque<T> paramDeque)
    {
      try
      {
        paramDeque.clear();
        return;
      }
      finally {}
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Deque localDeque = this.deque;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      int j;
      do
      {
        long l2 = this.requested.get();
        long l1 = 0L;
        boolean bool1;
        Object localObject3;
        while (l1 != l2)
        {
          if (this.cancelled)
          {
            clear(localDeque);
            return;
          }
          bool1 = this.done;
          try
          {
            localObject3 = localDeque.poll();
            if (localObject3 == null) {
              j = 1;
            } else {
              j = 0;
            }
            if (bool1)
            {
              Throwable localThrowable = this.error;
              if (localThrowable != null)
              {
                clear(localDeque);
                localSubscriber.onError(localThrowable);
                return;
              }
              if (j != 0)
              {
                localSubscriber.onComplete();
                return;
              }
            }
            if (j == 0)
            {
              localSubscriber.onNext(localObject3);
              l1 += 1L;
            }
          }
          finally {}
        }
        if (l1 == l2)
        {
          if (this.cancelled)
          {
            clear(localDeque);
            return;
          }
          bool1 = this.done;
          try
          {
            boolean bool2 = localDeque.isEmpty();
            if (bool1)
            {
              localObject3 = this.error;
              if (localObject3 != null)
              {
                clear(localDeque);
                ((Subscriber)localObject1).onError((Throwable)localObject3);
                return;
              }
              if (bool2)
              {
                ((Subscriber)localObject1).onComplete();
                return;
              }
            }
          }
          finally {}
        }
        if (l1 != 0L) {
          BackpressureHelper.produced(this.requested, l1);
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      synchronized (this.deque)
      {
        long l1 = ???.size();
        long l2 = this.bufferSize;
        int j = 0;
        int i = 1;
        if (l1 == l2)
        {
          switch (FlowableOnBackpressureBufferStrategy.1.$SwitchMap$io$reactivex$BackpressureOverflowStrategy[this.strategy.ordinal()])
          {
          default: 
            break;
          case 2: 
            ???.poll();
            ???.offer(paramT);
            j = 1;
            i = 0;
            break;
          case 1: 
            ???.pollLast();
            ???.offer(paramT);
            j = 1;
            i = 0;
            break;
          }
        }
        else
        {
          ???.offer(paramT);
          i = 0;
        }
        if (j != 0)
        {
          paramT = this.onOverflow;
          if (paramT != null) {
            try
            {
              paramT.run();
            }
            catch (Throwable paramT)
            {
              Exceptions.throwIfFatal(paramT);
              this.s.cancel();
              onError(paramT);
            }
          }
        }
        else if (i != 0)
        {
          this.s.cancel();
          onError(new MissingBackpressureException());
        }
        else
        {
          drain();
        }
        return;
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnBackpressureBufferStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */