package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableElementAt<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final T defaultValue;
  final boolean errorOnFewer;
  final long index;
  
  public FlowableElementAt(Publisher<T> paramPublisher, long paramLong, T paramT, boolean paramBoolean)
  {
    super(paramPublisher);
    this.index = paramLong;
    this.defaultValue = paramT;
    this.errorOnFewer = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new ElementAtSubscriber(paramSubscriber, this.index, this.defaultValue, this.errorOnFewer));
  }
  
  static final class ElementAtSubscriber<T>
    extends DeferredScalarSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = 4066607327284737757L;
    long count;
    final T defaultValue;
    boolean done;
    final boolean errorOnFewer;
    final long index;
    Subscription s;
    
    ElementAtSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, T paramT, boolean paramBoolean)
    {
      super();
      this.index = paramLong;
      this.defaultValue = paramT;
      this.errorOnFewer = paramBoolean;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        Object localObject = this.defaultValue;
        if (localObject == null)
        {
          if (this.errorOnFewer) {
            this.actual.onError(new NoSuchElementException());
          } else {
            this.actual.onComplete();
          }
        }
        else {
          complete(localObject);
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.count;
      if (l == this.index)
      {
        this.done = true;
        this.s.cancel();
        complete(paramT);
        return;
      }
      this.count = (l + 1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableElementAt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */