package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCombineLatest<T, R>
  extends Flowable<R>
{
  final Publisher<? extends T>[] array;
  final int bufferSize;
  final Function<? super Object[], ? extends R> combiner;
  final boolean delayErrors;
  final Iterable<? extends Publisher<? extends T>> iterable;
  
  public FlowableCombineLatest(Iterable<? extends Publisher<? extends T>> paramIterable, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
  {
    this.array = null;
    this.iterable = paramIterable;
    this.combiner = paramFunction;
    this.bufferSize = paramInt;
    this.delayErrors = paramBoolean;
  }
  
  public FlowableCombineLatest(Publisher<? extends T>[] paramArrayOfPublisher, Function<? super Object[], ? extends R> paramFunction, int paramInt, boolean paramBoolean)
  {
    this.array = paramArrayOfPublisher;
    this.iterable = null;
    this.combiner = paramFunction;
    this.bufferSize = paramInt;
    this.delayErrors = paramBoolean;
  }
  
  /* Error */
  public void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 33	io/reactivex/internal/operators/flowable/FlowableCombineLatest:array	[Lorg/reactivestreams/Publisher;
    //   4: astore 4
    //   6: aload 4
    //   8: ifnonnull +152 -> 160
    //   11: bipush 8
    //   13: anewarray 52	org/reactivestreams/Publisher
    //   16: astore 4
    //   18: aload_0
    //   19: getfield 35	io/reactivex/internal/operators/flowable/FlowableCombineLatest:iterable	Ljava/lang/Iterable;
    //   22: invokeinterface 58 1 0
    //   27: ldc 60
    //   29: invokestatic 66	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   32: checkcast 68	java/util/Iterator
    //   35: astore 6
    //   37: iconst_0
    //   38: istore_2
    //   39: aload 6
    //   41: invokeinterface 72 1 0
    //   46: istore_3
    //   47: iload_3
    //   48: ifne +6 -> 54
    //   51: goto +113 -> 164
    //   54: aload 6
    //   56: invokeinterface 76 1 0
    //   61: ldc 78
    //   63: invokestatic 66	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   66: checkcast 52	org/reactivestreams/Publisher
    //   69: astore 7
    //   71: aload 4
    //   73: astore 5
    //   75: iload_2
    //   76: aload 4
    //   78: arraylength
    //   79: if_icmpne +23 -> 102
    //   82: iload_2
    //   83: iconst_2
    //   84: ishr
    //   85: iload_2
    //   86: iadd
    //   87: anewarray 52	org/reactivestreams/Publisher
    //   90: astore 5
    //   92: aload 4
    //   94: iconst_0
    //   95: aload 5
    //   97: iconst_0
    //   98: iload_2
    //   99: invokestatic 84	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
    //   102: aload 5
    //   104: iload_2
    //   105: aload 7
    //   107: aastore
    //   108: iinc 2 1
    //   111: aload 5
    //   113: astore 4
    //   115: goto -76 -> 39
    //   118: astore 4
    //   120: aload 4
    //   122: invokestatic 90	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   125: aload 4
    //   127: aload_1
    //   128: invokestatic 96	io/reactivex/internal/subscriptions/EmptySubscription:error	(Ljava/lang/Throwable;Lorg/reactivestreams/Subscriber;)V
    //   131: return
    //   132: astore 4
    //   134: aload 4
    //   136: invokestatic 90	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   139: aload 4
    //   141: aload_1
    //   142: invokestatic 96	io/reactivex/internal/subscriptions/EmptySubscription:error	(Ljava/lang/Throwable;Lorg/reactivestreams/Subscriber;)V
    //   145: return
    //   146: astore 4
    //   148: aload 4
    //   150: invokestatic 90	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   153: aload 4
    //   155: aload_1
    //   156: invokestatic 96	io/reactivex/internal/subscriptions/EmptySubscription:error	(Ljava/lang/Throwable;Lorg/reactivestreams/Subscriber;)V
    //   159: return
    //   160: aload 4
    //   162: arraylength
    //   163: istore_2
    //   164: iload_2
    //   165: ifne +8 -> 173
    //   168: aload_1
    //   169: invokestatic 99	io/reactivex/internal/subscriptions/EmptySubscription:complete	(Lorg/reactivestreams/Subscriber;)V
    //   172: return
    //   173: iload_2
    //   174: iconst_1
    //   175: if_icmpne +27 -> 202
    //   178: new 101	io/reactivex/internal/operators/flowable/FlowableMap
    //   181: dup
    //   182: aload 4
    //   184: iconst_0
    //   185: aaload
    //   186: new 7	io/reactivex/internal/operators/flowable/FlowableCombineLatest$1
    //   189: dup
    //   190: aload_0
    //   191: invokespecial 104	io/reactivex/internal/operators/flowable/FlowableCombineLatest$1:<init>	(Lio/reactivex/internal/operators/flowable/FlowableCombineLatest;)V
    //   194: invokespecial 107	io/reactivex/internal/operators/flowable/FlowableMap:<init>	(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/Function;)V
    //   197: aload_1
    //   198: invokevirtual 110	io/reactivex/internal/operators/flowable/FlowableMap:subscribe	(Lorg/reactivestreams/Subscriber;)V
    //   201: return
    //   202: new 9	io/reactivex/internal/operators/flowable/FlowableCombineLatest$CombineLatestCoordinator
    //   205: dup
    //   206: aload_1
    //   207: aload_0
    //   208: getfield 37	io/reactivex/internal/operators/flowable/FlowableCombineLatest:combiner	Lio/reactivex/functions/Function;
    //   211: iload_2
    //   212: aload_0
    //   213: getfield 39	io/reactivex/internal/operators/flowable/FlowableCombineLatest:bufferSize	I
    //   216: aload_0
    //   217: getfield 41	io/reactivex/internal/operators/flowable/FlowableCombineLatest:delayErrors	Z
    //   220: invokespecial 113	io/reactivex/internal/operators/flowable/FlowableCombineLatest$CombineLatestCoordinator:<init>	(Lorg/reactivestreams/Subscriber;Lio/reactivex/functions/Function;IIZ)V
    //   223: astore 5
    //   225: aload_1
    //   226: aload 5
    //   228: invokeinterface 119 2 0
    //   233: aload 5
    //   235: aload 4
    //   237: iload_2
    //   238: invokevirtual 122	io/reactivex/internal/operators/flowable/FlowableCombineLatest$CombineLatestCoordinator:subscribe	([Lorg/reactivestreams/Publisher;I)V
    //   241: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	242	0	this	FlowableCombineLatest
    //   0	242	1	paramSubscriber	Subscriber<? super R>
    //   38	200	2	i	int
    //   46	2	3	bool	boolean
    //   4	110	4	localObject1	Object
    //   118	8	4	localThrowable1	Throwable
    //   132	8	4	localThrowable2	Throwable
    //   146	90	4	localThrowable3	Throwable
    //   73	161	5	localObject2	Object
    //   35	20	6	localIterator	java.util.Iterator
    //   69	37	7	localPublisher	Publisher
    // Exception table:
    //   from	to	target	type
    //   54	71	118	java/lang/Throwable
    //   39	47	132	java/lang/Throwable
    //   18	37	146	java/lang/Throwable
  }
  
  static final class CombineLatestCoordinator<T, R>
    extends BasicIntQueueSubscription<R>
  {
    private static final long serialVersionUID = -5082275438355852221L;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    final Function<? super Object[], ? extends R> combiner;
    int completedSources;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicReference<Throwable> error;
    final Object[] latest;
    int nonEmptySources;
    boolean outputFused;
    final SpscLinkedArrayQueue<Object> queue;
    final AtomicLong requested;
    final FlowableCombineLatest.CombineLatestInnerSubscriber<T>[] subscribers;
    
    CombineLatestCoordinator(Subscriber<? super R> paramSubscriber, Function<? super Object[], ? extends R> paramFunction, int paramInt1, int paramInt2, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.combiner = paramFunction;
      paramSubscriber = new FlowableCombineLatest.CombineLatestInnerSubscriber[paramInt1];
      for (int i = 0; i < paramInt1; i++) {
        paramSubscriber[i] = new FlowableCombineLatest.CombineLatestInnerSubscriber(this, i, paramInt2);
      }
      this.subscribers = paramSubscriber;
      this.latest = new Object[paramInt1];
      this.queue = new SpscLinkedArrayQueue(paramInt2);
      this.requested = new AtomicLong();
      this.error = new AtomicReference();
      this.delayErrors = paramBoolean;
    }
    
    public void cancel()
    {
      this.cancelled = true;
      cancelAll();
    }
    
    void cancelAll()
    {
      FlowableCombineLatest.CombineLatestInnerSubscriber[] arrayOfCombineLatestInnerSubscriber = this.subscribers;
      int j = arrayOfCombineLatestInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfCombineLatestInnerSubscriber[i].cancel();
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<?> paramSubscriber, SpscLinkedArrayQueue<?> paramSpscLinkedArrayQueue)
    {
      if (this.cancelled)
      {
        cancelAll();
        paramSpscLinkedArrayQueue.clear();
        return true;
      }
      if (paramBoolean1) {
        if (this.delayErrors)
        {
          if (paramBoolean2)
          {
            cancelAll();
            paramSpscLinkedArrayQueue = ExceptionHelper.terminate(this.error);
            if ((paramSpscLinkedArrayQueue != null) && (paramSpscLinkedArrayQueue != ExceptionHelper.TERMINATED)) {
              paramSubscriber.onError(paramSpscLinkedArrayQueue);
            } else {
              paramSubscriber.onComplete();
            }
            return true;
          }
        }
        else
        {
          Throwable localThrowable = ExceptionHelper.terminate(this.error);
          if ((localThrowable != null) && (localThrowable != ExceptionHelper.TERMINATED))
          {
            cancelAll();
            paramSpscLinkedArrayQueue.clear();
            paramSubscriber.onError(localThrowable);
            return true;
          }
          if (paramBoolean2)
          {
            cancelAll();
            paramSubscriber.onComplete();
            return true;
          }
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      if (this.outputFused) {
        drainOutput();
      } else {
        drainAsync();
      }
    }
    
    void drainAsync()
    {
      Subscriber localSubscriber = this.actual;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      int i = 1;
      int j;
      do
      {
        long l2 = this.requested.get();
        long l1 = 0L;
        while (l1 != l2)
        {
          boolean bool2 = this.done;
          Object localObject1 = localSpscLinkedArrayQueue.poll();
          boolean bool1;
          if (localObject1 == null) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if (checkTerminated(bool2, bool1, localSubscriber, localSpscLinkedArrayQueue)) {
            return;
          }
          if (!bool1)
          {
            Object localObject2 = (Object[])localSpscLinkedArrayQueue.poll();
            try
            {
              localObject2 = ObjectHelper.requireNonNull(this.combiner.apply(localObject2), "The combiner returned a null value");
              localSubscriber.onNext(localObject2);
              ((FlowableCombineLatest.CombineLatestInnerSubscriber)localObject1).requestOne();
              l1 += 1L;
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              cancelAll();
              ExceptionHelper.addThrowable(this.error, localThrowable);
              localSubscriber.onError(ExceptionHelper.terminate(this.error));
              return;
            }
          }
        }
        if ((l1 == l2) && (checkTerminated(this.done, localThrowable.isEmpty(), localSubscriber, localThrowable))) {
          return;
        }
        if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
          this.requested.addAndGet(-l1);
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void drainOutput()
    {
      Subscriber localSubscriber = this.actual;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      int i = 1;
      int j;
      do
      {
        if (this.cancelled)
        {
          localSpscLinkedArrayQueue.clear();
          return;
        }
        Throwable localThrowable = (Throwable)this.error.get();
        if (localThrowable != null)
        {
          localSpscLinkedArrayQueue.clear();
          localSubscriber.onError(localThrowable);
          return;
        }
        boolean bool2 = this.done;
        boolean bool1 = localSpscLinkedArrayQueue.isEmpty();
        if (!bool1) {
          localSubscriber.onNext(null);
        }
        if ((bool2) && (bool1))
        {
          localSubscriber.onComplete();
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void innerComplete(int paramInt)
    {
      try
      {
        Object[] arrayOfObject = this.latest;
        if (arrayOfObject[paramInt] != null)
        {
          paramInt = this.completedSources + 1;
          if (paramInt == arrayOfObject.length) {
            this.done = true;
          } else {
            this.completedSources = paramInt;
          }
        }
        else
        {
          this.done = true;
        }
        drain();
        return;
      }
      finally {}
    }
    
    void innerError(int paramInt, Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable))
      {
        if (!this.delayErrors)
        {
          cancelAll();
          this.done = true;
          drain();
        }
        else
        {
          innerComplete(paramInt);
        }
      }
      else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void innerValue(int paramInt, T paramT)
    {
      try
      {
        Object[] arrayOfObject = this.latest;
        int j = this.nonEmptySources;
        int i = j;
        if (arrayOfObject[paramInt] == null)
        {
          i = j + 1;
          this.nonEmptySources = i;
        }
        arrayOfObject[paramInt] = paramT;
        if (arrayOfObject.length == i)
        {
          this.queue.offer(this.subscribers[paramInt], arrayOfObject.clone());
          i = 0;
        }
        else
        {
          i = 1;
        }
        if (i != 0) {
          this.subscribers[paramInt].requestOne();
        } else {
          drain();
        }
        return;
      }
      finally {}
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public R poll()
      throws Exception
    {
      Object localObject1 = this.queue.poll();
      if (localObject1 == null) {
        return null;
      }
      Object localObject2 = (Object[])this.queue.poll();
      localObject2 = this.combiner.apply(localObject2);
      ((FlowableCombineLatest.CombineLatestInnerSubscriber)localObject1).requestOne();
      return (R)localObject2;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      boolean bool = false;
      if ((paramInt & 0x4) != 0) {
        return 0;
      }
      paramInt &= 0x2;
      if (paramInt != 0) {
        bool = true;
      }
      this.outputFused = bool;
      return paramInt;
    }
    
    void subscribe(Publisher<? extends T>[] paramArrayOfPublisher, int paramInt)
    {
      FlowableCombineLatest.CombineLatestInnerSubscriber[] arrayOfCombineLatestInnerSubscriber = this.subscribers;
      int i = 0;
      while (i < paramInt) {
        if ((!this.done) && (!this.cancelled))
        {
          paramArrayOfPublisher[i].subscribe(arrayOfCombineLatestInnerSubscriber[i]);
          i++;
        }
        else {}
      }
    }
  }
  
  static final class CombineLatestInnerSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -8730235182291002949L;
    final int index;
    final int limit;
    final FlowableCombineLatest.CombineLatestCoordinator<T, ?> parent;
    final int prefetch;
    int produced;
    
    CombineLatestInnerSubscriber(FlowableCombineLatest.CombineLatestCoordinator<T, ?> paramCombineLatestCoordinator, int paramInt1, int paramInt2)
    {
      this.parent = paramCombineLatestCoordinator;
      this.index = paramInt1;
      this.prefetch = paramInt2;
      this.limit = (paramInt2 - (paramInt2 >> 2));
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      this.parent.innerComplete(this.index);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(this.index, paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.parent.innerValue(this.index, paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void requestOne()
    {
      int i = this.produced + 1;
      if (i == this.limit)
      {
        this.produced = 0;
        ((Subscription)get()).request(i);
      }
      else
      {
        this.produced = i;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCombineLatest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */