package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableBufferTimed<T, U extends Collection<? super T>>
  extends AbstractFlowableWithUpstream<T, U>
{
  final Callable<U> bufferSupplier;
  final int maxSize;
  final boolean restartTimerOnMaxSize;
  final Scheduler scheduler;
  final long timeskip;
  final long timespan;
  final TimeUnit unit;
  
  public FlowableBufferTimed(Publisher<T> paramPublisher, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler, Callable<U> paramCallable, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.timespan = paramLong1;
    this.timeskip = paramLong2;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.bufferSupplier = paramCallable;
    this.maxSize = paramInt;
    this.restartTimerOnMaxSize = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    if ((this.timespan == this.timeskip) && (this.maxSize == Integer.MAX_VALUE))
    {
      this.source.subscribe(new BufferExactUnboundedSubscriber(new SerializedSubscriber(paramSubscriber), this.bufferSupplier, this.timespan, this.unit, this.scheduler));
      return;
    }
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    if (this.timespan == this.timeskip)
    {
      this.source.subscribe(new BufferExactBoundedSubscriber(new SerializedSubscriber(paramSubscriber), this.bufferSupplier, this.timespan, this.unit, this.maxSize, this.restartTimerOnMaxSize, localWorker));
      return;
    }
    this.source.subscribe(new BufferSkipBoundedSubscriber(new SerializedSubscriber(paramSubscriber), this.bufferSupplier, this.timespan, this.timeskip, this.unit, localWorker));
  }
  
  static final class BufferExactBoundedSubscriber<T, U extends Collection<? super T>>
    extends QueueDrainSubscriber<T, U, U>
    implements Subscription, Runnable, Disposable
  {
    U buffer;
    final Callable<U> bufferSupplier;
    long consumerIndex;
    final int maxSize;
    long producerIndex;
    final boolean restartTimerOnMaxSize;
    Subscription s;
    Disposable timer;
    final long timespan;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    BufferExactBoundedSubscriber(Subscriber<? super U> paramSubscriber, Callable<U> paramCallable, long paramLong, TimeUnit paramTimeUnit, int paramInt, boolean paramBoolean, Scheduler.Worker paramWorker)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong;
      this.unit = paramTimeUnit;
      this.maxSize = paramInt;
      this.restartTimerOnMaxSize = paramBoolean;
      this.w = paramWorker;
    }
    
    public boolean accept(Subscriber<? super U> paramSubscriber, U paramU)
    {
      paramSubscriber.onNext(paramU);
      return true;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        dispose();
      }
    }
    
    public void dispose()
    {
      this.w.dispose();
      try
      {
        this.buffer = null;
        this.s.cancel();
        return;
      }
      finally {}
    }
    
    public boolean isDisposed()
    {
      return this.w.isDisposed();
    }
    
    public void onComplete()
    {
      this.w.dispose();
      try
      {
        Collection localCollection = this.buffer;
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainMaxLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.w.dispose();
      try
      {
        this.buffer = null;
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    /* Error */
    public void onNext(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 87	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:buffer	Ljava/util/Collection;
      //   6: astore 4
      //   8: aload 4
      //   10: ifnonnull +6 -> 16
      //   13: aload_0
      //   14: monitorexit
      //   15: return
      //   16: aload 4
      //   18: aload_1
      //   19: invokeinterface 131 2 0
      //   24: pop
      //   25: aload 4
      //   27: invokeinterface 135 1 0
      //   32: aload_0
      //   33: getfield 54	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:maxSize	I
      //   36: if_icmpge +6 -> 42
      //   39: aload_0
      //   40: monitorexit
      //   41: return
      //   42: aload_0
      //   43: monitorexit
      //   44: aload_0
      //   45: getfield 56	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:restartTimerOnMaxSize	Z
      //   48: ifeq +27 -> 75
      //   51: aload_0
      //   52: aconst_null
      //   53: putfield 87	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:buffer	Ljava/util/Collection;
      //   56: aload_0
      //   57: aload_0
      //   58: getfield 137	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:producerIndex	J
      //   61: lconst_1
      //   62: ladd
      //   63: putfield 137	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:producerIndex	J
      //   66: aload_0
      //   67: getfield 139	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:timer	Lio/reactivex/disposables/Disposable;
      //   70: invokeinterface 140 1 0
      //   75: aload_0
      //   76: aload 4
      //   78: iconst_0
      //   79: aload_0
      //   80: invokevirtual 144	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:fastPathOrderedEmitMax	(Ljava/lang/Object;ZLio/reactivex/disposables/Disposable;)V
      //   83: aload_0
      //   84: getfield 48	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:bufferSupplier	Ljava/util/concurrent/Callable;
      //   87: invokeinterface 150 1 0
      //   92: ldc -104
      //   94: invokestatic 158	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   97: checkcast 65	java/util/Collection
      //   100: astore_1
      //   101: aload_0
      //   102: getfield 56	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:restartTimerOnMaxSize	Z
      //   105: ifeq +55 -> 160
      //   108: aload_0
      //   109: monitorenter
      //   110: aload_0
      //   111: aload_1
      //   112: putfield 87	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:buffer	Ljava/util/Collection;
      //   115: aload_0
      //   116: aload_0
      //   117: getfield 160	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:consumerIndex	J
      //   120: lconst_1
      //   121: ladd
      //   122: putfield 160	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:consumerIndex	J
      //   125: aload_0
      //   126: monitorexit
      //   127: aload_0
      //   128: getfield 58	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:w	Lio/reactivex/Scheduler$Worker;
      //   131: astore_1
      //   132: aload_0
      //   133: getfield 50	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:timespan	J
      //   136: lstore_2
      //   137: aload_0
      //   138: aload_1
      //   139: aload_0
      //   140: lload_2
      //   141: lload_2
      //   142: aload_0
      //   143: getfield 52	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:unit	Ljava/util/concurrent/TimeUnit;
      //   146: invokevirtual 164	io/reactivex/Scheduler$Worker:schedulePeriodically	(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
      //   149: putfield 139	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:timer	Lio/reactivex/disposables/Disposable;
      //   152: goto +17 -> 169
      //   155: astore_1
      //   156: aload_0
      //   157: monitorexit
      //   158: aload_1
      //   159: athrow
      //   160: aload_0
      //   161: monitorenter
      //   162: aload_0
      //   163: aload_1
      //   164: putfield 87	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:buffer	Ljava/util/Collection;
      //   167: aload_0
      //   168: monitorexit
      //   169: return
      //   170: astore_1
      //   171: aload_0
      //   172: monitorexit
      //   173: aload_1
      //   174: athrow
      //   175: astore_1
      //   176: aload_1
      //   177: invokestatic 169	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   180: aload_0
      //   181: invokevirtual 170	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:cancel	()V
      //   184: aload_0
      //   185: getfield 116	io/reactivex/internal/operators/flowable/FlowableBufferTimed$BufferExactBoundedSubscriber:actual	Lorg/reactivestreams/Subscriber;
      //   188: aload_1
      //   189: invokeinterface 126 2 0
      //   194: return
      //   195: astore_1
      //   196: aload_0
      //   197: monitorexit
      //   198: aload_1
      //   199: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	200	0	this	BufferExactBoundedSubscriber
      //   0	200	1	paramT	T
      //   136	6	2	l	long
      //   6	71	4	localCollection	Collection
      // Exception table:
      //   from	to	target	type
      //   110	127	155	finally
      //   156	158	155	finally
      //   162	169	170	finally
      //   171	173	170	finally
      //   83	101	175	java/lang/Throwable
      //   2	8	195	finally
      //   13	15	195	finally
      //   16	41	195	finally
      //   42	44	195	finally
      //   196	198	195	finally
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (!SubscriptionHelper.validate(this.s, paramSubscription)) {
        return;
      }
      this.s = paramSubscription;
      try
      {
        Object localObject = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
        this.buffer = ((Collection)localObject);
        this.actual.onSubscribe(this);
        localObject = this.w;
        long l = this.timespan;
        this.timer = ((Scheduler.Worker)localObject).schedulePeriodically(this, l, l, this.unit);
        paramSubscription.request(Long.MAX_VALUE);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.w.dispose();
        paramSubscription.cancel();
        EmptySubscription.error(localThrowable, this.actual);
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    public void run()
    {
      try
      {
        Collection localCollection2 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
        try
        {
          Collection localCollection1 = this.buffer;
          if ((localCollection1 != null) && (this.producerIndex == this.consumerIndex))
          {
            this.buffer = localCollection2;
            fastPathOrderedEmitMax(localCollection1, false, this);
            return;
          }
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        cancel();
        this.actual.onError(localThrowable);
      }
    }
  }
  
  static final class BufferExactUnboundedSubscriber<T, U extends Collection<? super T>>
    extends QueueDrainSubscriber<T, U, U>
    implements Subscription, Runnable, Disposable
  {
    U buffer;
    final Callable<U> bufferSupplier;
    Subscription s;
    final Scheduler scheduler;
    final AtomicReference<Disposable> timer = new AtomicReference();
    final long timespan;
    final TimeUnit unit;
    
    BufferExactUnboundedSubscriber(Subscriber<? super U> paramSubscriber, Callable<U> paramCallable, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
    }
    
    public boolean accept(Subscriber<? super U> paramSubscriber, U paramU)
    {
      this.actual.onNext(paramU);
      return true;
    }
    
    public void cancel()
    {
      DisposableHelper.dispose(this.timer);
      this.s.cancel();
    }
    
    public void dispose()
    {
      cancel();
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.timer.get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      DisposableHelper.dispose(this.timer);
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainMaxLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      DisposableHelper.dispose(this.timer);
      try
      {
        this.buffer = null;
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection != null) {
          localCollection.add(paramT);
        }
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        try
        {
          Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
          this.buffer = localCollection;
          this.actual.onSubscribe(this);
          if (!this.cancelled)
          {
            paramSubscription.request(Long.MAX_VALUE);
            paramSubscription = this.scheduler;
            long l = this.timespan;
            paramSubscription = paramSubscription.schedulePeriodicallyDirect(this, l, l, this.unit);
            if (!this.timer.compareAndSet(null, paramSubscription)) {
              paramSubscription.dispose();
            }
          }
        }
        catch (Throwable paramSubscription)
        {
          Exceptions.throwIfFatal(paramSubscription);
          cancel();
          EmptySubscription.error(paramSubscription, this.actual);
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    public void run()
    {
      try
      {
        Collection localCollection1 = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
        try
        {
          Collection localCollection2 = this.buffer;
          if (localCollection2 != null) {
            this.buffer = localCollection1;
          }
          if (localCollection2 == null)
          {
            DisposableHelper.dispose(this.timer);
            return;
          }
          fastPathEmitMax(localCollection2, false, this);
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        cancel();
        this.actual.onError(localThrowable);
      }
    }
  }
  
  static final class BufferSkipBoundedSubscriber<T, U extends Collection<? super T>>
    extends QueueDrainSubscriber<T, U, U>
    implements Subscription, Runnable
  {
    final Callable<U> bufferSupplier;
    final List<U> buffers;
    Subscription s;
    final long timeskip;
    final long timespan;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    BufferSkipBoundedSubscriber(Subscriber<? super U> paramSubscriber, Callable<U> paramCallable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.timespan = paramLong1;
      this.timeskip = paramLong2;
      this.unit = paramTimeUnit;
      this.w = paramWorker;
      this.buffers = new LinkedList();
    }
    
    public boolean accept(Subscriber<? super U> paramSubscriber, U paramU)
    {
      paramSubscriber.onNext(paramU);
      return true;
    }
    
    public void cancel()
    {
      this.w.dispose();
      clear();
      this.s.cancel();
    }
    
    void clear()
    {
      try
      {
        this.buffers.clear();
        return;
      }
      finally {}
    }
    
    public void onComplete()
    {
      try
      {
        Object localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>(this.buffers);
        this.buffers.clear();
        Iterator localIterator = ((List)localObject1).iterator();
        while (localIterator.hasNext())
        {
          localObject1 = (Collection)localIterator.next();
          this.queue.offer(localObject1);
        }
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainMaxLoop(this.queue, this.actual, false, this.w, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.done = true;
      this.w.dispose();
      clear();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Iterator localIterator = this.buffers.iterator();
        while (localIterator.hasNext()) {
          ((Collection)localIterator.next()).add(paramT);
        }
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (!SubscriptionHelper.validate(this.s, paramSubscription)) {
        return;
      }
      this.s = paramSubscription;
      try
      {
        final Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
        this.buffers.add(localCollection);
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
        paramSubscription = this.w;
        long l = this.timeskip;
        paramSubscription.schedulePeriodically(this, l, l, this.unit);
        this.w.schedule(new Runnable()
        {
          public void run()
          {
            synchronized (FlowableBufferTimed.BufferSkipBoundedSubscriber.this)
            {
              FlowableBufferTimed.BufferSkipBoundedSubscriber.this.buffers.remove(localCollection);
              FlowableBufferTimed.BufferSkipBoundedSubscriber localBufferSkipBoundedSubscriber1 = FlowableBufferTimed.BufferSkipBoundedSubscriber.this;
              localBufferSkipBoundedSubscriber1.fastPathOrderedEmitMax(localCollection, false, localBufferSkipBoundedSubscriber1.w);
              return;
            }
          }
        }, this.timespan, this.unit);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.w.dispose();
        paramSubscription.cancel();
        EmptySubscription.error(localThrowable, this.actual);
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    public void run()
    {
      if (this.cancelled) {
        return;
      }
      try
      {
        final Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The supplied buffer is null");
        try
        {
          if (this.cancelled) {
            return;
          }
          this.buffers.add(localCollection);
          this.w.schedule(new Runnable()
          {
            public void run()
            {
              synchronized (FlowableBufferTimed.BufferSkipBoundedSubscriber.this)
              {
                FlowableBufferTimed.BufferSkipBoundedSubscriber.this.buffers.remove(localCollection);
                ??? = FlowableBufferTimed.BufferSkipBoundedSubscriber.this;
                ???.fastPathOrderedEmitMax(localCollection, false, ???.w);
                return;
              }
            }
          }, this.timespan, this.unit);
          return;
        }
        finally {}
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        cancel();
        this.actual.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableBufferTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */