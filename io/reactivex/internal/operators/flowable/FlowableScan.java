package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableScan<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final BiFunction<T, T, T> accumulator;
  
  public FlowableScan(Publisher<T> paramPublisher, BiFunction<T, T, T> paramBiFunction)
  {
    super(paramPublisher);
    this.accumulator = paramBiFunction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new ScanSubscriber(paramSubscriber, this.accumulator));
  }
  
  static final class ScanSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final BiFunction<T, T, T> accumulator;
    final Subscriber<? super T> actual;
    boolean done;
    Subscription s;
    T value;
    
    ScanSubscriber(Subscriber<? super T> paramSubscriber, BiFunction<T, T, T> paramBiFunction)
    {
      this.actual = paramSubscriber;
      this.accumulator = paramBiFunction;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      Object localObject = this.value;
      if (localObject == null)
      {
        this.value = paramT;
        localSubscriber.onNext(paramT);
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.accumulator.apply(localObject, paramT), "The value returned by the accumulator is null");
        this.value = paramT;
        localSubscriber.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableScan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */