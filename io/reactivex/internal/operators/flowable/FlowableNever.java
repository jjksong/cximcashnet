package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.subscriptions.EmptySubscription;
import org.reactivestreams.Subscriber;

public final class FlowableNever
  extends Flowable<Object>
{
  public static final Flowable<Object> INSTANCE = new FlowableNever();
  
  public void subscribeActual(Subscriber<? super Object> paramSubscriber)
  {
    paramSubscriber.onSubscribe(EmptySubscription.INSTANCE);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableNever.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */