package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableConcatMap<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final ErrorMode errorMode;
  final Function<? super T, ? extends Publisher<? extends R>> mapper;
  final int prefetch;
  
  public FlowableConcatMap(Publisher<T> paramPublisher, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, ErrorMode paramErrorMode)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.prefetch = paramInt;
    this.errorMode = paramErrorMode;
  }
  
  public static <T, R> Subscriber<T> subscribe(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, ErrorMode paramErrorMode)
  {
    switch (paramErrorMode)
    {
    default: 
      return new ConcatMapImmediate(paramSubscriber, paramFunction, paramInt);
    case ???: 
      return new ConcatMapDelayed(paramSubscriber, paramFunction, paramInt, true);
    }
    return new ConcatMapDelayed(paramSubscriber, paramFunction, paramInt, false);
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    if (FlowableScalarXMap.tryScalarXMapSubscribe(this.source, paramSubscriber, this.mapper)) {
      return;
    }
    this.source.subscribe(subscribe(paramSubscriber, this.mapper, this.prefetch, this.errorMode));
  }
  
  static abstract class BaseConcatMapSubscriber<T, R>
    extends AtomicInteger
    implements Subscriber<T>, FlowableConcatMap.ConcatMapSupport<R>, Subscription
  {
    private static final long serialVersionUID = -3511336836796789179L;
    volatile boolean active;
    volatile boolean cancelled;
    int consumed;
    volatile boolean done;
    final AtomicThrowable errors;
    final FlowableConcatMap.ConcatMapInner<R> inner;
    final int limit;
    final Function<? super T, ? extends Publisher<? extends R>> mapper;
    final int prefetch;
    SimpleQueue<T> queue;
    Subscription s;
    int sourceMode;
    
    BaseConcatMapSubscriber(Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt)
    {
      this.mapper = paramFunction;
      this.prefetch = paramInt;
      this.limit = (paramInt - (paramInt >> 2));
      this.inner = new FlowableConcatMap.ConcatMapInner(this);
      this.errors = new AtomicThrowable();
    }
    
    abstract void drain();
    
    public final void innerComplete()
    {
      this.active = false;
      drain();
    }
    
    public final void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public final void onNext(T paramT)
    {
      if ((this.sourceMode != 2) && (!this.queue.offer(paramT)))
      {
        this.s.cancel();
        onError(new IllegalStateException("Queue full?!"));
        return;
      }
      drain();
    }
    
    public final void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            subscribeActual();
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            subscribeActual();
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        subscribeActual();
        paramSubscription.request(this.prefetch);
      }
    }
    
    abstract void subscribeActual();
  }
  
  static final class ConcatMapDelayed<T, R>
    extends FlowableConcatMap.BaseConcatMapSubscriber<T, R>
  {
    private static final long serialVersionUID = -2945777694260521066L;
    final Subscriber<? super R> actual;
    final boolean veryEnd;
    
    ConcatMapDelayed(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
    {
      super(paramInt);
      this.actual = paramSubscriber;
      this.veryEnd = paramBoolean;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.inner.cancel();
        this.s.cancel();
      }
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        for (;;)
        {
          if (this.cancelled) {
            return;
          }
          if (!this.active)
          {
            boolean bool = this.done;
            if ((bool) && (!this.veryEnd) && ((Throwable)this.errors.get() != null))
            {
              this.actual.onError(this.errors.terminate());
              return;
            }
            try
            {
              Object localObject = this.queue.poll();
              int i;
              if (localObject == null) {
                i = 1;
              } else {
                i = 0;
              }
              if ((bool) && (i != 0))
              {
                localObject = this.errors.terminate();
                if (localObject != null) {
                  this.actual.onError((Throwable)localObject);
                } else {
                  this.actual.onComplete();
                }
                return;
              }
              if (i == 0) {
                try
                {
                  localObject = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(localObject), "The mapper returned a null Publisher");
                  if (this.sourceMode != 1)
                  {
                    i = this.consumed + 1;
                    if (i == this.limit)
                    {
                      this.consumed = 0;
                      this.s.request(i);
                    }
                    else
                    {
                      this.consumed = i;
                    }
                  }
                  if ((localObject instanceof Callable))
                  {
                    localObject = (Callable)localObject;
                    try
                    {
                      localObject = ((Callable)localObject).call();
                      if (localObject == null) {
                        continue;
                      }
                      if (this.inner.isUnbounded())
                      {
                        this.actual.onNext(localObject);
                        continue;
                      }
                      this.active = true;
                      this.inner.setSubscription(new FlowableConcatMap.WeakScalarSubscription(localObject, this.inner));
                    }
                    catch (Throwable localThrowable1)
                    {
                      Exceptions.throwIfFatal(localThrowable1);
                      this.s.cancel();
                      this.errors.addThrowable(localThrowable1);
                      this.actual.onError(this.errors.terminate());
                      return;
                    }
                  }
                  this.active = true;
                  localThrowable1.subscribe(this.inner);
                }
                catch (Throwable localThrowable2)
                {
                  Exceptions.throwIfFatal(localThrowable2);
                  this.s.cancel();
                  this.errors.addThrowable(localThrowable2);
                  this.actual.onError(this.errors.terminate());
                  return;
                }
              } else if (decrementAndGet() != 0) {}
            }
            catch (Throwable localThrowable3)
            {
              Exceptions.throwIfFatal(localThrowable3);
              this.s.cancel();
              this.errors.addThrowable(localThrowable3);
              this.actual.onError(this.errors.terminate());
              return;
            }
          }
        }
      }
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        if (!this.veryEnd)
        {
          this.s.cancel();
          this.done = true;
        }
        this.active = false;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerNext(R paramR)
    {
      this.actual.onNext(paramR);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void request(long paramLong)
    {
      this.inner.request(paramLong);
    }
    
    void subscribeActual()
    {
      this.actual.onSubscribe(this);
    }
  }
  
  static final class ConcatMapImmediate<T, R>
    extends FlowableConcatMap.BaseConcatMapSubscriber<T, R>
  {
    private static final long serialVersionUID = 7898995095634264146L;
    final Subscriber<? super R> actual;
    final AtomicInteger wip;
    
    ConcatMapImmediate(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt)
    {
      super(paramInt);
      this.actual = paramSubscriber;
      this.wip = new AtomicInteger();
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.inner.cancel();
        this.s.cancel();
      }
    }
    
    void drain()
    {
      if (this.wip.getAndIncrement() == 0) {
        for (;;)
        {
          if (this.cancelled) {
            return;
          }
          if (!this.active)
          {
            boolean bool = this.done;
            try
            {
              Object localObject = this.queue.poll();
              int i;
              if (localObject == null) {
                i = 1;
              } else {
                i = 0;
              }
              if ((bool) && (i != 0))
              {
                this.actual.onComplete();
                return;
              }
              if (i == 0) {
                try
                {
                  localObject = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(localObject), "The mapper returned a null Publisher");
                  if (this.sourceMode != 1)
                  {
                    i = this.consumed + 1;
                    if (i == this.limit)
                    {
                      this.consumed = 0;
                      this.s.request(i);
                    }
                    else
                    {
                      this.consumed = i;
                    }
                  }
                  if ((localObject instanceof Callable))
                  {
                    localObject = (Callable)localObject;
                    try
                    {
                      localObject = ((Callable)localObject).call();
                      if (localObject == null) {
                        continue;
                      }
                      if (this.inner.isUnbounded())
                      {
                        if ((get() != 0) || (!compareAndSet(0, 1))) {
                          continue;
                        }
                        this.actual.onNext(localObject);
                        if (compareAndSet(1, 0)) {
                          continue;
                        }
                        this.actual.onError(this.errors.terminate());
                        return;
                      }
                      this.active = true;
                      this.inner.setSubscription(new FlowableConcatMap.WeakScalarSubscription(localObject, this.inner));
                    }
                    catch (Throwable localThrowable1)
                    {
                      Exceptions.throwIfFatal(localThrowable1);
                      this.s.cancel();
                      this.errors.addThrowable(localThrowable1);
                      this.actual.onError(this.errors.terminate());
                      return;
                    }
                  }
                  this.active = true;
                  localThrowable1.subscribe(this.inner);
                }
                catch (Throwable localThrowable2)
                {
                  Exceptions.throwIfFatal(localThrowable2);
                  this.s.cancel();
                  this.errors.addThrowable(localThrowable2);
                  this.actual.onError(this.errors.terminate());
                  return;
                }
              } else if (this.wip.decrementAndGet() != 0) {}
            }
            catch (Throwable localThrowable3)
            {
              Exceptions.throwIfFatal(localThrowable3);
              this.s.cancel();
              this.errors.addThrowable(localThrowable3);
              this.actual.onError(this.errors.terminate());
              return;
            }
          }
        }
      }
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.actual.onError(this.errors.terminate());
        }
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerNext(R paramR)
    {
      if ((get() == 0) && (compareAndSet(0, 1)))
      {
        this.actual.onNext(paramR);
        if (compareAndSet(1, 0)) {
          return;
        }
        this.actual.onError(this.errors.terminate());
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        this.inner.cancel();
        if (getAndIncrement() == 0) {
          this.actual.onError(this.errors.terminate());
        }
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void request(long paramLong)
    {
      this.inner.request(paramLong);
    }
    
    void subscribeActual()
    {
      this.actual.onSubscribe(this);
    }
  }
  
  static final class ConcatMapInner<R>
    extends SubscriptionArbiter
    implements Subscriber<R>
  {
    private static final long serialVersionUID = 897683679971470653L;
    final FlowableConcatMap.ConcatMapSupport<R> parent;
    long produced;
    
    ConcatMapInner(FlowableConcatMap.ConcatMapSupport<R> paramConcatMapSupport)
    {
      this.parent = paramConcatMapSupport;
    }
    
    public void onComplete()
    {
      long l = this.produced;
      if (l != 0L)
      {
        this.produced = 0L;
        produced(l);
      }
      this.parent.innerComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      long l = this.produced;
      if (l != 0L)
      {
        this.produced = 0L;
        produced(l);
      }
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(R paramR)
    {
      this.produced += 1L;
      this.parent.innerNext(paramR);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      setSubscription(paramSubscription);
    }
  }
  
  static abstract interface ConcatMapSupport<T>
  {
    public abstract void innerComplete();
    
    public abstract void innerError(Throwable paramThrowable);
    
    public abstract void innerNext(T paramT);
  }
  
  static final class WeakScalarSubscription<T>
    implements Subscription
  {
    final Subscriber<? super T> actual;
    boolean once;
    final T value;
    
    WeakScalarSubscription(T paramT, Subscriber<? super T> paramSubscriber)
    {
      this.value = paramT;
      this.actual = paramSubscriber;
    }
    
    public void cancel() {}
    
    public void request(long paramLong)
    {
      if ((paramLong > 0L) && (!this.once))
      {
        this.once = true;
        Subscriber localSubscriber = this.actual;
        localSubscriber.onNext(this.value);
        localSubscriber.onComplete();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableConcatMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */