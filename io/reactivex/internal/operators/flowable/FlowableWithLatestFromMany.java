package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableWithLatestFromMany<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final Function<? super Object[], R> combiner;
  final Publisher<?>[] otherArray;
  final Iterable<? extends Publisher<?>> otherIterable;
  
  public FlowableWithLatestFromMany(Publisher<T> paramPublisher, Iterable<? extends Publisher<?>> paramIterable, Function<? super Object[], R> paramFunction)
  {
    super(paramPublisher);
    this.otherArray = null;
    this.otherIterable = paramIterable;
    this.combiner = paramFunction;
  }
  
  public FlowableWithLatestFromMany(Publisher<T> paramPublisher, Publisher<?>[] paramArrayOfPublisher, Function<? super Object[], R> paramFunction)
  {
    super(paramPublisher);
    this.otherArray = paramArrayOfPublisher;
    this.otherIterable = null;
    this.combiner = paramFunction;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    Object localObject2 = this.otherArray;
    int j;
    if (localObject2 == null)
    {
      Object localObject1 = new Publisher[8];
      try
      {
        Iterator localIterator = this.otherIterable.iterator();
        int i = 0;
        for (;;)
        {
          localObject2 = localObject1;
          j = i;
          if (!localIterator.hasNext()) {
            break;
          }
          Publisher localPublisher = (Publisher)localIterator.next();
          localObject2 = localObject1;
          if (i == localObject1.length) {
            localObject2 = (Publisher[])Arrays.copyOf((Object[])localObject1, (i >> 1) + i);
          }
          localObject2[i] = localPublisher;
          i++;
          localObject1 = localObject2;
        }
        j = localObject2.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptySubscription.error(localThrowable, paramSubscriber);
        return;
      }
    }
    if (j == 0)
    {
      new FlowableMap(this.source, new Function()
      {
        public R apply(T paramAnonymousT)
          throws Exception
        {
          return (R)FlowableWithLatestFromMany.this.combiner.apply(new Object[] { paramAnonymousT });
        }
      }).subscribeActual(paramSubscriber);
      return;
    }
    WithLatestFromSubscriber localWithLatestFromSubscriber = new WithLatestFromSubscriber(paramSubscriber, this.combiner, j);
    paramSubscriber.onSubscribe(localWithLatestFromSubscriber);
    localWithLatestFromSubscriber.subscribe((Publisher[])localObject2, j);
    this.source.subscribe(localWithLatestFromSubscriber);
  }
  
  static final class WithLatestFromSubscriber<T, R>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 1577321883966341961L;
    final Subscriber<? super R> actual;
    final Function<? super Object[], R> combiner;
    volatile boolean done;
    final AtomicThrowable error;
    final AtomicLong requested;
    final AtomicReference<Subscription> s;
    final FlowableWithLatestFromMany.WithLatestInnerSubscriber[] subscribers;
    final AtomicReferenceArray<Object> values;
    
    WithLatestFromSubscriber(Subscriber<? super R> paramSubscriber, Function<? super Object[], R> paramFunction, int paramInt)
    {
      this.actual = paramSubscriber;
      this.combiner = paramFunction;
      paramSubscriber = new FlowableWithLatestFromMany.WithLatestInnerSubscriber[paramInt];
      for (int i = 0; i < paramInt; i++) {
        paramSubscriber[i] = new FlowableWithLatestFromMany.WithLatestInnerSubscriber(this, i);
      }
      this.subscribers = paramSubscriber;
      this.values = new AtomicReferenceArray(paramInt);
      this.s = new AtomicReference();
      this.requested = new AtomicLong();
      this.error = new AtomicThrowable();
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this.s);
      FlowableWithLatestFromMany.WithLatestInnerSubscriber[] arrayOfWithLatestInnerSubscriber = this.subscribers;
      int j = arrayOfWithLatestInnerSubscriber.length;
      for (int i = 0; i < j; i++) {
        arrayOfWithLatestInnerSubscriber[i].dispose();
      }
    }
    
    void cancelAllBut(int paramInt)
    {
      FlowableWithLatestFromMany.WithLatestInnerSubscriber[] arrayOfWithLatestInnerSubscriber = this.subscribers;
      for (int i = 0; i < arrayOfWithLatestInnerSubscriber.length; i++) {
        if (i != paramInt) {
          arrayOfWithLatestInnerSubscriber[i].dispose();
        }
      }
    }
    
    void innerComplete(int paramInt, boolean paramBoolean)
    {
      if (!paramBoolean)
      {
        this.done = true;
        cancelAllBut(paramInt);
        HalfSerializer.onComplete(this.actual, this, this.error);
      }
    }
    
    void innerError(int paramInt, Throwable paramThrowable)
    {
      this.done = true;
      SubscriptionHelper.cancel(this.s);
      cancelAllBut(paramInt);
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    void innerNext(int paramInt, Object paramObject)
    {
      this.values.set(paramInt, paramObject);
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        cancelAllBut(-1);
        HalfSerializer.onComplete(this.actual, this, this.error);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      cancelAllBut(-1);
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      AtomicReferenceArray localAtomicReferenceArray = this.values;
      int j = localAtomicReferenceArray.length();
      Object[] arrayOfObject = new Object[j + 1];
      int i = 0;
      arrayOfObject[0] = paramT;
      while (i < j)
      {
        paramT = localAtomicReferenceArray.get(i);
        if (paramT == null)
        {
          ((Subscription)this.s.get()).request(1L);
          return;
        }
        i++;
        arrayOfObject[i] = paramT;
      }
      try
      {
        paramT = ObjectHelper.requireNonNull(this.combiner.apply(arrayOfObject), "combiner returned a null value");
        HalfSerializer.onNext(this.actual, paramT, this, this.error);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      SubscriptionHelper.deferredSetOnce(this.s, this.requested, paramSubscription);
    }
    
    public void request(long paramLong)
    {
      SubscriptionHelper.deferredRequest(this.s, this.requested, paramLong);
    }
    
    void subscribe(Publisher<?>[] paramArrayOfPublisher, int paramInt)
    {
      FlowableWithLatestFromMany.WithLatestInnerSubscriber[] arrayOfWithLatestInnerSubscriber = this.subscribers;
      AtomicReference localAtomicReference = this.s;
      int i = 0;
      while (i < paramInt) {
        if ((!SubscriptionHelper.isCancelled((Subscription)localAtomicReference.get())) && (!this.done))
        {
          paramArrayOfPublisher[i].subscribe(arrayOfWithLatestInnerSubscriber[i]);
          i++;
        }
        else {}
      }
    }
  }
  
  static final class WithLatestInnerSubscriber
    extends AtomicReference<Subscription>
    implements Subscriber<Object>, Disposable
  {
    private static final long serialVersionUID = 3256684027868224024L;
    boolean hasValue;
    final int index;
    final FlowableWithLatestFromMany.WithLatestFromSubscriber<?, ?> parent;
    
    WithLatestInnerSubscriber(FlowableWithLatestFromMany.WithLatestFromSubscriber<?, ?> paramWithLatestFromSubscriber, int paramInt)
    {
      this.parent = paramWithLatestFromSubscriber;
      this.index = paramInt;
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)get());
    }
    
    public void onComplete()
    {
      this.parent.innerComplete(this.index, this.hasValue);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(this.index, paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (!this.hasValue) {
        this.hasValue = true;
      }
      this.parent.innerNext(this.index, paramObject);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableWithLatestFromMany.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */