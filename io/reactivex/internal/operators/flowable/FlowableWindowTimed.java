package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.processors.UnicastProcessor;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableWindowTimed<T>
  extends AbstractFlowableWithUpstream<T, Flowable<T>>
{
  final int bufferSize;
  final long maxSize;
  final boolean restartTimerOnMaxSize;
  final Scheduler scheduler;
  final long timeskip;
  final long timespan;
  final TimeUnit unit;
  
  public FlowableWindowTimed(Publisher<T> paramPublisher, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler, long paramLong3, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.timespan = paramLong1;
    this.timeskip = paramLong2;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.maxSize = paramLong3;
    this.bufferSize = paramInt;
    this.restartTimerOnMaxSize = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super Flowable<T>> paramSubscriber)
  {
    paramSubscriber = new SerializedSubscriber(paramSubscriber);
    if (this.timespan == this.timeskip)
    {
      if (this.maxSize == Long.MAX_VALUE)
      {
        this.source.subscribe(new WindowExactUnboundedSubscriber(paramSubscriber, this.timespan, this.unit, this.scheduler, this.bufferSize));
        return;
      }
      this.source.subscribe(new WindowExactBoundedSubscriber(paramSubscriber, this.timespan, this.unit, this.scheduler, this.bufferSize, this.maxSize, this.restartTimerOnMaxSize));
      return;
    }
    this.source.subscribe(new WindowSkipSubscriber(paramSubscriber, this.timespan, this.timeskip, this.unit, this.scheduler.createWorker(), this.bufferSize));
  }
  
  static final class WindowExactBoundedSubscriber<T>
    extends QueueDrainSubscriber<T, Object, Flowable<T>>
    implements Subscription
  {
    final int bufferSize;
    long count;
    final long maxSize;
    long producerIndex;
    final boolean restartTimerOnMaxSize;
    Subscription s;
    final Scheduler scheduler;
    volatile boolean terminated;
    final SequentialDisposable timer = new SequentialDisposable();
    final long timespan;
    final TimeUnit unit;
    UnicastProcessor<T> window;
    Scheduler.Worker worker;
    
    WindowExactBoundedSubscriber(Subscriber<? super Flowable<T>> paramSubscriber, long paramLong1, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt, long paramLong2, boolean paramBoolean)
    {
      super(new MpscLinkedQueue());
      this.timespan = paramLong1;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
      this.bufferSize = paramInt;
      this.maxSize = paramLong2;
      this.restartTimerOnMaxSize = paramBoolean;
    }
    
    public void cancel()
    {
      this.cancelled = true;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.timer);
    }
    
    void drainLoop()
    {
      Object localObject1 = this.queue;
      Subscriber localSubscriber = this.actual;
      UnicastProcessor localUnicastProcessor = this.window;
      int i = 1;
      for (;;)
      {
        if (this.terminated)
        {
          this.s.cancel();
          ((SimplePlainQueue)localObject1).clear();
          dispose();
          return;
        }
        boolean bool2 = this.done;
        Object localObject2 = ((SimplePlainQueue)localObject1).poll();
        int j;
        if (localObject2 == null) {
          j = 1;
        } else {
          j = 0;
        }
        boolean bool1 = localObject2 instanceof ConsumerIndexHolder;
        if ((bool2) && ((j != 0) || (bool1)))
        {
          this.window = null;
          ((SimplePlainQueue)localObject1).clear();
          dispose();
          localObject1 = this.error;
          if (localObject1 != null) {
            localUnicastProcessor.onError((Throwable)localObject1);
          } else {
            localUnicastProcessor.onComplete();
          }
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          long l;
          if (bool1)
          {
            localObject2 = (ConsumerIndexHolder)localObject2;
            if (this.producerIndex == ((ConsumerIndexHolder)localObject2).index)
            {
              localUnicastProcessor = UnicastProcessor.create(this.bufferSize);
              this.window = localUnicastProcessor;
              l = requested();
              if (l != 0L)
              {
                localSubscriber.onNext(localUnicastProcessor);
                if (l != Long.MAX_VALUE) {
                  produced(1L);
                }
                continue;
              }
              this.window = null;
              this.queue.clear();
              this.s.cancel();
              dispose();
              localSubscriber.onError(new MissingBackpressureException("Could not deliver first window due to lack of requests."));
            }
          }
          else
          {
            localUnicastProcessor.onNext(NotificationLite.getValue(localObject2));
            l = this.count + 1L;
            if (l >= this.maxSize)
            {
              this.producerIndex += 1L;
              this.count = 0L;
              localUnicastProcessor.onComplete();
              l = requested();
              if (l != 0L)
              {
                localUnicastProcessor = UnicastProcessor.create(this.bufferSize);
                this.window = localUnicastProcessor;
                this.actual.onNext(localUnicastProcessor);
                if (l != Long.MAX_VALUE) {
                  produced(1L);
                }
                if (this.restartTimerOnMaxSize)
                {
                  localObject2 = (Disposable)this.timer.get();
                  ((Disposable)localObject2).dispose();
                  Scheduler.Worker localWorker = this.worker;
                  Object localObject3 = new ConsumerIndexHolder(this.producerIndex, this);
                  l = this.timespan;
                  localObject3 = localWorker.schedulePeriodically((Runnable)localObject3, l, l, this.unit);
                  if (!this.timer.compareAndSet(localObject2, localObject3)) {
                    ((Disposable)localObject3).dispose();
                  }
                }
              }
              else
              {
                this.window = null;
                this.s.cancel();
                dispose();
                this.actual.onError(new MissingBackpressureException("Could not deliver window due to lack of requests"));
              }
            }
            else
            {
              this.count = l;
            }
          }
        }
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.terminated) {
        return;
      }
      if (fastEnter())
      {
        Object localObject = this.window;
        ((UnicastProcessor)localObject).onNext(paramT);
        long l = this.count + 1L;
        if (l >= this.maxSize)
        {
          this.producerIndex += 1L;
          this.count = 0L;
          ((UnicastProcessor)localObject).onComplete();
          l = requested();
          if (l != 0L)
          {
            paramT = UnicastProcessor.create(this.bufferSize);
            this.window = paramT;
            this.actual.onNext(paramT);
            if (l != Long.MAX_VALUE) {
              produced(1L);
            }
            if (this.restartTimerOnMaxSize)
            {
              paramT = (Disposable)this.timer.get();
              paramT.dispose();
              localObject = this.worker;
              ConsumerIndexHolder localConsumerIndexHolder = new ConsumerIndexHolder(this.producerIndex, this);
              l = this.timespan;
              localObject = ((Scheduler.Worker)localObject).schedulePeriodically(localConsumerIndexHolder, l, l, this.unit);
              if (!this.timer.compareAndSet(paramT, localObject)) {
                ((Disposable)localObject).dispose();
              }
            }
          }
          else
          {
            this.window = null;
            this.s.cancel();
            dispose();
            this.actual.onError(new MissingBackpressureException("Could not deliver window due to lack of requests"));
          }
        }
        else
        {
          this.count = l;
        }
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        Object localObject1 = this.actual;
        ((Subscriber)localObject1).onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        Object localObject2 = UnicastProcessor.create(this.bufferSize);
        this.window = ((UnicastProcessor)localObject2);
        long l = requested();
        if (l != 0L)
        {
          ((Subscriber)localObject1).onNext(localObject2);
          if (l != Long.MAX_VALUE) {
            produced(1L);
          }
          localObject2 = new ConsumerIndexHolder(this.producerIndex, this);
          if (this.restartTimerOnMaxSize)
          {
            localObject1 = this.scheduler.createWorker();
            this.worker = ((Scheduler.Worker)localObject1);
            l = this.timespan;
            ((Scheduler.Worker)localObject1).schedulePeriodically((Runnable)localObject2, l, l, this.unit);
          }
          else
          {
            localObject1 = this.scheduler;
            l = this.timespan;
            localObject1 = ((Scheduler)localObject1).schedulePeriodicallyDirect((Runnable)localObject2, l, l, this.unit);
          }
          if (this.timer.replace((Disposable)localObject1)) {
            paramSubscription.request(Long.MAX_VALUE);
          }
        }
        else
        {
          this.cancelled = true;
          paramSubscription.cancel();
          ((Subscriber)localObject1).onError(new MissingBackpressureException("Could not deliver initial window due to lack of requests."));
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    static final class ConsumerIndexHolder
      implements Runnable
    {
      final long index;
      final FlowableWindowTimed.WindowExactBoundedSubscriber<?> parent;
      
      ConsumerIndexHolder(long paramLong, FlowableWindowTimed.WindowExactBoundedSubscriber<?> paramWindowExactBoundedSubscriber)
      {
        this.index = paramLong;
        this.parent = paramWindowExactBoundedSubscriber;
      }
      
      public void run()
      {
        FlowableWindowTimed.WindowExactBoundedSubscriber localWindowExactBoundedSubscriber = this.parent;
        if (!localWindowExactBoundedSubscriber.cancelled)
        {
          localWindowExactBoundedSubscriber.queue.offer(this);
        }
        else
        {
          localWindowExactBoundedSubscriber.terminated = true;
          localWindowExactBoundedSubscriber.dispose();
        }
        if (localWindowExactBoundedSubscriber.enter()) {
          localWindowExactBoundedSubscriber.drainLoop();
        }
      }
    }
  }
  
  static final class WindowExactUnboundedSubscriber<T>
    extends QueueDrainSubscriber<T, Object, Flowable<T>>
    implements Subscriber<T>, Subscription, Runnable
  {
    static final Object NEXT = new Object();
    final int bufferSize;
    Subscription s;
    final Scheduler scheduler;
    volatile boolean terminated;
    final SequentialDisposable timer = new SequentialDisposable();
    final long timespan;
    final TimeUnit unit;
    UnicastProcessor<T> window;
    
    WindowExactUnboundedSubscriber(Subscriber<? super Flowable<T>> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.timespan = paramLong;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
      this.bufferSize = paramInt;
    }
    
    public void cancel()
    {
      this.cancelled = true;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this.timer);
    }
    
    void drainLoop()
    {
      SimplePlainQueue localSimplePlainQueue = this.queue;
      Subscriber localSubscriber = this.actual;
      Object localObject1 = this.window;
      int i = 1;
      for (;;)
      {
        boolean bool2 = this.terminated;
        boolean bool1 = this.done;
        Object localObject2 = localSimplePlainQueue.poll();
        if ((bool1) && ((localObject2 == null) || (localObject2 == NEXT)))
        {
          this.window = null;
          localSimplePlainQueue.clear();
          dispose();
          localObject2 = this.error;
          if (localObject2 != null) {
            ((UnicastProcessor)localObject1).onError((Throwable)localObject2);
          } else {
            ((UnicastProcessor)localObject1).onComplete();
          }
          return;
        }
        if (localObject2 == null)
        {
          int j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if (localObject2 == NEXT)
        {
          ((UnicastProcessor)localObject1).onComplete();
          if (!bool2)
          {
            localObject2 = UnicastProcessor.create(this.bufferSize);
            this.window = ((UnicastProcessor)localObject2);
            long l = requested();
            if (l != 0L)
            {
              localSubscriber.onNext(localObject2);
              localObject1 = localObject2;
              if (l != Long.MAX_VALUE)
              {
                produced(1L);
                localObject1 = localObject2;
              }
            }
            else
            {
              this.window = null;
              this.queue.clear();
              this.s.cancel();
              dispose();
              localSubscriber.onError(new MissingBackpressureException("Could not deliver first window due to lack of requests."));
            }
          }
          else
          {
            this.s.cancel();
          }
        }
        else
        {
          ((UnicastProcessor)localObject1).onNext(NotificationLite.getValue(localObject2));
        }
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.terminated) {
        return;
      }
      if (fastEnter())
      {
        this.window.onNext(paramT);
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(NotificationLite.next(paramT));
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.window = UnicastProcessor.create(this.bufferSize);
        Object localObject = this.actual;
        ((Subscriber)localObject).onSubscribe(this);
        long l = requested();
        if (l != 0L)
        {
          ((Subscriber)localObject).onNext(this.window);
          if (l != Long.MAX_VALUE) {
            produced(1L);
          }
          if (!this.cancelled)
          {
            localObject = this.timer;
            Scheduler localScheduler = this.scheduler;
            l = this.timespan;
            if (((SequentialDisposable)localObject).replace(localScheduler.schedulePeriodicallyDirect(this, l, l, this.unit))) {
              paramSubscription.request(Long.MAX_VALUE);
            }
          }
        }
        else
        {
          this.cancelled = true;
          paramSubscription.cancel();
          ((Subscriber)localObject).onError(new MissingBackpressureException("Could not deliver first window due to lack of requests."));
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    public void run()
    {
      if (this.cancelled)
      {
        this.terminated = true;
        dispose();
      }
      this.queue.offer(NEXT);
      if (enter()) {
        drainLoop();
      }
    }
  }
  
  static final class WindowSkipSubscriber<T>
    extends QueueDrainSubscriber<T, Object, Flowable<T>>
    implements Subscription, Runnable
  {
    final int bufferSize;
    Subscription s;
    volatile boolean terminated;
    final long timeskip;
    final long timespan;
    final TimeUnit unit;
    final List<UnicastProcessor<T>> windows;
    final Scheduler.Worker worker;
    
    WindowSkipSubscriber(Subscriber<? super Flowable<T>> paramSubscriber, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker, int paramInt)
    {
      super(new MpscLinkedQueue());
      this.timespan = paramLong1;
      this.timeskip = paramLong2;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
      this.bufferSize = paramInt;
      this.windows = new LinkedList();
    }
    
    public void cancel()
    {
      this.cancelled = true;
    }
    
    void complete(UnicastProcessor<T> paramUnicastProcessor)
    {
      this.queue.offer(new SubjectWork(paramUnicastProcessor, false));
      if (enter()) {
        drainLoop();
      }
    }
    
    public void dispose()
    {
      this.worker.dispose();
    }
    
    void drainLoop()
    {
      Object localObject2 = this.queue;
      Object localObject1 = this.actual;
      List localList = this.windows;
      int i = 1;
      for (;;)
      {
        if (this.terminated)
        {
          this.s.cancel();
          dispose();
          ((SimplePlainQueue)localObject2).clear();
          localList.clear();
          return;
        }
        boolean bool2 = this.done;
        final Object localObject3 = ((SimplePlainQueue)localObject2).poll();
        int j;
        if (localObject3 == null) {
          j = 1;
        } else {
          j = 0;
        }
        boolean bool1 = localObject3 instanceof SubjectWork;
        if ((bool2) && ((j != 0) || (bool1)))
        {
          ((SimplePlainQueue)localObject2).clear();
          dispose();
          localObject2 = this.error;
          if (localObject2 != null)
          {
            localObject1 = localList.iterator();
            while (((Iterator)localObject1).hasNext()) {
              ((UnicastProcessor)((Iterator)localObject1).next()).onError((Throwable)localObject2);
            }
          }
          localObject1 = localList.iterator();
          while (((Iterator)localObject1).hasNext()) {
            ((UnicastProcessor)((Iterator)localObject1).next()).onComplete();
          }
          localList.clear();
          return;
        }
        if (j != 0)
        {
          j = leave(-i);
          i = j;
          if (j != 0) {}
        }
        else if (bool1)
        {
          localObject3 = (SubjectWork)localObject3;
          if (((SubjectWork)localObject3).open)
          {
            if (!this.cancelled)
            {
              long l = requested();
              if (l != 0L)
              {
                localObject3 = UnicastProcessor.create(this.bufferSize);
                localList.add(localObject3);
                ((Subscriber)localObject1).onNext(localObject3);
                if (l != Long.MAX_VALUE) {
                  produced(1L);
                }
                this.worker.schedule(new Runnable()
                {
                  public void run()
                  {
                    FlowableWindowTimed.WindowSkipSubscriber.this.complete(localObject3);
                  }
                }, this.timespan, this.unit);
              }
              else
              {
                ((Subscriber)localObject1).onError(new MissingBackpressureException("Can't emit window due to lack of requests"));
              }
            }
          }
          else
          {
            localList.remove(((SubjectWork)localObject3).w);
            ((SubjectWork)localObject3).w.onComplete();
            if ((localList.isEmpty()) && (this.cancelled)) {
              this.terminated = true;
            }
          }
        }
        else
        {
          Iterator localIterator = localList.iterator();
          while (localIterator.hasNext()) {
            ((UnicastProcessor)localIterator.next()).onNext(localObject3);
          }
        }
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      if (enter()) {
        drainLoop();
      }
      dispose();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (fastEnter())
      {
        Iterator localIterator = this.windows.iterator();
        while (localIterator.hasNext()) {
          ((UnicastProcessor)localIterator.next()).onNext(paramT);
        }
        if (leave(-1) != 0) {}
      }
      else
      {
        this.queue.offer(paramT);
        if (!enter()) {
          return;
        }
      }
      drainLoop();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        if (this.cancelled) {
          return;
        }
        long l = requested();
        if (l != 0L)
        {
          final Object localObject = UnicastProcessor.create(this.bufferSize);
          this.windows.add(localObject);
          this.actual.onNext(localObject);
          if (l != Long.MAX_VALUE) {
            produced(1L);
          }
          this.worker.schedule(new Runnable()
          {
            public void run()
            {
              FlowableWindowTimed.WindowSkipSubscriber.this.complete(localObject);
            }
          }, this.timespan, this.unit);
          localObject = this.worker;
          l = this.timeskip;
          ((Scheduler.Worker)localObject).schedulePeriodically(this, l, l, this.unit);
          paramSubscription.request(Long.MAX_VALUE);
        }
        else
        {
          paramSubscription.cancel();
          this.actual.onError(new MissingBackpressureException("Could not emit the first window due to lack of requests"));
        }
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
    
    public void run()
    {
      SubjectWork localSubjectWork = new SubjectWork(UnicastProcessor.create(this.bufferSize), true);
      if (!this.cancelled) {
        this.queue.offer(localSubjectWork);
      }
      if (enter()) {
        drainLoop();
      }
    }
    
    static final class SubjectWork<T>
    {
      final boolean open;
      final UnicastProcessor<T> w;
      
      SubjectWork(UnicastProcessor<T> paramUnicastProcessor, boolean paramBoolean)
      {
        this.w = paramUnicastProcessor;
        this.open = paramBoolean;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableWindowTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */