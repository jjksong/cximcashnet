package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableJoin<TLeft, TRight, TLeftEnd, TRightEnd, R>
  extends AbstractFlowableWithUpstream<TLeft, R>
{
  final Function<? super TLeft, ? extends Publisher<TLeftEnd>> leftEnd;
  final Publisher<? extends TRight> other;
  final BiFunction<? super TLeft, ? super TRight, ? extends R> resultSelector;
  final Function<? super TRight, ? extends Publisher<TRightEnd>> rightEnd;
  
  public FlowableJoin(Publisher<TLeft> paramPublisher, Publisher<? extends TRight> paramPublisher1, Function<? super TLeft, ? extends Publisher<TLeftEnd>> paramFunction, Function<? super TRight, ? extends Publisher<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super TRight, ? extends R> paramBiFunction)
  {
    super(paramPublisher);
    this.other = paramPublisher1;
    this.leftEnd = paramFunction;
    this.rightEnd = paramFunction1;
    this.resultSelector = paramBiFunction;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    JoinSubscription localJoinSubscription = new JoinSubscription(paramSubscriber, this.leftEnd, this.rightEnd, this.resultSelector);
    paramSubscriber.onSubscribe(localJoinSubscription);
    paramSubscriber = new FlowableGroupJoin.LeftRightSubscriber(localJoinSubscription, true);
    localJoinSubscription.disposables.add(paramSubscriber);
    FlowableGroupJoin.LeftRightSubscriber localLeftRightSubscriber = new FlowableGroupJoin.LeftRightSubscriber(localJoinSubscription, false);
    localJoinSubscription.disposables.add(localLeftRightSubscriber);
    this.source.subscribe(paramSubscriber);
    this.other.subscribe(localLeftRightSubscriber);
  }
  
  static final class JoinSubscription<TLeft, TRight, TLeftEnd, TRightEnd, R>
    extends AtomicInteger
    implements Subscription, FlowableGroupJoin.JoinSupport
  {
    static final Integer LEFT_CLOSE = Integer.valueOf(3);
    static final Integer LEFT_VALUE = Integer.valueOf(1);
    static final Integer RIGHT_CLOSE = Integer.valueOf(4);
    static final Integer RIGHT_VALUE = Integer.valueOf(2);
    private static final long serialVersionUID = -6071216598687999801L;
    final AtomicInteger active;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    final CompositeDisposable disposables;
    final AtomicReference<Throwable> error;
    final Function<? super TLeft, ? extends Publisher<TLeftEnd>> leftEnd;
    int leftIndex;
    final Map<Integer, TLeft> lefts;
    final SpscLinkedArrayQueue<Object> queue;
    final AtomicLong requested;
    final BiFunction<? super TLeft, ? super TRight, ? extends R> resultSelector;
    final Function<? super TRight, ? extends Publisher<TRightEnd>> rightEnd;
    int rightIndex;
    final Map<Integer, TRight> rights;
    
    JoinSubscription(Subscriber<? super R> paramSubscriber, Function<? super TLeft, ? extends Publisher<TLeftEnd>> paramFunction, Function<? super TRight, ? extends Publisher<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super TRight, ? extends R> paramBiFunction)
    {
      this.actual = paramSubscriber;
      this.requested = new AtomicLong();
      this.disposables = new CompositeDisposable();
      this.queue = new SpscLinkedArrayQueue(Flowable.bufferSize());
      this.lefts = new LinkedHashMap();
      this.rights = new LinkedHashMap();
      this.error = new AtomicReference();
      this.leftEnd = paramFunction;
      this.rightEnd = paramFunction1;
      this.resultSelector = paramBiFunction;
      this.active = new AtomicInteger(2);
    }
    
    public void cancel()
    {
      if (this.cancelled) {
        return;
      }
      this.cancelled = true;
      cancelAll();
      if (getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
    
    void cancelAll()
    {
      this.disposables.dispose();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      label837:
      for (;;)
      {
        if (this.cancelled)
        {
          localSpscLinkedArrayQueue.clear();
          return;
        }
        if ((Throwable)this.error.get() != null)
        {
          localSpscLinkedArrayQueue.clear();
          cancelAll();
          errorAll(localSubscriber);
          return;
        }
        int j;
        if (this.active.get() == 0) {
          j = 1;
        } else {
          j = 0;
        }
        Object localObject2 = (Integer)localSpscLinkedArrayQueue.poll();
        int k;
        if (localObject2 == null) {
          k = 1;
        } else {
          k = 0;
        }
        if ((j != 0) && (k != 0))
        {
          this.lefts.clear();
          this.rights.clear();
          this.disposables.dispose();
          localSubscriber.onComplete();
          return;
        }
        if (k != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          Object localObject1 = localSpscLinkedArrayQueue.poll();
          Object localObject3;
          long l2;
          long l1;
          if (localObject2 == LEFT_VALUE)
          {
            j = this.leftIndex;
            this.leftIndex = (j + 1);
            this.lefts.put(Integer.valueOf(j), localObject1);
            try
            {
              localObject2 = (Publisher)ObjectHelper.requireNonNull(this.leftEnd.apply(localObject1), "The leftEnd returned a null Publisher");
              localObject3 = new FlowableGroupJoin.LeftRightEndSubscriber(this, true, j);
              this.disposables.add((Disposable)localObject3);
              ((Publisher)localObject2).subscribe((Subscriber)localObject3);
              if ((Throwable)this.error.get() != null)
              {
                localSpscLinkedArrayQueue.clear();
                cancelAll();
                errorAll(localSubscriber);
                return;
              }
              l2 = this.requested.get();
              localObject2 = this.rights.values().iterator();
              l1 = 0L;
              while (((Iterator)localObject2).hasNext())
              {
                localObject3 = ((Iterator)localObject2).next();
                try
                {
                  localObject3 = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject1, localObject3), "The resultSelector returned a null value");
                  if (l1 != l2)
                  {
                    localSubscriber.onNext(localObject3);
                    l1 += 1L;
                  }
                  else
                  {
                    ExceptionHelper.addThrowable(this.error, new MissingBackpressureException("Could not emit value due to lack of requests"));
                    localSpscLinkedArrayQueue.clear();
                    cancelAll();
                    errorAll(localSubscriber);
                    return;
                  }
                }
                catch (Throwable localThrowable1)
                {
                  fail(localThrowable1, localSubscriber, localSpscLinkedArrayQueue);
                  return;
                }
              }
              if (l1 == 0L) {
                break label837;
              }
              BackpressureHelper.produced(this.requested, l1);
            }
            catch (Throwable localThrowable2)
            {
              fail(localThrowable2, localSubscriber, localSpscLinkedArrayQueue);
              return;
            }
          }
          if (localObject2 == RIGHT_VALUE)
          {
            j = this.rightIndex;
            this.rightIndex = (j + 1);
            this.rights.put(Integer.valueOf(j), localThrowable2);
            try
            {
              localObject3 = (Publisher)ObjectHelper.requireNonNull(this.rightEnd.apply(localThrowable2), "The rightEnd returned a null Publisher");
              localObject2 = new FlowableGroupJoin.LeftRightEndSubscriber(this, false, j);
              this.disposables.add((Disposable)localObject2);
              ((Publisher)localObject3).subscribe((Subscriber)localObject2);
              if ((Throwable)this.error.get() != null)
              {
                localSpscLinkedArrayQueue.clear();
                cancelAll();
                errorAll(localSubscriber);
                return;
              }
              l2 = this.requested.get();
              localObject2 = this.lefts.values().iterator();
              l1 = 0L;
              while (((Iterator)localObject2).hasNext())
              {
                localObject3 = ((Iterator)localObject2).next();
                try
                {
                  localObject3 = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject3, localThrowable2), "The resultSelector returned a null value");
                  if (l1 != l2)
                  {
                    localSubscriber.onNext(localObject3);
                    l1 += 1L;
                  }
                  else
                  {
                    ExceptionHelper.addThrowable(this.error, new MissingBackpressureException("Could not emit value due to lack of requests"));
                    localSpscLinkedArrayQueue.clear();
                    cancelAll();
                    errorAll(localSubscriber);
                    return;
                  }
                }
                catch (Throwable localThrowable3)
                {
                  fail(localThrowable3, localSubscriber, localSpscLinkedArrayQueue);
                  return;
                }
              }
              if (l1 == 0L) {
                break label837;
              }
              BackpressureHelper.produced(this.requested, l1);
            }
            catch (Throwable localThrowable4)
            {
              fail(localThrowable4, localSubscriber, localSpscLinkedArrayQueue);
              return;
            }
          }
          FlowableGroupJoin.LeftRightEndSubscriber localLeftRightEndSubscriber;
          if (localObject2 == LEFT_CLOSE)
          {
            localLeftRightEndSubscriber = (FlowableGroupJoin.LeftRightEndSubscriber)localThrowable4;
            this.lefts.remove(Integer.valueOf(localLeftRightEndSubscriber.index));
            this.disposables.remove(localLeftRightEndSubscriber);
          }
          else if (localObject2 == RIGHT_CLOSE)
          {
            localLeftRightEndSubscriber = (FlowableGroupJoin.LeftRightEndSubscriber)localLeftRightEndSubscriber;
            this.rights.remove(Integer.valueOf(localLeftRightEndSubscriber.index));
            this.disposables.remove(localLeftRightEndSubscriber);
          }
        }
      }
    }
    
    void errorAll(Subscriber<?> paramSubscriber)
    {
      Throwable localThrowable = ExceptionHelper.terminate(this.error);
      this.lefts.clear();
      this.rights.clear();
      paramSubscriber.onError(localThrowable);
    }
    
    void fail(Throwable paramThrowable, Subscriber<?> paramSubscriber, SimpleQueue<?> paramSimpleQueue)
    {
      Exceptions.throwIfFatal(paramThrowable);
      ExceptionHelper.addThrowable(this.error, paramThrowable);
      paramSimpleQueue.clear();
      cancelAll();
      errorAll(paramSubscriber);
    }
    
    public void innerClose(boolean paramBoolean, FlowableGroupJoin.LeftRightEndSubscriber paramLeftRightEndSubscriber)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_CLOSE;
        } else {
          localInteger = RIGHT_CLOSE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramLeftRightEndSubscriber);
        drain();
        return;
      }
      finally {}
    }
    
    public void innerCloseError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerComplete(FlowableGroupJoin.LeftRightSubscriber paramLeftRightSubscriber)
    {
      this.disposables.delete(paramLeftRightSubscriber);
      this.active.decrementAndGet();
      drain();
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable))
      {
        this.active.decrementAndGet();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerValue(boolean paramBoolean, Object paramObject)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_VALUE;
        } else {
          localInteger = RIGHT_VALUE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramObject);
        drain();
        return;
      }
      finally {}
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this.requested, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */