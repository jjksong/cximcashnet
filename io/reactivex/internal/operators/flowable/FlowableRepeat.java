package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRepeat<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long count;
  
  public FlowableRepeat(Publisher<T> paramPublisher, long paramLong)
  {
    super(paramPublisher);
    this.count = paramLong;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SubscriptionArbiter localSubscriptionArbiter = new SubscriptionArbiter();
    paramSubscriber.onSubscribe(localSubscriptionArbiter);
    long l2 = this.count;
    long l1 = Long.MAX_VALUE;
    if (l2 != Long.MAX_VALUE) {
      l1 = l2 - 1L;
    }
    new RepeatSubscriber(paramSubscriber, l1, localSubscriptionArbiter, this.source).subscribeNext();
  }
  
  static final class RepeatSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -7098360935104053232L;
    final Subscriber<? super T> actual;
    long remaining;
    final SubscriptionArbiter sa;
    final Publisher<? extends T> source;
    
    RepeatSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, SubscriptionArbiter paramSubscriptionArbiter, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.sa = paramSubscriptionArbiter;
      this.source = paramPublisher;
      this.remaining = paramLong;
    }
    
    public void onComplete()
    {
      long l = this.remaining;
      if (l != Long.MAX_VALUE) {
        this.remaining = (l - 1L);
      }
      if (l != 0L) {
        subscribeNext();
      } else {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      this.sa.produced(1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.sa.setSubscription(paramSubscription);
    }
    
    void subscribeNext()
    {
      if (getAndIncrement() == 0)
      {
        int i = 1;
        int j;
        do
        {
          if (this.sa.isCancelled()) {
            return;
          }
          this.source.subscribe(this);
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRepeat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */