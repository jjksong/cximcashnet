package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.fuseable.HasUpstreamPublisher;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableReduceMaybe<T>
  extends Maybe<T>
  implements HasUpstreamPublisher<T>, FuseToFlowable<T>
{
  final BiFunction<T, T, T> reducer;
  final Flowable<T> source;
  
  public FlowableReduceMaybe(Flowable<T> paramFlowable, BiFunction<T, T, T> paramBiFunction)
  {
    this.source = paramFlowable;
    this.reducer = paramBiFunction;
  }
  
  public Flowable<T> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableReduce(this.source, this.reducer));
  }
  
  public Publisher<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    this.source.subscribe(new ReduceSubscriber(paramMaybeObserver, this.reducer));
  }
  
  static final class ReduceSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final MaybeObserver<? super T> actual;
    boolean done;
    final BiFunction<T, T, T> reducer;
    Subscription s;
    T value;
    
    ReduceSubscriber(MaybeObserver<? super T> paramMaybeObserver, BiFunction<T, T, T> paramBiFunction)
    {
      this.actual = paramMaybeObserver;
      this.reducer = paramBiFunction;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.done = true;
    }
    
    public boolean isDisposed()
    {
      return this.done;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject = this.value;
      if (localObject != null) {
        this.actual.onSuccess(localObject);
      } else {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      Object localObject = this.value;
      if (localObject == null) {
        this.value = paramT;
      } else {
        try
        {
          this.value = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.s.cancel();
          onError(paramT);
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableReduceMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */