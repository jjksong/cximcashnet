package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTakeLastTimed<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final int bufferSize;
  final long count;
  final boolean delayError;
  final Scheduler scheduler;
  final long time;
  final TimeUnit unit;
  
  public FlowableTakeLastTimed(Publisher<T> paramPublisher, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.count = paramLong1;
    this.time = paramLong2;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.bufferSize = paramInt;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new TakeLastTimedSubscriber(paramSubscriber, this.count, this.time, this.unit, this.scheduler, this.bufferSize, this.delayError));
  }
  
  static final class TakeLastTimedSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -5677354903406201275L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final long count;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final SpscLinkedArrayQueue<Object> queue;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    final Scheduler scheduler;
    final long time;
    final TimeUnit unit;
    
    TakeLastTimedSubscriber(Subscriber<? super T> paramSubscriber, long paramLong1, long paramLong2, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.count = paramLong1;
      this.time = paramLong2;
      this.unit = paramTimeUnit;
      this.scheduler = paramScheduler;
      this.queue = new SpscLinkedArrayQueue(paramInt);
      this.delayError = paramBoolean;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, Subscriber<? super T> paramSubscriber, boolean paramBoolean2)
    {
      if (this.cancelled)
      {
        this.queue.clear();
        return true;
      }
      Throwable localThrowable;
      if (paramBoolean2)
      {
        if (paramBoolean1)
        {
          localThrowable = this.error;
          if (localThrowable != null) {
            paramSubscriber.onError(localThrowable);
          } else {
            paramSubscriber.onComplete();
          }
          return true;
        }
      }
      else
      {
        localThrowable = this.error;
        if (localThrowable != null)
        {
          this.queue.clear();
          paramSubscriber.onError(localThrowable);
          return true;
        }
        if (paramBoolean1)
        {
          paramSubscriber.onComplete();
          return true;
        }
      }
      return false;
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      boolean bool2 = this.delayError;
      int i = 1;
      int j;
      do
      {
        if (this.done)
        {
          if (checkTerminated(localSpscLinkedArrayQueue.isEmpty(), localSubscriber, bool2)) {
            return;
          }
          long l2 = this.requested.get();
          for (long l1 = 0L;; l1 += 1L)
          {
            boolean bool1;
            if (localSpscLinkedArrayQueue.peek() == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool1, localSubscriber, bool2)) {
              return;
            }
            if (l2 == l1)
            {
              if (l1 == 0L) {
                break;
              }
              BackpressureHelper.produced(this.requested, l1);
              break;
            }
            localSpscLinkedArrayQueue.poll();
            localSubscriber.onNext(localSpscLinkedArrayQueue.poll());
          }
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      trim(this.scheduler.now(this.unit), this.queue);
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.delayError) {
        trim(this.scheduler.now(this.unit), this.queue);
      }
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      long l = this.scheduler.now(this.unit);
      localSpscLinkedArrayQueue.offer(Long.valueOf(l), paramT);
      trim(l, localSpscLinkedArrayQueue);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    void trim(long paramLong, SpscLinkedArrayQueue<Object> paramSpscLinkedArrayQueue)
    {
      long l2 = this.time;
      long l1 = this.count;
      int i;
      if (l1 == Long.MAX_VALUE) {
        i = 1;
      } else {
        i = 0;
      }
      while ((!paramSpscLinkedArrayQueue.isEmpty()) && ((((Long)paramSpscLinkedArrayQueue.peek()).longValue() < paramLong - l2) || ((i == 0) && (paramSpscLinkedArrayQueue.size() >> 1 > l1))))
      {
        paramSpscLinkedArrayQueue.poll();
        paramSpscLinkedArrayQueue.poll();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTakeLastTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */