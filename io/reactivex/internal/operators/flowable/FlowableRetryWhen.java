package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.UnicastProcessor;
import io.reactivex.subscribers.SerializedSubscriber;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRetryWhen<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Function<? super Flowable<Throwable>, ? extends Publisher<?>> handler;
  
  public FlowableRetryWhen(Publisher<T> paramPublisher, Function<? super Flowable<Throwable>, ? extends Publisher<?>> paramFunction)
  {
    super(paramPublisher);
    this.handler = paramFunction;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Object localObject = new SerializedSubscriber(paramSubscriber);
    FlowableProcessor localFlowableProcessor = UnicastProcessor.create(8).toSerialized();
    try
    {
      Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.handler.apply(localFlowableProcessor), "handler returned a null Publisher");
      FlowableRepeatWhen.WhenReceiver localWhenReceiver = new FlowableRepeatWhen.WhenReceiver(this.source);
      localObject = new RetryWhenSubscriber((Subscriber)localObject, localFlowableProcessor, localWhenReceiver);
      localWhenReceiver.subscriber = ((FlowableRepeatWhen.WhenSourceSubscriber)localObject);
      paramSubscriber.onSubscribe((Subscription)localObject);
      localPublisher.subscribe(localWhenReceiver);
      localWhenReceiver.onNext(Integer.valueOf(0));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class RetryWhenSubscriber<T>
    extends FlowableRepeatWhen.WhenSourceSubscriber<T, Throwable>
  {
    private static final long serialVersionUID = -2680129890138081029L;
    
    RetryWhenSubscriber(Subscriber<? super T> paramSubscriber, FlowableProcessor<Throwable> paramFlowableProcessor, Subscription paramSubscription)
    {
      super(paramFlowableProcessor, paramSubscription);
    }
    
    public void onComplete()
    {
      this.receiver.cancel();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      again(paramThrowable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRetryWhen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */