package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Subscriber;

public final class FlowableFromFuture<T>
  extends Flowable<T>
{
  final Future<? extends T> future;
  final long timeout;
  final TimeUnit unit;
  
  public FlowableFromFuture(Future<? extends T> paramFuture, long paramLong, TimeUnit paramTimeUnit)
  {
    this.future = paramFuture;
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    DeferredScalarSubscription localDeferredScalarSubscription = new DeferredScalarSubscription(paramSubscriber);
    paramSubscriber.onSubscribe(localDeferredScalarSubscription);
    try
    {
      Object localObject;
      if (this.unit != null) {
        localObject = this.future.get(this.timeout, this.unit);
      } else {
        localObject = this.future.get();
      }
      if (localObject == null) {
        paramSubscriber.onError(new NullPointerException("The future returned null"));
      } else {
        localDeferredScalarSubscription.complete(localObject);
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      if (!localDeferredScalarSubscription.isCancelled()) {
        paramSubscriber.onError(localThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFromFuture.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */