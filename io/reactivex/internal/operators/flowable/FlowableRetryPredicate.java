package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRetryPredicate<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long count;
  final Predicate<? super Throwable> predicate;
  
  public FlowableRetryPredicate(Publisher<T> paramPublisher, long paramLong, Predicate<? super Throwable> paramPredicate)
  {
    super(paramPublisher);
    this.predicate = paramPredicate;
    this.count = paramLong;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SubscriptionArbiter localSubscriptionArbiter = new SubscriptionArbiter();
    paramSubscriber.onSubscribe(localSubscriptionArbiter);
    new RepeatSubscriber(paramSubscriber, this.count, this.predicate, localSubscriptionArbiter, this.source).subscribeNext();
  }
  
  static final class RepeatSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -7098360935104053232L;
    final Subscriber<? super T> actual;
    final Predicate<? super Throwable> predicate;
    long remaining;
    final SubscriptionArbiter sa;
    final Publisher<? extends T> source;
    
    RepeatSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, Predicate<? super Throwable> paramPredicate, SubscriptionArbiter paramSubscriptionArbiter, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.sa = paramSubscriptionArbiter;
      this.source = paramPublisher;
      this.predicate = paramPredicate;
      this.remaining = paramLong;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      long l = this.remaining;
      if (l != Long.MAX_VALUE) {
        this.remaining = (l - 1L);
      }
      if (l == 0L) {
        this.actual.onError(paramThrowable);
      }
      try
      {
        boolean bool = this.predicate.test(paramThrowable);
        if (!bool)
        {
          this.actual.onError(paramThrowable);
          return;
        }
        subscribeNext();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      this.sa.produced(1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.sa.setSubscription(paramSubscription);
    }
    
    void subscribeNext()
    {
      if (getAndIncrement() == 0)
      {
        int i = 1;
        int j;
        do
        {
          if (this.sa.isCancelled()) {
            return;
          }
          this.source.subscribe(this);
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRetryPredicate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */