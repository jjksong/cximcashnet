package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscribers.InnerQueuedSubscriber;
import io.reactivex.internal.subscribers.InnerQueuedSubscriberSupport;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableConcatMapEager<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final ErrorMode errorMode;
  final Function<? super T, ? extends Publisher<? extends R>> mapper;
  final int maxConcurrency;
  final int prefetch;
  
  public FlowableConcatMapEager(Publisher<T> paramPublisher, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt1, int paramInt2, ErrorMode paramErrorMode)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.maxConcurrency = paramInt1;
    this.prefetch = paramInt2;
    this.errorMode = paramErrorMode;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    this.source.subscribe(new ConcatMapEagerDelayErrorSubscriber(paramSubscriber, this.mapper, this.maxConcurrency, this.prefetch, this.errorMode));
  }
  
  static final class ConcatMapEagerDelayErrorSubscriber<T, R>
    extends AtomicInteger
    implements Subscriber<T>, Subscription, InnerQueuedSubscriberSupport<R>
  {
    private static final long serialVersionUID = -4255299542215038287L;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    volatile InnerQueuedSubscriber<R> current;
    volatile boolean done;
    final ErrorMode errorMode;
    final AtomicThrowable errors;
    final Function<? super T, ? extends Publisher<? extends R>> mapper;
    final int maxConcurrency;
    final int prefetch;
    final AtomicLong requested;
    Subscription s;
    final SpscLinkedArrayQueue<InnerQueuedSubscriber<R>> subscribers;
    
    ConcatMapEagerDelayErrorSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt1, int paramInt2, ErrorMode paramErrorMode)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.maxConcurrency = paramInt1;
      this.prefetch = paramInt2;
      this.errorMode = paramErrorMode;
      this.subscribers = new SpscLinkedArrayQueue(Math.min(paramInt2, paramInt1));
      this.errors = new AtomicThrowable();
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      if (this.cancelled) {
        return;
      }
      this.cancelled = true;
      this.s.cancel();
      drainAndCancel();
    }
    
    void cancelAll()
    {
      for (;;)
      {
        InnerQueuedSubscriber localInnerQueuedSubscriber = (InnerQueuedSubscriber)this.subscribers.poll();
        if (localInnerQueuedSubscriber == null) {
          break;
        }
        localInnerQueuedSubscriber.cancel();
      }
    }
    
    public void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Object localObject1 = this.current;
      Subscriber localSubscriber = this.actual;
      ErrorMode localErrorMode = this.errorMode;
      int i = 1;
      int j;
      label375:
      label503:
      do
      {
        do
        {
          long l2 = this.requested.get();
          boolean bool1;
          Object localObject2;
          if (localObject1 == null)
          {
            if ((localErrorMode != ErrorMode.END) && ((Throwable)this.errors.get() != null))
            {
              cancelAll();
              localSubscriber.onError(this.errors.terminate());
              return;
            }
            bool1 = this.done;
            localObject2 = (InnerQueuedSubscriber)this.subscribers.poll();
            if ((bool1) && (localObject2 == null))
            {
              localObject1 = this.errors.terminate();
              if (localObject1 != null) {
                localSubscriber.onError((Throwable)localObject1);
              } else {
                localSubscriber.onComplete();
              }
              return;
            }
            localObject1 = localObject2;
            if (localObject2 != null)
            {
              this.current = ((InnerQueuedSubscriber)localObject2);
              localObject1 = localObject2;
            }
          }
          if (localObject1 != null)
          {
            localObject2 = ((InnerQueuedSubscriber)localObject1).queue();
            if (localObject2 != null)
            {
              l1 = 0L;
              while (l1 != l2)
              {
                if (this.cancelled)
                {
                  cancelAll();
                  return;
                }
                if ((localErrorMode == ErrorMode.IMMEDIATE) && ((Throwable)this.errors.get() != null))
                {
                  this.current = null;
                  ((InnerQueuedSubscriber)localObject1).cancel();
                  cancelAll();
                  localSubscriber.onError(this.errors.terminate());
                  return;
                }
                bool1 = ((InnerQueuedSubscriber)localObject1).isDone();
                try
                {
                  Object localObject3 = ((SimpleQueue)localObject2).poll();
                  if (localObject3 == null) {
                    j = 1;
                  } else {
                    j = 0;
                  }
                  if ((bool1) && (j != 0))
                  {
                    this.current = null;
                    this.s.request(1L);
                    localObject1 = null;
                    j = 1;
                    break label375;
                  }
                  if (j == 0)
                  {
                    localSubscriber.onNext(localObject3);
                    l1 += 1L;
                    ((InnerQueuedSubscriber)localObject1).requestOne();
                  }
                }
                catch (Throwable localThrowable)
                {
                  Exceptions.throwIfFatal(localThrowable);
                  this.current = null;
                  ((InnerQueuedSubscriber)localObject1).cancel();
                  cancelAll();
                  localSubscriber.onError(localThrowable);
                  return;
                }
              }
              j = 0;
              if (l1 == l2)
              {
                if (this.cancelled)
                {
                  cancelAll();
                  return;
                }
                if ((localErrorMode == ErrorMode.IMMEDIATE) && ((Throwable)this.errors.get() != null))
                {
                  this.current = null;
                  ((InnerQueuedSubscriber)localObject1).cancel();
                  cancelAll();
                  localSubscriber.onError(this.errors.terminate());
                  return;
                }
                boolean bool2 = ((InnerQueuedSubscriber)localObject1).isDone();
                bool1 = localThrowable.isEmpty();
                if ((bool2) && (bool1))
                {
                  this.current = null;
                  this.s.request(1L);
                  localObject1 = null;
                  j = 1;
                  break label503;
                }
              }
              break label503;
            }
          }
          long l1 = 0L;
          j = 0;
          if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
            this.requested.addAndGet(-l1);
          }
        } while (j != 0);
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void drainAndCancel()
    {
      if (getAndIncrement() == 0) {
        do
        {
          cancelAll();
        } while (decrementAndGet() != 0);
      }
    }
    
    public void innerComplete(InnerQueuedSubscriber<R> paramInnerQueuedSubscriber)
    {
      paramInnerQueuedSubscriber.setDone();
      drain();
    }
    
    public void innerError(InnerQueuedSubscriber<R> paramInnerQueuedSubscriber, Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        paramInnerQueuedSubscriber.setDone();
        if (this.errorMode != ErrorMode.END) {
          this.s.cancel();
        }
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerNext(InnerQueuedSubscriber<R> paramInnerQueuedSubscriber, R paramR)
    {
      if (paramInnerQueuedSubscriber.queue().offer(paramR))
      {
        drain();
      }
      else
      {
        paramInnerQueuedSubscriber.cancel();
        innerError(paramInnerQueuedSubscriber, new MissingBackpressureException());
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.errors.addThrowable(paramThrowable))
      {
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      try
      {
        paramT = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null Publisher");
        InnerQueuedSubscriber localInnerQueuedSubscriber = new InnerQueuedSubscriber(this, this.prefetch);
        if (this.cancelled) {
          return;
        }
        this.subscribers.offer(localInnerQueuedSubscriber);
        if (this.cancelled) {
          return;
        }
        paramT.subscribe(localInnerQueuedSubscriber);
        if (this.cancelled)
        {
          localInnerQueuedSubscriber.cancel();
          drainAndCancel();
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        int i = this.maxConcurrency;
        long l;
        if (i == Integer.MAX_VALUE) {
          l = Long.MAX_VALUE;
        } else {
          l = i;
        }
        paramSubscription.request(l);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableConcatMapEager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */