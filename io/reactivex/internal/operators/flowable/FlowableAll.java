package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableAll<T>
  extends AbstractFlowableWithUpstream<T, Boolean>
{
  final Predicate<? super T> predicate;
  
  public FlowableAll(Publisher<T> paramPublisher, Predicate<? super T> paramPredicate)
  {
    super(paramPublisher);
    this.predicate = paramPredicate;
  }
  
  protected void subscribeActual(Subscriber<? super Boolean> paramSubscriber)
  {
    this.source.subscribe(new AllSubscriber(paramSubscriber, this.predicate));
  }
  
  static final class AllSubscriber<T>
    extends DeferredScalarSubscription<Boolean>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -3521127104134758517L;
    boolean done;
    final Predicate<? super T> predicate;
    Subscription s;
    
    AllSubscriber(Subscriber<? super Boolean> paramSubscriber, Predicate<? super T> paramPredicate)
    {
      super();
      this.predicate = paramPredicate;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      complete(Boolean.valueOf(true));
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        boolean bool = this.predicate.test(paramT);
        if (!bool)
        {
          this.done = true;
          this.s.cancel();
          complete(Boolean.valueOf(false));
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableAll.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */