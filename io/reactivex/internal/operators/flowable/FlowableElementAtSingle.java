package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableElementAtSingle<T>
  extends Single<T>
  implements FuseToFlowable<T>
{
  final T defaultValue;
  final long index;
  final Publisher<T> source;
  
  public FlowableElementAtSingle(Publisher<T> paramPublisher, long paramLong, T paramT)
  {
    this.source = paramPublisher;
    this.index = paramLong;
    this.defaultValue = paramT;
  }
  
  public Flowable<T> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableElementAt(this.source, this.index, this.defaultValue, true));
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new ElementAtSubscriber(paramSingleObserver, this.index, this.defaultValue));
  }
  
  static final class ElementAtSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    long count;
    final T defaultValue;
    boolean done;
    final long index;
    Subscription s;
    
    ElementAtSubscriber(SingleObserver<? super T> paramSingleObserver, long paramLong, T paramT)
    {
      this.actual = paramSingleObserver;
      this.index = paramLong;
      this.defaultValue = paramT;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      if (!this.done)
      {
        this.done = true;
        Object localObject = this.defaultValue;
        if (localObject != null) {
          this.actual.onSuccess(localObject);
        } else {
          this.actual.onError(new NoSuchElementException());
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.count;
      if (l == this.index)
      {
        this.done = true;
        this.s.cancel();
        this.s = SubscriptionHelper.CANCELLED;
        this.actual.onSuccess(paramT);
        return;
      }
      this.count = (l + 1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableElementAtSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */