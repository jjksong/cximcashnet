package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.LongConsumer;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDoOnLifecycle<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  private final Action onCancel;
  private final LongConsumer onRequest;
  private final Consumer<? super Subscription> onSubscribe;
  
  public FlowableDoOnLifecycle(Flowable<T> paramFlowable, Consumer<? super Subscription> paramConsumer, LongConsumer paramLongConsumer, Action paramAction)
  {
    super(paramFlowable);
    this.onSubscribe = paramConsumer;
    this.onRequest = paramLongConsumer;
    this.onCancel = paramAction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new SubscriptionLambdaSubscriber(paramSubscriber, this.onSubscribe, this.onRequest, this.onCancel));
  }
  
  static final class SubscriptionLambdaSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super T> actual;
    final Action onCancel;
    final LongConsumer onRequest;
    final Consumer<? super Subscription> onSubscribe;
    Subscription s;
    
    SubscriptionLambdaSubscriber(Subscriber<? super T> paramSubscriber, Consumer<? super Subscription> paramConsumer, LongConsumer paramLongConsumer, Action paramAction)
    {
      this.actual = paramSubscriber;
      this.onSubscribe = paramConsumer;
      this.onCancel = paramAction;
      this.onRequest = paramLongConsumer;
    }
    
    public void cancel()
    {
      try
      {
        this.onCancel.run();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
      this.s.cancel();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      try
      {
        this.onSubscribe.accept(paramSubscription);
        if (SubscriptionHelper.validate(this.s, paramSubscription))
        {
          this.s = paramSubscription;
          this.actual.onSubscribe(this);
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        paramSubscription.cancel();
        RxJavaPlugins.onError(localThrowable);
        EmptySubscription.error(localThrowable, this.actual);
      }
    }
    
    public void request(long paramLong)
    {
      try
      {
        this.onRequest.accept(paramLong);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        RxJavaPlugins.onError(localThrowable);
      }
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDoOnLifecycle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */