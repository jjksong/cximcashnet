package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.HasUpstreamPublisher;
import io.reactivex.internal.subscribers.SubscriberResourceWrapper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Timed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableReplay<T>
  extends ConnectableFlowable<T>
  implements HasUpstreamPublisher<T>
{
  static final Callable DEFAULT_UNBOUNDED_FACTORY = new Callable()
  {
    public Object call()
    {
      return new FlowableReplay.UnboundedReplayBuffer(16);
    }
  };
  final Callable<? extends ReplayBuffer<T>> bufferFactory;
  final AtomicReference<ReplaySubscriber<T>> current;
  final Publisher<T> onSubscribe;
  final Publisher<T> source;
  
  private FlowableReplay(Publisher<T> paramPublisher1, Publisher<T> paramPublisher2, AtomicReference<ReplaySubscriber<T>> paramAtomicReference, Callable<? extends ReplayBuffer<T>> paramCallable)
  {
    this.onSubscribe = paramPublisher1;
    this.source = paramPublisher2;
    this.current = paramAtomicReference;
    this.bufferFactory = paramCallable;
  }
  
  public static <T> ConnectableFlowable<T> create(Publisher<T> paramPublisher, int paramInt)
  {
    if (paramInt == Integer.MAX_VALUE) {
      return createFrom(paramPublisher);
    }
    create(paramPublisher, new Callable()
    {
      public FlowableReplay.ReplayBuffer<T> call()
      {
        return new FlowableReplay.SizeBoundReplayBuffer(this.val$bufferSize);
      }
    });
  }
  
  public static <T> ConnectableFlowable<T> create(Publisher<T> paramPublisher, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    return create(paramPublisher, paramLong, paramTimeUnit, paramScheduler, Integer.MAX_VALUE);
  }
  
  public static <T> ConnectableFlowable<T> create(Publisher<T> paramPublisher, final long paramLong, TimeUnit paramTimeUnit, final Scheduler paramScheduler, int paramInt)
  {
    create(paramPublisher, new Callable()
    {
      public FlowableReplay.ReplayBuffer<T> call()
      {
        return new FlowableReplay.SizeAndTimeBoundReplayBuffer(this.val$bufferSize, paramLong, paramScheduler, this.val$scheduler);
      }
    });
  }
  
  static <T> ConnectableFlowable<T> create(Publisher<T> paramPublisher, final Callable<? extends ReplayBuffer<T>> paramCallable)
  {
    AtomicReference localAtomicReference = new AtomicReference();
    RxJavaPlugins.onAssembly(new FlowableReplay(new Publisher()
    {
      public void subscribe(Subscriber<? super T> paramAnonymousSubscriber)
      {
        Object localObject1;
        for (;;)
        {
          localObject2 = (FlowableReplay.ReplaySubscriber)this.val$curr.get();
          localObject1 = localObject2;
          if (localObject2 == null) {
            try
            {
              localObject1 = (FlowableReplay.ReplayBuffer)paramCallable.call();
              localObject1 = new FlowableReplay.ReplaySubscriber((FlowableReplay.ReplayBuffer)localObject1);
              if (!this.val$curr.compareAndSet(null, localObject1)) {}
            }
            catch (Throwable paramAnonymousSubscriber)
            {
              Exceptions.throwIfFatal(paramAnonymousSubscriber);
              throw ExceptionHelper.wrapOrThrow(paramAnonymousSubscriber);
            }
          }
        }
        Object localObject2 = new FlowableReplay.InnerSubscription((FlowableReplay.ReplaySubscriber)localObject1, paramAnonymousSubscriber);
        paramAnonymousSubscriber.onSubscribe((Subscription)localObject2);
        ((FlowableReplay.ReplaySubscriber)localObject1).add((FlowableReplay.InnerSubscription)localObject2);
        if (((FlowableReplay.InnerSubscription)localObject2).isDisposed())
        {
          ((FlowableReplay.ReplaySubscriber)localObject1).remove((FlowableReplay.InnerSubscription)localObject2);
          return;
        }
        ((FlowableReplay.ReplaySubscriber)localObject1).manageRequests();
        ((FlowableReplay.ReplaySubscriber)localObject1).buffer.replay((FlowableReplay.InnerSubscription)localObject2);
      }
    }, paramPublisher, localAtomicReference, paramCallable));
  }
  
  public static <T> ConnectableFlowable<T> createFrom(Publisher<? extends T> paramPublisher)
  {
    return create(paramPublisher, DEFAULT_UNBOUNDED_FACTORY);
  }
  
  public static <U, R> Flowable<R> multicastSelector(Callable<? extends ConnectableFlowable<U>> paramCallable, final Function<? super Flowable<U>, ? extends Publisher<R>> paramFunction)
  {
    Flowable.unsafeCreate(new Publisher()
    {
      public void subscribe(final Subscriber<? super R> paramAnonymousSubscriber)
      {
        try
        {
          ConnectableFlowable localConnectableFlowable = (ConnectableFlowable)ObjectHelper.requireNonNull(this.val$connectableFactory.call(), "The connectableFactory returned null");
          try
          {
            Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(paramFunction.apply(localConnectableFlowable), "The selector returned a null Publisher");
            paramAnonymousSubscriber = new SubscriberResourceWrapper(paramAnonymousSubscriber);
            localPublisher.subscribe(paramAnonymousSubscriber);
            localConnectableFlowable.connect(new Consumer()
            {
              public void accept(Disposable paramAnonymous2Disposable)
              {
                paramAnonymousSubscriber.setResource(paramAnonymous2Disposable);
              }
            });
            return;
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            EmptySubscription.error(localThrowable1, paramAnonymousSubscriber);
            return;
          }
          return;
        }
        catch (Throwable localThrowable2)
        {
          Exceptions.throwIfFatal(localThrowable2);
          EmptySubscription.error(localThrowable2, paramAnonymousSubscriber);
        }
      }
    });
  }
  
  public static <T> ConnectableFlowable<T> observeOn(ConnectableFlowable<T> paramConnectableFlowable, Scheduler paramScheduler)
  {
    RxJavaPlugins.onAssembly(new ConnectableFlowable()
    {
      public void connect(Consumer<? super Disposable> paramAnonymousConsumer)
      {
        this.val$co.connect(paramAnonymousConsumer);
      }
      
      protected void subscribeActual(Subscriber<? super T> paramAnonymousSubscriber)
      {
        this.val$observable.subscribe(paramAnonymousSubscriber);
      }
    });
  }
  
  /* Error */
  public void connect(Consumer<? super Disposable> paramConsumer)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 72	io/reactivex/internal/operators/flowable/FlowableReplay:current	Ljava/util/concurrent/atomic/AtomicReference;
    //   4: invokevirtual 144	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
    //   7: checkcast 35	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber
    //   10: astore 4
    //   12: aload 4
    //   14: ifnull +14 -> 28
    //   17: aload 4
    //   19: astore_3
    //   20: aload 4
    //   22: invokevirtual 148	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber:isDisposed	()Z
    //   25: ifeq +41 -> 66
    //   28: aload_0
    //   29: getfield 74	io/reactivex/internal/operators/flowable/FlowableReplay:bufferFactory	Ljava/util/concurrent/Callable;
    //   32: invokeinterface 153 1 0
    //   37: checkcast 32	io/reactivex/internal/operators/flowable/FlowableReplay$ReplayBuffer
    //   40: astore_3
    //   41: new 35	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber
    //   44: dup
    //   45: aload_3
    //   46: invokespecial 156	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber:<init>	(Lio/reactivex/internal/operators/flowable/FlowableReplay$ReplayBuffer;)V
    //   49: astore_3
    //   50: aload_0
    //   51: getfield 72	io/reactivex/internal/operators/flowable/FlowableReplay:current	Ljava/util/concurrent/atomic/AtomicReference;
    //   54: aload 4
    //   56: aload_3
    //   57: invokevirtual 160	java/util/concurrent/atomic/AtomicReference:compareAndSet	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   60: ifne +6 -> 66
    //   63: goto -63 -> 0
    //   66: aload_3
    //   67: getfield 164	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber:shouldConnect	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   70: invokevirtual 168	java/util/concurrent/atomic/AtomicBoolean:get	()Z
    //   73: ifne +20 -> 93
    //   76: aload_3
    //   77: getfield 164	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber:shouldConnect	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   80: iconst_0
    //   81: iconst_1
    //   82: invokevirtual 171	java/util/concurrent/atomic/AtomicBoolean:compareAndSet	(ZZ)Z
    //   85: ifeq +8 -> 93
    //   88: iconst_1
    //   89: istore_2
    //   90: goto +5 -> 95
    //   93: iconst_0
    //   94: istore_2
    //   95: aload_1
    //   96: aload_3
    //   97: invokeinterface 177 2 0
    //   102: iload_2
    //   103: ifeq +13 -> 116
    //   106: aload_0
    //   107: getfield 70	io/reactivex/internal/operators/flowable/FlowableReplay:source	Lorg/reactivestreams/Publisher;
    //   110: aload_3
    //   111: invokeinterface 183 2 0
    //   116: return
    //   117: astore_1
    //   118: iload_2
    //   119: ifeq +13 -> 132
    //   122: aload_3
    //   123: getfield 164	io/reactivex/internal/operators/flowable/FlowableReplay$ReplaySubscriber:shouldConnect	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   126: iconst_1
    //   127: iconst_0
    //   128: invokevirtual 171	java/util/concurrent/atomic/AtomicBoolean:compareAndSet	(ZZ)Z
    //   131: pop
    //   132: aload_1
    //   133: invokestatic 189	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   136: aload_1
    //   137: invokestatic 195	io/reactivex/internal/util/ExceptionHelper:wrapOrThrow	(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    //   140: athrow
    //   141: astore_1
    //   142: aload_1
    //   143: invokestatic 189	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
    //   146: aload_1
    //   147: invokestatic 195	io/reactivex/internal/util/ExceptionHelper:wrapOrThrow	(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    //   150: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	FlowableReplay
    //   0	151	1	paramConsumer	Consumer<? super Disposable>
    //   89	30	2	i	int
    //   19	104	3	localObject	Object
    //   10	45	4	localReplaySubscriber	ReplaySubscriber
    // Exception table:
    //   from	to	target	type
    //   95	102	117	java/lang/Throwable
    //   28	41	141	java/lang/Throwable
  }
  
  public Publisher<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.onSubscribe.subscribe(paramSubscriber);
  }
  
  static class BoundedReplayBuffer<T>
    extends AtomicReference<FlowableReplay.Node>
    implements FlowableReplay.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 2346567790059478686L;
    long index;
    int size;
    FlowableReplay.Node tail;
    
    BoundedReplayBuffer()
    {
      FlowableReplay.Node localNode = new FlowableReplay.Node(null, 0L);
      this.tail = localNode;
      set(localNode);
    }
    
    final void addLast(FlowableReplay.Node paramNode)
    {
      this.tail.set(paramNode);
      this.tail = paramNode;
      this.size += 1;
    }
    
    final void collect(Collection<? super T> paramCollection)
    {
      FlowableReplay.Node localNode = (FlowableReplay.Node)get();
      for (;;)
      {
        localNode = (FlowableReplay.Node)localNode.get();
        if (localNode == null) {
          break;
        }
        Object localObject = leaveTransform(localNode.value);
        if ((NotificationLite.isComplete(localObject)) || (NotificationLite.isError(localObject))) {
          break;
        }
        paramCollection.add(NotificationLite.getValue(localObject));
      }
    }
    
    public final void complete()
    {
      Object localObject = enterTransform(NotificationLite.complete());
      long l = this.index + 1L;
      this.index = l;
      addLast(new FlowableReplay.Node(localObject, l));
      truncateFinal();
    }
    
    Object enterTransform(Object paramObject)
    {
      return paramObject;
    }
    
    public final void error(Throwable paramThrowable)
    {
      paramThrowable = enterTransform(NotificationLite.error(paramThrowable));
      long l = this.index + 1L;
      this.index = l;
      addLast(new FlowableReplay.Node(paramThrowable, l));
      truncateFinal();
    }
    
    boolean hasCompleted()
    {
      boolean bool;
      if ((this.tail.value != null) && (NotificationLite.isComplete(leaveTransform(this.tail.value)))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    boolean hasError()
    {
      boolean bool;
      if ((this.tail.value != null) && (NotificationLite.isError(leaveTransform(this.tail.value)))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    Object leaveTransform(Object paramObject)
    {
      return paramObject;
    }
    
    public final void next(T paramT)
    {
      paramT = enterTransform(NotificationLite.next(paramT));
      long l = this.index + 1L;
      this.index = l;
      addLast(new FlowableReplay.Node(paramT, l));
      truncate();
    }
    
    final void removeFirst()
    {
      FlowableReplay.Node localNode = (FlowableReplay.Node)((FlowableReplay.Node)get()).get();
      if (localNode != null)
      {
        this.size -= 1;
        setFirst(localNode);
        return;
      }
      throw new IllegalStateException("Empty list!");
    }
    
    final void removeSome(int paramInt)
    {
      FlowableReplay.Node localNode = (FlowableReplay.Node)get();
      while (paramInt > 0)
      {
        localNode = (FlowableReplay.Node)localNode.get();
        paramInt--;
        this.size -= 1;
      }
      setFirst(localNode);
    }
    
    /* Error */
    public final void replay(FlowableReplay.InnerSubscription<T> paramInnerSubscription)
    {
      // Byte code:
      //   0: aload_1
      //   1: monitorenter
      //   2: aload_1
      //   3: getfield 125	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   6: ifeq +11 -> 17
      //   9: aload_1
      //   10: iconst_1
      //   11: putfield 128	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   14: aload_1
      //   15: monitorexit
      //   16: return
      //   17: aload_1
      //   18: iconst_1
      //   19: putfield 125	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   22: aload_1
      //   23: monitorexit
      //   24: aload_1
      //   25: invokevirtual 131	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:isDisposed	()Z
      //   28: ifeq +4 -> 32
      //   31: return
      //   32: aload_1
      //   33: invokevirtual 134	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:get	()J
      //   36: lstore 5
      //   38: lload 5
      //   40: ldc2_w 135
      //   43: lcmp
      //   44: ifne +8 -> 52
      //   47: iconst_1
      //   48: istore_2
      //   49: goto +5 -> 54
      //   52: iconst_0
      //   53: istore_2
      //   54: aload_1
      //   55: invokevirtual 138	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	()Ljava/lang/Object;
      //   58: checkcast 25	io/reactivex/internal/operators/flowable/FlowableReplay$Node
      //   61: astore 7
      //   63: aload 7
      //   65: ifnonnull +36 -> 101
      //   68: aload_0
      //   69: invokevirtual 46	io/reactivex/internal/operators/flowable/FlowableReplay$BoundedReplayBuffer:get	()Ljava/lang/Object;
      //   72: checkcast 25	io/reactivex/internal/operators/flowable/FlowableReplay$Node
      //   75: astore 7
      //   77: aload_1
      //   78: aload 7
      //   80: putfield 140	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	Ljava/lang/Object;
      //   83: aload_1
      //   84: getfield 144	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:totalRequested	Ljava/util/concurrent/atomic/AtomicLong;
      //   87: aload 7
      //   89: getfield 145	io/reactivex/internal/operators/flowable/FlowableReplay$Node:index	J
      //   92: invokestatic 150	io/reactivex/internal/util/BackpressureHelper:add	(Ljava/util/concurrent/atomic/AtomicLong;J)J
      //   95: pop2
      //   96: lconst_0
      //   97: lstore_3
      //   98: goto +5 -> 103
      //   101: lconst_0
      //   102: lstore_3
      //   103: lload 5
      //   105: lconst_0
      //   106: lcmp
      //   107: ifeq +116 -> 223
      //   110: aload 7
      //   112: invokevirtual 47	io/reactivex/internal/operators/flowable/FlowableReplay$Node:get	()Ljava/lang/Object;
      //   115: checkcast 25	io/reactivex/internal/operators/flowable/FlowableReplay$Node
      //   118: astore 8
      //   120: aload 8
      //   122: ifnull +101 -> 223
      //   125: aload_0
      //   126: aload 8
      //   128: getfield 51	io/reactivex/internal/operators/flowable/FlowableReplay$Node:value	Ljava/lang/Object;
      //   131: invokevirtual 55	io/reactivex/internal/operators/flowable/FlowableReplay$BoundedReplayBuffer:leaveTransform	(Ljava/lang/Object;)Ljava/lang/Object;
      //   134: astore 7
      //   136: aload 7
      //   138: aload_1
      //   139: getfield 154	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:child	Lorg/reactivestreams/Subscriber;
      //   142: invokestatic 158	io/reactivex/internal/util/NotificationLite:accept	(Ljava/lang/Object;Lorg/reactivestreams/Subscriber;)Z
      //   145: ifeq +9 -> 154
      //   148: aload_1
      //   149: aconst_null
      //   150: putfield 140	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	Ljava/lang/Object;
      //   153: return
      //   154: lload_3
      //   155: lconst_1
      //   156: ladd
      //   157: lstore_3
      //   158: lload 5
      //   160: lconst_1
      //   161: lsub
      //   162: lstore 5
      //   164: aload_1
      //   165: invokevirtual 131	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:isDisposed	()Z
      //   168: ifeq +4 -> 172
      //   171: return
      //   172: aload 8
      //   174: astore 7
      //   176: goto -73 -> 103
      //   179: astore 8
      //   181: aload 8
      //   183: invokestatic 163	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   186: aload_1
      //   187: aconst_null
      //   188: putfield 140	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	Ljava/lang/Object;
      //   191: aload_1
      //   192: invokevirtual 166	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:dispose	()V
      //   195: aload 7
      //   197: invokestatic 64	io/reactivex/internal/util/NotificationLite:isError	(Ljava/lang/Object;)Z
      //   200: ifne +22 -> 222
      //   203: aload 7
      //   205: invokestatic 61	io/reactivex/internal/util/NotificationLite:isComplete	(Ljava/lang/Object;)Z
      //   208: ifne +14 -> 222
      //   211: aload_1
      //   212: getfield 154	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:child	Lorg/reactivestreams/Subscriber;
      //   215: aload 8
      //   217: invokeinterface 171 2 0
      //   222: return
      //   223: lload_3
      //   224: lconst_0
      //   225: lcmp
      //   226: ifeq +19 -> 245
      //   229: aload_1
      //   230: aload 7
      //   232: putfield 140	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	Ljava/lang/Object;
      //   235: iload_2
      //   236: ifne +9 -> 245
      //   239: aload_1
      //   240: lload_3
      //   241: invokevirtual 175	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:produced	(J)J
      //   244: pop2
      //   245: aload_1
      //   246: monitorenter
      //   247: aload_1
      //   248: getfield 128	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   251: ifne +11 -> 262
      //   254: aload_1
      //   255: iconst_0
      //   256: putfield 125	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   259: aload_1
      //   260: monitorexit
      //   261: return
      //   262: aload_1
      //   263: iconst_0
      //   264: putfield 128	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   267: aload_1
      //   268: monitorexit
      //   269: goto -245 -> 24
      //   272: astore 7
      //   274: aload_1
      //   275: monitorexit
      //   276: aload 7
      //   278: athrow
      //   279: astore 7
      //   281: aload_1
      //   282: monitorexit
      //   283: aload 7
      //   285: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	286	0	this	BoundedReplayBuffer
      //   0	286	1	paramInnerSubscription	FlowableReplay.InnerSubscription<T>
      //   48	188	2	i	int
      //   97	144	3	l1	long
      //   36	127	5	l2	long
      //   61	170	7	localObject1	Object
      //   272	5	7	localObject2	Object
      //   279	5	7	localObject3	Object
      //   118	55	8	localNode	FlowableReplay.Node
      //   179	37	8	localThrowable	Throwable
      // Exception table:
      //   from	to	target	type
      //   136	153	179	java/lang/Throwable
      //   247	261	272	finally
      //   262	269	272	finally
      //   274	276	272	finally
      //   2	16	279	finally
      //   17	24	279	finally
      //   281	283	279	finally
    }
    
    final void setFirst(FlowableReplay.Node paramNode)
    {
      set(paramNode);
    }
    
    void truncate() {}
    
    void truncateFinal() {}
  }
  
  static final class InnerSubscription<T>
    extends AtomicLong
    implements Subscription, Disposable
  {
    static final long CANCELLED = Long.MIN_VALUE;
    private static final long serialVersionUID = -4453897557930727610L;
    final Subscriber<? super T> child;
    boolean emitting;
    Object index;
    boolean missed;
    final FlowableReplay.ReplaySubscriber<T> parent;
    final AtomicLong totalRequested;
    
    InnerSubscription(FlowableReplay.ReplaySubscriber<T> paramReplaySubscriber, Subscriber<? super T> paramSubscriber)
    {
      this.parent = paramReplaySubscriber;
      this.child = paramSubscriber;
      this.totalRequested = new AtomicLong();
    }
    
    public void cancel()
    {
      dispose();
    }
    
    public void dispose()
    {
      if (getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE)
      {
        this.parent.remove(this);
        this.parent.manageRequests();
      }
    }
    
    <U> U index()
    {
      return (U)this.index;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == Long.MIN_VALUE) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public long produced(long paramLong)
    {
      return BackpressureHelper.producedCancel(this, paramLong);
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        long l;
        do
        {
          l = get();
          if (l == Long.MIN_VALUE) {
            return;
          }
          if ((l >= 0L) && (paramLong == 0L)) {
            return;
          }
        } while (!compareAndSet(l, BackpressureHelper.addCap(l, paramLong)));
        BackpressureHelper.add(this.totalRequested, paramLong);
        this.parent.manageRequests();
        this.parent.buffer.replay(this);
        return;
      }
    }
  }
  
  static final class Node
    extends AtomicReference<Node>
  {
    private static final long serialVersionUID = 245354315435971818L;
    final long index;
    final Object value;
    
    Node(Object paramObject, long paramLong)
    {
      this.value = paramObject;
      this.index = paramLong;
    }
  }
  
  static abstract interface ReplayBuffer<T>
  {
    public abstract void complete();
    
    public abstract void error(Throwable paramThrowable);
    
    public abstract void next(T paramT);
    
    public abstract void replay(FlowableReplay.InnerSubscription<T> paramInnerSubscription);
  }
  
  static final class ReplaySubscriber<T>
    implements Subscriber<T>, Disposable
  {
    static final FlowableReplay.InnerSubscription[] EMPTY = new FlowableReplay.InnerSubscription[0];
    static final FlowableReplay.InnerSubscription[] TERMINATED = new FlowableReplay.InnerSubscription[0];
    final FlowableReplay.ReplayBuffer<T> buffer;
    boolean done;
    final AtomicInteger management;
    long maxChildRequested;
    long maxUpstreamRequested;
    final AtomicBoolean shouldConnect;
    final AtomicReference<FlowableReplay.InnerSubscription<T>[]> subscribers;
    volatile Subscription subscription;
    
    ReplaySubscriber(FlowableReplay.ReplayBuffer<T> paramReplayBuffer)
    {
      this.buffer = paramReplayBuffer;
      this.management = new AtomicInteger();
      this.subscribers = new AtomicReference(EMPTY);
      this.shouldConnect = new AtomicBoolean();
    }
    
    boolean add(FlowableReplay.InnerSubscription<T> paramInnerSubscription)
    {
      if (paramInnerSubscription != null)
      {
        FlowableReplay.InnerSubscription[] arrayOfInnerSubscription1;
        FlowableReplay.InnerSubscription[] arrayOfInnerSubscription2;
        do
        {
          arrayOfInnerSubscription1 = (FlowableReplay.InnerSubscription[])this.subscribers.get();
          if (arrayOfInnerSubscription1 == TERMINATED) {
            return false;
          }
          int i = arrayOfInnerSubscription1.length;
          arrayOfInnerSubscription2 = new FlowableReplay.InnerSubscription[i + 1];
          System.arraycopy(arrayOfInnerSubscription1, 0, arrayOfInnerSubscription2, 0, i);
          arrayOfInnerSubscription2[i] = paramInnerSubscription;
        } while (!this.subscribers.compareAndSet(arrayOfInnerSubscription1, arrayOfInnerSubscription2));
        return true;
      }
      throw new NullPointerException();
    }
    
    public void dispose()
    {
      this.subscribers.set(TERMINATED);
      this.subscription.cancel();
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.subscribers.get() == TERMINATED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    void manageRequests()
    {
      if (this.management.getAndIncrement() != 0) {
        return;
      }
      int i = 1;
      int j;
      do
      {
        if (isDisposed()) {
          return;
        }
        Object localObject = (FlowableReplay.InnerSubscription[])this.subscribers.get();
        long l2 = this.maxChildRequested;
        int k = localObject.length;
        j = 0;
        long l1 = l2;
        while (j < k)
        {
          l1 = Math.max(l1, localObject[j].totalRequested.get());
          j++;
        }
        long l3 = this.maxUpstreamRequested;
        localObject = this.subscription;
        l2 = l1 - l2;
        if (l2 != 0L)
        {
          this.maxChildRequested = l1;
          if (localObject != null)
          {
            if (l3 != 0L)
            {
              this.maxUpstreamRequested = 0L;
              ((Subscription)localObject).request(l3 + l2);
            }
            else
            {
              ((Subscription)localObject).request(l2);
            }
          }
          else
          {
            l2 = l3 + l2;
            l1 = l2;
            if (l2 < 0L) {
              l1 = Long.MAX_VALUE;
            }
            this.maxUpstreamRequested = l1;
          }
        }
        else if ((l3 != 0L) && (localObject != null))
        {
          this.maxUpstreamRequested = 0L;
          ((Subscription)localObject).request(l3);
        }
        j = this.management.addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.buffer.complete();
        for (FlowableReplay.InnerSubscription localInnerSubscription : (FlowableReplay.InnerSubscription[])this.subscribers.getAndSet(TERMINATED)) {
          this.buffer.replay(localInnerSubscription);
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.done)
      {
        this.done = true;
        this.buffer.error(paramThrowable);
        for (FlowableReplay.InnerSubscription localInnerSubscription : (FlowableReplay.InnerSubscription[])this.subscribers.getAndSet(TERMINATED)) {
          this.buffer.replay(localInnerSubscription);
        }
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        this.buffer.next(paramT);
        for (FlowableReplay.InnerSubscription localInnerSubscription : (FlowableReplay.InnerSubscription[])this.subscribers.get()) {
          this.buffer.replay(localInnerSubscription);
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.subscription, paramSubscription))
      {
        this.subscription = paramSubscription;
        manageRequests();
        for (FlowableReplay.InnerSubscription localInnerSubscription : (FlowableReplay.InnerSubscription[])this.subscribers.get()) {
          this.buffer.replay(localInnerSubscription);
        }
      }
    }
    
    void remove(FlowableReplay.InnerSubscription<T> paramInnerSubscription)
    {
      FlowableReplay.InnerSubscription[] arrayOfInnerSubscription2;
      FlowableReplay.InnerSubscription[] arrayOfInnerSubscription1;
      do
      {
        arrayOfInnerSubscription2 = (FlowableReplay.InnerSubscription[])this.subscribers.get();
        int m = arrayOfInnerSubscription2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerSubscription2[i].equals(paramInnerSubscription))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerSubscription1 = EMPTY;
        }
        else
        {
          arrayOfInnerSubscription1 = new FlowableReplay.InnerSubscription[m - 1];
          System.arraycopy(arrayOfInnerSubscription2, 0, arrayOfInnerSubscription1, 0, j);
          System.arraycopy(arrayOfInnerSubscription2, j + 1, arrayOfInnerSubscription1, j, m - j - 1);
        }
      } while (!this.subscribers.compareAndSet(arrayOfInnerSubscription2, arrayOfInnerSubscription1));
    }
  }
  
  static final class SizeAndTimeBoundReplayBuffer<T>
    extends FlowableReplay.BoundedReplayBuffer<T>
  {
    private static final long serialVersionUID = 3457957419649567404L;
    final int limit;
    final long maxAge;
    final Scheduler scheduler;
    final TimeUnit unit;
    
    SizeAndTimeBoundReplayBuffer(int paramInt, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.scheduler = paramScheduler;
      this.limit = paramInt;
      this.maxAge = paramLong;
      this.unit = paramTimeUnit;
    }
    
    Object enterTransform(Object paramObject)
    {
      return new Timed(paramObject, this.scheduler.now(this.unit), this.unit);
    }
    
    Object leaveTransform(Object paramObject)
    {
      return ((Timed)paramObject).value();
    }
    
    void truncate()
    {
      long l2 = this.scheduler.now(this.unit);
      long l1 = this.maxAge;
      Object localObject2 = (FlowableReplay.Node)get();
      Object localObject1 = (FlowableReplay.Node)((FlowableReplay.Node)localObject2).get();
      int i = 0;
      while (localObject1 != null)
      {
        FlowableReplay.Node localNode;
        if (this.size > this.limit)
        {
          i++;
          this.size -= 1;
          localNode = (FlowableReplay.Node)((FlowableReplay.Node)localObject1).get();
          localObject2 = localObject1;
          localObject1 = localNode;
        }
        else
        {
          if (((Timed)((FlowableReplay.Node)localObject1).value).time() > l2 - l1) {
            break;
          }
          i++;
          this.size -= 1;
          localNode = (FlowableReplay.Node)((FlowableReplay.Node)localObject1).get();
          localObject2 = localObject1;
          localObject1 = localNode;
        }
      }
      if (i != 0) {
        setFirst((FlowableReplay.Node)localObject2);
      }
    }
    
    void truncateFinal()
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.maxAge;
      Object localObject2 = (FlowableReplay.Node)get();
      Object localObject1 = (FlowableReplay.Node)((FlowableReplay.Node)localObject2).get();
      int i = 0;
      while ((localObject1 != null) && (this.size > 1) && (((Timed)((FlowableReplay.Node)localObject1).value).time() <= l1 - l2))
      {
        i++;
        this.size -= 1;
        FlowableReplay.Node localNode = (FlowableReplay.Node)((FlowableReplay.Node)localObject1).get();
        localObject2 = localObject1;
        localObject1 = localNode;
      }
      if (i != 0) {
        setFirst((FlowableReplay.Node)localObject2);
      }
    }
  }
  
  static final class SizeBoundReplayBuffer<T>
    extends FlowableReplay.BoundedReplayBuffer<T>
  {
    private static final long serialVersionUID = -5898283885385201806L;
    final int limit;
    
    SizeBoundReplayBuffer(int paramInt)
    {
      this.limit = paramInt;
    }
    
    void truncate()
    {
      if (this.size > this.limit) {
        removeFirst();
      }
    }
  }
  
  static final class UnboundedReplayBuffer<T>
    extends ArrayList<Object>
    implements FlowableReplay.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 7063189396499112664L;
    volatile int size;
    
    UnboundedReplayBuffer(int paramInt)
    {
      super();
    }
    
    public void complete()
    {
      add(NotificationLite.complete());
      this.size += 1;
    }
    
    public void error(Throwable paramThrowable)
    {
      add(NotificationLite.error(paramThrowable));
      this.size += 1;
    }
    
    public void next(T paramT)
    {
      add(NotificationLite.next(paramT));
      this.size += 1;
    }
    
    /* Error */
    public void replay(FlowableReplay.InnerSubscription<T> paramInnerSubscription)
    {
      // Byte code:
      //   0: aload_1
      //   1: monitorenter
      //   2: aload_1
      //   3: getfield 56	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   6: ifeq +11 -> 17
      //   9: aload_1
      //   10: iconst_1
      //   11: putfield 59	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   14: aload_1
      //   15: monitorexit
      //   16: return
      //   17: aload_1
      //   18: iconst_1
      //   19: putfield 56	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   22: aload_1
      //   23: monitorexit
      //   24: aload_1
      //   25: getfield 63	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:child	Lorg/reactivestreams/Subscriber;
      //   28: astore 11
      //   30: aload_1
      //   31: invokevirtual 67	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:isDisposed	()Z
      //   34: ifeq +4 -> 38
      //   37: return
      //   38: aload_0
      //   39: getfield 34	io/reactivex/internal/operators/flowable/FlowableReplay$UnboundedReplayBuffer:size	I
      //   42: istore_3
      //   43: aload_1
      //   44: invokevirtual 70	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	()Ljava/lang/Object;
      //   47: checkcast 72	java/lang/Integer
      //   50: astore 12
      //   52: aload 12
      //   54: ifnull +12 -> 66
      //   57: aload 12
      //   59: invokevirtual 76	java/lang/Integer:intValue	()I
      //   62: istore_2
      //   63: goto +5 -> 68
      //   66: iconst_0
      //   67: istore_2
      //   68: aload_1
      //   69: invokevirtual 80	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:get	()J
      //   72: lstore 8
      //   74: lload 8
      //   76: lstore 6
      //   78: lconst_0
      //   79: lstore 4
      //   81: lload 6
      //   83: lconst_0
      //   84: lcmp
      //   85: ifeq +93 -> 178
      //   88: iload_2
      //   89: iload_3
      //   90: if_icmpge +88 -> 178
      //   93: aload_0
      //   94: iload_2
      //   95: invokevirtual 83	io/reactivex/internal/operators/flowable/FlowableReplay$UnboundedReplayBuffer:get	(I)Ljava/lang/Object;
      //   98: astore 13
      //   100: aload 13
      //   102: aload 11
      //   104: invokestatic 87	io/reactivex/internal/util/NotificationLite:accept	(Ljava/lang/Object;Lorg/reactivestreams/Subscriber;)Z
      //   107: istore 10
      //   109: iload 10
      //   111: ifeq +4 -> 115
      //   114: return
      //   115: aload_1
      //   116: invokevirtual 67	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:isDisposed	()Z
      //   119: ifeq +4 -> 123
      //   122: return
      //   123: iinc 2 1
      //   126: lload 6
      //   128: lconst_1
      //   129: lsub
      //   130: lstore 6
      //   132: lload 4
      //   134: lconst_1
      //   135: ladd
      //   136: lstore 4
      //   138: goto -57 -> 81
      //   141: astore 12
      //   143: aload 12
      //   145: invokestatic 92	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   148: aload_1
      //   149: invokevirtual 95	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:dispose	()V
      //   152: aload 13
      //   154: invokestatic 98	io/reactivex/internal/util/NotificationLite:isError	(Ljava/lang/Object;)Z
      //   157: ifne +20 -> 177
      //   160: aload 13
      //   162: invokestatic 101	io/reactivex/internal/util/NotificationLite:isComplete	(Ljava/lang/Object;)Z
      //   165: ifne +12 -> 177
      //   168: aload 11
      //   170: aload 12
      //   172: invokeinterface 106 2 0
      //   177: return
      //   178: lload 4
      //   180: lconst_0
      //   181: lcmp
      //   182: ifeq +27 -> 209
      //   185: aload_1
      //   186: iload_2
      //   187: invokestatic 110	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   190: putfield 113	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:index	Ljava/lang/Object;
      //   193: lload 8
      //   195: ldc2_w 114
      //   198: lcmp
      //   199: ifeq +10 -> 209
      //   202: aload_1
      //   203: lload 4
      //   205: invokevirtual 119	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:produced	(J)J
      //   208: pop2
      //   209: aload_1
      //   210: monitorenter
      //   211: aload_1
      //   212: getfield 59	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   215: ifne +11 -> 226
      //   218: aload_1
      //   219: iconst_0
      //   220: putfield 56	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:emitting	Z
      //   223: aload_1
      //   224: monitorexit
      //   225: return
      //   226: aload_1
      //   227: iconst_0
      //   228: putfield 59	io/reactivex/internal/operators/flowable/FlowableReplay$InnerSubscription:missed	Z
      //   231: aload_1
      //   232: monitorexit
      //   233: goto -203 -> 30
      //   236: astore 11
      //   238: aload_1
      //   239: monitorexit
      //   240: aload 11
      //   242: athrow
      //   243: astore 11
      //   245: aload_1
      //   246: monitorexit
      //   247: aload 11
      //   249: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	250	0	this	UnboundedReplayBuffer
      //   0	250	1	paramInnerSubscription	FlowableReplay.InnerSubscription<T>
      //   62	125	2	i	int
      //   42	49	3	j	int
      //   79	125	4	l1	long
      //   76	55	6	l2	long
      //   72	122	8	l3	long
      //   107	3	10	bool	boolean
      //   28	141	11	localSubscriber	Subscriber
      //   236	5	11	localObject1	Object
      //   243	5	11	localObject2	Object
      //   50	8	12	localInteger	Integer
      //   141	30	12	localThrowable	Throwable
      //   98	63	13	localObject3	Object
      // Exception table:
      //   from	to	target	type
      //   100	109	141	java/lang/Throwable
      //   211	225	236	finally
      //   226	233	236	finally
      //   238	240	236	finally
      //   2	16	243	finally
      //   17	24	243	finally
      //   245	247	243	finally
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableReplay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */