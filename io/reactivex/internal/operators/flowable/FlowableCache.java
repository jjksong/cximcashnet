package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.LinkedArrayList;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCache<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final AtomicBoolean once;
  final CacheState<T> state;
  
  public FlowableCache(Flowable<T> paramFlowable, int paramInt)
  {
    super(paramFlowable);
    this.state = new CacheState(paramFlowable, paramInt);
    this.once = new AtomicBoolean();
  }
  
  int cachedEventCount()
  {
    return this.state.size();
  }
  
  boolean hasSubscribers()
  {
    boolean bool;
    if (((ReplaySubscription[])this.state.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  boolean isConnected()
  {
    return this.state.isConnected;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    ReplaySubscription localReplaySubscription = new ReplaySubscription(paramSubscriber, this.state);
    this.state.addChild(localReplaySubscription);
    paramSubscriber.onSubscribe(localReplaySubscription);
    if ((!this.once.get()) && (this.once.compareAndSet(false, true))) {
      this.state.connect();
    }
  }
  
  static final class CacheState<T>
    extends LinkedArrayList
    implements Subscriber<T>
  {
    static final FlowableCache.ReplaySubscription[] EMPTY = new FlowableCache.ReplaySubscription[0];
    static final FlowableCache.ReplaySubscription[] TERMINATED = new FlowableCache.ReplaySubscription[0];
    final AtomicReference<Subscription> connection = new AtomicReference();
    volatile boolean isConnected;
    final Flowable<? extends T> source;
    boolean sourceDone;
    final AtomicReference<FlowableCache.ReplaySubscription<T>[]> subscribers;
    
    CacheState(Flowable<? extends T> paramFlowable, int paramInt)
    {
      super();
      this.source = paramFlowable;
      this.subscribers = new AtomicReference(EMPTY);
    }
    
    public void addChild(FlowableCache.ReplaySubscription<T> paramReplaySubscription)
    {
      FlowableCache.ReplaySubscription[] arrayOfReplaySubscription1;
      FlowableCache.ReplaySubscription[] arrayOfReplaySubscription2;
      do
      {
        arrayOfReplaySubscription1 = (FlowableCache.ReplaySubscription[])this.subscribers.get();
        if (arrayOfReplaySubscription1 == TERMINATED) {
          return;
        }
        int i = arrayOfReplaySubscription1.length;
        arrayOfReplaySubscription2 = new FlowableCache.ReplaySubscription[i + 1];
        System.arraycopy(arrayOfReplaySubscription1, 0, arrayOfReplaySubscription2, 0, i);
        arrayOfReplaySubscription2[i] = paramReplaySubscription;
      } while (!this.subscribers.compareAndSet(arrayOfReplaySubscription1, arrayOfReplaySubscription2));
    }
    
    public void connect()
    {
      this.source.subscribe(this);
      this.isConnected = true;
    }
    
    public void onComplete()
    {
      if (!this.sourceDone)
      {
        this.sourceDone = true;
        add(NotificationLite.complete());
        SubscriptionHelper.cancel(this.connection);
        FlowableCache.ReplaySubscription[] arrayOfReplaySubscription = (FlowableCache.ReplaySubscription[])this.subscribers.getAndSet(TERMINATED);
        int j = arrayOfReplaySubscription.length;
        for (int i = 0; i < j; i++) {
          arrayOfReplaySubscription[i].replay();
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.sourceDone)
      {
        this.sourceDone = true;
        add(NotificationLite.error(paramThrowable));
        SubscriptionHelper.cancel(this.connection);
        paramThrowable = (FlowableCache.ReplaySubscription[])this.subscribers.getAndSet(TERMINATED);
        int j = paramThrowable.length;
        for (int i = 0; i < j; i++) {
          paramThrowable[i].replay();
        }
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!this.sourceDone)
      {
        add(NotificationLite.next(paramT));
        paramT = (FlowableCache.ReplaySubscription[])this.subscribers.get();
        int j = paramT.length;
        for (int i = 0; i < j; i++) {
          paramT[i].replay();
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this.connection, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void removeChild(FlowableCache.ReplaySubscription<T> paramReplaySubscription)
    {
      FlowableCache.ReplaySubscription[] arrayOfReplaySubscription2;
      FlowableCache.ReplaySubscription[] arrayOfReplaySubscription1;
      do
      {
        arrayOfReplaySubscription2 = (FlowableCache.ReplaySubscription[])this.subscribers.get();
        int m = arrayOfReplaySubscription2.length;
        if (m == 0) {
          return;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfReplaySubscription2[i].equals(paramReplaySubscription))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          paramReplaySubscription = EMPTY;
          return;
        }
        arrayOfReplaySubscription1 = new FlowableCache.ReplaySubscription[m - 1];
        System.arraycopy(arrayOfReplaySubscription2, 0, arrayOfReplaySubscription1, 0, j);
        System.arraycopy(arrayOfReplaySubscription2, j + 1, arrayOfReplaySubscription1, j, m - j - 1);
      } while (!this.subscribers.compareAndSet(arrayOfReplaySubscription2, arrayOfReplaySubscription1));
    }
  }
  
  static final class ReplaySubscription<T>
    extends AtomicInteger
    implements Subscription
  {
    private static final long CANCELLED = -1L;
    private static final long serialVersionUID = -2557562030197141021L;
    final Subscriber<? super T> child;
    Object[] currentBuffer;
    int currentIndexInBuffer;
    int index;
    final AtomicLong requested;
    final FlowableCache.CacheState<T> state;
    
    ReplaySubscription(Subscriber<? super T> paramSubscriber, FlowableCache.CacheState<T> paramCacheState)
    {
      this.child = paramSubscriber;
      this.state = paramCacheState;
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      if (this.requested.getAndSet(-1L) != -1L) {
        this.state.removeChild(this);
      }
    }
    
    public void replay()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.child;
      AtomicLong localAtomicLong = this.requested;
      int j = 1;
      for (;;)
      {
        long l = localAtomicLong.get();
        if (l < 0L) {
          return;
        }
        int i2 = this.state.size();
        if (i2 != 0)
        {
          Object localObject2 = this.currentBuffer;
          Object localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = this.state.head();
            this.currentBuffer = ((Object[])localObject1);
          }
          int i1 = localObject1.length - 1;
          int m = this.index;
          int i = this.currentIndexInBuffer;
          int k = 0;
          while ((m < i2) && (l > 0L))
          {
            if (localAtomicLong.get() == -1L) {
              return;
            }
            localObject2 = localObject1;
            int n = i;
            if (i == i1)
            {
              localObject2 = (Object[])localObject1[i1];
              n = 0;
            }
            if (NotificationLite.accept(localObject2[n], localSubscriber)) {
              return;
            }
            i = n + 1;
            m++;
            l -= 1L;
            k++;
            localObject1 = localObject2;
          }
          if (localAtomicLong.get() == -1L) {
            return;
          }
          if (l == 0L)
          {
            localObject2 = localObject1[i];
            if (NotificationLite.isComplete(localObject2))
            {
              localSubscriber.onComplete();
              return;
            }
            if (NotificationLite.isError(localObject2))
            {
              localSubscriber.onError(NotificationLite.getError(localObject2));
              return;
            }
          }
          if (k != 0) {
            BackpressureHelper.producedCancel(localAtomicLong, k);
          }
          this.index = m;
          this.currentIndexInBuffer = i;
          this.currentBuffer = ((Object[])localObject1);
        }
        j = addAndGet(-j);
        if (j == 0) {
          return;
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        long l2;
        long l1;
        do
        {
          l2 = this.requested.get();
          if (l2 == -1L) {
            return;
          }
          l1 = BackpressureHelper.addCap(l2, paramLong);
        } while (!this.requested.compareAndSet(l2, l1));
        replay();
        return;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */