package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.processors.UnicastProcessor;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableGroupJoin<TLeft, TRight, TLeftEnd, TRightEnd, R>
  extends AbstractFlowableWithUpstream<TLeft, R>
{
  final Function<? super TLeft, ? extends Publisher<TLeftEnd>> leftEnd;
  final Publisher<? extends TRight> other;
  final BiFunction<? super TLeft, ? super Flowable<TRight>, ? extends R> resultSelector;
  final Function<? super TRight, ? extends Publisher<TRightEnd>> rightEnd;
  
  public FlowableGroupJoin(Publisher<TLeft> paramPublisher, Publisher<? extends TRight> paramPublisher1, Function<? super TLeft, ? extends Publisher<TLeftEnd>> paramFunction, Function<? super TRight, ? extends Publisher<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super Flowable<TRight>, ? extends R> paramBiFunction)
  {
    super(paramPublisher);
    this.other = paramPublisher1;
    this.leftEnd = paramFunction;
    this.rightEnd = paramFunction1;
    this.resultSelector = paramBiFunction;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    GroupJoinSubscription localGroupJoinSubscription = new GroupJoinSubscription(paramSubscriber, this.leftEnd, this.rightEnd, this.resultSelector);
    paramSubscriber.onSubscribe(localGroupJoinSubscription);
    LeftRightSubscriber localLeftRightSubscriber = new LeftRightSubscriber(localGroupJoinSubscription, true);
    localGroupJoinSubscription.disposables.add(localLeftRightSubscriber);
    paramSubscriber = new LeftRightSubscriber(localGroupJoinSubscription, false);
    localGroupJoinSubscription.disposables.add(paramSubscriber);
    this.source.subscribe(localLeftRightSubscriber);
    this.other.subscribe(paramSubscriber);
  }
  
  static final class GroupJoinSubscription<TLeft, TRight, TLeftEnd, TRightEnd, R>
    extends AtomicInteger
    implements Subscription, FlowableGroupJoin.JoinSupport
  {
    static final Integer LEFT_CLOSE = Integer.valueOf(3);
    static final Integer LEFT_VALUE = Integer.valueOf(1);
    static final Integer RIGHT_CLOSE = Integer.valueOf(4);
    static final Integer RIGHT_VALUE = Integer.valueOf(2);
    private static final long serialVersionUID = -6071216598687999801L;
    final AtomicInteger active;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    final CompositeDisposable disposables;
    final AtomicReference<Throwable> error;
    final Function<? super TLeft, ? extends Publisher<TLeftEnd>> leftEnd;
    int leftIndex;
    final Map<Integer, UnicastProcessor<TRight>> lefts;
    final SpscLinkedArrayQueue<Object> queue;
    final AtomicLong requested;
    final BiFunction<? super TLeft, ? super Flowable<TRight>, ? extends R> resultSelector;
    final Function<? super TRight, ? extends Publisher<TRightEnd>> rightEnd;
    int rightIndex;
    final Map<Integer, TRight> rights;
    
    GroupJoinSubscription(Subscriber<? super R> paramSubscriber, Function<? super TLeft, ? extends Publisher<TLeftEnd>> paramFunction, Function<? super TRight, ? extends Publisher<TRightEnd>> paramFunction1, BiFunction<? super TLeft, ? super Flowable<TRight>, ? extends R> paramBiFunction)
    {
      this.actual = paramSubscriber;
      this.requested = new AtomicLong();
      this.disposables = new CompositeDisposable();
      this.queue = new SpscLinkedArrayQueue(Flowable.bufferSize());
      this.lefts = new LinkedHashMap();
      this.rights = new LinkedHashMap();
      this.error = new AtomicReference();
      this.leftEnd = paramFunction;
      this.rightEnd = paramFunction1;
      this.resultSelector = paramBiFunction;
      this.active = new AtomicInteger(2);
    }
    
    public void cancel()
    {
      if (this.cancelled) {
        return;
      }
      this.cancelled = true;
      cancelAll();
      if (getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
    
    void cancelAll()
    {
      this.disposables.dispose();
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Object localObject1 = this.queue;
      Subscriber localSubscriber = this.actual;
      int i = 1;
      for (;;)
      {
        if (this.cancelled)
        {
          ((SpscLinkedArrayQueue)localObject1).clear();
          return;
        }
        if ((Throwable)this.error.get() != null)
        {
          ((SpscLinkedArrayQueue)localObject1).clear();
          cancelAll();
          errorAll(localSubscriber);
          return;
        }
        int j;
        if (this.active.get() == 0) {
          j = 1;
        } else {
          j = 0;
        }
        Object localObject3 = (Integer)((SpscLinkedArrayQueue)localObject1).poll();
        int k;
        if (localObject3 == null) {
          k = 1;
        } else {
          k = 0;
        }
        if ((j != 0) && (k != 0))
        {
          localObject1 = this.lefts.values().iterator();
          while (((Iterator)localObject1).hasNext()) {
            ((UnicastProcessor)((Iterator)localObject1).next()).onComplete();
          }
          this.lefts.clear();
          this.rights.clear();
          this.disposables.dispose();
          localSubscriber.onComplete();
          return;
        }
        if (k != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          Object localObject2 = ((SpscLinkedArrayQueue)localObject1).poll();
          FlowableGroupJoin.LeftRightEndSubscriber localLeftRightEndSubscriber2;
          if (localObject3 == LEFT_VALUE)
          {
            localObject3 = UnicastProcessor.create();
            j = this.leftIndex;
            this.leftIndex = (j + 1);
            this.lefts.put(Integer.valueOf(j), localObject3);
            try
            {
              Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.leftEnd.apply(localObject2), "The leftEnd returned a null Publisher");
              localLeftRightEndSubscriber2 = new FlowableGroupJoin.LeftRightEndSubscriber(this, true, j);
              this.disposables.add(localLeftRightEndSubscriber2);
              localPublisher.subscribe(localLeftRightEndSubscriber2);
              if ((Throwable)this.error.get() != null)
              {
                ((SpscLinkedArrayQueue)localObject1).clear();
                cancelAll();
                errorAll(localSubscriber);
                return;
              }
              try
              {
                localObject2 = ObjectHelper.requireNonNull(this.resultSelector.apply(localObject2, localObject3), "The resultSelector returned a null value");
                if (this.requested.get() != 0L)
                {
                  localSubscriber.onNext(localObject2);
                  BackpressureHelper.produced(this.requested, 1L);
                  localObject2 = this.rights.values().iterator();
                  while (((Iterator)localObject2).hasNext()) {
                    ((UnicastProcessor)localObject3).onNext(((Iterator)localObject2).next());
                  }
                }
                fail(new MissingBackpressureException("Could not emit value due to lack of requests"), localSubscriber, (SimpleQueue)localObject1);
                return;
              }
              catch (Throwable localThrowable1)
              {
                fail(localThrowable1, localSubscriber, (SimpleQueue)localObject1);
                return;
              }
              if (localObject3 != RIGHT_VALUE) {
                break label643;
              }
            }
            catch (Throwable localThrowable2)
            {
              fail(localThrowable2, localSubscriber, (SimpleQueue)localObject1);
              return;
            }
          }
          else
          {
            j = this.rightIndex;
            this.rightIndex = (j + 1);
            this.rights.put(Integer.valueOf(j), localThrowable2);
            try
            {
              localObject3 = (Publisher)ObjectHelper.requireNonNull(this.rightEnd.apply(localThrowable2), "The rightEnd returned a null Publisher");
              localLeftRightEndSubscriber2 = new FlowableGroupJoin.LeftRightEndSubscriber(this, false, j);
              this.disposables.add(localLeftRightEndSubscriber2);
              ((Publisher)localObject3).subscribe(localLeftRightEndSubscriber2);
              if ((Throwable)this.error.get() != null)
              {
                ((SpscLinkedArrayQueue)localObject1).clear();
                cancelAll();
                errorAll(localSubscriber);
                return;
              }
              localObject3 = this.lefts.values().iterator();
              while (((Iterator)localObject3).hasNext()) {
                ((UnicastProcessor)((Iterator)localObject3).next()).onNext(localThrowable2);
              }
              if (localObject3 != LEFT_CLOSE) {
                break label703;
              }
            }
            catch (Throwable localThrowable3)
            {
              fail(localThrowable3, localSubscriber, (SimpleQueue)localObject1);
              return;
            }
            label643:
            FlowableGroupJoin.LeftRightEndSubscriber localLeftRightEndSubscriber1 = (FlowableGroupJoin.LeftRightEndSubscriber)localThrowable3;
            localObject3 = (UnicastProcessor)this.lefts.remove(Integer.valueOf(localLeftRightEndSubscriber1.index));
            this.disposables.remove(localLeftRightEndSubscriber1);
            if (localObject3 != null)
            {
              ((UnicastProcessor)localObject3).onComplete();
              continue;
              label703:
              if (localObject3 == RIGHT_CLOSE)
              {
                localLeftRightEndSubscriber1 = (FlowableGroupJoin.LeftRightEndSubscriber)localLeftRightEndSubscriber1;
                this.rights.remove(Integer.valueOf(localLeftRightEndSubscriber1.index));
                this.disposables.remove(localLeftRightEndSubscriber1);
              }
            }
          }
        }
      }
    }
    
    void errorAll(Subscriber<?> paramSubscriber)
    {
      Throwable localThrowable = ExceptionHelper.terminate(this.error);
      Iterator localIterator = this.lefts.values().iterator();
      while (localIterator.hasNext()) {
        ((UnicastProcessor)localIterator.next()).onError(localThrowable);
      }
      this.lefts.clear();
      this.rights.clear();
      paramSubscriber.onError(localThrowable);
    }
    
    void fail(Throwable paramThrowable, Subscriber<?> paramSubscriber, SimpleQueue<?> paramSimpleQueue)
    {
      Exceptions.throwIfFatal(paramThrowable);
      ExceptionHelper.addThrowable(this.error, paramThrowable);
      paramSimpleQueue.clear();
      cancelAll();
      errorAll(paramSubscriber);
    }
    
    public void innerClose(boolean paramBoolean, FlowableGroupJoin.LeftRightEndSubscriber paramLeftRightEndSubscriber)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_CLOSE;
        } else {
          localInteger = RIGHT_CLOSE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramLeftRightEndSubscriber);
        drain();
        return;
      }
      finally {}
    }
    
    public void innerCloseError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerComplete(FlowableGroupJoin.LeftRightSubscriber paramLeftRightSubscriber)
    {
      this.disposables.delete(paramLeftRightSubscriber);
      this.active.decrementAndGet();
      drain();
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (ExceptionHelper.addThrowable(this.error, paramThrowable))
      {
        this.active.decrementAndGet();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void innerValue(boolean paramBoolean, Object paramObject)
    {
      try
      {
        SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
        Integer localInteger;
        if (paramBoolean) {
          localInteger = LEFT_VALUE;
        } else {
          localInteger = RIGHT_VALUE;
        }
        localSpscLinkedArrayQueue.offer(localInteger, paramObject);
        drain();
        return;
      }
      finally {}
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this.requested, paramLong);
      }
    }
  }
  
  static abstract interface JoinSupport
  {
    public abstract void innerClose(boolean paramBoolean, FlowableGroupJoin.LeftRightEndSubscriber paramLeftRightEndSubscriber);
    
    public abstract void innerCloseError(Throwable paramThrowable);
    
    public abstract void innerComplete(FlowableGroupJoin.LeftRightSubscriber paramLeftRightSubscriber);
    
    public abstract void innerError(Throwable paramThrowable);
    
    public abstract void innerValue(boolean paramBoolean, Object paramObject);
  }
  
  static final class LeftRightEndSubscriber
    extends AtomicReference<Subscription>
    implements Subscriber<Object>, Disposable
  {
    private static final long serialVersionUID = 1883890389173668373L;
    final int index;
    final boolean isLeft;
    final FlowableGroupJoin.JoinSupport parent;
    
    LeftRightEndSubscriber(FlowableGroupJoin.JoinSupport paramJoinSupport, boolean paramBoolean, int paramInt)
    {
      this.parent = paramJoinSupport;
      this.isLeft = paramBoolean;
      this.index = paramInt;
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)get());
    }
    
    public void onComplete()
    {
      this.parent.innerClose(this.isLeft, this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerCloseError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (SubscriptionHelper.cancel(this)) {
        this.parent.innerClose(this.isLeft, this);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
  
  static final class LeftRightSubscriber
    extends AtomicReference<Subscription>
    implements Subscriber<Object>, Disposable
  {
    private static final long serialVersionUID = 1883890389173668373L;
    final boolean isLeft;
    final FlowableGroupJoin.JoinSupport parent;
    
    LeftRightSubscriber(FlowableGroupJoin.JoinSupport paramJoinSupport, boolean paramBoolean)
    {
      this.parent = paramJoinSupport;
      this.isLeft = paramBoolean;
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)get());
    }
    
    public void onComplete()
    {
      this.parent.innerComplete(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.parent.innerValue(this.isLeft, paramObject);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription)) {
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableGroupJoin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */