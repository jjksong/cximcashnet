package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscribers.FullArbiterSubscriber;
import io.reactivex.internal.subscriptions.FullArbiter;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTimeout<T, U, V>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Publisher<U> firstTimeoutIndicator;
  final Function<? super T, ? extends Publisher<V>> itemTimeoutIndicator;
  final Publisher<? extends T> other;
  
  public FlowableTimeout(Publisher<T> paramPublisher, Publisher<U> paramPublisher1, Function<? super T, ? extends Publisher<V>> paramFunction, Publisher<? extends T> paramPublisher2)
  {
    super(paramPublisher);
    this.firstTimeoutIndicator = paramPublisher1;
    this.itemTimeoutIndicator = paramFunction;
    this.other = paramPublisher2;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if (this.other == null) {
      this.source.subscribe(new TimeoutSubscriber(new SerializedSubscriber(paramSubscriber), this.firstTimeoutIndicator, this.itemTimeoutIndicator));
    } else {
      this.source.subscribe(new TimeoutOtherSubscriber(paramSubscriber, this.firstTimeoutIndicator, this.itemTimeoutIndicator, this.other));
    }
  }
  
  static abstract interface OnTimeout
  {
    public abstract void onError(Throwable paramThrowable);
    
    public abstract void timeout(long paramLong);
  }
  
  static final class TimeoutInnerSubscriber<T, U, V>
    extends DisposableSubscriber<Object>
  {
    boolean done;
    final long index;
    final FlowableTimeout.OnTimeout parent;
    
    TimeoutInnerSubscriber(FlowableTimeout.OnTimeout paramOnTimeout, long paramLong)
    {
      this.parent = paramOnTimeout;
      this.index = paramLong;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.timeout(this.index);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      if (this.done) {
        return;
      }
      this.done = true;
      cancel();
      this.parent.timeout(this.index);
    }
  }
  
  static final class TimeoutOtherSubscriber<T, U, V>
    implements Subscriber<T>, Disposable, FlowableTimeout.OnTimeout
  {
    final Subscriber<? super T> actual;
    final FullArbiter<T> arbiter;
    volatile boolean cancelled;
    boolean done;
    final Publisher<U> firstTimeoutIndicator;
    volatile long index;
    final Function<? super T, ? extends Publisher<V>> itemTimeoutIndicator;
    final Publisher<? extends T> other;
    Subscription s;
    final AtomicReference<Disposable> timeout = new AtomicReference();
    
    TimeoutOtherSubscriber(Subscriber<? super T> paramSubscriber, Publisher<U> paramPublisher, Function<? super T, ? extends Publisher<V>> paramFunction, Publisher<? extends T> paramPublisher1)
    {
      this.actual = paramSubscriber;
      this.firstTimeoutIndicator = paramPublisher;
      this.itemTimeoutIndicator = paramFunction;
      this.other = paramPublisher1;
      this.arbiter = new FullArbiter(paramSubscriber, this, 8);
    }
    
    public void dispose()
    {
      this.cancelled = true;
      this.s.cancel();
      DisposableHelper.dispose(this.timeout);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      dispose();
      this.arbiter.onComplete(this.s);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      dispose();
      this.arbiter.onError(paramThrowable, this.s);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      if (!this.arbiter.onNext(paramT, this.s)) {
        return;
      }
      Disposable localDisposable = (Disposable)this.timeout.get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      try
      {
        paramT = (Publisher)ObjectHelper.requireNonNull(this.itemTimeoutIndicator.apply(paramT), "The publisher returned is null");
        FlowableTimeout.TimeoutInnerSubscriber localTimeoutInnerSubscriber = new FlowableTimeout.TimeoutInnerSubscriber(this, l);
        if (this.timeout.compareAndSet(localDisposable, localTimeoutInnerSubscriber)) {
          paramT.subscribe(localTimeoutInnerSubscriber);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (!SubscriptionHelper.validate(this.s, paramSubscription)) {
        return;
      }
      this.s = paramSubscription;
      if (!this.arbiter.setSubscription(paramSubscription)) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      Publisher localPublisher = this.firstTimeoutIndicator;
      if (localPublisher != null)
      {
        paramSubscription = new FlowableTimeout.TimeoutInnerSubscriber(this, 0L);
        if (this.timeout.compareAndSet(null, paramSubscription))
        {
          localSubscriber.onSubscribe(this.arbiter);
          localPublisher.subscribe(paramSubscription);
        }
      }
      else
      {
        localSubscriber.onSubscribe(this.arbiter);
      }
    }
    
    public void timeout(long paramLong)
    {
      if (paramLong == this.index)
      {
        dispose();
        this.other.subscribe(new FullArbiterSubscriber(this.arbiter));
      }
    }
  }
  
  static final class TimeoutSubscriber<T, U, V>
    implements Subscriber<T>, Subscription, FlowableTimeout.OnTimeout
  {
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final Publisher<U> firstTimeoutIndicator;
    volatile long index;
    final Function<? super T, ? extends Publisher<V>> itemTimeoutIndicator;
    Subscription s;
    final AtomicReference<Disposable> timeout = new AtomicReference();
    
    TimeoutSubscriber(Subscriber<? super T> paramSubscriber, Publisher<U> paramPublisher, Function<? super T, ? extends Publisher<V>> paramFunction)
    {
      this.actual = paramSubscriber;
      this.firstTimeoutIndicator = paramPublisher;
      this.itemTimeoutIndicator = paramFunction;
    }
    
    public void cancel()
    {
      this.cancelled = true;
      this.s.cancel();
      DisposableHelper.dispose(this.timeout);
    }
    
    public void onComplete()
    {
      cancel();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      cancel();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      long l = this.index + 1L;
      this.index = l;
      this.actual.onNext(paramT);
      Disposable localDisposable = (Disposable)this.timeout.get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      try
      {
        Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.itemTimeoutIndicator.apply(paramT), "The publisher returned is null");
        paramT = new FlowableTimeout.TimeoutInnerSubscriber(this, l);
        if (this.timeout.compareAndSet(localDisposable, paramT)) {
          localPublisher.subscribe(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        cancel();
        this.actual.onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (!SubscriptionHelper.validate(this.s, paramSubscription)) {
        return;
      }
      this.s = paramSubscription;
      if (this.cancelled) {
        return;
      }
      paramSubscription = this.actual;
      Publisher localPublisher = this.firstTimeoutIndicator;
      if (localPublisher != null)
      {
        FlowableTimeout.TimeoutInnerSubscriber localTimeoutInnerSubscriber = new FlowableTimeout.TimeoutInnerSubscriber(this, 0L);
        if (this.timeout.compareAndSet(null, localTimeoutInnerSubscriber))
        {
          paramSubscription.onSubscribe(this);
          localPublisher.subscribe(localTimeoutInnerSubscriber);
        }
      }
      else
      {
        paramSubscription.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
    
    public void timeout(long paramLong)
    {
      if (paramLong == this.index)
      {
        cancel();
        this.actual.onError(new TimeoutException());
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTimeout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */