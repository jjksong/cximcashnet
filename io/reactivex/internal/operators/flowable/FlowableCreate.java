package io.reactivex.internal.operators.flowable;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Cancellable;
import io.reactivex.internal.disposables.CancellableDisposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCreate<T>
  extends Flowable<T>
{
  final BackpressureStrategy backpressure;
  final FlowableOnSubscribe<T> source;
  
  public FlowableCreate(FlowableOnSubscribe<T> paramFlowableOnSubscribe, BackpressureStrategy paramBackpressureStrategy)
  {
    this.source = paramFlowableOnSubscribe;
    this.backpressure = paramBackpressureStrategy;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Object localObject;
    switch (this.backpressure)
    {
    default: 
      localObject = new BufferAsyncEmitter(paramSubscriber, bufferSize());
      break;
    case ???: 
      localObject = new LatestAsyncEmitter(paramSubscriber);
      break;
    case ???: 
      localObject = new DropAsyncEmitter(paramSubscriber);
      break;
    case ???: 
      localObject = new ErrorAsyncEmitter(paramSubscriber);
      break;
    case ???: 
      localObject = new MissingEmitter(paramSubscriber);
    }
    paramSubscriber.onSubscribe((Subscription)localObject);
    try
    {
      this.source.subscribe((FlowableEmitter)localObject);
    }
    catch (Throwable paramSubscriber)
    {
      Exceptions.throwIfFatal(paramSubscriber);
      ((BaseEmitter)localObject).onError(paramSubscriber);
    }
  }
  
  static abstract class BaseEmitter<T>
    extends AtomicLong
    implements FlowableEmitter<T>, Subscription
  {
    private static final long serialVersionUID = 7326289992464377023L;
    final Subscriber<? super T> actual;
    final SequentialDisposable serial;
    
    BaseEmitter(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
      this.serial = new SequentialDisposable();
    }
    
    public final void cancel()
    {
      this.serial.dispose();
      onUnsubscribed();
    }
    
    public final boolean isCancelled()
    {
      return this.serial.isDisposed();
    }
    
    public void onComplete()
    {
      if (isCancelled()) {
        return;
      }
      try
      {
        this.actual.onComplete();
        return;
      }
      finally
      {
        this.serial.dispose();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      if (isCancelled())
      {
        RxJavaPlugins.onError((Throwable)localObject);
        return;
      }
      try
      {
        this.actual.onError((Throwable)localObject);
        return;
      }
      finally
      {
        this.serial.dispose();
      }
    }
    
    void onRequested() {}
    
    void onUnsubscribed() {}
    
    public final void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this, paramLong);
        onRequested();
      }
    }
    
    public final long requested()
    {
      return get();
    }
    
    public final FlowableEmitter<T> serialize()
    {
      return new FlowableCreate.SerializedEmitter(this);
    }
    
    public final void setCancellable(Cancellable paramCancellable)
    {
      setDisposable(new CancellableDisposable(paramCancellable));
    }
    
    public final void setDisposable(Disposable paramDisposable)
    {
      this.serial.update(paramDisposable);
    }
  }
  
  static final class BufferAsyncEmitter<T>
    extends FlowableCreate.BaseEmitter<T>
  {
    private static final long serialVersionUID = 2427151001689639875L;
    volatile boolean done;
    Throwable error;
    final SpscLinkedArrayQueue<T> queue;
    final AtomicInteger wip;
    
    BufferAsyncEmitter(Subscriber<? super T> paramSubscriber, int paramInt)
    {
      super();
      this.queue = new SpscLinkedArrayQueue(paramInt);
      this.wip = new AtomicInteger();
    }
    
    void drain()
    {
      if (this.wip.getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
      int i = 1;
      int j;
      do
      {
        long l2 = get();
        boolean bool1;
        Object localObject;
        for (long l1 = 0L; l1 != l2; l1 += 1L)
        {
          if (isCancelled())
          {
            localSpscLinkedArrayQueue.clear();
            return;
          }
          bool1 = this.done;
          localObject = localSpscLinkedArrayQueue.poll();
          if (localObject == null) {
            j = 1;
          } else {
            j = 0;
          }
          if ((bool1) && (j != 0))
          {
            localObject = this.error;
            if (localObject != null) {
              super.onError((Throwable)localObject);
            } else {
              super.onComplete();
            }
            return;
          }
          if (j != 0) {
            break;
          }
          localSubscriber.onNext(localObject);
        }
        if (l1 == l2)
        {
          if (isCancelled())
          {
            localSpscLinkedArrayQueue.clear();
            return;
          }
          bool1 = this.done;
          boolean bool2 = localSpscLinkedArrayQueue.isEmpty();
          if ((bool1) && (bool2))
          {
            localObject = this.error;
            if (localObject != null) {
              super.onError((Throwable)localObject);
            } else {
              super.onComplete();
            }
            return;
          }
        }
        if (l1 != 0L) {
          BackpressureHelper.produced(this, l1);
        }
        j = this.wip.addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.done) && (!isCancelled()))
      {
        Object localObject = paramThrowable;
        if (paramThrowable == null) {
          localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        this.error = ((Throwable)localObject);
        this.done = true;
        drain();
        return;
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if ((!this.done) && (!isCancelled()))
      {
        if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
          return;
        }
        this.queue.offer(paramT);
        drain();
        return;
      }
    }
    
    void onRequested()
    {
      drain();
    }
    
    void onUnsubscribed()
    {
      if (this.wip.getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
  }
  
  static final class DropAsyncEmitter<T>
    extends FlowableCreate.NoOverflowBaseAsyncEmitter<T>
  {
    private static final long serialVersionUID = 8360058422307496563L;
    
    DropAsyncEmitter(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    void onOverflow() {}
  }
  
  static final class ErrorAsyncEmitter<T>
    extends FlowableCreate.NoOverflowBaseAsyncEmitter<T>
  {
    private static final long serialVersionUID = 338953216916120960L;
    
    ErrorAsyncEmitter(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    void onOverflow()
    {
      onError(new MissingBackpressureException("create: could not emit value due to lack of requests"));
    }
  }
  
  static final class LatestAsyncEmitter<T>
    extends FlowableCreate.BaseEmitter<T>
  {
    private static final long serialVersionUID = 4023437720691792495L;
    volatile boolean done;
    Throwable error;
    final AtomicReference<T> queue = new AtomicReference();
    final AtomicInteger wip = new AtomicInteger();
    
    LatestAsyncEmitter(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    void drain()
    {
      if (this.wip.getAndIncrement() != 0) {
        return;
      }
      Object localObject1 = this.actual;
      AtomicReference localAtomicReference = this.queue;
      int i = 1;
      int j;
      do
      {
        long l2 = get();
        int k;
        boolean bool;
        for (long l1 = 0L;; l1 += 1L)
        {
          k = 0;
          if (l1 == l2) {
            break;
          }
          if (isCancelled())
          {
            localAtomicReference.lazySet(null);
            return;
          }
          bool = this.done;
          Object localObject2 = localAtomicReference.getAndSet(null);
          if (localObject2 == null) {
            j = 1;
          } else {
            j = 0;
          }
          if ((bool) && (j != 0))
          {
            localObject1 = this.error;
            if (localObject1 != null) {
              super.onError((Throwable)localObject1);
            } else {
              super.onComplete();
            }
            return;
          }
          if (j != 0) {
            break;
          }
          ((Subscriber)localObject1).onNext(localObject2);
        }
        if (l1 == l2)
        {
          if (isCancelled())
          {
            localAtomicReference.lazySet(null);
            return;
          }
          bool = this.done;
          j = k;
          if (localAtomicReference.get() == null) {
            j = 1;
          }
          if ((bool) && (j != 0))
          {
            localObject1 = this.error;
            if (localObject1 != null) {
              super.onError((Throwable)localObject1);
            } else {
              super.onComplete();
            }
            return;
          }
        }
        if (l1 != 0L) {
          BackpressureHelper.produced(this, l1);
        }
        j = this.wip.addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void onComplete()
    {
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.done) && (!isCancelled()))
      {
        if (paramThrowable == null) {
          onError(new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources."));
        }
        this.error = paramThrowable;
        this.done = true;
        drain();
        return;
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if ((!this.done) && (!isCancelled()))
      {
        if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
          return;
        }
        this.queue.set(paramT);
        drain();
        return;
      }
    }
    
    void onRequested()
    {
      drain();
    }
    
    void onUnsubscribed()
    {
      if (this.wip.getAndIncrement() == 0) {
        this.queue.lazySet(null);
      }
    }
  }
  
  static final class MissingEmitter<T>
    extends FlowableCreate.BaseEmitter<T>
  {
    private static final long serialVersionUID = 3776720187248809713L;
    
    MissingEmitter(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    public void onNext(T paramT)
    {
      if (isCancelled()) {
        return;
      }
      if (paramT != null)
      {
        this.actual.onNext(paramT);
        long l;
        do
        {
          l = get();
        } while ((l != 0L) && (!compareAndSet(l, l - 1L)));
        return;
      }
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
    }
  }
  
  static abstract class NoOverflowBaseAsyncEmitter<T>
    extends FlowableCreate.BaseEmitter<T>
  {
    private static final long serialVersionUID = 4127754106204442833L;
    
    NoOverflowBaseAsyncEmitter(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    public final void onNext(T paramT)
    {
      if (isCancelled()) {
        return;
      }
      if (paramT == null)
      {
        onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        return;
      }
      if (get() != 0L)
      {
        this.actual.onNext(paramT);
        BackpressureHelper.produced(this, 1L);
      }
      else
      {
        onOverflow();
      }
    }
    
    abstract void onOverflow();
  }
  
  static final class SerializedEmitter<T>
    extends AtomicInteger
    implements FlowableEmitter<T>
  {
    private static final long serialVersionUID = 4883307006032401862L;
    volatile boolean done;
    final FlowableCreate.BaseEmitter<T> emitter;
    final AtomicThrowable error;
    final SimplePlainQueue<T> queue;
    
    SerializedEmitter(FlowableCreate.BaseEmitter<T> paramBaseEmitter)
    {
      this.emitter = paramBaseEmitter;
      this.error = new AtomicThrowable();
      this.queue = new SpscLinkedArrayQueue(16);
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        drainLoop();
      }
    }
    
    void drainLoop()
    {
      FlowableCreate.BaseEmitter localBaseEmitter = this.emitter;
      SimplePlainQueue localSimplePlainQueue = this.queue;
      AtomicThrowable localAtomicThrowable = this.error;
      int i = 1;
      for (;;)
      {
        if (localBaseEmitter.isCancelled())
        {
          localSimplePlainQueue.clear();
          return;
        }
        if (localAtomicThrowable.get() != null)
        {
          localSimplePlainQueue.clear();
          localBaseEmitter.onError(localAtomicThrowable.terminate());
          return;
        }
        boolean bool = this.done;
        Object localObject = localSimplePlainQueue.poll();
        int j;
        if (localObject == null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((bool) && (j != 0))
        {
          localBaseEmitter.onComplete();
          return;
        }
        if (j != 0)
        {
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          localBaseEmitter.onNext(localObject);
        }
      }
    }
    
    public boolean isCancelled()
    {
      return this.emitter.isCancelled();
    }
    
    public void onComplete()
    {
      if ((!this.emitter.isCancelled()) && (!this.done))
      {
        this.done = true;
        drain();
        return;
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.emitter.isCancelled()) && (!this.done))
      {
        Object localObject = paramThrowable;
        if (paramThrowable == null) {
          localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        if (this.error.addThrowable((Throwable)localObject))
        {
          this.done = true;
          drain();
        }
        else
        {
          RxJavaPlugins.onError((Throwable)localObject);
        }
        return;
      }
      RxJavaPlugins.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if ((!this.emitter.isCancelled()) && (!this.done))
      {
        if (paramT == null)
        {
          onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
          return;
        }
        if ((get() == 0) && (compareAndSet(0, 1)))
        {
          this.emitter.onNext(paramT);
          if (decrementAndGet() != 0) {
            break label95;
          }
        }
        synchronized (this.queue)
        {
          ???.offer(paramT);
          if (getAndIncrement() != 0) {
            return;
          }
          label95:
          drainLoop();
          return;
        }
      }
    }
    
    public long requested()
    {
      return this.emitter.requested();
    }
    
    public FlowableEmitter<T> serialize()
    {
      return this;
    }
    
    public void setCancellable(Cancellable paramCancellable)
    {
      this.emitter.setCancellable(paramCancellable);
    }
    
    public void setDisposable(Disposable paramDisposable)
    {
      this.emitter.setDisposable(paramDisposable);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCreate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */