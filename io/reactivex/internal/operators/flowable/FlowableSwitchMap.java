package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSwitchMap<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final int bufferSize;
  final boolean delayErrors;
  final Function<? super T, ? extends Publisher<? extends R>> mapper;
  
  public FlowableSwitchMap(Publisher<T> paramPublisher, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.bufferSize = paramInt;
    this.delayErrors = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    if (FlowableScalarXMap.tryScalarXMapSubscribe(this.source, paramSubscriber, this.mapper)) {
      return;
    }
    this.source.subscribe(new SwitchMapSubscriber(paramSubscriber, this.mapper, this.bufferSize, this.delayErrors));
  }
  
  static final class SwitchMapInnerSubscriber<T, R>
    extends AtomicReference<Subscription>
    implements Subscriber<R>
  {
    private static final long serialVersionUID = 3837284832786408377L;
    final int bufferSize;
    volatile boolean done;
    int fusionMode;
    final long index;
    final FlowableSwitchMap.SwitchMapSubscriber<T, R> parent;
    volatile SimpleQueue<R> queue;
    
    SwitchMapInnerSubscriber(FlowableSwitchMap.SwitchMapSubscriber<T, R> paramSwitchMapSubscriber, long paramLong, int paramInt)
    {
      this.parent = paramSwitchMapSubscriber;
      this.index = paramLong;
      this.bufferSize = paramInt;
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      FlowableSwitchMap.SwitchMapSubscriber localSwitchMapSubscriber = this.parent;
      if (this.index == localSwitchMapSubscriber.unique)
      {
        this.done = true;
        localSwitchMapSubscriber.drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      FlowableSwitchMap.SwitchMapSubscriber localSwitchMapSubscriber = this.parent;
      if ((this.index == localSwitchMapSubscriber.unique) && (localSwitchMapSubscriber.error.addThrowable(paramThrowable)))
      {
        if (!localSwitchMapSubscriber.delayErrors) {
          localSwitchMapSubscriber.s.cancel();
        }
        this.done = true;
        localSwitchMapSubscriber.drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(R paramR)
    {
      FlowableSwitchMap.SwitchMapSubscriber localSwitchMapSubscriber = this.parent;
      if (this.index == localSwitchMapSubscriber.unique)
      {
        if ((this.fusionMode == 0) && (!this.queue.offer(paramR)))
        {
          onError(new MissingBackpressureException("Queue full?!"));
          return;
        }
        localSwitchMapSubscriber.drain();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.parent.drain();
            return;
          }
          if (i == 2)
          {
            this.fusionMode = i;
            this.queue = localQueueSubscription;
            paramSubscription.request(this.bufferSize);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.bufferSize);
        paramSubscription.request(this.bufferSize);
      }
    }
  }
  
  static final class SwitchMapSubscriber<T, R>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    static final FlowableSwitchMap.SwitchMapInnerSubscriber<Object, Object> CANCELLED = new FlowableSwitchMap.SwitchMapInnerSubscriber(null, -1L, 1);
    private static final long serialVersionUID = -3491074160481096299L;
    final AtomicReference<FlowableSwitchMap.SwitchMapInnerSubscriber<T, R>> active = new AtomicReference();
    final Subscriber<? super R> actual;
    final int bufferSize;
    volatile boolean cancelled;
    final boolean delayErrors;
    volatile boolean done;
    final AtomicThrowable error;
    final Function<? super T, ? extends Publisher<? extends R>> mapper;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    volatile long unique;
    
    static
    {
      CANCELLED.cancel();
    }
    
    SwitchMapSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.bufferSize = paramInt;
      this.delayErrors = paramBoolean;
      this.error = new AtomicThrowable();
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        disposeInner();
      }
    }
    
    void disposeInner()
    {
      FlowableSwitchMap.SwitchMapInnerSubscriber localSwitchMapInnerSubscriber2 = (FlowableSwitchMap.SwitchMapInnerSubscriber)this.active.get();
      FlowableSwitchMap.SwitchMapInnerSubscriber localSwitchMapInnerSubscriber1 = CANCELLED;
      if (localSwitchMapInnerSubscriber2 != localSwitchMapInnerSubscriber1)
      {
        localSwitchMapInnerSubscriber1 = (FlowableSwitchMap.SwitchMapInnerSubscriber)this.active.getAndSet(localSwitchMapInnerSubscriber1);
        if ((localSwitchMapInnerSubscriber1 != CANCELLED) && (localSwitchMapInnerSubscriber1 != null)) {
          localSwitchMapInnerSubscriber1.cancel();
        }
      }
    }
    
    void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = this.actual;
      int j = 1;
      int i;
      label545:
      do
      {
        do
        {
          FlowableSwitchMap.SwitchMapInnerSubscriber localSwitchMapInnerSubscriber;
          SimpleQueue localSimpleQueue;
          for (;;)
          {
            if (this.cancelled)
            {
              this.active.lazySet(null);
              return;
            }
            if (this.done) {
              if (this.delayErrors)
              {
                if (this.active.get() == null) {
                  if ((Throwable)this.error.get() != null) {
                    localSubscriber.onError(this.error.terminate());
                  } else {
                    localSubscriber.onComplete();
                  }
                }
              }
              else
              {
                if ((Throwable)this.error.get() != null)
                {
                  disposeInner();
                  localSubscriber.onError(this.error.terminate());
                  return;
                }
                if (this.active.get() == null)
                {
                  localSubscriber.onComplete();
                  return;
                }
              }
            }
            localSwitchMapInnerSubscriber = (FlowableSwitchMap.SwitchMapInnerSubscriber)this.active.get();
            if (localSwitchMapInnerSubscriber != null) {
              localSimpleQueue = localSwitchMapInnerSubscriber.queue;
            } else {
              localSimpleQueue = null;
            }
            if (localSimpleQueue == null) {
              break label545;
            }
            if (!localSwitchMapInnerSubscriber.done) {
              break;
            }
            if (!this.delayErrors)
            {
              if ((Throwable)this.error.get() != null)
              {
                disposeInner();
                localSubscriber.onError(this.error.terminate());
                return;
              }
              if (!localSimpleQueue.isEmpty()) {
                break;
              }
              this.active.compareAndSet(localSwitchMapInnerSubscriber, null);
            }
            else
            {
              if (!localSimpleQueue.isEmpty()) {
                break;
              }
              this.active.compareAndSet(localSwitchMapInnerSubscriber, null);
            }
          }
          long l2 = this.requested.get();
          for (long l1 = 0L;; l1 += 1L)
          {
            int k = 0;
            i = k;
            if (l1 == l2) {
              break;
            }
            if (this.cancelled) {
              return;
            }
            boolean bool = localSwitchMapInnerSubscriber.done;
            Object localObject2;
            try
            {
              Object localObject1 = localSimpleQueue.poll();
            }
            catch (Throwable localThrowable)
            {
              Exceptions.throwIfFatal(localThrowable);
              localSwitchMapInnerSubscriber.cancel();
              this.error.addThrowable(localThrowable);
              localObject2 = null;
              bool = true;
            }
            if (localObject2 == null) {
              i = 1;
            } else {
              i = 0;
            }
            if (localSwitchMapInnerSubscriber != this.active.get())
            {
              i = 1;
              break;
            }
            if (bool) {
              if (!this.delayErrors)
              {
                if ((Throwable)this.error.get() != null)
                {
                  localSubscriber.onError(this.error.terminate());
                  return;
                }
                if (i != 0)
                {
                  this.active.compareAndSet(localSwitchMapInnerSubscriber, null);
                  i = 1;
                  break;
                }
              }
              else if (i != 0)
              {
                this.active.compareAndSet(localSwitchMapInnerSubscriber, null);
                i = 1;
                break;
              }
            }
            if (i != 0)
            {
              i = k;
              break;
            }
            localSubscriber.onNext(localObject2);
          }
          if ((l1 != 0L) && (!this.cancelled))
          {
            if (l2 != Long.MAX_VALUE) {
              this.requested.addAndGet(-l1);
            }
            ((Subscription)localSwitchMapInnerSubscriber.get()).request(l1);
          }
        } while (i != 0);
        i = addAndGet(-j);
        j = i;
      } while (i != 0);
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if ((!this.done) && (this.error.addThrowable(paramThrowable)))
      {
        if (!this.delayErrors) {
          disposeInner();
        }
        this.done = true;
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.unique + 1L;
      this.unique = l;
      FlowableSwitchMap.SwitchMapInnerSubscriber localSwitchMapInnerSubscriber1 = (FlowableSwitchMap.SwitchMapInnerSubscriber)this.active.get();
      if (localSwitchMapInnerSubscriber1 != null) {
        localSwitchMapInnerSubscriber1.cancel();
      }
      try
      {
        paramT = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The publisher returned is null");
        FlowableSwitchMap.SwitchMapInnerSubscriber localSwitchMapInnerSubscriber2 = new FlowableSwitchMap.SwitchMapInnerSubscriber(this, l, this.bufferSize);
        do
        {
          localSwitchMapInnerSubscriber1 = (FlowableSwitchMap.SwitchMapInnerSubscriber)this.active.get();
          if (localSwitchMapInnerSubscriber1 == CANCELLED) {
            break;
          }
        } while (!this.active.compareAndSet(localSwitchMapInnerSubscriber1, localSwitchMapInnerSubscriber2));
        paramT.subscribe(localSwitchMapInnerSubscriber2);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        if (this.unique == 0L) {
          this.s.request(Long.MAX_VALUE);
        } else {
          drain();
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSwitchMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */