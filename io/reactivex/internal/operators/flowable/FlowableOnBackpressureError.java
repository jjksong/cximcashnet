package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnBackpressureError<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableOnBackpressureError(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new BackpressureErrorSubscriber(paramSubscriber));
  }
  
  static final class BackpressureErrorSubscriber<T>
    extends AtomicLong
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -3176480756392482682L;
    final Subscriber<? super T> actual;
    boolean done;
    Subscription s;
    
    BackpressureErrorSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (get() != 0L)
      {
        this.actual.onNext(paramT);
        BackpressureHelper.produced(this, 1L);
      }
      else
      {
        onError(new MissingBackpressureException("could not emit value due to lack of requests"));
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnBackpressureError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */