package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableObserveOn<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final boolean delayError;
  final int prefetch;
  final Scheduler scheduler;
  
  public FlowableObserveOn(Publisher<T> paramPublisher, Scheduler paramScheduler, boolean paramBoolean, int paramInt)
  {
    super(paramPublisher);
    this.scheduler = paramScheduler;
    this.delayError = paramBoolean;
    this.prefetch = paramInt;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      this.source.subscribe(new ObserveOnConditionalSubscriber((ConditionalSubscriber)paramSubscriber, localWorker, this.delayError, this.prefetch));
    } else {
      this.source.subscribe(new ObserveOnSubscriber(paramSubscriber, localWorker, this.delayError, this.prefetch));
    }
  }
  
  static abstract class BaseObserveOnSubscriber<T>
    extends BasicIntQueueSubscription<T>
    implements Runnable, Subscriber<T>
  {
    private static final long serialVersionUID = -8241002408341274697L;
    volatile boolean cancelled;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final int limit;
    boolean outputFused;
    final int prefetch;
    long produced;
    SimpleQueue<T> queue;
    final AtomicLong requested;
    Subscription s;
    int sourceMode;
    final Scheduler.Worker worker;
    
    BaseObserveOnSubscriber(Scheduler.Worker paramWorker, boolean paramBoolean, int paramInt)
    {
      this.worker = paramWorker;
      this.delayError = paramBoolean;
      this.prefetch = paramInt;
      this.requested = new AtomicLong();
      this.limit = (paramInt - (paramInt >> 2));
    }
    
    public final void cancel()
    {
      if (this.cancelled) {
        return;
      }
      this.cancelled = true;
      this.s.cancel();
      this.worker.dispose();
      if (getAndIncrement() == 0) {
        this.queue.clear();
      }
    }
    
    final boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<?> paramSubscriber)
    {
      if (this.cancelled)
      {
        clear();
        return true;
      }
      if (paramBoolean1)
      {
        Throwable localThrowable;
        if (this.delayError)
        {
          if (paramBoolean2)
          {
            localThrowable = this.error;
            if (localThrowable != null) {
              paramSubscriber.onError(localThrowable);
            } else {
              paramSubscriber.onComplete();
            }
            this.worker.dispose();
            return true;
          }
        }
        else
        {
          localThrowable = this.error;
          if (localThrowable != null)
          {
            clear();
            paramSubscriber.onError(localThrowable);
            this.worker.dispose();
            return true;
          }
          if (paramBoolean2)
          {
            paramSubscriber.onComplete();
            this.worker.dispose();
            return true;
          }
        }
      }
      return false;
    }
    
    public final void clear()
    {
      this.queue.clear();
    }
    
    public final boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public final void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        trySchedule();
      }
    }
    
    public final void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      trySchedule();
    }
    
    public final void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode == 2)
      {
        trySchedule();
        return;
      }
      if (!this.queue.offer(paramT))
      {
        this.s.cancel();
        this.error = new MissingBackpressureException("Queue is full?!");
        this.done = true;
      }
      trySchedule();
    }
    
    public final void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        trySchedule();
      }
    }
    
    public final int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
    
    public final void run()
    {
      if (this.outputFused) {
        runBackfused();
      } else if (this.sourceMode == 1) {
        runSync();
      } else {
        runAsync();
      }
    }
    
    abstract void runAsync();
    
    abstract void runBackfused();
    
    abstract void runSync();
    
    final void trySchedule()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      this.worker.schedule(this);
    }
  }
  
  static final class ObserveOnConditionalSubscriber<T>
    extends FlowableObserveOn.BaseObserveOnSubscriber<T>
  {
    private static final long serialVersionUID = 644624475404284533L;
    final ConditionalSubscriber<? super T> actual;
    long consumed;
    
    ObserveOnConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Scheduler.Worker paramWorker, boolean paramBoolean, int paramInt)
    {
      super(paramBoolean, paramInt);
      this.actual = paramConditionalSubscriber;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(7);
          if (i == 1)
          {
            this.sourceMode = 1;
            this.queue = localQueueSubscription;
            this.done = true;
            this.actual.onSubscribe(this);
            return;
          }
          if (i == 2)
          {
            this.sourceMode = 2;
            this.queue = localQueueSubscription;
            this.actual.onSubscribe(this);
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        this.actual.onSubscribe(this);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.queue.poll();
      if ((localObject != null) && (this.sourceMode != 1))
      {
        long l = this.consumed + 1L;
        if (l == this.limit)
        {
          this.consumed = 0L;
          this.s.request(l);
        }
        else
        {
          this.consumed = l;
        }
      }
      return (T)localObject;
    }
    
    void runAsync()
    {
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      long l2 = this.produced;
      long l1 = this.consumed;
      int i = 1;
      for (;;)
      {
        long l5 = this.requested.get();
        while (l2 != l5)
        {
          boolean bool2 = this.done;
          try
          {
            Object localObject = localSimpleQueue.poll();
            boolean bool1;
            if (localObject == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool2, bool1, localConditionalSubscriber)) {
              return;
            }
            if (!bool1)
            {
              long l3 = l2;
              if (localConditionalSubscriber.tryOnNext(localObject)) {
                l3 = l2 + 1L;
              }
              long l4 = l1 + 1L;
              l2 = l3;
              l1 = l4;
              if (l4 != this.limit) {
                continue;
              }
              this.s.request(l4);
              l1 = 0L;
              l2 = l3;
            }
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.s.cancel();
            localSimpleQueue.clear();
            localConditionalSubscriber.onError(localThrowable);
            this.worker.dispose();
            return;
          }
        }
        if ((l2 == l5) && (checkTerminated(this.done, localSimpleQueue.isEmpty(), localConditionalSubscriber))) {
          return;
        }
        int j = get();
        if (i == j)
        {
          this.produced = l2;
          this.consumed = l1;
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          i = j;
        }
      }
    }
    
    void runBackfused()
    {
      int i = 1;
      int j;
      do
      {
        if (this.cancelled) {
          return;
        }
        boolean bool = this.done;
        this.actual.onNext(null);
        if (bool)
        {
          Throwable localThrowable = this.error;
          if (localThrowable != null) {
            this.actual.onError(localThrowable);
          } else {
            this.actual.onComplete();
          }
          this.worker.dispose();
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void runSync()
    {
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      long l1 = this.produced;
      int i = 1;
      for (;;)
      {
        long l2 = this.requested.get();
        while (l1 != l2) {
          try
          {
            Object localObject = localSimpleQueue.poll();
            if (this.cancelled) {
              return;
            }
            if (localObject == null)
            {
              localConditionalSubscriber.onComplete();
              this.worker.dispose();
              return;
            }
            if (localConditionalSubscriber.tryOnNext(localObject)) {
              l1 += 1L;
            }
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.s.cancel();
            localConditionalSubscriber.onError(localThrowable);
            this.worker.dispose();
            return;
          }
        }
        if (this.cancelled) {
          return;
        }
        if (localThrowable.isEmpty())
        {
          localConditionalSubscriber.onComplete();
          this.worker.dispose();
          return;
        }
        int j = get();
        if (i == j)
        {
          this.produced = l1;
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          i = j;
        }
      }
    }
  }
  
  static final class ObserveOnSubscriber<T>
    extends FlowableObserveOn.BaseObserveOnSubscriber<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -4547113800637756442L;
    final Subscriber<? super T> actual;
    
    ObserveOnSubscriber(Subscriber<? super T> paramSubscriber, Scheduler.Worker paramWorker, boolean paramBoolean, int paramInt)
    {
      super(paramBoolean, paramInt);
      this.actual = paramSubscriber;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(7);
          if (i == 1)
          {
            this.sourceMode = 1;
            this.queue = localQueueSubscription;
            this.done = true;
            this.actual.onSubscribe(this);
            return;
          }
          if (i == 2)
          {
            this.sourceMode = 2;
            this.queue = localQueueSubscription;
            this.actual.onSubscribe(this);
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        this.actual.onSubscribe(this);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.queue.poll();
      if ((localObject != null) && (this.sourceMode != 1))
      {
        long l = this.produced + 1L;
        if (l == this.limit)
        {
          this.produced = 0L;
          this.s.request(l);
        }
        else
        {
          this.produced = l;
        }
      }
      return (T)localObject;
    }
    
    void runAsync()
    {
      Subscriber localSubscriber = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      long l1 = this.produced;
      int i = 1;
      for (;;)
      {
        long l3 = this.requested.get();
        while (l1 != l3)
        {
          boolean bool2 = this.done;
          try
          {
            Object localObject = localSimpleQueue.poll();
            boolean bool1;
            if (localObject == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool2, bool1, localSubscriber)) {
              return;
            }
            if (!bool1)
            {
              localSubscriber.onNext(localObject);
              long l4 = l1 + 1L;
              l1 = l4;
              if (l4 != this.limit) {
                continue;
              }
              long l2 = l3;
              if (l3 != Long.MAX_VALUE) {
                l2 = this.requested.addAndGet(-l4);
              }
              this.s.request(l4);
              l1 = 0L;
              l3 = l2;
            }
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.s.cancel();
            localSimpleQueue.clear();
            localSubscriber.onError(localThrowable);
            this.worker.dispose();
            return;
          }
        }
        if ((l1 == l3) && (checkTerminated(this.done, localSimpleQueue.isEmpty(), localSubscriber))) {
          return;
        }
        int j = get();
        if (i == j)
        {
          this.produced = l1;
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          i = j;
        }
      }
    }
    
    void runBackfused()
    {
      int i = 1;
      int j;
      do
      {
        if (this.cancelled) {
          return;
        }
        boolean bool = this.done;
        this.actual.onNext(null);
        if (bool)
        {
          Throwable localThrowable = this.error;
          if (localThrowable != null) {
            this.actual.onError(localThrowable);
          } else {
            this.actual.onComplete();
          }
          this.worker.dispose();
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    void runSync()
    {
      Subscriber localSubscriber = this.actual;
      SimpleQueue localSimpleQueue = this.queue;
      long l1 = this.produced;
      int i = 1;
      for (;;)
      {
        long l2 = this.requested.get();
        while (l1 != l2) {
          try
          {
            Object localObject = localSimpleQueue.poll();
            if (this.cancelled) {
              return;
            }
            if (localObject == null)
            {
              localSubscriber.onComplete();
              this.worker.dispose();
              return;
            }
            localSubscriber.onNext(localObject);
            l1 += 1L;
          }
          catch (Throwable localThrowable)
          {
            Exceptions.throwIfFatal(localThrowable);
            this.s.cancel();
            localSubscriber.onError(localThrowable);
            this.worker.dispose();
            return;
          }
        }
        if (this.cancelled) {
          return;
        }
        if (localSimpleQueue.isEmpty())
        {
          localSubscriber.onComplete();
          this.worker.dispose();
          return;
        }
        int j = get();
        if (i == j)
        {
          this.produced = l1;
          j = addAndGet(-i);
          i = j;
          if (j != 0) {}
        }
        else
        {
          i = j;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableObserveOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */