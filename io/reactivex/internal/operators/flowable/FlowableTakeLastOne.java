package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTakeLastOne<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableTakeLastOne(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new TakeLastOneSubscriber(paramSubscriber));
  }
  
  static final class TakeLastOneSubscriber<T>
    extends DeferredScalarSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -5467847744262967226L;
    Subscription s;
    
    TakeLastOneSubscriber(Subscriber<? super T> paramSubscriber)
    {
      super();
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      Object localObject = this.value;
      if (localObject != null) {
        complete(localObject);
      } else {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.value = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.value = paramT;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTakeLastOne.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */