package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTakeUntil<T, U>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Publisher<? extends U> other;
  
  public FlowableTakeUntil(Publisher<T> paramPublisher, Publisher<? extends U> paramPublisher1)
  {
    super(paramPublisher);
    this.other = paramPublisher1;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    TakeUntilMainSubscriber localTakeUntilMainSubscriber = new TakeUntilMainSubscriber(paramSubscriber);
    paramSubscriber.onSubscribe(localTakeUntilMainSubscriber);
    this.other.subscribe(localTakeUntilMainSubscriber.other);
    this.source.subscribe(localTakeUntilMainSubscriber);
  }
  
  static final class TakeUntilMainSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -4945480365982832967L;
    final Subscriber<? super T> actual;
    final AtomicThrowable error;
    final TakeUntilMainSubscriber<T>.OtherSubscriber other;
    final AtomicLong requested;
    final AtomicReference<Subscription> s;
    
    TakeUntilMainSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
      this.requested = new AtomicLong();
      this.s = new AtomicReference();
      this.other = new OtherSubscriber();
      this.error = new AtomicThrowable();
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this.s);
      SubscriptionHelper.cancel(this.other);
    }
    
    public void onComplete()
    {
      SubscriptionHelper.cancel(this.other);
      HalfSerializer.onComplete(this.actual, this, this.error);
    }
    
    public void onError(Throwable paramThrowable)
    {
      SubscriptionHelper.cancel(this.other);
      HalfSerializer.onError(this.actual, paramThrowable, this, this.error);
    }
    
    public void onNext(T paramT)
    {
      HalfSerializer.onNext(this.actual, paramT, this, this.error);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      SubscriptionHelper.deferredSetOnce(this.s, this.requested, paramSubscription);
    }
    
    public void request(long paramLong)
    {
      SubscriptionHelper.deferredRequest(this.s, this.requested, paramLong);
    }
    
    final class OtherSubscriber
      extends AtomicReference<Subscription>
      implements Subscriber<Object>
    {
      private static final long serialVersionUID = -3592821756711087922L;
      
      OtherSubscriber() {}
      
      public void onComplete()
      {
        SubscriptionHelper.cancel(FlowableTakeUntil.TakeUntilMainSubscriber.this.s);
        Subscriber localSubscriber = FlowableTakeUntil.TakeUntilMainSubscriber.this.actual;
        FlowableTakeUntil.TakeUntilMainSubscriber localTakeUntilMainSubscriber = FlowableTakeUntil.TakeUntilMainSubscriber.this;
        HalfSerializer.onComplete(localSubscriber, localTakeUntilMainSubscriber, localTakeUntilMainSubscriber.error);
      }
      
      public void onError(Throwable paramThrowable)
      {
        SubscriptionHelper.cancel(FlowableTakeUntil.TakeUntilMainSubscriber.this.s);
        Subscriber localSubscriber = FlowableTakeUntil.TakeUntilMainSubscriber.this.actual;
        FlowableTakeUntil.TakeUntilMainSubscriber localTakeUntilMainSubscriber = FlowableTakeUntil.TakeUntilMainSubscriber.this;
        HalfSerializer.onError(localSubscriber, paramThrowable, localTakeUntilMainSubscriber, localTakeUntilMainSubscriber.error);
      }
      
      public void onNext(Object paramObject)
      {
        SubscriptionHelper.cancel(this);
        onComplete();
      }
      
      public void onSubscribe(Subscription paramSubscription)
      {
        if (SubscriptionHelper.setOnce(this, paramSubscription)) {
          paramSubscription.request(Long.MAX_VALUE);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTakeUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */