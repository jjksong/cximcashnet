package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiPredicate;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRetryBiPredicate<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final BiPredicate<? super Integer, ? super Throwable> predicate;
  
  public FlowableRetryBiPredicate(Publisher<T> paramPublisher, BiPredicate<? super Integer, ? super Throwable> paramBiPredicate)
  {
    super(paramPublisher);
    this.predicate = paramBiPredicate;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SubscriptionArbiter localSubscriptionArbiter = new SubscriptionArbiter();
    paramSubscriber.onSubscribe(localSubscriptionArbiter);
    new RetryBiSubscriber(paramSubscriber, this.predicate, localSubscriptionArbiter, this.source).subscribeNext();
  }
  
  static final class RetryBiSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -7098360935104053232L;
    final Subscriber<? super T> actual;
    final BiPredicate<? super Integer, ? super Throwable> predicate;
    int retries;
    final SubscriptionArbiter sa;
    final Publisher<? extends T> source;
    
    RetryBiSubscriber(Subscriber<? super T> paramSubscriber, BiPredicate<? super Integer, ? super Throwable> paramBiPredicate, SubscriptionArbiter paramSubscriptionArbiter, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.sa = paramSubscriptionArbiter;
      this.source = paramPublisher;
      this.predicate = paramBiPredicate;
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        BiPredicate localBiPredicate = this.predicate;
        int i = this.retries + 1;
        this.retries = i;
        boolean bool = localBiPredicate.test(Integer.valueOf(i), paramThrowable);
        if (!bool)
        {
          this.actual.onError(paramThrowable);
          return;
        }
        subscribeNext();
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      this.sa.produced(1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.sa.setSubscription(paramSubscription);
    }
    
    void subscribeNext()
    {
      if (getAndIncrement() == 0)
      {
        int i = 1;
        int j;
        do
        {
          if (this.sa.isCancelled()) {
            return;
          }
          this.source.subscribe(this);
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRetryBiPredicate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */