package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.subscriptions.BasicQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import org.reactivestreams.Subscriber;

public final class FlowableRange
  extends Flowable<Integer>
{
  final int end;
  final int start;
  
  public FlowableRange(int paramInt1, int paramInt2)
  {
    this.start = paramInt1;
    this.end = (paramInt1 + paramInt2);
  }
  
  public void subscribeActual(Subscriber<? super Integer> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      paramSubscriber.onSubscribe(new RangeConditionalSubscription((ConditionalSubscriber)paramSubscriber, this.start, this.end));
    } else {
      paramSubscriber.onSubscribe(new RangeSubscription(paramSubscriber, this.start, this.end));
    }
  }
  
  static abstract class BaseRangeSubscription
    extends BasicQueueSubscription<Integer>
  {
    private static final long serialVersionUID = -2252972430506210021L;
    volatile boolean cancelled;
    final int end;
    int index;
    
    BaseRangeSubscription(int paramInt1, int paramInt2)
    {
      this.index = paramInt1;
      this.end = paramInt2;
    }
    
    public final void cancel()
    {
      this.cancelled = true;
    }
    
    public final void clear()
    {
      this.index = this.end;
    }
    
    abstract void fastPath();
    
    public final boolean isEmpty()
    {
      boolean bool;
      if (this.index == this.end) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public final Integer poll()
    {
      int i = this.index;
      if (i == this.end) {
        return null;
      }
      this.index = (i + 1);
      return Integer.valueOf(i);
    }
    
    public final void request(long paramLong)
    {
      if ((SubscriptionHelper.validate(paramLong)) && (BackpressureHelper.add(this, paramLong) == 0L)) {
        if (paramLong == Long.MAX_VALUE) {
          fastPath();
        } else {
          slowPath(paramLong);
        }
      }
    }
    
    public final int requestFusion(int paramInt)
    {
      return paramInt & 0x1;
    }
    
    abstract void slowPath(long paramLong);
  }
  
  static final class RangeConditionalSubscription
    extends FlowableRange.BaseRangeSubscription
  {
    private static final long serialVersionUID = 2587302975077663557L;
    final ConditionalSubscriber<? super Integer> actual;
    
    RangeConditionalSubscription(ConditionalSubscriber<? super Integer> paramConditionalSubscriber, int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.actual = paramConditionalSubscriber;
    }
    
    void fastPath()
    {
      int j = this.end;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      for (int i = this.index; i != j; i++)
      {
        if (this.cancelled) {
          return;
        }
        localConditionalSubscriber.tryOnNext(Integer.valueOf(i));
      }
      if (this.cancelled) {
        return;
      }
      localConditionalSubscriber.onComplete();
    }
    
    void slowPath(long paramLong)
    {
      int j = this.end;
      int i = this.index;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        long l2;
        if ((l1 != paramLong) && (i != j))
        {
          if (this.cancelled) {
            return;
          }
          l2 = l1;
          if (localConditionalSubscriber.tryOnNext(Integer.valueOf(i))) {
            l2 = l1 + 1L;
          }
          i++;
          l1 = l2;
        }
        else
        {
          if (i == j)
          {
            if (!this.cancelled) {
              localConditionalSubscriber.onComplete();
            }
            return;
          }
          l2 = get();
          paramLong = l2;
          if (l1 == l2)
          {
            this.index = i;
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
  
  static final class RangeSubscription
    extends FlowableRange.BaseRangeSubscription
  {
    private static final long serialVersionUID = 2587302975077663557L;
    final Subscriber<? super Integer> actual;
    
    RangeSubscription(Subscriber<? super Integer> paramSubscriber, int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.actual = paramSubscriber;
    }
    
    void fastPath()
    {
      int j = this.end;
      Subscriber localSubscriber = this.actual;
      for (int i = this.index; i != j; i++)
      {
        if (this.cancelled) {
          return;
        }
        localSubscriber.onNext(Integer.valueOf(i));
      }
      if (this.cancelled) {
        return;
      }
      localSubscriber.onComplete();
    }
    
    void slowPath(long paramLong)
    {
      int j = this.end;
      int i = this.index;
      Subscriber localSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        if ((l1 != paramLong) && (i != j))
        {
          if (this.cancelled) {
            return;
          }
          localSubscriber.onNext(Integer.valueOf(i));
          l1 += 1L;
          i++;
        }
        else
        {
          if (i == j)
          {
            if (!this.cancelled) {
              localSubscriber.onComplete();
            }
            return;
          }
          long l2 = get();
          paramLong = l2;
          if (l1 == l2)
          {
            this.index = i;
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */