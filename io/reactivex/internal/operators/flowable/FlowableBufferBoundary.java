package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableBufferBoundary<T, U extends Collection<? super T>, Open, Close>
  extends AbstractFlowableWithUpstream<T, U>
{
  final Function<? super Open, ? extends Publisher<? extends Close>> bufferClose;
  final Publisher<? extends Open> bufferOpen;
  final Callable<U> bufferSupplier;
  
  public FlowableBufferBoundary(Publisher<T> paramPublisher, Publisher<? extends Open> paramPublisher1, Function<? super Open, ? extends Publisher<? extends Close>> paramFunction, Callable<U> paramCallable)
  {
    super(paramPublisher);
    this.bufferOpen = paramPublisher1;
    this.bufferClose = paramFunction;
    this.bufferSupplier = paramCallable;
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    this.source.subscribe(new BufferBoundarySubscriber(new SerializedSubscriber(paramSubscriber), this.bufferOpen, this.bufferClose, this.bufferSupplier));
  }
  
  static final class BufferBoundarySubscriber<T, U extends Collection<? super T>, Open, Close>
    extends QueueDrainSubscriber<T, U, U>
    implements Subscription, Disposable
  {
    final Function<? super Open, ? extends Publisher<? extends Close>> bufferClose;
    final Publisher<? extends Open> bufferOpen;
    final Callable<U> bufferSupplier;
    final List<U> buffers;
    final CompositeDisposable resources;
    Subscription s;
    final AtomicInteger windows = new AtomicInteger();
    
    BufferBoundarySubscriber(Subscriber<? super U> paramSubscriber, Publisher<? extends Open> paramPublisher, Function<? super Open, ? extends Publisher<? extends Close>> paramFunction, Callable<U> paramCallable)
    {
      super(new MpscLinkedQueue());
      this.bufferOpen = paramPublisher;
      this.bufferClose = paramFunction;
      this.bufferSupplier = paramCallable;
      this.buffers = new LinkedList();
      this.resources = new CompositeDisposable();
    }
    
    public boolean accept(Subscriber<? super U> paramSubscriber, U paramU)
    {
      paramSubscriber.onNext(paramU);
      return true;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        dispose();
      }
    }
    
    void close(U paramU, Disposable paramDisposable)
    {
      try
      {
        boolean bool = this.buffers.remove(paramU);
        if (bool) {
          fastPathOrderedEmitMax(paramU, false, this);
        }
        if ((this.resources.remove(paramDisposable)) && (this.windows.decrementAndGet() == 0)) {
          complete();
        }
        return;
      }
      finally {}
    }
    
    void complete()
    {
      try
      {
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>(this.buffers);
        this.buffers.clear();
        SimplePlainQueue localSimplePlainQueue = this.queue;
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext()) {
          localSimplePlainQueue.offer((Collection)((Iterator)localObject2).next());
        }
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainMaxLoop(localSimplePlainQueue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void dispose()
    {
      this.resources.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.resources.isDisposed();
    }
    
    public void onComplete()
    {
      if (this.windows.decrementAndGet() == 0) {
        complete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      cancel();
      this.cancelled = true;
      try
      {
        this.buffers.clear();
        this.actual.onError(paramThrowable);
        return;
      }
      finally {}
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Iterator localIterator = this.buffers.iterator();
        while (localIterator.hasNext()) {
          ((Collection)localIterator.next()).add(paramT);
        }
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        FlowableBufferBoundary.BufferOpenSubscriber localBufferOpenSubscriber = new FlowableBufferBoundary.BufferOpenSubscriber(this);
        this.resources.add(localBufferOpenSubscriber);
        this.actual.onSubscribe(this);
        this.windows.lazySet(1);
        this.bufferOpen.subscribe(localBufferOpenSubscriber);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    /* Error */
    void open(Open paramOpen)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 83	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:cancelled	Z
      //   4: ifeq +4 -> 8
      //   7: return
      //   8: aload_0
      //   9: getfield 51	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:bufferSupplier	Ljava/util/concurrent/Callable;
      //   12: invokeinterface 214 1 0
      //   17: ldc -40
      //   19: invokestatic 222	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   22: checkcast 68	java/util/Collection
      //   25: astore_2
      //   26: aload_0
      //   27: getfield 49	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:bufferClose	Lio/reactivex/functions/Function;
      //   30: aload_1
      //   31: invokeinterface 228 2 0
      //   36: ldc -26
      //   38: invokestatic 222	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   41: checkcast 196	org/reactivestreams/Publisher
      //   44: astore_1
      //   45: aload_0
      //   46: getfield 83	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:cancelled	Z
      //   49: ifeq +4 -> 53
      //   52: return
      //   53: aload_0
      //   54: monitorenter
      //   55: aload_0
      //   56: getfield 83	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:cancelled	Z
      //   59: ifeq +6 -> 65
      //   62: aload_0
      //   63: monitorexit
      //   64: return
      //   65: aload_0
      //   66: getfield 56	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:buffers	Ljava/util/List;
      //   69: aload_2
      //   70: invokeinterface 231 2 0
      //   75: pop
      //   76: aload_0
      //   77: monitorexit
      //   78: new 233	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferCloseSubscriber
      //   81: dup
      //   82: aload_2
      //   83: aload_0
      //   84: invokespecial 236	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferCloseSubscriber:<init>	(Ljava/util/Collection;Lio/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber;)V
      //   87: astore_2
      //   88: aload_0
      //   89: getfield 61	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:resources	Lio/reactivex/disposables/CompositeDisposable;
      //   92: aload_2
      //   93: invokevirtual 188	io/reactivex/disposables/CompositeDisposable:add	(Lio/reactivex/disposables/Disposable;)Z
      //   96: pop
      //   97: aload_0
      //   98: getfield 45	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:windows	Ljava/util/concurrent/atomic/AtomicInteger;
      //   101: invokevirtual 239	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
      //   104: pop
      //   105: aload_1
      //   106: aload_2
      //   107: invokeinterface 200 2 0
      //   112: return
      //   113: astore_1
      //   114: aload_0
      //   115: monitorexit
      //   116: aload_1
      //   117: athrow
      //   118: astore_1
      //   119: aload_1
      //   120: invokestatic 244	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   123: aload_0
      //   124: aload_1
      //   125: invokevirtual 245	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:onError	(Ljava/lang/Throwable;)V
      //   128: return
      //   129: astore_1
      //   130: aload_1
      //   131: invokestatic 244	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   134: aload_0
      //   135: aload_1
      //   136: invokevirtual 245	io/reactivex/internal/operators/flowable/FlowableBufferBoundary$BufferBoundarySubscriber:onError	(Ljava/lang/Throwable;)V
      //   139: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	140	0	this	BufferBoundarySubscriber
      //   0	140	1	paramOpen	Open
      //   25	82	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   55	64	113	finally
      //   65	78	113	finally
      //   114	116	113	finally
      //   26	45	118	java/lang/Throwable
      //   8	26	129	java/lang/Throwable
    }
    
    void openFinished(Disposable paramDisposable)
    {
      if ((this.resources.remove(paramDisposable)) && (this.windows.decrementAndGet() == 0)) {
        complete();
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
  }
  
  static final class BufferCloseSubscriber<T, U extends Collection<? super T>, Open, Close>
    extends DisposableSubscriber<Close>
  {
    boolean done;
    final FlowableBufferBoundary.BufferBoundarySubscriber<T, U, Open, Close> parent;
    final U value;
    
    BufferCloseSubscriber(U paramU, FlowableBufferBoundary.BufferBoundarySubscriber<T, U, Open, Close> paramBufferBoundarySubscriber)
    {
      this.parent = paramBufferBoundarySubscriber;
      this.value = paramU;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.close(this.value, this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(Close paramClose)
    {
      onComplete();
    }
  }
  
  static final class BufferOpenSubscriber<T, U extends Collection<? super T>, Open, Close>
    extends DisposableSubscriber<Open>
  {
    boolean done;
    final FlowableBufferBoundary.BufferBoundarySubscriber<T, U, Open, Close> parent;
    
    BufferOpenSubscriber(FlowableBufferBoundary.BufferBoundarySubscriber<T, U, Open, Close> paramBufferBoundarySubscriber)
    {
      this.parent = paramBufferBoundarySubscriber;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.parent.openFinished(this);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(Open paramOpen)
    {
      if (this.done) {
        return;
      }
      this.parent.open(paramOpen);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableBufferBoundary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */