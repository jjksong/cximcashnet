package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.ScalarSubscription;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableScalarXMap
{
  private FlowableScalarXMap()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static <T, U> Flowable<U> scalarXMap(T paramT, Function<? super T, ? extends Publisher<? extends U>> paramFunction)
  {
    return RxJavaPlugins.onAssembly(new ScalarXMapFlowable(paramT, paramFunction));
  }
  
  public static <T, R> boolean tryScalarXMapSubscribe(Publisher<T> paramPublisher, Subscriber<? super R> paramSubscriber, Function<? super T, ? extends Publisher<? extends R>> paramFunction)
  {
    if ((paramPublisher instanceof Callable)) {
      try
      {
        paramPublisher = ((Callable)paramPublisher).call();
        if (paramPublisher == null)
        {
          EmptySubscription.complete(paramSubscriber);
          return true;
        }
        try
        {
          paramPublisher = (Publisher)ObjectHelper.requireNonNull(paramFunction.apply(paramPublisher), "The mapper returned a null Publisher");
          if ((paramPublisher instanceof Callable)) {
            try
            {
              paramPublisher = ((Callable)paramPublisher).call();
              if (paramPublisher == null)
              {
                EmptySubscription.complete(paramSubscriber);
                return true;
              }
              paramSubscriber.onSubscribe(new ScalarSubscription(paramSubscriber, paramPublisher));
            }
            catch (Throwable paramPublisher)
            {
              Exceptions.throwIfFatal(paramPublisher);
              EmptySubscription.error(paramPublisher, paramSubscriber);
              return true;
            }
          }
          paramPublisher.subscribe(paramSubscriber);
          return true;
        }
        catch (Throwable paramPublisher)
        {
          Exceptions.throwIfFatal(paramPublisher);
          EmptySubscription.error(paramPublisher, paramSubscriber);
          return true;
        }
        return false;
      }
      catch (Throwable paramPublisher)
      {
        Exceptions.throwIfFatal(paramPublisher);
        EmptySubscription.error(paramPublisher, paramSubscriber);
        return true;
      }
    }
  }
  
  static final class ScalarXMapFlowable<T, R>
    extends Flowable<R>
  {
    final Function<? super T, ? extends Publisher<? extends R>> mapper;
    final T value;
    
    ScalarXMapFlowable(T paramT, Function<? super T, ? extends Publisher<? extends R>> paramFunction)
    {
      this.value = paramT;
      this.mapper = paramFunction;
    }
    
    public void subscribeActual(Subscriber<? super R> paramSubscriber)
    {
      try
      {
        Object localObject = (Publisher)ObjectHelper.requireNonNull(this.mapper.apply(this.value), "The mapper returned a null Publisher");
        if ((localObject instanceof Callable)) {
          try
          {
            localObject = ((Callable)localObject).call();
            if (localObject == null)
            {
              EmptySubscription.complete(paramSubscriber);
              return;
            }
            paramSubscriber.onSubscribe(new ScalarSubscription(paramSubscriber, localObject));
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            EmptySubscription.error(localThrowable1, paramSubscriber);
            return;
          }
        }
        localThrowable1.subscribe(paramSubscriber);
        return;
      }
      catch (Throwable localThrowable2)
      {
        EmptySubscription.error(localThrowable2, paramSubscriber);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableScalarXMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */