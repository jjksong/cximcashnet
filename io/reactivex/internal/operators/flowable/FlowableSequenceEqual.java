package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.BiPredicate;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSequenceEqual<T>
  extends Flowable<Boolean>
{
  final BiPredicate<? super T, ? super T> comparer;
  final Publisher<? extends T> first;
  final int prefetch;
  final Publisher<? extends T> second;
  
  public FlowableSequenceEqual(Publisher<? extends T> paramPublisher1, Publisher<? extends T> paramPublisher2, BiPredicate<? super T, ? super T> paramBiPredicate, int paramInt)
  {
    this.first = paramPublisher1;
    this.second = paramPublisher2;
    this.comparer = paramBiPredicate;
    this.prefetch = paramInt;
  }
  
  public void subscribeActual(Subscriber<? super Boolean> paramSubscriber)
  {
    EqualCoordinator localEqualCoordinator = new EqualCoordinator(paramSubscriber, this.prefetch, this.comparer);
    paramSubscriber.onSubscribe(localEqualCoordinator);
    localEqualCoordinator.subscribe(this.first, this.second);
  }
  
  static final class EqualCoordinator<T>
    extends DeferredScalarSubscription<Boolean>
    implements FlowableSequenceEqual.EqualCoordinatorHelper
  {
    private static final long serialVersionUID = -6178010334400373240L;
    final BiPredicate<? super T, ? super T> comparer;
    final AtomicThrowable error;
    final FlowableSequenceEqual.EqualSubscriber<T> first;
    final FlowableSequenceEqual.EqualSubscriber<T> second;
    T v1;
    T v2;
    final AtomicInteger wip;
    
    EqualCoordinator(Subscriber<? super Boolean> paramSubscriber, int paramInt, BiPredicate<? super T, ? super T> paramBiPredicate)
    {
      super();
      this.comparer = paramBiPredicate;
      this.wip = new AtomicInteger();
      this.first = new FlowableSequenceEqual.EqualSubscriber(this, paramInt);
      this.second = new FlowableSequenceEqual.EqualSubscriber(this, paramInt);
      this.error = new AtomicThrowable();
    }
    
    public void cancel()
    {
      super.cancel();
      this.first.cancel();
      this.second.cancel();
      if (this.wip.getAndIncrement() == 0)
      {
        this.first.clear();
        this.second.clear();
      }
    }
    
    void cancelAndClear()
    {
      this.first.cancel();
      this.first.clear();
      this.second.cancel();
      this.second.clear();
    }
    
    public void drain()
    {
      if (this.wip.getAndIncrement() != 0) {
        return;
      }
      int i = 1;
      int j;
      label501:
      do
      {
        SimpleQueue localSimpleQueue1 = this.first.queue;
        SimpleQueue localSimpleQueue2 = this.second.queue;
        if ((localSimpleQueue1 != null) && (localSimpleQueue2 != null)) {
          for (;;)
          {
            if (isCancelled())
            {
              this.first.clear();
              this.second.clear();
              return;
            }
            if ((Throwable)this.error.get() != null)
            {
              cancelAndClear();
              this.actual.onError(this.error.terminate());
              return;
            }
            boolean bool2 = this.first.done;
            Object localObject2 = this.v1;
            Object localObject1 = localObject2;
            if (localObject2 == null) {
              try
              {
                localObject1 = localSimpleQueue1.poll();
                this.v1 = localObject1;
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                cancelAndClear();
                this.error.addThrowable(localThrowable1);
                this.actual.onError(this.error.terminate());
                return;
              }
            }
            if (localThrowable1 == null) {
              j = 1;
            } else {
              j = 0;
            }
            boolean bool1 = this.second.done;
            Object localObject3 = this.v2;
            localObject2 = localObject3;
            if (localObject3 == null) {
              try
              {
                localObject2 = localSimpleQueue2.poll();
                this.v2 = localObject2;
              }
              catch (Throwable localThrowable2)
              {
                Exceptions.throwIfFatal(localThrowable2);
                cancelAndClear();
                this.error.addThrowable(localThrowable2);
                this.actual.onError(this.error.terminate());
                return;
              }
            }
            int k;
            if (localObject2 == null) {
              k = 1;
            } else {
              k = 0;
            }
            if ((bool2) && (bool1) && (j != 0) && (k != 0))
            {
              complete(Boolean.valueOf(true));
              return;
            }
            if ((bool2) && (bool1) && (j != k))
            {
              cancelAndClear();
              complete(Boolean.valueOf(false));
              return;
            }
            if ((j != 0) || (k != 0)) {
              break label501;
            }
            try
            {
              bool1 = this.comparer.test(localThrowable2, localObject2);
              if (!bool1)
              {
                cancelAndClear();
                complete(Boolean.valueOf(false));
                return;
              }
              this.v1 = null;
              this.v2 = null;
              this.first.request();
              this.second.request();
            }
            catch (Throwable localThrowable3)
            {
              Exceptions.throwIfFatal(localThrowable3);
              cancelAndClear();
              this.error.addThrowable(localThrowable3);
              this.actual.onError(this.error.terminate());
              return;
            }
          }
        }
        if (isCancelled())
        {
          this.first.clear();
          this.second.clear();
          return;
        }
        if ((Throwable)this.error.get() != null)
        {
          cancelAndClear();
          this.actual.onError(this.error.terminate());
          return;
        }
        j = this.wip.addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void subscribe(Publisher<? extends T> paramPublisher1, Publisher<? extends T> paramPublisher2)
    {
      paramPublisher1.subscribe(this.first);
      paramPublisher2.subscribe(this.second);
    }
  }
  
  static abstract interface EqualCoordinatorHelper
  {
    public abstract void drain();
    
    public abstract void innerError(Throwable paramThrowable);
  }
  
  static final class EqualSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = 4804128302091633067L;
    volatile boolean done;
    final int limit;
    final FlowableSequenceEqual.EqualCoordinatorHelper parent;
    final int prefetch;
    long produced;
    volatile SimpleQueue<T> queue;
    int sourceMode;
    
    EqualSubscriber(FlowableSequenceEqual.EqualCoordinatorHelper paramEqualCoordinatorHelper, int paramInt)
    {
      this.parent = paramEqualCoordinatorHelper;
      this.limit = (paramInt - (paramInt >> 2));
      this.prefetch = paramInt;
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    void clear()
    {
      SimpleQueue localSimpleQueue = this.queue;
      if (localSimpleQueue != null) {
        localSimpleQueue.clear();
      }
    }
    
    public void onComplete()
    {
      this.done = true;
      this.parent.drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.parent.innerError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if ((this.sourceMode == 0) && (!this.queue.offer(paramT)))
      {
        onError(new MissingBackpressureException());
        return;
      }
      this.parent.drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            this.parent.drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            paramSubscription.request(this.prefetch);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.prefetch);
        paramSubscription.request(this.prefetch);
      }
    }
    
    public void request()
    {
      if (this.sourceMode != 1)
      {
        long l = this.produced + 1L;
        if (l >= this.limit)
        {
          this.produced = 0L;
          ((Subscription)get()).request(l);
        }
        else
        {
          this.produced = l;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSequenceEqual.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */