package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRefCount<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  volatile CompositeDisposable baseDisposable = new CompositeDisposable();
  final ReentrantLock lock = new ReentrantLock();
  final ConnectableFlowable<? extends T> source;
  final AtomicInteger subscriptionCount = new AtomicInteger();
  
  public FlowableRefCount(ConnectableFlowable<T> paramConnectableFlowable)
  {
    super(paramConnectableFlowable);
    this.source = paramConnectableFlowable;
  }
  
  private Disposable disconnect(final CompositeDisposable paramCompositeDisposable)
  {
    Disposables.fromRunnable(new Runnable()
    {
      public void run()
      {
        FlowableRefCount.this.lock.lock();
        try
        {
          if ((FlowableRefCount.this.baseDisposable == paramCompositeDisposable) && (FlowableRefCount.this.subscriptionCount.decrementAndGet() == 0))
          {
            FlowableRefCount.this.baseDisposable.dispose();
            FlowableRefCount localFlowableRefCount = FlowableRefCount.this;
            CompositeDisposable localCompositeDisposable = new io/reactivex/disposables/CompositeDisposable;
            localCompositeDisposable.<init>();
            localFlowableRefCount.baseDisposable = localCompositeDisposable;
          }
          return;
        }
        finally
        {
          FlowableRefCount.this.lock.unlock();
        }
      }
    });
  }
  
  private Consumer<Disposable> onSubscribe(final Subscriber<? super T> paramSubscriber, final AtomicBoolean paramAtomicBoolean)
  {
    new Consumer()
    {
      public void accept(Disposable paramAnonymousDisposable)
      {
        try
        {
          FlowableRefCount.this.baseDisposable.add(paramAnonymousDisposable);
          FlowableRefCount.this.doSubscribe(paramSubscriber, FlowableRefCount.this.baseDisposable);
          return;
        }
        finally
        {
          FlowableRefCount.this.lock.unlock();
          paramAtomicBoolean.set(false);
        }
      }
    };
  }
  
  void doSubscribe(Subscriber<? super T> paramSubscriber, CompositeDisposable paramCompositeDisposable)
  {
    paramCompositeDisposable = new ConnectionSubscriber(paramSubscriber, paramCompositeDisposable, disconnect(paramCompositeDisposable));
    paramSubscriber.onSubscribe(paramCompositeDisposable);
    this.source.subscribe(paramCompositeDisposable);
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.lock.lock();
    if (this.subscriptionCount.incrementAndGet() == 1)
    {
      AtomicBoolean localAtomicBoolean = new AtomicBoolean(true);
      try
      {
        this.source.connect(onSubscribe(paramSubscriber, localAtomicBoolean));
        if (localAtomicBoolean.get()) {
          this.lock.unlock();
        }
      }
      finally
      {
        if (localAtomicBoolean.get()) {
          this.lock.unlock();
        }
      }
    }
    try
    {
      doSubscribe(paramSubscriber, this.baseDisposable);
      return;
    }
    finally
    {
      this.lock.unlock();
    }
  }
  
  final class ConnectionSubscriber
    extends AtomicReference<Subscription>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 152064694420235350L;
    final CompositeDisposable currentBase;
    final AtomicLong requested;
    final Disposable resource;
    final Subscriber<? super T> subscriber;
    
    ConnectionSubscriber(CompositeDisposable paramCompositeDisposable, Disposable paramDisposable)
    {
      this.subscriber = paramCompositeDisposable;
      this.currentBase = paramDisposable;
      Disposable localDisposable;
      this.resource = localDisposable;
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
      this.resource.dispose();
    }
    
    void cleanup()
    {
      FlowableRefCount.this.lock.lock();
      try
      {
        if (FlowableRefCount.this.baseDisposable == this.currentBase)
        {
          FlowableRefCount.this.baseDisposable.dispose();
          FlowableRefCount localFlowableRefCount = FlowableRefCount.this;
          CompositeDisposable localCompositeDisposable = new io/reactivex/disposables/CompositeDisposable;
          localCompositeDisposable.<init>();
          localFlowableRefCount.baseDisposable = localCompositeDisposable;
          FlowableRefCount.this.subscriptionCount.set(0);
        }
        return;
      }
      finally
      {
        FlowableRefCount.this.lock.unlock();
      }
    }
    
    public void onComplete()
    {
      cleanup();
      this.subscriber.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      cleanup();
      this.subscriber.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.subscriber.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      SubscriptionHelper.deferredSetOnce(this, this.requested, paramSubscription);
    }
    
    public void request(long paramLong)
    {
      SubscriptionHelper.deferredRequest(this, this.requested, paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRefCount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */