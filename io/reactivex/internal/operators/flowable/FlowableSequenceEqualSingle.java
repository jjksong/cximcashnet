package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiPredicate;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

public final class FlowableSequenceEqualSingle<T>
  extends Single<Boolean>
  implements FuseToFlowable<Boolean>
{
  final BiPredicate<? super T, ? super T> comparer;
  final Publisher<? extends T> first;
  final int prefetch;
  final Publisher<? extends T> second;
  
  public FlowableSequenceEqualSingle(Publisher<? extends T> paramPublisher1, Publisher<? extends T> paramPublisher2, BiPredicate<? super T, ? super T> paramBiPredicate, int paramInt)
  {
    this.first = paramPublisher1;
    this.second = paramPublisher2;
    this.comparer = paramBiPredicate;
    this.prefetch = paramInt;
  }
  
  public Flowable<Boolean> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableSequenceEqual(this.first, this.second, this.comparer, this.prefetch));
  }
  
  public void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    EqualCoordinator localEqualCoordinator = new EqualCoordinator(paramSingleObserver, this.prefetch, this.comparer);
    paramSingleObserver.onSubscribe(localEqualCoordinator);
    localEqualCoordinator.subscribe(this.first, this.second);
  }
  
  static final class EqualCoordinator<T>
    extends AtomicInteger
    implements Disposable, FlowableSequenceEqual.EqualCoordinatorHelper
  {
    private static final long serialVersionUID = -6178010334400373240L;
    final SingleObserver<? super Boolean> actual;
    final BiPredicate<? super T, ? super T> comparer;
    final AtomicThrowable error;
    final FlowableSequenceEqual.EqualSubscriber<T> first;
    final FlowableSequenceEqual.EqualSubscriber<T> second;
    T v1;
    T v2;
    
    EqualCoordinator(SingleObserver<? super Boolean> paramSingleObserver, int paramInt, BiPredicate<? super T, ? super T> paramBiPredicate)
    {
      this.actual = paramSingleObserver;
      this.comparer = paramBiPredicate;
      this.first = new FlowableSequenceEqual.EqualSubscriber(this, paramInt);
      this.second = new FlowableSequenceEqual.EqualSubscriber(this, paramInt);
      this.error = new AtomicThrowable();
    }
    
    void cancelAndClear()
    {
      this.first.cancel();
      this.first.clear();
      this.second.cancel();
      this.second.clear();
    }
    
    public void dispose()
    {
      this.first.cancel();
      this.second.cancel();
      if (getAndIncrement() == 0)
      {
        this.first.clear();
        this.second.clear();
      }
    }
    
    public void drain()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      int i = 1;
      int j;
      label513:
      do
      {
        SimpleQueue localSimpleQueue2 = this.first.queue;
        SimpleQueue localSimpleQueue1 = this.second.queue;
        if ((localSimpleQueue2 != null) && (localSimpleQueue1 != null)) {
          for (;;)
          {
            if (isDisposed())
            {
              this.first.clear();
              this.second.clear();
              return;
            }
            if ((Throwable)this.error.get() != null)
            {
              cancelAndClear();
              this.actual.onError(this.error.terminate());
              return;
            }
            boolean bool2 = this.first.done;
            Object localObject2 = this.v1;
            Object localObject1 = localObject2;
            if (localObject2 == null) {
              try
              {
                localObject1 = localSimpleQueue2.poll();
                this.v1 = localObject1;
              }
              catch (Throwable localThrowable1)
              {
                Exceptions.throwIfFatal(localThrowable1);
                cancelAndClear();
                this.error.addThrowable(localThrowable1);
                this.actual.onError(this.error.terminate());
                return;
              }
            }
            if (localThrowable1 == null) {
              j = 1;
            } else {
              j = 0;
            }
            boolean bool1 = this.second.done;
            Object localObject3 = this.v2;
            localObject2 = localObject3;
            if (localObject3 == null) {
              try
              {
                localObject2 = localSimpleQueue1.poll();
                this.v2 = localObject2;
              }
              catch (Throwable localThrowable2)
              {
                Exceptions.throwIfFatal(localThrowable2);
                cancelAndClear();
                this.error.addThrowable(localThrowable2);
                this.actual.onError(this.error.terminate());
                return;
              }
            }
            int k;
            if (localObject2 == null) {
              k = 1;
            } else {
              k = 0;
            }
            if ((bool2) && (bool1) && (j != 0) && (k != 0))
            {
              this.actual.onSuccess(Boolean.valueOf(true));
              return;
            }
            if ((bool2) && (bool1) && (j != k))
            {
              cancelAndClear();
              this.actual.onSuccess(Boolean.valueOf(false));
              return;
            }
            if ((j != 0) || (k != 0)) {
              break label513;
            }
            try
            {
              bool1 = this.comparer.test(localThrowable2, localObject2);
              if (!bool1)
              {
                cancelAndClear();
                this.actual.onSuccess(Boolean.valueOf(false));
                return;
              }
              this.v1 = null;
              this.v2 = null;
              this.first.request();
              this.second.request();
            }
            catch (Throwable localThrowable3)
            {
              Exceptions.throwIfFatal(localThrowable3);
              cancelAndClear();
              this.error.addThrowable(localThrowable3);
              this.actual.onError(this.error.terminate());
              return;
            }
          }
        }
        if (isDisposed())
        {
          this.first.clear();
          this.second.clear();
          return;
        }
        if ((Throwable)this.error.get() != null)
        {
          cancelAndClear();
          this.actual.onError(this.error.terminate());
          return;
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    public void innerError(Throwable paramThrowable)
    {
      if (this.error.addThrowable(paramThrowable)) {
        drain();
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)this.first.get());
    }
    
    void subscribe(Publisher<? extends T> paramPublisher1, Publisher<? extends T> paramPublisher2)
    {
      paramPublisher1.subscribe(this.first);
      paramPublisher2.subscribe(this.second);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSequenceEqualSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */