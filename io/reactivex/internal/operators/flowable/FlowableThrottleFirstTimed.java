package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableThrottleFirstTimed<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Scheduler scheduler;
  final long timeout;
  final TimeUnit unit;
  
  public FlowableThrottleFirstTimed(Publisher<T> paramPublisher, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramPublisher);
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new DebounceTimedSubscriber(new SerializedSubscriber(paramSubscriber), this.timeout, this.unit, this.scheduler.createWorker()));
  }
  
  static final class DebounceTimedSubscriber<T>
    extends AtomicLong
    implements Subscriber<T>, Subscription, Runnable
  {
    private static final long serialVersionUID = -9102637559663639004L;
    final Subscriber<? super T> actual;
    boolean done;
    volatile boolean gate;
    Subscription s;
    final long timeout;
    final SequentialDisposable timer = new SequentialDisposable();
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    DebounceTimedSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      this.actual = paramSubscriber;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
    }
    
    public void cancel()
    {
      DisposableHelper.dispose(this.timer);
      this.worker.dispose();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      DisposableHelper.dispose(this.timer);
      this.worker.dispose();
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      DisposableHelper.dispose(this.timer);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (!this.gate)
      {
        this.gate = true;
        if (get() != 0L)
        {
          this.actual.onNext(paramT);
          BackpressureHelper.produced(this, 1L);
          paramT = (Disposable)this.timer.get();
          if (paramT != null) {
            paramT.dispose();
          }
          this.timer.replace(this.worker.schedule(this, this.timeout, this.unit));
        }
        else
        {
          this.done = true;
          cancel();
          this.actual.onError(new MissingBackpressureException("Could not deliver value due to lack of requests"));
          return;
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this, paramLong);
      }
    }
    
    public void run()
    {
      this.gate = false;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableThrottleFirstTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */