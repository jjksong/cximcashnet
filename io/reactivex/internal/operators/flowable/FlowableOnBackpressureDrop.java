package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnBackpressureDrop<T>
  extends AbstractFlowableWithUpstream<T, T>
  implements Consumer<T>
{
  final Consumer<? super T> onDrop;
  
  public FlowableOnBackpressureDrop(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
    this.onDrop = this;
  }
  
  public FlowableOnBackpressureDrop(Publisher<T> paramPublisher, Consumer<? super T> paramConsumer)
  {
    super(paramPublisher);
    this.onDrop = paramConsumer;
  }
  
  public void accept(T paramT) {}
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new BackpressureDropSubscriber(paramSubscriber, this.onDrop));
  }
  
  static final class BackpressureDropSubscriber<T>
    extends AtomicLong
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -6246093802440953054L;
    final Subscriber<? super T> actual;
    boolean done;
    final Consumer<? super T> onDrop;
    Subscription s;
    
    BackpressureDropSubscriber(Subscriber<? super T> paramSubscriber, Consumer<? super T> paramConsumer)
    {
      this.actual = paramSubscriber;
      this.onDrop = paramConsumer;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (get() != 0L)
      {
        this.actual.onNext(paramT);
        BackpressureHelper.produced(this, 1L);
      }
      else
      {
        try
        {
          this.onDrop.accept(paramT);
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          onError(paramT);
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnBackpressureDrop.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */