package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscribers.SinglePostCompleteSubscriber;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableMapNotification<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final Callable<? extends R> onCompleteSupplier;
  final Function<? super Throwable, ? extends R> onErrorMapper;
  final Function<? super T, ? extends R> onNextMapper;
  
  public FlowableMapNotification(Publisher<T> paramPublisher, Function<? super T, ? extends R> paramFunction, Function<? super Throwable, ? extends R> paramFunction1, Callable<? extends R> paramCallable)
  {
    super(paramPublisher);
    this.onNextMapper = paramFunction;
    this.onErrorMapper = paramFunction1;
    this.onCompleteSupplier = paramCallable;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    this.source.subscribe(new MapNotificationSubscriber(paramSubscriber, this.onNextMapper, this.onErrorMapper, this.onCompleteSupplier));
  }
  
  static final class MapNotificationSubscriber<T, R>
    extends SinglePostCompleteSubscriber<T, R>
  {
    private static final long serialVersionUID = 2757120512858778108L;
    final Callable<? extends R> onCompleteSupplier;
    final Function<? super Throwable, ? extends R> onErrorMapper;
    final Function<? super T, ? extends R> onNextMapper;
    
    MapNotificationSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends R> paramFunction, Function<? super Throwable, ? extends R> paramFunction1, Callable<? extends R> paramCallable)
    {
      super();
      this.onNextMapper = paramFunction;
      this.onErrorMapper = paramFunction1;
      this.onCompleteSupplier = paramCallable;
    }
    
    public void onComplete()
    {
      try
      {
        Object localObject = ObjectHelper.requireNonNull(this.onCompleteSupplier.call(), "The onComplete publisher returned is null");
        complete(localObject);
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      try
      {
        paramThrowable = ObjectHelper.requireNonNull(this.onErrorMapper.apply(paramThrowable), "The onError publisher returned is null");
        complete(paramThrowable);
        return;
      }
      catch (Throwable paramThrowable)
      {
        Exceptions.throwIfFatal(paramThrowable);
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      try
      {
        paramT = ObjectHelper.requireNonNull(this.onNextMapper.apply(paramT), "The onNext publisher returned is null");
        this.produced += 1L;
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.actual.onError(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableMapNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */