package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSwitchIfEmpty<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Publisher<? extends T> other;
  
  public FlowableSwitchIfEmpty(Publisher<T> paramPublisher, Publisher<? extends T> paramPublisher1)
  {
    super(paramPublisher);
    this.other = paramPublisher1;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SwitchIfEmptySubscriber localSwitchIfEmptySubscriber = new SwitchIfEmptySubscriber(paramSubscriber, this.other);
    paramSubscriber.onSubscribe(localSwitchIfEmptySubscriber.arbiter);
    this.source.subscribe(localSwitchIfEmptySubscriber);
  }
  
  static final class SwitchIfEmptySubscriber<T>
    implements Subscriber<T>
  {
    final Subscriber<? super T> actual;
    final SubscriptionArbiter arbiter;
    boolean empty;
    final Publisher<? extends T> other;
    
    SwitchIfEmptySubscriber(Subscriber<? super T> paramSubscriber, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.other = paramPublisher;
      this.empty = true;
      this.arbiter = new SubscriptionArbiter();
    }
    
    public void onComplete()
    {
      if (this.empty)
      {
        this.empty = false;
        this.other.subscribe(this);
      }
      else
      {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.empty) {
        this.empty = false;
      }
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.arbiter.setSubscription(paramSubscription);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSwitchIfEmpty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */