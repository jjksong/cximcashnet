package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.subscribers.DefaultSubscriber;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;

public final class BlockingFlowableMostRecent<T>
  implements Iterable<T>
{
  final T initialValue;
  final Publisher<? extends T> source;
  
  public BlockingFlowableMostRecent(Publisher<? extends T> paramPublisher, T paramT)
  {
    this.source = paramPublisher;
    this.initialValue = paramT;
  }
  
  public Iterator<T> iterator()
  {
    MostRecentSubscriber localMostRecentSubscriber = new MostRecentSubscriber(this.initialValue);
    this.source.subscribe(localMostRecentSubscriber);
    return localMostRecentSubscriber.getIterable();
  }
  
  static final class MostRecentSubscriber<T>
    extends DefaultSubscriber<T>
  {
    volatile Object value;
    
    MostRecentSubscriber(T paramT)
    {
      this.value = NotificationLite.next(paramT);
    }
    
    public Iterator<T> getIterable()
    {
      new Iterator()
      {
        private Object buf;
        
        public boolean hasNext()
        {
          this.buf = BlockingFlowableMostRecent.MostRecentSubscriber.this.value;
          return NotificationLite.isComplete(this.buf) ^ true;
        }
        
        public T next()
        {
          try
          {
            if (this.buf == null) {
              this.buf = BlockingFlowableMostRecent.MostRecentSubscriber.this.value;
            }
            if (!NotificationLite.isComplete(this.buf))
            {
              if (!NotificationLite.isError(this.buf))
              {
                localObject1 = NotificationLite.getValue(this.buf);
                return (T)localObject1;
              }
              throw ExceptionHelper.wrapOrThrow(NotificationLite.getError(this.buf));
            }
            Object localObject1 = new java/util/NoSuchElementException;
            ((NoSuchElementException)localObject1).<init>();
            throw ((Throwable)localObject1);
          }
          finally
          {
            this.buf = null;
          }
        }
        
        public void remove()
        {
          throw new UnsupportedOperationException("Read only iterator");
        }
      };
    }
    
    public void onComplete()
    {
      this.value = NotificationLite.complete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.value = NotificationLite.error(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.value = NotificationLite.next(paramT);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/BlockingFlowableMostRecent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */