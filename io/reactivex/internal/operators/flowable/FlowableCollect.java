package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCollect<T, U>
  extends AbstractFlowableWithUpstream<T, U>
{
  final BiConsumer<? super U, ? super T> collector;
  final Callable<? extends U> initialSupplier;
  
  public FlowableCollect(Publisher<T> paramPublisher, Callable<? extends U> paramCallable, BiConsumer<? super U, ? super T> paramBiConsumer)
  {
    super(paramPublisher);
    this.initialSupplier = paramCallable;
    this.collector = paramBiConsumer;
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    try
    {
      Object localObject = ObjectHelper.requireNonNull(this.initialSupplier.call(), "The initial value supplied is null");
      this.source.subscribe(new CollectSubscriber(paramSubscriber, localObject, this.collector));
      return;
    }
    catch (Throwable localThrowable)
    {
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class CollectSubscriber<T, U>
    extends DeferredScalarSubscription<U>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -3589550218733891694L;
    final BiConsumer<? super U, ? super T> collector;
    boolean done;
    Subscription s;
    final U u;
    
    CollectSubscriber(Subscriber<? super U> paramSubscriber, U paramU, BiConsumer<? super U, ? super T> paramBiConsumer)
    {
      super();
      this.collector = paramBiConsumer;
      this.u = paramU;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      complete(this.u);
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        this.collector.accept(this.u, paramT);
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCollect.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */