package io.reactivex.internal.operators.flowable;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableReduceSeedSingle<T, R>
  extends Single<R>
{
  final BiFunction<R, ? super T, R> reducer;
  final R seed;
  final Publisher<T> source;
  
  public FlowableReduceSeedSingle(Publisher<T> paramPublisher, R paramR, BiFunction<R, ? super T, R> paramBiFunction)
  {
    this.source = paramPublisher;
    this.seed = paramR;
    this.reducer = paramBiFunction;
  }
  
  protected void subscribeActual(SingleObserver<? super R> paramSingleObserver)
  {
    this.source.subscribe(new ReduceSeedObserver(paramSingleObserver, this.reducer, this.seed));
  }
  
  static final class ReduceSeedObserver<T, R>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super R> actual;
    final BiFunction<R, ? super T, R> reducer;
    Subscription s;
    R value;
    
    ReduceSeedObserver(SingleObserver<? super R> paramSingleObserver, BiFunction<R, ? super T, R> paramBiFunction, R paramR)
    {
      this.actual = paramSingleObserver;
      this.value = paramR;
      this.reducer = paramBiFunction;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      Object localObject = this.value;
      this.value = null;
      if (localObject != null)
      {
        this.s = SubscriptionHelper.CANCELLED;
        this.actual.onSuccess(localObject);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      Object localObject = this.value;
      this.value = null;
      if (localObject != null)
      {
        this.s = SubscriptionHelper.CANCELLED;
        this.actual.onError(paramThrowable);
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      Object localObject = this.value;
      if (localObject != null) {
        try
        {
          this.value = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.s.cancel();
          onError(paramT);
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableReduceSeedSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */