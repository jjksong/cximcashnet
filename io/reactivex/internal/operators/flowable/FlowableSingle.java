package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSingle<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final T defaultValue;
  
  public FlowableSingle(Publisher<T> paramPublisher, T paramT)
  {
    super(paramPublisher);
    this.defaultValue = paramT;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new SingleElementSubscriber(paramSubscriber, this.defaultValue));
  }
  
  static final class SingleElementSubscriber<T>
    extends DeferredScalarSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -5526049321428043809L;
    final T defaultValue;
    boolean done;
    Subscription s;
    
    SingleElementSubscriber(Subscriber<? super T> paramSubscriber, T paramT)
    {
      super();
      this.defaultValue = paramT;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject2 = this.value;
      this.value = null;
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = this.defaultValue;
      }
      if (localObject1 == null) {
        this.actual.onComplete();
      } else {
        complete(localObject1);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.value != null)
      {
        this.done = true;
        this.s.cancel();
        this.actual.onError(new IllegalArgumentException("Sequence contains more than one element!"));
        return;
      }
      this.value = paramT;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */