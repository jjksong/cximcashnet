package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.EmptyComponent;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDetach<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableDetach(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new DetachSubscriber(paramSubscriber));
  }
  
  static final class DetachSubscriber<T>
    implements Subscriber<T>, Subscription
  {
    Subscriber<? super T> actual;
    Subscription s;
    
    DetachSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      Subscription localSubscription = this.s;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asSubscriber();
      localSubscription.cancel();
    }
    
    public void onComplete()
    {
      Subscriber localSubscriber = this.actual;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asSubscriber();
      localSubscriber.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      Subscriber localSubscriber = this.actual;
      this.s = EmptyComponent.INSTANCE;
      this.actual = EmptyComponent.asSubscriber();
      localSubscriber.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDetach.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */