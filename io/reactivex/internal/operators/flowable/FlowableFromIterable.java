package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.subscriptions.BasicQueueSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.Iterator;
import org.reactivestreams.Subscriber;

public final class FlowableFromIterable<T>
  extends Flowable<T>
{
  final Iterable<? extends T> source;
  
  public FlowableFromIterable(Iterable<? extends T> paramIterable)
  {
    this.source = paramIterable;
  }
  
  public static <T> void subscribe(Subscriber<? super T> paramSubscriber, Iterator<? extends T> paramIterator)
  {
    try
    {
      boolean bool = paramIterator.hasNext();
      if (!bool)
      {
        EmptySubscription.complete(paramSubscriber);
        return;
      }
      if ((paramSubscriber instanceof ConditionalSubscriber)) {
        paramSubscriber.onSubscribe(new IteratorConditionalSubscription((ConditionalSubscriber)paramSubscriber, paramIterator));
      } else {
        paramSubscriber.onSubscribe(new IteratorSubscription(paramSubscriber, paramIterator));
      }
      return;
    }
    catch (Throwable paramIterator)
    {
      Exceptions.throwIfFatal(paramIterator);
      EmptySubscription.error(paramIterator, paramSubscriber);
    }
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    try
    {
      Iterator localIterator = this.source.iterator();
      subscribe(paramSubscriber, localIterator);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static abstract class BaseRangeSubscription<T>
    extends BasicQueueSubscription<T>
  {
    private static final long serialVersionUID = -2252972430506210021L;
    volatile boolean cancelled;
    Iterator<? extends T> it;
    boolean once;
    
    BaseRangeSubscription(Iterator<? extends T> paramIterator)
    {
      this.it = paramIterator;
    }
    
    public final void cancel()
    {
      this.cancelled = true;
    }
    
    public final void clear()
    {
      this.it = null;
    }
    
    abstract void fastPath();
    
    public final boolean isEmpty()
    {
      Iterator localIterator = this.it;
      boolean bool;
      if ((localIterator != null) && (localIterator.hasNext())) {
        bool = false;
      } else {
        bool = true;
      }
      return bool;
    }
    
    public final T poll()
    {
      Iterator localIterator = this.it;
      if (localIterator == null) {
        return null;
      }
      if (!this.once) {
        this.once = true;
      } else if (!localIterator.hasNext()) {
        return null;
      }
      return (T)ObjectHelper.requireNonNull(this.it.next(), "Iterator.next() returned a null value");
    }
    
    public final void request(long paramLong)
    {
      if ((SubscriptionHelper.validate(paramLong)) && (BackpressureHelper.add(this, paramLong) == 0L)) {
        if (paramLong == Long.MAX_VALUE) {
          fastPath();
        } else {
          slowPath(paramLong);
        }
      }
    }
    
    public final int requestFusion(int paramInt)
    {
      return paramInt & 0x1;
    }
    
    abstract void slowPath(long paramLong);
  }
  
  static final class IteratorConditionalSubscription<T>
    extends FlowableFromIterable.BaseRangeSubscription<T>
  {
    private static final long serialVersionUID = -6022804456014692607L;
    final ConditionalSubscriber<? super T> actual;
    
    IteratorConditionalSubscription(ConditionalSubscriber<? super T> paramConditionalSubscriber, Iterator<? extends T> paramIterator)
    {
      super();
      this.actual = paramConditionalSubscriber;
    }
    
    void fastPath()
    {
      Iterator localIterator = this.it;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        try
        {
          Object localObject = localIterator.next();
          if (this.cancelled) {
            return;
          }
          if (localObject == null)
          {
            localConditionalSubscriber.onError(new NullPointerException("Iterator.next() returned a null value"));
            return;
          }
          localConditionalSubscriber.tryOnNext(localObject);
          if (this.cancelled) {
            return;
          }
          try
          {
            boolean bool = localIterator.hasNext();
            if (bool) {
              continue;
            }
            if (!this.cancelled) {
              localConditionalSubscriber.onComplete();
            }
            return;
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            localConditionalSubscriber.onError(localThrowable1);
            return;
          }
          return;
        }
        catch (Throwable localThrowable2)
        {
          Exceptions.throwIfFatal(localThrowable2);
          localConditionalSubscriber.onError(localThrowable2);
        }
      }
    }
    
    void slowPath(long paramLong)
    {
      Iterator localIterator = this.it;
      ConditionalSubscriber localConditionalSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        if (l1 != paramLong)
        {
          if (this.cancelled) {
            return;
          }
          try
          {
            Object localObject = localIterator.next();
            if (this.cancelled) {
              return;
            }
            if (localObject == null)
            {
              localConditionalSubscriber.onError(new NullPointerException("Iterator.next() returned a null value"));
              return;
            }
            boolean bool1 = localConditionalSubscriber.tryOnNext(localObject);
            if (this.cancelled) {
              return;
            }
            try
            {
              boolean bool2 = localIterator.hasNext();
              if (!bool2)
              {
                if (!this.cancelled) {
                  localConditionalSubscriber.onComplete();
                }
                return;
              }
              if (!bool1) {
                continue;
              }
              l1 += 1L;
            }
            catch (Throwable localThrowable1)
            {
              Exceptions.throwIfFatal(localThrowable1);
              localConditionalSubscriber.onError(localThrowable1);
              return;
            }
            l2 = get();
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            localConditionalSubscriber.onError(localThrowable2);
            return;
          }
        }
        else
        {
          long l2;
          paramLong = l2;
          if (l1 == l2)
          {
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
  
  static final class IteratorSubscription<T>
    extends FlowableFromIterable.BaseRangeSubscription<T>
  {
    private static final long serialVersionUID = -6022804456014692607L;
    final Subscriber<? super T> actual;
    
    IteratorSubscription(Subscriber<? super T> paramSubscriber, Iterator<? extends T> paramIterator)
    {
      super();
      this.actual = paramSubscriber;
    }
    
    void fastPath()
    {
      Iterator localIterator = this.it;
      Subscriber localSubscriber = this.actual;
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        try
        {
          Object localObject = localIterator.next();
          if (this.cancelled) {
            return;
          }
          if (localObject == null)
          {
            localSubscriber.onError(new NullPointerException("Iterator.next() returned a null value"));
            return;
          }
          localSubscriber.onNext(localObject);
          if (this.cancelled) {
            return;
          }
          try
          {
            boolean bool = localIterator.hasNext();
            if (bool) {
              continue;
            }
            if (!this.cancelled) {
              localSubscriber.onComplete();
            }
            return;
          }
          catch (Throwable localThrowable1)
          {
            Exceptions.throwIfFatal(localThrowable1);
            localSubscriber.onError(localThrowable1);
            return;
          }
          return;
        }
        catch (Throwable localThrowable2)
        {
          Exceptions.throwIfFatal(localThrowable2);
          localSubscriber.onError(localThrowable2);
        }
      }
    }
    
    void slowPath(long paramLong)
    {
      Iterator localIterator = this.it;
      Subscriber localSubscriber = this.actual;
      long l1 = 0L;
      for (;;)
      {
        if (l1 != paramLong)
        {
          if (this.cancelled) {
            return;
          }
          try
          {
            Object localObject = localIterator.next();
            if (this.cancelled) {
              return;
            }
            if (localObject == null)
            {
              localSubscriber.onError(new NullPointerException("Iterator.next() returned a null value"));
              return;
            }
            localSubscriber.onNext(localObject);
            if (this.cancelled) {
              return;
            }
            try
            {
              boolean bool = localIterator.hasNext();
              if (!bool)
              {
                if (!this.cancelled) {
                  localSubscriber.onComplete();
                }
                return;
              }
              l1 += 1L;
            }
            catch (Throwable localThrowable1)
            {
              Exceptions.throwIfFatal(localThrowable1);
              localSubscriber.onError(localThrowable1);
              return;
            }
            l2 = get();
          }
          catch (Throwable localThrowable2)
          {
            Exceptions.throwIfFatal(localThrowable2);
            localSubscriber.onError(localThrowable2);
            return;
          }
        }
        else
        {
          long l2;
          paramLong = l2;
          if (l1 == l2)
          {
            paramLong = addAndGet(-l1);
            if (paramLong == 0L) {
              return;
            }
            l1 = 0L;
          }
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFromIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */