package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowablePublishMulticast<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final boolean delayError;
  final int prefetch;
  final Function<? super Flowable<T>, ? extends Publisher<? extends R>> selector;
  
  public FlowablePublishMulticast(Publisher<T> paramPublisher, Function<? super Flowable<T>, ? extends Publisher<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
  {
    super(paramPublisher);
    this.selector = paramFunction;
    this.prefetch = paramInt;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    MulticastProcessor localMulticastProcessor = new MulticastProcessor(this.prefetch, this.delayError);
    try
    {
      Publisher localPublisher = (Publisher)ObjectHelper.requireNonNull(this.selector.apply(localMulticastProcessor), "selector returned a null Publisher");
      localPublisher.subscribe(new OutputCanceller(paramSubscriber, localMulticastProcessor));
      this.source.subscribe(localMulticastProcessor);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class MulticastProcessor<T>
    extends Flowable<T>
    implements Subscriber<T>, Disposable
  {
    static final FlowablePublishMulticast.MulticastSubscription[] EMPTY = new FlowablePublishMulticast.MulticastSubscription[0];
    static final FlowablePublishMulticast.MulticastSubscription[] TERMINATED = new FlowablePublishMulticast.MulticastSubscription[0];
    int consumed;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final int limit;
    final int prefetch;
    volatile SimpleQueue<T> queue;
    final AtomicReference<Subscription> s;
    int sourceMode;
    final AtomicReference<FlowablePublishMulticast.MulticastSubscription<T>[]> subscribers;
    final AtomicInteger wip;
    
    MulticastProcessor(int paramInt, boolean paramBoolean)
    {
      this.prefetch = paramInt;
      this.limit = (paramInt - (paramInt >> 2));
      this.delayError = paramBoolean;
      this.wip = new AtomicInteger();
      this.s = new AtomicReference();
      this.subscribers = new AtomicReference(EMPTY);
    }
    
    boolean add(FlowablePublishMulticast.MulticastSubscription<T> paramMulticastSubscription)
    {
      FlowablePublishMulticast.MulticastSubscription[] arrayOfMulticastSubscription2;
      FlowablePublishMulticast.MulticastSubscription[] arrayOfMulticastSubscription1;
      do
      {
        arrayOfMulticastSubscription2 = (FlowablePublishMulticast.MulticastSubscription[])this.subscribers.get();
        if (arrayOfMulticastSubscription2 == TERMINATED) {
          return false;
        }
        int i = arrayOfMulticastSubscription2.length;
        arrayOfMulticastSubscription1 = new FlowablePublishMulticast.MulticastSubscription[i + 1];
        System.arraycopy(arrayOfMulticastSubscription2, 0, arrayOfMulticastSubscription1, 0, i);
        arrayOfMulticastSubscription1[i] = paramMulticastSubscription;
      } while (!this.subscribers.compareAndSet(arrayOfMulticastSubscription2, arrayOfMulticastSubscription1));
      return true;
    }
    
    void completeAll()
    {
      for (FlowablePublishMulticast.MulticastSubscription localMulticastSubscription : (FlowablePublishMulticast.MulticastSubscription[])this.subscribers.getAndSet(TERMINATED)) {
        if (localMulticastSubscription.get() != Long.MIN_VALUE) {
          localMulticastSubscription.actual.onComplete();
        }
      }
    }
    
    public void dispose()
    {
      SubscriptionHelper.cancel(this.s);
      if (this.wip.getAndIncrement() == 0)
      {
        SimpleQueue localSimpleQueue = this.queue;
        if (localSimpleQueue != null) {
          localSimpleQueue.clear();
        }
      }
    }
    
    void drain()
    {
      if (this.wip.getAndIncrement() != 0) {
        return;
      }
      Object localObject1 = this.queue;
      int i = this.consumed;
      int i1 = this.limit;
      int k;
      if (this.sourceMode != 1) {
        k = 1;
      } else {
        k = 0;
      }
      int m = 1;
      for (;;)
      {
        Object localObject3 = (FlowablePublishMulticast.MulticastSubscription[])this.subscribers.get();
        int n = localObject3.length;
        int j = i;
        if (localObject1 != null)
        {
          j = i;
          if (n != 0)
          {
            n = localObject3.length;
            long l1 = Long.MAX_VALUE;
            j = 0;
            while (j < n)
            {
              long l3 = localObject3[j].get();
              l2 = l1;
              if (l3 != Long.MIN_VALUE)
              {
                l2 = l1;
                if (l1 > l3) {
                  l2 = l3;
                }
              }
              j++;
              l1 = l2;
            }
            long l2 = 0L;
            boolean bool;
            Object localObject4;
            while (l2 != l1)
            {
              if (isDisposed())
              {
                ((SimpleQueue)localObject1).clear();
                return;
              }
              bool = this.done;
              if ((bool) && (!this.delayError))
              {
                localObject4 = this.error;
                if (localObject4 != null)
                {
                  errorAll((Throwable)localObject4);
                  return;
                }
              }
              try
              {
                localObject4 = ((SimpleQueue)localObject1).poll();
                if (localObject4 == null) {
                  j = 1;
                } else {
                  j = 0;
                }
                if ((bool) && (j != 0))
                {
                  localObject1 = this.error;
                  if (localObject1 != null) {
                    errorAll((Throwable)localObject1);
                  } else {
                    completeAll();
                  }
                  return;
                }
                if (j == 0)
                {
                  j = localObject3.length;
                  for (n = 0; n < j; n++)
                  {
                    Object localObject5 = localObject3[n];
                    if (((FlowablePublishMulticast.MulticastSubscription)localObject5).get() != Long.MIN_VALUE) {
                      ((FlowablePublishMulticast.MulticastSubscription)localObject5).actual.onNext(localObject4);
                    }
                  }
                  l2 += 1L;
                  j = i;
                  if (k != 0)
                  {
                    j = i + 1;
                    if (j == i1)
                    {
                      ((Subscription)this.s.get()).request(i1);
                      j = 0;
                    }
                  }
                  i = j;
                }
              }
              catch (Throwable localThrowable)
              {
                Exceptions.throwIfFatal(localThrowable);
                SubscriptionHelper.cancel(this.s);
                errorAll(localThrowable);
                return;
              }
            }
            if (l2 == l1)
            {
              if (isDisposed())
              {
                localThrowable.clear();
                return;
              }
              bool = this.done;
              if ((bool) && (!this.delayError))
              {
                localObject4 = this.error;
                if (localObject4 != null)
                {
                  errorAll((Throwable)localObject4);
                  return;
                }
              }
              if ((bool) && (localThrowable.isEmpty()))
              {
                localObject2 = this.error;
                if (localObject2 != null) {
                  errorAll((Throwable)localObject2);
                } else {
                  completeAll();
                }
                return;
              }
            }
            int i2 = localObject3.length;
            for (n = 0;; n++)
            {
              j = i;
              if (n >= i2) {
                break;
              }
              BackpressureHelper.produced(localObject3[n], l2);
            }
          }
        }
        this.consumed = j;
        m = this.wip.addAndGet(-m);
        if (m == 0) {
          return;
        }
        localObject3 = localObject2;
        if (localObject2 == null) {
          localObject3 = this.queue;
        }
        Object localObject2 = localObject3;
        i = j;
      }
    }
    
    void errorAll(Throwable paramThrowable)
    {
      for (FlowablePublishMulticast.MulticastSubscription localMulticastSubscription : (FlowablePublishMulticast.MulticastSubscription[])this.subscribers.getAndSet(TERMINATED)) {
        if (localMulticastSubscription.get() != Long.MIN_VALUE) {
          localMulticastSubscription.actual.onError(paramThrowable);
        }
      }
    }
    
    public boolean isDisposed()
    {
      return SubscriptionHelper.isCancelled((Subscription)this.s.get());
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.error = paramThrowable;
      this.done = true;
      drain();
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if ((this.sourceMode == 0) && (!this.queue.offer(paramT)))
      {
        ((Subscription)this.s.get()).cancel();
        onError(new MissingBackpressureException());
        return;
      }
      drain();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this.s, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.done = true;
            drain();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            QueueDrainHelper.request(paramSubscription, this.prefetch);
            return;
          }
        }
        this.queue = QueueDrainHelper.createQueue(this.prefetch);
        QueueDrainHelper.request(paramSubscription, this.prefetch);
      }
    }
    
    void remove(FlowablePublishMulticast.MulticastSubscription<T> paramMulticastSubscription)
    {
      FlowablePublishMulticast.MulticastSubscription[] arrayOfMulticastSubscription2;
      FlowablePublishMulticast.MulticastSubscription[] arrayOfMulticastSubscription1;
      do
      {
        arrayOfMulticastSubscription2 = (FlowablePublishMulticast.MulticastSubscription[])this.subscribers.get();
        if ((arrayOfMulticastSubscription2 == TERMINATED) || (arrayOfMulticastSubscription2 == EMPTY)) {
          break;
        }
        int m = arrayOfMulticastSubscription2.length;
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfMulticastSubscription2[i] == paramMulticastSubscription)
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfMulticastSubscription1 = EMPTY;
        }
        else
        {
          arrayOfMulticastSubscription1 = new FlowablePublishMulticast.MulticastSubscription[m - 1];
          System.arraycopy(arrayOfMulticastSubscription2, 0, arrayOfMulticastSubscription1, 0, j);
          System.arraycopy(arrayOfMulticastSubscription2, j + 1, arrayOfMulticastSubscription1, j, m - j - 1);
        }
      } while (!this.subscribers.compareAndSet(arrayOfMulticastSubscription2, arrayOfMulticastSubscription1));
      return;
    }
    
    protected void subscribeActual(Subscriber<? super T> paramSubscriber)
    {
      Object localObject = new FlowablePublishMulticast.MulticastSubscription(paramSubscriber, this);
      paramSubscriber.onSubscribe((Subscription)localObject);
      if (add((FlowablePublishMulticast.MulticastSubscription)localObject))
      {
        if (((FlowablePublishMulticast.MulticastSubscription)localObject).isCancelled())
        {
          remove((FlowablePublishMulticast.MulticastSubscription)localObject);
          return;
        }
        drain();
      }
      else
      {
        localObject = this.error;
        if (localObject != null) {
          paramSubscriber.onError((Throwable)localObject);
        } else {
          paramSubscriber.onComplete();
        }
      }
    }
  }
  
  static final class MulticastSubscription<T>
    extends AtomicLong
    implements Subscription
  {
    private static final long serialVersionUID = 8664815189257569791L;
    final Subscriber<? super T> actual;
    final FlowablePublishMulticast.MulticastProcessor<T> parent;
    
    MulticastSubscription(Subscriber<? super T> paramSubscriber, FlowablePublishMulticast.MulticastProcessor<T> paramMulticastProcessor)
    {
      this.actual = paramSubscriber;
      this.parent = paramMulticastProcessor;
    }
    
    public void cancel()
    {
      if (getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE)
      {
        this.parent.remove(this);
        this.parent.drain();
      }
    }
    
    public boolean isCancelled()
    {
      boolean bool;
      if (get() == Long.MIN_VALUE) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.addCancel(this, paramLong);
        this.parent.drain();
      }
    }
  }
  
  static final class OutputCanceller<R>
    implements Subscriber<R>, Subscription
  {
    final Subscriber<? super R> actual;
    final FlowablePublishMulticast.MulticastProcessor<?> processor;
    Subscription s;
    
    OutputCanceller(Subscriber<? super R> paramSubscriber, FlowablePublishMulticast.MulticastProcessor<?> paramMulticastProcessor)
    {
      this.actual = paramSubscriber;
      this.processor = paramMulticastProcessor;
    }
    
    public void cancel()
    {
      this.s.cancel();
      this.processor.dispose();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      this.processor.dispose();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      this.processor.dispose();
    }
    
    public void onNext(R paramR)
    {
      this.actual.onNext(paramR);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowablePublishMulticast.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */