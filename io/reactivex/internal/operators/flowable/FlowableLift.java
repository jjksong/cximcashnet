package io.reactivex.internal.operators.flowable;

import io.reactivex.FlowableOperator;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableLift<R, T>
  extends AbstractFlowableWithUpstream<T, R>
{
  final FlowableOperator<? extends R, ? super T> operator;
  
  public FlowableLift(Publisher<T> paramPublisher, FlowableOperator<? extends R, ? super T> paramFlowableOperator)
  {
    super(paramPublisher);
    this.operator = paramFlowableOperator;
  }
  
  public void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    try
    {
      paramSubscriber = this.operator.apply(paramSubscriber);
      if (paramSubscriber != null)
      {
        this.source.subscribe(paramSubscriber);
        return;
      }
      paramSubscriber = new java/lang/NullPointerException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append("Operator ");
      localStringBuilder.append(this.operator);
      localStringBuilder.append(" returned a null Subscriber");
      paramSubscriber.<init>(localStringBuilder.toString());
      throw paramSubscriber;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
      paramSubscriber = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
      paramSubscriber.initCause(localThrowable);
      throw paramSubscriber;
    }
    catch (NullPointerException paramSubscriber)
    {
      throw paramSubscriber;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableLift.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */