package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscribers.BasicFuseableSubscriber;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDistinct<T, K>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Callable<? extends Collection<? super K>> collectionSupplier;
  final Function<? super T, K> keySelector;
  
  public FlowableDistinct(Publisher<T> paramPublisher, Function<? super T, K> paramFunction, Callable<? extends Collection<? super K>> paramCallable)
  {
    super(paramPublisher);
    this.keySelector = paramFunction;
    this.collectionSupplier = paramCallable;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    try
    {
      Collection localCollection = (Collection)ObjectHelper.requireNonNull(this.collectionSupplier.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      this.source.subscribe(new DistinctSubscriber(paramSubscriber, this.keySelector, localCollection));
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      EmptySubscription.error(localThrowable, paramSubscriber);
    }
  }
  
  static final class DistinctSubscriber<T, K>
    extends BasicFuseableSubscriber<T, T>
  {
    final Collection<? super K> collection;
    final Function<? super T, K> keySelector;
    
    DistinctSubscriber(Subscriber<? super T> paramSubscriber, Function<? super T, K> paramFunction, Collection<? super K> paramCollection)
    {
      super();
      this.keySelector = paramFunction;
      this.collection = paramCollection;
    }
    
    public void clear()
    {
      this.collection.clear();
      super.clear();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.collection.clear();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
      }
      else
      {
        this.done = true;
        this.collection.clear();
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode == 0) {
        try
        {
          Object localObject = ObjectHelper.requireNonNull(this.keySelector.apply(paramT), "The keySelector returned a null key");
          boolean bool = this.collection.add(localObject);
          if (bool) {
            this.actual.onNext(paramT);
          } else {
            this.s.request(1L);
          }
        }
        catch (Throwable paramT)
        {
          fail(paramT);
          return;
        }
      }
      this.actual.onNext(null);
    }
    
    public T poll()
      throws Exception
    {
      Object localObject;
      for (;;)
      {
        localObject = this.qs.poll();
        if ((localObject == null) || (this.collection.add(ObjectHelper.requireNonNull(this.keySelector.apply(localObject), "The keySelector returned a null key")))) {
          break;
        }
        if (this.sourceMode == 2) {
          this.s.request(1L);
        }
      }
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDistinct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */