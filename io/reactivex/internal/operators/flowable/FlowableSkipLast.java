package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.ArrayDeque;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSkipLast<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final int skip;
  
  public FlowableSkipLast(Publisher<T> paramPublisher, int paramInt)
  {
    super(paramPublisher);
    this.skip = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new SkipLastSubscriber(paramSubscriber, this.skip));
  }
  
  static final class SkipLastSubscriber<T>
    extends ArrayDeque<T>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -3807491841935125653L;
    final Subscriber<? super T> actual;
    Subscription s;
    final int skip;
    
    SkipLastSubscriber(Subscriber<? super T> paramSubscriber, int paramInt)
    {
      super();
      this.actual = paramSubscriber;
      this.skip = paramInt;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.skip == size()) {
        this.actual.onNext(poll());
      } else {
        this.s.request(1L);
      }
      offer(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSkipLast.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */